USE [ract_prod]
GO


-- Burnie revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '224' WHERE UPPER([pf_printer]) =  N'BURCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'BURCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'BURCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Devonport revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '442' WHERE UPPER([pf_printer]) =  N'DEVCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'DEVCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '334' WHERE UPPER([pf_printer]) =  N'DEVCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Glenorchy revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '441' WHERE UPPER([pf_printer]) =  N'GLNCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'GLNCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'GLNCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Hobart revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '441' WHERE UPPER([pf_printer]) =  N'HOBCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'HOBCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'HOBCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Call Centre revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '441' WHERE UPPER([pf_printer]) =  N'HOBCUSBO1' AND LOWER([pf_form_type]) = N'memplus'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'HOBRSHBO1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Training revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '32' WHERE UPPER([pf_printer]) =  N'HOBTRNBO1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'HOBTRNBO1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Kingston revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '441' WHERE UPPER([pf_printer]) =  N'KNGCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'KNGCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'KNGCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Launceston revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '223' WHERE UPPER([pf_printer]) =  N'LAUCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '223' WHERE UPPER([pf_printer]) =  N'LAUCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Rosny revert
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '441' WHERE UPPER([pf_printer]) =  N'ROSCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'ROSCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'ROSCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO