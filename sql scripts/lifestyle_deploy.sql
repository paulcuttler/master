USE [ract_prod]
GO

-- Text values for letters/email
INSERT [PUB].[gn_text] ([sub_sys], [text_type], [text_name], [text_data], [notes]) VALUES (N'MEM', N'TANDC', N'LIFESTYLE', N'<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<termsConditions></termsConditions>', NULL)
GO
INSERT [PUB].[gn_text] ([sub_sys], [text_type], [text_name], [text_data], [notes]) VALUES (N'MEM', N'WEBADVICE', N'LIFESTYLE', N'Thank you for purchasing RACT Lifestyle through RACT on-line.

A full membership kit, including tax invoice, will be forwarded to you within the week and your new card RACT Lifestyle card will follow shortly after.

If you require any further information please call 13 27 22 or email membership@ract.com.au.

', NULL)
GO

INSERT [PUB].[gn_text] ([sub_sys], [text_type], [text_name], [text_data], [notes]) VALUES (N'MEM', N'ADVICE', N'CREATE_LIFESTYLE     ', N'Welcome to your RACT.You''ll receive your new RACT card within the next two weeks.', N'Tags <productName> <commenceDate> <expiryDate> <directDebit> ')
GO

-- Fee specifications
INSERT [PUB].[mem_fee_specification] ([fee_specification_id], [membership_type_code], [transaction_type_code], [fee_per_vehicle], [product_code], [product_benefit_code], [fee_per_member], [minimum_fee], [group_number], [effective_from_date], [effective_to_date], [fee_type_code], [additional_member_fee]) VALUES (863, N'Personal', N'Create', CAST(0.00 AS Numeric(17, 2)), N'Lifestyle', NULL, CAST(39.00 AS Numeric(17, 2)), CAST(39.00 AS Numeric(17, 2)), CAST(1 AS Numeric(15, 0)), CAST(0xA5A00000 AS SmallDateTime), CAST(0xD76F0000 AS SmallDateTime), N'Lifestyle', CAST(-1.00 AS Numeric(17, 2)))
GO
INSERT [PUB].[mem_fee_specification] ([fee_specification_id], [membership_type_code], [transaction_type_code], [fee_per_vehicle], [product_code], [product_benefit_code], [fee_per_member], [minimum_fee], [group_number], [effective_from_date], [effective_to_date], [fee_type_code], [additional_member_fee]) VALUES (864, N'Personal', N'Rejoin', CAST(0.00 AS Numeric(17, 2)), N'Lifestyle', NULL, CAST(39.00 AS Numeric(17, 2)), CAST(39.00 AS Numeric(17, 2)), CAST(1 AS Numeric(15, 0)), CAST(0xA5A00000 AS SmallDateTime), CAST(0xD76F0000 AS SmallDateTime), N'Lifestyle', CAST(-1.00 AS Numeric(17, 2)))
GO
INSERT [PUB].[mem_fee_specification] ([fee_specification_id], [membership_type_code], [transaction_type_code], [fee_per_vehicle], [product_code], [product_benefit_code], [fee_per_member], [minimum_fee], [group_number], [effective_from_date], [effective_to_date], [fee_type_code], [additional_member_fee]) VALUES (865, N'Personal', N'Change product', CAST(0.00 AS Numeric(17, 2)), N'Lifestyle', NULL, CAST(39.00 AS Numeric(17, 2)), CAST(39.00 AS Numeric(17, 2)), CAST(1 AS Numeric(15, 0)), CAST(0xA5A00000 AS SmallDateTime), CAST(0xD76F0000 AS SmallDateTime), N'Lifestyle', CAST(-1.00 AS Numeric(17, 2)))
GO
INSERT [PUB].[mem_fee_specification] ([fee_specification_id], [membership_type_code], [transaction_type_code], [fee_per_vehicle], [product_code], [product_benefit_code], [fee_per_member], [minimum_fee], [group_number], [effective_from_date], [effective_to_date], [fee_type_code], [additional_member_fee]) VALUES (866, N'Personal', N'Change option', CAST(0.00 AS Numeric(17, 2)), N'Lifestyle', NULL, CAST(39.00 AS Numeric(17, 2)), CAST(39.00 AS Numeric(17, 2)), CAST(1 AS Numeric(15, 0)), CAST(0xA5A00000 AS SmallDateTime), CAST(0xD76F0000 AS SmallDateTime), N'Lifestyle', CAST(-1.00 AS Numeric(17, 2)))
GO
INSERT [PUB].[mem_fee_specification] ([fee_specification_id], [membership_type_code], [transaction_type_code], [fee_per_vehicle], [product_code], [product_benefit_code], [fee_per_member], [minimum_fee], [group_number], [effective_from_date], [effective_to_date], [fee_type_code], [additional_member_fee]) VALUES (867, N'Personal', N'Default', CAST(0.00 AS Numeric(17, 2)), N'Lifestyle', NULL, CAST(39.00 AS Numeric(17, 2)), CAST(39.00 AS Numeric(17, 2)), CAST(1 AS Numeric(15, 0)), CAST(0xA5A00000 AS SmallDateTime), CAST(0xD76F0000 AS SmallDateTime), N'Lifestyle', CAST(-1.00 AS Numeric(17, 2)))
GO

-- Product
INSERT [PUB].[mem_product] ([product_code], [product_status], [description], [sort_order], [is_default], [credit_Product_Code], [product_identifier], [notifiable], [tier_applicable], [renewable], [groupable], [affiliate_product_level], [product_ranking], [directDebitAllowed], [vehicle_based], [upgradable]) VALUES (N'Lifestyle', N'Active', N'Lifestyle', 6, 0, N'Lifestyle', 6, NULL, 0, 0, 0, NULL, 5, 1, 0, 1)
GO

-- Product benefits
INSERT [PUB].[mem_product_benefit] ([product_code], [product_benefit_code], [sort_order]) VALUES (N'Lifestyle', N'Destinations Discount', 1)
GO
INSERT [PUB].[mem_product_benefit] ([product_code], [product_benefit_code], [sort_order]) VALUES (N'Lifestyle', N'United Fuel Discount', 7)
GO
INSERT [PUB].[mem_product_benefit_type] ([product_benefit_code], [product_benefit_status], [description], [optional], [selected_By_Default], [priv_id]) VALUES (N'Destinations Discount', N'Active', N'You are entitled to member only discounts at RACT/RACV Hobart Apartment Hotel, Freycinet Lodge, Cradle Mountain Hotel, Strahan Village and Gordon River Cruises', 0, 0, N'                    ')
GO
INSERT [PUB].[mem_product_benefit_type] ([product_benefit_code], [product_benefit_status], [description], [optional], [selected_By_Default], [priv_id]) VALUES (N'United Fuel Discount', N'Active', N'You''ll get 6c per litre discount at United Petroleum outletes when you present your discount card.', 0, 0, N'                    ')
GO

-- Fee type
INSERT [PUB].[mem_fee_type] ([fee_type_code], [fee_status], [unearned_account_id], [description], [earned_account_id], [termMultiplier], [onRoad], [joinType]) VALUES (N'Lifestyle', N'Active', CAST(26 AS Numeric(15, 0)), N'Subscription fee for Lifestyle product.', CAST(25 AS Numeric(15, 0)), 0, 0, 0)
GO

-- Accounts
INSERT [PUB].[mem_membership_account] ([account_id], [account_title], [account_number], [gst_account], [clearing_account], [cost_center], [sub_account], [suspense_account], [dd_clearing_account], [taxable], [writeOff]) VALUES (CAST(25 AS Numeric(15, 0)), N'Lifestyle earned income account', N'4011', 0, 0, N'11', N'', 0, 0, 0, 0)
GO
INSERT [PUB].[mem_membership_account] ([account_id], [account_title], [account_number], [gst_account], [clearing_account], [cost_center], [sub_account], [suspense_account], [dd_clearing_account], [taxable], [writeOff]) VALUES (CAST(26 AS Numeric(15, 0)), N'Lifestyle unearned income account', N'2327', 0, 0, N'10', N'', 0, 0, 0, 0)
GO

-- Allowed discounts
INSERT [PUB].[mem_allowed_fee_discount] ([fee_type_code], [discount_type_code]) VALUES (N'Lifestyle', N'Write Off')
GO

-- Reference data
INSERT [PUB].[gn_reference] ([ref_type], [ref_code], [description], [sort_order], [ref_active]) VALUES (N'MEMJOIN', N'LS', N'Lifestyle', 0, 1)
GO

-- Fix product ranking and sort order
update [PUB].[mem_product] set sort_order = 4 where product_code = 'Access'
GO
update [PUB].[mem_product] set sort_order = 3, product_ranking = 3 where product_code = 'Lifestyle'
GO
update [PUB].[mem_product] set product_ranking = 5 where product_code = 'Non-Motoring'
GO