USE [ract_prod]
GO

-- NEW memPlusNew printer/option/tray config

-- Burnie
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'bur', N'memPlusNew', N'BURCTRFC1', N'244', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'bur-ctr1', N'memPlusNew', N'BURCTRFC1', N'244', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'bur-ctr2', N'memPlusNew', N'BURCTRFC1', N'244', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'bur-ctr3', N'memPlusNew', N'BURCTRFC1', N'244', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'BUR-TRV', N'memPlusNew', N'BURCTRFC1', N'244', N'manual')
GO

-- Devonport
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'dev', N'memPlusNew', N'DEVCTRFC1', N'422', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'dev-ctr1', N'memPlusNew', N'DEVCTRFC1', N'422', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'dev-ctr2', N'memPlusNew', N'DEVCTRFC1', N'422', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'dev-ctr3', N'memPlusNew', N'DEVCTRFC1', N'422', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'dev-ctr4', N'memPlusNew', N'DEVCTRFC1', N'422', N'tray 1')
GO

-- Glenorchy
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'gln', N'memPlusNew', N'GLNCTRFC1', N'322', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'gln-ctr1', N'memPlusNew', N'GLNCTRFC1', N'322', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'gln-ctr2', N'memPlusNew', N'GLNCTRFC1', N'322', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'gln-ctr3', N'memPlusNew', N'GLNCTRFC1', N'322', N'tray 1')
GO

-- Hobart
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'hob-csh1', N'memPlusNew', N'HOBCTRFC1', N'411', N'tray 2')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'hob-csh2', N'memPlusNew', N'HOBCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'hob-csh3', N'memPlusNew', N'HOBCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'hob-csh4', N'memPlusNew', N'HOBCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'hob-csh5', N'memPlusNew', N'HOBCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'HOB-FC2', N'memPlusNew', N'HOBCTRFC1', N'411', N'manual')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'hob-rcp1', N'memPlusNew', N'HOBCTRFC1', N'411', NULL)
GO

-- Call Centre
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'hob-d2', N'memPlusNew', N'HOBCUSBO1', N'233', N'tray 4')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'HOB-RS', N'memPlusNew', N'HOBRSHBO1', N'233', N'tray 4')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'HOB-DL', N'memPlusNew', N'HOBCUSBO1', N'233', N'tray 2')
GO

-- Training
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'HOB-TR', N'memPlusNew', N'HOBTRNBO1', N'233', N'tray 1')
GO

-- IT-DEV
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'it-dev', N'memPlusNew', N'ITDEVEXT', N'111', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'it-devf', N'memPlusNew', N'ITDEVEXT', N'111', N'xx')
GO

-- Kingston
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'kng', N'memPlusNew', N'KNGCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'kng-ctr1', N'memPlusNew', N'KNGCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'kng-ctr2', N'memPlusNew', N'KNGCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'kng-ctr3', N'memPlusNew', N'KNGCTRFC1', N'433', N'tray 1')
GO

-- Launceston
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'LAU', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'LAU-CSH', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'LAU-CSH1', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'LAU-CSH2', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'LAU-CSH3', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'lau-ctr1', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'lau-ctr2', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'lau-ctr3', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'lau-ctr4', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'lau-ctr5', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'lau-ctr6', N'memPlusNew', N'LAUCTRFC1', N'433', N'tray 1')
GO

-- Rosny
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'ros-ctr1', N'memPlusNew', N'ROSCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'ros-ctr2', N'memPlusNew', N'ROSCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'ros-ctr3', N'memPlusNew', N'ROSCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'ros-off', N'memPlusNew', N'ROSCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'ros-off2', N'memPlusNew', N'ROSCTRFC1', N'411', N'tray 1')
GO
INSERT [PUB].[x_prnt_form] ([pf_print_grp], [pf_form_type], [pf_printer], [pf_form_options], [pf_tray]) VALUES (N'ROS-TRV', N'memPlusNew', N'ROSCTRFC1', N'411', N'manual')
GO

-- Update to existing definitions

-- Burnie
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '444' WHERE UPPER([pf_printer]) =  N'BURCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'BURCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '112' WHERE UPPER([pf_printer]) =  N'BURCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Devonport
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '222' WHERE UPPER([pf_printer]) =  N'DEVCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '333' WHERE UPPER([pf_printer]) =  N'DEVCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '332' WHERE UPPER([pf_printer]) =  N'DEVCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Glenorchy
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '222' WHERE UPPER([pf_printer]) =  N'GLNCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'GLNCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '112' WHERE UPPER([pf_printer]) =  N'GLNCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Hobart
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '333' WHERE UPPER([pf_printer]) =  N'HOBCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '222' WHERE UPPER([pf_printer]) =  N'HOBCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'HOBCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Call Centre
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '443' WHERE UPPER([pf_printer]) =  N'HOBCUSBO1' AND LOWER([pf_form_type]) = N'memplus'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '443' WHERE UPPER([pf_printer]) =  N'HOBRSHBO1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Training
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '32' WHERE UPPER([pf_printer]) =  N'HOBTRNBO1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'HOBTRNBO1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Kingston
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'KNGCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '222' WHERE UPPER([pf_printer]) =  N'KNGCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'KNGCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Launceston
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '223' WHERE UPPER([pf_printer]) =  N'LAUCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '223' WHERE UPPER([pf_printer]) =  N'LAUCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO

-- Rosny
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '111' WHERE UPPER([pf_printer]) =  N'ROSCTRFC1' AND LOWER([pf_form_type]) = N'blank'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '222' WHERE UPPER([pf_printer]) =  N'ROSCTRFC1' AND LOWER([pf_form_type]) = N'letterhead'
GO
UPDATE [PUB].[x_prnt_form] set [pf_form_options] = '221' WHERE UPPER([pf_printer]) =  N'ROSCTRFC1' AND LOWER([pf_form_type]) = N'memplus'
GO