update [ract_prod].[PUB].[gn_text] set text_data = N'Thank you for upgrading to RACT ROADSIDE ULTIMATE cover. 

You can continue to use your existing RACT member card to access Roadside service, but if you need to call for roadside service in the meantime please call 13 11 11 and quote your membership number shown above. 

If your previous ROADSIDE cover had not yet expired, the unexpired portion of that fee has been credited towards the new fee where appropriate. 

Please take a moment to read the enclosed information to make sure that you are fully aware of the comprehensive cover that you now have with RACT ROADSIDE ULTIMATE. If you have any questions or if we can be of any further assistance please do not hesitate to call.

<directDebit> ' where sub_sys = 'MEM' and text_type = 'ADVICE' and text_name like 'UPGRADE_ULTIMATE%'