<%@ page import="com.ract.common.*"%>

<!--
should be in Common
 * HTML include to make the admin maintenace lists the same
-->

<%
//get parameters from the main form
String title = (String)request.getAttribute("title");
String button = (String)request.getAttribute("button");
String uicPage = (String)request.getAttribute("uicPage");
String event = (String)request.getAttribute("event");
String msg = (String)request.getAttribute("msg");
String altButton = (String)request.getAttribute("altButton");
%>

  <table border="0" width="100%" cellpadding="5" cellspacing="2">
    <tr>
      <td class="listHeading" align = "center" >
          <%=title%>
      </td>
    </tr>
    <tr>
      <td align="center" >
         <a>
            <img name="AddButton"
                 src="<%=button%>"
                 onclick="addNew('<%=uicPage%>','<%=event%>')"
                 onMouseOver="mouseOverAdd('<%=msg%>','<%=altButton%>')"
                 onMouseOut="mouseOutAdd()"
                 border="0"
                 alt="<%=msg%>"
            >
        </a>
      </td>
    </tr>

 </table>