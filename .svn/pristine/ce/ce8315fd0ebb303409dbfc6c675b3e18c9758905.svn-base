package com.ract.common;

/**
 * Implementation of modified for EJB performance enhancement.
 * 
 * <h3>Changes to EJB to stop redundant stores (important for performance)</h3>
 * 
 * <b>In ejbStore of beanBMP class</b>
 * <p>
 * 
 * <pre>
 * public void ejbStore() throws RemoteException {
 *   // at the end of the code
 *   setModified(false);
 * </pre>
 * 
 * </p>
 * 
 * <b>In ejbPostCreate of bean class</b>
 * <p>
 * 
 * <pre>
 * public void ejbPostCreate(...) throws RemoteException {
 *   setModified(false);
 * </pre>
 * 
 * </p>
 * 
 * <b>In ejbLoad of beanBMP class</b>
 * <p>
 * 
 * <pre>
 * public void ejbLoad() throws RemoteException {
 *   //at the start of the code
 *   setModified(false);
 * </pre>
 * 
 * </p>
 * 
 * <b>In value object (assuming EJB extends VO) otherwise it must be in the
 * beanBMP class</b>
 * <p>
 * 
 * <pre>
 * protected boolean modified;
 * 
 * public boolean isModified() {
 *     return modified;
 * }
 * </pre>
 * 
 * </p>
 * 
 * <b>In all sets methods</b>
 * <p>
 * 
 * <pre>
 * public void setSurname(String surname) {
 *     this.surname = surname;
 *     setModified(true);
 * }
 * </pre>
 * 
 * </p>
 * 
 * @author John Holliday
 * @version 1.0
 */

public abstract class Mutable {

    /* should be used instead of setting it directly */
    public void setModified(boolean modified) {
	this.modified = modified;
    }

    public boolean isModified() {
	return modified;
    }

    private boolean modified = false;

}