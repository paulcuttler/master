package com.ract.membership;

import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

import com.ract.client.ClientVO;
import com.ract.common.CardHelper;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.DataSourceFactory;
import com.ract.common.ExtractControl;
import com.ract.common.GenericException;
import com.ract.common.PostalAddressVO;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.util.ConnectionUtil;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * Create a membership card extract file.
 * 
 * @author dgk
 * @created 5 March 2003
 * @version 1.0
 */

public class CardMgrBean implements SessionBean {
	private SessionContext sessionContext;

	private DataSource dataSource = null;

	private final String DELIMITER = "|";

	/**
	 * Run the card extract.
	 * 
	 * @return hashtable containing various counts.
	 * @exception RemoteException Description of the Exception
	 */
	public synchronized Hashtable doCardExtract(ArrayList productList, String userId, boolean eligibleLoyaltyScheme, boolean newAccessOnly, boolean cmoOnly) throws RemoteException {
		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		try {
			commonMgrLocal.startExtract(ExtractControl.EXTRACT_CARD, userId);
		} catch (GenericException e1) {
			throw new RemoteException(e1.getMessage());
		}

		Hashtable extractCounts = new Hashtable();

		LogUtil.log(this.getClass(), "productList=" + productList);
		LogUtil.log(this.getClass(), "eligibleLoyaltyScheme=" + eligibleLoyaltyScheme);
		LogUtil.log(this.getClass(), "newAccessOnly=" + newAccessOnly);
		LogUtil.log(this.getClass(), "cmoOnly=" + cmoOnly);

		// ensure a unique card file name.
		final SimpleDateFormat sd = new SimpleDateFormat("ddMMyyyyHHmmssSSS");

		final String CODE_LOYALTY = "loyalty";
		final String CARD_FILE_NAME = "memcard";

		// these values must match entries in gn_reference table
		final String CARD_REASON_ACCESS_INSURANCE = "ACCINS";
		final String CARD_REASON_ACCESS_TRAVEL = "ACCTRV";

		List<String> permissableAccessReasonCodes = new ArrayList<String>();
		permissableAccessReasonCodes.add(CARD_REASON_ACCESS_INSURANCE);
		permissableAccessReasonCodes.add(CARD_REASON_ACCESS_TRAVEL);

		int successfulCardCount = 0;
		int recordCount = 0;
		int errorCardCount = 0;

		String suffix = sd.format(new DateTime());

		Hashtable cardFiles = null;
		FileWriter extractFile = null;
		FileWriter errorFile = null;
		String extractFileName = null;
		String baseFileName = null;
		String errorFileName = null;
		String logFileName = null;
		FileWriter logFile = null;
		String directoryName = null;
		try {
			DateTime today = new DateTime();
			directoryName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DIRECTORY_MEMBERSHIP_EXTRACTS);
			extractFileName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.CARD_FILE_NAME);
			if (baseFileName == null) {
				baseFileName = CARD_FILE_NAME;
			}
			if (eligibleLoyaltyScheme) {
				baseFileName += CODE_LOYALTY;
			}
			if (cmoOnly) {
				baseFileName += "CMO"; // @todo should be looked up
			}

			// The IIN is used to construct the card number
			String IIN = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_CARD_IIN);

			logFileName = baseFileName + ".log";

			if (!newAccessOnly) {
				// construct normal file
				extractFileName = directoryName + baseFileName + suffix + "." + FileUtil.EXTENSION_TXT;
				extractFile = new FileWriter(extractFileName);
			} else {
				// prepare for multiple card files
				cardFiles = new Hashtable();
			}

			errorFileName = baseFileName + suffix + ".err";
			errorFile = new FileWriter(directoryName + errorFileName);
			logFile = new FileWriter(directoryName + logFileName, true);
			logFile.write(FileUtil.NEW_LINE + FileUtil.NEW_LINE + "Card Extract commenced " + new java.util.Date());
			logFile.write(FileUtil.NEW_LINE + "Data file = " + extractFileName);
			StringBuffer outLine = null;
			RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();

			ArrayList extractList = new ArrayList();
			String productCode = null;

			if (newAccessOnly) {
				extractList.addAll(findCardsForExtract(null, false, newAccessOnly));
			} else {
				// allow for tier transition only cards
				for (int i = 0; i < productList.size(); i++) {
					productCode = (String) productList.get(i);
					extractList.addAll(findCardsForExtract(productCode, eligibleLoyaltyScheme, newAccessOnly));
				}
			}

			LogUtil.log(this.getClass(), "Extract list has " + extractList.size() + " card requests");

			MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
			MembershipCardVO cardVO = null;
			MembershipVO memVO = null;

			// product-specific card expiry
			String memCardPeriodSpec = null;
			if (productCode != null) {
				memCardPeriodSpec = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, productCode.toUpperCase() + SystemParameterVO.DEFAULT_CARD_PERIOD);
			}
			// default card expiry
			if (memCardPeriodSpec == null) {
				memCardPeriodSpec = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DEFAULT_CARD_PERIOD);
			}

			Interval memCardPeriod = new Interval(memCardPeriodSpec);
			DateTime expiryDate = today.add(memCardPeriod);

			String cardExpiry = DateUtil.formatDate(expiryDate, "MMyy");
			ClientVO client = null;
			PostalAddressVO postalVO = null;
			String address1 = null;
			String address2 = null;
			String address3 = null;
			ProductVO productVO;
			String social = null;
			String firstName = null;
			String secondInitial = null;
			String membershipTier = null;
			DateTime lastDate = null;
			DateTime firstDate = null;
			double amountOutstanding = 0;
			UserTransaction userTransaction = this.sessionContext.getUserTransaction();
			Integer cardId;
			String reasonCode = null;
			logFile.write(FileUtil.NEW_LINE + "Extract list has " + extractList.size() + " card requests");
			for (int x = 0; x < extractList.size(); x++) {
				recordCount++;
				userTransaction.begin();
				// Fetch the VO for the membership card request.
				try {
					cardId = (Integer) extractList.get(x);
					cardVO = membershipMgrLocal.getMembershipCard(cardId);
				} catch (Exception e) {
					errorCardCount++;
					errorFile.write(FileUtil.NEW_LINE + "Unable to retrieve cardData for cardID " + extractList.get(x) + ": " + e);
					userTransaction.rollback();
					continue;
				}
				try {
					memVO = membershipMgrLocal.getMembership(cardVO.getMembershipID());
					if (MembershipVO.STATUS_CANCELLED.equals(memVO.getStatus())) {
						throw new ValidationException("Membership '" + memVO.getMembershipNumber() + "' has been cancelled.");
					}

					if (newAccessOnly) {
						reasonCode = cardVO.getReasonCode();

						// only generate specific extracts for certain types of
						// reason codes
						if (!permissableAccessReasonCodes.contains(reasonCode)) {
							reasonCode = "";
						}

						extractFileName = directoryName + baseFileName + suffix + reasonCode + "." + FileUtil.EXTENSION_TXT;
						LogUtil.log(this.getClass(), "extractFileName=" + extractFileName);
						extractFile = (FileWriter) cardFiles.get(extractFileName);
						if (extractFile == null) {
							extractFile = new FileWriter(extractFileName);
							LogUtil.log(this.getClass(), "adding file to hashtable");
							cardFiles.put(extractFileName, extractFile);
						}
					}

					// check if there is an amount outstanding on the client
					amountOutstanding = memVO.getAmountOutstanding(true, false);
					if (amountOutstanding > 0) {
						errorFile.write(FileUtil.NEW_LINE + "Client number " + memVO.getClientNumber() + " has an amount outstanding of " + CurrencyUtil.formatDollarValue(amountOutstanding) + " in the receipting system.");
						errorCardCount++;
						userTransaction.rollback();
						continue;
					}

					// check PA as well.
					MembershipGroupVO memGroup = memVO.getMembershipGroup();
					if (memGroup != null) {
						MembershipVO memPA = memGroup.getMembershipGroupDetailForPA().getMembership();
						amountOutstanding = memPA.getAmountOutstanding(true, false);
						if (amountOutstanding > 0) {
							errorFile.write(FileUtil.NEW_LINE + "Client number " + memPA.getClientNumber() + " (PA) has an amount outstanding of " + CurrencyUtil.formatDollarValue(amountOutstanding) + " in the receipting system.");
							errorCardCount++;
							userTransaction.rollback();
							continue;
						}
					}

					// check client type
					client = memVO.getClient();

					if (!client.isActive()) {
						throw new ValidationException("Client '" + client.getClientNumber() + "' is not active - " + client.getStatus() + ".");
					}

					if (!client.getClientType().equals(ClientVO.CLIENT_TYPE_PERSON)) {
						throw new ValidationException("Client '" + client.getClientNumber() + "' is not a person (" + client.getClientType() + "). A card may only be issued to person type clients.");
					}

					productVO = memVO.getProduct();

					if (!productVO.hasCard()) {
						errorFile.write(FileUtil.NEW_LINE + "Invalid product type for client number " + memVO.getClientNumber());
						errorCardCount++;
						userTransaction.rollback();
						continue;
					}

					// Change the card VO from a card request to an issued card
					cardVO.setProduct(productVO);
					cardVO.setCardNumber(IIN, memVO.getMembershipNumber(7));
					cardVO.setExpiryDate(expiryDate);
					cardVO.setIssueDate(today);
					cardVO.setJoinDate(memVO.getJoinDate());
					cardVO.setCardName(CardHelper.getCardName(memVO.getClient(), memVO.getMembershipProfile().getNameSuffix()));
					outLine = new StringBuffer();
					outLine.append(memVO.isPrimeAddressee() ? "Y" : "N"); // 1
					outLine.append(DELIMITER);
					outLine.append(client.getPostalName()); // 2
					outLine.append(DELIMITER);
					outLine.append(client.getAddressTitle()); // 3
					outLine.append(DELIMITER);
					postalVO = client.getPostalAddress();
					address3 = postalVO.getSuburb() + "   " + postalVO.getState() + " " + postalVO.getPostcode();
					address2 = postalVO.getAddressLine2();
					address1 = postalVO.getAddressLine1();
					outLine.append(address1); // 4
					outLine.append(DELIMITER);
					outLine.append(address2); // 5
					outLine.append(DELIMITER);
					outLine.append(address3); // 6
					outLine.append(DELIMITER);
					outLine.append(productVO.getProductIdentifier()); // 7
					outLine.append(DELIMITER);
					membershipTier = cardVO.getTierCode() != null ? cardVO.getTierCode() : String.valueOf(MembershipTier.TIER_CODE_DEFAULT);
					outLine.append(membershipTier); // 8 Changed 07/09/2005
					outLine.append(DELIMITER);
					outLine.append(memVO.getMembershipNumber()); // 9
					outLine.append(DELIMITER);
					outLine.append(memVO.isGroupMember() ? "J" : "P"); // 10
					outLine.append(DELIMITER);
					social = renewalMgr.isSocialMember(memVO.getMembershipID()) ? "S" : " ";
					outLine.append(social); // 11
					outLine.append(DELIMITER);
					outLine.append(cardVO.getCardNumber()); // 12
					outLine.append(DELIMITER);
					outLine.append(cardVO.getCardName()); // 13
					outLine.append(DELIMITER);
					// The next field has the membership number padded out with
					// leading zeros to 7 characters
					outLine.append(memVO.getMembershipNumber(7)); // 14
					outLine.append(DELIMITER);
					outLine.append(memVO.getJoinDate().getDateYear()); // 15
					outLine.append(DELIMITER);
					outLine.append("%B" + cardVO.getCardNumber() + "^" + CardHelper.getTrackName(client) + "^" + cardExpiry + "711" + productVO.getProductIdentifier() + membershipTier + memVO.getJoinDate().getDateYear() + "?"); // 16
					outLine.append(DELIMITER);
					outLine.append(cardVO.getCardNumber() + "=" + cardExpiry + "711" + productVO.getProductIdentifier() + membershipTier + "00000000000"); // 16
					outLine.append(DELIMITER);
					// discount
					outLine.append(getDiscountStatus(memVO));
					outLine.append(DELIMITER);

					if (productVO.isVehicleBased()) {
						outLine.append(cardVO.getRego());
					} else {
						outLine.append("");
					}
					outLine.append(DELIMITER);

					boolean replacement = true;
					boolean issued = false;

					// A "new" card is first time, or change from "Access" to
					// "Roadside" and "replacement" any subsequent card
					List<MembershipCardVO> cardList = (List<MembershipCardVO>) membershipMgrLocal.findMembershipCardsByMembership(memVO.getMembershipID());
					if (cardList != null && !cardList.isEmpty()) {
						Collections.reverse(cardList); // most recent first

						for (MembershipCardVO lastCard : cardList) {
							if (lastCard.getCardID() == cardVO.getCardID()) {
								continue; // skip
							}

							// get last issued card
							if (lastCard.getIssueDate() != null) {
								// change from "Access" to "Roadside" - treat as
								// New
								if (lastCard.getProductCode().equals(ProductVO.PRODUCT_ACCESS) && !cardVO.getProductCode().equals(ProductVO.PRODUCT_ACCESS)) {
									replacement = false;
								}
								issued = true;
								break;
							}
						}

						if (!issued) {
							// no cards have been issued - treat as New
							replacement = false;
						}
					}

					outLine.append(replacement ? 'R' : 'N');
					outLine.append(DELIMITER);

					// print profile code for CMO
					if (productVO.isVehicleBased()) {
						outLine.append(memVO.getMembershipProfileCode());
					} else {
						outLine.append("");
					}
					outLine.append(DELIMITER);

					outLine.append(FileUtil.NEW_LINE);

					extractFile.write(outLine.toString());

					// Now update the membership card request into an issued
					// card
					membershipMgrLocal.updateMembershipCard(cardVO);

					if (recordCount == 1) {
						firstDate = cardVO.getRequestDate();
					}
					lastDate = cardVO.getRequestDate();
				} catch (Exception cardE) {
					if (memVO == null) {
						errorFile.write(FileUtil.NEW_LINE + "Cannot get membership data for memberID = " + cardVO.getMembershipID() + ": " + cardE);
					} else {
						errorFile.write(FileUtil.NEW_LINE + "Error Extracting data for client " + memVO.getClientNumber() + ": " + cardE.fillInStackTrace());
					}
					errorCardCount++;
					userTransaction.rollback();
					continue;
				}
				userTransaction.commit();
			}

			successfulCardCount = recordCount - errorCardCount;

			errorFile.write(FileUtil.NEW_LINE + "Card requests successfully processed = " + successfulCardCount + FileUtil.NEW_LINE + "Request causing errors = " + errorCardCount);
			errorFile.write(FileUtil.NEW_LINE + "First card request date = " + firstDate);
			errorFile.write(FileUtil.NEW_LINE + "Last card request date = " + lastDate);
			logFile.write(FileUtil.NEW_LINE + "FINISHED " + new java.util.Date() + FileUtil.NEW_LINE);
			if (extractFile != null) {
				extractFile.write("End of File.");
			}

			extractCounts.put(CardMgr.COUNT_CARD_FAILURE, new Integer(errorCardCount));
			extractCounts.put(CardMgr.COUNT_CARD_SUCCESS, new Integer(successfulCardCount));

		} catch (Exception mainE) {
			throw new RemoteException("Unable to complete the card extract.", mainE);
		} finally {
			try {
				// close hashtable of filewriters
				if (cardFiles != null) {
					Collection extractFiles = cardFiles.values();
					if (extractFiles != null && !extractFiles.isEmpty()) {
						LogUtil.log(this.getClass(), "extractFileNames=" + extractFiles.size());
						FileWriter tmpWriter = null;
						Iterator extractFileIt = extractFiles.iterator();
						while (extractFileIt.hasNext()) {
							tmpWriter = (FileWriter) extractFileIt.next();
							tmpWriter.flush();
							tmpWriter.close();
						}
					}
					// make extract file null as we have already have closed the
					// reference
					extractFile = null;
				}
				if (extractFile != null) {
					extractFile.close();
				}
				if (errorFile != null) {
					errorFile.close();
				}
				if (logFile != null) {
					logFile.close();
				}
			} catch (IOException ioe) {
				LogUtil.warn(this.getClass(), "Unable to close files in doCardExtract");
			}

			try {
				commonMgrLocal.endExtract(ExtractControl.EXTRACT_CARD);
			} catch (GenericException e) {
				LogUtil.warn(this.getClass(), e.getMessage());
			}
		}
		return extractCounts;
	}

	/**
	 * Extract collection of card for creation of membership cards Only extracts cards for which the issue date is null;
	 * 
	 * @return Collection of primary keys
	 * @exception FinderException Description of the Exception
	 * @throws RemoteException
	 */
	private ArrayList findCardsForExtract(String productCode, boolean eligibleLoyaltyScheme, boolean newAccessOnly) throws RemoteException {

		LogUtil.debug(this.getClass(), "findCardsForExtract " + productCode + " " + eligibleLoyaltyScheme + " " + newAccessOnly);
		ArrayList keys = null;
		Connection connection = null;
		String statementText = null;
		PreparedStatement statement = null;
		int numberToProcess = 0;
		try {
			keys = new ArrayList();
			connection = dataSource.getConnection();
			statementText = "SELECT mc.card_id FROM PUB.mem_membership_card mc, PUB.mem_membership mm WHERE mc.membership_id = mm.membership_id AND mc.issue_date is null ";

			if (newAccessOnly) {
				// include only cards requested as part of the access card
				// initiative
				statementText += "AND mm.product_code = '" + ProductVO.PRODUCT_ACCESS + "' ";

				// include gifted insurance, gifted travel, non-motoring
				statementText += "AND (mm.profile_code = '" + MembershipProfileVO.PROFILE_GIFTED_INSURANCE + "'";
				statementText += "OR mm.profile_code = '" + MembershipProfileVO.PROFILE_GIFTED_TRAVEL + "'";
				statementText += "OR mm.profile_code = '" + MembershipProfileVO.PROFILE_NON_MOTORING + "')";

				// manually created
			} else {
				// exclude any cards for the access initiative - only do new and
				// replacement cards
				if (productCode != null) {
					statementText += "AND mm.product_code = ? "; // advantage,
					// ultimate,
					// not access
				}

				if (ProductVO.PRODUCT_ACCESS.equals(productCode)) {
					// exclude gifted insurance, gifted travel, non-motoring
					statementText += "AND (mm.profile_code <> '" + MembershipProfileVO.PROFILE_GIFTED_INSURANCE + "'";
					statementText += "OR mm.profile_code <> '" + MembershipProfileVO.PROFILE_GIFTED_TRAVEL + "'";
					statementText += "OR mm.profile_code <> '" + MembershipProfileVO.PROFILE_NON_MOTORING + "')";
				}

			}

			if (eligibleLoyaltyScheme) {
				// include cards with the reason of tier change
				statementText += "AND mc.ref_code = '" + ReferenceDataVO.CARD_REQUEST_TIER_CHANGE + "' AND mc.tier_code = '" + MembershipTier.TIER_CODE_LOYALTY + "'";
			} else {
				// exclude any that will be picked up by loyalty program
				statementText += "AND ((mc.tier_code <> '" + MembershipTier.TIER_CODE_LOYALTY + "' OR mc.tier_code is null) OR (mc.ref_code <> '" + ReferenceDataVO.CARD_REQUEST_TIER_CHANGE + "' and mc.tier_code = '" + MembershipTier.TIER_CODE_LOYALTY + "'))";
			}

			LogUtil.debug(this.getClass(), statementText);

			statement = connection.prepareStatement(statementText);
			if (productCode != null) {
				statement.setString(1, productCode);
			}
			ResultSet resultSet = statement.executeQuery();
			Integer cardID = null;
			while (resultSet.next()) {
				numberToProcess++;
				cardID = new Integer(resultSet.getInt(1));
				keys.add(cardID);
			}
		} catch (SQLException e) {
			throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return keys;
	}

	/**
	 * @todo should be in table
	 */
	private String getDiscountStatus(MembershipVO memVO) throws Exception {
		ArrayList discountList = memVO.getDiscountList();
		String discNum = "0";
		for (int x = 0; x < discountList.size(); x++) {
			MembershipDiscount memDiscount = (MembershipDiscount) discountList.get(x);
			if (memDiscount.getDiscountTypeCode().equals(DiscountTypeVO.TYPE_LIFE)) {
				discNum = "1";
				break;
			} else if (memDiscount.getDiscountTypeCode().equals(DiscountTypeVO.TYPE_RACT_STAFF)) {
				discNum = "2";
				break;
			} else if (memDiscount.getDiscountTypeCode().equals(DiscountTypeVO.TYPE_ISCU_STAFF)) {
				discNum = "3";
				break;
			}
		}
		return discNum;
	}

	public void ejbCreate() {
	}

	public void ejbRemove() throws RemoteException {
	}

	public void ejbActivate() throws RemoteException {
	}

	public void ejbPassivate() throws RemoteException {
	}

	public void setSessionContext(SessionContext sessionContext) throws RemoteException {
		this.sessionContext = sessionContext;
		try {
			this.dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
		} catch (Exception e) {
			System.out.println("Error initialising dataSource client in MembershipMgrBean");
			e.printStackTrace();
		}
	}

}
