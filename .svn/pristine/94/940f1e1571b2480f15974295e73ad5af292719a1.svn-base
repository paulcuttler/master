package com.ract.membership;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.util.CurrencyUtil;
import com.ract.util.HTMLUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * Representation of unearned value for a membership.
 * 
 * @author T. Bakker
 * @version 1.0
 */

public class UnearnedValue implements Serializable, TransactionCredit {
    private BigDecimal unearnedAmount;

    private int daysRemaining = 0;

    private String description;

    public UnearnedValue() {
	setUnearnedAmount(new BigDecimal(0.0));
	setDaysRemaining(0);
    }

    public UnearnedValue(BigDecimal amt, int days, String description) {
	setUnearnedAmount(amt);
	setDaysRemaining(days);
	setDescription(description);
    }

    public BigDecimal getUnearnedAmount() {
	LogUtil.debug(this.getClass(), "1 " + unearnedAmount);
	return unearnedAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public Interval getUnearnedPeriod() {
	Interval unearnedPeriod = null;
	try {
	    unearnedPeriod = new Interval(0, 0, this.getDaysRemaining(), 0, 0, 0, 0);
	} catch (Exception ex) {
	    // null
	}
	return unearnedPeriod;
    }

    public int getDaysRemaining() {
	return this.daysRemaining;
    }

    public void setUnearnedAmount(BigDecimal amount) {
	LogUtil.debug(this.getClass(), "amount" + amount);
	this.unearnedAmount = amount;
    }

    public void setDaysRemaining(int daysRemaining) {
	this.daysRemaining = daysRemaining;
    }

    /**
     * User friendly view of the information stored in the object.
     */
    public String formatForDisplay() {
	StringBuffer unearnedValue = new StringBuffer();
	unearnedValue.append(CurrencyUtil.formatDollarValue(this.getUnearnedAmount()));
	int days = this.getDaysRemaining();
	if (days > 0) {
	    unearnedValue.append(" (");
	    switch (days) {
	    case 1:
		unearnedValue.append(days + " day");
	    default:
		unearnedValue.append(days + " days");
	    }
	    unearnedValue.append(" remaining");
	    unearnedValue.append(")");
	}
	return unearnedValue.toString();
    }

    public String toString() {
	return "UnearnedValue = " + CurrencyUtil.formatDollarValue(this.getUnearnedAmount()) + " (" + this.getDaysRemaining() + " " + (this.getDaysRemaining() == 1 ? "day" : "days") + " remaining)";
    }

    /**
     * TransactionCredit interface implementation
     */
    public BigDecimal getTransactionCreditAmount() {
	return this.unearnedAmount;
    }

    public void setTransactionCreditAmount(BigDecimal creditAmount) {
	setUnearnedAmount(creditAmount);
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDescription() {
	return description;
    }

    /**
     * Get a description for the HTML anchor.
     * 
     * @return
     */
    public String getDescriptionForHTMLAnchor() {
	if (this.description != null) {
	    return StringUtil.replaceAll(this.description, "|", HTMLUtil.ANCHOR_NEW_LINE);
	} else {
	    return "";
	}
    }
}
