package com.ract.membership;

import java.rmi.RemoteException;

import com.ract.common.ValueObject;
import com.ract.user.User;
import com.ract.util.DateTime;

/**
 * Extends a membership transaction value object and provides specific
 * functionality for creating a membership transaction value object for a
 * "Change Product Option" type of transaction. Sets up the transaction with a
 * copy of the membership which has been adjusted for the transaction.
 * 
 * @hibernate.subclass discriminator-value="CHANGE PRODUCT OPTION"
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class MemTransChangeProductOption extends MembershipTransactionVO {

    public MemTransChangeProductOption() {
    }

    public MemTransChangeProductOption(String transGroupTypeCode, DateTime transDate, MembershipVO oldMemVO, User user, boolean primeAddressee, DateTime memGroupJoinDate) throws RemoteException {
	super(ValueObject.MODE_CREATE, transGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION, memGroupJoinDate);
	MembershipVO newMemVO = oldMemVO.copy(true);
	// These should already be set this way according to the business rules
	// but do it any way in case the rule implementation has been changed
	// elsewhere.
	newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
	newMemVO.setAllowToLapse(Boolean.valueOf(false));
	setTransactionDate(transDate);
	setUsername(user.getUserID());
	setSalesBranchCode(user.getSalesBranchCode());
	setMembership(oldMemVO);
	setNewMembership(newMemVO);
	setProductEffectiveDate();
	setPrimeAddressee(primeAddressee);

	setEffectiveDate(transDate);
	setEndDate(oldMemVO.getExpiryDate());
    }
}
