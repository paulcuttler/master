package com.ract.common;

import java.io.File;
import java.io.FileInputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class SystemParameterCache extends CacheBase2 {
    private static SystemParameterCache instance = new SystemParameterCache();

    /**
     * Holds locally defined parameters if they have been specified.
     */
    private Properties localProperties;

    public Collection getItemList() throws RemoteException {
	return new ArrayList(super.getItemList());
    }

    /**
     * This static initialiser load a local system parameter properties file if
     * one exists. The parameters from the local properties file overrides the
     * parameters in the database.
     */
    public SystemParameterCache() {
	FileInputStream inStream = null;
	File iniFile = new File("SystemParameterVO.ini");
	if (iniFile.exists()) {
	    try {
		inStream = new FileInputStream(iniFile);
		if (inStream.available() > 0) {
		    this.localProperties = new Properties();
		    this.localProperties.load(inStream);
		}
	    } catch (Exception e) {
		// Ignore all exceptions and forget about loading the local
		// parameter file.
		LogUtil.warn(this.getClass(), "Failed to load local properties file: " + e);
		this.localProperties = null;
	    } finally {
		// Try and close the file incase its open.
		try {
		    inStream.close();
		} catch (Exception e1) {
		}
		;
	    }
	}
    }

    public static SystemParameterCache getInstance() {
	return instance;
    }

    public void initialiseList() throws RemoteException {
	// if(isEmpty())
	// {
	// try
	// {
	// add(getSystemParameterVO(SystemParameterVO.CATEGORY_MEMBERSHIP,
	// SystemParameterVO.DIRECTORY_MEMBERSHIP_HISTORY));
	// }
	// catch(ObjectNotFoundException onfe)
	// {
	// LogUtil.warn(this.getClass(),
	// "Failed to initialise system parameter cache!!!\n" +
	// onfe.getMessage());
	// }
	// }
    }

    /**
     * Return the value of a specified system parameter based on the parameter
     * category and parameter name. If the parameter cound not be found then
     * return null.
     */
    public String getParameterValue(String cat, String param) throws RemoteException {
	String value = null;
	SystemParameterVO sysParamVO = null;
	LogUtil.debug(this.getClass(), "getParameterValue=" + cat + " " + param);
	if (cat != null && param != null) {
	    // Try and find it in the cache first
	    sysParamVO = new SystemParameterVO(cat, param);
	    sysParamVO = (SystemParameterVO) getItem(sysParamVO.getKey());
	    if (sysParamVO != null) {
		value = sysParamVO.getValue();
	    }

	    // If not found in the cache try and get it from the local parameter
	    // file. In this file the parameter name is the concatenation of
	    // cat + _ + param.
	    if (value == null && this.localProperties != null) {
		String paramValue = this.localProperties.getProperty(cat + "_" + param);
		if (paramValue != null) {
		    value = paramValue;
		    sysParamVO = new SystemParameterVO(cat, param, paramValue);
		    add(sysParamVO);
		}
	    }

	    // If not found in the cache try and get it from the
	    // master.properties
	    // file. In this file the parameter name is the concatenation of
	    // cat + _ + param.
	    if (value == null) {
		try {
		    String paramValue = FileUtil.getProperty("master", cat + "_" + param);
		    if (paramValue != null) {
			value = paramValue;
			sysParamVO = new SystemParameterVO(cat, param, paramValue);
			add(sysParamVO);
		    }
		} catch (Exception e) {
		}
	    }

	    // If not found in the cache or the local properties file then
	    // get it from the database and add it to the cache table.
	    if (value == null) {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		sysParamVO = commonMgr.getSystemParameter(cat, param);

		if (sysParamVO != null) {
		    value = sysParamVO.getValue();
		    add(sysParamVO);
		}

	    }
	}
	return value;
    }

    /**
     * Set the value of a system parameter in the database and the cache. The
     * category name and parameter name must be given. A null or empty parameter
     * value may be set. The parameter is created in the database if it does not
     * exist.
     */
    public void setSystemParameterValue(String cat, String param, String value) throws RemoteException {
	if (cat == null || cat.length() < 1 || param == null || param.length() < 1) {
	    throw new SystemException("Failed to set system parameter value. One of category '" + cat + "' or parameter name '" + param + "' is invalid.");
	}
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	SystemParameterVO sysParam;

	// Try and update it.
	sysParam = commonMgr.getSystemParameter(cat, param);
	sysParam.setValue(value);

	if (sysParam == null) {
	    commonMgr.createSystemParameter(sysParam);
	} else {
	    commonMgr.updateSystemParameter(sysParam);
	}

	// Now update the cache.
	SystemParameterVO sysParamVO = new SystemParameterVO(cat, param);
	sysParamVO = (SystemParameterVO) getItem(sysParamVO.getKey());
	if (sysParamVO != null) {
	    sysParamVO.setValue(value);
	} else {
	    // Wasn't in the cache so add it.
	    sysParamVO = new SystemParameterVO(cat, param, value);
	    add(sysParamVO);
	}
    }

}
