package com.ract.payment.finance;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.util.DateTime;

/**
 * <p>
 * A debtor's invoice.
 * </p>
 * 
 * @hibernate.class table="fin_invoice" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class DebtorInvoice implements Serializable {

    /**
     * @hibernate.property
     * @hibernate.column name="narration"
     */
    public String getNarration() {
	return narration;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="effective_date"
     */
    public DateTime getEffectiveDate() {
	return effectiveDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="amount"
     */
    public BigDecimal getAmount() {
	return amount;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public DebtorInvoicePK getDebtorInvoicePK() {
	return debtorInvoicePK;
    }

    private DebtorInvoicePK debtorInvoicePK;

    /**
     * @hibernate.property
     * @hibernate.column name="customer_number"
     */
    public String getCustomerNumber() {
	return customerNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="last_update"
     */
    public DateTime getLastUpdate() {
	return lastUpdate;
    }

    public void setNarration(String narration) {
	this.narration = narration;
    }

    public void setEffectiveDate(DateTime effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    public void setAmount(BigDecimal amount) {
	this.amount = amount;
    }

    public void setCustomerNumber(String customerNumber) {
	this.customerNumber = customerNumber;
    }

    public void setDebtorInvoicePK(DebtorInvoicePK debtorInvoicePK) {
	this.debtorInvoicePK = debtorInvoicePK;
    }

    public void setLastUpdate(DateTime lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    public DebtorInvoice() {
	// default
    }

    public DebtorInvoice(String customerNumber, BigDecimal amount, DateTime effectiveDate, String narration) throws FinanceException {
	if (customerNumber == null) {
	    throw new FinanceException("Customer number must not be null");
	}
	if (amount == null) {
	    throw new FinanceException("Outstanding amount must not be null");
	}
	if (effectiveDate == null) {
	    throw new FinanceException("Effective date must not be null");
	}
	setAmount(amount);
	setEffectiveDate(effectiveDate);
	setNarration(narration);
	setCustomerNumber(customerNumber);
    }

    // as per DRINV

    // OS_AMT1
    private BigDecimal amount;
    // DOC_DATEI1
    private DateTime effectiveDate;
    // Narration
    private String narration;

    // USER_FLD1
    // USER_FLD2
    // USER_FLD3
    // USER_FLD4
    // USER_FLD5
    // BAT_CRUSER
    // NOTE

    // ACCNBRI
    private String customerNumber;

    private DateTime lastUpdate;

    public String toCSVLine() {
	return "Not Implemented";
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(getDebtorInvoicePK().toString());
	desc.append(": ");
	desc.append(getAmount());
	desc.append(" ");
	desc.append(getCustomerNumber());
	desc.append(" ");
	desc.append(getEffectiveDate());
	desc.append(" ");
	desc.append(getNarration());
	return desc.toString();
    }

}
