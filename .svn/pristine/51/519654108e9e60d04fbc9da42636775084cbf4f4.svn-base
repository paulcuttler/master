package com.ract.payment.receipting;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Properties;

import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemDataException;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentMethod;
import com.ract.payment.billpay.IVRPayment;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.util.DateTime;

/**
 * The receipting adapter interface
 * 
 * @author Glenn Lewis
 * @created 14 January 2003
 * @version 1.0, 14/5/2002
 * @version 2.0, 7/1/2003 Terry Bakker
 * @version 3.0, 6/7/2003 Sam McLennan
 */
public interface ReceiptingAdapter extends TransactionAdapter {

    public static final String VISA = "VISA";

    public static final String EFTPOS = "EFTPOS";

    public static final String AMEX = "AMERICAN EXPRESS";

    public static final String BANKCARD = "BANKCARD";

    public static final String DIRECTDEBIT = "DIRECT DEBIT";

    public static final String DINERS = "DINERS CLUB";

    public static final String MASTERCARD = "MASTERCARD";

    public static final String TELSTRA = "TELSTRA";

    public static final String CASH = "CASH";

    public static final String CHEQUE = "CHEQUE";

    public static final String ROUNDING = "ROUNDING";

    public static final String CARD_NO = "cardNo";

    public static final String CARD_HOLDER = "cardHolder";

    public static final String CARD_EXPIRY = "cardExpiry";

    public static final String GENERIC_CARD_TYPE = "SomeCardType";

    public static final String CARD_TYPE = "cardType";

    public static final String CHEQUE_NO = "chequeNo";

    public static final String CHEQUE_BANK = "chequeBank";

    public static final String CHEQUE_DRAWER = "chequeDrawer";

    public static final String EXPIRY_DATE = "expiryDate";

    public static final String EFFECTIVE_DATE = "effectiveDate";

    public static final String REFERENCE_DOC = "referenceDoc";

    public static final String SEPARATOR = "-";

    /**
     * Payable does not exist.
     */
    public static final int PAYABLE_DOES_NOT_EXIST = 0;

    /**
     * Payable exists and is paid.
     */
    public static final int PAYABLE_EXISTS_PAID = 1;

    /**
     * Payable exists and has one or more receipts attached.
     */
    public static final String PAYABLE_EXISTS_RECEIPTS_ATTACHED = "Payable exists with receipts attached.";

    /**
     * Payable exists and is unpaid.
     */
    public static final int PAYABLE_EXISTS_UNPAID = 2;

    /**
     * Payable transaction already exists
     */
    public static final String ERROR_TRANSACTION_EXISTS = "Transaction already exists";

    /**
     * Add a payable fee to the receipting system. The payable fee may include
     * details about a pre payment.
     * 
     * @param payableItem
     *            the item to create the fee for
     * @return the ID of the payable fee in the receipting system.
     */
    public String createPayableFee(PayableItemVO payableItem) throws RollBackException;

    /**
     * Transfer receipts and associated receipting system items from one client
     * to another.
     * 
     * @param fromClientNumber
     *            Integer
     * @param toClientNumber
     *            Integer
     * @throws RollBackException
     */
    public void transferReceipts(Integer fromClientNumber, Integer toClientNumber, String userId) throws RollBackException;

    public void deleteReceiptingCustomer(Integer clientNumber, String userId) throws RollBackException;

    /**
     * Create a payable fee
     */
    public String createPayableFee(Integer clientNumber, String sequenceNumber, DateTime createDate, String product, double amountPayable, String description, String sourceSystem, String branch, String user, Properties[] properties) throws RollBackException;

    /**
     * Create a paid fee with RECEPTOR. This was implemented so that RECEPTOR
     * can provide accurate cash-reconciliation reports at the end of each day
     * for all systems.
     */
    public Integer createPaidFee(String username, String branch, String till, String receipt, String clientNumber, String[] products, double[] costs, String[] paymentType, double[] tendered, Properties[] data, String comments, java.util.Date date, String sourceSystem) throws RemoteException, RollBackException;

    /**
     * Cancel a payable fee.
     * 
     * @param payableItem
     *            the payable item to be cancelled
     * @exception SystemException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void cancelPayableFee(PaymentMethod paymentMethod) throws RollBackException;

    /**
     * Get a payable.
     * 
     * @param uniqueID
     * @param sourceSystem
     *            The source system is RACT, ISCU, etc.
     * @return the payable
     * @throws RemoteException
     */
    public Payable getPayable(String uniqueID, String sourceSystem) throws RemoteException, SystemDataException;

    /**
     * Remove a payable fee.
     * 
     * @param payableItem
     *            the payable item to be cancelled
     * @exception SystemException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void removePayableFee(PayableItemVO payableItem) throws RollBackException;

    /**
     * Remove a payable fee
     * 
     * @param clientNumber
     * @param paymentMethodID
     * @throws RollBackException
     */
    public void removePayableFee(Integer clientNumber, String paymentMethodID, String system, String userId) throws RollBackException;

    public Receipt receiptOnlinePayment(String sourceSystem, double amount, String cardNumber, DateTime paymentDate, String cardExpiryDate, String seqNumber, String clientNumber, String userId, String branchCode, String locationCode, String tillCode, String cardType, String cardHolder, String cvv) throws RollBackException;

    /**
     * Receipt an IVR payment
     * 
     * @return receipt The receipting system receipt.
     */
    public Receipt receiptIvrPayment(IVRPayment ivr) throws RollBackException;

    /**
     * Receipt an Electronic (BPAY,IVR) payment
     * 
     * @return receipt The receipting system receipt.
     */
    public Receipt receiptElectronicPayment(ElectronicPayment ep) throws RollBackException;

    public Receipt getReceiptByReceiptId(String receiptId);

    /**
     * Extract insurance IVR payments.
     * 
     * @return fileName the name of the extract file.
     * @throws RemoteException
     */
    public String extractInsuranceIVR() throws RemoteException;

    /**
     * Get a receipt given a unique ID.
     * 
     * @param uniqueID
     * @return
     * @throws RemoteException
     */
    public Receipt getReceipt(String uniqueID, SourceSystem sourceSystem) throws RemoteException, SystemDataException;

    /**
     * Retrieve a Collection of payables each with an associated set of receipt
     * objects.
     * 
     * @param clientNo
     *            The client number
     * @return Collection of payable objects
     * @throws RemoteException
     */
    public Collection getPayableHistory(Integer clientNo) throws RemoteException;

    public Collection getPayableHistory(Integer clientNumber, DateTime startDate, DateTime endDate, boolean outstandingOnly) throws RemoteException;

    public Collection getOutstandingPayables(Integer clientNumber) throws RemoteException;

    /**
     * Retrieve a Collection of historical receipts from the external receipting
     * system. Creates and returns a collection of ReceptorReceipt objects
     * 
     * @param start
     * @param end
     * @return Collection of ReceptorReceipts
     * @throws RemoteException
     */
    public Collection getReceiptingHistory(java.util.Date start, java.util.Date end) throws RemoteException;

    public void uncancelPayableFee(String uniqueID) throws RollBackException;

}
