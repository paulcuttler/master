package com.ract.payment;

import java.rmi.RemoteException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

@Entity
@Table(name = "[oi-rectype]")
public class PaymentTypeVO extends ValueObject implements CachedItem, Writable {
    public static final String WRITABLE_CLASSNAME = "PaymentType";

    // Constants that are the primary key to the oi-rectype table
    public static final String PAYMENTTYPE_CASH = "C";

    public static final String PAYMENTTYPE_DIRECTDEBIT = "DD";

    public static final String PAYMENTTYPE_IVR = "IVR";

    public static final String PAYMENTTYPE_EFTPOS = "E";

    public static final String PAYMENTTYPE_AMERICAN_EXPRESS = "EA";

    public static final String PAYMENTTYPE_BANKCARD = "EB";

    public static final String PAYMENTTYPE_DINERS_CLUB = "ED";

    public static final String PAYMENTTYPE_MASTER_CARD = "EM";

    public static final String PAYMENTTYPE_VISA = "EV";

    /**
     * This payment type is used when a payment fails and the source system
     * needs to link to an existing payable.
     */
    public static final String PAYMENTTYPE_REPAIR = "Repair";

    public static final String PAYMENTTYPE_CHEQUE = "Q";

    // Constants that define the payment type categories
    public static final String CASHTYPE_CASH = "C";

    public static final String CASHTYPE_DIRECTDEBIT = "D";

    public static final String CASHTYPE_CREDITCARD = "R";

    public static final String CASHTYPE_CHEQUE = "Q";

    public static final String CASHTYPE_REPAIR = "Repair";

    @Id
    @Column(name = "[amt-type]")
    public String paymentTypeCode;

    @Column(name = "[amt-type-desc]")
    public String description;

    @Column(name = "[cash-type]")
    public String cashTypeCode;

    @Column(name = "[floor-limit]")
    public double floorLimit;

    @Column(name = "[card-digits]")
    public int cardNumberLength;

    @Column(name = "[rep-type]")
    public String reportType;

    @Column(name = "mem_payment_type")
    public boolean membershipPaymentType;

    public PaymentTypeVO() {
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("cashTypeCode=" + cashTypeCode);
	desc.append(",paymentTypeCode=" + paymentTypeCode);
	desc.append(",description=" + description);
	return desc.toString();
    }

    public PaymentTypeVO(Node paymentTypeNode) {
	NodeList elementList = paymentTypeNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("cardNumberLength".equals(nodeName) && valueText != null) {
		this.cardNumberLength = Integer.parseInt(valueText);
	    } else if ("cashTypeCode".equals(nodeName) && valueText != null) {
		this.cashTypeCode = valueText;
	    } else if ("description".equals(nodeName) && valueText != null) {
		this.description = valueText;
	    } else if ("floorLimit".equals(nodeName) && valueText != null) {
		this.floorLimit = Double.parseDouble(valueText);
	    } else if ("membershipPaymentType".equals(nodeName) && valueText != null) {
		this.membershipPaymentType = Boolean.valueOf(valueText).booleanValue();
	    } else if ("paymentTypeCode".equals(nodeName) && valueText != null) {
		this.paymentTypeCode = valueText;
	    } else if ("reportType".equals(nodeName) && valueText != null) {
		this.reportType = valueText;
	    }

	}

    }

    public PaymentTypeVO(String paymentTypeCode, String description, String cashTypeCode, double floorLimit, int cardNumberLength, String reportType, boolean membershipPaymentType) {
	this.paymentTypeCode = paymentTypeCode;
	this.description = description;
	this.cashTypeCode = cashTypeCode;
	this.floorLimit = floorLimit;
	this.cardNumberLength = cardNumberLength;
	this.reportType = reportType;
	this.membershipPaymentType = membershipPaymentType;

	// LogUtil.log(this.getClass(), "cashTypeCode=" + cashTypeCode);
	// LogUtil.log(this.getClass(), "description=" + description);
	// LogUtil.log(this.getClass(), "paymentTypeCode=" + paymentTypeCode);
	// LogUtil.log(this.getClass(), "cardNumberLength=" + cardNumberLength);
	// LogUtil.log(this.getClass(), "reportType=" + reportType);
	// LogUtil.log(this.getClass(), "membershipPaymentType=" +
	// membershipPaymentType);

	setReadOnly(true);
    }

    public int getCardNumberLength() {
	return cardNumberLength;
    }

    public String getCashTypeCode() {
	LogUtil.debug(this.getClass(), "cashTypeCode=" + cashTypeCode);
	return cashTypeCode;
    }

    public String getDescription() {
	return description;
    }

    public double getFloorLimit() {
	return floorLimit;
    }

    public boolean isMembershipPaymentType() {
	return membershipPaymentType;
    }

    public String getPaymentTypeCode() {
	return paymentTypeCode;
    }

    public String getReportType() {
	return reportType;
    }

    public void setCardNumberLength(int cardNumberLength) throws RemoteException {
	checkReadOnly();
	this.cardNumberLength = cardNumberLength;
    }

    public void setCashTypeCode(String cashTypeCode) throws RemoteException {
	checkReadOnly();
	this.cashTypeCode = cashTypeCode;
    }

    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    public void setFloorLimit(double floorLimit) throws RemoteException {
	checkReadOnly();
	this.floorLimit = floorLimit;
    }

    public void setMembershipPaymentType(boolean membershipPaymentType) throws RemoteException {
	checkReadOnly();
	this.membershipPaymentType = membershipPaymentType;
    }

    public void setPaymentTypeCode(String paymentTypeCode) throws RemoteException {
	checkReadOnly();
	this.paymentTypeCode = paymentTypeCode;
    }

    public void setReportType(String reportType) throws RemoteException {
	checkReadOnly();
	this.reportType = reportType;
    }

    public PaymentTypeVO copy() {
	PaymentTypeVO paymentTypeVO = new PaymentTypeVO(this.paymentTypeCode, this.description, this.cashTypeCode, this.floorLimit, this.cardNumberLength, this.reportType, this.membershipPaymentType);
	return paymentTypeVO;
    }

    /**
     * Return the payment type code as the cached item key.
     * 
     * @return The key value
     */
    public String getKey() {
	return this.paymentTypeCode.trim();
    }

    /**
     * Determine if the specified key matches the discount type code. Return
     * true if both the key and the discount type code are null.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("cardNumberLength", this.cardNumberLength);
	cw.writeAttribute("cashTypeCode", this.cashTypeCode);
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("floorLimit", this.floorLimit);
	cw.writeAttribute("membershipPaymentType", this.membershipPaymentType);
	cw.writeAttribute("paymentTypeCode", this.paymentTypeCode);
	cw.writeAttribute("reportType", this.reportType);
	cw.endClass();
    }

}
