package com.ract.common;

import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONArray;
import org.json.JSONObject;

public class SMSManager {

	private static final String EMPTY_STRING = "";
	Logger log = Logger.getLogger(com.ract.common.SMSManager.class.getName());;

	/**
	 * sendSMS
	 * 
	 * Send an SMS to the MessageMedia message API
	 * 
	 * @param smsParams HashMap<String,String> the phone number and the content
	 * @return boolean
	 * @throws Exception
	 */
	public Boolean sendSMS(HashMap<String, String> smsParams) throws Exception {
		/*
		 * Make POST request to MessageMedia message API - APPS-237 JIRA
		 * 
		 * History ======= This is a replacement for the current Telstra email based SMS service for client/renewal SMS calls
		 * 
		 * GS 29/7/2020
		 */
		String requestBody = String.format("{" + "\"messages\": [" + "{" + "\"content\": \"%s\"," + "\"destination_number\": \"%s\"," + "\"format\": \"SMS\"," + "\"source_number\": \"RACT\"," + "\"source_number_type\": \"ALPHANUMERIC\"" + "}" + "]" + "}", smsParams.get(smsParams.keySet().toArray()[0]), smsParams.keySet().toArray()[0]);

		String restResponse = EMPTY_STRING;

		// Perform the POST - any exception is propagated to the caller
		PostMethod post = new PostMethod("http://mule-worker-internal-ract-message-api-prd.au-s1.cloudhub.io:8091/sendSMS");
		try {
			// Create the request
			StringRequestEntity requestEntity = new StringRequestEntity(requestBody);

			// Set the body
			post.setRequestEntity(requestEntity);

			// Set the encoded form header
			post.setRequestHeader("Content-Type", "application/json");

			// Create the client
			HttpClient httpclient = new HttpClient();

			// Excecute the verify method
			int result = httpclient.executeMethod(post);

			// If result not 200 throw an exception
			if (result != 200)
				throw new Exception("Server problem - result code is problematic (not 200): " + result);

			// Get the response body
			restResponse = post.getResponseBodyAsString();
		} finally {
			// Release the HTTP connection
			post.releaseConnection();
		}

		if (!restResponse.equals(EMPTY_STRING)) {
			JSONObject jResponse = new JSONObject(restResponse);
			JSONArray messages = jResponse.getJSONArray("messages");
			JSONObject message = messages.getJSONObject(0);
			log.info("Successfully sent message " + message.getString("message_id") + " to " + message.getString("destination_number"));
		}
		
		return true;
	}
	
	/**
	 * main
	 * 
	 * For testing purposes only
	 * @param args
	 */
	public static void main(String[] args) {
		SMSManager smsManager = new SMSManager();
		
		HashMap<String,String> smsParams = new HashMap<String,String>();
		smsParams.put("+61438164757", "Testing sending to Mr G. Sharp!");
		try {
			smsManager.sendSMS(smsParams);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
