/*
 * @(#)ProgressFinanceAdapter.java
 *
 */

package com.ract.payment.finance;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import com.progress.open4gl.BigDecimalHolder;
import com.progress.open4gl.BooleanHolder;
import com.progress.open4gl.DateHolder;
import com.progress.open4gl.IntHolder;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.SourceSystem;
import com.ract.common.SystemDataException;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentIDMgr;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * The ProgressFinance adapter
 * 
 * @author Glenn Lewis
 * @version 1.0, 20/5/2002
 * 
 * @todo: move the appserver configuration to a properties or XML config file
 */

public class ProgressFinanceAdapter extends TransactionAdapterType implements FinanceAdapter {

    // the general ledger screen reference in MFG-Pro
    private static String MFG_PRO_SYSTEM_NUMBER = "25.13.1";

    private static String MFG_PRO_STOCK_OK = "OK";

    private static String MFG_PRO_STOCK_INVALID = "INVALID";

    private static String MFG_PRO_STOCK_INACTIVE = "INACTIVE";

    // private static final String LOG_FILENAME = "/u20/transfer.csv";

    private ProgressFinTransactionProxy financeTransactionProxy;

    private FinanceProgressProxy transactionFinanceProgressProxy = null;

    public String getSystemName() {
	return FinanceAdapter.FINANCE_SYSTEM_MFGPRO;
    }

    /**
     * transfer vector of journals to MFG Pro
     */
    public FinanceResponse transferJournals(Vector journals, SourceSystem sourceSystem) throws FinanceException// ,
													       // ValidationException
    {
	return transferJournals(journals, sourceSystem, false);
    }

    /**
     * Create a file with debtor details to be sent to Finance One.
     * 
     * @param debtor
     *            Debtor
     * @throws FinanceException
     */
    public void updateDebtor(Debtor debtor) throws FinanceException {
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.f1drAMD(debtor.getClientNumber().toString());
	} catch (Exception ex) {
	    throw new FinanceException("Error getting subsystem list.", ex);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}
    }

    /**
     * Transfer with debug mode option
     * 
     * @return journal number
     */
    public FinanceResponse transferJournals(Vector journals, SourceSystem sourceSystem, boolean debug) throws FinanceException {

	int journalNumber = 0;
	LogUtil.log(this.getClass(), "Transferring journals to Progress");
	// create a resultset to pass to progress
	if (journals.size() == 0) 
	{
	    throw new FinanceException("No journals to transfer. Progress");
	}

	// check that journals total 0
	FinanceHelper.isBalanced(journals);

	LogUtil.debug(this.getClass(), "sourceSystem=" + sourceSystem);
	LogUtil.debug(this.getClass(), "debug=" + sourceSystem);

	// if debug then write a transaction file to disk
	// for testing only
	// FinanceHelper.logJournalDetails(journals, debug);

	BigDecimal amount = null;
	GregorianCalendar postAt = null;

	try {
	    this.financeTransactionProxy = this.getFinanceTransactionProxy();

	    PaymentIDMgr pMgr = PaymentEJBHelper.getPaymentIDMgr();
	    journalNumber = pMgr.getNextJournalNumber();
	    BooleanHolder ok = new BooleanHolder();

	    Journal journal = null;
	    // add the records
	    for (int i = 0; i < journals.size(); i++) 
	    {
	    	journal = (Journal) journals.get(i);
	    	LogUtil.debug(this.getClass(), "Journal " + i + ": " + journal.toString());
	    	postAt = new GregorianCalendar();
	    	postAt.setTime(journal.getPostAtDate());
	    	amount = new BigDecimal(journal.getAmount());
		// add a mfg post item
	    	financeTransactionProxy.addMfgPostItem(postAt, journal.getAccount(), journal.getSubAccount(), journal.getCostCentre(), journal.getProject(), journal.getCompany().getCompanyCode(), journal.getSalesBranch(), amount, sourceSystem.getAbbreviation(), journal.getNarration());
	    }
	    
	    LogUtil.debug(this.getClass(), "added journals to temp table");
	    // post using the added items
	    financeTransactionProxy.mfgPost(journalNumber, ok);
	    
	    LogUtil.debug(this.getClass(), "mfgpost");
	    
	    if (!ok.getBooleanValue()) 
	    {
		throw new FinanceException("Could not post journals to MFG-PRO.");
	    }

	} catch (Open4GLException fe) {
	    throw new FinanceException(fe);
	} catch (PaymentException pe) {
	    throw new FinanceException(pe);
	} catch (RemoteException re) {
	    throw new FinanceException(re);
	}

	return new FinanceResponse(new Integer(journalNumber), this.MFG_PRO_SYSTEM_NUMBER);
    }

    /**
     * Translate MFG-Pro accounts to Finance One accounts. If not valid will
     * throw an exception.
     * 
     * @param entity
     *            String
     * @param accountCode
     *            String
     * @param subAccountCode
     *            String
     * @param costCentre
     *            String
     * @throws FinanceException
     * @return ArrayList
     */
    public String translateAccounts(String entity, String accountCode, String subAccountCode, String costCentre) throws FinanceException {
	String financeOneAccount = "";

	StringBuffer accountIdentifer = new StringBuffer();
	accountIdentifer.append(entity);
	accountIdentifer.append("/");
	accountIdentifer.append(accountCode);
	accountIdentifer.append("/");
	accountIdentifer.append(subAccountCode);
	accountIdentifer.append("/");
	accountIdentifer.append(costCentre);

	FinanceProgressProxy financeProgressProxy = null;
	StringHolder financeOneAccountCodeHolder = new StringHolder();
	StringHolder financeOneCostCentreHolder = new StringHolder();
	BooleanHolder validHolder = new BooleanHolder();
	String financeOneCostCentre = null;
	String financeOneAccountCode = null;
	boolean valid = false;
	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.translateMFGProAccts(entity, accountCode, subAccountCode, costCentre, financeOneAccountCodeHolder, financeOneCostCentreHolder, validHolder);

	    financeOneAccountCode = financeOneAccountCodeHolder.getStringValue();
	    financeOneCostCentre = financeOneCostCentreHolder.getStringValue();
	    valid = validHolder.getBooleanValue();

	    LogUtil.debug(this.getClass(), "financeOneAccountCode=" + financeOneAccountCode);
	    LogUtil.debug(this.getClass(), "financeOneCostCentre=" + financeOneCostCentre);

	    handleProxyException(financeProgressProxy);

	    if (!valid) {
		throw new SystemDataException("Account combination '" + accountIdentifer.toString() + "' is invalid.");
	    }

	    financeOneAccount = financeOneCostCentre + financeOneAccountCode;
	    LogUtil.debug(this.getClass(), "financeOneAccount=" + financeOneAccount);

	} catch (Exception ex) {
	    throw new FinanceException("Error translating MFG-Pro account '" + accountIdentifer.toString() + "'.", ex);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}

	return financeOneAccount;
    }

    /**
     * Return a subsystem given a list and translation.
     * 
     * @param subsysTrans
     * @param subsystemList
     * @return
     */
    private Subsystem getSubsystem(String subsysTrans, Collection subsystemList) {
	Subsystem subsystem = null;

	if (subsystemList != null && subsystemList.size() > 0) {
	    Subsystem tmpSubsys = null;
	    boolean found = false;
	    Iterator it = subsystemList.iterator();
	    while (it.hasNext() && !found) {
		tmpSubsys = (Subsystem) it.next();
		if (tmpSubsys.getSubsystemTranslation().equals(subsysTrans)) {
		    subsystem = tmpSubsys;
		    found = true;
		}
	    }
	}
	return subsystem;
    }

    protected FinanceProgressProxy getFinanceProgressProxy() throws SystemException {
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    financeProgressProxy = (FinanceProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_FINANCE_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return financeProgressProxy;
    }

    private ProgressFinTransactionProxy getFinanceTransactionProxy() throws SystemException {
	// LogUtil.log(this.getClass(),"getting transaction proxy 1");
	if (this.financeTransactionProxy == null) {
	    // LogUtil.log(this.getClass(),"getting finance proxy 2");
	    this.transactionFinanceProgressProxy = this.getFinanceProgressProxy();
	    try {
		this.financeTransactionProxy = this.transactionFinanceProgressProxy.createPO_ProgressFinTransactionProxy();
		// LogUtil.log(this.getClass(),"getting transaction finance proxy 3");
	    } catch (Open4GLException gle) {
		throw new SystemException("Failed to get Finance transaction proxy.", gle);
	    }
	}
	return this.financeTransactionProxy;
    }

    private void handleProxyException(FinanceProgressProxy proxy) throws FinanceException {
	try {
	    if (proxy != null) {
		String returnString = proxy._getProcReturnString();
		if (returnString != null && returnString.length() > 0) {
		    throw new FinanceException(returnString);
		}
	    }
	} catch (Open4GLException o4gle2) {
	    throw new FinanceException("Failed to get returned exception from FinanceProgressProxy.", o4gle2);
	}
    }

    /**
     * Commit any pending transactions in the direct debit sub system. The
     * connection to the direct debit sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void commit() throws SystemException {
	LogUtil.debug(this.getClass(), "Committing finance adapter");
	try {
	    if (this.financeTransactionProxy != null) {
		this.financeTransactionProxy.commit();
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	} finally {
	    release();
	}
    }

    /**
     * Rollback any pending transactions in the direct debit sub system. The
     * connection to the direct debit sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws SystemException {
	LogUtil.debug(this.getClass(), "Rolling back finance adapter");
	try {
	    if (this.financeTransactionProxy != null) {
		this.financeTransactionProxy.rollback();
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	} finally {
	    release();
	}
    }

    /**
     * Release any connections held to the direct debit sub-system. If a
     * transaction is pending and not committed then the transaction will be
     * rolled back.
     */
    public synchronized void release() {
	// LogUtil.log(this.getClass(),"release() 1"+this);
	releaseFinanceTransactionProxy();
	// LogUtil.log(this.getClass(),"release() 3a"+(this.transactionFinanceProgressProxy==null));
    }

    private void releaseFinanceTransactionProxy() {
	LogUtil.debug(this.getClass(), "releaseFinanceTransactionProxy 1");
	if (this.financeTransactionProxy != null) {
	    LogUtil.debug(this.getClass(), "releaseFinanceTransactionProxy 2 != null.");
	    try {
		this.financeTransactionProxy._release();
	    } catch (Open4GLException gle) {
		LogUtil.fatal(this.getClass(), gle.getMessage());
	    } finally {
		this.financeTransactionProxy = null;
	    }
	}
	releaseFinancyProgressProxy(this.transactionFinanceProgressProxy);
	this.transactionFinanceProgressProxy = null;
    }

    public void createVoucher(FinancePayee payee, Cheque cheque) throws FinanceException {
	try {
	    this.financeTransactionProxy = this.getFinanceTransactionProxy();
	    this.financeTransactionProxy.createSupplier(payee.getPayeeNo(), payee.getPayeeName(), payee.getAddress1(), payee.getAddress2(), payee.getAddress3(), payee.getPostcode(), payee.getSuburb(), payee.getState(), payee.getFaxNo(), payee.getBankCode(), payee.getBsb(), payee.getAccountNo(), payee.getEmailAddr(), null, payee.getBankType(), payee.getAbn());

	    this.financeTransactionProxy.createVoucher(cheque.getPayeeNo(), cheque.getInvoiceDateGC(), cheque.getSubSystem(), cheque.getChqRet(), cheque.getGrossAmt(), cheque.getChqPrt(), cheque.getEntity(), cheque.getRemark(), cheque.isSeparateChequeYN(), cheque.getDescr(), cheque.getInvoice(), cheque.getVoucher());
	} catch (Exception e) {
	    throw new FinanceException("Error creating voucher in MFGPro: " + e);
	}
    }

    public Collection getSubsystemList() throws FinanceException, ValidationException {
	ArrayList subsystemList = new ArrayList();

	ResultSetHolder subsystemResultSet = new ResultSetHolder();

	FinanceProgressProxy financeProgressProxy = null;

	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.getSubsystems(null, subsystemResultSet);

	    ResultSet rs = subsystemResultSet.getResultSetValue();

	    if (rs != null) {
		String comments = "";
		String sourceSystem = "";
		String translation = "";
		Subsystem subsystem = null;

		while (rs.next()) {
		    sourceSystem = rs.getString(1);
		    translation = rs.getString(2);
		    comments = rs.getString(3);
		    subsystem = new Subsystem(sourceSystem, translation, comments);
		    subsystemList.add(subsystem);
		}
	    }
	} catch (Exception ex) {
	    throw new FinanceException("Error getting subsystem list.", ex);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}

	return subsystemList;
    }

    /**
     * <p>
     * Gets a list of general ledger transactions.
     * </p>
     * <p>
     * All parameters must have a value. Null parameters must be entered as ""
     * if it is a string.
     * </p>
     * 
     * @param effectiveStartDate
     *            the effective start date
     * @param effectiveEndDate
     *            the effective end date
     * @param enteredStartDate
     *            the entered start date
     * @param enteredEndDate
     *            the entered end date
     * @param subSystem
     *            the Subsystem is identified by a four character string eg.
     *            '5400'.
     * @param glAccount
     *            the GL account may have 'ALL' entered for all accounts or
     *            specify a particular GL account.
     * @return a collection of transactions
     * @throws FinanceException
     */
    public Collection getGLTransactions(DateTime effectiveStartDate, DateTime effectiveEndDate, DateTime enteredStartDate, DateTime enteredEndDate, String subSystem, String glAccount, String glEntity) throws FinanceException, ValidationException {
	LogUtil.debug(this.getClass(), " effectiveStartDate " + effectiveStartDate);
	LogUtil.debug(this.getClass(), " effectiveEndDate " + effectiveEndDate);
	LogUtil.debug(this.getClass(), " enteredStartDate " + enteredStartDate);
	LogUtil.debug(this.getClass(), " enteredEndDate " + enteredEndDate);
	LogUtil.debug(this.getClass(), " subSystem " + subSystem);
	LogUtil.debug(this.getClass(), " glAccount " + glAccount);
	LogUtil.debug(this.getClass(), " glEntity " + glEntity);

	ArrayList glTransactionList = new ArrayList();

	GregorianCalendar effStartDate = null;
	if (effectiveStartDate != null) {
	    effStartDate = new GregorianCalendar();
	    effStartDate.setTime(effectiveStartDate);
	}

	GregorianCalendar effEndDate = null;
	if (effectiveEndDate != null) {
	    effEndDate = new GregorianCalendar();
	    effEndDate.setTime(effectiveEndDate);
	}

	GregorianCalendar entStartDate = null;
	if (enteredStartDate != null) {
	    entStartDate = new GregorianCalendar();
	    entStartDate.setTime(enteredStartDate);
	}

	GregorianCalendar entEndDate = null;
	if (enteredEndDate != null) {
	    entEndDate = new GregorianCalendar();
	    entEndDate.setTime(enteredEndDate);
	}

	ResultSetHolder glResultSet = new ResultSetHolder();

	FinanceProgressProxy financeProgressProxy = null;

	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.mfggltrans(effStartDate, effEndDate, entStartDate, entEndDate, subSystem, glAccount, glEntity, glResultSet);
	    ResultSet rs = glResultSet.getResultSetValue();
	    GLTransaction glTransaction = null;
	    if (rs != null) {
		DateTime entDate = null;
		DateTime effDate = null;
		String account = "";
		String entity = "";
		String subAccount = "";
		String costCentre = "";
		String docType = "";
		BigDecimal amount = null;
		Subsystem subsys = null;
		String narration = null;
		while (rs.next()) {
		    entDate = new DateTime(rs.getDate(1));
		    effDate = new DateTime(rs.getDate(2));
		    account = rs.getString(3);
		    entity = rs.getString(4);
		    subAccount = rs.getString(5);
		    costCentre = rs.getString(6);
		    amount = new BigDecimal(rs.getDouble(7));
		    narration = rs.getString(8);
		    docType = rs.getString(9);
		    glTransaction = new GLTransaction(effDate, entDate, account, entity, subAccount, costCentre, amount, docType, narration);
		    glTransactionList.add(glTransaction);
		}
		rs.close();
	    }
	} catch (Exception ex) {
	    throw new FinanceException("Error getting GL transaction list.", ex);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}
	LogUtil.debug(this.getClass(), "glTransactionList=" + glTransactionList.toString());
	return glTransactionList;
    }

    public FinancePayee getPayee(String payeeNo) throws SystemException {
	FinancePayee payee = null;
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    StringHolder payeeName = new StringHolder(), address1 = new StringHolder(), address2 = new StringHolder(), address3 = new StringHolder(), postcode = new StringHolder(), city = new StringHolder(), state = new StringHolder(), faxNo = new StringHolder(), bank = new StringHolder(), abn = new StringHolder(), branchNo = new StringHolder(), bankAccount = new StringHolder(), chequeForm = new StringHolder(), bankType = new StringHolder(), email = new StringHolder();
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.getSupplier(payeeNo, payeeName, address1, address2, address3, postcode, city, state, faxNo, bank, abn, branchNo, bankAccount, chequeForm, bankType, email);
	    payee = new FinancePayee();
	    payee.setPayeeNo(payeeNo);
	    payee.setAbn(abn.getStringValue());
	    payee.setAccountNo(bankAccount.getStringValue());
	    payee.setAddress1(address1.getStringValue());
	    payee.setAddress2(address2.getStringValue());
	    payee.setAddress3(address3.getStringValue());
	    payee.setBankCode(bank.getStringValue());
	    payee.setBankType(bankType.getStringValue());
	    payee.setBsb(branchNo.getStringValue());
	    payee.setEmailAddr(email.getStringValue());
	    payee.setFaxNo(faxNo.getStringValue());
	    payee.setPayeeName(payeeName.getStringValue());
	    payee.setPostcode(postcode.getStringValue());
	    payee.setState(state.getStringValue());
	    payee.setSuburb(city.getStringValue());
	} catch (Exception e) {
	    throw new SystemException("Error retrieving payee data from MFGPro: ", e);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}
	return payee;
    }

    public Cheque getCheque(String voucherNo) throws SystemException {
	Cheque cheque = new Cheque(voucherNo);
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    StringHolder voInvoice = new StringHolder(), vdBank = new StringHolder(), vdCqForm = new StringHolder(), adName = new StringHolder(), address1 = new StringHolder(), address2 = new StringHolder(), address3 = new StringHolder(), csbdBank = new StringHolder(), csbdBranch = new StringHolder(), csbdAccount = new StringHolder();

	    DateHolder voDiscDate = new DateHolder(), chqDate = new DateHolder();
	    BigDecimalHolder chqAmount = new BigDecimalHolder();
	    IntHolder chqNumber = new IntHolder();

	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.getCheq(voucherNo, voInvoice, vdBank, vdCqForm, voDiscDate, adName, address1, address2, address3, csbdBank, csbdBranch, csbdAccount, chqAmount, chqNumber, chqDate);
	    if (chqAmount != null) {
		cheque.setChequeAmount(chqAmount.getBigDecimalValue());
	    }
	    if (chqDate != null && chqDate.getDateValue() != null) {
		cheque.setChequeDate(new DateTime(chqDate.getDateValue().getTime()));
	    }
	    if (chqNumber != null && chqNumber.getIntValue() != 0) {
		cheque.setChequeNumber(String.valueOf(chqNumber.getIntValue()));
	    }
	    if (voInvoice != null) {
		cheque.setInvoice(voInvoice.getStringValue());
	    }
	    LogUtil.log(this.getClass(), cheque.toString());
	} catch (Exception e) {
	    throw new SystemException("Unable to retrieve cheque data from MFGPro: " + e);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}
	return cheque;
    }

    public void createCustomerOrder(String debtorCode, String invoiceNumber, String salesBranch, DateTime postAtDate, DateTime effDate, boolean gst, BigDecimal totalAmount, String receiptNumber, Collection saleLines) throws SystemException {

	if (saleLines == null || saleLines.size() == 0) {
	    throw new SystemException("No customer orders to process attached to debtor " + debtorCode + ".");
	}

	LogUtil.debug(this.getClass(), "createCustomerOrder " + debtorCode + " " + invoiceNumber + " " + salesBranch + " " + postAtDate + " " + effDate + " " + gst + " " + receiptNumber + " " + totalAmount);
	LogUtil.debug(this.getClass(), "saleLines=" + (saleLines == null ? "null" : String.valueOf(saleLines.size())));

	BooleanHolder ok = new BooleanHolder();
	GregorianCalendar postDateGC = new GregorianCalendar();
	postDateGC.setTime(postAtDate);
	GregorianCalendar effDateGC = new GregorianCalendar();
	effDateGC.setTime(effDate);
	LineItem lineItem = null;

	try {
	    this.financeTransactionProxy = this.getFinanceTransactionProxy();

	    // load each line item
	    Iterator salesIt = saleLines.iterator();
	    int counter = 0;
	    while (salesIt.hasNext()) {
		counter++;
		lineItem = (LineItem) salesIt.next();
		LogUtil.debug(this.getClass(), "Line item " + counter + ": " + lineItem.toString());
		financeTransactionProxy.addItem(lineItem.getStockCode(), lineItem.getQuantity(), lineItem.getStockUnit(), lineItem.getStockPrice(), lineItem.getStockDisc(), lineItem.isTaxable());
	    }
	    LogUtil.debug(this.getClass(), "Completed line item");
	    financeTransactionProxy.createCustomerOrder(debtorCode, invoiceNumber, salesBranch, postDateGC, effDateGC, gst, receiptNumber, totalAmount, ok);
	    LogUtil.debug(this.getClass(), "Created customer order");
	} catch (Exception e) {
	    throw new SystemException("Error writing to MFGPro in ProgressFinanceProxy.createCustomerOrder: " + e);
	}

	if (ok.getBooleanValue() == false) {
	    throw new SystemException("Error in customerOrderProxy attempting to write to MFGPro");
	}
    }

    public boolean validateCustomer(String customerCode) throws FinanceException {
	BooleanHolder okHolder = new BooleanHolder();
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.validateMFGProDebtors(customerCode, okHolder);
	} catch (Open4GLException ex) {
	    throw new FinanceException("Error getting data from PROGRESS " + ex);
	} catch (SystemException se) {
	    throw new FinanceException(se);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}

	return okHolder.getBooleanValue();
    }

    /**
     * Return a stock item
     * 
     * @param stockItemCode
     *            String
     * @param profitCentre
     *            String
     * @throws FinanceException
     * @return StockItem
     */
    public StockItem getStockItem(String stockItemCode, String profitCentre) throws FinanceException {
	FinanceProgressProxy financeProgressProxy = null;
	StringHolder statusHolder = new StringHolder();
	BigDecimalHolder onHandHolder = new BigDecimalHolder();
	BigDecimalHolder sellPrice = new BigDecimalHolder();
	BigDecimalHolder sellPrice2 = new BigDecimalHolder();
	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.validateMFGProStock(stockItemCode, profitCentre, statusHolder, onHandHolder, sellPrice, sellPrice2);
	} catch (Open4GLException ex) {
	    throw new FinanceException("Unable to get validate stock from MFG PRO ", ex);
	} catch (SystemException se) {
	    throw new FinanceException(se);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}

	if (statusHolder.getStringValue().equals(MFG_PRO_STOCK_OK)) {
	    // if any values are null this will fail
	    StockItem stockItem = new StockItem(stockItemCode, profitCentre, onHandHolder.getBigDecimalValue(), sellPrice.getBigDecimalValue(), sellPrice2.getBigDecimalValue());
	    return stockItem;
	} else if (statusHolder.getStringValue().equals(MFG_PRO_STOCK_INACTIVE)) {
	    throw new FinanceException("The requested stock code is currently inactive and not able to used: " + stockItemCode);
	} else if (statusHolder.getStringValue().equals(MFG_PRO_STOCK_INVALID)) {
	    throw new FinanceException("The requested stock code is invalid: " + stockItemCode);
	} else {
	    throw new FinanceException("MFG-PRO returned unknown stock status value: " + statusHolder.getStringValue() + " for stock code: " + stockItemCode);
	}
    }

    /**
     * @todo return a stockitem object
     */
    public BigDecimal validateStockItem(String stockItem, String profitCentre) throws FinanceException {
	FinanceProgressProxy financeProgressProxy = null;
	StringHolder statusHolder = new StringHolder();
	BigDecimalHolder onHandHolder = new BigDecimalHolder();
	BigDecimalHolder sellPrice = new BigDecimalHolder();
	BigDecimalHolder sellPrice2 = new BigDecimalHolder();
	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.validateMFGProStock(stockItem, profitCentre, statusHolder, onHandHolder, sellPrice, sellPrice2);
	} catch (Open4GLException ex) {
	    throw new FinanceException("Unable to get validate stock from MFG-PRO " + ex.getMessage());
	} catch (SystemException se) {
	    throw new FinanceException(se);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}

	if (statusHolder.getStringValue().equals(MFG_PRO_STOCK_OK)) {
	    return onHandHolder.getBigDecimalValue();
	} else if (statusHolder.getStringValue().equals(MFG_PRO_STOCK_INACTIVE)) {
	    throw new FinanceException("The requested stock code is currently inactive and not able to used: " + stockItem);
	} else if (statusHolder.getStringValue().equals(MFG_PRO_STOCK_INVALID)) {
	    throw new FinanceException("The requested stock code is invalid: " + stockItem);
	} else {
	    throw new FinanceException("MFG-PRO returned unknown stock status value: " + statusHolder.getStringValue());
	}
    }

    public boolean validateGLCode(String accountCode, String subAccount, String costCentre) throws FinanceException {
	BooleanHolder okHolder = new BooleanHolder();
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.validateMFGProGLCodes(accountCode, subAccount, costCentre, okHolder);
	} catch (Open4GLException ex) {
	    throw new FinanceException("Unable to validate GL Code ", ex);
	} catch (SystemException se) {
	    throw new FinanceException(se);
	} finally {
	    LogUtil.debug(this.getClass(), "validateGLCode releasing financeProgressProxy" + financeProgressProxy);
	    releaseFinancyProgressProxy(financeProgressProxy);
	}
	return okHolder.getBooleanValue();
    }

    /**
     * Validate an invoice number in MFG-Pro.
     * 
     * @param invoiceNumber
     *            String
     * @throws FinanceException
     * @return boolean
     */
    public boolean validateInvoiceNumber(String invoiceNumber) throws FinanceException {
	BooleanHolder okHolder = new BooleanHolder();
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    financeProgressProxy = getFinanceProgressProxy();
	    financeProgressProxy.validateMFGProInvoice(invoiceNumber, okHolder);
	} catch (Open4GLException ex) {
	    throw new FinanceException("Unable to validate invoice number '" + invoiceNumber + "'.", ex);
	} catch (SystemException se) {
	    throw new FinanceException(se);
	} finally {
	    LogUtil.debug(this.getClass(), "validateGLCode releasing financeProgressProxy" + financeProgressProxy);
	    releaseFinancyProgressProxy(financeProgressProxy);
	}
	return okHolder.getBooleanValue();
    }

    protected void releaseFinancyProgressProxy(FinanceProgressProxy financeProgressProxy) {
	try {
	    LogUtil.debug(this.getClass(), "releaseFinancyProgressProxy 1");
	    if (financeProgressProxy != null) {
		LogUtil.debug(this.getClass(), "equal? financeProgressProxy=" + financeProgressProxy.equals(this.transactionFinanceProgressProxy));
	    }
	    LogUtil.debug(this.getClass(), "refs financeProgressProxy=" + financeProgressProxy + " " + this.transactionFinanceProgressProxy);
	    LogUtil.debug(this.getClass(), "null financeProgressProxy=" + (financeProgressProxy == null));
	    LogUtil.debug(this.getClass(), "null financeProgressProxy=" + (this.transactionFinanceProgressProxy == null));

	    LogUtil.debug(this.getClass(), "releaseFinancyProgressProxy 1a " + ProgressProxyObjectPool.getInstance().getNumIdle(ProgressProxyObjectFactory.KEY_FINANCE_PROXY));
	    LogUtil.debug(this.getClass(), "releaseFinancyProgressProxy 1b " + ProgressProxyObjectPool.getInstance().getNumActive(ProgressProxyObjectFactory.KEY_FINANCE_PROXY));
	} catch (Exception ex2) {
	}
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_FINANCE_PROXY, financeProgressProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), "Unable to return finance progress proxy to the pool." + ex);
	}

	try {
	    LogUtil.debug(this.getClass(), "releaseFinancyProgressProxy 2a " + ProgressProxyObjectPool.getInstance().getNumIdle(ProgressProxyObjectFactory.KEY_FINANCE_PROXY));
	    LogUtil.debug(this.getClass(), "releaseFinancyProgressProxy 2b " + ProgressProxyObjectPool.getInstance().getNumActive(ProgressProxyObjectFactory.KEY_FINANCE_PROXY));
	} catch (Exception ex1) {
	}
    }

}
