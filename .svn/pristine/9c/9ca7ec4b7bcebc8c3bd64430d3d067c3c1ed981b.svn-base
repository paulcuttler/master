package com.ract.membership;

import java.util.ArrayList;

import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.StringUtil;

/**
 * Run by Unix cron job to initiate the automatic renewal batch job.
 * 
 * @author Terry Bakker
 */

public class MembershipAutoRenewalRunner {

    public MembershipAutoRenewalRunner(String hostName, int portNumber, DateTime fromDate, DateTime toDate) {
	try {

	    RACTPropertiesProvider.setContextURL(hostName, portNumber);
	    RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();

	    MembershipVO membershipVO = null;

	    CreditCardAccount ccAccount = null;
	    DebitAccount dAccount = null;

	    ArrayList memList = (ArrayList) renewalMgr.getAutoRenewables(fromDate, toDate);
	    for (int i = 0; i < memList.size(); i++) {
		membershipVO = (MembershipVO) memList.get(i);
		System.out.print((i + 1) + " " + membershipVO.getClientNumber() + " " + membershipVO.getExpiryDate());
		BankAccount bankAccount = (membershipVO.getDirectDebitAuthority() != null ? membershipVO.getDirectDebitAuthority().getBankAccount() : null);
		if (bankAccount instanceof CreditCardAccount) {
		    ccAccount = (CreditCardAccount) bankAccount;
		    System.out.println(ccAccount.getAccountType() + " " + ccAccount.getCardTypeDescription() + " " + StringUtil.obscureFormattedCreditCardNumber(ccAccount.getCardNumber()) + " " + ccAccount.getCardExpiry());
		} else if (bankAccount instanceof DebitAccount) {
		    dAccount = (DebitAccount) bankAccount;
		    System.out.println(dAccount.getAccountType() + " " + dAccount.getAccountType() + " " + dAccount.getBsbNumber() + " " + StringUtil.obscureBankAccountNumber(dAccount.getAccountNumber()) + " " + dAccount.getAccountName());
		} else {
		    // ignore
		    System.out.println("No direct debit details found.");
		}
	    }
	    System.out.println("About to process automatic renewals.");
	    renewalMgr.processAutomaticRenewals(fromDate, toDate);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public static void main(String[] args) {
	// String providerURL = "localhost:1099";

	DateTime fromDate = null;
	DateTime toDate = null;

	String hostName = args[0];
	String portNumberString = args[1];
	int portNumber = Integer.parseInt(portNumberString);

	try {
	    String fromDateString = args[2];
	    fromDate = new DateTime(fromDateString);
	} catch (Exception e) {
	    System.err.println("Unable to create default from date. " + e.toString());
	    fromDate = new DateTime().getDateOnly();
	    Interval renewalPeriod = null;
	    try {
		// renewal period
		renewalPeriod = new Interval(1, 0, 0, 0, 0, 0, 0); // 1 year in
								   // arrears
		fromDate = fromDate.subtract(renewalPeriod);
	    } catch (Exception ex) {
		// throw exception and stop program
		System.err.println("Unable to create default from date from renewal period." + ex.getMessage());
		return;
	    }
	}

	try {
	    String toDateString = args[3];
	    toDate = new DateTime(toDateString);
	} catch (Exception e) {
	    System.err.println("Unable to create default to date. " + e.toString());
	    toDate = new DateTime().getDateOnly();
	    try {
		toDate = toDate.add(new Interval(0, 0, 2, 0, 0, 0, 0)); // 2
									// days
									// in
									// advance
	    } catch (Exception ex) {
		// ignore
	    }
	}

	System.out.println("hostName = " + hostName);
	System.out.println("portNumber = " + portNumber);
	System.out.println("fromDate = " + fromDate);
	System.out.println("toDate = " + toDate);

	MembershipAutoRenewalRunner autoRenewalRunner = new MembershipAutoRenewalRunner(hostName, portNumber, fromDate, toDate);

    }
}
