package com.ract.common.qas;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Element;

import com.ract.common.CommonEJBHelper;
import com.ract.common.StreetSuburbMgr;
import com.ract.common.StreetSuburbVO;
import com.ract.common.streetsuburb.StreetSuburbFactory;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XDocument;

public class QASAddress {
    private String suburb;
    private String dpid;
    private String state;
    private String postcode;
    private String street;
    private String stsubid;
    private String propertyQualifier;
    private String property;
    private String userid;

    public QASAddress() {
    }

    public QASAddress(String qasAddress, String street, String property, String propertyQualifier, String userid) {
	this.decodeQASSuburb(qasAddress);

	this.setStreet(street);
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setUserid(userid);

	findStreetSuburbId();

    }

    public String getSuburb() {
	return suburb;
    }

    public String getStsubid() {
	return stsubid;
    }

    public String getDpid() {
	return dpid;
    }

    public String getState() {
	return state;
    }

    public String getPostcode() {
	return postcode;
    }

    public String getPropertyQualifier() {
	return propertyQualifier;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public void setSuburb(String suburb) {
	this.suburb = suburb;
    }

    public void setStsubid(String stsubid) {
	this.stsubid = stsubid;
    }

    public void setDpid(String dpid) {
	this.dpid = dpid;
    }

    public void setState(String state) {
	this.state = state;
    }

    public void setPostcode(String postcode) {
	this.postcode = postcode;
    }

    public void setPropertyQualifier(String propertyQualifier) {
	this.propertyQualifier = propertyQualifier;
    }

    public void setProperty(String property) {
	this.property = property;
    }

    public void setUserid(String userid) {
	this.userid = userid;
    }

    public String getStreet() {
	return street;
    }

    public String getProperty() {
	return property;
    }

    public String getUserid() {
	return userid;
    }

    public void decodeQASSuburb(String qasSuburb) {

	// An Address from QAS has the following structure
	// <QAS>MOONAH 7009 41576376 TAS
	// qas tag suburb 2spaces postcode 2spaces dpid 2spaces state
	//
	// This will return xml like
	// <QASAddress><suburb>MOONAH</suburb><postcode>7009</postcode><state>TAS</state><dpid>41576376</dpid></QASAddress>

	if (qasSuburb.startsWith("<QAS>")) {
	    getQASAddressFields(qasSuburb);
	}

    }

    private void getQASAddressFields(String qasSuburb) {

	// An Address from QAS has the following structure
	// <QAS>MOONAH 7009 41576376 TAS
	// qas tag suburb 2spaces postcode 2spaces dpid 2spaces state

	String tmp[] = qasSuburb.split("  ");

	this.setSuburb(StringUtil.replace(tmp[0], "<QAS>", "").trim());
	this.setPostcode(tmp[1].trim());
	if (tmp.length > 3) {
	    this.setDpid(tmp[2].trim());
	    this.setState(tmp[3].trim());
	} else {
	    this.setDpid(" ");
	    this.setState(tmp[2].trim());
	}

    }

    private void findStreetSuburbId() {

	StreetSuburbMgr ssm = null;
	try {
	    ssm = CommonEJBHelper.getStreetSuburbMgr();
	    ArrayList streets = (ArrayList) ssm.getStreetSuburbList(this.getStreet(), this.getSuburb());

	    StreetSuburbVO sVO = null;
	    Iterator ai = streets.iterator();
	    Integer streetSuburbId = new Integer(-1);

	    while (ai.hasNext() && streetSuburbId.intValue() == -1) {
		sVO = (StreetSuburbVO) ai.next();
		LogUtil.log(this.getClass(), "RLG ++++ checking " + sVO.getStreetSuburbLine());
		if (sVO.getPostcode().equalsIgnoreCase(this.getPostcode())) {
		    streetSuburbId = sVO.getStreetSuburbID();
		}
	    }

	    if (streetSuburbId.intValue() == -1) {
		LogUtil.log(this.getClass(), "RLG ++++ adding street suburb ");
		streetSuburbId = StreetSuburbFactory.getStreetSuburbAdapter().createProgressStreetSuburb(this);
	    }

	    this.setStsubid(streetSuburbId.toString());
	} catch (RemoteException ex) {
	    LogUtil.log(this.getClass(), "Error processing qasaddress " + ex.getMessage());
	} catch (Exception ex1) {
	    LogUtil.log(this.getClass(), ex1.getMessage());
	}

    }

    public String toXML() {
	// StringBuffer xml = new StringBuffer ();

	XDocument doc = new XDocument();

	Element e = (Element) doc.addNode(doc, "QASAddress");

	doc.addLeaf(e, "street", this.getStreet());
	doc.addLeaf(e, "suburb", this.getSuburb());
	doc.addLeaf(e, "state", this.getState());
	doc.addLeaf(e, "postcode", this.getPostcode());
	doc.addLeaf(e, "property", this.getProperty());
	doc.addLeaf(e, "propertyQualifier", this.getPropertyQualifier());
	doc.addLeaf(e, "dpid", this.getDpid());
	doc.addLeaf(e, "userid", this.getUserid());
	doc.addLeaf(e, "stsubid", this.getStsubid());

	return doc.toXMLString();

    }

}
