<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.ui.*"%>
<%@ page import="com.ract.security.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>
<h2>View User Sessions</h2>

<div style="height:400; overflow:auto; border-width: 1; border-style: solid;">

<table>

<tr>
  <td>
    <span class="listHeadingPadded">Session Id</span>
  </td>
  <td>
    <span class="listHeadingPadded">Session Start</span>
  </td>
  <td>
    <span class="listHeadingPadded">User Id</span>
  </td>
  <td>
    <span class="listHeadingPadded">User Name</span>
  </td>
  <td>
    <span class="listHeadingPadded">Sales Branch</span>
  </td>
  <td>
    <span class="listHeadingPadded">IP Address</span>
  </td>
  <td>
    <span class="listHeadingPadded">System Name</span>
  </td>
</tr>

<%
Collection sessions = (Collection)request.getAttribute("sessions");
Iterator it = sessions.iterator();
UserSession us = null;
while (it.hasNext())
{
  us = (UserSession)it.next();
%>
<tr>
  <td>
    <%=us.getSessionId()%>
  </td>
  <td>
    <%=(us.getSessionStart()==null?"":us.getSessionStart().formatLongDate())%>
  </td>
  <td>
    <%=us.getUserId()%>
  </td>
  <td>
    <%=us.getUser().getUsername()%>
  </td>
  <td>
    <%=us.getUser().getSalesBranchCode()%>
  </td>
  <td>
    <%=us.getIp()%>
  </td>
  <td>
    <%=us.getSystemName()%>
  </td>
</tr>

<%
}
%>

</table>

</div>

</body>

</html>
