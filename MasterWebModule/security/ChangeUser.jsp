<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.security.ui.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;
User user = (User)request.getSession().getAttribute(LoginUIConstants.KEY_USER);

SecurityMgr sm          = SecurityEJBHelper.getSecurityMgr();
Collection allRoles     = sm.getRolesUserAllowedToAdd(sm.getAllRoles());
Collection userRoles    = user.getRoles();

SortedSet allRolesSet   = new TreeSet();
SortedSet userRolesSet  = new TreeSet();

SortedSet salesBranches = CommonEJBHelper.getCommonMgr().getSalesBranchCodesAndDescriptions();
SortedSet printers      = CommonEJBHelper.getCommonMgr().getPrinterCodesAndDescriptions();
SortedSet branches      = CommonEJBHelper.getCommonMgr().getBranchRegionsAndDescriptions();
String thisBranch       = new Integer(user.getBranchNumber()).toString();

if ( !(userRoles == null)  )
{
   Iterator it = userRoles.iterator();
   while( it.hasNext() & userRoles.size() > 0 )
   {
      Role r = (Role) it.next();
      userRolesSet.add( new AdminCodeDescription( r.getRoleID().trim(),r.getName().trim()));
   }
}

if ( !(allRoles == null) )
{
   Iterator it = allRoles.iterator();
   while ( it.hasNext() & allRoles.size() > 0 )
   {
      Role r = (Role) it.next();
      AdminCodeDescription ad = new AdminCodeDescription( r.getRoleID().trim(),r.getName().trim());
      if ( !(userRolesSet==null) & !userRolesSet.contains(ad) )
      {
         allRolesSet.add(ad);
      }
   }
}
String statMsg  = (String) request.getAttribute("statusMessage");
String refresh   = (String) request.getAttribute("Refresh");

String event = "createUser";

String saveEvent     = "maintainUser_btnSave";
String deleteEvent   = "maintainUser_btnDelete";
String onBlurEvent   = "onblur=\"checkUserId('maintainUser_checkUserId',this)\"";
SortedSet sStatusList = new TreeSet();
sStatusList.add(new AdminCodeDescription(User.STATUS_ACTIVE,User.STATUS_ACTIVE));
sStatusList.add(new AdminCodeDescription(User.STATUS_INACTIVE,User.STATUS_INACTIVE));

String status = HTMLUtil.stringValue(user,"getStatus");

request.setAttribute("saveEvent",saveEvent);
request.setAttribute("deleteEvent",deleteEvent);
request.setAttribute("onBlurEvent",onBlurEvent);
request.setAttribute("sStatusList",sStatusList);
request.setAttribute("event",event);
request.setAttribute("statMsg",statMsg);
request.setAttribute("refresh",refresh);

request.setAttribute("allRolesSet",allRolesSet);
request.setAttribute("userRolesSet",userRolesSet);
request.setAttribute("salesBranches",salesBranches);
request.setAttribute("printers",printers);
request.setAttribute("branches",branches);
request.setAttribute("thisBranch",thisBranch);

request.setAttribute("user",user);

request.setAttribute("status",status);

%>

<html>

<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>

<script language="Javascript">
function checkUserId( eventName, obj )
{
      document.all.listCode.value = obj.value;
      submitForm( eventName );
}

function validate()
{
   if (document.all.userID.value != '' &&
       document.all.userName.value != '' &&
       document.all.password.value != '')
   {
     return true;
   }
   else
   {
     return false;
   }
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="refreshList(document.all.refresh,'<%=listPage%>')">
<form name="mainForm" method="post" action="<%=LoginUIConstants.PAGE_SecurityUIC%>" onsubmit="return validate();">

<input type="hidden" name="event" value="<%=event%>">
<input type="hidden" name="listCode" value="">
<input type="hidden" name="refresh" value ="<%=refresh%>">

 <div id="mainpage" class="pg">
 <h1>Edit User</h1>
   <div id="contentpage" class="cnt">
        <%@include file="/security/EditUserInclude.jsp"%>
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSaveAndDelete(saveEvent,deleteEvent,"User", statMsg )%>
   </div>
</div>

</form>
</body>
</html>
