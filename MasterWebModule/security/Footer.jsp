<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.ract.user.*"%>

<%
//this is not used

UserSession us = UserSession.getUserSession(session);
if (us == null)
  return;
Integer totalMins = (Integer)session.getAttribute("totalMins");
SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
%>

<hr>

<h3>Current Session Details</h3>

<table border=1>
  <tr>
    <td>User Id:</td><td><%=us.getUserId()%></td>
  </tr>
  <tr>
    <td>Username:</td><td><%=us.getUser().getUsername()%></td>
  </tr>
  <tr>
    <td>Privileges:</td><td>
    <%
    Collection a = us.getUser().getPrivilegeList();
    Iterator it = a.iterator();
    while (it.hasNext()) {
      out.println(it.next()+"<br>");
    }
    %>

    </td>
  </tr>

  <tr>
    <td>Branch:</td><td><%=us.getUser().getSalesBranchCode()%></td>
  </tr>
  <tr>
    <td>Encrypted Password:</td><td><%=us.getUser().getPassword()%></td>
  </tr>
  <tr>
    <td>IP Address:</td><td><%=us.getIp()%></td>
  </tr>
  <tr>
    <td><b>System:</b></td><td><b><%=us.getSystemName()%></b></td>
  </tr>
  <%
    if (totalMins != null) {
  %>
   <tr>
   <td>Mins logged in:</td><td><%=totalMins.toString()%></td>
   </tr>
  <%
    }
  %>

  <tr>
    <td>Session created:</td><td><%=df.format(new Date(session.getCreationTime()))%></td>
  </tr>
  <tr>
    <td>Session last accessed:</td><td><%=df.format(new Date(session.getLastAccessedTime()))%></td>
  </tr>
  <tr>
    <td>Current time:</td><td><%=df.format(new Date(System.currentTimeMillis()))%></td>
  </tr>
  <tr>
    <td>Inactive after:</td><td><%=session.getMaxInactiveInterval()%></td>
  </tr>
  <tr>
    <td>Session Id:</td><td><%=session.getId()%></td>
  </tr>

</table>


</body>
</html>
