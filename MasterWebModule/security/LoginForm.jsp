<%@page import="com.ract.common.*"%>
<%@ page import="com.ract.security.ui.*" %>

<html>
<head>
  <title>
    LoginForm
  </title>
</head>

<%
//get the system name to display
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
String systemName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,SystemParameterVO.TYPE_SYSTEM);
%>

<body onload="document.all.user.focus()">

<table width="100%" height="100%">

<tr valign="top" align="center">
<td>

<form action="<%=LoginUIConstants.PAGE_LoginUIC%>" method="POST">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr align="center">
<!--             <td colspan="2"><img src="../images/RACTSmall.gif" width="150" height="150"></td>-->
            <td colspan="2"><img src="https://www.ract.com.au/-/media/project/ract-group/ract/external-links/ract-logo-200" width="150" height="150"></td>
          </tr>
          <tr align="center">
            <td colspan="2"><br /><span class="dataValueBlue"><%=systemName.toUpperCase()%></span></td>
          </tr>
          <tr align="center">
            <td colspan="2"><span class="errorText">Authorised access only</span></td>
          </tr>
          <tr align="center">
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">Enter username and password to log in:</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="33%">User:</td>
            <td>
              <input type=text name=user>
            </td>
          </tr>
          <tr>
            <td width="33%">Password:</td>
            <td>
              <input type=password name=pass>
            </td>
          </tr>
          <tr>
            <td>&nbsp; </td>
            <td>
              <input type=submit value="Log In" name="submit">
            </td>
          </tr>
        </table>
</form>

<%
String message = (String)request.getAttribute(LoginUIConstants.MESSAGE);
if (message!=null)
{
%>
  <span class="errorText"><%=message%></span>
<%
}
else
{
  message = request.getParameter(LoginUIConstants.MESSAGE);
  if (message!=null) {
%>
  <span class="helpTextSmall"><%=message%></span>
<%
  }
}
%>

</td>
</tr>

</table>

</body>
</html>
