<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.ui.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
User user = (User)request.getAttribute("user");
%>

<body>
<h2>Change Password </h2>

<form method="POST" action="<%=LoginUIConstants.PAGE_SecurityUIC%>">

<table border="0" cellspacing="1" cellpadding="1">

  <input type="hidden" name="event" value="changePassword">
    <tr>
      <td>User Id:</td>
     <td>
        <input type="text" name="userID" value="<%=user==null?"":user.getUserID()%>">
      </td>
	  </tr>
    <tr>
      <td>New Password:</td>
      <td>
        <input type="password" name="newPassword">
      </td>
    </tr>
    <tr>
      <td>Confirm Password:</td>
      <td>
        <input type="password" name="verifyPassword">
      </td>
    </tr>
    <tr>
      <td>
        <input type="submit" name="Submit" value="Change">
      </td>
      <td>&nbsp; </td>
    </tr>
    <tr>
    <td colspan="2">
<%
String message = (String)request.getAttribute(LoginUIConstants.MESSAGE);
if (message != null)
{
%>
  <span class="infoTextSmall"><%=message%></span>
<%
}
%>

    </td>
    </tr>
  </table>
</form>

</body>

</html>
