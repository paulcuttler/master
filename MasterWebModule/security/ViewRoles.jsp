<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.ui.*"%>
<%@ page import="com.ract.security.*"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

</head>

<body>

<h2>View Roles and Privileges</h2>

<table>
<tr class="headerRow">
  <td class="listHeadingPadded">Id</td><td class="listHeadingPadded">Name</td><td class="listHeadingPadded">Description</td>
</tr>

<%
SecurityMgr securityMgr = SecurityEJBHelper.getSecurityMgr();
Collection roles = securityMgr.getAllRoles();

Iterator roleIt = roles.iterator();
int i = 0;
while (roleIt.hasNext())
{

  Role role = (Role)roleIt.next();
%>
<tr class="headerRow">
  <td><%=role.getRoleID()%></td><td><%=role.getName()%></td><td><%=role.getDescription()!=null?role.getDescription():""%><td>
</tr>
<%
  String roleID = role.getRoleID();
  Collection privs = securityMgr.getRolePrivileges(roleID);

  Iterator privIt = privs.iterator();
  i = 0;
  while (privIt.hasNext())
  {
    i++;
    Privilege priv = (Privilege)privIt.next();
%>
<tr class=<%=HTMLUtil.getRowType(i)%>>
  <td><%=priv.getPrivilegeID()%></td><td><%=priv.getName()%></td><td><%=priv.getDescription()%></td>
</tr>
<%
  }
}
%>
</table>
</body>
</html>
