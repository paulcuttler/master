<%@ page import="com.ract.user.*" %>
<%@ page import="com.ract.security.*" %>
<%@ page import="com.ract.util.*" %>
<%@ page import="com.ract.security.ui.*" %>
<%@ page import="com.ract.common.*" %>

<%
final String MESSAGE_NO_ACCESS = "You are not logged in.";

UserSession userSession = null;

//authentication flag
boolean authenticated = false;

//check if security is enabled
CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
String sec = FileUtil.getProperty("master",SystemParameterVO.SECURITY_ENABLED);
boolean securityOn = Boolean.valueOf(sec).booleanValue();

if (securityOn)
{
  authenticated = LoginHandler.isLoggedIn(session.getId(),session);
}
else
{
  authenticated = true;
}

if (authenticated)
{
  userSession = ServletUtil.getUserSession(request, securityOn);
}

if (userSession == null)
{
  //forward to no access page
  //request dispatcher throws an error so we have to use a response redirect
  response.sendRedirect(LoginUIConstants.PAGE_Login+"?"+LoginUIConstants.MESSAGE+"=" + MESSAGE_NO_ACCESS);
  return;
}
%>
