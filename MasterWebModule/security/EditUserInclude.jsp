<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.util.*"%>

<%
User theUser = (User)request.getAttribute("user");

String theOnBlurEvent = (String)request.getAttribute("onBlurEvent");
String theBranch = (String)request.getAttribute("thisBranch");
String theStatus = (String)request.getAttribute("status");

SortedSet theBranches = (SortedSet)request.getAttribute("branches");
SortedSet theSalesBranches = (SortedSet)request.getAttribute("salesBranches");
SortedSet thePrinters = (SortedSet)request.getAttribute("printers");
SortedSet theStatusList = (SortedSet)request.getAttribute("sStatusList");
SortedSet theRolesSet = (SortedSet)request.getAttribute("allRolesSet");
SortedSet theUserRolesSet = (SortedSet)request.getAttribute("userRolesSet");
%>

  <table width="80%" border="0" cellspacing="1" cellpadding="1">

    <tr valign="top">
    <td width="15%" class="label" >User Id</td>
    <td width="85%" class="dataValue"> <%=HTMLUtil.inputOrReadOnly("userID",HTMLUtil.stringValue(theUser,"getUserID"),theOnBlurEvent)%>
    </td>
    </tr>

    <tr valign="top">
    <td width="15%" class="label">User Name:</td>
    <td width="85%"  class="dataValue">
      <input type="text" name="userName" value="<%=HTMLUtil.stringValue(theUser,"getUsername")%>">
      </td>
    </tr>

    <tr valign="top">
    <td width="15%" class="label">Password:</td>
    <td width="85%" class="dataValue">
      <%
        String password = HTMLUtil.stringValue(theUser,"getPassword");
        if (password.equals(""))
        {
        %>
           <input type="password" name="password">
        <%
        }
        else
        {
        %>
          <p>
            <span class="helpText">Passwords may only be changed through the "change password" facility or created when a new user is created.</span>
             <input type="hidden" name="password" value="<%=password%>">
          </p>
           <%

        }
        %>
      </td>
    </tr>

    <tr valign="top">
    <td width="15%" class="label">Branch:</td>
    <td width="85%" class="dataValue">
        <%=HTMLUtil.selectList( "branch",
                                 theBranches,
                                 theBranch,
                                 "",
                                 "No Branch"
                               )
         %>
    </td>
    </tr>

    <tr valign="top">
    <td width="15%" class="label">Sales Branch:</td>
    <td width="85%" class="dataValue"> <%=HTMLUtil.selectList( "salesBranch",
                                                               theSalesBranches,
                                                               theUser.getSalesBranchCode(),
                                                               "",
                                                               "No Sales Branch"
                                                             )
         %>
    </td>
    </tr>

    <tr valign="top">
    <td width="15%" class="label">Print Group:</td>
    <td width="85%" class="dataValue"> <%=HTMLUtil.selectList( "printGroup",
                                                               thePrinters,
                                                               theUser.getPrinterGroup(),
                                                               "",
                                                               "No Print Group"
                                                             )
         %>
    </td>
    </tr>

    <tr valign="top">
    <td width="15%" class="label">Email:</td>
    <td width="85%" class="dataValue">
      <input type="text" name="email" size="50" value="<%=HTMLUtil.stringValue(theUser,"getEmailAddress")%>">
      </td>
    </tr>

          <tr>
            <td class="label" valign "top">Status:</td>
            <td class="dataValue" valign "top">
               <%=HTMLUtil.selectList("selectStatus",theStatusList,theStatus,User.STATUS_ACTIVE,User.STATUS_ACTIVE)%>

            </td>
          </tr>

         <tr>
          <td colspan="2">
                  <%=HTMLUtil.codeDescriptionTable (  "All Roles.",
                                                      theRolesSet,
                                                      "maintainUserRoles_btnAdd",
                                                      "Double click to ADD this Role.",
                                                      "No Roles to be selected"
                                                   )
                  %>

          </td>
          </tr>

          <tr>
          <td colspan="2">

                  <%=HTMLUtil.codeDescriptionTable (  "User Roles.",
                                                      theUserRolesSet,
                                                      "maintainUserRoles_btnRemove",
                                                      "Double click to REMOVE this Role.",
                                                      "No selected Roles"
                                                   )
                  %>
         </td>
         </tr>

  </table>

