<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.ui.*"%>
<%@ page import="com.ract.security.*"%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

<script language="">
function submitForm(event)
{
  document.all.event.value = event;
  document.viewUser.submit();
}

</script>

</head>

<%
User user = (User)request.getAttribute("user");
%>

<body bgcolor="#FFFFFF" text="#000000">
<h2>View User</h2>



<table border="0" cellspacing="1" cellpadding="1">
  <tr valign="top">
    <td>User Id</td>
    <td class="dataValue"><%=user.getUserID()%></td>
  </tr>
  <tr valign="top">
    <td>User Name:</td>
    <td class="dataValue"><%=user.getUsername()%></td>
  </tr>
  <tr valign="top">
    <td>Sales Branch:</td>
    <td class="dataValue"><%=user.getSalesBranch()%></td>
  </tr>
  <tr valign="top">
    <td>Print Group:</td>
    <td class="dataValue"><%=user.getPrinterGroup()%></td>
  </tr>
  <tr valign="top">
    <td>Email:</td>
    <td class="dataValue"><%=user.getEmailAddress()%></td>
  </tr>
  <tr valign="top">
    <td><b>Roles</b></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
Role role = null;
Collection c = user.getRoles();
int count = 0;
if (c != null)
{
  Iterator uit = c.iterator();
  while (uit.hasNext())
  {
    count++;
    role = (Role)uit.next();
%>
        <tr>
          <td><%=count%></td>
          <td><%=role.getName()%></td>
          <td><%=role.getDescription()%></td>
        </tr>
        <%
  }
}
if (count == 0)
{
%>
        <tr>
          <td colspan="2">No roles found.</td>
        </tr>
        <%
}
%>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <form name="viewUser" method="post" action="<%=LoginUIConstants.PAGE_SecurityUIC%>">
        <input type="hidden" name="userID" value="<%=user.getUserID()%>">
        <input type="button" value="Change Password" onclick="submitForm('viewChangePassword');">
        <%--
        <input type="button" value="Change User" onclick="submitForm('changeUser');">
        --%>
        <input type="hidden" name="event">
      </form>
    </td>
  </tr>
</table>
</body>
</html>
