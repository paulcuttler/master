<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.ui.*"%>
<%@ page import="com.ract.security.*"%>

<html>

<%@include file="/security/VerifyAccess.jsp"%>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
//get the current user from the session
User user = userSession.getUser();
if (user == null)
  throw new ServletException("User not logged in.");
%>

<body>
<h2>Change Password </h2>

<table border="0" cellspacing="1" cellpadding="1">
<form method="POST" action="<%=LoginUIConstants.PAGE_SecurityUIC%>">

  <input type="hidden" name="event" value="changeMyPassword">
    <tr>
      <td>User Id:</td>
      <td class="dataValue">
        <%=user.getUserID()%>
        <input type="hidden" name="userID" value="<%=HTMLUtil.stringValue(user,"getUserID")%>">
      </td>
      </tr>

      <tr>
        <td>
        User Name:
        </td>
        <td class="dataValue">
          <%=HTMLUtil.stringValue(user,"getUsername")%>
        </td>
      </tr>

    <tr>
      <td>New Password:</td>
      <td>
        <input type="password" name="newPassword">
      </td>
    </tr>
    <tr>
      <td>Confirm Password:</td>
      <td>
        <input type="password" name="verifyPassword">
      </td>
    </tr>
    <tr>
      <td>
        <input type="submit" name="Submit" value="Change">
      </td>
      <td>&nbsp; </td>
    </tr>
    <tr>
    <td colspan="2">
<%
String message = (String)request.getAttribute(LoginUIConstants.MESSAGE);
if (message != null)
{
%>
  <span class="infoTextSmall"><%=message%></span>
<%
}
%>

    </td>
    </tr>
</form>
  </table>


</body>

</html>
