<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.travel.*"%>

<table border="0" cellspacing="0" cellpadding="0" width="400">
<tr>
<%
String tramadaEnabled = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA,SystemParameterVO.TRAMADA_ENABLE);
if (tramadaEnabled != null && new Boolean(tramadaEnabled).booleanValue()) {
	ClientVO tramadaClient = (ClientVO) request.getAttribute("client");
	
	//get the tramada profile code from the list of recorded transactions
	TramadaClientTransaction tramadaClientTransaction = TramadaClientHelper.getTramadaClientTransaction(tramadaClient.getClientNumber());
	if (tramadaClientTransaction == null)
	{
%>
  <td>
    <a class="actionItem"
       title="Transfer client to Tramada"
       href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=createTramadaClient&clientNumber=<%=tramadaClient.getClientNumber()%>">
       Transfer client to Tramada
    </a>
  </td>
<%
	}
	else if (tramadaClientTransaction.getTramadaClientTransactionPK().getActionType().equals(TramadaClientProfileImporter.TRAMADA_ACTION_DELETE))
	{
%>
  <td class="helpText" colspan="3" nowrap="nowrap">Tramada client profile has been terminated.</td>
  <!-- reinstate -->
  <td>
    <a class="actionItem"
       title="Reinstate Tramada client"
       href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=reinstateTramadaClient&clientNumber=<%=tramadaClient.getClientNumber()%>">
       Reinstate Tramada client
    </a>
  </td>
<%
	}
	else
	{
%>
  <td class="label" width="180" nowrap="nowrap">Tramada client profile code: </td><td width="220" class="dataValue" nowrap="nowrap"><%=tramadaClientTransaction.getTramadaClientTransactionPK().getTramadaProfileCode()%></td>
<%--  <td class="label" colspan="1" nowrap="nowrap">Debtor Code:</td><td colspan="3" class="dataValue" nowrap="nowrap"><%=tramadaClientTransaction.getCompanyId()==null?"<i>RETAIL</i>":tramadaClientTransaction.getCompanyId()%></td> --%>
<%
	}
}
%>
</tr>
</table>
