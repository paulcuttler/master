<%@page import="com.ract.common.*"%>
<%@page import="com.ract.insurance.ui.*"%>
<%@page import="com.ract.user.*"%>

<html>
<head>
<title>Insurance Excesses</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<%
  UserSession thisSession = UserSession.getUserSession(session);
%>
<!--<script type="text/javascript" src="C:/Class/DOJO_hullo_world/js/dojo/dojo.js"></script>-->
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<!--<script type="text/javascript"
        src="<%=CommonConstants.DIR_INSURANCE%>/scripts/.js"></script>-->

<script type="text/javascript">
  	dojo.require("dojo.lang.*");
	dojo.require("dojo.widget.Tree");
	dojo.require("dojo.widget.TreeNode");
	dojo.require("dojo.widget.TreeSelector");
	dojo.require("dojo.widget.TreeRPCController");
	dojo.require("dojo.io.*");
	dojo.require("dojo.event.*");
    dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.TreeContextMenu");
var DELIM = "<%=InsuranceUIC.DELIM%>";
var myTreeWidget;
var treeExists = false;
var treeSelector;
var treeController;
//var serverURL = 'http://localhost:8080/InsuranceUIC';
var serverURL = "/InsuranceUIC";
var tNode=null;
var TreeBuilder = {
	buildTreeNodes:function (dataObjs, treeParentNode)
                        {
                           for(var i=0; i<dataObjs.length;i++)
                           {
			       var node =  dojo.widget.createWidget("TreeNode",
                                                {
				                   title:dataObjs[i].title,
				                   isFolder: true,
				                   objetId:dataObjs[i].objectId
			                         });
			       treeParentNode.addChild(node);
			       treeParentNode.registerChild(node,i);
		            }
	                 },
	buildTree:function ()
                 {
		    var placeHolder = document.getElementById("treePlaceHolder");
		    var treeContainer = document.getElementById("myWidgetContainer");
		    myTreeWidget = dojo.widget.createWidget("Tree",{widgetId:"myTreeWidget",
		                                                    selector:'tSelector'});
                   var rootNode = [{title:"Excesses"}];
	           this.buildTreeNodes(rootNode,myTreeWidget);
	           treeContainer.replaceChild(myTreeWidget.domNode,placeHolder);
	           treeExists=true;
	         },
	replaceTree:function()
	           {
		     var treeContainer = document.getElementById("myWidgetContainer");
                     var placeHolder = document.createElement('span');
		     placeHolder.id = "treePlaceHolder"
		     placeHolder.innerHTML='Loading changed data';
                     treeContainer.replaceChild(placeHolder,myTreeWidget.domNode);
                     myTreeWidget.destroy();

		     var rootNode = [{title:"Excesses"}];
		     myTreeWidget = dojo.widget.createWidget("Tree",{widgetId:"myTreeWidget",
		                                                selector:'tSelector'});
                     this.buildTreeNodes(rootNode,myTreeWidget);
                    treeContainer.replaceChild(myTreeWidget.domNode,placeHolder);
	           } ,
         addController:function()
	 {
	    treeController = dojo.widget.createWidget("TreeBasicController",
	                                              {widgetId:"treeController",
	                                               DNDController:"create"});
         }

   }


function addType()
{
    if(tNode==null)
    {
      alert("First select the root, a risk type or effective date node");
      return;
    }
    var id = tNode.objectId;
    var t = id.split(DELIM);

    if(t[0]== ""
      || t[0] == "prodType"
      || t[0] == "riskType"
      || t[0] == "effDate")
    {
       handleRequest("setExcessType.jsp","newType");
    }
    else
    {
      alert("First select the root, risk type or effective date");
    }

}
function newVersion()
{
    if(tNode==null)
    {
      alert("First select a date node or type node");
      return;
    }
    var id = tNode.objectId;
    var t = id.split(DELIM);
    if(t[0]== "xsSeqNo")
    {
       handleRequest("setExcessType.jsp","newVersion");
    }
    else if(t[0] == "effDate")
    {
       var newEffDate = prompt("Enter new effective date",t[2]);
       var tbl = document.getElementById('detailDisp');
       tbl.style.display='none';
       dojo.io.bind({url:serverURL,
                    handler:refreshTree(),
	            content:{event:"newXsTypeVersions",
		             objectId:tNode.objectId,
                             newEffDate:newEffDate},
                    mimetype:"application/json"});
    }
    else
    {
      alert("First select a version (date)");
    }
}
function handleNewNode(type,data,evt)
{
   alert("add new node to current node"
      + "\n" + tNode.objectId
      + "\n" + data);
}
function newDetail()
{
    if(tNode==null)
    {
      alert("First select an excess type node");
      return;
    }
    var id = tNode.objectId;
    if(id.search("xsSeqNo")>-1) handleRequest("setExcessDetail.jsp","addDetail");
    else alert("First select an excess type");
}
function editDetail()
{
   if(tNode==null)
   {
     alert("First select an excess type node");
     return;
   }
   var id = tNode.objectId;
   if(id.search("excessAmt")>-1)
   {
     alert("Add or Delete options only");
   }
   else if(id.search("excess")>-1) handleRequest("setExcessDetail.jsp","editDetail");
   else if(id.search("xsSeqNo")>-1) handleRequest("setExcessType.jsp","editType");
   else alert("First select an excess detail");
}
function addOption()
{
    if(tNode==null)
    {
      alert("First select an excess type or Option node");
      return;
    }
    var id = tNode.objectId;
    if(id.search("options")>-1
       || id.search("xsSeqNo")>-1) handleRequest("setExcessOptions.jsp","addOption");
    else alert("First select an excess type");
}
function addLine()
{
   if(tNode==null)
   {
     alert("First select an excess detail node");
   }
   var id = tNode.objectId;
   if(id.search("excess")>-1
   && tNode.isFolder==true) handleRequest("setExcessLines.jsp","addLine");
   else
   {
     alert("Only available for multi line excesses");
   }
}
function handleRequest(nextWindow,requestType)
{
    var  url="insurance/" + nextWindow
             + "?action=" + requestType
             + "&userId=<%=thisSession.getUserId()%>"
             + "&id=" + tNode.objectId;
    var win = window.open(url,"_blank","height=500,width=500,left=50,top=50,"
                    + "resizable=yes,status=no,scrollbars=yes,toolbar=no,menubar=no,location=no");
    win.focus();
}
function deleteBranch()
{
    var tbl = document.getElementById('detailDisp');
    tbl.style.display='none';
//    var url;
    var treeLevel = tNode.objectId.split(DELIM)[0];
    if(treeLevel==""
       || treeLevel=="riskType"
       || treeLevel=="effDate"
       || treeLevel=="options")
    {
       alert("Cannot delete from this node");
       return;
    }
    dojo.io.bind({url:serverURL,
	          load:removeNode,
                  error:handleError,
		  content:{event:"deleteExcessNode",
		            objectId:tNode.objectId},
                  mimetype:"text/plain"});
}
function handleError(type,data,evt)
{
   alert("An Error occurred:\n" + data);
}
function removeNode(type,data,evt)
{

  if(data=="Error")
  {
    alert("Entries can only be removed on the create date");
  }
  else
  {
    TreeActions.removeNode(tNode,"treeController");
  }
 }

function treeClickHandler(message)
{
  tNode = message.node;
  var objectId = message.node.objectId;
  var tbl;
  var key1;
  var key2;
  var obArray;
  var keyArray;
  showDetail("","","");
  if(objectId!="")
  {
    obArray = objectId.split(DELIM);
    tbl = obArray[0];
    if(tbl!=""
       && tbl!="options"
       && tbl!="effDate"
       && tbl!="riskType")
    {
	getDetails(objectId);
    }
  }
}
function getDetails(objectId)
{
    var tbl = document.getElementById('detailDisp');
    tbl.style.display='none';
    var tObjectId = objectId;
    if(objectId=="")
    {
      var pNode = tNode.parent;
      tObjectId = "newType~" + pNode.objectId + tNode.title;
    }
    else
    {
      tObjectId = objectId;
    }

    dojo.io.bind({url:serverURL,
	           handler:showDetail,
		   content:{event:"getExcessDetail",
		            objectId:tObjectId},
                   mimetype:"application/json"});
}

function showDetail(type,data,evt)
{
    var e;
	var tbl = document.getElementById('detailDisp');
	tbl.style.display='';

	try
        {
        var bTable=dojo.widget.byId('vTable');
        bTable.store.setData(data);
        }
        catch(e)
        {
          alert("Error: " + e);
           //seem to get an error (which is actually a security warning)
           //everytime, but the procedure works, so do nothing.
	}

}

function loadTree()
{
    if(treeExists)TreeBuilder.replaceTree();
    else TreeBuilder.buildTree();
	var myRpcController = dojo.widget.createWidget("TreeRPCController",
	                                               {widgetId:"treeController",
			                                RPCUrl:serverURL+"?event=loadInsExcesses"});
	myRpcController.onTreeClick = function(message)
	                               {var node = message.source;
		                            if (node.isExpanded){
			                          this.expand(node);
		                            }
									else {
			                          this.collapse(node);
		                            }

									};
	var treeContainer = document.getElementById("myWidgetContainer");
	treeContainer.appendChild(myRpcController.domNode);
	if(treeExists)myRpcController.listenTree(dojo.widget.manager.getWidgetById("myTreeWidget"));
	treeExists = true;
}
var TreeActions = {addNewNode: function(parent,
                                        controllerId,
					nodeId,
					nodeCaption,
                                        isFolder)
                                  {
                                    this.controller = dojo.widget.manager.getWidgetById(controllerId);
                                    if (!parent.isFolder) {parent.setFolder();}
                                    var res = this.controller.createChild(parent, 0, {title:nodeCaption,
                                                                                      objectId:nodeId});
                                    if(isFolder) res.setFolder();
                                  },
                   removeNode: function(node,controllerId)
                                  {
                                    if (!node)
                                    {
                                       alert("Nothing selected to delete");
                                       return false;
                                    }
                                    this.controller = dojo.widget.manager.getWidgetById(controllerId);
                                    var res = this.controller.removeNode(node, dojo.lang.hitch(this));}};


function init()
{
        dojo.event.topic.subscribe("nodeSelected", treeClickHandler);
        loadTree();
	//TreeBuilder.addTreeContextMenu();
	//TreeBuilder.bindEvents();
	TreeBuilder.addController();

}
function clearDetails()
{
   var tbl = document.getElementById('detailDisp');
   tbl.style.display='none';
}
function handleReturnedData()
{
  var retValue = document.getElementById("pListener").value;
  var pList=retValue.split("&");
  var event=pList[0].split("=")[1];
  var pNodeId = pList[1].split("=")[1];
  var pNodeName = pList[2].split("=")[1];
  var isFolder = false;
  clearDetails();
  if(event=="newVersion")
  {
    loadTree();
  }
  else if(event=="newType")
  {
/*     if(pNodeId=="")
     {
       var parentNode = tNode.parent;
       var parentNodeId = parentNode.objectId;
       var gpNode = parentNode.parent;
       var parentNodeTitle = parentNode.title;
       TreeActions.removeNode(parentNodeId,"treeController");
       TreeActions.addNewNode(gpNode,"treeController",parentNodeId,parentNodeTitle,true);
     }
     else
     {
       isFolder=true;
       TreeActions.addNewNode(tNode,"treeController",pNodeId,pNodeName,isFolder);
     }*/
     loadTree();
  }
  else if(event=="editType")
  {
     //loadTree(); do nothing - tree should be unchanged
     getDetails(pNodeId);
  }
  else if(event=="addDetail")
  {
//        loadTree();
    	 TreeActions.addNewNode(tNode,"treeController",pNodeId,pNodeName,false);
  }
  else if(event=="editDetail")
  {
    // loadTree();  // do nothing to the tree.  No new or removed nodes
    getDetails(pNodeId);
  }
  else if(event=="addOptions")
  {
    var tNodeId = tNode.objectId;
    var parent = tNode.parent;
    if(pNodeName=="0")
    {
       TreeActions.addNewNode(tNode,"treeController",pNodeId,"Options",true);
    }
    else if(tNodeId.search("options")>-1)
    {
      //this is the "Options" node, just add the new leaf
      var xsSeqNo = tNodeId.split(DELIM)[2];
      var newNodeList = pList[3].split("=")[1];
      var newNodes = newNodeList.split(DELIM);
      var optNodeId = "";
      for(var xx=0;xx<newNodes.length;xx++)
      {
         if(newNodes[xx]!="")
         {
           optNodeId = "excessAmt" + DELIM + xsSeqNo + DELIM + newNodes[xx];
           TreeActions.addNewNode(tNode,"treeController",optNodeId,newNodes[xx],false);
         }
      }
    }
    else if(event=="addLines")
    {
      alert("Lines added");
    }
    else loadTree();
  }
}

dojo.addOnLoad(init);

</script>
</head>
<body>
<%@include file="/menu/PopupHeader.jsp"%>

<h2>Insurance Excesses</h2>

<form name="form1" method="post" action="">
</form>
<hr />
<table height="394">
  <tr>
    <td valign="top">
      <div dojoType="TreeSelector" id="myWidgetContainer" eventNames="select:nodeSelected" widgetId="tSelector"
	                               style="border: solid #888 1px;">
	<span id="treePlaceHolder" style="background-color:#F00; color:#FFF">
		Loading tree widget...
	</span>
</div>
</td>
    <td valign="top" ><span id='detailDisp'>
      <table  dojoType="filteringTable"
              id="vTable"
              cellpadding="2"
              alternateRows="true"
              class="dataTable" >
        <thead >
        <tr>
	      <th field="title" dataType="String" align="left" valign="top" >Heading</th>
	      <th field="value" dataType="Integer" align="left" valign="top" >Value</th>
	</tr>
     </thead>
   <tbody>
   </tbody>
   </table>
   </span>
</td>
</tr>
</table>
<table>
  <tr>
    <td>
      <input type="button" name="addType" value="Add Type" onclick="addType()">
    </td>
    <td>
      <input type="button" name="newDetail" value="Add Detail" onclick="newDetail()">
    </td>
    <td>
      <input type="button" name="addOption" value="Add Option" onclick="addOption()">
    </td>
    <td>
      <input type="button" name="addLine" value="Add Line" onclick="addLine()">
    </td>

    <td>
      <input type="button" name="delete" value="Delete" onclick="deleteBranch()"
             title="Delete a branch and everything under it.  Only available on the day the branch is created">
    </td>
  </tr>
  <tr>
    <td>
      <input type="button" name="newVersion" value="New Version" onclick="newVersion()">
    </td>
    <td>
      <input type="button" name="editDetail" value="Edit" onclick="editDetail()"/>
    </td>
  </tr>
</table>
<input type="hidden" name="pListener" onclick="handleReturnedData()"/><br />
<input type="button" name="refreshTree" value="Refresh" onclick="init()" />
</body>
</html>
