<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.insurance.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.insurance.ui.InsuranceUIC"%>

<html>
<head>
<title>Excess Options</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>

<%
String id = request.getParameter("id");
String requestType = request.getParameter("action");
//LogUtil.log("setExcessOptions.jsp","\n------------------------"
//                                 + "\n" + id
//                                 + "\n" + requestType);
String seqNoStr = null;
XSLine xsl = null;
String[] t = id.split(InsuranceUIC.DELIM);
Integer xsSeqNo = new Integer(t[1]);
String  xsCode = t[2];

ArrayList optionList = null;
   try
   {
     ServiceLocator serviceLocator = ServiceLocator.getInstance();
     InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
     String companyName = SourceSystem.INSURANCE_RACT.getAbbreviation();
     optionList = insAdapter.getXsLines(xsSeqNo,xsCode,companyName);
   }
   catch(Exception ex)
   {
     ex.printStackTrace();
   }

%>

<SCRIPT LANGUAGE="JavaScript">
//var thisRow = <%=optionList.size()%>;
var startCount = <%=optionList==null?0:optionList.size()%>;
var thisRow = startCount;
var DELIM = "<%=InsuranceUIC.DELIM%>";

function addRowToTable(){
    thisRow++;
    var tbody = document.getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    var td1 = document.createElement("TD");
	var ds = document.createElement("input");
	ds.type = "text";
	ds.name = "displaySeq" + DELIM + thisRow;
        ds.title = "Display Sequence"
                 +"\nDetermines the order in which the lines will be displayed ";
        ds.size = "10";
	td1.appendChild(ds);
    var td2 = document.createElement("TD");
	var text = document.createElement("input");
	text.type = "text";
	text.name = "lineText" + DELIM + thisRow;
        text.title = "The text to be displayed on this line";
        text.size = "45";
        text.value = "";
	td2.appendChild(text);
    var td3 = document.createElement("TD");
	var la = document.createElement("input");
	la.type = "text";
	la.name = "lineAmount" + DELIM + thisRow;
        la.title = "Excess amount for this line";
        la.size = "8";
        td3.appendChild (la);

    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    tbody.appendChild(row);
}

function submitForm()
{
   document.forms[0].submit();
   var nodeId = "xsLines" + DELIM
              + document.getElementById("xsSeqNo").value + DELIM
              + document.getElementById("xsCode").value;
   var newNodeList="";
   var bRow;
   var tOptions = document.getElementById("optTable");

   for(var xx =startCount+1;xx<tOptions.rows.length;xx++)
   {
      bRow = tOptions.rows[xx];
      if(newNodeList!="")
      {
        newNodeList = newNodeList + DELIM;
      }
      newNodeList = newNodeList + bRow.cells[0].firstChild.value;
   }
   sendToParent("event=addLines"
              + "&objectId=" + nodeId
              + "&startCount=" + document.getElementById("startCount").value
              + "&newNodeList=" + newNodeList);
   window.close();
}
function sendToParent(retMessage)
{
  window.opener.document.getElementById("pListener").value = retMessage;
  // raise button click event of parent window
  window.opener.document.getElementById("pListener").click();
}

</script>

<body bgcolor="#FFFFFF" text="#000000">
<form name="form1" method="post" action="/InsuranceUIC">
<h1>Excess Amounts</h1>
<table id="optTable" width="75%" border="0" cellspacing="2" cellpadding="2">

  <thead>
  <tr>
      <td>Display Sequence</td>
      <td>Line Text</td>
      <td>Line Amount</td>
  </tr>
  </thead>
  <tbody name="TBODY">
    <% for(int xx=0;xx<optionList.size();xx++)
       {
         xsl = (XSLine)optionList.get(xx);
         %><tr>
             <td><%=xsl.getDisplaySeq()%></td>
             <td><%=xsl.getLineText()%></td>
             <td><%=xsl.getLineAmount()%></td>
          </tr>
         <%
       }
       %>
  </tbody>
</table>
  <p>
    <input type="button" name="addRow" value="Add Row" onclick="addRowToTable();">
    <input type="button" name="save" value="Save" onclick="submitForm()"/>
    <input type="hidden" name="event" value="setExcessLines"/>
    <input type="hidden" name="xsSeqNo" value="<%=xsSeqNo%>"/>
    <input type="hidden" name="startCount" value="<%=optionList==null?0:optionList.size()%>"/>
    <input type="hidden" name="xsCode" value="<%=xsCode%>">

  </p>
</form>
</body>
</html>
