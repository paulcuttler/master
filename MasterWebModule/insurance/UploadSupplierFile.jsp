<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.ract.insurance.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
<s:head/>
<head>

<body>
<h2>Upload Supplier File</h2>

<s:form action="uploadSupplierFile.action" method="POST" enctype="multipart/form-data">

<p>
<span class="helpText">This screen will allow you to upload a tab delimited file containing supplier data. The data will
then be available via the search facility on the Intranet. To upload a file select the supplier file and then click on
the upload button.</span>
</p>

<table>
<tr>
  <s:file label="File to upload:" name="upfile"/>
</tr>
<tr>
  <td colspan="2"><span class="helpTextSmall">Click browse and select the tab delimited file containing supplier data.</span></td>
</tr>
<tr><s:submit/>
</tr>
</table>

</s:form>


<s:if test="upfile != null">
<table>
<tr><td class="headerRow" colspan="2">Upload Results:</td></tr>
<tr><td class="label">Records loaded</td><td class="dataValue"><s:property value="recordCount"/></td></tr>
<tr><td class="label">File</td><td class="dataValue"><s:property value="upfileFileName"/></td></tr>
<tr><td class="label">File Size</td><td class="dataValue"><s:property value="fileSize"/> kb</td></tr>
</table> 
</s:if>

</body>
</html>
