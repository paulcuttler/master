<%@page import="com.ract.util.*"%>
<%@page import="com.ract.insurance.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.insurance.ui.*"%>

<html>
<head>
<title>
Excess Detail
</title>
</head>

<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<%
String id = request.getParameter("id");
String requestType = request.getParameter("action");
String seqNoStr = null;
String xsCode = null;
String companyName = "INS_RAC";
XSDetail xsd = null;
String[] t = id.split(InsuranceUIC.DELIM);
seqNoStr = t[1];
DateTime today = new DateTime();
Integer xsSeqNo = new Integer(seqNoStr);
String showHasText = "";
String showHasAmount = "";
String showMultiLine = "";
String showRateable = "";
String showFixed = "";
String docText = "";
String editable = "yes";
String showMultiple = "";
if(requestType.equals("addDetail"))
{
  xsd = new XSDetail();
  xsd.setSeqNo(xsSeqNo);
  xsCode = "";
}
else if(requestType.equals("editDetail"))
{
   ServiceLocator serviceLocator = ServiceLocator.getInstance();
   InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
   XSType xst = insAdapter.getXSType(xsSeqNo,companyName);
   if(!xst.getCreateDate().formatShortDate().equals(today.formatShortDate()))
   {
//      editable = "no";
   }

   xsd = insAdapter.getXSDetail(xsSeqNo,t[2],companyName);
   xsCode = xsd.getXsCode();
   if(xsd.getHasText()!=null)
   {
     if(xsd.getHasText().booleanValue())showHasText="checked";
   }
   if(xsd.getFixed()!=null)
   {
      if(xsd.getFixed().booleanValue())showFixed = "checked";
   }
   if(xsd.getRateable()!=null)
   {
     if(xsd.getRateable().booleanValue())showRateable = "checked";
   }
   if(xsd.getDocText()!=null)docText = xsd.getDocText().trim();
   if(docText.indexOf("<")>-1)docText = docText.replaceAll("<","&lt;");
   if(docText.indexOf(">")>-1)docText = docText.replaceAll(">","&gt;");
   if(xsd.getAllowMultiples()!=null)
   {
     if(xsd.getAllowMultiples().booleanValue())showMultiple="checked";
   }
   if(xsd.getHasAmount()!=null)
   {
     if(xsd.getHasAmount().booleanValue())showHasAmount = "checked";
   }
   if(xsd.getMultiLine()!=null)
   {
     if(xsd.getMultiLine().booleanValue())showMultiLine = "checked";
   }
}

%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
var DELIM="<%=InsuranceUIC.DELIM%>";
function trim(sString)
{
   while (sString.substring(0,1) == ' ')
   {
      sString = sString.substring(1, sString.length);
   }
   while (sString.substring(sString.length-1, sString.length) == ' ')
   {
     sString = sString.substring(0,sString.length-1);
   }
   return sString;
}

function submitForm()
{
  //first do basic validation of the input.
  var xsCode = document.getElementById("xsCode").value;
  if(xsCode==null || xsCode=="")
  {
    alert("An Excess Code is required");
    return;
  }
  var xsName = document.getElementById("xsName").value;
  if(xsName==null || xsName=="")
  {
    alert("Enter a name for this excess");
    return;
  }
  var xAmount = document.getElementById("xsAmount").value;
  var xFixed  = document.getElementById("fixed").checked;
  if(xFixed && (xAmount==null || xAmount==""))
  {
    alert("If excess amount is fixed, you must enter the amount");
    return;
  }
  var xRateable = document.getElementById("rateable").checked;
  if(xRateable && xFixed)
  {
     alert("Excess Details cannot be both fixed and rateable");
     return;
  }
  var docText = document.getElementById("docText").innerHTML;
  docText = trim(docText);
  if(docText==null || docText=="")
  {
    var noText = confirm("You have not entered text to be printed on documents");
    if(!noText)  return;
  }
  var reqText = document.getElementById("hasText").checked;
  var hasTag = docText.indexOf("&lt;name&gt;") != -1;
  if(reqText && !hasTag)
  {
    alert("You have specified that a text entry will be required."
       + "\nYou must therefore include a <name> tag in the text");
    return;
  }
  var reqAmount = document.getElementById("hasAmount").checked;
  hasTag = docText.indexOf("&lt;amount&gt;") != -1;
  if(reqAmount && !hasTag)
  {
     alert("You have specified that an amount entry will be required."
          + "\nYou must therefore include an <amount> tag in the text");
     return;
  }
  var incTypes = document.getElementById("incidentTypes").value;
  if(incTypes==null || incTypes=="")
  {
    alert("List incident types to which this excess applies, or enter all");
    return;
  }
  document.forms[0].submit();
  //assemble new nodeId
  var newNodeId = "excess"
                + DELIM + document.getElementById("xsSeqNo").value
                + DELIM + xsCode;
  sendToParent("event=<%=requestType%>"
                + "&newNodeId=" + newNodeId
                + "&xsName=" + xsName);

  window.close();
}
function sendToParent(retMessage)
{
  window.opener.document.getElementById("pListener").value = retMessage;
  // raise button click event of parent window
  window.opener.document.getElementById("pListener").click();
}
function init()
{
   if("<%=editable%>"=="no")
   {
     alert(" Only editable on the day created ");
     window.close();
   }
   var xsCode = document.getElementById("xsCode");
   xsCode.focus();
}
dojo.addOnLoad(init);
</script>

<body bgcolor="#ffffff">
<h1>
Excess Detail
</h1>
<form name="xstype" method="post" action="<%="/InsuranceUIC"%>">

<table>
  <tr><td>Sequence No</td><td><%=xsSeqNo%></td></tr>
  <tr><td>Excess Code</td><td><input type="text" name="xsCode" value="<%=xsCode%>"/></td></tr>
    <tr><td>Excess Name</td><td><input type="text"
                                       name="xsName"
                                       title="Identifying name for users"
                                       value="<%=xsd.getXsName()%>"/></td></tr>
    <tr><td>Fixed?</td><td><input type="checkbox"
                                  name="fixed"
                                  title="If there are no options"
                                  <%=showFixed%> /></td></tr>
    <tr><td>Amount <br/>
            (for fixed excesses)</td><td><input type="text"
                                                name="xsAmount"
                                                title="Fixed amount or default amount"
                                                value="<%=xsd.getAmount()%>"/>
                                           &nbsp; Unit
                                           <input type="text"
                                                  name="xsUnit"
                                                  title="Amount unit - normally $;  'default' (multi line only); '=xsCode' for references"
                                                  value="<%=xsd.getXsUnit()%>" />
                                       </td></tr>
    <tr><td>Requires a <br/>text entry?</td>
                  <td><input type="checkbox"
                             name="hasText"
                             title="Check this box if the user is required to enter a name or other text"
                             <%=showHasText%> /></td></tr>
    <tr><td>Requires a <br/>numerical entry</td>
                  <td><input type="checkbox"
                             name="hasAmount"
                             title="Check this box if the user is required to enter a numerical value"
                             <%=showHasAmount%> /></td></tr>
    <tr><td>Document Text</td><td><textarea cols="40" rows="5"
                                            name="docText"
                                            title="Text to be displayed on documents. &#10;For multi line excesses this text is used for the excess heading.  &#10;Tags:  &#10;&nbsp;&lt;name&gt; displays the fill in text; &#10;&nbsp;&lt;amount&gt; displays fill in amount; &#10;&nbsp;&lt;xsAmount&gt; displays excess amount">
                                   <%=docText%>
                                   </textarea></td></tr>

    <tr><td>Is a rating factor</td><td><input type="checkbox"
                                              name="rateable"
                                              <%=showRateable%>
                                              title="Check here if the excess value is a rating factor"/></td></tr>
    <tr><td>Incident Types</td><td><input type="text"
                                          name="incidentTypes"
                                          value="<%=xsd.getIncidentTypes()%>"
                                          title="List incident type(comma separated) to which this excess applies, or enter 'ALL'"/></td></tr>
    <tr><td>Allow Multiples</td><td><input type="checkBox"
                                           name="allowMultiple"
                                           <%=showMultiple%>
                                           title="Check if multiple excess of this type are allowed" /></td>
    </tr>
    <tr><td>Multi line</td><td><input type="checkBox"
                                           name="multiLine"
                                           <%=showMultiLine%>
                                           title="Check if excess has multiple amounts.  If checked, this excess will need excess lines" /></td>
    </tr>
    <tr><td>Exclude</td><td><input type="text"
                                   name="exclude"
                                   value="<%=xsd.getExclude()%>"
                                   title="List incident type(comma separated) of mutually exclusive excesses"/></td></tr>


</table>
<input type="button" value="Save" onclick="submitForm()">
<input type="hidden" name="event" value="setExcessDetail"/>
<input type="hidden" name="xsSeqNo" value="<%=xsSeqNo%>"/>
</form>
</body>
</html>
