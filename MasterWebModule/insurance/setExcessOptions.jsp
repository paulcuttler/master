<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.insurance.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.insurance.ui.InsuranceUIC"%>

<html>
<head>
<title>Excess Options</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>

<%
String id = request.getParameter("id");
String requestType = request.getParameter("action");
//LogUtil.log("setExcessOptions.jsp","\n------------------------"
//                                 + "\n" + id
//                                 + "\n" + requestType);
String seqNoStr = null;
XSOption xso = null;
String[] t = id.split(InsuranceUIC.DELIM);
if(t[0].equals("options"))seqNoStr = t[2];
else seqNoStr = t[1];
Integer xsSeqNo = new Integer(seqNoStr);

ArrayList optionList = null;
   try
   {
     ServiceLocator serviceLocator = ServiceLocator.getInstance();
     InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
     String companyName = SourceSystem.INSURANCE_RACT.getAbbreviation();
     optionList = insAdapter.getXSOptions(xsSeqNo,companyName);
   }
   catch(Exception ex)
   {
     ex.printStackTrace();
   }

%>

<SCRIPT LANGUAGE="JavaScript">
//var thisRow = <%=optionList.size()%>;
var startCount = <%=optionList==null?0:optionList.size()%>;
var thisRow = startCount;
var DELIM = "<%=InsuranceUIC.DELIM%>";

function addRowToTable(){
    thisRow++;
    var tbody = document.getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    var td1 = document.createElement("TD");
	var xs = document.createElement("input");
	xs.type = "text";
	xs.name = "excess" + DELIM + thisRow;
        xs.title = "Excess Amount"
                 +"\nSetting this field to ? and the 'Unit' field to empty "
                 + "\nwill display this excess without an amount on the documents";
        xs.size = "10";
	td1.appendChild(xs);
    var td11 = document.createElement("TD");
	var unit = document.createElement("input");
	unit.type = "text";
	unit.name = "unit" + DELIM + thisRow;
        unit.title = "Amount unit.  Normally $"
                   + "\nLink this option to another excess by entering '=' "
                   + "\nand the excess code of the other excess";
        unit.size = "15";
        unit.value = "$";
	td11.appendChild(unit);
    var td2 = document.createElement("TD");
	var rType = document.createElement("input");
	rType.type = "text";
	rType.name = "rType" + DELIM + thisRow;
        rType.title = "Rating type: %, $ or p";
        rType.size = "3";
    td2.appendChild (rType);
    var td3 = document.createElement("TD");
	var rVal = document.createElement("input");
	rVal.type="text";
	rVal.name="rVal" + DELIM + thisRow;
        rVal.title="Rating amount";
        rVal.size = "10";
	td3.appendChild(rVal);

    var td4 = document.createElement("TD");
    var rVal = document.createElement("input");
	rVal.type="text";
	rVal.name="rReplace" + DELIM + thisRow;
        rVal.title="Comma separated list of replaced xs options";
        rVal.size = "20";
	td4.appendChild(rVal);

    row.appendChild(td1);
    row.appendChild(td11);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    tbody.appendChild(row);
}

function submitForm()
{
   document.forms[0].submit();
   var nodeId = "options" + DELIM + document.getElementById("xsSeqNo").value;
   var newNodeList="";
   var bRow;
   var tOptions = document.getElementById("optTable");

   for(var xx =startCount+1;xx<tOptions.rows.length;xx++)
   {
      bRow = tOptions.rows[xx];
      if(newNodeList!="")
      {
        newNodeList = newNodeList + DELIM;
      }
      newNodeList = newNodeList + bRow.cells[1].firstChild.value +bRow.cells[0].firstChild.value;
   }
   sendToParent("event=addOptions"
              + "&objectId=" + nodeId
              + "&startCount=" + document.getElementById("startCount").value
              + "&newNodeList=" + newNodeList);
   window.close();
}
function sendToParent(retMessage)
{
  window.opener.document.getElementById("pListener").value = retMessage;
  // raise button click event of parent window
  window.opener.document.getElementById("pListener").click();
}

</script>

<body bgcolor="#FFFFFF" text="#000000">
<form name="form1" method="post" action="/InsuranceUIC">
<h1>Excess Amounts</h1>
<table id="optTable" width="75%" border="0" cellspacing="2" cellpadding="2">

  <thead>
  <tr>
      <td>Excess</td>
      <td>Excess Unit </td>
      <td>Rate Type</td>
      <td>Rate Value</td>
      <td title="Comma separated list of replaced xs options">Replaces Values</td>
  </tr>
  </thead>
  <tbody name="TBODY">
    <% for(int xx=0;xx<optionList.size();xx++)
       {
         xso = (XSOption)optionList.get(xx);
         %><tr>
             <td><%=xso.getXsAmount()%></td>
             <td><%=xso.getUnit()%></td>
             <td><%=xso.getRatingType()%></td>
             <td><%=xso.getRateValue()%></td>
             <td><%=xso.getReplacesValues()%></td>
         </tr>
         <%
       }
       %>
  </tbody>
</table>
  <p>
    <input type="button" name="addRow" value="Add Row" onclick="addRowToTable();">
    <input type="button" name="save" value="Save" onclick="submitForm()"/>
    <input type="hidden" name="event" value="setExcessOptions"/>
    <input type="hidden" name="xsSeqNo" value="<%=xsSeqNo%>"/>
    <input type="hidden" name="startCount" value="<%=optionList==null?0:optionList.size()%>"/>

  </p>
</form>
</body>
</html>
