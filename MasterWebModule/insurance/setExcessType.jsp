<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.insurance.*"%>
<%@page import="com.ract.insurance.ui.InsuranceUIC"%>


<html>
<head>
<title>Excess Type
</title>
</head>
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<%
String requestType = request.getParameter("action");
String userId = request.getParameter("userId");
String id = request.getParameter("id");
String[] tokens = id.split(InsuranceUIC.DELIM);
String riskType = null;
String prodType = null;
DateTime effDate = null;
XSType xst = null;
String editable = "yes";
DateTime today = new DateTime();
String requestLevel = tokens[0];
System.out.println(id);
/*
possible values for first token are:
       empty --> root
riskType_BOAT
effDate_BOAT_effDate
xsSeqNo_seqNo
options_xsSeqNo
ignore others
*/
//System.out.println("\nsetExcessType.jsp "
//                + "\nuserId      = " + userId
//                + "\nid          = " + id
//                + "\nrequestType = " + requestType);

if(tokens[0].equals("prodType"))
{
   prodType = tokens[1];
}
else if(tokens[0].equals("riskType"))
{
   prodType = tokens[2];
   riskType = tokens[1];
}   
else if(tokens[0].equals("effDate"))
{
  prodType = tokens[2];
  riskType = tokens[1];
  effDate = new DateTime(tokens[3]);
}
if(tokens[0].equals("xsSeqNo"))
{
    ServiceLocator serviceLocator = ServiceLocator.getInstance();
    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
    String companyName = SourceSystem.INSURANCE_RACT.getAbbreviation();
    xst = insAdapter.getXSType(new Integer(tokens[1]),companyName);
    if(requestType.equalsIgnoreCase("editType"))
    {
       if(!xst.getCreateDate().formatShortDate().equals(today.formatShortDate()))
       {
 //         editable = "no";
       }
    }
    else if(requestType.equalsIgnoreCase("newVersion"))
    {
      xst = xst.cloneXsType();
 //     xst.setXsSeqNo(null);   ---> old sequence number is required
      xst.setEffDate(null);
    }
}
else
{
  xst = new XSType();
  xst.setProdType(prodType);
  xst.setRiskType(riskType);
  xst.setEffDate(effDate);
}
/*System.out.println("\nprodType = " + prodType
                 + "\nriskType = " + riskType
                 + "\neffDate  = " + effDate); */
%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
var DELIM="<%=InsuranceUIC.DELIM%>";

function formatNumber(df,formatStr)
{
   var value = df.value;
   var valueLen = value.length;
   var fChar = formatStr.charAt(valueLen);
   var kc=event.keyCode;
   if(kc>=96)kc=kc-48;
   var keyVal = String.fromCharCode(kc);
   if(kc==8)df.value=df.value.substring(0,valueLen-1);
   else if(kc>=48 && kc<=57)
   {
      if(fChar!=9)
	  {
	    df.value = df.value + fChar;
	  }
   }
   else if(kc!=9 && kc!= 13)event.returnValue=false;
   if(valueLen>=formatStr.length
      && kc>=48 && kc <=57)
   {
      df.value = df.value.substring(0,valueLen-1);
   }
}

function validateDate(thing)
{
   var dString = thing.value;
   var ddArray = dString.split("/");
   var day = Number(ddArray[0]);
   var month = Number(ddArray[1]);
   var year = Number(ddArray[2]);
   var e;
   try
   {
     if(month > 12)
	 {
	   e="Invalid Month";
	   throw e;
     }
	 if(day>31
	   ||((month==3 || month==6 || month==9 || month==11)&& day == 31)
	   ||(month==2 && year%4!=0 && day>28)
	   ||(month==2 && year%1000==0 && day > 28))
	 {
	   e = "Invalid day";
	   throw e;
	 }
	 if(year<1000)
	 {
	   e="Invalid year\nEnter four digits";
	   throw e;
	 }
       var dDate = new Date(Number(ddArray[2]),Number(ddArray[1])-1,Number(ddArray[0]));
   }
   catch(e)
   {
      alert(e+"");
	  thing.focus();
	  thing.selected=true;
   }
}
function submitForm()
{
   var riskType = document.getElementById("riskType").value;
   var requestType = document.getElementById("requestType").value;
   if(riskType==null || riskType=="")
   {
     alert("Risk type is required");
     return;
   }
   var effDate = document.getElementById("effDate").value;
   if(effDate==null || effDate=="")
   {
     alert("Effective date must be entered");
     return;
   }
   var xsType = document.getElementById("xsType").value;
   if(xsType==null || xsType=="")
   {
     alert("Excess Type must be entered");
     return;
   }
   document.forms[0].submit();
   var requestLevel = document.getElementById("requestLevel").value;
   var nodeId = "";
   var nodeName = "";
   if(requestLevel=="riskType")
   {
      nodeId="effDate" + DELIM + riskType + DELIM + effDate;
      nodeName = effDate;
   }
   else if(requestLevel=="effDate")
   {
     //need xsSeqNo, but it is not allocated yet
     nodeName = xsType;
   }
//  this used for edit
//   else if(requestLevel=="xsSeqNo")
//   {
//   }
   else  //attached to root node
   {
     nodeId = "riskType" + DELIM + riskType;
     nodeName = riskType;
   }
   sendToParent("event=" + requestType
              + "&nodeId=" + nodeId
              + "&nodeName=" + nodeName);
   window.close();
}

function sendToParent(retMessage)
{
  window.opener.document.getElementById("pListener").value = retMessage;
  // raise button click event of parent window
  window.opener.document.getElementById("pListener").click();
}
function init()
{
   if("<%=editable%>"=="no")
   {
     alert(" Only editable on the day created ");
     window.close();
   }
}
dojo.addOnLoad(init);
</script>

<body bgcolor="#ffffff">
<h1>&nbsp; Excess Type </h1>
  <form name="xsTypeForm" method="post" action="<%="/InsuranceUIC"%>" >

<table border="0" cellspacing="2" cellpadding="5">
  <tr>
      <td>Sequence Number</td>
      <td>
        <%=xst.getXsSeqNo()%>
        <input type="hidden" name="xsSeqNo" value="<%=xst.getXsSeqNo()%>" />
      </td>
  </tr>
  <tr>
      <td>Product Type</td>
      <td>
        <%
		  if(xst.getProdType()!=null)
		  {
		    %><%=xst.getProdType()%>
                    <input type="hidden" name="prodType" value="<%=xst.getProdType()%>"/>
                    <%
		   }
		   else
		   {
		      %><input type="text" name="prodType"><%
			}%>
 </td>
  <tr>
      <td>Risk Type</td>
      <td>
	    <%
		  if(xst.getRiskType()!=null)
		  {
		    %><%=xst.getRiskType()%>
                    <input type="hidden" name="riskType" value="<%=xst.getRiskType()%>"/>
                    <%
		   }
		   else
		   {
		      %><input type="text" name="riskType"><%
			}%>
      </td>
  </tr>
  <tr>
      <td>Effective Date</td>
      <td>
  	    <%if(xst.getEffDate()!=null
                && (requestType.equals("newType")
                    || requestType.equals("editType")
                ))
		{
                  %><%=xst.getEffDate()%>
                  <input type="hidden" name="effDate" value="<%=xst.getEffDate()%>" />
                  <%
		}
 		else
		{
		   %>
                     <input type="text"
                            name="effDate"
                            onkeydown="formatNumber(this,'99/99/9999')"
                            onblur="validateDate(this)"
                            value="<%=xst.getEffDate()!=null?xst.getEffDate().formatShortDate():""%>">
        <%
		} %>
      </td>
  </tr>
  <tr>
      <td>Excess Type</td>
      <td>
            <%if(xst.getXsType()!=null)
                {
                  %><%=xst.getXsType()%>
                  <input type="hidden" name="xsType" value="<%=xst.getXsType()%>"
                          title="Excess type name"/>
                  <%
                }
                else
                {
                   %><input type="text" name="xsType"><%
                } %>
      </td>
  </tr>
    <tr>
      <td>Display Order</td>
      <td>
        <input type="text"
               name="displayOrder"
               size="5"
               title="Order of display on documents - enter an integer"
               value="<%=xst.getDisplayOrder()==null?"":xst.getDisplayOrder()+""%>">
      </td>
  </tr>

  <tr>
      <td>Append Text</td>
      <td>
        <input type="text"
               name="appendText"
               size="10"
               title="Row heading (first column). Normally PLUS or blank"
               value="<%=xst.getAppendText()==null?"":xst.getAppendText()%>">
      </td>
  </tr>

  <tr>
      <td>Heading (for documents)</td>
      <td>
        <input type="text"
               name="xsHeading"
               size="30"
               title="Heading to be used with documents (if required)"
               value="<%=xst.getXsHeading()==null?"":xst.getXsHeading()%>">
      </td>
  </tr>
  <tr>
      <td>Print if Nil excess</td>
      <td>
        <input type="checkbox" name="printIfNil"
               title="If checked, excess text will be printed even if excess amount is nil"
            <%=xst.getPrintIfNil()!=null?(xst.getPrintIfNil().booleanValue()?"checked":""):""%>>
      </td>
  </tr>
  <tr>
      <td>Restrict Access</td>
      <td>
         <input type="checkbox" name="restrict"
                title="If checked, this excess will be restricted to Underwriting/Claims"
            <%=xst.getRestricted()!=null?(xst.getRestricted().booleanValue()?"checked":""):""%>>
      </td>
  </tr>
    <tr>
      <td>Required</td>
      <td>
         <input type="checkbox" name="required"
                title="If checked, this excess will be required on any matching risk"
            <%=xst.getRestricted()!=null?(xst.getRequired().booleanValue()?"checked":""):""%>>
      </td>
  </tr>
    <tr>
      <td>Reducing</td>
      <td>
         <input type="checkbox" name="reducible"
                title="If checked, this excess will reduce at renewal"
            <%=xst.getReducible()!=null?(xst.getReducible().booleanValue()?"checked":""):""%>>
      </td>
  </tr>
    <tr>
      <td>Exclude</td>
      <td>
         <input type="text" name="exclude"
                size="30"
                title="List of other excess type with which this excess is incompatible"
                value="<%=xst.getExclude()%>">
      </td>
  </tr>

  <tr>
      <td>Create Date</td>
      <td><%=xst.getCreateDate()%></td>
  </tr>
  <tr>
      <td>Create Id</td>
      <td><%=xst.getCreateId()%></td>
  </tr>
</table>
<input type="button" value="Save" onclick="submitForm()">
<input type="hidden" name="event" value="saveXsType"/>
<input type="hidden" name="requestType" value="<%=requestType%>"/>
<input type="hidden" name="userId" value="<%=userId%>"/>
<input type="hidden" name="requestLevel" value="<%=requestLevel%>"/>
</form>
</body>
</html>
