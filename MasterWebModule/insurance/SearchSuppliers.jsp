<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.ract.insurance.*"%>
<%@ page import="com.ract.common.hibernate.*"%>
<%@ page import="com.ract.insurance.ui.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">

function defaultSelect(selectList)
{
  selectList.options[0].selected = true
}

function clearForm()
{
  defaultSelect(document.all.payeeClass)
  defaultSelect(document.all.category)
  defaultSelect(document.all.region)
  defaultSelect(document.all.sortBy)
  defaultSelect(document.all.pageSize)
  document.all.payeeName.value = '';
  document.all.ABN.value = '';
}

var xmlhttp=null

function showClasses(obj)
{
//alert('showClasses');

  //clear categories
  document.getElementById('category').length = 0;

  var str = obj.value
  //alert('['+str+']')  
  
  document.all.payeeClass.options.length = 0;
  //add default
  document.all.payeeClass.options[0] = new Option("","",true)

/*
  if (str == null ||
    str.replace( /\s+$/g,'') == '')
  {
    return;
  }
*/
  var region = escape(str);
//  alert(region);

  var payeeClassURL="/InsuranceUIC?event=getClasses&region="+region;
//  alert(payeeClassURL)
  loadClasses(payeeClassURL)  

  showCategories(document.forms[0].payeeClass)
  
}

function loadClasses(url)
{
//alert('loadClasses');
	// code for Mozilla, etc.
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest()
	  }
	// code for IE
	else if (window.ActiveXObject)
	  {
	//  	alert('activex')
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
	  }
	if (xmlhttp!=null)
	{
	  //alert('check xmlhttp')
	  xmlhttp.onreadystatechange=state_Change_payeeClass
	  //alert('url '+url)  
	  xmlhttp.open("GET",url,false)
	  xmlhttp.send(null)
	} 
	else
	{
	  alert("Your browser does not support XMLHTTP.")
	}
}

function state_Change_payeeClass()
{
//alert('state_Change_payeeClass');
//alert('test'+xmlhttp.readyState+' '+xmlhttp.status)
// if xmlhttp shows "loaded"
if (xmlhttp.readyState==4)
{
  // if "OK"
  if (xmlhttp.status==200)
  {
//    alert('response='+xmlhttp.responseText)
    //remove all category elements from select list
    var payeeClassSelect = document.all.payeeClass;

    var selectedPayeeClass = document.all.selectedPayeeClass.value;
    //alert('selectedPayeeClass='+selectedPayeeClass);
    //rebuild category select list
    var doc = xmlhttp.responseXML; // assign the XML file to a var
    //alert('doc='+doc); 
    var payeeClassList = doc.getElementsByTagName('payeeClass');

    for (var i = 0; i < payeeClassList.length; i++)
    {
      if (payeeClassList[i].hasChildNodes())
      {
        var payeeClass = payeeClassList[i].firstChild.nodeValue;
        //alert('payeeClass='+payeeClass)
        //add them all
        if (payeeClass.replace( /\s+$/g,'') != '')
        {
          selected = false;
//          alert('['+payeeClass+']['+selectedPayeeClass+']')
          if (payeeClass == selectedPayeeClass)
          {
            selected = true;
          }
//          alert('opt selected '+selected)
          payeeClassSelect.options[i+1] = new Option(payeeClass, payeeClass, false, selected)
        }
      }

    }
//    alert('response='+xmlhttp.responseText)
  }
  else
  {
    alert("Problem retrieving XML data")
  }
}

}

function showCategories(obj)
{
//alert('showCategories');
  var str = obj.value
  //alert('['+str+']')

  document.all.category.options.length = 0;
  //add default
  document.all.category.options[0] = new Option("","",true)

  if (str == null ||
    str.replace( /\s+$/g,'') == '')
  {
    return;
  }

  var payeeClass = escape(str);
  //alert('payeeClass='+payeeClass);
  document.all.selectedPayeeClass.value = str;

  var catUrl="/InsuranceUIC?event=getCategories&payeeClass="+payeeClass;
//  alert('catURL='+catUrl)
  loadCategories(catUrl)
}

function loadCategories(url)
{
//alert('loadCategories');
	// code for Mozilla, etc.
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest()
	  }
	// code for IE
	else if (window.ActiveXObject)
	  {
	//  	alert('activex')
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
	  }
	if (xmlhttp!=null)
	{
	//	  alert('check xmlhttp')
	  xmlhttp.onreadystatechange=state_Change_category
	//  alert('after '+xmlhttp)
	  xmlhttp.open("GET",url,false)
	//  alert('after get')
	  xmlhttp.send(null)
	}
	else
	{
	  alert("Your browser does not support XMLHTTP.")
	}
}

function state_Change_category()
{
//alert('state_Change_category');
//alert('test'+xmlhttp.readyState+' '+xmlhttp.status)
// if xmlhttp shows "loaded"
if (xmlhttp.readyState==4)
{
//	alert('test2')
  // if "OK"
  if (xmlhttp.status==200)
  {
//    alert('response='+xmlhttp.responseText)
    //remove all category elements from select list
    var categorySelect = document.all.category;

    var selectedCategory = document.all.selectedCategory.value;
//    alert('selectedCategory='+selectedCategory)

    //rebuild category select list
    var doc = xmlhttp.responseXML; // assign the XML file to a var
    var categoryList = doc.getElementsByTagName('category');
    
    for (var i = 0; i < categoryList.length; i++)
    {
      if (categoryList[i].hasChildNodes())
      {
        var category = categoryList[i].firstChild.nodeValue;
//        alert('category='+category)
        //add them all
        if (category.replace( /\s+$/g,'') != '')
        {
          selected = false;
//          alert('['+category+']['+selectedCategory+']')
          if (category == selectedCategory)
          {
            selected = true;
          }
//          alert('opt selected '+selected)
          categorySelect.options[i+1] = new Option(category, category, false, selected)
        }
      }

    }
//    alert('response='+xmlhttp.responseText)
  }
  else
  {
    alert("Problem retrieving XML data")
  }
}

}

function setCategory(obj)
{
  document.all.selectedCategory.value = obj.value;
//  alert(document.all.selectedCategory.value)  
}
</script>

</head>

<body onload="showCategories(document.forms[0].payeeClass)">
<%
//log("1");
String region = (String)request.getAttribute("region");
String payeeClass = (String)request.getAttribute("payeeClass");
String category = (String)request.getAttribute("category");
String payeeType = (String)request.getAttribute("type");
String sortBy = (String)request.getAttribute("sortBy");
String payeeName = (String)request.getAttribute("payeeName");
String ABN = (String)request.getAttribute("ABN");
Integer pageSize = (Integer)request.getAttribute("pageSize");

//log("2");
HibernatePage payeeListPage = (HibernatePage)request.getAttribute("payeeListPage");
//log("3");

//log("region="+region);
//log("payeeClass="+payeeClass);
//log("category="+category);
//log("payeeType="+payeeType);
//log("sortBy="+sortBy);
//log("payeeName="+payeeName);
//log("ABN="+ABN);
//log("pageSize="+pageSize);
if (payeeListPage == null)
{
%>
<!-- h2>Claims Contact Search</h2 -->

<form method="post" action="<%=InsuranceConstants.UIC_INSURANCE%>" target="results">
  <input type="hidden" name="event" value="searchSuppliers"/>
  <input type="hidden" name="selectedCategory" value="<%=category%>"/>
  <input type="hidden" name="selectedPayeeClass" value="<%=payeeClass%>"/>  
  <input type="hidden" name="startPage" value="1">
  <table>
    <tr>
      <td>Region</td>
      <td>
        <%=InsuranceUIHelper.getSupplierSelectFromArrayList(InsuranceMgrBean.CRITERIA_REGION, region,null)%>
      </td>
    </tr>
    <tr>
      <td>Class</td>
      <td>
        <%=InsuranceUIHelper.getSupplierSelectFromArrayList(InsuranceMgrBean.CRITERIA_CLASS, payeeClass, region)%>
      </td>
    </tr>
    <tr>
      <td>Category</td>
      <td>
        <%=InsuranceUIHelper.getSupplierSelectFromArrayList(InsuranceMgrBean.CRITERIA_CATEGORY, category, null)%>
      </td>
    </tr>
    <%--
    <tr>
      <td>Type</td>
      <td>
        <%=InsuranceUIHelper.getHTMLSelectFromArrayList(InsuranceMgrBean.CRITERIA_TYPE, payeeType)%>
      </td>
    </tr>
     --%>
    <tr>
      <td>Name</td>
      <td>
        <input type="text" name="payeeName" value="<%=payeeName != null?payeeName:""%>">
        <input type="submit" name="Submit" value="Search">
  		
      </td>
    </tr>
    <%--
    <tr>
      <td>ABN</td>
      <td>
        <input type="text" name="ABN" value="<%=ABN != null?ABN:""%>">
      </td>
    </tr>
    <tr>
      <td>Sort By</td>
      <td>
        <%=InsuranceUIHelper.getSupplierSelectFromArrayList(InsuranceMgrBean.CRITERIA_SORT,sortBy,null)%>
      </td>
    </tr>
    <tr>
      <td>Results per page:</td> 
      <td>
        <%=InsuranceUIHelper.getSupplierSelectFromArrayList(CommonMgrBean.CRITERIA_RESULTS,(pageSize != null? pageSize.toString():null),null)%>
      </td>
    </tr>
    
     --%>
    <tr>
      <td colspan="2">
  		
  		<input type="hidden" name="pageSize" value="20" />
  		<input type="hidden" name="sortBy" value="comment" />
    	<%-- <input type="button" name="Clear" value="Clear" onclick="clearForm()"> --%>
      </td>
    </tr>
  </table>
 </form>

<%
}
//log("4");
Integer startPage = (Integer)request.getAttribute("startPage");
Integer totalElementSize = (Integer)request.getAttribute("totalElementSize");

if (payeeListPage != null)
{
  ArrayList payeeList = new ArrayList(payeeListPage.getElements());
  log("payeeList size="+payeeList.size());
  if (payeeList.size() > 0)
  {
    Iterator payeeIt = payeeList.iterator();
    Payee payee = null;
%>

<h3>Results:</h3>
  <table width="100%">
    <tr>
      <td colspan="7" class="helpText">Displaying suppliers <%=startPage%> to <%=payeeListPage.getEndPage()%> of <%=totalElementSize%> matching suppliers. </td>
    </tr>
    <tr>
      <td colspan="7"><%=InsuranceUIHelper.getPayeeNavigationBar(startPage.intValue(),pageSize.intValue(),totalElementSize.intValue(),payeeListPage.getCurrentPage(),payeeListPage.getTotalPages(),payeeName,ABN,region,payeeClass,category,payeeType,sortBy)%></td>
    </tr>
    <tr class="headerRow">
      <td>#</td>
      <td class="listHeadingPadded">Code</td>
      <td class="listHeadingPadded">Supplier</td>
      <td class="listHeadingPadded">Party Code</td>
      <td class="listHeadingPadded">Class</td>
      <td class="listHeadingPadded">Region</td>
      <td class="listHeadingPadded">Category</td>
      <td class="listHeadingPadded">Comments</td>
    </tr>
  <%
  int rowNumber = startPage.intValue();
  while (payeeIt.hasNext())
  {
//log("5");
    payee = (Payee)payeeIt.next();
    %>
    <tr valign="top" class="<%=HTMLUtil.getRowType(rowNumber)%>">
      <td><%=rowNumber%></td>
      <td><%=payee.getPayeeCode()%></td>
      <td><!-- Supplier -->
        <%=payee.getPayeeName()%></br>
        <%=(!InsuranceUIHelper.isPayeeValueEmpty(payee.getAddress()) ? (payee.getAddress()+"</br>") : "")%>
        <%=(!InsuranceUIHelper.isPayeeValueEmpty(payee.getContactName()) ? "<b>Contact:</b> "+payee.getContactName()+"</br>":"")%>
        <%=(!InsuranceUIHelper.isPayeeValueEmpty(payee.getPhone()) ? "<b>Primary:</b> "+payee.getPhone()+"</br>":"")%>
        <%=(!InsuranceUIHelper.isPayeeValueEmpty(payee.getMobile()) ? "<b>Secondary:</b> "+payee.getMobile()+"</br>":"")%>
        <%=(!InsuranceUIHelper.isPayeeValueEmpty(payee.getFax()) ? "<b>Fax:</b> "+payee.getFax()+"</br>":"")%>
        <%=(!InsuranceUIHelper.isPayeeValueEmpty(payee.getEmail()) ? "<b>E-mail:</b> "+payee.getEmail()+"</br>":"")%>
      </td>
      <td><%=payee.getPartyCode()%></td>
      <td><%=payee.getPayeeClass()%></td>
      <td><%=payee.getRegion()%></td>
      <td><%=payee.getCategory()%></td>
      <!-- Rating? -->
      <td><%=payee.getComment()%></td>
      <!-- Discount? -->
    </tr>
    <%
	rowNumber++;
  }
  %>
  </table>
  <%
  }
  else
  {
  %>
  <p><span class="helpText">No results found for search.</span>
  </p>
  <%
  }
}
%>
<p>&nbsp;</p>
</body>

</html>
