<%@page import="com.ract.common.*"%>

<html>
<head>
<title>Archive Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
	    dojo.require("dojo.io.*");
	    dojo.require("dojo.event.*");
            dojo.require("dojo.widget.*");
            dojo.require("dojo.widget.FilteringTable");
var serverURL="/ArchiveUIC";
var logonId="dgk";
var cTable;
var selectedRow = 0;
var numRows = 0;
var inProgress = false;

function handleClientNoEntry()
{
   if(window.event.keyCode==13)
   {
      var clientNo = dojo.byId('clientNo').value;
	  if(clientNo != "" )
	  {
	    getData(clientNo,"","","","","","");
	  }
   }
}
function loadData()
{
	  var surname = dojo.byId('surname').value;
	  var initial = dojo.byId('initial').value;
	  var street = dojo.byId('street').value;
	  var suburb = dojo.byId('suburb').value;
	  var streetChar = dojo.byId('streetChar').value;
	  var dateOfBirth = dojo.byId('dateOfBirth').value;
	  getData("",surname,initial,streetChar,street,suburb,dateOfBirth);
}
function getData(clientNo,surname,initial,streetChar,street,suburb,dateOfBirth)
{
    clearTable();
    showProgressBar();
    dojo.io.bind({
                  url: serverURL,
                  content:{event:"archiveSearch",
			   clientNo:clientNo,
		           surname:surname,
                           initial:initial,
			   streetChar:streetChar,
                           street:street,
                           suburb:suburb,
			   dateOfBirth:dateOfBirth},
                   load:loadClients,
                   error:handleError,
                   //timeout: handleTimeout,
                   mimetype: "application/json"
                 });
}

function clearTable()
{
		   cTable=dojo.widget.byId('cTable');
		   cTable.store.setData("");
}

function loadClients(type,data,evt) {
    var e;
	if(!inProgress) return;
	if(data.length==0)alert("No matching records");
    else if(data[0].Id=="Too many") alert("Too many.  Enter more criteria");
	else
	{
	    numRows = data.length;
		selectedRow = -1;
		try
		{
		  var cTable=dojo.widget.byId('cTable');
		  cTable.store.setData(data);
		}
		catch (e)
		{
		   var errstr=e.name+": "+e.message;
		   alert ("Error:  " + errStr);
		}
	}
	hideProgressBar();
}

function handleKeyPress()
{
 if(window.event.keyCode==13)
 {
    loadData();
 }
}

function selectRow()
{
  var cTable = dojo.widget.byId('cTable');
  var sRow = cTable.getSelectedData();
  if (sRow!=null)  alert(sRow.clientNo);

}

function handleError(type,data,evt)
{
  alert ("Error detected.....");
}

function showProgressBar()
{
   if(!inProgress)
   {
//      document.getElementById('pgBarId').style.display='';
      var pgb = dojo.widget.byId('pgBar');
      pgb.startAnimation();
      inProgress=true;
   }
}
function hideProgressBar()
{
	 var pgb = dojo.widget.byId('pgBar');
     pgb.stopAnimation();
 	 inProgress=false;
//	 document.getElmentById('pgBarId').style.display="none";
}

function formatNumber(df,formatStr)
{
   var value = df.value;
   var valueLen = value.length;
   var fChar = formatStr.charAt(valueLen);
   var kc=event.keyCode;
   if(kc>=96)kc=kc-48;
   var keyVal = String.fromCharCode(kc);
   if(kc==8)df.value=df.value.substring(0,valueLen-1);
   else if(kc>=48 && kc<=57)
   {
      if(fChar!=9)
	  {
	    df.value = df.value + fChar;
	  }
   }
   else if(kc!=9 && kc!= 13)event.returnValue=false;
   if(valueLen>=formatStr.length
      && kc>=48 && kc <=57)
   {
      df.value = df.value.substring(0,valueLen-1);
   }
}

function validateDate(thing)
{
   var dString = thing.value;
   var ddArray = dString.split("/");
   var day = Number(ddArray[0]);
   var month = Number(ddArray[1]);
   var year = Number(ddArray[2]);
   var e;
   try
   {
     if(month > 12)
	 {
	   e="Invalid Month";
	   throw e;
     }
	 if(day>31
	   ||((month==3 || month==6 || month==9 || month==11)&& day == 31)
	   ||(month==2 && year%4!=0 && day>28)
	   ||(month==2 && year%1000==0 && day > 28))
	 {
	   e = "Invalid day";
	   throw e;
	 }
	 if(year<1000)
	 {
	   e="Invalid year\nEnter four digits";
	   throw e;
	 }
       var dDate = new Date(Number(ddArray[2]),Number(ddArray[1])-1,Number(ddArray[0]));
   }
   catch(e)
   {
      alert(e+"");
	  thing.focus();
	  thing.selected=true;
   }
}

function initialise()
{
   document.form1.initial.focus();
}

</script>
<style type="text/css">
    h1 {font-family:Arial;
	    font-size:14pt;
		font-weight:bold;}
    h2 {font-family:Arial;
	    font-size:10pt;
		font-weight:bold;}
    body {font-family:Arial;
	    font-size:0.9em;
		font-weight:normal;}

	table {
           font-family:Lucida Grande, Verdana;
		       font-size:0.7em;
                       width:100%;
		       border:1px solid #ccc;
		       border-collapse:collapse;
		       cursor:default;
	}
	table thead td, table thead th {
		        padding:2px;
			font: 10ptArial
			font-weight:bold;
			font-style:italic;
			background-repeat:no-repeat;
			background-position:top right;
			background:#c0c0ff
			color:green;
	}
	table tbody tr td{alternateRows:true;
			          border-bottom:1px solid #ddd;}
    table tbody tr.alt td{border-bottom:1px solid #ddd;
                             background:#f3fdfa;}

	table tbody tr.selected td{
			background: yellow;
	}
	defText{font-family:Arial;
	        font-size=10pt;}

</style>

<body bgcolor="#FFFFFF" text="#000000" onload="initialise()">
<form name="form1" method="post" action="">
  <h1>Archive Search </h1>
  <table>
    <tr>
      <td align="right" width="16%">Client Number</td>
      <td width="84%">
        <input type="text" name="clientNo" onkeypress="handleClientNoEntry()" tabindex="1">
      </td>
     </tr>
    <tr>
      <td align="right" width="16%">Initial/Surname</td>
      <td width="84%">
        <input type="text" name="initial" size="4" onkeypress="handleKeyPress()" tabindex="2">
        <input type="text" name="surname" onkeypress="handleKeyPress()" tabindex="3">
        Date of Birth
        <input type="text" name="dateOfBirth" size="10"
		       onKeyPress="handleKeyPress()"
			   tabindex="8"
			   onKeyDown="formatNumber(this,'99/99/9999')"
			   onBlur="validateDate(this)">

    </tr>
    <tr>
      <td  align="right" width="16%"> No/Street<br>
        <br>
        Suburb </td>
      <td width="84%">
        <input type="text" name="streetChar" size="6" tabindex="5" onkeypress="handleKeyPress()">
        <input type="text" name="street" onkeypress="handleKeyPress()" tabindex="6">
        <br>
        <input type="text" name="suburb" onkeypress="handleKeyPress()" onchange="checkQASAddress()" tabindex="7">
      </td>
    </tr>
  </table>
  <div id="tablePlace">
    <table  dojoType="filteringTable" id="cTable" cellpadding="2" alternateRows="true" multiple="false">
      <thead >
      <tr>
         <th field="clientNo" 		dataType="Number" 	align="left" valign="top">Clt No	</th>
	  <th field="archiveDate" 	dataType="date" 	align="left" valign="top">Archived	</th>
	  <th field="gname" 		dataType="String" 	align="left" valign="top">Given Names</th>
	  <th field="surname" 		dataType="String" 	align="left" valign="top">Surname	</th>
	  <th field="bDate"         dataType="String"   align="left" valign="top">Birth Date </th>
	  <th field="streetChar" 	dataType="String" 	align="left" valign="top">No		</th>
	  <th field="street" 		dataType="String" 	align="left" valign="top">Street	</th>
	  <th field="suburb" 		dataType="String" 	align="left" valign="top" >Suburb	</th>
	  <th field="business" 		dataType="String" 	align="left" valign="top">Business	</th>
	</tr>
     </thead>
   <tbody>
   </tbody>
   </table>
</div>
<div id="pgBarId" align="center">
   <div  width="700" height="5" id="pgBar"  dojoType="ProgressBar"></div>
</div>
</form>
</body>
</html>
