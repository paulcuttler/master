var errDetailDisplayed = false;

function toggleDetails()
{
   if (!errDetailDisplayed)
   {
      document.all.errorDetail.style.display = '';
      document.all.btnDetails.value = "Hide details";
      errDetailDisplayed = true;
   }
   else
   {
      document.all.errorDetail.style.display = "none";
      document.all.btnDetails.value = "Show details";
      errDetailDisplayed = false;
    }
}

function browserBack ()
{    
    history.back();
}