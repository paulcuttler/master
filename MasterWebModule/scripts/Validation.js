/*******************************************************
Javascript script file for validation in HTML forms
Used to validate values.  Run when a field is complete, usually in the
onblur event.
Functions:

isAlphanumeric (STRING s [, BOOLEAN emptyOK])
isAlphabetic (STRING s [, BOOLEAN emptyOK])
isSignedFloat (STRING s [, BOOLEAN emptyOK])
isCurrency(s)
isFloat (STRING s [, BOOLEAN emptyOK])
isNonpositiveInteger (STRING s [, BOOLEAN emptyOK])
isNegativeInteger (STRING s [, BOOLEAN emptyOK])
isNonnegativeInteger (STRING s [, BOOLEAN emptyOK])
isPositiveInteger (STRING s [, BOOLEAN emptyOK])
isSignedInteger (STRING s [, BOOLEAN emptyOK])
isInteger (STRING s [, BOOLEAN emptyOK])
isLetterOrDigit (c)
isDigit (c)
isLetter (c)
stripInitialWhitespace (s)
stripWhitespace (s)
stripCharsNotInBag (s, bag)
stripCharsInBag (s, bag)
isWhitespace (s)
isEmpty(s)
doDateCheck(from, to) - checks that from date is before to date
LeapYear(intYear)
chkdate(objName)
checkdate(objName)  - checks for a valid date. if  not found returns to the field
validateEmail(objName)
validateRange(objName,min,max)
setFocus(objName)
validateLength(objName,maxLength)
calculateYears(date1,date2)
checkLength (obj,length)
isCardMatch(cardType CardNumber)
isAnyCard(cc)
isJCB(cc)
isEnRoute(cc)
isCarteBlanche(cc)
isDinersClub(cc)
isAmericanExpress(cc)
isMasterCard(cc)
isVisa(cc)
isCreditCard(st)
***********************************************************/
var whitespace = " "
var defaultEmptyOK = false 

function checkLength (obj,length)
{
   if (obj.value.length > length)
   {
      alert(length + ' chars max');
      obj.value = obj.value.substring(0,length - 1);
   }

}
// non-digit characters which are allowed in credit card numbers
var creditCardDelimiters = " "
var iCreditCardPrefix = "This is not a valid "
var iCreditCardSuffix = " credit card number. (Click the link on this form to see a list of sample numbers.) Please reenter it now."


function calculateYears(date1,date2)
{
  var diff = date1 - date2;
  var y = ((((diff/365)/24)/60)/60)/1000;
  return parseInt(y);
}

function validateLength(objName,maxLength)
{
  value = objName.value;
  if (value.length > maxLength)
  {
    alert("This field should contain "+maxLength+" characters.")
    setFocus(objName);
    return false;
  }
  else
  {
   return true;
  }
}

function setFocus(objName)
{
//  objName.select();
  objName.focus();
}

function validateRange(objName,min,max)
{
  value = objName.value;
  if (value >= min && value <= max)
  {
    return true;
  }
  else
  {
    alert("Value must be between "+min+" and "+max+"!");
    setFocus(objName);
    return false;
  }
}

function validateEmail(obj)
{
//	alert(obj.value);
  if (obj != null &&
      obj.value.length > 0)
   {
      if (obj.value.indexOf("@") == -1 || obj.value.indexOf("@") != obj.value.lastIndexOf("@"))
      {
    	  if (obj.value != 'Not Applicable' &&
	          obj.value != 'Refused' &&
	          obj.value != 'To Follow')
    	  {
		    alert("Email address does not contain an '@' and does not have a value of 'Not Applicable', 'Refused' or 'To Follow'!")
		    setFocus(obj);
		    return false; 
    	  }
      }
      else
      {
    	var x = obj.value.indexOf("@"); 
    	if (obj.value.indexOf(".",x+2) == -1 || //no . after the @
            obj.value.substring(obj.value.length-1,obj.value.length) == '.')
        {
    	    alert("Email address either does not contain a '.' or a '.' is present at the end!")
    	    setFocus(obj);
    	    return false;     		
        }
      }
   }

  return true;

}

function checkdate(objName)
{
  var datefield = objName;
  if (chkdate(objName) == false)
  {
    alert("Entered date is invalid.");
    setFocus(objName);
    return false;
  }
  else
  {
    return true;
  }
}

function chkdate(objName)
{
  //var strDatestyle = "US"; //United States date style
  var strDatestyle = "EU";  //European date style
  var strDate;
  var strDateArray;
  var strDay;
  var strMonth;
  var strYear;
  var intday;
  var intMonth;
  var intYear;
  var booFound = false;
  var datefield = objName;
//  var strSeparatorArray = new Array("-"," ","/",".");
  var strSeparatorArray = new Array("/");
  var intElementNr;
  var err = 0;

  var strMonthArray = new Array(12);
  strMonthArray[0] = "Jan";
  strMonthArray[1] = "Feb";
  strMonthArray[2] = "Mar";
  strMonthArray[3] = "Apr";
  strMonthArray[4] = "May";
  strMonthArray[5] = "Jun";
  strMonthArray[6] = "Jul";
  strMonthArray[7] = "Aug";
  strMonthArray[8] = "Sep";
  strMonthArray[9] = "Oct";
  strMonthArray[10] = "Nov";
  strMonthArray[11] = "Dec";

  strDate = datefield.value;
  if (strDate.length < 1)
  {
    return true;
  }
  for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++)
  {
    if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1)
    {
      strDateArray = strDate.split(strSeparatorArray[intElementNr]);

      if (strDateArray.length != 3)
      {
        err = 1;
        return false;
      }
      else
      {
        strDay = strDateArray[0];
        strMonth = strDateArray[1];
        strYear = strDateArray[2];
      }
    booFound = true;
    }
  }
  if (booFound == false)
  {
	//no 
    err = 100;
    return false;
  }
  if (strYear.length == 1)
  {
  	strYear =  '0'+strYear;
  }
  if (strYear.length == 2)
  {
    strYear = '20' + strYear;
  }

  // US style
  if (strDatestyle == "US")
  {
    strTemp = strDay;
    strDay = strMonth;
    strMonth = strTemp;
  }

  intday = parseInt(strDay, 10);
  if (isNaN(intday))
  {
    err = 2;
    return false;
  }
  intMonth = parseInt(strMonth, 10);
  if (isNaN(intMonth))
  {
    for (i = 0;i<12;i++)
    {
      if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase())
      {
        intMonth = i+1;
        strMonth = strMonthArray[i];
        i = 12;
      }
    }
    if (isNaN(intMonth))
    {
      err = 3;
      return false;
    }
  }
  intYear = parseInt(strYear, 10);
  if (isNaN(intYear))
  {
    err = 4;
    return false;
  }
  if (intMonth>12 || intMonth<1)
  {
    err = 5;
    return false;
  }
  if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1))
  {
    err = 6;
    return false;
  }
  if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1))
  {
    err = 7;
    return false;
  }
  if (intMonth == 2)
  {
    if (intday < 1)
    {
      err = 8;
      return false;
    }
    if (LeapYear(intYear) == true)
    {
      if (intday > 29)
      {
         err = 9;
         return false;
      }
    }
    else
    {
      if (intday > 28)
      {
        err = 10;
        return false;
      }
    }
  }
  // Pad out single digit days
  if (intday > 9)
  {
      strDay = new Number(intday).toString();
  }
  else
  {
      strDay = '0' + new Number(intday).toString();
  }
  // Pad out single digit months
  if (intMonth > 9)
  {
      strMonth = new Number(intMonth).toString();
  }
  else
  {
      strMonth = '0' + new Number(intMonth).toString();
  }
  
  // Format the date as all numbers
  // TB 9/4/2002
  if (strDatestyle == "US")
  {
    datefield.value = strMonth + " " + strDay+" " + strYear;
  }
  else
  {
    datefield.value = strDay + "/" + strMonth + "/" + strYear;
  }
  
  // Format the date with the month as a three letter acronym.
//  if (strDatestyle == "US") {
//    datefield.value = strMonthArray[intMonth-1] + " " + intday+" " + strYear;
//  }
//  else {
//    datefield.value = intday + "/" + strMonthArray[intMonth-1] + "/" + strYear;
//  }
  return true;
}

function LeapYear(intYear)
{
   if (intYear % 100 == 0)
   {
      if (intYear % 400 == 0) { return true; }
   }
   else
   {
      if ((intYear % 4) == 0) { return true; }
   }
   return false;
}

function doDateCheck(from, to)
{
   if (Date.parse(from.value) <= Date.parse(to.value))
   {
     // alert("The dates are valid.");
     // do nothing
   }
   else
   {
      if (from.value == "" || to.value == "")
      {
        alert("Both dates must be entered.");
      }
      else
      {
        alert("To date must occur after the from date.");
      }
   }
}

// Check whether string s is empty.

function isEmpty(s)
{
   return ((s == null) || (s.length == 0))
}

function checkDateKeyStroke(object){
  digits = new Array("0","1","2","3","4","5","6","7","8","9");
  inString = object.value;
  lastChar = inString.charAt(inString.length-1);
  len = inString.length;
  invalid=false;
  found = false;
  if(window.event.keyCode==8){
     return;
  }
  dateBit="days";
  dayString="";
  monthString="";
  yearString="";
  days=0;
  months=0;
  years=-1;
  outString="";
  for(x=0;x<inString.length;x++){
	if(inString.charAt(x)!="/"){
	   if(dateBit=="days") dayString+=inString.charAt(x);
	   else if(dateBit=="months") monthString += inString.charAt(x);
	   else if(dateBit=="years") yearString += inString.charAt(x);
	}
	else {
	   if(dateBit=="days") dateBit="months";
	   else if(dateBit=="months") dateBit="years";
	}
  }
  if(dayString!="")days = parseInt(dayString);
  if(monthString!="")months = parseInt(monthString);
  if(yearString!="")years = parseInt(yearString);
  if(months>0){
     if((months == 9 || months==4 || months==6 || months ==11)&& days>30)invalid=true;
 	 else if(months==2 && days>29 )invalid=true;
	 else if(months==2 && days>28 && yearString!=""){
	    if(years%4 !=0 && years >1000 )invalid=true;
     }
     else if(days>31) invalid=true;
  }
  if(years>2050)invalid=true;
  if(months>12)invalid=true;
  if(days>31) invalid=true;
  if(!invalid){
    outString = days.toString();
    if((days>3 && days<32)
	   || months>0
	   || (lastChar=="/" && months==0))outString+="/";
    if(months>0 && months<13)outString+=months.toString();
    if((months>9 && months<13)
	   || years>0)outString +="/";
    if(years>-1)outString+=years.toString();
	if(monthString!=""
	   && yearString==""
	   && lastChar=="/")outString+="/";

  }
  if(invalid) object.value=inString.substring(0,inString.length-1);
  else object.value = outString;
}

// Returns true if string s is empty or
// whitespace characters only.

function isWhitespace (s)
{
   var i;

    // Is s empty?
    if (isEmpty(s)) return true;

    // Search through string's characters one by one
    // until we find a non-whitespace character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}



// Removes all characters which appear in string bag from string s.

function stripCharsInBag (s, bag)
{
   var i;
    var returnString = "";

    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }

    return returnString;
}



// Removes all characters which do NOT appear in string bag
// from string s.

function stripCharsNotInBag (s, bag)
{
   var i;
   var returnString = "";

   // Search through string's characters one by one.
   // If character is in bag, append to returnString.

   for (i = 0; i < s.length; i++)
   {
      // Check that current character isn't whitespace.
      var c = s.charAt(i);
       if (bag.indexOf(c) != -1) returnString += c;
   }

   return returnString;
}



// Removes all whitespace characters from s.
// Global variable whitespace (see above)
// defines which characters are considered whitespace.

function stripWhitespace (s)
{
   return stripCharsInBag (s, whitespace)
}




// WORKAROUND FUNCTION FOR NAVIGATOR 2.0.2 COMPATIBILITY.
//
// The below function *should* be unnecessary.  In general,
// avoid using it.  Use the standard method indexOf instead.
//
// However, because of an apparent bug in indexOf on
// Navigator 2.0.2, the below loop does not work as the
// body of stripInitialWhitespace:
//
// while ((i < s.length) && (whitespace.indexOf(s.charAt(i)) != -1))
//   i++;
//
// ... so we provide this workaround function charInString
// instead.
//
// charInString (CHARACTER c, STRING s)
//
// Returns true if single character c (actually a string)
// is contained within string s.

function charInString (c, s)
{
   for (i = 0; i < s.length; i++)
   {
      if (s.charAt(i) == c) return true;
   }
   return false;
}



// Removes initial (leading) whitespace characters from s.
// Global variable whitespace (see above)
// defines which characters are considered whitespace.

function stripInitialWhitespace (s)
{
   var i = 0;

   while ((i < s.length) && charInString (s.charAt(i), whitespace))
       i++;

   return s.substring (i, s.length);
}


// Returns true if character c is an English letter
// (A .. Z, a..z).
//
// NOTE: Need i18n version to support European characters.
// This could be tricky due to different character
// sets and orderings for various languages and platforms.

function isLetter (c)
{
   return ( ((c >= "a") && (c <= "z")) || ((c >= "A") && (c <= "Z")) )
}



// Returns true if character c is a digit
// (0 .. 9).

function isDigit (c)
{
   return ((c >= "0") && (c <= "9"))
}



// Returns true if character c is a letter or digit.

function isLetterOrDigit (c)
{
   return (isLetter(c) || isDigit(c))
}



// isInteger (STRING s [, BOOLEAN emptyOK])
//
// Returns true if all characters in string s are numbers.
//
// Accepts non-signed integers only. Does not accept floating
// point, exponential notation, etc.
//
// We don't use parseInt because that would accept a string
// with trailing non-numeric characters.
//
// By default, returns defaultEmptyOK if s is empty.
// There is an optional second argument called emptyOK.
// emptyOK is used to override for a single function call
//      the default behavior which is specified globally by
//      defaultEmptyOK.
// If emptyOK is false (or any value other than true),
//      the function will return false if s is empty.
// If emptyOK is true, the function will return true if s is empty.
//
// EXAMPLE FUNCTION CALL:     RESULT:
// isInteger ("5")            true
// isInteger ("")             defaultEmptyOK
// isInteger ("-5")           false
// isInteger ("", true)       true
// isInteger ("", false)      false
// isInteger ("5", false)     true

function isInteger (s)
{
   var i;

    if (isEmpty(s))
       if (isInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isInteger.arguments[1] == true);

    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);

        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}

// isSignedInteger (STRING s [, BOOLEAN emptyOK])
//
// Returns true if all characters are numbers;
// first character is allowed to be + or - as well.
//
// Does not accept floating point, exponential notation, etc.
//
// We don't use parseInt because that would accept a string
// with trailing non-numeric characters.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.
//
// EXAMPLE FUNCTION CALL:          RESULT:
// isSignedInteger ("5")           true
// isSignedInteger ("")            defaultEmptyOK
// isSignedInteger ("-5")          true
// isSignedInteger ("+5")          true
// isSignedInteger ("", false)     false
// isSignedInteger ("", true)      true

function isSignedInteger (s)
{
   if (isEmpty(s))
       if (isSignedInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isSignedInteger.arguments[1] == true);

    else {
        var startPos = 0;
        var secondArg = defaultEmptyOK;

        if (isSignedInteger.arguments.length > 1)
            secondArg = isSignedInteger.arguments[1];

        // skip leading + or -
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;
        return (isInteger(s.substring(startPos, s.length), secondArg))
    }
}




// isPositiveInteger (STRING s [, BOOLEAN emptyOK])
//
// Returns true if string s is an integer > 0.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isPositiveInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isPositiveInteger.arguments.length > 1)
        secondArg = isPositiveInteger.arguments[1];

    // The next line is a bit byzantine.  What it means is:
    // a) s must be a signed integer, AND
    // b) one of the following must be true:
    //    i)  s is empty and we are supposed to return true for
    //        empty strings
    //    ii) this is a positive, not negative, number

    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s) > 0) ) );
}






// isNonnegativeInteger (STRING s [, BOOLEAN emptyOK])
//
// Returns true if string s is an integer >= 0.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isNonnegativeInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isNonnegativeInteger.arguments.length > 1)
        secondArg = isNonnegativeInteger.arguments[1];

    // The next line is a bit byzantine.  What it means is:
    // a) s must be a signed integer, AND
    // b) one of the following must be true:
    //    i)  s is empty and we are supposed to return true for
    //        empty strings
    //    ii) this is a number >= 0

    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s) >= 0) ) );
}






// isNegativeInteger (STRING s [, BOOLEAN emptyOK])
//
// Returns true if string s is an integer < 0.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isNegativeInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isNegativeInteger.arguments.length > 1)
        secondArg = isNegativeInteger.arguments[1];

    // The next line is a bit byzantine.  What it means is:
    // a) s must be a signed integer, AND
    // b) one of the following must be true:
    //    i)  s is empty and we are supposed to return true for
    //        empty strings
    //    ii) this is a negative, not positive, number

    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s) < 0) ) );
}






// isNonpositiveInteger (STRING s [, BOOLEAN emptyOK])
//
// Returns true if string s is an integer <= 0.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isNonpositiveInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isNonpositiveInteger.arguments.length > 1)
        secondArg = isNonpositiveInteger.arguments[1];

    // The next line is a bit byzantine.  What it means is:
    // a) s must be a signed integer, AND
    // b) one of the following must be true:
    //    i)  s is empty and we are supposed to return true for
    //        empty strings
    //    ii) this is a number <= 0

    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s) <= 0) ) );
}





// isFloat (STRING s [, BOOLEAN emptyOK])
//
// True if string s is an unsigned floating point (real) number.
//
// Also returns true for unsigned integers. If you wish
// to distinguish between integers and floating point numbers,
// first call isInteger, then call isFloat.
//
// Does not accept exponential notation.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isFloat (s)
{
    var i;
    var seenDecimalPoint = false;
    var decimalPointDelimiter = '.';

    if (isEmpty(s))
       if (isFloat.arguments.length == 1)
          return defaultEmptyOK;
       else
          return (isFloat.arguments[1] == true);

    if (s == decimalPointDelimiter)
       return false;

    var validateDecimalPlaces = false;
    var decimalPlaces;
    var decimalCount = 0;
    if (isFloat.arguments.length == 3)
    {
       validateDecimalPlaces = true;
       decimalPlaces = parseInt(isFloat.arguments[2]);
    }

    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);

        if ((c == decimalPointDelimiter) && !seenDecimalPoint)
           seenDecimalPoint = true;
        else if (!isDigit(c))
           return false;
        else if (validateDecimalPlaces && seenDecimalPoint)
           if (decimalCount >= decimalPlaces)
              return false;
           else
              decimalCount++;
    }

    // All characters are numbers.
    return true;
}


// Validate the string as a currency value.
// must be a valid float value with 2 decimal places
function isCurrency(s)
{
   return isFloat(s,true,2);
}


// isSignedFloat (STRING s [, BOOLEAN emptyOK])
//
// True if string s is a signed or unsigned floating point
// (real) number. First character is allowed to be + or -.
//
// Also returns true for unsigned integers. If you wish
// to distinguish between integers and floating point numbers,
// first call isSignedInteger, then call isSignedFloat.
//
// Does not accept exponential notation.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isSignedFloat (s)

{   if (isEmpty(s))
       if (isSignedFloat.arguments.length == 1) return defaultEmptyOK;
       else return (isSignedFloat.arguments[1] == true);

    else {
        var startPos = 0;
        var secondArg = defaultEmptyOK;

        if (isSignedFloat.arguments.length > 1)
            secondArg = isSignedFloat.arguments[1];

        // skip leading + or -
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;
        return (isFloat(s.substring(startPos, s.length), secondArg))
    }
}




// isAlphabetic (STRING s [, BOOLEAN emptyOK])
//
// Returns true if string s is English letters
// (A .. Z, a..z) only.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.
//
// NOTE: Need i18n version to support European characters.
// This could be tricky due to different character
// sets and orderings for various languages and platforms.

function isAlphabetic (s)

{   var i;

    if (isEmpty(s))
       if (isAlphabetic.arguments.length == 1) return defaultEmptyOK;
       else return (isAlphabetic.arguments[1] == true);

    // Search through string's characters one by one
    // until we find a non-alphabetic character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character is letter.
        var c = s.charAt(i);

        if (!isLetter(c))
        return false;
    }

    // All characters are letters.
    return true;
}




// isAlphanumeric (STRING s [, BOOLEAN emptyOK])
//
// Returns true if string s is English letters
// (A .. Z, a..z) and numbers only.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.
//
// NOTE: Need i18n version to support European characters.
// This could be tricky due to different character
// sets and orderings for various languages and platforms.

function isAlphanumeric (s)

{   var i;

    if (isEmpty(s))
       if (isAlphanumeric.arguments.length == 1) return defaultEmptyOK;
       else return (isAlphanumeric.arguments[1] == true);

    // Search through string's characters one by one
    // until we find a non-alphanumeric character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number or letter.
        var c = s.charAt(i);

        if (! (isLetter(c) || isDigit(c) ) )
        return false;
    }

    // All characters are numbers or letters.
    return true;
}


/*  ================================================================
    Credit card verification functions
    Originally included as Starter Application 1.0.0 in LivePayment.
    20 Feb 1997 modified by egk:
           changed naming convention to initial lowercase
                  (isMasterCard instead of IsMasterCard, etc.)
           changed isCC to isCreditCard
           retained functions named with older conventions from
                  LivePayment as stub functions for backward
                  compatibility only
           added "AMERICANEXPRESS" as equivalent of "AMEX"
                  for naming consistency
    ================================================================ */


/*  ================================================================
    FUNCTION:  isCreditCard(st)

    INPUT:     st - a string representing a credit card number

    RETURNS:  true, if the credit card number passes the Luhn Mod-10
                test.
            false, otherwise
    ================================================================ */

function isCreditCard(st) {
  // Encoding only works on cards with less than 19 digits
  if (st.length > 19)
    return (false);

  sum = 0; mul = 1; l = st.length;
  for (i = 0; i < l; i++) {
    digit = st.substring(l-i-1,l-i);
    tproduct = parseInt(digit ,10)*mul;
    if (tproduct >= 10)
      sum += (tproduct % 10) + 1;
    else
      sum += tproduct;
    if (mul == 1)
      mul++;
    else
      mul--;
  }
// Uncomment the following line to help create credit card numbers
// 1. Create a dummy number with a 0 as the last digit
// 2. Examine the sum written out
// 3. Replace the last digit with the difference between the sum and
//    the next multiple of 10.

//  document.writeln("<BR>Sum      = ",sum,"<BR>");
//  alert("Sum      = " + sum);

  if ((sum % 10) == 0)
    return (true);
  else
    return (false);

} // END FUNCTION isCreditCard()



/*  ================================================================
    FUNCTION:  isVisa()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid VISA number.

            false, otherwise

    Sample number: 4111 1111 1111 1111 (16 digits)
    ================================================================ */

function isVisa(cc)
{
  if (((cc.length == 16) || (cc.length == 13)) &&
      (cc.substring(0,1) == 4))
    return isCreditCard(cc);
  return false;
}  // END FUNCTION isVisa()




/*  ================================================================
    FUNCTION:  isMasterCard()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid MasterCard
                number.

            false, otherwise

    Sample number: 5500 0000 0000 0004 (16 digits)
    ================================================================ */

function isMasterCard(cc)
{
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 16) && (firstdig == 5) &&
      ((seconddig >= 1) && (seconddig <= 5)))
    return isCreditCard(cc);
  return false;

} // END FUNCTION isMasterCard()





/*  ================================================================
    FUNCTION:  isAmericanExpress()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid American
                Express number.

            false, otherwise

    Sample number: 340000000000009 (15 digits)
    ================================================================ */

function isAmericanExpress(cc)
{
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 15) && (firstdig == 3) &&
      ((seconddig == 4) || (seconddig == 7)))
    return isCreditCard(cc);
  return false;

} // END FUNCTION isAmericanExpress()




/*  ================================================================
    FUNCTION:  isDinersClub()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid Diner's
                Club number.

            false, otherwise

    Sample number: 30000000000004 (14 digits)
    ================================================================ */

function isDinersClub(cc)
{
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 14) && (firstdig == 3) &&
      ((seconddig == 0) || (seconddig == 6) || (seconddig == 8)))
    return isCreditCard(cc);
  return false;
}



/*  ================================================================
    FUNCTION:  isCarteBlanche()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid Carte
                Blanche number.

            false, otherwise
    ================================================================ */

function isCarteBlanche(cc)
{
  return isDinersClub(cc);
}




/*  ================================================================
    FUNCTION:  isDiscover()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid Discover
                card number.

            false, otherwise

    Sample number: 6011000000000004 (16 digits)
    ================================================================ */

function isDiscover(cc)
{
  first4digs = cc.substring(0,4);
  if ((cc.length == 16) && (first4digs == "6011"))
    return isCreditCard(cc);
  return false;

} // END FUNCTION isDiscover()





/*  ================================================================
    FUNCTION:  isEnRoute()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid enRoute
                card number.

            false, otherwise

    Sample number: 201400000000009 (15 digits)
    ================================================================ */

function isEnRoute(cc)
{
  first4digs = cc.substring(0,4);
  if ((cc.length == 15) &&
      ((first4digs == "2014") ||
       (first4digs == "2149")))
    return isCreditCard(cc);
  return false;
}



/*  ================================================================
    FUNCTION:  isJCB()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is a valid JCB
                card number.

            false, otherwise
    ================================================================ */

function isJCB(cc)
{
  first4digs = cc.substring(0,4);
  if ((cc.length == 16) &&
      ((first4digs == "3088") ||
       (first4digs == "3096") ||
       (first4digs == "3112") ||
       (first4digs == "3158") ||
       (first4digs == "3337") ||
       (first4digs == "3528")))
    return isCreditCard(cc);
  return false;

} // END FUNCTION isJCB()



/*  ================================================================
    FUNCTION:  isAnyCard()

    INPUT:     cc - a string representing a credit card number

    RETURNS:  true, if the credit card number is any valid credit
                card number for any of the accepted card types.

            false, otherwise
    ================================================================ */

function isAnyCard(cc)
{
  if (!isCreditCard(cc))
    return false;
  if (!isMasterCard(cc) && !isVisa(cc) && !isAmericanExpress(cc) && !isDinersClub(cc) &&
      !isDiscover(cc) && !isEnRoute(cc) && !isJCB(cc)) {
    return false;
  }
  return true;

} // END FUNCTION isAnyCard()



/*  ================================================================
    FUNCTION:  isCardMatch()

    INPUT:    cardType - a string representing the credit card type
            cardNumber - a string representing a credit card number

    RETURNS:  true, if the credit card number is valid for the particular
            credit card type given in "cardType".

            false, otherwise
    ================================================================ */

function isCardMatch (cardType, cardNumber)
{

      cardType = cardType.toUpperCase();
      var doesMatch = true;

      if ((cardType == "VISA") && (!isVisa(cardNumber)))
            doesMatch = false;
      if ((cardType == "MASTERCARD") && (!isMasterCard(cardNumber)))
            doesMatch = false;
      if ( ( (cardType == "AMERICANEXPRESS") || (cardType == "AMEX") )
                && (!isAmericanExpress(cardNumber))) doesMatch = false;
      if ((cardType == "DISCOVER") && (!isDiscover(cardNumber)))
            doesMatch = false;
      if ((cardType == "JCB") && (!isJCB(cardNumber)))
            doesMatch = false;
      if ((cardType == "DINERS") && (!isDinersClub(cardNumber)))
            doesMatch = false;
      if ((cardType == "CARTEBLANCHE") && (!isCarteBlanche(cardNumber)))
            doesMatch = false;
      if ((cardType == "ENROUTE") && (!isEnRoute(cardNumber)))
            doesMatch = false;
      return doesMatch;

}  // END FUNCTION CardMatch()
