
// Add the number of months to the date and
// return a new date object.
function addMonthsToDate(referenceDate,months)
{
//  alert("ref date="+referenceDate);
//  alert("months="+months);

  day = referenceDate.getDate();
  mnth = referenceDate.getMonth();
  yr = referenceDate.getFullYear();

  mnth += months;

  // Roll over the months into the years
  while (mnth > 11)
  {
     yr++;
     mnth -= 12;
  }

  // Cap the days to 30 if the new month is a 30 day month.
  if ((mnth == 3 || mnth == 5 || mnth == 8 || mnth == 10) && (day > 30))
  {
     day = 30;
  }
  // Cap the days to the number of days in february if the new month is february
  else if (mnth == 1)
  {
     if (LeapYear(yr) == true && day > 29)
     {
        day = 29;
     }
     else if (day > 28)
     {
        day = 28;
     }
  }

  futureDate = new Date(yr,mnth,day);
//  alert("futureDate="+futureDate);
  return futureDate;
}

//return the value associated with the select radio button.
function returnRadioButtonValue(radioButtonList)
{
//  alert('returnRadioButtonValue 1');
  var val;
  if (typeof(radioButtonList)=="undefined")
  {
    return null;
  }
  else if (typeof(radioButtonList.length)=="undefined")
  {
//  alert('returnRadioButtonValue 2');
    val = radioButtonList.value;
  }
  else
  {
//  alert('returnRadioButtonValue 3');
    for ( var i=0; i < radioButtonList.length; i++ )
    {
      if ( radioButtonList[i].checked == true )
      {
        val = radioButtonList[i].value;
      }
    }
  }
//  alert('returnRadioButtonValue 4'+val);
  return val;
}


// Add the number of years to the date and return
// a new date object.
function addYearsToDate(referenceDate,years)
{
  day = referenceDate.getDate();
  mnth = referenceDate.getMonth();
  yr = referenceDate.getYear();

  yr += years;

  // Cap the days to the number of days in february if the month is
  // february and the year is a leap year.
  if (mnth == 1)
  {
     if (LeapYear(yr) == true && day > 29)
     {
        day = 29;
     }
     else if (day > 28)
     {
        day = 28;
     }
  }

  futureDate = new Date(yr,mnth,day);
  return futureDate;
}

function upper(obj)
{
  var val = obj.value;
  if (val != null)
  {
    //trim
    val = val.replace(/^\s+|\s+$/, '');
    //upper
    val = val.toUpperCase();
    obj.value = val;
  }
}

function cleanString(str)
{
  var newStr
  // Replace hyphens
  re = /-/g;                          // a regular expression.
  newStr = str.replace(re,"_");

  // Replace spaces
  re = / /g;                          // a regular expression
  newStr = newStr.replace(re,"_");

  // Replace ampersands
  re = /&/g;                          // a regular expression
  newStr = newStr.replace(re,"_");

  return newStr;
}


// Return the number of days in the month.
// The month is specified as 0 to 11.
// The year is required to work out the number of days in february for leap years.
function getDaysInMonth(month,year)
{
  var monthEndDay = 31;

  // Cap the month end day to 30 if the month is a 30 day month.
  if (month == 3 || month == 5 || month == 8 || month == 10)
  {
     monthEndDay = 30;
  }
  // Cap the month end day to the number of days in february if the month is february
  else if (month == 1)
  {
     if (LeapYear(year) == true)
     {
        monthEndDay = 29;
     }
     else
     {
        monthEndDay = 28;
     }
  }
  return monthEndDay;
}

function getDate(stringObj)
{
  var stringDate = stringObj.value;
  if (stringDate.length == 0)
  {
    return null;
  }
  //should be in form dd/MM/yyyy

  //note: there is a bug in javascript that results in 08 and 09 being
  //parsed incorrectly hence the ,10 below.
  var day = parseInt(stringDate.substring(0,2),10);
  var monthString = stringDate.substring(3,5);
  var month = parseInt(monthString,10) -1;
  var year = parseInt(stringDate.substring(6,10),10);
//  alert('d='+day);
//  alert('m='+month);
//  alert('y='+year);
  var convertedDate = new Date(year,month,day);
  return convertedDate;
}

// Extended Tooltip Javascript
// copyright 9th August 2002, by Stephen Chapman, Felgall Pty Ltd
// permission is granted to use this javascript provided that the below code is not altered

var DH = 0;
var an = 0;
var al = 0;
var ai = 0;

if (document.getElementById)
{
  ai = 1;
  DH = 1;
}
else
{
  if (document.all)
  {
    al = 1;
    DH = 1;
  }
  else
  {
    browserVersion = parseInt(navigator.appVersion);
    if ((navigator.appName.indexOf('Netscape') != -1) && (browserVersion == 4))
    {
      an = 1;
      DH = 1;
    }
  }
}

function fd(oi,ws)
{
  if (ws == 1)
  {
    if (ai)
    {
      return (document.getElementById(oi).style);
    }
    else
    {
      if (al)
      {
        return (document.all[oi].style);
      }
      else
      {
        if (an)
        {
          return (document.layers[oi]);
        }
      };
    }
  }
  else
  {
    if (ai)
    {
      return (document.getElementById(oi));
    }
    else
    {
      if (al)
      {
        return (document.all[oi]);
      }
      else
      {
        if (an)
        {
          return (document.layers[oi]);
        }
      };
    }
  }
}

function pw()
{
  if (window.innerWidth != null)
    return window.innerWidth;
  if (document.body.clientWidth != null)
    return document.body.clientWidth; return (null);
}

function popUp(evt,oi)
{
  if (DH)
  {
    var wp = pw();
    ds = fd(oi,1);
    dm = fd(oi,0);
    st = ds.visibility;
    if (dm.offsetWidth)
      ew = dm.offsetWidth;
    else if (dm.clip.width)
      ew = dm.clip.width;
    if (st == "visible" || st == "show")
    {
      ds.visibility = "hidden";
    }
    else
    {
      if (evt.y || evt.pageY)
      {
        if (evt.pageY)
        {
          tv = evt.pageY + 20;
          lv = evt.pageX - (ew/4);
        }
        else
        {
          tv = evt.y + 20 + document.body.scrollTop;
          lv = evt.x  - (ew/4) + document.body.scrollLeft;
        }
        if (lv < 2)
          lv = 2;
        else if (lv + ew > wp)
          lv -= ew/2;
        if (!an)
        {
          lv += 'px';
          tv += 'px'};
          ds.left = lv;
          ds.top = tv;
        }
        ds.visibility = "visible";
      }
    }
  }

//Utilities to support scheduling UI elements

//function setRunDate ()
//{
//   ddate = new Date;
//   dday = ddate.getDate();
//   dmonth = ddate.getMonth() + 1;
//   dyear = ddate.getYear();
//   document.all.runDate.value= dday + "/" + dmonth + "/" + dyear;
//   dStr = formatDate(ddate.getFullYear() , dmonth , dday );
//   document.all.fileDate.value = dStr;
//}

function formatDate ( yy,mm,dd )
{
  mStrDate = yy;
  mStrDate += padString(mm);
  mStrDate += padString(dd);
  return mStrDate;
}

function padString ( num )
{
  myStr = '';
  if ( num < 10 )
  {
  	myStr += '0';
  }

  myStr += num;
  return myStr;
}

function checkHours(obj)
{
    val = obj.value * 1;
    if(val>23 || val<0)
    {
       document.all.runHours.focus();
       alert("Hours must be from 0 to 23");
       return false;
    }
    else return true;
}

function checkMins(obj)
{
   val = obj.value * 1;
   if(val > 59 || val < 0)
   {
      document.all.runMins.focus();
      alert("Minutes must be between 0 and 59");
      return false;
   }
   else return true;
}


function validateRunTime(runHours,runMins)
{
   hours = runHours.value * 1;
   mins = runMins.value * 1;
   if(hours>23 || hours < 0 || mins<0 || mins>59)
   {
      alert("Enter a valid date and time");
      return false;
   }
   else return true;
}

function newWindow(url) {  
  window.open(url, '', 'width=800,height=600,toolbar=no,status=no,scrollbars=yes,resizable=yes');
  return false;
}