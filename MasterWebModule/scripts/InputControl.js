/*
Input controls.

These functions are intended to validate keystrokes for various kinds of
input fields.  Normal usage is as the "onkeyup" method.
Typical usage:
    <input type="text" name="dateStr" value="<%=dateStr%>"
        onkeyup="javascript:checkDate(this)" >

Available functions:

checkInteger(object)  forces digits only in the field
checkFloat(object)  Allows digits and a decimal point only
checkLength(object, min, max) forces the string to have at least 'min'
                              and no more than 'max' characters
formatCardNumber(object) inserts a space after every four characters
*/

function checkInteger(object){
  digits = new Array("0","1","2","3","4","5","6","7","8","9");
  inString = object.value;
  lastChar = inString.charAt(inString.length-1);
  found = false;
  for(x=0;x<10;x++){
     if(lastChar == digits[x]) {
	   found=true;
	 }
  }
  if (!found) {
	object.value=inString.substring(0,inString.length-1);
  }
}
function checkFloat(object){
  digits = new Array("0","1","2","3","4","5","6","7","8","9");
  inString = object.value;
  lastChar = inString.charAt(inString.length-1);
  decimalPoint = ".";
  dpAt = inString.indexOf(decimalPoint);
  if(dpAt>-1 && lastChar==decimalPoint){
     if(dpAt != inString.length-1){
	    object.value=inString.substring(0,inString.length-1);
	 }
  }
  else {
    found=false;
    for(x=0;x<10;x++){
       if(lastChar == digits[x]) {
	     found=true;
	   }
    }
    if (!found) {
	  object.value=inString.substring(0,inString.length-1);
    }
  }
}
function checkLength(object,minLength,maxLength){
  len = object.value.length;
  if(len<minLength){
  alert(object.name + " must have at least " + minLength + " digits");
  object.focus();
  }
  else if(len>maxLength){
     alert(object.name + " must have no more than " + maxLength + " digits");
	 object.focus();
  }
}
