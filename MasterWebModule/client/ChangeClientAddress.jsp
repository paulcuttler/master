<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="java.util.*"%>

<html>
<title>Change Customer Address</title>
<head>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>
<body topmargin="3" onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/OkButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/ClearButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/CloseButton_over.gif');">

<%
	//get the address vo from the request
String addressType = (String)request.getAttribute("addressType");
String pageName = (String)request.getAttribute("page");

AddressVO avo = (AddressVO)request.getAttribute("addressVO");
if (avo == null)
{
  if (addressType.equals(AddressVO.ADDRESS_TYPE_STREET))
  {
    avo = new ResidentialAddressVO();
    avo.setProperty((String)request.getAttribute("property"));
    avo.setPropertyQualifier((String)request.getAttribute("propertyQualifier"));
    String stsubid = (String)request.getAttribute("stsubid");
    if (stsubid != null && !stsubid.equals(""))
      avo.setStreetSuburbID(new Integer(stsubid));
  }
  else if (addressType.equals(AddressVO.ADDRESS_TYPE_POSTAL))
  {
    avo = new PostalAddressVO();
    avo.setProperty((String)request.getAttribute("property"));
    avo.setPropertyQualifier((String)request.getAttribute("propertyQualifier"));
    String stsubid = (String)request.getAttribute("stsubid");
    if (stsubid != null && !stsubid.equals(""))
      avo.setStreetSuburbID(new Integer(stsubid));
  }
  request.setAttribute("addressVO",avo);
}

//collection of addressVOs
Collection streetSuburbList = (Collection)request.getAttribute("streetSuburbList");


//set the address type if not explicitly passed in.
if (addressType == null) {
  if (avo != null) {
    addressType = avo.getAddressType();
  }
}
//if avo null then something serious
%>


<script language="Javascript">
  //set stsubid of the main form with the value
  function setClientForm() {

    if (document.all.stsubid.value == '')
    {
      alert('You must enter a valid address or press close to close this window.');
      return;
    }

    /*
    Construct the addressText string (unfortunately we have to construct it using
    javascript)
    */
    var addressText = '';
    if (document.all.property.value != '') {
      addressText += document.all.property.value;
      addressText += ', ';
    }
    if (document.all.propertyQualifier.value != '') {
      if (addressText.length != 0)
        addressText += ' ';
      addressText += document.all.propertyQualifier.value;
    }
    if (document.all.streetName.value != '') {
      if (addressText.length != 0)
        addressText += ' ';
      addressText += document.all.streetName.value;
      addressText += ', ';
    }
    if (document.all.suburbName.value != '') {
      if (addressText.length != 0)
        addressText += ' ';
      addressText += document.all.suburbName.value;
    }
    if (document.all.state.value != '') {
      if (addressText.length != 0)
        addressText += ' ';
      addressText += document.all.state.value;
    }
    if (document.all.postcode.value != '') {
      if (addressText.length != 0)
        addressText += ' ';
      addressText += document.all.postcode.value;
    }

<%
//Set the property,property qualifier and stsubid of the calling form.
if (addressType != null) {
  if (addressType.equals(AddressVO.ADDRESS_TYPE_POSTAL))
  {
%>
    self.opener.top.rightFrame.document.all.paProperty.value = document.all.property.value;
    self.opener.top.rightFrame.document.all.paPropertyQualifier.value = document.all.propertyQualifier.value;
    self.opener.top.rightFrame.document.all.paStsubid.value = document.all.stsubid.value;
    self.opener.top.rightFrame.document.all.postalAddressText.value = addressText;
<%
  }
  else if (addressType.equals(AddressVO.ADDRESS_TYPE_STREET))
  {
%>
    self.opener.top.rightFrame.document.all.saProperty.value = document.all.property.value;
    self.opener.top.rightFrame.document.all.saPropertyQualifier.value = document.all.propertyQualifier.value;
    self.opener.top.rightFrame.document.all.saStsubid.value = document.all.stsubid.value;
    self.opener.top.rightFrame.document.all.streetAddressText.value = addressText;

    if (self.opener.top.rightFrame.document.all.paStsubid.value == '')
    {
      self.opener.top.rightFrame.document.all.paProperty.value = document.all.property.value;
      self.opener.top.rightFrame.document.all.paPropertyQualifier.value = document.all.propertyQualifier.value;
      self.opener.top.rightFrame.document.all.paStsubid.value = document.all.stsubid.value;
      self.opener.top.rightFrame.document.all.postalAddressText.value = addressText;
    }
<%
  }
}
%>
    self.close();
  }

</script>

<%--
need to save:
=============

property name (eg. RACT Estate)
property qualifier (eg. flat 29a) --> street number (eg. "29")
stsubid
--%>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td class="contentHeading">Change <%=(avo==null?"":avo.getAddressType())%> Address<br></td>
  </tr>
  <form name="changeAddressForm" method="post" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
    <!-- The "event" field will get set when the buttons are clicked.  -->
    <input type="hidden" name="event">
    <input type="hidden" name="page" value="<%=pageName%>">
    <input type="hidden" name="addressType" value="<%=addressType%>">

    <tr>
      <td valign="top">
        <!-- Include the change address fields -->
        <%@ include file="/client/ChangeAddressInclude.jsp"%>
      </td>
    </tr>
    <tr>
      <td align="left">
        <table border="0" cellpadding="5">
          <tr align="center">
            <td>
               <a onclick="setClientForm();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Accept the address changes.');MM_swapImage('OkButton','','<%=CommonConstants.DIR_IMAGES%>/OkButton_over.gif',1);return document.MM_returnValue" >
                  <img name="OkButton" src="<%=CommonConstants.DIR_IMAGES%>/OkButton.gif" border="0" alt="Accept the address changes.">
               </a>
            </td>
            <td>
               <a onclick="resetAddressFields();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Clear the search fields.');MM_swapImage('ClearButton','','<%=CommonConstants.DIR_IMAGES%>/ClearButton_over.gif',1);return document.MM_returnValue" >
                  <img name="ClearButton" src="<%=CommonConstants.DIR_IMAGES%>/ClearButton.gif" border="0" alt="Clear the search fields.">
               </a>
            </td>
            <td>
               <a onclick="self.close();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Close the advanced search window.');MM_swapImage('CloseButton','','<%=CommonConstants.DIR_IMAGES%>/CloseButton_over.gif',1);return document.MM_returnValue" >
                  <img name="CloseButton" src="<%=CommonConstants.DIR_IMAGES%>/CloseButton.gif" border="0" alt="Close the advanced search window.">
               </a>
            </td>
<%--
            <td> <input type="button" value="OK" onclick="setClientForm();"></td>
            <td> <input type="button" value="Clear" onclick="resetAddressFields();"></td>
            <td> <input type="button" value="Close" onclick="self.close();"></td>
--%>
          </tr>
        </table>
      </td>
    </tr>
  </form>
</table>

<script language="Javascript">

  document.all.streetName.focus();
</script>

</html>
