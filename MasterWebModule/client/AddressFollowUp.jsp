<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>

<html>
<head>
<title>
Address Change follow up handler
</title>

<!--<script type="text/javascript" src="c:/class/test_main/MasterWebModule/dojo/dojo.js"></script>-->
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>

<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript"
        src="<%=CommonConstants.DIR_CLIENT%>/scripts/AddressFollowUp.js"></script>
</head>
<body bgcolor="#ffffff">
<h1>
Address Change Follow-up
</h1>
<form>
  <input type="hidden" name="logonId" value="<%=ServletUtil.getUserSession(request,true).getUserId()%>"/>
<table width="81%" border="0" bordercolor="#CCCCCC">
  <tr valign="bottom">
    <td align="right"> User </td>

    <td align="left">
      <input type="text" id="forUserId" value="dgk">
	  </td>

    <td align="right"> Sales Branch </td>

    <td align="left">
      <input type="text" id="salesbranch" value="All">
     </td>

    <td align="right">Status </td>

	  <td>
      <select dojotype="ComboBox"
	          autocomplete="true"
			  maxlistlength="6"
			  mode="local"
			  name="status"
			  widgetId="status">
        <option value="All">All</option>
        <option value="Flag" selected>Flag & Duplicate</option>
        <option value="Change">Changed</option>
		<option value="NoChange">No Change</option>
      </select>
	  </td>
	  <td></td>
  </tr>
  <tr>
   <td align="right"> Between </td>
   <td align="left">
           <input type="text"
		          id="fDate"
		          onkeydown="formatNumber(this,'99/99/9999')"
				  onblur="validateDate(this)"/>
   </td>
   <td align="right"> and </td>
   <td align="left">
     <input type="text"
	        id="lDate"
			onkeydown="formatNumber(this,'99/99/9999')"
			onblur="validateDate(this)">
   </td>
   <td align="right">Result </td>
   <td>
             <select dojotype="ComboBox"
	          autocomplete="true"
			  maxlistlength="6"
			  mode="local"
			  name="result"
			  widgetId="result">
        <option value="All">All</option>
        <option value="To Do" selected>To do</option>
        <option value="Changed">Changed</option>
        <option value="No change">No change</option>
      </select>
    </td>
	<td>
	<td align="right"><input type="button" name="load" value="Load" onclick="loadData()"></td>

</table>
<div dojoType="contentPane" id="contentPane" style="overflow: auto;width:100%;height:300;">

<div id="tablePlace">

    <table  dojoType="filteringTable"
            id="fTable"
	    cellpadding="2"
	    alternateRows="true"
            multiple="false"
	    class="dataTable"
            onClick="getDetails()">
      <thead >
      <tr>
	   <!-- <th field="Id" dataType="int" align="left" valign="top"></th>-->
	      <th field="status" dataType="String" align="left" valign="top"  >Status</th>
	      <th field="sType" dataType="Integer" align="left" valign="top">Type</th>
	      <th field="sNumber" dataType="String" align="left" valign="top">Number</th>
	      <th field="name" dataType="String" align="left"  valign="top" >Name</th>
	      <th field="createDate" dataType="String" align="left"  valign="top">Create
            Date</th>
		  <th field="createId" dataType="String" align="left" valign="top">Create<br>
            Id</th>
	      <th field="cClientNo" dataType= "String" align="left" valign="top"  >Changed
            Clt</th>
          <th field="process" valign="top" align="left" >Result<br>
            OK&nbsp;&nbsp;&nbsp;NC</th>
	</tr>
     </thead>
	 <hr>
   <tbody>
   </tbody>
   </table>
</div>
</div>
<table><tr>
      <td align="right">
        <input type="button" name="saveButton" value="Save" onclick="saveChanges()">
        <input type="button" name="printButton" value="Report" onclick="print()">
</td></tr></table>
<div id="resultPane"></div>
</form>
</body>
</html>
