 <%@ taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>
<%@page import="javax.rmi.PortableRemoteObject"%>
<%@page import="java.rmi.RemoteException"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">
</script>

</head>

<%
//list of client matches
Collection clientAddressList = (Collection)request.getAttribute("clientAddressList");
Collection clientNameBirthList = (Collection)request.getAttribute("clientNameBirthList");
Collection clientNameTitleList = (Collection)request.getAttribute("clientNameTitleList");
Collection clientPhoneList = (Collection)request.getAttribute("clientPhoneList");
%>

<body>

<h1>
Client Match List
</h1>

<p><span class="helpText">If the current client matches one of the clients
  below click on the link below to choose that client and lose the changed details. If you are sure that the
  client being added has not been added previously then press save.</span></p>

<h2>Client Matching</h2>

<table>
  <tr>
    <td>
    Title:
    </td>
    <td class="dataValue">
    <s:property value="client.title"/>
    </td>
  </tr>
  <tr>
    <td>
    Initials:
    </td>
    <td class="dataValue">
    <s:property value="client.initials"/>
    </td>
  </tr>
  <tr>
    <td>
    Surname:
    </td>
    <td class="dataValue">
    <s:property value="client.surname"/>
    </td>
  </tr>
  <tr>
    <td>
    Birth Date:
    </td>
    <td class="dataValue">
    <s:property value="client.birthDate"/>
    </td>
  </tr>
  <tr>
    <td>
    Home Phone:
    </td>
    <td class="dataValue">
    <s:property value="client.homePhone"/>
    </td>
  </tr>
  <tr>
    <td>
    Address:
    </td>
    <td class="dataValue">
    <s:property value="clientAddress"/>
    </td>
  </tr>

</table>

<%
if (clientPhoneList != null && !clientPhoneList.isEmpty())
{
%>
  <h2>Clients matching phone number</h2>
  <%=ClientUIHelper.buildClientSummaryTable(clientPhoneList)%>
<%
}
if (clientNameBirthList != null && !clientNameBirthList.isEmpty())
{
%>
  <h2>Clients matching surname, initials and birth date</h2>
  <%=ClientUIHelper.buildClientSummaryTable(clientNameBirthList)%>
<%
}
if (clientNameTitleList != null && !clientNameTitleList.isEmpty())
{
%>
  <h2>Clients matching surname, initials and title</h2>
  <%=ClientUIHelper.buildClientSummaryTable(clientNameTitleList)%>
<%
}
if (clientAddressList != null && !clientAddressList.isEmpty())
{
%>
  <h2>Clients matching surname, street number and address</h2>
  <%=ClientUIHelper.buildClientSummaryTable(clientAddressList)%>
<%
}
%>
<s:form action="/saveClient.action" theme="simple">
  <s:token/>
  <s:hidden name="eventName"/>
  <s:hidden name="ignoreMatches" value="true"/>
  <s:hidden name="clientNumber"/> 
  <s:hidden name="createTramadaClient"/>
  <s:submit value="Proceed with changes"/>
</s:form>

</body>
</html>

