<%@ taglib prefix="s" uri="/struts-tags" %>

<%@page import="com.ract.common.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.ClientUIHelper"%>

<%--
Address include.

Check if the address type is null.
If it is not then display the address line.
--%>

<%
ClientHistory prevClientHistory = (ClientHistory)request.getAttribute("prevClientHistory");

AddressVO previousResidentialAddress = null;
AddressVO previousPostalAddress = null;

if (prevClientHistory != null &&
    prevClientHistory.getClient() != null)
{
  previousResidentialAddress = prevClientHistory.getClient().getResidentialAddress();
  previousPostalAddress = prevClientHistory.getClient().getPostalAddress();  
}

ClientVO addressClient = (ClientVO)request.getAttribute("client");
%>

<%--
residential address hidden fields
--%>
<table border="1" width="100%" cellspacing="0">
  <tr valign="top">
    <td>
    <table border="0" width="100%">
      <tr>
       <td class="listHeading" nowrap colspan="3"><center>Residential Address</center></td>
      <s:set name="residentialAddress" value="client.residentialAddress"></s:set>
      <s:if test="%{#residentialAddress != null}">
      <tr>
        <td class="label">Property:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_PROPERTY,addressClient.getResidentialAddress(),previousResidentialAddress)%>" colspan="2"><s:property value="#residentialAddress.property"/></td>
      </tr>
      <tr>
        <td class="label">Street#:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.fIELD_NAME_PROPERTY_QUALIFIER,addressClient.getResidentialAddress(),previousResidentialAddress)%>" colspan="2"><s:property value="#residentialAddress.propertyQualifier"/></td>
      </tr>
      <tr>
        <td class="label">Street:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_STREET,addressClient.getResidentialAddress(),previousResidentialAddress)%>" colspan="2"><s:property value="#residentialAddress.street"/></td>
      </tr>
      <tr>
        <td class="label">Suburb:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_SUBURB,addressClient.getResidentialAddress(),previousResidentialAddress)%>" colspan="2"><s:property value="#residentialAddress.suburb"/></td>
      </tr>
      <tr>
        <td class="label">Postcode:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_POSTCODE,addressClient.getResidentialAddress(),previousResidentialAddress)%>"><s:property value="#residentialAddress.postcode"/></td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_STATE,addressClient.getResidentialAddress(),previousResidentialAddress)%>"><s:property value="#residentialAddress.state"/></td>
      </tr>
    </s:if>
    </table>
    </td>
    <td>

    <table border="0" width="100%">
      <tr>
       <td class="listHeading" nowrap colspan="3"><center>Postal Address</center></td>
      </tr>
      <s:set name="postalAddress" value="client.postalAddress"></s:set>
      <s:if test="%{#postalAddress != null}"> 
      <tr>
        <td class="label">Property:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_PROPERTY,addressClient.getPostalAddress(),previousPostalAddress)%>" colspan="2"><s:property value="#postalAddress.property"/></td>
      </tr> 
      <tr>
        <td class="label">Street#:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.fIELD_NAME_PROPERTY_QUALIFIER,addressClient.getPostalAddress(),previousPostalAddress)%>" colspan="2"><s:property value="#postalAddress.propertyQualifier"/></td>
      </tr>
      <tr>
        <td class="label">Street:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_STREET,addressClient.getPostalAddress(),previousPostalAddress)%>" colspan="2"><s:property value="#postalAddress.street"/></td>
      </tr>
      <tr>
        <td class="label">Suburb:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_SUBURB,addressClient.getPostalAddress(),previousPostalAddress)%>" colspan="2"><s:property value="#postalAddress.suburb"/></td>
      </tr>
      <tr>
        <td class="label">Postcode:</td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_POSTCODE,addressClient.getPostalAddress(),previousPostalAddress)%>"><s:property value="#postalAddress.postcode"/></td><td class="<%=ClientUIHelper.historyMatch(AddressVO.FIELD_NAME_STATE,addressClient.getPostalAddress(),previousPostalAddress)%>"><s:property value="#postalAddress.state"/></td>
      </tr>
    </s:if>

    </table>

    </td>
  </tr>
</table>
