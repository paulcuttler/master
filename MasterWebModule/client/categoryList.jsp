<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<table>
<s:select emptyOption="true" name="categoryCode" listValue="categoryName"
	listKey="categoryCode" list="categoryList" label="Category"
	onchange="showCategory(this)" required="true">
</s:select>
</table>