<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.extracts.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="java.util.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<%
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
Collection publicationList = commonMgr.getPublicationList();
%>

<h2>Export Subscriber File</h2>
  <form method="post" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
  <input type="hidden" name="event" value="exportSubscriberFile"/>

<table>

  <tr><td>Publication:</td><td>
  <select name="publication">
    <option value=""></option>
  <%
  if (publicationList != null)
  {
    Publication publication = null;
    for (Iterator it = publicationList.iterator(); it.hasNext();)
    {
      publication = (Publication)it.next();
    %>
      <option value="<%=publication.getPublicationCode()%>"><%=publication.getDescription()%></option>
  <%
    }
  }
  %>
  </select></td></tr>
  <tr>
    <td>Format:</td>
    <td>
  <select name="format">
  <option value="<%=ExtractFormatter.FORMAT_CLIENT_FUTURE_MEDIUM_EMAIL%>">Future Medium Subscriber List (CSV)</option>
  </select> 
  </td></tr>
  <tr><td>Email address:</td><td><input type="text" name="emailAddress"/></td></tr>

  <tr><td colspan="2"><input type="submit" value="Export"/></td></tr>



</table>
</form>
</body>

</html>
