<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.travel.*"%>
<%@ page import="java.util.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<%
String clientNumber = request.getParameter("clientNumber");
%>

<html>
<head>
<title>TestClient</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>

<%
ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
ClientVO client = clientMgr.getClient(new Integer(clientNumber));
%>

<h2>Test Client</h2>
<pre>
Client number: <%=client.getClientNumber()%>
Branch number: <%=client.getBranchNumber()%>
Made date: <%=client.getMadeDate()%>
Last update: <%=client.getLastUpdate()%>
Birth date: <%=client.getBirthDate()%>
Sex: <%=client.getSex()%>
Surname: <%=client.getSurname()%>
Given names: <%=client.getGivenNames()%>
Initials: <%=client.getInitials()%>
ResiProperty: <%=client.getResiProperty()%>
ResiStreetChar: <%=client.getResiStreetChar()%>
ResiStreetNumber: <%=client.getResiStreetNumber()%>
ResiStsubId: <%=client.getResiStsubId()%>
PostProperty: <%=client.getPostProperty()%>
PostStreetChar: <%=client.getPostStreetChar()%>
PostStreetNumber: <%=client.getPostStreetNumber()%>
PostStsubId: <%=client.getPostStsubId()%>
Home phone: <%=client.getHomePhone()%>
Work phone: <%=client.getWorkPhone()%>
Memo number: <%=client.getMemoNumber()%>
Group client: <%=client.getGroupClient()%>
Marital status: <%=client.getMaritalStatus()%>
Children: <%=client.getNumberOfDependants()%> 
Title: <%=client.getTitle()%>
Postal name: <%=client.getPostalName()%>
Address title: <%=client.getAddressTitle()%>
Made Id: <%=client.getMadeID()%>
Status: <%=client.getStatus()%>
Market: <%=client.getMarket()%>
Mobile phone: <%=client.getMobilePhone()%>
Email address: <%=client.getEmailAddress()%>
Drivers license: <%=client.getDriversLicenseNumber()%>
Contact person name: <%=client.getContactPersonName()%>
Contact person phone: <%=client.getContactPersonPhone()%>
Motor news delivery method: <%=client.getMotorNewsDeliveryMethod()%>
Motor news send option: <%=client.getMotorNewsSendOption()%>
Occupation Id: <%=client.getOccupationID()%>
Master client number: <%=client.getMasterClientNumber()%>
</pre>

<%
String xml = ClientFactory.getClientAdapter().getTramadaClientXML(TramadaClientProfileImporter.TRAMADA_ACTION_ADD,TramadaClientHelper.getTramadaProfileCode(client),null,userSession.getUser().getUserID(), userSession.getUser().getSalesBranchCode(), client);
//replace < and > characters to allow display via html.
xml = StringUtil.replaceAll(xml,"<","&lt;");
xml = StringUtil.replaceAll(xml,">","&gt;");
%>
<h3>Tramada XML:</h3>
<%=xml%>

</body>
</html>

