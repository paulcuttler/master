<%@page import="com.ract.common.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<html>
<head>
<title>
Marketing Options
</title>
<%=CommonConstants.getRACTStylesheet()%>

</head>
<%
   CommonMgr cMgr = CommonEJBHelper.getCommonMgr();

   //convert to system parameters
   SystemParameterVO colHead = cMgr.getSystemParameter("MARKET_OPT","COL_HEADINGS");
   SystemParameterVO rowHead = cMgr.getSystemParameter("MARKET_OPT","ROW_HEADINGS");

   String[] columnHeadings = colHead.getValue().split(",");
   String[] rowHeadings = rowHead.getValue().split(",");
   String colHeadList = colHead.getValue();
   String rowHeadList = rowHead.getValue();

   String clientNoString = (String)request.getParameter("clientNo");
   if(clientNoString==null)clientNoString=(String)request.getAttribute("clientNo");

   Integer clientNo = new Integer(clientNoString);
   int numRows = rowHeadings.length;
   int numCols = columnHeadings.length;
   int xx;
   int yy;

   //breaks system boundaries - send to the UIC
   ClientMgr cltMgr = ClientEJBHelper.getClientMgr();
   String clientName = cltMgr.getClient(clientNo).getPostalName();
   ArrayList entries = (ArrayList)cltMgr.getMarketOptions(clientNo);

/*   ArrayList entries = null;*/
   MarketOption mOpt = null;
   String recFound;
   String cellRef;
   String linkColour="#CCFFFF";
   %>
   <!--<script type="text/javascript" src="c:/class/test_main/MasterWebModule/dojo/dojo.js"></script>-->

<script type="text/JavaScript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/JavaScript">
dojo.require("dojo.io.*");
var numRows = <%=numRows%>;
var numCols = <%=numCols%>;
var isClear = false;

var serverURL="<%=ClientUIConstants.PAGE_ClientUIC%>";

function handleColumnClick(colNo)
{
  var yy;
  var checkBox;
  for(yy=0;yy<numRows;yy++)
  {
     checkBox = document.getElementById(colNo + "_" + yy);
     checkBox.checked=false;
  }
}
function handleRowClick(rowNo)
{
  var xx;
  var checkBox;
  for(xx=0;xx<numCols;xx++)
  {
     checkBox = document.getElementById(xx + "_" + rowNo);
     checkBox.checked=false;
  }
}
function handleCornerClick()
{
   isClear = !isClear;
   var xx;
   var yy;
   var checkBox;
   for(yy=0;yy<numRows;yy++)
   {
     for(xx=0;xx<numCols;xx++)
     {
        checkBox=document.getElementById(xx + "_" + yy);
        checkBox.checked=isClear;
     }
   }
}

/*********************************************/
function sendData(eventName,jData)
{
    dojo.io.bind({
                 url:serverURL,
		  content:{event:eventName,
		           optArray:jData,
                           columnHeadings:"<%=colHeadList%>",
                           rowHeadings:"<%=rowHeadList%>",
                           clientNo:"<%=clientNo%>"},
                  error:handleError,
                  timeout: handleTimeout,
                  mimetype:"application/json"
                 });
}

function handleError(type,data,evt)
{
    alert("an error has occurred: " + data + "  " + type);
}
function handleTimeout()
{
    alert("timed out...");
}
function handleSaveChanges()
{
   var jDataArray = "";
   for(var yy = 0; yy < numRows; yy++)
   {
     for(var xx = 0;xx < numCols;xx++)
     {
       var checkBox = document.getElementById(xx + "_" + yy);
       if(jDataArray!="")jDataArray +=",";
	 jDataArray += xx + ":" + yy + ":" + checkBox.checked;
     }
   }
   sendData("saveMarketOptions",jDataArray);
   closeWindow();
}

function closeWindow()
{
  var daddy = window.self;
  daddy.opener = window.self;
  daddy.close();
}
</script>
<body bgcolor="#ffffff">
<h2>Marketing Options for <%=clientNoString + " " + clientName%>
</h2>
A tick in a box indicates that this marketing option IS allowed.
<table bordercolor="#666666" border="1" cellpadding="3" cellspacing="0">
  <tr>
    <td align="center" bgcolor="<%=linkColour%>">
      <a class="actionItem"
                  href="setAll"
                  onclick="handleCornerClick();event.returnValue=false;">All</a>
    </td>
    <%
      for(xx=0;xx<numCols;xx++)
      {
        %><td align="center" bgcolor="<%=linkColour%>">
           <a class="actionItem"
              href="set<%=columnHeadings[xx]%>"
                  onclick="handleColumnClick(<%=xx%>);event.returnValue=false;"><%=columnHeadings[xx]%></a>

        <!--    <input type="button" value="<%=columnHeadings[xx]%>" onclick="handleColumnClick(<%=xx%>)" >-->
          </td>
     <%}%>
  </tr>
  <%
     for(yy = 0; yy<numRows;yy++)
     {
       %><tr>
           <td align="center" bgcolor="<%=linkColour%>">
              <a class="actionItem"
                 href="set<%=rowHeadings[yy]%>"
                  onclick="handleRowClick(<%=yy%>);event.returnValue=false;"><%=rowHeadings[yy]%></a>
        <!--      <input type="button" value="<%=rowHeadings[yy]%>" onclick="handleRowClick(<%=yy%>)"/>-->
           </td>
           <%for(xx = 0;xx < numCols;xx++)
             {
                recFound = "checked";
                for(int zz = 0; zz < entries.size();zz++)
                {
                   mOpt = (MarketOption)entries.get(zz);
                   if(mOpt.getSubSystem().equals(columnHeadings[xx])
                      && mOpt.getMedium().equals(rowHeadings[yy])) recFound="";
                }
                cellRef = "" + xx + "_" + yy;
                %><td align="center">
                   <input type="checkbox" name="<%=cellRef%>" value="checkbox" <%=recFound%>>
                 </td>
          <%}%>
       </tr>
  <%    }
  %>
</table>
<br />
<input type="button" value="Save Changes" onclick="handleSaveChanges()"/>
<br />
Click a row heading to clear the entire row, or a column heading to clear the entire column<br>
Click 'All' to toggle the entire array, or click in an individual box to switch On/Off
</body>
</html>
