<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.travel.*"%>
<%@page import="com.ract.vehicle.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<title>Client advanced search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript" src="/scripts/shortcut.js"></script>
<script type="text/javascript">
   dojo.require("dojo.io.*");
   dojo.require("dojo.event.*");
   dojo.require("dojo.widget.*");
   dojo.require("dojo.widget.FilteringTable");
var serverURL="/ClientSearchUIC";
var logonId="<%=ServletUtil.getUserSession(request,true).getUserId()%>";
var cTable;
var selectedRow = 0;
var numRows = 0;
var inProgress = false;

function searchValue()
{
  var clientNo = dojo.byId('clientNo').value;
  if(clientNo != "")
  {
    getData(clientNo,"","","","","","","","");
  }
  else
  {
    loadData();  
  }
}

function handleClientNoEntry()
{
   if(window.event.keyCode==13)
   {
     searchValue()
   }
}
function loadData()
{
  var surname = dojo.byId('surname').value;
  var initial = dojo.byId('initial').value;
  var street = dojo.byId('street').value;
  var suburb = dojo.byId('suburb').value;
  var streetNo = dojo.byId('streetNo').innerHTML;
  var dateOfBirth = dojo.byId('dateOfBirth').value;
  var gender = dojo.byId('gender').value;
  var phoneNo = dojo.byId('phoneNo').value;
  var emailAddress = dojo.byId('emailAddress').value;
  getData("",surname,initial,streetNo,street,suburb,dateOfBirth,gender,phoneNo,emailAddress);
}
function getData(clientNo,surname,initial,streetNo,street,suburb,dateOfBirth,gender,phoneNo,emailAddress)
{
  clearTable();
  clearDetails();
  showProgressBar();
  dojo.io.bind({
                url: serverURL,
                content:{event:"clientSearch",
		         clientNo:clientNo,
		         surname:surname,
                         initial:initial,
			 streetNo:streetNo,
                         street:street,
                         suburb:suburb,
			 dateOfBirth:dateOfBirth,
			 gender:gender,
			 phoneNo:phoneNo,
			 emailAddress:emailAddress},
                   load:loadClients,
                   error:handleError,
                   //timeout: handleTimeout,
                   mimetype: "application/json"
                 });
}

function clearTable()
{
   cTable=dojo.widget.byId('cTable');
   cTable.store.setData("");
}

function loadClients(type,data,evt)
{
   var e;
   if(!inProgress) return;
   clearDetails();
   if(data.length==0)alert("No matching records");
   else if(data[0].Id=="Too many") alert("Too many.  Enter more criteria");
   else
   {
      numRows = data.length;
      selectedRow = -1;
      try
      {
	 cTable=dojo.widget.byId('cTable');
	 cTable.store.setData(data);
      }
      catch (e)
      {
         var errstr=e.name+": "+e.message;
         doLog(errstr,"error");
      }
   }
   hideProgressBar();
}
function getDetails()
{
  clearDetails();
  var cTable = dojo.widget.byId('cTable');
  var sData = cTable.getSelectedData();
  /*********** set selected row ************/
  for(xx = 0;xx<numRows;xx++)
  {
     if(cTable.isIndexSelected(xx)) selectedRow = xx;
  }
  /*****************************************/
  if(sData!=null && sData!="undefined")
  {
/*     alert("get Details for client " + sData.Id); */
     showProgressBar();
     dojo.io.bind({url:serverURL,
                   content:{event:"getClientDetail",
	                    clientNo:sData.clientNo},
			    load:showDetails,
			    mimetype: "application/json"})

  }
}
function showDetails(type,data,evt)
{
//  var rp = dojo.byId("resultPane");
//  rp.innerHTML=data;
   if(!inProgress) return;
   hideProgressBar();
   var dTable=dojo.widget.byId('dTable');
   dTable.store.setData(data);
}
function clearDetails()
{
//   var rp = dojo.byId("resultPane");
//   rp.innerHTML="";
    var dTable=dojo.widget.byId('dTable');
    dTable.store.setData("");
}
function handleKeyPress()
{
 if(window.event.keyCode==13)
 {
    loadData();
 }
}

function moveSelection(direction)
{
   var cTable = dojo.widget.byId('cTable');
  if(direction && selectedRow > 0)
  {
   cTable.resetSelections();
   selectedRow--;
   cTable.selectByIndex(selectedRow);
   cTable.renderSelections();
  }
  else if(!direction && selectedRow < numRows-1)
  {
	if(selectedRow>-1) cTable.toggleSelectionByIndex(selectedRow);
	selectedRow++;
	cTable.selectByIndex(selectedRow);
	cTable.renderSelections();
  }
  getDetails();
}
function handleArrowKeys(evt) {
    evt = (evt) ? evt : ((window.event) ? event : null);
   if (evt)
   {
      switch (evt.keyCode)
      {
         case 37:
         break;
         case 38:
            moveSelection(true);
         break;
         case 39:
         break;
         case 40:
	    moveSelection(false);
         break;
      }
    }
}

function handlePropertyKeyDown(evt)
{
    evt = (evt)? evt: ((window.event)? event:null);
	if(evt && evt.keyCode==113)
	{
	   document.getElementById('prop').style.display='';
	   document.form1.property.focus();
	   popUpQAS();
	}
}

function handleGenderKeyUp(evt)
{

    evt = (evt)? evt: ((window.event)? event:null);
    var gender = document.getElementById('gender');
    if(evt && evt.keyCode==70)
    {
	gender.value = 'Female';
    }
    else if(evt && evt.keyCode == 77)
    {
	gender.value = "Male";
    }
}

function loadInParent(url, closeSelf) {
    self.opener.top.leftFrame.location = url;
    if(closeSelf)  self.close();
}


function selectRow()
{
  var cTable = dojo.widget.byId('cTable');
  var sRow = cTable.getSelectedData();
  if (sRow!=null)
  {
     var url = "<%=ClientUIConstants.PAGE_ClientUIC%>?event=clientList_btnSearch&clientSearchType=<%=ClientUIConstants.SEARCHBY_CLIENT_NUMBER%>&clientSearchValue=" + sRow.clientNo;
     loadInParent(url,true);
  }
}

function popUpQAS ()
{
  var UIC = serverURL;
  //var qasUrl ="http://localhost:8080" + UIC + '?event=notifyQAS';
  var qasUrl = UIC;
  qasUrl += '&layout=' + "CustomerResidentialAuto";
  dojo.io.bind({
                url: serverURL,
                content:{event:"notifyQAS",
                         layout:"CustomerResidentialAuto"},
                 error:handleError
                });
}

function checkQASAddress()
{
  document.getElementById('prop').style.display='none';

	var suburb = dojo.byId("suburb").value;
	if ( suburb.indexOf("<QAS>") >= 0 )
	{
          decodeQASAddress (suburb);
    }
	else
	{
	// clear state and postcode fields
	   dojo.byId('state').innerHTML="";
	   dojo.byId('postcode').innerHTML="";
	}
}

function decodeQASAddress (inString)
{

  inString = inString.substring(5);
  var sArray = inString.split("  ");
  var nn = sArray.length;
  var suburb="";
  for(var xx=0;xx<nn-3;xx++)
  {
    if(suburb!="")suburb = suburb + " ";
    suburb = suburb + sArray[xx];
  }

  var postcode = sArray[nn-3];
  var state = sArray[nn-1];
  var dpid = sArray[nn-2];
  dojo.byId('state').innerHTML=state;
  dojo.byId('postcode').innerHTML=postcode;
  dojo.byId('suburb').value=suburb;
}


function getStreetNo()
{
   var streetChar = dojo.byId("streetChar").value;
   var streetNo = "";
   var pos=0;
   var lastDigitAt=0;
   var firstDigitAt=0;
   pos = streetChar.length - 1;
   while(pos >= 0
         && (streetChar.charAt(pos)<"0"
	     || streetChar.charAt(pos) >"9"))
    {
       pos = pos - 1;
    }
    lastDigitAt = pos + 1;
    while(pos >= 0
         && streetChar.charAt(pos)>="0"
		 && streetChar.charAt(pos)<="9")
    {
	pos = pos - 1;
    }
    firstDigitAt = pos + 1;
    streetNo =streetChar.substring(firstDigitAt,lastDigitAt);
    dojo.byId("streetNo").innerHTML=streetNo;
}
function handleError(type,data,evt)
{
  alert ("Error detected.....");
}

function showProgressBar()
{
   if(!inProgress)
   {
//      document.getElementById('pgBarId').style.display='';
      var pgb = dojo.widget.byId('pgBar');
      pgb.startAnimation();
      inProgress=true;
   }
}
function hideProgressBar()
{
   var pgb = dojo.widget.byId('pgBar');
   pgb.stopAnimation();
   inProgress=false;
//	 document.getElmentById('pgBarId').style.display="none";
}

function formatNumber(df,formatStr)
{
   var value = df.value;
   var valueLen = value.length;
   var fChar = formatStr.charAt(valueLen);
   var kc=event.keyCode;
   if(kc>=96)kc=kc-48;
   var keyVal = String.fromCharCode(kc);
   if(kc==8)df.value=df.value.substring(0,valueLen-1);
   else if(kc>=48 && kc<=57)
   {
      if(fChar!=9)
	  {
	    df.value = df.value + fChar;
	  }
   }
   else if(kc!=9 && kc!= 13)event.returnValue=false;
   if(valueLen>=formatStr.length
      && kc>=48 && kc <=57)
   {
      df.value = df.value.substring(0,valueLen-1);
   }
}

function validateDate(thing)
{
   var dString = thing.value;
   var ddArray = dString.split("/");
   var day = Number(ddArray[0]);
   var month = Number(ddArray[1]);
   var year = Number(ddArray[2]);
   var e;
   try
   {
     if(month > 12)
	 {
	   e="Invalid Month";
	   throw e;
     }
	 if(day>31
	   ||((month==3 || month==6 || month==9 || month==11)&& day == 31)
	   ||(month==2 && year%4!=0 && day>28)
	   ||(month==2 && year%1000==0 && day > 28))
	 {
	   e = "Invalid day";
	   throw e;
	 }
	 if(year<1000)
	 {
	   e="Invalid year\nEnter four digits";
	   throw e;
	 }
       var dDate = new Date(Number(ddArray[2]),Number(ddArray[1])-1,Number(ddArray[0]));
   }
   catch(e)
   {
      alert(e+"");
	  thing.focus();
	  thing.selected=true;
   }
}

function initialise()
{
   document.form1.initial.focus();
   shortcut.add("ctrl+Enter",selectRow);
}
document.onkeyup = handleArrowKeys;

</script>
<style type="text/css">
    h1 {font-family:Arial;
        font-size:14pt;
	font-weight:bold;}
    h2 {font-family:Arial;
	font-size:10pt;
	font-weight:bold;}
    body {font-family:Arial;
	  font-size:0.9em;
	  font-weight:normal;}

	table {
           font-family:Lucida Grande, Verdana;
		       font-size:0.7em;
                       width:100%;
		       border:1px solid #ccc;
		       border-collapse:collapse;
		       cursor:default;
	}
	table thead td, table thead th {
		        padding:2px;
			font: 10ptArial
			font-weight:bold;
			font-style:italic;
			background-repeat:no-repeat;
			background-position:top right;
			background:#c0c0ff
			color:green;
	}
	table tbody tr td{alternateRows:true;
			          border-bottom:1px solid #ddd;}
    table tbody tr.alt td{border-bottom:1px solid #ddd;
                             background:#f3fdfa;}

	table tbody tr.selected td{
			background: yellow;
	}
	defText{font-family:Arial;
	        font-size=10pt;}

</style>

<body bgcolor="#FFFFFF" text="#000000" onload="initialise()">
<form name="form1">
  <table border="0">
  <tr>
    <td colspan="4"><h4>Client Search</h4>
  </td>
  </tr>
  <tr>
    <td align="right">Client Number</td>
    <td>
      <input type="text" name="clientNo" onkeypress="handleClientNoEntry()" tabindex="1">
    </td>
    <td align="right">&nbsp;Date of Birth</td>
    <td><input type="text" name="dateOfBirth" size="10" onkeypress="handleKeyPress()" tabindex="8" onkeydown="formatNumber(this,'99/99/9999')" onblur="validateDate(this)" title="If you enter a birthdate, clients with no birthdate entered will be ignored"></td>
  </tr>
  
    <tr>
      <td align="right">Initial/Surname</td>
      
      <td>
        <input type="text" name="initial" size="4" onkeypress="handleKeyPress()" tabindex="2">
        <input type="text" name="surname" onkeypress="handleKeyPress()" tabindex="3"
                title="Enter surname Accepts *wildcard character">
      </td>
      <td align="right">Gender</td>
      <td>
        
      <input type="text" name="gender" size="6" onkeypress="handleKeyPress()" onkeyup="handleGenderKeyUp()" tabindex="9"></td>
    </tr>
    <tr>
      <td align="right">No/Street</td>
      <td width="">
      <span id="prop" style="display:none">
        <input type="text" name="property" tabindex="4" ></span>
        <input type="text" name="streetChar" size="6"
		                       onchange="getStreetNo()"
				       tabindex="5"
                                       onkeydown="handlePropertyKeyDown()"
                                      onkeypress="getStreetNo(); handleKeyPress()"
                                      title="F2 for QAS">
        <input type="text" name="street" onkeypress="handleKeyPress()"
		                         onkeydown="handlePropertyKeyDown()" tabindex="6"
                                         title="F2 for QAS"><span id="streetNo" ></span>
        </td>
        <td align="right">Phone</td>
        <td valign="top"><input type="text" name="phoneNo" size="13" onkeypress="handleKeyPress()" tabindex="10"></td><td><br></td>  
      </tr>
      <tr>
        <td align="right">Suburb</td>
        <td>
        <input type="text" name="suburb" onkeypress="handleKeyPress()"
		                          onchange="checkQASAddress()"
					 onkeydown="handlePropertyKeyDown()" tabindex="7"
                                          title="F2 for QAS">
	   <span id="state"></span>&nbsp;<span id="postcode"></span>
      </td>
      <td align="right" valign="top">Email</td>
      <td><input type="text" name="emailAddress"></td>
    </tr>
    <tr>
      <td valign="top"></td>
      <td valign="top"><input type="button" name="search" value="Search" onclick="searchValue()"></td>
      <td valign="top"></td>
      <td valign="top"></td>
    </tr>
  </table>
  <div dojoType="contentPane" id="contentPane" style="overflow: auto;width:100%;height:180;">
  <div id="tablePlace">

    <table  dojoType="filteringTable" id="cTable"
	        cellpadding="2"
			alternateRows="true"
			multiple="false"
			onClick="getDetails()"
                        title="Press Ctrl + Enter to select client"
			>
      <thead >
      <tr>
   	      <th field="Id" dataType="Number" align="left" valign="top"></th>
	      <th field="clientNo" dataType="Number" align="left" valign="top">Clt No</th>
	      <th field="gname" dataType="String" align="left" valign="top"  >Given Names</th>
              <th field="surname" dataType="String" align="left" valign="top">Surname</th>
              <th field="group" datatyp="String" align="left" valign="top">Group?</th>
	      <th field="streetNo" dataType="String" align="left" valign="top">No</th>
	      <th field="street" dataType="String" align="left" valign="top">Street</th>
	      <th field="suburb" dataType="String" align="left"  valign="top">Suburb</th>
              <th field="status" datatyp="String" align="left" valign="top">Status</th>
	</tr>
     </thead>
   <tbody>
   </tbody>
   </table>
</div>
</div>

  <table  dojoType="filteringTable" id="dTable"
	        cellpadding="2"
			alternateRows="true"
			multiple="false"
			onClick="getDetails()">
     <thead>
      <tr>
	      <th field="heading" dataType="char" align="left" valign="top"></th>
	      <th field="value" dataType="String" align="left" valign="top"></th>
	  </tr>
     </thead>
   <tbody>
   </tbody>
   </table>
   <div id="pgBarId" align="center">
      <div  width="700" height="5" id="pgBar"  dojoType="ProgressBar"></div>
   </div>
   
  <input type="button" name="xxx" value="Use Selected" onClick="selectRow()">   
   
</form>
</body>
</html>