<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="%{categoryCode!=null}">
<table>
<s:hidden name="due" value="%{followUpDate}"/>
<s:label label="Assignee/Notifications" name="notificationName"/>
<s:label label="Due" name="followUpDate"/>
</table>
</s:if>