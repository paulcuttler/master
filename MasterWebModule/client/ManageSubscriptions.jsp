<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.travel.*"%>
<%
Collection clientSubscriptionList = (Collection)request.getAttribute("clientSubscriptionList");
Collection travelBranchList = (Collection)request.getAttribute("travelBranchList");
Collection unsubscribedPublicationList = (Collection)request.getAttribute("unsubscribedPublicationList");

//remove publication types that are already in the client publication list.
%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
<script language="Javascript">

//add publication
function addSubscription(select)
{
//  alert('add sub');
  document.manageSubscriptions.event.value = 'addSubscription';
//  alert('event set');
  if (select != null)
  {
   for (var i=0; i < select.length; i++)
   {
     if (select[i].selected)
     {
       document.manageSubscriptions.publicationCode.value = select[i].value;
       document.manageSubscriptions.submit();
       return;
     }
   }
  }
}
//remove publication
function removeSubscription(radioButtonList)
{
  document.manageSubscriptions.event.value = 'removeSubscription';
  if (radioButtonList != null)
  {
    document.manageSubscriptions.publicationCode.value = returnRadioButtonValue(radioButtonList);
//    alert(document.manageSubscriptions.publicationCode.value);
    if (document.manageSubscriptions.publicationCode.value == null)
    {
      alert('No subscription selected.');
    }
    else
    {
      document.manageSubscriptions.submit();
    }
  }
  else
  {
    alert('No subscription selected.');
  }
}
</script>
</head>

<body>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <form name="manageSubscriptions" method="post" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
    <input type="hidden" name="publicationCode"/>
    <input type="hidden" name="event"/>
  <tr valign="top">
    <td>

<h2>Manage Subscriptions</h2>

<%@ include file="/client/ViewClientInclude.jsp"%>

    <input type="hidden" name="clientNumber" value="<%=client.getClientNumber()%>"/>

<table>
<tr>
    <td><h3>Unsubscribed Publications</h3></td>
</tr>

  <%
  if (unsubscribedPublicationList != null &&
      unsubscribedPublicationList.size() > 0)
  {
  %>
<tr>
<td class="listHeadingPadded">Subscription</td>
<td class="listHeadingPadded">Branch</td>
<td></td>
</tr>
<tr>
  <td>
    <select name="publicationList">
  <%
    Publication publication = null;
    Iterator pubListIt = unsubscribedPublicationList.iterator();
    while (pubListIt.hasNext())
    {
      publication = (Publication)pubListIt.next();
  %>
      <option value="<%=publication.getPublicationCode()%>"><%=publication.getDescription()%></option>
  <%
    }
  %>
    </select>


  </td>
  <td>
  <%
  if (travelBranchList != null &&
      travelBranchList.size() > 0)
  {
  %>
    <select name="travelBranchList">
      <option value=""></option>
  <%
    TravelBranch travelBranch = null;
    Iterator travelBranchListIt = travelBranchList.iterator();
    SalesBranchVO salesBranch = null;
    while (travelBranchListIt.hasNext())
    {
      travelBranch = (TravelBranch)travelBranchListIt.next();
      salesBranch = travelBranch.getSalesBranch();
  %>
      <option value="<%=travelBranch.getSalesBranchCode()%>"><%=salesBranch.getSalesBranchName()%></option>
  <%
    }
  %>
    </select>
  <%
  }
  %>
  </td>
  <td><input type="button" value="Add subscription" onclick="addSubscription(document.all.publicationList)"></td>
</tr>
  <%
  }
  else
  {
  %>
<tr>
  <td class="helpText" colspan="3">No available publications found.</td>
</tr>
  <%
  }
  %>
</table>

<table>
<tr>
    <td><h3>Subscription List</h3></td>
</tr>
<%
if (clientSubscriptionList != null &&
    clientSubscriptionList.size() > 0)
{
%>
<tr class="headerRow" valign="top">
  <td class="listHeadingPadded"></td>
  <td class="listHeadingPadded">Subscription</td>
  <td class="listHeadingPadded">Date</td>
  <td class="listHeadingPadded">Status</td>
  <td class="listHeadingPadded">User</td>
  <td class="listHeadingPadded">Delivery Method</td>
  <td class="listHeadingPadded">Branch</td>
</tr>
<%
  ClientSubscription clientSubscription = null;
  Iterator clientSubscriptionIt = clientSubscriptionList.iterator();
  int counter = 0;
  boolean first = true;
  Publication pub = null;
  while (clientSubscriptionIt.hasNext())
  {
    counter++;
    clientSubscription = (ClientSubscription)clientSubscriptionIt.next();
    pub = clientSubscription.getPublication();
    //assume a key exists
%>
<tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
<%
if (ClientSubscription.STATUS_ACTIVE.equals(clientSubscription.getClientSubscriptionPK().getSubscriptionStatus()) &&
    !pub.isSystemManaged())
{
%>
  <td class="listItemPadded" align="center">
    <input name="selectedLine" value="<%=clientSubscription.getClientSubscriptionPK().getSubscriptionCode()%>,<%=clientSubscription.getClientSubscriptionPK().getSubscriptionStatus()%>,<%=clientSubscription.getClientSubscriptionPK().getSubscriptionDate().formatLongDate()%>" type="radio" <%=(first ? "CHECKED" : "")%>>
  </td>
<%
}
else
{
%>
  <td>&nbsp;</td>
<%
}
%>
  <td><%=clientSubscription.getClientSubscriptionPK().getSubscriptionCode()%></td>
  <td><%=clientSubscription.getClientSubscriptionPK().getSubscriptionDate().formatLongDate()%></td>
  <td><%=clientSubscription.getClientSubscriptionPK().getSubscriptionStatus()%></td>
  <td><%=clientSubscription.getUserId()!=null?clientSubscription.getUserId():""%>  </td>
  <td><%=clientSubscription.getDeliveryMethod()!=null?clientSubscription.getDeliveryMethod():""%></td>
  <td><%=clientSubscription.getSalesBranchCode()!=null?clientSubscription.getSalesBranchCode():""%></td>
</tr>
<%
    first = false;
  }
}
else
{
%>
<tr>
  <td class="helpText" colspan="4">No subscriptions found.</td>
</tr>
<%
}
%>

</table>

    </td></tr>
    <tr valign="bottom">
    <td>
      <table width="0%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>
               <input type="button" name="btnRemove" value="Remove Subscription" onclick="removeSubscription(document.all.selectedLine)">
            </td>
        </tr>
      </table>
    </td>
  </tr>
  </form>
</table>

</body>

</html>
