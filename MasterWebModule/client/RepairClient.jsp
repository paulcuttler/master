<%@page import="com.ract.util.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.common.*"%>

<html>
<head>
<title>RepairMembership</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
int updateDeceasedClientsRepairCount = clientMgr.getDeceasedClientsToRepairCount();
%>

<body>
<h2>Repair Client
</h2>

<form name="repairClientForm" method="post" action="<%=ClientUIConstants.UIC_CLIENT_ADMIN%>">

<input type="hidden" name="event">

<p>Update clients that have "Estate" or "Late" as part of the surname with the deceased client status.
<br><%=updateDeceasedClientsRepairCount%> to repair.
<br><input type="submit" name="btnRepairDeceasedClientStatus" value="Go" onclick="document.all.event.value='RepairClient_btnRepairDeceasedClientStatus'" <%=(updateDeceasedClientsRepairCount < 1 ? "disabled" : "")%>>
</p>

</form>

</body>
</html>

