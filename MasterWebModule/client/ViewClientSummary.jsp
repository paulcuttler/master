<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.travel.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.payment.PaymentException"%>
<%@page import="com.ract.vehicle.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.payment.ui.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<title>Search Client Detail</title>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>
		<script language="JavaScript">
function showMembershipQuoteDetail(quoteNumber)
{
   var url = '<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewClientSummary_viewQuote&quoteNumber=' + quoteNumber;
   window.open(url, '', 'titlebar=yes,toolbar=no,status=no,scrollbars=yes,resizable=yes');
   return true;
}
</script>
	</head>
	
<%
Boolean askClientQuestions = (Boolean)request.getAttribute("askClientQuestions");
%>	
<script type="text/javascript">
   function askQuestions(url)
   {
      var nav = false;
      
      //check if customer is missing data
      if (<%=askClientQuestions%>) //true
      {     
        if (confirm("WARNING! Client details need to be updated. Do you want to OK: complete transaction anyway or Cancel: abort transaction?"))
        {
          nav = true;
        }
      }
      else //false
      {
        nav = true;
      }   
      if (nav)
      {  
	    document.location.href = url;
	  }
	}
</script>	
	<body>

		<table border="0" cellspacing="0" cellpadding="0" width="100%"
			height="100%">
			<tr valign="top">
				<td>
					<h1>
						View Client Summary
					</h1>

					<table border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td>
								<%@ include file="/client/ViewClientInclude.jsp"%>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<%@include file="/travel/TramadaClientInclude.jsp"%>
							</td>
						</tr>
							<%
								if (userSession.getUser().isPrivilegedUser(
										Privilege.PRIVILEGE_CREATE_DEBTOR))
								{
							%>
						
						<tr>
							<td colspan="4">
								<%@include file="/payment/DebtorInclude.jsp"%>
							</td>
						</tr>
						<%
							}
						%>

						<script language="javascript">
function editClient()
{
    var isOK = false;
    // first check to see if the client record has password protection
    if(<%=isProtected%>)
    {
       isOK = window.confirm("You have chosen to have your personal details"
                  + "\npassword protected.  So I have to ask you"
                  + "\nQUESTION: <%=phoneQuestion%> "
                  + "\nANSWER:   <%=phonePassword%>"
                  + "\nIf the correct answer was given, click OK else CANCEL");
    }
    else isOK = true;
    if(isOK)
    {
      //first form
      document.forms[0].submit();
    }
    else return;
}



function setPhonePassword()
{
  var isOK = false;
  var pQuestion="";
  var pPassword="";
  var nQuestion="";
  var nPassword="";
  if(<%=isProtected%>)
  {
    pQuestion = "<%=phoneQuestion%>";
    pPassword = "<%=phonePassword%>";
  }
  var url="client/PhonePassword.jsp?clientNo=" + <%=client.getClientNumber()%>
                + "&phoneQuestion=" + pQuestion
                + "&phonePassword=" + pPassword;
  var win = window.open(url,"_blank","height=130,width=550,left=50,top=50,"
                    + "resizable=no,status=no,scrollbars=no,toolbar=no,menubar=no,location=no");
  win.focus();
 }
 function setMarketOptions()
 {
     var url="client/MarketOptions.jsp?clientNo=" + <%=client.getClientNumber()%>;
     var win = window.open(url,"_blank","height=310,width=450,left=100,top=100,"
                    + "resizable=yes,status=no,scrollbars=auto,toolbar=no,menubar=no,location=no");
     win.focus();
 }
</script>

						<tr>
							<td colspan="4">
								<h4>
									Client
								</h4>
							</td>
						</tr>

						<tr>
							<td>

<% 
Boolean disableClientManagement = (Boolean) request.getAttribute("disableClientManagement");
if (disableClientManagement != null && !disableClientManagement) {
%>							  
			                    <form action="showEditClient.action">
									<input type="hidden" name="clientNumber"
										value="<%=client.getClientNumber()%>">
								</form> 							
								<a class="actionItem" title="Edit client"
									href="javascript:editClient();"> Edit
									client </a>
								<br />
<%
} // end disableClientManagement
%>
								<a class="actionItem" title="Set Password"
									href="javascript:setPhonePassword();"> Set
									Phone Password </a>
								<br />
<% 
Boolean disableEditMarketing = (Boolean) request.getAttribute("disableEditMarketing");
if (disableEditMarketing != null && !disableEditMarketing) {
%>  
								<a class="actionItem" title="Edit Market Options"
									href="javascript:setMarketOptions();"> Edit
									Marketing Options </a> 
								<br /> 
<% 
} // end disableEditMarketing

Boolean disableJourneysManagement = (Boolean) request.getAttribute("disableJourneysManagement");
if (disableJourneysManagement != null && !disableJourneysManagement) {
%>  
								<a class="actionItem"
									title="Edit the Journeys Magazine attributes of a customer that determine if they receive a Journeys Magazine."
									href="/showEditMotorNews.action?clientNumber=<%=client.getClientNumber()%>">
									Edit Journeys Magazine </a>
								<br />
<%
} // end disableJourneysManagement

Boolean disablePublicationManagement = (Boolean) request.getAttribute("disablePublicationManagement");
if (disablePublicationManagement != null && !disablePublicationManagement) {
%>
								<a class="actionItem" title="Manage publication subscriptions."
									href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=manageSubscriptions&clientNumber=<%=client.getClientNumber()%>">
									Manage Publications </a>
								<br />
<%
} // end disablePublicationManagement
%>
								<a class="actionItem" title="View client notes."
									href="viewNotes.action?clientNumber=<%=client.getClientNumber()%>">
									View Client Notes</a>									
							</td>
						</tr>


						<%
							Collection memList = null;
							MembershipVO memVO = null;

							MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
							memList = membershipMgr.findMembershipByClientNumber(client
									.getClientNumber());

							// Can't access membership data without the view membership privilege
							if (userSession != null)
							{
								if (userSession.getUser()
										.isPrivilegedUser(Privilege.PRIVILEGE_FIN_VIEW))
								{
						%>
						<tr>
							<td colspan="4">
								<h4>
									Financials
								</h4>
							</td>
						</tr>
						<tr valign="top">
							<td colspan="4"></td>
						</tr>
						<tr>
							<td>
								<a class="actionItem"
									title="View the direct debit authorities that have been given by the customer."
									href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientDDDetails&clientNumber=<%=client.getClientNumber()%>">
									View direct debit authorities </a>
								<br />
								<a class="actionItem"
									title="View the payment information for a customer including ledgers."
									href="<%=PaymentUIConstants.PAGE_PaymentUIC%>?event=viewPayableItemList&clientNumber=<%=client.getClientNumber()%>">
									View payable items </a>
								<br />
								<a class="actionItem"
									title="View the pending fees attached to a customer. These are typically generated at renewal."
									href="<%=PaymentUIConstants.PAGE_PaymentUIC%>?event=viewPendingFees&clientNumber=<%=client.getClientNumber()%>">
									View pending fees </a>
								<br />
								<a class="actionItem"
									title="View the receipting history for a customer including all financial transactions."
									href="<%=PaymentUIConstants.PAGE_PaymentUIC%>?event=viewReceiptingHistory&clientNumber=<%=client.getClientNumber()%>">
									View receipting history </a>
							</td>
						</tr>

						<%
							}
								if (userSession.getUser()
										.isPrivilegedUser(Privilege.PRIVILEGE_MEM_VIEW))
								{
						%>
						<tr>
							<td colspan="4">
								<h4>
									Membership
								</h4>
							</td>
						</tr>
						<tr>
							<td>
								<%
									//log("check group 1");
											// Make a link for creating a new membership group if the client does not already
											// belong to an active membership group

											String createGroupInvalidReason = null;
											boolean clientHasPersonalMembership = false;
											double amountOutstanding = 0.0;
											//log("check group 1a");
											//typically only one
											for (Iterator i = memList.iterator(); i.hasNext();)
											{
												memVO = (MembershipVO) i.next();

												if (MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL.equals(memVO
														.getMembershipTypeCode()))
												{
													clientHasPersonalMembership = true;
												}
												if (createGroupInvalidReason == null)
												{
													try
													{
														//log("check group 1b");
														createGroupInvalidReason = memVO
																.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP);
														//log("check group 1c");
													} catch (Exception pe)
													{
														createGroupInvalidReason = pe.getMessage();
													}
												}
											}

											//log("check group 2");

											// Provide a create link for each membership type.
											// If they already have a personal membership then don't show the link to create a personal membership.
											MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
											Collection memTypeList = refMgr.getMembershipTypeList();
											String createInvalidReason = MembershipHelper
													.validateClientForTransaction(client,
															MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, memVO);
											if (memTypeList != null && !memTypeList.isEmpty())
											{
												Iterator memTypeListIterator = memTypeList.iterator();
												while (memTypeListIterator.hasNext())
												{
													MembershipTypeVO memTypeVO = (MembershipTypeVO) memTypeListIterator
															.next();

													if (createInvalidReason != null)
													{
								%>
								<a class="actionItemGrey" title="<%=createInvalidReason%>"
									href="#">New <%=memTypeVO.getMembershipTypeCode()%>
									membership...</a>
								<br />
								<%
									}
													// Not a personal membership type.
													else if (!MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL
															.equals(memTypeVO.getMembershipTypeCode()))
													{
								%>
								<a class="actionItem" target="rightFrame"
									title="Create a new <%=memTypeVO.getMembershipTypeCode()%> membership."
									href="javascript:askQuestions('<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewClientSummary_createMembership&clientNumber=<%=client.getClientNumber()%>&membershipType=<%=memTypeVO.getMembershipTypeCode()%>');">New
									<%=memTypeVO.getMembershipTypeCode()%> membership...</a>
								<br />
								<%
									}
													// Is a personal membership type and they don't have an existing personal membership
													else if (MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL.equals(memTypeVO
															.getMembershipTypeCode())
															&& !clientHasPersonalMembership)
													{
								%> 
								<a class="actionItem" target="rightFrame"
									title="Create a new <%=memTypeVO.getMembershipTypeCode()%> membership."
									href="javascript:askQuestions('<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewClientSummary_createMembership&clientNumber=<%=client.getClientNumber()%>&membershipType=<%=memTypeVO.getMembershipTypeCode()%>');")>New
									<%=memTypeVO.getMembershipTypeCode()%> membership...</a>
								<br />
								<%
									}
													// Is a personal membership type and they DO have an existing personal membership
													else if (MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL.equals(memTypeVO
															.getMembershipTypeCode())
															&& clientHasPersonalMembership)
													{
								%>
								<a class="actionItemGrey"
									title="The client already has a personal membership." href="#">New
									<%=memTypeVO.getMembershipTypeCode()%> membership...</a>
								<br />
								<%
									}
												}
											}

											// Make a link for doing an interstate transfer as long as there's no active personal membership or it is undone.
											if (createInvalidReason == null
													&& (!clientHasPersonalMembership /*||
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			 (memVO != null &&  MembershipVO.STATUS_UNDONE.equals(memVO.getStatus()))*/))
											{
								%>
								<a class="actionItem"
									title="Transfer a membership from an affiliated club."
									target="rightFrame"
									href="javascript:askQuestions('<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=transferMembership&clientNumber=<%=client.getClientNumber()%>');">Transfer
									membership...</a>
								<br />
								<%
									} else
											{
												StringBuffer desc = new StringBuffer();
												if (createInvalidReason != null)
												{
													desc.append(createInvalidReason);
												} else
												{
													desc
															.append(clientHasPersonalMembership ? "Transfer an affiliated membership from the membership screen."
																	: "");
												}
								%>
								<a class="actionItemGrey" title="<%=desc.toString()%>" href="#">Transfer
									membership...</a>
								<br />

								<%
									}

											if (createInvalidReason != null)
											{
												createGroupInvalidReason = createInvalidReason;
											}

											if (createGroupInvalidReason == null)
											{
								%>
								<a class="actionItem" target="rightFrame"
									title="Create a new membership group."
									href="javascript:askQuestions('<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewClientSummary_createGroup&clientNumber=<%=client.getClientNumber()%>');">New
									group membership...</a>
								<br />
								<%
									} else
											{
								%>
								<a class="actionItemGrey" title="<%=createGroupInvalidReason%>"
									href="#">New group membership...</a>
								<br />
								<%
									}
											log("quote 1");
											// If the client has roadside quotes then add a link to access them
											Collection quoteList = MembershipEJBHelper.getMembershipMgr()
													.getCurrentQuoteList(clientNumber);
											if (!quoteList.isEmpty())
											{
								%>
								<a class="actionItem" target="rightFrame"
									title="View all Roadside quotes for the client."
									href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewClientSummary_viewQuoteList&clientNumber=<%=client.getClientNumber()%>">View
									all quotes...</a>
								<br />
								<%
									} else
											{
								%>
								<a class="actionItemGrey"
									title="The client does not have any Roadside quotes." href="#">View
									all quotes...</a>
								<br />
								<%
									}

											// Add links for each existing membership
											if (!memList.isEmpty())
											{
										Iterator memVOListIterator = memList.iterator();
													while (memVOListIterator.hasNext())
													{
														memVO = (MembershipVO) memVOListIterator.next();
									%>
									<li>
										<%=MembershipUIHelper.displayMembershipLine(memVO, true, true)%>
									</li>



									<%
										}
									}
											//log("quote 2");
											if (!quoteList.isEmpty())
											{
												MembershipQuote memQuote;
												MembershipQuoteComparator mqc = new MembershipQuoteComparator(true);
												Collections.sort((List) quoteList, mqc);
												for (Iterator i = quoteList.iterator(); i.hasNext();)
												{

										memQuote = (MembershipQuote) i.next();
														// Only show current non attached quotes
														if (memQuote.isCurrent() && !memQuote.isAttached())
														{
									%>

									<li>
										<a class="actionItem" target="rightFrame"
											title="View the details of this membership quote." href="#"
											onclick="showMembershipQuoteDetail(<%=memQuote.getQuoteNumber()%>); return true;">
											<%=memQuote%> </a>
									</li>
									<%
										}
									}
											}
											//log("quote 3");
								%>
							</td>
						</tr>
						<%
							} //end of mem
								else if (userSession.getUser().isPrivilegedUser(
										Privilege.PRIVILEGE_TRAVEL_MEMBERSHIP_VIEW))
								{
						%>
						<tr>
							<td colspan="4">
								<h4>
									Membership
								</h4>
							</td>
						</tr>

						<%
							for (Iterator i = memList.iterator(); i.hasNext();)
									{
										memVO = (MembershipVO) i.next();
									}
									if (memVO != null)
									{
						%>
						<tr>
							<td>
								<ul>
									<li><%=MembershipUIHelper.displayMembershipLine(memVO, false, true)%></li>
								</ul>
							</td>
						</tr>
						<%
							} else
									{
						%>
						<tr>
						  <td colspan="4" class="helpText">There are no memberships attached.</td>
						</tr>
						<%
							}
								}
							} //userSession is null 
							else
							{
						%>
						<tr>
							<td colspan="4" class="helpText">Your session is no longer valid.</td>
						</tr>
						<%
							}
						%>
					</table>
				</td>
			</tr>
			<%--  The buttons are made to stick to the bottom edge of the page --%>
			<tr valign="bottom">
				<td>
				<table>
				<tr>
				<td>
					<form name="viewclient" method="post"
						action="/viewClient.action">
						<input type="hidden" name="clientNumber"
							value="<%=client.getClientNumber()%>">
					<input type="submit" value="View Client Details">
					</form>
					</td>
					<td>
					<form name="addNote" method="post"
						action="addNote.action">
						<input type="hidden" name="clientNumber"
							value="<%=client.getClientNumber()%>">
									<input type="submit" value="Add Client Note">
					</form>				
				</td>
			</tr>
			</table>
			</td>
			</tr>
		</table>

	</body>

</html>
