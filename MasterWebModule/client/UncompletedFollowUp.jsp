<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>

<html>
<head>
<title>
UncompletedFollowUp
</title>


<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
   dojo.require("dojo.io.*");
   dojo.require("dojo.event.*");
   dojo.require("dojo.widget.*");
   dojo.require("dojo.widget.FilteringTable");
var serverURL="/AddressUpdateUIC";
var logonId;
function loadData()
{
   var slsbch = dojo.byId('salesbranch').value;
   var fDate = dojo.byId('fDate').value;
   var lDate = dojo.byId('lDate').value;
   getData(slsbch,fDate,lDate);
}

// formats an input field using a specified format string
// used for date format fields
function formatNumber(df,formatStr)
{
   var value = df.value;
   var valueLen = value.length;
   var fChar = formatStr.charAt(valueLen);
   var kc=event.keyCode;
   if(kc>=96)kc=kc-48;
   var keyVal = String.fromCharCode(kc);
   if(kc==8)df.value=df.value.substring(0,valueLen-1);
   else if(kc>=48 && kc<=57)
   {
      if(fChar!=9)
	  {
	    df.value = df.value + fChar;
	  }
   }
   else if(kc!=9 && kc!= 13)event.returnValue=false;
   if(valueLen>=formatStr.length
      && kc>=48 && kc <=57)
   {
      df.value = df.value.substring(0,valueLen-1);
   }
}

function validateDate(thing)
{
   var dString = thing.value;
   var ddArray = dString.split("/");
   var day = Number(ddArray[0]);
   var month = Number(ddArray[1]);
   var year = Number(ddArray[2]);
   var e;
   try
   {
     if(month > 12)
	 {
	   e="Invalid Month";
	   throw e;
     }
	 if(day>31
	   ||((month==3 || month==6 || month==9 || month==11)&& day == 31)
	   ||(month==2 && year%4!=0 && day>28)
	   ||(month==2 && year%1000==0 && day > 28))
	 {
	   e = "Invalid day";
	   throw e;
	 }
	 if(year<1000)
	 {
	   e="Invalid year\nEnter four digits";
	   throw e;
	 }
       var dDate = new Date(Number(ddArray[2]),Number(ddArray[1])-1,Number(ddArray[0]));
   }
   catch(e)
   {
      alert(e+"");
	  thing.focus();
	  thing.selected=true;
   }
}
function getData(salesBranch,fDate,lDate)
{
    dojo.io.bind({
                  url: serverURL,
                  content:{event:"loadUncompleted",
		           salesBranch:salesBranch,
                           fDate:fDate,
                           lDate:lDate},
                   load:showBusiness,
                   //error:handleError,
                   //timeout: handleTimeout,
                   mimetype: "application/json"
                 });
}
function showBusiness(type,data,evt) {
    var e;
    try {
        bTable=dojo.widget.byId('fTable');
        bTable.store.setData(data);
    }
	catch (e) {
       var errstr=e.name+": "+e.message;
       doLog(errstr,"error");
    }
    var businessT = document.getElementById('fTable');
    if(businessT.rows.length==1) //header row only
    {
	alert("No changes found.");
    }
}
function showUpdater(rowId,clientNo,logonId,userId,requestNo)
{
/*  alert(clientNo
     + "\n" + logonId
     + "\n" + userId
     + "\n" + requestNo);
*/
    /*find the row identified by rowId and nobble it somehow*/
    var businessT = document.getElementById('fTable');
    var row = businessT.rows[rowId + 1];  // row[0] is the header row

    row.cells[1].innerHTML="PROCESSED";


    var url="PopupUIC"
           + "?event=showAddressUpdater"
           + "&clientNo=" + clientNo
           + "&userid=" + userId
           + "&requestSeq=" + requestNo;
             var win = window.open(url,"_blank","height=300,width=1000,left=10,top=10,"
                                     + "resizable=yes,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no");
             win.focus();
 }

dojo.addOnLoad(loadData);
</script>
</head>
<body bgcolor="#ffffff">
<h1>
Address changes not checked for associated clients
</h1>

<form>
  <input type="hidden" name="logonId" value="<%=ServletUtil.getUserSession(request,true).getUserId()%>"/>
<table width="81%" border="0" bordercolor="#CCCCCC">
  <tr valign="bottom">
    <td align="right"> Sales Branch </td>

    <td align="left">
      <input type="text" id="salesbranch" value="All">
     </td>
     <td></td><td></td>
 </tr>
  <tr>
   <td align="right"> Between </td>
   <td align="left">
           <input type="text"
		          id="fDate"
		          onkeydown="formatNumber(this,'99/99/9999')"
				  onblur="validateDate(this)"/>
   </td>
   <td align="right"> and </td>
   <td align="left">
     <input type="text"
	        id="lDate"
			onkeydown="formatNumber(this,'99/99/9999')"
			onblur="validateDate(this)">
   </td>
   </tr>
   </table>
<input type="button" name="load" value="Load" onclick="loadData()"></td>
<div dojoType="contentPane" id="contentPane" style="overflow: auto;width:100%;height:300;">

<div id="tablePlace">

    <table  dojoType="filteringTable"
            id="fTable"
	    cellpadding="2"
	    alternateRows="true"
            multiple="false"
	    class="dataTable">
      <thead >
      <tr>
	      <th field="clientNumber" dataType="String" align="left" valign="top">Client Number</th>
	      <th field="name" dataType="String" align="left"  valign="top">Client Name</th>
	      <th field="createDate" dataType="String" align="left"  valign="top">Changed on</th>
	      <th field="logonId" dataType="String" align="left" valign="top">Changed By</th>
              <th field="salesBranch" dataType="String" align="left" valign="top">Sales Branch</th>
	</tr>
     </thead>
	 <hr>
   <tbody>
   </tbody>
   </table>
</div>
</div>

<div id="resultPane"></div>
</form>
</body>
</html>
