<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="java.util.*"%>

<%--
Search client include file.

This is the part of the search client functionality that goes in the body of the page.
You also need to include the search client header include page.
--%>

<%
//store default search in the current session
int lastSearchType = ClientUIConstants.SEARCHBY_DEFAULT;
Integer searchType = (Integer)session.getAttribute("searchType");
if (searchType != null) {
  lastSearchType = searchType.intValue();
}

String clientSearchValue = (String) request.getAttribute("clientSearchValue");

//log("default search: "+lastSearchType);
%>

  Customer search by:<br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="23">
        <select name="clientSearchType" alt="Select a type of search in the drop down list and enter a search term to search for a client.">
          <!--
          mapped by ClientConstants within ClientUIC
          single field search items only
          --> 
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_CLIENT_NUMBER,lastSearchType)%>Customer Number
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_CLIENT_SURNAME,lastSearchType)%>Customer Surname
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_IVR_NUMBER,lastSearchType)%>IVR Number
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_DOCUMENT_REF,lastSearchType)%>Document Ref
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_PAYABLE_ITEM_ID,lastSearchType)%>Payable Item ID
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_HOME_PHONE,lastSearchType)%>Home Phone
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_GROUP_NAME,lastSearchType)%>Group Name
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_MOBILE_PHONE,lastSearchType)%>Mobile Phone
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_MEMBERSHIP_CARD_NUMBER,lastSearchType)%>Membership Card No.          
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_MEMBERSHIP_ID,lastSearchType)%>Membership Id
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_EMAIL_ADDRESS,lastSearchType)%>Email Address
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_VIN,lastSearchType)%>VIN
          <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_REGO,lastSearchType)%>Rego
        </select>
      </td>
    </tr>
    <tr id="clientSearchSimpleRow" style="display:">
      <td>
        <input type=text name="clientSearchValue" size="20" value="<%=(clientSearchValue != null ? clientSearchValue : "")%>">
      </td>
      <td>
        <%--
        Open a new advanced client search window.
        --%>
 <!--       <a class="actionItem" href="javascript:openSearchWindow('<%=ClientUIConstants.PAGE_ClientUIC%>?event=advancedSearch');"><img border="0" src="<%=CommonConstants.DIR_IMAGES%>/Search16.gif" alt="Advanced Customer Search"></a>-->
            <a class="actionItem" href="javascript:openSearchWindow('<%=ClientUIConstants.PAGE_ClientSearchUIC%>?event=openClientSearch');"><img border="0" src="<%=CommonConstants.DIR_IMAGES%>/Search16.gif" alt="Advanced Customer Search"></a>

      </td>
   </tr>
  </table>
  <input type="button" value="Search" onclick="document.forms[0].submit();">
   <%--<a onclick="document.forms[0].submit();"
      onMouseOut="MM_swapImgRestore();"
      onMouseOver="MM_displayStatusMsg('Search for a client');MM_swapImage('SearchButton','','<%=CommonConstants.DIR_IMAGES%>/SearchButton_over.gif',1);return document.MM_returnValue" >
      <img name="SearchButton" src="<%=CommonConstants.DIR_IMAGES%>/SearchButton.gif" border="0" alt="Search for a client">
   </a>
   --%>
<%--
<input type="submit" name="btnSearch" value="Search">
--%>

<script language="Javascript">
  document.all.clientSearchValue.focus();
</script>
