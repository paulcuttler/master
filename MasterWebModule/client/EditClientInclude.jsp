 <%@ taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.ract.vehicle.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="java.util.*"%>
<%@page import="javax.rmi.PortableRemoteObject"%>

<head>

<link type="text/css" rel="stylesheet" href="<%=CommonConstants.DIR_DOJO_1_4_2%>/dijit/themes/tundra/tundra.css" />

<%=CommonConstants.getRACTStylesheet()%>

<%=HTMLUtil.includeJS("/scripts/Validation.js")%>
<%=HTMLUtil.includeJS("/client/scripts/Validation.js?v=1")%>
<%=HTMLUtil.includeJS("/scripts/Utilities.js")%>

<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_1_4_2%>/dojo/dojo.js" djConfig="parseOnLoad: true, isDebug:false"></script>
 
<script type="text/javascript">
  dojo.require("dijit.form.ComboBox");
  dojo.require("dijit.form.FilteringSelect");
</script>

<%
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
ClientVO client = (ClientVO)request.getAttribute("client");
//if null then search the list in the session.
if (client == null)
{
  log("Searching for client in the list.");
  Collection clientList = (Collection)session.getAttribute("clientList");
  Integer clientNumber = (Integer)request.getAttribute("clientNumber");
  client = ClientUIHelper.getClient(clientNumber,clientList);
}
%>

<script type="text/javascript">

var SUFFIX_ESTATE = 'ESTATE';
var SUFFIX_LATE = 'LATE';
var SUFFIX = ' ESTATE OF THE LATE';
var STATUS_DECEASED = 'DECEASED';

function newWindow(url) {
  //name if required
  window.open(url, '', 'width=550,height=300,toolbar=no,status=yes,scrollbars=yes,resizable=no')
}

<%=(ClientUIHelper.selectGenderByTitle())%>

function updateDeceasedSurname()
{
  var surname = document.forms['client'].surname.value;
  var statusString = document.forms['client'].status.value;
  if (statusString == STATUS_DECEASED &&
      //not estate or late already
      (surname.toUpperCase().indexOf(SUFFIX_ESTATE) == -1) &&
      (surname.toUpperCase().indexOf(SUFFIX_LATE) == -1)
      )
  {
    document.forms['client'].surname.value = surname + SUFFIX;
  } 

  //disable marketing
  document.forms['client'].market.value = 'No';
  if (statusString != STATUS_DECEASED &&
      //estate or late already.
      (surname.toUpperCase().indexOf(SUFFIX_ESTATE) != -1) &&
      (surname.toUpperCase().indexOf(SUFFIX_LATE) != -1)
      )
  {
    //get string from start of estate to end.
    var end = surname.toUpperCase().indexOf(SUFFIX_ESTATE);
    //remove trailing blank
    if (surname.substring((end-1),end) == ' ') 
    {
      end = end - 1;
    }
    document.forms['client'].surname.value = surname.substring(0,end);
  }
}

/*
look at js that sets the event

sets the default value of the radio button if it is not already set.
*/
function setRadioValue(rdoButton)
{
  var sValue = false;

  for (var i=0; i < rdoButton.length; i++)
  {
    if (rdoButton[i].checked)
    {
      sValue = true;
    }
  }
  if (sValue == false) {
    rdoButton[0].checked = true;
  }
}

function init()
{
  setRadioValue(document.forms['client'].gender);
  document.forms['client'].title.focus();
}

</script>

</head>

<body onload="init();" class="tundra">

<s:if test="client == null && eventName == 'edit'">
  <span class="helpText">Error: No client found!</span>
</s:if>
<s:else>

  <s:form name="client" id="client" action="saveClient" onsubmit="return validateClient();" theme="simple">
  <s:token/>
  <s:hidden name="eventName"/>
  <s:hidden name="clientNumber"/>
  
<table border="0" cellspacing="0" cellpadding="0" height="100%">
<%--  <input type="hidden" name="clientType" value="<%=clientType%>"> --%>
  <tr valign="top">
    <td>
      <h2 class="contentHeading"><s:property value="pageHeading"/></h2>
        <table border="0" cellspacing="0" cellpadding="0">
	      <%
	      if (client != null)
	      {
	      %>
          <tr>
            <td class="label" nowrap>Client Number:</td>
            <td class="dataValue" nowrap> <s:property value="clientNumber"/></td>
            <td class="label" nowrap>Branch:</td>
            <td class="dataValue" nowrap><s:property value="branchName"/> </td>
            <td class="label" nowrap>Client Type:</td>
            <td class="dataValue" nowrap><s:property value="client.clientType"/></td>
          </tr>
          <%
          }
          %>
          <tr>
            <td class="label" nowrap>Title:</td>
            <td height="25" colspan="3" nowrap>
              <%--<input type="text" name="titleText" size="15" onkeydown="setTitleList(this);">--%>
              <select name="title" dojoType="dijit.form.FilteringSelect" onchange="selectGenderByTitle()">
                <%=ClientUIHelper.getClientTitles(client)%>
              </select>
              <%=HTMLUtil.mandatoryItem()%></td>
            <td nowrap>Created by:</td>
            <td class="dataValue" nowrap><%=client==null?"":client.getMadeID()==null?"":client.getMadeID()%></td>
          </tr>
          <tr>
            <td class="label" nowrap>Given names:</td>
            <td colspan="3" nowrap>
              <input type="text" name="givenNames" size="40" value="<%=client==null?"":client==null?"":client.getGivenNames()%>" onblur="upper(this);">
              <%=client==null?"":client.getInitials()==null?"":client.getInitials()%>
            </td>
            <td nowrap>Last Update:</td>
            <td class="dataValue" nowrap><%=(client==null?"":client.getLastUpdate()==null?"":client.getLastUpdate().toString())%></td>
          </tr>
          <tr>
            <td class="label" nowrap>Surname:</td>
            <td colspan="3" height="27" nowrap>
              <input type="text" name="surname" size="40" value="<%=(client==null?"":client.getSurname()==null?"":client.getSurname())%>" onblur="upper(this)">
              <%=HTMLUtil.mandatoryItem()%> </td>
            <td nowrap height="27">Created:</td>
            <td class="dataValue" nowrap><%=client==null?"":client.getMadeDate()==null?"":client.getMadeDate().toString()%></td>
          </tr>
          <tr>
            <td class="label" nowrap>Home Phone:</td>
            <td colspan="3" nowrap>
              <select dojoType="dijit.form.ComboBox" id="homePhone" name="homePhone" size="15" selectOnClick="true" value="<%=(client==null?"":client.getHomePhone()==null?"":client.getHomePhone())%>" onchange="validatePhone(document.forms['client'].homePhone,10,false,'home');" maxlength="10" <s:if test="askHomePhone">class="askField"</s:if>>
              <option></option>
              <option><%=ClientVO.FIELD_STATUS_NOT_APPLICABLE%></option>
              <option><%=ClientVO.FIELD_STATUS_REFUSED%></option>              
              <option><%=ClientVO.FIELD_STATUS_TO_FOLLOW%></option>                            
              </select>  
              <%--
              <input type="text" name="homephone" size="15" value="<%=(client==null?"":client.getHomePhone()==null?"":client.getHomePhone())%>" onBlur="validatePhone(this,10);" maxlength="10">
               --%>
            </td>
            <td nowrap>Birth Date:</td>
            <td valign="top" nowrap>
            <%-- do not pass "this" into validateBirthDate as it does not pass the object reference it passes a dijit reference which can't have its value set programmatically  --%>
              <select dojoType="dijit.form.ComboBox" id="birthDateString" name="birthDateString" selectOnClick="true" value="<%=ClientUIHelper.getBirthDateString(client)%>" onchange="validateBirthDate(document.forms['client'].birthDateString);" maxlength="10" <s:if test="askBirthDate">class="askField"</s:if>>
              <option></option>
              <option><%=ClientVO.FIELD_STATUS_REFUSED%></option>
              <option><%=ClientVO.FIELD_STATUS_TO_FOLLOW%></option>                            
              </select>
              <%--
              <input type="text" name="birthdate" size="15" value="<%=client==null?"":client.getBirthDate()==null?"":client.getBirthDate().toString()%>" onBlur="checkdate(this);validateBirthDate(this);" maxlength="10">
              --%>
              <span class="helpTextSmall">DD/MM/YYYY</span></td>
          </tr>
          <tr>
            <td class="label" nowrap>Mobile Phone:</td>
            <td colspan="3" nowrap>
              <select dojoType="dijit.form.ComboBox" id="mobilePhone" name="mobilePhone" selectOnClick="true" value="<%=(client==null?"":client.getMobilePhone()==null?"":client.getMobilePhone())%>" onchange="validatePhone(document.forms['client'].mobilePhone,10,true,'mobile');" maxlength="10" <s:if test="askMobilePhone">class="askField"</s:if>>
              <option></option>
              <option><%=ClientVO.FIELD_STATUS_NOT_APPLICABLE%></option>
              <option><%=ClientVO.FIELD_STATUS_REFUSED%></option>              
              <option><%=ClientVO.FIELD_STATUS_TO_FOLLOW%></option>                            
              </select>
              <%--
              <input type="text" name="mobilephone" size="15" value="<%=(client==null?"":client.getMobilePhone()==null?"":client.getMobilePhone())%>" onBlur="validatePhone(this,10);" maxlength="10">
               --%>
            </td>
            <td nowrap>&nbsp;</td>
            <td nowrap>&nbsp;</td>
            <td valign="top" nowrap>&nbsp; </td>
          </tr>
          <tr>
            <td class="label" valign="top" nowrap height="2">Work Phone:</td>
            <td valign="top" colspan="3" height="2" nowrap>
              <select dojoType="dijit.form.ComboBox" id="workPhone" name="workPhone" selectOnClick="true" value="<%=(client==null?"":client.getWorkPhone()==null?"":client.getWorkPhone())%>" onchange="validatePhone(document.forms['client'].workPhone,10,false,'work');" maxlength="10" <s:if test="askWorkPhone">class="askField"</s:if>>
              <option></option>
              <option><%=ClientVO.FIELD_STATUS_NOT_APPLICABLE%></option>
              <option><%=ClientVO.FIELD_STATUS_REFUSED%></option>
              <option><%=ClientVO.FIELD_STATUS_TO_FOLLOW%></option>                            
              </select>                 
              <%--
              <input type="text" name="workphone" size="15" value="<%=(client==null?"":client.getWorkPhone()==null?"":client.getWorkPhone())%>" onBlur="validatePhone(this,10);" maxlength="10">
               --%>
            </td>
            <td nowrap valign="top" height="4">Gender:</td>
            <td valign="top" nowrap>
              <%
              	Boolean clientGender = client==null?null:client.getSex();
              %>
              <input <%=HTMLUtil.getChecked(clientGender,true)%> type="radio" name="gender" value="<%=PersonVO.GENDER_MALE%>">
              Male
              <input <%=HTMLUtil.getChecked(clientGender,false)%> type="radio" name="gender" value="<%=PersonVO.GENDER_FEMALE%>">
              Female </td>
          </tr>
          <tr>
            <td class="label" valign="top" nowrap height="2">Email:</td>
            <td valign="top" colspan="3" height="2" nowrap>
              <select dojoType="dijit.form.ComboBox" id="emailAddress" name="emailAddress" selectOnClick="true" value="<%=(client==null?"":client.getEmailAddress()==null?"":client.getEmailAddress())%>" onchange="validateEmailAddress(document.forms['client'].emailAddress)" <s:if test="askEmailAddress">class="askField"</s:if>>
              <option></option>
              <option><%=ClientVO.FIELD_STATUS_NOT_APPLICABLE%></option>
              <option><%=ClientVO.FIELD_STATUS_REFUSED%></option>              
              <option><%=ClientVO.FIELD_STATUS_TO_FOLLOW%></option>                            
              </select>                    
            <%--
              <input type="text" name="email" size="25" value="<%=(client==null?"":client.getEmailAddress()==null?"":client.getEmailAddress())%>" onblur="validateEmail(this)">
              --%>
            </td>
            <td nowrap valign="top" height="2">&nbsp;</td>
            <td nowrap>&nbsp; </td>
          </tr>
          <tr>
            <td class="label" valign="top" nowrap height="2">Memo:</td>
            <td class="helpTextSmall" valign="top" colspan="3" height="2" nowrap>
              <s:hidden name="client.memoNumber"/>
              <s:property value="client.memo.memoLine"/>
            </td>
            <td nowrap valign="top" height="2"></td>
            <td nowrap></td>
          </tr>
          <tr>
            <td class="label" valign="top" nowrap height="2">Group:</td>
            <td valign="top" colspan="3" nowrap>
            <%
            	if (client != null)
                      {
                        Boolean group = client.getGroupClient();
            %>
              <%=(group!=null?(group.booleanValue()?"Yes":"No"):"")%>
              <input type="hidden" name="group" value="<%=(group!=null?(group.booleanValue()?"Yes":"No"):"")%>"/>
  <%
          }
          else
          {
            //do not allow a null response
%>
              No
              <input type="hidden" name="group" value="No"/>
<%
          }
%>
            </td>
            <td nowrap valign="top" height="2"></td>
            <td nowrap></td>
          </tr>
          <tr>
<%
          Boolean market = new Boolean(true);
          if (client != null)
          {
            market = client.getMarket();
          }
%>
            <td colspan="4">
              	<input type="hidden" name="market" value="<%=market!=null?(market.booleanValue()?"Yes":"No"):"No"%>"/>
            </td>

            <td nowrap valign="top" height="2">Status:</td>
            <td nowrap>
              <select dojoType="dijit.form.ComboBox" name="status" <%=(client != null ? "" : "disabled")%> selectOnClick="true" onChange="updateDeceasedSurname();">
                <%=ClientUIHelper.getStatus(client)%>
              </select>
              <%=HTMLUtil.mandatoryItem()%></td>
          </tr>
          <tr>
            <td class="label" valign="top" nowrap height="2">SMS?</td>
            <td valign="top" colspan="3" height="2" nowrap>
                          <%
                          	if (client != null)
                                    {
                                      Boolean smsAllowed = client.getSmsAllowed();
                          %>
              <select dojoType="dijit.form.ComboBox" id="sms" name="sms">
                <%=ClientUIHelper.getSMSSelectList(smsAllowed)%>
              </select>
              <%
          }
          else
          {
            //do not allow a null response
%>
              <input type="hidden" name="sms" value="Yes"/>
              Yes
          <%
          }
%>
            </td>
            <td nowrap valign="top" height="2">Master Client Number:</td>
            <td nowrap>
              <input type="text" size="5" name="masterClientNumber" value="<%=client==null?"":client.getMasterClientNumber()==null?"":client.getMasterClientNumber().toString()%>" onblur="document.forms['client'].resiProperty.click()"/>
            </td>
          </tr>
          <tr>
            <td colspan="7">
            <jsp:include page="/client/EditAddressInclude.jsp"></jsp:include>
            </td>
          </tr>
<%
if (client == null)
{
%>
          <tr>
            <td>Create Tramada client?</td>
            <td colspan="5"><input type="checkbox" name="createTramadaClient"/></td>
          </tr>
<%
}
%>
          <!-- Motor News edited in dedicated screeme -->
          <input type="hidden" name="motorNewsSendOption" value="<%=ClientUIHelper.getMotorNewsSendOption(client)%>">
          <input type="hidden" name="motorNewsDeliveryMethod" value="<%=ClientUIHelper.getMotorNewsDeliveryMethod(client)%>">
        </table>

      </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
      <tr>
         <td>
           <s:submit name="Save" id="saveBtn"></s:submit>
         </td>
      </tr>
      </table>
    </td>
  </tr>
</table>
  </s:form>

</s:else>

</div>

</body>

