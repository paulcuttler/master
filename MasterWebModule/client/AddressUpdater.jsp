<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>
<%@page import="javax.rmi.PortableRemoteObject"%>
<%@page import="java.rmi.RemoteException"%>
<%@page import="com.ract.common.*"%>
<%@page import="java.sql.*"%>

<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<%
	String tUserid = (String)request.getAttribute("userId");
  String slsbch = (String)request.getAttribute("slsbch");
  ChangedAddress cAddress = (ChangedAddress)request.getAttribute("changedAddress");
  if(cAddress==null) cAddress = (ChangedAddress)session.getAttribute("changedAddress");
  Integer clientNo = cAddress.getClientNo();
  DateTime tTime = new DateTime();
  ClientMgr cltMgr = ClientEJBHelper.getClientMgr();
  ClientVO thisClient = cltMgr.getClient(clientNo);

  cAddress.setSurname(thisClient.getSurname());
  String clientName = null;
  String addHeading=null;
  String address=null;
  String hpHeading = "";
  ResidentialAddressVO stAddress = null;
  PostalAddressVO poAddress = null;
  String homePhone = null;

  if(thisClient instanceof PersonVO)
  {
    PersonVO p = (PersonVO)thisClient;
    clientName = p.getSurname() + " (" + p.getGivenNames() +")";
  }
  else clientName = thisClient.getDisplayName();
  stAddress = thisClient.getResidentialAddress();
  poAddress = thisClient.getPostalAddress();
  homePhone = thisClient.getHomePhone();
  if(homePhone==null)
  {
     homePhone="";
  }
  if(!homePhone.equals(""))
  {
     hpHeading = "<b>Home Phone:</b>";
     try
     {
       homePhone = NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_HOME,homePhone);
       //if valid then format otherwise leave as is
       homePhone = StringUtil.formatPhoneNo(homePhone);
     }
     catch (Exception e)
     {
       //ignore
     }
  }

  if(stAddress.getSingleLineAddress().equals(poAddress.getSingleLineAddress()))
  {
     addHeading="<b>Address:</b>";
     address=stAddress.getSingleLineAddress();
  }
  else
  {
     addHeading="<b>Residential:<br>Postal:</b>";
     address = stAddress.getSingleLineAddress()
               + "<br>" + poAddress.getSingleLineAddress();
  }
  ResidentialAddressVO oldSA = new ResidentialAddressVO();
  PostalAddressVO oldPA = new PostalAddressVO();
  oldSA.setProperty(cAddress.getResiProperty());
  oldSA.setPropertyQualifier(cAddress.getResiStreetChar());
  oldSA.setStreetSuburbID(cAddress.getResiStsubid());
  oldPA.setProperty(cAddress.getPostProperty());
  oldPA.setPropertyQualifier(cAddress.getPostStreetChar());
  oldPA.setStreetSuburbID(cAddress.getPostStsubid());
  String oldAddress;
  String oldAddHeading;
  String oldHPHeading;
  String oldHomePhone;
  if(cAddress.getHomePhone()==null
    || cAddress.getHomePhone().equals(""))
    {
      oldHomePhone = "";
      oldHPHeading = "";
    }
    else
    {
      
      oldHomePhone = cAddress.getHomePhone();
      try
      {
        oldHomePhone = NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_HOME,oldHomePhone);
       //if valid then format otherwise leave as is        
        oldHomePhone = StringUtil.formatPhoneNo(oldHomePhone);
      }
      catch (Exception e)
      {
        //ignore
      }
      oldHPHeading = "<b>Old Phone:</b>";
    }
    if(oldSA.getSingleLineAddress().equals(oldPA.getSingleLineAddress()))
    {
      oldAddHeading = "<b>Old Address:</b>";
      oldAddress = oldSA.getSingleLineAddress();
    }
    else
    {
      oldAddHeading = "<b>Residential:<br>Postal:</b>";
      oldAddress = oldSA.getSingleLineAddress()
                  + "<br>" + oldPA.getSingleLineAddress();
    }
%>
<title>Update related addresses for <%=clientNo+""%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<!--<script type="text/javascript" src="c:/class/test_main/MasterWebModule/dojo/dojo.js"></script>-->
<script type="text/javascript" src="<%=CommonConstants.DIR_CLIENT%>/scripts/AddressUpdater.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onBlur="self.focus()">
<%@include file="/menu/PopupHeader.jsp"%>
<form name="tForm">
  <input type="hidden" name="cDate" value="<%=tTime.formatLongDate()%>"/>
  <input type="hidden" name="slsbch" value="<%=slsbch%>" />
  <input type="hidden" name="userid" value="<%=tUserid%>"/>
  <input type="hidden" name="mClientNo" value="<%=clientNo.toString()%>"/>
   <table>
     <tr>
       <td align="left" valign="top"><h3 style="{font-size:1.0em;
	                                         font-weight:bold;
				                 margin-top:0;}">Client Updated:</h3></td>
       <td align="left" valign="top"><%=thisClient.getClientNumber() +"&nbsp;"
             + clientName%></td>
       <td align="right" valign="top"><%=addHeading%></td><td align="left" valign="top"><%=address%></td>
       <td align="right" valign="top"><%=hpHeading%></td><td align="left" valign="top"><%=homePhone%></td>
     </tr>

     <tr>
       <td align="left" valign="top">&nbsp;</td>
       <td align="left" valign="top">&nbsp;</td>
       <td align="right" valign="top"><%=oldAddHeading%></td><td align="left" valign="top"><%=oldAddress%></td>
       <td align="right" valign="top"><%=oldHPHeading%></td><td align="left" valign="top"><%=oldHomePhone%></td>
     </tr>



   </table>

    <table  dojoType="filteringTable"
            id="clientTable"
            cellpadding="2"
            alternateRows="true"
            class="dataTable">
     <thead>
	<tr>
	  <th field="clientNo" dataType="Integer" align="left" valign="top">Client<br/>Number</th>
	  <th field="surname" dataType="String" align="left" valign="top" >Surname</th>
	  <th field="givenNames" dataType="String" align="left"  valign="top" >Given Names</th>
	  <th field="birthDate" dataType="String" align="left"  valign="top">Birth Date</th>
	  <th field="addressHeading" dataType="String" align="right" valign="top"></th>
	  <th field="address" dataType="html"  valign="top">Address</th>
	  <th field="homePhone" dataType="String" valign="top" align="right">Home<br>Phone</th>
          <th field="process" valign="top" align="left">
                Update<br/>
                Now&nbsp;Flag&nbsp;Never&nbsp;Dup </th>
	</tr>
     </thead>
   <tbody>
<%
	Hashtable candidates = cltMgr.getRelatedClients(cAddress);
     Enumeration en = candidates.elements();
     PersonVO person = null;
     String add=null;
     String gNames=null;
     String birthDate="";
     int row=0;
     while(en.hasMoreElements())
     {
       ClientVO clt = (ClientVO)en.nextElement();
       FollowUp fu = new FollowUp(thisClient,clt,tTime,tUserid);
       fu.setCreateSlsbch(slsbch);
       fu.setStatus("Flag");         //if you do nothing is will show up in the follow up list
       cltMgr.createFollowUp(fu);
       if(clt instanceof PersonVO)
       {
         person = (PersonVO)clt;
         birthDate = person.getBirthDate()!=null? person.getBirthDate().formatShortDate():"";
        }
       else
       {
          person=null;
          birthDate="";
       }
       gNames = clt.getGivenNames();
       if(gNames==null)gNames="";
       stAddress = clt.getResidentialAddress();
       poAddress = clt.getPostalAddress();
       homePhone = clt.getHomePhone();
       if(homePhone==null)
       {
         homePhone="";
       }
       else
       {
	      try
	      {
	        homePhone = NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_HOME,homePhone);
	       //if valid then format otherwise leave as is   
	        homePhone = StringUtil.formatPhoneNo(homePhone);
	      }
	      catch (Exception e)
	      {
	        //ignore
	      }
       }
       if(stAddress.getSingleLineAddress().equals(poAddress.getSingleLineAddress()))
       {
         addHeading="<b>Address:</b>";
         address=stAddress.getSingleLineAddress();
       }
       else
       {
         addHeading="<b>Residential:<br>Postal:</b>";
         address = stAddress.getSingleLineAddress()
                 + "<br>" + poAddress.getSingleLineAddress();
       }
%>
         <tr value="<%=row++%>"
             <%if(row%2==0)
               {%> bgcolor="#f3fdfa"<%}
               else {%>bgcolor="#ffffff" <%}%>
              >
         <td valign="top"><%=clt.getClientNumber().toString()%></td>
         <td valign="top"><%=clt.getSurname()%></td>
         <td valign="top"><%=gNames%></td>
         <td valign="top"><%=birthDate%></td>
         <td valign="top" align="right"><%=addHeading%></td >
         <td valign="top"><%=address%></td>
         <td valign="top"><%=homePhone%>
         </td>
         <td valign="top">
<!--            <select name="select">
               <option value="C">Change</option>
               <option value="D">Duplicate</option>
               <option value="F">Follow Up</option>
               <option value="N" selected="selected">No Change</option>
            </select>
            -->
            <% if(cltMgr.isPasswordProtected(clt.getClientNumber()))
            {
              %><input type="radio" name="dummy" disabled="disabled">&nbsp;&nbsp;<%
            }
            else
            {%>
            <input type="radio" name="<%="rb"+clt.getClientNumber().toString()%>" value="0" >&nbsp;&nbsp;
            <%}%>
            <input type="radio" name="<%="rb"+clt.getClientNumber().toString()%>" value="1" checked>&nbsp;&nbsp;
            <input type="radio" name="<%="rb"+clt.getClientNumber().toString()%>" value="2" >&nbsp;&nbsp;
            <input type="radio" name="<%="rb"+clt.getClientNumber().toString()%>" value="3">
         </td>
         </tr><%
     }
%>
    </tbody>
  </table>
  <table width="100%">
    <tr>
      <td align="right"><input type="button" id="goBtn" value="Save Changes" onclick="handleBtnClick(this)"/>

      </td>
    </tr>
  </table>
  <div id="bSpace" style="display:none">
      <table  dojoType="filteringTable"
              id="bisTable"
              cellpadding="2"
              alternateRows="true"
              class="dataTable">
     <thead >
	<tr>
	  <th field="bOwner" dataType="Integer" align="left" valign="top">Owner</th>
	  <th field="bType" dataType="String" align="left" valign="top" >Type</th>
	  <th field="sysNumber" dataType="String" align="left"  valign="top" >Number</th>
	  <th field="description" dataType="String" align="left"  valign="top">Description</th>
          <th field="process" valign="top" align="left">Needs updating?</th>
	</tr>
     </thead>
   <tbody>
   </tbody>
   </table>
   </div>
  <table width="100%">
    <tr>
      <td align="right">
        <div id="bisBtnDiv" style="display:none">
          <input type="button" id="businessBtn" value="Save Business" onclick="handleBusinessChanges()"/>
        </div>
      </td>
    </tr>
  </table>

</form>
<div id="logSpace"></div>
</body>
</html>
