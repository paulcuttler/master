<%@ taglib prefix="s" uri="/struts-tags" %>

<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.travel.*"%>
<%@page import="com.ract.vehicle.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<title>View Client Detail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%=CommonConstants.getRACTStylesheet()%>
<link rel="stylesheet" href="<%=CommonConstants.DIR_DOJO_1_4_2%>/dijit/themes/tundra/tundra.css" />

<script language="Javascript" src="<%=CommonConstants.DIR_DOJO_1_4_2%>/dojo/dojo.js" djConfig="parseOnLoad: true, isDebug:false"></script>

<script language="JavaScript">
   function selectClient(clientnumber)
   {
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber='+clientnumber;
      openUpdater(clientnumber);
   }

   function openUpdater(clientNumber)
   {
      var su = "<%=request.getAttribute("showAddressUpdater")+""%>";
      if(su=="true")
      { 
           var url="client/AddressUpdater.jsp?clientNo="
             + clientNumber
             + "&userId=<%=request.getAttribute("userId")%>"
             + "&slsbch=<%=request.getAttribute("slsbch")%>"
              ;
          var win = window.open(url,"_blank","height=300,width=1000,left=10,top=10,"
                                  + "resizable=yes,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no");
          win.focus();
      }
   }
   
   function openHistory(url)
   {
     var win = window.open(url,"_blank","height=600,width=800,left=10,top=10,"
                                  + "resizable=yes,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no");
     win.focus
   }
   
	function closeWindow()
	{
	  var parent = window.self;
	  parent.opener = window.self;
	  parent.close();
	}

</script>

<script type="text/javascript">
  dojo.require("dijit.layout.ContentPane");
  dojo.require("dijit.layout.TabContainer");  
</script>

</head>

<%
// Use the client VO attached to the request if possible

ClientHistory clientHistory = (ClientHistory)request.getAttribute("clientHistory");
ClientHistory prevClientHistory = (ClientHistory)request.getAttribute("prevClientHistory");


ClientVO client = null;
String pageTitle = null;
if (clientHistory == null)
{
  client = (ClientVO)request.getAttribute("client");
  pageTitle = "View Client";
}
else
{
  client = clientHistory.getClient();
  client.setClientNumber(clientHistory.getClientHistoryPK().getClientNumber());//set client number
  pageTitle = "View Client History";
}

request.setAttribute("client",client);

String phonePassword = "";
String phoneQuestion = "";
boolean isProtected=false;
// Otherwise find the client in the search list held in the session
/*
if (client == null)
{
   Collection clientList = (Collection) session.getAttribute("clientList");
   Integer clientNumber = (Integer) request.getAttribute("clientNumber");
   client = ClientUIHelper.getClient(clientNumber,clientList);
}
*/
if (client == null)
{
%>

<body onLoad="">
<h1><%=pageTitle%></h1>
   <br/>
   Client not found!
<%
}
else
{
  if(client.getPhonePassword()!=null)phonePassword=client.getPhonePassword();
  if(client.getPhoneQuestion()!=null)phoneQuestion=client.getPhoneQuestion();
  if(!phonePassword.equals(""))isProtected=true;
%>
<script language="javascript">
function editClient()
{
    var isOK = false;
    // first check to see if the client record has password protection
    if(<%=isProtected%>)
    {
       isOK = window.confirm("You have chosen to have your personal details"
                  + "\npassword protected.  So I have to ask you"
                  + "\nQUESTION: <%=phoneQuestion%> "
                  + "\nANSWER:   <%=phonePassword%>"
                  + "\nIf the correct answer was given, click OK else CANCEL");
    }
    else isOK = true;
    if(isOK)
    {
      document.forms[0].submit();
    }
    else return;


}

</script>


<s:if test="clientHistory == null">
<body onload="selectClient('<%=client.getClientNumber()%>')" class="tundra">
</s:if>
<s:else>
<body class="tundra">
</s:else>

<h2><%=pageTitle%></h2>

 <div id="mainTabContainer" dojoType="dijit.layout.TabContainer"
        style="width:80%; height: 95%; margin: 10px;">

<div id="tab1" dojoType="dijit.layout.ContentPane" 
   title="Client Details"  selected="true" closable="false">

      <table border="0" cellspacing="0" cellpadding="3" width="80%">
        <s:if test="clientHistory != null">
        <tr>
          <td class="label" nowrap>Modified:</td>
          <td class="dataValue" nowrap><s:property value="clientHistory.clientHistoryPK.modifyDateTime"/></td>
          <td class="label" nowrap>Reason:</td>
          <td class="dataValue" nowrap><s:property value="clientHistory.transactionReason"/></td>
          <td class="label" colspan="2" nowrap>Comments:</td>
          <td class="dataValue" nowrap><s:property value="clientHistory.comments"/>
          </td>
        </tr>
        <tr>
          <td class="label" nowrap>Who:</td>
          <td class="dataValue" nowrap><s:property value="clientHistory.logonId"/></td>
          <td class="label" nowrap>Where:</td>
          <td class="dataValue" nowrap><s:property value="clientHistory.salesBranchCode"/></td>
          <td class="label" colspan="2" nowrap>From:</td>
          <td class="dataValue" nowrap><s:property value="clientHistory.subSystem"/>
          </td>
        </tr>        
        </s:if>
        <tr>
          <td class="label" nowrap>Client Number:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_CLIENT_NUMBER,clientHistory,prevClientHistory)%>" nowrap> <%=(client.getClientNumber()==null?"":client.getClientNumber().toString())%></td>
          <td class="label" nowrap>Branch:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_BRANCH,clientHistory,prevClientHistory)%>" nowrap><%=ClientUIHelper.formatBranchString(client)%></td>
          <td class="label" colspan="2" nowrap>Client Type:</td>
          <td class="dataValue" nowrap><%=client.getClientType()%> <%=(client.getGroupClient()?"<span class=\"helpTextSmall\">Group Client</span>":"")%>
          </td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Title:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_TITLE,clientHistory,prevClientHistory)%>" colspan="3" nowrap><%=client.getTitle()%></td>
          <td colspan="2" nowrap>Created by:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_CREATED_BY,clientHistory,prevClientHistory)%>" nowrap><%=(client.getMadeID()==null?"":client.getMadeID())%>
          </td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Given names:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_GIVEN_NAMES,clientHistory,prevClientHistory)%>" colspan="3" nowrap><%=client.getGivenNames()%>
          <%=client.getInitials()==null?"":" ("+client.getInitials()+")"%></td>
          <td class="label" colspan="2" nowrap>Created:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_CREATE_DATE,clientHistory,prevClientHistory)%>" nowrap> <%=(client.getMadeDate()==null?"":client.getMadeDate().formatShortDate())%>
          </td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Surname:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_SURNAME,clientHistory,prevClientHistory)%>" colspan="3" nowrap><%=client.getSurname()%></td>
          <td class="label" colspan="2" nowrap>Last Update:</td>
          <td class="dataValue" nowrap><%=(client.getLastUpdate()==null?"":client.getLastUpdate().toString())%></td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Home phone:</td>
          <td class="<s:if test="askHomePhone">askField</s:if><s:else><%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_HOME_PHONE,clientHistory,prevClientHistory)%></s:else>" colspan="3" nowrap><%=(client.getHomePhone()==null?"":client.getHomePhone())%></td>
          <td class="label" colspan="2" nowrap>Birth date:</td>
          <td class="<s:if test="askBirthDate">askField</s:if><s:else><%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_BIRTH_DATE,clientHistory,prevClientHistory)%></s:else>" nowrap><%=ClientUIHelper.getBirthDateString(client)%>
            <%=ClientUIHelper.formatAge(client)%>
          </td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Mobile phone:</td>
          <td class="<s:if test="askMobilePhone">askField</s:if><s:else><%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_MOBILE_PHONE, clientHistory,prevClientHistory)%></s:else>" colspan="3" nowrap><%=(client.getMobilePhone()==null?"":client.getMobilePhone())%></td>
          <td class="label" colspan="2" nowrap>Gender:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_GENDER,clientHistory,prevClientHistory)%>" nowrap><%=(client.getGender()==null?"":client.getGender())%></td>          
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Work phone:</td>
          <td class="<s:if test="askWorkPhone">askField</s:if><s:else><%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_WORK_PHONE,clientHistory,prevClientHistory)%></s:else>" colspan="3" nowrap><%=(client.getWorkPhone()==null?"":client.getWorkPhone())%></td>
          <td class="label" colspan="2" nowrap>&nbsp;</td>
          <td class="dataValue" nowrap>&nbsp;</td>        
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Email:</td>
          <td class="<s:if test="askEmailAddress">askField</s:if><s:else><%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_EMAIL,clientHistory,prevClientHistory)%></s:else>" colspan="3" nowrap><%=(client.getEmailAddress()==null?"":client.getEmailAddress())%></td>
          <td class="label" colspan="2" nowrap>&nbsp;</td>
          <td class="dataValue" nowrap>&nbsp;</td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Memo:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_MEMO,clientHistory,prevClientHistory)%>" colspan="3"><%=ClientUIHelper.formatMemoString(client)%></td>
          <td class="label" colspan="2" nowrap>Status:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_STATUS,clientHistory,prevClientHistory)%>" nowrap><%=ClientUIHelper.formatClientStatusHTML(client.getStatus())%></td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>Group</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_GROUP,clientHistory,prevClientHistory)%>" colspan="3" nowrap><%=client.getGroupClient()?"Yes":"No"%></td>
          <td class="label" colspan="2" nowrap></td>
          <td class="dataValue" nowrap></td>
        </tr>
        <tr valign="top">
          <td class="label" nowrap>SMS:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_SMS_ALLOWED,clientHistory,prevClientHistory)%>" colspan="3" nowrap><%=client.getSmsAllowed()?"Yes":"No"%></td>
          <td class="label" colspan="2" nowrap>Master Client Number:</td>
          <td class="<%=ClientUIHelper.historyMatch(ClientVO.FIELD_NAME_MASTER_CLIENT,clientHistory,prevClientHistory)%>" nowrap><%=client.getMasterClientNumber()==null?"":client.getMasterClientNumber().toString()%></td>
        </tr>
        <tr>
          <td colspan="7">
            <s:include value="/client/ViewAddressInclude.jsp"></s:include>
          </td>
        </tr>
        <%--
        <tr>
          <td class="label">Occupation:</td>
          <td colspan="4" nowrap>
            <%
          ClientOccupationVO cvo = client.getOccupation();
          if (cvo != null) {
             out.print(cvo.getOccupation());
          }
%>
          </td>
          <td valign="top" nowrap>Drivers License:</td>
          <td valign="top" nowrap><%=(client.getDriversLicenseNumber()==null?"":client.getDriversLicenseNumber())%></td>
        </tr>
        <tr>
          <td class="label">Vehicles:</td>
          <td colspan="4" nowrap>
            <%
          Collection c = client.getVehicleList();
          if (c != null) {
            VehicleVO vehicle = null;
            Iterator it = c.iterator();
            while(it.hasNext())
            {
              vehicle = (VehicleVO) it.next();
              %>
            <%=vehicle.getDescription()%><br/>
            <%
            }
          }
%>
          </td>
          <td valign="top" nowrap>ABN</td>
          <td valign="top" nowrap><%=(client.getABN()==null?"":client.getABN().toString())%></td>
        </tr>
        --%>
        <tr valign="top">
          <td colspan="7" class="helpText"><%=ClientUIHelper.formatMotorNewsString(client)%></td>
        </tr>
        
        <tr>
          <td colspan="7"><%@include file="/travel/TramadaClientInclude.jsp"%>
          </td>
        </tr>
      </table>

      <table border="0" cellpadding="5" cellspacing="0">

          <tr>
       		<s:if test="disableClientManagement == null or !disableClientManagement">
            	<td>
            	  <s:if test="clientHistory == null">
                    <s:form action="showEditClient.action">
					  <input type="hidden" name="clientNumber" value="<%=client.getClientNumber()%>">
                      <input type="button" name="edit" value="Edit" onClick="editClient()">							
					</s:form>
				  </s:if>
				</td>
			</s:if>
            <td>
            <s:if test="clientHistory == null">            
		        <form name="viewperson" method="post" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
		          <input type="hidden" name="event" value="editClient">
		          <input type="hidden" name="clientNumber" value="<%=client.getClientNumber()%>">            
		          <input type="submit" name="Submit" value="View Client Summary" onClick="document.all.event.value='viewClientSummary';">
		        </form>            
	        </s:if> 
	        <s:if test="clientHistory != null">
	        	<button dojoType="dijit.form.Button" type="button" onclick="closeWindow()">Close</button>
         	</s:if>
            </td> 
          </tr>

      </table>

<%}%>
</body>

</div>

<s:if test="clientHistory == null">

<div id="tab2" dojoType="dijit.layout.ContentPane" 
   title="Client History" closable="false">

   <s:if test="clientHistoryList != null && clientHistoryList.size() > 0">
   <table>
     <thead>
        <tr>
          <th align="left" valign="top">Changed</th>
          <th align="left" valign="top">Reason</th>
          <th align="left" valign="top">Id</th>
          <th align="left" valign="top">Name</th>          
          <th align="left" valign="top">Address</th>          
        </tr>
      </thead>
      <tbody>

           <s:iterator value="clientHistoryList" status="cStat">
               <tr class="<s:if test="#cStat.odd == false">oddLink</s:if><s:else>evenLink</s:else>">

		               <td>
		               <s:url action="viewClientHistory" id="myUrl" escapeAmp="false">
		                 <s:param name="clientNumber" value="%{clientHistoryPK.clientNumber}"/>
		                 <s:param name="modifyDateTimeString" value="%{clientHistoryPK.longModifyDateTime}"/>	                 
		               </s:url> 
		               
		               <a href="javascript:openHistory('<s:property value="#myUrl"/>')"> <s:date name="clientHistoryPK.modifyDateTime" format="dd/MM/yyyy HH:mm:ss"/> </a>
	                 
		               </td>

  	                   <td><s:property value="transactionReason"/></td>		               
		               <td><s:property value="logonId"/></td>		               
					   <td><s:property value="client.displayName"/></td>		               
					   <td><s:property value="client.residentialAddress.singleLineAddress"/></td>					   
               </tr>
               </s:iterator>
      </tbody>
      </table>
      </s:if>


</div>

</s:if>

</div>

</html>
