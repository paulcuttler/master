<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="java.util.*"%>

<%
String addressType = (String)request.getAttribute("addressType");
Collection streetSuburbList = (Collection)request.getAttribute("streetSuburbList");
String property = (String)request.getAttribute("property");
String propertyQualifier = (String)request.getAttribute("propertyQualifier");
String mode = (String)request.getAttribute("mode");
%>

<html>
<title>Select Client Address</title>
<head>
<%=CommonConstants.getRACTStylesheet()%>

<script>

function getSelectedStreetSuburb()
{
//    alert('A')
  var radioObj = document.forms['clientAddressF'].streetSuburb;
  if(!radioObj)
  {
    return "";
  }
  var radioLength = radioObj.length;
  if(radioLength == "undefined")
  {
//    alert('1')
    if(radioObj.checked)
    {
      return radioObj.value;
    }
    else
    {
      return "";
    }
  }
  for(var i = 0; i < radioLength; i++)
  {
//    alert('2')
    if(radioObj[i].checked)
    {
      return radioObj[i].value;
    }
  }
  return "";
}

function submitForm()
{
  if(event.keyCode == 13) //enter key
  {
    selectAddress('<%=addressType%>',getSelectedStreetSuburb());
  }
}

function selectAddress(addressType, addressLine)
{
  var delimiter = "|";
//  var addressLine = getSelectedStreetSuburb();
  var elements = addressLine.split(delimiter);
//alert('addressLine='+addressLine)
  var ref = null;
//  alert('mode='+document.forms['clientAddressF'].mode.value);
if (document.forms['clientAddressF'].mode.value == 'search')
{
  ref = self.opener.document.forms['client'];
}
else
{
  ref = self.opener.top.rightFrame.document.forms['client'];
}

//  alert('ref='+ref);

  if (addressType == 'street')
  {

    ref.resiStsubId.value = elements[0];
    ref.resiStreet.value = elements[1];
    ref.resiSuburb.value = elements[2];
    ref.resiState.value = elements[3];
    ref.resiPostcode.value = elements[4];

//CH 29/11/06
//    if (ref.all.paStsubid.value == '')
//    {
      //default the postal address to be the same as street address
    if (document.forms['clientAddressF'].mode.value != 'search')
    {
	    ref.postStsubId.value = elements[0];
	    ref.postStreet.value = elements[1];
	    ref.postSuburb.value = elements[2];
	    ref.postState.value = elements[3];
	    ref.postPostcode.value = elements[4];
	
	    ref.postProperty.value = document.forms['clientAddressF'].property.value;
	    ref.postStreetChar.value = document.forms['clientAddressF'].propertyQualifier.value;
    }
//    }
  }
  else if (addressType == 'postal')
  {
    ref.postStsubId.value = elements[0];
    ref.postStreet.value = elements[1];
    ref.postSuburb.value = elements[2];
    ref.postState.value = elements[3];
    ref.postPostcode.value = elements[4];
  }

  self.close();

}

function checkResults()
{

//alert('checkResults');

var obj = document.forms['clientAddressF'].streetSuburb;

//alert(obj);
var addressType = '<%=addressType%>';

//alert('add='+addressType);

var ref = null;
//alert('mode='+document.forms['clientAddressF'].mode.value);
if (document.forms['clientAddressF'].mode.value == 'search')
{
  ref = self.opener.document.forms['clientAddressF'];
}
else
{
  ref = self.opener.top.rightFrame.document;
}
//alert('ref='+ref);
/* handle case where object is not present */
if (typeof(obj)=="undefined")
{
//alert('undefined');
  if (addressType == 'street')
  {
    ref.all.resiStsubId.value = '';
    ref.all.resiState.value = '';
    ref.all.resiPostcode.value = '';
  }
  else if (addressType == 'postal')
  {
    ref.all.postStsubId.value = '';
    ref.all.postState.value = '';
    ref.all.postPostcode.value = '';
  }
//  self.close();
  return;
}

/* handle case where object is not an array */
if (typeof(obj.length)=="undefined")
{
//  alert('obj value '+obj.value);
  selectAddress('<%=addressType%>',obj.value);
}
else if (obj.length == 1)
{
//  alert('only one');
  document.forms['clientAddressF'].streetSuburb[0].checked = true;
  selectAddress('<%=addressType%>',getSelectedStreetSuburb());
}
else
{
//  alert('more than one')
  //set the first to selected
  document.forms['clientAddressF'].streetSuburb[0].checked = true;
  document.forms['clientAddressF'].streetSuburb[0].focus();
}
}

</script>

</head>

<body onkeyup="submitForm();" onload="checkResults()">

<form method="post" name="clientAddressF" id="clientAddressF" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
  <input type="hidden" name="event" value="changeClientAddress"/>
  <input type="hidden" name="mode" value="<%=mode==null?"":mode%>"/>
  <input type="hidden" name="property" value="<%=property==null?"":property%>">
  <input type="hidden" name="propertyQualifier" value="<%=propertyQualifier==null?"":propertyQualifier%>"/>

    <table width="100%" border="0" cellspacing="1" cellpadding="1">
<%
String addressText = "";
if (streetSuburbList != null && !streetSuburbList.isEmpty())
{
%>
        <tr bgcolor="#333333">
          <td>chec</td>
          <td><font color="#FFFFFF" size="1">Street</font></td>
          <td><font color="#FFFFFF" size="1">Suburb</font></td>
          <td><font color="#FFFFFF" size="1">State</font></td>
          <td><font color="#FFFFFF" size="1">Postcode</font></td>
        </tr>

<%
  StreetSuburbVO svo = null;
  Iterator it = streetSuburbList.iterator();
  int count = 0;
//  boolean first = true;
  while (it.hasNext())
  {
    count++;
    svo = (StreetSuburbVO)it.next();
    if (svo != null)
    {
        //construct a value string
        addressText = svo.getStreetSuburbID().toString();
        addressText += "|";
        addressText += svo.getStreet();
        addressText += "|";
        addressText += svo.getSuburb();
        addressText += "|";
        addressText += svo.getState();
        addressText += "|";
        addressText += svo.getPostcode();

%>
      <tr>
        <td><input type="radio" name="streetSuburb" value="<%=addressText%>"></td>
        <td><%=svo.getStreet()%></td>
        <td><%=svo.getSuburb()%></td>
        <td><%=svo.getState()%></td>
        <td><%=svo.getPostcode()%></td>
      </tr>
<%
    }
  }
%>

<tr>
  <td colspan="5"><input type="button" value="Select Address" onclick="selectAddress('<%=addressType%>',getSelectedStreetSuburb())" accesskey="C"/></td>
</tr>

<%
}
else
{
%>
  <tr>
    <td colspan="5" class="helpText">No addresses were found.</td>
  </tr>
  <tr>
    <td colspan="5"><input type="button" value="Close" onclick="self.close();"/></td>
  </tr>
  
<%
}
%>

</table>
</form>

</html>
