<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<link href="<s:url value="/css/ract.css"/>" rel="stylesheet"
			type="text/css" />	
			<s:head/>			
	</head>
	<body>
	
	<h2>Edit Note Category</h2>
	
	<s:form>
	
	<s:hidden key="id"/>
	<s:hidden key="actionMethod" value="%{actionMethod}"/>
	 
	<s:textfield key="categoryCode" readonly="%{readOnly}"></s:textfield>
	<s:textfield key="categoryName" readonly="%{readOnly}"></s:textfield>
	<s:select key="businessType" readonly="%{readOnly}" listValue="description" listKey="typeCode" list="businessTypes"></s:select>
	<s:checkbox key="notifiable" readonly="%{readOnly}"></s:checkbox>
	<s:select key="notificationUserId" emptyOption="true"
		listValue="username" listKey="userID" list="notificationUsers"></s:select>
	<s:textfield key="notificationEmailAddress" readonly="%{readOnly}"></s:textfield>	
	<s:checkbox key="followUp" readonly="%{readOnly}"></s:checkbox>
	<s:textfield key="followUpDays" readonly="%{readOnly}"></s:textfield>
	<s:include value="/common/auditInclude.jsp"></s:include>
	<s:actionerror/>
	<tr><td><s:submit action="%{actionMethod}" key="label.ok"/></td></tr> 	
	</s:form>
	
	</body>
</html>