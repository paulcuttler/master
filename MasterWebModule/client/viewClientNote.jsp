<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet" type="text/css"/>
<s:head/>

</head>

<body>

<h3>View Client Note</h3>

<s:include value="/client/ViewClientInclude.jsp"/> 

<s:include value="/client/ViewClientNoteInclude.jsp"/>
 
<table>
<tr> 
<td>
<s:form action="viewNotes.action">
<s:hidden name="clientNumber" value="%{clientNumber}"/>
<s:submit name="viewNotes" value="View Note List"></s:submit>
</s:form>
</td>
<td>
<s:if test="noteType.commentable">
<s:form action="addNoteComment.action">
<s:hidden name="noteId" value="%{noteId}"/>
<s:submit name="addComment" value="Add Comment"></s:submit>
</s:form>
</s:if>
</td>
<td>
<s:if test="clientNote.completionDate == null && noteType.requiresCompletion && canMarkComplete">
<s:form action="markNoteComplete.action">
<s:hidden name="noteId" value="%{noteId}"/> 
<s:submit name="markComplete" value="Mark Complete"></s:submit>
</s:form>
</s:if>
</td>
<td>
<s:if test="clientNote.blockTransactions && canUnblockTransactions">
<s:form action="unblockNote.action">
<s:hidden name="noteId" value="%{noteId}"/>
<s:submit name="unblockNote" value="Allow Transactions"></s:submit>
</s:form>
</s:if>
<s:if test="clientNote.cancelDate == null">
<s:form action="markNoteCancelled.action">
<s:hidden name="noteId" value="%{noteId}"/>
<s:submit name="markCancelled" value="Cancel Note"></s:submit>
</s:form>
</s:if>
</td>
</tr>
</table>

<s:if test="noteType.followup && clientNote.completionDate == null && noteType.requiresCompletion && canMarkComplete && canReassign">
	<s:form action="reassignNote.action">
		<s:hidden name="noteId" value="%{noteId}" />
		<s:hidden name="clientNumber" value="%{clientNumber}"/>
		<s:select name="categoryCode" list="categoryList" label="Category" listKey="categoryCode" listValue="categoryName" headerKey="" headerValue="" />
		<s:submit name="reassign" value="Reassign Note" />
	</s:form>
</s:if>

<s:actionerror/>

</body>

</html>