<%@ taglib prefix="s" uri="/struts-tags" %>
 
<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet" type="text/css"/>
<s:head/>
</head>

<body>

<h3>Add Note Comment</h3>

<%@ include file="/client/ViewClientInclude.jsp"%>

<%@ include file="/client/ViewClientNoteInclude.jsp"%>

<s:form action="saveNoteComment">
<s:hidden name="noteId" value="%{noteId}"></s:hidden>
<s:textarea cols="50" rows="5" label="Comment" name="comment"></s:textarea>

<s:submit value="Save"></s:submit>
</s:form>

</body>

</html>