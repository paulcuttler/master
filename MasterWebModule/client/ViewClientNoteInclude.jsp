<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<table>

<s:label name="clientNote.noteId" label="Note Id"></s:label>
<s:label name="clientNote.businessType" label="Business Type"></s:label>
<s:label name="clientNote.noteType" label="Note Type"></s:label>
<s:label name="clientNote.noteText" label="Notes"></s:label>
<s:label name="clientNote.createId" label="Created By"></s:label>
<tr>
<td class="tdLabel"><span class="label">Created</span></td><td><s:date name="clientNote.created" format="dd/MM/yyyy"></s:date></td>
</tr>
<tr>

<s:if test="clientNote.cancelDate != null">
<s:label name="clientNote.cancelId" label="Marked Cancelled by"></s:label>
<td class="tdLabel"><span class="label">Marked Cancelled on</span></td><td><s:date name="clientNote.cancelDate" format="dd/MM/yyyy"></s:date></td>
</s:if>

</tr>
<tr><td class="tdLabel"><span class="label">Block Transactions?</span></td><td>
<s:if test="clientNote.blockTransactions">
<img src="/images/check.gif">
</s:if>
<s:else> 
<img src="/images/cross.gif">
</s:else>
</td>
</tr>
<s:if test="clientNote.unblockDate != null">
<s:label name="clientNote.unblockId" label="Unblocked by"></s:label>
<tr>
<td class="tdLabel"><span class="label">Unblocked on</span></td><td><s:date name="clientNote.unblockDate" format="dd/MM/yyyy"></s:date></td>
</tr>
</s:if>

<s:if test="clientNote.noteType == 'Followup'">
<!-- followup -->
<s:label name="clientNote.categoryCode" label="Category"></s:label>
<tr>
<td class="tdLabel"><span class="label">Due Date</span></td><td><s:date name="clientNote.due" format="dd/MM/yyyy"></s:date></td>
</tr>
<s:if test="noteCategory != null">
<s:if test="noteCategory.notificationUserId != null && noteCategory.notificationUserId != ''">
<s:label name="noteCategory.notificationUserId" label="Assignee/Notifications"></s:label>
</s:if>
<s:else>
<s:label name="noteCategory.notificationEmailAddress" label="Assignee/Notifications"></s:label>
</s:else>
</s:if>

</s:if>

<s:if test="clientNote.noteType == 'Referral'">
<!-- referral -->
<s:label name="clientNote.sourceId" label="Referrer"></s:label>
<s:if test="clientNote.targetId != null">
<s:label name="clientNote.targetId" label="Referree"></s:label>
</s:if>
<s:if test="clientNote.targetSalesBranch != null">
<s:label name="clientNote.targetSalesBranch" label="Referree Sales Branch"></s:label>
</s:if>
<tr>
<td class="tdLabel"><span class="label">Preferred Contact Date</span></td><td><s:date name="clientNote.preferredContactDate" format="dd/MM/yyyy"></s:date></td>
</tr>
<tr><td><s:label name="clientNote.preferredContactTime" label="Preferred Contact Time"></s:label></td></tr>
<tr><td><s:label name="clientNote.preferredContactNumber" label="Preferred Contact Phone"></s:label></td></tr>
</s:if>

<s:if test="clientNote.completionDate != null">
<!-- completed -->
<tr>
<td class="tdLabel"><span class="label">Completion Date</span></td><td><s:date name="clientNote.completionDate" format="dd/MM/yyyy"></s:date></td>
</tr>
<tr>
<td class="tdLabel">Completion Notes</td><td class="highlightRow"><s:property value="clientNote.completionNotes"/></td>
</tr>
<s:label name="clientNote.completionReferenceId" label="Completion Reference"></s:label>
<s:label name="clientNote.resultCode" label="Completion Result"></s:label>
<s:if test="clientNote.resultValue != null">
<s:label name="clientNote.resultValue" label="Completion Value ($)"></s:label>
</s:if> 
<s:label name="clientNote.completionId" label="Completed By"></s:label>
</s:if>

</table>
<!--  list of comments -->

<s:if test="clientNote.clientNoteComments != null && clientNote.clientNoteComments.size() > 0">
<h3>Comments</h3>
<table>
<thead>
<tr>
<th>No</th>
<th>Comment</th>
<th>Created</th>
<th>Created By</th>
</tr>
</thead>
<tbody>
<s:iterator value="clientNote.clientNoteComments" status="cStat">
<tr class="<s:if test="#cStat.odd == false">oddLink</s:if><s:else>evenLink</s:else>">
<td><s:property value="clientNoteCommentPK.commentSeq"/></td>
<td><s:property value="comment"/></td>
<td><s:date name="created" format="dd/MM/yyyy"/></td>
<td><s:property value="createId"/></td>     
</tr>
</s:iterator> 
</tbody>

</table>
</s:if>
