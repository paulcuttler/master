	    dojo.require("dojo.io.*");
	    dojo.require("dojo.event.*");
            dojo.require("dojo.widget.*");
            dojo.require("dojo.widget.FilteringTable");
var serverURL="/AddressUpdateUIC";
var logonId;
function loadData()
{
	  var forUserId = dojo.byId('forUserId').value;
	  var slsbch = dojo.byId('salesbranch').value;
	  var status = dojo.widget.byId('status').getValue();
	  var fDate = dojo.byId('fDate').value;
	  var tDate = dojo.byId('lDate').value;
	  var result = dojo.widget.byId('result').getValue();
	getData(forUserId,slsbch,status,fDate,tDate,result);
}
// formats an input field using a specified format string
// used for date format fields
function formatNumber(df,formatStr)
{
   var value = df.value;
   var valueLen = value.length;
   var fChar = formatStr.charAt(valueLen);
   var kc=event.keyCode;
   if(kc>=96)kc=kc-48;
   var keyVal = String.fromCharCode(kc);
   if(kc==8)df.value=df.value.substring(0,valueLen-1);
   else if(kc>=48 && kc<=57)
   {
      if(fChar!=9)
	  {
	    df.value = df.value + fChar;
	  }
   }
   else if(kc!=9 && kc!= 13)event.returnValue=false;
   if(valueLen>=formatStr.length
      && kc>=48 && kc <=57)
   {
      df.value = df.value.substring(0,valueLen-1);
   }
}

function validateDate(thing)
{
   var dString = thing.value;
   var ddArray = dString.split("/");
   var day = Number(ddArray[0]);
   var month = Number(ddArray[1]);
   var year = Number(ddArray[2]);
   var e;
   try
   {
     if(month > 12)
	 {
	   e="Invalid Month";
	   throw e;
     }
	 if(day>31
	   ||((month==3 || month==6 || month==9 || month==11)&& day == 31)
	   ||(month==2 && year%4!=0 && day>28)
	   ||(month==2 && year%1000==0 && day > 28))
	 {
	   e = "Invalid day";
	   throw e;
	 }
	 if(year<1000)
	 {
	   e="Invalid year\nEnter four digits";
	   throw e;
	 }
       var dDate = new Date(Number(ddArray[2]),Number(ddArray[1])-1,Number(ddArray[0]));
   }
   catch(e)
   {
      alert(e+"");
	  thing.focus();
	  thing.selected=true;
   }
}

function saveChanges()
{
   var jDataArray = "[";
   var businessT = document.getElementById('fTable');
   var bRow;
   for(var xx =1;xx<businessT.rows.length;xx++)
   {
      bRow = businessT.rows[xx];
	  var rString = bRow.cells[7];
 	  if(rString.innerHTML!="CHANGED" && rString.innerHTML!="NO CHANGE"
	     && rString.innerHTML!="")
	  {

	    var rValue = getButtonGroupValue(bRow);
        if (rValue!="2")  //don't bother saving if it hasn't been processed
		{
			if(jDataArray!="[")jDataArray +=",";
			jDataArray += "{sType:" + bRow.cells[1].innerHTML
					   + ",sysNumber:" + bRow.cells[2].innerHTML
					   + ",createDate:\"" + bRow.cells[4].innerHTML + "\""
                                          + ",createId:" + bRow.cells[5].innerHTML
					   + ",cClient:" + bRow.cells[6].innerHTML
					   + ",result:" + rValue + "}";
		}
	  }
   }
   jDataArray += "]";  //we now have an array of client numbers and process selections in JSON object format
   saveData(logonId,jDataArray);
}

function getButtonGroupValue(br)
  {
	var buttonValue ;
	var hStr =br.cells[7].innerHTML;
    var sta = hStr.split(">");
	var st;
    for(var i=0;i<3;i++)
	{
	  st = sta[i];
	  if(st.indexOf("CHECKED")>-1) return i;
    }
    return null;
 }


function getData(userid,salesBranch,status,fDate,lDate,result)
{
    dojo.io.bind({
                  url: serverURL,
                  content:{event:"loadFollowUp",
		            userid:userid,
                           salesBranch:salesBranch,
                           status:status,
                           fDate:fDate,
                           lDate:lDate,
			   result:result},
                   load:showBusiness,
                   //error:handleError,
                   //timeout: handleTimeout,
                   mimetype: "application/json"
                 });
}

function saveData(userid,data)
{
   dojo.io.bind({url:serverURL,
                 content:{event:"saveFollowUp",
                          userId:userid,
                          data:data},
                 load:showResponse});
}

// clear table and settings
function showResponse(type,data,evt)
{
   bTable=dojo.widget.byId('fTable');
   bTable.store.setData("");
   clearDetails();
}

function showBusiness(type,data,evt) {
    var e;
    clearDetails();
    try {
        bTable=dojo.widget.byId('fTable');
        bTable.store.setData(data);
    }
	catch (e) {
       var errstr=e.name+": "+e.message;
       doLog(errstr,"error");
    }
    var businessT = document.getElementById('fTable');
    if(businessT.rows.length==1) //header row only
    {
	alert("No follow up entries matching these criteria.");
    }
}

function getDetails()
{
  var bTable = dojo.widget.byId('fTable');
  var sData = bTable.getSelectedData();
  if(sData!=null && sData!="undefined")
  {
     dojo.io.bind({url:serverURL,
                   content:{event:"getFollowUpDetail",
	                        type:sData.sType,
			                sysNumber:sData.sNumber,
						    sClientNo:sData.cClientNo,
				            cDate:sData.createDate},
			       load:showDetails})
  }
}
function showDetails(type,data,evt)
{
  var rp = dojo.byId("resultPane");
  rp.innerHTML=data;
}
function clearDetails()
{
   var rp = dojo.byId("resultPane");
   rp.innerHTML="";
}

function init()
{
   logonId=dojo.byId('logonId').value;
   var userinp = dojo.byId("forUserId");
   userinp.value=logonId;
}
function print()
{  //print report using current data
      var forUserId = dojo.byId('forUserId').value;
      var slsbch = dojo.byId('salesbranch').value;
      var status = dojo.widget.byId('status').getValue();
      var fDate = dojo.byId('fDate').value;
      var tDate = dojo.byId('lDate').value;
      var result = dojo.widget.byId('result').getValue();

      dojo.io.bind({
                  url: serverURL,
                  content:{event:"printFollowUp",
		            userid:forUserId,
                           salesBranch:slsbch,
                           status:status,
                           fDate:fDate,
                           lDate:tDate,
			   result:result} //,
                   //load:showBusiness,
                  // mimetype: "application/json"
                 });
}

dojo.addOnLoad(init);
