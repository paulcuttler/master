/*
validate client entry
 */
function validateClient(frm) {

	var clientNumber = document.forms['client'].clientNumber.value;

	//alert('test ['+document.forms['client'].title.value+"]");
	if (document.forms['client'].title.value == '') {
		alert("You must enter a client title.");
		//document.forms['client'].title.focus();
		return false;
	} else if (document.forms['client'].givenNames.value == '') {
		alert("You must enter a given name.");
		document.forms['client'].givenNames.focus();
		return false;
	} else if (document.forms['client'].surname.value == '') {
		alert("You must enter a surname.");
		document.forms['client'].surname.focus();
		return false;
	} else if (document.forms['client'].resiStsubId.value == '') {
		alert("You must enter a residential address.");
		return false;
	} else if (document.forms['client'].postStsubId.value == '') {
		alert("You must enter a postal address.");
		return false;
	} else if (document.forms['client'].sms.value == '') {
		alert("You must specify an SMS preference.");
		return false;
	}
	// birth date, home phone, work phone, mobile phone and email address are
	// mandatory
	if (!validateBirthDate(document.forms['client'].birthDateString)) {
//		alert('birthdate')
		return false;
	}
	if (!validatePhone(document.forms['client'].homePhone, 10, false, 'home')) {
//		alert('home')
		return false;
	}
	if (!validatePhone(document.forms['client'].mobilePhone, 10, true, 'mobile')) {
//		alert('mobile')
		return false;
	}
	if (!validatePhone(document.forms['client'].workPhone, 10, false, 'work')) {
//		alert('workPhone')	
		return false;
	}
	if (!validateEmailAddress(document.forms['client'].emailAddress)) {
//		alert('emailAddress')
		return false;
	}

	var status = document.forms['client'].status;
	var statusString = status.value.toUpperCase();// upper

	var masterClientNumber = document.forms['client'].masterClientNumber.value;

	if (statusString == 'DUPLICATE'
			&& (masterClientNumber == '' || masterClientNumber == '0')) {
		alert('Master client number may not be empty if the status is duplicate.');
		return false;
	} else if (statusString != 'DUPLICATE'
			&& (masterClientNumber != '' && masterClientNumber != '0')) {
		alert('Master client number is entered but the status is not duplicate.');
		return false;
	}

	if (clientNumber != null && clientNumber != ''
			&& clientNumber == masterClientNumber) {
		alert('Master client number must not be the same as the client number.');
		return false;
	}

	document.getElementById('saveBtn').disabled = true;
	
	return true;
}

function validateResidentialProperty(val) {
	if (val != '') {
		var rExp = /(^PO|^XO|^PO.|^P.O|BOX|^POST|OFFICE)/i
		var found = val.search(rExp);
		if (found != -1) {
			alert('Residential address should not contain Post Office details.');
			return false;
		}
	}
	return true;
}

// check phone numbers
function validatePhone(obj, phoneLength, mobile, type) {
	var val = obj.value;
	var formatType = val.substring(0,1).toUpperCase()+val.substring(1,val.length);
	// val = stripWhitespace(val);
//	 alert("val="+val);
	var error = false;
	if (val != null && // eg. na
			val.length > 0) {
		// 0400mytest
		if (isInteger(val)) // numeric
		{
			if (val.length != phoneLength || // not correct length
					val.substring(0, 1) != '0') {
				alert(formatType+' phone number must have ' + phoneLength + ' numeric characters and start with 0.');
				obj.focus();
				obj.select();
				return false;
			}
//			alert(val.substring(1, 2));
//			alert(mobile);
			
			if (mobile && val.substring(1, 2) != '4') {
				alert('Mobile phone number must start with 04.');
				obj.focus();
				obj.select();				
				return false;
			}
		} else // non-numeric
		{
			if (val != 'Not Applicable' && val != 'Refused'
					&& val != 'To Follow') {
				alert(formatType+' phone number must only have a non-numeric value of Not Applicable, Refused or To Follow.');
				obj.focus();
				obj.select();				
				return false;
			}
		}

	} else {
		alert('Enter a valid '+type+' phone number or select from the list.');
		obj.focus();
		obj.select();		
		return false;
	}
//	alert('end')

	return true;

}

// validate the birth date
function validateBirthDate(obj) {
	var stringDate = obj.value;
	if (stringDate != null && stringDate.length > 0) {
        var valid = chkdate(obj);
		if (valid == false) {
			if (stringDate != 'Refused' && stringDate != 'To Follow') {
				alert('Birth date can only be a valid date or a value of Refused or To Follow.');
				obj.focus();
				obj.select();
				return false;
			}		
		} else {		
				// should be in form dd/MM/yyyy
			var day = parseInt(stringDate.substring(0, 2));
			var month = parseInt(stringDate.substring(3, 5)) - 1;
			var year = parseInt(stringDate.substring(6, 10));
			// alert(''+day);
			// alert(''+month);
			// alert(''+year);
			var birthDate = new Date(year, month, day);
			var pastDate = new Date(1900, 01, 01);
			
			var today = new Date();
//			alert(today);
//			alert(birthDate);
			if (birthDate >= today) {
				alert('Birth date must not be after today.');
				obj.focus();
				obj.select();				
				return false;
			}
			if (birthDate < pastDate) {
				alert('Birth date must not be before 1900.');
				obj.focus();
				obj.select();				
				return false;
			}
		}
 
	} else {
		alert('Enter a valid birth date or select from the list');
		obj.focus();
		obj.select();	
		return false;
	}
	return true;	
}

function validateEmailAddress(obj) {
	var val = obj.value;
	if (val != null && // eg. na
			val.length > 0) {
		return validateEmail(obj)
	} else {
		alert('Enter a valid email address or select from the list');
		obj.focus();
		obj.select();		
		return false;
	}
	return true;
}