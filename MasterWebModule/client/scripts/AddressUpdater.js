dojo.require("dojo.widget.FilteringTable");
dojo.require("dojo.io.*");
var bTable;  //business table
var serverURL;
var cDate;
var slsbch;
var userid;
var mClientNo;

function init()
{
   serverURL="/AddressUpdateUIC";
   cDate = dojo.byId("cDate").value;
   slsbch = dojo.byId("slsbch").value;
   userid = dojo.byId("userid").value;
   mClientNo = dojo.byId('mClientNo').value;
   self.focus();

}
function handleBtnClick(btn)
{
   var cltTable = document.getElementById('clientTable');
   var jDataArray = "[";
   for(var xx = 1;xx<cltTable.rows.length;xx++)
   {
     var aRow = cltTable.rows[xx];
     var clientNo = aRow.cells[0].innerHTML;
     if(jDataArray!="[")jDataArray +=",";
     jDataArray += "{clientNo:" + clientNo
                  + ",process:" + getButtonGroupValue(clientNo) + "}";
   }
   jDataArray += "]";  //we now have an array of client numbers and process selections in JSON object format

   sendData("updateAddresses",jDataArray,mClientNo);
   btn.disabled=true;
 }
function getButtonGroupValue(clientNo)
{
    var buttonGroupName = "rb" + clientNo;
    var buttonGroup = document.tForm[buttonGroupName];
    var buttonValue ;
    for(var i=0;i<buttonGroup.length;i++)
    {
	if(buttonGroup[i].checked)
	{
	   return buttonGroup[i].value;
	}
    }
   return null;
 }


function sendData(eventName,jData,clNo)
{
    dojo.io.bind({
                 url:serverURL,
		  content:{event:eventName,
		           val:jData,
                           clientNo:clNo,
                           createDate:cDate,
                           userid:userid,
                           slsbch:slsbch},
                  load:returnHandler,
                  error:handleError,
                  timeout: handleTimeout,
                  mimetype: "application/json"
                 });
}
function returnHandler(type,data,evt) {
   showBusiness(data);
   document.getElementById('bSpace').style.display='';
   document.getElementById('bisBtnDiv').style.display='';

}
function handleError(type,data,evt)
{
    alert("an error has occurred: " + data + "  " + type);
}
function handleTimeout()
{
    alert("timed out...");
}

function handleBusinessChanges()
{
   var jDataArray = "[";
   var businessT = document.getElementById('bisTable');
   var bRow;
   for(var xx =1;xx<businessT.rows.length;xx++)
   {
      bRow = businessT.rows[xx];
      if(jDataArray!="[")jDataArray +=",";
	 jDataArray += "{bType:" + bRow.cells[1].innerHTML
                     + ",sysNumber:" + bRow.cells[2].innerHTML
                     + ",process:" + bRow.cells[4].firstChild.checked + "}";
   }
   jDataArray += "]";  //we now have an array of client numbers and process selections in JSON object format
   sendData("flagBusinessAddressChange",jDataArray,mClientNo);
   closeWindow();
}

function closeWindow()
{
  var parent = window.self;
  parent.opener = window.self;
  parent.close();
}


function showBusiness(data)
{
    var e;
    try
    {
        bTable=dojo.widget.byId('bisTable');
        bTable.store.setData(data);
    }
    catch (e)
    {
       var errstr=e.name+": "+e.message;
       doLog(errstr,"error");
    }
    var businessT = document.getElementById('bisTable');
    if(businessT.rows.length==1) //header row only
    {
	alert("No related business.  \nClosing window.");
	closeWindow();
    }

    for(var xx =1;xx<businessT.rows.length;xx++)
    {
      bRow = businessT.rows[xx];
      var cell = bRow.insertCell(4);
      var process = document.createElement('input');
      process.type='checkbox';
      process.name = 'process';
      cell.appendChild(process);
      process.checked=true;
    }
}

dojo.addOnLoad(init);
