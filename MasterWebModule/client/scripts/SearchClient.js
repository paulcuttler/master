
/*
Search client header include file.

This file contains the code that needs to go in the header part of the html file.
You also need to include the search client include file in the body.
*/

function checkLength(obj) {
  if (obj.value.length == 0) {
    alert("You must enter a search term.");
    obj.focus();
    return false;
  }
  return true;
}

function openSearchWindow(url) {
  //name if required
  window.open(url, '', 'width=800,height=500,toolbar=no,status=yes,scrollbars=yes,resizable=yes')
}

