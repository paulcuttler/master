<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.payment.PaymentException"%>
<%@page import="com.ract.vehicle.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.payment.ui.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<%
   ClientVO clientVO = (ClientVO) request.getAttribute("client");

   boolean isMember = false;

   MembershipVO  mem     = MembershipEJBHelper.getMembershipMgr().findMembershipByClientAndType(clientVO.getClientNumber(),MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
   
   DateTime memExpiryDate  = null;
   String memStatus        = null;
   String subscriberType   = "Not a Member";
   boolean isCurrent       = false;

   if ( mem != null )
   {
     isMember          = true;
     memExpiryDate     = mem.getExpiryDate();
     isCurrent         = memExpiryDate.afterDay(new Date());
     memStatus         = mem.getStatus();
   }

   boolean mailOption=false;
   mailOption = Client.MOTOR_NEWS_DELIVERY_METHOD_POSTAL.equalsIgnoreCase(clientVO.getMotorNewsDeliveryMethod());

   int motorNewsOption=0;
   String motorNewsSendOption = clientVO.getMotorNewsSendOption();

   if ( StringUtil.isBlank(motorNewsSendOption) )
   {
      motorNewsSendOption  = Client.MOTOR_NEWS_NO_SEND;
   }

   if (Client.MOTOR_NEWS_MEMBERSHIP_SEND.equalsIgnoreCase(motorNewsSendOption))
   {
       subscriberType      = "Normal Subscriber";
       motorNewsOption     =  1;
   }
   else if(Client.MOTOR_NEWS_SEND_INDIVIDUAL.equalsIgnoreCase(motorNewsSendOption))
   {
       subscriberType      = "Normal Subscriber - Individual Copy";
       motorNewsOption     =  2;
   }
   else if(Client.MOTOR_NEWS_NO_SEND.equalsIgnoreCase(motorNewsSendOption))
   {
       subscriberType      = "Not a Subscriber";
       motorNewsOption     =  3;
   }
   else if(Client.MOTOR_NEWS_SEND.equalsIgnoreCase(motorNewsSendOption))
   {
       subscriberType      = "Free Subscriber";
       motorNewsOption     = 4;
   }


   boolean isEditor =   (  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_MOTOR_NEWS_EDIT)
                      &&  !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_MOTOR_NEWS)
                      &&  !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_MOTOR_NEWS_ADMIN)
                        );

   boolean isAdmin  =   ( userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_MOTOR_NEWS) || userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_MOTOR_NEWS_ADMIN));
LogUtil.debug(this.getClass(),"isEditor="+isEditor);
LogUtil.debug(this.getClass(),"isAdmin="+isAdmin);
//   String mdtDisabled = (isAdmin) ? "" : "Disabled";  Change request 76
//   String mnoDisabled =  (isAdmin || (isCurrent && isEditor)) ? "" : "Disabled";
   String mnoDisabled = ""; //JH all users can access this now.
   String mdtDisabled = "Disabled"; //RLG Change request 76
%>

<html>
<head>
<title>Journeys Magazine Edit</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<%=HTMLUtil.includeJS( CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js")%>
<%=HTMLUtil.includeJS( CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js")%>

</head>

<body>
<form name="mainForm" method="post" action="/saveMotorNews.action">

<input type="hidden" name="clientNumber" value="<%=clientVO.getClientNumber()%>">
<input type="hidden" name="member" value="<%=new Boolean(isMember).toString()%>">
<input type="hidden" name="currentMember" value="<%=new Boolean(isCurrent).toString()%>">
<input type="hidden" name="mno" value="<%=motorNewsOption%>">
<input type="hidden" name="mnd" value="<%=new Boolean(mailOption).toString()%>">

<div id="mainpage" class="pg">
 <h2>Journeys Magazine Subscription</h2>
 
   <!--client details-->
   <jsp:include page="/client/ViewClientInclude.jsp"></jsp:include>

   <table border="0">
       <tr>
         <td height="20" colspan="2"></td>
       </tr>
      <%
         if ( memStatus !=null )
         {
      %>
        <tr>
         <td class="label" nowrap>Membership Expiry:</td>
         <td class="dataValue" nowrap><%=memExpiryDate%> <%=MembershipUIHelper.formatMembershipStatusHTML(memStatus)%></td>
       </tr>
       <tr>
         <td height="20" colspan="2"></td>
       </tr>
      <%
         }
      %>
       <tr>
          <td class="label">Journeys Magazine Status:</td>
          <td class="dataValueBlue"><%=subscriberType%></td>
       </tr>

       <tr>
         <td height="20" colspan="2"></td>
       </tr>

      <tr>
        <td class="label" nowrap>Journeys Magazine Option:</td>
        <td  >
           <select name="motorNewsSendOption" <%=mnoDisabled%>>
              <option value="<%=Client.MOTOR_NEWS_MEMBERSHIP_SEND%>"
                   <%=motorNewsOption==1?"SELECTED":"" %>>Send if membership current</option>
              <option value="<%=Client.MOTOR_NEWS_SEND_INDIVIDUAL%>"
                   <%=motorNewsOption==2?"SELECTED":"" %>>Send individual copy if membership current</option>
              <option value="<%=Client.MOTOR_NEWS_NO_SEND%>"
                   <%=motorNewsOption==3?"SELECTED":"" %>>Do not send</option>
               <option value="<%=Client.MOTOR_NEWS_SEND%>"
                   <%=motorNewsOption==4?"SELECTED":"" %>>Always send</option>
            </select>
         </td>
      </tr>
       <tr>
         <td height="20" colspan="2"></td>
       </tr>

      <tr valign="top" align="left">
         <td class="label" nowrap >Delivery Method:</td>
         <td >
           <select name="motorNewsDeliveryMethod" <%=mdtDisabled%> >
            <option value="<%=Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL%>"
                  <%=!mailOption?"SELECTED":""%>>Normal delivery</option>
            <option value="<%=Client.MOTOR_NEWS_DELIVERY_METHOD_POSTAL%>"
                 <%=mailOption?"SELECTED":""%>>Deliver by post</option>
          </select>
         </td>
       </tr>
       <tr>
         <td height="20" colspan="2"></td>
       </tr>

      <tr>
         <td colspan="2">
            <div id="MotorNewsExplanation" style="font-size: 11px; width:95%; overflow:auto; border-width: 1; border-style: solid;">
                If the subscriber has a current membership and the Journeys Magazine Option is <b>Send if membership current</b> then
                one copy of Journeys Magazine will be sent to a <b>household</b>.
                <br/><br/>
                A <b>household</b> is two or more Roadside members with the same surname living at the same address
                <br/><br/>
                If the subscriber has a current membership and wants to receive a copy of Journeys Magazine,
                use the <b>Send individual copy if membership current</b> option.
               <br/><br/>
               If the subscriber has a current membership and does not want to receive a copy of Journeys Magazine,
               use the <b>Do not send</b> option.
           </div>
         </td>

      </tr>
   </table>
   </div>
   <div id="buttonPage" class="btn">
     <input type="submit" value="Save">
   </div>
</div>
</form>
</body>
</html>
