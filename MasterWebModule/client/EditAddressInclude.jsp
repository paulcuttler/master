<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.qas.ui.*"%>
<%@page import="java.lang.*"%>
<%--
Edit Address include.

Check if the address type is null.
If it is not then display the address line.
--%>

<%
boolean displayPostalAddress = ((Boolean)request.getAttribute("displayPostalAddress")).booleanValue();
boolean displayResidentialAddress = ((Boolean)request.getAttribute("displayResidentialAddress")).booleanValue();
%>

<script language="Javascript">

function checkQASAddress (addressType,addressesMatch,street,suburb,property,propertyQualifier)
{
//	Check the Suburb to see if it starts with <QAS>. If not it has been typed in
//	call openSelectAddress

	if ( suburb.indexOf("<QAS>") >= 0 )
        {
          decodeQASAddress (addressType,addressesMatch,street,suburb,property,propertyQualifier);
        }
        else
        {
          openSelectAddress(addressType,street,suburb,property,propertyQualifier);
        }
}

function decodeQASAddress (addressType,addressesMatch,street,suburb,property,propertyQualifier)
{

  var url='<%=QASUIConstants.PAGE_QASUIC%>';
  url += '?event=processQASAddress';
  url += '&street=' + escape(street);
  url += '&qasSuburb=' + escape(suburb);
  url += '&property=' + escape(property);
  url += '&propertyQualifier=' + escape(propertyQualifier);

  var http_request = getHttpRequest();

  if (!http_request)
  {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
  }

  http_request.onreadystatechange = function() {processQASreply(http_request, addressType, addressesMatch); };
  transmitRequest(http_request,url);
}

function processQASreply ( http_request, addressType, addressesMatch )
{
   if (http_request.readyState == 4)
   {

       if (http_request.status == 200)
       {
         var doc = http_request.responseXML;

         suburb            = getElementValue ( doc,"suburb" );
         property          = getElementValue ( doc,"property");
         propertyQualifier = getElementValue (doc, "propertyQualifier");
         street            = getElementValue (doc,"street");
         stsubid           = getElementValue (doc,"stsubid");
         dpid              = getElementValue (doc,"dpid");
         postcode          = getElementValue (doc,"postcode");
         state             = getElementValue (doc,"state");

         if (addressType == "street" )
         {
    		document.all.resiStsubId.value  		= stsubid;
    		document.all.resiStreet.value   		= street;
    		document.all.resiSuburb.value   		= suburb;
    		document.all.resiState.value    		= state;
    		document.all.resiPostcode.value  		= postcode;
		document.all.resiDpid.value     		= dpid;
		document.all.showResiDpid.value           = dpid;
 		document.all.resiProperty.value		= property;
 		document.all.resiStreetChar.value	= propertyQualifier;

                if ( addressesMatch == "true" )
                {
                	document.all.postStsubId.value  		= stsubid;
    			document.all.postStreet.value   		= street;
    			document.all.postSuburb.value   		= suburb;
    			document.all.postState.value    		= state;
    			document.all.postPostcode.value  		= postcode;
			document.all.postDpid.value     		= dpid;
			document.all.showPostDpid.value           = dpid;
 			document.all.postProperty.value		= property;
 			document.all.postStreetChar.value	= propertyQualifier;
                }

                document.all.postProperty.click();
         }

         if ( addressType == "postal" )
         {
    		document.all.postStsubId.value  		= stsubid;
    		document.all.postStreet.value   		= street;
    		document.all.postSuburb.value   		= suburb;
    		document.all.postState.value    		= state;
    		document.all.postPostcode.value  		= postcode;
		document.all.postDpid.value     		= dpid;
		document.all.showPostDpid.value           = dpid;
 		document.all.postProperty.value		= property;
 		document.all.postStreetChar.value	= propertyQualifier;
         }
       }
       else
       {
           alert('There was a problem with the request. Status=' + http_request.status + ' readyState=' + http_request.readyState + " response=" + http_request.responseText);
       }
   }

}
function getElementValue ( doc, elementName )
{
  var xx = doc.getElementsByTagName(elementName);
  var elementValue = "";
  try
  {
     elementValue = xx[0].firstChild.nodeValue;
  }
  catch ( e )
  {
     elementValue = "";
  }

   return elementValue.toUpperCase();
}

function transmitRequest ( http_request, url )
{
  http_request.open('GET', url, false);
  http_request.send(null);
}


//edit address window
function openSelectAddress(addressType,street,suburb,property,propertyQualifier)
{
    if (suburb == '')
    {
      alert('A suburb must be entered.');
      return;
    }

    var UIC='<%=ClientUIConstants.PAGE_ClientUIC%>';
    var url = UIC + '?event=selectClientAddress';
    url += '&addressType=' + addressType;
    url += '&street=' + street;
    url += '&suburb=' + suburb;
    url += '&property=' + property;
    url += '&propertyQualifier=' + propertyQualifier;
    <%
    if (!displayPostalAddress)
    {
    %>
      url += '&mode=search'
    <%
    }
    %>
    openAddressWindow(url);
}

//open an address window
function openAddressWindow(url)
{
    window.open(url, '', 'width=550,height=300,toolbar=no,status=yes,scrollbars=yes,resizable=no');
}

function clearAddressUnknownStatus()
{
  try { // 'status' field may not be present.
	  var status = document.all.status;
	  var currentStatus = status.value;
	//  alert('currentStatus='+currentStatus);
	  if (currentStatus == 'Address Unknown')
	  {
	//  alert('1');  
		for (var x = 0; x < status.length; x++)
		{
	//  alert('status='+status[x].value);	
		  if (status[x].value == 'Active')
		  {
	//  alert('2');	  
		    status.selectedIndex = x;
		  }
		}
	  } 
  } catch (e) {}
}

function resetPostalStsubid()
{
  document.all.postStsubId.value=''
  document.all.postState.value=''
  document.all.postPostcode.value=''
  resetPostalDpid()
  clearAddressUnknownStatus();
}

function resetResidentialStsubid()
{
  document.all.resiStsubId.value=''
  document.all.resiState.value=''
  document.all.resiPostcode.value=''
  resetResidentialDpid()
  clearAddressUnknownStatus();  
}

function resetResidentialDpid ()
{
  document.all.resiDpid.value=''
  document.all.showResiDpid.value=''
}
function resetPostalDpid ()
{
  document.all.postDpid.value=''
  document.all.showPostDpid.value=''
}
function popUpQASResidential ()
{
   popUpQAS( "CustomerResidentialAuto")
}

function popUpQASPostal ()
{
  popUpQAS( "CustomerPostalAuto");
}

function popUpQAS ( layout )
{


  var UIC='<%=QASUIConstants.PAGE_QASUIC%>';
  var url = UIC + '?event=notifyQAS';
  url += '&layout=' + escape(layout);

  var http_request = getHttpRequest();

  if (!http_request)
  {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
  }

  http_request.onreadystatechange = function() { popupQASreply(http_request); };

  http_request.open('GET', url, false);
  http_request.send(null);

}

function popupQASreply( http_request )
{

   var doc = null;
   if (http_request.readyState == 4)
   {

       if (http_request.status == 200)
       {
         doc = http_request.responseText;
       }
       else
       {
           alert('There was a problem with the request. Status=' + http_request.status + ' readyState=' + http_request.readyState + " response=" + http_request.responseText);
           doc='Error ' + + http_request.status;
       }
   }
}


function getHttpRequest ( )
{
  var http_request = false;

  if (window.XMLHttpRequest) // Mozilla, Safari, ...
  {
 	http_request = new XMLHttpRequest();
  }
  else if (window.ActiveXObject) // IE
  {
       	http_request = new ActiveXObject("Microsoft.XMLHTTP");
  }

  if (!http_request)
  {
       alert('Giving up :( Cannot create an XMLHTTP instance');
  }

  return http_request;

}
</script>

<%
	AddressVO streetAddress = (AddressVO) request
			.getAttribute("streetAddress");

	String resiProperty = streetAddress == null ? "" : streetAddress
			.getProperty() == null ? "" : streetAddress.getProperty();
	String resiStreetChar = streetAddress == null ? "" : streetAddress
			.getPropertyQualifier() == null ? "" : streetAddress
			.getPropertyQualifier();
	String resiStreet = streetAddress == null ? ""
			: streetAddress.getStreet() == null ? "" : streetAddress.getStreet();
	String resiSuburb = streetAddress == null ? ""
			: streetAddress.getSuburb() == null ? "" : streetAddress.getSuburb();
	String saPostalcode = streetAddress == null ? "" : streetAddress
			.getPostcode() == null ? "" : streetAddress.getPostcode();
	String resiState = streetAddress == null ? ""
			: streetAddress.getState() == null ? "" : streetAddress.getState();
	String resiStsubIdString = streetAddress == null ? "" : streetAddress
			.getStreetSuburbID() == null ? "" : streetAddress.getStreetSuburbID()
			.toString();
	String resiDpid = streetAddress == null ? ""
			: streetAddress.getDpid() == null ? "" : streetAddress.getDpid();
	String showResiDpid = resiDpid;

	AddressVO postalAddress = (AddressVO) request
			.getAttribute("postalAddress");

	String postProperty = postalAddress == null ? "" : postalAddress
			.getProperty() == null ? "" : postalAddress.getProperty();
	String postStreetChar = postalAddress == null ? "" : postalAddress
			.getPropertyQualifier() == null ? "" : postalAddress
			.getPropertyQualifier();
	String postStreet = postalAddress == null ? ""
			: postalAddress.getStreet() == null ? "" : postalAddress.getStreet();
	String postSuburb = postalAddress == null ? ""
			: postalAddress.getSuburb() == null ? "" : postalAddress.getSuburb();
	String paPostalcode = postalAddress == null ? "" : postalAddress
			.getPostcode() == null ? "" : postalAddress.getPostcode();
	String postState = postalAddress == null ? ""
			: postalAddress.getState() == null ? "" : postalAddress.getState();
	String postStsubIdString = postalAddress == null ? "" : postalAddress
			.getStreetSuburbID() == null ? "" : postalAddress.getStreetSuburbID()
			.toString();
	String postDpid = postalAddress == null ? ""
			: postalAddress.getDpid() == null ? "" : postalAddress.getDpid();
	String showPostDpid = postDpid;
	String addressesMatch = Boolean.toString(resiStsubIdString
			.equalsIgnoreCase(postStsubIdString)
			&& resiProperty.equalsIgnoreCase(postProperty)
			&& resiStreetChar.equalsIgnoreCase(postStreetChar));
%>
<input type="hidden" name="addressesMatch" value="<%=addressesMatch%>">
<table border="1" width="100%" cellspacing="0">
	<tr>
		<td>
			<%
				if (displayResidentialAddress)
				{
			%>
			<table border="0">
				<input type="hidden" name="resiStsubId" value="<%=resiStsubIdString%>">
				<input type="hidden" name="resiDpid" value="<%=resiDpid%>">


				<tr>
					<td colspan="2">
						<center>
							Residential Address
						</center>
					</td>
				</tr>
				<tr>

					<td class="label">
						Property:
					</td>
					<td>
						<input type="text" name="resiProperty" value="<%=resiProperty%>"
							onclick="popUpQASResidential();" />
					</td>
				</tr>
				<tr>
					<td class="label">
						Street #:
					</td>
					<td>
						<input type="text" name="resiStreetChar"
							value="<%=resiStreetChar%>" onblur="upper(this)"
							onchange="resetResidentialDpid()" />
					</td>
				</tr>
				<tr>
					<td class="label">
						Street:
					</td>
					<td>
						<input type="text" name="resiStreet" value="<%=resiStreet%>"
							onchange="resetResidentialStsubid();" />
					</td>
				</tr>
				<tr>
					<td class="label">
						Suburb:
					</td>
					<td>
						<input type="text" name="resiSuburb" value="<%=resiSuburb%>"
							onblur="resetResidentialStsubid();
                                                                           checkQASAddress('street',
                                                                                            document.all.addressesMatch.value,
                                                                                            document.all.resiStreet.value,
                                                                                            document.all.resiSuburb.value,
                                                                                            document.all.resiProperty.value,
                                                                                            document.all.resiStreetChar.value)" />
					</td>
				</tr>
				<tr>
					<td class="label">
						Postcode:
					</td>
					<td>
						<input type="text" name="resiPostcode" size="5"
							value="<%=saPostalcode%>" disabled="disabled" />
						<input type="text" size="10" name="resiState" value="<%=resiState%>"
							disabled="disabled" />
						<input type="text" size="9" name="showResiDpid"
							value="<%=showResiDpid%>" disabled="disabled" />
					</td>
				</tr>
			</table>
			<%
				}
			%>
		</td>
		<td>
			<%
			if (displayPostalAddress)
			{
			%>
			<input type="hidden" name="postStsubId" value="<%=postStsubIdString%>">
			<input type="hidden" name="postDpid" value="<%=postDpid%>">

			<table border="0">
				<tr>
					<td colspan="2">
						<center>
							Postal Address
						</center>
					</td>
				</tr>
				<tr>
					<td class="label">
						Property:
					</td>
					<td>
						<input type="text" name="postProperty" value="<%=postProperty%>"
							onclick="popUpQASPostal();" />
					</td>
				</tr>
				<tr>
					<td class="label">
						Street #:
					</td>
					<td>
						<input type="text" name="postStreetChar"
							value="<%=postStreetChar%>" onblur="upper(this)"
							onchange="resetPostalDpid()" />
					</td>
				</tr>
				<tr>
					<td class="label">
						Street:
					</td>
					<td>
						<input type="text" name="postStreet" value="<%=postStreet%>"
							onchange="resetPostalStsubid();" />
					</td>
				</tr> 
				<tr>
					<td class="label">
						Suburb:
					</td>
					<td>
						<input type="text" name="postSuburb" value="<%=postSuburb%>"
							onblur="resetPostalStsubid();
                                                                             checkQASAddress('postal',
                                                                                            document.all.addressesMatch.value,
                                                                                            document.all.postStreet.value,
                                                                                            document.all.postSuburb.value,
                                                                                            document.all.postProperty.value,
                                                                                            document.all.postStreetChar.value)" />
					</td>
				</tr>
				<tr>
					<td class="label">
						Postcode:
					</td>
					<td>
						<input type="text" name="postPostcode" size="5"
							value="<%=paPostalcode%>" disabled="disabled" />
						<input type="text" name="postState" size="10" value="<%=postState%>"
							disabled="disabled" />
						<input type="text" name="showPostDpid" size="9"
							value="<%=showPostDpid%>" disabled="disabled" />
					</td>
				</tr>
			</table>
			<%
				}
			%>
		</td>
	</tr>

</table>
