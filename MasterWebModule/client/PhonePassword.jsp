<%@page import="com.ract.common.*"%>
<%@page import="com.ract.client.ui.*"%>

<html>
<head>
<title>Set Phone Password</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<%=CommonConstants.getRACTStylesheet()%>
<%
  String pPassword = (String)request.getParameter("phonePassword");
  String pQuestion = (String)request.getParameter("phoneQuestion");
  String clientNo = (String)request.getParameter("clientNo");
%>
<script type="text/JavaScript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/JavaScript">
dojo.require("dojo.io.*");

function saveChanges()
{
   var nQuestion = dojo.byId("nQuestion").innerHTML;
   var nPassword = dojo.byId("nPassword").value;
   dojo.io.bind({url:"<%=ClientUIConstants.PAGE_ClientUIC%>",
                content:{event:"setPhonePassword",
	                 clientNo:"<%=clientNo%>",
                         phonePassword:nPassword,
                         phoneQuestion:nQuestion}});
   var win = window.self;
   win.opener = window.self;
   win.close();
}
</script>
<body bgcolor="#FFFFFF" text="#000000">
<form action="#">
  <table width="462">
    <tr>
      <td width="115">Phone Question</td>
      <td width="347">
        <textarea name="nQuestion" id="nQuestion" rows="2" cols="45"><%=pQuestion%></textarea>
      </td>
  </tr>
  <tr>
      <td width="115">Phone Password</td>
      <td width="347">
        <input type="text" name="nPassword" id="nPassword" size="60" value="<%=pPassword%>">
      </td>
  </tr>
  <tr>
    <td>&nbsp
    </td>
    <td>
        <input type="button" name="saveButton" value="Save" onclick="saveChanges();">
    </td>
  </tr>
  </table>
  <input type="hidden" name="event" value="setPhonePassword"/>
  <input type="hidden" name="clientNo" value="<%=clientNo%>"/>
</form>
</body>
</html>
