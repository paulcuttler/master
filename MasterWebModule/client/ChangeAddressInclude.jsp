<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>

<%
AddressVO theAddressVO = (AddressVO)request.getAttribute("addressVO");
Collection theStreetSuburbList = (Collection)request.getAttribute("streetSuburbList");

%>

<%=HTMLUtil.includeJS(CommonConstants.DIR_CLIENT+"/scripts/ChangeAddress.js")%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>

<script language="Javascript">

  function findStreetSuburbList(street,suburb) {
    if (street.length == 0 && suburb.length == 0)
    {
      alert('At least one character must be entered in the street or suburb fields when searching.');
      return;
    }
    document.all.event.value = 'findStreetSuburbList';
    document.forms[0].submit();
  }

</script>

<body bgcolor="#FFFFFF" text="#000000" onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/FindAddressButton_over.gif');">
<table width="100%" border="0" cellspacing="1" cellpadding="0">
  <tr>
      <td colspan="4">
      <span class="helpTextSmall">
      Type in a street and suburb and press find address to return a list of street suburb combinations.
      Click on a highlighted address to select it. Press OK to return to the client edit form with the address
      changes.</span>
      </td>
  </tr>
  <tr>
    <td width="10%" valign="top">Property:</td>
    <td colspan="2" valign="top">
      <input type="text" name="property" value="<%=(theAddressVO!=null?theAddressVO.getProperty()!=null?theAddressVO.getProperty():"":"")%>">
    </td>
    <td valign="top"rowspan="6" width="67%">
      <div id="streetSuburbPanel" style="height:150; overflow:auto; border-width: 1; border-style: solid;">
      <table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#333333">
          <td><font color="#FFFFFF" size="1">Street</font></td>
          <td><font color="#FFFFFF" size="1">Suburb</font></td>
          <td><font color="#FFFFFF" size="1">State</font></td>
          <td><font color="#FFFFFF" size="1">Postcode</font></td>
        </tr>
<%--
for each row found...
--%>
<%
if (theStreetSuburbList != null && !theStreetSuburbList.isEmpty())
{
    StreetSuburbVO svo = null;
    Iterator it = theStreetSuburbList.iterator();
    String addressText = "";
    int count = 0;
    while (it.hasNext())
    {
      count++;
      svo = (StreetSuburbVO)it.next();
      if (svo != null)
      {
        //construct a value string
        addressText = svo.getStreetSuburbID().toString();
        addressText += "|";
        addressText += svo.getStreet();
        addressText += "|";
        addressText += svo.getSuburb();
        addressText += "|";
        addressText += svo.getState();
        addressText += "|";
        addressText += svo.getPostcode();

        //construct a line
%>
          <tr onclick="populateAddressFields('<%=addressText%>','|')" style="cursor: hand;" onMouseOver="this.bgColor = '#C0C0C0'" onMouseOut ="this.bgColor = '#FFFFFF'">
            <td><%=svo.getStreet()%></td>
            <td><%=svo.getSuburb()%></td>
            <td><%=svo.getState()%></td>
            <td><%=svo.getPostcode()%></td>
          </tr><%
      }
    }
}
else
{
%>
  <tr>
    <td colspan="5">No records match that street and suburb combination.</td>
  </tr>

<%
}
%>
        </table>
      </div>

    </td>
  </tr>
    <td height="20" width="10%" valign="top">Number.:</td>
    <td colspan="2" valign="top">
      <input type="text" name="propertyQualifier" size="20" value="<%=(theAddressVO!=null?theAddressVO.getPropertyQualifier()!=null?theAddressVO.getPropertyQualifier():"":"")%>">
    </td>
  </tr>
  <tr>
    <td width="10%" valign="top">Street:</td>
    <td colspan="2" valign="top">
      <input type="text" name="streetName" value="<%=(theAddressVO!=null?theAddressVO.getStreet()!=null?theAddressVO.getStreet():"":"")%>" onchange="setInvalid();">
    </td>
  </tr>
  <tr>
    <td width="10%" valign="top" height="39">Suburb:</td>
    <td colspan="2" valign="top">
      <input type="text" name="suburbName" value="<%=(theAddressVO!=null?theAddressVO.getSuburb()!=null?theAddressVO.getSuburb():"":"")%>"
      onchange="setInvalid();">
      <br/>
      <a onclick="findStreetSuburbList(document.all.streetName.value,document.all.suburbName.value);"
         onMouseOut="MM_swapImgRestore();"
         onMouseOver="MM_displayStatusMsg('Find the address.');MM_swapImage('FindAddressButton','','<%=CommonConstants.DIR_IMAGES%>/FindAddressButton_over.gif',1);return document.MM_returnValue" >
         <img name="FindAddressButton" src="<%=CommonConstants.DIR_IMAGES%>/FindAddressButton.gif" border="0" alt="Find the address.">
      </a>
<%--
      <input type="submit" class="smallButton" value="Find Address" onclick="findStreetSuburbList(document.all.streetName.value,document.all.suburbName.value);">
--%>
    </td>
  </tr>
  <tr>
    <td width="10%" valign="top">Postcode:</td>
    <td width="12%" valign="top">
      <input type="text" name="postcode" size="10" READONLY value="<%=(theAddressVO!=null?theAddressVO.getPostcode()!=null?theAddressVO.getPostcode():"":"")%>" style="border-style: none; font-weight: normal;">
    </td>
    <td valign="top">
      <input type="text" name="state" size="7" READONLY value="<%=(theAddressVO!=null?theAddressVO.getState()!=null?theAddressVO.getState():"":"")%>" style="border-style: none; font-weight: normal;">
      <input type="textfield" name="valid" size="2"DISABLED style="border-style: none; font-weight: normal;" value="<%=(theAddressVO==null?"X":"V")%>">
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <input type="hidden" name="stsubid" value="<%=(theAddressVO!=null?theAddressVO.getStreetSuburbID()!=null?theAddressVO.getStreetSuburbID().toString():"":"")%>">
      <input type="hidden" name="description" value="<%=(theAddressVO!=null?theAddressVO.getSingleLineAddress()!=null?theAddressVO.getSingleLineAddress():"":"")%>">
    </td>
      <td>&nbsp; </td>
  </tr>
</table>

<script language="Javascript">
  function setValid()
  {
    //invalidate the form
    if (document.all.stsubid.value != '')
      document.all.valid.value = 'VALID';
  }
  function setInvalid()
  {
    //invalidate the form
    document.all.stsubid.value = '';
    document.all.postcode.value = '';
    document.all.state.value = '';
    document.all.valid.value = 'INVALID';
  }
</script>
