<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%--

Client summary include for consistent header throughout application.

assumes a client vo is in scope

--%>

<%!
String emptyString = "";
String unknownString = "??????";
%>

<%
	ClientVO client = (ClientVO)request.getAttribute("client");

//LogUtil.log(this.getClass(),"client="+client);

String clientName = unknownString;
String postalAddress = emptyString;
String streetAddress = emptyString;
String clientStatus = null;
Integer clientNumber = null;
boolean groupClient = false;
boolean isProtected = false;
String phonePassword = "";
String phoneQuestion = "";

if (client != null)
{
   clientNumber = client.getClientNumber();
   clientName = client.getDisplayName();
   clientName = (clientName == null ? unknownString : clientName);
   postalAddress = (client.getPostalAddress() == null ? emptyString : client.getPostalAddress().getSingleLineAddress());
   streetAddress = (client.getResidentialAddress() == null ? emptyString : client.getResidentialAddress().getSingleLineAddress());
   clientStatus = client.getStatus();
   groupClient = client.getGroupClient();
   if(client.getPhonePassword()!=null && !client.getPhonePassword().equals(""))
   {
     isProtected = true;
     phonePassword = client.getPhonePassword();
     phoneQuestion = client.getPhoneQuestion();
   }
%>

<table border="0" cellspacing="3" cellpadding="3">
  <tr>
    <td class="label">Number:</td>
    <td class="dataValue"><%=clientNumber%></td>
    <td class="label">&nbsp;Name:</td>
    <td class="dataValue" nowrap> <%=clientName%> <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5"></td>
  </tr>
  <tr>
    <td class="label" nowrap>Type:</td>
    <td class="dataValue">
      <%=client.getClientType()%> <%=groupClient?"<span class=\"helpTextSmall\">Group Client</span>":""%>
    </td>
    <td class="label" nowrap>&nbsp;Status:</td>
    <td class="dataValue"><%=ClientUIHelper.formatClientStatusHTML(client.getStatus())%></td>
  </tr>
  <tr>
    <td class="label">Residential address:</td>
    <td class="dataValue" colspan="3"><%=streetAddress%></td>
  </tr>
  <tr>
    <td class="label">Postal address:</td>
    <td class="dataValue" colspan="3"><%=postalAddress%></td>
  </tr>
</table>
<%
}
%>