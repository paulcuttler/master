<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.payment.PaymentException"%>
<%@page import="java.util.*"%>
<%@page import="javax.naming.*"%>
<%@page import="javax.rmi.*"%>
<%@page import="javax.ejb.FinderException"%>

<%--
JSP to display the products that are associated with the currently selected client.

The format is as follows:

Client
======

*  Client Name - Client Number
*  Client Address

Insurance
=========

*  Products...

Membership
==========

*  Products...
--%>

<%!
/**
 * @todo control insurance and membership through security. Suppress
 * the membership details if they are an external insurance broker.
 */
private final boolean MEMBERSHIP_ENABLED = true;
%>

<%
ClientVO clientVO = null;
Integer clientNumber = null;
ArrayList memVOList = null;
memVOList = (ArrayList)request.getAttribute("memVOList");
clientVO = (ClientVO) request.getAttribute("client");

if (clientVO == null)
{
   String clientNumberStr = (String)request.getAttribute("clientNumber");
   if (clientNumberStr != null)
   {
      clientNumber = new Integer(clientNumberStr);
      Collection clientList = (Collection)session.getAttribute("clientList");
      clientVO = ClientUIHelper.getClient(clientNumber,clientList);
   }
}
else
{
   clientNumber = clientVO.getClientNumber();
}

UserSession userSession = UserSession.getUserSession(request.getSession());
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=HTMLUtil.noCacheTag()%>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_CLIENT%>/scripts/SearchClient.js">
</script>
</head>

<body class="leftFrame" onLoad="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/SearchButton_over.gif');">

<form name="form" method="POST" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
  <input type="hidden" name="event" value="productList_btnSearch">
<table>
<tr>
<td>
  <%@include file="/client/SearchClientInclude.jsp"%>
</td>
</tr>
</table>

<%
if (clientVO == null)
{
%>
<table>
  <tr>
    <td>
      <span class="errorTextSmall">Unable to locate the customer.</span>
    </td>
  </tr>
</table>
<%
}
else
{
%>

<span class="listHeading">Customer</span>

<table width="100%" cellspacing="0" cellpadding="0">
  <tr class="headerRow">
    <td class="listHeadingPadded">Number</td>
    <td class="listHeadingPadded">Name</td>
  </tr>
  <tr class="productListHighlight">
    <td class="dataValueSmall" valign="top" nowrap> <%=clientVO.getClientNumber()%> </td>
    <td class="dataValueSmall" valign="top">
    <a class="actionItem"
       target="rightFrame"
       title="View client summary"
       href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientSummary&clientNumber=<%=clientVO.getClientNumber()%>&clientType=<%=clientVO.getClientType()%>"><%=clientVO.getDisplayName()%></a>
       </td>
  </tr>
</table>
<%--
Determine what applications are active. This is used to shut individual applications down.
--%>
<br/>
<table border="0" cellpadding="0" cellspacing="0">
<%
if (    this.MEMBERSHIP_ENABLED
    &&  userSession != null
    &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_VIEW))
{
%>
<tr>
  <td nowrap>
    <span class="listHeading">Membership</span>
  </td>
</tr>
<%
   if (memVOList != null &&
   !memVOList.isEmpty())
   {
     MembershipVO memVO = null;
%>
<tr>
  <td>
      <ul>
<%
      Iterator memVOListIterator = memVOList.iterator();
      while (memVOListIterator.hasNext())
      {
         memVO = (MembershipVO) memVOListIterator.next();
%>
         <li><%=MembershipUIHelper.displayMembershipLine(memVO,true,true)%></li>
<%
      }
%>
      </ul>
    </td>
</tr>
<%
   }
   else
   {
%>
   <tr>
      <td class="helpTextSmall">No memberships found for this client.
      </td>
    </tr>
<%
   }
}
%>
</table>
<%
}
%>
</form>
</body>
</html>
