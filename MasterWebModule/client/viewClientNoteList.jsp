<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>


<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet" type="text/css"/>

<script language="Javascript">
function filterNoteList()
{
  document.forms[0].submit();
}
</script>
</head>
<body>
<h2>View Client Note List</h2>

  <table>
          <tr>
            <td>
            <!--client details-->
            <%@ include file="/client/ViewClientInclude.jsp"%>
            </td>
          </tr>
   </table>
   <table>
     <tr><td>
<s:form name="frm_viewnotes" action="filterNotes.action">
<s:hidden name="clientNumber" value="%{clientNumber}"/>
       <s:select emptyOption="true" name="noteType" listValue="noteType" listKey="noteType" list="noteTypeList" label="Filter by Note Type" onchange="javascript:filterNoteList()">
       </s:select>
       <s:select emptyOption="true" name="businessType" listValue="description" listKey="typeCode" list="businessTypeList" label="Filter by Business Type" onchange="javascript:filterNoteList()">
       </s:select>
</s:form>

</td>
     </tr>
   </table>
   <s:if test="clientNoteList != null && clientNoteList.size() > 0">
   <table>
     <thead>
        <tr>
          <th align="left" valign="top">Created</th>
          <th align="left" valign="top">Note Type</th>
          <th align="left" valign="top">Business Type</th>
          <th align="left" valign="top">Note Text</th>
          <th align="left" valign="top">User Id</th>
          <th align="left" valign="top">Due</th>          
          <th align="left" valign="top">Block Transactions?</th> 
          <th align="left" valign="top">Completed?</th>
          <th align="left" valign="top">Comments</th>
        </tr>
      </thead>
      <tbody>

           <s:iterator value="clientNoteList" status="cStat">
               <tr class="<s:if test="#cStat.odd == false">oddLink</s:if><s:else>evenLink</s:else>">
		               <td><s:date name="created" format="dd/MM/yyyy HH:mm:ss"/></td>
		               <td>
		               <s:url action="viewClientNote" id="myUrl">
		                 <s:param name="noteId" value="%{noteId}"/>
		               </s:url>
		               
		               <a href="<s:property value="#myUrl"/>"> <s:property value="noteType"/> </a>
		               
		               </td>
		               <td><s:property value="businessType"/></td>		               
		               <td><s:property value="noteText"/></td>
		               <td><s:property value="createId"/></td>
		               <td><s:property value="due"/></td>		               
		               <td><s:if test="blockTransactions">
<img src="/images/check.gif">
</s:if>
<s:else>
<img src="/images/cross.gif">
</s:else></td>
		               <td><s:property value="completionDate"/></td>		               
					   <td><s:property value="clientNoteComments.size"/></td>		               
               </tr>
               </s:iterator>
      </tbody>
</table>
</s:if>
<s:else>
<span class="helpText">No client notes found.</span>
</s:else> 

</body>
</html>
