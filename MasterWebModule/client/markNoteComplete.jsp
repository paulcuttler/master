<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet" type="text/css"/>
<sx:head/>

<script language="Javascript">
	function displayResultValue(selectBox) {
		var selectedIndex = selectBox.selectedIndex;
		var selectedValue = selectBox.options[selectedIndex].value;
		if (selectedValue != null) {
			dojo.event.topic.publish("showDet")
		}
	}
</script>

</head>
<body>

<s:include value="/client/ViewClientInclude.jsp"/> 

<s:include value="/client/ViewClientNoteInclude.jsp"/>

<s:form id="frm_marknotecomplete" name="frm_marknotecomplete" action="completeNote.action">

<s:hidden name="noteId" value="%{noteId}"/>
<s:textarea cols="50" rows="5" label="Completion Notes" name="completionNotes"></s:textarea>
<s:textfield label="Completion Reference" name="completionReferenceId"></s:textfield> 
<s:select label="Completion Code" name="resultCode" listKey="resultCode" listValue="resultText" list="completionResults" onchange="displayResultValue(this)" required="true" emptyOption="true"></s:select>

<!-- display if result requires it - see note category for details of implementing -->
<tr>
<td colspan="2">

	<s:url id="resultURL" action="showResultValue" />
	<sx:div showLoadingText="false"
				href="%{resultURL}" theme="ajax" listenTopics="showDet"
				formId="frm_marknotecomplete">
	</sx:div>

</td>
</tr>

<s:submit value="Save"></s:submit>
</s:form>

</body>

</html>