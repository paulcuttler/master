<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="noteResult.requiresValue">
<table>
<s:textfield label="Completion Value ($)" name="resultValue"></s:textfield>
</table>
</s:if>