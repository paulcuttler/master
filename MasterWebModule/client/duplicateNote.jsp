<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>Duplicate Page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

	<link rel="stylesheet" type="text/css" href="css/ract.css">

  </head>
  
  <body> 
  <h2>Client note already submitted</h2>
  <p>The client note has already been submitted. You do not need to click the button again. Please check the client notes via view notes.</p>
  </body>
</html>
