<%@page import="com.ract.common.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="javax.naming.*"%>
<%@page import="javax.rmi.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<%--
displays a list of clients after a search has been performed
--%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=HTMLUtil.noCacheTag()%>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_CLIENT%>/scripts/SearchClient.js">
</script>
</head>
<body class="leftFrame" onLoad="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/SearchButton_over.gif');">
<form name="form" method="POST" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
  <input type="hidden" name="event" value="clientList_btnSearch">
  <table width="100%" cellspacing="0" cellpadding="0">
  <tr>
  <td>
  <jsp:include page="/client/SearchClientInclude.jsp"></jsp:include>
  </td>
  </tr>
  <tr>
  <td>

<%
int clientListSize;
Iterator it;
String clientType;
String row;

Collection clientList = (Collection)session.getAttribute("clientList");

int count = 0;
String message = (String) request.getAttribute("message");

/*
if a message is available then display it.
*/
if (message != null) {
%>
  <span class="errorTextSmall"><%=message%></span>
  <br/>
<%
}
/*
determine if the clientList in memory is null
*/
if (clientList != null && clientList.size() > 0)
{
  it = clientList.iterator();
  clientListSize = clientList.size();
  if (clientListSize == 0)
  {
  %>
    <b>No clients were found.</b>
  <%
  }
  else
  {
  %>
    <span class="dataValueItalicSmall">The search returned <%=clientListSize%> <%=(clientListSize==1?"record":"records")%>.</span>
    <p>
    <input type="checkbox" name="showAddress" onclick="viewAddress();"><span class="helpTextSmall">Display address</span>
    </p>

  <script language="Javascript">

  function viewAddress()
  {
    for (var x=0; x < <%=clientListSize%>; x++)
    {
      var elem = 'address_'+x;
      var obj = document.getElementById(elem);
      if (document.all.showAddress.checked == true) {
        obj.style.display = '';
      }
      else {
        obj.style.display = 'none';
      }
    }
  }
  </script>

  <%
  }
  %>

  <table width="100%" cellspacing="0" cellpadding="0">
  <tr class="headerRow">
    <td class="listHeadingPadded">Number</td>
    <td class="listHeadingPadded">Name</td>
  </tr>
  <%
  int counter = 0;
  while (it.hasNext()) {
    counter++;
    ClientVO client = (ClientVO)it.next();
    //list each client with their address
    %>
      <tr valign="top" class="<%=HTMLUtil.getRowType(counter)%>">
        <td class="listItem" nowrap><%=client.getClientNumber()%></td>
        <td class="listItem"><a
          title="View client summary"
          class="actionItem" target="rightFrame" href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientSummary&clientNumber=<%=client.getClientNumber()%>&clientType=<%=client.getClientType()%>"><%=client.getDisplayName()%></a></td>
      </tr>
    <%
    	//if no street address then don't display else display it.
          if (client.getResidentialAddress() != null) {
    %>
      <tr class="<%=HTMLUtil.getRowType(counter)%>" id="address_<%=count%>" style="display:none">
        <td class="listItemItalic" colspan=2><%=client.getResidentialAddress().getSingleLineAddress()%> </td>
      </tr>
    <%}
      /*
      if no address was found then display a message to facilitate an admin task.
      */
      else {
    %>
      <tr class="<%=HTMLUtil.getRowType(counter)%>" id="address_<%=count%>" style="display:none">
        <td colspan=2 class="errorTextSmall"><%=ClientUIConstants.ADDRESS_UNKNOWN%></td>
      </tr>
    <%}
    count++;
  }
  %>
  </table>
  <%
}
%>
</td>
</tr>
</table>
</form>
</body>
</html>
