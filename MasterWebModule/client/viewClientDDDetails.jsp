<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.bank.*"%>
<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
ClientVO clientVO = (ClientVO)request.getAttribute("client");
Collection c = (Collection)request.getAttribute("directDebitAuthorities");
request.setAttribute("client",clientVO);
%>

<html>
<head>
<title>
ViewClientDDDetails
</title>
<%=CommonConstants.getRACTStylesheet()%>


</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" height="100%">
<tr valign="top">
<td>
  <h2> Direct Debit Authority list for customer number <%=clientVO.getClientNumber()%></h2>

  <%@ include file="/client/ViewClientInclude.jsp"%>

<table border="0" cellspacing="3" cellpadding="0">
<%

//log("in view client ddr");

Iterator it = null;
DirectDebitAuthority ddAuthority = null;
if (c != null && !c.isEmpty())
{
  it = c.iterator();
  while (it.hasNext())
  {
    ddAuthority = (DirectDebitAuthority)it.next();
    BankAccount ba = ddAuthority.getBankAccount();
%>
          <tr valign="top">
            <td nowrap align="left" valign="top">Direct Debit Date (PDC):</td>
            <td nowrap class="dataValue" align="left" valign="top"><a title="<%=ddAuthority.getDirectDebitAuthorityID()%>"><%=ddAuthority.getDdrDate()%></a></td>
            <td nowrap align="left" valign="top">Payment Type:</td>
            <td class="dataValue" align="left" valign="top" nowrap><%=PaymentMethod.DIRECT_DEBIT%></td>
            <td nowrap align="left" valign="top">Frequency:</td>
            <td class="dataValue" nowrap align="left" valign="top">
<%
            DirectDebitFrequency ddf = ddAuthority.getDeductionFrequency();
            if (ddf != null)
            {
%>
              <%=ddf.getDescription()%>;
<%
            }
%>
            </td>
            <td nowrap>Day of Payment:</td>
            <td class="dataValue" nowrap><%=ddAuthority.getDayToDebit()%><%=DateUtil.getDaySuffix(ddAuthority.getDayToDebit())%></td>
            <td nowrap>&nbsp;</td>
            <td nowrap class="dataValue">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" nowrap>Source System:</td>
            <td align="left" valign="top" class="dataValueRed"><%=ddAuthority.getSourceSystem()%></td>
            <td nowrap align="left" valign="top">Owner?</td>
            <td class="dataValue" align="left" valign="top" nowrap><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(ddAuthority.isClientIsOwner() ? "check.gif" : "cross.gif")%>"></td>
            <td nowrap align="left" valign="top">Approved?</td>
            <td nowrap class="dataValue" align="left" valign="top"><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(ddAuthority.isApproved() ? "check.gif" : "cross.gif")%>"></td>
            <td nowrap align="left" valign="top">User Id:</td>
            <td class="dataValue" align="left" valign="top" nowrap><%=ddAuthority.getUserID()%></td>
            <td nowrap>&nbsp;</td>
            <td nowrap class="dataValue">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" nowrap>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td nowrap>Sales Branch:</td>
            <td nowrap class="dataValue"><%=ddAuthority.getSalesBranchCode()%></td>
            <td nowrap align="left" valign="top">&nbsp;</td>
            <td nowrap class="dataValue" align="left" valign="top">&nbsp;</td>
            <td nowrap align="left" valign="top">&nbsp;</td>
            <td class="dataValue" align="left" valign="top" nowrap>&nbsp;</td>
            <td nowrap>&nbsp;</td>
            <td nowrap class="dataValue">&nbsp;</td>
          </tr>
          <%
    String accountType = "";
    if (ba instanceof DebitAccount)
    {
       DebitAccount da = (DebitAccount)ba;
       accountType = "Debit Account";
%>
          <tr>
            <td align="left" valign="top" nowrap>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td nowrap align="left" valign="top">Account Type:</td>
            <td class="dataValue" align="left" valign="top" nowrap><%=accountType%></td>
            <td nowrap align="left" valign="top">Account Name:</td>
            <td class="dataValue" nowrap align="left" valign="top"> <%=da.getAccountName()%></td>
            <td nowrap align="left" valign="top">Account No.:</td>
            <td class="dataValue" align="left" valign="top" nowrap> <%=da.getAccountNumber()%></td>
            <td nowrap>&nbsp;</td>
            <td class="dataValue" nowrap>&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" nowrap>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td nowrap>BSB No.:</td>
            <td class="dataValue" nowrap> <%=da.getBsbNumber()%></td>
            <td nowrap align="left" valign="top">&nbsp;</td>
            <td class="dataValue" align="left" valign="top" nowrap>&nbsp;</td>
            <td width="125" align="left" valign="top" nowrap>&nbsp;</td>
            <td class="dataValue" nowrap align="left" valign="top">&nbsp;</td>
            <td nowrap>&nbsp;</td>
            <td class="dataValue" nowrap>&nbsp;</td>
          </tr>
          <%
    }
    else if (ba instanceof CreditCardAccount)
    {
       CreditCardAccount cca = (CreditCardAccount)ba;
       accountType = "Credit Card Account";
%>
          <tr>
            <td align="left" valign="top" nowrap>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td nowrap align="left" valign="top">Credit Card Type:</td>
            <td class="dataValue" align="left" valign="top" nowrap> <%=(cca.getCardTypeDescription()==null?"":cca.getCardTypeDescription().toUpperCase())%></td>
            <td nowrap>Credit Card No.:</td>
            <td class="dataValue" nowrap> <%=StringUtil.obscureFormattedCreditCardNumber(cca.getCardNumber())%></td>
            <td nowrap align="left" valign="top">&nbsp;</td>
            <td class="dataValue" align="left" valign="top" nowrap>&nbsp;</td>
            <td nowrap>&nbsp;</td>
            <td class="dataValue" nowrap>&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" nowrap>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td nowrap align="left" valign="top">Credit Card Expiry:</td>
            <td class="dataValue" align="left" valign="top" nowrap> <%=cca.getCardExpiry()%></td>
            <td nowrap align="left" valign="top">Credit Card Verification:</td>
            <td class="dataValue" nowrap align="left" valign="top"> <%=cca.getVerificationValue()%></td>
            <td nowrap align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top" nowrap>&nbsp;</td>
          </tr>
          <%
    }
  }
}
else
{
%>
          <tr>
             <td class="helpText">No direct debit authorities to display.</td>
          </tr>
<%
}
%>
      </table>
  </td>
</tr>
<form method="post" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
<input type="hidden" name="event" value="printMembershipAccountDetails">
<input type="hidden" name="clientNumber" value="<%=client.getClientNumber()%>">
<tr valign="bottom">
  <td>
    <table cellpadding="5" cellspacing="0">
      <tr>
        <td><input type="button" name="btnBack" value="Back" onClick="history.back();"></td>
      </tr>
    </table>
  </td>
</tr>
</form>
</table>

</body>
</html>
