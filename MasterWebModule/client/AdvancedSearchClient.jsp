<%@page import="com.ract.common.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<title>Advanced Customer Search</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js"/>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"/>
<script language="Javascript">

  function loadInParent(url, closeSelf) {
    if (checkVal()) {
      self.opener.top.leftFrame.location = url;
      if(closeSelf)
        self.close();
    }
  }

  function searchAddress() {
    var url = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=clientList_btnSearch&clientSearchType=<%=ClientUIConstants.SEARCHBY_ADDRESS%>&clientSearchValue='+document.all.saProperty.value+","+document.all.saPropertyQualifier.value+","+document.all.saStsubid.value+'&message='+getMessage();
    loadInParent(url,true);
  }

  function checkVal(val) {
    //alert('stsubid: '+val);
    if (document.all.saStsubid.value == '')
    {
      alert('You must identify a street and suburb for your search.');
      return false;
    }
    else if (document.all.saPropertyQualifier.value == '' &&
             document.all.saProperty.value == '')
    {
      alert('You must enter a street number or property name.');
      return false;
    }
    return true;
  }

  function getMessage() {
    var message = "";
    message += 'Searched for '+document.all.saPropertyQualifier.value;
//    message += ' '+document.all.description.value;
    return escape(message);
  }

</script>
</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/SearchButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/ClearButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/CloseButton_over.gif');">
<h2 class="contentHeading">Advanced Client Search</h2>
<form name="address" method="post" action="<%=ClientUIConstants.PAGE_ClientUIC%>">

<%
/**
 * @todo add other complex searches and remove avo reference
 */
AddressVO avo = null;
String pageName = (String)request.getAttribute("page");
request.setAttribute("displayResidentialAddress",new Boolean(true));
request.setAttribute("displayPostalAddress",new Boolean(false));
Collection streetSuburbList = (Collection)request.getAttribute("streetSuburbList");
%>

<%@include file="/client/EditAddressInclude.jsp"%>

<input type="hidden" name="event" value="searchAddress">
<input type="hidden" name="page" value="<%=pageName%>">

<input type="button" value="Search" onclick="searchAddress();">
<input type="button" value="Close" onclick="self.close();" >

</form>

<script language="Javascript">
  document.all.saProperty.focus();
</script>

</body>
</html>


