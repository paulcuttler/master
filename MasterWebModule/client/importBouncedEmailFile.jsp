<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="java.util.*"%>
<%--
Requests parameters for the EmailAddress Update

request attributes:
	inputFileName

returned parameters:
	inputFileName		this is passed through
--%>
<%
CommonMgr cm = CommonEJBHelper.getCommonMgr();
String inputFileName = cm.getSystemParameterValue( SystemParameterVO.CATEGORY_EMAIL_ADDRESS_MAINTENANCE,
                                                       SystemParameterVO.EMAIL_ADDRESS_MAINTENANCE_INPUT_FILE
                                                      );
%>
<html>
<head>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/Utilities.js") %>
<script language="javascript">

function validate()
{
  return validateRunTime(document.all.runHours, document.all.runMins);
}

</script>
<title>
EmailAddressUpdate
</title>
<%=CommonConstants.getRACTStylesheet()%>

</head>
<body>
<form name = "EmailAddressUpdate" method="post" onsubmit="return validate(this)" action="<%=ClientUIConstants.PAGE_ClientUIC%>">

<input type="hidden" name="event" value="EmailAddress_DoUpdate">
<input type="hidden" name="fileDate" value="">



<table border="0" height="100%">
<tr valign="top">
<td>
  <table>
    <tr>
      <td><h2>Remove Bounced Email Addresses</h2></td>
    </tr>
    <tr>
      <td>
        Bounced email file:
      </td>
      <td>
        <%=HTMLUtil.inputOrReadOnly("inputFileName",inputFileName)%>
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <%@include file="/common/scheduleInclude.jsp"%>
      </td>
    </tr>


   </table>

   </td>
   </tr>
   <tr valign="bottom">
   <td>
   <table border="0">
   <tr>
   <td >
     <input type="submit" value="Execute"/>
   </td>
   </tr>
   </table>
</td>
</tr>
</table>
</form>
</body>
</html>
