<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="java.util.*"%>
<%--
Requests parameters for the Subscriptions Update

request attributes:
	inputFileName

returned parameters:
	inputFileName		this is passed through
   	runDate
   	runHours
   	runMins

--%>
<%
    CommonMgr cm = CommonEJBHelper.getCommonMgr();

    String inputFileName = cm.getSystemParameterValue( SystemParameterVO.CATEGORY_SUBSCRIPTIONS,
                                                       SystemParameterVO.SUBSCRIPTIONS_INPUT_FILE
                                                      );
%>
<html>
<head>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/Validation.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/Utilities.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<script language="javascript">

function validate()
{
  return validateRunTime(document.all.runHours, document.all.runMins);
}

</script>
<title>
SubscriptionsUpdate
</title>
<%=CommonConstants.getRACTStylesheet()%>

</head>
<body>

<form name = "SubscriptionsUpdate" method="post" onsubmit="return validate()" action="<%=ClientUIConstants.PAGE_ClientUIC%>">
<input type="hidden" name="event" value="Subscriptions_DoUpdate">
<input type="hidden" name="fileDate" value="">

<table border="0" height="100%">
<tr valign="top">
<td>
<table>
  <tr>
    <td><h1>Remove Newsletter Subscriptions</h1></td>
  </tr>

    <tr>
      <td>
        Unsubscribe file:
      </td>
      <td>
        <%=HTMLUtil.inputOrReadOnly("inputFileName",inputFileName)%>
      </td>
    </tr>

    <!-- tr>
    	<td>
              Date to Process (dd/mm/yyyy)
    	</td>
        <td>
          <input type="text" name="dateToProcess" size="11" value ="" onblur="checkdate(this);" />
        </td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr -->

    <tr>
      <td colspan="2">
        <%@include file="/common/scheduleInclude.jsp"%>
      </td>
    </tr>

   </table>
   </td>
   </tr>

   <tr valign="bottom">
   <td>
   <table border="0">
   <tr>
   <td >
     <input type="submit" value="Execute"/>
   </td>
   </tr>
   </table>
</td>
</tr>
</table>
</form>
</body>
</html>
