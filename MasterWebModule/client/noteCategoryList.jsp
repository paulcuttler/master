<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<link href="<s:url value="/css/ract.css"/>" rel="stylesheet"
			type="text/css" />
		<s:head/>
	</head>
	<body>
	
	<h2>Note Categories</h2>
	
	<table>
	
	<thead>
	  <tr>
	  <th>Category Code</th>
	  <th>Category Name</th>
	  <th>Business Type</th>
	  <th>Follow Up</th>
	  <th>Followup Days</th>
	  <th>Notifiable?</th>
	  <th>Notification User Id</th>
	  <th>Notification Email Address</th>
	  <th colspan="3">Action</th>	
	  </tr>	  	  	  	  	  
	</thead>
	
	<tbody>
	
	<s:iterator value="list">
	
	<tr>
	  <td>
	  <s:property value="categoryCode"/>
	  </td>	  
	  <td>
	  <s:property value="categoryName"/>
	  </td>	  
	  <td>
	  <s:property value="businessType"/>
	  </td>	  
	  <td>
	  <s:property value="followUp"/>
	  </td>	  	  	
	  <td>
	  <s:property value="followUpDays"/>
	  </td>	  	  	
	  <td>
	  <s:property value="notifiable"/>
	  </td>	  	  	
	  <td>
	  <s:property value="notificationUserId"/>
	  </td>		  
	  <td>
	  <s:property value="notificationEmailAddress"/>
	  </td>		  
	  <td>
	  <s:url id="url" action="%{actionClass}_view">
	  <s:param name="requestId" value="categoryCode"/>
	  </s:url>
	  <s:a href="%{url}">View</s:a>
	  </td>	  
	  <td>
	  <s:url id="url" action="%{actionClass}_edit">
	  <s:param name="requestId" value="categoryCode"/>
	  </s:url>
	  <s:a href="%{url}">Edit</s:a>
	  </td>	 
	  <td>
	  <s:url id="url" action="%{actionClass}_delete">
	  <s:param name="requestId" value="categoryCode"/>
	  </s:url>
	  <s:a href="%{url}">Delete</s:a>
	  </td>	 	  	  
	</tr>
	
	</s:iterator>
	
	<tr>
    <td>
	  <s:url id="url" action="%{actionClass}_create">
	  </s:url>
	  <s:a href="%{url}">New</s:a>    
    </td>
	</tr>
	
	</tbody>
	
	</table>
	
	</body>
	
</html>
	