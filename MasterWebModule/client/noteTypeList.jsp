<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<link href="<s:url value="/css/ract.css"/>" rel="stylesheet"
			type="text/css" />			
		<s:head/>
	</head>
	<body>
	
	<h2>Note Types</h2>
	
	<table>
	
	<thead>
	  <tr>
	  <th>Note Type</th>
	  <th>Requires Notification?</th>
	  <th>Block Transactions?</th>
	  <th>Referral?</th>
	  <th>Followup?</th>
	  <th>Notification Email Address</th>
	  <th>Last Update</th>
	  <th>Requires Completion?</th>
	  <th>Add Comments?</th>
	  <th colspan="3">Action</th>	
	  </tr>	  	  	  	  	  
	</thead>
	
	<tbody>
	
	<s:iterator value="list">
	
	<tr>
	  <td>
	  <s:property value="noteType"/>
	  </td>	  
	  <td>
	  <s:property value="notifiable"/>
	  </td>	  
	  <td>
	  <s:property value="blockTransactions"/>
	  </td>	  
	  <td>
	  <s:property value="referral"/>
	  </td>	  
	  <td>
	  <s:property value="followup"/>
	  </td>	  	  
	  <td>
	  <s:property value="notificationEmailAddress"/>
	  </td>	  	  	
	  <td>
	  <s:property value="lastUpdate"/>
	  </td>	  	  	
	  <td>
	  <s:property value="requiresCompletion"/>
	  </td>	  	  	
	  <td>
	  <s:property value="commentable"/>
	  </td>		  
	  <td>
	  <s:url id="url" action="%{actionClass}_view">
	  <s:param name="requestId" value="noteType"/>
	  </s:url>
	  <s:a href="%{url}">View</s:a>
	  </td>	  
	  <td>
	  <s:url id="url" action="%{actionClass}_edit">
	  <s:param name="requestId" value="noteType"/>
	  </s:url>
	  <s:a href="%{url}">Edit</s:a>
	  </td>	 
	  <td>
	  <s:url id="url" action="%{actionClass}_delete">
	  <s:param name="requestId" value="noteType"/>
	  </s:url>
	  <s:a href="%{url}">Delete</s:a>
	  </td>	 	  	  
	</tr>
	
	</s:iterator>
	
	<tr>
    <td>
	  <s:url id="url" action="%{actionClass}_create">
	  </s:url>
	  <s:a href="%{url}">New</s:a>    
    </td>
	</tr>
	
	</tbody>
	
	</table>
	
	</body>
	
</html>
	