<%@include file="/security/VerifyAccess.jsp"%>

<%@page import="com.ract.util.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
        "http://www.w3.org/TR/html4/frameset.dtd">
<html>

<head>
  <title>RACT Application</title>
</head>

<%@include file="/common/NoCache.jsp"%>

<%!

/*
"official" RACT colours

blue:       #003366
red:        #CC0000
light blue: #CDD7E1
*/

final static String MRM_CLIENT_VIEW = "ViewClient";
final static String MRM_MEMBER_VIEW = "ViewMembership";
final static String MRM_GROUP_VIEW = "ViewGroup";
final static String MRM_CLIENT_CREATE = "CreateClient";

%>

<%--
Accept parameters as a GET request and then show relevant screens

* Change the default left frame to be the selected client.
--%>

<frameset rows="45,*" frameborder="0" border="0" framespacing="0">
  <%--<frame name="topFrame" scrolling="NO" noresize src="header.html" >--%>

  <frameset cols="180,*" frameborder="0" border="0" framespacing="2">
    <frame name="titleFrame" scrolling="NO" noresize src="/menu/TitleFrame.jsp">
    <frameset rows="20,25" frameborder="0" border="0" framespacing="0">
      <frame name="loginFrame" scrolling="NO" noresize src="/menu/Header.jsp">
      <frame name="menuFrame" scrolling="NO" noresize src="/menu/Menu.jsp" >
    </frameset>
  </frameset>

  <frameset cols="180,*" frameborder="0" border="0" framespacing="2">

  <%
  String event = request.getParameter("event");
  String clientNumber = request.getParameter("clientNumber");
  String membershipId = request.getParameter("membershipId");
  if (clientNumber != null) {
  	clientNumber = clientNumber.replaceAll("\\D", ""); // strip non-digits
  }
  %>

    <frame name="leftFrame" noresize src="/client/ClientList.jsp">

  <%
  if (MRM_CLIENT_VIEW.equals(event))
  {
  %>
    <frame name="rightFrame" noresize src="/viewClient.action?clientNumber=<%=clientNumber%>">
  <%
  }
  else if (MRM_CLIENT_CREATE.equals(event))
  {
  %>
    <frame name="rightFrame" noresize src="/showCreateClient.action">
  <%
  }
  else if (MRM_MEMBER_VIEW.equals(event))
  {
  %>
    <frame name="rightFrame" noresize src="/MembershipUIC?event=viewClientSummary_viewMembership&clientNumber=<%=clientNumber%>">
  <%
  }
  else if (MRM_GROUP_VIEW.equals(event))
  {
  %>
    <frame name="rightFrame" noresize src="/MembershipUIC?event=viewMembership_viewGroupMembers&membershipID=<%=membershipId%>">
  <%
  }
  else
  {
  %>
    <frame name="rightFrame" noresize src="/menu/DefaultPage.jsp">
  <%
  }
  %>

  </frameset>

</frameset>

<noframes>
  <body>
	Your browser does not support frames! Please use Internet Explorer 5+.
  </body>
</noframes>

</html>
