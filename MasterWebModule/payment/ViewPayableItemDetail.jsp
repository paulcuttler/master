<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.payment.receipting.Receipt"%>
<%@page import="com.ract.payment.receipting.Payable"%>

<%
PayableItemVO payItem = (PayableItemVO) request.getAttribute("payableItem");
log("test 1");
ClientVO clientVO = payItem.getClient();
// Attach the clientVO to the request to pass it to ViewClientInclude.jsp
request.setAttribute("client",clientVO);
%>

<html>
<head>
<title>Payable Item Detail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script language="Javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
<script language="Javascript">
function viewPayableItemList()
{
  document.payableItemDetail.submit();
}
</script>
</head>
<form name="payableItemDetail">
<input type="hidden" name="clientNumber" value="<%=clientVO.getClientNumber()%>">
<input type="hidden" name="event" value="viewPayableItemList">
</form>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
  <tr valign="top"><td>
    <h2> Payable Item Detail </h2>
    <table border="0" cellpadding="3" cellspacing="0">
    <tr>
      <td colspan="3">
      <%@include file="/client/ViewClientInclude.jsp"%>
      </td>
    </tr>
    <tr>
      <td class="label">Item ID:</td>
      <td class="dataValue" ><%=payItem.getPayableItemID()%></td>
    </tr>
    <tr>
      <td class="label">Payment Method:</td>
      <td class="dataValue" ><%=payItem.getPaymentMethod()!=null?payItem.getPaymentMethod().getDescription():"<span class=\"errorText\">No payment method defined</span>"%></td>
    </tr>
    <tr>
      <td class="label">Payment System:</td>
      <td class="dataValue" ><%=payItem.getPaymentSystem()==null?"":payItem.getPaymentSystem()%></td>
    </tr>
    <tr>
      <td class="label">Payment Method ID:</td>
      <td class="dataValue" ><%=payItem.getPaymentMethodID()==null?"":payItem.getPaymentMethodID()%></td>
    </tr>
    <tr>
      <td class="label">Source System:</td>
      <td class="dataValue" ><%=payItem.getSourceSystem().getAbbreviation()%></td>
    </tr>
    <tr>
      <td class="label">Source System Reference:</td>
      <td class="dataValue" ><%=payItem.getSourceSystemReferenceID()%></td>
    </tr>
    <tr>
      <td class="label">Sales Branch:</td>
      <td class="dataValue" ><%=payItem.getSalesBranchCode()%></td>
    </tr>
    <tr>
      <td class="label">Branch Region:</td>
      <td class="dataValue" ><%=payItem.getBranchNumber()%></td>
    </tr>
    <tr>
      <td class="label">Product Code:</td>
      <td class="dataValue" ><%=payItem.getProductCode()==null?"":payItem.getProductCode()%></td>
    </tr>
    <tr>
      <td class="label">Logon ID:</td>
      <td class="dataValue" ><%=payItem.getUserID()%></td>
    </tr>
    <tr>
      <td class="label">Create Date/Time:</td>
      <td class="dataValue" ><%=payItem.getCreateDateTime().formatLongDate()%></td>
    </tr>
    <tr>
      <td class="label">Start Date:</td>
      <td class="dataValue" ><%=DateUtil.formatShortDate(payItem.getStartDate())%></td>
    </tr>
    <tr>
      <td class="label">End Date:</td>
      <td class="dataValue" ><%=DateUtil.formatShortDate(payItem.getEndDate())%></td>
    </tr>
    <tr>
      <td class="label">Amount Payable:</td>
      <td class="dataValue" ><%=CurrencyUtil.formatDollarValue(payItem.getAmountPayable())%></td>
    </tr>
    <tr>
      <td class="label">Amount Outstanding:</td>
       <%
              String amountOutstanding = "";
              String errorDescription = "";
              try
              {
                amountOutstanding = CurrencyUtil.formatDollarValue(payItem.getAmountOutstanding());
              %>
                <td class="listItemPadded"><%=amountOutstanding%></td>
              <%
              }
              catch (ValidationException ex)
              {
                errorDescription = ex.getMessage();
              %>
                <td class="listItemPadded"><a title="<%=errorDescription%>">N/A</a></td>
              <%
              }
              %>
    </tr>
    <tr>
       <%
              String receiptNumber = "";
              errorDescription = "";
              try
              {
              	if (payItem.getReceiptNo() != null) {
              	  receiptNumber = payItem.getReceiptNo();
              	} else {              
	              Receipt receipt = payItem.getReceipt();
	              if (receipt != null) {
					receiptNumber = receipt.getReceiptNumber();
				  }
				}
				  
				if (!receiptNumber.equals("")) {
              %>
                <td class="label">Receipt Number:</td>              
                <td class="listItemPadded"><%=receiptNumber%></td>
              <%
                }                
              }
              catch (ValidationException ex)
              {
                errorDescription = ex.getMessage();
              %>
                <td class="listItemPadded"><a title="<%=errorDescription%>">N/A</a></td>
              <%
              }
              %>
    </tr>    
    <tr>
      <td class="label">Description:</td>
      <td class="dataValue" ><%=payItem.getDescription()%></td>
    </tr>
    </table>
    </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><input type="button" name="btnBack" value="Back" onClick="history.back();"></td>
          <td><input type="button" name="btnViewPayableList" value="View Payable List" onclick="viewPayableItemList()"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
