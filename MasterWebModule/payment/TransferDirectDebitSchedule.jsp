<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>

<%
PayableItemVO payableItem = (PayableItemVO)request.getAttribute("payableItem");
DirectDebitSchedule ddSchedule = null;

if (payableItem != null)
{
  ddSchedule = (DirectDebitSchedule)payableItem.getPaymentMethod();
}

%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<h2> Transfer Direct Debit Schedule</h2>

<form name="ddtransfer" method="post" action="<%=PaymentUIConstants.PAGE_PaymentUIC%>">
<input type="hidden" name="event">
<%
if (payableItem == null)
{
%>
<p>
  <table border="0">
    <tr>
      <td>Payable Item ID</td>
      <td>
        <input type="text" name="payableItemID">
      </td>
    </tr>
  </table>
  </form>
  <p>
  <p>
    <input type="button" name="getPayableItem" value="Get Payable Item" onclick="document.all.event.value='transferDirectDebitSchedule_getPayable';document.all.ddtransfer.submit();">
  </p>
<%
}
else
{
%>
<p class="helpText">
The transfer schedule facility will cancel the payable fee in the receipting system resulting from failed direct
debits and schedule the failed amount in the direct debit system.
</p>
<p>
  <input type="hidden" name="payableItemID" value="<%=payableItem.getPayableItemID()%>"/>
  <table border="0">
  <tr class="headerRow">
    <td class="listHeadingPadded">Payable Item ID</td>
    <td class="listHeadingPadded">Client Number</td>
    <td class="listHeadingPadded">Create Date</td>
    <td class="listHeadingPadded">Payment Method</td>
    <td class="listHeadingPadded">Schedule ID</td>
    <td class="listHeadingPadded">Amount Payable</td>
    <td class="listHeadingPadded">Outstanding Amount in Receipting</td>
    <td class="listHeadingPadded">Schedule Summary</td>
  </tr>
  <tr valign="top" class="<%=HTMLUtil.getRowType(0)%>">
    <td><%=payableItem.getPayableItemID()%></td>
    <td><%=payableItem.getClientNo()%></td>
    <td><%=payableItem.getCreateDateTime()%></td>
    <td><%=payableItem.getPaymentMethodDescription()%></td>
    <td><%=payableItem.getPaymentMethodID()%></td>
    <td><%=CurrencyUtil.formatDollarValue(payableItem.getAmountPayable())%></td>
    <td><%=CurrencyUtil.formatDollarValue(ddSchedule.getPayable().getAmountOutstanding())%></td>
    <td><pre><%=ddSchedule.toString()%></pre></td>
    </td>
  </tr>
</table>
</p>
<p>
  <input type="button" name="transferSchedule" value="Transfer Schedule" onclick="document.all.event.value='transferDirectDebitSchedule_transferSchedule';document.all.ddtransfer.submit();">
</p>
<%
}
%>

</form>

</body>

</html>
