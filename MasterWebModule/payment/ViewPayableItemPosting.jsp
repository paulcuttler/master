<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
UserSession userSession = UserSession.getUserSession(session);

PayableItemPostingVO payItemPosting = null;
PayableItemComponentVO payItemComponent = (PayableItemComponentVO) request.getAttribute("payableItemComponent");
Collection payableItemPostings = (Collection)request.getAttribute("payableItemPostings");
Iterator payablePostingIT = null;
%>
<html>
<head>
<title>Payable item posting</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
<script language="Javascript">
   function removePosting(radioButtonList)
   {
      document.all.event.value = 'removePosting';
      document.all.postingID.value = returnRadioButtonValue(radioButtonList);
      document.viewPostingList.submit();
   }
   function copyPosting(radioButtonList)
   {
      document.all.event.value = 'copyPosting';
      document.all.postingID.value = returnRadioButtonValue(radioButtonList);
      document.viewPostingList.submit();
   }
</script>

</head>
<body>
  <form name="viewPostingList" method="post" action="<%=PaymentUIConstants.PAGE_PaymentUIC%>">
  <input type="hidden" name="payableItemID" value="<%=payItemComponent.getPayableItemID()%>">
  <input type="hidden" name="payableItemComponentID" value="<%=payItemComponent.getPayableItemComponentID()%>">
  <input type="hidden" name="event">
  <input type="hidden" name="postingID">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
  <tr valign="top">
      <td>
        <h2>Payable Items Postings for <%=payItemComponent.getDescription()%>    Amount: <%=CurrencyUtil.formatDollarValue(payItemComponent.getAmount())%></h2>
        <%@include file="/client/ViewClientInclude.jsp"%>
        <div style="height:440; overflow:auto; border-width: 1; border-style: solid;">
        <table border="0" cellpadding="3" cellspacing="0">
<%

if (payableItemPostings != null && !payableItemPostings.isEmpty())
{
  payablePostingIT = payableItemPostings.iterator();
  int counter = 0;
  boolean first = true;
  while (payablePostingIT.hasNext())
  {
    payItemPosting = (PayableItemPostingVO) payablePostingIT.next();
    counter++;
    //this is actually the amount unearned
%>
          <tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
              <td class="listItemPadded" align="center">
                <input name="selectedLine" value="<%=payItemPosting.getPayableItemPostingID()%>" type="radio" <%=(first ? "CHECKED" : "")%>>
              </td>
            <td class="label" nowrap >Company</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getCompany()!=null?payItemPosting.getCompany().getDescription():""%></td>
            <td class="label" nowrap ><a title="postingID=<%=payItemPosting.getPayableItemPostingID()%>">Acc No</a></td>
            <td class='dataValue' nowrap><%=payItemPosting.getAccount()!=null?payItemPosting.getAccount().getAccountNumber().toString():"<span class=\"errorText\">No Account Number</span>"%></td>
            <td class="label" nowrap >Cost Centre</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getCostCentre()!=null?payItemPosting.getCostCentre():""%></td>
            <td class="label" nowrap >Sub Acc</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getSubAccountNumber()!=null?payItemPosting.getSubAccountNumber().toString():""%></td>
            <td class="label" nowrap >Amount</td>
            <td class='dataValue' nowrap align="right"><%=payItemPosting.getAmount()!=0?NumberUtil.formatValue(payItemPosting.getAmount()):""%></td>
          </tr>
<%
if (payItemPosting.getIncomeAccountNumber() != null)
{
%>
          <tr class="<%=HTMLUtil.getRowType(counter)%>">
            <td>&nbsp;</td>
            <td class="label" nowrap >Inc Acc No</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getIncomeAccountNumber().toString()%></td>
            <td class="label" nowrap >Inc Cost Centre</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getIncomeCostCentre()!=null?payItemPosting.getIncomeCostCentre():""%></td>
            <td class="label" nowrap >Inc Sub Acc No</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getIncomeSubAccountNumber()!=null?payItemPosting.getIncomeSubAccountNumber():""%></td>
            <td colspan="4">&nbsp;</td>
          </tr>
<%
}
%>          
          <tr class="<%=HTMLUtil.getRowType(counter)%>">
            <td>&nbsp;</td>
            <td class="label" nowrap >Effective Date</td>
            <td class="dataValue" nowrap><%=payItemPosting.getPostAtDate()!=null?DateUtil.formatShortDate(payItemPosting.getPostAtDate()):""%></td>
            <td class="label" nowrap >Posted Date</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getPostedDate()!=null?DateUtil.formatShortDate(payItemPosting.getPostedDate()):""%></td>
            <td class="label" nowrap >Journal No</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getJournalNumber()!=null?payItemPosting.getJournalNumber().toString():""%></td>
            <td class="label" nowrap >Create Date</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getCreateDate()!=null?payItemPosting.getCreateDate().toString():""%></td>
            <td class="label" nowrap >Journal Key</td>
            <td class='dataValue' nowrap ><%=payItemPosting.getJournalKey()!=null?payItemPosting.getJournalKey():""%></td>
          </tr>
          <tr class="<%=HTMLUtil.getRowType(counter)%>">
            <td>&nbsp;</td>
            <td class="label" nowrap >Description</td>
            <td class="dataValue" colspan="5"><%=payItemPosting.getDescription()!=null?payItemPosting.getDescription():""%></td>
          </tr>
          <%
          first = false;
  }
}
else
{
%>
          <tr>
            <td >&nbsp;</td>
            <td >No postings to display</td>
          </tr>
          <%
}
%>
        </table>
      </div>
      </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><input type="button" name="btnBack" value="Back" onClick="history.back();"></td>
           <%
           if (userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_PT_REMOVE_PAYABLEITEM))
           {
           %>
          <td><input type="button" name="btnRemove" value="Remove" onClick="removePosting(document.all.selectedLine)"></td>
          <td><input type="button" name="btnCopy" value="Copy" onClick="copyPosting(document.all.selectedLine)"></td>
          <%
          }
          %>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html>
