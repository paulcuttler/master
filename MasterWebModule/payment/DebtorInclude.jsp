<%@page import="com.ract.client.*"%>
<%@page import="com.ract.client.ui.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.finance.*"%>

<table border="0" cellspacing="0" cellpadding="0" width="400">
<tr>
<%
ClientVO debtorClient = (ClientVO)request.getAttribute("client");

//get the tramada profile code from the list of recorded transactions
Debtor debtor = debtorClient.getDebtor();
if (debtor == null)
{
%>
  <td>
    <a class="actionItem"
       title="Transfer debtor to Finance One"
       href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=createDebtor&clientNumber=<%=debtorClient.getClientNumber()%>">
       Transfer debtor to Finance One
    </a>
  </td>
<%
}
else
{
%>
  <td class="label" width="180" nowrap="nowrap">Debtor Created: </td><td width="220" class="dataValue" nowrap="nowrap"><%=debtor.getCreateDate().formatShortDate()%></td>
<%
}
%>
</tr>
</table>
