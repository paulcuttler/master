 function handleOnLoad()
 {
    return showPaymentType(document.all.paymentType.options[document.all.paymentType.selectedIndex].value);
 }

 function showPaymentType(payTypeCode)
 {
    // Get the cash type code given the payment type code
    var cashTypeCode = paymentCashTypeList[payTypeCode];

    if (cashTypeCode == 'C')
    {
       document.all.directDebitTable.style.display = "none";
       document.all.repairTable.style.display = "none";
       document.all.receiptingTable.style.display = "";
    }
    else if (cashTypeCode == 'D')
    {
       document.all.ddFromDebitAccountHeadingRow.style.display = "";
       document.all.bsbNumberRow.style.display = "";
       document.all.accountNumberRow.style.display = "";

       document.all.ddFromCCAccountHeadingRow.style.display = "none";
       document.all.cardNumberRow.style.display = "none";
       document.all.cardExpiryRow.style.display = "none";
       document.all.cardVerificationValue.style.display = "none";
       document.all.repairTable.style.display = "none";
       document.all.receiptingTable.style.display = "none";
       document.all.directDebitTable.style.display = "";
    }
    else if (cashTypeCode == 'R')
    {
       document.all.ddFromDebitAccountHeadingRow.style.display = "none";
       document.all.bsbNumberRow.style.display = "none";
       document.all.accountNumberRow.style.display = "none";
       document.all.repairTable.style.display = "none";

       document.all.ddFromCCAccountHeadingRow.style.display = "";
       document.all.cardNumberRow.style.display = "";
       document.all.cardExpiryRow.style.display = "";
       document.all.cardVerificationValue.style.display = "";

       document.all.directDebitTable.style.display = "";
       document.all.receiptingTable.style.display = "none";
       document.all.cardNumberLength.value = ptCCNumberSizeList[payTypeCode] + " digits";
    }
    else if (cashTypeCode == 'Repair')
    {
       document.all.directDebitTable.style.display = "none";
       document.all.receiptingTable.style.display = "none";
       document.all.repairTable.style.display = "";
    }
    else {
       document.all.directDebitTable.style.display = "none";
       document.all.receiptingTable.style.display = "none";
       document.all.repairTable.style.display="none";
    }

    return true;
 }


 // A regular expression to match spaces
 var reSpace  = / /gi;
 var reHyphen = /-/gi;

 function validateBsbNumber(fld)
 {
    var bsbNumValue = fld.value.replace(reSpace,"").replace(reHyphen,"");

    if (bsbNumValue.length != 6)
    {
       alert('The BSB number must contain exactly 6 digits. '+bsbNumValue.length+' entered.');
       setFocus(fld);
       return false;
    }

    var bsbNum = Number(bsbNumValue);
    if (isNaN(bsbNum))
    {
       alert('The BSB number must only contain digits, spaces or hyphens.');
       setFocus(fld);
       return false;
    }

    if (bsbNum < 0)
    {
       alert('The BSB number cannot have a negative sign.');
       setFocus(fld);
       return false;
    }

//      fld.value = bsbNumValue;
    return true;
 }

 function validateAccountNumber(fld)
 {
    var accNumValue = fld.value.replace(reSpace,"").replace(reHyphen,"");
    if (accNumValue == '')
    {
       alert('A account number must be entered.');
       setFocus(fld);
       return false;
    }

    if (accNumValue.length > 9)
    {
       alert('The account number cannot contain more than 9 digits. '+accNumValue.length+' entered.');
       setFocus(fld);
       return false;
    }

    var accNum = Number(accNumValue);
    if (isNaN(accNum))
    {
       alert('The account number must only contain digits.');
       setFocus(fld);
       return false;
    }

    if (accNum < 0)
    {
       alert('The account number cannot have a negative sign.');
       setFocus(fld);
       return false;
    }

//      fld.value = accNumValue;
    return true;
 }


 function validateCardNumber(fld)
 {
    var ccNumValue = fld.value.replace(reSpace,"").replace(reHyphen,"");

    var validNumerLength = ptCCNumberSizeList[document.all.paymentType.options[document.all.paymentType.selectedIndex].value];
    if (validNumerLength > 0 && ccNumValue.length != validNumerLength)
    {
       alert('The credit card number must contain exactly ' + validNumerLength + ' digits. '+ccNumValue.length+' entered.');
       setFocus(fld);
       return false;
    }

    if (ccNumValue == '')
    {
       alert('A credit card number must be entered.');
       setFocus(fld);
       return false;
    }

    var ccNum = Number(ccNumValue);
    if (isNaN(ccNum))
    {
       alert('The credit card number must only contain digits.');
       setFocus(fld);
       return false;
    }

    if (ccNum < 0)
    {
       alert('The credit card number cannot have a negative sign.');
       setFocus(fld);
       return false;
    }

//      fld.value = ccNumValue;
    return true;
 }

 function validateCardNumberWithOldValue(fld,
		                                 oldString)
 {
    var ccNumValue = fld.value.replace(reSpace,"").replace(reHyphen,"");
//alert(fld.value + "\n" + oldString.value + "\n" + ccNumValue);
    
    if(oldString.value != ""
       && fld.value==oldString.value)return true;
    var validNumerLength = ptCCNumberSizeList[document.all.paymentType.options[document.all.paymentType.selectedIndex].value];
    if (validNumerLength > 0 && ccNumValue.length != validNumerLength)
    {
       alert('The credit card number must contain exactly ' + validNumerLength + ' digits. '+ccNumValue.length+' entered.');
       setFocus(fld);
       return false;
    }

    if (ccNumValue == '')
    {
       alert('A credit card number must be entered.');
       setFocus(fld);
       return false;
    }

    var ccNum = Number(ccNumValue);
    if (isNaN(ccNum))
    {
       alert('The credit card number must only contain digits.');
       setFocus(fld);
       return false;
    }

    if (ccNum < 0)
    {
       alert('The credit card number cannot have a negative sign.');
       setFocus(fld);
       return false;
    }

//      fld.value = ccNumValue;
    return true;
 }
 
 
 function validateCardExpiry(fld)
 {
    var ccExpiryValue = fld.value.replace(reSpace,"");
    if (ccExpiryValue == '')
    {
       alert('A credit card expiry must be entered.');
       setFocus(fld);
       return false;
    }

    var month = 0;
    var year = 0;

    if (ccExpiryValue.length < 4)
    {
       alert('A credit card expiry must be in the format mm/yy.');
       setFocus(fld);
       return false;
    }
    else if (ccExpiryValue.length > 5)
    {
       alert('A credit card expiry must be in the format mm/yy.');
       setFocus(fld);
       return false;
    }
    else if (ccExpiryValue.length == 4)  // assume mmyy format
    {
       month = Number(ccExpiryValue.substr(0,2));
       year  = Number(ccExpiryValue.substr(2));
    }
    else  // assume mm/yy format
    {
       var elements = ccExpiryValue.split('/');
       month = Number(ccExpiryValue.substr(0,2));
       year  = Number(ccExpiryValue.substr(3));
    }

    if (isNaN(month))
    {
       alert('The credit card expiry month is not a valid number.');
       setFocus(fld);
       return false;
    }

    // Convert the month to a zero based value
    month--;

    if (month < 0 || month > 11)
    {
       alert('The credit card expiry month must be between 1 and 12.');
       setFocus(fld);
       return false;
    }

    if (isNaN(year))
    {
       alert('The credit card expiry year is not a valid number.');
       setFocus(fld);
       return false;
    }

    // The year is specified as two digits.  Add the current century.
    var fullYear = year + 2000;
    // Make sure that the credit card has not already expired.
    // Construct an end of the month date using the credit card expiry month and year
    var ccMonthEndDate = new Date(fullYear,month,getDaysInMonth(month,fullYear));

    // Make a date for the end of this month.
    var today = new Date();
    var todayMonth = today.getMonth();
    var todayYear = today.getYear();
    var todayMonthEndDate = new Date(todayYear,todayMonth,getDaysInMonth(todayMonth,todayYear));

    if (ccMonthEndDate < todayMonthEndDate)
    {
       alert('The credit card has already expired.');
       setFocus(fld);
       return false;
    }

    // The month and year are valid now reconstruct the field value in the correct format.
    var monthStr = '';
    var yearStr = '';
    month++;  // Convert back to 1 based value;
    if (month < 10)
       monthStr = '0' + month;
    else
       monthStr = String(month);

    if (year < 10)
       yearStr = '0' + year;
    else
       yearStr = String(year);

    fld.value = monthStr + '/' + yearStr;

    return true;
 }


 function validateAccountName(fld)
 {
    var accNameValue = fld.value.replace(reSpace,"");
    if (accNameValue == '')
    {
       alert('The account name must be entered.');
       setFocus(fld);
       return false;
    }

    return true;
 }


 function validateDayToDebit(fld)
 {
    var dayToDebitValue = fld.value.replace(reSpace,"");
    if (dayToDebitValue == '')
    {
       alert('A day to debit must be entered.');
       setFocus(fld);
       return false;
    }

    var dayToDebit = Number(dayToDebitValue);
    if (isNaN(dayToDebit))
    {
       alert('The day to debit must only contain digits.');
       setFocus(fld);
       return false;
    }

    if (dayToDebit < 1 || dayToDebit > 31)
    {
       alert('The day to debit must be between 1 and 31.');
       setFocus(fld);
       return false;
    }

    fld.value = dayToDebitValue;
    return true;
 }

 var submitted = false;

 function submitPaymentMethodForm(message,ddPolicyMessage)
 {
	 if (!submitted)
    {
       if (validateForm())
       {
    	   var ddDetails = "";
         var ddType = false;

      var cashTypeCode = paymentCashTypeList[document.all.paymentType.options[document.all.paymentType.selectedIndex].value];

      if (cashTypeCode == 'C' ||
          cashTypeCode == 'Repair')
      {
        // No field validation required.
      }
      else if (cashTypeCode == 'D')
      {
         ddDetails += "BSB Number: "+document.all.bsbNumber.value+"\n";
         ddDetails += "Account Number: "+document.all.accountNumber.value+"\n";
         var elt = document.all.frequencyOfDebit;
         alert(elt.nodeName);
         if (elt.nodeName == 'select')
        	 ddDetails += "Frequency: "+document.all.frequencyOfDebit.options[document.all.frequencyOfDebit.selectedIndex].value+"\n";
         else if (elt.nodeName == 'input')
        	 ddDetails += "Frequency: "+document.all.frequencyOfDebit.value+"\n";
         ddType = true;
      }
      else if (cashTypeCode == 'R')
      {
         ddDetails += "Card Number: "+formatCreditCardNumber(document.all.cardNumber.value)+"\n";
         ddDetails += "Card Expiry: "+document.all.cardExpiry.value+"\n";
         ddType = true;
      }
      else
      {
         alert('Unknown cash payment type "' + cashTypeCode + '". Unable to validate form.');
         return false;
      }

      if (cashTypeCode != 'C')
      {
        ddDetails += "Account Name: "+document.all.accountName.value+"\n";
        ddDetails += "Day To Debit: "+document.all.dayToDebit.value+"\n";
      }

         if (ddType)
         {
           message += "\n\n" + ddDetails;
           ddConfirm = confirm(message);
           if (ddConfirm)
           {
             if (ddPolicyMessage != '')
             {
               alert(ddPolicyMessage);
             }
           }
         }
         else
         {
           ddConfirm = true;
         }
         if (ddConfirm)
         {
           submitted = true;
           document.forms[0].submit();
         }
       }
    }
    else
       alert('The save button only needs to be clicked once!');
 }

   function validateForm()
   {
	   var valid = true;
      // Get the cash type code given the payment type code
      var cashTypeCode = paymentCashTypeList[document.all.paymentType.options[document.all.paymentType.selectedIndex].value];

      if (cashTypeCode == 'C' ||
          cashTypeCode == 'Repair')
      {
        // No field validation required.
      }
      else if (cashTypeCode == 'D')
      {
         valid = !valid ? false : validateBsbNumber(document.all.bsbNumber);
         if (!valid)
        	 alert('BSB is not valid');
         valid = !valid ? false : validateAccountNumber(document.all.accountNumber);
         if (!valid)
        	 alert('Account number is not valid');
      }
      else if (cashTypeCode == 'R')
      {
         valid = !valid ? false : validateCardNumberWithOldValue(document.all.cardNumber,
        		                                     document.all.oldCardNumber);
         if (!valid)
        	 alert('Card number is not valid');
         valid = !valid ? false : validateCardExpiry(document.all.cardExpiry);
         if (!valid)
        	 alert('Card expiry date is not valid');
      }
      else
      {
         alert('Unknown cash payment type "' + cashTypeCode + '". Unable to validate form.');
         valid = false;
      }

      if (cashTypeCode != 'C' &&
          cashTypeCode != 'Repair')
      {
        valid = !valid ? false : validateAccountName(document.all.accountName);
        if (!valid)
       	 alert('Account name is not valid');
        valid = !valid ? false : validateDayToDebit(document.all.dayToDebit);
        if (!valid)
       	 alert('Day to debit is not valid');
      }
      return valid;
   }

function formatCreditCardNumber(cardNumber)
{
  var formattedCreditCardNumber = '';
  var space = 4;
  if (cardNumber != null &&
      cardNumber.length > 0)
  {
    for (var x=0; x<cardNumber.length; x++)
    {
      if (x % space == 0)
      {
        formattedCreditCardNumber += ' ';
      }
      formattedCreditCardNumber += cardNumber.substring(x,x+1);
    }
  }
  return formattedCreditCardNumber
}
