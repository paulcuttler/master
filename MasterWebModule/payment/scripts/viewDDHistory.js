	dojo.require("dojo.lang.*");
	dojo.require("dojo.widget.Tree");
	dojo.require("dojo.widget.TreeNode");
	dojo.require("dojo.widget.TreeSelector");
	dojo.require("dojo.widget.TreeRPCController");
	dojo.require("dojo.io.*");
	dojo.require("dojo.event.*");
       dojo.require("dojo.widget.*");

var myTreeWidget;
var treeExists = false;
var treeSelector;
//var serverURL = 'http://localhost:8080/DgkTestUIC';
var serverURL = "/dduic";
var TreeBuilder = {
	buildTreeNodes:function (dataObjs, treeParentNode){
		for(var i=0; i<dataObjs.length;i++){
			var node =  dojo.widget.createWidget("TreeNode",{
				title:dataObjs[i].title,
				isFolder: true,
				objetId:dataObjs[i].objectId
			});
			treeParentNode.addChild(node);
			treeParentNode.registerChild(node,i);
		}
	},
	buildTree:function (sysNo,subSys){
		var placeHolder = document.getElementById("treePlaceHolder");
		var treeContainer = document.getElementById("myWidgetContainer");
		myTreeWidget = dojo.widget.createWidget("Tree",{widgetId:"myTreeWidget",
		                                                selector:'tSelector'});
        var rootNode = [{title:subSys + " " + sysNo}];
		this.buildTreeNodes(rootNode,myTreeWidget);
		treeContainer.replaceChild(myTreeWidget.domNode,placeHolder);
		treeExists=true;
	},
	replaceTree:function(sysNo,subSys)
	{
		var treeContainer = document.getElementById("myWidgetContainer");
        var placeHolder = document.createElement('span');
			placeHolder.id = "treePlaceHolder"
			placeHolder.innerHTML='Loading changed data';

		treeContainer.replaceChild(placeHolder,myTreeWidget.domNode);
        myTreeWidget.destroy();
		var rootNode = [{title:subSys + " " + sysNo}];
		myTreeWidget = dojo.widget.createWidget("Tree",{widgetId:"myTreeWidget",
		                                                selector:'tSelector'});

		this.buildTreeNodes(rootNode,myTreeWidget);
		treeContainer.replaceChild(myTreeWidget.domNode,placeHolder);
	}
}

function treeClickHandler(message)
{
  var tNode = message.node;
  var objectId = message.node.objectId;
  var tbl;
  var key1;
  var key2;
  var obArray;
  var keyArray;
 /* alert("You clicked " + tNode.title
       + "\n " + tNode.parent.title
	   + "\n " + tNode.objectId);*/
  if(objectId!="")
  {
    obArray = objectId.split("_");
	tbl = obArray[0];
	key1 = obArray[1];
	if(tbl!="ddPayable")key2=obArray[2];
	else key2 = "";
        if(tbl!="itemMemo")
        {
	   getDetails(tbl,key1,key2);
        }
  }
}
function getDetails(tableName,key1,key2)
{
    var tbl = document.getElementById('detailDisp');
	tbl.style.display='none';
    dojo.io.bind({url:serverURL,
	           handler:showDetail,
		   content:{event:"getDDDetail",
		            tableName:tableName,
		            key1:key1,
		            key2:key2},
                   mimetype:"application/json"});
}

function showDetail(type,data,evt)
{
    var e;
	var tbl = document.getElementById('detailDisp');
	tbl.style.display='';

	try
        {
        var bTable=dojo.widget.byId('vTable');
        bTable.store.setData(data);
        }
        catch(e)
        {
          alert("Error: " + e);
           //seem to get an error (which is actually a security warning)
           //everytime, but the procedure works, so do nothing.
	}

}

function loadTree(){
    var subSys = dojo.byId('subSystem').value;
    var sysNo = dojo.byId('systemNo').value;
//    var subSys = "<%=request.getAttribute("subSystem")%>";
//    var sysNo = <%=request.getAttribute("systemNo")%>;
    if(sysNo=="" || sysNo==null)
	{
	   alert("Enter a valid number");
	}
    if(subSys=="Insurance") subSys = "INS";
	else if(subSys=="Membership") subSys = "MEM";

    if(treeExists)TreeBuilder.replaceTree(sysNo,subSys);
    else TreeBuilder.buildTree(sysNo,subSys);
	var myRpcController = dojo.widget.createWidget("TreeRPCController",
	                                               {widgetId:"treeController",
			                                RPCUrl:serverURL+"?event=loadDDTree&systemNo="+sysNo+"&ddType="+subSys});
	myRpcController.onTreeClick = function(message)
	                               {var node = message.source;
		                            if (node.isExpanded){
			                          this.expand(node);
		                            }
									else {
			                          this.collapse(node);
		                            }

									};
	var treeContainer = document.getElementById("myWidgetContainer");
	treeContainer.appendChild(myRpcController.domNode);
	if(treeExists)myRpcController.listenTree(dojo.widget.manager.getWidgetById("myTreeWidget"));
	treeExists = true;
}
function init()
{
        dojo.event.topic.subscribe("nodeSelected", treeClickHandler);
        loadTree();
}

dojo.addOnLoad(init);
