<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="java.math.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.finance.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
Hashtable calculatedAmounts = (Hashtable)request.getAttribute("calculatedAmounts");

TreeMap sortedAmounts = null;

//create a new treemap with the natural sort order of
if (calculatedAmounts != null)
  sortedAmounts = new TreeMap(calculatedAmounts);

DateTime startDate = (DateTime)request.getAttribute("startDate");
DateTime endDate = (DateTime)request.getAttribute("endDate");

String calculationType = (String)request.getAttribute("calculationType");

DateTime effectiveDate = (DateTime)request.getAttribute("effectiveDate");
   
Boolean audit = (Boolean)request.getAttribute("audit");
Boolean capDaysToEarn = (Boolean)request.getAttribute("capDaysToEarn");
Boolean correctDuration = (Boolean)request.getAttribute("correctDuration");
String emailAddress = (String)request.getAttribute("emailAddress");
String selectionMode = (String)request.getAttribute("selectionMode");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html">
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">

   function submitForm()
   {
     document.forms[0].submit();
   }

   //showCalcFields
   function showCalcFields()
   {
     if (document.all.calcType.value == '<%=FinanceMgr.CALCULATION_TYPE_EARNED%>' ||
         document.all.calcType.value == '<%=FinanceMgr.CALCULATION_TYPE_UNEARNED%>')
     {
       hide(document.all.calcDateRange);
       show(document.all.calcDateOnly);
       enableButton(document.all.calcButton);
     }
     else if (document.all.calcType.value == '<%=FinanceMgr.CALCULATION_TYPE_EARNED_FOR_DATE_RANGE%>')
     {
       hide(document.all.calcDateOnly);
       show(document.all.calcDateRange);
       enableButton(document.all.calcButton);
     }
     else
     {
       hide(document.all.calcDateOnly);
       hide(document.all.calcDateRange);
       disableButton(document.all.calcButton);
     }

   }

  function hide(obj)
  {
    obj.style.display = 'none';
  }

  function show(obj)
  {
    obj.style.display = '';
  }

  function enableButton(obj)
  {
    obj.disabled = false;
  }

  function disableButton(obj)
  {
    obj.disabled = true;
  }

</script>

</head>

<body>

<h2>Calculate Amounts</h2>

<p>
<span class="helpTextSmall">
Note: this report may take a long time to complete depending on the criteria selected.
</span>
</p>

<p> <span class="infoText"> Choose a calculation type and enter the displayed criteria:</span>
<form action="<%=PaymentUIConstants.PAGE_PaymentUIC%>">

<input type="hidden" name="event" value="calculateAmounts">

<table>

  <tr>
  <td colspan="2">
  <table>
  <tr>
      <td class="label">Type of Calculation:</td>
      <td>
        <select name="calcType" onChange="showCalcFields();">
          <option selected value=""/>
          <option value="<%=FinanceMgr.CALCULATION_TYPE_EARNED_FOR_DATE_RANGE%>"><%=FinanceMgr.CALCULATION_TYPE_EARNED_FOR_DATE_RANGE%></option>
<%--
this has been removed as it does not really give an accurate indication of the earnings
for all time as it suggests.
          <option value="<%=FinanceMgr.CALCULATION_TYPE_EARNED%>"><%=FinanceMgr.CALCULATION_TYPE_EARNED%></option>
--%>
          <option value="<%=FinanceMgr.CALCULATION_TYPE_UNEARNED%>"><%=FinanceMgr.CALCULATION_TYPE_UNEARNED%></option>
        </select>
      </td>
  </tr>
  </table>
  </td>
  </tr>

  <!-- calculate range only -->

  <tr id="calcDateRange" style="display:none">
    <td colspan="2">
    <table>
     <tr>
       <td colspan="2" class="helpTextSmall">Enter a start date and end date and then press the "calculate" button to calculate the earnings for that period.</td>
     </tr>

     <tr>
            <td>Start Date:</td>
            <td>
              <input type="text" name="startDate" value="<%=startDate==null?"":startDate.formatShortDate()%>"><span class="helpTextSmall">dd/mm/yyyy</span></td>
     </tr>

     <tr>
            <td>End Date:</td>
            <td>
              <input type="text" name="endDate" value="<%=endDate==null?"":endDate.formatShortDate()%>"><span class="helpTextSmall">dd/mm/yyyy</span></td>
     </tr>
     <tr>
            <td colspan="2">
            <input type="checkbox" name="selectionMode"><a title="Uses the post at date by default"></a>Select records by the start date of the payable item.</a></td>
     </tr>
    </table>
    </td>
  </tr>

  <!-- calculate point in time -->

  <tr id="calcDateOnly" style="display:none">
  <td colspan="2">
  <table>
     <tr>
       <td colspan="2" class="helpTextSmall">Enter an effective date then press the "calculate" button to calculate the earned or unearned amounts for that period.</td>
     </tr>

     <tr>
            <td width="104">Effective Date:</td>
            <td width="460">
              <input type="text" name="effectiveDate" value="<%=effectiveDate==null?"":effectiveDate.formatShortDate()%>"><span class="helpTextSmall">dd/mm/yyyy</span></td>
     </tr>
   </table>
   </td>
  </tr>

  <tr>
    <td colspan="2"><input type="checkbox" name="audit">Create audit file?</td>
  </tr>

  <tr>
    <td colspan="2"><input type="checkbox" name="capDaysToEarn">Cap days to earn?</td>
  </tr>

  <tr>
    <td colspan="2"><input type="checkbox" name="correctDuration">Include full amount when earn from equals earn to?</td>
  </tr>

  <tr>
  <td colspan="2">
  <table>
  <tr>
    <td>Email Address for result and audit files:</td><td><input type="text" name="emailAddress"></td>
    </tr>
    </table>
    </td>
  </tr>

</table>

</form>

<input type="button" name="calcButton" value="Calculate" disabled onclick="submitForm();">

<hr/>

<%
BigDecimal totalAmount = new BigDecimal(0);

if (sortedAmounts != null)
{

TreeMap totalMap = new TreeMap();
%>

<table>
<tr>
<td>Audit file?</td><td><%=audit%></td>
</tr>
<% 
if (emailAddress != null &&
emailAddress.trim().length() > 0)
{
%>
<tr>
<td>Email Address</td><td><%=emailAddress%></td>
</tr>
<%
}
%>
<tr>
<td>Selection mode</td><td><%=selectionMode%></td>
</tr>
<tr>
<td>Cap days to earn</td><td><%=capDaysToEarn%></td>
</tr>
<tr>
<td>Include full amount when earn from equals earn to?</td><td><%=correctDuration%></td>
</tr>
</table>

<%
StringBuffer description = new StringBuffer();
if (calculationType.equals(FinanceMgr.CALCULATION_TYPE_EARNED_FOR_DATE_RANGE))
{
  description.append(calculationType);
  description.append(" for period ");
  description.append(startDate.formatShortDate());
  description.append(" to ");
  description.append(endDate.formatShortDate());
}
else
{
  description.append(calculationType);
  description.append(" as at ");
  description.append(effectiveDate.formatShortDate());
}
%>
<h2>Calculation Results (<%=description%>)</h2>

<table cellpadding="0" cellspacing="0">
  <tr class="headerRow">
    <td>Year</td>
    <td>Month</td>
    <td>Account Number</td>
    <td align="right">Amount</td>
  </tr>
<%

 BigDecimal amount = null;

 Object keys[] = (sortedAmounts.keySet()).toArray();

 String year = "";
 String month = "";
 Integer accountNumber = null;
 Integer incomeAccountNumber = null;

 BigDecimal accountAmount = new BigDecimal(0);
 Object key = null;
 String stringKey = "";

 //get the detail
 for (int i = 0; i < keys.length; i++)
 {

   key = keys[i];

   log("key = "+key);

   if (key instanceof Integer)
   {
     accountNumber = new Integer(key.toString());
     amount = (BigDecimal)calculatedAmounts.get(key);
   }
   else if (key instanceof String)
   {
      stringKey = key.toString();

      year = stringKey.substring(0,4); //YYYY
      month = stringKey.substring(4,6); //MM
      accountNumber = new Integer(stringKey.substring(6,10)); //1234

      amount = (BigDecimal)calculatedAmounts.get(stringKey);

      if (totalMap.get(accountNumber)==null)
      {
        totalMap.put(accountNumber,amount);
      }
      else
      {
        accountAmount = (BigDecimal)totalMap.get(accountNumber);
        accountAmount = accountAmount.add(amount);
        totalMap.put(accountNumber,accountAmount);
      }
   }

   if (amount != null)
   {
     totalAmount = totalAmount.add(amount);
   }

%>

<tr class="<%=HTMLUtil.getRowType(i)%>">
    <td><%=year%></td>
    <td><%=month%></td>
    <td><%=accountNumber.toString()%></td>
    <td align="right"><%=CurrencyUtil.formatDollarValue(amount)%></td>
  </tr>

<%
}
//get the sub totals
Object totalKeys[] = (totalMap.keySet()).toArray();
Object totalKey = null;
BigDecimal totalAccountAmount = null;
int keyLength = totalKeys.length;
if (keyLength > 0)
{
%>
  <tr>
    <td colspan="n3"></td>
    <td><hr/></td>
  <tr>
<%
}
for (int x = 0; x < keyLength; x++)
{
  totalKey = totalKeys[x];
  totalAccountAmount = (BigDecimal)totalMap.get(totalKey);
%>
  <tr>
    <td></td>
    <td></td>
    <td class="dataValue"><%=totalKey%></td>
    <td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(totalAccountAmount)%></td>
  </tr>
<%
}
//the grand total
%>

  <tr>
    <td colspan="3"></td>
    <td><hr/></td>
  <tr>

  <tr class="highlightRow">
    <td></td>
    <td></td>
    <td class="dataValue">Total</td>
    <td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(totalAmount)%></td>
  </tr>

</table>

<%
}
%>

</body>

</html>
