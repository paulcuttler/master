<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.ract.user.UserSession"%>
<%@page import="com.ract.security.Privilege"%>

<%
UserSession userSession = UserSession.getUserSession(session);

DirectDebitSchedule ddSchedule = (DirectDebitSchedule) request.getAttribute("ddSchedule");
ClientVO clientVO = (ClientVO)request.getAttribute("client");
MembershipMgr membershipMgr =  MembershipEJBHelper.getMembershipMgr();
List<MembershipVO> memList = (List<MembershipVO>) membershipMgr.findMembershipByClientNumber(clientVO.getClientNumber());
MembershipVO memVO = memList.get(0);

String rowClass = null;
String actionDesc = null;
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">
function printSchedule()
{
  var url = "<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>?event=printMembershipDirectDebitSchedule&directDebitScheduleID=<%=ddSchedule.getDirectDebitScheduleID()%>&clientNumber=<%=clientVO.getClientNumber()%>";
  window.open(url, '', 'width=250,height=150,toolbar=no,status=no,scrollbars=no,resizable=no')
}
function emailSchedule()
{
  //open window to enter email details
  var url = 'EmailAdviceForm.action?membershipId=<%=memVO.getMembershipID()%>&directDebitScheduleId=<%=ddSchedule.getDirectDebitScheduleID()%>&adviceType=<%=SystemParameterVO.EMAIL_ADVICE_DD%>';
  window.open(url, '', 'width=640,height=480,toolbar=no,status=no,scrollbars=no,resizable=no');
}


</script>

</head>
<body>

<table height="100%">
  <tr valign="top">
  <td>
<%
if (ddSchedule != null)
{
%>
  <h2>Direct Debit Schedule</h2>
<table>
  <tr>
      <td>
      <%@include file="/client/ViewClientInclude.jsp"%>
      </td>
  </tr>
</table>
<hr/>
<table>
  <tr>
    <td>Total Amount:</td><td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmount())%></td><td class="helpTextSmall">The original amount that was sent to the direct debit system.</td>
  </tr>
  <tr>
    <td>Amount Paid:</td><td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmountPaid())%></td><td class="helpTextSmall">The amount paid.</td>
  </tr>
  <tr>
    <td>Amount Outstanding:</td><td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmountOutstanding())%></td><td class="helpTextSmall">The amount owed.</td>
  </tr>
  <tr>
    <td>Amount Waiting:</td><td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmountWaiting())%></td><td class="helpTextSmall">The amount with a status of waiting.</td>
  </tr>
  <tr>
    <td>Amortised Amount:</td><td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(ddSchedule.getAmortisedAmount())%></td><td class="helpTextSmall">The amount to be spread across the period by the frequency.</td>
  </tr>
  <tr>
    <td>Immediate Amount:</td><td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(ddSchedule.getImmediateAmount())%></td><td class="helpTextSmall">The amount that is earned immediately. It is added to the first scheduled payment.</td>
  </tr>
  <tr>
    <td>Frequency:</td><td class="dataValue"><%=ddSchedule.getDirectDebitAuthority().getDeductionFrequency().getDescription()%></td><td></td>
  </tr>
  <tr>
    <td>Description:</td><td class="dataValue"><%=ddSchedule.getScheduleDescription()%></td><td></td>
  </tr>
  <%
  BigDecimal receiptingAmount = ddSchedule.getReceiptingAmount();
  if (receiptingAmount.compareTo(new BigDecimal(0)) > 0)
  {
  %>
  <tr class="<%=HTMLUtil.getHighlightRowClass()%>">
    <td>Receipting Amount</td><td class="dataValue"><%=CurrencyUtil.formatDollarValue(receiptingAmount)%></td>
  </tr>
  <%
  }
  %>
  <tr>
    <td>Number of Scheduled Items:</td><td class="dataValue"><%=ddSchedule.getScheduledItemCount()%></td><td></td>
  </tr>
</table>

<%
int itemCount = ddSchedule.getScheduledItemCount();
if (itemCount > 0)
{
%>

<!-- direct debit details -->
<div style="height:300; overflow:auto; border-width: 1; border-style: solid;">

<table width="100%" border="0" cellspacing="0" cellpadding="3">

<tr class="headerRow">
  <td class="listHeadingPadded">No.</td>
  <td class="listHeadingPadded">Create Date</td>
  <td class="listHeadingPadded">Create User Id</td>
  <td class="listHeadingPadded">Scheduled Date</td>
  <td class="listHeadingPadded">Processed Date</td>
  <td class="listHeadingPadded">Amount</td>
  <td class="listHeadingPadded">Status</td>
  <td class="listHeadingPadded">Receipt No.</td>
  <td class="listHeadingPadded">Fee Amount</td>
  <td class="listHeadingPadded">Transaction No.</td>
  <td class="listHeadingPadded">Action</td>
  <td class="listHeadingPadded">Replace No.</td>
  <td class="listHeadingPadded">Return Code</td>
  <td class="listHeadingPadded">Auth No.</td>
  <td class="listHeadingPadded">Return Date</td>
</tr>

<%
  DirectDebitScheduleItem ddItem = null;
  int row = 0;
  Collection scheduleItemList = ddSchedule.getScheduleItemList();
  //most recent first
  Collections.sort((List)scheduleItemList);
  int recNumber;
  boolean highlightThisRow;
  Iterator scheduleIt = scheduleItemList.iterator();
  while (scheduleIt.hasNext())
  {

    row++;
    ddItem = (DirectDebitScheduleItem)scheduleIt.next();
    recNumber = (ddItem.getReceiptNumber()==null?0:ddItem.getReceiptNumber().intValue());

    highlightThisRow = 		ddItem.getStatus().equals(DirectDebitItemStatus.SCHEDULED)
             		|| 	ddItem.getStatus().equals(DirectDebitItemStatus.DISHONOURED);

    if (   ddItem.isLetter()
        && highlightThisRow )
    {
      rowClass = HTMLUtil.getHighlightRowClass();
    }
    else
    {
      rowClass = HTMLUtil.getRowType(row);
    }

%>

<tr class="<%=rowClass%>">
  <td><%=row%></td>
  <td><%=DateUtil.formatShortDate(ddItem.getCreateDate())%></td>
  <td><%=ddItem.getCreateId()%></td>
  <td><%=DateUtil.formatShortDate(ddItem.getScheduledDate())%></td>
  <td><%=DateUtil.formatShortDate(ddItem.getProcessedDate())%></td>
  <td align="right" ><%=CurrencyUtil.formatDollarValue(ddItem.getAmount())%></td>
  <td><%=ddItem.getStatus().getDescription()%></td>
  <td><%=recNumber==0?"":String.valueOf(recNumber)%></td>
  <td align="right" ><%=CurrencyUtil.formatDollarValue(ddItem.getFeeAmount())%></td>
  <td><%=ddItem.getTransactionNumber().intValue()==0?"":String.valueOf(ddItem.getTransactionNumber())%></td>
  <td><%=ddItem.getActionDescription()%></td>
  <td><%=ddItem.getReplaceNumber().intValue()==0?"":String.valueOf(ddItem.getReplaceNumber())%></td>
  <td><%=ddItem.getReturnCode()%></td>
  <td><%=ddItem.getAuthorisationNumber()%></td>
  <td><%=ddItem.getReturnDate()==null?"":ddItem.getReturnDate().toString()%></td>
</tr>

<%
  }
%>
</table>
</div>
<%
}
else
{
%>

<span class="helpText ">No scheduled payments found.</span>

<%
}

}//end off dd schedule
else
{
%>
<span class="errorText ">No schedule attached.</span>
<%
}
%>

  </td>
  </tr>

  <tr valign="bottom">
    <td>
      <table>
        <tr>
          <td><input type="button" name="btnBack" value="Back" onClick="history.back();"></td>
          <td><input type="button" name="btnPrint" value="Print" onClick="printSchedule();"></td>          
          <td><input type="button" name="btnPrint" value="Email" onClick="emailSchedule();"></td>
<%
if (userSession != null
		 		&&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CANCEL_DD))
		 		{
	if (ddSchedule != null && (ddSchedule.isCurrent() || ddSchedule.isRenewalVersion())) {
%>        
          <td>
	          <form action="PaymentUIC" method="get" style="display: inline;" onsubmit="return confirm('Are you sure?');">
	          	<input type="hidden" name="event" value="cancelDirectDebitSchedule" />
	          	<input type="hidden" name="membershipID" value="<%= memVO.getMembershipID() %>" />
	          	<input type="hidden" name="ddScheduleID" value="<%= ddSchedule.getDirectDebitScheduleID() %>" />
	          	<input type="submit" name="btnCancel" value="Cancel Schedule" />
	          </form>
          </td>
<%
	}
	if(false // disabled
       && ddSchedule != null
	   && ddSchedule.isFailed()
	   && !memVO.isGroupMember()
	   && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN)) //only admin users
	{%>
	<td> 
	    <form  action="MembershipUIC" method="get" style="display: inline;" onsubmit="return confirm('About to cancel the membership. \nAre you sure?');">
	      <input type="hidden" name="event" value="cancelMembership"/>
	      <input type="hidden" name="membershipID" value="<%=memVO.getMembershipID() %>"/>
          <input type="hidden" name="forceCancelDueToFailedDD" value="true"/>  
          <input type="submit" name="btnCancelMembership" value="Force Cancel Membership" />
        </form>  
	</td>
	<%}   
}
 %>
        </tr>
      </table>
    </td>
  </tr>

</table>

</body>
</html>
