<%@ taglib prefix="s" uri="/struts-tags" %>

<%@page import="com.ract.payment.FinanceVoucher"%>
<%@page import="com.ract.client.ClientVO"%>
<%@ page import="com.ract.payment.finance.*"%>
<%@ page import="com.ract.common.*"%>

<html>
<head>
<s:head/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet" type="text/css"/>
</head>

<%
ClientVO client = (ClientVO)request.getAttribute("client");
FinanceVoucher voucher = (FinanceVoucher)request.getAttribute("voucher");
%>

<body>
<s:form theme="simple">
<h1>
Voucher Details
</h1>
<table>
<tr><td>  Disposal Id         </td><td class="dataValue"><s:property value="voucher.disposalId"/></td></tr>
<tr><td>  Approver         </td><td class="dataValue"><s:property value="voucher.approvingUserId"/></td></tr>

<s:if test='voucher.status == "A"'>
<tr><td>  Status</td><td class="dataValueGreen">Approved</td></tr>
<tr><td>  Process Date</td><td><s:date name="voucher.processDate" format="dd/MM/yyyy HH:mm:ss"/></td></tr>
</s:if>
<s:elseif test='voucher.status == "R"'>
<tr><td>  Status        </td><td class="dataValueRed">Rejected</td></tr>
<tr><td>  Process Date</td><td><s:date name="voucher.processDate" format="dd/MM/yyyy HH:mm:ss"/></td></tr>
</s:elseif>
<s:else>
<tr><td>Status</td><td>Not processed</td></tr>
</s:else>

<tr><td><h2>Request:</h2></td><td></td></tr>
<tr><td>  Disposal Type           </td><td class="dataValue"><s:property value="voucher.disposalType"/></td></tr>
<tr><td>  Disposal Mode           </td><td class="dataValue"><s:property value="voucher.disposalMode"/></td></tr>
<tr><td>  Client Number   </td><td class="dataValue"><s:property value="voucher.clientNumber"/></td></tr>
<tr><td>  Source System         </td><td class="dataValue"><s:property value="voucher.sourceSystem"/></td></tr>
<tr><td>  Reference Number   </td><td class="dataValue"><s:property value="voucher.sourceSystemReference"/></td></tr>
<tr><td>  Create Date        </td><td class="dataValue"><s:date name="voucher.createDate" format="dd/MM/yyyy HH:mm:ss"/></td></tr>
<tr><td>  Amount             </td><td class="dataValue"><s:text name="format.money"><s:param name="value" value="voucher.disposalAmount"/></s:text></td></tr>
<tr><td>  Fee Amount             </td><td class="dataValue"><s:text name="format.money"><s:param name="value" value="voucher.feeAmount"/></s:text></td></tr>
<tr><td>  Creator               </td><td class="dataValue"><s:property value="voucher.userId"/></td></tr>

<tr><td>  Particulars        </td><td class="dataValue"><s:property value="voucher.particulars"/></td></tr>
<tr><td>  Reason code </td><td class="dataValue"><s:property value="voucher.disposalReason"/></td></tr>

<!--  TODO convert to constants -->

<s:if test="voucher.disposalType == 'EFT to account'">

<tr><td>  Bank Code           </td><td class="dataValue"><s:property value="voucher.bankCode"/></td></tr>
<tr><td>  BSB Number           </td><td class="dataValue"><s:property value="voucher.bsb"/></td></tr>
<tr><td>  Account Number           </td><td class="dataValue"><s:property value="voucher.bankAccountNumber"/></td></tr>
<tr><td>  Payee Name </td><td class="dataValue"><s:property value="voucher.payeeName"/></td></tr>

</s:if>

<s:elseif test="voucher.disposalType == 'Cheque to third party'">

<tr><td>  Cheque Name </td><td class="dataValue"><s:property value="voucher.payeeName"/></td></tr>
<tr><td>  Address 1          </td><td class="dataValue"><s:property value="voucher.address1"/></td></tr>
<tr><td>  Address 2          </td><td class="dataValue"><s:property value="voucher.address2"/></td></tr>
<tr><td>  State           </td><td class="dataValue"><s:property value="voucher.state"/></td></tr>
<tr><td>  Postcode           </td><td class="dataValue"><s:property value="voucher.postcode"/></td></tr>

</s:elseif>

<s:elseif test="voucher.disposalType == 'Cheque to member'">

<tr><td>  Member Name </td><td class="dataValue"><s:property value="voucher.payeeName"/></td></tr>
<tr><td>  Address 1          </td><td class="dataValue"><s:property value="voucher.address1"/></td></tr>
<tr><td>  Address 2          </td><td class="dataValue"><s:property value="voucher.address2"/></td></tr>
<tr><td>  State           </td><td class="dataValue"><s:property value="voucher.state"/></td></tr>
<tr><td>  Postcode           </td><td class="dataValue"><s:property value="voucher.postcode"/></td></tr>

</s:elseif>

<s:elseif test="voucher.disposalType == 'Write off'">

<tr><td>  Cost Centre           </td><td class="dataValue"><s:property value="voucher.costCentre"/></td></tr>
<tr><td>  Account Number           </td><td class="dataValue"><s:property value="voucher.writeOffAccountNumber"/></td></tr>

</s:elseif>

<s:elseif test="voucher.disposalType == 'Transfer to member'">

<tr><td>  Transfer Member number           </td><td class="dataValue"><s:property value="voucher.tfrMembershipNumber"/></td></tr>

</s:elseif>

<%
if (voucher.isFinanceSystem()) 
{
%>

<tr><td>&nbsp;</td></tr>
<tr><td><h2>Finance System Details:</h2></td><td></td></tr>
<tr><td>  Payee Number       </td><td class="dataValue"><s:property value="voucher.payeeNo"/></td></tr>
<tr><td>  Voucher Number     </td><td class="dataValue"><s:property value="voucher.voucherId"/></td></tr>
<tr><td></td><td></td></tr>
<tr><td><h3>Payee Details</h3></td><td></td></tr>
<tr><td>Payee Name       </td><td class="dataValue"><s:property value="payee.payeeName"/></td></tr>
<tr><td>Address          </td><td class="dataValue"><s:property value="payee.address1"/>&nbsp;
                                                    <s:property value="payee.address2"/>&nbsp;
                                                    <s:property value="payee.address3"/>&nbsp;
                                                    <s:property value="payee.suburb"/> <s:property value="payee.getState"/> 
                                                    <s:property value="payee.getPostcode"/>
</td></tr>

<s:if test="%{voucher.eft}">
<tr><td><h3>EFT Details</h3></td><td></td></tr>
<tr><td>Bank             </td><td class="dataValue"><s:property value="payee.bankCode"/></td></tr>
<tr><td>BSB              </td><td class="dataValue"><s:property value="payee.bsb"/></td></tr>
<tr><td>Account Number   </td><td class="dataValue"><s:property value="payee.accountNo"/></td></tr>
</s:if>

<tr><td><h3>Payment Details</h3></td><td></td></tr>
<tr><td>Invoice Number         </td><td class="dataValue"><s:property value="cheque.invoice"/></td></tr>
<tr><td>Payment Date           </td><td class="dataValue"><s:date name="cheque.chequeDate" format="dd/MM/yyyy HH:mm:ss"/></td></tr>
<tr><td>Payment Number         </td><td class="dataValue"><s:property value="cheque.chequeNumber"/></td></tr>
<tr><td>Payment Amount         </td><td class="dataValue"><s:text name="format.money"><s:param name="value" value="cheque.chequeAmount"/></s:text></td></tr>

<%
}
%>
</table>
</s:form>
</body>
</html>
