<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.finance.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<%
String financeOneAccount = (String)request.getAttribute("financeOneAccount");
%>

<h2>Translate Finance Codes</h2>
<form action="/PaymentUIC">
<input type="hidden" name="event" value="translateAccountCodes"/>
<table>
  <tr>
    <td>Entity</td><td><input name="entity" type="text"/></td>
  </tr>
  <tr>
    <td>Account Code</td><td><input name="accountCode" type="text"/></td>
  </tr>
  <tr>
    <td>Sub Account Code</td><td><input name="subAccountCode" type="text"/></td>
  </tr>
  <tr>
    <td>Cost Centre</td><td><input name="costCentre" type="text"/></td>
  </tr>


    <tr>
      <td><%=FinanceHelper.formatFinanceOneAccount(financeOneAccount)%></td>
    </tr>
   <tr>
      <td><input type="submit"/></td>
     </tr>

    </table>

    </body>
</form>
</html>
