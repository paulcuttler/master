<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.security.ui.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="com.ract.payment.receipting.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
Integer cNumber = (Integer) request.getAttribute("clientNumber");
Collection receiptingHistoryList = (Collection)request.getAttribute("receiptingHistoryList");

ClientVO clientVO = null;

// If the client VO is not passed then look it up.
if (clientVO == null)
{
   ClientMgr cMgr = ClientEJBHelper.getClientMgr();
   clientVO = cMgr.getClient(cNumber);
   request.setAttribute("client",clientVO);
}
%>

<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
  <tr valign="top">
      <td>
          <h2>Receipting History for <%=cNumber%></h2>
          <%@include file="/client/ViewClientInclude.jsp"%>
          <div style="height:400; overflow:auto; border-width: 1; border-style: solid;">
          <table border="0" cellpadding="1" cellspacing="0" >

<%
int counter = 0;
if (receiptingHistoryList != null && receiptingHistoryList.size() > 0)
{
%>
<tr class="headerRow">
  <td class="listHeadingPadded">System Name</td>
  <td class="listHeadingPadded">Sequence Number</td>
  <td class="listHeadingPadded">Description</td>
  <td class="listHeadingPadded">Create Date</td>
  <td class="listHeadingPadded">Source System</td>
  <td class="listHeadingPadded">Product Code</td>
  <td class="listHeadingPadded">Sales Branch</td>
  <td class="listHeadingPadded">User ID</td>
  <td class="listHeadingPadded">Amount</td>
  <td class="listHeadingPadded">Balance</td>
  <td class="listHeadingPadded">Product Qualifier</td>
  <td class="listHeadingPadded">Receipting Information</td>
</tr>
<%
  Payable payable = null;
  Collection receiptLineList = null;
  ReceiptLine receiptLine = null;
  Collection receiptList = null;
  Iterator payList = receiptingHistoryList.iterator();
  while (payList.hasNext())
  {
    counter++;
    payable = (Payable)payList.next();
    receiptList = payable.getReceiptList();
%>
<tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
  <td><%=payable.getSystemName()%></td>
  <td><%=payable.getSequenceNumber()%></td>
  <td><%=payable.getDescription()%></td>
  <td><%=payable.getSystemName().equals(SourceSystem.RECEPTOR.getAbbreviation()) || payable.getSystemName().equals(SourceSystem.ECR.getAbbreviation()) ? " - " : payable.getCreateDate()%></td>
  <td class="dataValue"><%=payable.getSourceSystem()%></td>
  <td><%=payable.getProductCode()%></td>
  <td><%=payable.getSystemName().equals(SourceSystem.ECR.getAbbreviation()) ? " - " : payable.getSalesBranchCode()%></td>
  <td><%=payable.getSystemName().equals(SourceSystem.ECR.getAbbreviation()) ? " - " : payable.getCreateUserID()%></td>
  <td align="right"><%=CurrencyUtil.formatDollarValue(payable.getAmount())%></td>
  <td align="right"><%=CurrencyUtil.formatDollarValue(payable.getAmountOutstanding())%></td>
  <td><%=payable.getProductQualifier()==null?"":payable.getProductQualifier().toString()%></td>
  <td>
<%
if (payable.getReceiptNo() != null) {
%>
  <%=payable.getReceiptNo()%>
<%
} 
else if (receiptList != null && receiptList.size() > 0)
{
%>
<table>
<%
  Iterator recIt = receiptList.iterator();
  Receipt receipt = null;
  while (recIt.hasNext())
  {
    receipt = (Receipt)recIt.next();
%>
    <tr>
      <td class="dataValueItalic"><%=receipt.getReceiptNumber()%></td>
    </tr>
    <tr>
      <td><a title="back date is <%=receipt.getBackDate()%>"><%=receipt.getCreateDate()%></a></td>
    </tr>
    <tr>
      <td><%=receipt.getReceiptName()%></td>
    </tr>
    <tr>
      <td><%=CurrencyUtil.formatDollarValue(receipt.getReceiptAmount())%></td>
    </tr>
<%
    //get receipt line list
    try
    {
      receiptLineList = receipt.getReceiptLineList();
      if (receiptLineList != null)
      {
        Iterator receiptLineIt = receiptLineList.iterator();
        while (receiptLineIt.hasNext())
        {
          receiptLine = (ReceiptLine)receiptLineIt.next();
%>
    <tr>
      <td><%=receiptLine.getLineNumber()%>. <%=receiptLine.getPaymentTypeCode()%> - <%=CurrencyUtil.formatDollarValue(receiptLine.getAmountPaid())%></td>
    </tr>
<%
        }
      }
    }
    catch (Exception e)
    {
      //ignore
    }
  }
%>
</table>
<%
}
%>
  </td>
</tr>
<%
  }
}
else
{
%>
  <tr>
    <td colspan="11" class="helpText">No receipting history found.</td>
  </tr>
<%
}
%>

          </table>
          </div>
      </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><input type="button" name="btnBack" value="Back" onClick="history.back();"></td>
        </tr>
      </table>
    </td>
  </tr>
</form>
</table>
</body>
</html>
