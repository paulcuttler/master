<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.security.ui.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.math.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<title>
</title>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<%

System.out.println("1");

Collection postingsSet = (Collection)request.getAttribute("postingsSet");
String journalKey = (String)request.getAttribute("journalKey");
System.out.println("2");

%>

<h2>Payment System Postings Summary</h2>

<table>
<tr>
  <td>Journal Key</td><td class="dataValue"><%=journalKey%></td><td></td>
</tr>
</table>

<%
System.out.println("3");

if (postingsSet != null && postingsSet.size() != 0)
{
Iterator postingsIt = postingsSet.iterator();
int counter = 0;

%>

<table border="0" cellpadding="0" cellspacing="0">
<tr class="headerRow">
  <td class="listHeadingPadded"><a title="The posted date">Entered Date</a></td>
  <td class="listHeadingPadded">Effective Date</td>
  <td class="listHeadingPadded">Account Number</td>
  <td class="listHeadingPadded">Client Number</td>
  <td class="listHeadingPadded">Transaction Reference</td>
  <td class="listHeadingPadded">Narration</td>  
  <td align="right" class="listHeadingPadded">Amount</td>
</tr>

<%
BigDecimal total = new BigDecimal(0);
BigDecimal amount = new BigDecimal(0);
String lineAccountNumber = "";
System.out.println("4");
while (postingsIt.hasNext())
{
  counter++;
  PostingSummary ps = (PostingSummary)postingsIt.next();
  lineAccountNumber = ps.getAccountNumber();
  amount = ps.getAmount();
  total = total.add(amount);
  
System.out.println("5 "+amount+" "+total);  
%>
<tr class="<%=HTMLUtil.getRowType(counter)%>">
  <td class="dataValue"><%=ps.getEnteredDate()%></td>
  <td class="dataValue"><%=ps.getEffectiveDate()%></td>
  <td class="dataValue"><%=lineAccountNumber==null||lineAccountNumber.equals("")?"<span class=\"errorText\">No account no. defined</span>":lineAccountNumber%></td>
  <td align="center" class="dataValue"><%=ps.getClientNumber()!=null?ps.getClientNumber():""%></td>
  <td align="center" class="dataValue"><%=ps.getTransactionReference()!=null?ps.getTransactionReference():""%></td>
  <td class="dataValue"><%=ps.getNarration()!=null?ps.getNarration():""%></td>
  <td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(amount)%></td>
</tr>
<%
}
System.out.println("6");
%>

<tr class="highlightRow">
  <td>Total</td>
  <td colspan="7" align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(total)%></td>
</tr>

</table>
 
<%
}
else
{
%>

<p><span class="helpText">No postings were found matching the criteria.</span></p>

<%
}
%>
</body>

</html>
