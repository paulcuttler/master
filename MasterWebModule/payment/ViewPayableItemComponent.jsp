<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
UserSession userSession = UserSession.getUserSession(session);

PayableItemComponentVO payComponent = null;

Integer payableItemID = (Integer) request.getAttribute("payableItemID");

Collection payableItemComponents = (Collection)request.getAttribute("payableItemComponents");

Iterator payableComponentsIT = null;
String selected = "checked ";
%>

<html>

<head>
<title>
</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="Javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
<script language="Javascript">
function viewPosting(radioButtonList)
{
    document.all.payableItemComponentID.value = returnRadioButtonValue(radioButtonList);
    document.all.event.value = 'viewPayableItemComponent_viewPosting';
    document.all.viewPayableItemComponent.submit();
}

function reverseComponent(radioButtonList)
{
    document.all.componentID.value = returnRadioButtonValue(radioButtonList);
    document.all.event.value = 'reverseComponent';
    document.viewPayableItemComponent.submit();
}

</script>
</head>

<body>
<form name="viewPayableItemComponent" method="post" action="<%= PaymentUIConstants.PAGE_PaymentUIC%>">
<input type="hidden" name="event">
<input type="hidden" name="componentID">
<input type="hidden" name="payableItemID" value='<%=payableItemID%>'>
<input type="hidden" name="payableItemComponentID">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
  <tr valign="top">
      <td>
              <h2>Payable Items Components</h2>
              <%@include file="/client/ViewClientInclude.jsp"%>
              <div style="height:400; overflow:auto; border-width: 1; border-style: solid;">
                <table border="0" cellpadding="0" cellspacing="0" >
<%
if (payableItemComponents != null && !payableItemComponents.isEmpty())
{
%>
                  <tr class="headerRow">
                    <td class="listHeadingPadded" align="center"></td>
                    <td class="listHeadingPadded">Description</td>
                    <td class="listHeadingPadded" align="right">Amount</td>
                    <td class="listHeadingPadded">Reference</td>
                    <td class="listHeadingPadded" align="center">Balanced?</td>
                  </tr>
<%
  payableComponentsIT = payableItemComponents.iterator();
  int counter = 0;
  boolean first = true;
  while (payableComponentsIT.hasNext())
  {
    counter++;
    payComponent = (PayableItemComponentVO) payableComponentsIT.next();

%>
            <tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
              <td class="listItemPadded" align="center">
                <input name="selectedLine" value="<%=payComponent.getPayableItemComponentID()%>" type="radio" <%=(first ? "CHECKED" : "")%>>
              </td>
                    <td class="listItemPadded"><a title="componentID=<%=payComponent.getPayableItemComponentID()%>"><%=payComponent.getDescription()%></a></td>
                    <td class="listItemPadded" align="right"><%=CurrencyUtil.formatDollarValue(payComponent.getAmount())%></td>
                    <td class="listItemPadded"><%=payComponent.getSourceSystemReference()%></td>
                    <td class="listItemPadded" align="center"><%=(payComponent.doPostingsBalance()?"Yes":"<span class=\"errorTextSmall\">No</span>")%></td>
                  </tr>

<%
    if ("checked".equals(selected))
    {
      selected = "";
%>
      <input type="hidden" name="payableItemComponentID" value='<%=payComponent.getPayableItemComponentID()%>'>
<%
    }
    first = false;
  }
}
else
{
%>
              <tr>
                <td class="helpText">No payable item components to display</td>
              </tr>
<%

}
%>
          </table>
      </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><input type="button" name="btnBack" value="Back" onClick="history.back();"></td>
          <td><input type="button" value="Postings" onclick="viewPosting(document.all.selectedLine)"></td>
          <%
          if (userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_PT_REMOVE_PAYABLEITEM))
      	  {
          %>
          <td><input type="button" value="Reverse" onclick="reverseComponent(document.all.selectedLine,'viewPayableItem_viewPosting')"></td>
          <%
          }
          %>
        </tr>
      </table>
      </div>
    </td>
  </tr>
</table>
</form>
</body>
</html>
