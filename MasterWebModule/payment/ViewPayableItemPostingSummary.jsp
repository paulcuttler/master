<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.util.*"%>

<%
PayableItemComponentVO componentVO;
Vector postingList;
PayableItemPostingVO postingVO;
Vector allPostingList = new Vector();
PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
Integer payableItemID = new Integer(request.getParameter("payableItemID"));
PayableItemVO payableItemVO = paymentMgr.getPayableItem(payableItemID);
ClientVO clientVO = null;
Iterator picIt = null;
if (payableItemVO != null) {
   // Get the clientVO and attach it to the request so the include page can get it.
   clientVO = payableItemVO.getClient();
   request.setAttribute("client",clientVO);
   // Create a list of all of the postings on all of the components
   Collection componentList = payableItemVO.getComponents();
   if (componentList != null) {
       picIt = componentList.iterator();
       while (picIt.hasNext())
       {
         componentVO = (PayableItemComponentVO) picIt.next();
         postingList = componentVO.getPostings();
         if (postingList != null) {
            for (int k = 0; k < postingList.size(); k++) {
               postingVO = (PayableItemPostingVO) postingList.get(k);
               allPostingList.add(postingVO);
            }
         }
      }
   }
   // Now sort all of the postings into ascending date order.
   // We assume that the postings that become effective on the same date belong to the same set.
   Collections.sort(allPostingList);
}
%>
<html>
<head>
<title>ViewPayableItemPostingSummary</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<h2>Payable Item Posting Summary</h2>
For payable item created <%=payableItemVO.getCreateDateTime().formatLongDate()%>
<%@include file="/client/ViewClientInclude.jsp"%>

<%
if (payableItemVO == null) {
%>
<hr>
No payable item!
<%
}
else if (allPostingList.isEmpty()) {
%>
<hr>
No postings!
<%
}
else{
%>
<table border="0" cellpadding="3" cellspacing="0">
<%
   int rowCount = 0;
   DateTime lastEffectiveDate = null;
   for (int i = 0; i < allPostingList.size(); i++) {
      postingVO = (PayableItemPostingVO) allPostingList.get(i);
      rowCount = i + 1;
      // Put a divider between sets of postings
      if (!postingVO.getPostAtDate().equals(lastEffectiveDate)){
         // Show a line between sets of postings
         lastEffectiveDate = postingVO.getPostAtDate();
%>
<tr>
   <td colspan="9"><hr></td>
</tr>
<tr>
   <td class="label" nowrap>Effective</td>
   <td class="dataValue" nowrap><%=DateUtil.formatShortDate(lastEffectiveDate)%></td>
   <td width="5"></td>   
   <td class="label" nowrap>Create Date</td>
   <td class="dataValue" nowrap colspan="5"><%=postingVO.getCreateDate()==null?"":postingVO.getCreateDate().toString()%></td>
</tr>
<tr class="headerRow">
   <td class="listHeadingPadded" nowrap>Account</td>
   <td class="listHeadingPadded" nowrap>Amount</td>
   <td></td>
   <td class="listHeadingPadded" nowrap colspan="7">Detail</td>
</tr>
<%
      }
%>
<tr class="<%=HTMLUtil.getRowType(rowCount)%>">
   <td class='dataValue' nowrap>
      <a title="posting ID=<%=postingVO.getPayableItemPostingID()%> on component ID=<%=postingVO.getPayableItemComponentID()%>">
         <%=postingVO.getAccount()!=null?postingVO.getAccount().getAccountNumber().toString():"<span class=\"errorText\">No Account Number</span>"%>
      </a>
   </td>
   <td class='dataValue' nowrap align="right"><%=postingVO.getAmount()!=0?NumberUtil.formatValue(postingVO.getAmount()):""%></td>
   <td width="5"></td>
   <td class="label" nowrap>Cost Centre:</td>
   <td class='dataValue' nowrap ><%=postingVO.getCostCentre()!=null?postingVO.getCostCentre():""%></td>
   <td class="label" nowrap>Sub A/c:</td>
   <td class='dataValue' nowrap ><%=postingVO.getSubAccountNumber()!=null?postingVO.getSubAccountNumber().toString():""%></td>
   <td class="label" nowrap>Company:</td>
   <td class='dataValue' nowrap ><%=postingVO.getCompany()!=null?postingVO.getCompany().getDescription():""%></td>
</tr>
<tr class="<%=HTMLUtil.getRowType(rowCount)%>">
   <td></td>
   <td></td>
   <td></td>
   <td class="label" nowrap>Description:</td>
   <td class="dataValue" nowrap colspan="5"><%=postingVO.getDescription()!=null?postingVO.getDescription():""%></td>
</tr>
<%
      if (postingVO.getIncomeAccountNumber() != null) {
%>
<tr class="<%=HTMLUtil.getRowType(rowCount)%>">
   <td></td>
   <td></td>
   <td></td>
   <td class="label" nowrap>Inc A/c No:</td>
   <td class='dataValue' nowrap><%=postingVO.getIncomeAccountNumber()!=null?postingVO.getIncomeAccountNumber():""%></td>
   <td class="label" nowrap>Inc Cost Centre:</td>
   <td class='dataValue' nowrap><%=postingVO.getIncomeCostCentre()!=null?postingVO.getIncomeCostCentre():""%></td>
   <td class="label" nowrap>Inc Sub A/c No:</td>
   <td class='dataValue' nowrap><%=postingVO.getIncomeSubAccountNumber()!=null?postingVO.getIncomeSubAccountNumber():""%></td>
</tr>
<%
      }
      if (postingVO.getPostedDate() != null) {
%>
<tr class="<%=HTMLUtil.getRowType(rowCount)%>">
   <td></td>
   <td></td>
   <td></td>
   <td class="label" nowrap>Posted Date:</td>
   <td class='dataValue' nowrap ><%=postingVO.getPostedDate()!=null?DateUtil.formatShortDate(postingVO.getPostedDate()):""%></td>
   <td class="label" nowrap>Journal Key:</td>
   <td class='dataValue' nowrap><%=postingVO.getJournalKey()!=null?postingVO.getJournalKey().toString():""%></td>
   <td class="label" nowrap>Journal No:</td>
   <td class='dataValue' nowrap><%=postingVO.getJournalNumber()!=null?postingVO.getJournalNumber().toString():""%></td>
</tr>
<%
      }
   }
%>
</table>
<%
}
%>

</body>
</html>
