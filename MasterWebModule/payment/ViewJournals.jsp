<%@page import="com.ract.payment.finance.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.security.ui.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.math.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<title>
</title>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
Hashtable journals = (Hashtable)request.getAttribute("journals");

String srcSystem = (String)request.getAttribute("srcsystem");
DateTime effDate = (DateTime)request.getAttribute("effDate");
DateTime entDate = (DateTime)request.getAttribute("entDate");
BigDecimal financeAmount = (BigDecimal)request.getAttribute("amount");
String accountNumber = (String)request.getAttribute("accountNumber");
//Integer clientNumber = (Integer)request.getAttribute("clientNumber");
%>

<body>

<h2>Journals</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr class="headerRow">
  <td class="listHeadingPadded">Account</td>
  <td class="listHeadingPadded">Journal Key</td>  
  <td class="listHeadingPadded">Company</td>
  <td class="listHeadingPadded">Sales Branch</td>
  <td class="listHeadingPadded">Effective Date</td>
  <td class="listHeadingPadded">Posted Date</td>
  <td align="right" class="listHeadingPadded">Amount</td>
</tr>

<%
String key = null;
Journal journal = null;
int counter = 0;
for (Iterator i = journals.keySet().iterator();i.hasNext();)
{
  counter++;
  key = (String)i.next();
  journal = (Journal)journals.get(key);
%>

<tr class="<%=HTMLUtil.getRowType(counter)%>">
  <td class="dataValue"><%=journal.getAccount()%></td>
  <td class="dataValue"><%=journal.getSubAccount()%></td>
  <td class="dataValue"><%=journal.getCostCentre()%></td>
  <td class="dataValue"><%=journal.getCompany().getCompanyCode()%></td>
  <td class="dataValue"><%=journal.getSalesBranch().toUpperCase()%></td>
  <td class="dataValue"><%=journal.getPostAtDate()%></td>
  <td class="dataValue"><%=journal.getPostedDate()%></td>
  <td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(journal.getAmount())%></td>
</tr>

<%
}
%>

</table>
</body>

</html>
