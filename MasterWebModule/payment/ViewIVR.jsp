<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.payment.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%
String tbody    = (String) request.getAttribute("tableBody");
String tots     = (String) request.getAttribute("totals");
String filename = (String) request.getAttribute("filename");
%>
<html>
<head>
<title>BillPay Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<%=CommonConstants.getRACTStylesheet()%>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<script language="JavaScript">
<!--
   function printRightFrame ()
   {
   //   if (window.print)
   //    document.write('<form><input type="button" value="Print" onClick="parent.frames[1].focus();parent.frames[1].print()"><\/form>');
         parent.rightFrame.focus();parent.rightFrame.print()
   }
   function setParams ( filename )
   {
      document.all.ivrFilename.value=filename;
      document.all.listCode.value="No";
   }
   function setListCodeAndSubmitForm( eventName, obj )
   {
      document.all.listCode.value = obj;
      submitForm( eventName );
   }

-->
</script>

</head>

<body class="rightFrame" bgcolor="#FFFFFF" text="#000000" onload="setParams('<%=filename%>')">
<form name="mainForm" method="post" action="<%=PaymentUIConstants.PAGE_PaymentUIC%>">
<input type="hidden" name="listCode" value = "No">
<input type="hidden" name="ivrFilename" value ="">
<input type="hidden" name="event" value = "">

<div id="mainpage">
<h2>IVR Audit Report</h2>
<br/>

<table align="center" width="90%" border="0" cellspacing="1" >
 <%=tots%>
</table>
<span class="helpText">Double Click highlighted row to see all IVR details</span>
<br>
<table width="100%" border="0" cellspacing="1">
   <thead bgcolor="lavender">
     <th width="14%" class="label">Date Paid</th>
     <th width="14%" class="label">Credit Card Number</th>
     <th width="14%" class="label">Amount</th>
     <th width="14%" class="label">Date Processed</th>
     <th width="14%" class="label">Client</th>
     <th width="14%" class="label">Sub System</th>
     <th width="14%" class="label">RACT Receipt</th>
 </thead>
 <tbody>
   <%=tbody%>
 </tbody>
</table>
</div>
<div id="buttonPage">
      <%=HTMLUtil.buttonPrintRightFrame()%>
      <%=HTMLUtil.buttonViewAll("setListCodeAndSubmitForm('viewIVRReport','Yes')","View all IVR Records")%>
</div>
</form>
</body>
</html>
