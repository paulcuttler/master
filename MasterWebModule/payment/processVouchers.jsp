<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet" type="text/css"/>

</head>
<body>

<h2>Process Vouchers</h2>

List of vouchers to process for <s:property value="approvingUser"/>.

<s:form theme="simple" id="frm_processvoucher" name="frm_processvoucher" action="processVoucher.action">

   <s:hidden name="approvingUserId"></s:hidden>

<table>

   <s:if test="voucherList != null && voucherList.size() > 0">
   <tr class="headerRow">
   <td class="listHeadingPadded">Id</td>
   <td class="listHeadingPadded">Create Date</td>
   <td class="listHeadingPadded">Client Number</td>
   <td class="listHeadingPadded">Disposal Mode</td>
   <td class="listHeadingPadded">Disposal Type</td>
   <td class="listHeadingPadded">Disposal Amount</td>
   <td class="listHeadingPadded">Fee Amount</td>
   <td class="listHeadingPadded">User</td>
   <td class="listHeadingPadded">Particulars</td>
   <td class="listHeadingPadded">Status</td>
   <td class="listHeadingPadded">Action</td>  
   </tr>
   
     
           <s:iterator value="voucherList" status="cStat">
               <tr class="<s:if test="#cStat.odd == false">oddLink</s:if><s:else>evenLink</s:else>">
				<td><s:a href="viewVoucherDetails.action?disposalId=%{disposalId}"><s:property value="disposalId"/></s:a></td>
				<td><s:date name="createDate" format="dd/MM/yyyy HH:mm:ss"/></td>
				<td><s:property value="clientNumber"/></td>
				<td><s:property value="disposalMode"/></td>
				<td><s:property value="disposalType"/></td>
				<td><s:text name="format.money"><s:param name="value" value="disposalAmount"/></s:text></td>
				<td><s:text name="format.money"><s:param name="value" value="feeAmount"/></s:text></td>
				<td><s:property value="userId"/></td>	
                <td><s:property value="particulars"/></td>
				<s:if test="status==null">
				<td class="dataValueOrange">Unprocessed</td>
				</s:if>				
				<s:elseif test='status=="R"'>
				<td class="dataValueRed">Rejected</td>
				</s:elseif>
				<s:elseif test='status=="A"'>
				<td class="dataValueGreen">Approved</td>				
				</s:elseif>	
				<s:if test="status==null">
				<td><s:a href="processVoucher.action?processAction=A&disposalId=%{disposalId}">Approve</s:a>&nbsp;|&nbsp;
				<s:a href="processVoucher.action?processAction=R&disposalId=%{disposalId}">Reject</s:a></td>
				</s:if>
				<s:else><td>&nbsp;</td></s:else>

				</tr>
			</s:iterator>

    </s:if>

</table>

<s:actionerror/>

</s:form>

</body>

</html>