<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="java.util.*"%>
<%--
Requests parameters for the Motor News delivery extract

request attributes:

returned parameters:
   mailFileName
   deliveryFileName
   errorFileName
   runDate
   runHours
   runMins

--%>
<%
boolean allowRecall;
boolean allowExtract;
boolean allowPublish;
boolean allowUndo;
boolean selectNext;
%>
<html>
<head>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<script language="javascript">

function setRunDate ()
{
   ddate = new Date;
   dday = ddate.getDate();
   dmonth = ddate.getMonth() + 1;
   dyear = ddate.getYear();
   document.all.runDate.value= dday + "/" + dmonth + "/" + dyear;
   dStr = formatDate(ddate.getFullYear() , dmonth , dday );
   document.all.fileDate.value = dStr;
   document.all.deliveryFileName.value = "MotorNews_" + dStr + ".dat";
   document.all.mailFileName.value = "AnnualReport_" + dStr + ".dat";
   document.all.errorFileName.value = "MotorNews_Errors_" + dStr + ".dat";
}

function formatDate ( yy,mm,dd )
{
  mStrDate = yy;
  mStrDate += padString(mm);
  mStrDate += padString(dd);

  return mStrDate;

}

function padString ( num )
{
  myStr = '';
  if ( num < 10 )
  {
  	myStr += '0';
  }

  myStr += num;
  return myStr;
}

function checkHours(thing)
{
    val = thing.value * 1;
    if(val>23 || val<0)
    {
       document.all.runHours.focus();
       alert("Hours must be from 0 to 23");
       return false;
    }
    else return true;
}

function checkMins(thing)
{
   val = thing.value * 1;
   if(val > 59 || val < 0)
   {
      document.all.runMins.focus();
      alert("Minutes must be between 0 and 59");
      return false;
   }
   else return true;
}


function validate(thing)
{
   if(document.all.event.value=="MotorNews_Cancel") return true;
   hours = document.all.runHours.value * 1;
   mins = document.all.runMins.value * 1;
   if(hours>23 || hours < 0 || mins<0 || mins>59)
   {
      alert("Enter a valid date and time");
      return false;
   }
   else return true;
}


   function submitForm(eventName)
   {
      document.all.event.value = eventName;
      var valid = true;//validateForm();
      if (valid)
      {
        document.all.MNForm.submit();
      }
   }

</script>
<title>
MotorNewsExtract
</title>
<%=CommonConstants.getRACTStylesheet()%>
<%--<link rel="stylesheet" href="../css/ract.css" type="text/css">--%>
</head>
<body onload="setRunDate();">
<form name = "MNForm" method="post" onsubmit="return validate(this)" action="<%= request.getContextPath() + "/MembershipReportsUIC"%>">

<input type="hidden" name="event" value="">
<input type="hidden" name="fileDate" value="">

<h1 >Extract Motor News Delivery Lists</h1>

<table border="0">
<tr height="90%">
<td>
<table>

    <tr>
      <td>
        Write the normal delivery list to
      </td>
      <td>
        <input type="text" name="deliveryFileName" size="50" maxlength="250">
      </td>
    </tr>
    <tr>
      <td>
        Write the annual report list to
      </td>
      <td>
        <input type="text" name="mailFileName" size="50" maxlength="250" >
      </td>
    </tr>
    <tr>
      <td>
        Write the errors to
      </td>
      <td>
        <input type="text" name="errorFileName" size="50" maxlength="250" >
      </td>
    </tr>
    <tr >
      <td>
        Schedule to run on
      </td>
      <td>
        <input type = "text" name = "runDate" size = "11" value = ""
         onkeyup = "javascript:checkDate(this)">
        &nbsp at &nbsp
        <input type = "text" name = "runHours" size = "2" value = "17"
             onkeyup = "javascript:checkInteger(this)"
             onblur = "javascript:checkHours(this)">
        :
        <input type = "text" name = "runMins" size = "2" value = "30"
             onkeyup = "javascript:checkInteger(this)"
             onblur = "javascript:checkMins(this)">
      </td>
      </tr>
   </table>
   </td>
   </tr>
   <tr valign="bottom">
   <td>
   <table border="0">
   <tr>
   <td >
         <a  onclick="submitForm('MotorNews_DoExtract');return true;"
             onMouseOut="MM_swapImgRestore();"
             onMouseOver="MM_displayStatusMsg('Submit Extract Motor News.');MM_swapImage('btnOK','','<%=CommonConstants.DIR_IMAGES%>/OkButton_over.gif',1);return document.MM_returnValue"
         >
             <img  name="btnOK"
                   src="<%=CommonConstants.DIR_IMAGES%>/OkButton.gif"
                   border="0"
                   alt="Submit Extract Motor News."
             >
          </a>
   </td>
   <td>
         <a  onclick="submitForm('MotorNews_Cancel');return true;"
             onMouseOut="MM_swapImgRestore();"
             onMouseOver="MM_displayStatusMsg('Cancel Motor News  Extract.');MM_swapImage('btnCancel','','<%=CommonConstants.DIR_IMAGES%>/CancelButton_over.gif',1);return document.MM_returnValue"
         >
             <img  name="btnCancel"
                   src="<%=CommonConstants.DIR_IMAGES%>/CancelButton.gif"
                   border="0"
                   alt="Cancel Motor News Extract."
             >
          </a>
   </td>
   </tr>
   </table>
</td>
</tr>
</table>
</form>
</body>
</html>
