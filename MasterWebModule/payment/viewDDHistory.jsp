<%@page import="com.ract.common.*"%>

<html>
<head>
<title>Direct Debit Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript"
        src="<%=CommonConstants.DIR_PAYMENT%>/scripts/viewDDHistory.js"></script>

</head>
<body>
<%@include file="/menu/PopupHeader.jsp"%>

<input type="hidden" name="subSystem" value="<%=request.getAttribute("subSystem")%>"/>
<input type="hidden" name="systemNo" value="<%=request.getAttribute("systemNo")%>"/>
<h2>Direct Debit Details<br>&nbsp;&nbsp;&nbsp;&nbsp; <%=request.getAttribute("ddType")+":"%>
                                     <%=request.getAttribute("systemNo")%></h2>

<form name="form1" method="post" action="">
</form>
<hr />
<table height="394">
  <tr>
    <td valign="top">
      <div dojoType="TreeSelector" id="myWidgetContainer" eventNames="select:nodeSelected" widgetId="tSelector"
	                               style="border: solid #888 1px;">
	<span id="treePlaceHolder" style="background-color:#F00; color:#FFF">
		Loading tree widget...
	</span>
</div>
</td>
    <td valign="top" ><span id='detailDisp'>
      <table  dojoType="filteringTable"
              id="vTable"
              cellpadding="2"
              alternateRows="true"
              class="dataTable" >
        <thead >
        <tr>
	      <th field="title" dataType="String" align="left" valign="top" >Heading</th>
	      <th field="value" dataType="Integer" align="left" valign="top" >Value</th>
	</tr>
     </thead>
   <tbody>
   </tbody>
   </table>
   </span>
</td>
</tr>
</table>

</body>
</html>
