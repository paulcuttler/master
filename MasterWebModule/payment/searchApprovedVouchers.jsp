<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet" type="text/css"/>

  </head>
  
  <body>
    
    <h2>Search Approved Vouchers</h2>
    
    <s:form theme="simple" action="searchApprovedVouchers">
    
    <table>
    <tr>
      <td>Start Date</td>
      <td><s:textfield name="startDate"></s:textfield></td>
    </tr>
    <tr>
      <td>End Date</td>
      <td><s:textfield name="endDate"></s:textfield></td>    
    </tr>
    <tr>
      <td>Show unpaid only?</td>    
      <td><s:checkbox name="unpaid"></s:checkbox></td>    
    </tr>
    <tr>
      <td colspan="2"><s:submit></s:submit></td>
    </tr>
    
    </table>
    
    </s:form>
    
    
    <!--  results -->    
     
   <s:if test="voucherList != null && voucherList.size() > 0">
   <table>
   
   <tr class="headerRow">
   <td class="listHeadingPadded">Id</td>
   <td class="listHeadingPadded">Approval Date</td>
   <td class="listHeadingPadded">Client Number</td>
   <td class="listHeadingPadded">Disposal Mode</td>
   <td class="listHeadingPadded">Disposal Type</td>
   <td class="listHeadingPadded">Disposal Amount</td>
   <td class="listHeadingPadded">Fee Amount</td>
   <td class="listHeadingPadded">User</td>
   <td class="listHeadingPadded">Sales Branch</td>   
   <td class="listHeadingPadded">Particulars</td>
   <td class="listHeadingPadded">Payee No</td>
   <td class="listHeadingPadded">Payee Name</td>   
   <td class="listHeadingPadded">Invoice No</td>   
   <td class="listHeadingPadded">Payment Date</td>
   <td class="listHeadingPadded">Payment Ref</td>
   <td class="listHeadingPadded">Payment Amount</td>         
   </tr>
      
      
           <s:iterator value="voucherList" status="cStat">

               <tr class="<s:if test="#cStat.odd == false">oddLink</s:if><s:else>evenLink</s:else>">
				<td><s:a href="viewVoucherDetails.action?disposalId=%{disposalId}"><s:property value="disposalId"/></s:a></td>
				<td><s:date name="processDate" format="dd/MM/yyyy HH:mm:ss"/></td>
				<td><s:property value="clientNumber"/></td>
				<td><s:property value="disposalMode"/></td>
				<td><s:property value="disposalType"/></td>
				<td><s:text name="format.money"><s:param name="value" value="disposalAmount"/></s:text></td>
				<td><s:text name="format.money"><s:param name="value" value="feeAmount"/></s:text></td>
				<td><s:property value="userName"/></td>	
				<td><s:property value="salesBranch"/></td>				
                <td><s:property value="particulars"/></td>
                <td><s:property value="payeeNo"/></td>
                <td><s:property value="payeeName"/></td>
                <td><s:property value="voucherId"/></td>                
				<td><s:date name="paymentDate" format="dd/MM/yyyy"/></td>                
				<td><s:property value="paymentReference"/></td>
				<td><s:text name="format.money"><s:param name="value" value="paidAmount"/></s:text></td>
				</tr>
			</s:iterator>
			<tr>
			  <td colspan="5"></td>
			  <td><s:text name="format.money"><s:param name="value" value="totalAmount"/></s:text></td>
			  <td><s:text name="format.money"><s:param name="value" value="feeAmount"/></s:text></td>			       
			  <td colspan="8"></td>
              <td><s:text name="format.money"><s:param name="value" value="paidAmount"/></s:text></td>
			</tr>
       </table>
       
    </s:if>
  </body>
</html>
