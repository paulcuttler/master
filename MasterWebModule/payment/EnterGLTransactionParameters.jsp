<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.finance.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>

<html>

<head>

<%=CommonConstants.getRACTStylesheet()%>

</head>

<body>

<%
//DEFAULTS
DateTime effStartDate = null;
DateTime effEndDate = null;
DateTime entStartDate = null;
DateTime entEndDate = null;

effStartDate = new DateTime();
effEndDate = new DateTime();
entStartDate = new DateTime();
entEndDate = new DateTime();

String entity = "ALL";

%>

<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr valign="top">
<td>

<form action="<%=PaymentUIConstants.PAGE_PaymentUIC%>">

<h2>Enter Parameters for GL Transactions</h2>
<input type="hidden" name="event" value="viewGLTransactionList">
<table>
<tr>
  <td>Effective Start Date</td><td><input type="text" name="effStartDate" value="<%=effStartDate%>"></td>
</tr>
<tr> 
  <td>Effective End Date</td><td><input type="text" name="effEndDate" value="<%=effEndDate%>"></td>
</tr>
<tr>
  <td>Entered Start Date</td><td><input type="text" name="entStartDate" value="<%=entStartDate%>"></td>
</tr>
<tr>
  <td>Entered End date</td><td><input type="text" name="entEndDate" value="<%=entEndDate%>"></td>
</tr>
<tr>
  <td>Account</td><td><input type="text" name="account" value="101340"></td>
</tr>
<tr>
  <td>Subsystem</td><td><input type="text" name="subsystem" value="GLRS"></td>
</tr>
<tr>
  <td>Entity</td><td>
<select name="entity">
<option value="ALL">All Entities</option>
<option value="10">RACT</option>
<option value="20">RACTI</option>
</select></td>
</tr>

</table>
</td>
 </tr>

  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><input type="submit" name="btnExec" value="Execute Query"></td>
        </tr>
      </table>
    </td>
  </tr>
</form>
</table>


</body>
</html>
