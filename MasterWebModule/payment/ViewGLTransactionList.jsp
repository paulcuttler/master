<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.finance.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>

<html>

<head>

<%=CommonConstants.getRACTStylesheet()%>

</head>

<body>

<h2>View GL Transactions</h2>

<%

PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();

ArrayList glTransactionList = (ArrayList)request.getAttribute("glTransactionList");
//ref
DateTime effStartDate = (DateTime)request.getAttribute("effStartDate");
DateTime effEndDate = (DateTime)request.getAttribute("effEndDate");
DateTime entStartDate = (DateTime)request.getAttribute("entStartDate");
DateTime entEndDate = (DateTime)request.getAttribute("entEndDate");
String account = (String)request.getAttribute("account");
String subsystem = (String)request.getAttribute("subsystem");
String mode = (String)request.getAttribute("mode");
%>

<table>
<tr>
  <td>Effective Start Date</td><td class="dataValue"><%=effStartDate%></td>
</tr>
<tr>
  <td>Effective End Date</td><td class="dataValue"><%=effEndDate%></td>
</tr>
<tr>
  <td>Entered Start Date</td><td class="dataValue"><%=entStartDate%></td>
</tr>
<tr>
  <td>Entered End date</td><td class="dataValue"><%=entEndDate%></td>
</tr>
<tr>
  <td>Account</td><td class="dataValue"><%=account%></td>
</tr>
<tr>
  <td>Subsystem</td><td class="dataValue"><%=subsystem%></td>
</tr>
</table>

<p></p>
<%
//end of move
if (glTransactionList != null && !glTransactionList.isEmpty())
{
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="headerRow">
  <td class="listHeadingPadded">SubSystem</td>
  <td class="listHeadingPadded">Entered Date</td>
  <td class="listHeadingPadded">Effective Date</td>
  <td class="listHeadingPadded">Account</td>
  <td class="listHeadingPadded">Narration</td>  
  <td class="listHeadingPadded">Finance System</td>
  <td class="listHeadingPadded">Source System</td>
  <td class="listHeadingPadded">Variance</td>
</tr>
<%
  GLTransaction glTrans = null;
  String srcSystem = "";
  GLTransaction tmpGLTrans = null;

  BigDecimal varianceLineTotal = new BigDecimal(0);
  BigDecimal varianceTotal = new BigDecimal(0);
  
  BigDecimal financeLineTotal = new BigDecimal(0);
  BigDecimal financeTotal = new BigDecimal(0);
  
  BigDecimal ssLineTotal = new BigDecimal(0);
  BigDecimal ssTotal = new BigDecimal(0);  
  
  Hashtable journals = null;  
  
  DateTime enteredDate = null;

  int listSize = glTransactionList.size();
LogUtil.log(this.getClass(),"elements = "+listSize);

  ArrayList procs = new ArrayList();
  int counter = 0;
  //iterate through the list of gl transactions.
  for (int i = 0; i < listSize; i++)
  {
LogUtil.log(this.getClass(),"counter "+counter);
LogUtil.log(this.getClass(),i + "a - "+procs.toString());

    //have we already processed it?
    if (procs.indexOf(new Integer(i)) != -1)
    {
      //skip it
      continue;
    }
    counter++; 
//LogUtil.log(this.getClass(),i + "b - "+procs.toString());

    //get the next gl transaction
    glTrans = (GLTransaction)glTransactionList.get(i);

    srcSystem = glTrans.getSubsystem();

    varianceLineTotal = new BigDecimal(0);
    
    financeLineTotal = new BigDecimal(0);
    financeLineTotal = financeLineTotal.add(glTrans.getAmount());
    
    String journalKey = glTrans.getNarration();
    
    ssLineTotal = new BigDecimal(0);
    if ("GLRS".equals(srcSystem))
    {
      journalKey = glTrans.getNarration().split(" ")[0];
	  journals = paymentMgrLocal.getJournals(journalKey);  
	  if (journals != null)
	  {
	    Journal journal = (Journal)journals.get(journalKey);
	    if (journal != null)
	    {
	      ssLineTotal = ssLineTotal.add(new BigDecimal(journal.getAmount()));
	    }
	    else
	    {
	      System.out.println("no amount for journal "+journalKey);
	    }
	  }  
    }
    
    //total
    ssTotal = ssTotal.add(ssLineTotal);
    financeTotal = financeTotal.add(financeLineTotal);

    
    varianceLineTotal = financeLineTotal.subtract(ssLineTotal);

    //add processed to current list 
    procs.add(new Integer(i));

    enteredDate = glTrans.getEnteredDate();

    //total variance
    varianceTotal = varianceTotal.add(varianceLineTotal);

%>
<tr class="<%=HTMLUtil.getRowType(counter)%>">
  <td><%=srcSystem%></td>
  <td><%=enteredDate==null?"-":enteredDate.toString()%></td>
  <td><%=glTrans.getEffectiveDate()%></td>
  <td><%=glTrans.getAccount()%></td>
<%
if (srcSystem.equals("GLRS"))
{
%>
  <td><a title="View source system transactions" href="<%=PaymentUIConstants.PAGE_PaymentUIC%>?event=viewPostingDetails&journalKey=<%=journalKey%>"/><%=glTrans.getNarration()%></td></a></td>
<%
}
else
{  
%>
 <td><%=glTrans.getNarration()%></td>
<%
}
%> 
  <td align="right"><%=CurrencyUtil.formatDollarValue(financeLineTotal)%></td>
  <td align="right"><%=CurrencyUtil.formatDollarValue(ssLineTotal)%></td>
  <td align="right"><%=CurrencyUtil.formatDollarValue(varianceLineTotal)%></td>
</tr>
<%
  }
%>

<tr class="highlightRow">
  <td align="right" colspan="5"/><td align="right"><%=CurrencyUtil.formatDollarValue(financeTotal)%></td><td align="right"><%=CurrencyUtil.formatDollarValue(ssTotal)%></td><td align="right"><%=CurrencyUtil.formatDollarValue(varianceTotal)%></td>
</tr> 

</table>
<%
}
%>

</body>
</html>
