<%@ page import="java.util.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.payment.finance.*"%>
<%@ page import="com.ract.payment.ui.*"%>
<%@ page import="com.ract.common.*"%>

<%
DebtorInvoice invoice = (DebtorInvoice)request.getAttribute("invoice");
Collection invoiceList = (Collection)request.getAttribute("invoiceList");
String searchType = (String)request.getAttribute("searchType");
String searchTerm = (String)request.getAttribute("searchTerm");
%>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

</head>

<body onload="document.all.event.value='getInvoice'">
<h2>Search Invoices</h2>
<form name="form1" method="post" action="<%=PaymentUIConstants.PAGE_PaymentUIC%>">
<input type="hidden" name="event"/>
  <table width="75%" border="0">
    <tr>
      <td colspan="3" class="helpText">Search by one of the following fields. Customer numbers must have 8 digits (eg. 00430741).</td>
    </tr>
    <tr>
      <td width="4%">
        <input type="radio" name="searchType" value="invoiceNumber" checked onclick="document.all.event.value='getInvoice'">
      </td>
      <td width="21%">Invoice number</td>
      <td width="75%">
        <input type="text" name="invoiceNumber">
      </td>
    </tr>
    <tr>
      <td height="22" width="4%">
        <input type="radio" name="searchType" value="customerNumber" onclick="document.all.event.value='getInvoicesByClientNumber'">
      </td>
      <td height="22" width="21%">Customer number</td>
      <td height="22" width="75%">
        <input type="text" name="customerNumber">
      </td>
    </tr>
  </table>

  <p>
    <input type="submit" name="Submit" value="Search">
  </p>
</form>

<%
int count = 0;
if (searchTerm != null &&
    searchType != null)
{
if (invoice != null)
{
  count++;
  invoiceList = new ArrayList();
  invoiceList.add(invoice);
}

if (invoiceList != null)
{
%>
<table>
  <thead class="headerRow">
    <tr>
      <th class="listHeadingPadded">Invoice Number</th><th class="listHeadingPadded">Customer Number</th><th class="listHeadingPadded">Create Date</th><th class="listHeadingPadded">Amount</th>
    </tr>
  </thead>
  <tbody>
<%
  DebtorInvoice di = null;
  for (Iterator i = invoiceList.iterator(); i.hasNext();)
  {
    count++;
    di = (DebtorInvoice)i.next();
%>

  <tr class="<%=HTMLUtil.getRowType(count)%>" >
    <td><%=di.getDebtorInvoicePK().getInvoiceNumber()%></td><td><%=di.getCustomerNumber()%></td><td><%=di.getDebtorInvoicePK().getCreateDate()%></td><td><%=CurrencyUtil.formatDollarValue(di.getAmount())%></td>
  </tr>

<%
  }
%>
  </tbody>
</table>
<%
}

  if (count == 0)
  {
%>
  <span class="helpText">No results found for <%=searchTerm%>.</span>
<%
  }
}
%>
</body>

</html>
