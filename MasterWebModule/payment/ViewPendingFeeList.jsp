<%@page import="com.ract.payment.billpay.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.security.*"%>
<%@page import="com.ract.security.ui.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
UserSession userSession = UserSession.getUserSession(session);

Integer cNumber = (Integer) request.getAttribute("clientNumber");
ClientVO clientVO = null;

// If the client VO is not passed then look it up.
if (clientVO == null)
{
   ClientMgr cMgr = ClientEJBHelper.getClientMgr();
   clientVO = cMgr.getClient(cNumber);
   request.setAttribute("client",clientVO);
}

//should be in the UIC
Collection pendingFeeList = (Collection)request.getAttribute("pendingFeeList");
PendingFee pendingFee = null;
%>

<html>
<head>
<title>
</title>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
<form name="viewPendingFeeList" method="post" action="<%= PaymentUIConstants.PAGE_PaymentUIC%>">
<input type="hidden" name="event">
<input type="hidden" name="clientNumber" value="<%=cNumber%>">
  <tr valign="top">
      <td>
          <h2>Pending fees for <%=(clientVO.getClientNumber())%></h2>
          <%@include file="/client/ViewClientInclude.jsp"%>
          <div style="height:400; overflow:auto; border-width: 1; border-style: solid;">
          <table border="0" cellpadding="0" cellspacing="0" >
<%
if (pendingFeeList != null && !pendingFeeList.isEmpty())
{
%>
            <tr class="headerRow" valign="bottom">
              <td class="listHeadingPadded">Pay Ref No</td>
              <td class="listHeadingPadded">Source System</td>
              <td class="listHeadingPadded">Effective Date</td>
              <td class="listHeadingPadded">Expiry Date</td>
              <td class="listHeadingPadded">Amount</td>
              <td class="listHeadingPadded">Sequence No</td>
              <td class="listHeadingPadded">Date Paid</td>
              <td class="listHeadingPadded">OI-Payable Seq No</td>
              <td class="listHeadingPadded">Payment System</td>
            </tr>
<%
  Iterator pendingFeeIT = pendingFeeList.iterator();
  int counter = 0;
  while (pendingFeeIT.hasNext())
  {
    counter++;
    pendingFee = (PendingFee) pendingFeeIT.next();

%>
            <tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
              <td class="listItemPadded">
                 <%=pendingFee.getPendingFeeID()%>
              </td>
              <td class="listItemPadded">
                  <a title="source system reference number=<%=pendingFee.getSourceSystemReferenceNo()%>&#10;reference number=<%=pendingFee.getRefNumber()%>">
                  <%=pendingFee.getSourceSystem()%>
                  </a>
              </td>
              <td class="listItemPadded">
                  <a>
                     <%=pendingFee.getFeeEffectiveDate().formatShortDate()%>
                  </a>
              </td>
              <td class="listItemPadded">
                     <%=pendingFee.getFeeExpiryDate().formatShortDate()%>
              </td>

              <td class="listItemPadded">
                     <%=CurrencyUtil.formatDollarValue(pendingFee.getAmount())%>
              </td>
              <!--doc id in MEM -->
              <td class="listItemPadded"><%=pendingFee.getSeqNumber()%></td>
              <td class="listItemPadded">
              <%
                DateTime datePaid = pendingFee.getDatePaid();
                if (datePaid != null)
                {
                  if (userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN))
                  {
              %>
                <a title="credit card number = <%=pendingFee.getCreditCardNumber()%>&#10;billpay receipt = <%=pendingFee.getBillpayReceipt()%>&#10;authorisation no. for cc = <%=pendingFee.getBankReceipt()%>">
                <%=datePaid%>
                </a>
              <%
                  }
                  else
                  {
              %>
                <%=datePaid%>
              <%
                  }
                }
              %>
              <td class="listItemPadded"><%=pendingFee.getPaymentSystem().equals(SourceSystem.ECR.getAbbreviation()) ? " - " : pendingFee.getOpSequenceNumber()%></td>
              <td class="listItemPadded"><%=pendingFee.getPaymentSystem()%></td>
            </tr>
<%
  }
}
else
{
%>
            <tr>
              <td class="helpText">No pending fees to display.</td>
            </tr>
<%
}
%>
          </table>
          </div>
      </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><input type="button" name="btnBack" value="Back" onClick="history.back();"></td>
        </tr>
      </table>
    </td>
  </tr>
</form>
</table>
</body>
</html>
