<%@page import="com.ract.payment.directdebit.*"%>
<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
PayableItemVO payItem = (PayableItemVO) request.getAttribute("payableItem");
ClientVO clientVO = payItem.getClient();
// Attach the clientVO to the request to pass it to ViewClientInclude.jsp
request.setAttribute("client",clientVO);

PaymentMethod paymentMethod = null;

try
{
  paymentMethod = payItem.getPaymentMethod();
}
catch (Exception e)
{
}

%>

<html>
<head>
<title>Payable Item Detail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>
<form action="<%=PaymentUIConstants.PAGE_PaymentUIC%>">

<input type="hidden" name="event" value="editPayableItem_btnSave">
<input type="hidden" name="payableItemID" value="<%=payItem.getPayableItemID()%>">

<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
  <tr valign="top"><td>
    <h2>Edit Payable Item</h2>
    <table border="0" cellpadding="3" cellspacing="0">
    <tr>
      <td colspan="3">
      <%@include file="/client/ViewClientInclude.jsp"%>
      </td>
    </tr>
    <tr>
      <td class="label">Item ID:</td>
      <td class="dataValue" >&nbsp<%=payItem.getPayableItemID()%></td>
    </tr>
    <tr>
      <td class="label">Payment Method:</td>
      <td class="dataValue" >&nbsp<%=paymentMethod!=null?paymentMethod.getDescription():"<span class=\"errorText\">No payment method defined</span>"%></td>
    </tr>
    <tr>
      <td class="label">Payment Method ID:</td>
      <td class="dataValue" ><input type="text" name="paymentMethodID" value="<%=payItem.getPaymentMethodID()==null?"":payItem.getPaymentMethodID()%>"/></td>
    </tr>
    </table>
    </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><input type="button" name="btnSave" value="Save" onClick="document.forms[0].submit();"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html>
