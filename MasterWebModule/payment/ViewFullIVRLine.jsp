<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.payment.ui.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%

ArrayList fields = (ArrayList) request.getAttribute("lineToShow");
String filename  = (String)    request.getAttribute("filename");

String viewClientAction = ClientUIConstants.PAGE_ClientUIC;
String paymentUICAction = PaymentUIConstants.PAGE_PaymentUIC;
%>
<html>
<head>
<title>BillPay Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<%=CommonConstants.getRACTStylesheet()%>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<script language="JavaScript">
<!--
   function printRightFrame ()
   {
         parent.rightFrame.focus();parent.rightFrame.print()
   }
   function setParams ( filename, gotoUIC )
   {
      document.all.ivrFilename.value=filename;
      document.all.listCode.value="No";
      document.all.mainForm.action = gotoUIC;
   }
   function setListCodeAndSubmitForm( eventName, obj )
   {
      document.all.listCode.value = obj;
      submitForm( eventName );
   }

   function setSearchParams ( obj,gotoUIC )
   {
      document.all.clientSearchType.value  = obj.name;
      document.all.clientSearchValue.value = obj.childNodes[1].innerHTML;
      document.all.mainForm.action = gotoUIC;
      document.all.event.value = 'findClientFromIVRPage';
      document.all.mainForm.submit();
   }
-->
</script>

</head>

<body class="rightFrame" bgcolor="#FFFFFF" text="#000000" onload="setParams('<%=filename%>','<%=paymentUICAction%>')">
<form name="mainForm" method="post" >
<input type="hidden" name="listCode" value = "No">
<input type="hidden" name="ivrFilename" value ="">
<input type="hidden" name="event" value = "">
<input type="hidden" name="clientSearchType" value = "">
<input type="hidden" name="clientSearchValue" value = "">
<input type="hidden" name="message" value = "">

 <div id="mainpage" class="pg">
 <h1>IVR Data</h1>
    <div id="contentpage" class="cnt">
    <span class="helpText11">Double Click highlighted row to see <b>CLIENT</b> details</span>
    <table width="100%" border="0">
     <tr>
      <td width="10%">
         &nbsp;
      </td>
      <td width="90%">
         <table width="100%" border="0" cellspacing="3" cellpadding="3">
           <tr name="<%=ClientUIConstants.SEARCHBY_IVR_NUMBER%>"
               onMouseOver="bgColor = '#CDD7E1'"
               onMouseOut="bgColor = '#FFFFFF'"
               ondblclick="setSearchParams(this,'<%=viewClientAction%>')"
               >
                   <td width="26%" class="label">IVR Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(0).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Date Paid</td>
                   <td width="74%" class="dataValue"><%=fields.get(1).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Telstra Receipt Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(2).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Authorisation Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(3).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Credit Card Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(4).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Amount Paid</td>
                   <td width="74%" class="dataValue"><%=fields.get(5).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Membership Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(6).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Date Processed</td>
                   <td width="74%" class="dataValue"><%=fields.get(7).toString()%></td>
           </tr>
           <tr name="<%=ClientUIConstants.SEARCHBY_CLIENT_NUMBER%>"
               onMouseOver="bgColor = '#CDD7E1'"
               onMouseOut="bgColor = '#FFFFFF'"
               ondblclick="setSearchParams(this,'<%=viewClientAction%>')"
          >
                   <td width="26%" class="label">Client Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(8).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Sequence Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(9).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Source System</td>
                   <td width="74%" class="dataValue"><%=fields.get(10).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">RACT Receipt Number</td>
                   <td width="74%" class="dataValue"><%=fields.get(11).toString()%></td>
           </tr>
           <tr>
                   <td width="26%" class="label">Processing Message</td>
                   <td width="74%" class="dataValue"><%=fields.get(12).toString()%></td>
           </tr>
         </table>
      </td>
     </tr>
     </table>
      </div>
   <div id="buttonPage" class="btn">
      <%=HTMLUtil.buttonPrintRightFrame()%>
      <span>
         <input type="button" name="back" value="Back" onclick="setListCodeAndSubmitForm('viewIVRReport','No')">
      </span>
   </div>
</div>
</form>
</body>
</html>
