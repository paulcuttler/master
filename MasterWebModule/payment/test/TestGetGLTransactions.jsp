<%@page import="com.ract.payment.*"%>
<%@page import="com.ract.payment.finance.*"%>
<%@page import="com.ract.payment.ui.*"%>
<%@page import="java.text.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>

<html>

<head>

<%=CommonConstants.getRACTStylesheet()%>

</head>

<body>

<span class="h2">GL Transactions</span>

<%
//move to the uic
FinanceMgr fMgr = PaymentEJBHelper.getFinanceMgr();

DateTime effStartDate = null;
DateTime effEndDate = null;
DateTime entStartDate = null;
DateTime entEndDate = null;
effStartDate = new DateTime("1/12/03");
effEndDate = new DateTime("31/12/03");

entStartDate = new DateTime("1/11/03");
entEndDate = new DateTime("31/12/03");

String subsystem = "5400";
String account = "ALL";
String entity = "ALL";
%>

<table>
<tr>
  <td>Effective Start Date</td><td><%=effStartDate%></td>
</tr>
<tr>
  <td>Effective End Date</td><td><%=effEndDate%></td>
</tr>
<tr>
  <td>Account</td><td><%=effEndDate%></td>
</tr>
</table>

<%
Collection glTransactions = fMgr.getGLTransactions(effStartDate,effEndDate,entStartDate,entEndDate,subsystem,account,entity);
//end of move
if (glTransactions != null && glTransactions.size() != 0)
{
%>
<table>
<tr>
  <td class="listHeading">Entered Date</td>
  <td class="listHeading">Effective Date</td>
  <td class="listHeading">Account</td>
  <td class="listHeading">Entity</td>
  <td class="listHeading">Sub Account</td>
  <td class="listHeading">Cost Centre</td>
  <td class="listHeading">Amount</td>
</tr>
<%
  GLTransaction glTrans = null;
  Iterator it = glTransactions.iterator();
  while (it.hasNext())
  {
    glTrans = (GLTransaction)it.next();
%>
<tr>
  <td><%=glTrans.getEnteredDate()%></td>
  <td><%=glTrans.getEffectiveDate()%></td>
  <td><%=glTrans.getAccount()%></td>
  <td><%=glTrans.getEntity()%></td>
  <td><%=glTrans.getSubAccount()%></td>
  <td><%=glTrans.getCostCentre()%></td>
  <td><%=CurrencyUtil.formatDollarValue(glTrans.getAmount())%></td>
</tr>

<%
  }
%>
</table>
<%
}
%>

</body>
</html>