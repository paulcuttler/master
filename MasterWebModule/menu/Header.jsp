<%@page import="com.ract.common.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.security.ui.LoginUIConstants"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
UserSession us = UserSession.getUserSession(session);
String backgroundColour = "";
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
String systemName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,SystemParameterVO.TYPE_SYSTEM);

if (CommonConstants.SYSTEM_DEVELOPMENT.equals(systemName))
{
  backgroundColour = "GREEN";
}
else if (CommonConstants.SYSTEM_TEST.equals(systemName))
{
  backgroundColour = "ORANGE";
}
else if (CommonConstants.SYSTEM_TRAINING.equals(systemName))
{
  backgroundColour = "PINK";
}
else if (CommonConstants.SYSTEM_PRODUCTION.equals(systemName))
{
  //normal colour
  backgroundColour = "#CDD7E1";
}
%>

<%--
Header contains information about the user currently logged into the application.
will contain the security information
--%>

<body bgcolor="<%=backgroundColour%>" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" rightmargin="0" bottommargin="0">
<table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0" >

<%
if (us != null)
{
%>
  <tr>
    <td><span class="dataValueYellow"><%=systemName%></span></td>
    <td><span class="infoTextSmall"><a href="#" title="<%=us.getUser().getRoles()%>"><%=us.getUserId()%></a></span></td>
    <td><span class="infoTextSmall"><%=us.getUser().getUsername()%></span></td>
    <td><span class="infoTextSmall"><%=us.getUser().getSalesBranchCode()%></span></td>
    <td><span class="infoTextSmall"><%=us.getUser().getPrinterGroup()%></span></td>
    <td><span class="infoTextSmall"><%=us.getIp()%></span></td>
    <td><span class="infoTextSmall"><%=us.getSessionStart().formatLongDate()%></span></td>
    <td>
       <a class="actionItem" title="Click on this link to log out of the system." target="_top" href="/LoginUIC?user=<%=us.getUserId()%>&logout=true">Logout</a>
    </td>
  </tr>
<%
}
else {
%>
  <tr>
    <td>
      <span class="infoTextSmall">User currently not logged in.</span> <a target="_top" href="/security/Login.jsp">Login</a>
    </td>
  </tr>
<%
}
%>
</table></body>
</html>
