<%@page import="com.ract.common.CommonConstants"%>
<html>
<head>
  <title>Blank</title>
  <%=CommonConstants.getRACTStylesheet()%>
</head>

<%-- include message of the day --%>

<body onload="javascript:parent.leftFrame.location='<%=CommonConstants.DIR_CLIENT%>/ClientList.jsp';">

<h1>Welcome to the RACT Roadside Application</h1>
<p/>

<h2>Only authorised users may access this system.</h2>

<p>
<i>Please note that MRM should be used to initiate all personal memberships</i>
</p>

<p>
Select an option from the menu or perform a search to begin.
</p>

</body>
</html>
