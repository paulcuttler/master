<%@page import="com.ract.common.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.security.ui.LoginUIConstants"%>

<%
UserSession us = UserSession.getUserSession(session);
String backgroundColour = "";
if(us.getSystemName()==null
  || us.getSystemName().equals(""))
{
   CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
   us.setSystemName(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,SystemParameterVO.TYPE_SYSTEM));
}
if (CommonConstants.SYSTEM_DEVELOPMENT.equals(us.getSystemName()))
{
  backgroundColour = "GREEN";
}
else if (CommonConstants.SYSTEM_TEST.equals(us.getSystemName()))
{
  backgroundColour = "ORANGE";
}
else if (CommonConstants.SYSTEM_PRODUCTION.equals(us.getSystemName()))
{
  //normal colour
  backgroundColour = "#CDD7E1";
}
%>

<%--
Header contains information about the user currently logged into the application.
will contain the security information
--%>

<div style="background:<%=backgroundColour%>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td><span class="dataValueYellow"><%=us.getSystemName()%></span></td>
    <td><span class="infoTextSmall"><a href="#" title="<%=us.getUser().getRoles()%>"><%=us.getUserId()%></a></span></td>
    <td><span class="infoTextSmall"><%=us.getUser().getUsername()%></span></td>
    <td><span class="infoTextSmall"><%=us.getUser().getSalesBranchCode()%></span></td>
    <td><span class="infoTextSmall"><%=us.getUser().getPrinterGroup()%></span></td>
    <td><span class="infoTextSmall"><%=us.getIp()%></span></td>
    <td><span class="infoTextSmall"><%=us.getSessionStart().formatLongDate()%></span></td>
 </tr>

</table></div>
</html>
