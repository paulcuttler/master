<%@include file="/security/VerifyAccess.jsp"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.notifier.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.w3c.dom.*"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<form name="notificationMgrForm" method="post" action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>" >
<input type="hidden" name="event" value="">
<input type="hidden" name="fileName" value="">
<%
String eventFileName = request.getParameter("eventFileName");
File eventFile = new File(NotificationUtil.getFailureDirectory(),eventFileName);
%>

<table>

<%
NotificationEventFailure failureEvent;
NotificationEvent event;
ArrayList failedSubscriberList;
Exception eventException;
NotificationEventSubscriberFailure subscriberFailure;
NotificationEventSubscriber subscriber;
Exception subscriberException;
NotificationMgr notificationMgr = CommonEJBHelper.getNotificationMgr();
failureEvent = notificationMgr.getNotificationFailure(eventFile);
event = failureEvent.getNotificationEvent();
%>
  <tr valign="top">
   <td colspan="2">
     <h1>Notification Failure - <%=event.toString()%></h1>
  </td>
  </tr>

<tr>
  <td class="label">Event:</td><td class="dataValue"><%=event.toString()%></a></td>
</tr>
<tr>
  <td class="label">Sent at:</td><td class="dataValue"><%=DateUtil.formatDate(event.getEventTime(),"dd/MM/yyyy HH:mm:ss:SS")%></td>
</tr>
<tr>
  <td class="label">Failed at:</td><td class="dataValue"><%=DateUtil.formatDate(failureEvent.getFailureTime(),"dd/MM/yyyy HH:mm:ss:SS")%></td>
</tr>
<tr>
  <td class="label">File name:</td><td class="dataValue"><%=eventFileName%></td>
</tr>
<%

         eventException = failureEvent.getException();
         failedSubscriberList = failureEvent.getFailedSubscriberList();


            if (eventException != null) {
               out.print("<tr><td  class=\"label\" valign=\"top\">Exception:</td><td valign=\"top\"><pre>");
               out.print(ExceptionHelper.getExceptionStackTrace(eventException));
               out.print("</pre></td></tr>");
            }

            if (failedSubscriberList != null) {
               for (int j = 0; j < failedSubscriberList.size(); j++) {
                  subscriberFailure = (NotificationEventSubscriberFailure) failedSubscriberList.get(j);
                  subscriber = subscriberFailure.getSubscriber();
                  subscriberException = subscriberFailure.getException();
%>
         <tr>
            <td class="label">Subscriber:</td>
            <td class="dataValue"><%=subscriber.getSubscriberName()%></td>
         </tr>
<%
                  if (subscriberException != null) {
                     out.print("<tr><td  class=\"label\" valign=\"top\">Subscriber exception:</td><td valign=\"top\"><pre>");
                     out.print(ExceptionHelper.getExceptionStackTrace(subscriberException));
                     out.print("</pre></td></tr>");
                  }

               }
            }
            else {
%>
         <tr>
            <td class="label">Subscriber:</td>
            <td class="dataValue">All subscribers!</td>
         </tr>
<%
            }
            String detail = event.getDetail();
            if (detail.length() > 0) {
%>
         <tr>
            <td class="label" valign="top">Detail:</td>
            <td class="dataValue"><xmp><%=event.getDetail()%></xmp></td>
         </tr>
<%
            }
%>
         <tr>
            <td colspan="2">
               <input type="submit" name="btnResend" value="Re-send" onclick="document.all.event.value='notificationMgr_btnResend'; document.all.fileName.value='<%=eventFileName%>';">
               &nbsp;&nbsp;
               <input type="submit" name="btnDelete" value="Delete" onclick="document.all.event.value='notificationMgr_btnDelete'; document.all.fileName.value='<%=eventFileName%>';">
            </td>
         </tr>
 </table>
 </form>
 </body>
