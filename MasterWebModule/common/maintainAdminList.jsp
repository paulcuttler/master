<%@ page import="java.util.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.MembershipUIConstants"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%
String altButton        = CommonConstants.DIR_IMAGES + "/AddButton_over.gif";
String button           = CommonConstants.DIR_IMAGES + "/AddButton.gif";

String uicPage          = (String) request.getSession().getAttribute("UICPage");
String msg              = (String) request.getSession().getAttribute("buttonMessage");
String createEvent      = (String) request.getSession().getAttribute("createEvent");
String selectEvent      = (String) request.getSession().getAttribute("selectEvent");
String title            = (String) request.getSession().getAttribute("pageTitle");
String selCode          = (String) request.getSession().getAttribute("codeName");
SortedSet thisList      = (SortedSet) request.getSession().getAttribute(CommonUIConstants.MAINTAIN_CODELIST);

String showAddButton    = StringUtil.makeSpaces((String) request.getAttribute("showAddButton"));
boolean addButtonVisible = ( showAddButton.equalsIgnoreCase("") ||showAddButton.equalsIgnoreCase("null") || showAddButton.equalsIgnoreCase("yes")) ? true : false;

String fullWidth         = StringUtil.makeSpaces((String) request.getAttribute("useFullWidth"));
boolean useFullWidth     = ( fullWidth .equalsIgnoreCase("") ||fullWidth .equalsIgnoreCase("null") || fullWidth.equalsIgnoreCase("no")) ? false :true;

uicPage = (uicPage == null) ? MembershipUIConstants.PAGE_MembershipAdminUIC : uicPage;

%>

<html>
<head>
<title>Maintain <%=title%></title>
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/MaintainListsHeaderButtons.js") %>
</head>

<body class="leftFrame" >
  <table border="0" width="100%" cellpadding="5" cellspacing="2">
    <tr>
      <td class="listHeading" align = "center" >
          <%=title%>
      </td>
    </tr>
    <tr>
    <% if ( addButtonVisible )
       {
   %>
      <td align="center" >
         <a>
            <img name="AddButton"
                 src="<%=button%>"
                 onclick="addNew('<%=uicPage%>','<%=createEvent%>')"
                 onMouseOver="mouseOverAdd('<%=msg%>','<%=altButton%>')"
                 onMouseOut="mouseOutAdd()"
                 border="0"
                 alt="<%=msg%>"
            >
        </a>
      </td>
    <%
       }
    %>
    </tr>
 </table>
<div style="height:85%; width:100%; overflow:auto; ">
<table border="0" >
<%
if (thisList != null)
{
  Iterator it = thisList.iterator();
  while (it.hasNext())
  {
    AdminCodeDescription cd = (AdminCodeDescription)it.next();
   %>
      <tr >
      <% if ( !useFullWidth )
         {
      %>
         <td width="20%" >&nbsp;</td>
      <%
         }
      %>
         <td align="left">
             <a class="actionItem"
                href= "<%=uicPage%>?event=<%=selectEvent%>&<%=selCode%>=<%=cd.getCode()%>" target= "rightFrame">
                <%=cd.getDescription()%>
            </a>
         </td>
       </tr>
   <%
  }
}
%>
</table>
</div>
</body>
</html>
