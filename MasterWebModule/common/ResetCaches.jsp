<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.pool.*"%>
<%@page import="net.sf.ehcache.CacheManager"%>

<html>
<head>
<title>Reset Caches</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<h1>Reset Caches</h1>

<ol>
<%
      MembershipRefMgr memRefMgr = MembershipEJBHelper.getMembershipRefMgr();
      memRefMgr.resetDiscountTypeCache();
%>
<li>Membership discount type cache reset</li>
<%
      memRefMgr.resetFeeTypeCache();
%>
<li>Membership fee type cache reset</li>
<%
      memRefMgr.resetFeeSpecificationCache();
%>
<li>Membership fee specification cache reset</li>
<%
      memRefMgr.resetJoinReasonCache();
%>
<li>Membership join reason cache reset</li>
<%
      memRefMgr.resetProductBenefitTypeCache();
%>
<li>Membership product benefit type cache reset</li>
<%
      memRefMgr.resetProductCache();
%>
<li>Membership product cache reset</li>
<%
      memRefMgr.resetMembershipAccountCache();
%>
<li>Membership account cache reset</li>
<%
      memRefMgr.resetMembershipTransactionTypeCache();
%>
<li>Membership transaction type cache reset</li>
<%
      memRefMgr.resetMembershipTypeCache();
%>
<li>Membership type cache reset</li>
<%
  memRefMgr.resetMembershipProfileCache();
%>
<li>Membership profile cache reset</li>
<%
      MembershipIDMgr memIDMgr = MembershipEJBHelper.getMembershipIDMgr();
      memIDMgr.reset();
%>
<li>Membership ID cache reset</li>
<%
      PaymentMgr payMgr = PaymentEJBHelper.getPaymentMgr();
      payMgr.resetPaymentTypeCache();
%>
<li>Payment type cache reset</li>
<%
      PaymentIDMgr payIDMgr = PaymentEJBHelper.getPaymentIDMgr();
      payIDMgr.reset();
%>
<li>Payment ID cache reset</li>
<%
      ClientMgr cMgr = ClientEJBHelper.getClientMgr();
      cMgr.resetClientOccupationCache();
%>
<li>Client occupation cache reset</li>
<%
      cMgr.resetClientTitleCache();
%>
<li>Client title cache reset</li>
<%
      CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
      commonMgr.resetSystemParameterCache();
%>
<li>System parameter cache reset</li>
<%
      commonMgr.resetSalesBranchCache();
%>
<li>Sales branch cache reset</li>
<!--  remove socket server cache -->
<%
      ProgressProxyObjectPool.removeCurrentObjectPool();
%>
<li>Progress proxy object pool reset</li>
<%
MethodCounter.resetCounters();
%>
<li>Method counter reset</li>
<%
CacheHandler.clearAll(); 
%>
<li>Cache handlers reset</li>
<%
FileUtil.clearPropertiesFileCache("master"); 
%>
<li>Properties file cache reset</li>

</ol>

</body>
</html>
