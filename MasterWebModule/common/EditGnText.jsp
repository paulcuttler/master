<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<title>
EditGnText
</title>
</head>
<body>
<h1>
Maintain Report Text
</h1>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>

<%=CommonConstants.getRACTStylesheet()%>
<%
GnTextVO textVO = null;
String subSystem = (String)request.getAttribute("subSystem");
String textType  = (String)request.getAttribute("textType");
String textName =  (String)request.getAttribute("textName");
if(subSystem!=null)
{
   CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
   textVO = commonMgr.getGnTextVO(subSystem,textType,textName);
}
if(textVO == null)
{
   textVO = new GnTextVO();
   textVO.setNotes("");
   textVO.setSubSystem("");
   textVO.setTextData("");
   textVO.setTextName("");
   textVO.setTextType("");
}

%>
<form method="post" action="/CommonAdminUIC" target="leftFrame">
  <hr>
  <div style="height:425; overflow: auto;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
        <td width="12%" valign="top">Sub System </td>
        <td width="88%">
          <input type="text" name="subSystem" value="<%=textVO.getSubSystem()%>">
     </td>
   </tr>
   <tr>
        <td width="12%" valign="top">Type </td>
        <td width="88%">
          <input type="text" name="textType" value="<%=textVO.getTextType()%>">
     </td>
   </tr>
   <tr>
        <td width="12%" valign="top">Name </td>
        <td width="88%">
          <input type="text" name="textName" value="<%=textVO.getTextName()%>">
     </td> 
   </tr> 
   <tr>
        <td width="12%" valign="top">Text </td>
        <td width="88%">
          <textarea name="textData" rows="14" cols="67" class="body"><%=textVO.getTextData()%></textarea>
     </td>
   </tr>
   <tr>
        <td width="12%" valign="top">Notes </td>
        <td width="88%">
          <textarea name="notes" cols="67" rows="4"><%=textVO.getNotes()%></textarea>
     </td>
   </tr>
   <tr>
     <td colspan=2>
       <hr>
	 </td>
	</tr>
</table>
</div>
<br><br>
<a onclick="document.forms[0].submit()"
   onMouseOut="MM_swapImgRestore();"
   onMouseOver="MM_displayStatusMsg('Save changes.');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
   <img name="SaveButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Save Changes.">
</a>
<input type="hidden" name="event" value="GnTextSave">
</form>
</body>
</html>
