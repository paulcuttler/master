<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="javax.sql.*"%>
<%@page import="javax.rmi.*"%>
<%@page import="javax.naming.*"%>

<html>

<head>
<title>
SQLExplorer
</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%!
final int MAX = 2000;

%>

<body>

<i>Record limit: <%=MAX%></i>

<h2>SQL Explorer</h2>

<%
String sql = request.getParameter("sql");
if (sql == null) sql = "";

DataSource dataSource = null;
try {
  dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
}
catch(Exception e) {
  throw new ServletException("Error looking up dataSource: " + e.toString());
}

%>

<p><b>SQL</b>: <%=sql%></p>

<p>
<form method="POST">
<textarea type="textarea" name="sql" rows="10" cols="50"><%=sql%>
</textarea>
<br>
<input type="submit" value="Execute query">
</form>
</p>

<table border="1">
<%
if (!sql.equals("")) {

    int counter = 0;
    Connection connection = null;
    Statement statement = null;
    try {
      connection = dataSource.getConnection();
      statement = connection.createStatement();

      ResultSet resultSet = statement.executeQuery(sql);
      ResultSetMetaData rsmd = resultSet.getMetaData();

      String columnName = "";
%>
<tr>
<%
      for (int i = 1; i <= rsmd.getColumnCount(); i++) {
        columnName = rsmd.getColumnName(i);
%><td><b><%=columnName.toUpperCase()%></b></td>
<%
      }
%>
</tr>
<%
      Object currentCol = null;
      while(resultSet.next() && counter != MAX) {
        counter++;
%>
<tr>
<%
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
          currentCol = resultSet.getObject(i);
          if (currentCol == null) {
%>
<td></td>
<%
          }
          else {
%><td><%=currentCol%></td><%
          }
        }
%>
</tr>
<%
      }
      if (counter == 0) {
%>

<%
      }
    }
    catch(SQLException e) {
      out.println("Error:<br/>");
      out.println(e.toString());
    }
    finally {
      ConnectionUtil.closeConnection(connection,statement);
    }

}
%>
</table>


</body>
</html>
