<%@ page import="com.ract.common.*"%>


<%--
Generic, self submitting splash page.
This page can be used where a change of screen is needed to show that an action has been commenced,
but no new input is required.
It automatically passes up to 3 parameters to the destination program.
Required request attributes:
  headingText
  bodyText
  sendTo
  event
  wait
Note wait = null => show no wait symbol
     wait = 1 => show hourglass
     wait = 2 => show clock
--%>
<%
String headingText = (String)request.getAttribute("headingText");
String bodyText = (String)request.getAttribute("bodyText");
String sendTo = (String)request.getAttribute("sendTo");
String event = (String)request.getAttribute("event");
String parameter1 = (String)request.getAttribute("parameter1");
String parameter2 = (String)request.getAttribute("parameter2");
String parameter3 = (String)request.getAttribute("parameter3");
String parameter4 = (String)request.getAttribute("parameter4");
String wait = (String)request.getAttribute("wait");
String alertMessage = (String)request.getAttribute("alertMessage");
if(wait==null)wait="";
%>

<html>
<head>
<title>
CoverPage
</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<h1>
<%=headingText%>
</h1>
<form name="viewMembershipVehicles" method="post" action="<%=sendTo%>">
<input type="hidden" name="event" value="<%=event%>">
<input type="hidden" name="parameter1" value="<%=parameter1%>">
<input type="hidden" name="parameter2" value="<%=parameter2%>">
<input type="hidden" name="parameter3" value="<%=parameter3%>">
<input type="hidden" name="parameter4" value="<%=parameter4%>">
<br><%=bodyText%><br>
</form>
</body>
</html>

<%if(wait.equals("2")){
%>
<script language="JavaScript">
    if(document.all)
      for(var i=0;i<document.all.length;i++)
         document.all(i).style.cursor='wait';
</script>
<%}%>
<script language="JavaScript">
   <%if(alertMessage!=null){%>
      alert("<%=alertMessage%>");
   <%}%>
   document.forms[0].submit();
</script>
