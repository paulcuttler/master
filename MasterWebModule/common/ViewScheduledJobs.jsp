<%@page import="com.ract.common.schedule.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="org.quartz.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="org.quartz.impl.StdScheduler"%>

<html>
<head>

<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<!--<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenance.js") %>-->
<script language="javascript">
   function setJobName(jobName)
   {
      document.all.jobName.value=jobName;
   }

   function submitForm(eventName)
   {
      document.all.event.value = eventName;
      var valid = true;//validateForm();
      if (valid)
      {
        document.all.mainForm.submit();
      }
   }

</script>
<title>View Scheduled Jobs</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>
<form name="mainForm" method="post" action="<%=request.getContextPath() + "/CommonAdminUIC"%>">

<input type="hidden" name="jobName" value="">
<input type="hidden" name="event" value="">

<div id="mainpage" class="pg">
    <h1 >View Scheduled Jobs</h1>
        <div id="contentpage" class="cnt">
            <table width="100%" height="100%">
            <tr valign="top">
            <td>
            <%
                ScheduleMgr sMgr = CommonEJBHelper.getScheduleMgr();
%>
            <table>
              <tr>
                <td class="label">Started:</td><td class="dataValue"><%=sMgr.isStarted()%></td>
              </tr>
              <tr>
                <td class="label">Standy Mode:</td><td class="dataValue"><%=sMgr.isInStandbyMode()%></td>
              </tr>
              <tr>
                <td class="label">Paused (Deprecated):</td><td class="dataValue"><%=sMgr.isPaused()%></td>
              </tr>
              <tr>
                <td class="label">Shutdown:</td><td class="dataValue"><%=sMgr.isShutdown()%></td>
              </tr>
            </table>
<%
                JobDetail thisJob = null;
                Trigger trigger[] = null;
                SimpleTrigger st = null;
                Trigger thisTrigger = null;
                String jobNames[] = sMgr.getJobNames(Scheduler.DEFAULT_GROUP);
                for (int i = 0; i < jobNames.length;i++)
                {
                  log("jobName="+jobNames[i]);
                }
                String createDateTime = null;
                String repeating = "";
                long repeatInterval=0;
                long days=0, hours, mins=0, secs=0, msecs=0;
                double secsDouble = 0;
                String intervalString = "";
                String userId = null;
                String details = null;
                String triggerDetails = null;
                Object parameter = null;
                if (jobNames.length != 0)
            {
            %>
            <table border="0" cellspacing="2" cellpadding="2" width="100%">
              <tr class="headerRow">
                <td>&nbsp;</td>
                <td>User ID</td>
                <td>Description</td>
                <td>Group</td>
                <td>Created</td>
                <td>Run Time</td>
              </tr>
              <%
            for (int i = 0; i < jobNames.length; i++)
            {
                  log("loop jobName="+jobNames[i]);            
              thisJob = sMgr.getJobDetail(jobNames[i],Scheduler.DEFAULT_GROUP);
              trigger = sMgr.getTriggersOfJob(jobNames[i],Scheduler.DEFAULT_GROUP);
              JobDataMap dataMap = thisJob.getJobDataMap();
              //should be a constant!!!
              createDateTime = dataMap.getString("createDateTime"); 
              if(createDateTime==null) createDateTime = ""; 
              userId = dataMap.getString("userId"); 
              if(userId==null)userId="";
              details = "Is Stateful: " + thisJob.isStateful()
                      + "\nIs Durable: " + thisJob.isDurable()
                      + "\nIs Volatile: " + thisJob.isVolatile()
                      + "\n\nParameters:";
              String[] keys = dataMap.getKeys();
              for(int xd = 0;xd<keys.length;xd++)
              {
                 parameter = dataMap.get(keys[xd]);
                 try {
                 	details += "\n" + keys[xd] + ":  " + parameter.toString();
                 } catch (Exception e) {}
              }
            %>
              <tr class="<%=HTMLUtil.getRowType(i)%>">
                <td>
                  <input type="radio" name="itemSelect" onClick="javascript:setJobName('<%=thisJob.getName()%>')">
                </td>
              <td>
                <%=userId%>
               </td>
                <td>
               <a title="<%=details%>"><%=thisJob.getDescription()%></a>

            </td>
            <td>
               <%=thisJob.getGroup()%>
            </td>
            <td>
               <%=createDateTime%>
            </td>
                <%
              for (int x = 0; x<trigger.length; x++)
              {
               repeating = "";
               intervalString = "";
               thisTrigger = trigger[x];
               triggerDetails = "Name: " + thisTrigger.getName()
                           + "\nStart Time: " + thisTrigger.getStartTime()
                           + "\nEnd Time: " + thisTrigger.getEndTime()
                           + "\nPrevious: " + thisTrigger.getPreviousFireTime()
                           + "\nNext: " + thisTrigger.getNextFireTime();
               if(thisTrigger.getFinalFireTime()==null)
               {
                  repeating = "(R)";
                  if(thisTrigger instanceof SimpleTrigger)
                  {
                     st = (SimpleTrigger)thisTrigger;
                     repeatInterval = st.getRepeatInterval();
                     triggerDetails += "\nRepeat Interval(ms): " + repeatInterval
                                 + "\nNumber of Repeats: " + st.getRepeatCount()
                                 + "\nTimes triggered: " + st.getTimesTriggered();
                     if(repeatInterval > 0)
                     {
                       msecs = repeatInterval % 1000;
                       secs  = repeatInterval / 1000;
                       mins  = secs/60;
                       secs  = secs%60;
                       hours = mins/60;
                       mins  = mins%60;
                       days  = hours/24;
                       hours = hours%24;
                       if(days>0)
                       {
                         intervalString = days + " days " + hours + ":" + mins;
                       }
                       else
                       {
                         secsDouble = secs + msecs / 1000;
                         intervalString = hours + " hours " + mins + ":" + secsDouble;
                       }
                       repeating = "<br>(R " + intervalString + ")";
                     }
                  }
               }
               else
               {
                  repeating = "";
               }
               if(x>0) //start new row with empty columns
                {%>
               </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <%}
              %>
                <td>

                  <a title="<%=triggerDetails%>"><%=DateUtil.formatLongDate(thisTrigger.getNextFireTime()) + repeating%></a>

          <!--          <%=DateUtil.formatLongDate(thisTrigger.getNextFireTime()) + repeating%>-->
                </td>
                <%
              }
              %>
              </tr>
              <%

            }
            %>
            </table>
            <%
            }
            else
            {
            %>
              <p>
              No scheduled jobs for any category were found.
              </p>
            <%
            }
            %>

            </td>
           </tr>
        </table>
        </div>
        <div id="buttonPage" class="btn">
            <table border="0" >
                <tr valign="bottom">
                <td>
                    <%=HTMLUtil.buttonRemove("CancelScheduledJob","Remove this Job from the Queue")%>
                </td>
                </tr>
            </table>
        </div>
</div>
</form>
</body>
</html>
