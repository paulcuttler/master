<%
//this jsp should be included above the body to prevent caching. However,
//this prevents the back button.

response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Expires", "-1"); //prevents caching at the proxy server
response.setHeader("Pragma", "no-cache"); //HTTP 1.0
response.addHeader("Cache-control", "no-store"); // tell proxy not to cache
response.addHeader("Cache-control", "max-age=0"); // stale right away
%>