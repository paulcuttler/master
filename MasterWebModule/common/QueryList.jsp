<%@ page import="java.util.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.common.reporting.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>

<script type="text/javascript">
function submitQueryListForm(event,queryCode)
{
  document.all.event.value = event;
  document.all.queryCode.value = queryCode;
  document.all.queryList.submit();
}
</script>

</head>
<h2>Stored Queries</h2>

<%
Collection queryList = (Collection)request.getAttribute("queryList");
Query query = null;
String queryName = "";
String queryCode = "";
String queryDescription = "";
String queryText = "";
String queryCategory = "";
String prevQueryCategory = null;

if (queryList != null)
{

TreeSet queryTreeSet = new TreeSet(queryList);

%>
<form name=queryList>
<input type="hidden" name="event"/>
<input type="hidden" name="queryCode"/>
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="headerRow">
  <td></td>
  <td class="listHeadingPadded">Category</td>
  <td class="listHeadingPadded">Name</td>
  <td class="listHeadingPadded">Description</td>
  <td class="listHeadingPadded"></td>
</tr>
<%
  Iterator it = queryTreeSet.iterator();
  int counter = 0;
  User user = userSession.getUser();
  while (it.hasNext())
  {
    query = (Query)it.next();
    queryCategory = query.getQueryCategory();
    queryName = query.getQueryName();
    queryCode = query.getQueryCode();
    queryText = query.getQueryText();
    queryDescription = query.getQueryDescription();
    //is logged in user privileged
    if (user.isPrivilegedUser(query.getQueryPrivilege()))
    {
      counter++;
%>
  <tr class="<%=HTMLUtil.getRowType(counter)%>">
    <td></td>
    <td class="listHeadingPadded"><%=queryCategory!=null&&(!queryCategory.equals(prevQueryCategory))?queryCategory:""%></td>
    <td><a title="<%=queryText%>" href="<%=CommonUIConstants.PAGE_CommonAdminUIC%>?event=enterQueryParameters&queryCode=<%=queryCode%>"><%=queryName%></a></td>
    <td><%=queryDescription!=null?queryDescription:""%></td>
    <td>
<%
         // Show the edit transaction button if the user has the privilege to do so.
         if (userSession != null
         &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_EDIT_QUERY))
         {
%>
  <input type="button" value="Edit" onclick="submitQueryListForm('editQuery','<%=queryCode%>')">
<%
         }
%>
    </td>
  </tr>
<%
    }
    prevQueryCategory = queryCategory;
  }
%>
</table>
<%
}
else
{
%>
  <p>No queries found.</p>
<%
}
%>

<%
         // Show the edit transaction button if the user has the privilege to do so.
         if (userSession != null
         &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_EDIT_QUERY))
         {
%>
  <input type="button" value="New" onclick="submitQueryListForm('createQuery','')">
<%
         }
%>
</form>
</html>
