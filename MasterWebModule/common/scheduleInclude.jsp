<%@ page import="java.util.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.schedule.*"%>

<%
DateTime currentDate = new DateTime().getDateOnly();
%>

<script language="Javascript">

function show()
{
    var elem = 'sched';
    var obj = document.getElementById(elem);
    var radioValue;
    var radioObj = document.all.runType;
    if(!radioObj)
    {
      return;
    }
    var radioLength = radioObj.length;
    for(var i = 0; i < radioLength; i++)
    {
      if(radioObj[i].checked)
      {
        radioValue = radioObj[i].value;
        if (radioValue == 'scheduleLater')
        {
          obj.style.display = '';
        }
        else
        {
          obj.style.display = 'none';
        }
      }
    }


}

</script>

<table width="100%" border="0">
  <tr >
    <td colspan="2">
      <h3>Run Options</h3>
    </td>
  </tr >
  <tr >
    <td width="22">
      <input type="radio" name="runType" value="<%=ScheduleMgr.MODE_RUN_NOW%>" onclick="show()">
    </td>
    <td>Run Now </td>
  </tr>
  <tr id="schedule">
    <td width="22">
<input type="radio" name="runType" value="<%=ScheduleMgr.MODE_SCHEDULE_LATER%>" checked onclick="show()">
    </td>
    <td>Schedule later</td>
  </tr>
  <tr id="sched" style="">
    <td width="22" height="27">&nbsp;</td>
    <td height="27">
	  <table>
	     <tr>
		 <td>Date:</td><td>
      <input type = "text" name = "runDate" size = "11" value = "<%=currentDate.formatShortDate()%>"
         onKeyUp = "javascript:checkDate(this)">
      <span class="helpTextSmall">(dd/MM/yyyy)</span></td>
	  </tr>
	  <tr>
	  <td>Time:</td>
	  <td>
<input type = "text" name = "runHours" size = "2" value = "20"
             onKeyUp = "javascript:checkInteger(this)"
             onBlur = "javascript:checkHours(this)">
      :
      <input type = "text" name = "runMins" size = "2" value = "30"
             onKeyUp = "javascript:checkInteger(this)"
             onBlur = "javascript:checkMins(this)">
            <span class="helpTextSmall">(24 hour time)</span></td>
          </tr>
        </table>
    </td>
  </tr>
</table>

