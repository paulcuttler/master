<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.reporting.*"%>

<%
String xmlResults = (String)request.getAttribute("xmlResults");
String query = (String)request.getAttribute("query");
String key = "key";

//convert to html

Hashtable attr = XMLHelper.getResultSetAttributes(xmlResults);
Integer colCount = (Integer)attr.get(ReportMgr.TAG_COLUMN_COUNT);
Integer size = (Integer)attr.get(ReportMgr.TAG_RESULTSETSIZE);

String htmlResults = HTMLUtil.buildHTMLResults(xmlResults);

%>
<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<h2>Results</h2>

<p>Query produced <%=size%> results.</p>

<%=htmlResults%>

<form action="<%=CommonConstants.REPORT_DATA_SERVER%>">
	<input type="hidden" name="event" value="saveCSV">
	<input type="hidden" name="<%=ReportMgr.XML_REPORT_KEY%>" value="<%=query%>">
	<input type="submit" onclick="this.form.event.value='saveCSV'" value="Save as CSV">
	<!-- not currently licensed -->
	<!-- input type="submit" onclick="this.form.event.value='savePDF'" value="Save as PDF" -->
	<input type="checkbox" name="excludeHeader" value="true" id="header" /><label for="header">Exclude header row?</label>
</form>

</html>
