<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.payment.finance.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.common.*"%>

<html>
<head>
<title></title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>

<h2>Test Util</h2>

<%
String stockCode = request.getParameter("stockCode");
String profitCentre = request.getParameter("profitCentre");

if (stockCode == null ||
    profitCentre == null)
{
  out.println("Enter stock code and profit centre!");
  return;
}

//test server code
StockItem stockItem = null;
try
{
  FinanceAdapter finAdapter = PaymentFactory.getFinanceAdapter();
  stockItem = finAdapter.getStockItem(stockCode,profitCentre);
  if (stockItem != null)
  {
    out.println(stockItem.toString());
  }
}
catch (Exception e)
{
  e.printStackTrace();
  out.println(e.getMessage());
}
%>

</body>
</html>
