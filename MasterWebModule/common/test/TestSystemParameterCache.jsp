<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.common.*"%>

<html>
<head>
<title>TestSystemParameterCache</title>
</head>
<body>
<h1>TestSystemParameterCache</h1>
<%
CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
Collection paramList = comMgr.getCachedSystemParameterList();
if (paramList != null)
{
%>
<table border="1">
<%
   SystemParameterVO sysParamVO = null;
   Iterator listIT = paramList.iterator();
   while (listIT.hasNext())
   {
      sysParamVO = (SystemParameterVO) listIT.next();
%>
      <tr>
         <td><%=sysParamVO.getSystemParameterPK().getCategory()%></td>
         <td><%=sysParamVO.getSystemParameterPK().getParameter()%></td>
         <td><%=sysParamVO.getValue()%></td>
      </tr>
<%
   }
%>
</table>
<%
}
else
{
%>
System parameter cache list is empty.
<%
}
%>
</body>
</html>
