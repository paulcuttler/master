<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.reflect.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.pool.*"%>
<%@ page import="org.apache.commons.pool.*"%>
<%@ page import="org.apache.commons.pool.impl.*"%>

<%!
//convenience method
private String constructLabel(String objectType,String state)
{
  return "Total "+state+" "+objectType+" progress proxy objects in pool";
}
%>

<%

//object pool class name
//object factory fields

GenericKeyedObjectPool progressProxyPool = ProgressProxyObjectPool.getInstance();

Field[] keyList = new ProgressProxyObjectFactory().getClass().getFields();
%>

<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>
<h2>
Progress Proxy Pool Details
</h2>


<table>
<tr>
  <td>Maximum active</td><td class="dataValue"><%=progressProxyPool.getMaxActive()%></td>
</tr>
<tr>
  <td>Maximum idle</td><td class="dataValue"><%=progressProxyPool.getMaxIdle()%></td>
</tr>
<tr>
  <td>Maximum wait</td><td class="dataValue"><%=progressProxyPool.getMaxWait()%></td>
</tr>
<tr>
  <td>Minimum evictable idle time (ms)</td><td class="dataValue"><%=progressProxyPool.getMinEvictableIdleTimeMillis()%></td>
</tr>
<tr>
  <td>Total number active</td><td class="dataValue"><%=progressProxyPool.getNumActive()%></td>
</tr>
<tr>
  <td>Total number idle</td><td class="dataValue"><%=progressProxyPool.getNumIdle()%></td>
</tr>
<tr>
  <td>Number of tests per eviction</td><td class="dataValue"><%=progressProxyPool.getNumTestsPerEvictionRun()%></td>
</tr>
<tr>
  <td>Test on borrow</td><td class="dataValue"><%=progressProxyPool.getTestOnBorrow()%></td>
</tr>
<tr>
  <td>Test on return</td><td class="dataValue"><%=progressProxyPool.getTestOnReturn()%></td>
</tr>
<tr>
  <td>Test while idle</td><td class="dataValue"><%=progressProxyPool.getTestWhileIdle()%></td>
</tr>
<tr>
  <td>Time between eviction runs (ms)</td><td class="dataValue"><%=progressProxyPool.getTimeBetweenEvictionRunsMillis()%></td>
</tr>
<tr>
  <td>When exhausted action</td><td class="dataValue"><%=progressProxyPool.getWhenExhaustedAction()%></td>
</tr>
</table>

<table>

<%
String keyName = null;
String keyValue = null;
for (int i = 0; i < keyList.length; i++)
{
  keyName = ((Field)keyList[i]).getName();
  keyValue = ((Field)keyList[i]).get(keyName).toString();
%>
<tr>
  <td><%=constructLabel(keyValue,"idle")%></td><td class="dataValue"><%=progressProxyPool.getNumIdle(keyValue)%></td>
</tr>
<tr>
  <td><%=constructLabel(keyValue,"active")%></td><td class="dataValue"><%=progressProxyPool.getNumActive(keyValue)%></td>
</tr>
<%
}
%>
</table>

</body>
</html>
