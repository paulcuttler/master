<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.HTMLUtil"%>

<html>
<head>
<title>
MaintainGnText
</title>
</head>
<body>
<h1>
Maintain Report Text
</h1>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>

<%=CommonConstants.getRACTStylesheet()%>
<%
GnTextVO textVO = null;
String subSystem = (String)request.getAttribute("subSystem");
String textType  = (String)request.getAttribute("textType");
String textName =  (String)request.getAttribute("textName");
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
textVO = commonMgr.getGnTextVO(subSystem,textType,textName);
if(textVO == null)
{
   textVO = new GnTextVO();
   textVO.setNotes("");
   textVO.setSubSystem("");
   textVO.setTextData("");
   textVO.setTextName("");
   textVO.setTextType("");
}

%>
<form method="post" action = "/CommonAdminUIC">
  <hr>
  <div style="height:425; overflow: auto;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
        <td width="12%" valign="top">Sub System </td>
        <td width="88%">
          <%=textVO.getSubSystem()%>
     </td>
   </tr>
   <tr>
        <td width="12%" valign="top">Type </td>
        <td width="88%">
          <%=textVO.getTextType()%>
     </td>
   </tr>
   <tr>
        <td width="12%" valign="top">Name </td>
        <td width="88%">
          <%=textVO.getTextName()%>
     </td>
   </tr>
   <tr>
        <td width="12%" valign="top">Text </td>
        <td width="88%">
          <%=HTMLUtil.showTags(textVO.getTextData())%>
     </td>
   </tr>
   <tr>
        <td width="12%" valign="top">Notes </td>
        <td width="88%">

          <%=HTMLUtil.showTags(textVO.getNotes())%>

     </td>
   </tr>
   <tr>
     <td colspan=2>
       <hr>
	 </td>
	</tr>
</table>
</div>
<br><br>
<a onclick="document.forms[0].submit()"
   onMouseOut="MM_swapImgRestore();"
   onMouseOver="MM_displayStatusMsg('Edit new entry.');MM_swapImage('EditButton','','<%=CommonConstants.DIR_IMAGES%>/EditButton_over.gif',1);return document.MM_returnValue" >
   <img name="EditButton" src="<%=CommonConstants.DIR_IMAGES%>/EditButton.gif" border="0" alt="Edit details.">
</a>
<input type="hidden" name="event" value="GnTextEdit">
<input type="hidden" name="subSystem" value="<%=subSystem%>">
<input type="hidden" name="textType" value="<%=textType%>">
<input type="hidden" name="textName" value="<%=textName%>">
</form>
</body>
</html>
