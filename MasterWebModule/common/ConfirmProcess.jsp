<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="java.util.*"%>

<%--
author: dgk, jyh

generic process completed page that can be sent a heading and associated text.

--%>
<%
String heading = (String) request.getAttribute("heading");
String message = (String) request.getAttribute("message");
if (heading == null) {
   heading = "Confirmation";
}
if (message == null) {
   message = "The task completed successfully";
}
%>
<html>
<head>
<title>Process Completed</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<h2><%=heading%></h2>

<p>
<%=message%>

<%
//locate a hashtable variable and display it

Object key = null;
Object value = null;
Integer numValue = null;
int total = 0;

Hashtable tableData = (Hashtable)request.getAttribute("tableData");
TreeMap tableMap = null;

if (tableData != null && !tableData.isEmpty())
{
  //sort the table into a natural order
  tableMap = new TreeMap(tableData);
}


if (tableMap != null && tableMap.size() != 0)
{
%>
<table>
<%
  Object keys[] = (tableMap.keySet()).toArray();

  for (int i = 0; i < keys.length; i++)
  {
    key = keys[i];
    value = tableMap.get(key);
    if (value instanceof Integer)
    {
      //cast to integer
      numValue = (Integer)value;
      //add to total
      total += numValue.intValue();
    }

  %>
  <tr>
    <td class="label"><%=key%></td>
    <td class="dataValue"><%=numValue.toString()%></td>
  </tr>
  <%
  }
%>
  <tr>
    <td class="label">Total</td>
    <td class="dataValue"><%=total%></td>
  </tr>
</table>
<%
}
%>

</p>

</body>
</html>
