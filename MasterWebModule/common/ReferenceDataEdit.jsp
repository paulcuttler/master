<%@page import="com.ract.common.ui.CommonUIConstants"%>
<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.HTMLUtil"%>

<%
String saveEvent     = "ReferenceDataSave";
String deleteEvent   = "ReferenceDataDelete";
//String listPage = MembershipUIConstants.PAGE_MAINTAIN_ADMIN_LIST;
String statMsg       = (String) request.getAttribute("statusMessage");
String refresh       = (String) request.getAttribute("Refresh");
refresh = " ";
statMsg = " ";
String refType = (String)request.getAttribute("refType");
String refCode = (String)request.getAttribute("refCode");

ReferenceDataVO refVO = null;
boolean refActive     =false;
String sortOrder      = "";
String description    = "";

if(refCode!=null && !"".equals(refCode))
{
   CommonMgr mgr = CommonEJBHelper.getCommonMgr();
   refVO         = mgr.getReferenceData(refType,refCode);
   refActive     = refVO.isActive();
   sortOrder     = "" + refVO.getSortOrder();
   description   = refVO.getDescription();
}
else
{
   refVO = new ReferenceDataVO();
   refType = "";
   refCode = "";
}


%>
<html>
<head>
<title>ReferenceDataView</title>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body class="rightFrame" >

<form method="post" name="mainForm" action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>" target="leftFrame">

<input type="hidden" name="event" value="ReferenceDataSave">

<div id="mainpage" class="pg">
 <h1>Maintain Reference Data</h1>
   <div id="contentpage" class="cnt">
<!--
   START REFERENCE DATA BODY
-->
      <table>
         <tr>
           <td>Reference Type</td>
           <td>
               <%=HTMLUtil.inputOrReadOnly("refType",refType)%>
               <!--<input type="text" name="refType" value="<%=refType%>">-->
           </td>
         </tr>
         <tr>
           <td>Reference Code </td>
           <td>
               <%=HTMLUtil.inputOrReadOnly("refCode",refCode)%>
               <!--<input type="text" name="refCode" value="<%=refCode%>">-->
           </td>
         </tr>
         <tr>
           <td>Sort Order</td>
           <td><input type="text" name="sortOrder" value="<%=sortOrder%>"
             onkeyup = "javascript:checkInteger(this)"></td>
         </tr>
         <tr>
           <td>Active</td>
           <td>
              <select name="refActive">
                <option value="true" <%=refActive?"SELECTED":""%>>True</option>
                <option value="false" <%=!refActive?"SELECTED":""%>>False</option>
              </select>
            </td>
         </tr>
         <tr>
           <td>Description</td>
           <td>
              <textarea name="description" cols="50" rows="3"><%=description%></textarea>
            </td>
         </tr>
      </table>
<!--
   END REFERENCE DATA BODY
-->

   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSave(saveEvent,"Reference Data" )%>
        <%=HTMLUtil.statusMessage(statMsg) %>
   </div>
</div>
</form>
</body>
</html>
