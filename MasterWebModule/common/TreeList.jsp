<%@page import="com.ract.common.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.util.*"%>
<%--
TreeList.jsp

Displays a "folding tree" in the form of nested unordered lists,
and provides the facility to "open and close" branches
(  Smart Folding Menu tree- By Dynamic Drive (rewritten 03/03/02)
   For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
)

Supply the following request attributes:
heading      (String) the heading at the top of the list
htmlString   (String) the HTML definition of the
sendTo       (String) the destination servlet for the "Add" button
event        (String) the event name to be run for the "Add" function

Return parameters:
If the "Add" button is chosen, returns the 'event' value as a request paremeter

If one of the tree leaves is clicked, the url supplied with the leaf
in the htmlString is called.

The htmlString can be constructed using the TreeBranch.buildTree method.
For details of the format see documentation with TreeBranch.buildTree, or
the authors web site (http://www.dynamicdrive.com)
--%>
<%
String altButton        = CommonConstants.DIR_IMAGES + "/AddButton_over.gif";
String button           = CommonConstants.DIR_IMAGES + "/AddButton.gif";
String msg              = "Add new entry";

String heading          = (String)request.getAttribute("heading");
String htmlString       = (String)request.getAttribute("htmlString");
String sendTo           = (String)request.getAttribute("sendTo");
String eventText        = (String)request.getAttribute("event");
String readOnlyString   = (String)request.getAttribute("readOnly");
boolean readOnly = readOnlyString == null?false:readOnlyString.equals("true");
//tree structure may be empty, in which case do not attempt to create listing
%>
<html>
<head>
<title>
GnTextList
</title>
<style>
<!--
#foldheader{cursor:pointer;cursor:hand ; font-weight:bold ;}
#foldinglist{font-weight:normal;}
//-->
</style>
<script language="JavaScript1.2">
//Smart Folding Menu tree- By Dynamic Drive (rewritten 03/03/02)
//For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
//This credit MUST stay intact for use

var head="display:''"
var ns6=document.getElementById&&!document.all
var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1

function checkcontained(e)
{
   var iscontained=0
   cur=ns6? e.target : event.srcElement
   i=0
   if (cur.id=="foldheader")
   iscontained=1
   else
   while (ns6&&cur.parentNode||(ie4&&cur.parentElement))
   {
      if (cur.id=="foldheader"||cur.id=="foldinglist")
      {
         iscontained=(cur.id=="foldheader")? 1 : 0
         break
      }
      cur=ns6? cur.parentNode : cur.parentElement
   }

   if (iscontained)
   {
      var foldercontent=ns6? cur.nextSibling.nextSibling : cur.all.tags("UL")[0]
      if (foldercontent.style.display=="none")
      {
         foldercontent.style.display=""
         //cur.style.listStyleImage="url(open.gif)"
      }
      else
      {
         foldercontent.style.display="none"
         //cur.style.listStyleImage="url(fold.gif)"
      }
   }
}

if (ie4||ns6)
document.onclick=checkcontained

//-->
</script>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/MaintainListsHeaderButtons.js") %>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body class="leftFrame">
<!--<body topmargin=16 marginheight=16 class="leftFrame">

<form method="post" action="<%=sendTo%>" target="rightFrame">-->

<input type="hidden" name="event" value="<%=eventText%>">

  <table border="0" width="100%" cellpadding="5" cellspacing="2">
    <tr>
      <td class="listHeading" align = "center" >
          <%=heading%>
      </td>
    </tr>
    <tr>
      <td align="center" >
<%
if (!readOnly)
{
%>
         <a>
            <img name="AddButton"
                 src="<%=button%>"
                 onclick="addNew('<%=sendTo%>','<%=eventText%>')"
                 onMouseOver="mouseOverAdd('<%=msg%>','<%=altButton%>')"
                 onMouseOut="mouseOutAdd()"
                 border="0"
                 alt="<%=msg%>"
            >
        </a>
<%
}
%>
      </td>
    </tr>
 </table>
 <div style="height:85%; width:100%; overflow:auto; ">
   <table border="0" >
      <tr>
         <td>
            <%=htmlString%>
         </td>
      </tr>
   </table>
 </div>

</body>
</html>
