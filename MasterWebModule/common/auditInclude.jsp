<%@ taglib prefix="s" uri="/struts-tags" %>
<!-- audit tags -->
<s:label name="lastUpdateId" label="Last Updated By" value="%{lastUpdateId}"></s:label>
<tr><td class="tdLabel">Last Updated:</td><td><s:date name="lastUpdate" format="dd/MM/yyyy HH:mm:ss"/></td></tr>