<%@ page import="java.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.reporting.*"%>


<%
Query query = (Query)request.getAttribute("query");
%>

<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<%--
send request to the data server to process
--%>

<form action="<%=CommonConstants.REPORT_DATA_SERVER%>">

<input type="hidden" name="query" value="<%=query.getQueryCode()%>">

<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">

<tr valign="top">
<td>
<table>
<tr>
  <td colspan="3"><h2><%=query.getQueryName()%></h2></td>
</tr>
<tr>
  <td colspan="3" class="helpTextSmall">The wildcard symbol is "%".</td>
</tr>

<tr>
  <td colspan="3" class="dataValue"><%=query.getQueryDescription() != null? query.getQueryDescription():""%></td>
</tr>

<%
Collection queryParameters = query.getQueryParameters();

QueryParameter queryParameter = null;

//order parameters in natural sort order
TreeSet queryTreeSet = new TreeSet(queryParameters);

if (queryTreeSet != null && queryTreeSet.size() > 0)
{
  Iterator it = queryTreeSet.iterator();
  while (it.hasNext())
  {
    queryParameter = (QueryParameter)it.next();
%>
   <tr>
     <td><%=queryParameter.getParameterLabel()%></td><td><input type="text" name="<%=queryParameter.getQueryParameterPK().getParameterName()%>"></td><td class="helpTextSmall"><%=queryParameter.getParameterDescription()%></td>
   </tr>
<%
  }
}
else
{
%>
<tr>
  <td class="helpText">No parameters required for the query.</td>
</tr>
<%
}
%>
</table>
</td>
</tr>


<tr valign="bottom">
  <td colspan="2"><input type="submit" value="Execute"><td>
</tr>

</table>

</form>

</html>
