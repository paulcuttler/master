<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.common.CommonConstants"%>
<html>
<head>
<title>Untitled Document</title>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>
<%=CommonConstants.getRACTStylesheet()%>
<%
String directTo = (String)request.getAttribute("directTo");
String year = (String)session.getAttribute("year");
if(year==null) year="";
%>
<script language="javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js">
</script>
<Script language = "javascript">
function next(){
  document.getElementById("waitText").innerHTML="<h2>Please wait while the list is loaded</h2>";
  document.vehicleYear.submit();
}
</script>

<body onLoad="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif'); document.all.year.focus(); document.all.year.select();">
<form name="form1" method="post" action="">
</form>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form name="vehicleYear" method="post"  action="<%= directTo%>">
<input type="hidden" name = "event" value = "MotorVehicleYear_Next">
<%--
<input type="hidden" name="SourceContext" value="<%=request.getAttribute("SourceContext")%>">
--%>
<input type="hidden" name="returnTo" value="<%=request.getAttribute("returnTo")%>">
<input type="hidden" name="returnEvent" value="<%=request.getAttribute("returnEvent")%>">
<%--
<input type="hidden" name="refNumber" value="<%=request.getAttribute("refNumber")%>">
--%>
<tr valign="top">
   <td>
      <h1>Motor Vehicle Selection</h1>
      <table border="0" cellpadding="3" cellspacing="0">
      <tr>
            <td>
              <p>Enter the year of manufacture of the motor vehicle (2 digits
                only).</p>
              <p><span class="helpTextSmall">Leave the year blank to delete a vehicle.</span><br/>
                <input type="text" name="year" size="5" maxlength="2" value="<%=year%>"
                    onkeyup = "javascript:checkInteger(this)">
              </p>
            </td>
      </tr>
      <tr>
         <td><span id="waitText"><h2>&nbsp;</h2></span></td>
      </tr>
      </table>
   </td>
</tr>
<tr valign="bottom">
   <td>
      <table border="0" cellpadding="5" cellspacing="0">
      <tr>
         <td>
            <a onclick="javascript:history.back();"
               onMouseOut="MM_swapImgRestore();"
               onMouseOver="MM_displayStatusMsg('Return to the previous screen.');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
               <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Return to the previous screen.">
            </a>
         </td>
         <td>
            <a onclick="javascript:next();"
               onMouseOut="MM_swapImgRestore();"
               onMouseOver="MM_displayStatusMsg('Go to the next page.');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
               <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Go to the next page.">
            </a>
         </td>
<%--
         <td>
              <input type="button" name="Back" value="Back" onclick="javascript:history.back()">
              <input type="button" name="Next" value="Next"
                    onclick="javascript:next()">
         </td>
--%>
      </tr>
      </table>
   </td>
</tr>
</form>
</table>
</body>
</html>
