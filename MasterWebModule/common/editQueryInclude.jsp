<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.reporting.*"%>
<%@page import="com.ract.security.*"%>

<html>
<head>
<title>\</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">
function addQueryParameter(obj)
{
  if (document.all.parameterName.value == '' ||
      document.all.parameterLabel.value == '')
  {
    alert('Parameter name and label must be entered.')
  }
  else
  {
	 //set event
	 document.all.event.value = 'addQueryParameter';
	 //submit form
	 document.all.query.submit();
  }
}

function removeQueryParameter(pName)
{
  //set event
  document.all.event.value = 'removeQueryParameter';
  
  document.all.removeParameterName.value = pName;
  
  //submit form
  document.all.query.submit();
}

function moveParameterUp(pName)
{
  //set event
  document.all.event.value = 'moveParameterUp';
  
  document.all.selectedParameterName.value = pName;
  
  //submit form
  document.all.query.submit();
}

function moveParameterDown(pName)
{
  //set event
  document.all.event.value = 'moveParameterDown';

  document.all.selectedParameterName.value = pName;
  
  //submit form
  document.all.query.submit();
}

function saveQuery()
{
  //set event
  document.all.event.value = 'saveQuery';

  //submit form
  document.all.query.submit();
}

</script>
</head>

<%
Query query = (Query)session.getAttribute("query");

String pageHeading = (String)request.getAttribute("pageHeading");
String formAction = (String)request.getAttribute("formAction");

String parameterName = null;
String parameterLabel = null;
String parameterType = null;
String parameterDescription = null;
int parameterOrder = 0;

SecurityMgr securityMgr = SecurityEJBHelper.getSecurityMgr();
Collection privs = securityMgr.getAllPrivileges();
%>

<body>
<form name="query" method="post" action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>">
  <input type="hidden" name="formAction" value="<%=formAction%>"/>
  <input type="hidden" name="selectedParameterName"/> 
  <input type="hidden" name="removeParameterName"/>  
  <input type="hidden" name="event" value=""/>  
  <table width="100%" height="100%" border="0">
    <tr valign="top">
      <td>
        <table border="0">
          <tr>
            <td colspan="2">
              <h1><%=pageHeading%></h1>
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap">Query Code:</td>
            <td>
<%
if (formAction.equals("createQuery"))
{
%>              
              <input type="text" name="queryCode" value="<%=query.getQueryCode()==null?"":query.getQueryCode()%>">
<%
}
else if (formAction.equals("editQuery"))
{	
%>
              <%=query.getQueryCode()%>
              <input type="hidden" name="queryCode" value="<%=query.getQueryCode()%>">
<%
}
%>              
            </td>
          </tr>
          <tr valign="top">
            <td nowrap="nowrap">Query Name:</td>
            <td>
              <input type="text" size="50" name="queryName" value="<%=query.getQueryName()==null?"":query.getQueryName()%>">
            </td>
          </tr>
          <tr valign="top">
            <td nowrap="nowrap">Query Description:</td>
            <td>
              <input type="text" size="100" name="queryDescription" value="<%=query.getQueryDescription()==null?"":query.getQueryDescription()%>">
            </td>
          </tr>          
          <tr>
            <td valign="top" nowrap="nowrap">Query Text:</td>
            <td>
              <textarea name="queryText" cols="100" rows="10"><%=query.getQueryText()==null?"":query.getQueryText()%></textarea>
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap">Time Restricted:</td>
            <td>
              <input <%=query.getTimeRestricted()!=null?(query.getTimeRestricted().booleanValue()?"checked":""):""%> type="checkbox" name="timeRestricted" value="Yes">
            </td>
          </tr>
          <tr>
            <td  nowrap="nowrap" height="22">Query Category:</td>
            <td height="22">
              <input type="text" name="queryCategory" value="<%=query.getQueryCategory()==null?"":query.getQueryCategory()%>">
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap">Query Privilege:</td>
            <td>
            
<select name="queryPrivilege">
            <option value=""></option>
<%
Privilege priv;
for (Iterator i = privs.iterator();i.hasNext();)
{
  priv = (Privilege)i.next();
 %>
  	<option <%=priv.getPrivilegeID().equals(query.getQueryPrivilege())?"selected":""%> value="<%=priv.getPrivilegeID()%>"><%=priv.getName()%></option>
<%
}
 %>
            
</select>
               
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap">Datasource Name:</td>
            <td><select name="dataSourceName">
            <option <%=CommonConstants.DATASOURCE_CLIENT.equals(query.getDataSourceName())?"selected":""%> value="<%=CommonConstants.DATASOURCE_CLIENT%>">RACT Client Database</option>
            <option <%=CommonConstants.DATASOURCE_DW.equals(query.getDataSourceName())?"selected":""%> value="<%=CommonConstants.DATASOURCE_DW%>">Data Warehouse</option>           
            <option <%=CommonConstants.DATASOURCE_WEB.equals(query.getDataSourceName())?"selected":""%> value="<%=CommonConstants.DATASOURCE_WEB%>">Web</option>            
            <option <%=CommonConstants.DATASOURCE_PROSPER.equals(query.getDataSourceName())?"selected":""%> value="<%=CommonConstants.DATASOURCE_PROSPER%>">Prosper</option>            
            </select>
            </td>
          </tr>
        </table>
        <h2>Add Parameter</h2>
        <table>
          <tr class="headerRow" >
            <td class="listHeadingPadded">Name</td>
            <td class="listHeadingPadded">Screen Label</td>
            <td class="listHeadingPadded">Type</td>
            <td class="listHeadingPadded">Description</td>            
            <td><br></td>
          </tr>
          <tr>
            <td>
              <input type="text" name="parameterName">
            </td>
            <td>
              <input type="text" name="parameterLabel">
            </td>
            <td><select name="parameterType"> 
            <option selected="selected" value="<%=QueryParameter.DATATYPE_STRING%>"><%=QueryParameter.DATATYPE_STRING%></option> 
            <option value="<%=QueryParameter.DATATYPE_DOUBLE%>"><%=QueryParameter.DATATYPE_DOUBLE%></option> 
            <option value="<%=QueryParameter.DATATYPE_INTEGER%>"><%=QueryParameter.DATATYPE_INTEGER%></option> 
            <option value="<%=QueryParameter.DATATYPE_DATE%>"><%=QueryParameter.DATATYPE_DATE%></option>                         
            <option value="<%=QueryParameter.DATATYPE_BOOLEAN%>"><%=QueryParameter.DATATYPE_BOOLEAN%></option> 
            <option value="<%=QueryParameter.DATATYPE_NUMERIC%>"><%=QueryParameter.DATATYPE_NUMERIC%></option>                                     
            <option value="<%=QueryParameter.DATATYPE_TEXT%>"><%=QueryParameter.DATATYPE_TEXT%></option>           
</select>
            </td>
            <td>
              <input type="text" name="parameterDescription">
            </td>            
            <td>
              <input type="button" name="Submit" value="Add" onclick="addQueryParameter()">
            </td>
          </tr>
        </table>
        <h2>Parameters</h2>
        <table>
<%
Collection queryParameters = query.getQueryParameters();
if (queryParameters != null &&
    !queryParameters.isEmpty())
    {
%>
          <tr class="headerRow">
            <td class="listHeadingPadded">Name</td>
            <td class="listHeadingPadded">Screen Label</td>
            <td class="listHeadingPadded">Type</td>
            <td class="listHeadingPadded">Order</td>            
            <td class="listHeadingPadded">Description</td>
            <td></td>            
            <td></td>                        
            <td>&nbsp;</td>
          </tr>
<%
    Collections.sort((List)queryParameters);
    QueryParameter queryParameter = null;
    int counter = 0;
    int maxSize = queryParameters.size();
    for (Iterator i = queryParameters.iterator();i.hasNext();)
    {
      counter++;
      queryParameter = (QueryParameter)i.next();
      LogUtil.debug(this.getClass(),"queryParameter="+queryParameter);      
      parameterName = queryParameter.getQueryParameterPK().getParameterName();
      parameterDescription = queryParameter.getParameterDescription();
      parameterLabel = queryParameter.getParameterLabel();
      parameterType = queryParameter.getParameterType();
      parameterOrder = queryParameter.getParameterOrder();
%>
          <tr class="<%=HTMLUtil.getRowType(counter)%>">
            <td><%=parameterName%></td>
            <td><%=parameterLabel%></td>
            <td><%=parameterType%></td>
            <td><%=parameterOrder%></td>
            <td><%=parameterDescription%></td>   
            <td>
<%
if (counter != maxSize)
{
%>            
              <input type="button" name="Submit" value="Down" onclick="moveParameterDown('<%=parameterName%>');">
<%
}
 %>                            
            </td>                     
            <td>
<%
if (counter != 1)
{
%>            
              <input type="button" name="Submit" value="Up" onclick="moveParameterUp('<%=parameterName%>');">
<%
}
%>
</td>
            <td>
              <input type="button" name="Submit" value="Remove" onclick="removeQueryParameter('<%=parameterName%>');">
            </td>
          </tr>
<%
      }
    }
%>
        </table>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
        <input type="button" value="Save" onclick="saveQuery()"/>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
