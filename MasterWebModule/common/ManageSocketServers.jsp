<%@include file="/security/VerifyAccess.jsp"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@ page import="java.lang.Exception"%>
<%@page import="java.util.*"%>


<html>
<head>
<title>Manage SocketServers</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
<form name="notificationMgrForm" method="post" action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>" >
<input type="hidden" name="event" value="">
<input type="hidden" name="socketServerKey" value="">
<tr valign="top">
   <td>
      <h1>Manage Socket Servers</h1>
      <table border="0" cellspacing="0" cellpadding="3">
<%
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
Collection keyList = commonMgr.getSocketServerKeyList();
if (keyList != null && !keyList.isEmpty()) {
%>
         <tr class="headerRow">
            <td class="dataValue" colspan="2">Name</td>
            <td class="dataValue" align="right">Port</td>
            <td class="dataValue">State</td>
            <td class="dataValue">Threads</td>
            <td></td>
            <td></td>
            <td></td>
         </tr>
<%
   String socketServerKey;
   int socketServerPort = 0;
   int socketServerThreadCount = 0;
   String socketServerState;
   Exception exception = null;
   PrintWriter pw = new PrintWriter(out);
   Iterator keyIterator = keyList.iterator();
   while (keyIterator.hasNext()) {
      socketServerKey = (String) keyIterator.next();
      socketServerPort = commonMgr.getSocketServerPort(socketServerKey);
      socketServerState = commonMgr.getSocketServerState(socketServerKey);
      socketServerThreadCount = commonMgr.getSocketServerThreadCount(socketServerKey);
      exception = commonMgr.getSocketServerException(socketServerKey);
%>
         <tr>
            <td colspan="2"><%=socketServerKey%></td>
            <td align="right"><%=socketServerPort%></td>
            <td><%=socketServerState%></td>
            <td><%=socketServerThreadCount%></td>
            <td><input type="submit" name="Submit" value="Start" onclick="document.all.event.value='manageSocketServers_btnStart';  document.all.socketServerKey.value='<%=socketServerKey%>';" ></td>
            <td><input type="submit" name="Submit" value="Stop" onclick="document.all.event.value='manageSocketServers_btnStop';  document.all.socketServerKey.value='<%=socketServerKey%>';" ></td>
            <td><input type="submit" name="Submit" value="Reset" onclick="document.all.event.value='manageSocketServers_btnReset';  document.all.socketServerKey.value='<%=socketServerKey%>';" ></td>
            <td width="50%"></td>
         </tr>
<%
      if (exception != null) {
%>
         <tr>
            <td width="5%"></td>
            <td colspan="7" width="95%"><pre><% exception.printStackTrace(pw); %></pre></td>
         </tr>
<%
      }
   }
}
else {
%>
         <tr><td>No socket servers defined.</td></tr>
<%
}
%>
      </table>
   </td>
</tr>
<tr valign="bottom">
  <td>
    <table cellpadding="5" cellspacing="0">
    <tr>
        <td><input type="submit" name="btnStartAll" value="Start All" onclick="document.all.event.value='manageSocketServers_btnStartAll';"></td>
        <td><input type="submit" name="btnStartAll" value="Stop All" onclick="document.all.event.value='manageSocketServers_btnStopAll';"></td>
        <td><input type="submit" name="btnStartAll" value="Reset All" onclick="document.all.event.value='manageSocketServers_btnResetAll';"></td>
    </tr>
    </table>
  </td>
</tr>
</form>
</table>
</body>
</html>
