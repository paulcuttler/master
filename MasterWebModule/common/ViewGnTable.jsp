<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.HTMLUtil"%>


<html>
<head>
<title>
ViewGnTable
</title>

<%=HTMLUtil.includeJS( CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js")%>
<%=HTMLUtil.includeJS( CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js")%>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
String tabName = (String)request.getAttribute("tabName");
String tabKey  = (String)request.getAttribute("tabKey");
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
SystemParameterVO tableVO = commonMgr.getSystemParameter(tabName,tabKey);
%>
<body>
<h1>
GN Table Entry
</h1>
<form name="mainForm" method="post" action = "/CommonAdminUIC">

<input type="hidden" name="event" value="GnTableEdit">
<input type="hidden" name="tabName" value="<%=tabName%>">
<input type="hidden" name="tabKey" value="<%=tabKey%>">
 <div id="mainpage" class="pg">
 <h1>General Table (GN) Entry</h1>
   <div id="contentpage" class="cnt">
   <hr>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
        <td width="14%" valign="top">Table Name: &nbsp </td>
        <td width="86%"> <%=tableVO.getSystemParameterPK().getCategory()%></td>
   </tr>
   <tr>
        <td width="14%" valign="top">Key: &nbsp </td>
        <td width="86%"> <%=tableVO.getSystemParameterPK().getParameter()%></td>
   </tr>
   <tr>
        <td width="14%" valign="top">Text Field: &nbsp </td>
        <td width="86%"> <%=HTMLUtil.showTags(tableVO.getValue())%></td>
   </tr>
   <tr>
        <td width="14%" valign="top">Logical Field: &nbsp </td>
        <td width="86%"> <%=tableVO.getBooleanValue()%></td>
   </tr>
   <tr>
        <td width="14%" valign="top">Date Field: &nbsp </td>
        <td width="86%"> <%=tableVO.getDateValue()%></td>
   </tr>
   <tr>
        <td width="14%" valign="top">Integer: &nbsp </td>
        <td width="86%"> <%=tableVO.getNumericValue1()%></td>
   </tr>
   <tr>
        <td width="14%" valign="top">Decimal: &nbsp </td>
        <td width="86%"> <%=tableVO.getNumericValue2()%></td>
   </tr>
   <tr>
     <td colspan="2">
   <hr>
    </td>
   </tr>
</table>
</div>
   <div id="buttonPage" class="btn">
             <%=HTMLUtil.buttonEdit("GnTableEdit","Entry")%>
   </div>
</div>
</form>
</body>
</html>
