<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.common.CommonConstants"%>
<%--
SelectionList.jsp

Displays a scrollable list of url's from an Array list
Together with two levels of heading text
Requires:
Session Attributes:

request Attributes:
   mainHeading
   generalText
   itemList  (ArrayList)
   directTo  - the name of the servlet receiving the request when the form is submitted
   context
Makes the following parameters available:
   event = "SelectionList_selection" / "SelectionList_previous"
   itemSelected
   context
   itemIndex = the index in the array of the selected item

--%>
<%
String mainHeading =  (String)request.getAttribute("mainHeading");
String generalText = (String)request.getAttribute("generalText");
ArrayList itemList = (ArrayList)request.getAttribute("itemList");
String directTo = (String)request.getAttribute("directTo");
String context  = (String)request.getAttribute("context");
int rows = 0;
%>
<script language=javascript>

function checkItem(item,index){
   document.all.itemSelected.value = item;
   document.all.event.value="<%=context%>_next";
   document.all.itemIndex.value=index;
   next();
}


function back(){
  document.all.event.value="<%=context%>_Back"  ;
  next();
}


function next(){
  document.getElementById("waitText").innerHTML="<h2>Please wait while the list is loaded</h2>";
  document.selectionlist.submit();
}

function enterHandler(event){
  key = event.keyCode;
  char="";
  if(key==13){
     document.all.event.value="<%=context%>_next";
     itemNumber = document.all.currentRow.value *1;
     document.all.itemIndex.value=itemNumber;
     numRows = document.all.numRows.value*1;
     itemSelected = "";
     if(numRows>1)
        itemSelected = rowID[itemNumber].innerHTML;
     else
        itemSelected = document.getElementById("rowID").innerHTML;
     document.all.itemSelected.value = itemSelected;
     next();
  }
  else
    return false;
}


function keystrokehandler(event){
  key = event.keyCode;
  char="";
  oldRow = document.all.currentRow.value*1;
  newRow=oldRow;
  numRows=document.all.numRows.value*1;
  if(key==38) newRow--;
  if(key==40)newRow++;
  if(key==36)newRow = 0;
  if(key==35)newRow = numRows-1;
  if(key==33)newRow -= 10;
  if(key==34)newRow += 10;
  if(key>=65 && key <=90)
  newRow = matchString(key);
  if(newRow>=numRows)newRow = numRows-1;
  if(newRow<0)newRow = 0;
  if(newRow != oldRow){
     status="Move to row " + newRow;
     for(x=0;x<numRows;x++){
        if(x==newRow){
           document.getElementById(""+newRow).innerHTML = "> "+x;
        }
        else {
          document.getElementById(""+x).innerHTML="&nbsp &nbsp "+x;
        }
     }
     document.all.currentRow.value = newRow;
     document.location.hash=newRow;
  }
  event.keycode=0;
  return false;
}


function matchString(keyCode){
   instr = String.fromCharCode(keyCode);
   x=0;

  found = false;
  while(x<rowID.length && !found){
      x++;
        textString=rowID[x].innerHTML.substring(0,1);
        if(textString>=instr) found=true;
   }
   document.all.currentRow.value=x;
   return x;
}

</script>
<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
<title>GenericSelectionList</title>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>
<body onkeyup="javascript:keystrokehandler(event)"
      onkeydown="javascript:enterHandler(event)"
      onLoad="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif');">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form name=selectionlist method="post" action="<%=directTo%>">
<tr valign="top">
   <td>
      <h1><%= mainHeading %></h1>
      <table border="0" cellpadding="3" cellspacing="0">
      <tr>
            <td> <%=generalText%><br/>
            <hr>
<%
if (itemList != null)
{
%>
            <div style="height:300; overflow: auto;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
<%
   rows = itemList.size();
   String thisItem = null;
   for(int x=0;x<rows;x++)
   {
      thisItem = (String) itemList.get(x);
%>
            <tr>
               <td width="40" class="dataValue">
                  <a name="<%=x%>"> <%=((x==0)?">&nbsp":"&nbsp&nbsp")%><%=x%></a>
               </td>
               <td><a  class="actionItem" href="javascript:checkItem('<%=thisItem%>','<%=x%>')"><span id="rowID"><%= thisItem%></span></a></td>
            </tr>
<% }  %>
            </table>
            </div>
<%}%>
            <hr>
            <p></p>
            <span id="waitText"><h2>&nbsp;</h2></span>
         </td>
      </tr>
      </table>
   </td>
</tr>
<tr>
      <td> <span class="helpTextSmall">Use Cursor keys (Up/Down,Page Up, Page
        Down) to scroll. Type a single letter to move.<br>
        Press Enter or Click on a row to select. </span>
      </td>
</tr>
<tr valign="bottom">
   <td>
      <table border="0" cellpadding="5" cellspacing="0">
      <tr>
         <td>
            <a onclick="javascript:history.back();"
               onMouseOut="MM_swapImgRestore();"
               onMouseOver="MM_displayStatusMsg('Return to the previous screen.');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
               <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Return to the previous screen.">
            </a>
         </td>
<%--
         <td><input type="button" name="Back" value="Back" onclick="javascript:back()"></td>
--%>
      </tr>
      </table>
   </td>
</tr>
<input type="hidden" name ="currentRow" value="0">
<input type="hidden" name="numRows" value = "<%=rows%>">
<input type="hidden" name="context" value ="<%=context%>">
<input type="hidden" name="itemSelected" value="">
<input type="hidden" name="event" value ="">
<input type="hidden" name="itemIndex" value="">
<input type="hidden" name="SourceContext" value="<%=request.getAttribute("SourceContext")%>">
<input type="hidden" name="returnTo" value="<%=request.getAttribute("returnTo")%>">
<input type="hidden" name="returnEvent" value="<%=request.getAttribute("returnEvent")%>">
<input type="hidden" name="refNumber" value="<%=request.getAttribute("refNumber")%>">
</form>
</table>
</body>
</html>
