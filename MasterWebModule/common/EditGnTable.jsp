<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>

<%
SystemParameterVO tableVO = null;

String tabName = (String)request.getAttribute("tabName");
String tabKey  = (String)request.getAttribute("tabKey");

if(tabName!=null)
{
   CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
   tableVO = commonMgr.getSystemParameter(tabName,tabKey);
}

tabName="";
tabKey="";
String description = "";
String dateStr = "";
String logicalStr = "";
String intStr = "";
String decStr = "";
if(tableVO != null)
{
   tabName = tableVO.getSystemParameterPK().getCategory();
   tabKey = tableVO.getSystemParameterPK().getParameter();
   description = tableVO.getValue();
   if(tableVO.getDateValue()!=null)
   {
      dateStr = tableVO.getDateValue().toString();
   }
   if(tableVO.getBooleanValue()!=null)
   {
      logicalStr = tableVO.getBooleanValue().toString();
   }
   if(tableVO.getNumericValue1()!=null)
   {
      intStr = tableVO.getNumericValue1().toString();
   }
   if(tableVO.getNumericValue2()!=null)
   {
      decStr = tableVO.getNumericValue2().toString();
   }
}

%>
<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
<%=HTMLUtil.includeJS( CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js")%>
<%=HTMLUtil.includeJS( CommonConstants.DIR_SCRIPTS+ "/InputControl.js")%>
<%=HTMLUtil.includeJS( CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js")%>
<title>EditGnTable</title>
</head>

<body>

<form name="mainForm" method="post" action="/CommonAdminUIC" target="leftFrame">

<input type="hidden" name="event" value="GnTableSave">

 <div id="mainpage" class="pg">
 <h1>Edit General Table Entry</h1>
 <hr>
   <div id="contentpage" class="cnt">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td width="12%" valign="top">Table Name </td>
        <td width="88%">
          <input type="text" name="tabName" value="<%=tabName%>">
       </td>
      </tr>
      <tr>
        <td width="12%" valign="top">Type </td>
        <td width="88%">
          <input type="text" name="tabKey" value="<%=tabKey%>">
        </td>
     </tr>
     <tr>
        <td width="12%" valign="top">Description </td>
        <td width="88%">
          <textArea name="description" cols="60" rows="5"><%=description%></textArea>
       </td>
     </tr>
     <tr>
     <td width="12%" valign="top">Logical </td>
     <td width="88%">
          <select name="logicalStr">
            <option <%=logicalStr.equals("")?"SELECTED":""%>>null</option>
            <option value="true" <%=logicalStr.equals("true")?"SELECTED":""%>>true</option>
            <option value="false" <%=logicalStr.equals("false")?"SELECTED":""%>>false</option>
          </select>
        </td>
   </tr>
   <tr>
     <td width="12%" valign="top">Date </td>
     <td width="88%">
        <input type="text" name="dateStr" value="<%=dateStr%>"
        onkeyup="javascript:checkDate(this)" >
     </td>
   </tr>
   <tr>
     <td width="12%" valign="top">Integer </td>
     <td width="88%">
          <input type="text" name="intStr" value="<%=intStr%>"
            onkeyup = "javascript:checkInteger(this)">
     </td>
   </tr>
   <tr>
     <td width="12%" valign="top">Decimal </td>
     <td width="88%">
        <input type="text" name="decStr" value="<%=decStr%>"
          onkeyup="javascript:checkFloat(this)">
     </td>
   </tr>
   <tr>
     <td colspan="2">
       <hr>
    </td>
   </tr>
  </table>
   </div>
   <div id="buttonPage" class="btn">
     <%=HTMLUtil.buttonSave("GnTableSave","Save Entry")%>
   </div>
</div>

</form>
</body>
</html>
