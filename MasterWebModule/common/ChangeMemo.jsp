<%@page import="com.ract.common.*"%>

<script language="Javascript">
//set stsubid of the main form with the value
function setClientForm() {
  self.opener.top.rightFrame.document.all.memo.value = document.all.memo.value;
  self.close();
}
</script>

<%
MemoVO mvo = (MemoVO)request.getAttribute("memo");
%>

<html>
<head>
<title>Change Memo</title>
<%=CommonConstants.getRACTStylesheet()%>

</head>
<body>

<h2>Change Memo</h2>

<table>
<tr>
<td>
<%
if (mvo != null) {
%>

<textarea type="textarea" name="memo" rows="10" cols="50">
<%=mvo.getMemoLine()%></textarea>

<%
}
else {
%>

<textarea type="textarea" name="memo" rows="10" cols="50">
</textarea>

<%}%>
</td>
</tr>

<tr>
  <td>
    <input type="button" value="Save" onClick="setClientForm();">
    <input type="button" value="Close" onClick="self.close();">
  </td>
</tr>

</table>

</body>

</html>