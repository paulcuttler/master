<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="javax.naming.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<link href="<s:url value="/css/ract.css"/>" rel="stylesheet"
			type="text/css" />	
			<s:head/>
	</head>
	<body>
	
	<h2>Edit Business Type</h2>
	
	<s:form>
	
	<s:hidden key="id"/>
	<s:hidden key="actionMethod" value="%{actionMethod}"/>
	 
	<s:textfield key="typeCode" readonly="%{readOnly}"></s:textfield>
	<s:textfield key="description" readonly="%{readOnly}"></s:textfield>	
	<s:include value="/common/auditInclude.jsp"></s:include>
	
	<s:submit action="%{actionMethod}" key="label.ok"/> 	
	</s:form>
	
	</body>
</html>