<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.HTMLUtil"%>

<html>
<head>
<title>
ReferenceDataView
</title>
</head>
<body>
<h1>
View Reference Data
</h1>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<%=CommonConstants.getRACTStylesheet()%>

<%
String refType = (String)request.getAttribute("refType");
String refCode = (String)request.getAttribute("refCode");
ReferenceDataVO refVO = null;
if(refCode!=null && !"".equals(refCode))
{
   CommonMgr mgr = CommonEJBHelper.getCommonMgr();
   refVO = mgr.getReferenceData(refType,refCode);
}
else
{
   refVO = new ReferenceDataVO();
}
%>
<form method="post" action = "/CommonAdminUIC">
<br><br>
<table>
   <tr>
     <td>Reference Type</td>
     <td><%=refVO.getReferenceDataPK().getReferenceType()%></td>
   </tr>
   <tr>
     <td>Reference Code </td>
     <td><%=refVO.getReferenceDataPK().getReferenceCode()%></td>
   </tr>
   <tr>
     <td>Sort Order</td>
     <td><%=refVO.getSortOrder()%></td>
   </tr>
   <tr>
     <td>Active</td>
     <td><%=refVO.isActive()%></td>
   </tr>
   <tr>
     <td>Description</td>
     <td><%=refVO.getDescription()%></td>
   </tr>
</table>
<a onclick="document.forms[0].submit()"
   onMouseOut="MM_swapImgRestore();"
   onMouseOver="MM_displayStatusMsg('Edit new entry.');MM_swapImage('EditButton','','<%=CommonConstants.DIR_IMAGES%>/EditButton_over.gif',1);return document.MM_returnValue" >
   <img name="EditButton" src="<%=CommonConstants.DIR_IMAGES%>/EditButton.gif" border="0" alt="Edit details.">
</a>
<input type="hidden" name="event" value="ReferenceDataEdit">
<input type="hidden" name="refType" value="<%=refType%>">
<input type="hidden" name="refCode" value="<%=refCode%>">
</form>
</body>
</html>
