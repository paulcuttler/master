<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.reporting.*"%>
<%@ page import="com.ract.user.*"%>

<%--
Include this file in a report parameter selection page to add the selection
of delivery methods and formats.

Wrap this included file inbetween some <table> tags. This way you can line up
these screen fields with the other report parameter fields.

T. Bakker, 16 May 2003
--%>
<tr>
   <td class="label">Deliver to:</td>
   <td colspan="2">
      <select name="<%=ReportRequestBase.PARAMETER_DELIVERY_METHOD%>" onchange="setDeliveryMethod(true); return true;">
         <option value="<%=ReportRequestBase.REPORT_DELIVERY_METHOD_SCREEN%>" selected>Screen</option>
         <option value="<%=ReportRequestBase.REPORT_DELIVERY_METHOD_EMAIL%>">Email</option>
      </select>
   </td>
</tr>
<tr id="dm_screen_row">
   <td></td>
</tr>
<%
String emailAddress = null;
UserSession userSession = UserSession.getUserSession(request.getSession());
if (userSession != null)
{
   emailAddress = userSession.getUser().getEmailAddress();
}
if (emailAddress == null)
{
   emailAddress = "";
}
%>
<tr>
   <td class="label" valign="top">Delivery format:</td>
   <td colspan="2">
      <select name="<%=ReportRequestBase.PARAMETER_DELIVERY_FORMAT%>">
         <option value="<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF%>" selected>PDF file</option>
         <option value="<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_CSV%>">CSV file</option>
         <option value="<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_HTML%>">Web page</option>
         <option value="<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_JGF%>">Report viewer</option>
         <option value="<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_XML%>">XML file</option>
      </select>
   </td>
</tr>
<tr id="dm_email_row" style="display: none;">
   <td class="label">Email address :</td>
   <td colspan="2"><input type="text" name="emailAddress" value="<%=emailAddress%>" size="50"></td>
</tr>
<script language="JavaScript">
   var currentOnLoadHandler = window.onload;

   function setDeliveryMethod(notLoading)
   {
//      setDestinations();
      var deliveryMethod = document.all.selectedDeliveryMethod.options[document.all.selectedDeliveryMethod.selectedIndex].value;

      if (deliveryMethod == '<%=ReportRequestBase.REPORT_DELIVERY_METHOD_SCREEN%>')
      {
         // Show the parameter fields for deliverying to the screen
         document.all.dm_screen_row.style.display = "";
         document.all.dm_email_row.style.display = "none";

         setDeliveryFormats([new Option('PDF file','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF%>'),
                             new Option('CSV file','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_CSV%>'),
                             new Option('XML file','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_XML%>'),
                             new Option('Web page','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_HTML%>'),
                             new Option('Report viewer','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_JGF%>')]);
      }
      else if (deliveryMethod == '<%=ReportRequestBase.REPORT_DELIVERY_METHOD_EMAIL%>')
      {
         // Show the parameter fields for deliverying to email
         document.all.dm_screen_row.style.display = "none";
         document.all.dm_email_row.style.display = "";

         setDeliveryFormats([new Option('PDF file','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF%>'),
                             new Option('CSV file','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_CSV%>'),
                             new Option('XML file','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_XML%>')]);
      }
      else
      {
         // Don't know what we are delivering to so hide all parameter fields
         document.all.dm_screen_row.style.display = "none";
         document.all.dm_email_row.style.display = "none";

         setDeliveryFormats([new Option('PDF file','<%=ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF%>')]);
      }

      if (!notLoading && currentOnLoadHandler)
      {
         currentOnLoadHandler;
      }
   }

   function setDeliveryFormats(optionArray)
   {
      // Remove any current options
      for (var i = document.all.selectedDeliveryFormat.length-1; i >= 0;  i--)
      {
         document.all.selectedDeliveryFormat.remove(i);
      }

      // Add the new options
      for (var i = 0; i < optionArray.length; i++)
      {
         document.all.selectedDeliveryFormat.add(optionArray[i]);
      }
      document.all.selectedDeliveryFormat.selectedIndex = 0;
   }

   window.onload = setDeliveryMethod;

   function setDestinations()
   {
      if (navigator.appVersion.lastIndexOf('MSIE') != -1
      && navigator.appVersion.lastIndexOf('5.5')!= -1)
      {
        //alert("This is ie 5.5");
         document.all.selectedDeliveryMethod.remove(0);
         document.all.selectedDeliveryMethod.value="<%=ReportRequestBase.REPORT_DELIVERY_METHOD_EMAIL%>";
      }
      else
      {
        //do nothing
      }
   }

</script>
