<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.reporting.*"%>
<%@ page import="com.ract.membership.reporting.*"%>
<%@ page import="java.util.*"%>
<%
ArrayList salesBranchList = (ArrayList)request.getAttribute("salesBranchList");
%>
<input type="hidden" name="<%=BranchSalesReportRequest.PARAMETER_SALES_BRANCH_LIST%>" value="">
<tr>
  <td class="label" valign="top">Sales branches:</td>
  <td>
    <select name="salesBranchSelect" size="10" multiple>
    <option value="*" selected>All</option>
    <%
    SalesBranchVO salesBranch;
    String selection;
    for(int x=0; x < salesBranchList.size(); x++)
    {
      salesBranch = (SalesBranchVO)salesBranchList.get(x);
      selection = "<option value=\"" + salesBranch.getSalesBranchCode() + "\">" + salesBranch.getSalesBranchName() + "</option>\n";
      %><%=selection%><%}%>
    </select>
  </td>
  <td class="helpTextSmall" valign="top" colspan="2">
    Click to select required sales branch.<br/>
    Ctrl-click to select multiple sales branches.<br/>
    Shift-click to select range.
  </td>
</tr>

<script language="Javascript">
function getSalesBranchList()
{
   var outString = "";
   var ALL_SALES_BRANCHES = "*";
   size = document.all.salesBranchSelect.length;
   for(x = 0; x < size; x++)
   {
     salesBranch = document.all.salesBranchSelect[x].value;
     //add to list if selected
     if (document.all.salesBranchSelect[x].selected &&
         salesBranch == ALL_SALES_BRANCHES)
     {
       outString = salesBranch;
     }
   }
   if (outString != ALL_SALES_BRANCHES)
   {
     for(x=0; x < size; x++)
     {
       if (document.all.salesBranchSelect[x].selected)
       {
         if(outString !="")
         {
           outString += ",";
         }
         //add to list if selected
         outString += "'" + document.all.salesBranchSelect[x].value+"'";
       }
     }
   }
   return outString;
}
</script>
