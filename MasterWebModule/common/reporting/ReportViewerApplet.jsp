<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>


<%
StringBuffer parameterListStr = new StringBuffer();
Hashtable parameterList = (Hashtable) request.getAttribute("parameterList");

// Convert the hash table of parameters into a URL style string of parameters
if (parameterList != null && !parameterList.isEmpty())
{
   Enumeration keyList = parameterList.keys();
   String key = (String) keyList.nextElement();
   parameterListStr.append(key);
   parameterListStr.append("=");
   parameterListStr.append((String)parameterList.get(key));

   while (keyList.hasMoreElements())
   {
      key = (String) keyList.nextElement();
      parameterListStr.append("&amp;");
      parameterListStr.append(key);
      parameterListStr.append("=");
      parameterListStr.append((String)parameterList.get(key));
   }
}

%>
<html>
<head>
<title>Report Viewer</title>
</head>
<body>
   <applet
     codebase = "."
     code     = "com.ract.common.reporting.ReportViewerApplet.class"
     archive  = "reportviewer.jar, applet.jar, ElixirReport.jar, rptclient.jar"
     name     = "reportViewer"
     width    = "300"
     height   = "100"
     hspace   = "0"
     vspace   = "0"
     align    = "top"
   >
      <param name = "parameterList" value = "<%=parameterListStr.toString()%>">
      <param name = "URL" value = "localhost:1099">
   </applet>
</body>
</html>
