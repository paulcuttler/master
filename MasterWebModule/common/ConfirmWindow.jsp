<%@ page import="com.ract.common.CommonConstants"%>

<%--
author: jyh

generic printing completed page that can be sent a heading and associated text
and a "ok" button
--%>
<html>

<head>
<title>Process Completed</title>
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">
function closeWindow() {
  self.close();
}
</script>

</head>

<body>
<h2><%=request.getAttribute("heading")%></h2>
<p>
<%=request.getAttribute("message")%>
</p>
<p>
<input type="button" onclick="closeWindow();" value="OK">
</p>

</body>

</html>
