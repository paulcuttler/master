<%@include file="/security/VerifyAccess.jsp"%>
<%@page import="com.ract.client.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.notifier.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.w3c.dom.*"%>


<html>
<head>
<title>NotificationMgr</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
<form name="notificationMgrForm" method="post" action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>" >
<input type="hidden" name="event" value="">
<tr valign="top">
   <td>
     <h1>Notification Failure List</h1>
<%
   ArrayList failureList = CommonEJBHelper.getNotificationMgr().getNotificationFailureList();
   if (failureList != null && !failureList.isEmpty()) {
%>
      <h2>Failed notification events - <%=failureList.size()%> failures</h2>
      <table border="0" cellpadding="3" cellspacing="0">
<%
      NotificationEventFailure failureEvent;
      NotificationEvent event;
      String eventFileName;
%>
         <tr>
            <td class="listHeadingPadded"></td>
            <td class="listHeadingPadded">Event</td>
            <td class="listHeadingPadded">Sent at</td>
            <td class="listHeadingPadded">Failed at</td>
            <td class="listHeadingPadded">File name</td>
         </tr>
<%
      int counter = 0;
      for (int i = 0; i < failureList.size(); i++) {
         failureEvent = (NotificationEventFailure) failureList.get(i);
         event = failureEvent.getNotificationEvent();
         eventFileName = failureEvent.getPersistenceFileName();
         counter++;
%>
         <tr>
            <td class="dataValue"><%=counter%></td> <!--link to view notification failure-->
            <td class="dataValue"><a href="<%=CommonUIConstants.PAGE_VIEWNOTIFICATIONFAILURE%>?eventFileName=<%=eventFileName%>"><%=event.toString()%></a></td> <!--link to view notification failure-->
            <td class="dataValue"><%=DateUtil.formatDate(event.getEventTime(),"dd/MM/yyyy HH:mm:ss:SS")%></td>
            <td class="dataValue"><%=DateUtil.formatDate(failureEvent.getFailureTime(),"dd/MM/yyyy HH:mm:ss:SS")%></td>
            <td class="dataValue"><%=eventFileName%></td>
         </tr>

<%
      }
%>
      </table>
<%
   }
   else {
%>
No failed notification events!
<%
   }
%>
   </td>
</tr>
<tr valign="bottom">
  <td>
    <table cellpadding="5" cellspacing="0">
    <tr>
        <td><input type="submit" name="btnResendAll" value="Resend all failures" onclick="document.all.event.value='notificationMgr_btnResendAll';"></td>
    </tr>
    </table>
  </td>
</tr>
</form>
</table>

</body>
</html>
