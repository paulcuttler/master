<%@page import="com.ract.common.*"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.HTMLUtil"%>


<html>
<head>
<title>
View Server Configuration
</title>

<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<h2>System Parameter List</h2>

<p class="helpText">
Note: System parameters are added when the system requests them for the first time.
</p>

<table>
<tr class="headerRow">
  <td class="listPaddedHeading">Category</td><td class="listPaddedHeading">System Parameter</td><td class="listPaddedHeading">Value</td>
</tr>
<%
Collection paramList = SystemParameterCache.getInstance().getItemList();
Collections.sort((ArrayList)paramList);
if (paramList != null)
{
  SystemParameterVO sysParam = null;
  Iterator it = paramList.iterator();
  int counter = 0;
  while (it.hasNext())
  {
    counter++;
    sysParam = (SystemParameterVO)it.next();
%>
<tr class="<%=HTMLUtil.getRowType(counter)%>">
  <td class="label"><%=sysParam.getSystemParameterPK().getCategory()%></td><td class="dataValue"><%=sysParam.getSystemParameterPK().getParameter()%></td><td class="dataValue"><%=sysParam.getValue()%></td>
</tr>
<%
  }
}
%>

</table>
</body>
</html>
