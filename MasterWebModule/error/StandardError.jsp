<%@ taglib prefix="s" uri="/struts-tags" %>

<%@page import="java.lang.Exception"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="com.ract.common.*"%>

<%
Exception exception = (Exception) request.getAttribute("exception");
Exception rootCauseException = (Exception) request.getAttribute("rootCauseException");

String errorMessage = null;

if (rootCauseException != null)
{
   errorMessage = rootCauseException.getMessage();
}
else
{
  if (exception != null)
  {
    errorMessage = exception.getMessage();
  }
}

if (errorMessage == null || errorMessage.length() < 1)
{
  if (exception != null)
  {
    errorMessage = "A '" + exception.getClass().getName() + "' type of warning was raised. Show the details for more information.";
  }
}
%>

<html>
<head>
<title>Error!</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="/scripts/ExceptionDetail.js"/>
</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr valign="top">
   <td>
   <%
   if (exception != null)
   {
    %>
      <h1>Error!</h1>
      <br/>
      <b><%=errorMessage%></b>
      <p>
      <input type="button" name="btnDetails" onclick="toggleDetails();" value="Show detail">
      <div id="errorDetail" style="display:none">
      <p>Details:</p>
      <pre>
      <%=ExceptionHelper.getExceptionStackTrace(exception)%>
      </pre>
      </div>
   <%
   }
   else
   {
   %>
    <s:actionerror/>   
   <%
   }
   %>
   </td>
</tr>
<tr valign="bottom">
   <td>
      <table border="0" cellpadding="5" cellspacing="0">
      <tr>
         <td><input type="button" name="btnBack" value="Back" onclick="history.back()"></td>
      </tr>
      </table>
   </td>
</tr>
</table>
</body>
</html>
