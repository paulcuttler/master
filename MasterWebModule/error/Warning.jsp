<%@ page import="java.io.PrintWriter"%>
<%@ page import="com.ract.common.CommonConstants"%>
<%@page import="com.ract.common.ExceptionHelper"%>

<%
Exception exception = (Exception) request.getAttribute("exception");
Exception rootCauseException = (Exception) request.getAttribute("rootCauseException");

String errorMessage = null;

if (rootCauseException != null)
   errorMessage = rootCauseException.getMessage();
else
   errorMessage = exception.getMessage();

if (errorMessage == null || errorMessage.length() < 1)
   errorMessage = "A '" + exception.getClass().getName() + "' type of warning was raised. Show the details for more information.";
%>
<html>
<head>
<title>Warning</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=request.getContextPath()%>/scripts/ExceptionDetail.js">
</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr valign="top">
   <td>
      <h1>Warning</h1>
      <br>
      <%=errorMessage%>
      <p>
      <input type="button" name="btnDetails" onclick="toggleDetails();" value="Show detail">
      <div id="errorDetail" style="display:none">
      <p>Details:</p>
      <pre>
      <%
         ExceptionHelper.getExceptionStackTrace(exception);
      %>
      </pre>
      </div>
   </td>
</tr>
<tr valign="bottom">
   <td>
      <table border="0" cellpadding="5" cellspacing="0">
      <tr>
         <td><input type="button" name="btnBack" value="Back" onclick="history.back()"></td>
      </tr>
      </table>
   </td>
</tr>
</table>
</body>
</html>
