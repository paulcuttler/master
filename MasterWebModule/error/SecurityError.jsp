<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.security.ui.*"%>

<%
Exception e = (Exception) request.getAttribute("exception");
String errorMessage = e.getMessage();
if (errorMessage == null || errorMessage.length() < 1)
   errorMessage = "A '" + e.getClass().getName() + "' type of security error was raised.";
%>
<html>
<head>
<title>Security error</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr valign="top">
<td>
   <h1>Security error</h1>
   <p><%=errorMessage%></p>

   <p><a target="_top" href="<%=LoginUIConstants.PAGE_Login%>">Login</a></p>
</td>
</tr>
</table>
</body>
</html>
