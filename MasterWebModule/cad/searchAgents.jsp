<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.ract.insurance.*"%>
<%@ page import="com.ract.common.hibernate.*"%>
<%@ page import="com.ract.common.cad.*"%>
<%@ page import="com.ract.membership.ui.*"%>



<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
<script language="Javascript">
function clearForm()
{
  document.all.location.options[0].selected = true;
  document.all.agentId.value = "";
  document.all.agentName.value = "";
  document.all.roadside.checked = false;
  document.all.towing.checked = false;
  document.all.battery.checked = false;
}
</script>
</head>
<%
Collection agentList = (Collection)request.getAttribute("agentList");
String agentId = (String)request.getAttribute("agentId");
String agentName = (String)request.getAttribute("agentName");
String location = (String)request.getAttribute("location");
Boolean roadside = (Boolean)request.getAttribute("roadside");
Boolean towing = (Boolean)request.getAttribute("towing");
Boolean battery = (Boolean)request.getAttribute("battery");
%>
<body>
<h2>Agent Search</h2>
<form method="post" action="/MembershipUIC">
  <input type="hidden" name="event" value="searchAgents"/>
  <table width="25%">
    <tr>
      <td>Location:</td>
      <td> <%=MembershipUIHelper.getAgentSelectFromArrayList(location)%> </td>
    </tr>
    <tr>
      <td>Agent Id:</td>
      <td>
        <input type="text" name="agentId" value="<%=agentId!=null?agentId:""%>"/>
      </td>
    </tr>
    <tr>
      <td>Agent Name:</td>
      <td>
        <input type="text" name="agentName" value="<%=agentName!=null?agentName:""%>"/>
      </td>
    </tr>
    <tr>
      <td>Roadside?</td>
      <td>
        <input type="checkbox" name="roadside" value="Y" <%=roadside!=null&&roadside.booleanValue()?"checked":""%>/>
      </td>
    </tr>
    <tr>
      <td>Towing?</td>
      <td>
        <input type="checkbox" name="towing" value="Y" <%=towing!=null&&towing.booleanValue()?"checked":""%>/>
      </td>
    </tr>
    <tr>
      <td>Battery?</td>
      <td>
        <input type="checkbox" name="battery" value="Y" <%=battery!=null&&battery.booleanValue()?"checked":""%>/>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <input type="submit" name="Submit" value="Search">
        <input type="button" value="Clear" onclick="clearForm()">
      </td>
    </tr>
  </table>
</form>
<%

  if (agentList != null &&
  !agentList.isEmpty())
  {
    int agentListSize = agentList.size();
%>
<h3>Search Results:</h3>
<table width="100%">
  <tr>
    <td colspan="7" class="helpText">Displaying <%=agentListSize%> results.</td>
  </tr>
  <tr>
    <td colspan="7"><hr/></td>
  </tr>
  <%

  Iterator agentIt = agentList.iterator();
  Agent agent = null;
  int rowNumber = 0;
  while (agentIt.hasNext())
  {
    agent = (Agent)agentIt.next();

    %>
  <tr valign="top" class="<%=HTMLUtil.getRowType(rowNumber)%>">
    <td>
      <table width="60%">
        <tr>
          <td width="21%">Location:</td>
          <td width="29%" class="dataValue"><%=agent.getAgentPK().getLocation()%></td>
          <td width="19%">Agent Id:</td>
          <td colspan="2" class="dataValue"><%=agent.getAgentPK().getAgentId()%></td>
        </tr>
        <tr>
          <td width="21%">Priority:</td>
          <td colspan="4" class="dataValue"><%=agent.getPriority()%></td>
        </tr>
        <tr>
          <td width="21%">Agent Name:</td>
          <td class="dataValue" width="29%"><%=agent.getAgentName().toUpperCase()%></td>
          <td width="19%">
          <td width="19%">
          <td width="19%">
        </tr>
        <%
        if (agent.getBusinessPhone() != null &&
           !agent.getBusinessPhone().trim().equals(""))
        {
        %>
        <tr>
          <td width="19%">
          <td class="<%=agent.CONTACT_METHOD_BUSINESS.equalsIgnoreCase(agent.getPreferredContactMethod())?"highlightRow":""%>">Work (BH): <%=agent.getBusinessPhone()%><br/>
          </td>
          <td width="19%">
          <td width="19%">
          <td width="19%">
        </tr>
        <%
        }
        if (agent.getMobilePhone() != null &&
           !agent.getMobilePhone().trim().equals(""))
        {
            %>
        <tr>
          <td width="19%">
          <td class="<%=agent.CONTACT_METHOD_MOBILE.equalsIgnoreCase(agent.getPreferredContactMethod())?"highlightRow":""%>"><%=agent.getMobilePhone()!=null?"Mobile: "+agent.getMobilePhone()+"<br/>":""%></td>
          <td width="19%">
          <td width="19%">
          <td width="19%">
        </tr>
        <%
        }
        if (agent.getHomePhone() != null &&
           !agent.getHomePhone().trim().equals(""))
        {
                %>
        <tr>
          <td width="19%">
          <td class="<%=agent.CONTACT_METHOD_HOME.equalsIgnoreCase(agent.getPreferredContactMethod())?"highlightRow":""%>"><%=agent.getHomePhone()!=null?"Home (AH): "+agent.getHomePhone()+"<br/>":""%></td>
          <td width="19%">
          <td width="19%">
          <td width="19%">
        </tr>
        <%
        }
        if (agent.getContact1Name() != null &&
           !agent.getContact1Name().trim().equals(""))
        {
        %>
        <tr>
          <td width="21%">Contact 1 Name:</td>
          <td width="29%" class="dataValue"><%=agent.getContact1Name()%></td>
          <td width="19%">Contact 1 Phone:</td>
          <td colspan="2" class="dataValue"><%=agent.getContact1Phone()%></td>
        </tr>
        <%
        }
        if (agent.getContact2Name() != null &&
           !agent.getContact2Name().trim().equals(""))
        {
        %>
        <tr>
          <td width="21%">Contact 2 Name:</td>
          <td width="29%" class="dataValue"><%=agent.getContact2Name()%></td>
          <td width="19%">Contact 2 Phone:</td>
          <td colspan="2" class="dataValue"><%=agent.getContact2Phone()%></td>
        </tr>
        <%
        }
        %>
        <tr>
          <td width="21%">Towing Type:</td>
          <td class="dataValue"><%=agent.getTowingType()!=null?agent.getTowingType():""%></td>
          <td>Roadside?<%=agent.isRoadside()?"<img src=\""+CommonConstants.DIR_IMAGES+"/check.gif\">":"<img src=\""+CommonConstants.DIR_IMAGES+"/cross.gif\">"%></td>
          <td>Towing?<%=agent.isTowing()?"<img src=\""+CommonConstants.DIR_IMAGES+"/check.gif\">":"<img src=\""+CommonConstants.DIR_IMAGES+"/cross.gif\">"%></td>
          <td>Battery?<%=agent.isBattery()?"<img src=\""+CommonConstants.DIR_IMAGES+"/check.gif\">":"<img src=\""+CommonConstants.DIR_IMAGES+"/cross.gif\">"%></td>
        <tr>
          <td width="21%">Notes:</td>
          <td colspan="4" class="dataValue"><%=agent.getNotes()!=null?agent.getNotes():""%></td>
        </tr>
      </table>
    </td>
  </tr>
  <%
	rowNumber++;
  }
  %>
</table>
<%
  }
  else
  {
  %>
<p><span class="helpText">No results found for search.</span> </p>
<%
  }
%>
<p>&nbsp;</p>
</body>
</html>
