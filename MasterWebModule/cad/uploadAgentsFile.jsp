<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
<s:head theme="simple"/>
</head>

<body>
<h2>Upload Agent File</h2>

<s:form method="POST" enctype="multipart/form-data" action="uploadAgentsFile.action">

  <p> <span class="helpText">This screen will allow you to upload a csv delimited
    file containing agent data. To upload a file select the agent file and then
    click on the upload button.</span> </p>

<table>
<tr>
  <td>File to upload:</td><td><s:file name="upfile"/><td>
</tr>
<tr>
      <td colspan="2"><span class="helpTextSmall">Click browse and select the
        csv delimited file containing agent data.</span></td>
</tr>
<tr>
  <td colspan="2"><s:submit value="upload"/>
  </td>
</tr>
</table>

</s:form>

<s:if test="upfile != null">
<table>
<tr><td class="headerRow" colspan="2">Upload Results:</td></tr>
<tr><td class="label">Records loaded</td><td class="dataValue"><s:property value="recordCount"/></td></tr>
<tr><td class="label">File</td><td class="dataValue"><s:property value="upfileFileName"/></td></tr>
<tr><td class="label">File Size</td><td class="dataValue"><s:property value="fileSize"/> kb</td></tr>
</table>
</s:if>

</body>
</html>
