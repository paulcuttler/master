<%@ page import="com.ract.membership.*"%>

<%
MembershipGroupVO membershipDetailsVO = (MembershipGroupVO)request.getAttribute("membershipGroupVO");
if (membershipDetailsVO != null)
{
  GroupVO groupVO = membershipDetailsVO.getGroup();
  if (groupVO != null)
  {
log("groupVO="+groupVO);
    Integer groupId = groupVO.getGroupId();
    String groupName = groupVO.getGroupName();
    String groupTypeCode = groupVO.getGroupTypeCode();
    String billingAddress = groupVO.getBillingAddress().getSingleLineAddress();
    String contactPerson = groupVO.getContactPerson();
%>

<table>

  <tr>
    <td>Group Name</td>
    <td class="dataValue"><a title="<%=groupId%>"><%=groupName%></a></td>
  </tr>
  <tr>
    <td>Group Type</td>
    <td class="dataValue"><%=groupTypeCode%></td>
  </tr>
  <tr>
    <td>Billing Address</td>
    <td class="dataValue"><%=billingAddress%></td>
  </tr>
  <tr>
    <td>Contact Person</td>
    <td class="dataValue"><%=contactPerson%></td>
  </tr>
</table>

<%
  }
  else
  {
%>
  <table>
    <tr>
      <td colspan="2" class="helpText">No group header details exist.</td>
    </tr>
  </table>
<%
  }
}
%>
