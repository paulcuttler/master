<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<%
// See if the membership ID was specified
Integer membershipID = (Integer) request.getAttribute("membershipID");
if (membershipID == null) {
   throw new ServletException("A membership ID must be specified with the request to the ViewGroupMembers page.");
}

int memberCount = 0;
String groupID = "";
// This reference is required by the included jsp "ViewGroupMembersInclude"
MembershipGroupVO memGroupVO = (MembershipGroupVO)request.getAttribute("membershipGroupVO");
if (memGroupVO != null)
{
   memberCount = memGroupVO.getMemberCount();
   groupID = memGroupVO.getMembershipGroupID().toString();
}
%>
<html>
<head>
<title>View membership</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<%=CommonConstants.getSortableStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
   dojo.require("dojo.widget.*");
</script>
</head>
<body>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <form name="groupMembersForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="viewGroupMembers_btnMembership">
  <input type="hidden" name="membershipID" value="<%=membershipID%>">
  <input type="hidden" name="groupId" value="<%=memGroupVO.getMembershipGroupID()%>"/>
    <tr valign="top">
      <td>
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> View membership group</h1>

       <%@ include file="/membership/ViewGroupDetailsInclude.jsp"%>

        <a title="Membership group ID = <%=groupID%>"><h3><%=memberCount + (memberCount == 1 ? " group member" : " group members")%></h3></a>
<%
if (memberCount > 0)
{
%>
        <div style="height:280; overflow:auto; border-width: 1; border-style: solid;">
       <%@ include file="/membership/ViewGroupMembersInclude.jsp"%>
        </div>
<%
}
%>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
         <!-- View data links -->
         <table border="0" cellpadding="5" cellspacing="0">
         <tr>
           <td><input type="submit" name="btnMembership" value="View Membership" onclick="document.all.event.value='viewGroupMembers_btnMembership';"></td>
           <td><input type="submit" name="btnEditGroupDetails" value="Edit Group Details" onclick="document.all.event.value='viewGroupMembers_btnEditGroupDetails';"></td>
         </tr>
         </table>
      </td>
    </tr>
    </form>
  </table>
</body>
</html>
