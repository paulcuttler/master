<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.CommonConstants"%>

<%
log("in servlet");
%>

<%@ include file="/membership/TransactionInclude.jsp"%>

<%
String discountAmountFld = null;
String discountReasonFld = null;

// Variables to obtain the membership transaction value objects
Vector memberTxList = transGroup.getTransactionList();
log("memberTxList="+memberTxList);

Iterator memberTxListIterator = null;
MembershipTransactionVO membershipTxVO = null;
MembershipVO membershipVO = null;
//String addressee = null;
double feeTypeTotal = 0.0;
int memberCount = memberTxList.size();
String membersText = (memberCount == 1 ? "member" : "members");
%>


<html>
<head>
<title>Discretionary Discount</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Validation.js">
</script>
<%=CommonConstants.getSortableStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
   dojo.require("dojo.widget.*");
</script>
</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">

<form method="post"  onsubmit="return validate();"  action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">

<input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
<input type="hidden" name="event" value="discretionaryDiscount_btnNext">

<tr valign="top">
<td>
  <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Discretionary Discounts</h1>
<%

Collection disctinctFeeTypeList = transGroup.getDistinctTransactionFeeTypeList();
// The maximum amount that can be discretionarily discounted.
double maxDiscretionaryDiscountAmount = transGroup.getMaxDiscretionaryDiscountAmount();

log("maxDiscretionaryDiscountAmount="+maxDiscretionaryDiscountAmount);
log("disctinctFeeTypeList="+disctinctFeeTypeList);

//double totalFee = 0.0;
if (disctinctFeeTypeList != null && maxDiscretionaryDiscountAmount > 0.0)
{
%>
  <table border="0" cellpadding="3" cellspacing="0" >
<%
   String feeTypeCode;
   FeeTypeVO feeTypeVO;
   Iterator feeIterator = disctinctFeeTypeList.iterator();
   while (feeIterator.hasNext())
   {
      feeTypeCode = (String) feeIterator.next();
      feeTypeVO = refMgr.getFeeType(feeTypeCode);

      // Get total amount that can be discretionarily discounted for the fee type.
      feeTypeTotal = transGroup.getMaxDiscretionaryDiscountAmount(feeTypeCode);
log("feeTypeTotal="+feeTypeTotal);
      // check that the fee has non-discretionary discounts
      // And that there is a fee that can be discounted
      if (feeTypeTotal > 0.0)
      {
%>
    <tr>
       <td class="label" colspan="2"><%=feeTypeCode%>:</td>
       <td class="datavalue" align="right"><%=CurrencyUtil.formatDollarValue(feeTypeTotal)%>&nbsp;</td>
       <td class="helpTextSmall">including GST</td>
    </tr>
<%
      }
   }
%>

  <tr>
     <td class="dataValue" colspan="2" nowrap>Amount discountable:</td>
     <td class="dataValue" align="right"><%=CurrencyUtil.formatDollarValue(maxDiscretionaryDiscountAmount)%>&nbsp;</td>
     <td class="helpTextSmall" nowrap>including GST</td>
  <tr>
  <tr>
     <td width="10">&nbsp;</td>
     <td class="label" nowrap>Discount Amount:</td>
     <td><input type="text" name="discountAmountText" size=10 align="right" value="<%=(transGroup.getDiscretionaryDiscountAmount() > 0.0 ? NumberUtil.formatValue(transGroup.getDiscretionaryDiscountAmount(),"#####0.00") : "")%>"></td>
     <td width="80%"></td>
   </tr>
  <tr>
     <td width="10" class="label">&nbsp;</td>
     <td class="label" nowrap>Discount Reason:</td>
     <td colspan="2"><input type="text" name="discountReason" maxlength="50" size=40 value="<%=(transGroup.getDiscretionaryDiscountReason() != null ? transGroup.getDiscretionaryDiscountReason() : "")%>"></td>
  </tr>
</table>

<%
   // Only show the list of member names if there is more than one.
   if (memberTxList.size() > 1)
   {
%>
<h3>Member details: <%=memberCount%> <%=membersText%> (only members with fees displayed)</h3>
<div id="feeDetails" style="height:250; overflow:auto; border-width: 1; border-style: solid;">
  <table  dojoType="filteringTable"
            id="feeTable"
            cellpadding="2"
            alternateRows="true"
            class="dataTable">
  <thead>
   <tr>
      <th field="clientNumber" dataType="Number" align="left" valign="top">Client</th><th field="desc" dataType="String" align="left" valign="top">Name</th><th field="trans" dataType="String" align="left" valign="top">Fee</th>
    </tr>
  </thead>
  <tbody>
<%
       memberTxListIterator = memberTxList.iterator();
       int row = 0;
       while (memberTxListIterator.hasNext())
       {
         row++;
          membershipTxVO = (MembershipTransactionVO) memberTxListIterator.next();
          if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(membershipTxVO.getTransactionTypeCode()))
          {
             membershipVO = membershipTxVO.getNewMembership();
             double memberFeeTypeTotal = membershipTxVO.getNetTransactionFee();
             if (memberFeeTypeTotal > 0)
             {
%>
      <tr value="<%=row%>">
         <td class="label" nowrap><%=membershipVO.getClient().getClientNumber()%></td>
         <td class="dataValue" nowrap><%=membershipVO.getClient().getSortableDisplayName()%></td>
         <td class="label" align="right"><%=CurrencyUtil.formatDollarValue(memberFeeTypeTotal)%>&nbsp;</td>
         <td width="80%"></td>
      </tr>
<%
             }
         }
      }
%>
    <tbody>
  </table>
</div>
<%
   }
}
%>
</td>
</tr>
<tr valign="bottom">
  <td>
    <table cellpadding="5" cellspacing="0" >
      <tr>
         <td>
            <input type="button" value="Back" onclick="history.back(); return true;"/>
         </td>
         <td>
            <input type="button" value="Next" onclick="document.forms[0].submit();"/>
         </td>
      </tr>
    </table>
  </td>
</tr>
</form>
</table>
</body>
<script language="javascript">
function validate() {
   alert("Validating:"
        + "\namount =" + document.all.discountAmountText.value
        + "\nreason = " +document.all.discountReason.value);
   if (!isEmpty(document.all.discountAmount.value)) {
      if (!isCurrency(document.all.discountAmountText.value)) {
         setFocus(document.all.discountAmountText);
         alert("The discount amount is not a valid currency amount.");
         return false;
      }
      var discountAmount = parseFloat(document.all.discountAmountText.value);
      if (discountAmount > <%=maxDiscretionaryDiscountAmount%>)
      {
         setFocus(document.all.discountAmountText);
         alert("The discount amount cannot be greater than the " + <%=maxDiscretionaryDiscountAmount%> + ".");
         return false;
      }
      if (isEmpty(document.all.discountReason.value)) {
         setFocus(document.all.discountReason);
         alert("You must enter a discount reason.");
         return false;
      }
   }
   return true;
}
</script>
</html>
