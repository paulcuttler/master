<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.*"%>

<%
UserSession userSession = UserSession.getUserSession(request.getSession());
if (userSession == null
|| !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_EDIT_TRANSACTION)) {
   throw new com.ract.common.SecurityException("You do not have the required privilege to use this page.");
}

Integer transactionID = (Integer) request.getAttribute("transactionID");
MembershipTransactionVO transVO = MembershipEJBHelper.getMembershipMgr().getMembershipTransaction(transactionID);
if (transVO == null) {
   throw new SystemException("Failed to get transactionID " + transactionID);
}
%>

<html>
<head>
<title>Edit Transaction</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form name="editTransactionForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
<input type="hidden" name="event" value="editTransaction_btnSave">
<input type="hidden" name="transactionID" value="<%=transVO.getTransactionID()%>">
<tr>
  <td valign="top">
    <h1>Edit Transaction</h1>
    <table border="0" cellpadding="3">
    <tr>
      <td class="label">Trans ID:</td>
      <td class="dataValue"><%=transVO.getTransactionID()%></td>
    </tr>
    <tr>
      <td class="label">Trans group type code:</td>
      <td class="dataValue"><input type="text" name="transGroupTypeCode" value="<%=transVO.getTransactionGroupTypeCode()%>" size="20" ></td>
    </tr>
    <tr>
      <td class="label">Trans type code:</td>
      <td class="dataValue"><input type="text" name="transTypeCode" value="<%=transVO.getTransactionTypeCode()%>" size="20"></td>
    </tr>
    <tr>
      <td class="label">Trans date:</td>
      <td class="dataValue"><%=transVO.getTransactionDate().formatLongDate()%></td>
    </tr>
    <tr>
      <td class="label">Product code:</td>
      <td class="dataValue"><input type="text" name="productCode" value="<%=transVO.getProductCode()%>" size="20"></td>
    </tr>
    <tr>
      <td class="label">Group count:</td>
      <td class="dataValue"><input type="text" name="groupCount" value="<%=transVO.getGroupCount()%>" size="20"></td>
    </tr>
    <tr>
      <td class="label">Effective date:</td>
      <td class="dataValue"><input type="text" name="effectiveDate" value="<%=transVO.getEffectiveDate()%>" size="20"></td>
    </tr>
    <tr>
      <td class="label">End date:</td>
      <td class="dataValue"><input type="text" name="endDate" value="<%=transVO.getEndDate()%>" size="20"></td>
    </tr>
    <tr>
      <td class="label">Payable Item ID</td>
      <td class="dataValue"><input type="text" name="payableItemID" value="<%=transVO.getPayableItemID()==null?"":transVO.getPayableItemID().toString()%>" size="20"></td>
    </tr>
    <tr>
      <td class="label">Membership XML</td>
      <td class="dataValue"><textarea name="membershipXML" cols="50" rows="10"><%=transVO.getMembershipXML()%></textarea></td>
    </tr>
    </table>
  </td>
</tr>
<tr>
  <td valign="bottom">
    <table border="0" cellpadding="5" cellspacing="0">
    <tr>
      <td><input type="submit" name="btnSave" value="Save"></td>
    </tr>
    </table>
  </td>
</tr>
</form>
</table>
</body>
</html>
