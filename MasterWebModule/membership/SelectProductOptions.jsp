<%@ page import="java.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.client.ui.ClientUIConstants"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.util.*"%>
<%@ include file="/membership/TransactionInclude.jsp"%>

<%
ProductVO productVO 		= transGroup.getSelectedProduct();
Collection optionalBenefitList 	= productVO.getProductBenefitList(true,true);
Collection memTransList 	= transGroup.getTransactionList();
int memTransListSize 		= (memTransList != null ? memTransList.size() : 0);
UserSession userSession 	= UserSession.getUserSession(request.getSession());
%>
<html>
<head>
<title>Select Product Options</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>
<body onLoad="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif');">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
  <form name="productOptionsForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
    <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
    <input type="hidden" name="event" value="selectProductOptions_btnNext">
    <tr valign="top">
      <td>
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Select Product Options</h1>
        <table border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td class="label">Product:</td>
            <td class="dataValue"><%=productVO.getProductCode()%></td>
          </tr>
        </table>
        <h3>Product options per member: <%=memTransListSize%> <%=(memTransListSize == 1 ? "member" : "members")%></h3>
        <div style="height:292; overflow:auto; border-width: 1; border-style: solid;">
        <table cellpadding="5" cellspacing="0">
        <tr>
          <td>
<%
if (memTransList != null && optionalBenefitList != null)
{
   int memTransCount 				= memTransList.size();
   String privilegeID;
   MembershipTransactionVO memTransVO 		= null;
   MembershipVO membershipVO 			= null;
   MembershipVO oldMembershipVO 		= null;
   String addressee 				= null;
   ClientVO clientVO 				= null;
   AddressVO addrVO 				= null;
   String postalAddress 			= null;
   ProductBenefitTypeVO prodBenefitTypeVO 	= null;
   String cleanBenefitCode 			= null;
   Iterator optionalBenefitListIT 		= null;
   boolean checked 				= false;
   boolean disabled 				= false;
   boolean selectByDefaultTransaction 		= false;
   String thisTrans 				= null;

   Iterator memTransListIT = memTransList.iterator();
   while (memTransListIT.hasNext())
   {
      memTransVO = (MembershipTransactionVO) memTransListIT.next();

      // Can't change product options for members being removed from a group.
      if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransVO.getTransactionTypeCode()))
      {
         addressee = (memTransCount > 1 && memTransVO.isPrimeAddressee() ? " <span class=\"helpTextSmall\"> (addressee)</span>" : "");
         oldMembershipVO = memTransVO.getMembership();
         membershipVO = memTransVO.getNewMembership();
         clientVO = membershipVO.getClient();
         addrVO = (AddressVO) clientVO.getPostalAddress();
         if (addrVO != null)
         {
            postalAddress = addrVO.getSingleLineAddress();
         }
         else
         {
            postalAddress = "";
         }
%>
        <span class="dataValue"><%=clientVO.getClientNumber()%> <%=clientVO.getDisplayName()%>. <%=postalAddress%> <%=addressee%></span>
        <table border="0" cellspacing="0" cellpadding="3" style="margin-left:15px;">
<%
         optionalBenefitListIT = optionalBenefitList.iterator();
         while (optionalBenefitListIT.hasNext())
         {
            prodBenefitTypeVO = (ProductBenefitTypeVO) optionalBenefitListIT.next();

            // Disable product options that the user does not have the privilege
            // to administer.  Must do this by hiding the table row rather than
            // disabling the checkbox.  If the checkbox is disabled then
            // it does not have a value even if it is checked and so the membership
            // looses the product option.
            privilegeID = prodBenefitTypeVO.getPrivilegeID();
            if (privilegeID == null ||  userSession.getUser().isPrivilegedUser(privilegeID))
            {
               disabled = false;
            }
            else
            {
               disabled = true;
            }

            // See if the new membership has the product option selected
            checked = membershipVO.hasProductOption(prodBenefitTypeVO.getProductBenefitCode());

            // Otherwise see if the old membership has the product option selected
            if (!checked && oldMembershipVO != null)
            {
               checked = oldMembershipVO.hasProductOption(prodBenefitTypeVO.getProductBenefitCode());
            }
            // For some transactions, default options are checked
            if (!checked && memTransVO.selectProductOptionByDefault() )
            {
               checked = prodBenefitTypeVO.isSelectedByDefault();
            }
            // Check the motor news product option if the client motor news flag
            // indicates that they should get it.  This overrides any previous
            // check setting.
//            if (ProductBenefitTypeVO.PBT_MOTOR_NEWS.equals(prodBenefitTypeVO.getProductBenefitCode())  )
//            {
//               checked = Client.MOTOR_NEWS_SEND.equals(clientVO.getMotorNewsSendOption()) ||
//                         Client.MOTOR_NEWS_MEMBERSHIP_SEND.equals(clientVO.getMotorNewsSendOption());
//            }
            // The product benefit code may contain illegal characters when using it
            // as the name of a html control.  It needs to be stripped of these characters first.
            cleanBenefitCode = HTMLUtil.cleanString(prodBenefitTypeVO.getProductBenefitCode());
%>
          <tr <%=(disabled ? " style=\"display: none;\"" : "")%>>
            <td valign="top"><input type="checkbox" name="pb_<%=clientVO.getClientNumber()%>_<%=cleanBenefitCode%>" value="YES"<%=(checked ? " checked" : "")%>></td>
            <td valign="top" class="dataValue"><%=prodBenefitTypeVO.getProductBenefitCode()%></td>
            <td valign="top" class="label"><%=prodBenefitTypeVO.getDescription()%></td>
          </tr>
<%
         }
%>
        </table>
        <br>
<%
      }
   }
}
%>
          </td>
        </tr>
        </table>
        </div>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
        <table border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td>
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
            </td>
            <td>
               <a onclick="document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
                  <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Show the next page">
               </a>
            </td>
<%--
            <td><input type="button" name="btnBack" value="Back" onclick="history.back()"></td>
            <td><input type="submit" name="bntNext" value="Next"></td>
--%>
          </tr>
        </table>
      </td>
    </tr>
  </form>
</table>
</body>
</html>
