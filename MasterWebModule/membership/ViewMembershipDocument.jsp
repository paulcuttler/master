<%@page import="com.ract.common.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.stream.*"%>

<%
MembershipDocument memDocVO = (MembershipDocument) request.getAttribute("membershipDocumentVO");

//converted fetch pattern to lazy to avoid hibernate boundaries being broken.

StringWriter writer = new StringWriter();
writer.write(XMLHelper.getXMLHeaderLine());

String transactionXML = memDocVO.getTransactionXML();

ByteArrayInputStream xmlDocStream = new ByteArrayInputStream(transactionXML.getBytes());

TransformerFactory tFactory = TransformerFactory.newInstance();
Transformer transformer = tFactory.newTransformer(new StreamSource(getServletContext().getResourceAsStream(XMLHelper.XSL_MEMBERSHIP_DOCUMENT_VIEW)));
transformer.transform(new StreamSource(xmlDocStream), new StreamResult(out));
%>
