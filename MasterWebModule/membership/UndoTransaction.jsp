<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>

<%
UndoTransactionGroup transGroup = (UndoTransactionGroup) request.getAttribute("undoTransactionGroup");
%>

<html>
<head>
<title>UndoTransaction</title>
<%-- =CommonConstants.getRACTStylesheet()--%>
<link rel="stylesheet" href="/css/ract.css" type="text/css">
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif');">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
   <form name="undoForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
      <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
      <input type="hidden" name="event" value="undoTransaction_btnSave">
      <tr>
         <td valign="top">
            <h1><%@ include file="/help/HelpLinkInclude.jsp"%> Undo transaction</h1>
            <table border="0" cellspacing="0" cellpadding="3">
<%
MembershipTransactionVO undoTransVO;
MembershipTransactionVO revertToTransVO;
MembershipVO undoMemVO;
MembershipVO revertToMemVO;
ClientVO clientVO;
AddressVO postalAddressVO;
StringBuffer infoTag;
ArrayList undoFailureReasonList;

for (int i = 0; i < transGroup.getTransactionListSize(); i++)
{
   undoTransVO = transGroup.getUndoTransactionVO(i);
   revertToTransVO = transGroup.getRevertToTransactionVO(i);
   undoMemVO = transGroup.getUndoMembershipVO(i);
   revertToMemVO = transGroup.getRevertToMembershipVO(i);
   undoFailureReasonList = transGroup.getUndoFailureReasonList(i);
   clientVO = undoMemVO.getClient();
   postalAddressVO = clientVO.getPostalAddress();

   infoTag = null;
   if (i == transGroup.getContextTransactionIndex())
      infoTag = new StringBuffer("(context");

   if (transGroup.isPayeeTransaction(i))
      if (infoTag == null)
         infoTag = new StringBuffer("(payee");
      else
         infoTag.append(",payee");

   if (infoTag != null)
      infoTag.insert(0,"<br><span class=\"helpTextSmall\">").append(")</span>");
%>
               <tr>
                  <td valign="top" rowspan="3">Membership:<%=(infoTag != null ? infoTag.toString() : "")%></td>
                  <td colspan="3" class="dataValue" valign="top">
                     <a class="actionItemBlack" href="#" title="membership ID = <%=undoMemVO.getMembershipID()%>">
                        <%=undoMemVO.getMembershipNumber()%>
                     </a>
                  </td>
               </tr>
               <tr>
                  <td colspan="3" class="dataValue"><%=clientVO.getDisplayName()%>&nbsp;&nbsp;&nbsp;<span class="label">Number:</span> <%=clientVO.getClientNumber()%></td>
               </tr>
               <tr>
                  <td colspan="3" class="dataValue"><%=postalAddressVO.getSingleLineAddress()%></td>
               </tr>
               <tr>
                  <td class="listHeading"></td>
                  <td class="listHeading"><u>Current</u></td>
                  <td class="listHeading" width="5">&nbsp;</td>
                  <td class="listHeading"><u>Previous</u></td>
               </tr>
               <tr>
                  <td class="label">Status:</td>
                  <td><%=MembershipUIHelper.formatMembershipStatusHTML(undoMemVO.getStatus())%></td>
                  <td>&nbsp;</td>
                  <td><%=(revertToMemVO != null ? MembershipUIHelper.formatMembershipStatusHTML(revertToMemVO.getStatus()) : "")%></td>
               </tr>
               <tr>
                  <td class="label">Expiry date:</td>
                  <td><%=MembershipUIHelper.formatMembershipExpiryDateHTML(undoMemVO.getExpiryDate(), new DateTime())%></td>
                  <td>&nbsp;</td>
                  <td><%=(revertToMemVO != null ? MembershipUIHelper.formatMembershipExpiryDateHTML(revertToMemVO.getExpiryDate(), new DateTime()) : "")%></td>
               </tr>
               <tr>
                  <td class="label">Product:</td>
                  <td class="dataValue"><%=undoMemVO.getProductCode()==null?"":undoMemVO.getProductCode()%></td>
                  <td class="dataValue">&nbsp;</td>
                  <td class="dataValue"><%=((revertToMemVO != null && revertToMemVO.getProductCode() != null) ? revertToMemVO.getProductCode() : "")%></td>
               </tr>
               <tr>
                  <td class="label">Transaction ID:</td>
                  <td class="dataValue">
                    <%=undoTransVO.getTransactionID()%>
                  </td>
                  <td class="dataValue">&nbsp;</td>
                  <td class="dataValue"><%=(revertToTransVO != null ? revertToTransVO.getTransactionID().toString() : "")%></td>
               </tr>
               <tr>
                  <td class="label">Transaction date:</td>
                  <td class="dataValue"><%=undoTransVO.getTransactionDate().formatLongDate()%></td>
                  <td class="dataValue">&nbsp;</td>
                  <td class="dataValue"><%=(revertToTransVO != null ? revertToTransVO.getTransactionDate().formatLongDate() : "")%></td>
               </tr>
               <tr>
                  <td class="label">Transaction type:</td>
                  <td class="dataValue"><%=undoTransVO.getTransactionTypeCode()%></td>
                  <td class="dataValue">&nbsp;</td>
                  <td class="dataValue"><%=(revertToTransVO != null ? revertToTransVO.getTransactionTypeCode() : "")%></td>
               </tr>
<%
   if (undoFailureReasonList != null && !undoFailureReasonList.isEmpty())
   {
%>
               <tr>
                  <td class="label" valign="top">Invalid reasons:</td>
                  <td class="dataValueRed" colspan="3">
                     <ul>
<%
       for (int j = 0 ; j < undoFailureReasonList.size(); j++)
       {
//          out.print("<li>" + undoFailureReasonList.get(j) + (j < (undoFailureReasonList.size() - 1) ? "<br>" : ""));
          out.print("<li>" + undoFailureReasonList.get(j) + "</li>");
       }
%>
                     </ul>
                  </td>
               </tr>
<%
   }
%>
               <tr>
                  <td colspan="4">
                     <hr/>
                  </td>
               </tr>
<%
}
%>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="bottom">
<%
if (transGroup.isUndoable())
{
%>
            <table border="0" cellpadding="5" cellspacing="0">
               <tr>
                  <td> <a onclick="document.all.event.value='undoTransaction_btnSave'; document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Save the transaction');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
                     <img name="SaveButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Save the transaction">
                     </a>
                  </td>
               </tr>
            </table>
<%
}
%>
         </td>
      </tr>
   </form>
</table>
</body>
</html>
