<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.MembershipUIConstants"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ include file="/membership/TransactionInclude.jsp"%>
<%
String transactionTypeCode = transGroup.getTransactionGroupTypeCode();
ClientVO contextClient = transGroup.getContextClient();
if (contextClient == null)
   throw new SystemException("SelectMembershipOptions.jsp : Failed to get context client from transaction group.");
DateTime today = new DateTime();
DateTime prefComDate = transGroup.getPreferredCommenceDate();
DateTime expiryDate = transGroup.getExpiryDate();
if (prefComDate == null)
   prefComDate = today;
DateTime commenceDate = prefComDate;

%>
<html>
<head>
<title>Select Membership Options</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Validation.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js">
</script>
<script language="JavaScript">

   // Used to format the commence date and expiry date.
   var strMonthArray = new Array(12);
   strMonthArray[0] = "Jan";
   strMonthArray[1] = "Feb";
   strMonthArray[2] = "Mar";
   strMonthArray[3] = "Apr";
   strMonthArray[4] = "May";
   strMonthArray[5] = "Jun";
   strMonthArray[6] = "Jul";
   strMonthArray[7] = "Aug";
   strMonthArray[8] = "Sep";
   strMonthArray[9] = "Oct";
   strMonthArray[10] = "Nov";
   strMonthArray[11] = "Dec";

   function convertDate(fld)
   {
      var aDate;
      var dateElements = fld.value.split('/');

      var aDate = new Date(dateElements[2], dateElements[1] - 1, dateElements[0]);
      aDate.setHours(0);
      aDate.setMinutes(0);
      aDate.setSeconds(0);
      aDate.setMilliseconds(0);
      return aDate;
   }

   function validateDate(fld)
   {
      var datefield = fld;

      if (datefield == null) return true;

      // Make sure that something was entered.
      var fldValue = datefield.value;
      if (fldValue.length < 1)
      {
         alert("The preferred commence date must be entered.");
         datefield.focus();
         return false;
      }

      // Make sure all of the fields are valid numbers
      var dateElements = fld.value.split('/');
      var day = new Number(dateElements[0]);
      var month = new Number(dateElements[1]);
      var year = new Number(dateElements[2]);
      if (isNaN(day) || isNaN(month) || isNaN(year))
      {
         alert("The preferred commence date is not valid. Please try again.");
         datefield.focus();
         return false;
      }

      // Do some more rigourous checking of the date.
      if (!checkdate(datefield)) return false;

      // Get todays date as at 12 am.
      var today = new Date();
      today.setHours(0);
      today.setMinutes(0);
      today.setSeconds(0);
      today.setMilliseconds(0);

      // Get the preferred commence date as a date.
      var prefComDate = convertDate(datefield);

      // Make sure that the preferred commence date is today or later
      if (prefComDate.getTime() < today.getTime())
      {
         alert('The preferred commence date cannot be earlier than today.');
         datefield.focus();
         return false;
      }

      // Make sure that the preferred commence date is not more than
      // 3 months into the future.
<%
// Get the period into the future that the preferred commence date can be set to
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
String delayedCommencePeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DELAYED_COMMENCE_PERIOD);
Interval delayedCommencePeriod = delayedCommencePeriod = new Interval(delayedCommencePeriodSpec);

%>
      var futureDate = addMonthsToDate(today,<%=delayedCommencePeriod.getTotalMonths()%>);
      futureDate.setHours(23);
      futureDate.setMinutes(59);
      futureDate.setSeconds(59);
      futureDate.setMilliseconds(999);

      if (prefComDate.getTime() > futureDate.getTime())
      {
         alert('The preferred commence date cannot be more than <%=delayedCommencePeriod.formatForDisplay(Interval.FORMAT_MONTH,Interval.FORMAT_MONTH)%> in the future.');
         datefield.focus();
         return false;
      }
      calculateExpiryDate(prefComDate);
      return true;
   }

   function calculateExpiryDate(prefComDate)
   {
      var term = parseInt(document.all.membershipTerm.value);
//      var term = 12;
      var expDate = addMonthsToDate(prefComDate,term);
      document.all.commenceDate.value = prefComDate.getDate() + ' ' + strMonthArray[prefComDate.getMonth()] + ' ' + prefComDate.getFullYear();;
      document.all.expiryDate.value   = expDate.getDate() + ' ' + strMonthArray[expDate.getMonth()] + ' ' + expDate.getFullYear();;
   }

   function handleChangeOfProfile(profile)
   {
//   alert('New profile = ' + profile);
      if (document.all.prefComDateRow)
      {
         if (profile == '<%=MembershipProfileVO.PROFILE_CLIENT%>')
            document.all.prefComDateRow.style.display = "";
         else
            document.all.prefComDateRow.style.display = "none";
      }
   }

   function handleOnLoad()
   {
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber='+<%=contextClient.getClientNumber()%>;
      MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif');
      return true;
   }

</script>
</head>
<body onLoad="handleOnLoad();">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <form name="selectOptionsForm"
          method="post"
          action="<%=MembershipUIConstants.PAGE_MembershipUIC%>"
          onSubmit="return validateDate(document.all.preferredCommenceDate);">
    <input type="hidden" name="event" value="selectMembershipOptions_btnNext">
    <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
    <tr>
      <td valign="top">
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Select membership options</h1>
        <table border="0" cellspacing="0" cellpadding="3">

          <tr>
            <td class="label">Term of membership:</td>
            <td><img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5"></td>
            <td>
              <select name="membershipTerm" onchange="calculateExpiryDate(convertDate(document.all.preferredCommenceDate));">
                <option value="12" selected>1 year</option>
                <option value="24">2 years</option>
                <option value="36">3 years</option>
              </select>
            </td>
            <td class="helpTextSmall">Select the number of years that the membership is to be taken out for.</td>
          </tr>

<%
String reasonLabel = "";
String reasonHelp = "";
Collection reasonList = null;
Collection rejoinTypeList = null;
if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transactionTypeCode))
{
   reasonLabel = "Join reason:";
   reasonHelp = "Why did the member join?";
   reasonList = refMgr.getJoinReasonList();
}
else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transactionTypeCode))
{
   reasonLabel = "Rejoin reason:";
   reasonHelp = "Why did the member rejoin?";
   reasonList = refMgr.getRejoinReasonList();
   rejoinTypeList = refMgr.getRejoinTypeList();
}

// Only display the rejoin type select list if we found some rejoin types to select
if (rejoinTypeList != null && !rejoinTypeList.isEmpty())
{
%>
          <tr>
            <td class="label">Rejoin type:</td>
            <td>&nbsp;</td>
            <td>
              <select name="rejoinTypeCode">
<%
   ReferenceDataVO selectedRejoinType = transGroup.getRejoinType();
   String selectedRejoinTypeCode = selectedRejoinType == null ? null : selectedRejoinType.getReferenceDataPK().getReferenceCode();
   String typeSelected = null;
   Iterator rejoinTypeListIT = rejoinTypeList.iterator();
   while (rejoinTypeListIT.hasNext())
   {
      ReferenceDataVO rejoinType = (ReferenceDataVO) rejoinTypeListIT.next();
      if ( rejoinType.isActive() )
      {
         typeSelected = rejoinType.getReferenceDataPK().getReferenceCode().equals(selectedRejoinTypeCode) ? " selected" : "";
         out.println("<option value=\"" + rejoinType.getReferenceDataPK().getReferenceCode() + "\"" + typeSelected + ">" + rejoinType.getDescription() + "</option>");
      }
   }
%>
              </select>
            </td>
            <td class="helpTextSmall">Select the type of rejoin required.</td>
          </tr>

<%
}

// Only display the reason select list if we found some reasons to select
if (reasonList != null && !reasonList.isEmpty()) {
%>
          <tr>
            <td class="label"><%=reasonLabel%></td>
            <td>&nbsp;</td>
            <td>
              <select name="reasonCode">
<%
   ReferenceDataVO selectedReason = transGroup.getTransactionReason();
   String selectedReasonCode = selectedReason == null ? null : selectedReason.getReferenceDataPK().getReferenceCode();
   String reasonSelected = null;
   Iterator reasonListIT = reasonList.iterator();
   while (reasonListIT.hasNext())
   {
      ReferenceDataVO reason = (ReferenceDataVO) reasonListIT.next();
      if  ( reason.isActive() )
      {
         reasonSelected = reason.getReferenceDataPK().getReferenceCode().equals(selectedReasonCode) ? " selected" : "";
         out.println("<option value=\"" + reason.getReferenceDataPK().getReferenceCode() + "\"" + reasonSelected + ">" + reason.getDescription() + "</option>");
      }
   }
%>
              </select>
            </td>
            <td class="helpTextSmall"><%=reasonHelp%></td>
          </tr>
<%
}

// Don't let the user enter a preferred commence date when doing a renewal or a transfer.
// And also not when an existing membership group is being joined.
if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(transactionTypeCode)
&& !MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(transactionTypeCode)
&&  transGroup.getMembershipGroupID() == null)
{
%>
          <tr id="prefComDateRow">
            <td class="label">Preferred commence date:</td>
            <td>&nbsp;</td>
            <td>
              <input type="text"
               name="preferredCommenceDate"
               size="10"
               maxlength="10"
               value="<%=prefComDate%>"
               onblur="calculateExpiryDate(convertDate(this));">
            </td>
            <td class="helpTextSmall">Enter a future start date. Use the format dd/mm/yyyy.</td>
          </tr>
<%
}
%>
          <tr>
            <td class="label">Commence date:</td>
            <td>&nbsp;</td>
            <td>
              <input type="text"
                     name="commenceDate"
                     size="15"
                     value="<%=DateUtil.formatDate(commenceDate,"d MMM yyyy")%>"
                     style="border-style: none; font-weight: normal;"
                     disabled>
            </td>
            <td class="helpTextSmall">The membership will start on this date.</td>
          </tr>
          <tr>
            <td class="label">Expiry date:</td>
            <td>&nbsp;</td>
            <td>
              <input type="text"
                     name="expiryDate"
                     size="15"
                     value="<%=DateUtil.formatDate(expiryDate,"d MMM yyyy")%>"
                     style="border-style: none; font-weight: normal;"
                     disabled>
            </td>
            <td class="helpTextSmall">The membership will expire on this date.</td>
          </tr>
<%
UserSession userSession = UserSession.getUserSession(request.getSession());
if (userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CHANGE_PROFILE))
//if (true)
{
%>
          <tr>
            <td class="label">Profile:</td>
            <td>&nbsp;</td>
            <td>
               <select name="profile" onchange='handleChangeOfProfile(document.all.profile.options[document.all.profile.selectedIndex].value)'>
<%
   Collection profileList = refMgr.getMembershipProfileList();
   if (profileList != null)
   {
      MembershipProfileVO currentProfile = transGroup.getMembershipProfile();
      String currentProfileCode = null;
      if (currentProfile != null)
      {
         currentProfileCode = currentProfile.getProfileCode();
      }
      String selected = null;
      MembershipProfileVO memProfileVO = null;
      Iterator profileListIterator = profileList.iterator();
      while (profileListIterator.hasNext())
      {
         memProfileVO = (MembershipProfileVO) profileListIterator.next();
         selected = memProfileVO.getProfileCode().equals(currentProfileCode) ? "selected" : "";
%>
                 <option value="<%=memProfileVO.getProfileCode()%>" <%=selected%>><%=memProfileVO.getDescription()%></option>
<%
      }
   }
%>
               </select>
            </td>
            <td class="helpTextSmall">This effects the expiry date, discounts and where cards are sent.</td>
          </tr>
<%
}
%>
        </table>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
         <table border="0" cellpadding="5" cellspacing="0">
         <tr>
            <td>
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
            </td>
            <td>
               <a onclick="document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
                  <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Show the next page">
               </a>
            </td>

<%--
            <td><input type="button" name="btnBack" value="Back" onclick="history.back()"></td>
            <td><input type="submit" name="btnSubmit" value="Next"></td>
--%>
         </tr>
         </table>
      </td>
    </tr>
    </form>
  </table>
</body>
</html>
