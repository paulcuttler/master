<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>


<%
MembershipGroupVO membershipGroupVO = (MembershipGroupVO) request.getAttribute("membershipGroupVO");
if (membershipGroupVO != null) {
%>
<table  dojoType="filteringTable"
      id="groupMemberTable"
      cellpadding="2"
      alternateRows="true"
      class="dataTable">
<thead>
  <tr>
    <th field="client" dataType="Number" align="left" valign="top">Client</th>
    <th field="member" dataType="String" align="left" valign="top">Member</th>
    <th field="desc" dataType="String" align="left" valign="top">Name</th>
    <th field="status" dataType="String" align="left" valign="top">Status</th>
    <th field="expiry" dataType="String" align="left" valign="top">Expiry</th>
    <th field="join" dataType="String" align="left" valign="top">Group Join Date</th>
  </tr>
</thead>
<tbody>

<%
   ArrayList groupDetailList = new ArrayList(membershipGroupVO.getMembershipGroupDetailList());
   Collections.sort(groupDetailList);
   Iterator groupDetailIterator = groupDetailList.iterator();
   MembershipGroupDetailVO memGroupDetVO;
   MembershipVO groupMemVO;
   ClientVO groupClientVO;
   String addressee;
   int row = 0;
   while (groupDetailIterator.hasNext())
   {
      memGroupDetVO = (MembershipGroupDetailVO) groupDetailIterator.next();
      groupMemVO = memGroupDetVO.getMembership();
      if (groupMemVO != null)
      {
        row++;
         addressee = memGroupDetVO.isPrimeAddressee() ? "<span class=\"helpTextSmall\">(addressee)</span>" : "";
         groupClientVO = groupMemVO.getClient();
         if (groupClientVO != null)
         {
%>
          <tr value="<%=row%>">

            <td class="dataValue"><%=groupClientVO.getClientNumber()%></td>
            <td class="dataValue">
              <!--<%=groupMemVO.getMembershipNumber()%>-->
<%
            // Don't link to the membership if in history mode.
            if (!membershipGroupVO.isHistory())
            {
%>
               <a class="actionItem"
                  href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewGroupMembers_selectMembership&membershipID=<%=groupMemVO.getMembershipID()%>"
                  title="View the membership details.">
                  <%=groupMemVO.getMembershipNumber()%>
               </a>
<%
            }
            else
            {
%>
                  <%=groupMemVO.getMembershipNumber()%>
<%
            }
%>
            </td>
            <td class="dataValue">

<%
            // Don't link to the client if in history mode.
            if (!membershipGroupVO.isHistory())
            {
%>
              <a class="actionItem"
                  href="/viewClient.action?clientNumber=<%=groupClientVO.getClientNumber()%>"
                  title="View the client details.">
                  <%=groupClientVO.getSortableDisplayName()%> <%=addressee%>
               </a>
<%
            }
            else
            {
%>
              <%=groupClientVO.getSortableDisplayName()%> <%=addressee%>
<%
            }
%>
            </td>
            <td class="dataValue"><%=MembershipUIHelper.formatMembershipStatusHTML(groupMemVO.getStatus())%></td>
            <td class="dataValue"><%=MembershipUIHelper.formatMembershipExpiryDateHTML(groupMemVO.getExpiryDate(),new DateTime())%></td>
            <td class="dataValue">
               <a title="<%=DateUtil.formatDate(memGroupDetVO.getGroupJoinDate(),"dd/MM/yyyy HH:mm:ss:SS")%>">
                  <%=memGroupDetVO.getGroupJoinDate().formatShortDate()%>
               </a>
            </td>
          </tr>
<%
         }
      }
   }
%>
        </table>
<%
}
%>
