<%@ page import="java.rmi.RemoteException"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<%
ClientVO contextClientVO = (ClientVO) request.getAttribute("contextClientVO");
%>

<html>
<head>
<title>Transaction complete</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript">

// Refresh the product list in the left frame.
function refreshClientProducts(clientNumber)
{
   if (clientNumber > 0)
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber='+clientNumber;
//   printAdvices();
}

function updateItems(checked)
{
  if (checked)
  {
    checkAll();
  }
  else
  {
    uncheckAll();
  }
}

function checkAll()
{
  count = document.items.elements.length;
  for (i=0; i < count; i++)
  {
    document.items.elements[i].checked = 1;
  }
}

function uncheckAll(){
  count = document.items.elements.length;
  for (i=0; i < count; i++)
  {
    document.items.elements[i].checked = 0;
  }
}

function printAdvices()
{
  count = document.items.elements.length;
  var membershipIdList = "";
  for (i=0; i < count; i++)
  {
    if(document.items.elements[i].checked == 1)
    {
      //add comma if there is already something added to the string
      if (membershipIdList.length > 0)
      {
        membershipIdList += ",";
      }
      //add it to string
      membershipIdList += document.items.elements[i].value;
    }
  }
  if (membershipIdList.length != 0)
  {
    //comma delimited
    var url = 'MembershipReportsUIC?event=transactionComplete_PrintAdvices&membershipIdList=' + membershipIdList;
    window.open(url, '', 'width=250,height=150,toolbar=no,status=no,scrollbars=no,resizable=no');
  }
  else
  {
    alert('No reports have been selected for printing.');
    return;
  }
}

function emailAdvices()
{  
  var url = 'EmailAdviceForm.action?membershipId=' + document.getElementById('pa').value + '&adviceType=' + '<%=SystemParameterVO.EMAIL_ADVICE_GENERAL%>';
  window.open(url, '', 'width=640,height=480,toolbar=no,status=no,scrollbars=no,resizable=no');
 }

function showDetails ( ref )
{
   alert ("Display details for " + ref);
}
</script>
</head>
<body onload="refreshClientProducts(<%=(contextClientVO != null ? contextClientVO.getClientNumber().intValue() : 0)%>); MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/PrintAdviceButton_over.gif');">

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">

<tr valign="top">
<td>

<h1><%@ include file="/help/HelpLinkInclude.jsp"%> Transaction Complete</h1>
<p class="dataValue">The transaction has been completed successfully. </p>

        <div style="height=350; overflow: auto; ">
        <table border="0" cellpadding="3" cellspacing="0" style="">
          <tr class="headerRow">
            <td class="listHeadingPadded"><input type="checkbox" title="Select or deselect all items for printing" checked onclick="updateItems(this.checked)"></td>
            <td class="listHeadingPadded">Membership</td>
            <td class="listHeadingPadded">Product</td>
            <td class="listHeadingPadded">Status</td>
            <td class="listHeadingPadded">Client Name</td>
            <td class="listHeadingPadded">Expiry Date</td>
         </tr>
<%
Collection transactionList = (Collection) request.getAttribute("transactionList");
if (transactionList != null)
{
%>
<form name="items">
<%
   MembershipVO membershipVO = null;
   MembershipTransactionVO memTransVO = null;
   String addressee;
   ClientVO clientVO = null;
   boolean allClientsDeceased = true;
   Iterator transactionListIterator = transactionList.iterator();
   int count = 0;
   while ( transactionListIterator.hasNext() )
   {

      memTransVO = (MembershipTransactionVO) transactionListIterator.next();
      membershipVO = memTransVO.getNewMembership();
      clientVO = (ClientVO) membershipVO.getClient();
      
      if (clientVO.getStatus().equals(Client.STATUS_DECEASED)) {
      	allClientsDeceased = allClientsDeceased && true;
      } else {
      	allClientsDeceased = false;
      }
      
      if (membershipVO.isGroupMember() && membershipVO.isPrimeAddressee())
      {
         addressee = "&nbsp;<span class=\"helpTextSmall\">(addressee)</span>";
      }
      else
      {
         addressee = "";
      }
      
      if (membershipVO.isPrimeAddressee()) {
      	addressee += "<input type=\"hidden\" name=\"pa\" id=\"pa\" value=\"" + membershipVO.getMembershipID() + "\" />";
      }
      
      count++;
%>
         <tr valign="top" class="<%=HTMLUtil.getRowType(count)%>">
           <td><input type="checkbox" name="item_<%=count%>" value="<%=membershipVO.getMembershipID()%>" title="Select or deselect an item for printing" checked></td>
           <td class="label" nowrap>
              <a href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=transactionComplete_viewMembership&membershipID=<%=membershipVO.getMembershipID()%>"
                 class="actionItem"
                 title = "View the membership details"
              >
                <%=membershipVO.getMembershipNumber()%> <%=membershipVO.getMembershipTypeCode()%>
              </a>
           </td>
           <td class="actionItem">
             <%=membershipVO.getProductCode()==null?"":membershipVO.getProductCode()%>
           </td>
           <td><%=membershipVO.getStatus()%></td>
           <td><a href="<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientSummary&clientNumber=<%=clientVO.getClientNumber()%>&clientType=<%=clientVO.getClientType()%>"
                  class="actionItem"
                  title = "View the client details"><%=clientVO.getDisplayName()%></a>
                <%=addressee%>
           </td>
           <td class="actionItem"><%=membershipVO.getExpiryDate().formatShortDate()%>
           </td>
         </tr>
<%
   }
   request.setAttribute("allClientsDeceased", allClientsDeceased);
%>
</form>
<%
}
%>
       </table>
       </div>

</td>
</tr>
<tr valign="bottom">
  <td colspan="5">
  	<input type="button" name="btnPrintAdvice" value="Print Advice" onclick="printAdvices();" <%= (Boolean) request.getAttribute("allClientsDeceased") ? " disabled=\"disabled\" title=\"Cannot print advice for decease clients\"" : "" %>>
  	<input type="button" name="btnEmailAdvice" value="Email Advice" onclick="emailAdvices();" <%= (Boolean) request.getAttribute("allClientsDeceased") ? " disabled=\"disabled\" title=\"Cannot email advice for decease clients\"" : "" %>>
  </td>
</tr>
</table>
</body>
</html>
