<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<%@ include file="/membership/TransactionInclude.jsp"%>

<%
String emptyString = "";
String unknownString = "??????";
DateTime currentDate = new DateTime();

MembershipTransactionVO memTransVO = transGroup.getMembershipTransactionForPA();
MembershipVO memVO = memTransVO.getMembership();
MembershipVO newMemVO = memTransVO.getNewMembership();
String membershipID = (String) request.getParameter("membershipID");

Interval remainingMembership = null;

if (memVO != null)
{
  remainingMembership = new Interval(currentDate,memVO.getExpiryDate());
}

//String amtOutstanding = (String) request.getAttribute("amountOutstanding");

//get the old expiry date
DateTime expiryDate = memVO.getExpiryDate();

//check if membership on hold
boolean membershipOnHold = MembershipVO.STATUS_ONHOLD.equals(memVO.getStatus());

//get the start date
DateTime startDate = membershipOnHold ? expiryDate : currentDate;

if (memVO == null)
{
   // See if the membership ID was specified
   if (membershipID == null) throw new ServletException("A membership VO or a membership ID must be passed to the ViewPersonalMembership page.");

   // Get the membership VO from the app server.
   Integer memID = new Integer(membershipID);

   // Look up the home membership interface
//   MembershipHome memHome = MembershipEJBHelper.getMembershipHome();
MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

   // Find the specified membership
   memVO = membershipMgr.getMembership(memID);

   // Get the membership value object.
//   memVO = membership.getVO();

}
// Show an error page if the membership could not be obtained.
if (memVO == null) throw new Exception("Unable to get membership details in HoldPersonalMembership page.");

ClientVO clientVO = memVO.getClient();
request.setAttribute("client",clientVO);

   ArrayList cancelReasonList = null;
   if (refMgr != null)
   {
      try
      {
         cancelReasonList = new ArrayList(refMgr.getCancelReasonList());
      }
      catch (Exception e)
      {
      throw new ServletException(e.getMessage());
      }
   }
   //	Find Display Name of Prime Addressee
   String primeAddresseeName = null;
   String paName = " ";
   Integer newPAClientNo = new Integer(0);
   ClientVO tmpClientVO = null;
   MembershipVO tmpoldestMemVO = null;

   // get the maximum hold period
   CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
   Interval maxHoldPeriod = new Interval(comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MAX_HOLD_PERIOD));
   int maxYears = maxHoldPeriod.getYears();

%>
<html>
<head>
<title>Cancel Membership</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>

<script language="javascript">

function submitForm()
{
  if (validateDate(document.all.endDate))
  {
    document.forms[0].submit();
  }
}

function updateEndDate()
{
  var startDate = new Date();
  var membershipOnHold = document.all.membershipOnHold.value;
  var expiryDateString = document.all.expiryDate.value;

  var day,month,year;

//  alert(expiryDateString);

  //if already on hold
  if (membershipOnHold == 'true')
  {
     //use the expiry date as the basis for the date
     var dateElements = expiryDateString.split('/');
     day = new Number(dateElements[0]);
     month = new Number(dateElements[1]);
     year = new Number(dateElements[2]);
  }
  else
  {
     //use the transaction date
     //alert('in tx date');
     year = startDate.getFullYear();
     month = startDate.getMonth()+1;
     day = startDate.getDate();
  }
//  alert(membershipOnHold);
  var holdPeriod = parseInt(document.all.period[document.all.period.selectedIndex].value);
  if (holdPeriod == 0)
  {
    document.all.endDate.value = '';
    return;
  }

  var holdPeriodYear = (year + holdPeriod);
  var newEndDate = day+'/'+month+'/'+holdPeriodYear;
  document.all.endDate.value = newEndDate;
}

function validateDate ( inputDate )
{
    var holdToDate = inputDate;
    if (holdToDate.value == null || holdToDate.value == '')
    {
      alert("You must enter a period of hold.");
      holdToDate.focus();
      return false;
    }

    var dateElements = inputDate.value.split('/');
    var day = new Number(dateElements[0]);
    var month = new Number(dateElements[1]);
    var year = new Number(dateElements[2]);
    if (isNaN(day) || isNaN(month) || isNaN(year))
    {
      alert("The hold to date is invalid.");
      holdToDate.focus();
      return false;
    }
    if (!checkdate(inputDate)) return false;

    var aDate = new Date(dateElements[2], dateElements[1]-1, dateElements[0]);
    var today = new Date();
    var threeYears = addMonthsToDate(today, <%=(maxYears*12)%>);

    if (aDate.getTime() <= today.getTime() || aDate.getTime() > threeYears.getTime())
    {
      alert("Hold to date must be in the future but not more than <%=maxYears%> years.");
      holdToDate.focus();
      return false;
    }
    return true;
}
</script>

</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif');">

<form name="cancelForm" method="post" action="<%= MembershipUIConstants.PAGE_MembershipUIC%>"
          onSubmit="return validateDate(document.all.endDate);">
<input type="hidden" name="event" value="holdMembership_btnNext">
<input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
<input type="hidden" name="newPrimeAddressee" value="<%=clientVO.getClientNumber().toString()%>">
<input type="hidden" name="startDate" value="<%=startDate%>">
<input type="hidden" name="expiryDate" value="<%=expiryDate%>">
<input type="hidden" name="remainingMembership" value="<%=remainingMembership.toString()%>">
<input type="hidden" name="membershipOnHold" value="<%=membershipOnHold%>">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr valign="top">
<td>
<h1><%@ include file="/help/HelpLinkInclude.jsp"%> Hold <%=(memVO.getMembershipTypeCode() == null ? "?" : memVO.getMembershipTypeCode())%> Membership</h1>

<%@include file="/client/ViewClientInclude.jsp"%>

        <table border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td class="label" width="148">Membership number:</td>
            <td class="dataValue" width="4">&nbsp;</td>
            <td class="dataValue" width="50"><%=(memVO.getMembershipNumber() == null ? emptyString : memVO.getMembershipNumber())%></td>
            <td class="dataValue" width="108">&nbsp;</td>
          </tr>
          <tr>
            <td class="label" width="148">Status:</td>
            <td class="dataValue" width="4">&nbsp;</td>
            <td class="dataValue" width="60"><%=MembershipUIHelper.formatMembershipStatusHTML(memVO.getStatus())%></td>
            <td class="dataValue" width="108"><%=membershipOnHold?"(Extending hold period)":""%></td>
          </tr>
          <tr>
            <td class="label" width="148">Product:</td>
            <td class="dataValue" width="4">&nbsp;</td>
            <td class="dataValue" width="50"><%=(memVO.getProductCode() == null ? emptyString : memVO.getProductCode())%></td>
            <td class="dataValue" nowrap><span class="label">Effective from:
              </span> <%=(memVO.getProductEffectiveDate() == null ? emptyString : memVO.getProductEffectiveDate().formatShortDate())%></td>
          </tr>
          <tr>
            <td class="label" width="148">Join date:</td>
            <td class="dataValue" width="4">&nbsp;</td>
            <td class="dataValue" width="50"><%=(memVO.getJoinDate() == null ? emptyString : memVO.getJoinDate().formatShortDate())%></td>
            <td class="dataValue" width="108"><%=memVO.getMembershipYears().formatForDisplay(Interval.FORMAT_YEAR,Interval.FORMAT_YEAR)%>
            </td>
          </tr>
          <tr>
            <td class="label" width="148">Commence date:</td>
            <td class="dataValue" width="4">&nbsp;</td>
            <td class="dataValue" width="50"><%=(memVO.getCommenceDate() == null ? emptyString : memVO.getCommenceDate().formatShortDate())%></td>
            <td class="dataValue" width="108">&nbsp;</td>
          </tr>
          <tr>
            <td class="label" width="148">Expiry date:</td>
            <td class="dataValue" width="4">&nbsp;</td>
            <td class="dataValue" width="50"><%=expiryDate%></td>
            <td class="dataValue" width="108">&nbsp;</td>
          </tr>

          <tr>
           <td class="label">Estimated credit:</td>
           <td class="dataValue" width="4">&nbsp;</td>
           <td class="dataValue">
           <%
           UnearnedValue memUnearnedValue = memVO.getPaidRemainingMembershipValue();
           %>
           <a title="<%=memUnearnedValue.getDescriptionForHTMLAnchor()%>"><%=memUnearnedValue.formatForDisplay()%></a>
           </td>
           <td class="dataValue">&nbsp;</td>
          </tr>

          <tr>
            <td class="label" width="148">Amount Outstanding:</td>
            <td class="dataValue" width="4">&nbsp;</td>
            <td class="dataValue" width="50"><%=memVO.getIndividualAmountOutstanding().formatForDisplay()%></td>
            <td class="dataValue" width="108">&nbsp;</td>
          </tr>

        </table>
        <table border="0" cellspacing="1" cellpadding="3">
          <tr>
            <td colspan="4">
              <hr>
            </td>
          </tr>
          <tr>
            <td class="label">Hold start date:</td>
            <td class="dataValue" colspan="3"><%=startDate%></td>
            <!--          <td class="dataValue">&nbsp;</td> -->
          </tr>
          <tr>
            <td class = "label">Period of hold:</td>
<!--            <td class = "label">End date for hold (dd/mm/yyyy)</td>-->
            <td>
              <select name="period" onchange="updateEndDate();">
                <option value="0"></option>
                <option value="1">1 Year</option>
                <option value="2">2 Years</option>
                <option value="3">3 Years</option>
              </select>
              <input type="text" readonly name="endDate" size="10">
            </td>
          </tr>
        </table>
      </td>
</tr>
<tr>
<td valign="bottom">
    <table border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td>
            <a onclick="history.back(); return true;"
               onMouseOut="MM_swapImgRestore();"
               onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
               <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
            </a>
         </td>
         <td>
            <a onclick="submitForm();"
               onMouseOut="MM_swapImgRestore();"
               onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
               <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Show the next page">
            </a>
        </td>
    </tr>
    </table>
</td></tr>
</table>
</form>
</body>
</html>
