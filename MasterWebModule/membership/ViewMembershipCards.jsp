<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>

<%@include file="/common/NoCache.jsp"%>

<%
MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");

String productCode = memVO.getProductCode();
ClientVO clientVO = memVO.getClient();
// Required for ViewClientInclude.jsp
request.setAttribute("client",clientVO);
String proposedCardName = CardHelper.getCardName(clientVO,memVO.getMembershipProfile().getNameSuffix());
if (proposedCardName == null || proposedCardName.length() < 1) {
   proposedCardName = "???";
}
%>

<html>
<head>
  <title>View membership cards</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>

<script language="Javascript">
   function removeCardRequest(cardID)
   {
     document.all.event.value = 'viewMembershipCards_btnRemove';
     document.all.cardID.value = cardID;
     document.viewMembershipCards.submit();
     return 0;
   }
</script>

</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif');">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <form name="viewMembershipCards" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="viewMembershipCards_btnMembership">
  <input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
    <tr valign="top">
      <td>
         <h2><%@ include file="/help/HelpLinkInclude.jsp"%> Membership cards</h2>
         <%@ include file="/client/ViewClientInclude.jsp"%>
         <table border="0" cellpadding="3" cellspacing="0">
         <tr>
            <td class="label">Proposed card name:</td>
            <td class="dataValue"><%=proposedCardName%></td>
         </tr>
         </table>
         <%@ include file="/membership/ViewMembershipCardInclude.jsp"%>
         <p>
         <%
         //allow an optional message
         String message = (String)request.getAttribute("message");
         if (message != null)
         {
         %>
           <span class="helpText"><%=message%></span>
         <%
         }
         %>
         </p>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
         <table border="0" cellpadding="5" cellspacing="0">
         <tr>
            <td align="left">
               <a onclick="document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('View the membership details');MM_swapImage('ViewMembershipButton','','<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif',1);return document.MM_returnValue" >
                  <img name="ViewMembershipButton" src="<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton.gif" border="0" alt="View the membership details">
               </a>
            </td>
         </tr>
         </table>
      </td>
    </tr>
    </form>
  </table>
</body>
</html>
