
<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>

<%
MembershipRefMgr membershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();
%>

<html>
<head>
<title>Products and Product Benefits</title>
</head>
<body>
<h1>Products and Product Benefits</h1>

<%
   ArrayList productList = null;

   try
   {
      productList = new ArrayList(membershipRefMgr.getProductList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving product list.<br>");
      out.print(e.getMessage());
   }

   if (productList != null)
   {
%>
<table border=1>
   <tr>
      <td>ProductCode</td>
      <td>Description</td>
      <td>Status</td>
      <td>isDefault</td>
   </tr>
<%
      Iterator it = productList.iterator();
      while (it.hasNext())
      {
         ProductVO tmp = (ProductVO) it.next();
%>
   <tr>
      <td><%=tmp.getProductCode()%></td>
      <td><%=tmp.getDescription()%></td>
      <td><%=tmp.getStatus()%></td>
      <td><%=tmp.isDefault()%></td>
   </tr>
   <tr>
      <td>&nbsp;</td>
      <td colspan=3>
         All product benefits:<br>
<%
         try
         {
            Collection benefitList = tmp.getProductBenefitList();
            if (benefitList != null)
            {
%>
         <table border=1>
            <tr>
               <td>ProductBenefitCode</td>
               <td>Description</td>
               <td>Status</td>
               <td>Optional</td>
               <td>Selected</td>
            </tr>
<%
               Iterator it2 = benefitList.iterator();
               while (it2.hasNext())
               {
                  ProductBenefitTypeVO pbtVO = (ProductBenefitTypeVO) it2.next();
%>
               <tr>
                  <td><%=pbtVO.getProductBenefitCode()%></td>
                  <td><%=pbtVO.getDescription()%></td>
                  <td><%=pbtVO.getStatus()%></td>
                  <td><%=pbtVO.isOptional()%></td>
                  <td><%=pbtVO.isSelectedByDefault()%></td>
               </tr>
<%
               }
%>
         </table>
<%
            }
         }
         catch (Exception e)
         {
            PrintWriter pw = new PrintWriter(out);
            e.printStackTrace(pw);
         }
%>
         Optional product benefits:<br>
<%
         try
         {
            Collection benefitList = tmp.getProductBenefitList(false,true);
            if (benefitList != null)
            {
%>
         <table border=1>
            <tr>
               <td>ProductBenefitCode</td>
               <td>Description</td>
               <td>Status</td>
               <td>Optional</td>
               <td>Selected</td>
            </tr>
<%
               Iterator it3 = benefitList.iterator();
               while (it3.hasNext())
               {
                  ProductBenefitTypeVO pbtVO = (ProductBenefitTypeVO) it3.next();
%>
               <tr>
                  <td><%=pbtVO.getProductBenefitCode()%></td>
                  <td><%=pbtVO.getDescription()%></td>
                  <td><%=pbtVO.getStatus()%></td>
                  <td><%=pbtVO.isOptional()%></td>
                  <td><%=pbtVO.isSelectedByDefault()%></td>
               </tr>
<%
               }
%>
         </table>
<%
            }
         }
         catch (Exception e)
         {
            PrintWriter pw = new PrintWriter(out);
            e.printStackTrace(pw);
         }
%>
      </td>
   </tr>
<%
      }
%>
</table>
<%
   }
%>



</body>
</html>
