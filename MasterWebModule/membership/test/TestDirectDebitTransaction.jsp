<%@ page import="java.io.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>

<%--
test direct debit transaction
--%>

<html>

<head>
</head>

<%
//get two memberships
int mem1ID = 80;
int mem2ID = 81;
%>

<h2>Memberships</h2>

<table>

<tr>
<td colspan="3"><hr/></td>
</tr>

<tr>
  <td>Membership 1</td><td class="dataValue"><%=mem1ID%></td>
</tr>
<tr>
  <td>Membership 2</td><td class="dataValue"><%=mem2ID%></td>
</tr>
<%--
<tr>
  <td>Membership 3</td><td class="dataValue"></td>
</tr>
--%>
<tr>
<td colspan="3"><hr/></td>
</tr>

</table>

<table>

<%

MembershipMgr mMgr = MembershipEJBHelper.getMembershipMgr();
MembershipVO mem1VO = mMgr.getMembership(new Integer(mem1ID));
MembershipVO mem2VO = mMgr.getMembership(new Integer(mem2ID));

TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP,SecurityHelper.getSystemUser());
//add the members
transGroup.addSelectedClient (mem1VO);
transGroup.addSelectedClient(mem2VO);
transGroup.setContextClient (mem1VO);

// Get the personal membership type and set in on the transaction group.
MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr ();
MembershipTypeVO memTypeVO = refMgr.getMembershipType ( MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL );
transGroup.setMembershipType ( memTypeVO );

ProductVO productVO = null;
productVO = mem1VO.getProduct();

transGroup.setSelectedProduct(productVO);

transGroup.createTransactionsForSelectedClients();

//transGroup.createTransactionFees();

transGroup.calculateTransactionFee();

Collection transList = transGroup.getTransactionList();
if (transList != null && !transList.isEmpty())
{

   Iterator transListIT = transList.iterator();
   while (transListIT.hasNext())
   {
      MembershipTransactionVO memTransVO = (MembershipTransactionVO) transListIT.next();
      MembershipVO memVO = memTransVO.getNewMembership();

%>
 <tr>
   <td>Membership Number</td><td class="dataValue"><%=memVO.getMembershipNumber()%></td>
</tr>
<tr>
   <td>Transaction Type Code</td><td><%=memTransVO.getTransactionTypeCode()%></td>
</tr>
<tr>
   <td>Gross Transaction Fee</td><td><%=CurrencyUtil.formatDollarValue(memTransVO.getGrossTransactionFee())%></td>
</tr>
<tr>
  <td>Product Code</td><td><%=memTransVO.getProductCode()%></td>
</tr>
<tr>
  <td>Effective Date</td><td><%=memTransVO.getEffectiveDate()%></td>
</tr>
<tr>
  <td>End Date</td><td><%=memTransVO.getEndDate()%></td>
</tr>
<tr>
  <td>Payment Method</td><td><%=mem1VO.getDirectDebitAuthorityID()==null?"Receipting":"DD"%></td>
</tr>
<tr>
  <td>AdjustmentTotal</td><td><%=memTransVO.getAdjustmentTotal()%></td>
</tr>
<tr>
  <td></td><td></td>
</tr>
<tr>
<td colspan="3"><hr/></td>
</tr>

<%
   }
}
%>

<tr>
<td colspan="3"><hr/></td>
</tr>

</table>

<table>
<tr>
  <td>PA direct debit debt</td><td><%=transGroup.hasPADirectDebitDebt()%></td>
</tr>
<tr>
  <td>Amount payable</td><td><%=CurrencyUtil.formatDollarValue( transGroup.getAmountPayable())%></td>
</tr>
<tr>
  <td>Join fee count</td><td></td>
</tr>
</table>

</html>














