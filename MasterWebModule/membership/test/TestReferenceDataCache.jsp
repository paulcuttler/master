
<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.common.*"%>


<%
MembershipRefMgr membershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();
%>
<html>
<head>
<title>CacheTestClient</title>
</head>
<body>
<h1>Cache test page</h1>
Membership types:
<%
   ArrayList membershipTypeList = null;

   try
   {
      membershipTypeList = new ArrayList(membershipRefMgr.getMembershipTypeList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving  membership type list.<br>");
      out.print(e.getMessage());
   }

   if (membershipTypeList != null){
      out.print("<table border=1>");
      Iterator it = membershipTypeList.iterator();
      while (it.hasNext()){
         MembershipTypeVO tmp = (MembershipTypeVO) it.next();
         MembershipTypeVO tmp2 = membershipRefMgr.getMembershipType(tmp.getMembershipTypeCode());
         out.print("<tr>");
         out.print("<td>" + tmp.getMembershipTypeCode() + "</td><td>"
                          + tmp.getDescription() + "</td><td>"
                          + tmp.isVehiclesAllowed() + "</td><td>"
                          + tmp.isVehiclesRequired() + "</td><td>"
                          + tmp.isDefault() + "</td><td>"
                          + tmp.getSortOrder() + "</td><td>"
                          + tmp.isMultipleMembershipsAllowed() + "</td><td>"
                          + tmp.isGroupMembershipAllowed() + "</td><td>"
                          + tmp2.getMembershipTypeCode() + "</td>"
                  );
         out.print("</tr>");
      }
      out.print("</table>");
   }
%>
<br>
Products:
<%
   ArrayList productList = null;

   try
   {
      productList = new ArrayList(membershipRefMgr.getProductList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving product list.<br>");
      out.print(e.getMessage());
   }

   if (productList != null){
      out.print("<table border=1>");
      Iterator it = productList.iterator();
      while (it.hasNext()){
         ProductVO tmp = (ProductVO) it.next();
         ProductVO tmp2 = membershipRefMgr.getProduct(tmp.getProductCode());
         out.print("<tr>");
         out.print("<td>" + tmp.getProductCode() + "</td><td>"
                          + tmp.getDescription() + "</td><td>"
                          + tmp.getStatus() + "</td><td>"
                          + tmp.isDefault() + "</td><td>");
         Collection codeList = tmp.getProductBenefitList();
         if (codeList != null)
         {
            Iterator it2 = codeList.iterator();
            while (it2.hasNext())
            {
               String code = (String) it2.next();
               out.print(code + "<br>");
            }
         }
         out.print("</td><td>" + tmp2.getProductCode() + "</td>");
         out.print("</tr>");
      }
      out.print("</table>");
   }
%>
<br>
Products benefits types:
<%
   ArrayList productBenefitList = null;

   try
   {
      productBenefitList = new ArrayList(membershipRefMgr.getProductBenefitTypeList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving product benefit list.<br>");
      out.print(e.getMessage());
   }

   if (productBenefitList != null){
      out.print("<table border=1>");
      Iterator it = productBenefitList.iterator();
      while (it.hasNext()){
         ProductBenefitTypeVO tmp = (ProductBenefitTypeVO) it.next();
         ProductBenefitTypeVO tmp2 = membershipRefMgr.getProductBenefitType(tmp.getProductBenefitCode());
         out.print("<tr>");
         out.print("<td>" + tmp.getProductBenefitCode() + "</td><td>"
                          + tmp.getDescription() + "</td><td>"
                          + tmp.getStatus() + "</td><td>"
                          + tmp.isOptional() + "</td><td>"
                          + tmp.isReadOnly() + "</td><td>"
                          + tmp2.getProductBenefitCode() + "</td>"
                  );
         out.print("</tr>");
      }
      out.print("</table>");
   }
%>
<br>
Transaction types:
<%
   ArrayList transactionTypeList = null;

   try
   {
      transactionTypeList = new ArrayList(membershipRefMgr.getMembershipTransactionTypeList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving transaction type list.<br>");
      out.print(e.getMessage());
   }

   if (transactionTypeList != null){
      out.print("<table border=1>");
      Iterator it = transactionTypeList.iterator();
      while (it.hasNext()){
         MembershipTransactionTypeVO tmp = (MembershipTransactionTypeVO) it.next();
         MembershipTransactionTypeVO tmp2 = membershipRefMgr.getMembershipTransactionType(tmp.getTransactionTypeCode());
         out.print("<tr>");
         out.print("<td>" + tmp.getTransactionTypeCode() + "</td><td>"
                          + tmp.getDescription() + "</td><td>"
                          + tmp.isReadOnly() + "</td><td>"
                          + tmp2.getTransactionTypeCode() + "</td>");
         out.print("</tr>");
      }
      out.print("</table>");
   }
%>

<br>
Discount types:
<%
   ArrayList discountTypeList = null;

   try
   {
      discountTypeList = new ArrayList(membershipRefMgr.getDiscountTypeList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving discount type list.<br>");
      out.print(e.getMessage());
   }
   if (discountTypeList != null)
   {
      out.print("<table border=1>");
      Iterator it = discountTypeList.iterator();
      while (it.hasNext()){
         DiscountTypeVO tmp = (DiscountTypeVO) it.next();
         DiscountTypeVO tmp2 = membershipRefMgr.getDiscountType(tmp.getDiscountTypeCode());
         out.print("<tr>");
         out.print("<td>" + tmp.getDiscountTypeCode() + "</td><td>"
                          + tmp.getDescription() + "</td><td>"
                          + tmp.getStatus() + "</td><td>"
                          + tmp.getDiscountAmount() + "</td><td>"
                          + tmp.getDiscountPercentage() + "</td><td>"
                          + (tmp.getDiscountPeriod() != null ? tmp.getDiscountPeriod().toString() : "null") + "</td><td>"
                          + tmp.getEffectiveFromDate() + "</td><td>"
                          + tmp.getEffectiveToDate() + "</td><td>"
                          + tmp.isSystemControlled() + "</td><td>"
                          + tmp.isAutomaticDiscount() + "</td><td>"
                          + tmp.isRecurringDiscount() + "</td><td>"
                          + tmp.isMinimumFeeApplicable() + "</td><td>"
                          + tmp.isReadOnly() + "</td><td>"
                          + tmp2.getDiscountTypeCode() + "</td>");
         out.print("</tr>");
      } 
      out.print("</table>");
   } 
%>


<br>
Join reasons:
<%
   ArrayList joinReasonList = null;

   try
   {
      joinReasonList = new ArrayList(membershipRefMgr.getJoinReasonList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving join reason list.<br>");
      out.print(e.getMessage());
   }
   if (joinReasonList != null)
   {
      out.print("<table border=1>");
      Iterator it = joinReasonList.iterator();
      while (it.hasNext()){
         ReferenceDataVO tmp = (ReferenceDataVO) it.next();
         ReferenceDataVO tmp2 = membershipRefMgr.getJoinReason(tmp.getReferenceDataPK().getReferenceCode());
         out.print("<tr>");
         out.print("<td>" + tmp.getReferenceDataPK().getReferenceType() + "</td><td>"
                          + tmp.getReferenceDataPK().getReferenceCode() + "</td><td>"
                          + tmp.getDescription() + "</td><td>"
                          + tmp.isReadOnly() + "</td><td>"
                          + tmp2.getReferenceDataPK().getReferenceType() + "/" + tmp2.getReferenceDataPK().getReferenceCode() + "</td>");
         out.print("</tr>");
      }
      out.print("</table>");
   }
%>


<form method="post">
<br><br>
<input type="submit" name="Submit" value="Submit">
<input type="reset" value="Reset">
</form>
</body>
</html>
