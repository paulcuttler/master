<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>


<html>
<head>
<title>Test Product Benefit Fees</title>
</head>
<body>
<h1>Test Product Benefit Fees</h1>


<%
   MembershipRefMgr membershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();

   String productBenefitCode = request.getParameter("productBenefitCode");
   String transactionTypeCode = request.getParameter("transactionTypeCode");
   String membershipTypeCode = request.getParameter("membershipTypeCode");

   Collection productBenefitList = null;
   Collection transTypeList = null;
   Collection memTypeList = null;

   try
   {
      productBenefitList = membershipRefMgr.getProductBenefitTypeList();
   }
   catch(Exception e)
   {
      out.print("Failed retrieving product benefit list.<br>");
      out.print(e.getMessage());
   }

   try
   {
      transTypeList = membershipRefMgr.getMembershipTransactionTypeList();
   }
   catch(Exception e)
   {
      out.print("Failed retrieving transaction type list.<br>");
      out.print(e.getMessage());
   }

   try
   {
      memTypeList = membershipRefMgr.getMembershipTypeList();
   }
   catch(Exception e)
   {
      out.print("Failed retrieving membership type list.<br>");
      out.print(e.getMessage());
   }

%>

<form method="post">
<table border=0>
<tr>
   <td>Product Benefit</td>
   <td>Transaction Type</td>
   <td>Membership Type</td>
</tr>
<tr>
   <td>
      <select name="productBenefitCode">
<%
if (productBenefitList != null)
{
   Iterator prodBenefitIT = productBenefitList.iterator();
   while (prodBenefitIT.hasNext())
   {
      ProductBenefitTypeVO prodBenefitVO = (ProductBenefitTypeVO) prodBenefitIT.next();
      String prodBenefitCode = prodBenefitVO.getProductBenefitCode();
      if (prodBenefitCode.equals(productBenefitCode))
         out.print("<option selected>" + prodBenefitCode + "</option>");
      else
         out.print("<option>" + prodBenefitCode + "</option>");
   }
}
%>
      </select>
   </td>
   <td>
      <select name="transactionTypeCode">
<%
if (transTypeList != null)
{
   Iterator transTypeIT = transTypeList.iterator();
   while (transTypeIT.hasNext())
   {
      MembershipTransactionTypeVO transTypeVO = (MembershipTransactionTypeVO) transTypeIT.next();
      String transTypeCode = transTypeVO.getTransactionTypeCode();
      if (transTypeCode.equals(transactionTypeCode))
         out.print("<option selected>" + transTypeCode + "</option>");
      else
         out.print("<option>" + transTypeCode + "</option>");
   }
}
%>
      </select>
   </td>
   <td>
      <select name="membershipTypeCode">
<%
if (memTypeList != null)
{
   Iterator memTypeIT = memTypeList.iterator();
   while (memTypeIT.hasNext())
   {
      MembershipTypeVO memTypeVO = (MembershipTypeVO) memTypeIT.next();
      String memTypeCode = memTypeVO.getMembershipTypeCode();
      if (memTypeCode.equals(membershipTypeCode))
         out.print("<option selected>" + memTypeCode + "</option>");
      else
         out.print("<option>" + memTypeCode + "</option>");
   }
}
%>
      </select>
   </td>
</tr>
</table>
<input type="submit" name="Submit" value="Submit">
</form>
<P>

<%
   Collection feeList = null;

   if (productBenefitCode != null && productBenefitCode.length() > 0)
   {
      try
      {
         feeList = membershipRefMgr.findProductBenefitFee(productBenefitCode,membershipTypeCode,transactionTypeCode,0,null);
      }
      catch(Exception e)
      {
         out.print("Failed retrieving product benefit fee list.<br>");
         out.print(e.getMessage());
      }
   }
   else
   {
      out.print("No product benefit code selected.");
   }

   if (feeList == null || !(feeList.size() > 0))
   {
      out.print("Product benefit fee list is empty.");
   }
   else
   {
%>
      <table border=1>
         <tr>
            <td>FeeSpecificationID</td>
            <td>FeeTitle</td>
            <td>ProductBenefitCode</td>
            <td>MembershipTypeCode</td>
            <td>TransactionTypeCode</td>
            <td>GroupNumber</td>
            <td>EffectiveFromDate</td>
            <td>EffectiveToDate</td>
            <td>FeePerMember</td>
            <td>FeePerVehicle</td>
         </tr>
<%
      Iterator feeIT = feeList.iterator();
      while (feeIT.hasNext()){
         FeeSpecificationVO feeSpecVO = (FeeSpecificationVO) feeIT.next();
%>
         <tr>
            <td><%=feeSpecVO.getFeeSpecificationID()%></td>
            <td><%=feeSpecVO.getProductBenefitCode()%></td>
            <td><%=feeSpecVO.getMembershipTypeCode()%></td>
            <td><%=feeSpecVO.getTransactionTypeCode()%></td>
            <td><%=feeSpecVO.getGroupNumber()%></td>
            <td><%=feeSpecVO.getEffectiveFromDate()%></td>
            <td><%=feeSpecVO.getEffectiveToDate()%></td>
            <td><%=feeSpecVO.getFeePerMember()%></td>
            <td><%=feeSpecVO.getFeePerVehicle()%></td>
         </tr>
<%
      }
%>
      </table>
<%
   }
%>
</body>
</html>
