<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.util.*"%>

<%

MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
MembershipVO membership = null;
   String searchMembershipID = request.getParameter("searchMembershipID");

   if (searchMembershipID != null && searchMembershipID.length() > 0){
     try
     {
        membership = membershipMgr.getMembership(new Integer(searchMembershipID));
     }
     catch(Exception e)
     {
        out.print("Failed to get search membership ID : " + searchMembershipID + "<br>");
        out.print(e.getMessage());
     }
   }
   else {

      String membershipID = request.getParameter("membershipID");

      if (membershipID != null){

         String updateMode = request.getParameter("updateMode");

         if (updateMode.equals("1"))
         {
            try
            {
               membership = membershipMgr.getMembership(new Integer(membershipID));

               membership.setMembershipNumber(request.getParameter("membershipNumber"));
               membership.setStatus(request.getParameter("status"));
               membership.setClientNumber(new Integer(request.getParameter("clientNumber")));
               membership.setMembershipTypeCode(request.getParameter("membershipTypeCode"));
               membership.setJoinDate(request.getParameter("joinDate"));
               membership.setCommenceDate(request.getParameter("commenceDate"));
               membership.setExpiryDate(request.getParameter("expiryDate"));
               membership.setProductCode(request.getParameter("productCode"));
//               membership.setProductEffectiveDate(request.getParameter("productEffectiveDate"));
               membership.setVehicleCount(request.getParameter("vehicleCount"));
               membership.setVehicleExpiryType(request.getParameter("vehicleExpiryType"));

               membershipMgr.updateMembership(membership);

               out.print("saved membership data through entity<br>");
            }
            catch(Exception e)
            {
               out.print("Failed to update membership through entity<br>");
               out.print(e.getMessage());
            }
         }
         else if (updateMode.equals("2"))
         {
            try
            {
//               MembershipVO memVO = new MembershipVO(new Integer(membershipID));
//
//                  memVO.setMembershipTypeCode(request.getParameter("membershipTypeCode"));
//                  memVO.setMembershipNumber(request.getParameter("membershipNumber"));
//                  memVO.setClientNumber(new Integer(request.getParameter("clientNumber")));
//                  memVO.setProductCode(request.getParameter("productCode"));
//                  memVO.setProductEffectiveDate( new DateTime(DateUtil.parseDate(request.getParameter("productEffectiveDate"))));
//                  memVO.setJoinDate( new DateTime(DateUtil.parseDate(request.getParameter("joinDate"))));
//                  memVO.setCommenceDate( new DateTime(DateUtil.parseDate(request.getParameter("commenceDate"))));
//                  memVO.setExpiryDate( new DateTime(DateUtil.parseDate(request.getParameter("expiryDate"))));
//                  memVO.setVehicleCount( new Integer(request.getParameter("vehicleCount")).intValue());
//                  memVO.setVehicleExpiryType( request.getParameter("vehicleExpiryType"));
//                  memVO.setStatus(request.getParameter("status"));
//
//               membership = membershipHome.findByPrimaryKey(new Integer(membershipID));
//
//               membership.(memVO);setFromVO

               out.print("saved membership data through VO<br>");
            }
            catch(Exception e)
            {
               out.print("Failed to update membership through VO<br>");
               out.print(e.getMessage());
            }
         }
         else if (updateMode.equals("3"))
         {
            try
            {

               MembershipVO memVO = new MembershipVO();

                  memVO.setMembershipTypeCode(request.getParameter("membershipTypeCode"));
                  memVO.setMembershipNumber(request.getParameter("membershipNumber"));
                  memVO.setClientNumber(new Integer(request.getParameter("clientNumber")));
                  memVO.setProductCode(request.getParameter("productCode"));
                  memVO.setProductEffectiveDate( new DateTime(DateUtil.parseDate(request.getParameter("productEffectiveDate"))));
                  memVO.setJoinDate( new DateTime(DateUtil.parseDate(request.getParameter("joinDate"))));
                  memVO.setCommenceDate( new DateTime(DateUtil.parseDate(request.getParameter("commenceDate"))));
                  memVO.setExpiryDate( new DateTime(DateUtil.parseDate(request.getParameter("expiryDate"))));
                  memVO.setVehicleCount( new Integer(request.getParameter("vehicleCount")).intValue());
                  memVO.setVehicleExpiryType( request.getParameter("vehicleExpiryType"));
                  memVO.setStatus(request.getParameter("status"));

              membershipMgr.createMembership(memVO);

               out.print("Created membership through VO<br>");
            }
            catch(Exception e)
            {
               out.print("Failed to create membership through VO<br>");
               out.print(e.getMessage());
            }
         }
      }
   }


%>

<html>
<head>
<title>Test Membership Update
</title>
</head>
<body>
<h1>Test Membership Update</h1>

<% if (membership == null){ %>

<form name="searchForm" method="post">
<br><br>
<Table border=0>
<tr>
<td>MembershipID</td><td><input type="text" name="searchMembershipID"></td>
</tr>
<tr>
<td></td><td><input type="submit" name="Submit" value="Submit"><input type="reset" value="Reset"></td>
</tr>
</table>
</form>

<% } else { %>

<form name="updateForm" method="post">
<input type="hidden" name="updateMode" value="1">
<table border=0>
<tr>
   <td>Membership ID</td>
   <td><input type="text" name="membershipID" value="<%=membership.getMembershipID()%>"></td>
</tr>
<tr>
   <td>Membership number</td>
   <td><input type="text" name="membershipNumber" value="<%=membership.getMembershipNumber()%>"></td>
</tr>
<tr>
   <td>Status</td>
   <td><input type="text" name="status" value="<%=membership.getStatus()%>"></td>
</tr>
<tr>
   <td>Client Number</td>
   <td><input type="text" name="clientNumber" value="<%=membership.getClientNumber()%>"></td>
</tr>
<tr>
   <td>Membership type</td>
   <td><input type="text" name="membershipTypeCode" value="<%=membership.getMembershipTypeCode()%>"></td>
</tr>
<tr>
   <td>Join date</td>
   <td><input type="text" name="joinDate" value="<%=membership.getJoinDate().formatShortDate()%>"></td>
</tr>
<tr>
   <td>Commence date</td>
   <td><input type="text" name="commenceDate" value="<%=membership.getCommenceDate().formatShortDate()%>"></td>
</tr>
<tr>
   <td>Expiry date</td>
   <td><input type="text" name="expiryDate" value="<%=membership.getExpiryDate().formatShortDate()%>"></td>
</tr>
<tr>
   <td>Product</td>
   <td><input type="text" name="productCode" value="<%=membership.getProductCode()%>"></td>
</tr>
<tr>
   <td>Product effective date</td>
   <td><input type="text" name="productEffectiveDate" value="<%=membership.getProductEffectiveDate().formatShortDate()%>"></td>
</tr>
<tr>
   <td>Vehicle count</td>
   <td><input type="text" name="vehicleCount" value="<%=membership.getVehicleCount()%>"></td>
</tr>
<tr>
   <td>Vehicle expiry type</td>
   <td><input type="text" name="vehicleExpiryType" value="<%=membership.getVehicleExpiryType()%>"></td>
</tr>
<tr>
   <td></td>
   <td><button type="button" name="btnDirectSubmit" onclick="document.all.updateForm.submit();">Submit direct</button>&nbsp;<button type="button" name="btnVOSubmit" onclick="document.all.updateForm.updateMode.value=2;document.all.updateForm.submit();">Submit through VO</button>&nbsp;<button type="button" name="btnVOSubmit" onclick="document.all.updateForm.updateMode.value=3;document.all.updateForm.submit();">Create new record</button></td>
</tr>
</table>
</form>
<% } %>
</body>
</html>
