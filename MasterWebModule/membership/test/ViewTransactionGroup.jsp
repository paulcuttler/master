<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.MembershipUIConstants"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%

   MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
   TransactionGroup transGroup = (TransactionGroup) request.getAttribute("transactionGroup");

   if (transGroup == null)
      throw new ServletException("The transaction is no longer valid. Please log in again.");

%>

<html>
<head>
<title>View Transaction Group</title>
</head>
<body>
<h1>View Transaction Group</h1>

<table border="1" cellpadding="3" cellspacing="0">
<%
Collection transList = transGroup.getTransactionList();
if (transList != null && !transList.isEmpty())
{
   Iterator transListIT = transList.iterator();
   while (transListIT.hasNext())
   {
      MembershipTransactionVO memTransVO = (MembershipTransactionVO) transListIT.next();
      MembershipVO memVO = memTransVO.getNewMembership();
      ClientVO clientVO = memVO.getClient();
%>
<tr>
   <td nowrap>Client: <%=clientVO.getClientNumber()%> <%=clientVO.getDisplayName()%></td>
   <td nowrap>PA?: <%=memTransVO.isPrimeAddressee()%></td>
   <td nowrap>Mem #: <%=memVO.getMembershipNumber()%></td>
   <td nowrap>Mem type: <%=memVO.getMembershipTypeCode()%></td>
   <td nowrap>Join date: <%=memVO.getJoinDate()%></td>
   <td nowrap>Commence date: <%=memVO.getCommenceDate()%></td>
   <td nowrap>Expiry date: <%=memVO.getExpiryDate()%></td>
   <td nowrap>Term: <%=memVO.getMembershipTerm()%></td>
   <td nowrap>Profile: <%=memVO.getMembershipProfileCode()%></td>
   <td nowrap>Status: <%=memVO.getBaseStatus()%></td>
   <td nowrap>Product: <%=memVO.getProductCode()%></td>
   <td nowrap>Product effective: <%=memVO.getProductEffectiveDate()%></td>
</tr>
<%
      Collection prodOptionList = memVO.getProductOptionList();
      if (prodOptionList != null)
      {
         Iterator prodOptionListIT = prodOptionList.iterator();
         while (prodOptionListIT.hasNext())
         {
            MembershipProductOption prodOption = (MembershipProductOption) prodOptionListIT.next();
%>
<tr>
   <td nowrap></td>
   <td nowrap colspan="12">Product option: <%=prodOption.getProductBenefitTypeCode()%></td>
</tr>
<%
         }
      }

      // Show each membership transaction fee for the membership transaction
      Collection memTransFeeList = memTransVO.getTransactionFeeList();
      if (memTransFeeList != null)
      {
         Iterator memTransFeeListIT = memTransFeeList.iterator();
         while (memTransFeeListIT.hasNext())
         {
            MembershipTransactionFee memTransFee = (MembershipTransactionFee) memTransFeeListIT.next();
%>
<tr>
   <td nowrap></td>
   <td nowrap>Fee: <%=memTransFee.getFeeSpecificationVO().getProductBenefitCode()%></td>
   <td nowrap>fee per member: <%=memTransFee.getFeePerMember()%></td>
   <td nowrap>fee per vehicle: <%=memTransFee.getFeePerVehicle()%></td>
   <td nowrap>vehicle count: <%=memTransFee.getVehicleCount()%></td>
   <td nowrap>gross fee: <%=memTransFee.getGrossFee()%></td>
   <td nowrap>total discount:<%=memTransFee.getTotalDiscount()%></td>
</tr>
<%
            // Show discounts for each memnership transaction fee.
            Collection discList = memTransFee.getDiscountList();
            if (discList != null)
            {
               Iterator discListIT = discList.iterator();
               while (discListIT.hasNext())
               {
                  MembershipTransactionDiscount memDisc = (MembershipTransactionDiscount) discListIT.next();
%>
<tr>
   <td nowrap></td>
   <td nowrap>Fee discount: <%=memDisc.getDiscountCode()%></td>
   <td nowrap><%=memDisc.getDiscountAmount()%></td>
</tr>
<%
               }
            }
         }
      }
   }
}

%>
</table>
</body>
</html>
