<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="java.io.StringWriter"%>
<%@ page import="java.util.ArrayList"%>

<%
String memIDText = request.getParameter("memID");
MembershipVO memVO = null;
if (memIDText != null)
{
   Integer memID = new Integer(memIDText);
//   MembershipHome memHome = MembershipEJBHelper.getMembershipHome();
MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
   memVO = membershipMgr.getMembership(memID);

}
if (memVO != null)
{
   ArrayList excludedClassList = new ArrayList();
   excludedClassList.add(Class.forName("com.ract.membership.ProductVO"));
   excludedClassList.add(Class.forName("com.ract.membership.ProductBenefitTypeVO"));
   excludedClassList.add(Class.forName("com.ract.membership.MembershipTypeVO"));
   StringWriter sw = new StringWriter();
   XMLClassWriter xmlcw = new XMLClassWriter(sw,null,excludedClassList);
   ClassWriter cw = (ClassWriter) xmlcw;
   xmlcw.startDocument();

   memVO.write(cw);

   out.print(sw);
   sw.close();
}
else
{
   out.print("Membership not found for memID = " + memIDText);
}
%>
