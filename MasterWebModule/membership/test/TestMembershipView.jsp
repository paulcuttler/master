<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>

<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>


<%

Collection membershipList = null;
MembershipVO membership = null;

MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
   String membershipID = request.getParameter("membershipID");

   if (membershipID != null){
     try
     {
        membership = membershipMgr.getMembership(new Integer(membershipID));
     }
     catch(Exception e)
     {
        out.print("Failed to get membership ID : " + membershipID + "<br>");
        out.print(e.getMessage());
     }
   }
   else{
     //not safe to list ALL memberships
//      try
//      {
//         membershipList = membershipHome.findAll();
//      }
//      catch(Exception e)
//      {
//         out.print("Failed get membership list.<br>");
//         out.print(e.getMessage());
//      }
   }


%>


<html>
<head>
<title>
TestMembershipView
</title>
</head>
<body>
<h1>Membership view test client</h1>
<%
   if (membership != null){
      out.print("<table border=1>");
         out.print("<tr>");
         out.print("<td>"+membership.getMembershipID()+"</td>");
         out.print("<td>"+membership.getClientNumber()+"</td>");
         out.print("<td>"+membership.getMembershipNumber()+"</td>");
         out.print("<td>"+membership.getMembershipTypeCode()+"</td>");
         out.print("<td>"+membership.getStatus()+"</td>");
         out.print("<td>"+membership.getJoinDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getCommenceDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getExpiryDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getProductCode()+"</td>");
         out.print("<td>"+membership.getProductEffectiveDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getVehicleCount()+"</td>");
         out.print("<td>"+membership.getVehicleExpiryType()+"</td>");
         out.print("</tr>");
      out.print("</table>");
   }
   else if (membershipList != null){
      out.print("<table border=1>");
      Iterator it = membershipList.iterator();
      while (it.hasNext()){
          membership = (MembershipVO) it.next();
         out.print("<tr>");
         out.print("<td>"+membership.getMembershipID()+"</td>");
         out.print("<td>"+membership.getClientNumber()+"</td>");
         out.print("<td>"+membership.getMembershipNumber()+"</td>");
         out.print("<td>"+membership.getMembershipTypeCode()+"</td>");
         out.print("<td>"+membership.getStatus()+"</td>");
         out.print("<td>"+membership.getJoinDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getCommenceDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getExpiryDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getProductCode()+"</td>");
         out.print("<td>"+membership.getProductEffectiveDate().formatShortDate()+"</td>");
         out.print("<td>"+membership.getVehicleCount()+"</td>");
         out.print("<td>"+membership.getVehicleExpiryType()+"</td>");
         out.print("</tr>");
      }
      out.print("</table>");
   }
%>
<form method="post">
<br><br>
<Table border=0>
<tr>
<td>MembershipID</td><td><input type="text" name="membershipID"></td>
</tr>
<tr>
<td></td><td><input type="submit" name="Submit" value="Submit"><input type="reset" value="Reset"></td>
</tr>
</table>
</form>
</body>
</html>
