
<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="com.ract.membership.*"%>

<html>
<head>
<title>TestResetCache</title>
</head>
<body>
<h1>Test Reset Cache</h1>

<%
MembershipRefMgr membershipRefMgr = null;

membershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();


    membershipRefMgr.resetDiscountTypeCache();
    membershipRefMgr.resetJoinReasonCache();
    membershipRefMgr.resetMembershipTransactionTypeCache();
    membershipRefMgr.resetMembershipTypeCache();
    membershipRefMgr.resetProductCache();
    membershipRefMgr.resetProductBenefitTypeCache();

%>

Done.
</body>
</html>
