<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.payment.bank.*"%>
<%@ page import="com.ract.payment.directdebit.*"%>
<%@ page import="com.ract.payment.receipting.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>

<%!
private void closeConnection(Connection con)
{
   try {con.close();}
   catch (Exception e) {}
}

private void closeStatement(PreparedStatement stmt)
{
   try {stmt.close();}
   catch (Exception e) {}
}
%>


<%
String transIDStr = request.getParameter("transactionID");
System.out.print("transactionID=" + transIDStr);
String payableItemIDStr = request.getParameter("payableItemID");
String paymentMethodDescription = request.getParameter("paymentMethod");
String paymentMethodIDStr = request.getParameter("paymentMethodID");
String btnRetrieve = request.getParameter("btnRetrieve");
String btnVerify = request.getParameter("btnVerify");


Integer payableItemID = null;
String paymentMethodID = null;

if (btnVerify != null)
{
   payableItemID = payableItemIDStr != null && payableItemIDStr.length() > 0 ? new Integer(payableItemIDStr) : null;
   paymentMethodID = paymentMethodIDStr;
}


CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
String systemName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,SystemParameterVO.TYPE_SYSTEM);


PayableItemVO payableItemVO = null;

PreparedStatement cadStatement = null;
PreparedStatement ocrStatement = null;
Connection  ractDevConnection = null;
Connection  develCltConnection = null;

try
{
// Load the jdbc driver classes
Class.forName("com.progress.sql.jdbc.JdbcProgressDriver");
Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");

if ("Development".equalsIgnoreCase(systemName)) {
   // Make a connection to the SQL Server database
   ractDevConnection =
         DriverManager.getConnection(
            "jdbc:microsoft:sqlserver://203.20.20.51:1433;databasename=ract_dev",
            "PUB","pub");

   // Make a connection to the Progress database
//   develCltConnection =
//         DriverManager.getConnection(
//            "jdbc:JdbcProgress:T:aston:9021:develclt",
//            "sysprogress","x");
}
else if ("Test".equalsIgnoreCase(systemName)) {
   // Make a connection to the SQL Server database
   ractDevConnection =
         DriverManager.getConnection(
            "jdbc:microsoft:sqlserver://203.20.20.51:1433;databasename=ract_test",
            "PUB","pub");

   // Make a connection to the Progress database
//   develCltConnection =
//         DriverManager.getConnection(
//            "jdbc:JdbcProgress:T:aston:9011:trainclt",
//            "server","server1");
}
else {
   throw new Exception("The verify undo page only works in the test and development environments.");
}


// Fetch the group of membership transactions
//MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
Collection transList = null;
if (transIDStr != null)
{
   transList = membershipMgr.findMembershipTransactionsByTransactionID(new Integer(transIDStr));
}


%>
<html>
<head>
<title>Verify Undo</title>
<link rel="stylesheet" href="/css/ract.css" type="text/css">
</head>
<body>
<form name="verifyUndoForm" method="post" action="/membership/test/VerifyUndo.jsp">
   <h1>View membership transactions - <%=(btnVerify != null ? "Verify" : "Retrieve")%> (<%=systemName%>)</h1>
   <table border="0" cellspacing="0" cellpadding="3">
      <tr>
         <td>Context Transaction ID:</td>
         <td>
            <input type="text" name="transactionID" size="10" value="<%=(transIDStr != null ? transIDStr : "")%>">
         </td>
         <td>
            <input type="submit" name="btnRetrieve" value="Retrieve">
         </td>
         <td>
            <input type="submit" name="btnVerify" value="Verify">
         </td>
      </tr>
   </table>
   <hr>
<%
if (transList != null)
{
   MembershipTransactionVO memTransVO;
   MembershipVO memVO;
   ClientVO clientVO;
   Iterator txIt = transList.iterator();
   while (txIt.hasNext())
   {
      memTransVO = (MembershipTransactionVO) txIt.next();
      memVO = memTransVO.getMembership();
      clientVO = memVO.getClient();
      if (clientVO != null)
      {
%>
   <table border="0" cellspacing="0" cellpadding="3">
      <tr>
         <td class="dataValue"><%=clientVO.getDisplayName()%></td>
         <td class="label">Delivery option:</td>
         <td class="dataValue"><%=clientVO.getMotorNewsSendOption()%></td>
         <td class="label">Delivery type:</td>
         <td class="dataValue"><%=clientVO.getMotorNewsDeliveryMethod()%></td>
      </tr>
   </table>
<%
      }
      else
      {
%>
   <table border="0" cellspacing="0" cellpadding="4">
      <tr>
         <td class="dataValue">----- No Client -----</td>
      </tr>
   </table>
<%
      }
%>
   <table border="0" cellspacing="0" cellpadding="3">
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Membership:</td>
         <td class="dataValue"><%=memVO.getMembershipNumber()%></td>
         <td class="label">Expire:</td>
         <td class="dataValue"><%=memVO.getExpiryDate().formatShortDate()%></td>
         <td class="label">Status:</td>
         <td class="dataValue"><%=memVO.getStatus()%></td>
         <td class="label">Product:</td>
         <td class="dataValue"><%=memVO.getProductCode()%></td>
         <td class="label">Effective:</td>
         <td class="dataValue"><%=memVO.getProductEffectiveDate().formatShortDate()%></td>
      </tr>
<%
      Collection memCardList = memVO.getMembershipCardList(true);
      if (memCardList != null && !memCardList.isEmpty())
      {
         Iterator memCardIterator = memCardList.iterator();
         while (memCardIterator.hasNext())
         {
            MembershipCardVO memCardVO = (MembershipCardVO) memCardIterator.next();
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label" nowrap>Membership card:</td>
         <td class="dataValue"><%=memCardVO.getCardID()%></td>
         <td class="label">Issuance:</td>
         <td class="dataValue"><%=memCardVO.getIssuanceNumber()%></td>
         <td class="label">Requested:</td>
         <td class="dataValue"><%=memCardVO.getRequestDate().formatLongDate()%></td>
         <td class="label">Issued:</td>
         <td class="dataValue"><%=(memCardVO.getIssueDate() != null ? memCardVO.getIssueDate().formatLongDate() : "")%></td>
         <td class="label">Expire:</td>
         <td class="dataValue"><%=(memCardVO.getExpiryDate() != null ? memCardVO.getExpiryDate().formatLongDate() : "")%></td>
      </tr>
<%
         }
      }
      else
      {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label" nowrap>Membership card:</td>
         <td class="dataValue" colspan="4">----- No membership cards -----</td>
      </tr>
<%
      }

      // Display the CAD Export details
      String sql = "select \"member-type\", \"expiry-date\", "
      + "\"service-level\", \"membership-years\", \"effective_date\", "
      + "\"deleted\", \"membership-hold\", \"remove-record\", "
      + "\"plus-expiry\", \"plus-commence-date\", \"plus-flag\" "
      + "from PUB.\"cad-export\" "
      + "where \"client-no\" = ?";
      cadStatement = develCltConnection.prepareStatement(sql);
      cadStatement.setInt(1,memVO.getClientNumber().intValue());
      ResultSet rs = cadStatement.executeQuery();
      if (rs.next()) {

         do {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">CAD member type:</td>
         <td class="dataValue"><%=rs.getString(1)%></td>
         <td class="label">Expiry date:</td>
         <td class="dataValue"><%=rs.getString(2)%></td>
         <td class="label">Service level:</td>
         <td class="dataValue"><%=rs.getString(3)%></td>
         <td class="label">Membership years:</td>
         <td class="dataValue"><%=rs.getInt(4)%></td>
         <td class="label">Effective date:</td>
         <td class="dataValue"><%=rs.getDate(5)%></td>
         <td class="label">Deleted:</td>
         <td class="dataValue"><%=rs.getString(6)%></td>
      </tr>
      <tr>
         <td width="10">&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td class="label">Hold:</td>
         <td class="dataValue"><%=rs.getString(7)%></td>
         <td class="label">Remove:</td>
         <td class="dataValue"><%=rs.getString(8)%></td>
         <td class="label">Plus expiry:</td>
         <td class="dataValue"><%=rs.getString(9)%></td>
         <td class="label">Plus commence:</td>
         <td class="dataValue"><%=rs.getString(10)%></td>
         <td class="label">Plus flag:</td>
         <td class="dataValue"><%=rs.getString(11)%></td>
      </tr>
<%
         }
         while (rs.next());

      } else {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">CAD:</td>
         <td class="dataValue" colspan="4">---  No records ---</td>
      </tr>
<%
      }

      // Display the transaction details
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Transaction:</td>
         <td class="dataValue">
            <a class="actionItemBlack" href="#" title="transaction group ID = <%=memTransVO.getTransactionGroupID() %>">
               <%=memTransVO.getTransactionID()%>
            </a>
         </td>
         <td class="label">Type:</td>
         <td class="dataValue"><%=memTransVO.getTransactionTypeCode()%></td>
         <td class="label">Group type:</td>
         <td class="dataValue"><%=memTransVO.getTransactionGroupTypeCode()%></td>
         <td class="label">Date:</td>
         <td class="dataValue"><%=memTransVO.getTransactionDate().formatLongDate()%></td>
      </tr>
      <tr>
         <td width="10">&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td class="label">Undone by:</td>
         <td class="dataValue"><%=memTransVO.getUndoneUsername()%></td>
         <td class="label">Undone date:</td>
         <td class="dataValue"><%=(memTransVO.getUndoneDate() != null ? memTransVO.getUndoneDate().formatLongDate() : "")%></td>
         <td class="label">PayItemID:</td>
         <td class="dataValue"><%=memTransVO.getPayableItemID()%></td>
      </tr>
<%
      // Retrieve the payable item referenced by the transaction if we are
      // not verifying.
      if (btnVerify == null)
         payableItemID = memTransVO.getPayableItemID();

      if (payableItemID != null)
      {
         try {

            payableItemVO = PaymentEJBHelper.getPaymentMgr().getPayableItem(payableItemID);
         } catch (Exception e) { payableItemVO = null; }
      }
      else
      {
         payableItemVO = null;
      }

      if (payableItemID ==  null
      &&  payableItemVO == null)
      {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Payable item:</td>
         <td class="dataValue" colspan="4">-----  No Payable item -----</td>
      </tr>
<%
      }
      else if (payableItemID != null
      &&       payableItemVO == null)
      {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Payable item:</td>
         <td class="dataValue"><%=payableItemID%></td>
         <td class="dataValue" colspan="4">-----  Not found -----</td>
      </tr>
<%
      }
      else
      {
         // If we are retrieving then get the payment method details
         // from the retrieved payable item.
         if (btnVerify == null)
         {
            paymentMethodDescription = payableItemVO.getPaymentMethodDescription();
            paymentMethodID = payableItemVO.getPaymentMethodID();
         }
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Payable item:</td>
         <td class="dataValue"><%=payableItemVO.getPayableItemID()%></td>
         <td class="label">Payable:</td>
         <td class="dataValue"><%=NumberUtil.roundDouble(payableItemVO.getAmountPayable(),2)%></td>
         <td class="label">Outstanding:</td>
         <td class="dataValue"><%=NumberUtil.roundDouble(payableItemVO.getAmountOutstanding(),2)%></td>
         <td class="label">SS ref:</td>
         <td class="dataValue"><%=payableItemVO.getSourceSystemReferenceID()%></td>
         <td class="label">PayMeth:</td>
         <td class="dataValue"><%=payableItemVO.getPaymentMethodDescription()%></td>
         <td class="label">PayMethID:</td>
         <td class="dataValue"><%=payableItemVO.getPaymentMethodID()%></td>
      </tr>
<%
      }

      if (ReceiptingPaymentMethod.RECEIPTING.equalsIgnoreCase(paymentMethodDescription))
      {
         if (paymentMethodID != null)
         {
            // Fetch the oi-payable data
            ocrStatement = develCltConnection.prepareStatement("select \"prod-code\", amount, \"os-amt\", description, \"create-date\" from PUB.\"oi-payable\" where \"client-no\" = ? and \"seq-no\" = ?");
            ocrStatement.setInt(1,clientVO.getClientNumber().intValue());
            ocrStatement.setInt(2,Integer.parseInt(paymentMethodID));
            ResultSet oiPayableRS = null;
            String message = "";
            try
            {
               oiPayableRS = ocrStatement.executeQuery();
            }
            catch (Exception e)
            {
               oiPayableRS = null;
               message = e.getMessage();
            }

            if (oiPayableRS != null && oiPayableRS.next())
            {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Receipting:</td>
         <td class="dataValue"><%=oiPayableRS.getString(1)%></td>
         <td class="label">Amount:</td>
         <td class="dataValue"><%=oiPayableRS.getDouble(2)%></td>
         <td class="label">Outstanding:</td>
         <td class="dataValue"><%=oiPayableRS.getDouble(3)%></td>
         <td class="label">Desc:</td>
         <td class="dataValue"><%=oiPayableRS.getString(4)%></td>
         <td class="label">Created:</td>
         <td class="dataValue"><%=oiPayableRS.getDate(5)%></td>
      </tr>
<%
            }
            else
            {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Receipting:</td>
         <td class="dataValue" colspan="4"><%=clientVO.getClientNumber()%>,<%=paymentMethodID%></td>
         <td class="dataValue" colspan="4">----- No oi-payable record -----</td>
         <td class="label"><%=message%></td>
      </tr>
<%
            }
         }
         else
         {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Receipting:</td>
         <td class="dataValue" colspan="4">----- No oi-payable record -----</td>
      </tr>
<%
         }
      }

      if (PaymentMethod.DIRECT_DEBIT.equalsIgnoreCase(paymentMethodDescription)
      && paymentMethodID != null)
      {

         // Try and get the direct debit schedule and authority
         DirectDebitAuthority ddAuthority = null;
         DirectDebitSchedule ddSchedule = null;
         try{
            DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
            ddSchedule = ddAdapter.getSchedule(new Integer(paymentMethodID));
            if (ddSchedule != null) {
               ddAuthority = ddAdapter.getAuthority(ddSchedule.getDirectDebitAuthorityID());
            }
         } catch (Exception e) { ddSchedule = null; }

         if (ddAuthority != null)
         {
            BankAccount bankAccount = ddAuthority.getBankAccount();
%>
      <tr>
         <td>&nbsp;</td>
         <td class="label" nowrap>Direct debit:</td>
         <td class="dataValue"><%=paymentMethodID%></td>
         <td class="label" nowrap>Type:</td>
         <td class="dataValue"><%=bankAccount.getAccountType()%></td>
         <td class="label">Frequency:</td>
         <td class="dataValue" nowrap><%=ddAuthority.getDeductionFrequency().getDescription()%></td>
         <td class="label">Day to debit:</td>
         <td class="dataValue"><%=ddAuthority.getDayToDebit()%></td>
<!--
        <td class="label">DDR Date:</td>
        <td class="dataValue"><%=ddAuthority.getDdrDate().formatShortDate()%></td>
-->
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
<%
            if (bankAccount instanceof CreditCardAccount)
            {
               CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
%>
         <td class="label" nowrap>Card type:</td>
         <td class="dataValue" nowrap><%=ccAccount.getCardTypeDescription()%></td>
         <td class="label">Card num:</td>
         <td class="dataValue"><%=ccAccount.getCardNumber()%></td>
         <td class="label">Expiry:</td>
         <td class="dataValue"><%=ccAccount.getCardExpiry()%></td>
<%
            }
            else
            {
               DebitAccount debitAccount = (DebitAccount) bankAccount;
%>
         <td class="label">BSB:</td>
         <td class="dataValue"><%=debitAccount.getBsbNumber()%></td>
         <td class="label">Acc num:</td>
         <td class="dataValue"><%=debitAccount.getAccountNumber()%></td>
<%
            }
%>
      </tr>
<%
            if (ddSchedule != null
            &&  ddSchedule.getScheduledItemCount() > 0)
            {
               DirectDebitScheduleItem scheduleItem;
               for (int itemIndex = 0; itemIndex < ddSchedule.getScheduledItemCount(); itemIndex++)
               {
                  scheduleItem = (DirectDebitScheduleItem) ddSchedule.getScheduleItem(itemIndex);
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Scheduled for:</td>
         <td class="dataValue" nowrap><%=DateUtil.formatShortDate(scheduleItem.getScheduledDate()) %></td>
         <td class="label">Amount:</td>
         <td class="dataValue"><%=scheduleItem.getAmount() %></td>
         <td class="label">Status:</td>
         <td class="dataValue"><%=scheduleItem.getStatus().getDescription() %></td>
         <td class="label">Processed:</td>
         <td class="dataValue"><%=DateUtil.formatShortDate(scheduleItem.getProcessedDate()) %></td>
      </tr>
<%
               }
            }
            else
            {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Schedule:</td>
         <td class="dataValue" colspan="4">-----  No direct debit schedule -----</td>
      </tr>
<%
            }
         }
         else
         {
%>
      <tr>
         <td width="10">&nbsp;</td>
         <td class="label">Direct debit:</td>
         <td class="dataValue" colspan="4">-----  Direct debit not found -----</td>
      </tr>
<%
         }
      }
%>
   </table>
<%
   }
}
else
{
   out.print("A transaction has not been selected.");
}

}
finally
{
   closeStatement(cadStatement);
   closeStatement(ocrStatement);
   closeConnection(ractDevConnection);
   closeConnection(develCltConnection);
}
%>
   <input type="hidden" name="payableItemID" value="<%=(payableItemID != null ? payableItemID.toString() : "")%>">
   <input type="hidden" name="paymentMethod" value="<%=(paymentMethodDescription != null ? paymentMethodDescription : "")%>">
   <input type="hidden" name="paymentMethodID" value="<%=(paymentMethodID != null ? paymentMethodID.toString() : "")%>">
</form>
</body>
</html>
