<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.common.CommonConstants"%>

<%
MembershipVO mem = null;
Collection memList = null;
%>
<html>
<head>
<title>TestMembershipWriterList</title>
</head>
<body>
<h1>Test Membership Writer List</h1>
<%
if (memList != null)
{
%>
   <table border="0" cellpadding="5">
<%
   ClientVO clientVO = null;
   Iterator memListIT = memList.iterator();
   while (memListIT.hasNext())
   {
      mem = (MembershipVO) memListIT.next();
      clientVO = mem.getClient();
%>
   <tr>
      <td><a href="<%=CommonConstants.DIR_MEMBERSHIP + "/test/TestMembershipWriter.jsp?memID="%><%=mem.getMembershipID()%>"><%=mem.getMembershipID()%> - <%=(clientVO == null ? "" : clientVO.getDisplayName())%></a></td>
   </tr>
<%
   }
%>
   </table>
<%
}
%>
</body>
</html>
