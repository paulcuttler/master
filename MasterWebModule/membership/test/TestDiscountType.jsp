
<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>

<%
MembershipRefMgr membershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();
%>

<html>
<head>
<title>
TestDiscountType
</title>
</head>
<body>
<h1>Discount types:</h1>
<%
   ArrayList discountTypeList = null;

   try
   {
      discountTypeList = new ArrayList(membershipRefMgr.getDiscountTypeList());
   }
   catch(Exception e)
   {
      out.print("Failed retrieving discount type list.<br>");
      out.print(e.getMessage());
   }
   if (discountTypeList != null)
   {
      out.print("<table border=1>");
      Iterator it = discountTypeList.iterator();
      while (it.hasNext()){
         DiscountTypeVO tmp = (DiscountTypeVO) it.next();
         DiscountTypeVO tmp2 = membershipRefMgr.getDiscountType(tmp.getDiscountTypeCode());
         out.print("<tr>");
         out.print("<td>" + tmp.getDiscountTypeCode() + "</td><td>"
                          + tmp.getDescription() + "</td><td>"
                          + tmp.isSystemControlled() + "</td><td>"
                          + tmp.isAutomaticDiscount() + "</td><td>"
                          + tmp.isRecurringDiscount() + "</td><td>"
                          + tmp.isMinimumFeeApplicable() + "</td><td>"
                          + tmp.getStatus() + "</td><td>"
                          + tmp.getDiscountAmount() + "</td><td>"
                          + tmp.getDiscountPercentage() + "</td><td>"
                          + (tmp.getDiscountPeriod() != null ? tmp.getDiscountPeriod().toString() : "null") + "</td><td>"
                          + tmp.getDiscountAccountID() + "</td><td>"
                          + tmp.getEffectiveFromDate() + "</td><td>"
                          + tmp.getEffectiveToDate() + "</td><td>");
         out.print("</tr>");
      }
      out.print("</table>");
   }
%>
</body>
</html>
