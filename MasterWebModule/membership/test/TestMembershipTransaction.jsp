<%@ page import="java.io.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%!
public void printErrorMessage(Exception e,javax.servlet.jsp.JspWriter out)
{
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        try
        {
          out.print("<pre>");
          out.print(sw.toString());
          out.print("</pre>");
        }
        catch (IOException ioe)
{}
}
%>

<%
DateTime dt = null;
try
{
dt = new DateTime();
log("date is "+dt.formatLongDate());
}
catch(Exception e){}

TimeZone tz = TimeZone.getDefault();
boolean dl = tz.inDaylightTime((Date)dt);
log("dn "+tz.getDisplayName());
log("d1 "+dl);
log("id "+tz.getID());
log("use dl "+tz.useDaylightTime());

/**
* @todo move to the JUnit test structure.
*/
//MembershipHome membershipHome = null;
MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
Collection membershipList = null;
//Membership membership = null;
String txType = request.getParameter("txType");

   String membershipNumber = request.getParameter("membershipNumber");
   MembershipVO memVO = null;

   Collection txList = null;

   if (membershipNumber != null)
   {
     try
     {
        memVO = (MembershipVO)membershipMgr.findMembershipByClientNumber(new Integer(membershipNumber)).iterator().next();
//        memVO = membership.getVO();

        txList = memVO.getCurrentTransactionList();
     }
     catch(Exception e)
     {
        printErrorMessage(e,out);
     }
   }
%>

<html>
<head>
<title>
TestMembershipView
</title>
</head>
<body>
<h1>Membership View Transactions 5</h1>

<form method="post">
<br><br>
<Table border=0>
<tr>
<td>Membership Number</td><td><input type="text" name="membershipNumber"></td>
</tr>
<tr>
<td>Transaction Type</td><td><select name="txType">
<option value="<%=MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD%>"><%=MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD%></option>
<option value="<%=MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL%>"><%=MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL%></option>
</select>
</td>
</tr>
<tr>
<td></td><td><input type="submit" name="Submit" value="Submit"><input type="reset" value="Reset"></td>
</tr>
</table>
</form>

<%
out.println("<p>"+membershipNumber+"</p>");
out.println("<p>"+txType+"</p>");
if (txList != null && txList.size() > 0)
{
  MembershipTransactionVO memTransVO = null;
  Iterator it = txList.iterator();
  out.print("<table border=1>");
  out.print("<tr>");
  out.print("<td class=\"headerRow\" colspan=\"7\">Transactions affecting membership value</td>");
  out.print("</tr>");
  while (it.hasNext())
  {
    memTransVO = (MembershipTransactionVO)it.next();
    out.print("<tr>");
    out.print("<td>"+memTransVO.getTransactionDate()+"</td>");
    out.print("<td>"+memTransVO.getTransactionTypeCode()+"</td>");
    out.print("<td>"+memTransVO.getTransactionGroupTypeCode()+"</td>");
    out.print("<td>"+memTransVO.getProductCode()+"</td>");
    out.print("<td>"+memTransVO.getAmountPayable()+"</td>");
    out.print("<td>"+memTransVO.getEffectiveDate()+"</td>");
    out.print("<td>"+memTransVO.getEndDate()+"</td>");
    out.print("</tr>");
  }
  out.print("</table>");

  try
  {
    UnearnedValue uValue = memVO.getPaidRemainingMembershipValue();
    out.println("<table>");
    out.print("<tr class=\"headerRow\">");
    out.print("<td><a title=\""+uValue.getDescriptionForHTMLAnchor()+"\">"+uValue.toString()+"</a></td>");
    out.print("</tr>");
    out.print("<tr>");
    out.print("<td>"+CurrencyUtil.formatDollarValue(uValue.getTransactionCreditAmount())+"</td>");
    out.print("</tr>");
    out.println("</table>");
  }
  catch (Exception e)
  {
    printErrorMessage(e,out);
  }

  //dummy user
  User us = new User("jyh","test","test","test","test","test@test.com",0,null,null);
  TransactionGroup transGroup = null;
  if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(txType))
  {
    transGroup = TransactionGroupFactory.getTransactionGroupForCancel(us);
  transGroup.setMembershipType(memVO.getMembershipType());

  // Add the clients membership being cancelled to the transaction
  // group. This will also add the other members of the membership group
  // that they might belong to.
  transGroup.addSelectedClient(memVO);
  transGroup.setContextClient(memVO);

  // Create transactions for all selected clients
  transGroup.createTransactionsForSelectedClients();
  }
  else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(txType))
  {
    transGroup = TransactionGroupFactory.getTransactionGroupForHold(us);

    transGroup.setMembershipType(memVO.getMembershipType());
    transGroup.addSelectedClient(memVO);
    transGroup.setContextClient(memVO);
    transGroup.setSelectedProduct(memVO.getProduct());

  transGroup.calculateTransactionFee();

    MembershipTransactionVO mTransVO = (MembershipTransactionVO) transGroup.getMembershipTransactionForClient(transGroup.getContextClient().getClientNumber());
    if (!(mTransVO instanceof MemTransHold)) {
       throw new SystemException("The context client (clientID="+ memTransVO.getMembership().getClientNumber() +") transaction type ("+ memTransVO.getTransactionTypeCode() +") has not been set correctly. The MembershipTransactionVO should be an instance of MemTransHold.");
    }

    if (mTransVO == null)
    {
       throw new SystemException("Trans is null.");
    }
    else
    {
      LogUtil.log(this.getClass(),"class"+mTransVO.getClass().getName());
    }
    MemTransHold holdTransaction = (MemTransHold) mTransVO;
    holdTransaction.setHoldPeriod(memVO.getExpiryDate().add(new Interval(1,0,0,0,0,0,0)),
                                  1,
                                  new Interval(transGroup.getTransactionDate(),memVO.getExpiryDate()));
  }
  else
  {
    return;
  }

  transGroup.calculateTransactionFee();

  MembershipTransactionVO txVO = null;
  Collection transList = transGroup.getTransactionList();
  int counter = 0;
  if (transList != null && transList.size() > 0)
  {
    Iterator txIt = transList.iterator();
    while (txIt.hasNext())
    {
      counter++;
      txVO = (MembershipTransactionVO)txIt.next();
      txVO.setTransactionID(new Integer(counter));
      LogUtil.log(this.getClass(),txVO.toString());
    }
  }

  Collection c = null;
  try
  {
    c = ((PayableItemGenerator)transGroup).generatePayableItemList();
  }
  catch (Exception e)
  {
    printErrorMessage(e,out);
  }
  PayableItemVO piVO = null;
  PayableItemComponentVO picVO = null;
  PayableItemPostingVO pipVO = null;
  Collection pics = null;
  Collection pips = null;
  Iterator picIt = null;
  Iterator pipIt = null;
  if (c != null && c.size() > 0)
  {
    out.println("<table border=\"1\">");
    out.println("<tr>");
    out.println("<td class=\"headerRow\" colspan=\"6\">Postings</td>");
    out.println("</tr>");
    Iterator itP = c.iterator();
    while (itP.hasNext())
    {
      piVO = (PayableItemVO)itP.next();
      //new one only
      if (piVO.getPayableItemID() != null)
      {
        continue;
      }
      pics = piVO.getComponents();
      picIt = pics.iterator();
      {
        picVO = (PayableItemComponentVO)picIt.next();
        pips = picVO.getPostings();
        pipIt = pips.iterator();
        while (pipIt.hasNext())
        {
          pipVO = (PayableItemPostingVO)pipIt.next();
          out.println("<tr>");
          out.println("<td>"+pipVO.getPostAtDate()+"</td>");
          out.println("<td>"+pipVO.getAccountNumber()+"</td>");
          out.println("<td>"+pipVO.getAmount()+"</td>");
          out.println("<td>"+pipVO.getIncomeAccountNumber()+"</td>");
          out.println("<td>"+pipVO.getDescription()+"</td>");
          out.println("</tr>");
        }
      }
    }
    out.println("</table>");
  }

}
%>

</body>
</html>
