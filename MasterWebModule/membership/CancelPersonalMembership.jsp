<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<%
MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");
ClientVO theClientVO = memVO.getClient();
// Attach the client to the request so the ViewClientInclude.jsp can get it.
request.setAttribute("client",theClientVO);

MembershipVO newPrimeAddressee = (MembershipVO) request.getAttribute("newPrimeAddressee");
MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
Collection cancelReasonList = refMgr.getCancelReasonList();
%>
<html>
<head>
<title>Cancel Membership</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">
function checkReason()
{
  var reason = document.all.cancelReason.options[document.all.cancelReason.selectedIndex].value;
  //default refused and dishonoured payments
  if (reason == 'Refused' || reason == 'HON')
  {
    //default check box
    document.all.blockTransactions.value = "true";
    showBlockReason();
  }
  else
  {
    document.all.blockTransactions.value = "false";
    showBlockReason();
  }
}

function showBlockReason()
{
  if (document.all.blockTransactions.value=="true")
  {
    document.all.blockReason.style.display="";
  }
  else
  {
    document.all.blockReason.style.display="none";
  }
}

function submitForm()
{
  if (document.all.blockTransactions.value == "true" &&
      document.all.blockReasonText.value == '')
  {
    alert('A block reason must be entered if blocking transactions.');
    return false;
  }

  //check type of note
  document.forms[0].submit();
}
</script>


</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
<form name="cancelForm" method="post" action="<%= MembershipUIConstants.PAGE_MembershipUIC%>" >
<input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
<input type="hidden" name="primeAddresseeMemID" value="<%=(newPrimeAddressee == null ? "" : newPrimeAddressee.getMembershipID().toString())%>">
<input type="hidden" name="event" value="cancelMembership_btnSave">
<input type="hidden" name="blockTransactions" value="false">
<tr valign="top">
  <td>
     <h1><%@include file="/help/HelpLinkInclude.jsp"%> Cancel <%=memVO.getMembershipTypeCode()%> Membership</h1>

       <%@ include file="/client/ViewClientInclude.jsp"%>

       <table>
       <tr>
         <td class="label">Membership number:</td>
         <td class="dataValue"><%=memVO.getMembershipNumber()%></td>
       </tr>
       <tr>
         <td class="label">Status:</td>
         <td class="dataValue"><%=MembershipUIHelper.formatMembershipStatusHTML(memVO.getStatus())%></td>
       </tr>
       <tr>
         <td class="label">Product:</td>
         <td class="dataValue"><%=memVO.getProductCode()%></td>
       </tr>
       <tr>
         <td class="label">Join date:</td>
         <td class="dataValue">
               <%=(memVO.getJoinDate() == null ? "" : memVO.getJoinDate().formatShortDate())%>
               &nbsp;&nbsp;
               <%=memVO.getMembershipYears().formatForDisplay(Interval.FORMAT_YEAR,Interval.FORMAT_YEAR)%>
         </td>
       </tr>
       <tr>
         <td class="label">Commence date:</td>
         <td class="dataValue"><%=(memVO.getCommenceDate() == null ? "" : memVO.getCommenceDate().formatShortDate())%></td>
       </tr>
       <tr>
         <td class="label">Expiry date:</td>
         <td class="dataValue"><%=(memVO.getExpiryDate() == null ? "" : memVO.getExpiryDate().formatShortDate())%></td>
       </tr>
       <tr>
         <td class="label">Estimated credit:</td>
         <td class="dataValue" nowrap>
         <%
         UnearnedValue memUnearnedValue = memVO.getPaidRemainingMembershipValue();
         %>
         <a title="<%=memUnearnedValue.getDescriptionForHTMLAnchor()%>"><%=memUnearnedValue.formatForDisplay()%></a>
         </td>
       </tr>

       <tr>
         <td class="label">Amount Outstanding:</td>
         <td class="dataValue"><%=memVO.getIndividualAmountOutstanding().formatForDisplay()%></td>
       </tr>

       <tr>
            <td colspan="2">
              <hr>
            </td>
       </tr>
<%
if (newPrimeAddressee != null)
{
%>
       <tr>
            <td class="label" valign="top">New Prime Addressee:</td>
            <td class="dataValue"><%=newPrimeAddressee.getClient().getDisplayName()%></td>
       </tr>
<%
}
%>
       <tr>
            <td class="Label">Cancellation reason:</td>
            <td>
              <select name="cancelReason" onchange="checkReason()">
<%

    // Select Box for Affliated CLubs
Iterator cancelReasonIterator = cancelReasonList.iterator();

while ( cancelReasonIterator.hasNext() )
{
   ReferenceDataVO cancelReason = (ReferenceDataVO) cancelReasonIterator.next();
   String reasonCode = cancelReason.getReferenceDataPK().getReferenceCode();

   // Can only select the deceased reason if the client has a status of deceased.
   if (((reasonCode.equalsIgnoreCase(ReferenceDataVO.MEMCANCELREASON_DECEASED)
      && Client.STATUS_DECEASED.equalsIgnoreCase(theClientVO.getStatus()))
    || !reasonCode.equalsIgnoreCase(ReferenceDataVO.MEMCANCELREASON_DECEASED))
    && cancelReason.isActive() )
   {
%>
                <option value="<%=reasonCode%>"><%=cancelReason.getDescription()%></option>
<%
   }
}
%>
              </select>
            </td>
          </tr>
          <!-- show on check above -->
          <tr id="blockReason" valign="top" style="display:none">
            <td>Transaction block reason:</td>
            <td><textarea name="blockReasonText" cols="50" rows="5"></textarea></td>
          </tr>
     </table>
   </td>
</tr>
<tr valign="bottom">
  <td>
    <table>
    <tr>
      <td>
        <input type="button" onclick="submitForm()" value="Save"/>
      </td>
    </tr>
    </table>
  </td>
</tr>
</form>
</table>
</body>
</html>
