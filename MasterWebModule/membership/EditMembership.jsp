<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.user.UserSession"%>
<%@ page import="com.ract.util.*"%>
<%
TransactionGroup transGroup = (TransactionGroup) request.getAttribute("transactionGroup");
if (transGroup == null)
{
   throw new SystemException("Failed to get transaction group from request.");
}

UserSession userSession = UserSession.getUserSession(request.getSession());

if (userSession == null)
{
   throw new SystemException("Failed to get user session from transaction group");
}

if (!userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_EDIT))
{
   throw new com.ract.common.SecurityException("You do not have the privilege to edit membership data.");
}

ClientVO clientVO = transGroup.getContextClient();
MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(clientVO.getClientNumber());
MembershipVO oldMemVO = contextMemTransVO.getMembership();
MembershipVO newMemVO = contextMemTransVO.getNewMembership();

if (newMemVO == null)
{
   throw new SystemException("Failed to get context client membership.");
}
// Got a valid membership. Now see if the product is current.
String productCode = newMemVO.getProductCode();
DateTime productEffectiveDate = newMemVO.getProductEffectiveDate();

String clientName = "";
String postalAddress = "";
String streetAddress = "";
String clientStatus = null;
String motorNewsSendOption = "";
if (clientVO != null)
{
   clientName = clientVO.getDisplayName();
   clientName = (clientName == null ? "unknown" : clientName);
   postalAddress = (clientVO.getPostalAddress() == null ? "" : clientVO.getPostalAddress().getSingleLineAddress());
   streetAddress = (clientVO.getResidentialAddress() == null ? "" : clientVO.getResidentialAddress().getSingleLineAddress());
   motorNewsSendOption = clientVO.getMotorNewsSendOption();
}

MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
Collection cancelReasonList = refMgr.getCancelReasonList();
%>

<%@include file="/common/NoCache.jsp"%>

<html>
<head>
<title>Edit membership</title>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<%=CommonConstants.getRACTStylesheet()%>

<script language="JavaScript">
   function submitForm(eventName)
   {
      document.all.event.value = eventName;
      var valid = validateForm();
      if (valid)
        document.all.editMembershipForm.submit();
   }

   function removeDiscount(discountCode)
   {
      document.all.removedDiscountTypeCode.value=discountCode;
      submitForm('editMembership_RemoveButton');
   }


   var reSpace  = / /gi;
   var reHyphen = /-/gi;

   function validateDate(fld)
   {
      var dateValue = fld.value.replace(reSpace,"");
      if (dateValue == '')
      {
         alert('The date must be entered.');
         setFocus(fld);
         return false;
      }
      // Make sure all of the fields are valid numbers
      var dateElements = dateValue.split('/');
      var day = new Number(dateElements[0]);
      var month = new Number(dateElements[1]);
      var year = new Number(dateElements[2]);
      if (isNaN(day) || isNaN(month) || isNaN(year))
      {
         alert("The date is not valid. Please try again.");
         setFocus(fld);
         return false;
      }

      if (year < 1900)
      {
         alert("The dates year is to early. Please try again.");
         setFocus(fld);
         return false;
      }

      // Do some more rigourous checking of the date.
      return checkdate(fld);
   }

   function validateForm()
   {
      var retval = true;

      if (document.all.event.value != 'editMembership_CancelButton')
      {
         retval = validateDate(document.all.joinDate);
      }
      return retval;
   }

</script>
</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/CancelButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/AddButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/RemoveButton_over.gif');">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
   <form name="editMembershipForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
   <input type="hidden" name="<%=TransactionGroup.KEY%>" value="<%=transGroup.getTransactionGroupKey()%>">
   <input type="hidden" name="event">
   <input type="hidden" name="removedDiscountTypeCode">
      <tr>
         <td valign="top">

           <h1><%@ include file="/help/HelpLinkInclude.jsp"%> Edit membership</h1>

            <table border="0" cellpadding="0" cellspacing="3">
               <tr>
                  <td class="label">Client:</td>
                  <td>&nbsp;</td>
                  <td> <a class="actionItemBlack" href="#" title="<%=streetAddress%>">
                     <span class="dataValue"><%=clientName%></span> <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5" border="0">
                     <span class="label">Number: </span> <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                     <span class="dataValue"><%=newMemVO.getClientNumber()%> </span>
                     </a> </td>
               </tr>
               <tr>
                  <td class="label">Membership:</td>
                  <td>&nbsp;</td>
                  <td> <a  class="actionItemBlack" title="ID = <%=(newMemVO.getMembershipID() == null ? "" : newMemVO.getMembershipID().toString())%>">
                     <span class="dataValue"><%=(newMemVO.getMembershipNumber() == null ? "" : newMemVO.getMembershipNumber())%></span>
                     <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                     <span class="dataValue"><%=productCode%></span> <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                     <span class="dataValue"><%=MembershipUIHelper.formatMembershipStatusHTML(newMemVO.getStatus())%></span>
                     <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5" border="0">
                     <span class="label">Expire:</span> <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                     <span class="dataValue"><%=MembershipUIHelper.formatMembershipExpiryDateHTML(newMemVO.getExpiryDate(),null)%></span>
                     </a> </td>
               </tr>
               <tr>
                  <td colspan="3" height="10">&nbsp;</td>
               </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="3">
<%--  Changing of profile implemented RLG 29-01-03. --%>
               <tr>
                  <td class="label">Profile:</td>
                  <td>&nbsp;</td>
                  <td class="dataValue">
<%


if ( userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CHANGE_PROFILE))
{

   Collection profileList = refMgr.getMembershipProfileList();
   if (profileList != null)
   {
    %>
       <select name="selectedProfileCode" onchange="submitForm('editMembership_ChangeProfile'); return true;">
    <%
      String profileCode = newMemVO.getMembershipProfileCode();
      String selected;
      MembershipProfileVO memProfileVO;
      Iterator profileListIterator = profileList.iterator();
      while (profileListIterator.hasNext())
      {
         memProfileVO = (MembershipProfileVO) profileListIterator.next();

         if ( (memProfileVO.isAdminOnly() &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN))
            || !memProfileVO.isAdminOnly() )
         {
            selected = memProfileVO.getProfileCode().equalsIgnoreCase(profileCode) ? " selected" : "";
            out.println("<option"+selected+">"+memProfileVO.getProfileCode());
         }
      }
   }
%>
      </select>
<%
}
else
{
%>

      <%=newMemVO.getMembershipProfileCode()%>
<%
}
%>

                  </td>
               </tr>

               <tr>
                  <td class="label">Join date:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="joinDate" size="12" value="<%=(newMemVO.getJoinDate() != null ? newMemVO.getJoinDate().formatShortDate() : "")%>" onchange="return validateDate(this);">
                  </td>
               </tr>
<%
if ( userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN))
{
%>
               <tr>
                  <td class="label">Expiry Date:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="expiryDate" value="<%=newMemVO.getExpiryDate()==null?"":newMemVO.getExpiryDate().toString()%>" onchange="return validateDate(this);"><span class="helpText">Expiry date has implications on financial earnings and should only be changed if the full impact is known.</span>
                  </td>
               </tr>
<%
}
else
{
%>
               <input type="hidden" name="expiryDate" value="<%=newMemVO.getExpiryDate()==null?"":newMemVO.getExpiryDate().formatShortDate()%>">
<%
}
%>
               <tr>
                  <td class="label">Allow to lapse:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="checkbox" name="allowToLapse" value="Yes" <%=(newMemVO.isAllowToLapse()==null?"":newMemVO.isAllowToLapse().booleanValue()?"checked":"")%>>
                  </td>
               </tr>
       <tr>
            <td class="Label">Allow to lapse reason:</td>
            <td>&nbsp;</td>
            <td>
              <select name="allowToLapseReason" >
                <option value=""></option>
<%

    // Select Box for Affliated CLubs
Iterator cancelReasonIterator = cancelReasonList.iterator();
boolean selected = false;
while ( cancelReasonIterator.hasNext() )
{
   selected = false;
   ReferenceDataVO cancelReason = (ReferenceDataVO) cancelReasonIterator.next();
   String reasonCode = cancelReason.getReferenceDataPK().getReferenceCode();

   // Can only select the deceased reason if the client has a status of deceased.
   if (((reasonCode.equalsIgnoreCase(ReferenceDataVO.MEMCANCELREASON_DECEASED)
      && Client.STATUS_DECEASED.equalsIgnoreCase(clientVO.getStatus()))
    || !reasonCode.equalsIgnoreCase(ReferenceDataVO.MEMCANCELREASON_DECEASED))
    && cancelReason.isActive() )
   {
     if (reasonCode.equals(newMemVO.getAllowToLapseReasonCode()))
     {
       selected = true;
     }
%>
                <option <%=selected?"selected":""%> value="<%=reasonCode%>"><%=cancelReason.getDescription()%></option>
<%
   }
}
%>
              </select>
            </td>
          </tr>
               <tr>
                  <td class="label">CreditAmount:</td>
                  <td>&nbsp;</td>
<%
if ( userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN))
{
%>
                  <td>
                     <input type="text" name="creditAmount" value="<%=newMemVO.getCreditAmount()%>" onchange="return validateCurrency(this);"><span class="helpText">Credit amount changes do not generate ledgers. The credit amount should equal the suspense account total.</span>
                  </td>
<%
}
else
{
%>
                  <td>
                     <%=CurrencyUtil.formatDollarValue(newMemVO.getCreditAmount())%>
                     <input type="hidden" name="creditAmount" value="<%=newMemVO.getCreditAmount()%>">                     
                  </td>
<%
}
%>
               </tr>
<%
if ( userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN))
{
%>
               <tr>
                  <td class="label">Direct Debit Authority ID:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="ddAuthorityID" value="<%=newMemVO.getDirectDebitAuthorityID()==null?"":newMemVO.getDirectDebitAuthorityID().toString()%>" onchange="return validateCurrency(this);">
                  </td>
               </tr>
<%
}
else
{
%>
               <input type="hidden" name="ddAuthorityID" value="<%=newMemVO.getDirectDebitAuthorityID()==null?"":newMemVO.getDirectDebitAuthorityID().toString()%>">
<%
}
%>
<%
if (newMemVO.getProduct().isVehicleBased()) {
	if (userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN)) {
 %>
               <tr>
                  <td class="label">Rego:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="rego" value="<%=newMemVO.getRego() == null ? "" : newMemVO.getRego()%>" />
                  </td>
               </tr>
               <tr>
                  <td class="label">VIN:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="vin" value="<%=newMemVO.getVin() == null ? "" : newMemVO.getVin()%>" />
                  </td>
               </tr>
               <tr>
                  <td class="label">Year:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="year" value="<%=newMemVO.getYear() == null ? "" : newMemVO.getYear()%>" maxlength="4" />
                  </td>
               </tr>
               <tr>
                  <td class="label">Make:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="make" value="<%=newMemVO.getMake() == null ? "" : newMemVO.getMake()%>" />
                  </td>
               </tr>
               <tr>
                  <td class="label">Model:</td>
                  <td>&nbsp;</td>
                  <td>
                     <input type="text" name="model" value="<%=newMemVO.getModel() == null ? "" : newMemVO.getModel()%>" />
                  </td>
               </tr>
<%
	} else {
%>
				<input type="hidden" name="rego" value="<%=newMemVO.getRego() == null ? "" : newMemVO.getRego()%>" />
				<input type="hidden" name="vin" value="<%=newMemVO.getVin() == null ? "" : newMemVO.getVin()%>" />
                <input type="hidden" name="year" value="<%=newMemVO.getYear() == null ? "" : newMemVO.getYear()%>" maxlength="4" />
                <input type="hidden" name="make" value="<%=newMemVO.getMake() == null ? "" : newMemVO.getMake()%>" />
                <input type="hidden" name="model" value="<%=newMemVO.getModel() == null ? "" : newMemVO.getModel()%>" />
<%
	}
}
 %>
               <tr>
                  <td valign="top" class="label">
                     Recurring discounts:
<%
Collection discountTypeList = refMgr.getDiscountTypeList();
if (discountTypeList != null)
{
   ArrayList filteredList = new ArrayList();

   DiscountTypeVO discTypeVO;
   Iterator discTypeListIterator = discountTypeList.iterator();
   while (discTypeListIterator.hasNext())
   {
      discTypeVO = (DiscountTypeVO) discTypeListIterator.next();
      // Only show active recurring discounts that have not already been selected.
      if (discTypeVO.isRecurringDiscount()
      &&  DiscountTypeVO.STATUS_Active.equals(discTypeVO.getStatus())
      //restrict the life discount types
      &&  !(discTypeVO.getDiscountTypeCode()).startsWith(DiscountTypeVO.TYPE_LIFE)
      &&  !newMemVO.hasRecurringDiscount(discTypeVO.getDiscountTypeCode()))
      {
         filteredList.add(discTypeVO);
      }
   }

   if (!filteredList.isEmpty())
   {
%>
                     <p>
                     <br/>
                     <select name="selectedDiscountTypeCode">
                        <option selected></option>
<%
      Iterator filteredListIterator = filteredList.iterator();
      while (filteredListIterator.hasNext())
      {
         discTypeVO = (DiscountTypeVO) filteredListIterator.next();
         out.println("<option>"+discTypeVO.getDiscountTypeCode()+"</option>");
      }
%>
                     </select>
                     <input type="button" value="Add" onclick="submitForm('editMembership_AddButton'); return true;"/>
<%
   }
}
%>
                  </td>
                  <td>&nbsp;</td>
                  <td valign="top">
<%
Collection memDiscountList = newMemVO.getDiscountList();
if (memDiscountList != null &&  memDiscountList.size() > 0)
{
%>
                     <table width="0%" border="0" cellspacing="0" cellpadding="3">
<%
   int count = 0;
   MembershipDiscount memDiscount;
   Iterator discListIterator = memDiscountList.iterator();
   while (discListIterator.hasNext())
   {
      memDiscount = (MembershipDiscount) discListIterator.next();
      count++;
%>
                        <tr>
<%
      if (DiscountTypeVO.STATUS_Active.equals(memDiscount.getDiscountType().getStatus()))
      {
%>
                           <td class="dataValue"><%=memDiscount.getDiscountTypeCode()%></td>
<%
      } else
      {
%>
                           <td class="dataValueRed">
                              <a title="This discount type is no longer active!">
                              <%=memDiscount.getDiscountTypeCode()%>
                              </a>
                           </td>
<%
      }
%>
                           <td>
                              <a onclick="removeDiscount('<%=memDiscount.getDiscountTypeCode()%>'); return true;"
                                 onMouseOut="MM_swapImgRestore();"
                                 onMouseOver="MM_displayStatusMsg('Remove the selected discount.');MM_swapImage('RemoveButton<%=count%>','','<%=CommonConstants.DIR_IMAGES%>/RemoveButton_over.gif',1);return document.MM_returnValue" >
                                 <img name="RemoveButton<%=count%>" src="<%=CommonConstants.DIR_IMAGES%>/RemoveButton.gif" border="0" alt="Remove the selected discount.">
                              </a>
                           </td>
                        </tr>
<%
   }
%>
                     </table>
<%
}
else
{
   out.print("<span class=\"dataValue\">None</span>");
}
%>
                  </td>
               </tr>

<%--
               <tr>
                  <td colspan="3">&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>
                     <a onclick="submitForm('editMembership_AddButton'); return true;"
                        onMouseOut="MM_swapImgRestore();"
                        onMouseOver="MM_displayStatusMsg('Add the selected discount.');MM_swapImage('AddButton','','<%=CommonConstants.DIR_IMAGES%>/AddButton_over.gif',1);return document.MM_returnValue" >
                        <img name="AddButton" src="<%=CommonConstants.DIR_IMAGES%>/AddButton.gif" border="0" alt="Add the selected discount.">
                     </a>
                  </td>
               </tr>
--%>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="bottom">
            <table border="0" cellspacing="0" cellpadding="5">
               <tr>
                  <td>
<!--
                     <input type="button" name="CancelButton" value="Cancel" onclick="submitForm('editMembership_CancelButton'); return true;">
-->
			<%=HTMLUtil.buttonCancel("editMembership_CancelButton","Cancel  the changes")%>
                  </td>
                  <td>
			<%=HTMLUtil.buttonSave("editMembership_SaveButton","Save the changes")%>
<!--
                     <a onclick="submitForm('editMembership_SaveButton'); return true;"
                        onMouseOut="MM_swapImgRestore();"
                        onMouseOver="MM_displayStatusMsg('Save the changes.');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
                        <img name="SaveButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Save the changes.">
                     </a>
-->
                 </td>
               </tr>
            </table>
         </td>
      </tr>
   </form>
</table>
</body>
</html>
