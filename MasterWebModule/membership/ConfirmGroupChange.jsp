<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Hashtable"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.CommonConstants"%>
<%@ include file="/membership/TransactionInclude.jsp"%>

<%
// Get the amount outstanding for the current prime addressee of the membership group.
//double amountOutstanding = transGroup.getAmountOutstandingForMembershipGroupCurrentPA();
double amountOutstanding = 0.0;

// Create a distinct list of all clients that were in the membership group.
Hashtable clientMap = new Hashtable();
ClientVO clientVO = null;
Integer memGroupPaClientNumber = null;   // The client number of the groups original PA.
MembershipGroupVO memGroupVO = transGroup.getSelectedMembershipGroup();
if (memGroupVO != null)
{
   Collection groupList = memGroupVO.getMembershipGroupDetailList();
   MembershipGroupDetailVO memGroupDetVO = null;
   Iterator groupListIterator = groupList.iterator();
   while (groupListIterator.hasNext())
   {
      memGroupDetVO = (MembershipGroupDetailVO) groupListIterator.next();
      clientVO = memGroupDetVO.getMembership().getClient();

      // Hold onto the client that was the PA.
      if (memGroupPaClientNumber == null && memGroupDetVO.isPrimeAddressee())
         memGroupPaClientNumber = clientVO.getClientNumber();

      // Add the client to the client list if they are not already in it.
      if (!clientMap.containsKey(clientVO.getClientNumber()))
         clientMap.put(clientVO.getClientNumber(),clientVO);
   }
}

// Now add any new clients to the client list that wern't in the original membership group.
Iterator clientListIterator = null;
//Collection selectedClientList = transGroup.getClientList();
Collection selectedClientList = null;
int selectedClientCount = 0;
if (selectedClientList != null)
{
   selectedClientCount = selectedClientList.size();
   clientListIterator = selectedClientList.iterator();
   while (clientListIterator.hasNext())
   {
      clientVO = (ClientVO) clientListIterator.next();
      if (!clientMap.containsKey(clientVO.getClientNumber()))
         clientMap.put(clientVO.getClientNumber(),clientVO);
   }
}


%>
<html>
<head>
<title>Confirm Group Change</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form name="viewMembershipVehicles" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
<input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
<input type="hidden" name="event" value="confirmGroupChange_btnNext">
  <tr valign="top">
    <td>
       <h1><%=transGroup.getTransactionHeadingText()%> - Confirm changes</h1>
       <h3>Group members: <%=selectedClientCount%> <%=(selectedClientCount == 1 ? "client" : "clients")%> selected.</h3>
       <p>
       <table border="0" cellpadding="3" cellspacing="0">
<%
//   Integer newPaClientNumber = transGroup.getPrimeAddresseeClientNumber();
/** @todo fix idientifying new prime addressee */
   Integer newPaClientNumber = null;
   String newPaName = null;
   String status = null;
   Collection clientList = clientMap.values();
   clientListIterator = clientList.iterator();
   while (clientListIterator.hasNext())
   {
      clientVO = (ClientVO) clientListIterator.next();
//      if (transGroup.isClientInMembershipGroup(clientVO.getClientNumber()))
//      {
//         // The client was part of the original membership group.
//         // See if they are still selected.
//         if (transGroup.isClientSelected(clientVO.getClientNumber()))
//            // Has there primary addressee status changed
//            if (clientVO.getClientNumber().equals(newPaClientNumber)
//            && clientVO.getClientNumber().equals(memGroupPaClientNumber))
//               status = "prime addressee";
//            else if (clientVO.getClientNumber().equals(newPaClientNumber)
//            && !clientVO.getClientNumber().equals(memGroupPaClientNumber))
//            {
//               status = "new prime addressee";
//               newPaName = clientVO.getDisplayName();
//            }
//            else if (!clientVO.getClientNumber().equals(newPaClientNumber)
//            && clientVO.getClientNumber().equals(memGroupPaClientNumber))
//               status = "old prime addressee";
//            else
//               status = "no change";
//         else
//            status = "removed from group";
//      }
//      else
//      {
//         // They must be a new client added to the group
//         status = "added to group";
//      }
%>
         <tr>
            <td class="label"><%=clientVO.getClientNumber()%></td>
            <td class="label"><%=clientVO.getDisplayName()%></td>
            <td class="dataValue"><%=status%></td>
         </tr>
<%
   }

%>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td class="label" colspan="3">Amount outstanding for current prime addressee: <span class="dataValue"><%=CurrencyUtil.formatDollarValue(amountOutstanding)%></span></td>
         </tr>
<%
if (amountOutstanding > 0.0 && newPaName != null)
{
%>
         <tr>
            <td class="label" colspan="3"><span class="dataValue"><%=newPaName%></span> will become responsible for this amount.</td>
         </tr>
<%
}
%>
       </table>
    </td>
  </tr>
  <tr valign="bottom">
    <td>
       <!-- View data links -->
       <table border="0" cellpadding="5" cellspacing="0">
         <tr>
           <td><input type="button" name="btnBack" value="Back" onclick="history.back();"></td>
           <td><input type="submit" name="btnNext" value="Next"></td>
         </tr>
       </table>
    </td>
  </tr>
</form>
</table>
</body>
</html>
