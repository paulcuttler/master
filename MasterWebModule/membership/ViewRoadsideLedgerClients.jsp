<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.ract.payment.ui.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="en-AU" />

<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>

		<script type="text/javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
		
	    <meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0"> 
	
		<style type="text/css">
			.header { background: #003366 url("/menu/images/Blank.gif") repeat-x left bottom; }
			.header img { display: block; }
			.holder { margin: 11px; }			
			.listHeadingPadded { border-bottom: 1px solid #fff; }		
		</style>
		<title>Roadside Ledger Report: Client Breakdown</title>
	</head>
	<body>
		<div class="header">
		  <img src="/menu/images/NamePanel.gif" alt="RACT" width="180" height="47" />
		</div> 
		<div class="holder">
			<h1>Roadside Ledger Report - Client Breakdown</h1>
			<ul>
				<li>Start Date: <s:property value="start" /></li>
				<li>End Date: <s:property value="end" /></li>
			</ul>
			<s:actionmessage />
			<s:actionerror />
			<s:fielderror />
			
			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr class="headerRow">
					<td class="listHeadingPadded" colspan="3">Client</td>
					<td class="listHeadingPadded" style="text-align: right">Ledgers</td>
					<td class="listHeadingPadded">&nbsp;</td>
					<td class="listHeadingPadded" style="text-align: right">Vouchers</td>
					<td class="listHeadingPadded">&nbsp;</td>
					<td class="listHeadingPadded" style="text-align: right">Credit</td>
					<td class="listHeadingPadded">&nbsp;</td>
					<td class="listHeadingPadded" style="text-align: right">Variance</td>
				</tr>			
				<s:if test="clientBreakdown != null and !clientBreakdown.isEmpty()">
					<s:iterator var="clientNo" value="clientBreakdown.keySet()" status="status">
						<tr class="${status.even ? 'even' : 'odd'}Row">
							<td><s:property value="#clientNo" /></td>
							<td>
								<a href="RoadsideLedgerReportPostings.action?start=${start}&end=${end}&clientNumber=${clientNo}" onclick="return newWindow(this.href);" title="View 2310 Postings">View Postings</a>
							</td>
							<td>
								<a href="MembershipUIC?event=viewClientSummary_viewMembership&membershipID=${membershipLookup[clientNo].membershipID}" onclick="return newWindow(this.href);">View Membership</a>
							</td>							
							<td style="text-align: right"><fmt:formatNumber value="${!empty clientBreakdown[clientNo]['postings'] ? clientBreakdown[clientNo]['postings'] : 0}" type="currency" /></td>
							<td style="text-align: right">+</td>
							<td style="text-align: right"><fmt:formatNumber value="${!empty clientBreakdown[clientNo]['vouchers'] ? clientBreakdown[clientNo]['vouchers'] : 0}" type="currency" /></td>
							<td style="text-align: right">+</td>
							<td style="text-align: right"><fmt:formatNumber value="${!empty clientBreakdown[clientNo]['credit'] ? clientBreakdown[clientNo]['credit'] : 0}" type="currency" /></td>
							<td style="text-align: right">=</td>
							<td style="text-align: right"><fmt:formatNumber value="${!empty clientBreakdown[clientNo]['variance'] ? clientBreakdown[clientNo]['variance'] : 0}" type="currency" /></td>
						</tr>
					</s:iterator>
				</s:if>
			
			</table>
		</div>				
	</body>
</html>