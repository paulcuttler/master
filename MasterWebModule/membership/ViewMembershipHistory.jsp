<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="javax.ejb.ObjectNotFoundException"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.security.ui.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.payment.ui.*"%>

<%
UserSession userSession = UserSession.getUserSession(session);

UserMgr uMgr = UserEJBHelper.getUserMgr();

ArrayList memTransList = null;
Iterator memTransListIT = null;

MembershipVO memVO = null;
MembershipTransactionVO memTransVO = null;
ReferenceDataPK pk = null;

memVO = (MembershipVO) request.getAttribute("membershipVO");

boolean showUndone = false;
if ("true".equals(request.getParameter("showUndone"))) {
   showUndone = true;
}

// get the list of history that the membership has.
memTransList = new ArrayList(memVO.getTransactionList());

//reverse order the list on transaction date.
MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE,true);
Collections.sort(memTransList,mtc);

%>

<html>
<head>
  <title>View membership history</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript">
   function removeTransaction(transID)
   {
      var fRet;
      fRet = confirm('Are you sure that you want to REMOVE the transaction? This cannot be reversed.');
      if (fRet)
      {
        document.all.event.value = 'viewMembershipHistory_btnRemove';
        document.all.transactionID.value = transID;
        document.viewMembershipHistory.submit();
        return 0;
      }
   }

   function reverseTransaction(transID)
   {

      var fRet;
      fRet = confirm('Are you sure that you want to reverse the transaction? This cannot be reversed and will remove any issued cards and reverse ledgers.');
      if (fRet)
      {
        document.all.event.value = 'viewMembershipHistory_btnReverse';
        document.all.transactionID.value = transID;
        document.viewMembershipHistory.submit();
        return 0;
      }
   }

   function undoTransaction(transID)
   {
      document.all.event.value = 'viewMembershipHistory_btnUndo';
      document.all.transactionID.value = transID;
      document.viewMembershipHistory.submit();
      return 0;
   }

   function editTransaction(transID)
   {
      document.all.event.value = 'viewMembershipHistory_btnEdit';
      document.all.transactionID.value = transID;
      document.viewMembershipHistory.submit();
      return 0;
   }

   function showUndoneTransactions()
   {
      document.all.event.value = 'viewMembershipHistory_showUndone';
      document.all.viewMembershipHistory.submit();
      return 0;
   }

   function createTransactionHistory()
   {
      document.all.event.value = 'viewMembershipHistory_btnCreateHistory';
      document.all.viewMembershipHistory.submit();
      return 0;
   }

</script>
</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif');">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <form name="viewMembershipHistory" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="viewMembershipHistory_btnMembership">
  <input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
  <input type="hidden" name="transactionID">
    <tr valign="top">
      <td nowrap>
        <h2><%@ include file="/help/HelpLinkInclude.jsp"%> Membership history</h2>
<%
if (memTransList != null && !memTransList.isEmpty())
{
   CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
%>
        <h3>There <%=memTransList.size()==1?"is 1 transaction ":"are "+memTransList.size()+" transactions "%> for membership <%=memVO.getClientNumber()%>.</h3>
        <input type="checkbox" name="showUndone" value="true" <%=(showUndone ? "checked" : "")%> onClick="showUndoneTransactions(); return true;"> show undone transactions.

        <table border="0" cellspacing="0" cellpadding="3">
<%
   boolean canUndo = true;
   ReferenceDataVO refData = null;
   String refType = null;
   String refCode = null;
   String historyFilename = null;
   memTransListIT = memTransList.iterator();
   int transactionCounter = 0;
   boolean historyAvailable = false;
   while (memTransListIT.hasNext())
   {
      //reinitialise for the loop
      historyAvailable = false;
      transactionCounter++;
      memTransVO = (MembershipTransactionVO) memTransListIT.next();
      historyAvailable = memTransVO.isHistoryAvailable();

      if (!memTransVO.isUndone() || showUndone)
      {
%>
          <tr>
            <td class="dataValue" nowrap>
<%
         if (historyAvailable)
         {
%>
              <a href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembershipHistory_selectHistory&transactionID=<%=memTransVO.getTransactionID()%>"
               title="Click here to view the membership details at the completion of the transaction."
              >
              <%=DateUtil.formatLongDate(memTransVO.getTransactionDate())%>
              </a>
<%
         }
         else
         {
%>
              <%=DateUtil.formatLongDate(memTransVO.getTransactionDate())%>
<%
         }
%>
            </td>
            <td class="label" nowrap>Transaction type:</td>
            <td class="dataValue" nowrap>
              <a class="dataValue" title="transaction ID = <%=memTransVO.getTransactionID()%>">
               <%=memTransVO.getTransactionTypeCode() + (memTransVO.isUndone() ? "<span class=\"helpTextSmall\"> (undone)</span>" : "")%>
              </a>
            </td>
            <td>
<%
         // Show the edit transaction button if the user has the privilege to do so.
         if (userSession != null
         &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_EDIT_TRANSACTION))
         {
%>
              <input type="button" name="btnEdit" value="Edit" onclick="editTransaction(<%=memTransVO.getTransactionID()%>);"  title="Edit this transaction.">
<%
         }
         // Show the remove transaction button if the user has the privilege to do so.
         if (userSession != null
         &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_REMOVE_TRANSACTION))
         {
%>
              <input type="button" name="btnRemove" value="Remove" onclick="removeTransaction(<%=memTransVO.getTransactionID()%>);"  title="Remove this transaction." style="background: #f00; color: #fff; font-weight: bold;">
<%
         }
         //show the reverse transaction button if the user has the privilege to do so.
         if (canUndo &&
            !memTransVO.isUndone() &&
             userSession != null
         &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_REVERSE_TRANSACTION))
         {
%>
              <input type="button" name="btnReverse" value="Reverse" onclick="reverseTransaction(<%=memTransVO.getTransactionID()%>);"  title="Reverse this transaction. Allows a paid transaction to be reversed.">
<%
         }


         // Show the undo button if this transaction has not already been undone
         // and it is the first transaction displayed. Relies on the transactions being
         // in reverse date order.
         if (canUndo &&
             !memTransVO.isUndone() &&
             userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_UNDO))
         {
%>
              <input type="button" name="btnUndo" value="Undo" onclick="undoTransaction(<%=memTransVO.getTransactionID()%>);" title="Undo this transaction.">
<%
         }
         canUndo = false;
%>
            </td>

          </tr>
<%

         // If the transaction has been undone then show who did the undo and when.
         if (memTransVO.getUndoneDate() != null)
         {
%>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>Undone by:</td>
            <td class="dataValue" colspan="3" nowrap><%=memTransVO.getUndoneUsername()%></td>
          </tr>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>Undone date:</td>
            <td class="dataValue" colspan="3" nowrap><%=memTransVO.getUndoneDate().formatLongDate() %></td>
          </tr>
<%
         }

         if (memTransVO.getTransactionGroupID() > 0)
         {
%>
          <tr>
            <td></td>
            <td class="label" nowrap>Group transaction:</td>
            <td class="dataValue" colspan="3" nowrap>
              <a class="dataValue" title="Transaction group ID = <%=memTransVO.getTransactionGroupID()%>">
                 <%=memTransVO.getTransactionGroupTypeCode()%>
              </a>
            </td>
          </tr>
<%
         }

         refData = null;
         refType = memTransVO.getTransactionReasonType();
         refCode = memTransVO.getTransactionReasonCode();
         if (refType != null && refCode != null)
         {
               refData = commonMgr.getReferenceData(refType, refCode);
%>
          <tr>
            <td>&nbsp;</td>
            <td class="label">Reason:</td>
            <td class="dataValue" colspan="3"><%=(refData == null ? "" : refData.getDescription())%></td>
          </tr>
<%
         }  // if (refType != null && refCode != null)
%>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>User:</td>
            <td class="dataValue" colspan="3" nowrap>
<%
         String userID = memTransVO.getUsername();
         User user = null;
         if (userID != null && !userID.equals(""))
         {
            try
            {
               user = uMgr.getUser(userID);
            }
            catch (Exception e)
            {
               out.print(memTransVO.getUsername() + "(user name not found)");
            }
            if (user != null)
               out.print(user.getUsername() + "(" +user.getUserID() + ")" );
         }
%>
            </td>
          </tr>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>Amount payable:</td>
<%
//         if (memTransVO.getAmountPayable() > 0.0) {
         if (true) {
%>
            <td class="dataValue" colspan="3" nowrap>
              <a class="actionItem"
                 href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembershipHistory_selectAmountPayable&transactionID=<%=memTransVO.getTransactionID()%>"
                 title="Click here to view the details of the fees and discounts applied to this members transaction only."
              >
              <%=CurrencyUtil.formatDollarValue(memTransVO.getAmountPayable())%>
              </a>
            </td>
<%
         }
         else {
%>
            <td class="dataValue" colspan="3" nowrap><%=CurrencyUtil.formatDollarValue(memTransVO.getAmountPayable())%></td>
<%
         }
%>
          </tr>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>GST:</td>
            <td class="dataValue" colspan="3" nowrap><%=CurrencyUtil.formatDollarValue(memTransVO.getGstFee()) %></td>
          </tr>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>Effective Date:</td>
            <td class="dataValue" colspan="3" nowrap><%=memTransVO.getEffectiveDate()%></td>
          </tr>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>End Date:</td>
            <td class="dataValue" colspan="3" nowrap><%=memTransVO.getEndDate()%></td>
          </tr>
          <%
          if (memTransVO.getDisposalId() != null)
          {
          %>
          <tr>
            <td nowrap>&nbsp;</td>
            <td class="label" nowrap>Credit Voucher:</td>
            <td class="dataValue" colspan="3" nowrap><a href="viewVoucherDetails.action?disposalId=<%=memTransVO.getDisposalId()%>">View Voucher Details</a></td>
          </tr>   
          <%
          }
          %>       

<%
         if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_REMOVE_TRANSACTION))
         {
%>
          <tr>
            <td>&nbsp;</td>
            <td class="label" nowrap>Payable Item ID:</td>
            <td class="dataValue" colspan="3" nowrap>
<%
            Integer payableItemID = memTransVO.getPayableItemID();
            if (payableItemID != null && !payableItemID.equals(new Integer(0)))
            {
%>
              <a class="actionItem"
                 href="<%=PaymentUIConstants.PAGE_PaymentUIC%>?event=viewPayableItem_viewDetail&payableItemID=<%=payableItemID%>"
                 title="View the payable item."
              >
                 <%=payableItemID%>
              </a>
<%
            }
%>
            </td>
          </tr>

<%
         }
      }
   }  //  while (memTransListIT.hasNext())
%>
        </table>
<%
}
else
{
   out.print("No history to be displayed!");
}
%>
      </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table width="0%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>
               <a onclick="document.all.event.value='viewMembershipHistory_btnMembership'; document.forms[0].submit(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('View the membership details');MM_swapImage('ViewMembershipButton','','<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif',1);return document.MM_returnValue" >
                  <img name="ViewMembershipButton" src="<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton.gif" border="0" alt="View the membership details">
               </a>
            </td>
        </tr>
      </table>
    </td>
  </tr>
  </form>
</table>
</body>
</html>
