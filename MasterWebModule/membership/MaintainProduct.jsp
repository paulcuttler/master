<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>

<%
String saveEvent     = "maintainProduct_btnSave";
String deleteEvent   = "maintainProduct_btnDelete";
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;

String description                     = "";
boolean isDefault                      = false;
String status                          = "";
String creditProductCode               = "";
int  prodIdentifier                    = 0;
int  sortOrder                         = 0;

SortedSet selBenefitsList = new TreeSet();
SortedSet availBenefitsList = new TreeSet();

Collection selectedBenefits            = null;
ArrayList availableBenefits            = null;
ProductBenefitTypeVO prodBenefitType   = null;

// Get the ProductVO from the session
ProductVO prodVO = (ProductVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_PRODUCT_VO);
String statMsg  = (String) request.getAttribute("statusMessage");
String    refresh   = (String) request.getAttribute("Refresh");

SortedSet sStatusList = new TreeSet();
sStatusList.add(new AdminCodeDescription(ProductVO.STATUS_ACTIVE,ProductVO.STATUS_ACTIVE));
sStatusList.add(new AdminCodeDescription(ProductVO.STATUS_INACTIVE,ProductVO.STATUS_INACTIVE));

MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
availableBenefits = (ArrayList) refMgr.getProductBenefitTypeList();

String productCode = prodVO.getProductCode();

if ( !(productCode == null))
{

description       = prodVO.getDescription();
isDefault         = prodVO.isDefault();
status            = prodVO.getStatus();
creditProductCode = prodVO.getCreditProductCode();
sortOrder         = prodVO.getSortOrder();
prodIdentifier    = prodVO.getProductIdentifier();
selectedBenefits  = prodVO.getProductBenefitList();
}
else
{
   StringUtil.makeSpaces(productCode);
}

if ( !(selectedBenefits == null)  )
{
   Iterator it = selectedBenefits.iterator();
   while( selectedBenefits.size() > 0 & it.hasNext() )
   {
      Object x = it.next();
      if (x != null) {
	      prodBenefitType = (ProductBenefitTypeVO) x;
	      selBenefitsList.add( new AdminCodeDescription( prodBenefitType.getProductBenefitCode(),prodBenefitType.getDescription()));
      }
   }
}
// Remove already selected benefits from the available benefits
if (!availableBenefits.isEmpty() )
{
  Iterator it = availableBenefits.iterator();
  while (it.hasNext())
  {
    prodBenefitType = (ProductBenefitTypeVO) it.next();
    AdminCodeDescription ad = new AdminCodeDescription( prodBenefitType.getProductBenefitCode(),prodBenefitType.getDescription());
    if ((selBenefitsList == null) || !(selBenefitsList.contains(ad)))
    {
      availBenefitsList.add( ad );
    }
  }
}
%>
<head>
<title>Maintain Product</title>
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
</head>
<body class="rightFrame" onload="refreshList(document.all.refresh,'<%=listPage%>')">

<form name="mainForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>">

<input type="hidden" name="event" value="">
<input type="hidden" name="listCode" value="">
<input type="hidden" name="refresh" value ="<%=refresh%>">

 <div id="mainpage" class="pg">
 <h1>Maintain  Product</h1>
   <div id="contentpage" class="cnt">
<!--
   START PRODUCT BODY
-->
<table border="0" cellspacing="1" cellpadding="0"  width="100%">
   <tr>
    <td class="label" width="17%">Product Code:</td>
    <td class="dataValue" width="2%">&nbsp;</td>
    <td class="dataValue" width="81%">
       <%=HTMLUtil.inputOrReadOnly("productCode",productCode)%>
    </td>
   </tr>

   <tr>
      <td class="label" width="17%">Description:</td>
      <td class="dataValue" width="2%">&nbsp;</td>
      <td class="dataValue" width="81%">
        <input type="text" name="description" value="<%=description%>">
      </td>
   </tr>
   <tr>
      <td class="label" width="17%">Default:</td>
      <td class="dataValue" width="2%">&nbsp;</td>
      <td class="dataValue" width="81%">
        <input type="checkbox" name="isDefault" value="true" <% if (isDefault) {out.print("checked");} %>>
      </td>
   </tr>
   <tr>
      <td class="label" width="17%">Status:</td>
      <td class="dataValue" width="2%">&nbsp;</td>
      <td class="dataValue" width="81%">
          <%=HTMLUtil.selectList("selectStatus",sStatusList,status,ProductVO.STATUS_ACTIVE,ProductVO.STATUS_ACTIVE)%>
      </td>
   </tr>
   <tr>
      <td class="label" width="17%">Identifier:</td>
      <td class="dataValue" width="2%">&nbsp;</td>
      <td class="dataValue" width="81%">
         <input type="text" name="identifier" value="<%=prodIdentifier%>">
      </td>
   </tr>
   <tr>
     <td class="label" width="17%">Credit Product Code:</td>
     <td class="dataValue" width="2%">&nbsp;</td>
     <td class="dataValue" width="81%">
         <input type="text" name="creditProductCode" value="<%=creditProductCode%>">
     </td>
   </tr>
  <tr>
     <td class="label" width="17%">Sort Order:</td>
     <td class="dataValue" width="2%">&nbsp;</td>
     <td class="dataValue" width="81%">
       <input type="text" name="sortOrder" value="<%=sortOrder%>">
     </td>
   </tr>
   <tr>
   		<td colspan="3">
                  <%=HTMLUtil.codeDescriptionTable (  "Allowable Benefits.",
                                                      availBenefitsList,
                                                      "maintainProduct_Benefit_btnAdd",
                                                      "Double click to ADD this Benefit.",
                                                      "No Benefits to be selected"
                                                   )
                  %>

		</td>
    </tr>
    <tr>
    	<td colspan="3">

                  <%=HTMLUtil.codeDescriptionTable (  "Selected Benefits.",
                                                      selBenefitsList,
                                                      "maintainProduct_Benefit_btnRemove",
                                                      "Double click to REMOVE this Benefits.",
                                                      "No selected Benefits"
                                                   )
                  %>
         </td>
     </tr>
</table>
<!--
   FINISH PRODUCT BODY
-->
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSaveAndDelete(saveEvent,deleteEvent,"Product", statMsg )%>
   </div>
</div>


</form>
</body>
</html>
