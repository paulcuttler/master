<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.*"%>


<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

<script language="Javascript">
function issueGift()
{
  document.all.event.value="issueGift";
  document.manageGifts.submit();
}

function returnGift()
{
  document.all.event.value="returnGift";
  var reason = document.all.returnNotes.value;
  if (reason == null || reason.length < 1)
  {
    alert('A reason must be entered if returning a gift.');
    return false
  }
  else
  {
    document.manageGifts.submit();
  }
}

function deleteGift()
{
  document.all.event.value="deleteGift";
  var reason = document.all.returnNotes.value;
  if (reason == null || reason.length < 1)
  {
    alert('A reason must be entered if deleting a gift.');
    return false
  }
  else
  {
    document.manageGifts.submit();
  }
}
</script>

</head>

<%
MembershipVO membership = (MembershipVO)request.getAttribute("membership");

log(""+membership);

DateTime now = new DateTime();

Collection giftList = CommonEJBHelper.getCommonMgrLocal().getGiftList();
Gift freeMembership = (Gift)CommonEJBHelper.getCommonMgrLocal().getGiftListByType(Gift.TYPE_GIFT_FREE_MEMBERSHIP).iterator().next();
Gift accommodation = (Gift)CommonEJBHelper.getCommonMgrLocal().getGiftListByType(Gift.TYPE_GIFT_ACCOMMODATION_GUIDE).iterator().next();
MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
Collection membershipGiftList = membershipMgrLocal.getMembershipGiftByMembershipID(membership.getMembershipID());
%>

<body bgcolor="#FFFFFF" text="#000000">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
    <td>


<table width="75%" border="0">

<tr>
  <td colspan="2"> 
<h2><strong>Manage Membership Gifts</strong></h2>
  <form name="manageGifts" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
<strong><input type="hidden" name="event"> 
<input type="hidden" name="membershipId" value="<%=membership.getMembershipID()%>"></strong>
  </td>
</tr>

  <tr>
    <td colspan="2"><strong><%@include file="/client/ViewClientInclude.jsp"%></strong><br></td>
  </tr>

  <tr valign="top">
    <td colspan="5">
      <h3><strong>Available Gift List</strong></h3>
    </td>
  </tr>
  <tr>
      <td valign="top" class="label"><strong>Gift</strong></td>
      <td>
        <table border="0">
          <tr>
            <td>
              <strong><input type="radio" name="giftType" value="<%=Gift.TYPE_GIFT_GOLD_MEDALLION%>" checked="checked"></strong>
            </td>
            <td><strong><%=Gift.TYPE_GIFT_GOLD_MEDALLION%></strong>
            <br></td>
            <td><strong><select name="gift"> 
          <% 
          if (giftList != null) 
          { 
            Gift gift = null; 
            Iterator giftIt = giftList.iterator(); 
            while (giftIt.hasNext()) 
            { 
              gift = (Gift)giftIt.next(); 
              if (gift.getGiftType().equals(Gift.TYPE_GIFT_GOLD_MEDALLION)) 
              { 
          %> 
                <option value="<%=gift.getGiftCode()%>"><%=gift.getGiftName()%></option> 
                <% 
              } 
            } 
          } 
          %> 
              </select></strong></td>
          </tr>
          <tr>
            <td>
              <strong><input type="radio" name="giftType" value="<%=Gift.TYPE_GIFT_FREE_MEMBERSHIP%>"></strong>
            </td>
            <td><strong>Free membership</strong></td>
            <td>
              <strong><input type="hidden" name="freeMembership" value="<%=freeMembership.getGiftCode()%>"> 
              <input type="text" name="membershipNumber"></strong>
            </td>
          </tr>
          <tr>
            <td>
              <strong><input type="radio" name="giftType" value="<%=Gift.TYPE_GIFT_ACCOMMODATION_GUIDE%>"></strong>
            </td>
            <td><strong>Accommodation Guide</strong></td>
            <td>
              <strong><input type="hidden" name="accommodation" value="<%=accommodation.getGiftCode()%>"></strong>
            </td>
          </tr>
        </table>
      </td>
  </tr>
  <tr valign="top">
      <td class="label"><strong>Issued</strong></td>
    <td class="dataValue">
        <strong><input type="hidden" name="create_date" value="<%=now%>"><%=now%></strong>
      </td>
  </tr>
  <tr valign="top">
      <td class="label"><strong>Renewal Tier</strong></td>
      <td class="dataValue"><strong><input type="hidden" name="tierCode" value="<%=membership.getRenewalMembershipTier().getTierCode()%>"> <%=membership.getRenewalMembershipTier().getTierCode()%> (<%=membership.getRenewalMembershipTier().getTierName()%>)</strong></td>
  </tr>
  <tr valign="top">
      <td class="label"><strong>Notes</strong></td>
    <td>
        <strong><textarea name="notes" rows="2" cols="50"></textarea></strong>
      </td>
  </tr>
  <tr>
    <td colspan="2"><strong><input type="button" name="Submit" value="Issue" onclick="javascript:issueGift()"></strong>
</td>
  </tr>
</table>

<table width="75%" border="0">
  <tr><td colspan="5"><h3><strong>Issued Gift List</strong></h3></td></tr>
<strong><% 
int counter = 0; 
if (membershipGiftList != null && 
   !membershipGiftList.isEmpty()) 
   { 
    %></strong>
       <tr class="headerRow" >
         <td><br></td><td><strong>Name</strong><br></td><td><strong>Issued</strong><br></td><td><strong>User Id</strong><br></td><td><strong>Sales Branch Code</strong><br></td><td><strong>Status</strong><br></td><td><strong>Tier Code</strong><br></td><td><strong>Notes</strong><br></td><td><br></td>
       </tr>
     <strong><% 
     Iterator memGiftList = membershipGiftList.iterator(); 
     MembershipGift memGift = null; 
     while (memGiftList.hasNext()) 
     { 
       counter++; 
       memGift = (MembershipGift)memGiftList.next(); 
       %></strong> 
       <tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
         <td><strong><input type="radio" name="selectedGift" value="<%=membership.getMembershipID()%>,<%=memGift.getGift().getGiftCode()%>,<%=memGift.getMembershipGiftPK().getStatus()%>,<%=memGift.getMembershipGiftPK().getTierCode()%>" <%=counter==1?"checked=\"checked\"":""%>></strong><br></td><td><strong><%=memGift.getGift().getGiftName()%></strong><br></td><td><strong><%=memGift.getCreateDate().formatLongDate()%></strong><br></td><td><strong><%=memGift.getUserId()%></strong><br></td><td><strong><%=memGift.getSalesBranchCode()%></strong><br><td><strong><%=memGift.getMembershipGiftPK().getStatus()%></strong><br></td><td><strong><%=memGift.getMembershipGiftPK().getTierCode()%></strong><br></td><td><strong><%=memGift.getNotes()%></strong><br></td><td><strong><%=memGift.getMembershipNumber()==null?"":memGift.getMembershipNumber()%></strong><br></td>
       </tr>
       <strong><% 
     } 
   } 
%></strong>
</table>
<strong><% 
if (counter > 0) 
{ 
%></strong>
<table>
<tr valign="top">
  <td class="label"><strong>Reason:</strong></td>
      <td>
        <strong><textarea name="returnNotes" rows="2" cols="50"></textarea></strong>
      </td>
</tr>
</table>
<strong><% 
} 
%></strong>
</td>
</tr>

<tr valign="bottom">
  <td>
<table>
<tr>
  <td><strong><input type="button" name="" value="Delete" onclick="javascript:deleteGift()"><input type="button" name="" value="Return" onclick="returnGift()"></strong></td>
</tr>
</table>
  </td>
  </tr>
  </table>
</form>
</body>
</html>
