<%@ page import="javax.naming.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@page import="com.ract.common.CommonConstants"%>

<%@ include file="/membership/TransactionInclude.jsp"%>
<%
// get the member transaction list from the group transaction
Collection memberTransactionList = transGroup.getTransactionList();
int memberCount = memberTransactionList.size();
String productCode = transGroup.getSelectedProduct().getProductCode();
Iterator memberTransactionListIterator;
Iterator distinctFeeTypeListIterator;
Collection availableDiscountList = null;
Iterator availableDiscountListIterator = null;
MembershipTransactionVO membershipTxVO = null;
ClientVO clientVO = null;
MembershipVO newMemVO = null;
DiscountTypeVO discountTypeVO = null;
String discountTypeCode;
String discountSelected;
String addressee = null;
double netFee = 0.0;
String membersText = (memberCount == 1 ? "member" : "members");
%>
<html>
<head>
<title>Member Discount</title>
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
   dojo.require("dojo.widget.*");
</script>
<script language="JavaScript">
function toggleClients()
{
  var myTable = dojo.widget.byId("clientTable");
  var rows = myTable.domNode.tBodies[0].rows;
  for(var i=0; i<rows.length; i++)
  {
    var row = rows[i];
    //workaround valign bug in filteringTable
    row.vAlign = "top"
    //check if toggleClient checkbox has been rendered
    if (typeof(document.all.toggleClient)!="undefined")
    {
      var txType = row.cells[2].innerHTML
      if (document.all.toggleClient.checked == true)
      {
        if (txType.indexOf("Added") == -1)
        {
          row.style.display = 'none';
        }
        else
        {
          row.style.display = '';
        }
      }
      else
      {
        row.style.display = '';
      }
    }
  }
}

function toggleDiscountAttribute(check,row)
{
  //this is a row
  if (check.checked == true)
  {
    row.style.display = '';
  }
  else
  {
    row.style.display = 'none';
  }
}
</script>
</head>
<body onLoad="toggleClients()">
<form name="memberDiscount" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
<input type="hidden" name="<%=transGroup.KEY%>" value="<%=transGroup.getTransactionGroupKey()%>">
<input type="hidden" name="event" value="nonDiscretionaryDiscount_btnNext">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <tr valign="top">
    <td>
      <h1><%=transGroup.getTransactionHeadingText()%> - Select Member Discounts</h1>
      <%
      if (memberCount > 1)
      {
      %>
      <p><input type="checkbox" name="toggleClient" onclick="toggleClients()">Show added members only</p>
      <%
      }
      %>
      <h3>Member details: <%=memberCount%> <%=membersText%></h3>
      <div style="height:315; overflow:auto; border-width: 1; border-style: solid;">
      <table  dojoType="filteringTable"
            id="clientTable"
            cellpadding="2"
            alternateRows="true"
            class="dataTable">
        <thead>
          <tr>
            <th field="clientNumber" dataType="Number" align="left" valign="top">Client</th><th field="desc" dataType="String" align="left" valign="top">Name</th><th field="trans" dataType="String" align="left" valign="top">Transaction</th>
          </tr>
        </thead>
        <tbody>
<%
// for each member transaction in the list
memberTransactionListIterator = memberTransactionList.iterator();
int row = 0;
while (memberTransactionListIterator.hasNext())
{
   membershipTxVO = (MembershipTransactionVO) memberTransactionListIterator.next();
   //!MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(membershipTxVO.getTransactionTypeCode())
   //has a fee
   if (true)
   {
      row++;
      newMemVO = membershipTxVO.getNewMembership();
      clientVO = newMemVO.getClient();

      boolean added =  membershipTxVO.getMembership()==null?true:!transGroup.isMembershipInMembershipGroup(membershipTxVO.getMembership().getMembershipID());

      // Get a list of disctinct fee types attached to the membership
      // transaction
      Collection distinctFeeTypeList = membershipTxVO.getDistinctFeeTypeList();

     // tag the prime addressee
      if (membershipTxVO.isPrimeAddressee())
      {
         addressee = "<span class=\"helpTextSmall\"> (Addressee)</span>";
      }
      else
      {
         addressee = "";
      }

%>
         <tr value="<%=row%>" id="client_<%=row%>">
           <td valign="top"><%=clientVO.getClientNumber()%></td>
           <td valign="top">
             <table>
               <tr>
                 <td class="dataValue" colspan=4><%=clientVO.getSortableDisplayName()%><%=addressee%>
               </tr>
<%
      if (distinctFeeTypeList != null)
      {
         // For each fee type in the list, show the fee details and show the
         // non automatic fees that apply.
         String feeTypeCode = null;
         distinctFeeTypeListIterator = distinctFeeTypeList.iterator();
         while (distinctFeeTypeListIterator.hasNext())
         {
            feeTypeCode = (String) distinctFeeTypeListIterator.next();
            FeeTypeVO feeTypeVO = refMgr.getFeeType(feeTypeCode);

            // Get the discounted fee for this fee type.
            netFee = membershipTxVO.getNetTransactionFee(feeTypeCode);

            // check that the fee type can have non automatic discounts
            //and a fee exists
            if (feeTypeVO.hasAllowedDiscounts(false) &&
                netFee > 0)
            {

%>
         <tr>
            <td width="5%">&nbsp;</td>
            <td class="label" colspan="3"><%=feeTypeCode%>:&nbsp;&nbsp;<span class="dataValue"><%=CurrencyUtil.formatDollarValue(netFee)%></span> <span class="helpTextSmall">including GST</span></td>
         </tr>
<%
               // check for product discounts not selected for the group
               availableDiscountList = refMgr.getAllowableDiscountList(feeTypeCode, new DateTime());

               // now write the fee discounts
               availableDiscountListIterator = availableDiscountList.iterator();

               while (availableDiscountListIterator.hasNext())
               {
                  discountTypeVO  = (DiscountTypeVO) availableDiscountListIterator.next();

                  // Filter out discretionary discounts, automatic discounts and system controlled discounts
                  if (!discountTypeVO.isAutomaticDiscount() &&
                   !discountTypeVO.isSystemControlled()) 
                  {
                     discountTypeCode = discountTypeVO.getDiscountTypeCode();
                     discountSelected = "";
                     if (membershipTxVO.hasDiscountType(feeTypeCode,discountTypeCode)) {
                        discountSelected = "checked";
                     } 
                     else {
                        discountSelected = "";
                     }
//
//          See if Life Membership is the reason code. If it is automatically select the
//          LIFE-J and LIFE-U/LIFE-A discount types
//
                     if ("life".equalsIgnoreCase(membershipTxVO.getTransactionReasonCode())
                     &&  discountTypeCode.startsWith("Life")) {
                        discountSelected = "checked";
                     }
                     //String discountLabel = "disc_" + clientVO.getClientNumber() + "_" + HTMLUtil.cleanString(feeTypeCode) + "_" + HTMLUtil.cleanString(discountTypeCode);
                     String discountLabel = MembershipUIHelper.getDiscountLabel(clientVO.getClientNumber(),feeTypeCode,discountTypeCode);
                     Collection discountAttributes = discountTypeVO.getDiscountAttributes();
%>
         <tr>
            <td width="">&nbsp;</td>
            <td width="">&nbsp;</td>
            <td nowrap><input type="checkbox" name="<%=discountLabel %>" id="<%=discountLabel %>" value="Yes" <%=discountSelected%> <%=(discountAttributes!= null && !discountAttributes.isEmpty()?"onclick=\"toggleDiscountAttribute(this,"+discountLabel+"_attr)\"":"")%>>&nbsp;&nbsp;<span class="dataValue"><%=discountTypeCode%></span><%=(discountTypeVO.isRecurringDiscount() ? "<span class=\"helpTextSmall\"> (recurring)</span>" : "")%></td>
            <td class="label"><%=discountTypeVO.getDescription()%></td>
         </tr>


<%

  if (discountAttributes != null && !discountAttributes.isEmpty())
  {
    Iterator mdaIt = discountAttributes.iterator();
    while (mdaIt.hasNext())
    {
      MembershipDiscountAttribute mda = (MembershipDiscountAttribute)mdaIt.next();
      String discountAttributeLabel = MembershipUIHelper.getDiscountAttributeLabel(clientVO.getClientNumber(),feeTypeCode,discountTypeCode,mda.getMembershipDiscountAttributePK().getAttributeName());
%>
    <tr id="<%=discountLabel%>_attr" style="display:none">
            <td width="">&nbsp;</td>
            <td width="">&nbsp;</td>
            <td width="">&nbsp;</td>
            <td class="label"><%=mda.getAttributeDescription()%>: <input type="text" name="<%=discountAttributeLabel%>" id="<%=discountAttributeLabel%>"/></td>
    </tr>
<%
    }
  }
                  }  // if (!discountTypeVO.isAutomaticDiscount())
               }  // while (availableDiscountListIterator.hasNext())
            }  // if (feeSpecVO.hasNonDiscretionaryDiscounts())
         }  // while (membershipTxFeeListIterator.hasNext())
      }  // if (membershipTxFeeList != null)

%>
          </table>
        </td>
        <td><%=MembershipUIHelper.getTransactionHeadingText(membershipTxVO.getTransactionTypeCode())%> <%=added?"(Added)":""%></td>
        </tr>
<%
   }  // if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_NOT_RENEWED.equals(membershipTxVO.getTransactionTypeCode()))
}  // while (memberTransactionListIterator.hasNext())
%>
        </tbody>
      </table>
        </div>
    </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table cellpadding="5" cellspacing="0" >
      <tr>
        <td><input type="button" name="btnBack" value="Back" onclick="history.back(); return true;"></td>
        <td><input type="button" onclick="document.forms[0].submit();" name="btnNext" value="Next"></td>
      </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html>
