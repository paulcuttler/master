<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%

String saveEvent     = "maintainFeeSpecification_btnSave";
String deleteEvent   = "maintainFeeSpecification_btnDelete";

String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;
AdminCodeDescription ad = null;
MembershipRefMgr memRefMgr = MembershipEJBHelper.getMembershipRefMgr();

Collection feeSpecList = (Collection) request.getSession().getAttribute("FeeSpecificationList");
request.setAttribute("FeeSpecificationList",feeSpecList);

String origCode = (String) request.getSession().getAttribute("origCode");
String statMsg  = (String) request.getAttribute("statusMessage");
String refresh  = (String) request.getAttribute("Refresh");

DateTime today            = new DateTime();
DateTime nextYear         = new DateTime().add(new Interval(1,0,0,0,0,0,0));

String membershipType     = "";
String productType        = "";
String productBenefitCode = "";
String transactionType    = "";
String feeType            = "";
int    groupNo            = 0;

boolean first             = true;
ArrayList feeList         = null;
HashMap cell              = null;
ArrayList rowList         = new ArrayList();

DateTime maxToDate        = new DateTime("01/01/2001");
DateTime thisToDate       = null;
DateTime maxFromDate      = new DateTime("01/01/2001");
DateTime thisFromDate     = null;

if ( !(feeSpecList == null) )
{

   Iterator<FeeSpecificationVO> it = feeSpecList.iterator();
   while ( it.hasNext() )
   {
      FeeSpecificationVO fsVO =  it.next();
      if ( first )
      {
         membershipType     =  fsVO.getMembershipTypeCode();
         productType        =  fsVO.getProductCode();
         productBenefitCode =  fsVO.getProductBenefitCode();
         transactionType    =  fsVO.getTransactionTypeCode();
         feeType            =  fsVO.getFeeTypeCode();
         groupNo            =  fsVO.getGroupNumber();

         if ( productType == null )
         {
            productType = "[pb]" + productBenefitCode;
         }
         else
         {
            productType = "[p]" + productType;
         }
         first              =  false;
      }

      feeList    =  new ArrayList();
      cell = new HashMap();
      cell.put("rowId",fsVO.getFeeSpecificationID().toString());
      feeList.add(cell);

      cell = new HashMap();
      cell.put("align","right");
      cell.put("width","13%");
      cell.put("value",fsVO.getFeeSpecificationID().toString());
      feeList.add(cell);

      thisFromDate = fsVO.getEffectiveFromDate();
      if ( thisFromDate==null )
      {
         thisFromDate = new DateTime("01/01/2001");
      }
      if ( thisFromDate.afterDay(maxFromDate) )
      {
         maxFromDate = thisFromDate;
      }

      cell = new HashMap();
      cell.put("align","right");
      cell.put("width","13%");
      cell.put("value",HTMLUtil.formatShortDate(fsVO.getEffectiveFromDate()));
      feeList.add(cell);

      thisToDate = fsVO.getEffectiveToDate();
      if ( thisToDate==null )
      {
         thisToDate = new DateTime("01/01/2001");
      }
      if ( thisToDate.afterDay(maxToDate) )
      {
         maxToDate = thisToDate;
      }

//      cell = new HashMap();
//      cell.put("align","right");
//      cell.put("width","13%");
//      cell.put("value",HTMLUtil.formatShortDate(fsVO.getEffectiveToDate()));
//      feeList.add(cell);


      cell = new HashMap();
      cell.put("align","right");
      cell.put("width","13%");
      cell.put("value",NumberUtil.formatValue(fsVO.getFeePerMember()));
      feeList.add(cell);

      cell = new HashMap();
      cell.put("align","right");
      cell.put("width","13%");
      cell.put("value",NumberUtil.formatValue(fsVO.getMinimumFee()));
      feeList.add(cell);

      cell = new HashMap();
      cell.put("align","right");
      cell.put("width","13%");
      cell.put("value",NumberUtil.formatValue(fsVO.getAdditionalMemberFee()));
      feeList.add(cell);

      cell = new HashMap();
      cell.put("align","right");
      cell.put("width","13%");
      cell.put("value",NumberUtil.formatValue(fsVO.getFeePerVehicle()));
      feeList.add(cell);

      rowList.add(feeList);
   }
}

String nextFromDate = null;
if ( maxFromDate.add(new Interval(0,0,1,0,0,0,0)).beforeDay(today) )
{
   nextFromDate = today.formatShortDate();
}
else
{
   nextFromDate = today.add(new Interval(0,0,1,0,0,0,0)).formatShortDate();
}

String nextToDate = null;
if ( maxToDate.beforeDay(nextYear) )
{
   nextToDate = nextYear.formatShortDate();
}
else
{
   nextToDate   = maxToDate.formatShortDate();
}

Collection feeTypeList = memRefMgr.getFeeTypeList();
SortedSet sFeeTypeList = new TreeSet();
if ( !(feeTypeList ==null ) )
{
   Iterator it = feeTypeList.iterator();
   while(it.hasNext())
   {
      FeeTypeVO f = (FeeTypeVO) it.next();
      ad = new AdminCodeDescription(f.getFeeTypeCode(),f.getFeeTypeCode());
      sFeeTypeList.add(ad);
   }
}

Collection productList = memRefMgr.getProductList();
SortedSet sProductList = new TreeSet();
if ( !(productList == null ) )
{
   Iterator it = productList.iterator();
   while(it.hasNext())
   {
      ProductVO f = (ProductVO) it.next();
      ad = new AdminCodeDescription("[p]"+f.getProductCode(),f.getProductCode() + " [p]");
      sProductList.add(ad);
   }
}

Collection productBenefitList = memRefMgr.getProductBenefitTypeList();
if ( !(productBenefitList ==null ) )
{
   Iterator it = productBenefitList.iterator();
   while(it.hasNext())
   {
      ProductBenefitTypeVO f = (ProductBenefitTypeVO) it.next();
      ad = new AdminCodeDescription("[pb]"+f.getProductBenefitCode(),f.getProductBenefitCode() + " [pb]");
      sProductList.add(ad);
   }
}

Collection membershipTypeList = memRefMgr.getMembershipTypeList();
SortedSet sMembershipTypeList = new TreeSet();
if ( !(membershipTypeList ==null ) )
{
   Iterator it = membershipTypeList.iterator();
   while(it.hasNext())
   {
      MembershipTypeVO f = (MembershipTypeVO) it.next();
      ad = new AdminCodeDescription(f.getMembershipTypeCode(),f.getMembershipTypeCode());
      sMembershipTypeList.add(ad);
   }
}

Collection transactionTypeList = memRefMgr.getMembershipTransactionTypeList();
SortedSet sTransactionTypeList = new TreeSet();
if ( !(transactionTypeList ==null ) )
{
   Iterator it = transactionTypeList.iterator();
   while(it.hasNext())
   {
      MembershipTransactionTypeVO f = (MembershipTransactionTypeVO) it.next();
      ad = new AdminCodeDescription(f.getTransactionTypeCode(),f.getTransactionTypeCode());
      sTransactionTypeList.add(ad);
   }
}

SortedSet sGroupNoList = new TreeSet();
ad = new AdminCodeDescription("1","One");
sGroupNoList.add(ad);
ad = new AdminCodeDescription("2","Two");
sGroupNoList.add(ad);
ad = new AdminCodeDescription("3","Three");
sGroupNoList.add(ad);

ArrayList headings = new ArrayList();
HashMap hd = new HashMap();
hd.put("align","right");
hd.put("width","13%");
hd.put("value","Id");
headings.add(hd);

hd = new HashMap();
hd.put("align","right");
hd.put("width","13%");
hd.put("value","Effective From");
headings.add(hd);


//hd = new HashMap();
//hd.put("align","right");
//hd.put("width","13%");
//hd.put("value","Effective To");
//headings.add(hd);


hd = new HashMap();
hd.put("align","right");
hd.put("width","13%");
hd.put("value","Fee Per Member");
headings.add(hd);

hd = new HashMap();
hd.put("align","right");
hd.put("width","13%");
hd.put("value","Minimum Fee");
headings.add(hd);

hd = new HashMap();
hd.put("align","right");
hd.put("width","13%");
hd.put("value","Additional Member Fee");
headings.add(hd);

hd = new HashMap();
hd.put("align","right");
hd.put("width","13%");
hd.put("value","Fee Per Vehicle");
headings.add(hd);

%><head>
<title>Maintain FeeSpecification</title>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<script>
function moveToEditRow ( obj )
{
    document.all.inpId.value        = obj.cells[0].innerText;
    document.all.inpFromDate.value  = obj.cells[1].innerText;
    document.all.inpMemFee.value    = obj.cells[2].innerText;
    document.all.inpMinFee.value    = obj.cells[3].innerText;
    document.all.inpAddFee.value    = obj.cells[4].innerText
    document.all.inpVehFee.value    = obj.cells[5].innerText
}

function checkInpFromDate()
{
  var datefield = document.all.inpFromDate;
  if (chkdate(datefield) == false)
  {
    datefield.select();
    alert(" The FROM date is invalid.  Please try again.");
    datefield.focus();
    return false;
  }
  else
  {
    return true;
  }
}
function checkInpToDate()
{
  var datefield = document.all.inpToDate;
  if (chkdate(datefield) == false)
  {
    alert(" The TO date is invalid.  Please try again.");
    datefield.select();
    datefield.focus();
    return false;
  }
  else
  {
    var fromDate = dateFromString(document.all.inpFromDate);
    var toDate = dateFromString(document.all.inpToDate);
    if ( fromDate > toDate)
    {
      alert ( "From date cannot be after To Date");
      document.all.inpFromDate.select();
      document.all.inpFromDate.focus();
      return false;
    }
    return true;
  }
}
function dateFromString ( obj )
{
   flds = obj.value.split("/");
   thisDate = new Date(flds[2],flds[1]-1,flds[0]);
   return thisDate;
}
</script>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body class="rightFrame">
<form name="mainForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>">
  <input type="hidden" name="event" value="">
  <input type="hidden" name="listCode" value ="">
  <input type="hidden" name="refresh" value ="<%=refresh%>">
  <input type="hidden" name="dataChanged" value="no">
  <input type="hidden" name="inpToDate" value="<%=nextToDate%>">
<%

%>
 <div id="mainpage" class="pg">
   <h1>Maintain Fee Specification</h1>

   <div id="contentpage" class="cnt">
<!--
      FEE SPECIFICATION BODY
-->
      <table width="100%" border="0" cellspacing="1">
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="23%">Membership Type Code</td>
          <td width="74%">
          <%
             if ( origCode == null )
             {
          %>
                 <%=HTMLUtil.selectList("selectMemberType",sMembershipTypeList,"Personal","Personal","Personal")%>
          <%
             }
             else
             {
          %>
               <%=HTMLUtil.inputOrReadOnly("selectMemberType",membershipType)%>
          <%
             }
          %>
         </td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="23%">Product/Product Benefit Code</td>
          <td width="74%">
          <%
             if ( origCode == null )
             {
         %>
              <%=HTMLUtil.selectList("selectProduct",sProductList,"Ultimate","","No Product")%>
          <%
             }
             else
             {
          %>
               <%=HTMLUtil.inputOrReadOnly("selectProduct",productType)%>
          <%
             }
          %>
          </td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="23%">Transaction Type Code</td>
          <td width="74%">
          <%
             if ( origCode == null )
             {
         %>
               <%=HTMLUtil.selectList("selectTransaction",sTransactionTypeList,"Default"," ","Default")%>
          <%
             }
             else
             {
          %>
               <%=HTMLUtil.inputOrReadOnly("selectTransaction",transactionType)%>
          <%
             }
          %>

            </td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="23%">Fee Type Code</td>
          <td width="74%">
          <%
             if ( origCode == null )
             {
         %>
            <%=HTMLUtil.selectList("selectFeeType",sFeeTypeList,"Ultimate","Ultimate","Ultimate")%>
          <%
             }
             else
             {
          %>
               <%=HTMLUtil.inputOrReadOnly("selectFeeType",feeType)%>
          <%
             }
          %>
          </td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="23%">Group Number</td>
            <td width="74%">
          <%
             if ( origCode == null )
             {
         %>
              <%=HTMLUtil.selectList("selectGroupNo",sGroupNoList,"1","1","One")%>
          <%
             }
             else
             {
          %>
               <%=HTMLUtil.inputOrReadOnly("selectGroupNo",new Integer(groupNo).toString())%>
          <%
             }
          %>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
          <td colspan="3">
           <table width="100%" border="0">
             <tr>
               <td>
                   <%=HTMLUtil.dataTable("Fee Table",headings,rowList,"DoSometing","DoubleClick","No Fees")%>
               </td>
	     </tr>
          </table>
	 </td>
        </tr>
        <tr>
         <td colspan="3" >

         <div style="height:110;overflow:auto; border-width: 1; border-style: solid;">
        <table width="100%" height="90%" border="0">
          <tr >
           <td>

             <table id="dataEntry" width="100%" height="100%" border="0">
               <tr bgColor="#CDD7E1" >
                  <td width="13%">
                     <input type="text" name="inpId" size="13" readonly value="">
                  </td>
                  <td width="13%">
                     <input type="text" name="inpFromDate" size="13" value="<%=nextFromDate%>" ><!--onblur="checkInpFromDate()"-->
                  </td>
<!--
                  <td width="13%">
                     <input type="text" name="inpToDate" size="13" value="<%=nextToDate%>" onblur="checkInpToDate()">
                  </td>
-->
                  <td width="13%">
                     <input type="text" name="inpMemFee" size="13" value="0.0">
                  </td>
                  <td width="13%">
                     <input type="text" name="inpMinFee" size="13" value="0.0">
                  </td>
                  <td width="13%">
                     <input type="text" name="inpAddFee" size="13" value="0.0">
                  </td>
                  <td width="13%">
                     <input type="text" name="inpVehFee" size="13" value="0.0">
                  </td>
               </tr>
             </table>

           </td>
          </tr>
          <tr >
           <td>

             <table width="90%" height="100%" border="0">
              <div id="buttonPage" class="btn">
               <tr>
                <td width="10%">
                    <%=HTMLUtil.buttonSave(saveEvent,"Fee Specification" )%>
                </td>
                <td>
                    <%=HTMLUtil.buttonDelete(deleteEvent,"Fee Specification" )%>
                </td>
               </tr>
              </div>
             </table>

          </td>
          </tr>

          </table>
      </div>

       </td>
      </tr>
<!--      <tr><td colspan="3"><br></td></tr> -->
      <tr>
      <td colspan="3" align="center">
         <%=HTMLUtil.statusMessage(statMsg)%>
      </td>
      </tr>
      </table>
<!--
   FINISH FEE SPECIFICATION BODY
-->
   </div>

</div>

</form>
</body>
</html>
