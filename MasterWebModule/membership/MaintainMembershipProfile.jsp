<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="java.util.*"%>

<%
String saveEvent     = "maintainProfile_btnSave";
String deleteEvent   = "maintainProfile_btnDelete";
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;

MembershipProfileVO memProfile = (MembershipProfileVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_MEMBERSHIP_PROFILE_VO);
String    origCode  = (String) request.getSession().getAttribute("origCode");
String    statMsg   = (String) request.getAttribute("statusMessage");
String    refresh   = (String) request.getAttribute("Refresh");

MembershipRefMgr refMgr        = MembershipEJBHelper.getMembershipRefMgr();
ArrayList availableDiscounts   = (ArrayList) refMgr.getActiveDiscountTypeList();
DiscountTypeVO discountType    = null;
SortedSet availDiscountList = new TreeSet();
SortedSet selDiscountList = new TreeSet();

String profileCode             = memProfile.getProfileCode();
String abcd = null;
SortedSet sStatusList = new TreeSet();
sStatusList.add(new AdminCodeDescription(MembershipProfileVO.STATUS_Active,MembershipProfileVO.STATUS_Active));
sStatusList.add(new AdminCodeDescription(MembershipProfileVO.STATUS_Inactive,MembershipProfileVO.STATUS_Inactive));

String    description                  = "";
String    status                       = " ";
String    nameSuffix                   = " ";
boolean   adminOnly                    = false;
boolean   allowedInGroup               = false;
Collection discountList                = null;

AdminCodeDescription ad = null;

if (!(memProfile == null))
{
   profileCode       = memProfile.getProfileCode();
  description        = memProfile.getDescription()== null ? " " : memProfile.getDescription();
  status             = memProfile.getProfileStatus();
  adminOnly          = memProfile.isAdminOnly();
  discountList       = memProfile.getDiscountList();
  nameSuffix         = memProfile.getNameSuffix() == null ? " " : memProfile.getNameSuffix();

}
else
{
  profileCode = null;
  discountList = null;
}

if ( !(discountList == null)  )
{
   Iterator it = discountList.iterator();
   while( discountList.size() > 0 & it.hasNext() )
   {
      discountType = (DiscountTypeVO) it.next();
      selDiscountList.add( new AdminCodeDescription( discountType.getDiscountTypeCode(),discountType.getDescription()));
   }
}

if ( !(availableDiscounts == null) )
{
   Iterator it = availableDiscounts.iterator();
   while ( it.hasNext() & availableDiscounts.size() > 0 )
   {
      discountType = (DiscountTypeVO) it.next();
      ad = new AdminCodeDescription( discountType.getDiscountTypeCode(),discountType.getDescription());

      if ( !(selDiscountList==null) & !selDiscountList.contains(ad) )
      {
         availDiscountList.add(ad);
      }
   }
}
%>
<html>
<head>
<title>Maintain Membership Profile</title>
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
</head>
<body class="rightFrame" onload="refreshList(document.all.refresh,'<%=listPage%>');">

<BR>
<form name="mainForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>">
  <input type="hidden" name="event" value="">
  <input type="hidden" name="listCode" value="">
  <input type="hidden" name="refresh" value ="<%=refresh%>">

 <div id="mainpage" class="pg">
 <h1>Maintain Membership Profile</h1>
   <div id="contentpage" class="cnt">
      <!--
         START PROFILE BODY
      -->
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
         <tr>
          <td class="label" width="20%">Profile Code:</td>
          <td class="dataValue" width="80%">
            <%=HTMLUtil.inputOrReadOnly("profileCode",profileCode)%>
          </td>
         </tr>
         <tr>
          <td class="label" width="20%">Description:</td>
          <td class="dataValue" width="80%">
            <input type="text" name="description" value="<%=description%>">
          </td>
         </tr>
         <tr>
          <td class="label" width="20%">Card Name Suffix</td>
          <td class="dataValue" width="80%">
            <input type="text" name="nameSuffix" value="<%=nameSuffix%>">
          </td>
         </tr>
         <tr>
          <td class="label" width="20%">Admin Only</td>
          <td class="dataValue" width="80%">
            <input type="checkbox" name="adminOnly" value="true" <% if (adminOnly) {out.print("checked");} %>>
          </td>
         </tr>
         <tr>
          <td class="label" width="20%">Profile Status:</td>
          <td class="dataValue" width="80%">
              <%=HTMLUtil.selectList("profileStatus",sStatusList,status,MembershipProfileVO.STATUS_Active,MembershipProfileVO.STATUS_Active)%>
          </td>
         </tr>
         <tr>
          <td colspan="2">
              <%=HTMLUtil.codeDescriptionTable (  "Allowable Discounts.",
                                                  availDiscountList,
                                                  "maintainProfileDiscount_btnAdd",
                                                  "Double click to ADD this discount.",
                                                  "No Discounts to be selected"
                                               )
             %>
          </td>
         </tr>
         <tr>
          <td colspan="2" >
            <%=HTMLUtil.codeDescriptionTable (  "Profiles Discount List.",
                                                selDiscountList,
                                                "maintainProfileDiscount_btnRemove",
                                                "Double click to REMOVE this discount.",
                                                "No Discounts selected"
                                             )
            %>
          </td>
         </tr>
      </table>
      <!--
         FINISH PROFILE BODY
      -->
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSaveAndDelete(saveEvent,deleteEvent,"Profile", statMsg )%>
   </div>
</div>
</form>
</body>
</html>
