<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.extracts.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
<%@ page import="java.lang.reflect.*"%>

<%
Hashtable params       = (Hashtable) request.getSession().getAttribute("params");
String pageTitle       = (String) params.get("TITLE");
String mailTo          = (String) params.get("EMAIL");

String ExtractOrReport = (String) request.getSession().getAttribute("ExtractOrReport");
String okEvent         = ExtractOrReport + "_okButton";
String cancelEvent     = ExtractOrReport + "_cancelButton";
boolean isExtract      = (ExtractOrReport.toLowerCase().indexOf("extract") > -1);
String rowHeight       = null;
String jobType         = null;
if ( isExtract )
{
   jobType = "Extract";
   rowHeight = "85%";
}
else
{
   jobType = "Report";
   rowHeight = "75%";
}

String submitMessage = "Submit " + jobType;
String cancelMessage = "Cancel " + jobType;

//     Get the e-mail addressess
SortedSet emails = new TreeSet();
StringTokenizer mailTokens = new StringTokenizer(mailTo, ",");
String defEmail = null;
String defEmailDesc = null;

while ( mailTokens.hasMoreTokens() )
{
   String em =  mailTokens.nextToken();
   String emf = StringUtil.toProperCase(StringUtil.replace(em.substring(0,em.indexOf("@")),"."," "));

   if ( defEmail == null)
   {
      defEmail = em;
      defEmailDesc = emf;
   }
   emails.add(new AdminCodeDescription(em,emf));
}

Vector pVector = ExtractHelper.getHTMLParameters(request);
%>
<html>
<head>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<script language="javascript">

function setRunDate ()
{
   if ( document.all.jobType.value=="Report" )
   {
      return true;
   }

   ddate = new Date;
   dday = ddate.getDate();
   dmonth = ddate.getMonth() + 1;
   dyear = ddate.getYear();

   document.all.runDate.value= formatNumber(dday,2) + "/" + formatNumber(dmonth,2) + "/" + formatNumber(dyear,2);

   dTime = ddate.getTime();
   dTime = dTime + (2 * 60 * 1000); // Add 2 minutes

   tDate = new Date();
   tDate.setTime(dTime);

   document.all.runHours.value = formatNumber(tDate.getHours(),2);
   document.all.runMins.value = formatNumber(tDate.getMinutes(),2);

}
function formatNumber ( num,digits )
{
   s  = new String(num);
   sl = s.length;
   z  = digits - sl;

   return "0000000000".substr(0,z) + s;
}

function checkHours(thing)
{
    val = thing.value * 1;
    if(val>23 || val<0)
    {
       document.all.runHours.focus();
       alert("Hours must be from 0 to 23");
       return false;
    }
    else return true;
}

function checkMins(thing)
{
   val = thing.value * 1;
   if(val > 59 || val < 0)
   {
      document.all.runMins.focus();
      alert("Minutes must be between 0 and 59");
      return false;
   }
   else return true;
}


function validate()
{
   //
   // Don't bother to validate the run time if it is a report
   // It will run straight away
   //
   if ( document.all.jobType.value=="Report" )
   {
      return true;
   }

   if(document.all.event.value=="Cancel")
   {
      return true;
   }

   rd = document.all.runDate.value;
   rde = rd.split("/");

   hours = document.all.runHours.value * 1;
   mins = document.all.runMins.value * 1;

   newRunDate = new Date(rde[2],rde[1]-1,rde[0],hours,mins,0);
   newRunTime = newRunDate.getTime();
   thisDate = new Date();
   thisTime = thisDate.getTime();

   if(hours>23 || hours < 0 || mins<0 || mins>59)
   {
      alert("Enter a valid date and time");
      document.all.runHours.focus();
      return false;
   }
   else if ( newRunTime <= thisTime )
   {
      alert ("Please enter a time in the future");
      document.all.runDate.focus();
      return false;
   }
   else
   {
      return true;
   }
}

function checkSelectLists ( )
{
   var selList = document.getElementsByTagName("select");

   for ( var i = 0; i < selList.length; i++ )
   {

        thisList = selList[i];

        if ( (thisList[0].value == "ALL") && thisList[0].selected )
        {
            for ( var t=1;t < thisList.length; t++)
            {
               thisList[t].selected = true;
            }
         }

        thisList[0].selected = false;
    }
}


   function submitForm(eventName)
   {
      document.all.event.value = eventName;
      checkSelectLists();
      var valid = validate();
      if (valid)
      {
        document.all.mainForm.submit();
      }
   }

</script>
<title>
RoadSide Extracts
</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body onload="setRunDate();">
<form name = "mainForm" method="post" action="<%= request.getContextPath() + "/MembershipReportsUIC"%>">

<input type="hidden" name="event" value="">
<input type="hidden" name="jobType" value="<%=jobType%>">

<div id="mainpage" class="pg">

<h1 ><%=pageTitle%></h1>

<div id="contentpage" class="cnt">
   <table border="0" width="80%">
   <tr valign="top"><!--PAGE_ROADSIDE_EXTRACTS-->
       <td>
         <table border="0" cellpadding="3" cellspacing="3" width="100%">
         <%
            AdminCodeDescription ad = null;
            for (int i=0;i < pVector.size();i++)
            {
              ad = (AdminCodeDescription) pVector.get(i);
         %>
               <tr>
                  <td class="label" width="20%" valign="top" >
                     <%=ad.getCode()%>
                  </td>
                  <td class="dataValue" valign="top">
                     <%=ad.getDescription()%>
                  </td>
               </tr>
         <%
            }
         %>
         </table>
       </td>
  </tr>
  <% if ( isExtract )
     {
  %>
      <tr valign="bottom" >
        <td>
         <table border="0" width="100%">
            <tr >
             <td align="right" class="label" valign="bottom" width="20%">Schedule to run on</td>
             <td class="dataValue" valign="bottom" >
               <input type = "text" name = "runDate" size = "11" value = ""
               onkeyup = "javascript:checkDate(this)">
               &nbsp at &nbsp
               <input type = "text" name = "runHours" size = "2" value = ""
                  onkeyup = "javascript:checkInteger(this)"
                  onblur = "javascript:checkHours(this)">
               :
               <input type = "text" name = "runMins" size = "2" value = ""
                  onkeyup = "javascript:checkInteger(this)"
                  onblur = "javascript:checkMins(this)">
             </td>
            </tr>
            <tr>
               <td align="right" class="label" width="20%" valign="bottom">Mail to </td>
               <td class="dataValue" valign="bottom" ><%=HTMLUtil.selectList("email",emails,defEmail,defEmail,defEmailDesc)%></td>
            </tr>
           </table>
           </td>
         </tr>
   <%
   }
   else
   {
   %>
         <tr>
            <td colspan="2">
               <table>
                  <%@ include file="/common/reporting/SelectReportDeliveryMethodInclude.jsp"%>
               </table>
            </td>
         </tr>
   <%
   }
   %>
   </table>
   </div>
   <div id="buttonPage" class="btn">
      <table border="0" >
      <tr>
      <td valign="top">
          <%=HTMLUtil.buttonOk( okEvent ,submitMessage)%>
      </td>
      <td valign="top">
          <%=HTMLUtil.buttonCancel(cancelEvent,cancelMessage)%>
      </td>
      </tr>
      </table>
   </div>
</div>
</form>
</body>
</html>
