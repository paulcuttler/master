<%@ page import="java.util.*"%>
<%@ page import="java.math.*"%>

<%@ page import="java.text.NumberFormat"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.club.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>
<%
ClientVO clientVO = (ClientVO) request.getAttribute("client");
request.setAttribute("client",clientVO);
SortedSet clubList = (SortedSet) request.getAttribute("clubList");
%>

<html>
<head>
<title>Select Affiliated Club</title>
<%=CommonConstants.getRACTStylesheet()%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/PageMovement.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Utilities.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<script language="Javascript">
//join dates can't be in the future
function validateJoinDate(obj)
{
  var now = new Date();
  var joinDate = getDate(obj);
  if (joinDate > now)
  {
    alert("Join dates may not be in the future.");
    obj.focus();
    return;
  }
}
//expiry date can't be in the past
function validateExpiryDate(obj)
{
  var now = new Date();
  var expiryDate = getDate(obj);
  if (expiryDate != null)
  {
    var threeMonths = addMonthsToDate(expiryDate,3);//3 months expired
//    alert('threeMonths='+threeMonths);
//    alert('expiryDate='+expiryDate);    
    if (threeMonths <= now) 
    {
      alert("The expiry date must up to three months expired or in the future.");
      obj.focus();
      return;
    }
  }
}
</script>


</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form name="mainForm" method="post" action="<%= MembershipUIConstants.PAGE_MembershipUIC%>">

<input type="hidden" name="event" value="">
<input type="hidden" name="listCode" value="">
<input type="hidden" name="clientNumber" value="<%=clientVO.getClientNumber().toString()%>">

<tr valign="top">
   <td>
     <h1><%@ include file="/help/HelpLinkInclude.jsp"%> Select Affiliated Club</h1>
        <table border="0" cellpadding="3" cellspacing="0">
          <tr valign="top">
            <td colspan="2">
            <!--client details-->
            <%@ include file="/client/ViewClientInclude.jsp"%>
            </td>
          </tr>
          <tr>
            <td class="label">Club name:</td>
              <td><%=HTMLUtil.selectList("clubCode",clubList,"","","","")%></td>
          </tr>
          <tr>
            <td class="label">Product Level:</td>
              <td><select name="productLevel">
                <option value="<%=ProductVO.AFFILIATE_PRODUCT_LEVEL_BASIC%>" selected="selected"><%=ProductVO.AFFILIATE_PRODUCT_LEVEL_BASIC%></option>
                <option value="<%=ProductVO.AFFILIATE_PRODUCT_LEVEL_PREMIUM%>"><%=ProductVO.AFFILIATE_PRODUCT_LEVEL_PREMIUM%></option>
                </select></td>
              <td class="helpTextSmall">The membership product level.</td>
          </tr>

          <tr>
            <td class="label">Membership Id:</td>
              <td><input type="text" name="clubMembershipId" value=""></td>
              <td class="helpTextSmall">Enter the membership Id of the affiliated club.</td>
          </tr>
          <tr>
            <td class="label">Join date:</td>
            <td><input type="text" name="clubJoinDate"
                onblur="checkdate(this);validateJoinDate(this);" value=""></td>
            <td class="helpTextSmall">Enter the date client joined affiliated
              club. Use the format dd/mm/yyyy.</td>
          </tr>
          <tr>
            <td class="label">Expiry date:</td>
             <td><input type="text" name="clubExpiryDate"
                onblur="checkdate(this);validateExpiryDate(this);" value=""></td>
             <td class="helpTextSmall">Enter the expiry date of the affiliated
              club membership. Use the format dd/mm/yyyy.</td>
            </td>
          </tr>
        </table>
   </td>
</tr>
<tr>
   <td valign="bottom">
      <table border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td>
          <%=HTMLUtil.buttonBack("historyBack()","Previous")%>
          <%=HTMLUtil.buttonNext("selectAffiliatedClub_btnNext","Next")%>
	</td>
      </tr>
      </table>
   </td>
</tr>
  </form>
</table>
</body>
</html>
