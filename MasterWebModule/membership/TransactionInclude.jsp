<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.MembershipUIConstants"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%

   MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();

   TransactionGroup transGroup = (TransactionGroup) request.getAttribute("transactionGroup");

   if (transGroup == null)
   {
      throw new ServletException("The transaction is no longer valid. Please log in again.");
   }

%>

