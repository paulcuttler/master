<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.payment.directdebit.*"%>
<%@ page import="com.ract.payment.bank.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.security.*"%>
<%@page import="com.ract.user.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<%
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
String ddConfirmMessage = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,SystemParameterVO.DD_CONFIRM_MESSAGE);

PaymentMgr payMgr = PaymentEJBHelper.getPaymentMgr();
Collection paymentTypeList = payMgr.getPaymentTypeListForMembership();

String ddPolicyMessage = (String)request.getAttribute("ddPolicyMessage");

TransactionGroup transGroup = (TransactionGroup) request.getAttribute("transactionGroup");
if (transGroup == null)
{
   throw new ServletException("The transaction is no longer valid.");
}

MembershipTransactionVO primeMemTxVO =  transGroup.getMembershipTransactionForPA();
LogUtil.log(this.getClass(),"primeMemTxVO="+primeMemTxVO);
String transactionTypeCode = primeMemTxVO.getTransactionTypeCode();
MembershipVO primeMemVO = primeMemTxVO.getNewMembership();

DirectDebitAuthority ddAuthority = transGroup.getDirectDebitAuthority();

LogUtil.log(this.getClass(),"ddAuthority" + ddAuthority); 

String accountName = "";
String accountNumber = "";
boolean approved = false;
String bsbNumber = "";
String cardType = "";
String creditCardExpiry = "";
String creditCardNumber = "";
String dayToDebit = "";
String frequency = null;
boolean isOwner = false;
String paymentTypeCode = PaymentTypeVO.PAYMENTTYPE_CASH;
String verificationNumber = "";
boolean autoRenew = true;
boolean isAdministrator = userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN);
String ccString = null;

DateTime today = new DateTime();
Calendar cal = Calendar.getInstance();
cal.setTime(today);
dayToDebit = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));

if (ddAuthority != null)
{
   BankAccount bankAccount = ddAuthority.getBankAccount();
   isOwner = ddAuthority.isClientIsOwner();
   dayToDebit = Integer.toString(ddAuthority.getDayToDebit());
   approved = ddAuthority.isApproved();
   DirectDebitFrequency ddFrequency = ddAuthority.getDeductionFrequency();
   frequency = ddFrequency.getDescription();
   accountName = bankAccount.getAccountName();
   autoRenew = transGroup != null ? transGroup.isAutomaticallyRenewable() : false;

   if (bankAccount instanceof DebitAccount)
   {
      DebitAccount debitAccount = (DebitAccount) bankAccount;
      bsbNumber = debitAccount.getBsbNumber();
      accountNumber = debitAccount.getAccountNumber();
      paymentTypeCode = PaymentTypeVO.PAYMENTTYPE_DIRECTDEBIT;
   }
   else if (bankAccount instanceof CreditCardAccount)
   {
      CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
      cardType = ccAccount.getCardTypeDescription();
      creditCardNumber = ccAccount.getCardNumber();
      creditCardExpiry = ccAccount.getCardExpiry();
      verificationNumber = ccAccount.getVerificationValue();
      paymentTypeCode = ccAccount.getPaymentType().getPaymentTypeCode();
      HttpSession sess = request.getSession();
      sess.setAttribute("oldCardNumber", creditCardNumber);
      ccString = StringUtil.obscureFormattedCreditCardNumber(creditCardNumber);
   }
}


%>
<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
<title>Select Payment Method</title>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Validation.js">
</script>

<script language="JavaScript" src="/payment/scripts/Validation.js?v=1.0">
</script>

<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js">
</script>

<script language="JavaScript">
var paymentCashTypeList = new Array();  // Maps payment type codes to cash type codes
var ptCCNumberSizeList = new Array();   // Maps payment type codes to credit card number lengths
<%
PaymentTypeVO payType;
Iterator paymentTypeListIterator = paymentTypeList.iterator();
while (paymentTypeListIterator.hasNext())
{
   payType = (PaymentTypeVO) paymentTypeListIterator.next();
   out.println("paymentCashTypeList['"+ payType.getPaymentTypeCode() +"'] = '" + payType.getCashTypeCode() + "';");
   out.println("ptCCNumberSizeList['"+ payType.getPaymentTypeCode() +"'] = " + payType.getCardNumberLength() + ";");
}
if(isAdministrator)
{
   out.println("paymentCashTypeList['"+ PaymentTypeVO.PAYMENTTYPE_REPAIR + "']='"+ PaymentTypeVO.CASHTYPE_REPAIR + "'");
   out.println("ptCCNumberSizeList['"+ PaymentTypeVO.PAYMENTTYPE_REPAIR + "'] = 0;");
}
%>
</script>
</head>
<body onload="handleOnLoad();">
<%
// for easily adjusting column widths
String indent = "5";
double txPayableFee = transGroup.getAmountPayable();
ClientVO primeAddressee = transGroup.getClientForPA();
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>" onsubmit="return validateForm();">
<input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
<input type="hidden" name="event" value="selectPaymentMethod_btnSave">
<input type="hidden" name="autoRenew" value="Yes">
<tr valign="top">
<td>
<h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Select Payment Method</h1>
<table border="0" cellpadding="3" cellspacing="0" >
   <tr>
      <td class="dataValue" colspan=2 ><%=primeAddressee.getClientNumber()%></td>
      <td class="dataValue" colspan=2 ><%=primeAddressee.getDisplayName()%></td>
      <td class="label">Amount Payable:</td>
      <td class="dataValue"><%=CurrencyUtil.formatDollarValue(txPayableFee)%></td>
   </tr>
</table>
<table border="0" cellpadding="3" cellspacing="0" >
   <tr>
      <td valign="top">Payment Method:</td>
      <td>
<%=MembershipUIHelper.getPaymentTypeHTML(paymentTypeList,paymentTypeCode,transactionTypeCode,primeMemVO,transGroup,isAdministrator,false)%>
      </td>
      <td><img width="5" src="<%=CommonConstants.DIR_IMAGES%>/clear.gif">
      </td>
      <td valign="top">
        <input type="button" onclick="submitPaymentMethodForm('<%=ddConfirmMessage%>','<%=ddPolicyMessage%>');" value="Save" alt="Save the transaction"/>
      </td>
   </tr>
</table>
<table id="repairTable" cellpadding="3" cellspacing="0">
   <tr>
      <td class="label">Client:</td>
      <td class="dataValue" colspan="2"><%=primeAddressee.getClientNumber()%>&nbsp;&nbsp;<%=primeAddressee.getDisplayName()%></td>
   </tr>
   <tr>
      <td>Receipt No.:</td>
      <td><input type="text" name="sequenceNumber" size="10" maxlength="10" ></td>
      <td class="helpTextSmall">Enter the receipting system reference number (oi-payable sequence no in OCR)</td>
   </tr>
</table>
<table id="directDebitTable" style="display:none" cellpadding="3" cellspacing="0" >
   <tr id="ddFromDebitAccountHeadingRow">
      <td colspan="3" >
         <h3>Direct Debit from debit account:</h3>
      </td>
   </tr>
   <tr id="ddFromCCAccountHeadingRow" style="display:none">
      <td colspan="3" >
         <h3>Direct Debit from credit card account:</h3>
      </td>
   </tr>
   <tr id="bsbNumberRow">
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">BSB No:</td>
      <td><input type="text" name="bsbNumber" size="10" value="<%=bsbNumber%>" onchange="return validateBsbNumber(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%></td>
   </tr>
   <tr id="accountNumberRow">
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Account No:</td>
      <td><input type="text" name="accountNumber" size="20" value="<%=accountNumber%>" onchange="return validateAccountNumber(this);"/>&nbsp;<%=HTMLUtil.mandatoryItem()%></td>
   <tr>
   <tr id="cardNumberRow" style="display:none">
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Card No:</td>
      <td>
        <input type="hidden" name="oldCardNumber" value="<%=ccString%>"/> 
        <input type="text" name="cardNumber" size="20" value="<%=ccString%>" />&nbsp;<%=HTMLUtil.mandatoryItem()%>
          &nbsp;<input class="helpTextSmall" readonly type="text" id="cardNumberLength" size="10" style="border-style: none;" >
      </td>
   </tr>
   <tr id="cardExpiryRow" style="display:none">
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Card expiry:</td>
      <td><input type="text" name="cardExpiry" size="5" value="<%=creditCardExpiry%>" onchange="return validateCardExpiry(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%>&nbsp;<span class="helpTextSmall">Use the format mm/yy</span></td>
   </tr>
   <tr id="cardVerificationValueRow" style="display:none">
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Card verification value:</td>
      <td><input type="text" name="cardVerificationValue" value="<%=verificationNumber%>" size="5" /></td>
   </tr>
   <tr>
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Account Name:</td>
      <td><input type="text" name="accountName" size="20" value="<%=accountName%>" onchange="return validateAccountName(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%></td>
   <tr>
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Client is Account Owner:</td>
      <td><input type="checkbox" name="isOwner"  value="Yes" <%=(isOwner ? "checked" : "")%>/></td>
   <tr>
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Deduction Frequency:</td>
      <td>
        <%=MembershipUIHelper.getFrequencyHTML(transGroup.getActualGroupMemberCount(),frequency, primeMemVO.getProductCode())%>
      </td>
   </tr>
   <tr>
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Day of month to debit:</td>
      <td><input type="text" name="dayToDebit" value="<%=dayToDebit%>" size="3" onchange="return validateDayToDebit(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%>&nbsp;<span class="helpTextSmall">Numbers 1 to 31 only.</span></td>
   </tr>
</table>
<table id="receiptingTable" style="display:none" cellpadding="3" cellspacing="0">
</table>
</td>
</tr>
<tr valign="bottom">
   <td>
      <table cellpadding="5" cellspacing="0" >
         <tr>
            <td>
              <input type="button" onclick="history.back();" value="Back"/>
            </td>
         </tr>
      </table>
   </td>
</tr>
</form>
</table>
</body>

</html>
