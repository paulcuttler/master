<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ include file="/membership/TransactionInclude.jsp"%>

<%@include file="/common/NoCache.jsp"%>

<%
Collection reqList = null;
Iterator reqListIT = null;
MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");
ReferenceDataVO refData = null;

// get the list of reasons for card request
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
reqList = commonMgr.getReferenceDataListByType(ReferenceDataVO.REF_TYPE_MEMBERSHIP_CARD_REQUEST);
reqListIT = reqList.iterator();
%>

<html>
<head>
  <title>Request New Membership Card</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <form method="post" name="membershipCardRequest" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="membershipCardRequest_btnSave">
  <input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
  <tr valign="top">
    <td>
      <h2>Request New Membership Card</h2>
<%
      if (!reqList.isEmpty())
      {
%>
      <h3>Card History</h3>

      <%@include file="/membership/ViewMembershipCardInclude.jsp"%>

      <hr/>

      <table border="0" cellspacing="0" cellpadding="0">
        <tr><p>
          <td class="label">Reason for new card &nbsp;</td>
          <td>
            <select name="selReason">
<%

                while (reqListIT.hasNext())
                {
                  refData = (ReferenceDataVO) reqListIT.next();
                  if ( refData.isActive() )
                  {
%>
                     <option value="<%=refData.getReferenceDataPK().getReferenceCode()%>">
                             <%=refData.getDescription()%>
                     </option>
<%
                  }
                }
%>
                </select>
          </td>
            <td>&nbsp;</td>
        </tr>
      </table>
<%
      }
%>

    </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellspacing="0" cellpadding="5" width="28">
        <tr>
          <td>
              <input type="button" name="btnBack" value="Back" onclick="history.back();">
          </td>
<%
      if (!reqList.isEmpty())
      {
%>
          <td> <button type="submit" name="btnSave" value="Save">Save</button>
          </td>
<%
      }
%>
        </tr>
      </table>
    </td>
  </tr>

      </form>
</table>
</body>
</html>
