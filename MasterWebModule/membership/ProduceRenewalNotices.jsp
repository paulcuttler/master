<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="java.util.*"%>
<%--
Requests parameters for the Producing Renewal Notices

request attributes:

returned parameters:
   mailFileName
   deliveryFileName
   errorFileName
   runDate
   runHours
   runMins

--%>
<%
boolean allowRecall;
boolean allowExtract;
boolean allowPublish;
boolean allowUndo;
boolean selectNext;
%>
<html>
<head>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<script language="javascript">

//function setRunDate ()
//{
//   ddate = new Date;
//   dday = ddate.getDate();
//   dmonth = ddate.getMonth() + 1;
//   dyear = ddate.getYear();
//   document.all.runDate.value= dday + "/" + dmonth + "/" + dyear;
//}

function checkHours(thing)
{
    val = thing.value * 1;
    if(val>23 || val<0)
    {
       document.all.runHours.focus();
       alert("Hours must be from 0 to 23");
       return false;
    }
    else return true;
}

function checkMins(thing)
{
   val = thing.value * 1;
   if(val > 59 || val < 0)
   {
      document.all.runMins.focus();
      alert("Minutes must be between 0 and 59");
      return false;
   }
   else return true;
}


function validate(thing)
{
   if(document.all.event.value=="RenewalNoticesRun")
   {
      hours = document.all.runHours.value * 1;
      mins = document.all.runMins.value * 1;
      if(hours>23 || hours < 0 || mins<0 || mins>59)
      {
         alert("Enter a valid date and time");
         return false;
      }
      else return true;
   }
   else return true;
}


function submitForm(eventName)
   {
      if(eventName=='RenewalNoticesCancel')
      document.all.event.value=eventName;
      var valid = validate(eventName);
      if (valid)
      {
        document.all.DoRenewalRun.submit();
      }
   }
function setOption(thing)
{
   if(thing.value=="Renewals")
   {
      dates.style.display='';
       batchList.style.display='none';
       runParameters.style.display='none';
       descText.innerHTML="Do a renewal run for a specified period, and produce Notices, Advices, Reminders and Final Notices.";
       document.all.event.value="RenewalNoticesRun";
   }
   else if(thing.value=="SetParameters")
   {
      dates.style.display="none";
       batchList.style.display='none';
       runParameters.style.display='';
       descText.innerHTML="Set run parameters for automatic run, as well as report destinations";
       document.all.event.value="RenewalNoticesSaveParameters";
   }
   else if(thing.value=="PreviousRuns")
   {
       batchList.style.display='';
       runParameters.style.display='none';
       dates.style.display='none';
       descText.innerHTML="Look up a previous renewal run";
       document.all.event.value="RenewalNoticesViewPrevious";
   }
   else
   {
      alert("Option " + thing.value + " is not recognised");
   }
}
function doBatchNo(thing)
{
   document.all.runNumber.value=thing;
   document.all.DoRenewalRun.submit();
}
</script>
<%

Vector batchList = (Vector)request.getAttribute("batchList");
DateTime lastDate = (DateTime)request.getAttribute("lastDate");
DateTime firstDate = (DateTime)request.getAttribute("firstDate");
String emailTo = (String)request.getAttribute("emailTo");
String ccTo    = (String)request.getAttribute("ccTo");
String autoRunStr = (String)request.getAttribute("autoRun");
String nextProjectedRunDate = null;
RenewalBatchVO batchVO=null;
boolean autoRun = false;
Boolean alreadyRun = (Boolean) request.getAttribute("alreadyRun");

if(firstDate==null)
{
   firstDate = new DateTime();
   try
   {
      lastDate = firstDate.add(new Interval(0,0,42,0,0,0,0));
   }
   catch(Exception e)
   {
   }
}
if(autoRunStr==null)
{
   autoRun=false;
}
else
{
   autoRun = autoRunStr.equals("true");
}
if(batchList.size()>0)
{
   batchVO = (RenewalBatchVO)batchList.elementAt(0);
   nextProjectedRunDate = DateUtil.formatShortDate(batchVO.getRunDate().add(new Interval(0,0,1,0,0,0,0)));
}
else
{
   nextProjectedRunDate="";
}
%>
<title>
produceRenewalNotices
</title>
<%=CommonConstants.getRACTStylesheet()%>
<link rel="stylesheet" href="../css/ract.css" type="text/css">
</head>
<body >
<form name = "DoRenewalRun" method="post" action="<%= request.getContextPath() + "/MembershipReportsUIC"%>">
<input type="hidden" name="event" value="<%=alreadyRun.booleanValue()?"RenewalNoticesSaveParameters":"RenewalNoticesRun"%>">
  <h1>Renewal Notices</h1>

  <table width="510" border="0" cellspacing="0" cellpadding="2">
<%if(!alreadyRun.booleanValue())
{%>
    <tr>
      <td width="249" height="25" >
        <div align="right">Create renewals</div>
    </td>
      <td width="261" height="25">
        <input type="radio" name="option" value="Renewals" checked
                 onclick="javascript:setOption(this)">
    </td>
    </tr>
<%}
else
{%>
    <tr>
      <td width="249" height="25" >
        <div align="right">A renewal run has already been started today</div>
    </td>
      <td width="261" height="25">&nbsp;</td>
    </tr>
<%}%>
    <tr>
      <td width="249">
        <div align="right">Set Run Parameters </div>
      </td>
      <td width="261">
        <input type="radio" name="option" value="SetParameters"
                                 onclick="javascript:setOption(this)" <%=alreadyRun.booleanValue()?"checked":""%>>
      </td>
    </tr>
    <tr>
      <td width="249">
        <div align="right">Previous Runs</div>
      </td>
      <td width="261">
        <input type="radio" name="option" value="PreviousRuns"
                                 onclick="javascript:setOption(this)">
      </td>
    </tr>
  </table>

  <br>
  <h3><div id="descText">

  Do a renewal run for a specified period, and produce Notices, Advices, Reminders and Final Notices

  </div> </h3>
  <div id="batchList" style="display:none">

    <table  border="0" cellspacing="0" cellpadding="0">
        <td  valign="top" width="248">
          <div align="right">Select Batch:&nbsp;&nbsp; </div>
        </td>
        <td>
        <div style="height:300;overflow:auto;">
          <%
int lineCount = batchList.size();
String thisItem = null;

for(int x=0;x<lineCount;x++)
{
   batchVO = (RenewalBatchVO)batchList.elementAt(x);
   thisItem = batchVO.getRunNumber() + ":   "
            + DateUtil.formatShortDate(batchVO.getFirstDate()) + " to "
            + DateUtil.formatShortDate(batchVO.getLastDate())
            + "     " + batchVO.getRunStatus();
   %>
          <a class="batchNo" href="javascript:doBatchNo('<%=batchVO.getRunNumber()%>')"><%=thisItem%>
          </a><br>
          <%
}

%>
         </div>
        </td>
    </tr>
  </table>
  </div>
<div id="dates" style="">
<%if(!alreadyRun.booleanValue())
{%>
<table border="0" >
    <tr>
        <td width="244" >
          <div align="right">Create Renewal Notices/Advices for memberships expiring
            between: </div>
      </td>
        <td width="224"> <%=firstDate.formatShortDate()%>&nbsp and:
          <input type="text" name="lastDate" size="10" maxlength="10" value="<%=lastDate.formatShortDate()%>">
        </td>
    </tr>
    <tr >
      <td width="244">
        <div align="right">Schedule to run on: </div>
      </td>
      <td width="224">
        <input type = "text" name = "runDate" size = "11" value = "<%=new DateTime().formatShortDate()%>"
         onkeyup = "javascript:checkDate(this)">
          &nbsp at &nbsp
          <input type = "text" name = "runHours" size = "2" value = "20"
             onKeyUp = "javascript:checkInteger(this)"
             onBlur = "javascript:checkHours(this)">
          :
          <input type = "text" name = "runMins" size = "2" value = "30"
             onkeyup = "javascript:checkInteger(this)"
             onblur = "javascript:checkMins(this)">
      </td>
    </tr>
    <tr>
        <td width="224" align="right">Results will be sent to:</td>
        <td width="224"><%=emailTo%><%=(ccTo == null ? "" : "<br>" + ccTo)%></td>
    </tr>
  </table>

 <%}%>
  </div>
  <%if(alreadyRun.booleanValue())
    {%>
   <div id="runParameters" style="">
   <%}
   else
   {
   %>
   <div id="runParameters" style="display:none">
   <%}%>
    <table>
      <tr>
        <td width="243" align="right">Auto Run:</td>
        <td width="223">
          <input type="checkbox" name="autoRun" <%=autoRun?"checked":""%> >
        </td>
      </tr>
      <tr>
        <td width="243" align="right">The next batch should be run on</td>
        <td width="223">
          <input type="text" name="nextRunDate" size = "11" value = "<%=nextProjectedRunDate%>"
         onkeyup = "javascript:checkDate(this)">
          (Next run date)</td>
      </tr>
      <tr>
        <td width="243" align="right">At</td>
        <td width="223">
          <input type="text" name="runTimeHours" size = "2" value = "20"
             onKeyUp = "javascript:checkInteger(this)"
             onBlur = "javascript:checkHours(this)">
          <input type="text" name="runTimeMins" size="2" value="30"
             onkeyup = "javascript:checkInteger(this)"
             onblur = "javascript:checkMins(this)">
          (Run time)</td>
      </tr>
      <tr>
        <td width="243" align="right">And thereafter every </td>
        <td width="223">
          <input type="text" name="runInterval" size="2" value="14"
                     onkeyup="javascript:checkInteger(this)">
          days</td>
      </tr>
      <tr>
        <td width="243" align="right">The resulting files should be emailed to</td>
        <td width="223">
          <input type="text" name="runEmailTo" value="<%=emailTo%>" size="50">
        </td>
      </tr>
      <tr>
        <td width="243" align="right">and</td>
        <td width="223">
          <input type="text" name="runCcTo" value="<%=(ccTo == null ? "" : ccTo)%>" size="50">
        </td>
      </tr>
    </table>
  </div>

   <table border="0">
   <tr>
   <td >
         <a  onclick="javascript:submitForm('RenewalNoticesRun')"
             onMouseOut="MM_swapImgRestore();"
             onMouseOver="MM_displayStatusMsg('Renewal Run');MM_swapImage('btnOK','','<%=CommonConstants.DIR_IMAGES%>/OkButton_over.gif',1);return document.MM_returnValue"
         >
             <img  name="btnOK"
                   src="<%=CommonConstants.DIR_IMAGES%>/OkButton.gif"
                   border="0">
          </a>
   </td>
   <td>
         <a  onclick="javascript:submitForm('RenewalNoticesCancel')"
             onMouseOut="MM_swapImgRestore();"
             onMouseOver="MM_displayStatusMsg('Cancel Motor News  Extract.');MM_swapImage('btnCancel','','<%=CommonConstants.DIR_IMAGES%>/CancelButton_over.gif',1);return document.MM_returnValue"
         >
             <img  name="btnCancel"
                   src="<%=CommonConstants.DIR_IMAGES%>/CancelButton.gif"
                   border="0"
                   alt="Cancel Renewal Run."
             >
          </a>
   </td>
   </tr>
   </table>
<input type="hidden" name="runNumber" value="">
</form>
</body>
</html>
