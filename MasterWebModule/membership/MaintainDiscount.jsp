<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>

<%
String saveEvent     = "maintainDiscount_btnSave";
String deleteEvent   = "maintainDiscount_btnDelete";
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;
AdminCodeDescription ad = null;

DiscountTypeVO discountTypeVO = (DiscountTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_DISCOUNT_VO);
String origCode = (String) request.getSession().getAttribute("origCode");
String statMsg  = (String) request.getAttribute("statusMessage");
String refresh  = (String) request.getAttribute("Refresh");
// Get the list of valid account for selection

Collection discountFeeTypes = discountTypeVO.getFeeTypeList();
SortedSet sDiscountFeeTypeList = new TreeSet();
if ( !(discountFeeTypes ==null ) )
{
   Iterator it = discountFeeTypes.iterator();
   while(it.hasNext())
   {
      FeeTypeVO f = (FeeTypeVO) it.next();
      sDiscountFeeTypeList.add(new AdminCodeDescription( f.getFeeTypeCode(),f.getFeeTypeCode()));
   }
}
Collection feeTypeList = MembershipEJBHelper.getMembershipRefMgr().getFeeTypeList();
SortedSet sFeeTypeList = new TreeSet();
if ( !(feeTypeList ==null ) )
{
   Iterator it = feeTypeList.iterator();
   while(it.hasNext())
   {

      FeeTypeVO f = (FeeTypeVO) it.next();
      ad = new AdminCodeDescription(f.getFeeTypeCode(),f.getFeeTypeCode());
      if ( !(sDiscountFeeTypeList==null) & !sDiscountFeeTypeList.contains(ad))
      {
         sFeeTypeList.add(new AdminCodeDescription( f.getFeeTypeCode(),f.getFeeTypeCode()));
      }
   }
}

MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
Collection accountList = membershipMgr.getMembershipAccounts();
SortedSet sAccountList = new TreeSet();
if ( accountList != null & accountList.size() > 0 )
{
   Iterator<MembershipAccountVO> it = accountList.iterator();
   while( it.hasNext() )
   {
      MembershipAccountVO ma = it.next();
      sAccountList.add( new AdminCodeDescription( ma.getAccountID().toString(),ma.getAccountTitle()));
   }
}

//DiscountType. discountType = null;
//
String    discountCode;
String    description;
boolean   systemControlledDiscount;
boolean   recurringDiscount;
boolean   automaticDiscount;
boolean   minFeeApplicableDiscount;
boolean   reducesMembershipValue;
String    status;
double    discountAmount;
double    discountPercentage;
String    percentageOf;
Interval  discountPeriod;
int       discountPeriodYear;       // Years    in discount period
int       discountPeriodMonth;      // Months   in discount period
int       discountPeriodDay;        // Days     in discount period
Integer   discountAccountId;
DateTime  effectiveFromDate;
String    strEffFromDate;
DateTime  effectiveToDate;
String    strEffToDate;
//
// if the discountTypeCode is present then modify an existing discount type code
// otherwise
// create a new one
if ( StringUtil.isNull(discountTypeVO.getDiscountTypeCode()) )
{
  discountCode             = "";
  description              = " ";
  systemControlledDiscount = false;
  recurringDiscount        = false;
  automaticDiscount        = false;
  minFeeApplicableDiscount = false;
  reducesMembershipValue   = false;
  status                   = "";
  discountAmount           = 0.0;
  discountPercentage       = 0.0;
  percentageOf             = "";
  discountPeriod           = new Interval("0:0:0:0:0:0:0");
  discountPeriodYear       = 0;
  discountPeriodMonth      = 0;
  discountPeriodDay        = 0;
  discountAccountId        = new Integer(0);
  effectiveFromDate        = DateUtil.nullOrDate(" ");
  effectiveToDate          = effectiveFromDate;
}
else
{
  discountCode             = discountTypeVO.getDiscountTypeCode();
  description              = discountTypeVO.getDescription();
  systemControlledDiscount = discountTypeVO.isSystemControlled();
  recurringDiscount        = discountTypeVO.isRecurringDiscount();
  automaticDiscount        = discountTypeVO.isAutomaticDiscount();
  minFeeApplicableDiscount = discountTypeVO.isMinimumFeeApplicable();
  reducesMembershipValue   = discountTypeVO.isMembershipValueReducing();
  status                   = discountTypeVO.getStatus();
  discountAmount           = discountTypeVO.getDiscountAmount();
  discountPercentage       = discountTypeVO.getDiscountPercentage();
  discountPeriod           = (Interval) discountTypeVO.getDiscountPeriod();
  percentageOf             = discountTypeVO.getPercentageOf();
  if(percentageOf==null)percentageOf = "";
  if ( discountPeriod == null )
  {
      discountPeriodYear       = 0;
      discountPeriodMonth      = 0;
      discountPeriodDay        = 0;
  }
  else
  {
      discountPeriodYear       = discountPeriod.getYears();
      discountPeriodMonth      = discountPeriod.getMonths();
      discountPeriodDay        = discountPeriod.getDays();
  }
  discountAccountId        = (Integer) discountTypeVO.getDiscountAccountID();
  effectiveFromDate        = (DateTime) discountTypeVO.getEffectiveFromDate();
  effectiveToDate          = (DateTime) discountTypeVO.getEffectiveToDate();

  }


  strEffFromDate = effectiveFromDate == null ? " " : effectiveFromDate.formatShortDate();
  strEffToDate   = effectiveToDate == null ? " " : effectiveToDate.formatShortDate();
  description = StringUtil.makeSpaces(description);

Collection discountAttributes = discountTypeVO.getDiscountAttributes();

%>
<head>
<title>Maintain Discount</title>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body class="rightFrame" onload="refreshList(document.all.refresh,'<%=listPage%>');">

<form name="mainForm"
      method="post"
      action="<%=MembershipUIConstants.PAGE_MEMBERSHIP_DISCOUNT_ADMIN_UIC%>"
>

  <input type="hidden" name="event" value="">
  <input type="hidden" name="discountInterval" value ="">
  <input type="hidden" name="pnum" value="<%=discountCode%>">
  <input type="hidden" name="refresh" value ="<%=refresh%>">
  <input type="hidden" name="listCode" value ="">

 <div id="mainpage" class="pg">
 <h1>Maintain Discount</h1>
   <div id="contentpage" class="cnt">
<!--
   START DISCOUNT BODY
-->
<table border="0" cellspacing="1" cellpadding="0" width="100%">
  <tr>
    <td class="label" width="233">Discount Type Code:</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692"> <%=HTMLUtil.inputOrReadOnly("discountCode",discountCode)%> </td>
  </tr>
  <tr>
    <td class="label" width="233">Description:</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="text"
             name="description"
             value="<%=description%>" maxlength="256" size="50"
      >
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Account ID</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" valign "top width="692""> <%=HTMLUtil.selectList("discountAccount",sAccountList,discountAccountId.toString(),"","No Account")%> </td>
  </tr>
  <tr>
    <td class="label" width="233">System Controlled</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="checkbox"
             name="systemControlled"
             value="true"
             <% if (systemControlledDiscount) {out.print("checked");} %>
      >
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Recurring Discount</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="checkbox"
             name="recurring"
             value="true"
             <% if (recurringDiscount) {out.print("checked");} %>
    >
    </td>
   </tr>
  <tr>
    <td class="label" width="233">Automatic </td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="checkbox"
             name="automatic"
             value="true"
             <% if (automaticDiscount) {out.print("checked");} %>
      >
    </td>
    <!--
    <td class="label" width="157">Minimum Fee Applicable </td>
    <td class="dataValue" width="10">&nbsp;</td>
    <td class="dataValue" width="258">
      <input type="checkbox"
             name="minimumFeeApplicable"
             value="<%=minFeeApplicableDiscount%>"
             <% if (minFeeApplicableDiscount) {out.print("checked");} %>
      >
    </td>
-->
  </tr>
  <tr>
    <td class="label" width="233">Minimum Fee Applicable </td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="checkbox"
             name="minimumFeeApplicable"
             value="<%=minFeeApplicableDiscount%>"
             <% if (minFeeApplicableDiscount) {out.print("checked");} %>
      >
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Reduces Membership Value</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="checkbox"
             name="reducesMembershipValue"
             value="true"
             <% if (reducesMembershipValue) {out.print("checked");} %>
      >
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Discount Amount</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="text"
             name="discountAmount"
             value="<%=discountAmount%>"
      >
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Discount Percentage</td>
  
    
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="text"
             name="discountPercentage"
             value="<%=discountPercentage%>" >
    &nbsp; of &nbsp;
       <input type="text"
              name="percentageOf"
              value="<%=percentageOf%>"
              title=
" Product name.
Only needed where the percentage is of a different product fee ">
    </td>

   </tr>
  <tr>
    <td class="label" width="233">Discount Period</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td width="692">
      <table>
        <tr valign="middle">
          <td class="label" width="36">Years</td>
          <td class="dataValue" width="39">
            <input type="text"
                    name="discountPeriodYear"
                    value="<%=discountPeriodYear%>" size="4"
             >
          </td>
          <td class="label" width="60">Months</td>
          <td class="dataValue" width="44">
            <input type="text"
                    name="discountPeriodMonth"
                    value="<%=discountPeriodMonth%>" size="4"
             >
          </td>
          <td class="label" width="43">Days</td>
          <td class="dataValue" width="74">
            <input type="text"
                    name="discountPeriodDay"
                    value="<%=discountPeriodDay%>" size="4"
             >
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Effective From</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="text"
             name="effectiveFrom"
             value="<%=strEffFromDate%>"
             onChange = "checkdate(this);"
      >
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Effective To</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <input type="text"
             name="effectiveTo"
             value="<%=strEffToDate%>"
             onChange = "checkdate(this);"
      >
    </td>
  </tr>
  <tr>
    <td class="label" width="233">Status:</td>
    <td class="dataValue" width="43">&nbsp;</td>
    <td class="dataValue" width="692">
      <select name="status">
        <option <% if (DiscountTypeVO.STATUS_Active.equals(status)) {out.print("selected");} %> ><%=DiscountTypeVO.STATUS_Active%></option>
        <option <% if (DiscountTypeVO.STATUS_Inactive.equals(status)) {out.print("selected");} %> ><%=DiscountTypeVO.STATUS_Inactive%></option>
      </select>
    </td>
  </tr>
  <tr>
    <td colspan="3">
   <table width="100%">
   <tr>
   <td width="50%">
      <%=HTMLUtil.codeDescriptionTable (  "All Fee Types.",
                                            sFeeTypeList,
                                            "maintainDiscountFeeTypes_btnAdd",
                                            "Double click to ADD this Fee Type.",
                                            "No Fee Types to be selected."
                                        )

      %>
    </td>
     <td width="50%">
     <%=HTMLUtil.codeDescriptionTable (  "Selected Fee Types.",
                                         sDiscountFeeTypeList,
                                         "maintainDiscountFeeTypes_btnRemove",
                                         "Double click to REMOVE this Fee Type.",
                                         "No selected Fee Types."
                                       )
    %>
   </td>
   </tr>
   </table>
   </td>
  </tr>
  <%
  if (discountAttributes != null && !discountAttributes.isEmpty())
  {
  %>
<tr>
  <td>
  <table>
  <tr>
    <td colspan="3"><h3>Discount Attributes</h3></td>
  </tr>
  <tr class="headerRow" valign="top">
    <td class="listHeadingPadded">Name</td><td class="listHeadingPadded">Description</td><td class="listHeadingPadded">Mandatory?</td>
  </tr>
  <%
    MembershipDiscountAttribute mda = null;
    Iterator discountAttrIt = discountAttributes.iterator();
    int counter = 0;
    while (discountAttrIt.hasNext())
    {
      counter++;
      mda = (MembershipDiscountAttribute)discountAttrIt.next();
  %>

  <tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
    <td class="dataValue"><%=mda.getMembershipDiscountAttributePK().getAttributeName()%></td><td class="dataValue"><%=mda.getAttributeDescription()%></td><td class="dataValue"><%=mda.isMandatory()%></td>
  </tr>

  <%
    }
  %>
  </table>
  </td>
  </tr>
  <%
  }
  %>

</table>
<!--
   FINISH DISCOUNT BODY
-->
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSaveAndDelete(saveEvent,deleteEvent,"Discount", statMsg )%>
   </div>
</div>
</form>
</body>
</html>

