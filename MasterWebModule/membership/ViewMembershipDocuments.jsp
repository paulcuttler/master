<%@page import="com.ract.common.*"%>
<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@page import="com.ract.user.UserSession"%>
<%@page import="com.ract.security.Privilege"%>

<%
UserSession userSession = UserSession.getUserSession(session);
Vector memDocList = null;
Iterator memDocListIT = null;

MembershipDocument memDocVO = null;
// See if the membership ID was specified
MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");

ClientVO clientVO = memVO.getClient();
// Required for ViewClientInclude.jsp
request.setAttribute("client",clientVO);

// get the list of membership cards that the membership has.
memDocList = (Vector)memVO.getDocumentList(true);
MembershipDocumentComparator memDocComparator = new MembershipDocumentComparator();
//sort using comparator
Collections.sort(memDocList,memDocComparator);
//reverse natural sort order
Collections.sort(memDocList);
%>


<html>
<head>
  <title>View membership documents</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
<script language="JavaScript">

function showDocumentDetail(inputVar1)
{
  var val = returnRadioButtonValue(inputVar1);
//alert("val="+val);
    var url = '<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembershipDocument_detail&documentId=' + val;
    window.open(url, '', 'titlebar=yes,toolbar=no,status=no,scrollbars=yes,resizable=yes');
 }

 function validateRadio ( inputVar1,inputVar2 )
 {
   var val = returnRadioButtonValue(inputVar1);
//alert(" val="+val);
   document.all.documentId.value = val;
   document.all.event.value = inputVar2;
   document.all.viewMembershipDocuments.submit();
 }

 function showMembership(inputVar1)
 {
   document.all.event.value = inputVar1;
   document.all.viewMembershipDocuments.submit();
 }

</script>
</head>
<body>
  <form name="viewMembershipDocuments" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="viewMembershipDocuments_btnMembership">
  <input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
  <input type="hidden" name="documentId" value="">
  
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top" >
    <td>

      <h2><%@ include file="/help/HelpLinkInclude.jsp"%> Membership Documents</h2>

      <%@ include file="/client/ViewClientInclude.jsp"%>

      <table border="0" cellspacing="0" cellpadding="3">
<%
int counter = 0;
if (memDocList != null && !memDocList.isEmpty())
{
%>
        <tr class="headerRow" valign="top">
          <td></td>
          <td class="listHeadingPadded">Date</td>
          <td class="listHeadingPadded">Type</td>
          <td class="listHeadingPadded">Status</td>
          <td class="listHeadingPadded">Cancel Date</td>          
          <td class="listHeadingPadded">Run Number</td>
          <td class="listHeadingPadded">Effective Date</td>
          <td class="listHeadingPadded">Expiry Date</td>
          <td class="listHeadingPadded">Amount</td>
          <td class="listHeadingPadded">Discounts</td>
          <td class="listHeadingPadded">Group Number</td>
          <td class="listHeadingPadded">Product Code</td>
          <td class="listHeadingPadded">Profile Code</td>
          <td class="listHeadingPadded">Printed?</td>
          <td class="listHeadingPadded">Emailed?</td>
          <td class="listHeadingPadded">EmailAddress</td>
          <td class="listHeadingPadded">EmailDate</td>
          <td class="listHeadingPadded">Transaction XML</td>
        </tr>
<%
  MembershipDocumentComparator mdc = new MembershipDocumentComparator();
  //sort by date order
  Collections.reverse((Vector)memDocList);
  memDocListIT = memDocList.iterator();
  int size = 0;
  boolean first = true;
  String transactionXML = null;
  while (memDocListIT.hasNext())
  {
    counter++;
    memDocVO = (MembershipDocument) memDocListIT.next();
    transactionXML = memDocVO.getTransactionXML();
    if (transactionXML != null)
    {
      size = StringUtil.getSizeInKB(transactionXML);
    }
%>
        <tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
          <td class="listItemPadded" align="center">
            <input name="selectedLine" value="<%=memDocVO.getDocumentId()%>" type="radio" <%=(first ? "CHECKED" : "")%>>
          </td>
          <td><a title ="<%=memDocVO.getDocumentId()%>" ><%=memDocVO.getDocumentDate().formatShortDate()%></a></td>
          <td><%=memDocVO.getDocumentTypeCode()%></td>
          <td><%=memDocVO.getDocumentStatus() == null?"":memDocVO.getDocumentStatus() %></td>
          <td><%=memDocVO.getCancelDate() == null ? "": memDocVO.getCancelDate().formatMediumDate()%></td>          
          <!-- new attributes -->
          <td><%=memDocVO.getRunNumber() == null? "":memDocVO.getRunNumber().toString()%></td>
          <td><%=memDocVO.getFeeEffectiveDate() == null ? "": memDocVO.getFeeEffectiveDate().formatShortDate()%></td>
          <!-- expiry date -->
          <td><%=memDocVO.getMembershipDate() == null? "": memDocVO.getMembershipDate().formatShortDate()%></td>
          <td><%=CurrencyUtil.formatDollarValue(memDocVO.getAmountPayable())%></td>
          <td><%=CurrencyUtil.formatDollarValue(memDocVO.getDiscounts())%></td>
          <td><%=memDocVO.getGroupNumber() == null?"":memDocVO.getGroupNumber().toString()%></td>
          <td><%=memDocVO.getProductCode() == null?"":memDocVO.getProductCode()%></td>
          <td><%=memDocVO.getProfileCode() == null? "": memDocVO.getProfileCode()%></td>
		  <td><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(memDocVO.isPrinted() ? "check.gif" : "cross.gif")%>"></td>
		  <td><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(memDocVO.isEmailed() ? "check.gif" : "cross.gif")%>"></td>
		  <td><%=memDocVO.getEmailAddress() == null? "": memDocVO.getEmailAddress()%></td>
		  <td><%=memDocVO.getEmailDate() == null? "": memDocVO.getEmailDate().formatMediumDate()%></td>                    
          <td><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(transactionXML!=null ? "check.gif" : "cross.gif")%>"> <%=transactionXML!=null?size+"kb":""%></td>
        </tr>
 
<%
    first = false;
  }
%>

<%
}
else
{
%>
      <tr>
        <td class="helpText">No documents to display</td>
      </tr>
<%
}
%>

  </table>

    </td></tr>
    <tr valign="bottom">
    <td>
      <table width="0%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>
               <input type="button" name="btnMembership" value="View Membership" onclick="showMembership('viewMembershipDocuments_btnMembership')">
            </td>
            <%
            if (counter > 0) {
            %>
            <td>
               <input type="button" name="btnViewDocumentDetails" value="View Document Details" onclick="showDocumentDetail(document.all.selectedLine)">
            </td>
            <td>
               <input type="button" name="btnCancelRenewalIgnoreErrors" value="Cancel Renewal" onclick="validateRadio(document.all.selectedLine,'viewMembershipDocuments_btnCancelRenewalIgnoreErrors')">
            </td>
            <%
            } // end if
            %>	
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html>
