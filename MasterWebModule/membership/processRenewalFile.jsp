<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.user.UserSession"%>
<%@ page import="com.ract.util.*"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>
		<s:head theme="simple" />

		<script type="text/javascript">
		  function toggleDisplay() {
		
		    for (i = 0; i < document.all.process.length; i++) {
		      if (document.all.process[i].checked == true) {
		        var val = document.all.process[i].value;
		        //     alert(val)
		        if (val == 'emailRenewals') {
		          document.all.email.style.display = "";
		          document.all.merge.style.display = "none";
		        } else if (val == 'merge') {
		          document.all.email.style.display = "none";
		          document.all.merge.style.display = "";
		        }
		        break;
		      }
		    }
		
		  }
		</script>

	</head>

	<body onload="toggleDisplay()">

		<h2>Process Renewal File</h2>

		<s:if test="actionMessages == null or actionMessages.isEmpty()">
			<s:form name="mergeFile" enctype="multipart/form-data" method="post"
				action="processRenewalFile" theme="simple">
				<table border="0">
					<tbody>
						<tr>
							<td>Renewal File</td>
							<td><s:file name="renewalFile" /></td>
						</tr>
						<tr>
							<td colspan="2">

								<table border="0">
									<thead>
										<tr>
											<th align="left">
												<s:radio theme="simple" id="process" name="process"
													list="merge" value="process" onclick="toggleDisplay()" />
											</th>
											<th></th>
										</tr>
									</thead>

									<tbody id="merge" style="display: none">
										<tr>
											<td colspan="2" class="helpTextSmall">
												Select a marketing file that has lines consisting of three
												elements: client number, insert code and message code
												delimited with commands. The renewal and marketing files
												will be merged and a prompt will appear to allow the the
												merged file to be saved to the local PC. An audit report
												will be sent to the specified email address.
											</td>
										</tr>
										<tr>
											<td>Marketing File</td>
											<td><s:file name="marketingFile" /></td>
										</tr>
										<tr>
											<td>Use default marketing options</td>
											<td><s:checkbox name="defaultMarketing" /></td>
										</tr>
										<tr>
											<td>Email address</td>
											<td><s:textfield name="emailAddress" /></td>
										</tr>
									</tbody>
								</table>

								<table border="0">
									<thead>
										<tr>
											<th align="left">
												<s:radio theme="simple" id="process" name="process"
													list="email" value="process" onclick="toggleDisplay()" />
											</th>
											<th></th>
										</tr>
									</thead>

									<tbody id="email" style="display: none">
										<tr>
											<td>
												<table>
													<tr>
														<td>
															<s:radio name="emailOption" list="test" value="test" />
														</td>
													</tr>
													<tr>
														<td>
															<s:radio name="emailOption" list="update" value="update" />
														</td>
													</tr>
													<tr>
														<td>
															<s:radio name="emailOption" list="count" value="count" />
														</td>
													</tr>
												</table>
											</td>
										</tr>

									</tbody>

								</table>

							</td>
						</tr>

						<s:if test="counts != null">
							<tr>
								<td>Records Processed</td>
								<td><s:property value="primaryLineCount" /></td>
							</tr>
							<tr>
								<td>Valid records processed</td>
								<td><s:property value="validLineCount" /></td>
							</tr>
							<tr>
								<td>Test emails sent</td>
								<td><s:property value="testEmailsSent" /></td>
							</tr>
							<tr>
								<td>Documents updated and emails sent</td>
								<td><s:property value="updateCount" /></td>
							</tr>
							<tr>
								<td>Records with no delivery preference</td>
								<td><s:property value="noDeliveryCount" /></td>
							</tr>
							<tr>
								<td>Email delivery preference</td>
								<td><s:property value="emailCount" /></td>
							</tr>
							<tr>
								<td>Print delivery preference</td>
								<td><s:property value="printCount" /></td>
							</tr>
						</s:if>

						<tr>
							<td colspan="2"><s:actionerror /></td>
						</tr>
						<tr>
							<td colspan="2"><s:submit value="Process" /></td>
						</tr>
				</table>
			</s:form>
		</s:if>
		<s:else>
			<s:actionmessage />
		</s:else>
	</body>
</html>

