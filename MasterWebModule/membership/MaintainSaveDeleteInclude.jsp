<%@ page import="com.ract.common.*"%>

<!--
should be in Common!
 * HTML include to make the admin maintenace lists the same
-->

<%
//get parameters from the main form
String saveEvent = (String)request.getAttribute("saveEvent");
String deleteEvent = (String)request.getAttribute("deleteEvent");
String statMsg = (String)request.getAttribute("statMsg");
%>

   <tr valign="bottom" height="5%" >
    <td >
     <table border="0" cellspacing="3" cellpadding="3" height="94%" width="100%" >
       <tr bgcolor="#FFFFFF">
         <td valign="top">
              <a  onclick="submitForm('<%=saveEvent%>');return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Save to DataBase.');MM_swapImage('btnSave','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
                  <img  name="btnSave"
                        src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif"
                        border="0"
                        alt="Save to DataBase."
                  >
               </a>
         </td>
         <td valign="top">
              <a  onclick="submitForm('<%=deleteEvent%>');return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Delete Drom DataBase.');MM_swapImage('btnDelete','','<%=CommonConstants.DIR_IMAGES%>/DeleteButton_over.gif',1);return document.MM_returnValue" >
                  <img  name="btnDelete"
                        src="<%=CommonConstants.DIR_IMAGES%>/DeleteButton.gif"
                        border="0"
                        alt="Delete from DataBase."
                  >
               </a>
         </td>
         <td width="15%">&nbsp;</td>
         <td class="adminInfoText" valign="top" width="80%" ><%=statMsg%></td>
         </tr>
       </table>
    </td>
   </tr>
