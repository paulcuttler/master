<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.cad.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.payment.directdebit.*"%>
<%@ page import="com.ract.payment.receipting.*"%>
<%@ page import="com.ract.security.Privilege"%>
<%@ page import="com.ract.user.UserSession"%>
<%@ page import="com.ract.membership.club.*"%>
<%@ page import="java.text.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<%
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
String emptyString = "";
//final String unknownString = "??????";
ArrayList warningList = new ArrayList();
String headingText = "Membership";
MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");
Boolean askClientQuestions = (Boolean)request.getAttribute("askClientQuestions");
boolean showMemberCards = false;
try {
  showMemberCards = (Boolean)request.getAttribute("showMemberCards");
} catch (Exception e) {}

String showTransferFailedDDButton = (String)request.getAttribute("showTransferFailedDDButton");
String membershipID = (String) request.getParameter("membershipID");
String refresh = (String) request.getAttribute("refreshProductList");
boolean refreshProductList = "Yes".equalsIgnoreCase(refresh);
UserSession us = UserSession.getUserSession(request.getSession());
User user = us.getUser();

// Membership history is implemented by the membership value object so we can cast it
// and work as if it were a membership value object.
// But now we know we are viewing history and not current data.
MembershipHistory memHist = (MembershipHistory) request.getAttribute("membershipHistory");
if (memHist != null)
{
   memVO = (MembershipVO) memHist;
   headingText = "Membership history"; 
}
if (memVO == null)
{
   // See if the membership ID was specified instead
   if (membershipID == null)
   {
      throw new ServletException("A membership VO or a membership ID must be passed to the ViewPersonalMembership page.");
   }

   Integer memID = new Integer(membershipID);
   memVO = memMgr.getMembership(memID);
}

// Show an error page if the membership could not be obtained.

if (memVO == null)
{
   throw new Exception("Unable to get membership details in ViewPersonalMembership page.");
}

// Got a valid membership. Now see if the product is current.
// If the product is effective in the future and the membership commenced in
// the past then we need to find the current product from the history.
String currentProductCode = memVO.getCurrentProductCode();
DateTime currentProductEffectiveDate = memVO.getCurrentProductEffectiveDate();
String productCode = memVO.getProductCode();
DateTime productEffectiveDate = memVO.getProductEffectiveDate();
ProductVO prodVO = null;
if (productCode != null)
{
  prodVO = refMgr.getProduct(productCode);
}

DirectDebitAuthority ddAuthority = null;
try
{
   ddAuthority = memVO.getDirectDebitAuthority();
}
catch (Exception e)
{
   warningList.add("Failed to get direct debit authority. " + e.getMessage());
}
String membershipStatus = memVO.getStatus();

//if no product code then add a warning
if (currentProductCode == null)
{
  //if no history available
  //    warningList.add("Membership history is not available.  The displayed product may not be accurate if the product has been changed.");
  currentProductCode = productCode;
  currentProductEffectiveDate = productEffectiveDate;
}

ClientVO clientVO = memVO.getClient();
request.setAttribute("client",clientVO);

//log client
//log("ViewPersonalMembership.jsp clientNumber="+(clientVO==null?"":clientVO.getClientNumber().toString()));

String clientRequiresVehicle = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,SystemParameterVO.CLIENT_REQUIRES_VEHICLE);
if ("Y".equalsIgnoreCase(clientRequiresVehicle) &&  !memVO.isHistory())
{
   Collection vehicleList = clientVO.getVehicleList();
   if (vehicleList == null || vehicleList.isEmpty())
   {
      warningList.add("Transactions are disabled until vehicle information is added to the client.");
   }
}

// Add any exceptions from the membership value object to the list of warnings.
if (memVO.hasException())
{
   Exception e;
   Vector exceptionList = memVO.getExceptionList();
   for (int i = 0; i < exceptionList.size(); i++)
   {
      e = (Exception) exceptionList.get(i);
//e.printStackTrace();
      warningList.add(e.getMessage());
   }
}

MembershipGroupVO memGroupVO = memVO.getMembershipGroup();
if (memGroupVO != null &&  memGroupVO.hasException())
{
   warningList.add(memGroupVO.getMembershipGroupExceptionMessages());
}

double amountUnpaid = 0.0;
String cadStatusHTML = "";
if (!memVO.isHistory())
{
    try
    {
       amountUnpaid = memVO.getAmountOutstanding(true,false);
    }
    catch (Exception pe)
    {
      //already handled.
    }  	
   cadStatusHTML = MembershipUIHelper.getCadStatusHTML(clientVO,warningList);
}

String menu = MembershipUIHelper.buildMenu(memVO,userSession,prodVO,amountUnpaid);

ClientNote membershipNote = memVO.getLastClientNote(); 
%>

<html>
<head>
<title>View membership</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>

<link rel="stylesheet" href="<%=CommonConstants.DIR_DOJO_1_4_2%>/dijit/themes/tundra/tundra.css" />

<%
// Don't enable the popup menu if we are viewing history.
if (!memVO.isHistory())
{
%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js">
</script>

<script language="Javascript" src="<%=CommonConstants.DIR_DOJO_1_4_2%>/dojo/dojo.js" djConfig="parseOnLoad: true, isDebug:false"></script>

<script language="JavaScript">

   dojo.require("dojo.parser");
   dojo.require("dijit.form.Button");
   dojo.require("dijit.Menu");

   window.onload=onLoadEventHandler;

   function onLoadEventHandler()
   {
      if (<%=refreshProductList%>)
      {
         parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber=<%=clientVO.getClientNumber()%>';
      }
   }

   function doMenuItem(menuItemName)
   {
      var nav = false;
      
      //check if customer is missing data
      if (<%=askClientQuestions%>) //true
      {
        if (confirm("WARNING! Client details need to be updated. Do you want to OK: complete transaction anyway or Cancel: abort transaction?"))
        {
          nav = true;
        }
      }
      else //false
      {
        nav = true;
      }   
      if (nav)
      {  
	      if(menuItemName=="mnuPrintAdvice")
	      {
	         var url ="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>?event=printMembershipAdvice&membershipID=<%=memVO.getMembershipID()%>";
	         window.open(url, '', 'width=250,height=150,toolbar=no,status=yes,scrollbars=no,resizable=no')
	      }
	      else if(menuItemName=="mnuPrintDDAdvice")
	      {
	      	 var url = "<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>?event=printMembershipAccountDetails&membershipId=<%=memVO.getMembershipID()%>";
 			 window.open(url, '', 'width=250,height=150,toolbar=no,status=no,scrollbars=no,resizable=no');
	      }
	      else if(menuItemName=="mnuPrintRenewalNotice")
	      {
	        document.location.href = "printRenewal.action?membershipId=<%=memVO.getMembershipID()%>";     
	      }
	      else if(menuItemName=="mnuEmailGeneralAdvice")
	      {
	         var url ="EmailAdviceForm.action?membershipId=<%=memVO.getMembershipID()%>&adviceType=<%= SystemParameterVO.EMAIL_ADVICE_GENERAL %>";
	         window.open(url, '', 'width=640,height=480,toolbar=no,status=yes,scrollbars=no,resizable=no')
	      }
	      else if(menuItemName=="mnuEmailDDAdvice")
	      {
	         var url ="EmailAdviceForm.action?membershipId=<%=memVO.getMembershipID()%>&adviceType=<%= SystemParameterVO.EMAIL_ADVICE_DD %>";
	         window.open(url, '', 'width=640,height=480,toolbar=no,status=yes,scrollbars=no,resizable=no')
	      }
	      else if(menuItemName=="mnuEmailRenewalAdvice")
	      {
	         var url ="EmailAdviceForm.action?membershipId=<%=memVO.getMembershipID()%>&adviceType=<%= SystemParameterVO.EMAIL_ADVICE_RENEWAL %>";
	         window.open(url, '', 'width=640,height=480,toolbar=no,status=yes,scrollbars=no,resizable=no')
	      }
	      else if(menuItemName=="mnuAddNote") 
	      {
	         document.location.href = "addNote.action?referenceId=<%=memVO.getMembershipID()%>&businessType=MEM";      
	      }
	      else if(menuItemName=="mnuManageCredit")
	      {
	         document.location.href = "showManageCredits.action?membershipId=<%=memVO.getMembershipID()%>";        
	      }
	      else if(menuItemName=="mnuToggleEmailRenewals")
	      {
	         document.location.href = "toggleEmailRenewals.action?membershipId=<%=memVO.getMembershipID()%>";        
	      }
	      else 
	      {
	         document.location.href = "<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_menuItem&menuItemName=" + menuItemName + "&membershipID=<%=memVO.getMembershipID()%>";
	      }
      }
   } 
</script>
<%
}
%>
</head>
<body class="tundra" style="<%=(prodVO != null && !prodVO.includesRoadsideAssistance())?"background-color: lightblue":""%>">

  <table border="0" cellpadding="0" cellspacing="1" width="100%" height="100%">
    <tr valign="top">
      <td>
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=headingText%> - <%=(memVO.getMembershipTypeCode() == null ? "?" : memVO.getMembershipTypeCode())%></h1>
<%
if (!warningList.isEmpty())
{
   out.println("<ul>");
   String warning;  
   for(int i = 0; i < warningList.size(); i++)
   {
      warning = (String) warningList.get(i);
      out.println("<li class=\"errorTextSmall\">" + warning + "</li>");
   }
   out.println("</ul>");
}
%>
        <!--client details-->
        <%@ include file="/client/ViewClientInclude.jsp"%>

        <!-- Start of membership details -->
        <img width="10" height="5" src="<%=CommonConstants.DIR_IMAGES%>/clear.gif">
        <table border="0" cellspacing="1" cellpadding="3">
          <tr>
            <td class="label">Membership number:</td>
            <td><img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5"></td>
            <td><a  class="dataValue" title="<%=MembershipHelper.getMembershipHTMLSummary(memVO)%>">
<%=(memVO.getMembershipNumber() == null ? emptyString : memVO.getMembershipNumber())%></a></td>
            <td><img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5"></td>
            <td class="label" colspan="2">
<%
if (memGroupVO != null)
{
%>
               <a class="label" title="groupID=<%=memGroupVO.getMembershipGroupID()%>">Group</a>&nbsp;&nbsp;<img src="<%=CommonConstants.DIR_IMAGES%>/check.gif">(<span class="dataValue"><%=memGroupVO.getMemberCount()%></span>)&nbsp;&nbsp;&nbsp;&nbsp;
<%
   Integer paMembershipID = null;
   if (!memVO.isPrimeAddressee())
   {
      // Provide a link to the PA of the group if this member is not the PA
      MembershipGroupDetailVO memGroupDetailVO = memGroupVO.getMembershipGroupDetailForPA();
      if (memGroupDetailVO != null)
      {
        paMembershipID = memGroupDetailVO.getMembershipGroupDetailPK().getMembershipID();
      }
      else
      {
        throw new ServletException("Unable to get the PA for group "+memGroupVO.getMembershipGroupID()+".");
      }

%>
               <a class="actionItem"
                  href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_viewGroupPA&membershipID=<%=paMembershipID%>"
                  title="Go to prime addressee for membership group.">PA</a>&nbsp;&nbsp;<img src="<%=CommonConstants.DIR_IMAGES%>/cross.gif">
<%
   }
   else
   {
%>
               PA&nbsp;&nbsp;<img src="<%=CommonConstants.DIR_IMAGES%>/check.gif">
<%
   }

}
else
{
%>
               Group&nbsp;&nbsp;<img src="<%=CommonConstants.DIR_IMAGES%>/cross.gif">&nbsp;&nbsp;&nbsp;&nbspPA&nbsp;&nbsp;<img src="<%=CommonConstants.DIR_IMAGES%>/cross.gif">
<%
}
%>
            </td>
          </tr>
          <tr>
            <td class="label">Status:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=MembershipUIHelper.formatMembershipStatusHTML(membershipStatus)%></td>
            <td>&nbsp;</td>
            <td><%=(membershipNote!=null?"<a class=\"actionItem\" title=\""+membershipNote.getNoteType()+":"+membershipNote.getNoteText()+"\" href=\"viewNotes.action?businessType=MEM&referenceId="+memVO.getMembershipID()+"\">Notes&nbsp;</a><img src=\""+CommonConstants.DIR_IMAGES+"/check.gif\">":"Notes&nbsp;<img src=\""+CommonConstants.DIR_IMAGES+"/cross.gif\"")%></td>
            <td><%=cadStatusHTML%></td>
          </tr>
          <%--
          @todo
          hide expiry date if access product
          --%>
          <%
          if (prodVO != null &&
              prodVO.isRenewable())
          {
            %>
          <tr>
            <td class="label">Expiry:</td>
            <td>&nbsp;</td>
            <td class="dataValue">
               <%=MembershipUIHelper.formatMembershipExpiryDateHTML(memVO.getExpiryDate(),null)%>
            </td>
            <td>&nbsp;</td>
<%
//get a default term
String defaultTermStr = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,SystemParameterVO.DEFAULT_MEMBERSHIP_TERM);
Interval defaultTerm = null;
try
{
  defaultTerm = new Interval(defaultTermStr);
}
catch (ParseException pe)
{
  throw new SystemException("Error getting default membership term : " + pe.getMessage(),pe);
}
String formattedRemainingTerm = memVO.getRemainingTerm().formatForDisplay(Interval.FORMAT_YEAR,Interval.FORMAT_DAY);
if (formattedRemainingTerm != null &&  formattedRemainingTerm.length() > 0)
{
  if (memVO.getRemainingTerm().getTotalDays() > defaultTerm.getTotalDays())
  {
%>
            <td class="dataValueBlue" colspan="2"><%=formattedRemainingTerm%> <span class="label">remaining</span></td>
<%
  }
  else
  {
%>
            <td class="dataValue" colspan="2"><%=formattedRemainingTerm%> <span class="label">remaining</span></td>
<%
  }
}
else
{
%>
            <td class="dataValue" colspan="2"></td>
<%
}

          }//end of expiryDate
%>
          </tr>

<%

if (memVO.isAllowToLapse() != null &&
    memVO.isAllowToLapse().booleanValue())
{
  //historically not all marked with allowed to lapse had a reason.
%>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="dataValueRed" colspan="2">Allow to lapse (<%=memVO.getAllowToLapseReasonCode()!= null?refMgr.getCancelReason(memVO.getAllowToLapseReasonCode()).getDescription():emptyString%>)</td>
          </tr>
<%
}

if (showTransferFailedDDButton != null && showTransferFailedDDButton.equals("true")) {
   if(user.isPrivilegedUser(Privilege.PRIVILEGE_MEM_XFER_TO_RECEIPTING))
   {
%>
<tr>
            <td class="label" valign="top">Amount outstanding:</td>
            <td>&nbsp;</td>
            <td colspan="4" style="border: 1px dashed red; text-align: center; padding: 10px;" class="dataValueRed">
            	Direct debit schedule is marked as failed.
            	There are outstanding amounts in the direct debit system.<br />
           	  	<input type="button" style="margin-top: 10px" value="Transfer outstanding amounts to Receipting" onclick="if (confirm('***Warning*** Direct debit will be removed and ALL outstanding amounts are to be paid NOW in ECR. \nAre you sure you want to continue?')) { location.href='<%= MembershipUIConstants.PAGE_MembershipUIC %>?event=viewClientSummary_viewMembership&xferDD=y&membershipID=<%=membershipID %>' }" />
            </td>
</tr>
<%
   }
   else  //not a membership manager
   {
   %>
<tr>
            <td class="label" valign="top">Amount outstanding:</td>
            <td>&nbsp;</td>
            <td colspan="4" style="border: 1px dashed red; text-align: center; padding: 10px;" class="dataValueRed">
            	Direct debit schedule is marked as failed. <br />
            	There are outstanding amounts in the direct debit system.<br />
            	Contact Roadside administration. <br /> 
            </td>
</tr>
<%
   }
}
else 
if (amountUnpaid > 0.0)
{
%>
          <tr>
            <td class="label">Amount outstanding:</td>
            <td>&nbsp;</td>
            <td class="dataValueRed">
<%
   String clearOutstandingEnabledStr = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,SystemParameterVO.MEM_CLEAR_OUTSTANDING_AMOUNT);
   if ("Yes".equalsIgnoreCase(clearOutstandingEnabledStr))
   {
      out.print("<a class=\"dataValueRed\" href=\"");
      out.print(MembershipUIConstants.PAGE_MembershipUIC);
      out.print("?event=viewMembership_clearAmountOutstanding&membershipID=");
      out.print(membershipID);
      out.println("\" title=\"Clear amount outstanding.\">");
      out.print(CurrencyUtil.formatDollarValue(amountUnpaid));
      out.println("</a>");
   }
   else
   {
      out.print(CurrencyUtil.formatDollarValue(amountUnpaid));
   }
%>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
<%
}
double credit = memVO.getCreditAmount();
if (memVO.isHistory() &&
    credit > 0.0)
{
%>
          <tr>
            <td class="label">Credit:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=CurrencyUtil.formatDollarValue(credit)%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
<%
}

if (memVO.isHistory())
{
String affiliatedClubCode = memVO.getAffiliatedClubCode();
if (affiliatedClubCode != null &&
   !affiliatedClubCode.equals(emptyString))
{
      AffiliatedClubVO affClub = AffiliatedClubHelper.getAffiliatedClub(memVO.getAffiliatedClubCode());
%>
          <tr>
            <td>Affiliated club name:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=affClub.getDescription()%></td>
            <td>&nbsp;</td>
            <td>Affiliated club expiry:</td>
            <td class="dataValue"><%=memVO.getAffiliatedClubExpiryDate()==null?emptyString:memVO.getAffiliatedClubExpiryDate().formatShortDate()%></td>
          </tr>
          <tr>
            <td class="label">Affiliated club Id:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=memVO.getAffiliatedClubId()%></td>
            <td colspan="3">
            </td>
          </tr>
          <tr>
            <td class="label">Affiliated product level:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=memVO.getAffiliatedClubProductLevel()%></td>
            <td colspan="3">
            </td>
          </tr>

<%
}
}
%>
          <tr>
            <td class="label">Product:</td>
            <td>&nbsp;</td>
<%
            if (currentProductCode==null)
            {
            %>
              <td>&nbsp;</td>
            <%
            }
            else if (currentProductCode.equals(ProductVO.PRODUCT_ADVANTAGE))
            {
            %>
              <td class="dataValueRed"><%=currentProductCode%></td>
            <%
            }
            else if (currentProductCode.equals(ProductVO.PRODUCT_ULTIMATE))
            {
            %>
              <td class="dataValueBlue"><%=currentProductCode%></td>
            <%
            }
            else
            {
            %>
              <td class="dataValue"><%=currentProductCode%></td>
            <%
            }
            %>

            <td>&nbsp;</td>
            <td class="label">Effective:</td>
            <td class="dataValue"><%=currentProductEffectiveDate==null?emptyString:currentProductEffectiveDate.formatShortDate()%></td>
          </tr>
<%
if (productEffectiveDate != null &&
productEffectiveDate.afterDay(currentProductEffectiveDate))
{
%>
          <tr>
            <td class="label">Product:</td>
            <td>&nbsp;</td>
            <td class="dataValueRed"><%=productCode%></td>
            <td>&nbsp;</td>
            <td class="label">Effective:</td>
            <td class="dataValueRed"><%=productEffectiveDate.formatShortDate()%></td>
          </tr>
<%
}
String nextProduct = memVO.getNextProductCode();
String nextProductDisplay = "";
if (nextProduct != null && nextProduct.trim().length() > 0)
{
%>
          <tr>
            <td class="label" valign="top"> </td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=nextProduct %></td>
            <td>&nbsp;</td>
            <td class="label" valign="top" colspan="2">from next renewal</td>
          </tr>
   <%
}
Collection optionList = memVO.getProductOptionList();
if (optionList == null || optionList.isEmpty())
{
   %>
          <tr>
            <td class="label" valign="top">Product options:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
   <%
}
else
{
  boolean firstOption = true;
  MembershipProductOption option = null;
  Iterator it = optionList.iterator();
  while (it.hasNext())
  {
     option = (MembershipProductOption) it.next();
   %>

          <tr>
            <td class="label" valign="top"><%if (firstOption){ out.print("Product options:"); firstOption = false;}%></td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=option.getProductBenefitTypeCode()%></td>
            <td>&nbsp;</td>
            <td class="label">Effective:</td>
            <td class="dataValue"><%=(option.getEffectiveDate() == null ? emptyString : option.getEffectiveDate().formatShortDate())%></td>
          </tr>
<%
  }
}
if (memVO.getProduct().isVehicleBased()) {
%>
		  <tr>
            <td class="label" valign="top">Rego:</td>
            <td></td>
            <td class="dataValue"><%=memVO.getRego()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">VIN:</td>
            <td></td>
            <td class="dataValue"><%=memVO.getVin()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Vehicle:</td>
            <td></td>
            <td class="dataValue"><%=memVO.getYear()%> <%=memVO.getMake()%> <%=memVO.getModel()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
<%
}
%>
          <tr>
            <td class="label">Join date:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><a title="tier code=<%=memVO.getMembershipTier()%>"><%=(memVO.getJoinDate() == null ? emptyString : memVO.getJoinDate().formatShortDate())%></a></td>
            <td>&nbsp;</td>
            <td class="dataValue" colspan="2"><%=memVO.getMembershipYears().formatForDisplay(Interval.FORMAT_YEAR,Interval.FORMAT_YEAR)%></td>
          </tr>
<%--
          <tr>
            <td class="label" valign="top">
            Joined in current<br/>
            membership period?
            </td>
            <td>&nbsp;</td>
            <td class="dataValue" valign="top"><%=memVO.hasJoinedInCurrentPeriod()?"<img src=\""+CommonConstants.DIR_IMAGES+"/check.gif\">":"<img src=\""+CommonConstants.DIR_IMAGES+"/cross.gif\">"%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
--%>
          <tr>
            <td class="label">Commence date:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=(memVO.getCommenceDate() == null ? emptyString : memVO.getCommenceDate().formatShortDate())%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
<%
Interval term = memVO.getMembershipTerm();
String termText = (term == null ? emptyString : term.formatForDisplay(Interval.FORMAT_YEAR,Interval.FORMAT_MONTH));
%>
          <tr>
            <td class="label">Membership term:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=termText%></td>
            <td>&nbsp;</td>
            <td class="label">Profile:</td>
            <td class="dataValue"><%=memVO.getMembershipProfileCode()%></td>
          </tr>
<%
Collection discountList = memVO.getDiscountList();
if (discountList == null || discountList.isEmpty())
{
%>
          <tr>
            <td class="label" valign="top">Recurring discounts:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
<%
}
else
{
   boolean firstDisc = true;
   MembershipDiscount discVO = null;
   Iterator it = discountList.iterator();
   while (it.hasNext())
   {
      discVO = (MembershipDiscount) it.next();
%>
          <tr>
            <td class="label" valign="top"><%if (firstDisc) { out.print("Recurring discounts:"); firstDisc = false;}%></td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=discVO.getDiscountTypeCode()%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
<%
   }
}
%>
          <tr>
            <td class="label" valign="top">Payment method:</td>
            <td>&nbsp;</td>
            <td colspan="4">
<%
if (!memVO.isHistory())
{
%>
               <a title="View the payment method details"
               class="actionItem"
               href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_viewPaymentMethod&membershipID=<%=memVO.getMembershipID()%>">
               <%=(ddAuthority != null ? PaymentMethod.DIRECT_DEBIT : PaymentMethod.RECEIPTING)%>
               </a>
<%
} else {
%>
               <%=(ddAuthority != null ? PaymentMethod.DIRECT_DEBIT : PaymentMethod.RECEIPTING)%>
<%
}
%>
            </td>
          </tr>
          <tr>
          <td>Receive Batch Email renewals?</td><td>&nbsp;</td><td colspan="4">
          	<img src="<%=CommonConstants.DIR_IMAGES%>/<%=(!memVO.definedEmailRenewals() ? "icon_question.jpg" : (memVO.receivesEmailRenewals() ? "check.gif" : "cross.gif"))%>">
          </td>
          </tr>
        </table>
<%
log("view 1");
if (memVO.isHistory())
{
log("view 2");
%>
         <br>
         <center><hr/></center>
         <h2>Membership cards</h2>
         <%@ include file="/membership/ViewMembershipCardInclude.jsp"%>
         <br>
         <center><hr/></center>
         <h2>Group members</h2>
<%
   if (memGroupVO != null)
   {
   log("view 3");
%>
        <table border="0" cellspacing="1" cellpadding="3">
<%
      Collection groupDetailList = memGroupVO.getMembershipGroupDetailList();
      Iterator groupDetailIterator = groupDetailList.iterator();
      MembershipGroupDetailVO memGroupDetVO;
      MembershipVO groupMemVO;
      ClientVO groupClientVO;
      String addressee;
   log("view 4");      
      while (groupDetailIterator.hasNext())
      {
   log("view 5");      
         memGroupDetVO = (MembershipGroupDetailVO) groupDetailIterator.next();
         //get membership for display of client and high level membership details only. Do not use
         //expiry as the MembershipGroupDetailVO does not support point in time analysis.
         groupMemVO = memMgr.getMembership(memGroupDetVO.getMembershipGroupDetailPK().getMembershipID());
         if (groupMemVO != null)
         {
   log("view 6");         
            addressee = memGroupDetVO.isPrimeAddressee() ? "<span class=\"helpTextSmall\">(addressee)</span>" : "";
            groupClientVO = groupMemVO.getClient();
            if (groupClientVO != null)
            {
   log("view 7");            
%>
          <tr>
            <td class="dataValue" colspan="3">
               <%=groupClientVO.getDisplayName()%> <%=addressee%>&nbsp;&nbsp;<span class="label">Number: </span><%=groupClientVO.getClientNumber()%>
            </td>
          </tr>
          <tr>
            <td width="10">&nbsp;</td>
            <td class="label">Membership:</td>
            <td class="dataValue"><%=groupMemVO.getMembershipNumber()%>&nbsp;</td>
          </tr>
<%
            }
         }
      }
%>

        </table>
<%
   }
}
%>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
         <!-- View data links -->
         <table border="0" cellpadding="5" cellspacing="1">
         <tr>
         <td colspan="5" class="helpText"><s:property value="message"/></td>
         </tr>
         <tr>
         <td colspan="5" class="dataValueRed"><s:actionerror/></td>
         </tr>
         <tr>
<%
// Don't show the actions button if we are viewing a membership history
if (!memVO.isHistory())
{
%>
<%--
Can't get the pop up menu to work on images
            <td>
               <a onclick="return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the actions menu');MM_swapImage('ActionButton','','<%=CommonConstants.DIR_IMAGES%>/ActionButton_over.gif',1);return document.MM_returnValue" >
                  <img name="ActionButton" src="<%=CommonConstants.DIR_IMAGES%>/ActionButton.gif" border="0" alt="Show the actions menu">
               </a>
            </td>
--%>
            <td><%=menu%></td>
<%
  if (showMemberCards) {
 %>
            <td><a class="actionItem" href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_viewCards&membershipID=<%=memVO.getMembershipID()%>">Cards</a></td>
<%
  }
 %>
            <td><a class="actionItem" href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_viewDocuments&membershipID=<%=memVO.getMembershipID()%>">Documents</a></td>
            <%--<td><a class="actionItem" href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_viewVehicles&membershipID=<%=memVO.getMembershipID()%>">Vehicles</a></td>--%>
<%
}
if (memVO.isGroupMember(true)) //ignore exceptions
{
%>
            <td><a class="actionItem" href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_viewGroupMembers&membershipID=<%=memVO.getMembershipID()%>">Group members</a></td>
<%
}
%>
            <td><a class="actionItem" href="<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembership_viewHistory&membershipID=<%=memVO.getMembershipID()%>">History</a></td>
         </tr>
         </table>
      </td>
    </tr>
  </table>
</body>
</html>
<%
//log(MethodCounter.outputCounts());
%>
