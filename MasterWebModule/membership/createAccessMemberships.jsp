<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.MembershipUIConstants"%>
<html>
<head>
<title>
Create Access Memberships
</title>
<%=CommonConstants.getRACTStylesheet()%>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/Utilities.js") %>
<script language="javascript">

function validate()
{
  return validateRunTime(document.all.runHours, document.all.runMins);
}

</script>
</head>
<body>

<%
DateTime now = new DateTime();
%>

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
   <form name="createAccessMemberships"
         method="post"
         action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
   <input type="hidden" name="event" value="createAccessMemberships">
   <tr valign="top">
      <td>
        <table>
          <tr>
            <td colspan="2">
              <h2>Create Access Memberships</h2>
            </td>
          </tr>
          <tr> 
            <td>Effective Date:</td>
            <td>
              <input type="text" name="effectiveDate" value="<%=now.formatShortDate()%>"/>
            </td>
          </tr>
          <tr>
            <td>Create memberships?</td>
            <td>
              <input type="checkbox" name="createMemberships" value="true">
            </td>
          </tr>
          <tr >
		      <td colspan="2">
		        <%@include file="/common/scheduleInclude.jsp"%>
		      </td>
	    </tr>
        </table>
      </td>
   </tr>
   <tr valign="bottom">
      <td>
         <table border="0" cellpadding="5" cellspacing="0" >
            <tr>
               <td>
                  <input type="submit" name="btnSubmit" value="OK">
               </td>
            </tr>
         </table>
      </td>
   </tr>
   </form>
</table>
</body>
</html>
