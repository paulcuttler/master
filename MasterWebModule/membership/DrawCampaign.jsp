<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.ract.payment.ui.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="en-AU" />

<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>
		<script type="text/javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>

	    <meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0"> 
	
		<style type="text/css">
			.errorMessage { color: red; }			
		</style>
		<title>Draw Campaign</title>
	</head>
	<body>
		<h1>Draw Campaign</h1>
		<s:actionmessage />
		<s:actionerror />
		<s:fielderror />
		
		<s:form>		
			<s:select label="Select campaign to draw" 
					  name="ref" list="campaignList" 
					  headerKey="" headerValue="Please select"
					  listKey="campaignRef" listValue="friendlyName" />
			<s:textfield name="numWinners" label="Enter number of winners" />
			<s:submit />
		</s:form>
		
		<s:if test="drawnCampaignList != null and !drawnCampaignList.isEmpty()">
		  <h2>Previously Drawn Campaigns</h2>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr class="headerRow">
		    <td class="listHeadingPadded">Campaign</td>
		    <td class="listHeadingPadded">Drawn</td>
		    <td class="listHeadingPadded">Winning client numbers</td>
		    <td class="listHeadingPadded">Action</td>
	      </tr>
		  <s:iterator value="drawnCampaignList" status="istatus">
		    <tr class="${istatus.even ? 'even' : 'odd'}Row">
		      <td style="vertical-align:top;"><s:property value="friendlyName"/></td>
		      <td style="vertical-align:top;"><s:date name="drawnDate" /></td>
		      <td style="vertical-align:top;"><s:property value="winners.replaceAll(',','<br/>')" escape="false" /></td>
		      <td><input type="button" value="Clear" onclick="if (confirm('This will clear the winners list and drawn date, are you sure?')) { location.href='DrawCampaign.action?ref=${campaignRef}&clear=true' }" /></td>
	        </tr> 
		  </s:iterator>
		  </table>
		</s:if>
						
	</body>
</html>