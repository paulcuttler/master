<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.user.UserSession"%>
<%@ include file="/membership/TransactionInclude.jsp"%>
<%
   UserSession userSession = UserSession.getUserSession(request.getSession());
   Collection productList = refMgr.getProductListByStatus(ProductVO.STATUS_ACTIVE);
   Iterator productListIterator = null;
   ProductVO productVO = null;
   ProductVO groupProductVO = transGroup.getSelectedProduct();
   String memProdCode = null;
   if (groupProductVO != null)
      memProdCode = groupProductVO.getProductCode();
%>
<html>
<head>
<title><%=transGroup.getTransactionHeadingText()%> - Select a membership product</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js">
</script>
<script language="JavaScript">

   // Holds the currently selected product code (cleaned) so that
   // it can be hidden when a new product code is selected.
   var currentSelectedProduct = '';

   function initialise(){
      // Hide all of the product descriptions
<%
if (productList != null)
{
   productListIterator = productList.iterator();
   while (productListIterator.hasNext())
   {
      productVO = (ProductVO) productListIterator.next();
      String cleanCode = HTMLUtil.cleanString(productVO.getProductCode());
      out.println("document.all.pd_" + cleanCode + ".style.display = \"none\";");   // Hide product description row
      out.println("document.all.pb_" + cleanCode + ".style.display = \"none\";");   // Hide product benefits row
   }
}
%>
      // Show the selected product description
      selectProduct(document.all.productSelected.options[document.all.productSelected.selectedIndex].text);

      // Set the state of the buttons
      buttonSwitch();
   }

   function selectProduct(productCode)
   {
      buttonSwitch();
      if (productCode != '')
      {
         if (currentSelectedProduct != '')
         {
            // Hide the currently displayed product description row.
            expression = 'document.all.pd_' + currentSelectedProduct + '.style.display = "none"';
            eval(expression);

            // Hide the currently displayed product benefit row.
            expression = 'document.all.pb_' + currentSelectedProduct + '.style.display = "none"';
            eval(expression);
         }

         //Clean the product code
         cleanProductCode = cleanString(productCode);

         // Show the selected product description
         expression = 'document.all.pd_' + cleanProductCode + '.style.display = ""';
         eval(expression);

         // Show the selected product benefits
         expression = 'document.all.pb_' + cleanProductCode + '.style.display = ""';
         eval(expression);

         // Set the product code as the currently selected one.
         currentSelectedProduct = cleanProductCode;
      }
   }

   var oldProductCode = <%="\"" + (groupProductVO != null ? groupProductVO.getProductCode() : "") + "\";"%>

   function buttonSwitch ()
   {
<%
      // If we are doing a change of product transaction:
      // If the product has not been changed then don't show any buttons.
      // If the product has been changed to a lower risk product then show the save button.
      // If the product has been changed to an equivalent or higer risk product
      // then show the next button.  Product sort order indicates risk level.
      if (groupProductVO != null && MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transGroup.getTransactionGroupTypeCode()))
      {
         out.println("if (document.all.productSelected.options[document.all.productSelected.selectedIndex].text == oldProductCode)");
         out.println("{");
         out.println("   document.all.btnSaveCol.style.display = \"none\";");
         out.println("   document.all.btnNextCol.style.display = \"none\";");
         out.println("}");
         out.println("else if (document.all.productSelected.selectedIndex > " + groupProductVO.getSortOrder() + ")");
         out.println("{");
         out.println("   document.all.btnSaveCol.style.display = \"\";");
         out.println("   document.all.btnNextCol.style.display = \"none\";");
         out.println("}");
         out.println("else {");
         out.println("   document.all.btnSaveCol.style.display = \"none\";");
         out.println("   document.all.btnNextCol.style.display = \"\";");
         out.println("}");
      }
      out.println("return true;");
%>
   }

</script>
</head>
<body onload="initialise(); MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif');">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <form name="productForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
  <input type="hidden" name="event" value="selectProduct_btnNext">
  <tr valign="top">
    <td>
      <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Select a membership product</h1>
      <table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <table border="0" cellspacing="0" cellpadding="3">
          <tr>
             <td valign="top" class="label" width="60">Product:</td>
             <td valign="top">
               <select name="productSelected" onchange="selectProduct(document.all.productSelected.options[document.all.productSelected.selectedIndex].text);">
<%
// Fill the select list with the products.
// If the membership already has a product associated with it then make
// it the selected one otherwise make the indicated default product the selected one.
String productListString = "";
if (productList != null)
{
   String selected = null;
   productListIterator = productList.iterator();
   while (productListIterator.hasNext())
   {
      productVO = (ProductVO) productListIterator.next();
      if (memProdCode != null)
      {
         if (productVO.getProductCode().equals(memProdCode))
            selected = " selected";
         else
            selected = "";
      }
      else {
         selected = (productVO.isDefault() ? " selected" : "");
      }

      out.println("<option" + selected + ">" + productVO.getProductCode() + "</option>");
      if (productListString.length() > 1) {
         productListString += ", ";
      }
      productListString += "\"" + productVO.getProductCode() + "\"";
   }
}
%>
               </select>
             </td>
             <td valign="top">
               <table border="0" cellspacing="0" cellpadding="0">
<%
// Construct the table that holds the product description.
// Each row can be addressed through it's ID and set to be visible or invisible.
if (productList != null)
{
   productListIterator = productList.iterator();
   while (productListIterator.hasNext())
   {
      productVO = (ProductVO) productListIterator.next();
      String cleanCode = HTMLUtil.cleanString(productVO.getProductCode());
      out.println("<tr id=\"pd_" + cleanCode + "\"><td><img src=\"" + CommonConstants.DIR_IMAGES + "/clear.gif\" width=\"10\" height=\"5\" border=\"0\">" + productVO.getDescription() + "");
      if (productVO.isVehicleBased()) {
      	if (userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN)) {
	   		out.println("<table>");
	   		out.println("<tr><td>Rego:</td><td><input type=\"text\" name=\"rego_" + cleanCode + "\" /></td></tr>");
		    out.println("<tr><td>VIN:</td><td><input type=\"text\" name=\"vin_" + cleanCode + "\" /></td></tr>");
	   		out.println("<tr><td>Year:</td><td><input type=\"text\" name=\"year_" + cleanCode + "\" /></td></tr>");
	   		out.println("<tr><td>Make:</td><td><input type=\"text\" name=\"make_" + cleanCode + "\" /></td></tr>");
		    out.println("<tr><td>Model:</td><td><input type=\"text\" name=\"model_" + cleanCode + "\" /></td></tr>");
		    out.println("</table>");
	    }
      }
      out.println("</td></tr>");
   }
}
%>
               </table>
             </td>
           </tr>           
			
           </table>
         </td>
       </tr>
       <tr>
         <td><img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="0" height="10" border="0"></td>
         <td></td>
       </tr>
<%
// Construct the table that shows the product benefits.
// Each row can be addressed through it's ID and set to be visible or invisible.
if (productList != null)
{
   productListIterator = productList.iterator();
   while (productListIterator.hasNext())
   {
      productVO = (ProductVO) productListIterator.next();
      String cleanProductCode = HTMLUtil.cleanString(productVO.getProductCode());
%>
       <tr id="pb_<%=cleanProductCode%>">
         <td>
<%
      Collection benefitList = productVO.getProductBenefitList(true,false);
      if (benefitList != null)
      {
         // Construct the table that holds the list of benefits for each product.
%>
           <div style="height:300; overflow:auto; border-width: 1; border-style: solid;">
           <table border="0" cellspacing="0" cellpadding="3">
<%
         Iterator it5 = benefitList.iterator();
         boolean first = true;
         String optional = null;
         while (it5.hasNext())
         {
            ProductBenefitTypeVO benefit = (ProductBenefitTypeVO) it5.next();
            String benefitCode = benefit.getProductBenefitCode();
            String cleanBenefitCode = HTMLUtil.cleanString(benefitCode);
%>
           <tr>
<%
            if (first)
            {
               out.println("<td valign=\"top\" width=\"60\" class=\"label\">Benefits:</td>");
               first = false;
            }
            else
            {
               out.println("<td></td>");
            }
            optional = (benefit.isOptional()) ? "(optional)" : "";
%>
             <td class="dataValue" valign="top" nowrap><%=benefitCode%></td>
             <td class="helpTextSmall" valign="top"><%=optional%></td>
             <td><img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5" border="0"></td>
             <td><%=benefit.getDescription()%></td>
           </tr>
<%
         }
%>
           </table>
           </div>
<%
      }
%>
         </td>
       </tr>
<%
   }
}
%>
       </table>
     </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
      <tr id="nextButtons">
            <td>
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
            </td>
            <td id="btnNextCol">
               <a onclick="document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
                  <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Show the next page">
               </a>
            </td>
            <td id="btnSaveCol" style="display:none">
               <a onclick="document.all.event.value='selectProduct_btnSave'; document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('The downgrade of product will be applied on the next renewal.');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
                  <img name="SaveButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Save the product change.">
               </a>
            </td>
      </tr>
      </table>
    </td>
  </tr>
  </form>
</table>
</body>
</html>

