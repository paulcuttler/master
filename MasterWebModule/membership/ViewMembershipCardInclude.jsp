<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.util.*"%>

<input type="hidden" name="cardID">

<%
MembershipVO memVOForCard = (MembershipVO) request.getAttribute("membershipVO");
boolean noCardsFound = true;

if (memVOForCard != null)
{
   // get the list of membership cards that the membership has.
   Collection memCardList = memVOForCard.getMembershipCardList(true);
   Collections.reverse((ArrayList)memCardList);
   if (memCardList != null && !memCardList.isEmpty())
   {
      noCardsFound = false;
      CommonMgr commonMgrforCard = CommonEJBHelper.getCommonMgr();

      // Work out the expected next membership expiry date
      String defaultMembershipTermText = commonMgrforCard.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,"DEFTERM");
      Interval defaultMembershipTerm = null;
      try
      {
         defaultMembershipTerm = new Interval(defaultMembershipTermText);
      }
      catch (Exception e)
      {
         throw new ServletException("The default membership term specification from system parameters '" + defaultMembershipTermText + "' is not valid. : " + e.getMessage());
      }
      DateTime nextMembershipExpiry = memVOForCard.getExpiryDate().add(defaultMembershipTerm);

      // Work out a date 8 weeks (56 days) in the future ie 2 weeks before the renewal run (6 weeks in advance)
//      DateTime eightWeeksFromToday = new DateTime().add(new Interval("0:0:56"));

      MembershipCardVO memCardVO = null;
      ReferenceDataVO refDataVO;
      String cardRequestReason = null;
      String cardNumber = null;
      String cardName = null;
      String cardProductCode = null;
      String cardTierCode = null;
      String cardRego = null;
      DateTime cardJoinDate = null;
   //   DateTime cardExpiry = null;
%>
         <table width="100%" border="0" cellspacing="0" cellpadding="3">
           <tr class="headerRow" valign="top">
             <td class="listHeadingPadded" nowrap>Requested</td>
             <td class="listHeadingPadded" nowrap>User</td>
             <td class="listHeadingPadded" nowrap>Issued</td>
             <td class="listHeadingPadded" nowrap>Expires</td>
             <td class="listHeadingPadded" nowrap>Card number</td>
             <td class="listHeadingPadded" nowrap>Card name</td>
             <td class="listHeadingPadded" nowrap align="right">Issue</td>
             <td class="listHeadingPadded" nowrap>Tier Code</td>
             <td class="listHeadingPadded" nowrap>Reason</td>
             <td></td>
           </tr>
<%
     Iterator memCardListIT = memCardList.iterator();
     int counter = 0;
     while (memCardListIT.hasNext())
     {
         counter++;
         memCardVO = (MembershipCardVO) memCardListIT.next();
         cardNumber = memCardVO.getCardNumber();
         cardName = memCardVO.getCardName();
         cardProductCode = memCardVO.getProductCode();
         cardJoinDate = memCardVO.getJoinDate();
         cardTierCode = memCardVO.getTierCode();
         cardRego = memCardVO.getRego();
         refDataVO = commonMgrforCard.getReferenceData(memCardVO.getReasonType(),memCardVO.getReasonCode());
         if (refDataVO != null){
            cardRequestReason = refDataVO.getDescription();
         }
         else {
            cardRequestReason = "Not available!";
         }
%>
           <tr class="<%=HTMLUtil.getRowType(counter)%>" valign="top">
             <td>
                <a class="label" title="cardID=<%=memCardVO.getCardID()%>"><%=memCardVO.getRequestDate() %></a>
             </td>
             <td><%=memCardVO.getUserId()!=null?memCardVO.getUserId():""%></td>
             <td><%=memCardVO.getIssueDate()!=null ? memCardVO.getIssueDate().formatShortDate() : "<span class=\"helpText\">Not Issued</span>"%></td>
             <td><%=memCardVO.getExpiryDate()!=null ? memCardVO.getExpiryDate().formatShortDate() : ""%></td>
             <td nowrap><%=(cardNumber != null ? CardHelper.formatCardNumber(memCardVO.getCardNumber()) : "")%></span></td>
             <td><%=memCardVO.getCardName()==null ? "" : memCardVO.getCardName()%></td>
             <td align="right"><%=String.valueOf(memCardVO.getIssuanceNumber())%></td>
             <td><%=memCardVO.getTierCode()!=null?memCardVO.getTierCode():""%></td>
             <td><%=cardRequestReason%></td>
             <td>
                <input <%=memCardVO.getIssueDate()!=null?"disabled title=\"The card has been issued.  Cannot remove.\"":"title=\"Remove the card request.\""%> type="button" value="Remove" onclick="removeCardRequest(<%=memCardVO.getCardID()%>);">
             </td>
           </tr>
<%
         if (cardName != null && cardName.length() > 0) {
%>
           <tr class="<%=HTMLUtil.getRowType(counter)%>">
             <td></td>
             <td nowrap class="dataValue">Card name:</td>
             <td colspan="8"><%=cardName%></td>
           </tr>
<%
         }
         if (cardProductCode != null && cardProductCode.length() > 0) {
%>
           <tr class="<%=HTMLUtil.getRowType(counter)%>">
             <td></td>
             <td nowrap class="dataValue">Product:</td>
             <td colspan="8"><a title="<%=cardTierCode%>"><%=cardProductCode%></a></td>
           </tr>
<%
         }
         if (memVOForCard.getProduct().isVehicleBased()) {
%>
           <tr class="<%=HTMLUtil.getRowType(counter)%>">
             <td></td>
             <td nowrap class="dataValue">Rego:</td>
             <td colspan="8"><%=cardRego%></td>
           </tr>
<%
         }
         if (cardJoinDate != null) {
%>
           <tr class="<%=HTMLUtil.getRowType(counter)%>">
             <td></td>
             <td nowrap class="dataValue">Since:</td>
             <td colspan="8"><%=DateUtil.formatDate(cardJoinDate,"yyyy")%></td>
           </tr>
   <%
         }
     }
   %>
     </table>
   <%
   }
}
//there should be one
if (noCardsFound)
{
%>
   <span class="helpText">No cards were found!</span><br>
<%
}
%>
