
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>

<%
String saveEvent     = "maintainFeeType_btnSave";
String deleteEvent   = "maintainFeeType_btnDelete";
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;

FeeTypeVO feeTypeVO = (FeeTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_FEE_TYPE_VO);
String    origCode  = (String) request.getSession().getAttribute("origCode");
String    statMsg   = (String) request.getAttribute("statusMessage");
String    refresh   = (String) request.getAttribute("Refresh");

String             feeTypeCode   = "";
String             description   = "";
boolean            isDefault     = false;
String             status        = "";
Integer            earned        = null;
Integer            unearned      = null;
boolean termMultiplier = false;
boolean onRoad = false;
boolean joinType = false;
FeeTypeVO          ftVO          = null;

// Get the list of valid account for selection

MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
Collection accountList = membershipMgr.getMembershipAccounts();
SortedSet sAccountList = new TreeSet();
if ( accountList != null & accountList.size() > 0 )
{
   Iterator<MembershipAccountVO> it = accountList.iterator();
   while( it.hasNext() )
   {
      MembershipAccountVO ma = it.next();
      sAccountList.add( new AdminCodeDescription( ma.getAccountID().toString(),ma.getAccountTitle()));
   }
}


// Get the discount list
MembershipRefMgr refMgr        = MembershipEJBHelper.getMembershipRefMgr();
ArrayList availableDiscounts   = (ArrayList) refMgr.getActiveDiscountTypeList();
Collection discountList          = null;
DiscountTypeVO discountType      = null;
AdminCodeDescription ad = null;
SortedSet selDiscountList = new TreeSet();
SortedSet availDiscountList = new TreeSet();

SortedSet sStatusList = new TreeSet();
sStatusList.add(new AdminCodeDescription(FeeTypeVO.STATUS_ACTIVE,FeeTypeVO.STATUS_ACTIVE));
sStatusList.add(new AdminCodeDescription(FeeTypeVO.STATUS_INACTIVE,FeeTypeVO.STATUS_INACTIVE));

if (feeTypeVO.getFeeTypeCode() == null )
{
  feeTypeCode = "";
  description  = "";
  status = "";
  earned = new Integer("0");
  unearned = new Integer("0");
}
else
{
  discountList = feeTypeVO.getAllowableDiscountList();

  description  = feeTypeVO.getDescription();
  status       = feeTypeVO.getStatus();
  earned       = (feeTypeVO.getEarnedAccountID() == null) ? new Integer("0") : feeTypeVO.getEarnedAccountID();
  unearned     = (feeTypeVO.getUnearnedAccountID() == null) ? new Integer("0") : feeTypeVO.getUnearnedAccountID();
  feeTypeCode  = feeTypeVO.getFeeTypeCode();
  termMultiplier = feeTypeVO.isTermMultiplier();
  onRoad = feeTypeVO.isOnRoad();
  joinType = feeTypeVO.isJoinType();
}
description = StringUtil.makeSpaces(description);

if ( !(discountList == null)  )
{
   Iterator it = discountList.iterator();
   while( discountList.size() > 0 & it.hasNext() )
   {
      discountType = (DiscountTypeVO) it.next();
      selDiscountList.add( new AdminCodeDescription( discountType.getDiscountTypeCode(),discountType.getDescription()));
   }
}

if ( !(availableDiscounts == null) )
{
   Iterator it = availableDiscounts.iterator();
   while ( it.hasNext() & availableDiscounts.size() > 0 )
   {
      discountType = (DiscountTypeVO) it.next();
      ad = new AdminCodeDescription( discountType.getDiscountTypeCode(),discountType.getDescription());

      if ( !(selDiscountList==null) & !selDiscountList.contains(ad) )
      {
         availDiscountList.add(ad);
      }
   }
}
%>

<head>
<title>Maintain Discount</title>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body class="rightFrame" onload="refreshList(document.all.refresh,'<%=listPage%>');">

<form name="mainForm"
      method="post"
      action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>"
>
  <input type="hidden" name="event" value="">
  <input type="hidden" name="feeType" value="<%=feeTypeCode%>">
  <input type="hidden" name="earnedID" value="">
  <input type="hidden" name="unearnedID" value="">
  <input type="hidden" name="listCode" value ="">
  <input type="hidden" name="refresh" value ="<%=refresh%>">

 <div id="mainpage" class="pg">
 <h1>Maintain Fee Type</h1>
   <div id="contentpage" class="cnt">
<!--
START FEE TYPE BODY
-->
        <table border="0" cellspacing="4" cellpadding="0" bgcolor="#FEFEFE">

          <tr>
            <td class="label" valign "top">Fee Type Code:</td>
            <td class="dataValue">&nbsp;</td>
            <td class="dataValue" valign "top">
                <%=HTMLUtil.inputOrReadOnly("feeTypeCode",feeTypeCode)%>
            </td>
          </tr>
          <tr>
            <td class="label" valign "top">Status:</td>
            <td class="dataValue">&nbsp;</td>
            <td class="dataValue" valign "top">
               <%=HTMLUtil.selectList("selectStatus",sStatusList,status,FeeTypeVO.STATUS_ACTIVE,FeeTypeVO.STATUS_ACTIVE)%>

            </td>
          </tr>
          <tr>
            <td class="label" valign "top">Earned account:</td>
            <td class="helpTextSmall">(Required)</td>
            <td class="dataValue" valign "top">
              <%=HTMLUtil.selectList("selectEarned",sAccountList,earned.toString(),"","No Earned Account")%>
            </td>
          </tr>
          <tr>
            <td class="label" valign "top">Unearned account:</td>
            <td class="helpTextSmall">(Optional)</td>
            <td class="dataValue" valign "top">
                <%=HTMLUtil.selectList("selectUnearned",sAccountList,unearned.toString(),"","No Unearned Account")%>
            </td>
          </tr>
          <tr>
            <td class="label"  valign "top" >Description:</td>
            <td class="dataValue" >&nbsp;</td>
            <td class="dataValue"  valign "top">
              <textarea type="textarea" name="description" rows="3" cols="50" onkeyup="checkLength(this, 200);"><%=description%></textarea>
            </td>
          </tr>
          <tr>
            <td class="label"  valign "top" >Multiply terms for fee calculation?</td>
            <td class="dataValue" >&nbsp;</td>
            <td class="dataValue"  valign "top">
                    <input type="checkbox" name="termMultiplier" <%=termMultiplier?"checked":""%>>
            </td>
          </tr>
          <tr>
            <td class="label"  valign "top" >On Road?</td>
            <td class="dataValue" >&nbsp;</td>
            <td class="dataValue"  valign "top">
                    <input type="checkbox" name="onRoad" <%=onRoad?"checked":""%>>
            </td>
          </tr>
          <tr>
            <td class="label"  valign "top" >Join?</td>
            <td class="dataValue" >&nbsp;</td>
            <td class="dataValue"  valign "top">
                    <input type="checkbox" name="joinType" <%=joinType?"checked":""%>>
            </td>
          </tr>          
          <tr>
          <td colspan="3">
                  <%=HTMLUtil.codeDescriptionTable (  "Allowable Discounts.",
                                                      availDiscountList,
                                                      "maintainFeeTypeDiscount_btnAdd",
                                                      "Double click to ADD this discount.",
                                                      "No Discounts to be selected"
                                                   )
                  %>

          </td>
          </tr>
          <tr>
          <td colspan="3">

                  <%=HTMLUtil.codeDescriptionTable (  "Selected Discounts.",
                                                      selDiscountList,
                                                      "maintainFeeTypeDiscount_btnRemove",
                                                      "Double click to REMOVE this discount.",
                                                      "No selected Discounts"
                                                   )
                  %>
         </td>
         </tr>
         </table>
<!--
FINISH FEE TYPE BODY
-->
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSaveAndDelete(saveEvent,deleteEvent,"Fee Type", statMsg )%>
   </div>
</div>
</form>
</body>
</html>

