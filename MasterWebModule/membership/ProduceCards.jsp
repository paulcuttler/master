<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.MembershipUIConstants"%>
<html>
<head>
<title>
Produce Membership Cards
</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>

<%
//get product list
MembershipRefMgr memRefMgr = MembershipEJBHelper.getMembershipRefMgr();
Collection productList = memRefMgr.getProductList();
ProductVO product = null;
%>

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
   <form name="productCardExtract"
         method="post"
         action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">
   <input type="hidden" name="event" value="produceCards">
   <tr valign="top">
      <td>
         <h1>Produce Membership Cards Extract</h1>

        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="2"> <span class=""> Produces card extract files for processing by our nominated card production company.<br/> Note: there is no overlap for any of the options below.</span> </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="mode" value="normal" checked>
            </td>
            <td>Normal Run</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="helpText">Produce card extract for the selected products only for new and
              replacement cards. </td>
          </tr>
          <%
              if (productList != null &&
                  productList.size() > 0)
                  {
                    Iterator productIt = productList.iterator();
                    while (productIt.hasNext())
                    {
                      product = (ProductVO)productIt.next();
                      if (!product.isVehicleBased()) {
              %>
          <tr>
            <td> </td>
            <td>
              <input type="checkbox" name="productCodes" value="<%=product.getProductCode()%>" checked>
              <%=product.getDescription()%></td>
          </tr>
          <%
                      }
                    }
                  }
              %>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="mode" value="loyalty">
            </td>
            <td> Issue new loyalty based cards only</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="helpText">Produce card extract for cards that have a reason code of &quot;tier
              change&quot; that have been auto-generated due to a change of tier.</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="mode" value="newAccess">
            </td>
            <td> Issue new access cards only</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="helpText">Produce card extract for access cards that have been generated
              by the bi-monthly access member creation program. The card files are split into Travel (TRV) and Insurance (INS).</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="mode" value="cmo">
            </td>
            <td> Issue CMO cards only</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="helpText">Produce card extract for CMO.</td>
          </tr>
        </table>
      </td>
   </tr>
   <tr valign="bottom">
      <td>
         <table border="0" cellpadding="5" cellspacing="0" >
            <tr>
               <td>

                  <input type="submit" name="btnSubmit" value="OK">
               </td>
            </tr>
         </table>
      </td>
   </tr>
   </form>
</table>
</body>
</html>
