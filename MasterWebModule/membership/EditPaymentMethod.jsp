<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.payment.ui.*"%>
<%@ page import="com.ract.payment.directdebit.*"%>
<%@ page import="com.ract.payment.bank.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.security.*"%>
<%@page import="com.ract.user.*"%>

<%@include file="/security/VerifyAccess.jsp"%>

<%
CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
String ddConfirmMessage = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,SystemParameterVO.DD_CONFIRM_MESSAGE);

String ddPolicyMessage = (String)request.getAttribute("ddPolicyMessage");

//TODO ddr date should be a system parameter
DateTime ddrDate = new DateTime().add(new Interval("0:0:14")); //14 days
PaymentMgr payMgr = PaymentEJBHelper.getPaymentMgr();
Collection paymentTypeList = payMgr.getPaymentTypeListForMembership();
//UserSession userSession = UserSession.getUserSession(session);

String transactionTypeCode = null;
TransactionGroup transGroup = null;
boolean isAdministrator = userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_USER_ADMIN);

MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");
if (memVO == null)
   throw new SystemException("Failed to get membership VO from request.");

double amountUnpaid = memVO.getAmountOutstanding(false, true);
log("amountUnpaid="+amountUnpaid);

MembershipGroupVO memGroup = memVO.getMembershipGroup();
int memCount = 0;
if (memGroup != null)
{
  memCount = memGroup.getMemberCount();
}

// Got a valid membership. Now see if the product is current.
String productCode = memVO.getProductCode();
DateTime productEffectiveDate = memVO.getProductEffectiveDate();

DirectDebitAuthority ddAuthority = memVO.getDirectDebitAuthority();
//may be receipting!!!

//if (ddAuthority == null)
//   throw new SystemException("Failed to get direct debit authority from membership VO.");

ClientVO clientVO = memVO.getClient();
String clientName = "";
String postalAddress = "";
String streetAddress = "";
String clientStatus = null;
if (clientVO != null)
{
   clientName = clientVO.getDisplayName();
   clientName = (clientName == null ? "unknown" : clientName);
   postalAddress = (clientVO.getPostalAddress() == null ? "" : clientVO.getPostalAddress().getSingleLineAddress());
   streetAddress = (clientVO.getResidentialAddress() == null ? "" : clientVO.getResidentialAddress().getSingleLineAddress());
}

String accountName = "";
String accountNumber = "";
//boolean approved = false;
String bsbNumber = "";
String cardType = "";
String creditCardExpiry = "";
String creditCardNumber = "";
String dayToDebit = "";
String frequency = null;
boolean isOwner = false;
boolean isApproved = false;
String paymentTypeCode = PaymentTypeVO.PAYMENTTYPE_CASH;
String verificationNumber = "";
boolean autoRenew = false;
String ccString = "";

if (ddAuthority != null)
{
   BankAccount bankAccount = ddAuthority.getBankAccount();
   isOwner = ddAuthority.isClientIsOwner();
   dayToDebit = Integer.toString(ddAuthority.getDayToDebit());
   isApproved = ddAuthority.isApproved();
   ddrDate = ddAuthority.getDdrDate();
   DirectDebitFrequency ddFrequency = ddAuthority.getDeductionFrequency();
   frequency = ddFrequency.getDescription();
   accountName = bankAccount.getAccountName();
   autoRenew = true;

   if (bankAccount instanceof DebitAccount)
   {
      DebitAccount debitAccount = (DebitAccount) bankAccount;
      bsbNumber = debitAccount.getBsbNumber();
      accountNumber = debitAccount.getAccountNumber();
      paymentTypeCode = PaymentTypeVO.PAYMENTTYPE_DIRECTDEBIT;
   }
   else if (bankAccount instanceof CreditCardAccount)
   {
      CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
      cardType = ccAccount.getCardTypeDescription();
      creditCardNumber = ccAccount.getCardNumber();
      creditCardExpiry = ccAccount.getCardExpiry();
      verificationNumber = ccAccount.getVerificationValue();
      paymentTypeCode = ccAccount.getPaymentType().getPaymentTypeCode();
      ccString = StringUtil.obscureFormattedCreditCardNumber(creditCardNumber);
      HttpSession sess = request.getSession();
      sess.setAttribute("oldCCNum", creditCardNumber);
      sess.setAttribute("oldCCString",ccString);
   }
}


%>
<html>
<head>
<%=CommonConstants.getRACTStylesheet()%>
<title>Edit Payment Method</title>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Validation.js">
</script>

<script language="JavaScript" src="/payment/scripts/Validation.js?v=1.0">
</script>


<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js">
</script>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js">
</script>

<script language="JavaScript">
   var paymentCashTypeList = new Array();  // Maps payment type codes to cash type codes
   var ptCCNumberSizeList = new Array();   // Maps payment type codes to credit card number lengths

<%
PaymentTypeVO payType;
Iterator paymentTypeListIterator = paymentTypeList.iterator();
while (paymentTypeListIterator.hasNext())
{
   payType = (PaymentTypeVO) paymentTypeListIterator.next();
   out.println("paymentCashTypeList['"+ payType.getPaymentTypeCode() +"'] = '" + payType.getCashTypeCode() + "';");
   out.println("ptCCNumberSizeList['"+ payType.getPaymentTypeCode() +"'] = " + payType.getCardNumberLength() + ";");
}
%>

</script>
</head>
<body onload="handleOnLoad(); MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif');">
<%
   // for easily adjusting column widths
   String indent = "5";

%>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
<input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
<input type="hidden" name="event" value="editPaymentMethod_btnSave">
<input type="hidden" name="ddrDate" value="<%=ddrDate%>" >
   <tr valign="top">
      <td>
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> Edit direct debit authority</h1>
        <table border="0" cellpadding="0" cellspacing="3">
          <tr>
            <td class="label">Client:</td>
            <td>&nbsp;</td>
            <td>
                 <a class="actionItemBlack" href="#" title="<%=streetAddress%>">
                 <span class="dataValue"><%=clientName%></span>
                 <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5" border="0">
                 <span class="label">Number: </span>
                 <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                 <span class="dataValue"><%=memVO.getClientNumber()%> </span>
                 </a>
            </td>
          </tr>
          <tr>
            <td class="label">Membership:</td>
            <td>&nbsp;</td>
            <td>
                 <a  class="actionItemBlack" title="ID = <%=(memVO.getMembershipID() == null ? "" : memVO.getMembershipID().toString())%>">
                 <span class="dataValue"><%=(memVO.getMembershipNumber() == null ? "" : memVO.getMembershipNumber())%></span>
                 <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                 <span class="dataValue"><%=productCode%></span>
                 <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                 <span class="dataValue"><%=MembershipUIHelper.formatMembershipStatusHTML(memVO.getStatus())%></span>
                 <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="10" height="5" border="0">
                 <span class="label">Expire:</span>
                 <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif" width="5" height="5" border="0">
                 <span class="dataValue"><%=MembershipUIHelper.formatMembershipExpiryDateHTML(memVO.getExpiryDate(),null)%></span>
                 </a>
            </td>
          </tr>
          <tr>
            <td colspan="3" height="10">&nbsp;</td>
          </tr>
        </table>

        <!-- start of payment method details -->
        <table border="0" cellpadding="3" cellspacing="0" >
          <tr>
            <td valign="top">Payment Method:</td>
            <td>
<%=MembershipUIHelper.getPaymentTypeHTML(paymentTypeList,paymentTypeCode,transactionTypeCode,memVO,transGroup,isAdministrator,true)%>
           </td>
           <td><img width="5" src="<%=CommonConstants.DIR_IMAGES%>/clear.gif"></td>
        </tr>
     </table>
      <table id="directDebitTable" style="display:none" cellpadding="3" cellspacing="0" >
         <tr id="ddFromDebitAccountHeadingRow">
            <td colspan="3" >
               <h3>Direct Debit from debit account:</h3>
            </td>
         </tr>
         <tr id="ddFromCCAccountHeadingRow" style="display:none">
            <td colspan="3" >
               <h3>Direct Debit from credit card account:</h3>
            </td>
         </tr>
         <tr id="bsbNumberRow">
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">BSB No:</td>
            <td><input type="text" name="bsbNumber" size="10" value="<%=bsbNumber%>" onchange="return validateBsbNumber(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%></td>
         </tr>
         <tr id="accountNumberRow">
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Account No:</td>
            <td><input type="text" name="accountNumber" size="20" value="<%=accountNumber%>" onchange="return validateAccountNumber(this);"/>&nbsp;<%=HTMLUtil.mandatoryItem()%></td>
         <tr>
         <tr id="cardNumberRow" style="display:none">
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Card No:</td>
            <td>
               <input type="hidden" name="oldCardNumber" value="<%=ccString%>"/> 
               <input type="text" name="cardNumber" size="20" value="<%=ccString%>" onkeypress="formatCardNumber(this)" />
                &nbsp;<%=HTMLUtil.mandatoryItem()%>
                &nbsp;<input class="helpTextSmall" type="text" id="cardNumberLength" size="10" style="border-style: none;" >
            </td>
         </tr>
         <tr id="cardExpiryRow" style="display:none">
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Card expiry:</td>
            <td><input type="text" name="cardExpiry" size="5" value="<%=creditCardExpiry%>" onchange="return validateCardExpiry(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%>&nbsp;<span class="helpTextSmall">Use the format mm/yy</span></td>
         </tr>
         <tr id="cardVerificationValueRow" style="display:none">
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Card verification value:</td>
            <td><input type="text" name="cardVerificationValue" value="<%=verificationNumber%>" size="5" /></td>
         </tr>
         <tr>
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Account Name:</td>
            <td><input type="text" name="accountName" size="20" value="<%=accountName%>" onchange="return validateAccountName(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%></td>
         <tr>
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Client is Account Owner:</td>
            <td><input type="checkbox" name="isOwner"  value="Yes" <%=(isOwner ? "checked" : "")%>/></td>
         </tr>

         <tr>
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Deduction Frequency:</td>
            <td>
              <%=MembershipUIHelper.getFrequencyHTML(memCount, frequency, memVO.getProductCode())%>
            </td>
         </tr>

         <tr>
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Day of month to debit:</td>
            <td><input type="text" name="dayToDebit" value="<%=dayToDebit%>" size="3" onchange="return validateDayToDebit(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%>&nbsp;<span class="helpTextSmall">Numbers 1 to 31 only.</span></td>
         </tr>
<!--
         <tr>
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">DDR Date:</td>
            <td><input type="text" name="ddrDate" size="10" value="<%=ddrDate.formatShortDate()%>" onchange="return validateDdrDate(this);" />&nbsp;<%=HTMLUtil.mandatoryItem()%>&nbsp;<span class="helpTextSmall">Use format dd/mm/yyyy. Note: choosing the current date will bypass current scheduling rules.</span></td>
         </tr>
-->
         <tr>
            <td width="<%=indent %>%">&nbsp;</td>
            <td class="label">Approved:</td>
            <td><img border="0" src="<%=CommonConstants.DIR_IMAGES + "/" + (isApproved ? "check.gif" : "cross.gif")%>"></td>
         </tr>
<%--
The fact that a direct debit has been entered indicates automatic renewal.
   <tr>
      <td width="<%=indent %>%">&nbsp;</td>
      <td class="label">Automatically renew:</td>
      <td><input type="checkbox" name="autoRenew"  value="Yes" <%=(autoRenew ? "checked" : "")%>></td>
   </tr>
--%>
      </table>


      <table id="receiptingTable" style="display:none" cellpadding="3" cellspacing="0" >
<%
//show an error if there are amounts outstanding
if (amountUnpaid > 0)
{
%>
      <tr>
        <td class="errorText">There is an amount outstanding <%=CurrencyUtil.formatDollarValue(amountUnpaid)%> in the direct debit system.
        </td>
      </tr>
      <tr>
        <td class="errorText">You may not change to the receipting payment method until the amount outstanding is paid.
        The amount in direct debit may be paid immediately through the receipting system.
        </td>
      </tr>
      <tr>
        <td class="errorText">You may also cancel the membership if the member does not intend to pay for the membership.
        </td>
      </tr>
<%
}
%>
      </table>
<!-- allow the same script to be used for selectPaymentMethod.jsp and editPaymentMethod.jsp-->
   <table id="repairTable" style="display:none" cellpadding="3" cellspacing="0"></table>
  </td>
</tr>
<tr valign="bottom">
   <td>
      <table cellpadding="5" cellspacing="0" >
         <tr>
            <td>
              <input type="button" onclick="history.back();" value="Back"/>
              <%--
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
               --%>
            </td>
            <td valign="top">
              <input type="button" onclick="submitPaymentMethodForm('<%=ddConfirmMessage%>','<%=ddPolicyMessage%>');" value="Save"/>
              <%--
               <a onclick="submitForm();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Save the changes');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
                  <img name="SaveButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Save the changes">
               </a>
               --%>
            </td>
         </tr>
      </table>
   </td>
</tr>
</form>
</table>
</body>

</html>
