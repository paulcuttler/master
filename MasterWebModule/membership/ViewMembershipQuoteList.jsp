<%@page import="com.ract.client.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.security.*"%>

<%
UserSession userSession = UserSession.getUserSession(session);
ClientVO clientVO = (ClientVO) request.getAttribute("client");
Vector quoteList = MembershipEJBHelper.getMembershipMgr().getMembershipQuoteListByClientNumber(clientVO.getClientNumber(),true);

String refresh = (String) request.getAttribute("refreshProductList");
boolean refreshProductList = "Yes".equalsIgnoreCase(refresh);
%>
<html>
<head>
<title>View Roadside Quote List</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>

<script language="JavaScript">

   function onLoadEventHandler()
   {
      MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif');
      if (<%=clientVO != null && refreshProductList%>) {
         parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber=<%=clientVO.getClientNumber()%>';
      }
   }

   function showQuoteDetail(quoteNumber)
   {
      var url = '<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembershipQuoteList_viewQuote&quoteNumber=' + quoteNumber;
      window.open(url, '', 'titlebar=yes,toolbar=no,status=no,scrollbars=yes,resizable=yes');
      return true;
   }
</script>

</head>
<body onload="onLoadEventHandler(); return true;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr valign="top">
   <td>
      <h1><%@ include file="/help/HelpLinkInclude.jsp"%> Roadside Quotes</h1>
      <%@ include file="/client/ViewClientInclude.jsp"%>
      <p>
<%
if (quoteList != null
&& !quoteList.isEmpty()) {
%>
      <table border="0" cellpadding="0" cellspacing="0">
<%
   MembershipQuoteComparator mqc = new MembershipQuoteComparator(true);
   Collections.sort(quoteList,mqc);
   MembershipQuote memQuote;
   for (int i = 0; i < quoteList.size(); i++) {
      memQuote = (MembershipQuote) quoteList.get(i);
%>
      <tr>
         <td class="listItemPadded">
            <a class="actionItem"
               title="View the quote details"
               href="#"
               onclick="showQuoteDetail(<%=memQuote.getQuoteNumber()%>); return true;">
            Quote <%=memQuote.getQuoteNumber()%>
            </a>
         </td>
         <td class="listItemPadded">
            <%=memQuote.getQuoteDate().formatShortDate()%>
         </td>
         <td class="listItemPadded">
            <%=memQuote.getTransactionGroupTypeCode()%>
         </td>
         <td class="listItemPadded" align="right">
            <%=CurrencyUtil.formatDollarValue(memQuote.getAmountPayable())%>
         </td>
         <td class="listItemPadded" align="right">
            <%=memQuote.getUserId()%>
         </td>
         <td class="listItemPadded" align="right">
            <%=memQuote.getSalesBranchCode()%>
         </td>
         <td class="listItemPadded">
            <%=(memQuote.isCurrent() ? "current" : "not current")%>
         </td>
         <td class="listItemPadded">
            <%=(memQuote.isAttached() ? "transacted" : "not transacted")%>
         </td>
         <td class="listItemPadded">
            <%= memQuote.getTransactionXML() != null ? ""+StringUtil.getSizeInKB(memQuote.getTransactionXML()) : "0"%>kb
         </td>
         <td>
         <%
         if (userSession != null
         &&  userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_EDIT_TRANSACTION))
         {
%>

         <form>
           <input type="hidden" name="event" value="editMembershipQuote">
           <input type="hidden" name="quoteNumber" value="<%=memQuote.getQuoteNumber()%>">
           <input type="submit" name="btnEdit" value="Edit" title="Edit this quote.">
         </form>
<%
         }
         %>
         </td>
      </tr>
<%
   }
%>
      </table>
<%
}
else {
   out.println("This client does not have any Roadside quotes.");
}
%>
   </td>
</tr>
<tr valign="bottom">
   <td>
        <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td>
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
          </td>
        </tr>
        </table>
   </td>
</tr>
</table>
</body>
</html>
