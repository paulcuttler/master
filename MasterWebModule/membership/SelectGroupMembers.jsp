<%@ page import="java.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.client.ui.ClientUIConstants"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.payment.PaymentException"%>
<%@ page import="com.ract.util.*"%>

<%@ include file="/membership/TransactionInclude.jsp"%>
<%
ClientVO contextClient = transGroup.getContextClient();
if (contextClient == null) {
   throw new SystemException("SelectGroupMembers.jsp : Failed to get context client from transaction group.");
}
String searchValue = (String) request.getAttribute("searchValue");
String transGroupTypeCode = transGroup.getTransactionGroupTypeCode();
%>
<html>
<head>
<title>Select Group Members</title>
<%=CommonConstants.getRACTStylesheet()%>
<%=CommonConstants.getSortableStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
   dojo.require("dojo.widget.*");
</script>

<script type="text/javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>

<script type="text/javascript">

   function handleOnLoad()
   {
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber='+<%=contextClient.getClientNumber()%>;
      MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/SearchButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/AddButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/RemoveButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/UndoButton_over.gif');
      return true;
   }

</script>
</head>
<%
// These variables are used repeatedly throughout the jsp.
SelectedClient selectedClient = null;
MembershipVO membershipVO = null;
String productCode = null;
String membershipExpiryDate = null;
String membershipStatus = null;
boolean existingGroupMember = false;
ClientVO clientVO = null;
AddressVO addrVO = null;
String postalAddress = null;
String checked = null;
String disableRadioButton = "";
boolean disabled = false;
String addButtonTitle = null;
String invalidReason = null;

// Set up the list of clients selected to be in the group
Collection selectedClientList = transGroup.getSelectedClientList();
int clientListSize = selectedClientList == null ? 0 : selectedClientList.size();
int clientSpaceCount = clientListSize == 0 ? 1 : selectedClientList.size();
int clientListDivHeight = 25 + (clientSpaceCount * 35);
clientListDivHeight = clientListDivHeight > 90 ? 90 : clientListDivHeight;
%>
<body onLoad="handleOnLoad();">

<form name="groupMembersForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
  <input type="hidden" name="event" value="selectGroupMembers_btnSearch">
  <input type="hidden" name="selectedClientKey" value="0">
  
  <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
  <tr valign="top">
    <td>
      <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Select group members</h1>
      <h3>Group members: <%=clientListSize%> <%=(clientListSize == 1 ? "client" : "clients")%> selected.</h3>
      <div style="height:<%=clientListDivHeight%>; overflow:auto; border-width: 1; border-style: solid;">
      <table  dojoType="filteringTable"
            id="clientTable"
            cellpadding="2"
            alternateRows="true"
            class="dataTable">
        <thead>
        <tr>
          <th field="add" dataType="String" align="left" valign="top" sort="desc">Addressee</th>
          <th field="client" dataType="Number" align="left" valign="top">Client</th>
          <th field="name" dataType="String" align="left" valign="top">Name</th>
          <th field="product" dataType="String" align="left" valign="top">Product</th>
          <th field="expiry" dataType="String" align="left" valign="top">Expiry</th>
          <th field="status" dataType="String" align="left" valign="top">Status</th>
          <th>&nbsp;</th>
        </tr>
        </thead>
<%
if (selectedClientList != null)
{
%>
<tbody>
<%
   String tooltip = null;
   String paSelectedClientKey = transGroup.getSelectedClientKeyForPA();
   ArrayList selectedClients = new ArrayList(selectedClientList);
   Collections.sort(selectedClients);
   Iterator selectedClientListIT = selectedClients.iterator();
   int selectedCount = 0;
   int counter = 0;
   boolean isPA = false;
   while (selectedClientListIT.hasNext())
   {
      counter++;
      isPA = false;
      selectedClient = (SelectedClient) selectedClientListIT.next();
      selectedCount++;
      invalidReason = selectedClient.getInvalidReason();
      clientVO = selectedClient.getClient();
      membershipVO = selectedClient.getMembership();

      // Check the radio button if this is the prime addressee
      if (selectedClient.getKey().equals(paSelectedClientKey))
      {
        isPA = true;
      }

//      if (selectedClient.isRemovedFromGroup())
//      {
//         disableRadioButton = "disabled";
//      }
//      else
//      {
//         disableRadioButton = "";
//      }

      // Disable the remove button if this is the context client or
      // the client / membership is invalid for the transaction
      if ( (  contextClient != null
           && contextClient.getClientNumber().equals(clientVO.getClientNumber())
           )
         || selectedClient.getKey().equals(paSelectedClientKey)
         )
      {
         disabled = true;
         tooltip = "The context client cannot be removed.";
      }
      else if (invalidReason != null)
      {
         disabled = true;
         tooltip = invalidReason;
      }
      else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(transGroupTypeCode) ||
               MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transGroupTypeCode) ||
               MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(transGroupTypeCode) ||
               MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transGroupTypeCode)
               )
      {
         disabled = true;
         tooltip = "The client must be removed from the membership group by a '"+MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP+"' transaction only.";
      }
      else
      {
         disabled = false;
         tooltip = "Remove the client from the membership group.";
      }

      // Get the address string.
      addrVO = clientVO.getPostalAddress();
      if (addrVO != null)
         postalAddress = addrVO.getSingleLineAddress();
      else
         postalAddress = null;

      if (membershipVO != null)
      {
         productCode = membershipVO.getCurrentProductCode();
         membershipExpiryDate = DateUtil.formatShortDate(membershipVO.getExpiryDate());
         membershipStatus = MembershipUIHelper.formatMembershipStatusHTML(membershipVO.getStatus());
      }
      else
      {
         productCode = "";
         membershipExpiryDate = "";
         membershipStatus = "";
      }
%>
       <tr value="<%=counter%>" valign="top">
         <td class="listItemPadded" align="center">
<%
if (isPA)
{
%>
  <input name="paSelectedClientKey" type="hidden" value="<%=selectedClient.getKey()%>">
  <img src="<%=CommonConstants.DIR_IMAGES%>/check.gif">
<%
}
else
{
%>
  <img src="<%=CommonConstants.DIR_IMAGES%>/clear.gif">
<%
}
%>
         </td>
         <td class="listItemPadded"><%=clientVO.getClientNumber()%></td>
<%
         // Show the address as a tooltip when the mouse hovers over the client name
      if (postalAddress != null)
      {
%>
         <td class="listItemPadded"><!--<%=clientVO.getSortableDisplayName()%>--><a href="#" class="actionItemBlack" title="<%=postalAddress%>"><%=clientVO.getSortableDisplayName()%></a></td>
<%
      }
      else
      {
%>
         <td class="listItemPadded"><%=clientVO.getSortableDisplayName()%></td>
<%
      }
%>
         <td class="listItemPadded"><%=productCode==null?"":productCode%></td>
         <td class="listItemPadded"><a class="label" title="Joined group on <%=DateUtil.formatDate(selectedClient.getMembershipGroupJoinDate(),"dd/MM/yyyy HH:mm:ss:SS")%>"><%=membershipExpiryDate%></a></td>
         <td class="listItemPadded"><%=membershipStatus%></td>
         <td>
<%
      if (disabled)
      {
%>
               <img name="RemoveButton<%=selectedCount%>" src="<%=CommonConstants.DIR_IMAGES%>/RemoveButton_disabled.gif" border="0" alt="<%=tooltip%>">
<%
      }
      else if ( selectedClient.isRemovedFromGroup())
      {
%>
               <a onclick="document.forms['groupMembersForm'].event.value='selectGroupMembers_btnUndo'; document.forms['groupMembersForm'].selectedClientKey.value='<%=selectedClient.getKey()%>';document.forms['groupMembersForm'].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Undo the removal of the client from the membership group.');MM_swapImage('UndoButton<%=selectedCount%>','','<%=CommonConstants.DIR_IMAGES%>/UndoButton_over.gif',1);return document.MM_returnValue" >
                  <img name="UndoButton<%=selectedCount%>" src="<%=CommonConstants.DIR_IMAGES%>/UndoButton.gif" border="0" alt="Undo the removal of the client from the membership group.">
               </a>
<%
      }
      else
      {
%>
               <a onclick="document.forms['groupMembersForm'].event.value='selectGroupMembers_btnRemove'; document.forms['groupMembersForm'].selectedClientKey.value='<%=selectedClient.getKey()%>';document.forms['groupMembersForm'].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Remove the client from the selected client list.');MM_swapImage('RemoveButton<%=selectedCount%>','','<%=CommonConstants.DIR_IMAGES%>/RemoveButton_over.gif',1);return document.MM_returnValue" >
                  <img name="RemoveButton<%=selectedCount%>" src="<%=CommonConstants.DIR_IMAGES%>/RemoveButton.gif" border="0" alt="<%=tooltip%>">
               </a>
<%
      }
%>
         </td>
       </tr>
<%
   }
%>
</tbody>
<%
}
%>
      </table>
      </div>
<%

// Set up the client search fields
int lastSearchType = ClientUIConstants.SEARCHBY_DEFAULT;
Integer searchType = (Integer) session.getAttribute("searchType");
if (searchType != null)
{
  lastSearchType = searchType.intValue();
}

String searchHeading = "";
if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(transGroupTypeCode))
{
   searchHeading = " (clients must have a membership)";
}

%>

  <h3>Search by:<%=searchHeading%></h3>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <!-- search boxes  -->
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <select name="clientSearchType">
                <%--mapped by ClientConstants within ClientUIC--%>
                <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_CLIENT_NUMBER,lastSearchType,"Client Number")%>
                <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_CLIENT_SURNAME,lastSearchType,"Client Surname")%>
                <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_HOME_PHONE,lastSearchType,"Home Phone")%>
                <%=HTMLUtil.option(ClientUIConstants.SEARCHBY_SMART_SEARCH,lastSearchType,"Smart Search")%>
              </select>
            </td>
            <td>
               <a onclick="document.forms['groupMembersForm'].event.value='selectGroupMembers_btnSearch'; document.forms['groupMembersForm'].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Search for the client');MM_swapImage('SearchButton','','<%=CommonConstants.DIR_IMAGES%>/SearchButton_over.gif',1);return document.MM_returnValue" >
                  <img name="SearchButton" src="<%=CommonConstants.DIR_IMAGES%>/SearchButton.gif" border="0" alt="Search for the client">
               </a>
            </td>
          </tr>
          <tr id="clientSearchSimpleRow" style="display:">
            <td>
              <input type="text" name="clientSearchValue" value="<%=(searchValue != null ? searchValue : "")%>" size="20">
            </td>
            <td>
              <span class="helpTextSmall">e.g. surname, initial</span>
            </td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td>
<%
// Display a search message if required.
String message = (String) request.getAttribute("message");
if (message != null)
{
%>
         <span class="infoTextSmall"><%=message%></span><br/>
<%
}
//display an error message if one exists
String errorMessage = (String)request.getAttribute("errorMessage");
if (errorMessage != null)
{
%>
         <span class="errorTextSmall"><%=errorMessage%></span><br/>
<%
}
%>
              </td>
           </tr>
        </table>
      </td>
    </tr>
  </table>

<%--        Display the client search results         --%>

<%
// Get the list of clients that resulted from the search
Vector clientSearchList = transGroup.getClientSearchList();

// Indicate how many records were returned.
String searchMessage = "";
if (clientSearchList != null && !clientSearchList.isEmpty()) {
   searchMessage = clientSearchList.size() + " matching " + (clientSearchList.size() > 1 ? "clients" : "client") + " found.";
}
%>
    <h3>Client search results: <%=searchMessage%></h3>
<%

if (clientSearchList != null && !clientSearchList.isEmpty())
{
   int searchListDivHeight = 25 + (clientSearchList.size() * 38);
   searchListDivHeight = (searchListDivHeight > 180 ? 180 : searchListDivHeight);

%>
    <div style="height:<%=searchListDivHeight%>; overflow:auto; border-width: 1; border-style: solid;">
      <table  dojoType="filteringTable"
            id="clientTable"
            cellpadding="2"
            alternateRows="true"
            class="dataTable">
        <thead>
       <tr>
          <th field="client" dataType="Number" align="left" valign="top">Client</th>
          <th field="name" dataType="String" align="left" valign="top" sort="asc">Name</th>
          <th field="mem" dataType="String" align="left" valign="top">Product</th>
          <th field="exp" dataType="String" align="left" valign="top">Expiry</th>
          <th field="status" dataType="String" align="left" valign="top">Status</th>
          <th field="group" dataType="String" align="left" valign="top">In group?</th>
          <th>&nbsp;</th>
       </tr>
       </thead>
       <tbody>
<%
      Iterator clientSearchListIT = clientSearchList.iterator();
      int searchCount = 0;
      int counter = 0;
      while (clientSearchListIT.hasNext())
      {
        counter++;
         selectedClient = (SelectedClient) clientSearchListIT.next();
         searchCount++;
         clientVO = selectedClient.getClient();
         addrVO = clientVO.getPostalAddress();
         if (addrVO != null) {
            postalAddress = addrVO.getSingleLineAddress();
         }
         else {
            postalAddress = null;
         }

         try
         {
         membershipVO = selectedClient.getMembership();
         if (membershipVO != null) {
            invalidReason = membershipVO.getInvalidForTransactionReason(transGroupTypeCode);
            productCode = membershipVO.getCurrentProductCode();
            membershipExpiryDate = DateUtil.formatShortDate(membershipVO.getExpiryDate());
            membershipStatus = membershipVO.getStatus();
            existingGroupMember = membershipVO.isGroupMember();
         }
         else {
            invalidReason = MembershipHelper.validateClientForTransaction(clientVO,transGroupTypeCode,null);
            productCode = "";
            membershipExpiryDate = "";
            membershipStatus = "";
            existingGroupMember = false;
         }
         }
         catch (Exception e)
         {
           disabled = true;
           //button message is the exception
           LogUtil.warn(this.getClass(),e);
           addButtonTitle = e.getMessage();
         }

         try {
            if (invalidReason != null) {
               disabled = true;
               addButtonTitle = invalidReason;
            }
            else if (transGroup.isClientSelected(selectedClient))
            {
               disabled = true;
               addButtonTitle = "The client is already selected.";
            }
            else if (membershipVO != null
            /*&&       MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(transGroupTypeCode)*/
            &&       existingGroupMember /*
            &&       (MembershipVO.STATUS_ACTIVE.equals(membershipStatus)
                   || MembershipVO.STATUS_FUTURE.equals(membershipStatus)
                   || MembershipVO.STATUS_INRENEWAL.equals(membershipStatus))*/)
            {
               // The membership already belongs to an active membership group
               disabled = true;
               addButtonTitle = "The membership already belongs to a membership group.";
            }
            else if (membershipVO != null
            &&       membershipVO.getAmountOutstanding(true,false) > 0.0)
            {
               // There is an outstanding amount on the membership in the payment system
               disabled = true;
               addButtonTitle = "There is an outstanding amount on the clients membership.";
            }
            else
            {
               disabled = false;
               addButtonTitle = "Add the client";
            }
         }
         catch (Exception pe)
         {
             disabled = true;
             addButtonTitle = "The client has invalid payment data. "+pe.getMessage();
         }
%>
       <tr value="<%=counter%>" valign="top">
          <td class="listItemPadded"><%=clientVO.getClientNumber()%></td>
<%
         // Show the address as a tooltip when the mouse hovers over the client name
         if (postalAddress != null) {
%>
          <td class="listItemPadded"><!--<%=clientVO.getSortableDisplayName()%>--><a href="#" class="actionItemBlack" title="<%=postalAddress%>"><%=clientVO.getSortableDisplayName()%></a></td>
<%
         } else {
%>
          <td class="listItemPadded"><%=clientVO.getSortableDisplayName()%></td>
<%
         }
%>
          <td class="listItemPadded"><%=productCode%></td>
          <td class="listItemPadded"><%=membershipExpiryDate%></td>
          <td class="listItemPadded"><%=MembershipUIHelper.formatMembershipStatusHTML(membershipStatus)%></td>
          <td class="listItemPadded" align="center"><%=(existingGroupMember ? "Y" : "N")%></td>
          <td>
<%     if (!disabled) {  %>
               <a onclick="document.forms['groupMembersForm'].event.value='selectGroupMembers_btnAdd'; document.forms['groupMembersForm'].selectedClientKey.value='<%=selectedClient.getKey()%>'; document.forms['groupMembersForm'].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Add the client to the selected client list');MM_swapImage('AddButton<%=searchCount%>','','<%=CommonConstants.DIR_IMAGES%>/AddButton_over.gif',1);return document.MM_returnValue" >
                  <img name="AddButton<%=searchCount%>" src="<%=CommonConstants.DIR_IMAGES%>/AddButton.gif" border="0" alt="<%=addButtonTitle%>">
               </a>
<%     } else { %>
               <img name="AddButton<%=searchCount%>" src="<%=CommonConstants.DIR_IMAGES%>/AddButton_disabled.gif" border="0" alt="<%=addButtonTitle%>">
<%     } %>
          </td>
          <td>&nbsp;</td>
        </tr>
<%
      }
%>
      </tbody>
    </table>
    </div>
<%
}
%>
    </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
         <tr>
            <td>
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
            </td>
            <td>
               <a onclick="document.forms['groupMembersForm'].event.value='selectGroupMembers_btnNext'; document.forms['groupMembersForm'].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
                  <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Show the next page">
               </a>
            </td>
         </tr>
      </table>
    </td>
  </tr>
  </table>
</form>
  
<script type="text/javascript">
  document.forms['groupMembersForm'].clientSearchValue.focus();
</script>
</body>
</html>
