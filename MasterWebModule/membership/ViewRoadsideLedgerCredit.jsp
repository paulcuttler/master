<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.ract.payment.ui.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="en-AU" />

<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>

		<script type="text/javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
		
	    <meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0"> 
	
		<style type="text/css">
			.header { background: #003366 url("/menu/images/Blank.gif") repeat-x left bottom; }
			.header img { display: block; }
			.holder { margin: 11px; }			
			.listHeadingPadded { border-bottom: 1px solid #fff; }		
			
			.varOnly .noVar { display: none; }
			.creditOnly .noCredit { display: none; }
			#creditTable .showAll { display: none; }
			#creditTable.varOnly .showAll { display: block; }
			#creditTable.creditOnly .showAll { display: block; }  
			
		</style>
		<title>Roadside Ledger Report: Credit Breakdown</title>
	</head>
	<body>
		<div class="header">
		  <img src="/menu/images/NamePanel.gif" alt="RACT" width="180" height="47" />
		</div> 
		<div class="holder">
			<div id="creditTable">
				<h1>Roadside Ledger Report - Credit Breakdown</h1>
				<p class="noVar"><a href="#" onclick="document.getElementById('creditTable').className = 'varOnly'; return false;">Show Credit with Variance only</a></p>
				<p class="noCredit"><a href="#" onclick="document.getElementById('creditTable').className = 'creditOnly'; return false;">Show Credit only</a></p>
				<p class="showAll"><a href="#" onclick="document.getElementById('creditTable').className = ''; return false;">Show All</a></p>
				<s:actionmessage />
				<s:actionerror />
				<s:fielderror />
				
				<s:set name="postingTotal" value="0" />
				<s:set name="creditTotal" value="0" />
				<s:set name="voucherTotal" value="0" />
				<s:set name="varianceTotal" value="0" />
				
				<table width="600" border="0" cellspacing="0" cellpadding="0">
					<tr class="headerRow">
						<td class="listHeadingPadded">Client</td>
						<td class="listHeadingPadded">Status</td>
						<td class="listHeadingPadded" style="text-align: right">Ledgers</td>
						<td class="listHeadingPadded" style="text-align: right">Vouchers</td>
						<td class="listHeadingPadded" style="text-align: right">Credit</td>
						<td class="listHeadingPadded" style="text-align: right">Variance</td>
						<td class="listHeadingPadded" style="text-align: right">Last Transaction</td>
					</tr>			
					<s:if test="members != null and !members.isEmpty()">
						<s:iterator var="result" value="members" status="istatus">
							<tr class="${istatus.even ? 'even' : 'odd'}Row ${result['posting'] + result['voucher'] + result['credit'] ne 0 and result['credit'] gt 0 ? ' hasVar' : ' noVar'}${result['credit'] gt 0 ? ' hasCredit' : ' noCredit'}">
								<td><a href="MembershipUIC?event=viewClientSummary_viewMembership&membershipID=${result['membershipId']}" onclick="return newWindow(this.href);"><s:property value="#result['clientNumber']" /></a></td>
								<td><s:property value="#result['status']" /></td>
								<td style="text-align: right">
									<a href="RoadsideLedgerReportPostings.action?clientNumber=${result['clientNumber']}" onclick="return newWindow(this.href);" title="View 2310 Postings">
										<fmt:formatNumber value="${!empty result['posting'] ? result['posting'] : 0}" type="currency" />
									</a>
								</td>
								<td style="text-align: right"><fmt:formatNumber value="${!empty result['voucher'] ? result['voucher'] : 0}" type="currency" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${!empty result['credit'] ? result['credit'] : 0}" type="currency" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${result['posting'] + result['voucher'] + result['credit']}" type="currency" /></td>
								<td style="text-align: right"><fmt:formatDate value="${result['txnDate']}" pattern="dd/MM/yyyy" /></td>
							</tr>
							
							<s:set name="postingTotal" value="%{#postingTotal + (#result['posting'] != null ? #result['posting'] : 0)}" />
							<s:set name="creditTotal" value="%{#creditTotal + (#result['credit'] != null ? #result['credit'] : 0)}" />
							<s:set name="voucherTotal" value="%{#voucherTotal + (#result['voucher'] != null ? #result['voucher'] : 0)}" />							
							<s:set name="varianceTotal" value="%{#postingTotal + #creditTotal + #voucherTotal}" />
						</s:iterator>
					</s:if>
					<tr>
					  <td colspan="8">&nbsp;</td>
					</tr>
					<tr class="headerRow">
						<td class="listHeadingPadded">&nbsp;</td>
						<td class="listHeadingPadded">&nbsp;</td>
						<td class="listHeadingPadded" style="text-align: right">Ledgers</td>
						<td class="listHeadingPadded" style="text-align: right">Vouchers</td>
						<td class="listHeadingPadded" style="text-align: right">Credit</td>
						<td class="listHeadingPadded" style="text-align: right">Variance</td>
						<td class="listHeadingPadded" style="text-align: right">&nbsp;</td>
					</tr>	
					<tr>
						<td class="listHeadingPadded">&nbsp;</td>
						<td class="listHeadingPadded">&nbsp;</td>
						<td style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000;"><fmt:formatNumber value="${postingTotal}" type="currency" /></td>
						<td style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000;"><fmt:formatNumber value="${voucherTotal}" type="currency" /></td>
						<td style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000;"><fmt:formatNumber value="${creditTotal}" type="currency" /></td>
						<td style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000;"><fmt:formatNumber value="${varianceTotal}" type="currency" /></td>
						<td class="listHeadingPadded" style="text-align: right">&nbsp;</td>
					</tr>			
				</table>
			</div>	
		</div>			
	</body>
</html>