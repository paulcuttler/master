<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.reporting.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.reporting.*"%>

<html>
<head>
<title>
BranchSalesReport
</title>
<%=CommonConstants.getRACTStylesheet()%>

<script language="javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js"></script>
<script language="javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Validation.js"></script>

</head>

<body onload="document.all.periodStartDate.focus();">
<h2> Membership Branch Sales Report</h2>
<form name="salesBranchParameters" method="post" target="_blank" action="<%= request.getContextPath() + "/MembershipReportsUIC"%>">
  <input type="hidden" name="event" value="BranchSales_DoReport">
  <table cellpadding="2" border="0" width="475">
    <tr>
      <td>From:</td>
      <td><input type="text" name="periodStartDate"
		    onblur = "checkdate(this)">
      </td>
      <td>To:</td>
      <td><input type="text" name="periodEndDate"
		    onblur = "checkdate(this)">
      </td>
    </tr>
    <!-- standard sales branch list -->
    <%@include file="/common/reporting/SelectSalesBranch.jsp"%>
    <tr>
      <td>Delivery Format:</td>
      <td colspan="3">
        <input type="hidden" name="<%=BranchSalesReportRequest.PARAMETER_DELIVERY_METHOD%>" value="<%=BranchSalesReportRequest.REPORT_DELIVERY_METHOD_SCREEN%>"/>
        <select name="<%=BranchSalesReportRequest.PARAMETER_DELIVERY_FORMAT%>"> 
          <option value="<%=BranchSalesReportRequest.REPORT_DELIVERY_FORMAT_PDF%>" selected>Screen/PDF</option>
        </select>
      </td>
    </tr>
    <tr>
      <td>
      <div>Group by transaction sales branch?</div>
      </td> 
      <td colspan="3"><input type="checkbox" value="Y" name="transactionSalesBranch"%></td>
    </tr>
        <tr>
      <td> 
      <div>Exclude web transactions?</div>
      </td>
      <td colspan="3"><input type="checkbox" value="Y" name="excludeWeb"%></td>
    </tr>    
    <tr>
      <td colspan="4"><input type="button" name="Submit" value="OK"
            onclick="submitForm()"><input type="submit" name="cancel" value="Cancel"
            onclick="event.value='BranchSales_Cancel'"></td>
    </tr>
  </table>

<script language="javascript">
function submitForm()
{
   document.all.salesBranchList.value = getSalesBranchList();
   document.salesBranchParameters.submit();
}
</script>

</form>
</body>
</html>
