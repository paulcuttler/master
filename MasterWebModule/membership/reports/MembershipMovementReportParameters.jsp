<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.reporting.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>

<%
//ArrayList salesBranchList = CommonEJBHelper.getCommonMgr().getSalesBranchList();
SortedSet branchRegionList = CommonEJBHelper.getCommonMgr().getBranchRegionsAndDescriptions();
%>

<html>
<head>
<title>Membership Movement Statistic Report</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Validation.js">
</script>
<script language="JavaScript">

   function onLoadEventHandler()
   {
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=showClientList';
   }


   function setBranchSelections()
   {
      var outString = "";
      if(document.all.selectedBranches[0].selected){
         //all branches.  Fill list
         // exclude the ALL option
        for (i = 1; i < document.all.selectedBranches.length; i++){
           if (outString != "") {
              outString += ",";
           }
           outString += document.all.selectedBranches[i].value;
        }
        outString +=",0";
      }
      else{
         // ALL not selected.  List only selected branches
         // exclude the ALL option
         for (i = 1; i < document.all.selectedBranches.length; i++){
            if (document.all.selectedBranches[i].selected){
               if (outString != "") {
                  outString += ",";
               }
               outString += document.all.selectedBranches[i].value;
            }
         }
      }
      document.all.<%=MembershipMovementReportRequest.PARAMETER_SALES_BRANCH_LIST%>.value = outString;
     // document.forms[0].submit();
   }
function submitParameters()
{
   //validate data
   if(!checkDateRange(document.all.fromDate,document.all.toDate))
   {
      document.all.fromDate.select();
      document.all.fromDate.focus();
   }
   else
   {
      setBranchSelections();
      document.forms[0].submit();
   }
}
</script>
</head>
<body onload="onLoadEventHandler(); return true;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form name="parameterForm" method="post" onsubmit ="submitSelections" action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">
<input type="hidden" name="event" value="MembershipMovementReportParameters_okButton">
<input type="hidden" name="<%=MembershipMovementReportRequest.PARAMETER_SALES_BRANCH_LIST%>" value="">
<tr>
<td valign="top">
   <h1>Membership Movement Statistics Report Parameters</h1>
        <table cellpadding="1" cellspacing="0" border="0">
          <tr>
      <td class="label">Collect data for the period:</td>
      <td>
        <input type="text" name="fromDate" size="11"  value = ""
                onkeyup="JavaScript:checkDateKeyStroke(this)"
                onblur="JavaScript:checkDateValue(this)">
				to:
         <input type="text" name="toDate" size="11" value = ""
                onkeyup="JavaScript:checkDateKeyStroke(this)"
                onblur="JavaScript:checkDateValue(this)">
      </td>
    </tr>
    <tr>
            <td class="label" valign="top">For branch regions:</td>
      <td  >
        <select name="selectedBranches" size="10" multiple >
          <option value = "-1" >-- All --</option>
<%
AdminCodeDescription vo;
Iterator it = branchRegionList.iterator();
while(it.hasNext())
{
   vo =  (AdminCodeDescription)it.next();
   out.println(HTMLUtil.option(vo.getCode(),vo.getDescription()+"",""));
}
%>
        </select>
      </td>
      <td class="helpTextSmall"> Click to select required branch.<br/>
        Ctrl-Click to select multiple branches.<br/>
        Shift-click to select range. </td>
    </tr>
    <%@ include file="/common/reporting/SelectReportDeliveryMethodInclude.jsp"%>
  </table>
</td>
</tr>
<tr>
<td valign="bottom">
   <table border="0" cellpadding="5" cellspacing="0">
   <tr>
      <td><input type="button" name="okButton" value="OK"
            onclick="submitParameters()">
      </td>
   </tr>
   </table>
</td>
</tr>
</form>
</table>
<script language="JavaScript">
   document.all.fromDate.focus();
</script>
</body>
</html>
