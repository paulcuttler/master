<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.reporting.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="java.util.*"%>
<%
String OkEvent = "MembershipRenewalsDueExtractParameters_okButton";
// Fetch a list of months that can be queried on.
// The first date is the date that the system started recording data
GregorianCalendar firstGC = new GregorianCalendar(2002,10,1);
GregorianCalendar aGC = new GregorianCalendar();
Date now = aGC.getTime();
// Work backwards from now to the first date.
ArrayList dateList = new ArrayList();
while (aGC.after(firstGC))
{
  dateList.add(aGC.getTime());
  aGC.add(GregorianCalendar.MONTH,-1);
}

%>

<html>
<head>
<title>MembershipRenewalsDueReportParameters</title>

<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=CommonConstants.getRACTStylesheet()%>

<script language="JavaScript">
   function onLoadEventHandler()
   {
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=showClientList';
   }

</script>

</head>
<body onload="onLoadEventHandler(); return true;">

<form name="mainForm"
      method="post"
      action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>"
>

  <input type="hidden" name="event" value="">

 <div id="mainpage" class="pg">
 <h1>Membership Renewals Due</h1>
   <div id="contentpage" class="cnt">
      <table>
      <tr>
        <td valign="top">
           <table border="0" cellpadding="3" cellspacing="0">
             <tr>
             </tr>
           </table>
        </td>
      </tr>
      </table>
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonOk(OkEvent,"Run Membership Renewals Due" )%>
   </div>
</div>
</form>
</body>
</html>