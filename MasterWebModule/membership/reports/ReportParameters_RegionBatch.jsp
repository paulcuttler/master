<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%--
Requests parameters for the MembershipRenewalStatisticsReport.jsp

request attributes:
    ArrayList branchList List of branch Regions on which to report
	String ReportName
	String Heading
returned parameters:
   fromDate
   toDate

   runDate
   runHours
   runMins

--%>
<script language="javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js">
</script>
<%
ArrayList branchList  = (ArrayList)request.getAttribute("branchList");
String selection = null;
BranchVO vo = null;
String heading = (String) request.getAttribute("heading");
String reportName = (String)request.getAttribute("reportName");
%>
<html>
<head>
<title>
Renewals generic data collection for branch regions with batch data
</title>
<%=CommonConstants.getRACTStylesheet()%>
<%--<link rel="stylesheet" href="../css/ract.css" type="text/css">--%>
</head>
<body>
<h1>
<%=heading%>
</h1>
<form name = "MNForm" method="post"  action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">
  <table cellpadding="2" border="0" width="445">
    <tr>
      <td width="114">
        <div align="right">Collect data for the period:</div>
      </td>
      <td width="65">
        <input type="text" name="fromDate" size="11"  value = ""
		    onkeyup = "javascript:checkDate(this)">
      </td>
      <td width="55">to</td>
      <td width="185">
        <input type="text" name="toDate" size="11" value = ""
		    onKeyUp = "javascript:checkDate(this)">
      </td>
    </tr>
    <tr>
      <td width="114" valign="top">
        <div align="right">For branch regions:</div>
      </td>
      <td  colspan = 2>
        <select name="selectedBranches" size="10" multiple >
          <option value = "-1" selected>All</option>
          <%for(int x=0;x<branchList.size();x++){
                vo = (BranchVO)branchList.get(x);
             selection = "<option value=\"" + vo.getBranchNumber() + "\">" + vo.getDescription() + "</option>\n";
             %>
          <%=selection%>
          <%}%>
        </select>
      </td>
      <td class="helpTextSmall" width="185" valign="top" > Click to select required
        branch.<br/>
        Ctrl-Click to select multiple branches.<br/>
        Shift-click to select range. </td>


    </tr>
    <tr>
      <td width="114">
        <div align="right">Schedule to run on: </div>
      </td>
      <td width="65">
        <input type = "text" name = "runDate" size = "11" value = ""
		   onkeyup = "javascript:checkDate(this)">
      <td width="55">&nbsp at: &nbsp
      <td width="185">
        <input type = "text" name = "runHours" size = "2" value = "17"
             onKeyUp = "javascript:checkInteger(this)"
             onBlur = "javascript:checkHours(this)">
        :
        <input type = "text" name = "runMins" size = "2" value = "30"
             onKeyUp = "javascript:checkInteger(this)"
             onBlur = "javascript:checkMins(this)">
    </table>
 <input type="hidden" name = "numItems" value = "<%=branchList.size()%>">
 <input type="hidden" name = "branchList" value="">
 <input type="hidden" name="event" value="">
&nbsp&nbsp<input type="button" name="OK" value="OK"
            onclick="javascript:submitSelections()">
&nbsp&nbsp<input type="submit" name="cancel" value="Cancel"
            onclick="event.value='<%=reportName%>_Cancel'">
<script language="javascript">
   ddate = new Date;
   dday = ddate.getDate();
   dmonth = ddate.getMonth() + 1;
   dyear = ddate.getYear();
   document.all.runDate.value= dday + "/" + dmonth + "/" + dyear;
   document.all.fromDate.focus();

function checkHours(thing){
    val = thing.value * 1;
    if(val>23 || val<0){
       document.all.runHours.focus();
       alert("Hours must be from 0 to 23");
       return false;
    }
    else return true;
}

function checkMins(thing){
   val = thing.value * 1;
   if(val > 59 || val < 0){
      document.all.runMins.focus();
      alert("Minutes must be between 0 and 59");
      return false;
   }
   else return true;
}
function validate(){
   hours = document.all.runHours.value * 1;
   mins = document.all.runMins.value * 1;
   if(hours>23 || hours < 0 || mins<0 || mins>59){
      alert("Enter a valid date and time");
      return false;
   }
   else return true;
}
function submitSelections(){
   var OK = validate();
   if(!OK) return;
   outString = "";
   size = document.all.numItems.value * 1 +1;
   if(document.all.selectedBranches[0].selected){
      //all branches.  Fill list
      // exclude the ALL option
     for(x=1;x<size;x++){
        if(outString !="")outString += ",";
        outString += document.all.selectedBranches[x].value;
     }
   }
   else{
      // ALL not selected.  List only selected branches
      // exclude the ALL option
      for(x = 1; x<size;x++){
         if(document.all.selectedBranches[x].selected){
            if(outString != "")outString += ",";
	    outString += document.all.selectedBranches[x].value;
         }
      }
   }
   document.all.branchList.value = outString;
   document.all.event.value='<%=reportName%>_DoReport';
   //alert('outString = '+outString);
   document.MNForm.submit();
}
</script>

</form>
</body>
</html>
