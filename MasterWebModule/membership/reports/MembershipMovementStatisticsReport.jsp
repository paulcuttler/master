<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%--
Requests parameters for the MembershipMovementStatisticsReport.jsp

request attributes:
    ArrayList branchList (List of branch regions VO objects)
returned parameters:
   fromDate
   toDate
   branches (list of integers)
   runDate
   runHours
   runMins
   selectType (ie Printer,File,Email)
   destination (filename of email address)

--%>
<script language="javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js">
</script>

<html>
<head>
<title>
MembershipMovementStatisticReport
</title>
<%=CommonConstants.getRACTStylesheet()%>
<%--<link rel="stylesheet" href="../css/ract.css" type="text/css">--%>
</head>
<body>
<%
//get the branch list from the request
ArrayList branchList = (ArrayList)request.getAttribute("branchList");
String selection = null;
BranchVO vo = null;

%>
<h1> Create Membership Movement Statistics Report </h1>
<form name = "MNForm" method="post" onsubmit ="submitSelections" action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">
  <table cellpadding="2" border="0" width="475">
    <tr>
      <td width="139">
        <div align="right">Collect data for the period:</div>
      </td>
      <td width="74">
        <input type="text" name="fromDate" size="11"  value = ""
		    onkeyup = "javascript:checkDate(this)">
      </td>
      <td width="15">
        <div align="right">to:</div>
      </td>
      <td width="221">
        <input type="text" name="toDate" size="11" value = ""
		    onKeyUp = "javascript:checkDate(this)">
      </td>
    </tr>
    <tr>
      <td width="139">
        <div align="right">For branch regions:</div>
      </td>
      <td  colspan = 2>
        <select name="selectedBranches" size="10" multiple >
          <option value = "-1" >All</option>
          <%for(int x=0;x<branchList.size();x++){
                vo = (BranchVO)branchList.get(x);
             selection = "<option value=\"" + vo.getBranchNumber() + "\">" + vo.getDescription() + "</option>\n";
             %>
          <%=selection%>
          <%}%>
        </select>
      </td>
      <td class="helpTextSmall" width="221" > Click to select required branch.<br/>
        Ctrl-Click to select multiple branches.<br/>
        Shift-click to select range. </td>
    </tr>
    <tr>
      <td width="139">
        <div align="right">Send to:</div>
      </td>
      <td  colspan = 3>
        <select name="selectType" size="3" onchange = "javascript:showAddress(this)">
<%--          <option value="Printer">Printer</option>--%>
          <option value="Screen" selected>Screen</option>
          <option value="Extract">Extract</option>
        </select>
      </td>
    </tr>
  </table>
 <input type="hidden" name = "numItems" value = "<%=branchList.size()%>">
 <input type="hidden" name="event" value="">
 <input type="hidden" name = "branchList" value="">
&nbsp&nbsp<input type="button" name="Submit" value="OK"
            onclick="javascript:submitSelections()">
&nbsp&nbsp<input type="submit" name="cancel" value="Cancel"
            onclick="event.value='MembershipMovementStatisticsReport_Cancel'">
<script language="javascript">

  document.all.fromDate.focus();

  function showAddress(thing){
    thisVal = thing.value + "";
    if(thisVal=="Extract"){
       document.getElementById("spanText").innerHTML = "File name";
           document.all.destination.style.display="";
    }
    else {
       document.getElementById("spanText").innerHTML = "";
           document.all.destination.style.display = "none";
    }
  }

  function submitSelections(){
    outString = "";
    size = document.all.numItems.value * 1 +1;
    for(x = 0; x<size;x++){
      if(document.all.selectedBranches[x].selected){
         if(outString != "")outString += ",";
             outString += document.all.selectedBranches[x].value;
          }
    }
    document.all.branchList.value = outString;
    document.all.event.value='MembershipMovementStatisticsReport_DoReport';
    document.MNForm.submit();
}
</script>

</form>
</body>
</html>
