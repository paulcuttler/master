<%@ page import="com.ract.common.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%--
Requests parameters for the MembershipMovementStatisticsReport.jsp

request attributes:
    ArrayList branchList (List of branch regions VO objects)
    String heading (main heading to display)
    String reportName  (Name of report to be generated)
    ArrayList optionList (Print, Extract etc)
returned parameters:
   fromDate
   toDate
   branches (list of integers)
   runDate
   runHours
   runMins
   selectType (ie Printer,File,Email)
   destination (filename of email address)

--%>
<script language="javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js">
</script>

<html>
<head>
<title>
MembershipMovementStatisticReport
</title>
<%=CommonConstants.getRACTStylesheet()%>
<%--<link rel="stylesheet" href="../css/ract.css" type="text/css">--%>
</head>
<body>
<%
//get the branch list from the request
ArrayList branchList = (ArrayList)request.getAttribute("branchList");
String selection = null;

BranchVO bvo = null;
SalesBranchVO svo = null;

String reportName = (String)request.getAttribute("reportName");
String heading = (String)request.getAttribute("heading");
ArrayList optionList = (ArrayList)request.getAttribute("optionList");

%>
<h1> <%=heading%> </h1>
<form name = "MNForm" method="post"  action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">
  <table cellpadding="2" border="0" width="475">
    <tr>
      <td width="139">
        <div align="right">Collect data for the period:</div>
      </td>
      <td width="74">
        <input type="text" name="fromDate" size="11"  value = ""
		    onkeyup = "javascript:checkDate(this)">
      </td>
      <td width="15">
        <div align="right">to:</div>
      </td>
      <td width="221">
        <input type="text" name="toDate" size="11" value = ""
		    onKeyUp = "javascript:checkDate(this)">
      </td>
    </tr>
    <tr>
      <td width="139" valign="top">
        <div align="right">For branch:</div>
      </td>
      <td  colspan = 2>
        <select name="selectedBranches" size="10" multiple >
          <option value = "-1" selected>All</option>
          <%for(int x=0;x<branchList.size();x++){
                Object o = branchList.get(x);
                if (o instanceof BranchVO)
                {
                  bvo = (BranchVO)o;
                  selection = "<option value=\"" + bvo.getBranchNumber() + "\">" + bvo.getDescription() + "</option>\n";
                }
                else if (o instanceof SalesBranchVO)
                {
                  svo = (SalesBranchVO)o;
                  selection = "<option value=\"" + svo.getSalesBranchCode() + "\">" + svo.getSalesBranchName() + "</option>\n";
                }
             %>
          <%=selection%>
          <%}%>
        </select>
      </td>
      <td class="helpTextSmall" width="221" valign="top" > Click to select required
        branch.<br/>
        Ctrl-Click to select multiple branches.<br/>
        Shift-click to select range. </td>
    </tr>
<%    if(optionList!=null){%>
    <tr>
      <td width="139">
        <div align="right">Send to:</div>
      </td>
      <td  colspan = 3>
        <select name="optionList" size="3" >
<%       for(int i = 0;i<optionList.size();i++){
%>          <option value="<%=(String)optionList.get(i)%>"
                   <%=(i==0)?" SELECTED":""%>    ><%=(String)optionList.get(i)%></option>
<%       }%>
        </select>
      </td>
    </tr>
<%    }%>
  </table>
 <input type="hidden" name = "numItems" value = "<%=branchList.size()%>">
 <input type="hidden" name="event" value="">
 <input type="hidden" name = "branchList" value="">
&nbsp&nbsp<input type="button" name="OK" value="OK"
            onclick="javascript:submitSelections()">
&nbsp&nbsp<input type="submit" name="cancel" value="Cancel"
            onclick="event.value='<%=reportName%>_Cancel'">
<script language="javascript">
   document.all.fromDate.focus();
function showAddress(thing){
  thisVal = thing.value + "";
  if(thisVal=="Extract"){
     document.getElementById("spanText").innerHTML = "File name";
	 document.all.destination.style.display="";
  }
  else {
     document.getElementById("spanText").innerHTML = "";
	 document.all.destination.style.display = "none";
  }
}
function submitSelections(){
   outString = "";
   size = document.all.numItems.value * 1 +1;
   if(document.all.selectedBranches[0].selected){
      //all branches.  Fill list
      // exclude the ALL option
     for(x=1;x<size;x++){
        if(outString !="")outString += ",";
        outString += document.all.selectedBranches[x].value;
     }
   }
   else{
      // ALL not selected.  List only selected branches
      // exclude the ALL option
      for(x = 1; x<size;x++){
         if(document.all.selectedBranches[x].selected){
            if(outString != "")outString += ",";
	    outString += document.all.selectedBranches[x].value;
         }
      }
   }
   document.all.branchList.value = outString;
   document.all.event.value='<%=reportName%>_DoReport';
   //alert('outString = '+outString);
   document.MNForm.submit();
}
</script>

</form>
</body>
</html>
