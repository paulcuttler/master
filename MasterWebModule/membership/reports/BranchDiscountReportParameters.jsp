<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.reporting.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="java.util.*"%>
<%
//get the branch list from the request


//ArrayList salesBranchList = CommonEJBHelper.getCommonMgr().getSalesBranchList();

// Fetch a list of months that can be queried on.
// The first date is the date that the system started recording data
GregorianCalendar firstGC = new GregorianCalendar(2002,10,1);
GregorianCalendar aGC = new GregorianCalendar();
Date now = aGC.getTime();
// Work backwards from now to the first date.
ArrayList dateList = new ArrayList();
while (aGC.after(firstGC)) {
  dateList.add(aGC.getTime());
  aGC.add(GregorianCalendar.MONTH,-1);
}
%>

<html>
<head>
<title>ReportParameters</title>
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<script language="JavaScript">
   function onLoadEventHandler()
   {
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=showClientList';
   }

   function submitForm(eventName)
   {
      setBranchSelections();

      document.all.event.value = eventName;
      var valid = true;//validateForm();
      if (valid)
      {
        document.all.mainForm.submit();
      }
   }

   function setBranchSelections()
   {
     document.all.<%=BranchSalesReportRequest.PARAMETER_SALES_BRANCH_LIST%>.value = getSalesBranchList();
   }

   function submitForm(eventName)
   {
//      document.all.event.value = eventName;
      setBranchSelections();
      var valid = true;//validateForm();
      if (valid)
      {
        document.all.mainForm.submit();
      }
   }
</script>
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/InputControl.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
</head>
<body onload="onLoadEventHandler(); return true;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form name = "mainForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">
<input type="hidden" name="event" value="BranchDiscountReportParameters_okButton">
<tr>
  <td valign="top">
     <h2>Branch Discounts Report</h2>
     <table border="0" cellpadding="3" cellspacing="0">
       <tr>
         <td class="label">For period ending:</td>
         <td>
            <select name="<%=BranchSalesReportRequest.PARAMETER_PERIOD_END_DATE%>">
<%

Date aDate;
for (int i = 0; i < dateList.size(); i++) {
   aDate = (Date) dateList.get(i);
%>
   <%=HTMLUtil.option(DateUtil.formatDate(aDate,"01/MM/yyyy"),DateUtil.formatDate(aDate,"MMMM yyyy"),String.valueOf(now))%>
<%
}
%>
            </select>
         </td>
       </tr>
       <%@include file="/common/reporting/SelectSalesBranch.jsp"%>
       <%@include file="/common/reporting/SelectReportDeliveryMethodInclude.jsp"%>
     </table>

  </td>
</tr>
<tr>
   <td valign="bottom">
         <%=HTMLUtil.buttonOk("","Submit Report")%>
  </td>
</tr>
</form>
</table>


<script language="JavaScript">
   document.all.<%=BranchSalesReportRequest.PARAMETER_PERIOD_END_DATE%>.focus();
</script>

</body>
</html>
