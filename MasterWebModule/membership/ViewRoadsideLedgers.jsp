<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.ract.payment.ui.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="en-AU" />

<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>
		<script type="text/javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>

	    <meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0"> 
	
		<style type="text/css">
			td.tdLabel { vertical-align: top; padding-top: 4px; }
			.errorMessage { color: red; }

			.suppressed { display: none; }
			
			.listHeadingPadded { border-bottom: 1px solid #fff; }			
		</style>
		<title>Roadside Ledger Report</title>
	</head>
	<body>
		<h1>Roadside Ledger Report</h1>
		<s:actionmessage />
		<s:actionerror />
		<s:fielderror />
		
		<table width="600" border="0" cellspacing="0" cellpadding="0">
			<tr class="headerRow">
				<td class="listHeadingPadded">Period</td>
				<td class="listHeadingPadded" style="text-align: right">Ledgers</td>
				<td class="listHeadingPadded">&nbsp;</td>
				<td class="listHeadingPadded" style="text-align: right">Vouchers</td>
				<td class="listHeadingPadded">&nbsp;</td>
				<td class="listHeadingPadded" style="text-align: right">Credit</td>
				<td class="listHeadingPadded">&nbsp;</td>
				<td class="listHeadingPadded" style="text-align: right">Variance</td>
			</tr>
			<tr>
				<td>Overall</td>
				<td style="text-align: right"><fmt:formatNumber value="${postingTotal}" type="currency" /></td>
				<td style="text-align: right">+</td>
				<td style="text-align: right">
					<a href="RoadsideLedgerReportVoucher.action" onclick="return newWindow(this.href);" title="View Vouchers">
						<fmt:formatNumber value="${voucherTotal}" type="currency" />
					</a>
				</td>
				<td style="text-align: right">+</td>
				<td style="text-align: right">
					<a href="RoadsideLedgerReportCredit.action" onclick="return newWindow(this.href);" title="View Clients">
						<fmt:formatNumber value="${creditTotal}" type="currency" />
					</a>
				</td>
				<td style="text-align: right">=</td>
				<td style="text-align: right"><fmt:formatNumber value="${varianceTotal}" type="currency" /></td>
			</tr>
		
			<s:if test="monthlyBreakdown != null and !monthlyBreakdown.isEmpty()">
				<tr class="headerRow">
					<td colspan="8" class="listHeadingPadded"><h2>Monthly breakdown</h2></td>
				</tr>
				<s:iterator var="month" value="monthlyBreakdown" status="status">
					<s:iterator var="label" value="#month.keySet()">
						<s:set var="resultSet" value="%{#month.get(#label)}" />						
						<tr class="${status.even ? 'even' : 'odd'}Row">
							<td>
								<s:if test="#label eq LABEL_BALANCE_BROUGHT_FORWARD">
									<s:property value="#label" />
								</s:if>
								<s:else>
									<!-- <a href="RoadsideLedgerReportDaily.action?start=${label}" onclick="return newWindow(this.href);" title="View Daily Breakdown"> -->
										<fmt:parseDate var="labelDate" value="${label}" pattern="dd/MM/yyyy"/>
										<fmt:formatDate value="${labelDate}" pattern="MMMM yyyy"/>
									<!-- </a> -->
								</s:else>
							</td>							
							<td style="text-align: right"><fmt:formatNumber value="${resultSet['postings']}" type="currency" /></td>
							<td style="text-align: right">&nbsp;</td>
							<td style="text-align: right"><fmt:formatNumber value="${resultSet['vouchers']}" type="currency" /></td>
							<td style="text-align: right">&nbsp;</td>
							<td style="text-align: right"><fmt:formatNumber value="${resultSet['credits']}" type="currency" /></td>
							<td style="text-align: right">&nbsp;</td>
							<td style="text-align: right"><fmt:formatNumber value="${resultSet['variance']}" type="currency" /></td>
						</tr>
					</s:iterator>
				</s:iterator>
				<tr>
				  <td colspan="8">&nbsp;</td>
				</tr>
				<tr>
					<td>Sub Total</td>
					<td style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000;"><fmt:formatNumber value="${postingSubTotal}" type="currency" /></td>
					<td style="text-align: right">&nbsp;</td>
					<td style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000;"><fmt:formatNumber value="${voucherSubTotal}" type="currency" /></td>
					<td style="text-align: right">&nbsp;</td>
					<td style="text-align: right">&nbsp;</td>
					<td style="text-align: right">&nbsp;</td>
					<td style="text-align: right">&nbsp;</td>
				</tr>
			</s:if>
		
		</table>
				
	</body>
</html>