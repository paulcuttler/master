<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd" />
<xsl:template match="/">
<html>
<head>
   <title>Roadside Quote</title>
   <link rel="stylesheet" href="/css/ract.css" type="text/css" />

<script language="Javascript">
<![CDATA[
	function printMembershipQuote() {
	  var url = "/MembershipReportsUIC?event=printMembershipQuote&quoteNumber=]]><xsl:value-of select="MembershipQuote/quoteNumber"/><![CDATA["
	  window.open(url, '', 'width=250,height=150,toolbar=no,status=no,scrollbars=no,resizable=no')
	}
	   
	function emailMembershipQuote() {
	  var url = 'EmailAdviceForm.action?adviceType=QUOTE&quoteNumber=]]><xsl:value-of select="MembershipQuote/quoteNumber"/><![CDATA[';
	  window.open(url, '', 'width=640,height=480,toolbar=no,status=no,scrollbars=no,resizable=no'); 
	}
]]>

</script>

</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr valign="top">
   <td>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
         <td valign="top"><h1>Roadside Quote</h1></td>
         <td align="right" valign="center">
         	<input type="button" value="Print Quote" onclick="printMembershipQuote()"/>
	        <input type="button" value="Email Quote" onclick="emailMembershipQuote()"/>
         	<input type="button" value="Print Screen" onclick="window.print(); return true;"/>
       	</td>
      </tr>
      </table>
      <xsl:apply-templates select="MembershipQuote"/>
      <xsl:apply-templates select="MembershipQuote/transactionXML/TransactionGroup/transactionList"/>
   </td>
</tr>
<tr valign="bottom">
   <td></td>
</tr>
</table>
</body>
</html>
</xsl:template>


<xsl:template match="MembershipQuote">
      <table border="0" cellspacing="0" cellpadding="0">
      <tr>
         <td valign="top">
            <table border="0" cellspacing="0" cellpadding="3">
            <tr>
               <td class="label">Quote No.:</td>
               <td class="dataValue"><xsl:value-of select="quoteNumber"/></td>
            </tr>
            <tr>
               <td class="label">Quote Date:</td>
               <td class="dataValue"><xsl:value-of select="quoteDate"/></td>
            </tr>
            <tr>
               <td class="label">User:</td>
               <td class="dataValue"><xsl:value-of select="userId"/></td>
            </tr>
            <tr>
               <td class="label">Sales branch:</td>
               <td class="dataValue"><xsl:value-of select="salesBranchCode"/></td>
            </tr>
            <tr>
               <td class="label">Transaction type:</td>
               <td class="dataValue"><xsl:value-of select="transactionGroupTypeCode"/></td>
            </tr>
   <xsl:if test="transactionXML/TransactionGroup/rejoinType/ReferenceData">
            <tr>
               <td class="label" valign="top">Rejoin type:</td>
               <td class="dataValue" colspan="3" valign="top"><xsl:value-of select="transactionXML/TransactionGroup/rejoinType/ReferenceData/description"/></td>
            </tr>
   </xsl:if>
   <xsl:if test="transactionXML/TransactionGroup/transactionReason/ReferenceData">
            <tr>
               <td class="label" valign="top">Reason:</td>
               <td class="dataValue" colspan="3" valign="top"><xsl:value-of select="transactionXML/TransactionGroup/transactionReason/ReferenceData/description"/></td>
            </tr>
   </xsl:if>
            <tr>
               <td class="label">Product:</td>
               <td class="dataValue"><xsl:value-of select="transactionXML/TransactionGroup/selectedProductCode"/></td>
            </tr>
            <tr>
               <td class="label">Expiry date:</td>
               <td class="dataValue"><xsl:value-of select="transactionXML/TransactionGroup/expiryDate"/></td>
            </tr>
            </table>
         </td>
         <td width="20"></td>
         <td valign="top">
            <table border="1" bordercolor="#CC0000" cellpadding="3" cellspacing="0">
            <tr>
               <td>
                  <table border="0" cellspacing="0" cellpadding="3">
                  <tr>
                     <td class="label" valign="top">Total net fee:</td>
                     <td class="dataValue" align="right" valign="top"><xsl:value-of select="transactionXML/TransactionGroup/netTransactionFee"/></td>
                  </tr>
   <xsl:for-each select="transactionXML/TransactionGroup/adjustmentList/MembershipTransactionAdjustment">
                  <tr>
                     <td class="label" valign="bottom"><xsl:value-of select="adjustmentType/AdjustmentType/adjustmentTypeCode"/></td>
                     <td class="dataValue" align="right" valign="bottom">
                        <xsl:if test="adjustmentType/AdjustmentType/debitAdjustment = 'true'">-</xsl:if>
                        <xsl:value-of select="adjustmentAmount"/>
                     </td>
                     <td class="helpTextSmall" valign="bottom">(<xsl:value-of select="adjustmentReason"/>)</td>
                  </tr>
   </xsl:for-each>
                  <tr>
                     <td colspan="3"><hr size="1"/></td>
                  </tr>
                  <tr>
                     <td class="label" valign="top">Amount payable:</td>
                     <td class="dataValue" align="right" valign="top">
                        $<xsl:value-of select="transactionXML/TransactionGroup/amountPayable"/>
                     </td>
                     <td valign="top">
                        (inc. $<xsl:value-of select="transactionXML/TransactionGroup/gstAmount"/> gst)
                     </td>
                  </tr>
                  </table>
               </td>
            </tr>
            </table>
         </td>
      </tr>
      </table>
</xsl:template>


<xsl:template match="MembershipQuote/transactionXML/TransactionGroup/transactionList">
        <h3>Membership transaction details - <xsl:value-of select="count(MembershipTransaction)"/> member</h3>
        <table border="0" cellspacing="0" cellpadding="3">
   <xsl:for-each select="MembershipTransaction">
        <tr>
          <td style="white-space: nowrap;" colspan="8">
             <span class="dataValue"><xsl:value-of select="newMembership/Membership/client/Client/clientNumber"/></span>&#xA0;&#xA0;
             <span class="dataValue"><xsl:value-of select="newMembership/Membership/client/Client/displayName"/></span>&#xA0;&#xA0;
             <xsl:if test="primeAddressee = 'true'"><span class="helpTextSmall">(addressee)&#xA0;&#xA0;</span></xsl:if>
             <span class="dataValue">-&#xA0;&#xA0;<xsl:value-of select="transactionTypeCode"/></span>
          </td>
        </tr>
        <tr>
          <td class="dataValue" colspan="8" style="white-space: nowrap;"><xsl:value-of select="newMembership/Membership/client/Client/streetAddress/Address/singleLineAddress"/></td>
        </tr>
        <tr>
          <td width="10"></td>
          <td class="label" colspan="3">Product effective:</td>
          <td class="dataValue" colspan="4"> <xsl:value-of select="newMembership/Membership/productEffectiveDate"/></td>
         </tr>
         <tr>
          <td width="10"></td>
          <td class="label" colspan="3">Join date:</td>
          <td class="dataValue" colspan="4"><xsl:value-of select="newMembership/Membership/joinDate"/></td>
        </tr>
         <tr>
          <td width="10"></td>
          <td class="label" colspan="3">Commence date:</td>
          <td class="dataValue" colspan="4"><xsl:value-of select="newMembership/Membership/commenceDate"/></td>
        </tr>
   <xsl:if test="newMembership/Membership/affiliatedClubCode">
         <tr>
          <td width="10"></td>
          <td class="label" colspan="3">Affiliated club code:</td>
          <td class="dataValue" colspan="4"><xsl:value-of select="newMembership/Membership/affiliatedClubCode"/></td>
        </tr>
         <tr>
          <td width="10"></td>
          <td class="label" colspan="3">Affiliated club Id:</td>
          <td class="dataValue" colspan="4"><xsl:value-of select="newMembership/Membership/affiliatedClubId"/></td>
        </tr>
         <tr>
          <td width="10"></td>
          <td class="label" colspan="3">Affiliated club expiry date:</td>
          <td class="dataValue" colspan="4"><xsl:value-of select="newMembership/Membership/affiliatedClubExpiryDate"/></td>
        </tr>
   </xsl:if>

         <tr>
          <td width="10"></td>
          <td class="label" colspan="7">Product options:</td>
        </tr>

      <xsl:for-each select="newMembership/Membership/productOptionList/MembershipProductOption">
         <tr>
          <td width="10"></td>
          <td width="10"></td>
          <td class="dataValue" colspan="2"><xsl:value-of select="productBenefitTypeCode"/></td>
          <td class="label">Effective:&#xA0;<span class="dataValue"><xsl:value-of select="effectiveDate"/></span>
          </td>
        </tr>
      </xsl:for-each>
         <tr>
          <td width="10"></td>
          <td class="label" colspan="4">Fees:</td>
          <td><img src="images/clear.gif" width="70" height="5"/></td>
          <td><img src="images/clear.gif" width="70" height="5"/></td>
        </tr>
      <xsl:for-each select="feeList/MembershipTransactionFee">
         <tr>
          <td width="10"></td>
          <td width="10"></td>
          <td class="dataValue" colspan="2" valign="bottom"><xsl:value-of select="feeTypeCode"/></td>
          <td class="helpTextSmall" valign="bottom"><xsl:value-of select="feeDescription"/></td>
          <td class="dataValue" align="right" valign="bottom"><xsl:value-of select="grossFee"/></td>
        </tr>
         <xsl:for-each select="discountList/MembershipTransactionDiscount">
        <tr>
          <td width="10"></td>
          <td width="10"></td>
          <td width="10"></td>
          <td class="label">Discount:</td>
          <td class="dataValue"><xsl:value-of select="discountTypeCode"/></td>
          <td class="dataValue" align="right">-<xsl:value-of select="discountAmount"/></td>
        </tr>
         </xsl:for-each>
        <tr>
          <td width="10"></td>
          <td width="10"></td>
          <td width="10"></td>
          <td class="label" colspan="2">Net fee:</td>
          <td class="dataValue" align="right">
               <hr size="1"/>
               <xsl:value-of select="netFee"/>
               <hr size="3"/>
          </td>
          <td colspan="2"></td>
        </tr>
      </xsl:for-each>
        <tr>
          <td width="10"></td>
          <td class="label" colspan="5">Fee total:</td>
          <td class="dataValue" align="right"><xsl:value-of select="netTransactionFee"/></td>
          <td ></td>
        </tr>
        <tr>
          <td colspan="8" height="10"></td>
        </tr>
   </xsl:for-each>
      </table>
</xsl:template>

</xsl:stylesheet>
