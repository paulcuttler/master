<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.util.HTMLUtil"%>

<html>
<head>
<title>
EditGroupDetails
</title>
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Utilities.js") %>
</head>

<%!
private String constructGroupTypeOption(GroupVO groupVO, String groupTypeCode)
{
  String option = "";
  boolean selected = false;
  if (groupVO != null &&
      groupVO.getGroupTypeCode().equals(groupTypeCode))
  {
    selected = true;
  }
  option += "<option "+(selected?"selected":"")+" value=\""+groupTypeCode+"\">"+groupTypeCode+"</option>";
  return option;
}
%>

<%
Integer groupId = (Integer)request.getAttribute("groupId");
String membershipId = (String)request.getAttribute("membershipID");
GroupVO groupVO = (GroupVO)request.getAttribute("groupVO");

log("groupVO null "+(groupVO==null));

if (groupVO != null)
{
  //set for include
  request.setAttribute("postalAddress",groupVO.getBillingAddress());
}
request.setAttribute("displayResidentialAddress",new Boolean(false));
request.setAttribute("displayPostalAddress",new Boolean(true));
%>

<body>
<h2><%@ include file="/help/HelpLinkInclude.jsp"%> Edit Group Details </h2>

<form action="<%=MembershipUIConstants.PAGE_MembershipUIC%>" name="client" id="client">
  <input type="hidden" name="event" value="saveGroupDetails"/>
  <input type="hidden" name="groupId" value="<%=groupId%>"/>
  <input type="hidden" name="membershipID" value="<%=membershipId%>"/>
<table>
  <tr>
    <td>Group Name</td>
    <td><input type="text" size="40" name="groupName" value="<%=groupVO==null?"":groupVO.getGroupName()%>"/></td>
  </tr>
  <tr>
    <td>Group Type</td>
    <td><select name="groupTypeCode">
      <%=constructGroupTypeOption(groupVO,GroupVO.GROUP_TYPE_CORPORATE)%>
      <%=constructGroupTypeOption(groupVO,"")%>
    </select></td>
  </tr>
  <tr>
    <td>Group Address</td>
    <td>
      <%@include file="/client/EditAddressInclude.jsp"%>
    </td>
  </tr>
  <tr>
    <td>Contact Person</td>
    <td><input type="text" size="40" name="contactPerson" value="<%=groupVO==null?"":groupVO.getContactPerson()%>"/></td>
  </tr>

<tr>
  <td colspan="2">
    <input type="submit" name="Submit" value="Save">
  </td>
</tr>

</table>



</form>
</body>
</html>
