<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>

<%--
Requests parameters for the Motor News delivery extract

request attributes:

returned parameters:
   annualReportFileName
   motorNewsFileName
   errorFileName
   runDate
   runHours
   runMins
--%>

<html>
<head>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/Utilities.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<script language="javascript">

function validate()
{
  return validateRunTime(document.all.runHours, document.all.runMins);
}

</script>
<title>
Journeys Magazine Extract
</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<%
DateTime now = new DateTime().getDateOnly();
SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
String fileDate = sd.format(now);
String defaultMotorNewsFileName = "JourneysMagazine_" + fileDate + ".dat";
String defaultAccessMembersFileName = "AccessMembers_" + fileDate + ".dat";
String defaultAnnualReportFileName =  "AnnualReport_" + fileDate + ".dat";
String defaultErrorFileName =  "JourneysMagazine_Errors_" + fileDate + ".dat";
%>

<form name = "MNForm" method="post" onsubmit="return validate()" action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">

<input type="hidden" name="event" value="MotorNews_DoExtract">

<table border="0" height"100%">
<tr valign="top">
<td>
<table>
<tr>
  <td colspan="2">
    <h2>Extract Journeys Magazine Delivery Lists</h2>
  </td>
</tr>
    <tr>
      <td>
        Normal Journeys Magazine list file name:
      </td>
      <td>
        <input type="text" name="motorNewsFileName" size="50" maxlength="250" value="<%=defaultMotorNewsFileName%>">
      </td>
    </tr>
    <tr>
      <td>
        Access members list file name:
      </td>
      <td>
        <input type="text" name="accessMembersFileName" size="50" maxlength="250" value="<%=defaultAccessMembersFileName%>">
      </td>
    </tr>

    <tr>
      <td>
        Annual report list file name:
      </td>
      <td>
        <input type="text" name="annualReportFileName" size="50" maxlength="250" value="<%=defaultAnnualReportFileName%>">
      </td>
    </tr>
    <tr>
      <td>
        Error log file name:
      </td>
      <td>
        <input type="text" name="errorFileName" size="50" maxlength="250" value="<%=defaultErrorFileName%>">
      </td>
    </tr>
    <tr >
      <td colspan="2">
        <%@include file="/common/scheduleInclude.jsp"%>
      </td>
      </tr>
   </table>
   </td>
   </tr>
   <tr valign="bottom">
   <td>
   <table border="0">
   <tr>
   <td>
         <input type="submit" value="OK"/>
   </td>
   </tr>
   </table>
</td>
</tr>
</table>
</form>
</body>
</html>
