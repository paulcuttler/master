<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Iterator"%>

<html>
<head>
<title>RepairMembership</title>
<%=HTMLUtil.noCacheTag()%>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<h1>Invalid membership quotes</h1>
<%
//MembershipQuoteHome memQuoteHome = MembershipEJBHelper.getMembershipQuoteHome();
MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
MembershipQuote memQuote;

// Update a quote if required
String quoteNumberStr = (String) request.getParameter("quoteNumber");
String transactionXML = (String) request.getParameter("transactionXML");
if (quoteNumberStr != null
&&  transactionXML != null)
{
   try
   {
      memQuote = membershipMgr.getMembershipQuote(new Integer(quoteNumberStr));
      memQuote.setTransactionXML(transactionXML);
      //Update to database
      membershipMgr.updateMembershipQuote(memQuote);

      out.println("<b>Updated quote " + quoteNumberStr + "</b><br>");
   }
   catch (Exception e)
   {
       out.println("<b>Failed to update quote " + quoteNumberStr + "!</b><br>" + e.getMessage() + "<br>");
   }
}


// Fetch the remaining invalid quotes
Vector quoteList = null; //too many quotes
//membershipMgr.getMembershipQuoteList();
if (quoteList == null || quoteList.size() == 0)
{
   out.println("No quotes found!<br>");
}
else
{
//   ArrayList sortedQuoteList = new ArrayList();
//   Iterator quoteListIterator = quoteList.iterator();
//   while(quoteListIterator.hasNext())
//   {
//      memQuote = (MembershipQuote) quoteListIterator.next();
//      sortedQuoteList.add(memQuote.getVO());
//   }

   Iterator quoteListIterator = quoteList.iterator();
   MembershipQuoteComparator comparator = new MembershipQuoteComparator();
   comparator.setSortMode(MembershipQuoteComparator.SORT_BY_CLIENT_NUMBER);
   Collections.sort(quoteList,comparator);
%>
   <table border="0" cellspacing="0" cellpadding="3">
<%
   int totalQuotes = 0;
   int invalidQuotes = 0;
//   MembershipQuote memQuote;
   TransactionGroup transGroup;
   PrintWriter printWriter = new PrintWriter(out);
   boolean invalid = false;
   Exception exception = null;
//   quoteListIterator = sortedQuoteList.iterator();
   while(quoteListIterator.hasNext())
   {
      memQuote = (MembershipQuote) quoteListIterator.next();
      totalQuotes++;
      try
      {
         transGroup = memQuote.getTransactionGroup();
         if (transGroup == null)
         {
            invalid = true;
         }
         else
         {
            invalid = false;
         }
         exception = null;
      }
      catch (Exception e)
      {
         // List this one because the transaction group xml is invalid.
         invalid = true;
         exception = e;
      }
      if (invalid)
      {
         invalidQuotes++;
%>
      <form name="form<%=(memQuote != null ? memQuote.getQuoteNumber().toString() : "XXX")%>" method="post" action="<%=MembershipUIConstants.PAGE_InvalidQuotes%>">
      <input type="hidden" name="quoteNumber" value="<%=(memQuote != null ? memQuote.getQuoteNumber().toString() : "")%>">
      <tr>
         <!--<td valign="top"><b>INVALID</b></td>-->
         <td valign="top" nowrap>
         	<b>Client Number: <%=(memQuote != null ? memQuote.getClientNumber().toString() : "???")%></b><br>
         	Quote Number: <%=(memQuote != null ? memQuote.getQuoteNumber().toString() : "???")%><br>
                Quote Date: <%=(memQuote != null ? memQuote.getQuoteDate().toString() : "???")%><br>
                <% if (memQuote != null) { %>
	              <input type="submit" name="saveButton" value="Save">
                <% } %>
         </td>
         <td valign="top"><textarea name="transactionXML" rows=20 cols=70><%=(memQuote != null ? memQuote.getTransactionXML() : "???")%></textarea></td>
         <td valign="top"><pre><% if (exception != null) exception.printStackTrace(printWriter); %></pre></td>
      </tr>
      </form>
<%
      }
   }
%>
   </table>
   Total quotes scanned = <%=totalQuotes%><br>
   Invalid quotes found = <%=invalidQuotes%><br>
<%
}
%>
</body>
</html>
