<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ page import="com.ract.common.UploadServlet"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="site" tagdir="/WEB-INF/tags" %>

<%@ include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>

	    <meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0"> 
	
		<style type="text/css">
			td.tdLabel { vertical-align: top; padding-top: 4px; }
			.errorMessage { color: red; }
			.header { background: #003366 url("/menu/images/Blank.gif") repeat-x left bottom; }
			.header img { display: block; }
			.holder { margin: 11px; }
		</style>
		<title>Email Advice</title>
	</head>
	<body>
		<div class="header">	
		  <img src="/menu/images/NamePanel.gif" alt="RACT" width="180" height="47" />
		</div> 
		<div class="holder">
			<h1>Email Advice</h1>
			<s:actionmessage />
			<s:actionerror />
			<s:fielderror>
				<s:param>membershipId</s:param>
				<s:param>adviceType</s:param>
			</s:fielderror>
		
			<s:if test="!success">
				<s:form action="EmailAdvice" method="post" enctype="multipart/form-data" >
					<s:textfield name="emailTo" label="Email to" required="true" />
					<!--<site:customField label="From" fieldName="emailFrom">
					  <s:property value="emailFrom" />
					</site:customField>-->
					<s:checkbox name="copyToMe" label="Copy to me" />
					<s:checkbox name="updateEmail" label="Update client email" />
					<s:textarea name="message" label="Message" rows="12" cols="100" required="true" />
					<s:hidden name="membershipId" />
					<s:hidden name="adviceType" />
					<s:hidden name="noticeAction" />
					<s:hidden name="quoteNumber" />
					<s:hidden name="directDebitScheduleId" />
					<table>
						<tr>
						  <td>Attach file (if required): <s:file name="upfile"/><td>
						</tr>
					</table>
					<s:submit />
				</s:form>
<!-- 				<s:if test="upfile != null">
					<table>
						<tr><td class="headerRow" colspan="2">Upload Result:</td></tr>
						<tr><td class="label">File</td><td class="dataValue"><s:property value="upfileFileName"/></td></tr>
					</table>
				</s:if>-->
			</s:if>
			<s:else>
			  <input type="button" value="Close" onclick="window.close();" />
			</s:else>
		</div>
	</body>
</html>
