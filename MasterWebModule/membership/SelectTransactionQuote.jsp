<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.stream.*"%>


<%--
<%@ page import="org.xml.sax.*"%>
<%@ page import="org.apache.xalan.xslt.*"%>
--%>

<%@ include file="/membership/TransactionInclude.jsp"%>

<%
Integer attachedQuoteNumber = null;
MembershipQuote memQuote = transGroup.getAttachedQuote();
if (memQuote == null){
   attachedQuoteNumber = new Integer(0);
}
else{
   attachedQuoteNumber = memQuote.getQuoteNumber();
}
Vector quoteList = transGroup.getAllowedQuoteList();
MembershipQuoteComparator mqc = new MembershipQuoteComparator(true);
Collections.sort(quoteList,mqc);
%>


<html>
<head>
<title>Select Transaction Quote</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript">
   function onLoadEventHandler()
   {
      MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif');
      selectQuote(document.all.selectedQuote.options[document.all.selectedQuote.selectedIndex].value);
      return true;
   }


   // This function makes sure that the quote details are synchronised with the
   // selected quote when using the browser back button.
   function window_onFocus_eventHandler()
   {
      return selectQuote(document.all.selectedQuote.options[document.all.selectedQuote.selectedIndex].value);
   }

   window.onFocus = window_onFocus_eventHandler;


   function selectQuote(quoteNumberStr)
   {
<%
if (!quoteList.isEmpty()) {
   MembershipQuote memQuote2;
   // Hide all detail rows if no quote selected
   out.println("      if (quoteNumberStr == '0') {");
   for (int j = 0; j < quoteList.size(); j++) {
      memQuote2 = (MembershipQuote) quoteList.get(j);
      out.println("            document.all.quoteDetailRow_" + memQuote2.getQuoteNumber() + ".style.display = \"none\";");
   }
   out.println("      }");

   for (int i = 0; i < quoteList.size(); i++) {
      memQuote = (MembershipQuote) quoteList.get(i);
      out.println("      else if (quoteNumberStr == '" + memQuote.getQuoteNumber() + "') {");
      for (int j = 0; j < quoteList.size(); j++) {
         memQuote2 = (MembershipQuote) quoteList.get(j);
         out.println("         document.all.quoteDetailRow_" + memQuote2.getQuoteNumber() + ".style.display = \"" + (memQuote2.getQuoteNumber().equals(memQuote.getQuoteNumber()) ? "" : "none") + "\";");
      }
      out.println("      }");
   }
%>
<%
} else {
	  out.println("alert('Invalid quote number ' + quoteNumberStr)");
      out.println("return false;");
}
%>
      return true;
   }  // function
</script>
</head>
<body onLoad="return onLoadEventHandler();">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<form method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
<input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
<input type="hidden" name="event" value="selectTransactionQuote_btnNext">
<tr valign="top">
<td>
  <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Select Quote</h1>
  Apply quoted fee:&nbsp;
  <select name="selectedQuote" onchange="return selectQuote(document.all.selectedQuote.options[document.all.selectedQuote.selectedIndex].value);">
     <option value="0" <%=(attachedQuoteNumber.intValue() == 0 ? "selected" : "")%>>-- none --</option>
<%
   // Write out the options for the select list first
   for (int i = 0; i < quoteList.size(); i++) {
      memQuote = (MembershipQuote) quoteList.get(i);
%>
     <option value="<%=memQuote.getQuoteNumber()%>" <%=(attachedQuoteNumber.equals(memQuote.getQuoteNumber()) ? "selected" : "")%>><%=memQuote%></option>
<%
   }
%>
  </select>
  <p/>
  <table border="0" cellpadding="0" cellspacing="0" >
<%
   // Write out a table row with each quotes detail
   StringWriter writer = null;
   ArrayList excludedClassList = new ArrayList();
   //try {
   //             excludedClassList.add(Class.forName("com.ract.client.PersonVO"));
   //             excludedClassList.add(Class.forName("com.ract.client.OrganisationVO"));
   //}
   //catch (ClassNotFoundException cnfe){
   //    throw new SystemException("Failed to find class : " + cnfe.getMessage());
   //}

   XMLClassWriter xmlClassWriter = null;
   ClassWriter classWriter = null;
   ByteArrayInputStream xmlDocStream = null;

   TransformerFactory tFactory = TransformerFactory.newInstance();
   Transformer transformer = tFactory.newTransformer(new StreamSource(getServletContext().getResourceAsStream("/membership/xsl/MembershipQuoteViewInclude.xsl")));

   for (int i = 0; i < quoteList.size(); i++) {
      memQuote = (MembershipQuote) quoteList.get(i);
      writer = new StringWriter();
      writer.write(XMLHelper.getXMLHeaderLine());
      xmlClassWriter = new XMLClassWriter(writer,null,excludedClassList);
      classWriter = (ClassWriter) xmlClassWriter;
      memQuote.write(classWriter);
      xmlDocStream = new ByteArrayInputStream(writer.toString().getBytes());

%>
  <tr id="quoteDetailRow_<%=memQuote.getQuoteNumber()%>">
    <td>
      <div style="height:300; overflow:auto; border-width: 1; border-style: solid; padding: 3px 3px 3px 3px;">
<%
      transformer.transform(new StreamSource(xmlDocStream), new StreamResult(out));
%>
      </div>
    </td>
  </tr>
<%
//      processor.reset();
   }
%>
  </table>
</td>
</tr>
<tr valign="bottom">
  <td>
    <table cellpadding="5" cellspacing="0" >
      <tr>
         <td>
            <a onclick="history.back(); return true;"
               onMouseOut="MM_swapImgRestore();"
               onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
               <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
            </a>
         </td>
         <td>
            <a onclick="document.forms[0].submit();"
               onMouseOut="MM_swapImgRestore();"
               onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
               <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Show the next page">
            </a>
         </td>
      </tr>
    </table>
  </td>
</tr>
</form>
</table>
</body>
</html>
