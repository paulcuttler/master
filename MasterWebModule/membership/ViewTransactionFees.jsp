<%@ page import="java.util.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.security.ui.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.payment.ui.*"%>

<html>
<head>
<title>View Transaction Fees</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>

<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif');">
<%
Collection groupTxList = (Collection) request.getAttribute("groupTxList");
Integer membershipID = (Integer)request.getAttribute("membershipID");
MembershipTransactionVO paMemTxVO = (MembershipTransactionVO)request.getAttribute("primeAddresseeTransaction");
%>

  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <form name="viewTransactionFeesForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="viewTransactionFees_btnMembership">
  <input type="hidden" name="membershipID" value="<%=membershipID%>">
  <input type="hidden" name="transactionID">
  <tr valign="top">
    <td nowrap>

<%
//get the generic information from the attached transaction vo.
if (paMemTxVO != null)
{
%>
      <table border="0" cellpadding="3" cellspacing="0">
      <tr>
        <td colspan="2"><%@ include file="/help/HelpLinkInclude.jsp"%> <h2>Membership transaction fees for prime addressee <%=paMemTxVO.getMembership().getMembershipNumber()%></h2></td>
      </tr>
      <tr>
         <td class="label">Group transaction type:</td>
         <td class="dataValue"><%=paMemTxVO.getTransactionGroupTypeCode()!=null?paMemTxVO.getTransactionGroupTypeCode():""%></td>
      </tr>
      <tr>
         <td class="label">Transaction date:</td>
         <td class="dataValue"><%=paMemTxVO.getTransactionDate().formatLongDate()%></td>
      </tr>
      <tr>
         <td class="label">Amount payable:</td>
         <td class="dataValue"><%=CurrencyUtil.formatDollarValue(paMemTxVO.getAmountPayable())%></td>
      </tr>
      <tr>
         <td class="label">GST:</td>
         <td class="dataValue"><%=CurrencyUtil.formatDollarValue(paMemTxVO.getGstFee())%></td>
      </tr>
      </table>
<%
}
else
{
%>
      <table border="0" cellpadding="3" cellspacing="0">
        <tr>
          <td colspan="2" class="helpText">No membership with Id '<%=membershipID%>' found.</td>
        </tr>
      </table>
<%
}
%>
    <h3>Transaction details:</h3>
    <div style="height:300; overflow:auto; border-width: 1; border-style: solid;">
    <table border="0">
<%
Iterator groupTxIt = null;
ArrayList adjustmentList = null;
MembershipTransactionVO memTransVO = null;
MembershipVO memVO = null;
ClientVO clientVO = null;
String addressee = "";
//should be at least one.
if (groupTxList != null && groupTxList.size() > 0)
{
  int recordCounter = 0;
  groupTxIt = groupTxList.iterator();
  while (groupTxIt.hasNext())
  {
    recordCounter++;
    memTransVO = (MembershipTransactionVO)groupTxIt.next();
    memVO = (MembershipVO)memTransVO.getMembershipHistory();

    if (memVO == null)
    {
%>
      <tr>
        <td colspan="5" class="dataValueBlue">Unable to find membership history with Id '<%=membershipID%>'.</td>
      <tr>
<%
    }
    else
    {
      if (paMemTxVO != null)
      {
        addressee = (memTransVO.getMembershipID().equals(paMemTxVO.getMembershipID()) ? "<span class=\"helpTextSmall\"> (Addressee)</span>" : "");
      }
      else
      {
        addressee = "";
      }
      clientVO = memVO.getClient();
%>
      <tr>
        <td colspan="5" class="dataValueBlue"><%=clientVO.getClientNumber()%> <%=clientVO.getDisplayName()%>. <%=addressee%> - <%=MembershipUIHelper.getTransactionHeadingText(memTransVO.getTransactionTypeCode())%></td>
      <tr>
<%
    }
Collection feeList = memTransVO.getTransactionFeeList();
if (feeList != null
&&  !feeList.isEmpty())
{
   MembershipTransactionFee memTransFee = null;
   FeeSpecificationVO feeSpecVO = null;
   FeeTypeVO feeTypeVO = null;
   TransactionFeeDiscountAttribute tfda = null;
   Collection discountList = null;
   Collection discountAttributeList = null;
   MembershipTransactionDiscount memTransDiscount = null;
   Iterator feeIterator = feeList.iterator();
   while (feeIterator.hasNext())
   {
      memTransFee = (MembershipTransactionFee) feeIterator.next();
      feeSpecVO = memTransFee.getFeeSpecificationVO();
      feeTypeVO = feeSpecVO.getFeeType();
%>
      <tr>
         <td class="label" colspan="3"><%=feeSpecVO.getFeeType().getDescription()%></td>
         <td class="label" align="right"><%=CurrencyUtil.formatDollarValue(memTransFee.getGrossFee())%></td>
         <td nowrap></td>
      </tr>
<%
      discountList = memTransFee.getDiscountList();
      if (discountList != null && !discountList.isEmpty())
      {
         Iterator memTransDiscIt = discountList.iterator();
         while (memTransDiscIt.hasNext())
         {
            memTransDiscount = (MembershipTransactionDiscount) memTransDiscIt.next();
%>
      <tr>
         <td class="label" colspan="3"><i><%=memTransDiscount.getDiscountTypeVO().getDescription()%></i></td>
         <td class="label" align="right">-<!--discounts always subtract--><%=CurrencyUtil.formatDollarValue(memTransDiscount.getDiscountAmount())%></td>
         <td nowrap align="right"></td>
      </tr>
<%
log("1");
           discountAttributeList = memTransDiscount.getDiscountAttributeList(memTransFee.getTransactionID(),memTransFee.getFeeSpecificationID());
           if (discountAttributeList != null &&
              !discountAttributeList.isEmpty())
           {
log("2");
             Iterator discAttrIt = discountAttributeList.iterator();
             while (discAttrIt.hasNext())
             {
               tfda = (TransactionFeeDiscountAttribute)discAttrIt.next();
log("3");
%>
      <tr>
         <td class="helpTextSmall" colspan="3">&nbsp;</td>
         <td class="helpTextSmall" align="right"><%=tfda.getTransactionFeeDiscountAttributePK().getAttributeName()%>: <%=tfda.getAttributeValue()%></td>
         <td nowrap align="right"></td>
      </tr>
<%
             }
           }
         }
      }
%>
   <tr>
     <td colspan="3"></td>
     <td><hr/></td>
     <td></td>
   </tr>
<!-- total fee per member -->
   <tr>
     <td colspan="3"></td>
     <td class="dataValue" align="right"><%=CurrencyUtil.formatDollarValue(memTransFee.getNetTransactionFee())%></td>
     <td></td>
   </tr>
   <tr>
     <td colspan="3"></td>
     <td><hr/></td>
     <td></td>
   </tr>
<%
   }
}
else {
%>
   <tr>
     <td colspan="5" class="helpText">No Fees</td>
   </tr>
<%
}
%>
   <tr>
     <td colspan="3"></td>
     <td></td>
     <td align="right" class="dataValue"><%=CurrencyUtil.formatDollarValue(memTransVO.getNetTransactionFee())%></td>
   </tr>
<%
adjustmentList = memTransVO.getAdjustmentList();
if (adjustmentList != null
&&  !adjustmentList.isEmpty()) {
%>
      <tr>
        <td colspan="5" class="dataValue">Adjustments</td>
      <tr>

<%
   MembershipTransactionAdjustment memTransAdjustment;
   AdjustmentTypeVO atVO = null;
   for (int i = 0; i < adjustmentList.size(); i++)
   {
      memTransAdjustment = (MembershipTransactionAdjustment) adjustmentList.get(i);
      atVO = memTransAdjustment.getAdjustmentTypeVO();
%>
      <tr>
         <td class="label" colspan="3"><%=memTransAdjustment.getAdjustmentTypeVO().getDescription()%></td>
         <td class="label" align="right"><%=atVO.isDebitAdjustment()?"-":""%><%=CurrencyUtil.formatDollarValue(memTransAdjustment.getAdjustmentAmount())%></td>
         <td><%=memTransAdjustment.getAdjustmentReason()%></td>
      </tr>
<%
   }
%>
   <!-- total adjustments -->
   <tr>
     <td colspan="3"></td>
     <td></td>
     <td class="dataValue" align="right"><%=CurrencyUtil.formatDollarValue(memTransVO.getAdjustmentTotal())%></td>
   </tr>
<%
}
  }
}
%>
  </table>
  </div>
    </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table width="0%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>
               <a onclick="document.all.event.value='viewMembershipHistory_btnMembership'; document.forms[0].submit(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('View the membership details');MM_swapImage('ViewMembershipButton','','<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif',1);return document.MM_returnValue" >
                  <img name="ViewMembershipButton" src="<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton.gif" border="0" alt="View the membership details">
               </a>
            </td>
        </tr>
      </table>
    </td>
  </tr>
  </form>
  </table>

</body>
</html>
