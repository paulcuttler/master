<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.ract.payment.ui.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="en-AU" />

<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>

		<script type="text/javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js"></script>
		
	    <meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0"> 
	
		<style type="text/css">
			.header { background: #003366 url("/menu/images/Blank.gif") repeat-x left bottom; }
			.header img { display: block; }
			.holder { margin: 11px; }			
			.listHeadingPadded { border-bottom: 1px solid #fff; }		
		</style>
		
		<title>Roadside Ledger Report: Daily Breakdown</title>
	</head>
	<body>
		<div class="header">
		  <img src="/menu/images/NamePanel.gif" alt="RACT" width="180" height="47" />
		</div> 
		<div class="holder">
			<h1>Roadside Ledger Report - Daily Breakdown</h1>
			<s:actionmessage />
			<s:actionerror />
			<s:fielderror />
			
			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr class="headerRow">
					<td class="listHeadingPadded">Period</td>
					<td class="listHeadingPadded" style="text-align: right">Ledgers</td>
					<td class="listHeadingPadded">&nbsp;</td>
					<td class="listHeadingPadded" style="text-align: right">Vouchers</td>
					<td class="listHeadingPadded">&nbsp;</td>
					<td class="listHeadingPadded" style="text-align: right">Credit</td>
					<td class="listHeadingPadded">&nbsp;</td>
					<td class="listHeadingPadded" style="text-align: right">Variance</td>
				</tr>
			
				<s:if test="dailyBreakdown != null and !dailyBreakdown.isEmpty()">
					<s:iterator var="day" value="dailyBreakdown" status="status">
						<s:iterator var="label" value="#day.keySet()">
							<s:set var="start" value="%{#label.split(':')[0]}" />
							<s:set var="end" value="%{#label.split(':')[1]}" />
							<s:set var="resultSet" value="%{#day.get(#label)}" />						
							<tr class="${status.even ? 'even' : 'odd'}Row">
								<td><a href="RoadsideLedgerReportClients.action?start=${start}&end=${end}" onclick="return newWindow(this.href);" title="View Clients"><s:property value="#start" /></a></td>							
								<td style="text-align: right"><fmt:formatNumber value="${resultSet['postings']}" type="currency" /></td>
								<td style="text-align: right">+</td>
								<td style="text-align: right"><fmt:formatNumber value="${resultSet['vouchers']}" type="currency" /></td>
								<td style="text-align: right">+</td>
								<td style="text-align: right"><fmt:formatNumber value="${resultSet['credits']}" type="currency" /></td>
								<td style="text-align: right">=</td>
								<td style="text-align: right"><fmt:formatNumber value="${resultSet['variance']}" type="currency" /></td>
							</tr>
						</s:iterator>
					</s:iterator>
				</s:if>
			
			</table>
		</div>				
	</body>
</html>