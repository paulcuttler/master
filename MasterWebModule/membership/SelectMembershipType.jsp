<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>

<%@ include file="/membership/TransactionInclude.jsp"%>

<%
Collection memTypeList = (Collection) request.getAttribute("allowedMembershipTypeList");
%>
<html>
<head>
<title>Select membership type</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<form name="selectMembershipTypeForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
    <input type="hidden" name="event" value="selectMembershipType_btnNext">
    <tr valign="top">
      <td>
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Select a membership type</h1>
        <table border="0" cellpadding="3" cellspacing="0">
<%
if (memTypeList != null)
{
   Iterator it = memTypeList.iterator();
   while (it.hasNext())
   {
      MembershipTypeVO memType = (MembershipTypeVO) it.next();
      String memTypeCode = memType.getMembershipTypeCode();
      String checked = (memType.isDefault() || memTypeList.size() == 1 ? " checked" : "");
%>
          <tr>
            <td valign="top"><input type="radio" name="membershipTypeCode" value="<%=memTypeCode%>" <%=checked%>></td>
            <td valign="top" class="dataValue"><%=memTypeCode%></td>
            <td><img src="<%=CommonConstants.DIR_IMAGES + "/clear.gif"%>" width="20" height="5" border="0"></td>
            <td valign="top" class="label"><%=memType.getDescription()%></td>
          </tr>
<%
   }
}
%>
        </table>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
         <table border="0" cellpadding="3" cellspacing="0">
         <tr>
            <td><input type="button" name="btnBack" value="Back" onclick="history.back()"></td>
            <td><input type="submit" name="btnSubmit" value="Next"></td>
         </tr>
         </table>
      </td>
  </table>
</form>
</body>
</html>
