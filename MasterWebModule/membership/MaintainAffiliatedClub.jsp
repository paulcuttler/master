<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.club.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.math.*"%>

<%
String saveEvent     = "maintainAffiliatedClub_btnSave";
String deleteEvent   = "maintainAffiliatedClub_btnDelete";
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;

AffiliatedClubVO affiliatedClub = (AffiliatedClubVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_AFFILIATED_CLUB_VO);
String    origCode  = (String) request.getSession().getAttribute("origCode");
String    statMsg   = (String) request.getAttribute("statusMessage");
String    refresh   = (String) request.getAttribute("Refresh");

String clubCode			= null;
String clubDescription	        = "";
DateTime createDate		= new DateTime();
boolean clubActive		= false;
//BigDecimal clubFee		= new BigDecimal(0.0);

if (!(affiliatedClub==null))
{
  clubCode           = affiliatedClub.getClubCode();
  clubDescription    = affiliatedClub.getDescription();
  createDate         = affiliatedClub.getCreateDate();
  clubActive         = affiliatedClub.isActive();
//  clubFee            = affiliatedClub.getFee();
}
else clubCode = null;

%>

<head>
<title>Maintain Affiliated Club</title>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body class="rightFrame" onload="refreshList(document.all.refresh,'<%=listPage%>');">

<form name="mainForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>">
  <input type="hidden" name="event" value="">
  <input type="hidden" name="listCode" value ="">
  <input type="hidden" name="refresh" value ="<%=refresh%>">

 <div id="mainpage" class="pg">
 <h1>Maintain Affiliated Club</h1>
   <div id="contentpage" class="cnt">
<!--
   START AFFILIATED CLUB BODY
-->
   <table border="0" cellspacing="0" cellpadding="0">
     <tr>
       <td class="label">Club Code:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
       <%
       if (clubCode != null &&
       !clubCode.trim().equals(""))
       {
       %>
       <%=clubCode%>
         <input type="hidden" name="clubCode" value="<%=clubCode%>">
       <%
       }
       else
       {
       %>
         <%=HTMLUtil.inputOrReadOnly("clubCode",clubCode)%>
       <%
       }
       %>
       </td>
     </tr>
     <tr>
       <td class="label">Description:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue" >
         <input type="text" size="55" maxlength ="40" name="clubDescription" value="<%=clubDescription==null?"":clubDescription%>">
       </td>
     </tr>
     <tr>
       <td class="label">Create Date:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
         <input type="text" size="55" maxlength ="40" name="createDate" value="<%=createDate==null?"":createDate%>">
       </td>
     </tr>
     <tr>
       <td class="label">Active:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
         <input type="checkbox" name="clubActive" value="true" <%=clubActive?"checked":""%>>
       </td>
     </tr>
   </table>
<!--
   FINISH AFFILIATED CLUB BODY
-->
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSaveAndDelete(saveEvent,deleteEvent,"AffiliatedClub", statMsg )%>
   </div>
</div>
</form>
</body>
</html>
