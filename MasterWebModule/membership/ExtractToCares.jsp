<%@include file="/security/VerifyAccess.jsp"%>

<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.cad.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.rmi.RemoteException"%>
<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>

<html>
<head>
<title>Extract To CARES</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif');">
<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
<form name="extractForm" method="post" action="<%= MembershipUIConstants.PAGE_MembershipAdminUIC%>" >
<input type="hidden" name="event" value="extractToCARES">
<tr valign="top">
  <td>
     <h1>Extract to CARES</h1>
<%
CadExportMgr exportMgr = CommonEJBHelper.getCadExportMgr();
String status = exportMgr.getRefreshMembershipStatus();
int recordCount = exportMgr.getRefreshMembershipRecordCount();
RemoteException exception = exportMgr.getRefreshMembershipException();
%>
     <table border="0" cellpadding="3" cellspacing="0">
     <tr>
       <td class="label">Current status:</td>
       <td class="dataValue"><%=status%></td>
       <td><input type="submit" name="btnCancel" value="Cancel" onclick="document.all.event.value='extractToCARES_btnCancel'; return true;"></td>
     </tr>
<%
if (recordCount > 0) {
%>
     <tr>
       <td class="label">Records processed:</td>
       <td class="datavalue"><%=recordCount%></td>
     </tr>
<%
}
%>
     </table>
<%
if (exception != null) {
%>
     <p>Last exception:<br>
     <pre>
<%
   PrintWriter pw = new PrintWriter(out);
   exception.printStackTrace(pw);
%>
     </pre>
     </p>

<%
}
%>
     <br>
     <span class="listHeading">Extract history</span>
     <table border="0" cellpadding="3" cellspacing="0">
      <tr class="headerRow">
         <td class="listHeadingPadded">Control ID</td>
         <td class="listHeadingPadded">Start date</td>
         <td class="listHeadingPadded">End date</td>
         <td class="listHeadingPadded">Extract date</td>
      </tr>
<%
ArrayList cadExportControlList = exportMgr.getCadExportControlList();
Collections.reverse(cadExportControlList);
if (cadExportControlList != null
&&  cadExportControlList.size() > 0) {
   CadExportControlVO controlVO = null;
   for (int i = 0; i < cadExportControlList.size(); i++) {
      controlVO = (CadExportControlVO) cadExportControlList.get(i);
%>
      <tr class="<%=HTMLUtil.getRowType(i)%>">
         <td class="listItem"><%=controlVO.getControlID()%></td>
         <td class="listItem"><%=(controlVO.getStartDate() != null ? controlVO.getStartDate().formatLongDate() : "")%></td>
         <td class="listItem"><%=(controlVO.getEndDate() != null ? controlVO.getEndDate().formatLongDate() : "")%></td>
         <td class="listItem"><%=(controlVO.getExtractDate() != null ? controlVO.getExtractDate().formatLongDate() : "")%></td>
      </tr>
<%
   }
}
%>
     </table>
   </td>
</tr>
<tr valign="bottom">
  <td>
    <table cellpadding="5" cellspacing="0">
    <tr>
<%--
      <td>
         <a onclick="document.forms[0].submit();"
            onMouseOut="MM_swapImgRestore();"
            onMouseOver="MM_displayStatusMsg('Save the cancellation.');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
            <img name="ExtractButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Save the cancellation.">
         </a>
      </td>
--%>

        <td><input type="submit" name="btnExtract" value="Extract" onclick="document.all.event.value='extractToCARES_btnExtract'; return true;"></td>
        <td><input type="submit" name="btnRefresh" value="Refresh" onclick="document.all.event.value='extractToCARES_btnRefresh'; return true;"></td>
    </tr>
    </table>
  </td>
</tr>
</form>
</table>
</body>
</html>
