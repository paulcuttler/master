<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.security.*"%>
<%@ page import="com.ract.user.*"%>
<%@ page import="com.ract.util.*"%>

<html>

  <head>
    <%=CommonConstants.getRACTStylesheet()%>
  </head>

  <body>
  <form enctype="multipart/form-data"  name="uploadMembershipFile" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="uploadMembershipFile">

<table>
<tr valign="top">
  <td>
    <h2><%@ include file="/help/HelpLinkInclude.jsp"%> Upload membership file</h2>
  </td>
</tr>
<tr>
  <td colspan="2"><span class="helpTextSmall">Click browse and select the comma delimited file containing member data.</span></td>
</tr>
<tr>
  <td>File to upload:</td><td><input type="file" name="upfile"><td>
</tr>
<tr>
  <td colspan="2"><input type="submit" value="Upload">
  </td>
</tr>
</table>

    </form>
  </body>

</html>
