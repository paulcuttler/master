<%@page import="com.ract.common.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%--
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.stream.*"%>

<%
MembershipQuote memQuote = (MembershipQuote) request.getAttribute("membershipQuote");
StringWriter writer = new StringWriter();
writer.write(XMLHelper.getXMLHeaderLine());

XMLClassWriter xmlClassWriter = new XMLClassWriter(writer,null,null);
ClassWriter classWriter = (ClassWriter) xmlClassWriter;
memQuote.write(classWriter);
ByteArrayInputStream xmlDocStream = new ByteArrayInputStream(writer.toString().getBytes());

TransformerFactory tFactory = TransformerFactory.newInstance();
Transformer transformer = tFactory.newTransformer(new StreamSource(getServletContext().getResourceAsStream(XMLHelper.XSL_MEMBERSHIP_QUOTE_VIEW)));
transformer.transform(new StreamSource(xmlDocStream), new StreamResult(out));
%>
--%>
<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
	MembershipQuote quote = (MembershipQuote) request.getAttribute("membershipQuote");
	quote.getTransactionGroup().calculateTransactionFee();
	request.setAttribute("membershipQuote", quote);
 %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<link href="/css/ract.css" rel="stylesheet" type="text/css" />

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">

		<title>Roadside Quote</title>


		<script type="text/javascript">
		  function printMembershipQuote() {
		    var url = '/MembershipReportsUIC?event=printMembershipQuote&quoteNumber=<c:out value="${membershipQuote.quoteNumber}" />';
		    window.open(url, '',
		        'width=250,height=150,toolbar=no,status=no,scrollbars=no,resizable=no')
		  }
		
		  function emailMembershipQuote() {
		    var url = 'EmailAdviceForm.action?adviceType=QUOTE&quoteNumber=<c:out value="${membershipQuote.quoteNumber}" />';
		    window.open(url, '',
		        'width=640,height=480,toolbar=no,status=no,scrollbars=no,resizable=no');
		  }
		</script>
	</head>

	<body>
		<table border="0" cellpadding="0" cellspacing="0" width="100%"
			height="100%">
			<tr valign="top">
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td valign="top">
								<h1>
									Roadside Quote
								</h1>
							</td>
							<td align="right" valign="center">
								<input type="button" value="Print Quote"
									onclick="printMembershipQuote()" />
								<input type="button" value="Email Quote"
									onclick="emailMembershipQuote()" />
								<input type="button" value="Print Screen"
									onclick="window.print(); return true;" />
							</td>
						</tr>
					</table>
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top">
								<table border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td class="label">
											Quote No.:
										</td>
										<td class="dataValue">
											<c:out value="${membershipQuote.quoteNumber}" />
										</td>
									</tr>
									<tr>
										<td class="label">
											Quote Date:
										</td>
										<td class="dataValue">
											<c:out value="${membershipQuote.quoteDate}" />
										</td>
									</tr>
									<tr>
										<td class="label">
											User:
										</td>
										<td class="dataValue">
											<c:out value="${membershipQuote.userId}" />
										</td>
									</tr>
									<tr>
										<td class="label">
											Sales branch:
										</td>
										<td class="dataValue">
											<c:out value="${membershipQuote.salesBranchCode}" />
										</td>
									</tr>
									<tr>
										<td class="label">
											Transaction type:
										</td>
										<td class="dataValue">
											<c:out value="${membershipQuote.transactionGroupTypeCode}" />
										</td>
									</tr>
									<c:if
										test="${membershipQuote.transactionGroup.rejoinType != null}">
										<tr>
											<td class="label" valign="top">
												Rejoin type:
											</td>
											<td class="dataValue" colspan="3" valign="top">
												<c:out
													value="${membershipQuote.transactionGroup.rejoinType.description}" />
											</td>
										</tr>
									</c:if>
									<c:if
										test="${membershipQuote.transactionGroup.transactionReason != null}">
										<tr>
											<td class="label" valign="top">
												Reason:
											</td>
											<td class="dataValue" colspan="3" valign="top">
												<c:out
													value="${membershipQuote.transactionGroup.transactionReason.description}" />
											</td>
										</tr>
									</c:if>
									<tr>
										<td class="label">
											Product:
										</td>
										<td class="dataValue">
											<c:out
												value="${membershipQuote.transactionGroup.selectedProduct.productCode}" />
										</td>
									</tr>
									<tr>
										<td class="label">
											Expiry date:
										</td>
										<td class="dataValue">
											<c:out
												value="${membershipQuote.transactionGroup.expiryDate}" />
										</td>
									</tr>
								</table>
							</td>
							<td width="20"></td>
							<td valign="top">
								<table border="1" bordercolor="#CC0000" cellpadding="3"
									cellspacing="0">
									<tr>
										<td>
											<table border="0" cellspacing="0" cellpadding="3">
												<tr>
													<td class="label" valign="top">
														Total net fee:
													</td>
													<td class="dataValue" align="right" valign="top">
														<c:out
															value="${membershipQuote.transactionGroup.netTransactionFee}" />
													</td>
												</tr>
												<c:forEach var="adj"
													items="${membershipQuote.transactionGroup.adjustmentList}">
													<tr>
														<td class="label" valign="bottom">
															<c:out value="${adj.adjustmentTypeCode}" />
														</td>
														<td class="dataValue" align="right" valign="bottom">
															<c:if test="${adj.debitAdjustment}">-</c:if>
															<c:out value="${adj.adjustmentAmount}" />
														</td>
														<td class="helpTextSmall" valign="bottom">
															(
															<c:out value="${adj.adjustmentReason}" />
															)
														</td>
													</tr>
												</c:forEach>
												<tr>
													<td colspan="3">
														<hr size="1" />
													</td>
												</tr>
												<tr>
													<td class="label" valign="top">
														Amount payable:
													</td>
													<td class="dataValue" align="right" valign="top">
														$
														<c:out
															value="${membershipQuote.transactionGroup.amountPayable}" />
													</td>
													<td valign="top">
														(inc. $
														<c:out
															value="${membershipQuote.transactionGroup.gstAmount}" />
														gst)
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>

					<h3>
						Membership transaction details -
						<c:out value="${fn:length(membershipQuote.transactionGroup.transactionList)}" />
						member(s)
					</h3>
					<table border="0" cellspacing="0" cellpadding="3">
						<c:forEach var="txn" items="${membershipQuote.transactionGroup.transactionList}">
							<tr>
								<td style="white-space: nowrap;" colspan="8">
									<span class="dataValue"><c:out
											value="${txn.newMembership.client.clientNumber}" />
									</span>&#xA0;&#xA0;
									<span class="dataValue"><c:out
											value="${txn.newMembership.client.displayName}" />
									</span>&#xA0;&#xA0;
									<c:if test="${txn.newMembership.primeAddressee}">
										<span class="helpTextSmall">(addressee)&#xA0;&#xA0;</span>
									</c:if>
									<span class="dataValue">-&#xA0;&#xA0;<c:out
											value="${txn.transactionTypeCode}" />
									</span>
								</td>
							</tr>
							<tr>
								<td class="dataValue" colspan="8" style="white-space: nowrap;">
									<c:out
										value="${txn.newMembership.client.residentialAddress.singleLineAddress}" />
								</td>
							</tr>
							<tr>
								<td width="10"></td>
								<td class="label" colspan="3">
									Product effective:
								</td>
								<td class="dataValue" colspan="4">
									<c:out value="${txn.newMembership.productEffectiveDate}" />
								</td>
							</tr>
							<tr>
								<td width="10"></td>
								<td class="label" colspan="3">
									Join date:
								</td>
								<td class="dataValue" colspan="4">
									<c:out value="${txn.newMembership.joinDate}" />
								</td>
							</tr>
							<tr>
								<td width="10"></td>
								<td class="label" colspan="3">
									Commence date:
								</td>
								<td class="dataValue" colspan="4">
									<c:out value="${txn.newMembership.commenceDate}" />
								</td>
							</tr>
							<c:if test="${txn.newMembership.affiliatedClubCode != null}">
								<tr>
									<td width="10"></td>
									<td class="label" colspan="3">
										Affiliated club code:
									</td>
									<td class="dataValue" colspan="4">
										<c:out value="${txn.newMembership.affiliatedClubCode}" />
									</td>
								</tr>
								<tr>
									<td width="10"></td>
									<td class="label" colspan="3">
										Affiliated club Id:
									</td>
									<td class="dataValue" colspan="4">
										<c:out value="${txn.newMembership.affiliatedClubId}" />
									</td>
								</tr>
								<tr>
									<td width="10"></td>
									<td class="label" colspan="3">
										Affiliated club expiry date:
									</td>
									<td class="dataValue" colspan="4">
										<c:out value="${txn.newMembership.affiliatedClubExpiryDate}" />
									</td>
								</tr>
							</c:if>

							<tr>
								<td width="10"></td>
								<td class="label" colspan="7">
									Product options:
								</td>
							</tr>

							<c:forEach var="prd" items="${txn.newMembership.productOptionList}">
								<tr>
									<td width="10"></td>
									<td width="10"></td>
									<td class="dataValue" colspan="2">
										<c:out value="${prd.productBenefitTypeCode}" />
									</td>
									<td class="label">
										Effective:&#xA0;
										<span class="dataValue"><c:out
												value="${prd.effectiveDate}" />
										</span>
									</td>
								</tr>
							</c:forEach>
							<tr>
								<td width="10"></td>
								<td class="label" colspan="4">
									Fees:
								</td>
								<td>
									<img src="images/clear.gif" width="70" height="5" />
								</td>
								<td>
									<img src="images/clear.gif" width="70" height="5" />
								</td>
							</tr>
							<c:forEach var="fee" items="${txn.transactionFeeList}">
								<tr>
									<td width="10"></td>
									<td width="10"></td>
									<td class="dataValue" colspan="2" valign="bottom">
										<c:out value="${fee.feeSpecificationVO.feeTypeCode}" />
									</td>
									<td class="helpTextSmall" valign="bottom">
										<c:out value="${fee.feeDescription}" />
									</td>
									<td class="dataValue" align="right" valign="bottom">
										<c:out value="${fee.grossFee}" />
									</td>
								</tr>
								<c:forEach var="dsc" items="${fee.discountList}">
									<tr>
										<td width="10"></td>
										<td width="10"></td>
										<td width="10"></td>
										<td class="label">
											Discount:
										</td>
										<td class="dataValue">
											<c:out value="${dsc.discountCode}" />
										</td>
										<td class="dataValue" align="right">
											-
											<c:out value="${dsc.discountAmount}" />
										</td>
									</tr>
								</c:forEach>
								<tr>
									<td width="10"></td>
									<td width="10"></td>
									<td width="10"></td>
									<td class="label" colspan="2">
										Net fee:
									</td>
									<td class="dataValue" align="right">
										<hr size="1" />
										<c:out value="${fee.netTransactionFee}" />
										<hr size="3" />
									</td>
									<td colspan="2"></td>
								</tr>
							</c:forEach>
							<tr>
								<td width="10"></td>
								<td class="label" colspan="5">
									Fee total:
								</td>
								<td class="dataValue" align="right">
									<c:out value="${txn.netTransactionFee}" />
								</td>
								<td></td>
							</tr>
							<tr>
								<td colspan="8" height="10"></td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</tr>
			<tr valign="bottom">
				<td><br><br></td>
			</tr>
		</table>
	</body>
</html>