<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.ract.payment.ui.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="en-AU" />

<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ include file="/security/VerifyAccess.jsp"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<%=CommonConstants.getRACTStylesheet()%>

	    <meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0"> 
	
		<style type="text/css">
			.header { background: #003366 url("/menu/images/Blank.gif") repeat-x left bottom; }
			.header img { display: block; }
			.holder { margin: 11px; }			
			.listHeadingPadded { border-bottom: 1px solid #fff; }			
		</style>
		<title>Roadside Ledger Report Postings: <s:property value="clientNumber" /></title>
	</head>
	<body>	
		<div class="header">	
		  <img src="/menu/images/NamePanel.gif" alt="RACT" width="180" height="47" />
		</div> 
		<div class="holder">
			<h1>Roadside Ledger Report Postings</h1>
			<ul>
				<li>Client No: <s:property value="clientNumber" /></li>
				<li>Start Date: <s:property value="start" /></li>
				<li>End Date: <s:property value="end" /></li>
			</ul>
			<s:actionmessage />
			<s:actionerror />
			<s:fielderror />
					
			<s:if test="postings != null and !postings.isEmpty()">				
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="headerRow">
					<td class="listHeadingPadded">Create Date</td>
					<td class="listHeadingPadded">Posted Date</td>
					<td class="listHeadingPadded">Post At Date</td>
					<td class="listHeadingPadded">Amount</td>
					<td class="listHeadingPadded">Account No</td>
					<td class="listHeadingPadded">Journal No</td>
					<td class="listHeadingPadded">Description</td>
				</tr>
				<s:set name="total" value="%{0}" />
				<s:iterator value="postings" status="status">
					<tr class="${status.even ? 'even' : 'odd'}Row">
						<td><s:date name="createDate" format="dd/MM/yyyy" /></td>
						<td><s:date name="postedDate" format="dd/MM/yyyy" /></td>
						<td><s:date name="postAtDate" format="dd/MM/yyyy" /></td>
						<td style="text-align: right"><fmt:formatNumber value="${amount}" type="currency" /></td>
						<td style="text-align: center"><s:property value="accountNumber" /></td>
						<td><s:property value="journalNumber" /></td>
						<td><s:property value="description" /></td> 
						
						<s:set name="total" value="%{#total + amount}" />
					</tr>
				</s:iterator>				
				<tr>
					<td colspan="3" style="font-weight: bold;">Total</td>
					<td style="text-align: right; font-weight: bold;"><fmt:formatNumber value="${total}" type="currency" /></td>
					<td colspan="3">&nbsp;</td>
				</tr>
				</table>
			</s:if>
			<s:else>
				<p>No 2310 postings found</p>
			</s:else>
		</div>
	</body>
</html>