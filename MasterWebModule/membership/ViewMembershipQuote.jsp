<%@page import="com.ract.client.*"%>
<%@page import="com.ract.membership.*"%>
<%@page import="com.ract.membership.ui.*"%>
<%@page import="com.ract.util.*"%>
<%
MembershipQuote memQuote = (MembershipQuote) request.getAttribute("membershipQuote");
ClientVO clientVO = null;
if (memQuote != null) {
   clientVO = ClientEJBHelper.getClientMgr().getClient(memQuote.getClientNumber());
}
request.setAttribute("client",clientVO);

String refresh = (String) request.getAttribute("refreshProductList");
boolean refreshProductList = "Yes".equalsIgnoreCase(refresh);
%>
<html>
<head>
<title>View Roadside Quote</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript">
   function showQuoteDetail(quoteNumber)
   {
      var url = '<%=MembershipUIConstants.PAGE_MembershipUIC%>?event=viewMembershipQuote_detail&quoteNumber=' + quoteNumber;
      window.open(url, '', 'titlebar=yes,toolbar=no,status=no,scrollbars=yes,resizable=yes');
      return true;
   }

   function onLoadEventHandler()
   {
      MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif');
//      if (<%=clientVO != null && refreshProductList%>) {
//         parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber=<%=(clientVO != null ? clientVO.getClientNumber().toString() : "")%>';
//      }
      return true;
   }
</script>
</head>
<body onload="return onLoadEventHandler();">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr valign="top">
   <td>
      <h1><%@ include file="/help/HelpLinkInclude.jsp"%> Roadside Quote</h1>
      <%@ include file="/client/ViewClientInclude.jsp"%>
<%
if (memQuote != null) {
%>
      <table border="0" cellpadding="3" cellspacing="0">
      <tr>
         <td class="label">Quote number:</td>
         <td class="dataValue"><%=memQuote.getQuoteNumber()%></td>
      </tr>
      <tr>
         <td class="label">Quote date:</td>
         <td class="dataValue"><%=memQuote.getQuoteDate().formatShortDate()%></td>
      </tr>
      <tr>
         <td class="label">Transaction type:</td>
         <td class="dataValue"><%=memQuote.getTransactionGroupTypeCode()%></td>
      </tr>
      <tr>
         <td class="label">Quoted amount:</td>
         <td class="dataValue"><%=CurrencyUtil.formatDollarValue(memQuote.getAmountPayable())%></td>
      </tr>
      <tr>
         <td class="label">Attached to transaction:</td>
<%
   MembershipTransactionVO memTransVO = memQuote.getAttachedTransaction();
   if (memTransVO != null) {
%>
         <td class="dataValue">
            <%=memTransVO.getTransactionGroupTypeCode()%>&nbsp;-&nbsp;
            <%=memTransVO.getTransactionDate().formatLongDate()%>&nbsp;-&nbsp;
            <%=CurrencyUtil.formatDollarValue(memTransVO.getAmountPayable())%>
         </td>
<%
   }
   else {
%>
         <td class="dataValue">none.</td>
<%
   }
%>
      </tr>
      <tr>
         <td colspan="2">
            <input type="button" value="Show detail" onclick="showQuoteDetail(<%=memQuote.getQuoteNumber()%>); return true;">
         </td>
      </tr>
      </table>
<%
}
%>
   </td>
</tr>
<tr valign="bottom">
   <td>
        <table border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td>
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
          </td>
        </tr>
        </table>
   </td>
</tr>
</table>
</body>
</html>
