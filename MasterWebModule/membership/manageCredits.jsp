<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.client.*"%>
<html>

<head>
<title> 
ManageCredits
</title>
<link href="<s:url value="/css/ract.css"/>" rel="stylesheet"
			type="text/css" />
<%=HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/InputControl.js") %>

<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_1_4_2%>/dojo/dojo.js" djConfig="parseOnLoad: false, isDebug:false"></script>

<script type="text/javascript">
   	dojo.require("dojo.parser");
</script>
 
<s:head theme="simple"/>

<script type="text/javascript">
	var DEFAULT_MESSAGE = "Refund of membership credit";
	
	function setOption(thing)
	{
	   if(thing.value=="<%=FinanceVoucher.TYPE_CHEQUE_MEMBER%>")
	   {
	       dojo.byId("member").style.display='block';
	       dojo.byId("tfrData").style.display='none';
	       dojo.byId("glData").style.display='none';       
	       updateParticulars(DEFAULT_MESSAGE);
	       document.all.disposeType.value="<%=FinanceVoucher.TYPE_CHEQUE_MEMBER%>";
	   }
	   else if(thing.value=="<%=FinanceVoucher.TYPE_WRITE_OFF%>")
	   {
	       dojo.byId("member").style.display='none';  
	       dojo.byId("tfrData").style.display='none';  
	       dojo.byId("glData").style.display='block';
	       updateParticulars("Write off membership credit");
	       document.all.disposeType.value="<%=FinanceVoucher.TYPE_WRITE_OFF%>";
	   }
	   else if(thing.value=="<%=FinanceVoucher.TYPE_TRANSFER_TO_MEMBER%>")
	   {
	       dojo.byId("member").style.display='none';
	       dojo.byId("tfrData").style.display='block';
	       dojo.byId("glData").style.display='none'; 
	       updateParticulars("Transfer of membership credit");      
	       document.all.disposeType.value="<%=FinanceVoucher.TYPE_TRANSFER_TO_MEMBER%>";
	    }
	 }
	 
	 function updateParticulars(msg) {
	 	var fld = dojo.byId("createVoucher_particulars");
	 	fld.value =	msg;
	 }
	 
	 function toggleMemberAddress(fld) {
	 	var memberNode = dojo.byId("member");
	 	if (!fld.checked) {
			dojo.removeClass(memberNode, "hideMemberAddress"); 	
	 	} else {
	 		dojo.addClass(memberNode, "hideMemberAddress");
	 	}
	 }
</script>
</head>

<body>
<h1>Manage Credit Amounts</h1>

<s:include value="/client/ViewClientInclude.jsp"></s:include>
 
<s:form theme="simple" action="createVoucher"> 
 
  <table>
    <tr>
      <td>Available Credit</td>
      <td><s:property value="creditAmount"/><s:hidden name="creditAmount" /></td>
    </tr>
    <tr> 
	  <td>Amount to dispose of</td>
	  <td>
	    <s:textfield id="amountToDisposeOf" name="disposalAmount" onKeyUp = "checkFloat(this)" />
  	  </td>
    </tr>
    <tr>
      <td>Fee</td>
      <td>
        <s:textfield id="fee" name="feeAmount" onKeyUp = "checkFloat(this)" />
      </td>
    </tr>
    <tr>
      <td>Reason for Refund</td>
      <td>
        <s:select name="disposalReason" list="refDataList" listKey="referenceDataPK.referenceCode" listValue="description" />
      </td>
    </tr>
    <tr>
      <td valign="top">Particulars</td>
      <td>
        <s:textarea name="particulars" cols="50" rows="2" value="Refund of membership credit" />
      </td>
    </tr>
    <tr>
      <td>Approving User</td>
      <td>
        <s:select name="approvingUserId" list="approvingUserList" listKey="userID" listValue="username" headerKey="" headerValue="Please select" />
      </td>
    </tr>      
    <tr>
      <td colspan="2">Action:</td> 
    </tr>
    <tr>
      <td>
        <input type="radio" checked="checked" name="disposalType" value="<%=FinanceVoucher.TYPE_CHEQUE_MEMBER%>"  onclick="setOption(this)">
      </td>
     <td>Cheque to member</td>
    </tr>
    <tr>
      <td>
        <input type="radio" name="disposalType" value="<%=FinanceVoucher.TYPE_WRITE_OFF%>" onclick="setOption(this)">
      </td>
      <td>Write off</td>
    </tr>
    <tr>
      <td>
        <input type="radio" name="disposalType" value="<%=FinanceVoucher.TYPE_TRANSFER_TO_MEMBER%>" onclick="setOption(this)">
      </td>
      <td>Transfer to member</td>
    </tr>    
  </table>
  
  <br/>

  <table id="member" style="">
  <colgroup>
    <col style="width: 120px" />
    <col />
  </colgroup>
  <tbody>
    <tr>
      <td>
        <s:checkbox name="memberDetails" onclick="toggleMemberAddress(this);" />
      </td>
      <td>Use member's address details</td>
    </tr>
    <tr class="memberAddress">
      <td colspan="2">If you don't use member's address details the following address details will be used:</td>
    </tr>    
    <tr class="memberAddress">
      <td>Address 1</td>
      <td>
        <s:textfield name="memberAddress1" size="50" maxlength="100" />
      </td>
    </tr>
    <tr class="memberAddress">
      <td>Address 2</td>
      <td>
        <s:textfield name="memberAddress2" size="50" maxlength="100" />
      </td>
    </tr>
    <tr class="memberAddress">
      <td>State</td>
      <td>
        <s:textfield name="memberState" value="TAS" size="6" maxlength="10" />
      </td>
    </tr>
    <tr class="memberAddress">
      <td>Postcode</td>
      <td>
        <s:textfield name="memberPostcode" size="6" maxlength="10" />
      </td>
    </tr>
    </tbody>
  </table>
    
  <table id="glData" style="display:none">
    <tr>
      <td>Cost Centre</td>
      <td>
        <s:textfield name="costCentre" size="12" maxlength="10" onKeyUp = "checkInteger(this)" />
      </td>
    </tr>
    <tr>
      <td>Account Number</td>
      <td>
        <s:textfield name="writeoffAccountNumber" size="12" maxlength="9" onKeyUp = "checkInteger(this)" />
      </td>
    </tr>
  </table>  
  
  <table id="tfrData" style="display:none">
    <tr>
      <td>Membership Number:</td>
      <td>
        <s:textfield name="tfrMembershipNumber" size="12" maxlength="10" onKeyUp = "checkInteger(this)" />
      </td>
    </tr>
  </table>
  
  <br/>

  <span class="dataValueRed"><s:actionerror/></span>

  <s:hidden name="event" />

  <s:hidden name="membershipId" />
  <s:hidden name="disposeType" />
  <s:hidden name="disposalMode" />
 
  <s:submit />
  
</s:form>

<script type="text/javascript">

	if (dojo.byId("member").style.display != 'none') {
		toggleMemberAddress(dojo.byId("createVoucher_memberDetails"));
	}

</script>

</body>
</html>
