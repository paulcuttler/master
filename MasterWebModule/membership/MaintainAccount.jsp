<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.common.ui.*"%>
<%@ page import="com.ract.common.admin.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>

<%
String saveEvent     = "maintainAccount_btnSave";
String deleteEvent   = "maintainAccount_btnDelete";
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;

MembershipAccountVO accountVO = (MembershipAccountVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_ACCOUNT_VO);
String    origCode  = (String) request.getSession().getAttribute("origCode");
String    statMsg   = (String) request.getAttribute("statusMessage");
String    refresh   = (String) request.getAttribute("Refresh");

Integer accountID = null;
String accountTitle = "";
String accountNumber = "";
boolean isGstAccount = false;
boolean isClearingAccount = false;
String costCentre = "";
String subAccount = "";

if (!(accountVO==null))
{
  accountID          = accountVO.getAccountID();
  Account account    = accountVO;
  accountTitle       = accountVO.getAccountTitle();
  accountNumber      = account.getAccountNumber();
  isGstAccount       = accountVO.isGstAccount();
  isClearingAccount  = accountVO.isClearingAccount();
  costCentre         = account.getCostCenter();
  subAccount         = account.getSubAccountNumber();
}
else accountID = null;

%>

<head>
<title>Maintain Account</title>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/ButtonRollover.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body class="rightFrame" onload="refreshList(document.all.refresh,'<%=listPage%>');">

<form name="mainForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>">
  <input type="hidden" name="event" value="">
  <input type="hidden" name="accountId" value="<%=accountID%>">
  <input type="hidden" name="listCode" value ="">
  <input type="hidden" name="refresh" value ="<%=refresh%>">

 <div id="mainpage" class="pg">
 <h1>Maintain Account</h1>
   <div id="contentpage" class="cnt">
<!--
   START ACCOUNT BODY
-->
   <table border="0" cellspacing="0" cellpadding="0">
   <%
   if (accountID != null)
   {
   %>
     <tr>
       <td class="label">Account ID:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
             <%=accountID%>
       </td>
     </tr>
     <%
     }
      %>
     <tr>
       <td class="label">Account title:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue" >
         <input type="text" size="55" maxlength ="40" name="accountTitle" value="<%=accountTitle==null?"":accountTitle%>">
       </td>
     </tr>
     <tr>
       <td class="label">Account number:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
         <input type="text" size="55" maxlength ="40" name="accountNumber" value="<%=accountNumber==null?"":accountNumber%>">
       </td>
     </tr>
     <tr>
       <td class="label">GST:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
         <input type="checkbox" name="isGstAccount" value="true" <%=isGstAccount?"checked":""%>>
       </td>
     </tr>
     <tr>
       <td class="label">Clearing account:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
         <input type="checkbox" name="isClearingAccount" value="true" <%=isClearingAccount?"checked":""%>>
       </td>
     </tr>
     <tr>
       <td class="label">Cost centre:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
         <input type="text" size="55" maxlength ="40" name="costCentre" value="<%=costCentre==null?"":costCentre%>">
       </td>
     </tr>
     <tr>
       <td class="label">Sub account:</td>
       <td class="dataValue">&nbsp;</td>
       <td class="dataValue">
         <input type="text" size="55" maxlength ="40" name="subAccount" value="<%=subAccount==null?"":subAccount%>">
       </td>
     </tr>
   </table>
<!--
   FINISH ACCOUNT BODY
-->
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonSaveAndDelete(saveEvent,deleteEvent,"Account", statMsg )%>
   </div>
</div>
</form>
</body>
</html>
