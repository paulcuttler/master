<%@ page import="java.util.*"%>

<%@ page import="java.text.NumberFormat"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.club.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<%
String next_event = "useFastTrack_next";
String remove_event = "useFastTrack_remove";
String transGroupKey = (String) request.getAttribute("transGroupKey");
String pfId = (String) request.getAttribute("pendingFeeId");
String memId = (String) request.getAttribute("memId");
String docDetails = (String) request.getAttribute("docDetails");

%>

<html>
<head>
<title>Use Fast Track</title>
<%=CommonConstants.getRACTStylesheet()%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/Validation.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/PageMovement.js") %>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS + "/ButtonRollover.js") %>
</head>

<body>

<form name="mainForm" method="post" action="<%= MembershipUIConstants.PAGE_MembershipUIC%>">

<input type="hidden" name="event" value="">
<input type="hidden" name="listCode" value="">
<input type="hidden" name="transGroupKey" value="<%=transGroupKey%>">
<input type="hidden" name="pendingFeeId" value="<%=pfId%>">
<input type="hidden" name="membershipID" value="<%=memId%>">

 <div id="mainpage" class="pg">
 <h1><%@ include file="/help/HelpLinkInclude.jsp"%> </h1>
   <div id="contentpage" class="cnt">
	<table height="70%" align="center">
	  <tr height="25%">
            <td valign="center" align="center" class="dataValueBlue" style="font-size:15px">
		This membership has a current pending fee.
		<br>
		<br>
		Please use the Fast Track option to renew it
            </td>
          </tr>
	  <tr height="25%">
            <td valign="center" align="center" style="font-size:15px">
            	<br>
		<span style="font-size:15px"><%=docDetails%></span>
           </td>
          </tr>

	  <tr height="25%">
          <td valign="center" align="center" class="dataValueBlue" style="font-size:15px">
            If you wish to disregard the current pending fee and make changes
            to the renewal click NEXT
          </td>
          </tr>
	  <tr height="25%">
            <td valign="center" align="center" class="dataValueBlue" style="font-size:15px">
		<span class="dataValueRed" style="font-size:15px">Warning: </span> If you click NEXT the
		changes made to the renewal will be based on the new fees effective from the 3<sup>rd</sup> October 2003
            </td>
          </tr>
	</table>
   </div>
   <div id="buttonPage" class="btn">
        <%=HTMLUtil.buttonBack("historyBack()","Membership")%>
	<%=HTMLUtil.buttonNext(next_event,"Continue Renewal")%>
   </div>
</div>
</form>
</body>
</html>
