<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.util.*"%>

<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="java.rmi.RemoteException"%>

<%@ include file="/membership/TransactionInclude.jsp"%>

<%
// This will be set to true if the transaction has just been saved as a quote.
boolean quoteSaved = "Yes".equalsIgnoreCase((String) request.getAttribute("quoteSaved"));
Integer quoteNumber = (Integer)request.getAttribute("quoteNumber");

String transTypeCode = transGroup.getTransactionGroupTypeCode();
//double ddOutstandingTotal = 0;
//double creditsTotal=0;
final String errorMessage = "The transaction is not valid. Please start again.";
ProductVO productVO = transGroup.getSelectedProduct();

DateTime expiryDate = transGroup.getExpiryDate();
GregorianCalendar nextYear = new GregorianCalendar();
nextYear.add(GregorianCalendar.YEAR, 1);
GregorianCalendar expiry = new GregorianCalendar();
expiry.setTime(expiryDate);
//does the membership term exceed one year?
boolean membershipTermExceedsOneYear =  expiry.after(nextYear);

if (productVO == null)
{
   throw new SystemException(errorMessage, new Exception("The transaction group contains a null product."));
}

Collection transactionList = transGroup.getTransactionList();
if (transactionList == null || transactionList.isEmpty())
{
   throw new SystemException(errorMessage, new Exception("The transaction list is null or empty."));
}

/*get outstanding dd's and credits if there are any */
MembershipVO memVO = null;
MembershipVO newMemVO = null;
MembershipTransactionVO memTransVO=null;
Iterator transactionListIT = transactionList.iterator();
String oldProductCode = "";
while (transactionListIT.hasNext())
{
   memTransVO = (MembershipTransactionVO) transactionListIT.next();

   // If a new membership has not been set then the membership is part of a
   // membership group and it is not being changed.  Use the current membership instead.
   newMemVO = memTransVO.getNewMembership();
   memVO = memTransVO.getMembership();
   if (memVO != null) {
   		oldProductCode = memVO.getProductCode();
   }
   
   if (newMemVO == null)
   {
      newMemVO = memTransVO.getMembership();
   }
}

int memberCount = transactionList.size();
DateTime productDate = null;
double netTransactionFee = transGroup.getNetTransactionFee();
double gstFee = transGroup.getGstAmount();
double totalFee = transGroup.getAmountPayable();
%>
<html>
<head>
<title>Confirm transaction</title>
<%=CommonConstants.getRACTStylesheet()%>
<%--<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>--%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/Utilities.js">
</script>
<%=CommonConstants.getSortableStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_DOJO_0_4_1%>/dojo.js"></script>
<script type="text/javascript">
   dojo.require("dojo.widget.*");
</script>

<script language="JavaScript">

   function onLoadEventHandler()
   {
      if (<%=quoteSaved%>)
      {
         var printQuote = confirm('Print the quote now?');
         if (printQuote)
         {
            printMembershipQuote();
         }
         parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber=<%=transGroup.getContextClient().getClientNumber()%>';
      }
      resetMemberTable();
   }

  function resetMemberTable()
  {
    var myTable = dojo.widget.byId("memberTable");
    var rows = myTable.domNode.tBodies[0].rows;
    for(var i=0; i<rows.length; i++)
    {
      var row = rows[i];
      //workaround valign bug in filteringTable
      row.vAlign = "top";
    }
  }

   var leftIndent = 20;    // Left indent of div
   var topIndent = 50;    // Top space for div
   var leftInc = 20;       // Amount to incrament the left indent by for each div
   var topInc = 20;        // amount it incrament the top indent by for each div
   var zIndex = 9;

   function showFeeDetail(ref)
   {
      var styleRef = 'document.all.' + 'feeDiv_' + ref + '.style.';
      var expression = styleRef + 'top = "50px"';
      eval(expression);
      var expression = styleRef + 'left = "20px"';
      eval(expression);
      var expression = styleRef + 'display = ""';
      eval(expression);
   }

   function hideFeeDetail(ref)
   {
      var styleRef = 'document.all.' + 'feeDiv_' + ref + '.style.';
      var expression = styleRef + 'display = "none"';
      eval(expression);
   }


   /***************************  div drag functions  *************************/
   var dragapproved=false
   var z,x,y

   function move	()
   {
      if ( event.button==1 && dragapproved )
      {
         z.style.pixelLeft=temp1+event.clientX-x;
         z.style.pixelTop=temp2+event.clientY-y;
         return false;
      }
   }


   function drags()
   {
      // Don't do drag if the browser is not IE.
      if (!document.all)  return;

//      window.status = "event.srcElement.className = "+event.srcElement.className;

      if ( event.srcElement.className == "feeDiv" )
      {
         dragapproved=true;
         z=event.srcElement;
         temp1=z.style.pixelLeft;
         temp2=z.style.pixelTop;
         x=event.clientX;
         y=event.clientY;
         document.onmousemove=move;
      }
      else
      {
         return true;
      }
   }


   document.onmousedown=drags
   document.onmouseup=new Function("dragapproved=false")


   /*********************  Print quote  *****************************/
   function printMembershipQuote()
   {
      var url = "<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>?event=printMembershipQuote&quoteNumber=<%=quoteNumber%>";
      window.open(url, '', 'width=250,height=150,toolbar=no,status=no,scrollbars=no,resizable=no')
   }

   function calculateExpiryDate(effectiveDate)
   {
	  if (document.all.newExpiryDate.value != 'true') {
		  var expDate = addMonthsToDate(effectiveDate, 12);
	      var mon = '00' + (expDate.getMonth() + 1);
	      document.all.expiryDate.value   = expDate.getDate() + '/' + mon.substr(mon.length - 2) + '/' + expDate.getFullYear();
	  }
      document.all.newExpiryDate.value = "true";
   }

   function convertDate(fld)
   {
      var aDate;
      var dateElements = fld.value.split('/');

      var aDate = new Date(dateElements[2], dateElements[1] - 1, dateElements[0]);
      aDate.setHours(0);
      aDate.setMinutes(0);
      aDate.setSeconds(0);
      aDate.setMilliseconds(0);
      return aDate;
   }
</script>

<style type="text/css">
<!--

.feeDiv {position: absolute;
         cursor: hand;
         border-width: 2;
         border-style: solid;
         border-color: #8B0000;
         background-color: #003366;
        }
-->
</style>

</head>
<body onLoad="return onLoadEventHandler();">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <form name="confirmForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
    <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
    <input type="hidden" name="event" value="confirmTransaction_unknown">
    <input type="hidden" name="newExpiryDate" value="">
    <tr valign="top">
      <td>
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Confirm transaction</h1>
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
           <td valign="top">
             <table border="0" cellpadding="3" cellspacing="0">
             <tr>
               <td>

              <h3>Membership group details:</h3>
              <table border="0" cellspacing="0" cellpadding="3" style="margin-left:10px">
              <tr>
                 <td class="label">Expiry date:</td>
                 <%
                 String transactionTypeCode = transGroup.getTransactionGroupTypeCode();
                 if (membershipTermExceedsOneYear)
                 {
            	 	if (transactionTypeCode != null && transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT) && oldProductCode.equals(ProductVO.PRODUCT_NON_MOTORING) && productVO.getProductCode().equals(ProductVO.PRODUCT_LIFESTYLE)) {
                 %>
                 <td class="highlightRow">
                 	<a title="Membership term exceeds one year from today!">
		             	<input type="text"
		                     name="expiryDate"
		                     size="15"
		                     value="<%=expiryDate.formatShortDate()%>"
		                     style="font-weight: normal;">
                 	</a>
                 	<script type="text/javascript">
                 		document.all.newExpiryDate.value = 'true';
                 	</script>
                 </td>
                 <%
            	 	} else {
                 %>
                 <td class="highlightRow">
                 	<a title="Membership term exceeds one year from today!&nbsp;"><%=expiryDate.formatShortDate()%></a>
                 </td>
                 <%
            	 	}
                 }
                 else
                 {
             	 	if (transactionTypeCode != null && transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT) && oldProductCode.equals(ProductVO.PRODUCT_NON_MOTORING) && productVO.getProductCode().equals(ProductVO.PRODUCT_LIFESTYLE)) {
                 %>
	             <td>
	             	<input type="text"
	                     name="expiryDate"
	                     size="15"
	                     value="<%=expiryDate.formatShortDate()%>"
	                     style="font-weight: normal;">
                 	<script type="text/javascript">
                 		document.all.newExpiryDate.value = 'true';
                 	</script>
 	             </td>
 	             <%
             	 	} else {
 	             %>
                 <td class="dataValue"><%=expiryDate.formatShortDate()%></td>
                 <%
             	 	}
                 }
                 %>
                 <td colspan="2"/>
              </tr>
              <tr>
                 <td class="label">Product:</td>
                 <td class="dataValue"><%=productVO.getProductCode()%></td>
<%
// Show the product effective date here if there is only one membership
// in the transaction.  If there are two or more then it is shown for each
// membership as it may be different.
MembershipTransactionVO paMemTransVO = transGroup.getMembershipTransactionForPA();
if (memberCount < 2 && paMemTransVO != null)
{
   productDate = paMemTransVO.getNewMembership().getProductEffectiveDate();

	if (transactionTypeCode != null && transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT) && oldProductCode.equals(ProductVO.PRODUCT_NON_MOTORING) && productVO.getProductCode().equals(ProductVO.PRODUCT_LIFESTYLE)) {

%>

	            <td class="label">Effective:</td>
	            <td>
	              <input type="text"
	                     name="effectiveDate"
	                     size="15"
	                     value="<%=(productDate == null ? "??????" : productDate.formatShortDate())%>"
	                     style="font-weight: normal;">
	            </td>
<%
   } else {
%>	         
	 
                 <td class="label">Effective:</td>
                 <td class="dataValue"><%=(productDate == null ? "??????" : productDate.formatShortDate())%></td>
<%
   }
}
else
{
%>
                 <td></td>
                 <td></td>
<%
}
%>
              </tr>
<%
// Display the rejoin type
if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode))
{
%>
              <tr>
                 <td class="label" valign="top">Rejoin type:</td>
                 <td class="dataValue" colspan="3" valign="top"><%=transGroup.getRejoinType().getDescription()%></td>
              </tr>
<%
}

String reasonLabel = null;
if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode))
{
   reasonLabel = "Join reason:";
}
else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode))
{
   reasonLabel = "Rejoin reason:";
}

// Display the reason for join / rejoin
if (reasonLabel != null)
{
   ReferenceDataVO transReason = transGroup.getTransactionReason();
   String reasonDescription = (transReason != null ? transReason.getDescription() : null);
   if (reasonDescription == null) reasonDescription = "";
%>
              <tr>
                 <td class="label" valign="top"><%=reasonLabel%></td>
                 <td class="dataValue" colspan="3" valign="top"><%=reasonDescription%></td>
              </tr>
<%
}
%>
              <tr>
                 <td class="label">Profile:</td>
                 <td class="dataValue" colspan="3"><%=(transGroup.getMembershipProfile()==null?"":transGroup.getMembershipProfile().getProfileCode())%></td>
              </tr>
              </table>
              </td>
              </tr>
              </table>
           </td>
           <td width="10">&nbsp;</td>
           <td valign="top">
             <!--  Transaction fee table -->
             <table border="1" bordercolor="#CC0000" cellpadding="3" cellspacing="0">
             <tr>
               <td>
               <h3>Transaction fee:<%=(transGroup.getAttachedQuote() != null ? "&nbsp;-&nbsp;Adjusted for quote " + transGroup.getAttachedQuote().getQuoteNumber() + "&nbsp;:&nbsp;" + CurrencyUtil.formatDollarValue(transGroup.getAttachedQuote().getAmountPayable()) : "")%></h3>
               <table border="0" cellspacing="0" cellpadding="3" style="margin-left:10px">
                <tr>
                  <td class="label" align="left" valign="top">Total net fee:</td>
                  <td class="dataValue" align="right" valign="top"><%=NumberUtil.formatValue(netTransactionFee)%></td>
                  <td></td>
                </tr>
<%
ArrayList adjustmentList = transGroup.getAdjustmentList();
if (adjustmentList != null && !adjustmentList.isEmpty())
{
   MembershipTransactionAdjustment memTransAdjustment;
   String reason;
   for (int i = 0; i < adjustmentList.size(); i++)
   {
      memTransAdjustment = (MembershipTransactionAdjustment) adjustmentList.get(i);
%>
                <tr>
                  <td class="label" align="left" valign="bottom"><%=memTransAdjustment.getAdjustmentTypeCode()%>:</td>
                  <td class="dataValue" align="right" valign="bottom"><%=NumberUtil.formatValue(memTransAdjustment.getSignedAdjustmentAmount())%></td>
                  <td class="helpTextSmall" align="left" valign="bottom">
<%
      reason = memTransAdjustment.getAdjustmentReason();
      if (reason != null && reason.length() > 0)
      {
         out.print("(");
         out.print(reason);
         out.print(")");
      }
%>
                  </td>
                </tr>
<%
   }
}
%>
                <tr>
                  <td colspan="3"><hr size="1"></td></tr>
                <tr>
                  <td class="label" align="left" valign="top">Amount payable:</td>
                  <td class="dataValue" align="right" valign="top"><%=CurrencyUtil.formatDollarValue(totalFee)%></td>
                  <td class="dataValue" valign="top">(inc. <%=CurrencyUtil.formatDollarValue(gstFee)%> gst)</td>
                </tr>

               </table>
               </td>
             </tr>
             </table>
           </td>
        </tr>
        </table>

<%
// Display the list of member details
memTransVO = null;
newMemVO = null;
memVO = null;
ClientVO clientVO = null;
AddressVO addressVO = null;
String addressText = null;
String addressee = null;
int memYears = 0;
DateTime joinDate = null;
DateTime commenceDate = null;
String yearsText = null;
Collection optionList = null;
Iterator optionListIT = null;
MembershipProductOption option = null;
Collection discountList = null;
Iterator discountListIT = null;
MembershipDiscount memDiscountVO = null;
String membersText = (memberCount == 1 ? "member" : "members");
//int memListDivHeight = 200;
%>
        <h3>Member details: <%=memberCount%> <%=membersText%></h3>

<%-- <table border="0" cellspacing="0" cellpadding="3" style="margin-left: 10px;"> --%>

<div id="memberDetails" style="height:200; overflow:auto; border-width: 1; border-style: solid;">

  <table  dojoType="filteringTable"
            id="memberTable"
            cellpadding="2"
            alternateRows="true"
            class="dataTable">
  <thead>
    <tr>
      <th field="clientNumber" dataType="Number" align="left" valign="top">Client</th><th field="desc" dataType="String" align="left" valign="top">Name</th><th field="trans" dataType="String" align="left" valign="top">Transaction</th>
    </tr>
  </thead>
  <tbody>
<%
transactionListIT = transactionList.iterator();
int counter = 0;
while (transactionListIT.hasNext())
{
   counter++;
   memTransVO = (MembershipTransactionVO) transactionListIT.next();

   // If a new membership has not been set then the membership is part of a
   // membership group and it is not being changed.  Use the current membership instead.
   newMemVO = memTransVO.getNewMembership();
   memVO = memTransVO.getMembership();
   if (newMemVO == null)
   {
      newMemVO = memTransVO.getMembership();
   }

   clientVO = newMemVO.getClient();
   addressee = (memTransVO.isPrimeAddressee() ? "<span class=\"helpTextSmall\"> (Addressee)</span>" : "");
   addressVO = clientVO.getPostalAddress();
   addressText = (addressVO == null ? null : addressVO.getSingleLineAddress());
   joinDate = newMemVO.getJoinDate();
   commenceDate = newMemVO.getCommenceDate();
   productDate = newMemVO.getProductEffectiveDate();

//start of row
%>
  <tr value="<%=counter%>">

    <td class="dataValue" colspan="4"><%=clientVO.getClientNumber()%></td>
    <td>
      <!--<%=clientVO.getSortableDisplayName()%>-->
      <table>
<%
   // Show the address as a tool time when the mouse hovers over the client name
   if (addressText != null)
   {
%>
         <tr>
            <td class="dataValue" colspan="4"><a href="#" class="actionItemBlack" title="<%=addressText%>"><%=clientVO.getSortableDisplayName()%>.</a> <%=addressee%></td>
         </tr>
<%
   }
    else
   {
%>
         <tr>
            <td class="dataValue" colspan="4"><%=clientVO.getSortableDisplayName()%>. <%=addressee%></td>
         </tr>
<%
   }

   // If there is more than one membership transaction then we show the product
   // effective date on a per member basis as it may be different for each member.
   if (memberCount > 1)
   {
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label">Product effective:</td>
            <td class="dataValue"><%=(productDate == null ? "??????" : productDate.formatShortDate())%></td>
            <td class="label"><%=(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(memTransVO.getTransactionTypeCode()) ? "(changed from " + memTransVO.getMembership().getProductCode() + ")" : "")%></td>
          </tr>
<%
   }
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label">Join date:</td>
            <td class="dataValue"><%=(joinDate == null ? "??????" : joinDate.formatShortDate())%></td>
            <td class="dataValue"><%=newMemVO.getMembershipYears().formatForDisplay(Interval.FORMAT_YEAR,Interval.FORMAT_YEAR)%></td>
          </tr>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label">Commence date:</td>
            <td class="dataValue"><%=(commenceDate == null ? "??????" : commenceDate.formatShortDate())%></td>
            <td></td>
          </tr>
<%
   //if a remove from group and the expiry date is different to the group's expiry date then display the expiry date.
   if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransVO.getTransactionTypeCode()) &&
       !newMemVO.getExpiryDate().equals(expiryDate))
   {
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label">Expiry date:</td>
            <td class="dataValue"><%=newMemVO.getExpiryDate()%></td>
            <td><%--<%=newMemVO.getRemainingTerm().formatForDisplay(Interval.FORMAT_DAY,Interval.FORMAT_DAY)%>--%></td>
          </tr>
<%
   }

   // Display product options selected for the member.
   optionList = newMemVO.getProductOptionList();
   if (optionList == null || optionList.isEmpty())
   {
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label" valign="top">Product options:</td>
            <td></td>
            <td></td>
          </tr>
<%
   }
   else
   {
      boolean firstOption = true;
      optionListIT = optionList.iterator();
      while (optionListIT.hasNext())
      {
         option = (MembershipProductOption) optionListIT.next();
%>

          <tr>
            <td width="15">&nbsp;</td>
            <td class="label" valign="top"><%if (firstOption){ out.print("Product options:"); firstOption = false;}%></td>
            <td class="dataValue"><%=option.getProductBenefitTypeCode()%></td>
            <td class="dataValue"><span class="label">Effective: </span><%=(option.getEffectiveDate() == null ? "" : option.getEffectiveDate().formatShortDate())%></td>
          </tr>
<%
      }
   }

   // Display recurring discounts for the member.
   discountList = newMemVO.getDiscountList();

   if (discountList == null || discountList.isEmpty())
   {
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label" valign="top">Recurring discounts:</td>
            <td></td>
            <td></td>
          </tr>
<%
   }
   else
   {
      boolean firstDisc = true;
      discountListIT = discountList.iterator();
      while (discountListIT.hasNext())
      {
         memDiscountVO = (MembershipDiscount) discountListIT.next();
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label" valign="top">
            <% if(firstDisc)
            {
            %>
            Recurring discounts
            <%
            }
            else
            {
              firstDisc=false;
            }
            %></td>
            <td class="dataValue" colspan="2"><%=memDiscountVO.getDiscountTypeCode()%></td>
          </tr>
<%
      }
   }

   // Display affilliated motor club details if we are doing a transfer
   // and an affiliated motor club has been selected.
   if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(transTypeCode)
   && newMemVO.getAffiliatedClubCode() != null)
   {
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label">Affiliated motor club:</td>
            <td class="dataValue" colspan="2"><%=newMemVO.getAffiliatedClubCode()%></td>
          </tr>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label">Affiliated club number:</td>
            <td class="dataValue" colspan="2"><%=(newMemVO.getAffiliatedClubId() == null ? "" : newMemVO.getAffiliatedClubId())%></td>
          </tr>
          <tr>
            <td width="15">&nbsp;</td>
            <td class="label">Affiliated club expiry:</td>
            <td class="dataValue" colspan="2"><%=(newMemVO.getAffiliatedClubExpiryDate() == null ? "" : newMemVO.getAffiliatedClubExpiryDate().formatShortDate())%></td>
          </tr>
<%
   }

//double creditAmount = newMemVO.getCreditAmount();
//double ddOutstanding = 0;
//if(memVO!=null) ddOutstanding = memVO.getDDAmountOutstanding();
//if(creditAmount > ddOutstanding) {
//   creditAmount -= ddOutstanding;
//   newMemVO.setCreditAmount(creditAmount);
//}
//else{
//   creditAmount = 0;
//}
//if (creditAmount > 0.0)
//{
//}
%>
          <tr>
            <td width="15">&nbsp;</td>
            <td><a class="label" title="Membership value = <%=memTransVO.getNewMembership().getMembershipValue(true).setScale(2,BigDecimal.ROUND_HALF_UP)%> (inc gst)">Member fee:</a></td>
<%
   // Don't show the fee popup if the membership is being removed from the membership group.
   if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransVO.getTransactionTypeCode()))
   {
%>
            <td class="dataValue">
               <a href="#" class="actionItem"
                  title="Show fee details for <%=memTransVO.getTransactionTypeCode()%> transaction."
                  onclick="showFeeDetail(<%=newMemVO.getClientNumber()%>)"
               >
                  <%=CurrencyUtil.formatDollarValue(memTransVO.getNetTransactionFee())%>
               </a>
            </td>
<% } else {  %>
            <td class="dataValue">
                  <%=CurrencyUtil.formatDollarValue(memTransVO.getNetTransactionFee())%>
            </td>
<% } %>
            <td class="helpTextSmall">(discretionary discount not included)</td>
          </tr>
    </table>
    </td>
    <td><%=MembershipUIHelper.getTransactionHeadingText(memTransVO.getTransactionTypeCode())%></td>
  </tr>
<%
}  // while (transactionListIT.hasNext())
%>
    <tbody>
        </table>
        </div>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
        <!-- button bar -->
        <table border="0" cellpadding="5" cellspacing="0">
          <tr>
            <td>
               <%--<a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">--%>
                <input type="button" value="Back" onclick="history.back(); return true;"/>
               </a>
            </td>
<% if (totalFee  > 0) { %>
            <td>
              <%--
               <a onclick="document.all.event.value='confirmTransaction_btnNext'; document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('NextButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
                  <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/NextButton.gif" border="0" alt="Show the next page">
               </a>
               --%>
                <input type="button" value="Next" onclick="document.all.event.value='confirmTransaction_btnNext'; document.forms[0].submit();"/>
            </td>
<%} else { %>
            <td>
              <%--
               <a onclick="document.all.event.value='confirmTransaction_btnSave'; document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Save the transaction');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/SaveButton_over.gif',1);return document.MM_returnValue" >
                  <img name="SaveButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Save the transaction">
               </a>
            --%>
           <input type="button" value="Save" onclick="document.all.event.value='confirmTransaction_btnSave'; document.forms[0].submit();">
            </td>
<% } 
	if (!transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT)) {
%>
            <td>
              <%--
               <a onclick="document.all.event.value='confirmTransaction_btnChange'; document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Change the transaction details');MM_swapImage('ChangeDetailsButton','','<%=CommonConstants.DIR_IMAGES%>/ChangeDetailsButton_over.gif',1);return document.MM_returnValue" >
                  <img name="ChangeDetailsButton" src="<%=CommonConstants.DIR_IMAGES%>/ChangeDetailsButton.gif" border="0" alt="Change the transaction details">
               </a>--%>
               
               <input type="button" value="Change Details" onclick="document.all.event.value='confirmTransaction_btnChange'; document.forms[0].submit();">
            </td>
<%
	}
%>
            <td>
               <input type="button" value="Save as quote" onclick="document.all.event.value='confirmTransaction_btnSaveAsQuote'; document.forms[0].submit();">
<%--
               <a onclick="document.all.event.value='confirmTransaction_btnSaveAsQuote'; document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Save as a quote');MM_swapImage('saveAsQuoteButton','','<%=CommonConstants.DIR_IMAGES%>/PrintQuoteButton_over.gif',1);return document.MM_returnValue" >
                  <img name="saveAsQuoteButton" src="<%=CommonConstants.DIR_IMAGES%>/PrintQuoteButton.gif" border="0" alt="Save as a quote">
--%>
               </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </form>
  </table>
<%




/******************************************************************************
   Set up the hidden divs for displaying popup transaction fee details
******************************************************************************/
Collection memTransFeeList = null;
Iterator memTransFeeListIT = null;
MembershipTransactionFee memTransFee = null;
Collection memTransDiscList = null;
Iterator memTransDiscListIT = null;
MembershipTransactionDiscount memTransDisc = null;
double optionFee = 0.0;
String divKey = null;
// Create a hidden div for each membership transaction in the transaction list.
transactionListIT = transactionList.iterator();
int txCounter = 0;
while (transactionListIT.hasNext())
{
   txCounter++;
   memTransVO = (MembershipTransactionVO) transactionListIT.next();
   memVO = memTransVO.getMembership();
   newMemVO = memTransVO.getNewMembership();
//   if (newMemVO == null)
//      newMemVO = memTransVO.getMembership();

   clientVO = newMemVO.getClient();
%>

          <div id="feeDiv_<%=newMemVO.getClientNumber()%>" class="feeDiv" style="display: none;">
          <span style="color: white"><%=MembershipUIHelper.getTransactionHeadingText(memTransVO.getTransactionTypeCode())%></span>
          <table border="0" cellpadding="10" cellspacing="0" style="background-color: lightyellow;">
            <tr>
              <td>
                <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                  <td class="dataValue" align="left" colspan="5">
                     <%=clientVO.getClientNumber()%> - <%=clientVO.getDisplayName()%>
                     <hr>
                  </td>
                </tr>
                <tr>
                  <td align="left" colspan="5">
                     <span class="label">Product: </span>
                     <span class="dataValue"><%=newMemVO.getProductCode()%></span>
                  </td>
                </tr>
<%
   String discountDescription;
   double grossFee = 0.0;
   memTransFeeList = memTransVO.getTransactionFeeListForProduct();
   MembershipTransactionDiscount unearnedDiscount = null;
   if (memTransFeeList != null && !memTransFeeList.isEmpty())
   {
      // List the fees applied to the base membership product.
      int feeCounter = 0;
      memTransFeeListIT = memTransFeeList.iterator();
      while (memTransFeeListIT.hasNext())
      {
         feeCounter++;
         memTransFee = (MembershipTransactionFee) memTransFeeListIT.next();
         grossFee = memTransFee.getGrossFee();
         if (grossFee > 0.0)
         {
%>
                <tr>
                  <td width="5"></td>
                  <td class="label" align="left" colspan="2"><%=memTransFee.getFeeSpecificationVO().getFeeTypeCode()%></td>
                  <td class="dataValue" align="right">
                     <a href="#" title="<%=memTransFee.getFeeDescription()%>">
                        <%=NumberUtil.formatValue(grossFee)%>
                     </a>
                  </td>
                  <td></td>
                </tr>
<%
            // List the non-discretionary discounts applied to the membership transaction fee.
            double discountAmount = 0.0;
            memTransDiscList = memTransFee.getDiscountList();
            if (memTransDiscList != null)
            {
               int discCounter = 0;
               memTransDiscListIT = memTransDiscList.iterator();
               while (memTransDiscListIT.hasNext())
               {
                  discCounter++;
                  divKey = "t"+txCounter+""+feeCounter+""+discCounter;
                  memTransDisc = (MembershipTransactionDiscount) memTransDiscListIT.next();
                  discountAmount = memTransDisc.getDiscountAmount();
                  discountDescription = memTransDisc.getDiscountDescription();
//LogUtil.log(this.getClass(),"memTransDisc = "+discountDescription);
                  if (discountDescription != null)
                  {
                    discountDescription = StringUtil.replaceAll(discountDescription,"|","<br/>");
                  }
                  else
                  {
                    discountDescription = "";
                  }
//LogUtil.log(this.getClass(),discountAmount+ ", "+memTransDisc.getDiscountCode()+" "+DiscountType.TYPE_UNEARNED_CREDIT);
                  //discountReason = memTransDisc.getDiscountReason();
                  if ((discountAmount != 0.0 &&
                       DiscountTypeVO.TYPE_UNEARNED_CREDIT.equals(memTransDisc.getDiscountCode())) ||
                       !DiscountTypeVO.TYPE_UNEARNED_CREDIT.equals(memTransDisc.getDiscountCode()))
                  {

%>
               <%-- Show the non-discretionary discount for the transaction fee --%>
  <div id="<%=divKey%>" class="tip"><%=discountDescription%></div>
                <tr>
                  <td width="5"></td>
                  <td width="5"></td>
                  <td class="label" align="left"><%=discountAmount < 0 ? "Amount Outstanding" :memTransDisc.getDiscountCode()%></td>
                  <td class="dataValue" align="right">
                     <a href="#" onmouseout="popUp(event,'<%=divKey%>')" onmouseover="popUp(event,'<%=divKey%>')" ><%=discountAmount < 0 ? NumberUtil.formatValue(-discountAmount):NumberUtil.formatValue(discountAmount)%> </a>
                  </td>
                  <td></td>
                </tr>
<%
                  }
               }  //  while (memTransDiscListIT.hasNext())
            }  //  if (memTransDiscList != null)

%>

               <%-- Show the total for the fee  --%>
                <tr>
                  <td width="5"></td>
                  <td width="5"></td>
                  <td></td>
                  <td></td>
                  <td class="dataValue" align="right"><%=NumberUtil.formatValue(memTransFee.getNetTransactionFee())%></td>
                </tr>
<%
         }  //  if (grossFee > 0.0)
      }  //  while (memTransFeeListIT.hasNext())
   }  //  if (memTransFeeList != null)
   else
   {
      // No fees for the product so display a zero fee amount.
%>
                <tr>
                  <td width="5"></td>
                  <td width="5"></td>
                  <td></td>
                  <td></td>
                  <td class="dataValue" align="right"><%=NumberUtil.formatValue(0.0)%></td>
                </tr>
<%
   }

   // List the product options selected and their fees.
   // If there is more than one fee, just show a total and include
   // any discounts given. Business rules are that discounts are not given for
   // options but the data structure allows for it and you never know what
   // might happen in the future.
   optionList = newMemVO.getProductOptionList();
   if (optionList != null && !optionList.isEmpty())
   {
      // Show the option heading.
%>
                <tr>
                  <td class="label" align="left" colspan="5">Product option:</td>
                </tr>
<%
      optionListIT = optionList.iterator();
      while (optionListIT.hasNext())
      {
         option = (MembershipProductOption) optionListIT.next();
         optionFee = 0.0;

         // Sum the fees that apply to the product option
         memTransFeeList = memTransVO.getTransactionFeeListForProductOption(option.getProductBenefitTypeCode());
         if (memTransFeeList != null)
         {
            memTransFeeListIT = memTransFeeList.iterator();
            while (memTransFeeListIT.hasNext())
            {
               memTransFee = (MembershipTransactionFee) memTransFeeListIT.next();
               optionFee += memTransFee.getGrossFee() - memTransFee.getTotalDiscount();   // Options don't get discounted but incase they do in the future account for any discounts given.
            }
         }

         if (optionFee != 0.0)
         {
%>
               <%-- Show product option and its fee  --%>
                <tr>
                  <td width="5"></td>
                  <td class="label" align"left" colspan="3"><%=option.getProductBenefitTypeCode()%></td>
                  <td class="dataValue" align="right"><%=NumberUtil.formatValue(optionFee)%></td>
                </tr>

<%
         }
      }   //  while (optionListIT.hasNext())
   }   // if (optionList != null)
%>
               <%-- show the net transaction fee --%>
                <tr>
                  <td class="label" align"left" colspan="4"><hr>Net transaction fee:</td>
                  <td class="dataValue" align="right"><hr><%=CurrencyUtil.formatDollarValue(memTransVO.getNetTransactionFee())%></td>
                </tr>
<%
   double creditAmount = memTransVO.getNewMembership().getCreditAmount();
   // Add TransactionCredit amounts
   // This is a partial implementation.  The only credit types in the list
   // will be 'Unearned value', which has already been factored into the credit
   // amount, and 'Quote Discounts' which have not.
   TransactionCredit quoteCredit = newMemVO.getTransactionCredit(TransactionCredit.CREDIT_QUOTE);
   if (quoteCredit != null) {
      creditAmount += quoteCredit.getTransactionCreditAmount().doubleValue();
   }
   if (creditAmount > 0.0) {
%>
                <tr>
                  <td class="label" align"left" colspan="4">Credit held over:</td>
                  <td class="dataValue" align="right"><%=CurrencyUtil.formatDollarValue(creditAmount)%></td>
                </tr>
<%
   }
%>
                <tr>
                  <td align"right" colspan="5">
                     <button onclick="hideFeeDetail(<%=newMemVO.getClientNumber()%>);">
                      Hide</button>
                        <%--<img name="HideButton" src="<%=CommonConstants.DIR_IMAGES%>/HideButton.gif" border="0" alt="Hide the fee window.">--%>
                     </a>
                  </td>
                </tr>
                </table>
              </td>
            </tr>
          </table>
          </div>
<%
}
%>
</body>
</html>
