<%@ page import="com.ract.common.CommonConstants"%>
<%@page import="com.ract.membership.ui.*"%>
<%--
Requests parameters for the MembershipRenewalStatisticsReport.jsp

request attributes:

returned parameters:
   fromDate
   toDate

   runDate
   runHours
   runMins

--%>
<script language="javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/InputControl.js">
</script>

<html>
<head>
<title>
MotorNewsExtract
</title>
<%=CommonConstants.getRACTStylesheet()%>
<%--<link rel="stylesheet" href="../css/ract.css" type="text/css">--%>
</head>
<body>
<h1>
Create Membership Renewal Statistics Report  THIS FORM IS OBSOLETE I THINK
</h1>
<form name = "MNForm" method="post" onsubmit="return validate(this)" action="<%=MembershipUIConstants.PAGE_MembershipReportsUIC%>">
  <table cellpadding="2" border="0" width="445">
    <tr>
      <td width="165">
        <div align="right">Collect data for the period:</div>
      </td>
      <td width="254">
        <input type="text" name="fromDate" size="11"  value = ""
		    onkeyup = "javascript:checkDate(this)">
      </td>
    </tr>
    <tr>
      <td width="165">
        <div align="right"> to:</div>
      </td>
      <td width="254">
        <input type="text" name="toDate" size="11" value = ""
		    onkeyup = "javascript:checkDate(this)">
      </td>
    </tr>
    <tr>
      <td width="165">
        <div align="right">Schedule to run on: </div>
      </td>
      <td width="254">
        <input type = "text" name = "runDate" size = "11" value = ""
		   onkeyup = "javascript:checkDate(this)">
        &nbsp at: &nbsp
        <input type = "text" name = "runHours" size = "2" value = "17"
             onkeyup = "javascript:checkInteger(this)"
             onblur = "javascript:checkHours(this)">
        :
        <input type = "text" name = "runMins" size = "2" value = "30"
             onkeyup = "javascript:checkInteger(this)"
             onblur = "javascript:checkMins(this)">
  </table>
 <input type="hidden" name="event" value="">
&nbsp&nbsp<input type="button" name="OK" value="OK"
            onclick="javascript:checkvalues()">
&nbsp&nbsp<input type="submit" name="cancel" value="Cancel"
            onclick="event.value='MembershipRenewalStatisticsReport_Cancel'">
<script language="javascript">
  ddate = new Date;
  dday = ddate.getDate();
  dmonth = ddate.getMonth() + 1;
  dyear = ddate.getYear();
  document.all.runDate.value= dday + "/" + dmonth + "/" + dyear;
  document.all.fromDate.focus();

  function checkValues(){
   alert("check values and submit");
    document.all.event.value='MembershipRenewalStatisticsReport_DoExtract';
    if(validate()){
       document.MNForm.submit();

    }
  }

  function checkHours(thing){
      val = thing.value * 1;
      if(val>23 || val<0){
         document.all.runHours.focus();
         alert("Hours must be from 0 to 23");
         return false;
      }
      else return true;
  }

  function checkMins(thing){
     val = thing.value * 1;
     if(val > 59 || val < 0){
        document.all.runMins.focus();
        alert("Minutes must be between 0 and 59");
        return false;
     }
     else return true;
  }
  function validate(){
     hours = document.all.runHours.value * 1;
     mins = document.all.runMins.value * 1;
     if(hours>23 || hours < 0 || mins<0 || mins>59){
        alert("Enter a valid date and time");
        return false;
     }
     else return true;
  }

</script>

</form>
</body>
</html>
