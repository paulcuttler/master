<%@ page import="com.ract.common.CommonConstants"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>

<%--
author: dgk

Renewal Batch display
--%>
<html>

<head>
<title>Process Completed</title>
<%=CommonConstants.getRACTStylesheet()%>

<%
RenewalBatchVO batch = (RenewalBatchVO)request.getAttribute("renewalBatch");
%>

<script language="Javascript">
function removeRenewalBatch()
{
  var runNumber = document.all.runNumber.value;
  document.all.event.value = 'btnRemoveRenewalBatch';
  var fRet;
  fRet = confirm('Are you sure that you want to remove batch run number '+runNumber+' including all associated documents, cards, payables and schedules?');
  if (fRet)
  {
    document.viewBatch.submit();
  }
}

function clearEmailSentFlag()
{
  var runNumber = document.all.runNumber.value;
  document.all.event.value = 'btnClearEmailSentFlag';
  var fRet;
  fRet = confirm('Are you sure that you want to clear the email sent flag for '+runNumber+'? This will allow you to email a batch again.');
  if (fRet)
  {
    document.viewBatch.submit();
  }
}

</script>

</head>

<body>

<form name="viewBatch" method="post" action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>">

<input type="hidden" name="runNumber" value="<%=batch.getRunNumber()%>"/>

<input type="hidden" name="event" value=""/>

<h2><%=request.getAttribute("heading")%></h2>
<table>
  <tr><td>Renewal Batch Number</td><td class="dataValue"><%=batch.getRunNumber()%></td></tr>
  <tr><td>Run By</td><td class="dataValue"><%=batch.getRunBy()%></td></tr>
  <tr><td>On</td><td class="dataValue"><%=batch.getRunDate()%></td></tr>
  <tr><td>For memberships expirying between</td><td class="dataValue"><%=batch.getFirstDate().toString() + " and " + batch.getLastDate().toString()%>  </td></tr>
  <tr><td>Notice count:</td><td class="dataValue"><%=batch.getNoticeCount()%></td></tr>
  <tr><td>Advice count:</td><td class="dataValue"><%=batch.getAdviceCount()%></td></tr>
  <tr><td>Reminder count:</td><td class="dataValue"><%=batch.getReminderCount()%></td></tr>
  <tr><td>Final Notices: </td><td class="dataValue"><%=batch.getFinalsCount()%></td></tr>
  <tr><td>Unfinancial Notices: </td><td class="dataValue"><%=batch.getUnfinancialsCount()%></td></tr>
  <tr><td colspan="2"><hr/></td></tr>
  <tr><td>Total Documents </td><td class="dataValue"><%=batch.getBatchCount()%>  </td></tr>
  <tr><td colspan="2"></td></tr>
  <tr><td colspan="2"><hr/></td></tr>
  <tr><td>Print/Email/Emails sent</td><td class="dataValue"><%=batch.getPrintCount()%>/<%=batch.getEmailCount()%>/<%=batch.getEmailSentCount()%></td></tr>


  <tr><td colspan="2">&nbsp;</td></tr>
  
  <tr><td><input type="button" name="btnRemoveRenewalBatch" value="Remove Renewal Batch" onclick="removeRenewalBatch()"/></td><td><input type="button" name="btnRemoveRenewalBatch" value="Clear Email Sent Flag" onclick="clearEmailSentFlag()"/></td></tr>
  
</table>


</form>

</body>

</html>
