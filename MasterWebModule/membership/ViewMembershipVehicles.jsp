<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>

<%
Iterator memVehicleIT = null;
//ArrayList memVehicle = null;

MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");

// Show an error page if the membership could not be obtained.
if (memVO == null) throw new Exception("Unable to get membership details in VewPersonalMembership page.");

//memVehicle = memVO.getVehicleList();

%>
<html>
<head>
<title>View membership vehicles</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif');">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <form name="viewMembershipVehicles" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="viewMembershipVehicles_btnMembership">
  <input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
  <tr valign="top">
      <td>
      <h2 ><%@ include file="/help/HelpLinkInclude.jsp"%> Membership Vehicles</h2>
      <span class="infoTextSmall"> NOT IMPLEMENTED YET </span>
      </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table width="0%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>
               <a onclick="document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('View the membership details');MM_swapImage('ViewMembershipButton','','<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif',1);return document.MM_returnValue" >
                  <img name="ViewMembershipButton" src="<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton.gif" border="0" alt="View the membership details">
               </a>
            </td>
<%--
          <td><input type="submit" name="btnMembership" value="View Membership" onclick="document.all.event.value='viewMembershipVehicles_btnMembership';"></td>
          <td><input type="submit" name="btnChange" value="Change vehicles" onclick="document.all.event.value='viewMembershipVehicles_btnChange';"></td>
--%>
        </tr>
      </table>
    </td>
  </tr>
  </form>
</table>
</body>
</html>
