<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Iterator"%>

<html>
<head>
<%=HTMLUtil.noCacheTag()%>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>

<%
MembershipQuote memQuote = (MembershipQuote)request.getAttribute("memQuote");
%>

<h1>Edit Quote</h1>

   <table border="0" cellspacing="0" cellpadding="3">
      <form name="form<%=(memQuote != null ? memQuote.getQuoteNumber().toString() : "XXX")%>" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
      <input type="hidden" name="quoteNumber" value="<%=(memQuote != null ? memQuote.getQuoteNumber().toString() : "")%>">
      <input type="hidden" name="event" value="editMembershipQuote_btnSave">
      <tr>
        <td>Client Number:</td><td class="dataValue"><%=(memQuote != null ? memQuote.getClientNumber().toString() : "???")%></td>
      </tr>
      <tr>
        <td>Quote Number:</td><td class="dataValue"><%=(memQuote != null ? memQuote.getQuoteNumber().toString() : "???")%></td>
      </tr>
      <tr>
        <td>Quote Date:</td><td class="dataValue"><%=(memQuote != null ? memQuote.getQuoteDate().toString() : "???")%></td>
      </tr>
      <tr>
         <td valign="top">Transaction XML:</td><td valign="top"><textarea name="transactionXML" rows=25 cols=70><%=(memQuote != null ? memQuote.getTransactionXML() : "???")%></textarea></td>
      <tr>
        <td>&nbsp;
        </td>
        <td>
          <% if (memQuote != null) { %>
	    <input type="submit" name="saveButton" value="Save">
          <% } %>
        </td>
      </tr>
      </form>

   </table>
</body>
</html>
