<%@ page import="java.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.client.ui.ClientUIConstants"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.payment.PaymentException"%>
<%@ page import="com.ract.util.*"%>

<%@ include file="/membership/TransactionInclude.jsp"%>
<%
ClientVO contextClient = transGroup.getContextClient();
if (contextClient == null) {
   throw new SystemException("Failed to get context client from transaction group.");
}
String transGroupTypeCode = transGroup.getTransactionGroupTypeCode();
String paSelectedClientKey = transGroup.getSelectedClientKeyForPA();
%>
<html>
<head>
<title>Change Group PA</title>
<%=CommonConstants.getRACTStylesheet()%>
<script language="JavaScript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js">
</script>
<script language="JavaScript">

   function handleOnLoad()
   {
      parent.leftFrame.location = '<%=ClientUIConstants.PAGE_ClientUIC%>?event=viewClientProducts&clientNumber='+<%=contextClient.getClientNumber()%>;
      MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/SearchButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/AddButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/RemoveButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/UndoButton_over.gif');
      return true;
   }

</script>
</head>
<%
// These variables are used repeatedly throughout the jsp.
SelectedClient selectedClient;
MembershipVO membershipVO;
String productCode;
String membershipExpiryDate;
String membershipStatus;
boolean existingGroupMember = false;
ClientVO clientVO = null;
AddressVO addrVO = null;
String postalAddress = null;
String checked = null;

// Set up the list of clients selected to be in the group
Collection selectedClientList = transGroup.getSelectedClientList();
int clientListSize = selectedClientList == null ? 0 : selectedClientList.size();
int clientSpaceCount = clientListSize == 0 ? 1 : selectedClientList.size();
int clientListDivHeight = 25 + (clientSpaceCount * 35);
clientListDivHeight = clientListDivHeight > 90 ? 90 : clientListDivHeight;
%>
<body onLoad="handleOnLoad();">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
  <form name="groupMembersForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="transactionGroupKey" value="<%=transGroup.getTransactionGroupKey()%>">
  <input type="hidden" name="event" value="selectGroupMembers_btnSearch">
  <input type="hidden" name="selectedClientKey" value="0">
  <input type="hidden" name="oldPAClientKey" value="<%=paSelectedClientKey%>">
  <tr valign="top">
    <td>
      <h1><%@ include file="/help/HelpLinkInclude.jsp"%> <%=transGroup.getTransactionHeadingText()%> - Change Group PA</h1>
      <h3>Group members: <%=clientListSize%> <%=(clientListSize == 1 ? "client" : "clients")%> selected.</h3>
      <div style="height:<%=clientListDivHeight%>; overflow:auto; border-width: 1; border-style: solid;">
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="listHeadingPadded">Addressee</td>
          <td class="listHeadingPadded">Client</td>
          <td class="listHeadingPadded">Name</td>
          <td class="listHeadingPadded">Membership</td>
          <td class="listHeadingPadded">Expiry</td>
          <td class="listHeadingPadded">Status</td>
          <td>&nbsp;</td>
        </tr>
<%
if (selectedClientList != null)
{
   String tooltip = null;
   ArrayList selectedClients = new ArrayList(selectedClientList);
   Collections.sort(selectedClients);
   Iterator selectedClientListIT = selectedClients.iterator();
   String addressee = "";
   int selectedCount = 0;
   while (selectedClientListIT.hasNext())
   {
      selectedClient = (SelectedClient) selectedClientListIT.next();
      selectedCount++;
//      invalidReason = selectedClient.getInvalidReason();
      clientVO = selectedClient.getClient();
      membershipVO = selectedClient.getMembership();

      // Check the radio button if this is the prime addressee
      if (selectedClient.getKey().equals(paSelectedClientKey))
      {
         addressee = "<span class=\"helpTextSmall\"> (addressee)</span>";
         checked = " checked";
      }
      else
      {
         checked = "";
         addressee = "";
      }

      // Get the address string.
      addrVO = clientVO.getPostalAddress();
      if (addrVO != null)
         postalAddress = addrVO.getSingleLineAddress();
      else
         postalAddress = null;

      if (membershipVO != null)
      {
         productCode = membershipVO.getCurrentProductCode();
         membershipExpiryDate = DateUtil.formatShortDate(membershipVO.getExpiryDate());
         membershipStatus = MembershipUIHelper.formatMembershipStatusHTML(membershipVO.getStatus());
      }
      else
      {
         productCode = "";
         membershipExpiryDate = "";
         membershipStatus = "";
      }
%>
       <tr valign="top">
         <td class="listItemPadded" align="center">
            <input name="paSelectedClientKey"
                   value="<%=selectedClient.getKey()%>"
                   type="radio" <%=checked%>
                   onclick="document.all.selectedClientKey.value='<%=selectedClient.getKey()%>';"
            >
         </td>
         <td class="listItemPadded"><a class="label" title="Joined group on <%=DateUtil.formatDate(selectedClient.getMembershipGroupJoinDate(),"dd/MM/yyyy HH:mm:ss:SS")%>"><%=clientVO.getClientNumber()%></a></td>
<%
         // Show the address as a tooltip when the mouse hovers over the client name
      if (postalAddress != null)
      {
%>
         <td class="listItemPadded"><a href="#" class="actionItemBlack" title="<%=postalAddress%>"><%=clientVO.getDisplayName()%><%=addressee%></a></td>
<%
      }
      else
      {
%>
         <td class="listItemPadded"><%=clientVO.getDisplayName()%><%=addressee%></td>
<%
      }
%>
         <td class="listItemPadded"><%=productCode%></td>
         <td class="listItemPadded"><%=membershipExpiryDate%></td>
         <td class="listItemPadded"><%=membershipStatus%></td>
         <td>
         </td>
       </tr>
<%
   }
}
%>
      </table>
      </div>
    </td>
  </tr>
  <tr valign="bottom">
    <td>
      <table border="0" cellpadding="5" cellspacing="0">
         <tr>
            <td>
               <a onclick="history.back(); return true;"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the previous page');MM_swapImage('BackButton','','<%=CommonConstants.DIR_IMAGES%>/BackButton_over.gif',1);return document.MM_returnValue" >
                  <img name="BackButton" src="<%=CommonConstants.DIR_IMAGES%>/BackButton.gif" border="0" alt="Show the previous page">
               </a>
            </td>
            <td>
               <a onclick="document.all.event.value='changeGroupPA_btnSave'; document.forms[0].submit();"
                  onMouseOut="MM_swapImgRestore();"
                  onMouseOver="MM_displayStatusMsg('Show the next page');MM_swapImage('SaveButton','','<%=CommonConstants.DIR_IMAGES%>/NextButton_over.gif',1);return document.MM_returnValue" >
                  <img name="NextButton" src="<%=CommonConstants.DIR_IMAGES%>/SaveButton.gif" border="0" alt="Show the next page">
               </a>
            </td>
         </tr>
      </table>
    </td>
  </tr>
  </form>
</table>
</body>
</html>
