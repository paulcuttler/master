<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>

<%
MembershipRepairMgr membershipRepairMgr = MembershipEJBHelper.getMembershipRepairMgr();
int joinDateToRepairCount = membershipRepairMgr.getMembershipCardJoinDateToRepairCount();
int cardNameToRepairCount = membershipRepairMgr.getMembershipCardNameToRepairCount();
int transToRepairCount = membershipRepairMgr.getTransactionDetailsToRepairCount();
int duplicateTransToRepairCount = membershipRepairMgr.getDuplicationTransactionsToRepairCount();
int accessMemberCount = membershipRepairMgr.getAccessMemberMarketingCount(); 
int membershipTransferred = membershipRepairMgr.getMembershipsWithTransferredStatusCount();
int oneMemberGroupCount = membershipRepairMgr.getGroupsWithOneMemberCount();
%>

<html>
<head>
<title>RepairMembership</title>
<%=CommonConstants.getRACTStylesheet()%>
</head>
<body>
<h1>Repair membership</h1>
<form name="repairMembershipForm" method="post" action="<%=MembershipUIConstants.PAGE_MembershipAdminUIC%>">

<input type="hidden" name="event" value="RepairMembership">

<p>Set membership card join dates to the join date of the membership where the membership card join date is null.
<br><%=joinDateToRepairCount%> membership cards with a null join date.
<br><input type="submit" name="btnRepairCardJoinDate" value="Go" onclick="document.all.event.value='RepairMembership_btnRepairCardJoinDate'" <%=(joinDateToRepairCount < 1 ? "disabled" : "")%>>
</p>

<p>Generate a membership card name where the the membership card name is null.
<br><%=cardNameToRepairCount%> membership cards with a null card name.
<br><input type="submit" name="btnRepairCardName" value="Go" onclick="document.all.event.value='RepairMembership_btnRepairCardName'" <%=(cardNameToRepairCount < 1 ? "disabled" : "")%>>
</p>

<p>Generate effective date and end date for membership transactions
<br><%=transToRepairCount%> transactions with a null effective date and end date.
<br><input type="submit" name="btnRepairTrans" value="Go" onclick="document.all.event.value='RepairMembership_btnRepairTrans'" <%=(transToRepairCount < 1 ? "disabled" : "")%>>
</p>

<p>Repair duplicate transactions resulting from fast track renewals being committed twice.
<br><%=duplicateTransToRepairCount%> transactions that require an undo.
<br><input type="submit" name="btnRepairTrans" value="Go" onclick="document.all.event.value='RepairMembership_btnRepairDupTrans'" <%=(duplicateTransToRepairCount < 1 ? "disabled" : "")%>>
</p>

<p>Repair Access members that do not have the marketing flag set.
<br><%=accessMemberCount%> Access customers that need marketing flag set to true.
<br><input type="submit" name="btnRepairAccessMembers" value="Go" onclick="document.all.event.value='RepairMembership_btnRepairAccessMembers'" <%=(accessMemberCount < 1 ? "disabled" : "")%>>
</p>

<p>Repair memberships with a status of Transferred
<br><%=membershipTransferred%> memberships with a status of Transferred.
<br><input type="submit" name="btnRepairTransferredStatus" value="Go" onclick="document.all.event.value='RepairMembership_btnRepairTransferredStatus'" <%=(membershipTransferred < 1 ? "disabled" : "")%>>
</p>

<p>Repair one member groups
<br><%=oneMemberGroupCount%> groups with one member.
<br><input type="submit" name="btnRepairTransferredStatus" value="Go" onclick="document.all.event.value='RepairMembership_btnRepairOneMemberGroups'" <%=(oneMemberGroupCount < 1 ? "disabled" : "")%>>
</p>
 
</form>
</body>
</html>
