<%@ page import="javax.naming.*"%>
<%@ page import="javax.rmi.PortableRemoteObject"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ract.membership.*"%>
<%@ page import="com.ract.membership.ui.*"%>
<%@ page import="com.ract.client.*"%>
<%@ page import="com.ract.client.ui.*"%>
<%@ page import="com.ract.util.*"%>
<%@ page import="com.ract.common.*"%>
<%@ page import="com.ract.payment.ui.*"%>
<%@ page import="com.ract.payment.*"%>
<%@ page import="com.ract.payment.bank.*"%>
<%@ page import="com.ract.payment.directdebit.*"%>
<%@ page import="com.ract.payment.receipting.*"%>

<%
MembershipVO memVO = (MembershipVO) request.getAttribute("membershipVO");

// Show an error page if the membership could not be obtained.
if (memVO == null)
{
   throw new Exception("Unable to get membership details in ViewPaymentMethod page.");
}
ClientVO clientVO = memVO.getClient();
request.setAttribute("client",clientVO);

// test if failed DD
boolean failedDD = false;
try {
  	memVO.getAmountOutstanding(true,false);
} catch (DirectDebitFailedValidationException e) {
	failedDD = true;
} catch (Exception e) {}

// Got a valid membership. Now see if the product is current.
// If the product is effective in the future and the membership commenced in
// the past then we need to find the current product from the history.
String currentProductCode = memVO.getCurrentProductCode();
DateTime currentProductEffectiveDate = memVO.getCurrentProductEffectiveDate();

String productCode = memVO.getProductCode();
DateTime productEffectiveDate = memVO.getProductEffectiveDate();

//if no product code then add a warning
if (currentProductCode == null)
{
  currentProductCode = productCode;
  currentProductEffectiveDate = productEffectiveDate;
}

DirectDebitAuthority ddAuthority = memVO.getDirectDebitAuthority();

%>
<html>
<head>
<title>View membership</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=CommonConstants.getRACTStylesheet()%>
<script type="text/javascript" src="<%=CommonConstants.DIR_SCRIPTS%>/ButtonRollover.js"></script>
<script type="text/javascript">
function printMembershipAccountDetails()
{
  //print membership account details
  var url = 'MembershipReportsUIC?event=printMembershipAccountDetails&membershipId=' + <%=memVO.getMembershipID()%>;
  window.open(url, '', 'width=250,height=150,toolbar=no,status=no,scrollbars=no,resizable=no');
}
function emailMembershipAccountDetails()
{
  //open window to enter email details
  var url = 'EmailAdviceForm.action?membershipId=' + <%=memVO.getMembershipID()%> + '&adviceType=' + '<%=SystemParameterVO.EMAIL_ADVICE_DD%>';
  window.open(url, '', 'width=640,height=480,toolbar=no,status=no,scrollbars=no,resizable=no');
}
</script>
</head>
<body onload="MM_preloadImages('<%=CommonConstants.DIR_IMAGES%>/ViewMembershipButton_over.gif','<%=CommonConstants.DIR_IMAGES%>/EditButton_over.gif');">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <form name="viewPaymentMethod" method="post" action="<%=MembershipUIConstants.PAGE_MembershipUIC%>">
  <input type="hidden" name="event" value="viewPaymentMethod_btnMembership">
  <input type="hidden" name="membershipID" value="<%=memVO.getMembershipID()%>">
    <tr valign="top">
      <td>
        <h1><%@ include file="/help/HelpLinkInclude.jsp"%> View membership payment method</h1>
        <table border="0" cellpadding="0" cellspacing="3">
          <tr>
            <td>
              <%@ include file="/client/ViewClientInclude.jsp"%>
            </td>
          </tr>
        </table>
<%
if (ddAuthority != null) {
   BankAccount bankAccount = ddAuthority.getBankAccount();
%>
        <h2>Direct Debit Authority Details</h2>
        <table border="0" cellspacing="0" cellpadding="3">
<%
   if (bankAccount != null
   &&  bankAccount instanceof DebitAccount)
   {
      DebitAccount debitAccount = (DebitAccount) bankAccount;
%>
          <tr>
            <td class="label" valign="top">Account type:</td>
            <td>&nbsp;</td>
            <td class="dataValue">Debit account</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">BSB number:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=debitAccount.getBsbNumber()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Account number:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=debitAccount.getAccountNumber()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
<%
   }
   else if (bankAccount != null
   &&       bankAccount instanceof CreditCardAccount)
   {
      CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
%>
          <tr>
            <td class="label" valign="top">Account type:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=ccAccount.getCardTypeDescription()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Credit card number:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=StringUtil.obscureFormattedCreditCardNumber(ccAccount.getCardNumber())%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Credit card expiry:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=ccAccount.getCardExpiry()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
<%
   }
   else
   {
%>
          <tr>
            <td class="dataValueRed" valign="top" colspan="6">Unknown bank account type!</td>
          </tr>
<%
   }
%>
          <tr>
            <td class="label" valign="top">Account name:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=bankAccount.getAccountName()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Client is account owner:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(ddAuthority.isClientIsOwner() ? "check.gif" : "cross.gif")%>"></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Deduction frequency:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=(ddAuthority.getDeductionFrequency() != null ? ddAuthority.getDeductionFrequency().getDescription() : "")%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Day of month to debit:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=ddAuthority.getDayToDebit()%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
<!--
          <tr>
            <td class="label" valign="top">DDR date:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><%=(ddAuthority.getDdrDate() != null ? ddAuthority.getDdrDate().formatShortDate() : "")%></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
-->
          <tr>
            <td class="label" valign="top">Approved:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(ddAuthority.isApproved() ? "check.gif" : "cross.gif")%>"></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="label" valign="top">Automatically renew:</td>
            <td>&nbsp;</td>
            <td class="dataValue"><img src="<%=CommonConstants.DIR_IMAGES%>/check.gif"></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
<%
} else {
%>
      <span class="label" valign="top">Payment method:</span>
      <span class="dataValue"><%=ReceiptingPaymentMethod.RECEIPTING%></span>
<%
}
%>

<%
if (ddAuthority != null)
{
Collection scheduleList = ddAuthority.getScheduleList();
LogUtil.log(this.getClass(),"scheduleList="+scheduleList);
Collections.sort((List)scheduleList);
Collections.reverse((List)scheduleList);
LogUtil.log(this.getClass(),"scheduleList="+scheduleList);
if (scheduleList != null && scheduleList.size() > 0)
{
%>
 <table border="0" cellspacing="0" cellpadding="3">
         <tr>
           <td colspan="5"><h2>Dependent Schedules</h2></td>
         </tr>
         <tr class="headerRow">
           <td class="listHeadingPadded">Active</td>
           <td class="listHeadingPadded">Renewal?</td>
           <td class="listHeadingPadded">Create Date</td>
           <td class="listHeadingPadded">User Id</td>
           <td class="listHeadingPadded">Start Date</td>
           <td class="listHeadingPadded">End Date</td>
           <td class="listHeadingPadded">Scheduled Items</td>
           <td class="listHeadingPadded">Amount</td>
           <td class="listHeadingPadded">Amount Paid</td>
           <td class="listHeadingPadded">Amount Outstanding</td>
           <td class="listHeadingPadded">Payable Ref.</td>
           <td class="listHeadingPadded">Description</td>
         </tr>
<%
  DirectDebitSchedule ddSchedule = null;
  Iterator it = scheduleList.iterator();
  int counter = 0;
  while (it.hasNext())
  {
    ddSchedule = (DirectDebitSchedule)it.next();
    counter++;
%>
  <tr class="<%=HTMLUtil.getRowType(counter)%>">
    <td><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(ddSchedule.isCurrent() ? "check.gif" : "cross.gif")%>"></td>
    <td><img src="<%=CommonConstants.DIR_IMAGES%>/<%=(ddSchedule.isRenewalVersion() ? "check.gif" : "cross.gif")%>"></td>
    <td><a title="Schedule ID=<%=ddSchedule.getDirectDebitScheduleID()%>" href="<%=PaymentUIConstants.PAGE_PaymentUIC%>?event=viewDirectDebitSchedule&directDebitPaymentMethodID=<%=ddSchedule.getDirectDebitScheduleID()%>">
<%=ddSchedule.getCreateDate()%></a></td>
<td><%=ddSchedule.getUserID()%></td>
<td><%=ddSchedule.getStartDate()%></td>
<td><%=ddSchedule.getEndDate()%></td>
<td><%=ddSchedule.getScheduledItemCount()%></td>
<td><%=CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmount())%></td>
<td><%=CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmountPaid())%></td>
<td><%=CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmountOutstanding())%></td>
<td><%=ddSchedule.getPayable().getSequenceNumber()%></td>
<td><%=ddSchedule.getScheduleDescription()%></td>
  </tr>
<%
  }
%>
  </table>
<%
}
}
%>
      </td>
    </tr>
    <tr valign="bottom">
      <td>
         <!-- View data links -->
         <table border="0" cellpadding="5" cellspacing="0">
         <tr>
            <td>
               <input type="button" value="View Membership" onclick="document.forms[0].event.value = 'viewPaymentMethod_btnMembership'; document.forms[0].submit();">
            </td>
			<%
			if (!failedDD) { // cannot edit payment method if DD failed
			%>
            <td>
               <input type="button" value="Edit" onclick="document.forms[0].event.value = 'viewPaymentMethod_btnEdit'; document.forms[0].submit();">
            </td>
            <%
            }
            if (ddAuthority != null)
            {
            %>
           <td>
             <input type="button" name="btnPrint" value="Print Account Details" onClick="printMembershipAccountDetails();">
           </td>
           <td>
             <input type="button" name="btnEmail" value="Email Account Details" onClick="emailMembershipAccountDetails();">
           </td>
           <%
            }
           %>
         </tr>
         </table>
      </td>
    </tr>
    </form>
  </table>
</body>
</html>
