<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.help.*"%>

<%
HelpPage help = (HelpPage)request.getAttribute("help");
%>

<table border="0">

<%
if (help != null)
{
%>
  <tr valign="top">
    <td>Help Id:</td>
    <td>
    <%=help.getHelpId()%>
    <input type="hidden" name="helpId" value="<%=help.getHelpId()%>"/>
    </td>
  </tr>  
<%
} 
%>

  <tr valign="top">
      <td>Help Category</td>
    <td>
        <input type="text" name="helpCategory" size="50" value="<%=help!=null?help.getHelpCategory():""%>">
      </td>
  </tr>
  <tr valign="top">
      <td>Help Heading</td>
    <td>
        <input type="text" name="helpHeading" size="50" value="<%=help!=null?help.getHelpHeading():""%>">
      </td>
  </tr>
  <tr valign="top">
      <td>Help Text</td>
    <td>
        <textarea name="helpText" cols="100" rows="10"><%=help!=null?help.getHelpText():""%></textarea>
      </td>
  </tr>
  <tr valign="top">
      <td>Help Page Reference</td>
    <td>
        <input type="text" name="helpPageReference" size="50" value="<%=help!=null?help.getHelpPageReference():""%>"><span class="helpTextSmall">IS will need to advise this reference to allow live links.</span>
      </td>
  </tr>  
</table>
