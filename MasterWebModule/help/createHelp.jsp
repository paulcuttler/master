<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.help.*"%>
<%@page import="java.util.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body>

<form method="post" action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>">
<input type="hidden" name="event" value="createHelp"/>

<table height="100%">
<tr valign="top">
<td>
  <table>
    <tr>
      <td><h2>Create Help</h2></td>
    </tr>
  </table>

<%@ include file="/help/editHelpInclude.jsp"%>
</td>
</tr>
<tr valign="bottom">
<td>
<input type="submit" value="Save"/>
</td>
</tr>
</table>
</form>

</body>
</html>
