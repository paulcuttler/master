<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.help.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
<%= HTMLUtil.includeJS(CommonConstants.DIR_SCRIPTS+ "/AdminMaintenancePage.js") %>
</head>

<%
HelpPage help = (HelpPage)request.getAttribute("help");
String refresh = (String)request.getAttribute("refresh");
String listPage = CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST;
%>

<body onload="refreshList(document.all.refresh,'<%=listPage%>')">
<form action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>">
<input type="hidden" name="refresh" value ="<%=refresh%>">
<table height="100%">
<tr valign="top">
<td>

<table>
  <tr>
    <td><h3>View Help</h3></td>
  </tr>
</table>

<table>
  <tr><td class="label">Help Id:</td><td class="dataValue"><%=help.getHelpId()%></td></tr>
  <tr><td class="label">Help Category:</td><td class="dataValue"><%=help.getHelpCategory()%></td></tr>
  <tr><td class="label">Help Heading:</td><td class="dataValue"><%=help.getHelpHeading()%></td></tr>
  <tr><td class="label">Help Text:</td><td><%=help.getHelpText()%></td></tr>
  <tr><td class="label">Help Page Reference:</td><td class="dataValue"><%=help.getHelpPageReference()%></td></tr>
</table>

</td>
</tr>
<tr valign="bottom">
<td>

<input type="hidden" name="event" value="editHelp"/>
<input type="hidden" name="helpId" value="<%=help.getHelpId()%>"/>
<input type="submit" value="Edit"/>
</td>
</tr>
</table>
</form>
</body>
</html>

