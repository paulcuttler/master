<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.help.*"%>
<%@page import="java.util.*"%>
<%@include file="/security/VerifyAccess.jsp"%>
<%
ArrayList searchResults = (ArrayList)request.getAttribute("searchResults");
String searchTerm = (String)request.getAttribute("searchTerm");

%>

<title>Search Help</title>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<body onload="document.all.searchTerm.focus();">
  <form action="<%=CommonUIConstants.PAGE_CommonAdminUIC%>">
<input type="hidden" name="event" value="searchHelp"/>
<h2>Search Help</h2>
<table>
<tr>
  <td><input type="text" name="searchTerm" value="<%=searchTerm==null?"":searchTerm%>"/></td><td><input type="submit" value="Search"/> </td>
</tr>
<tr>
  <td colspan="2" class="helpTextSmall">Enter a search term or leave blank to retrieve all help pages.</td>
</tr>
</table>

<%
final int MAX = 50;
HelpPage helpPage = null;
if (searchResults != null)
{
%>
<hr/>

<table>
<tr>
  <td class="listHeading" colspan="5">Results</td>
</tr>
<%
if (searchResults.size() > 0)
{
%>
<tr>
  <td colspan="5">Results for <b><%=searchTerm%></b>.</td>
</tr>
<tr class="headerRow" >
  <td class="listHeadingPadded">Category</td>
  <td class="listHeadingPadded">Heading</td>
  <td class="listHeadingPadded">Text (Preview)</td>
</tr>
<%
  for (int i = 0; i < searchResults.size(); i++)
  {
    helpPage = (HelpPage)searchResults.get(i);
%>
<tr class="<%=HTMLUtil.getRowType(i)%>">
  <td class="listItemPadded"><%=helpPage.getHelpCategory()%></td>
  <td class="listItemPadded"><a href="<%=CommonUIConstants.PAGE_CommonAdminUIC%>?event=viewHelpText&helpId=<%=helpPage.getHelpId()%>" target="_blank" title="Show help page for this topic."><%=helpPage.getHelpHeading()%></a></td>
  <td class="listItemPadded"><%=helpPage.getHelpText()!=null?(helpPage.getHelpText().length()>MAX?helpPage.getHelpText().substring(0,MAX)+"...":helpPage.getHelpText()):""%></td>
</tr>
<%
  }
}
else if (searchResults.size() == 0)
{
%>
<tr>
  <td colspan="5" class="helpText">No results were found for search term <b><%=searchTerm%></b>. Click <a href="<%=CommonUIConstants.PAGE_CommonAdminUIC%>?event=notifyHelpAdministrator&searchTerm=<%=searchTerm%>">here</a> to notify an administrator.</td>
</tr>
<%
}
}
%>

</table>

</form>

</body>


</html>
