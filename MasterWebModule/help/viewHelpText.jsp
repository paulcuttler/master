<%@page import="java.util.*"%>
<%@page import="com.ract.util.*"%>
<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.help.*"%>

<html>

<head>
<%=CommonConstants.getRACTStylesheet()%>
</head>

<%
HelpPage help = (HelpPage)request.getAttribute("help");
%>

<body>
<table height="100%">
<tr valign="top">
<td>

<table>
  <tr><td colspan="2"><h2><%=help.getHelpHeading()%></h2></td></tr>
  <tr><td colspan="2"><%=help.getHelpText()%></td></tr>
</table>

</td>
</tr>
</table>
</body>
</html>

