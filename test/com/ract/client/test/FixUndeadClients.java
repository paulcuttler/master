package com.ract.client.test;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;

public class FixUndeadClients {
    private ClientMgr clientMgrRemote;

    @Before
    public void setUp() throws Exception {
	clientMgrRemote = ClientEJBHelper.getClientMgr();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void fixClients() throws RemoteException {

	String userID = "COM";
	String transactionReason = Client.HISTORY_UPDATE;
	String salesBranchCode = "HOB";
	String comments = "Fixing incorrect status";
	String sourceSystem = "RSS";

	List<Integer> clientNumberList = new ArrayList<Integer>();

	String inputFile = "c:/temp/deceasedClients.csv";

	CSVReader reader = null;
	try {
	    // Open the file, ignore quotes
	    System.out.println("reading file: " + inputFile);
	    reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));

	    String[] line = null;

	    // Process the file line by line
	    while ((line = reader.readNext()) != null) {
		// assume first col is clientNo

		try {
		    clientNumberList.add(Integer.parseInt(line[0]));
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	} catch (Exception e) {
	    System.err.println("Unable to parse inputFile: " + inputFile + ": " + e);
	}

	int count = 1;

	for (Integer clientNo : clientNumberList) {
	    ClientVO client = clientMgrRemote.getClient(clientNo);

	    if (client.getStatus().equalsIgnoreCase(Client.STATUS_DECEASED)) {
		System.out.println(count + ": ignoring client: " + client.getClientNumber() + ", already Deceased");
		count++;
		continue;
	    }

	    System.out.println(count + ": updating client: " + client.getClientNumber() + ", status: " + Client.STATUS_DECEASED);
	    client.setStatus(Client.STATUS_DECEASED);

	    clientMgrRemote.updateClient(client, userID, transactionReason, salesBranchCode, comments, sourceSystem);

	    count++;
	}

	System.out.print("Done.");

    }
}
