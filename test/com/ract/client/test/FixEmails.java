package com.ract.client.test;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.util.DateTime;

public class FixEmails {

    private ClientMgr clientMgrRemote;

    @Before
    public void setUp() throws Exception {
	clientMgrRemote = ClientEJBHelper.getClientMgr();
    }

    @After
    public void tearDown() throws Exception {
    }

    // @Test
    public void fixEmails() throws RemoteException {

	Map<String, String> emailMap = new LinkedHashMap<String, String>();
	emailMap.put("%@gmail.com.au", "gmail.com");
	emailMap.put("%@netspace.com.au", "netspace.net.au");

	String userID = "COM";
	String transactionReason = Client.HISTORY_UPDATE;
	String salesBranchCode = "HOB";
	String comments = "Fixing incorrect email domain";
	String sourceSystem = "RSS";

	for (String email : emailMap.keySet()) {
	    System.out.print("Searching for emails matching: " + email + "...");
	    Collection<ClientVO> clients = clientMgrRemote.findClientsByEmailAddress(email);
	    System.out.println(" found " + clients.size());

	    for (ClientVO client : clients) {
		String[] parts = client.getEmailAddress().split("@");
		String fixedEmail = parts[0] + "@" + emailMap.get(email);

		System.out.println("updating email: " + client.getEmailAddress() + " to: " + fixedEmail + " ...");
		client.setEmailAddress(fixedEmail);

		clientMgrRemote.updateClient(client, userID, transactionReason, salesBranchCode, comments, sourceSystem);
	    }
	}

	System.out.print("Done.");

    }

    @Test
    public void fixUnsubscribedClients() throws RemoteException, ParseException {
	DateTime startDate = new DateTime("1/1/1950");

	System.out.println((new DateTime()).formatLongDate() + ": Fixing unsubscribed clients: ");

	List<ClientVO> clientList = clientMgrRemote.getClientsWithoutSubscriptions(startDate);

	System.out.println("Found " + clientList.size() + " clients added since " + startDate + " without subscriptions");
	for (ClientVO c : clientList) {
	    System.out.print(c.getClientNumber());
	    clientMgrRemote.createDefaultClientSubscriptions(c);
	    System.out.println(" - created.");
	}

	System.out.println((new DateTime()).formatLongDate() + ": Done.");
    }
}
