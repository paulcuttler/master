package com.ract.client.test;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientNote;
import com.ract.client.ClientSubscription;
import com.ract.client.MarketOption;
import com.ract.common.GenericException;
import com.ract.common.ServiceLocator;
import com.ract.common.SourceSystem;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.payment.electronicpayment.EftPayment;
import com.ract.payment.electronicpayment.EftPaymentMgr;
import com.ract.payment.receipting.Payable;
import com.ract.travel.TramadaClientHelper;
import com.ract.travel.TramadaClientTransaction;
import com.ract.vehicle.VehicleVO;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.security.User;
import com.ract.web.security.UserSecurityMgrRemote;

public class TestMerge {
    private ClientMgr clientMgr;
    private MembershipMgr membershipMgr;
    private EftPaymentMgr eftPaymentMgr;
    private PaymentMgr paymentMgr;
    private CustomerMgrRemote customerMgr;
    private UserSecurityMgrRemote userSecurityMgr;

    @Before
    public void setUp() throws Exception {
	clientMgr = ClientEJBHelper.getClientMgr();
	membershipMgr = MembershipEJBHelper.getMembershipMgr();
	eftPaymentMgr = PaymentEJBHelper.getEftPaymentMgr();
	paymentMgr = PaymentEJBHelper.getPaymentMgr();
	customerMgr = (CustomerMgrRemote) ServiceLocator.getInstance().getObject("CustomerMgr/remote");
	userSecurityMgr = (UserSecurityMgrRemote) ServiceLocator.getInstance().getObject("UserSecurityMgr/remote");
    }

    @Test
    public void logBusiness() throws RemoteException, GenericException {
	Integer clientNo = 619629;

	System.out.println("Client no: " + clientNo);

	System.out.println("Memberships:");
	Collection<MembershipVO> membershipList = membershipMgr.findMembershipByClientNumber(clientNo);
	for (MembershipVO v : membershipList) {
	    System.out.println(v);
	}

	System.out.println("Tramada:");
	TramadaClientTransaction tramadaClientTxn = TramadaClientHelper.getTramadaClientTransaction(clientNo);
	if (tramadaClientTxn != null) {
	    System.out.println(tramadaClientTxn.getTramadaClientTransactionPK().getTramadaProfileCode());
	}

	System.out.println("Vehicles:");
	Collection<VehicleVO> vehicleList = clientMgr.getClientVehicleList(clientNo);
	for (VehicleVO v : vehicleList) {
	    System.out.println(v);
	}

	System.out.println("Marketing options:");
	Collection<MarketOption> marketList = clientMgr.getMarketOptions(clientNo);
	for (MarketOption v : marketList) {
	    try {
		System.out.println(v);
	    } catch (Exception e) {
	    }
	}

	System.out.println("Subscriptions:");
	Collection<ClientSubscription> subscriptionList = clientMgr.getClientSubscriptionList(clientNo);
	for (ClientSubscription v : subscriptionList) {
	    System.out.println(v);
	}

	System.out.println("Client notes:");
	Collection<ClientNote> clientNoteList = clientMgr.getClientNoteList(clientNo);
	for (ClientNote v : clientNoteList) {
	    System.out.println(v);
	}

	System.out.println("EFT transactions:");
	Collection<EftPayment> eftList = eftPaymentMgr.getClientEFTPayments(clientNo);
	for (EftPayment v : eftList) {
	    System.out.println(v);
	}

	// web clients?
	// ...

	System.out.println("Web user:");
	User user = userSecurityMgr.getUserByClientNumber(clientNo);
	if (user != null) {
	    System.out.println(user.getUserId());
	}

	System.out.println("External clients:");
	List<Map<String, String>> externalClientList = clientMgr.getExternalClients(clientNo);
	for (Map<String, String> m : externalClientList) {
	    System.out.println(m.get("sourceSystem") + " : " + m.get("sourceSystemId"));
	}

	System.out.println("Receptor receipts:");
	Collection<Payable> receiptList = paymentMgr.getReceiptingHistory(clientNo, SourceSystem.MEMBERSHIP);
	for (Payable v : receiptList) {
	    System.out.println(v);
	}

	System.out.println("Done.");
    }

}
