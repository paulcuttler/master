package com.ract.client.test;

import java.rmi.RemoteException;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;

public class TestSalutation {
    private ClientMgr clientMgr;

    @Before
    public void setUp() throws Exception {
	clientMgr = ClientEJBHelper.getClientMgr();
    }

    @Test
    public void test() throws RemoteException {

	String publicationCode = "General";
	Collection<ClientVO> clientList = clientMgr.getSubscribedClients(publicationCode);

	for (ClientVO clientVO : clientList) {
	    System.out.println("Salutation: " + clientVO.getSalutation() + ", given names: " + clientVO.getGivenNames() + ", surname: " + clientVO.getSurname() + ", address title: " + clientVO.getAddressTitle());
	}
    }

}
