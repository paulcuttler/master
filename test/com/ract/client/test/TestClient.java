package com.ract.client.test;

import java.rmi.RemoteException;

import junit.framework.TestCase;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.integration.RACTPropertiesProvider;

public class TestClient extends TestCase {
    protected void setUp() throws Exception {
	RACTPropertiesProvider.setContextURL("localhost", 1099);
	super.setUp();
    }

    protected void tearDown() throws Exception {
	super.tearDown();
    }

    public void testGetClient() throws RemoteException {
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	ClientVO client = clientMgr.getClient(new Integer(430741));

	assertNotNull(client);

	assertEquals("Holliday", client.getSurname());
	assertEquals("Mr", client.getTitle());

    }

}
