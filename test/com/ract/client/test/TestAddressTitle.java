package com.ract.client.test;

import com.ract.util.StringUtil;

public class TestAddressTitle
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
	String finalAddressTitle = "";
	StringBuffer addressTitle = new StringBuffer();
	String thisTitle = "Dr";
	if (thisTitle != null) {
	    addressTitle.append(StringUtil.toProperCase(thisTitle.trim()));
	    addressTitle.append(" ");
	}
	String thisSurname = "O'Rourke";
	if (thisSurname != null) {
	    int endIndex = thisSurname.indexOf("'");
	    // cater for apostrophes
	    if (endIndex > 0) {
		addressTitle.append(thisSurname.substring(0, endIndex).toUpperCase() + StringUtil.toProperCase(thisSurname.substring(endIndex, thisSurname.length())));
	    } else {
		addressTitle.append(StringUtil.toProperCase(thisSurname.trim()));
	    }
	}
	if (addressTitle.length() > 0) {
	    finalAddressTitle = addressTitle.toString();
	} else {
	    finalAddressTitle = "Bozo";
	}
	
	System.out.println(finalAddressTitle);
    }
}
