package com.ract.payment.test;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.quartz.JobDetail;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SourceSystem;
import com.ract.common.SystemDataException;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValueObject;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.membership.MembershipDocument;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipTransactionMgr;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.TransactionGroup;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.directdebit.DirectDebitAuditJob;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.finance.FinanceHelper;
import com.ract.payment.finance.FinanceMgr;
import com.ract.payment.finance.GLTransaction;
import com.ract.payment.receipting.OCRAdapter;
import com.ract.payment.receipting.Payable;
import com.ract.security.SecurityHelper;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.web.common.PaymentFileGeneratorJob;

public class TestPayment {
    private PaymentMgr paymentMgr;
    private PaymentTransactionMgr paymentTxnMgr;
    private MembershipMgr membershipMgr;
    private FinanceMgr financeMgr;
    private MembershipTransactionMgr memTransMgr;

    @Before
    public void setUp() throws Exception {
	paymentMgr = PaymentEJBHelper.getPaymentMgr();
	paymentTxnMgr = PaymentEJBHelper.getPaymentTransactionMgr();
	membershipMgr = MembershipEJBHelper.getMembershipMgr();
	financeMgr = PaymentEJBHelper.getFinanceMgr();
	memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
    }

    @After
    public void tearDown() throws Exception {
    }

    // @Test
    public void fireDDAudit() throws Exception {
	System.out.println("Firing DD Audit Job...");

	JobDetail jobDetail = new JobDetail("DDAuditJob", Scheduler.DEFAULT_GROUP, DirectDebitAuditJob.class);
	jobDetail.setDescription("DD Audit Job");

	SimpleTrigger trigger = new SimpleTrigger("DDAuditTrigger", Scheduler.DEFAULT_GROUP, new DateTime());

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);

	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	System.out.println("Fired DD Audit Job.");
    }

    // @Test
    public void verifyDDSchedules() throws Exception {
	/*
	 * DateTime startDate = new DateTime();//"30/11/2011"); Interval since =
	 * new Interval(0,1,0,0,0,0,0); //startDate = startDate.subtract(since);
	 * 
	 * System.out.println("Looking for DD payables since: " + startDate +
	 * "...");
	 * 
	 * Map<String,String> results =
	 * paymentMgr.getUnlinkedDDSchedules(startDate);
	 * 
	 * for (String key : results.keySet()) { System.out.println("client: " +
	 * key + ", payableItemId: " + results.get(key)); }
	 * 
	 * System.out.println("Done.");
	 */
	/*
	 * String renewalFile = "U:\\export\\mem\\20111212renWithoutCards.dat";
	 * 
	 * BufferedReader br = new BufferedReader(new InputStreamReader(new
	 * FileInputStream(renewalFile))); String line = null; String[] parts;
	 * String clientNo; String type;
	 * 
	 * System.out.println("Checking DD schedules from renewalFile: " +
	 * renewalFile + "...");
	 * 
	 * while((line = br.readLine()) != null) { parts = line.split("\\|");
	 * clientNo = parts[1]; type = parts[2];
	 * 
	 * if (type.equalsIgnoreCase(MembershipDocument.RENEWAL_ADVICE)) {
	 * System.out.print("Checking DD schedule for client: " + clientNo +
	 * "..."); List<MembershipVO> memberList = (List<MembershipVO>)
	 * membershipMgr
	 * .findMembershipByClientNumber(Integer.parseInt(clientNo));
	 * MembershipVO memVO = memberList.get(0);
	 * 
	 * ddReport(memVO); } }
	 * 
	 * System.out.println("Done.");
	 * 
	 * br.close();
	 */

	DateTime expiryDate = new DateTime();
	Map<String, String> advicesWithoutSchedules = membershipMgr.getRenewalAdvicesWithoutDDSchedules(expiryDate);
	System.out.println("Checking DD schedules for ALL renewals since: " + expiryDate + "...");

	String clientNumber = null;
	for (String ddPayableSeq : advicesWithoutSchedules.keySet()) {
	    clientNumber = advicesWithoutSchedules.get(ddPayableSeq);
	    System.out.println("ddPayableSeq: " + ddPayableSeq + ", clientNumber: " + clientNumber);
	}

	System.out.println("Done.");
    }

    private void ddReport(MembershipVO memVO) throws RemoteException {

	// System.out.print("Checking DD schedule for client: " +
	// memVO.getClientNumber() + "...");

	// reproduce functionality from MembershipVO.getCurrentRenewalDocument()
	// as internally it uses a Local mgr.
	Integer targetMembershipId = null;
	if (!memVO.isGroupMember()) {
	    targetMembershipId = memVO.getMembershipID();
	} else {
	    targetMembershipId = memVO.getMembershipGroup().getMembershipGroupDetailForPA().getMembershipGroupDetailPK().getMembershipID();
	}

	MembershipDocument doc = membershipMgr.getCurrentDocument(targetMembershipId, memVO.getExpiryDate());
	if (doc == null) {
	    // System.out.println("no renewal doc found");
	    return;
	}
	TransactionGroup transGroup = MembershipHelper.getTransactionGroup(doc.getTransactionXML());

	try {
	    transGroup.getDirectDebitSchedule();
	    // System.out.println("ok");
	} catch (RemoteException e) {
	    // System.out.println("");
	    System.out.println("Unable to retrieve DD schedule for client: " + memVO.getClientNumber());
	}
    }

    @SuppressWarnings("unchecked")
    // @Test
    public void listFailedDDClients() throws SystemException {
	System.out.println("Retrieving failed DD schedules...");

	List<Map<String, Object>> results = paymentMgr.getFailedDDSchedules(0, SourceSystem.MEMBERSHIP.getAbbreviation());

	for (Map<String, Object> r : results) {
	    Integer payableSeq = (Integer) r.get("payableSeq");
	    // if (payableSeq != 0) {
	    System.out.println("clientNo: " + r.get("clientNo") + ", opSeqNo: " + r.get("opSeqNo") + ", amt: " + r.get("amt") + ", payableSeq: " + payableSeq);
	    // }
	}

	System.out.println("Done.");
    }

    // @Test
    public void testFailedDD() throws SystemException {
	Integer clientNo = 811612;

	System.out.println("Transferring failed DD for client: " + clientNo);

	try {
	    membershipMgr.transferFailedDirectDebit(clientNo);
	} catch (Exception e) {
	    System.out.println("Unable to transfer failed DD");
	    e.printStackTrace();
	}

	System.out.println("Done.");
    }

    // @Test
    public void testMembershipCredit() throws ParseException, RemoteException {
	DateTime startDate = new DateTime("20/07/2011");
	DateTime endDate = new DateTime("21/07/2011");

	System.out.println("Retrieving Client vouchers between " + startDate + " and " + endDate);
	// BigDecimal total =
	// membershipMgr.getTotalInstantaneousMembershipCredit(startDate,
	// endDate);

	Map<Integer, BigDecimal> results = paymentMgr.getVouchersByRange(startDate, endDate);

	for (Integer cn : results.keySet()) {
	    System.out.println(cn + ": " + results.get(cn));
	}
    }

    // @Test
    public void verifyLedgers() throws Exception {
	// LedgerReportJob job = new LedgerReportJob();
	// job.execute(null);
	String subSystem = "GLRS";
	String baseAccount = SystemParameterVO.CREDIT_ACCOUNT.toString();
	String glEntity = "10";
	String glAccount = glEntity + baseAccount;

	Interval oneMonth = new Interval();
	try {
	    oneMonth.setMonths(1);
	} catch (Exception e1) {
	} // should never happen

	DateTime startDate = new DateTime();
	startDate = startDate.subtract(oneMonth);
	DateTime endDate = new DateTime();

	List<GLTransaction> glTxns = (List<GLTransaction>) financeMgr.getGLTransactions(startDate, endDate, startDate, endDate, subSystem, glAccount, glEntity);

	BigDecimal glTotal = BigDecimal.ZERO;
	BigDecimal rsTotal = paymentMgr.getTotalPostings(Integer.parseInt(baseAccount), startDate, endDate);

	System.out.println("GL Txns: " + glTxns.size());

	for (GLTransaction gl : glTxns) {
	    glTotal = glTotal.add(gl.getAmount());
	}

	System.out.println("GL Total: " + glTotal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
	System.out.println("RS Total: " + rsTotal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
    }

    // @Test
    public void fixAutoRenewals() throws RemoteException, ParseException {

	List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
	// membershipMgr.getAutoRenAuditEntries();
	Map<String, Object> map = new HashMap<String, Object>();
	// map.put("clientNo", new Integer(414442));
	results.add(map);

	/*
	 * Interval oneDay = new Interval(); oneDay.addDays(1);
	 */
	int count = 0;
	DateTime txnDate = new DateTime("1/5/2012");

	MembershipVO memVO, prevMemVO;
	User user = SecurityHelper.getRoadsideUser();
	System.out.println("clientNo: paymentMethodID");

	for (Map<String, Object> m : results) {
	    count++;

	    memVO = membershipMgr.findMembershipByClientAndType((Integer) m.get("clientNo"), MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
	    Integer clientNo = memVO.getClientNumber();

	    MembershipTransactionVO lastTxn = memVO.getLastTransaction();

	    /*
	     * if (!lastTxn.getTransactionTypeCode().equals("Renew")) {
	     * System.out.println(count + ": " + clientNo +
	     * ": last txn was not Renew, intervention required, skipping.");
	     * continue; }
	     */
	    if (lastTxn.getTransactionDate().equals(txnDate)) {
		System.out.println(count + ": " + clientNo + ": already repaired, skipping.");
		continue;
	    }

	    DirectDebitSchedule ddSched = memVO.getCurrentDirectDebitSchedule();
	    prevMemVO = (MembershipVO) lastTxn.getMembershipHistory();
	    prevMemVO.setMode(ValueObject.MODE_NORMAL); // mark as not history
	    String payMethod = null;

	    if (ddSched != null) {
		payMethod = ddSched.getPaymentMethodID();
	    }

	    System.out.println(count + ": " + clientNo + ": " + payMethod);

	    TransactionGroup transGroup = TransactionGroup.getRenewalTransactionGroup(prevMemVO, user, prevMemVO.getExpiryDate(), txnDate, payMethod, false);

	    memTransMgr.processAutoRenewalTransaction(transGroup);

	    /*
	     * if (count >= 10) { System.out.println(count + ": " +
	     * "Hit hard limit, exiting."); break; }
	     */
	}

    }

    // @Test
    public void lookupOiPayables() throws RemoteException, ParseException, SystemDataException {

	OCRAdapter ocrAdapter = new OCRAdapter();
	/*
	 * Collection<OCRPayable> oipayables =
	 * ocrAdapter.getOutstandingPayables(811425);
	 * 
	 * for (OCRPayable payable : oipayables) { if
	 * (payable.getDescription().indexOf("NEW") != -1) {
	 * System.out.println(payable.getSequenceNumber() + "," +
	 * payable.getDescription() + "," + payable.getAmountOutstanding() + ","
	 * + payable.getProductCode() + "," + payable.getClientNumber()); } }
	 * 
	 * Collection<PayableItemVO> payables = ocrAdapter.getPayables(new
	 * DateTime("1/4/2013"), new DateTime(), true, "INS"); for
	 * (PayableItemVO payable : payables) { if
	 * (payable.getDescription().indexOf("NEW") != -1) {
	 * System.out.println(payable.getPayableItemID() + "," +
	 * payable.getDescription() + "," + payable.getAmountOutstanding() + ","
	 * + payable.getProductCode() + "," + payable.getClientNo()); } }
	 */

	Payable payable = ocrAdapter.getPayable("811425/7", "INS");
	System.out.println(payable.getSequenceNumber() + "," + payable.getDescription() + "," + payable.getAmountOutstanding() + "," + payable.getProductCode() + "," + payable.getClientNumber());

    }

    // @Test
    public void testPaymentFileGenerator() throws JobExecutionException {
	PaymentFileGeneratorJob job = new PaymentFileGeneratorJob();
	job.execute(null);
    }

    @Test
    public void testCreateJournals() throws RemoteException, PaymentException, ParseException {
	DateTime effectiveDate = new DateTime();
	Vector v = financeMgr.createJournals(SourceSystem.MEMBERSHIP, effectiveDate, null);
	for (int i = 0; i < v.size(); i++) {
	    System.out.println(v.get(i));
	}
	System.out.println("isBalanced=" + FinanceHelper.isBalanced(v));
    }

}
