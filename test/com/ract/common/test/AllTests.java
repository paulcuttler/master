package com.ract.common.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase {

    public AllTests(String s) {
	super(s);
    }

    public static Test suite() {
	TestSuite suite = new TestSuite();
	// suite.addTestSuite(com.ract.membership.test.TestMembershipTransaction.class);
	// suite.addTestSuite(com.ract.membership.test.TestMembership.class);
	// suite.addTestSuite(com.ract.common.integration.test.TestSystemIntegration.class);
	suite.addTestSuite(com.ract.common.test.TestCommon.class);
	return suite;
    }
}
