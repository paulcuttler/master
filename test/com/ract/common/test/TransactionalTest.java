package com.ract.common.test;

import static org.junit.Assert.assertNotNull;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;

public class TransactionalTest {
	
	private UserTransaction tx;
	
	@Before
	public void setupTransaction() {
		try {
			
			final InitialContext ctx = new InitialContext();
			assertNotNull("The InitialContext is null", ctx);
			
			this.tx = (UserTransaction) ctx.lookup("java:UserTransaction");
            assertNotNull("The SessionContext got from java:UserTransaction is null", tx);
            
            this.tx.begin();
            
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDownTransaction() {
		try {
			
			if(this.tx != null) {
				this.tx.rollback();
			}
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		};
	}
	
}
