package com.ract.common.test;

//import junit.framework.*;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.TransactionGroup;
import com.ract.security.SecurityHelper;

/**
 * Test frame work.
 * 
 * @todo delete when satisfied it is working.
 * 
 * @author jyh
 * @version 1.0
 */

public class TestHarness
// extends TestCase
{

    public TestHarness(String s) {
	// super(s);
    }

    // public static Test suite()
    // {
    // TestSuite suite = new TestSuite();
    // suite.addTest(new TestHarness("testTransactionGroup"));
    // return suite;
    // }

    public void testTransactionGroup() {
	int mem1ID = 80;
	int mem2ID = 81;

	try {
	    MembershipMgr mMgr = MembershipEJBHelper.getMembershipMgr();
	    MembershipVO mem1VO = mMgr.getMembership(new Integer(mem1ID));
	    MembershipVO mem2VO = mMgr.getMembership(new Integer(mem2ID));

	    TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP, SecurityHelper.getSystemUser());
	    // add the members
	    transGroup.addSelectedClient(mem1VO);
	    transGroup.addSelectedClient(mem2VO);
	    transGroup.setContextClient(mem1VO);

	    // Get the personal membership type and set in on the transaction
	    // group.
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    MembershipTypeVO memTypeVO = refMgr.getMembershipType(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
	    transGroup.setMembershipType(memTypeVO);

	    ProductVO productVO = null;
	    productVO = mem1VO.getProduct();

	    transGroup.setSelectedProduct(productVO);

	    transGroup.createTransactionsForSelectedClients();
	    // transGroup.createCreditDiscounts();
	    transGroup.calculateTransactionFee();

	    Collection transList = transGroup.getTransactionList();
	    if (transList != null && !transList.isEmpty()) {

		Iterator transListIT = transList.iterator();
		while (transListIT.hasNext()) {
		    MembershipTransactionVO memTransVO = (MembershipTransactionVO) transListIT.next();
		    MembershipVO memVO = memTransVO.getNewMembership();
		    // assertEquals(new
		    // Double(memTransVO.getAmountPayable()),new Double(34.45));
		}
	    }
	} catch (RemoteException ex) {
	    // this.fail();
	}

    }

}
