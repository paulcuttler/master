package com.ract.web.membership.roadside;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ract.util.DateTime;

public class TransactionBean {
	
	EntityManager em;
	
	
	public Collection getRoadserviceWebMembershipTransactions()
	{		
	  Query query = em.createQuery("select e from WebMembershipTransaction e where e.cadCreated is null and e.webClientNo > 0 and e.joinDate > ?1");
	  
	  Calendar cal = Calendar.getInstance();
	  cal.setTime(new Date());
	  cal.add(Calendar.MONTH, -6);
			  
	  query.setParameter(1, new DateTime(cal.getTime()));

	  
	  return query.getResultList();
	}
}
