package com.ract.web.membership.roadside;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class GetRoadsideTransactionsTest {
	
	TransactionBean trBean;
	EntityManagerFactory emf;
	EntityManager em;
	
	@Before
	public void Setup() {
		Map<String, Object> configOverrides = new HashMap<String, Object>();
		configOverrides.put("javax.persistence.jtaDataSource", "");
		configOverrides.put("javax.persistence.provider", "org.hibernate.ejb.HibernatePersistence");
		configOverrides.put("hibernate.connection.driver_class", "net.sourceforge.jtds.jdbc.Driver");
		configOverrides.put("hibernate.connection.url", "jdbc:jtds:sqlserver://torana:1433;databasename=ract_web_dev;tds=8.0");
		configOverrides.put("hibernate.connection.user", "webuser");
		configOverrides.put("hibernate.connection.password", "webuser");
		configOverrides.put("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
				
		this.emf = Persistence.createEntityManagerFactory("ractPublicEJB", configOverrides);
		this.em = emf.createEntityManager();

		this.trBean = new TransactionBean();
		trBean.em = em;
	}
	
	@After 
	public void TearDown() {
		this.em.close();
		this.emf.close();
	}
	
	@Test
	public void testGetTransactions() {
		Collection c = trBean.getRoadserviceWebMembershipTransactions();
		System.out.println(c.size());
	}
}
