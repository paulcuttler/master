package com.ract.membership.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ract.membership.importer.cmo.CMOFile;

public class CMOFileTest {

	@Test
	public void createFromValidFileName() {
		String path = "/src/master/resources/test/data/valid_fcm.csv";
		CMOFile file = new CMOFile(path);
		assertEquals("Expect number of lines to be 6", file.lineCount(), new Integer(6));
	}
	
}
