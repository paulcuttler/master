package com.ract.membership.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.test.TransactionalTest;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.importer.cmo.ActionNew;
import com.ract.membership.importer.cmo.CMOLineItem;

public class ActionNewTest extends TransactionalTest  {
	
	private static final String line = "02,A00966,N,FCM,FORDCM,11,2017-01-23,,MPB3XXMXC3GD49166,FOCUS  2016.75  5 DOOR HA  TREND  .  1.5L PET  6SPD AUTO,FORD,FOCUS,HATCHBACK,TREND,,SILVER,,2017,F56RD,P,MR,GRAEME,HITCHCOCK,,,1939-11-05,0362287778,,,,N,Y,TAS,Y,0018310,,20 McGuinness Cres,,LENAH VALLEY,TAS,7008,,,,,,14025956,1,CLB,C";
	private CMOLineItem validLine;
	private ClientMgr clientMgr;
	private MembershipMgr memMgr;
	
	@Before 
	public void setup() {
		String[] line = readLine(ActionNewTest.line);
		this.validLine = new CMOLineItem(line);
		this.validLine.validateLine("FCM");
		this.clientMgr = ClientEJBHelper.getClientMgr();
		this.memMgr = MembershipEJBHelper.getMembershipMgr();
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void testActionNew() {
		ActionNew a = new ActionNew(this.clientMgr, this.memMgr);
		
		a.execute(this.validLine);
		
		assertFalse(a.hasError());
	}
	
	private String[] readLine(String in) {
		try {
			CSVReader reader = new CSVReader(new InputStreamReader(new ByteArrayInputStream(in.getBytes("UTF-8")) ));
			return reader.readNext();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		} catch (IOException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		}
		
	}
}
