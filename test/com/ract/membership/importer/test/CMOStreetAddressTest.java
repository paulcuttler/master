package com.ract.membership.importer.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.membership.importer.cmo.CMOStreetAddress;

public class CMOStreetAddressTest {
	
	
	@Before
	public void setup() {
		
	}
	
	@After 
	public void tearDown() {
		
	}
	
	@Test
	public void testCreateStreetAddress() {
		CMOStreetAddress st = new CMOStreetAddress("20 McGuinness Cres");
		
		assertNotNull(st);
		assertEquals(st.getStreetNumber(), "20");
		assertEquals(st.getStreetName(), "McGuinness Cres");
		assertEquals(st.getStreetType(), "CRES");
	}
	
}
