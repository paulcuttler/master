package com.ract.membership.importer.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientEJBHelper;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.test.TransactionalTest;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.importer.cmo.ActionFactory;
import com.ract.membership.importer.cmo.Importer;

public class ImporterTest extends TransactionalTest {

	
	private ActionFactory af;
	private CommonMgr cmnMgr;
	
	@Before 
	public void setup() {
		this.cmnMgr = CommonEJBHelper.getCommonMgr();
		this.af = new ActionFactory(
				ClientEJBHelper.getClientMgr(),
				this.cmnMgr,
				MembershipEJBHelper.getMembershipMgr(),
				MembershipEJBHelper.getMembershipRefMgr()
		);
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void testAssertEquals() {
		String path = "/src/master/resources/test/data/20180509_RACT_SMY.csv";
		
		Importer importer = new Importer(this.cmnMgr, this.af);
		
		importer.execute(path);

		assertFalse(importer.hasErrorLog());
	}
	
}
