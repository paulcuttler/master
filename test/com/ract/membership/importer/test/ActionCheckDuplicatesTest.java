package com.ract.membership.importer.test;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.test.TransactionalTest;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.importer.cmo.ActionCheckDuplicates;
import com.ract.membership.importer.cmo.ActionGeneral;
import com.ract.membership.importer.cmo.CMOClient;
import com.ract.membership.importer.cmo.CMOLineItem;
import com.ract.membership.importer.cmo.CMOMembership;

public class ActionCheckDuplicatesTest extends TransactionalTest {
	
	private static final String line1 = "02,A00966,N,FCM,FORDCM,11,2017-01-23,,MPB3XXMXC3GD49166,FOCUS  2016.75  5 DOOR HA  TREND  .  1.5L PET  6SPD AUTO,FORD,FOCUS,HATCHBACK,TREND,,SILVER,,2017,F56RD,P,MR,LKAJHF,GFKJSDEHK,,,1939-11-05,0362287778,,,,N,Y,TAS,Y,0018310,,20 McGuinness Cres,,LENAH VALLEY,TAS,7008,,,,,,14025956,1,CLB,C";
	private static final String line2 = "02,A00966,N,FCM,FORDCM,11,2017-01-23,,MPB3XXMXC3GD49166,FOCUS  2016.75  5 DOOR HA  TREND  .  1.5L PET  6SPD AUTO,FORD,FOCUS,HATCHBACK,TREND,,SILVER,,2017,F56RD,P,MR,LKAJHF,GFKJSDEHK,,,1939-11-05,0362287778,,,,N,Y,TAS,Y,0018310,,20 McGuinness Cres,,LENAH VALLEY,TAS,7008,,,,,,14025956,1,CLB,C";

	private CMOLineItem validLine;
	private CMOLineItem validLine2;
	
	private MembershipRefMgr refMgr;
	private MembershipMgr memMgr;
	private ClientMgr clientMgr;
	
	@Before 
	public void setup() {
		String[] line = readLine(ActionCheckDuplicatesTest.line1);
		this.validLine = new CMOLineItem(line);
		this.validLine.validateLine("FCM");
		
		String[] line2 = readLine(ActionCheckDuplicatesTest.line2);
		this.validLine2 = new CMOLineItem(line2);
		this.validLine2.validateLine("FCM");
		
		this.memMgr = MembershipEJBHelper.getMembershipMgr();
		this.refMgr = MembershipEJBHelper.getMembershipRefMgr();
		this.clientMgr = ClientEJBHelper.getClientMgr();
		
		try {
			
			CMOClient cl = new CMOClient(this.clientMgr, validLine);
			cl.create();
			
			CMOMembership mem = CMOMembership.FindByVin(this.memMgr, this.validLine.get(CMOLineItem.FIELD_VIN), false);
			mem.create(cl.getClientNumber(), this.validLine);
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test 
	public void testWhenDuplicates() {
		try {
			CMOClient cl = new CMOClient(this.clientMgr, validLine);
			cl.forceCreate();
			
			CMOMembership mem = CMOMembership.FindByVin(this.memMgr, this.validLine.get(CMOLineItem.FIELD_VIN), false);
			mem.create(cl.getClientNumber(), this.validLine);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		ActionGeneral a = new ActionCheckDuplicates(this.clientMgr);
		
		a.execute();
		
		assertEquals(new Integer(a.error().size()), new Integer(0));
		assertEquals(new Integer(a.result().size()), new Integer(1));
	}
	
	@Test 
	public void testWhenNoDuplicates() {
		ActionGeneral a = new ActionCheckDuplicates(this.clientMgr);
		
		a.execute();
		
		assertEquals(new Integer(a.error().size()), new Integer(0));
		assertEquals(new Integer(a.result().size()), new Integer(0));
	}
	
	private String[] readLine(String in) {
		try {
			CSVReader reader = new CSVReader(new InputStreamReader(new ByteArrayInputStream(in.getBytes("UTF-8")) ));
			return reader.readNext();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		} catch (IOException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		}
		
	}
}
