package com.ract.membership.importer.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.importer.cmo.Action;
import com.ract.membership.importer.cmo.ActionCancellation;
import com.ract.membership.importer.cmo.ActionChangeDetail;
import com.ract.membership.importer.cmo.ActionChangeOwner;
import com.ract.membership.importer.cmo.ActionFactory;
import com.ract.membership.importer.cmo.ActionNew;
import com.ract.membership.importer.cmo.ActionService;
import com.ract.membership.importer.cmo.ActionType;

public class ActionFactoryTest {
	
	private ClientMgr clientMgr;
	private CommonMgr commonMgr;
	private MembershipMgr membershipMgr;
	private MembershipRefMgr membershipRegMgr;
	
	@Before
	public void setup() {
		this.clientMgr = ClientEJBHelper.getClientMgr();
		this.commonMgr = CommonEJBHelper.getCommonMgr();
		this.membershipMgr = MembershipEJBHelper.getMembershipMgr();
		this.membershipRegMgr = MembershipEJBHelper.getMembershipRefMgr();
	}
	
	@After 
	public void tearDown() {
		
	}
	
	@Test
	public void testCreateFactory() {
		ActionFactory af = new ActionFactory(
					this.clientMgr,
					this.commonMgr,
					this.membershipMgr,
					this.membershipRegMgr
				);
		
		assertNotNull(af);
	}
	
	@Test
	public void testCreateActionNew() {
		ActionFactory af = new ActionFactory(
				this.clientMgr,
				this.commonMgr,
				this.membershipMgr,
				this.membershipRegMgr
			);
		
		
		try {
			Action a = af.build(ActionType.TRANS_TYPE_NEW_VEHICLE, "");
			assertTrue(a.getClass().toString() + " is equal to " + ActionNew.class.toString()
					   + " for type " + ActionType.TRANS_TYPE_NEW_VEHICLE, 
					   a.getClass().isAssignableFrom(ActionNew.class));   
		} catch(Exception e) {
			assertNull("failed to get action type for: " + e, e);
		}
	
	}
	
	@Test
	public void testCreateActionDemo() {
		ActionFactory af = new ActionFactory(
				this.clientMgr,
				this.commonMgr,
				this.membershipMgr,
				this.membershipRegMgr
			);
		
		try {
			Action a = af.build(ActionType.TRANS_TYPE_DEMO_VEHICLE, "");
			assertTrue(a.getClass().toString() + " is equal to " + ActionNew.class.toString()
					   + " for type " + ActionType.TRANS_TYPE_DEMO_VEHICLE, 
					   a.getClass().isAssignableFrom(ActionNew.class));   
		} catch(Exception e) {
			assertNull("failed to get action type for: " + e, e);
		}

	}
	
	@Test
	public void testCreateActionChangeDetail() {
		ActionFactory af = new ActionFactory(
				this.clientMgr,
				this.commonMgr,
				this.membershipMgr,
				this.membershipRegMgr
			);
		
		try {
			Action a = af.build(ActionType.TRANS_TYPE_CHANGE_DETAIL, "");
			assertTrue(a.getClass().toString() + " is equal to " + ActionNew.class.toString()
					   + " for type " + ActionType.TRANS_TYPE_CHANGE_DETAIL, 
					   a.getClass().isAssignableFrom(ActionChangeDetail.class));   
		} catch(Exception e) {
			assertNull("failed to get action type for: " + e, e);
		}
	}
	
	
	@Test
	public void testCreateActionChangeOwner() {
		ActionFactory af = new ActionFactory(
				this.clientMgr,
				this.commonMgr,
				this.membershipMgr,
				this.membershipRegMgr
			);
		
		try {
			Action a = af.build(ActionType.TRANS_TYPE_CHANGE_OWNER, "");
			assertTrue(a.getClass().toString() + " is equal to " + ActionNew.class.toString()
					   + " for type " + ActionType.TRANS_TYPE_CHANGE_OWNER, 
					   a.getClass().isAssignableFrom(ActionChangeOwner.class));   
		} catch(Exception e) {
			assertNull("failed to get action type for: " + e, e);
		}
	}
	
	@Test
	public void testCreateActionCancellation() {
		ActionFactory af = new ActionFactory(
				this.clientMgr,
				this.commonMgr,
				this.membershipMgr,
				this.membershipRegMgr
			);
		
		try {
			Action a = af.build(ActionType.TRANS_TYPE_CANCELLATION, "");
			assertTrue(a.getClass().toString() + " is equal to " + ActionNew.class.toString()
					   + " for type " + ActionType.TRANS_TYPE_CANCELLATION, 
					   a.getClass().isAssignableFrom(ActionCancellation.class));   
		} catch(Exception e) {
			assertNull("failed to get action type for: " + e, e);
		}
	}
	
	@Test
	public void testCreateActionService() {
		ActionFactory af = new ActionFactory(
				this.clientMgr,
				this.commonMgr,
				this.membershipMgr,
				this.membershipRegMgr
			);
		
		try {
			Action a = af.build(ActionType.TRANS_TYPE_SERVICE, "");
			assertTrue(a.getClass().toString() + " is equal to " + ActionNew.class.toString()
					   + " for type " + ActionType.TRANS_TYPE_SERVICE, 
					   a.getClass().isAssignableFrom(ActionService.class));   
		} catch(Exception e) {
			assertNull("failed to get action type for: " + e, e);
		}
	}
}
