package com.ract.membership.importer.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.test.TransactionalTest;
import com.ract.membership.importer.cmo.CMOClient;
import com.ract.membership.importer.cmo.CMOLineItem;

public class CMOClientTest extends TransactionalTest {
	
	private String line  = "02,A00966,N,FCM,FORDCM,11,2017-01-23,,MPB3XXMXC3GD49166,FOCUS  2016.75  5 DOOR HA  TREND  .  1.5L PET  6SPD AUTO,FORD,FOCUS,HATCHBACK,TREND,,SILVER,,2017,F56RD,P,MR,GRAEME,HITCHCOCK,,,1939-11-05,0362287778,,,,N,Y,TAS,Y,0018310,,20 McGuinness Cres,,LENAH VALLEY,TAS,7008,,,,,,14025956,1,CLB,C";
	private String line2 = "02,A00966,N,FCM,FORDCM,11,2017-01-23,,MPB3XXMXC3GD49166,FOCUS  2016.75  5 DOOR HA  TREND  .  1.5L PET  6SPD AUTO,FORD,FOCUS,HATCHBACK,TREND,,SILVER,,2017,F56RD,P,MRS,GRAEME,HITCHCOCK,,,1939-11-05,0362287778,,,,N,Y,TAS,Y,0018310,,20 McGuinness Cres,,LENAH VALLEY,TAS,7008,,,,,,14025956,1,CLB,C";
	
	private CMOLineItem validLine;
	private CMOLineItem validLine2;
	
	private ClientMgr mgr;
	

	@Before
	public void setup() {
		String[] line = readLine(this.line);
		String[] line2 = readLine(this.line2);
		
		this.validLine = new CMOLineItem(line);
		this.validLine2 = new CMOLineItem(line2);
		
		this.mgr = ClientEJBHelper.getClientMgr();
		
		validLine.validateLine("FCM");
		validLine2.validateLine("FCM");
	}
	
	@After 
	public void tearDown() {
		this.validLine = null;
		this.mgr = null;
	}
	
	@Test
	public void testCreateEmptyClient() {
		CMOClient client = new CMOClient(mgr, validLine);
		
		assertNotNull(client);
		assertNotNull(client.get());
		assertNotNull(client.get().getBirthDate());
		
		try {
			assertFalse(client.existsRemote());
		} catch (RemoteException e) {
			e.printStackTrace();
			assertNull(e);
		}
	}
	
	@Test
	public void createNewClient() {
		CMOClient client = new CMOClient(mgr, validLine);
		
		try {
			client.create();
		} catch (RemoteException e) {
			e.printStackTrace();
			assertNull(e);
		}
	}

	@Test
	public void compareClients() {
		CMOClient client = new CMOClient(mgr, validLine);
		
		try {
			client.getChangeLog(validLine2);
		} catch (RemoteException e) {
			e.printStackTrace();
			assertNull(e);
		}
	}
	
	private String[] readLine(String in) {
		try {
			CSVReader reader = new CSVReader(new InputStreamReader(new ByteArrayInputStream(in.getBytes("UTF-8")) ));
			return reader.readNext();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		} catch (IOException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		}
		
	}
}
