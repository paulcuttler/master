package com.ract.membership.importer.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.junit.Test;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.membership.importer.cmo.CMOLineItem;
import com.ract.membership.importer.cmo.CMOValidationError;

public class CMOLineItemTest {

	private static String validCMOLineItem = "02,A00966,N,FCM,FORDCM,11,2017-01-23,,MPB3XXMXC3GD49166,FOCUS  2016.75  5 DOOR HA  TREND  .  1.5L PET  6SPD AUTO,FORD,FOCUS,HATCHBACK,TREND,,SILVER,,2017,F56RD,P,MR,GRAEME,HITCHCOCK,,,1939-11-05,0362287778,,,,N,Y,TAS,Y,0018310,,20 McGuinness Cres,,LENAH VALLEY,TAS,7008,,,,,,14025956,1,CLB,C";
	private static String invalidCMOLineItem_ProductCode = "02,A00966,N,SDC,FORDCM,11,2017-01-23,,MPB3XXMXC3GD49166,FOCUS  2016.75  5 DOOR HA  TREND  .  1.5L PET  6SPD AUTO,FORD,FOCUS,HATCHBACK,TREND,,SILVER,,2017,F56RD,P,MR,GRAEME,HITCHCOCK,,,1939-11-05,0362287778,,,,N,Y,TAS,Y,0018310,,20 McGuinness Cres,,LENAH VALLEY,TAS,7008,,,,,,14025956,1,CLB,C";

	@Test
	public void createCMOLineItem() {
		String[] line = readLine(validCMOLineItem);
		CMOLineItem l = new CMOLineItem(line);
		assertTrue("Expect a non null line item", l != null);
	}
	
	@Test 
	public void createValidCMOLineItem() {
		String[] line = readLine(validCMOLineItem);
		CMOLineItem l = new CMOLineItem(line);
		boolean success = l.validateLine("FCM");
		assertTrue("Expect a valid line item", success);
	}
	
	@Test 
	public void createInvalidCMOLineItemInvalidProductCode() {
		String[] line = readLine(invalidCMOLineItem_ProductCode);
		CMOLineItem l = new CMOLineItem(line);
		boolean success = l.validateLine("FCM");
		assertFalse("Expect a invalid line item", success);
		assertEquals("Expect to have 1 error", l.errors().size(), 1);
	}
	
	@Test 
	public void testBirthDate() {
		String[] line = readLine(validCMOLineItem);
		CMOLineItem l = new CMOLineItem(line);
		
		try {
			assertNotNull(l.getDobDate());
			assertNotNull(l.mustGetDobDate());
		} catch (CMOValidationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String[] readLine(String in) {
		try {
			CSVReader reader = new CSVReader(new InputStreamReader(new ByteArrayInputStream(in.getBytes("UTF-8")) ));
			return reader.readNext();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		} catch (IOException e) {
			e.printStackTrace();
			String[] s = null;
			return s;
		}
		
	}
	
}
