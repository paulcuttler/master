package com.ract.membership.test;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.util.DateTime;
import com.ract.util.Interval;

public class TestLedger {
    private MembershipMgr membershipMgr;

    @Before
    public void setUp() throws Exception {
	membershipMgr = MembershipEJBHelper.getMembershipMgr();
    }

    @Test
    public void testLedgers() throws ParseException, RemoteException {

	DateTime startDate = new DateTime("1/6/2011");
	DateTime endDate = startDate.add(new Interval("0:1:0:0:0:0")); // 1
								       // month
	DateTime now = new DateTime();
	BigDecimal monthlyTotal = new BigDecimal(0);

	while (endDate.before(now)) {

	    System.out.println("Looking up memberships with unbalanced credit between " + startDate + " and " + endDate + "...");

	    Map<Integer, BigDecimal> results = membershipMgr.getMembershipsWithUnbalancedCredit(startDate, endDate);

	    for (Integer clientNo : results.keySet()) {
		System.out.println(clientNo + ": $" + results.get(clientNo));

		monthlyTotal = monthlyTotal.add(results.get(clientNo));
	    }

	    System.out.println("Monthly Total for " + startDate + ": $" + monthlyTotal);

	    startDate = startDate.add(new Interval("0:1:0:0:0:0")); // 1 month
	    endDate = startDate.add(new Interval("0:1:0:0:0:0")); // 1 month
	    monthlyTotal = new BigDecimal(0);
	}

	System.out.println("Done.");
    }

}
