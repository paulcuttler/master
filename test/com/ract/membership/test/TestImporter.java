package com.ract.membership.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.quartz.JobExecutionException;

import com.ract.membership.importer.FcmoExporter;
import com.ract.membership.importer.FcmoImporter;
import com.ract.membership.importer.FcmoImporterCardRequestRetry;
import com.ract.membership.importer.ImportMgrBean;
import com.ract.util.XMLHelper;

public class TestImporter {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testImporter() throws JobExecutionException {
	System.out.println("Start CMO Importer");
	FcmoImporter importer = new FcmoImporter();
	assertTrue(importer != null);
	importer.execute(null);
	System.out.println("End CMO Importer");
    }

//    @Test
    public void testRetryCardRequest() throws JobExecutionException {
	System.out.println("Start CMO Card Request Retry");
	FcmoImporterCardRequestRetry importerRetry = new FcmoImporterCardRequestRetry();
	assertTrue(importerRetry != null);
	importerRetry.execute(null);
	System.out.println("End CMO Card Request Retry");
    }

    // @Test
    public void testExporter() throws JobExecutionException {
	System.out.println("Start CMO Exporter");
	FcmoExporter exporter = new FcmoExporter();
	exporter.execute(null);
	System.out.println("End CMO Exporter");
    }

    // @Test
    public void testPOBox() {
	String regexp = ImportMgrBean.REGEXP_PO_BOX;

	assertTrue(Pattern.compile(regexp, Pattern.CASE_INSENSITIVE).matcher("PO BOX 686").find());
	assertTrue(Pattern.compile(regexp, Pattern.CASE_INSENSITIVE).matcher("GPO BOX 686").find());
	assertTrue(Pattern.compile(regexp, Pattern.CASE_INSENSITIVE).matcher("P O BOX 686").find());
	assertTrue(Pattern.compile(regexp, Pattern.CASE_INSENSITIVE).matcher("G P O BOX 686").find());
	assertFalse(Pattern.compile(regexp, Pattern.CASE_INSENSITIVE).matcher("686 BOX ST").find());
    }

    // @Test
    public void testXML() {
	String input = "SLEEPY'S HOBART 12/33 CAMBRIDGE PARK DR GETTING TOO LONG NOW";
	System.out.println(input.length() + ": " + input);

	String output = XMLHelper.cDATA(input);
	System.out.println(output.length() + ": " + output);

	output = XMLHelper.xmlElement("residentialAddress2", input, 50);
	System.out.println(output.length() + ": " + output);
    }
}