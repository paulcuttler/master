package com.ract.membership.test;

import java.rmi.RemoteException;
import java.text.ParseException;

import com.ract.common.SystemException;
import com.ract.common.reporting.ReportException;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.RenewalMgr;
import com.ract.membership.RenewalNoticeException;
import com.ract.util.DateTime;

public class TestEmailRenewal
{

    /**
     * @param args
     * @throws ReportException 
     * @throws RemoteException 
     * @throws SystemException 
     * @throws RenewalNoticeException 
     * @throws ParseException 
     */
    public static void main(String[] args) throws SystemException, RemoteException, ReportException, RenewalNoticeException, ParseException
    {
		// get the renewal manager
		RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
		DateTime dt = new DateTime("31/05/2021");
	    renewalMgr.createMembershipRenewalNotices(dt, "gzs");
//		renewalMgr.emailRenewal(378316, "j.docking@ract.com.au", false, false);

    }

}
