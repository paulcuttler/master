package com.ract.membership.test;

import java.rmi.RemoteException;

import junit.framework.TestCase;

import com.ract.common.ValueObject;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.payment.PaymentMethod;
import com.ract.util.DateTime;

/**
 * Test the transaction class
 * 
 * @author jyh
 * @version 1.0
 */
public class TestMembershipTransaction extends TestCase {
    MembershipTransactionVO membershipTransactionVO = null;

    protected void setUp() throws Exception {
	super.setUp();
	// @todo verify the constructors
	membershipTransactionVO = new MembershipTransactionVO(ValueObject.MODE_NORMAL, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP, new DateTime());
    }

    protected void tearDown() throws Exception {
	membershipTransactionVO = null;
	super.tearDown();
    }

    public void testIsSuperseding() {
	String paymentMethod = PaymentMethod.DIRECT_DEBIT;
	boolean expectedReturn = true;
	boolean actualReturn = membershipTransactionVO.isSuperseding(paymentMethod);
	assertEquals("return value", expectedReturn, actualReturn);
	// @todo fill in the test code
    }

    public void testSetTransactionGroupTypeCode() {
	// String paymentMethod = PaymentMethod.DIRECT_DEBIT;
	try {
	    membershipTransactionVO.setTransactionGroupTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP);
	} catch (RemoteException ex) {
	}
	String expectedReturn = MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP;
	String actualReturn = membershipTransactionVO.getTransactionGroupTypeCode();
	assertEquals("return value", expectedReturn, actualReturn);
	// @todo fill in the test code
    }

}
