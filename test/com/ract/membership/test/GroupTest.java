package com.ract.membership.test;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.SelectedClient;
import com.ract.membership.TransactionGroup;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;
import com.ract.util.DateTime;

public class GroupTest {

    MembershipMgr membershipMgr;
    UserMgr userMgr;
    MembershipRefMgr refMgr;
    PaymentMgr paymentMgr;
    User user;

    @Before
    public void setUp() throws Exception {
	membershipMgr = MembershipEJBHelper.getMembershipMgr();
	userMgr = UserEJBHelper.getUserMgr();
	refMgr = MembershipEJBHelper.getMembershipRefMgr();
	paymentMgr = PaymentEJBHelper.getPaymentMgr();
	user = userMgr.getUser("gzn");
    }

    // @Test
    public void deleteSingleGroups() throws Exception {
	String inputFile = "c:/temp/singleGroups.csv";

	System.out.println("Parsing file " + inputFile);

	List<Integer> groupIDList = new ArrayList<Integer>();

	CSVReader reader = null;
	try {
	    // Open the file, ignore quotes
	    reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));

	    String[] line = null;

	    // Process the EmailAddress file line by line
	    while ((line = reader.readNext()) != null) {
		// assume first col is groupID
		try {

		    int groupId = Integer.parseInt(line[0]);
		    int clientNo = Integer.parseInt(line[1]);

		    System.out.println("deleting group: " + groupId + " for client: " + clientNo);

		    membershipMgr.deleteMembershipGroup(groupId);

		} catch (Exception e) {
		}
	    }
	} catch (Exception e) {
	    System.err.println("Unable to parse inputFile: " + inputFile + ": " + e);
	}

	System.out.println("Done.");
    }

    // @Test
    public void addMemberToExpiredGroup() {

	Integer existingGroupPAId = 585683;
	Integer existingGroupMemberId = 585682;
	Integer memberToAddId = 505262;

	try {

	    MembershipVO contextMemVO = membershipMgr.findMembershipByClientAndType(existingGroupPAId, MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);

	    TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP, user);
	    transGroup.addSelectedClient(contextMemVO);
	    transGroup.setContextClient(contextMemVO);

	    MembershipTypeVO memTypeVO = refMgr.getMembershipType(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
	    transGroup.setMembershipType(memTypeVO);

	    // add selected client
	    MembershipVO newMemVO = membershipMgr.findMembershipByClientAndType(memberToAddId, MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
	    SelectedClient selectedClient = new SelectedClient(newMemVO, new DateTime());
	    transGroup.addSelectedClient(selectedClient);

	    // add selected client
	    MembershipVO newMemVO2 = membershipMgr.findMembershipByClientAndType(existingGroupMemberId, MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
	    selectedClient = new SelectedClient(newMemVO2, new DateTime());
	    transGroup.addSelectedClient(selectedClient);

	    // set PA
	    // transGroup.setPrimeAddresseeSelectedClientKey(existingGroupPAId.toString());

	    // set product
	    String prodCode = "Ultimate";
	    ProductVO prodVO = refMgr.getProduct(prodCode);
	    transGroup.setSelectedProduct(prodVO);

	    transGroup.createTransactionsForSelectedClients();

	    /*
	     * transGroup.createTransactionFees();
	     * transGroup.setDiscretionaryDiscount(0d, "");
	     * transGroup.setDirectDebitAuthority(null, false);
	     */

	    Collection<MembershipTransactionVO> transactionList = transGroup.submit();

	    Integer masterTxnId = null;

	    for (MembershipTransactionVO trans : transactionList) {
		// System.out.println(trans);

		if (trans.getMembership().isPrimeAddressee()) {
		    masterTxnId = trans.getTransactionID();
		}

		System.out.print("Client number: " + trans.getMembership().getClientNumber() + ", group ID: ");
		if (trans.getMembership().getMembershipGroup() != null) {
		    System.out.println(trans.getMembership().getMembershipGroup().getMembershipGroupID());
		} else {
		    System.out.println("null");
		}

	    }

	    // undo
	    /*
	     * UndoTransactionGroup undoTransGroup = new
	     * UndoTransactionGroup(masterTxnId, null); undoTransGroup.submit();
	     */

	} catch (RemoteException e) {
	    System.err.println("Unable to complete add member to expired group test: " + e);
	    e.printStackTrace();
	}

    }

    @Test
    public void testMembershipInGroup() throws RemoteException {

	Integer membershipId = 137790;

	Collection results = membershipMgr.findMembershipGroupsByMembershipID(membershipId);

	System.out.println("Found group of size " + results.size() + " for membershipId: " + membershipId);
    }
}
