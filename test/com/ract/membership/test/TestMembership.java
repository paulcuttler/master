package com.ract.membership.test;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;

import junit.framework.TestCase;

import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.membership.MembershipDocument;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipVO;

public class TestMembership extends TestCase {
    MembershipVO membershipVO = null;

    MembershipMgr membershipMgr;
    Integer clientNumber = new Integer(432002);

    protected void setUp() throws Exception {
	super.setUp();
	// get the membership
	RACTPropertiesProvider.setContextURL("localhost", 1099);
	membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection membershipList = membershipMgr.findMembershipByClientNumber(clientNumber);
	if (membershipList != null && membershipList.size() == 1) {
	    membershipVO = (MembershipVO) membershipList.iterator().next();
	}
	if (membershipVO == null) {
	    throw new Exception("Membership '" + clientNumber + "' is null.");
	}
    }

    protected void tearDown() throws Exception {
	membershipVO = null;
	super.tearDown();
    }

    public void testGetCurrentPayableItemList() throws RemoteException {
	int expectedReturn = 1; // 1 current tx
	int actualReturn = membershipVO.getCurrentPayableItemList().size();
	assertEquals("return value", expectedReturn, actualReturn);
    }

    public void testGetIndividualAmountOutstanding() throws RemoteException {
	double expectedReturn = 0; // 1 current tx
	double actualReturn = membershipVO.getIndividualAmountOutstanding().getAmountOutstanding().doubleValue();
	assertEquals("return value", expectedReturn, actualReturn, 0);
    }

    public void testERenewalDocuments() throws RemoteException {
	List<MembershipDocument> docList = membershipMgr.getERenewalDocumentList(membershipVO.getMembershipID());
	assertEquals(1, docList.size());
    }

}
