package com.ract.membership.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CheckDigit;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;

public class TestCards {

    private MembershipMgr membershipMgr;
    private ClientMgr clientMgr;
    private Map<String, String> productLookup;

    @Before
    public void setUp() throws Exception {
	membershipMgr = MembershipEJBHelper.getMembershipMgr();
	clientMgr = ClientEJBHelper.getClientMgr();

	productLookup = new HashMap<String, String>();
	productLookup.put("1", "Advantage");
	productLookup.put("2", "Ultimate");
	productLookup.put("3", "Non-Motoring");
	productLookup.put("4", "Access");
    }

    public void validateCard() throws Exception {
	String cardNum = "3084070104509100";

	boolean valid = CheckDigit.validateCardNumber(cardNum);

	assertTrue(valid);
    }

    @Test
    public void lookupClientByCard() throws RemoteException {

	ClientVO client = clientMgr.getClientByMembershipCardNumber("3084070104320029");

	assertTrue(client.getSurname().equals("NEWTON"));

    }

    // @Test
    public void testCards() throws IOException {
	/*
	 * String filePath = "u:\\export\\mem"; File filePathDir = new
	 * File(filePath);
	 * 
	 * File logFile = new File(filePath + "\\" + DateUtil.formatYYYYMMDD(new
	 * DateTime()) + "-card-audit.log");
	 * 
	 * IOFileFilter cardFiles =
	 * FileFilterUtils.and(FileFilterUtils.prefixFileFilter("memcard"),
	 * FileFilterUtils.suffixFileFilter(".txt"));
	 * 
	 * Collection<File> list = FileUtils.listFiles(filePathDir, cardFiles,
	 * null);
	 * 
	 * System.out.println("Found " + list.size() + " card files");
	 * 
	 * Map<String,String> record = new HashMap<String,String>();
	 * MembershipCardVO cardVO = null; DateTime expiryDate = null; DateTime
	 * issueDate = null; String issueDateString = null; String reasonType =
	 * "MEMCARDREQ"; String reasonCode = "CardNotRecd";
	 * 
	 * for (File f : list) {
	 * 
	 * int start = f.getName().length() - 15; int end = start + 8;
	 * System.out.println("Checking card file: " + f.getName() + "...");
	 * 
	 * issueDateString = f.getName().substring(start, end);
	 * System.out.println("date string: " + issueDateString); try {
	 * issueDate = new DateTime(issueDateString.substring(0,2) + "/" +
	 * issueDateString.substring(2,4) + "/" +
	 * issueDateString.substring(4,8)); } catch (ParseException e) {
	 * System.out.println("Unable to parse issue date from file name: " +
	 * f.getName() + ", skipping"); continue; }
	 * 
	 * List<String> lines = FileUtils.readLines(f); for (String line :
	 * lines) { // Y|MRS AJ OCKENDEN|Mrs Ockenden||18 HAMEL ST|MOONAH TAS
	 * 7009|2|3|42595|P| |3084070200425953|MRS ANGELA J
	 * OCKENDEN|0042595|1976|%B3084070200425953^OCKENDEN/ANGELA
	 * JANE.MRS^0112711231976?|3084070200425953=01127112300000000000|0|
	 * 
	 * if (!line.contains("|")) { continue; }
	 * 
	 * String parts[] = line.split("\\|"); record.put("productId",
	 * parts[6]); record.put("productCode", productLookup.get(parts[6]));
	 * record.put("tier", parts[7]); record.put("clientNo", parts[8]);
	 * record.put("cardNo", parts[11]); record.put("issueNo",
	 * parts[11].substring(6, 7)); // 7th number record.put("cardName",
	 * parts[12]); record.put("memberSince", parts[14]);
	 * 
	 * cardVO =
	 * membershipMgr.getMembershipCardByCardNumber(record.get("cardNo"));
	 * 
	 * if (cardVO == null) { List<MembershipVO> membershipList =
	 * (List<MembershipVO>)
	 * membershipMgr.findMembershipByClientNumber(Integer
	 * .parseInt(record.get("clientNo")));
	 * 
	 * if (membershipList == null || membershipList.isEmpty() ||
	 * membershipList.size() > 1) {
	 * System.out.println("Could not find unique membership by clientNo: " +
	 * record.get("clientNo")); continue; }
	 * 
	 * MembershipVO memVO = membershipList.get(0); try { expiryDate =
	 * issueDate.add(new Interval(3,0,0,0,0,0,0)); } catch (Exception e) {
	 * // should never happen }
	 * 
	 * cardVO = new MembershipCardVO(memVO.getMembershipID(),
	 * Integer.parseInt(record.get("issueNo")), issueDate, reasonType,
	 * reasonCode, record.get("tier"), memVO.getJoinDate());
	 * cardVO.setProductCode(record.get("productCode"));
	 * cardVO.setCardNumber(record.get("cardNo"));
	 * cardVO.setExpiryDate(expiryDate); cardVO.setIssueDate(issueDate);
	 * cardVO.setJoinDate(memVO.getJoinDate());
	 * cardVO.setCardName(record.get("cardName")); cardVO.setUserId("COM");
	 * 
	 * System.out.println("Creating card: " + cardVO);
	 * 
	 * membershipMgr.createMembershipCard(cardVO);
	 * 
	 * FileUtils.writeStringToFile(logFile, "No card record found for " +
	 * record.get("cardNo") + ", c/n: " + record.get("clientNo") + "\n",
	 * true); } } }
	 * 
	 * FileUtils.writeStringToFile(logFile, "Done.\n", true);
	 * System.out.println("Done.");
	 */
    }

}
