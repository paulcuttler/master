package com.ract.membership.test;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.ract.common.CommonEJBHelper;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.membership.CancelExpiredRenewalDocumentsJob;
import com.ract.membership.MembershipAutoCancelJob;
import com.ract.membership.MembershipAutoRenewalRunner;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipVO;
import com.ract.util.DateTime;

public class AutoCancelTest {

    /**
     * Triggers the auto-cancel job.
     */
    // @Test
    public void testAutoCancel() {
	System.out.println("Starting Membership Auto Cancel Job...");

	JobDetail jobDetail = new JobDetail("MembershipAutoCancelJob", Scheduler.DEFAULT_GROUP, MembershipAutoCancelJob.class);
	jobDetail.setDescription("Membership Auto Cancel Job");

	SimpleTrigger trigger = new SimpleTrigger("MembershipAutoCancelTrigger", Scheduler.DEFAULT_GROUP, new Date());

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);

	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	System.out.println("Started Membership Auto Cancel Job.");
    }

    /**
     * Triggers the cancel expired renewal docs job.
     */
    // @Test
    public void testCancelExpiredRenewalDocs() {
	System.out.println("Starting Cancel Expired Renewal Docs Job...");

	JobDetail jobDetail = new JobDetail("CancelExpiredRenewalDocumentsJob", Scheduler.DEFAULT_GROUP, CancelExpiredRenewalDocumentsJob.class);
	jobDetail.setDescription("Cancel Expired Renewal Docs Job");

	SimpleTrigger trigger = new SimpleTrigger("CancelExpiredRenewalDocumentsTrigger", Scheduler.DEFAULT_GROUP, new Date());

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);

	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	System.out.println("Started Cancel Expired Renewal Docs Job.");
    }

    // @Test
    public void testSMSList() throws RemoteException {

	Collection<MembershipVO> memberList = MembershipEJBHelper.getRenewalMgr().getSMSRenewalReminderList(new DateTime());

	System.out.println("SMS recipient list: " + memberList.size());

    }

    @Test
    public void testAutoRenewal() throws ParseException {
	DateTime start = new DateTime("14/07/2014");
	DateTime end = new DateTime("16/07/2014");
	System.out.println("Running auto-renew for: " + start + " to " + end);
	MembershipAutoRenewalRunner runner = new MembershipAutoRenewalRunner("localhost", 8080, start, end);

    }

}
