package com.ract.membership.test;

import static org.junit.Assert.assertEquals;

import java.rmi.RemoteException;
import java.util.regex.Pattern;

import org.junit.Test;

import com.ract.membership.CreateVoucherAction;
import com.ract.membership.DeliveryRoundMgrBean;

public class TestEstate {

    // @Test
    public void testEstate() {
	// need to strip variations of 'ESTATE OF THE LATE', '(ESTATE OF THE
	// LATE)', 'ESTATE OF'

	String rawSurname = "HOLLIDAY ESTATE OF THE LATE";
	rawSurname = Pattern.compile(CreateVoucherAction.ESTATE_MATCHER).matcher(rawSurname).replaceAll("");
	assertEquals("HOLLIDAY", rawSurname);

	rawSurname = "HOLLIDAY (ESTATE OF THE LATE)";
	rawSurname = Pattern.compile(CreateVoucherAction.ESTATE_MATCHER).matcher(rawSurname).replaceAll("");
	assertEquals("HOLLIDAY", rawSurname);

	rawSurname = "HOLLIDAY ESTATE OF";
	rawSurname = Pattern.compile(CreateVoucherAction.ESTATE_MATCHER).matcher(rawSurname).replaceAll("");
	assertEquals("HOLLIDAY", rawSurname);
    }

    @Test
    public void testMNJSql() {

	DeliveryRoundMgrBean bean;
	try {
	    bean = new DeliveryRoundMgrBean();
	    System.out.println(bean.toString());
	} catch (RemoteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }
}
