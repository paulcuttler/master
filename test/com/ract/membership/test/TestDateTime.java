package com.ract.membership.test;

import java.util.Calendar;
import java.util.Date;

public class TestDateTime {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
 		Calendar today = Calendar.getInstance();
 		today.setTime(new Date());
 		long now = today.getTimeInMillis();
 		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		long midnight = today.getTimeInMillis();
		System.out.println(now - midnight);
			
	}

}
