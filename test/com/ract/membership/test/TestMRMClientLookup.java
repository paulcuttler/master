package com.ract.membership.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.membership.CancelExpiredRenewalDocumentsJob;
import com.ract.membership.MembershipAutoCancelJob;
import com.ract.membership.MembershipAutoRenewalRunner;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipVO;
import com.ract.util.DateTime;

public class TestMRMClientLookup {

    @Test
    public void testClientLookup() {
    	String testCardNumber = "3084070203783168";
        ClientMgr mgr= ClientEJBHelper.getClientMgr();
        try {
			ClientVO clt = mgr.getClientByMembershipCardNumber(testCardNumber);
			assertTrue(clt != null);
			assertTrue(clt.getClientNumber().equals(Integer.parseInt(testCardNumber.substring(9, 15))));
		} catch (RemoteException e) {
			fail(e.getMessage());
		}
    }

}
