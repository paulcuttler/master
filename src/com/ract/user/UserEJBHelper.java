package com.ract.user;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

/**
 * EJBHelper to get Home interface of user EJBs.
 * 
 * @author John Holliday
 * @version 1.0
 */

public class UserEJBHelper {

    public static UserMgr getUserMgr() {
	UserMgr userMgr = null;
	try {
	    userMgr = (UserMgr) ServiceLocator.getInstance().getObject("UserMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return userMgr;
    }

    public static UserMgrLocal getUserMgrLocal() {
	UserMgrLocal userMgrLocal = null;
	try {
	    userMgrLocal = (UserMgrLocal) ServiceLocator.getInstance().getObject("UserMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return userMgrLocal;
    }

}
