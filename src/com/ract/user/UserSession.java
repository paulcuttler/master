package com.ract.user;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.ract.security.SecurityEJBHelper;
import com.ract.security.SecurityMgr;
import com.ract.security.ui.LoginUIConstants;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * Holds a user's session details.
 * 
 * @author John Holliday
 * @created 31 July 2002
 * 
 * @version 1.0
 */

public class UserSession implements Serializable, HttpSessionBindingListener {

    private String sessionId;

    private String systemName;

    private String ip;

    private DateTime sessionStart;

    private String userId;

    // the user vo object
    private User user;

    // priv list is of the form priv|priv|priv|priv
    /**
     * Constructor for the UserSession object
     * 
     * @param userID
     *            Description of the Parameter
     * @param password
     *            Description of the Parameter
     * @param sessionId
     *            Description of the Parameter
     * @param ip
     *            Description of the Parameter
     * @param systemName
     *            Description of the Parameter
     * @param userName
     *            Description of the Parameter
     * @param privilegeList
     *            Description of the Parameter
     * @param sessionStart
     *            Description of the Parameter
     * @param defaultPrinter
     *            Description of the Parameter
     * @param salesBranch
     *            Description of the Parameter
     */
    public UserSession(/*
		        * String userID, String password, String sessionId,
		        * String ip, String systemName, String userName, String
		        * salesBranch, Collection privilegeList,
		        */
    DateTime sessionStart, String defaultPrinter, String emailAddress) {
	// User userVO = new User(userID, userName, password, defaultPrinter,
	// salesBranch,
	// emailAddress, privilegeList);
	// this.user = userVO;
	this.userId = userId;
	this.sessionId = sessionId;
	this.ip = ip;
	this.systemName = systemName;
	this.sessionStart = sessionStart;
    }

    public UserSession(User user) {
	this.user = user;
    }

    /**
     * Constructor for the UserSession object
     * 
     * @param user
     *            Description of the Parameter
     * @param sessionId
     *            Description of the Parameter
     * @param ip
     *            Description of the Parameter
     * @param systemName
     *            Description of the Parameter
     * @param sessionStart
     *            Description of the Parameter
     */
    public UserSession(User user, String sessionId, String ip, String systemName, DateTime sessionStart) {
	this.userId = user.getUserID();
	this.sessionId = sessionId;
	this.ip = ip;
	this.systemName = systemName;
	this.sessionStart = sessionStart;
    }

    /**
     * Sets the sessionId attribute of the UserSession object
     * 
     * @param sessionId
     *            The new sessionId value
     */
    public void setSessionId(String sessionId) {
	this.sessionId = sessionId;
    }

    /**
     * Gets the sessionId attribute of the UserSession object
     * 
     * @return The sessionId value
     */
    public String getSessionId() {
	return sessionId;
    }

    /**
     * Sets the systemName attribute of the UserSession object
     * 
     * @param systemName
     *            The new systemName value
     */
    public void setSystemName(String systemName) {
	this.systemName = systemName;
    }

    /**
     * Gets the systemName attribute of the UserSession object
     * 
     * @return The systemName value
     */
    public String getSystemName() {
	return systemName;
    }

    /**
     * Sets the ip attribute of the UserSession object
     * 
     * @param ip
     *            The new ip value
     */
    public void setIp(String ip) {
	this.ip = ip;
    }

    /**
     * Gets the ip attribute of the UserSession object
     * 
     * @return The ip value
     */
    public String getIp() {
	return ip;
    }

    /**
     * Sets the sessionStart attribute of the UserSession object
     * 
     * @param sessionStart
     *            The new sessionStart value
     */
    public void setSessionStart(DateTime sessionStart) {
	this.sessionStart = sessionStart;
    }

    /**
     * Gets the sessionStart attribute of the UserSession object
     * 
     * @return The sessionStart value
     */
    public DateTime getSessionStart() {
	return sessionStart;
    }

    // String userID,String password,String sessionId,String ip,
    // String systemName, String userName,String branch, String privilegeList
    // DateTime sessionStart,String defaultPrinter) {

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public String toString() {
	StringBuffer sb = new StringBuffer();
	sb.append(this.getUserId());
	sb.append(FileUtil.SEPARATOR_COMMA);
	sb.append(this.getSessionId());
	sb.append(FileUtil.SEPARATOR_COMMA);
	sb.append(this.getIp());
	sb.append(FileUtil.SEPARATOR_COMMA);
	sb.append(this.getSystemName());
	sb.append(FileUtil.SEPARATOR_COMMA);
	sb.append(this.getUser().getPrivilegeList());
	sb.append(FileUtil.SEPARATOR_COMMA);
	sb.append(this.getSessionStart());
	sb.append(FileUtil.SEPARATOR_COMMA);
	// sb.append(this.getPrinterGroup());
	return sb.toString();
    }

    /**
     * Sets the user attribute of the UserSession object
     * 
     * @param user
     *            The new user value
     */
    // public void setUser(User user)
    // {
    // this.user = user;
    // }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    /**
     * Gets the user attribute of the UserSession object
     * 
     * @return The user value
     */
    public User getUser() {

	if (this.user == null) {
	    try {
		UserMgr userMgr = UserEJBHelper.getUserMgr();
		this.user = userMgr.getUser(this.getUserId());
		LogUtil.debug(this.getClass(), "getUser()" + this.user);
	    } catch (RemoteException ex) {
		LogUtil.warn(this.getClass(), "Unable to get user '" + this.getUserId() + "'." + ex);
	    }
	}
	return this.user;
    }

    public String getUserId() {
	return this.userId;
    }

    /**
     * fires when a value is bound in the session
     * 
     * @param event
     *            The HttpSessionBindingEvent event
     */
    public void valueBound(HttpSessionBindingEvent event) {
	this.logIn(this.getUser(), this.getSessionStart(), this.getIp(), this.getSystemName(), this.getSessionId());
    }

    /**
     * Description of the Method
     * 
     * @param event
     *            Description of the Parameter
     */
    public void valueUnbound(HttpSessionBindingEvent event) {
	String sessionId = event.getSession().getId();
	this.logOut(sessionId);
    }

    /**
     * Description of the Method
     * 
     * @param sessionId
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     */
    private void logOut(String sessionId) {
	try {
	    SecurityMgr sMgr = SecurityEJBHelper.getSecurityMgr();
	    sMgr.logOut(sessionId);
	} catch (Exception e) {
	    LogUtil.warn(this.getClass(), "Error: unable to remove session. " + e.toString());
	    e.printStackTrace();
	}
    }

    /**
     * Description of the Method
     * 
     * @param userVO
     *            Description of the Parameter
     * @param createTime
     *            Description of the Parameter
     * @param ipAddress
     *            Description of the Parameter
     * @param systemName
     *            Description of the Parameter
     * @param sessionId
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     */
    private void logIn(User userVO, DateTime createTime, String ipAddress, String systemName, String sessionId) {
	try {
	    SecurityMgr sMgr = SecurityEJBHelper.getSecurityMgr();
	    sMgr.logIn(sessionId, userVO.getUserID(), createTime, ipAddress, systemName);
	} catch (Exception e) {
	    LogUtil.warn(this.getClass(), "Error: unable to create session. " + e.toString());
	    e.printStackTrace();
	}
    }

    /**
     * Fetch the user session object from the session. If security is on and the
     * user session does not exist then return null. If security is not on and
     * the user session does not exist then create a user session for the
     * specified user. If the specified user does not exist the no user session
     * is returned
     */
    public static UserSession getUserSession(HttpSession session) throws RemoteException {
	UserSession userSession = (UserSession) session.getAttribute(LoginUIConstants.USER_SESSION);
	return userSession;
    }

}
