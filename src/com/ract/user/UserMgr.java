package com.ract.user;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.Remote;

/**
 * User Mgr
 * 
 * @author John Holliday
 * @version 1.0
 */
@Remote
public interface UserMgr {

    public void saveUser(User user) throws RemoteException;

    public void updateUser(User user) throws RemoteException;

    public void deleteUser(User user) throws RemoteException;

    public User getUser(String userId) throws RemoteException;

    public Collection getUsers() throws RemoteException;

    public Collection getActiveUsers() throws RemoteException;

    // public SortedSet getUpperUserIDsAndNames()
    // throws RemoteException;
}
