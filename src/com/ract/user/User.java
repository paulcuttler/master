package com.ract.user;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Vector;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.SalesBranchVO;
import com.ract.common.SystemException;
import com.ract.common.ValueObject;
import com.ract.security.SecurityEJBHelper;
import com.ract.security.SecurityMgrLocal;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * User value object.
 * 
 * @hibernate.class table="x_user" lazy="false"
 * 
 * @author John Holliday created 31 July 2002
 * @version 1.0
 */

public class User extends ValueObject {

    public static final String STATUS_ACTIVE = "Active";

    public static final String STATUS_INACTIVE = "Inactive";

    // private final String LIST_DELIMITER = "|";

    public User() {

    }

    public User(String userID, String username, String password, String printerGroup, String salesBranchCode, String emailAddress, Collection privilegeList) {
	this.userID = userID;
	this.username = username;
	this.password = password;
	this.printerGroup = printerGroup;
	this.salesBranchCode = salesBranchCode;
	this.emailAddress = emailAddress;
	this.status = User.STATUS_ACTIVE;
	this.branchNumber = 0;
	// next may be null
	this.privilegeList = privilegeList;
    }

    public User(String userID, String username, String password, String printerGroup, String salesBranchCode, String emailAddress, int branchNo, String status, Collection privilegeList) {
	this.userID = userID;
	this.username = username;
	this.password = password;
	this.printerGroup = printerGroup;
	this.salesBranchCode = salesBranchCode;
	this.emailAddress = emailAddress;
	this.branchNumber = branchNo;
	this.status = status;
	// next may be null
	this.privilegeList = privilegeList;
    }

    /**
     * User id
     */
    private String userID;

    /**
     * Print group of the user
     */
    private String printerGroup;

    private String password;

    private String status;

    private String salesBranchCode;

    private String username;

    private Collection privilegeList;

    private String emailAddress;

    /**
     * @hibernate.id column="us_user" generator-class="assigned"
     */
    public String getUserID() {
	return userID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="us_prgrp"
     */
    public String getPrinterGroup() {
	return printerGroup;
    }

    public void setPrinterGroup(String printerGroup) {
	this.printerGroup = printerGroup;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="us_password"
     */
    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[sales-branch]"
     */
    public String getSalesBranchCode() {
	return this.salesBranchCode;
    }

    public SalesBranchVO getSalesBranch() throws RemoteException {
	SalesBranchVO salesBranch = null;
	if (this.getSalesBranchCode() != null) {
	    try {
		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		salesBranch = commonMgrLocal.getSalesBranch(this.getSalesBranchCode());
	    } catch (Exception e) {
		throw new SystemException("Error getting sales branch. ", e);
	    }
	}
	return salesBranch;
    }

    public void setSalesBranch(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="us_username"
     */
    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="status"
     */
    public String getStatus() {
	return this.status;
    }

    @Override
    public String toString() {
	return "User [branchNumber=" + branchNumber + ", emailAddress=" + emailAddress + ", password=" + password + ", printerGroup=" + printerGroup + ", privilegeList=" + privilegeList + ", roles=" + roles + ", salesBranchCode=" + salesBranchCode + ", status=" + status + ", userID=" + userID + ", username=" + username + "]";
    }

    public User copy() {
	User user = new User(this.userID, this.username, this.password, this.printerGroup, this.salesBranchCode, this.emailAddress, this.branchNumber, this.status, this.privilegeList);
	return user;
    }

    public void setUserID(String userID) {
	this.userID = userID;
    }

    public void setPrivilegeList(Collection privilegeList) {
	this.privilegeList = privilegeList;
    }

    /**
     * Sets the privilegeList attribute of the UserSession object in the format
     * priv|priv|priv
     * 
     * @param privilegeList
     *            The new privilegeList value
     */
    public void setPrivilegeList(String privilegeList) {
	// StringTokenizer st = new StringTokenizer(privilegeList,
	// this.LIST_DELIMITER);
	String privList[] = privilegeList.split(FileUtil.SEPARATOR_PIPE);
	Collection c = new Vector();
	for (int i = 0; i < privList.length; i++) {
	    c.add(privList[i]);
	}
	this.privilegeList = c;
    }

    public Collection getRoles() {
	if (this.roles == null) {
	    try {
		SecurityMgrLocal securityMgrLocal = SecurityEJBHelper.getSecurityMgrLocal();
		this.roles = securityMgrLocal.getUserRoles(this.userID);
	    } catch (RemoteException e) {
		LogUtil.warn(this.getClass(), e);
	    }
	}
	return this.roles;
    }

    private Collection roles;

    public void setRoles(Collection roles) {
	this.roles = roles;
    }

    private int branchNumber;

    public Collection getPrivilegeList() {
	if (this.privilegeList == null) {
	    try {
		SecurityMgrLocal securityMgrLocal = SecurityEJBHelper.getSecurityMgrLocal();
		this.privilegeList = securityMgrLocal.getUserPrivileges(this.userID);
	    } catch (RemoteException e) {
		LogUtil.warn(this.getClass(), e);
	    }
	}
	return privilegeList;
    }

    public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
    }

    public void setBranchNumber(int branchNumber) {
	this.branchNumber = branchNumber;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="email"
     */
    public String getEmailAddress() {
	return emailAddress;
    }

    public String getUserEmailAddress() {
	String os = null;
	if (this.emailAddress != null && !this.emailAddress.equals(""))
	    os = this.emailAddress;
	else {
	    String[] sA = this.username.trim().toLowerCase().split(" ");
	    String surname = sA[sA.length - 1];
	    os = this.username.charAt(0) + "." + surname + "@ract.com.au";
	}
	return os;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[branch-no]"
     */
    public int getBranchNumber() {
	return branchNumber;
    }

    /**
     * Return true if the user has the specified privilege
     * 
     * @param priv
     *            Description of the Parameter
     * @return The privilegedUser value
     */
    public boolean isPrivilegedUser(String priv) {
	try {
	    return SecurityEJBHelper.getSecurityMgr().isPrivilegedUser(this.getUserID(), priv);
	} catch (RemoteException e) {
	    e.printStackTrace();
	    return false;
	}

	/*
	 * LogUtil.debug(this.getClass(),"user="+this.getUserID());
	 * LogUtil.debug(this.getClass(),"priv="+priv); if(priv == null) {
	 * return false; }
	 * 
	 * Privilege thePriv = null; Collection privList = getPrivilegeList();
	 * String privString = null;
	 * LogUtil.debug(this.getClass(),"privList="+privList); if(privList !=
	 * null) { Iterator privIterator = privList.iterator();
	 * while(privIterator.hasNext()) { thePriv =
	 * (Privilege)privIterator.next(); privString =
	 * thePriv.getPrivilegeID().trim();
	 * LogUtil.debug(this.getClass(),priv+"="+privString);
	 * if(privString.equalsIgnoreCase(priv)) {
	 * LogUtil.debug(this.getClass(),"matched "+priv); return true; } } }
	 * return false;
	 */
    }
}
