package com.ract.user;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ract.common.SystemException;
import com.ract.security.Role;
import com.ract.security.UserRole;
import com.ract.security.UserRolePK;
import com.ract.util.LogUtil;

/**
 * Manage user data.
 * 
 * @author John Holliday
 * @version 1.0
 */
@Stateless
@Remote({ UserMgr.class })
@Local({ UserMgrLocal.class })
public class UserMgrBean {

    @PersistenceContext(unitName = "master")
    Session hsession;

    @Resource(mappedName = "java:/ClientDS")
    private DataSource dataSource;

    public void saveUser(User user) throws RemoteException {
	LogUtil.log(this.getClass(), "saveUser start");
	try {
	    LogUtil.debug(this.getClass(), "user=" + user);
	    hsession.save(user);
	    insertRoles(user);
	} catch (HibernateException ex) {
	    throw new SystemException("Unable to save user.", ex);
	}
	LogUtil.log(this.getClass(), "saveUser end");
    }

    public void updateUser(User user) throws RemoteException {
	LogUtil.log(this.getClass(), "updateUser start");
	try {
	    LogUtil.debug(this.getClass(), "user=" + user);

	    Collection<Role> tmpRoles = user.getRoles();
	    hsession.update(user);

	    deleteRoles(user);
	    insertRoles(user, tmpRoles);
	} catch (HibernateException ex) {
	    throw new SystemException("Unable to update user", ex);
	}
	LogUtil.log(this.getClass(), "updateUser end");
    }

    public void deleteUser(User user) throws RemoteException {
	try {
	    hsession.delete(user);
	} catch (HibernateException ex) {
	    throw new SystemException(ex);
	}
    }

    /**
     * Gets all users in the system as value objects
     * 
     * more efficient than findAll
     */
    public Collection getUsers() throws RemoteException {
	ArrayList queryList = null;
	try {
	    Query crit = hsession.createQuery("from User u order by u.username");
	    queryList = (ArrayList) crit.list();
	} catch (HibernateException ex) {
	    throw new SystemException(ex);
	}
	return queryList;
    }

    public Collection getActiveUsers() throws RemoteException {
	ArrayList queryList = null;
	try {
	    Criteria crit = hsession.createCriteria(User.class).add(Expression.not(Expression.eq("status", User.STATUS_INACTIVE))).addOrder(Order.asc("username"));
	    queryList = (ArrayList) crit.list();
	} catch (HibernateException ex) {
	    throw new SystemException(ex);
	}
	return queryList;
    }

    public User getUser(String userId) throws RemoteException {
	LogUtil.debug(this.getClass(), "userId=" + userId);

	// force to lower case to avoid hibernate issue with internal
	// many-to-many joins
	userId = userId.toLowerCase();

	User user = null;
	user = (User) hsession.get(User.class, new String(userId));
	return user;
    }

    private void deleteRoles(User user) {
	LogUtil.debug(this.getClass(), "deleteRoles start");
	String hql = "delete from UserRole e where e.userRolePK.userId = :userId";
	Query query = hsession.createQuery(hql);
	query.setString("userId", user.getUserID());
	int rowCount = query.executeUpdate();
	LogUtil.debug(this.getClass(), "Deleted " + rowCount + " roles.");
	LogUtil.debug(this.getClass(), "deleteRoles end");
    }

    private void insertRoles(User user) {
	insertRoles(user, user.getRoles());
    }

    private void insertRoles(User user, Collection<Role> roles) {
	LogUtil.debug(this.getClass(), "insertRoles start");
	LogUtil.debug(this.getClass(), "roles=" + roles);
	Role role = null;
	for (Iterator<Role> i = roles.iterator(); i.hasNext();) {
	    role = i.next();
	    UserRole userRole = new UserRole();
	    UserRolePK userRolePK = new UserRolePK();
	    userRolePK.setUserId(user.getUserID());
	    userRolePK.setRoleId(role.getRoleID());
	    userRole.setUserRolePK(userRolePK);
	    hsession.save(userRole);
	}
	LogUtil.debug(this.getClass(), "insertRoles end");
    }
}
