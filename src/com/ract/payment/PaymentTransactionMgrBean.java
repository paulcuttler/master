package com.ract.payment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.CreateException;
import javax.ejb.Init;
import javax.ejb.Local;
import javax.ejb.ObjectNotFoundException;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remote;
import javax.ejb.RemoveException;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.ract.common.ClassWriter;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.XMLClassWriter;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.membership.MembershipDocument;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipHistory;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipRenewalMgrLocal;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.TransactionGroup;
import com.ract.payment.billpay.IVRPayment;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.directdebit.DirectDebitAdapter;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.payment.finance.Cheque;
import com.ract.payment.finance.FinanceAdapter;
import com.ract.payment.finance.FinancePayee;
import com.ract.payment.receipting.HistoricalOCRAdapter;
import com.ract.payment.receipting.Payable;
import com.ract.payment.receipting.Receipt;
import com.ract.payment.receipting.ReceiptingAdapter;
import com.ract.payment.receipting.ReceiptingIdentifier;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgrLocal;
import com.ract.user.UserSession;
import com.ract.util.ConnectionUtil;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * Stateful session bean to manage payment transactions.
 * 
 * @author jholliday
 */
@Stateful
@Remote({ PaymentTransactionMgr.class })
@Local({ PaymentTransactionMgrLocal.class })
public class PaymentTransactionMgrBean {

	@PersistenceContext(unitName = "master")
	private EntityManager em;

	@PersistenceContext(unitName = "master")
	Session hsession;

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource dataSource;

	/**
	 * Remove the debt record from the receipting system.
	 * 
	 * The rules for removal are delegated to the receipting adapter implementation.
	 */
	public void removePayableFee(Integer clientNo, String paymentMethodId, SourceSystem sourceSystem, String userId) throws RollBackException {
		ReceiptingAdapter receiptingAdapter = null;
		try {
			receiptingAdapter = getReceiptingAdapter(sourceSystem);
		} catch (Exception ex) {
			throw new RollBackException(ex);
		}

		LogUtil.debug(this.getClass(), "removePayableFee");
		LogUtil.debug(this.getClass(), "clientNo=" + clientNo);
		LogUtil.debug(this.getClass(), "paymentMethodId=" + paymentMethodId);
		LogUtil.debug(this.getClass(), "sourceSystem=" + sourceSystem);
		LogUtil.debug(this.getClass(), "userId=" + userId);
		receiptingAdapter.removePayableFee(clientNo, paymentMethodId, sourceSystem.getAbbreviation(), userId);
	}

	/**
	 * Remove the pending fee record so it will be deleted from IVR. Also delete the payable fee record if one exists.
	 */
	public void removePendingFee(String pendingFeeId) throws RollBackException, RemoteException {

		try {
			PendingFee pendingFee = (PendingFee) hsession.get(PendingFee.class, pendingFeeId);
			removePendingFee(pendingFee);
		} catch (HibernateException ex) {
			throw new RemoteException("Error loading pending fee.", ex);
		}
	}

	public void transferReceipts(Integer fromCustomerNumber, Integer toCustomerNumber, String userId) throws RollBackException, RemoteException {
		ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(null);
		receiptingAdapter.transferReceipts(fromCustomerNumber, toCustomerNumber, userId);
	}

	public void deleteReceiptingCustomer(String customerNumber, String userId) throws RollBackException, RemoteException {
		ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(null);
		receiptingAdapter.deleteReceiptingCustomer(new Integer(customerNumber), userId);
	}

	/**
	 * Remove the payable item. This will cascade to removing the components, postings and actions but wont remove the direct debit or receipting records
	 */
	public void removePayableItem(Integer payableItemID, UserSession userSession) throws RemoteException, RemoveException, PaymentException, ObjectNotFoundException {
		PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
		PayableItemVO payableItemVO = pMgr.getPayableItem(payableItemID);
		if (payableItemVO != null) {

			// Check for amount outstanding in the direct debit or receipting
			// system

			// getting the payment method gets the amount oustanding from the
			// direct debit system
			PaymentMethod paymentMethod = payableItemVO.getPaymentMethod();
			double amountOutstanding = payableItemVO.getAmountOutstanding();

			if (payableItemVO.getAmountPayable() > 0.0 && payableItemVO.getAmountPayable() > amountOutstanding) {
				throw new RemoveException("Cannot remove a payable item that has been paid.");
			}

			// Log the payable item to the remove log
			File removeLog = getRemoveLog();
			FileWriter fileWriter = null;
			try {
				// Append to the log file.
				fileWriter = new FileWriter(removeLog.getPath(), true);

				// Exclude some classes from the class map that is written out
				ArrayList excludedClassList = new ArrayList();

				// Create an XML class writer.
				// We need a reference to an XMLClassWriter to do the first line
				// of the XML file.
				// We need an ordinary ClassWriter to pass to the write method
				// of the VO.
				XMLClassWriter xmlClassWriter = new XMLClassWriter(fileWriter, null, excludedClassList);
				ClassWriter classWriter = (ClassWriter) xmlClassWriter;

				// If the file is being written to for the first time then write
				// the first line of the docment according to the XML standard.
				if (!removeLog.exists()) {
					xmlClassWriter.startDocument();
				}

				// Write an audit node for this payable item removal
				String userID = "";
				User user = null;
				if (userSession != null) {
					user = userSession.getUser();
					if (user != null) {
						userID = user.getUserID();
					}
				}
				fileWriter.write("<REMOVE_PAYABLEITEM userId=\"" + user == null ? "Unknown" : userID + "\" date=\"" + DateUtil.formatLongDate() + "\">\n");

				// Write the object map to the file starting at the membership.
				payableItemVO.write(classWriter);

				// Close the audit node tag
				fileWriter.write("</REMOVE_PAYABLEITEM>\n");

				// Close the file
				fileWriter.close();
			} catch (CreateException ce) {
				try {
					fileWriter.close();
				} catch (Exception e) {
				}

				throw new RemoteException("Create exception thrown when writing remove log for payable item " + payableItemVO.getPayableItemID(), ce);
			} catch (IOException ioe) {
				try {
					fileWriter.close();
				} catch (Exception e) {
				}

				throw new RemoteException("IO exception thrown when writing remove log for payable item " + payableItemVO.getPayableItemID(), ioe);
			}

			// Now remove the payable item
			deletePayableItem(payableItemVO);
		}
	}

	public void createPayableItem(PayableItemVO payableItem) throws RemoteException, PaymentException {

		if (payableItem.getPayableItemID() == null) {
			throw new RemoteException("Attempting to create a payable item with a null identifier.");
		}

		// create payable item

		hsession.save(payableItem);

		// create components
		Collection components = payableItem.getComponents();
		for (Iterator it = components.iterator(); it.hasNext();) {
			PayableItemComponentVO pic = (PayableItemComponentVO) it.next();
			pic.setPayableItemID(payableItem.getPayableItemID());
			createPayableItemComponent(pic);

			Collection postings = pic.getPostings();
			// create postings
			for (Iterator it2 = postings.iterator(); it2.hasNext();) {
				PayableItemPostingVO pip = (PayableItemPostingVO) it2.next();
				pip.setPayableItemComponentID(pic.getPayableItemComponentID());
				createPayableItemPosting(pip);
			}

		}

	}

	public void createPayableItemComponent(PayableItemComponentVO payableItemComponent) throws RemoteException, PaymentException {
		Integer payableItemComponentID = new Integer(PaymentEJBHelper.getPaymentIDMgr().getNextPayableItemComponentID());
		payableItemComponent.setPayableItemComponentID(payableItemComponentID);

		hsession.save(payableItemComponent);
	}

	public void deletePayableItemComponent(PayableItemComponentVO payableItemComponent) throws RemoteException, PaymentException {

		hsession.delete(payableItemComponent);
	}

	public void createPayableItemPosting(PayableItemPostingVO payableItemPosting) throws RemoteException, PaymentException {
		Integer payableItemPostingID = new Integer(PaymentEJBHelper.getPaymentIDMgr().getNextPayableItemPostingID());
		payableItemPosting.setPayableItemPostingID(payableItemPostingID);

		hsession.save(payableItemPosting);
	}

	public void deletePayableItemPosting(PayableItemPostingVO payableItemPosting) throws RemoteException, PaymentException {

		hsession.delete(payableItemPosting);
	}

	public void updatePayableItemPosting(PayableItemPostingVO payableItemPosting) throws RemoteException, PaymentException {

		hsession.update(payableItemPosting);
	}

	/**
	 * TRANSACTIONAL
	 * 
	 * Update the payable item.
	 */
	public void updatePayableItem(PayableItemVO payableItem) throws RemoteException, PaymentException {

		hsession.update(payableItem);
	}

	public void mergePayableItem(PayableItemVO payableItem) throws RemoteException, PaymentException {
		hsession.merge(payableItem);
	}

	public void deletePayableItem(PayableItemVO payableItem) throws RemoteException, PaymentException {

		LogUtil.log(this.getClass(), "hsession=" + hsession);
		// Changed to hsql delete due to NonUniqueException associated with
		// getting posting and component lists
		// via a query and then using delete(object) on each one. It appears
		// that the items in the list could not be tested via
		// Session.contains or evicted via Session.evict.
		String hql = null;
		Query query = null;

		Collection components = payableItem.getComponents();
		PayableItemComponentVO payableItemComponent = null;
		for (Iterator c = components.iterator(); c.hasNext();) {
			payableItemComponent = (PayableItemComponentVO) c.next();
			// delete postings
			hql = "delete from PayableItemPostingVO e where e.payableItemComponentID = :payableItemComponentId";
			query = hsession.createQuery(hql);
			query.setInteger("payableItemComponentId", payableItem.getPayableItemID());
			int rowCount = query.executeUpdate();
			LogUtil.log(this.getClass(), "posting rowCount=" + rowCount);
		}
		// delete components
		hql = "delete from PayableItemComponentVO e where e.payableItemID = :payableItemId";
		query = hsession.createQuery(hql);
		query.setInteger("payableItemId", payableItem.getPayableItemID());
		int rowCount = query.executeUpdate();
		LogUtil.log(this.getClass(), "component rowCount=" + rowCount);

		LogUtil.log(this.getClass(), "delete payable");
		hsession.delete(payableItem);
		// unable to resolve a Unique Entity exception so...
		// hql =
		// "delete from PayableItemVO e where e.payableItemID = :payableItemId";
		// query = hsession.createQuery(hql);
		// query.setInteger("payableItemId",payableItem.getPayableItemID());
		// query.executeUpdate();
	}

	/**
	 * Description of the Method
	 * 
	 * @param payableItemVO Description of the Parameter
	 * @param userSession Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 */
	private void logPayableItem(PayableItemVO payableItemVO, UserSession userSession) throws RemoteException {
		// now log the transaction to the remove log
		File removeLog = getRemoveLog();
		FileWriter fileWriter = null;
		try {
			// Append to the log file.
			fileWriter = new FileWriter(removeLog.getPath(), true);

			// Exclude some classes from the class map that is written out
			ArrayList excludedClassList = new ArrayList();

			// Create an XML class writer.
			// We need a reference to an XMLClassWriter to do the first line of
			// the XML file.
			// We need an ordinary ClassWriter to pass to the write method of
			// the VO.
			XMLClassWriter xmlClassWriter = new XMLClassWriter(fileWriter, null, excludedClassList);
			ClassWriter classWriter = (ClassWriter) xmlClassWriter;

			// If the file is being written to for the first time then write
			// the first line of the docment according to the XML standard.
			if (!removeLog.exists()) {
				xmlClassWriter.startDocument();
			}

			// Write an audit node for this payable item removal
			String userID = "";
			User user = null;
			if (userSession != null) {
				user = userSession.getUser();
				if (user != null) {
					userID = user.getUserID();
				}
			}
			fileWriter.write("<REMOVE_PAYABLEITEM userId=\"" + user == null ? "Unknown" : userID + "\" date=\"" + DateUtil.formatLongDate() + "\">\n");

			// Write the object map to the file starting at the membership.
			payableItemVO.write(classWriter);

			// Close the audit node tag
			fileWriter.write("</REMOVE_PAYABLEITEM>\n");

			// Close the file
			fileWriter.close();
		} catch (CreateException ce) {
			try {
				fileWriter.close();
			} catch (Exception e) {
			}

			throw new RemoteException("Create exception thrown when writing remove log for payable item " + payableItemVO.getPayableItemID(), ce);
		} catch (IOException ioe) {
			try {
				fileWriter.close();
			} catch (Exception e) {
			}

			throw new RemoteException("IO exception thrown when writing remove log for payable item " + payableItemVO.getPayableItemID(), ioe);
		}
	}

	/**
	 * Return a filed handle to the remove log file for payable items
	 * 
	 * @return The removeLog value
	 * @exception RemoteException Description of the Exception
	 */
	private File getRemoveLog() throws RemoteException {
		File removeLog = null;
		CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
		String paymentLogDirectoryName = comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.LOGFILE_DIR);
		if (paymentLogDirectoryName == null || paymentLogDirectoryName.length() == 0) {
			throw new RemoteException("Failed to get payment system log file directory name from system parameters.");
		}

		try {
			File paymentLogDirectory = new File(paymentLogDirectoryName);
			if (!paymentLogDirectory.exists()) {
				LogUtil.info(this.getClass(), "Creating payment log file directory '" + paymentLogDirectoryName + "'");
				if (!paymentLogDirectory.mkdirs()) {
					throw new SystemException("Failed to create payment log file directory with '" + paymentLogDirectoryName + "'");
				}
			}

			String removeLogFilename = comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.REMOVE_LOGFILE);
			if (removeLogFilename == null || removeLogFilename.length() == 0) {
				throw new RemoteException("Failed to get payment system remove log file name from system parameters.");
			}

			removeLog = new File(paymentLogDirectory, removeLogFilename);

		} catch (IOException ioe) {
			throw new RemoteException("IO error getting payment remove log file.", ioe);
		}
		return removeLog;
	}

	public void undoPayableItem(Integer payableItemID, Integer previousPayableItemID, UserSession userSession) throws RemoteException, ValidationException, RollBackException {
		undoPayableItem(payableItemID, previousPayableItemID, null, userSession);
	}

	/**
	 * TRANSACTIONAL
	 * 
	 * Undo a payable item. This means removing (deleting) the payable item and reinstating a previous payable item. If there has been no payment made and the postings have not been transferred to the finance system, remove the payable item and its children (components, postings and actions). If the payment method was direct debit then remove the direct debit schedule in the direct debit sub-system and reinstate the previous direct debit schedule (if any). If the payment method was receipting then remove the payable fee in the receipting system. NOTE: The payment manager must be committed or rolled back.
	 * 
	 * @param payableItemID Description of the Parameter
	 * @param previousPayableItemID Description of the Parameter
	 * @param userSession Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 * @exception ValidationException Description of the Exception
	 */
	public void undoPayableItem(Integer payableItemID, Integer previousPayableItemID, String action, UserSession userSession) throws RemoteException, ValidationException, RollBackException {
		LogUtil.debug(this.getClass(), "payableItemID=" + payableItemID);
		LogUtil.debug(this.getClass(), "previousPayableItemID=" + previousPayableItemID);
		LogUtil.debug(this.getClass(), "action=" + action);
		LogUtil.debug(this.getClass(), "userSession=" + userSession);

		LogUtil.debug(this.getClass(), "undo 1a");
		// Integer tmpPayableItemID = payableItemID;
		// PayableItemVO tmpPayableItem = null;
		PayableItemVO prevPayableItem = null;
		PaymentMethod prevPaymentMethod = null;
		PaymentMethod paymentMethod = null;

		boolean uncancel = false;
		if (action != null && action.equals(PaymentTransactionMgr.ACTION_UNCANCEL)) {
			uncancel = true;
		}
		LogUtil.debug(this.getClass(), "undo 1a.1");
		// get the payable item
		PaymentMgrLocal pMgr = PaymentEJBHelper.getPaymentMgrLocal();
		// get the previous payment
		if (previousPayableItemID != null) {
			try {
				prevPayableItem = pMgr.getPayableItem(previousPayableItemID);
			} catch (PaymentException ex1) {
				throw new RemoteException("Unable to get previous payable item.", ex1);
			}
		}

		LogUtil.debug(this.getClass(), "undo 1a.2");
		// get current payable item
		PayableItemVO payableItem = null;
		try {
			payableItem = pMgr.getPayableItem(payableItemID);
		} catch (PaymentException ex2) {
			throw new RemoteException("Unable to get payable item.", ex2);
		}
		if (payableItem == null) {
			throw new SystemException("Unable to get Payable Item with ID " + payableItemID);
		}
		LogUtil.debug(this.getClass(), "undo 1a.3");
		// check that the postings haven't been transferred
		if (payableItem.postingsHaveBeenTransferred()) {
			throw new ValidationException("Cannot remove a Payable Item once the postings have been transferred to Finance.");
		}

		LogUtil.debug(this.getClass(), "undo 1b");

		if (uncancel) {
			LogUtil.debug(this.getClass(), "undo 1bb" + (payableItem == null));
			// current payment method
			paymentMethod = payableItem.getPaymentMethod();

			LogUtil.debug(this.getClass(), "paymentMethod b " + paymentMethod);

			if (paymentMethod != null) {
				LogUtil.debug(this.getClass(), "undo 1bc");
				if (paymentMethod.isDirectDebitPaymentMethod()) {
					// set previous payment method
					if (prevPayableItem != null) {
						LogUtil.debug(this.getClass(), "undo 1bd");
						paymentMethod = prevPayableItem.getPaymentMethod();
					}
					// reinstate old direct debit if one exists
				} else if (paymentMethod.isReceiptingPaymentMethod()) {
					LogUtil.debug(this.getClass(), "undo 1be");
					// remove cancel payable item only
					// reinstate old oi-payable if possible
				}
			} else {
				prevPaymentMethod = (prevPayableItem != null ? prevPayableItem.getPaymentMethod() : null);
				LogUtil.debug(this.getClass(), "undo 1bf" + prevPaymentMethod);
				// cancel - get previous payment method
				if (prevPaymentMethod != null) {
					// if previous is direct debit then current payment method
					// is equal
					// to previous
					if (prevPaymentMethod.isDirectDebitPaymentMethod()) {
						LogUtil.debug(this.getClass(), "prev paymentmethod");
						paymentMethod = prevPaymentMethod;
					}
				}

			}
		} else {
			// current payment method
			paymentMethod = payableItem.getPaymentMethod();
		}

		// Different payment methods require different processing for the undo.
		if (paymentMethod != null && paymentMethod.isDirectDebitPaymentMethod()) {
			DirectDebitSchedule ddSchedule = (DirectDebitSchedule) paymentMethod;

			LogUtil.debug(this.getClass(), "undo 2");
			LogUtil.debug(this.getClass(), "undo 3");

			DirectDebitAdapter ddAdapter = getDirectDebitAdapter();
			if (uncancel) {
				LogUtil.debug(this.getClass(), "undo 3a");
				// Uncancel the direct debit schedule by reinstating all
				// cancelled
				// scheduled items and removing the returned oi-item if it has
				// not been posted.
				ddAdapter.uncancelSchedule(ddSchedule.getDirectDebitScheduleID());

			} else {
				// Find the previous payable item and get the direct debit
				// schedule ID
				// off it if it was paid by direct debit.
				LogUtil.debug(this.getClass(), "undo 3b");

				DirectDebitSchedule prevSchedule = null;

				// prevScheduleID = prevPayableItem.getPaymentMethodID();
				// get schedule
				boolean replace = true;
				if (prevPayableItem != null) {
					prevPaymentMethod = prevPayableItem.getPaymentMethod();
					LogUtil.debug(this.getClass(), "undo 3b 1 - " + prevPaymentMethod);
					if (prevPaymentMethod == null) {
						LogUtil.debug(this.getClass(), "undo 3b 2");
						// cancel the schedule only
						replace = false;
					} else if (prevPaymentMethod.isReceiptingPaymentMethod()) {
						LogUtil.debug(this.getClass(), "undo 3b 3");
						// cancel the schedule only
						replace = false;
					} else if (prevPaymentMethod.isDirectDebitPaymentMethod()) {
						prevSchedule = (DirectDebitSchedule) prevPaymentMethod;
						LogUtil.debug(this.getClass(), "undo 3b 4 " + prevSchedule);
						if (prevSchedule.isReplaced()) {
							LogUtil.debug(this.getClass(), "undo 3b 5");
							// roll back the schedule
							replace = true;
						} else {
							replace = false;
						}
					}
				} else {
					LogUtil.debug(this.getClass(), "undo 3b 6");
					// cancel the schedule only
					replace = false;
				}

				// check that a payment hasn't been made since the Payable Item
				// was created.
				// getTotalAmountOutstanding includes any schedule item with a
				// status other than PAID.
				if (ddSchedule.getTotalAmountPaid().doubleValue() > 0) {
					throw new ValidationException("An amount payable cannot be removed after a payment has been made.  Total payable = " + ddSchedule.getTotalAmount() + ", total unpaid = " + ddSchedule.getTotalAmountOutstanding());
				}

				LogUtil.debug(this.getClass(), "undo 3c");

				if (replace) {
					LogUtil.debug(this.getClass(), "undo 3d");

					LogUtil.debug(this.getClass(), "prevScheduleID=" + prevSchedule);
					LogUtil.debug(this.getClass(), "DDScheduleID=" + ddSchedule.getDirectDebitScheduleID());

					// Now undo the direct debit schedule in the direct debit
					// system.
					// This deletes the first schedule specified and reinstates
					// the
					// previous schedule specified.
					ddAdapter.undoSchedule(ddSchedule.getDirectDebitScheduleID(), new Integer(prevSchedule.getPaymentMethodID()), userSession.getUserId());
				} else {
					LogUtil.debug(this.getClass(), "undo 3e");
					// Cancel it if there was not a previous one.
					ddAdapter.cancelSchedule(ddSchedule.getDirectDebitScheduleID(), userSession.getUserId());
				}
			}
			LogUtil.debug(this.getClass(), "undo 3f");
		} else // if(paymentMethod.isReceiptingPaymentMethod()) or null
		{
			LogUtil.debug(this.getClass(), "undo 4a");

			if (uncancel) {
				LogUtil.debug(this.getClass(), "undo 4b");
				// remove the cancel payable fee
				LogUtil.debug(this.getClass(), "payableItem " + payableItem);
				LogUtil.debug(this.getClass(), "prevPayableItem " + prevPayableItem);
				LogUtil.debug(this.getClass(), "paymentMethod " + paymentMethod);

				// no payable fee to remove
				if (paymentMethod != null) {
					LogUtil.debug(this.getClass(), "undo 4ba");
					ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(payableItem.getSourceSystem());
					receiptingAdapter.removePayableFee(payableItem);
				}
				LogUtil.debug(this.getClass(), "undo 4bb");
				if (prevPayableItem != null) {
					// only receipting payment method
					ReceiptingIdentifier ri = new ReceiptingIdentifier(prevPayableItem.getClientNo(), prevPayableItem.getPaymentMethodID());
					LogUtil.debug(this.getClass(), "undo 4bc" + ri);
					ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(prevPayableItem.getSourceSystem());
					LogUtil.debug(this.getClass(), "undo 4bd");
					try {
						receiptingAdapter.uncancelPayableFee(ri.getUniqueID());
					} catch (Exception ex) {
						// ignore exception as a receipt has been issued
						LogUtil.warn(this.getClass(), ex.toString());
					}
				}
			} else {
				LogUtil.debug(this.getClass(), "no else");

				// if a payment method id exists
				if (payableItem.getPaymentMethodID() != null && !payableItem.getPaymentMethodID().equals("")) {
					ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(payableItem.getSourceSystem());
					// remove the payble fee
					receiptingAdapter.removePayableFee(payableItem);

				}

				// may not be a previous payable item
				if (prevPayableItem != null) {

					prevPaymentMethod = prevPayableItem.getPaymentMethod();

					// revert to dd?
					if (prevPaymentMethod != null && prevPaymentMethod.isDirectDebitPaymentMethod()) {
						// uncancel previous schedule as the current receipting
						// transaction superseded it.
						DirectDebitSchedule prevSchedule = (DirectDebitSchedule) prevPaymentMethod;
						DirectDebitAdapter ddAdapter = this.getDirectDebitAdapter();
						ddAdapter.uncancelSchedule(prevSchedule.getDirectDebitScheduleID());
					}
				}
			}
		}
		LogUtil.debug(this.getClass(), "undo 5");

		// remove the payable item,components and postings
		LogUtil.debug(this.getClass(), "undo 8");
		try {
			deletePayableItem(payableItem);
		} catch (PaymentException ex3) {
			throw new SystemException(ex3);
		}

		LogUtil.debug(this.getClass(), "undo 9");

	}

	// undoPayableItem

	public Integer updateDirectDebitAuthority(DirectDebitAuthority newAuthority, String userID) throws RemoteException, RollBackException {
		LogUtil.debug(this.getClass(), "updateDirectDebitAuthority start");
		Integer newAuthorityID = null;
		Hashtable ddTable = null;
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		DirectDebitSchedule newSchedule = null;

		Integer oldScheduleID = null;
		Integer newScheduleID = null;
		MembershipVO membershipVO = null;

		boolean createNewRenewalNotice = false;

		// update the membership data with the new authority ID
		if (newAuthority != null) {
			Collection ddScheduleList = null;
			if (newAuthority.getDirectDebitAuthorityID() != null) {
				try {
					// get schedules if this is a valid authority
					ddScheduleList = paymentMgr.getDirectDebitScheduleListByDirectDebitAuthority(newAuthority);
				} catch (PaymentException ex) {
					throw new RemoteException("Unable to get schedule list by authority.", ex);
				}
			}
			// ignore schedules that are not current
			if (ddScheduleList != null) {
				DirectDebitSchedule tmpDDSchedule = null;
				LogUtil.debug(this.getClass(), "old schedules");

				int scheduleCount = 0;
				Iterator schedIt = ddScheduleList.iterator();
				while (schedIt.hasNext()) {
					// get the old schedule
					tmpDDSchedule = (DirectDebitSchedule) schedIt.next();
					LogUtil.debug(this.getClass(), "tmpDDSchedule=" + tmpDDSchedule);
					// no changes may be made if amount waiting on schedule
					if (tmpDDSchedule.containsUnscheduledDishonouredPayments()) {
						throw new RollBackException("Unable to update direct debit details as there are dishonoured payments that have not been rescheduled.");
					} else if (tmpDDSchedule.isRenewalVersion()) {
						if (!newAuthority.getSourceSystem().equals(SourceSystem.MEMBERSHIP)) {
							throw new RollBackException("Unable to update direct debit details as a renewal version of the schedule exists. Cancel the renewal document first.");
						}
					} else if (tmpDDSchedule.isCurrent()) {

						LogUtil.debug(this.getClass(), "is current");
						scheduleCount++;
						if (scheduleCount > 1) {
							throw new RollBackException("Existing authority '" + newAuthority + "' has more than 1 current schedule attached (" + scheduleCount + ".");
						} else {
							oldScheduleID = tmpDDSchedule.getDirectDebitScheduleID();
							LogUtil.debug(this.getClass(), "in else");
							// if a scheduled item has been paid
							if (tmpDDSchedule.getLastPaidScheduledItem() != null) {
								LogUtil.debug(this.getClass(), "paid items exist");
								// if a payment is made there is no immediate
								// amount remaining
								BigDecimal immediateAmount = new BigDecimal(0);
								BigDecimal amortisedAmount = tmpDDSchedule.getTotalAmountOutstanding();
								newSchedule = tmpDDSchedule;
								newSchedule.setImmediateAmount(immediateAmount);
								newSchedule.setAmortisedAmount(amortisedAmount);
								LogUtil.debug(this.getClass(), "newSchedule=" + newSchedule);
							} else {
								LogUtil.debug(this.getClass(), "paid items don't exist");
								newSchedule = tmpDDSchedule;
								LogUtil.debug(this.getClass(), "newSchedule=" + newSchedule);
							}
						}
					}

					/*
					 * probably redundant as MembershipUIC.handleEvent_editPaymentMethod_btnSave() cancels and recreates active renewal notices.
					 */
					if (newAuthority.getSourceSystem().equals(SourceSystem.MEMBERSHIP)) {
						// cancel renewal document
						MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
						List<MembershipVO> membershipList = (List<MembershipVO>) membershipMgrLocal.findMembershipByClientNumber(newAuthority.getClientNumber());
						if (membershipList.size() != 1) {
							throw new RollBackException("Unable to find unique membership with client number [" + newAuthority.getClientNumber() + "]");
						}
						membershipVO = membershipList.get(0);
						MembershipDocument renewalDocument = membershipVO.getCurrentRenewalDocument();
						if (renewalDocument != null) {
							try {
								membershipMgrLocal.cancelRenewalDocument(renewalDocument.getDocumentId(), true); // ignore
								// errors
							} catch (Exception e) {
								throw new RollBackException("Unable to cancel existing renewal document: " + e);
							}

							if (tmpDDSchedule.isRenewalVersion()) {
								// set flag to create new renewal document
								createNewRenewalNotice = true;
							}

						} else {
							if (tmpDDSchedule.isRenewalVersion()) {
								// orphaned renewal schedule
								removeDirectDebitSchedule(tmpDDSchedule.getDirectDebitScheduleID());
							}
						}

					}

				}
			}

			// Create / update the direct debit authority in the payment system
			ddTable = createDirectDebitAuthority(newAuthority);
			newAuthorityID = (Integer) ddTable.get(DirectDebitAdapter.DD_AUTHORITY_KEY);
			LogUtil.debug(this.getClass(), "newAuthorityID=" + newAuthorityID);

			// if there are existing active schedules
			if (newSchedule != null) {
				// update with new authority id
				newSchedule.setDirectDebitAuthorityID(newAuthorityID);
				LogUtil.debug(this.getClass(), "newSchedule=" + newSchedule);
				// replace existing schedule
				DirectDebitAdapter ddAdapter = getDirectDebitAdapter();
				LogUtil.debug(this.getClass(), "oldScheduleID=" + oldScheduleID);
				newScheduleID = ddAdapter.replaceSchedule(newSchedule);
				LogUtil.debug(this.getClass(), "newScheduleID=" + newScheduleID);
				if (oldScheduleID != null && newScheduleID != null && !oldScheduleID.equals(newScheduleID)) {
					LogUtil.debug(this.getClass(), "updateDirectDebitScheduleID");
					// update existing references in payment system
					updateDirectDebitScheduleID(oldScheduleID, newScheduleID);
				}
			}

			/*
			 * probably redundant as MembershipUIC.handleEvent_editPaymentMethod_btnSave() cancels and recreates active renewal notices.
			 */
			// if request originated from Membership system and we have just
			// cancelled a renewal notice
			if (newAuthority.getSourceSystem().equals(SourceSystem.MEMBERSHIP) && createNewRenewalNotice) {
				// if membershipVO is null at this point probably best to fail
				// loudly...

				DateTime feeEffectiveDate = MembershipHelper.calculateRenewalEffectiveDate(membershipVO.getExpiryDate(), null);

				UserMgrLocal userMgrLocal = UserEJBHelper.getUserMgrLocal();
				User user = userMgrLocal.getUser(userID);

				TransactionGroup transactionGroup = TransactionGroup.getRenewalTransactionGroup(membershipVO, user, feeEffectiveDate);

				MembershipRenewalMgrLocal renewalMgrLocal = MembershipEJBHelper.getMembershipRenewalMgrLocal();
				// pass null PaymentTransactionMgr as no payment required
				renewalMgrLocal.produceIndividualRenewalNotice(membershipVO, transactionGroup, user, null);
			}
		}
		LogUtil.debug(this.getClass(), "updateDirectDebitAuthority end");
		return newAuthorityID;
	}

	/**
	 * Update payable data where the start and end date of a payable item is the same.
	 * 
	 * @throws RemoteException
	 */
	public void updatePayableData() throws RemoteException {
		// history selection sql
		String hist_sql = "SELECT transaction_id,b.payableitemid,b.startDate,b.enddate ";
		hist_sql += "FROM   PUB.mem_transaction a,pub.pt_payableitem b ";
		hist_sql += "WHERE b.sourcesystemreferenceid = a.transaction_id ";
		hist_sql += "AND b.startdate = b.enddate ";
		// hist_sql += "AND membership_xml is not null ";
		// update pi sql
		String upd_pi_sql = "UPDATE PUB.pt_payableitem SET enddate = ? WHERE payableitemid = ?";

		MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
		Connection connection = null;
		PreparedStatement statement = null;
		PreparedStatement statementUpdPI = null;
		int payableItemID;
		int transactionID;
		MembershipTransactionVO txVO = null;
		MembershipHistory memHist = null;
		DateTime oldExpiryDate = null;
		DateTime startDate = null;
		DateTime endDate = null;
		int updCount = 0;
		int counter = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(hist_sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				counter++;
				transactionID = rs.getInt(1);
				payableItemID = rs.getInt(2);
				startDate = new DateTime(rs.getDate(3));
				endDate = new DateTime(rs.getDate(4));

				txVO = memMgr.getMembershipTransaction(new Integer(transactionID));

				if (txVO.isHistoryAvailable()) {
					memHist = txVO.getMembershipHistory();
					oldExpiryDate = memHist.getExpiryDate();
					if (oldExpiryDate != null && !oldExpiryDate.equals(endDate)) {
						LogUtil.debug(this.getClass(), payableItemID + ": start " + startDate + " end " + endDate + " old expiry " + oldExpiryDate);
						// update the record
						statementUpdPI = connection.prepareStatement(upd_pi_sql);
						statementUpdPI.setDate(1, oldExpiryDate.toSQLDate());
						statementUpdPI.setInt(2, payableItemID);
						updCount += statementUpdPI.executeUpdate();
					}
				} else {
					LogUtil.debug(this.getClass(), "No history available for transaction '" + txVO.getTransactionID() + "'.");
				}

			}
			rs.close();

			LogUtil.info(this.getClass(), "Updated " + updCount + " of " + counter + " records that were processed.");

			// this.sessionContext.setRollbackOnly();
		} catch (SQLException se) {
			throw new RemoteException("Error executing update.", se);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
			ConnectionUtil.closeStatement(statementUpdPI);
		}
	}

	/**
	 * Update schedule reference for the payable item.
	 * 
	 * @param oldScheduleID
	 * @param newScheduleID
	 * @throws RemoteException
	 */
	private int updateDirectDebitScheduleID(Integer oldScheduleID, Integer newScheduleID) throws RemoteException {

		Query q = hsession.createQuery("from PayableItemVO where paymentMethodId = :paymentMethodId and paymentMethod = :paymentMethod");
		q.setString("paymentMethodId", oldScheduleID.toString());
		q.setString("paymentMethod", PaymentMethod.DIRECT_DEBIT);
		PayableItemVO payableItem = null;
		int rowsUpdated = 0;
		List list = q.list();
		for (Iterator<PayableItemVO> i = list.iterator(); i.hasNext();) {
			payableItem = i.next();
			payableItem.setPaymentMethodID(newScheduleID.toString());
			hsession.update(payableItem);
			rowsUpdated++;
		}
		return rowsUpdated;
	}

	/**
	 * Update a pending fee in the database.
	 * 
	 * @param pendingFee
	 */
	public void updatePendingFee(PendingFee pendingFee) {

		hsession.merge(pendingFee);
	}

	/**
	 * TRANSACTIONAL
	 * 
	 * Create or update a direct debit authority in the direct debit sub system. If the authority specifies an existing authority (it has a ID) then the existing authority will be updated otherwise a new authority will be created.
	 * 
	 * @param authority Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException Description of the Exception
	 */
	public Hashtable createDirectDebitAuthority(DirectDebitAuthority authority) throws RemoteException, RollBackException {
		if (authority == null) {
			throw new SystemException("Failed to create/update direct debit authority.  The authority is null!");
		}
		Hashtable ddTable = null;
		DirectDebitAdapter ddAdapter = this.getDirectDebitAdapter();
		ddTable = ddAdapter.createAuthority(authority);
		return ddTable;
	}

	/**
	 * Create a direct debit schedule.
	 * 
	 * @param ddSchedule DirectDebitSchedule
	 * @throws RemoteException
	 * @throws RollBackException
	 * @return Integer
	 */
	public Integer createDirectDebitSchedule(DirectDebitSchedule ddSchedule) throws RemoteException, RollBackException {
		if (ddSchedule == null) {
			throw new SystemException("Failed to create direct debit schedule.  The schedule is null!");
		}
		Integer ddScheduleId = null;
		DirectDebitAdapter ddAdapter = this.getDirectDebitAdapter();
		ddScheduleId = ddAdapter.createSchedule(ddSchedule);
		return ddScheduleId;
	}

	/**
	 * Remove a direct debit schedule.
	 * 
	 * @param ddScheduleId Integer
	 * @throws RemoteException
	 * @throws RollBackException
	 */
	public void removeDirectDebitSchedule(Integer ddScheduleId) throws RemoteException, RollBackException {
		if (ddScheduleId == null) {
			throw new SystemException("Failed to remove direct debit schedule.  The id is null!");
		}
		DirectDebitAdapter ddAdapter = this.getDirectDebitAdapter();
		ddAdapter.removeSchedule(ddScheduleId);
	}

	/**
	 * Create a renewal version of the direct debit schedule.
	 * 
	 * @param ddSchedule DirectDebitSchedule
	 * @throws RemoteException
	 * @throws RollBackException
	 * @return Integer
	 */
	public Integer createRenewalDirectDebitSchedule(DirectDebitSchedule ddSchedule) throws RemoteException, RollBackException {
		if (ddSchedule == null) {
			throw new SystemException("Failed to create renewal direct debit schedule.  The schedule is null!");
		}
		Integer ddScheduleId = null;
		DirectDebitAdapter ddAdapter = this.getDirectDebitAdapter();
		ddScheduleId = ddAdapter.createRenewalSchedule(ddSchedule);
		return ddScheduleId;
	}

	/**
	 * <p>
	 * Transfers failed amounts that are in the receipting system back into the direct debit system.
	 * </p>
	 * <p>
	 * The flow of events is:
	 * </p>
	 * <ul>
	 * <li>Identify the payable item with the failed direct debit schedule.
	 * <li>Get the direct debit schedule attached to the payable item id.
	 * <li>Get the direct debit authority attached to the direct debit schedule.
	 * <li>Get the payable attached to the direct debit schedule.
	 * <li>Cancel the payable attached to the schedule.
	 * <li>Use the payable to specify a new direct debit schedule specification.
	 * <li>Create a new schedule.
	 * <li>Update payable items that referenced the previous schedule to the new schedule.
	 * </ul>
	 */
	public void transferDirectDebitSchedule(Integer payableItemID, User user) throws RemoteException, RollBackException {
		PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
		PayableItemVO payableItem = null;
		try {
			payableItem = pMgr.getPayableItem(payableItemID);
		} catch (PaymentException ex1) {
			throw new RemoteException("Unable to get payable item.", ex1);
		}

		// client number attached to payable item
		Integer clientNumber = payableItem.getClientNo();

		PaymentMethod paymentMethod = payableItem.getPaymentMethod();
		if (!(paymentMethod instanceof DirectDebitSchedule)) {
			throw new SystemException("Payable item '" + payableItemID + "' does not have a schedule attached.");
		}

		DirectDebitSchedule ddSchedule = (DirectDebitSchedule) paymentMethod;
		Integer oldScheduleID = ddSchedule.getDirectDebitScheduleID();

		// no amount outstanding apart from the receipting amount
		if (ddSchedule.getTotalAmountOutstanding().compareTo(new BigDecimal(0)) > 0 && !ddSchedule.getTotalAmountOutstanding().equals(ddSchedule.getReceiptingAmount())) {
			throw new SystemException("The current schedule '" + oldScheduleID + "' still has an amount outstanding of " + CurrencyUtil.formatDollarValue(ddSchedule.getTotalAmountOutstanding()) + ".");
		}

		// cancel schedule
		cancelPaymentMethod(ddSchedule, payableItem.getSourceSystem());

		// get the attached authority
		DirectDebitAuthority ddAuthority = ddSchedule.getDirectDebitAuthority();
		if (ddAuthority == null) {
			throw new SystemException("The direct debit authority is null attached to direct debit schedule '" + oldScheduleID + "'.");
		}

		String paymentMethodID = ddSchedule.getReceiptingSystemID();

		// get the payable attached to the schedule.
		Payable payable = ddSchedule.getPayable();
		if (payable == null) {
			throw new SystemException("Payable attached to schedule '" + oldScheduleID + "' is null.");
		}

		// get the amount outstanding
		BigDecimal amountOutstanding = payable.getAmountOutstanding();
		if (amountOutstanding.compareTo(new BigDecimal(0)) <= 0) {
			throw new SystemException("Payable attached to schedule '" + oldScheduleID + "' has no amount outstanding " + amountOutstanding + ".");
		}
		// get the amount payable
		BigDecimal amountPayable = payable.getAmount();
		if (amountOutstanding.compareTo(amountPayable) > 0) {
			throw new SystemException("Amount outstanding '" + amountOutstanding + "' is greater than amount payable '" + amountPayable + "'.");
		}

		SourceSystem ss = payable.getSourceSystem();
		ReceiptingPaymentMethod recPaymentMethod = new ReceiptingPaymentMethod(paymentMethodID, clientNumber, ss);

		// cancel payable fee - nothing in RECEPTOR!!!
		cancelPaymentMethod(recPaymentMethod, payableItem.getSourceSystem());

		// get the date as at now
		DateTime now = new DateTime().getDateOnly();
		Interval oneWeek = null;
		Interval oneYear = null;

		try {
			oneWeek = new Interval("0:0:7");
			oneYear = new Interval("1:0:0");
		} catch (ParseException ex) {
			throw new SystemException(ex);
		}
		// create the direct debit schedule

		// start from one week from today
		DateTime startDate = new DateTime().add(oneWeek);
		// end date is one week and one year from today
		DateTime endDate = startDate.add(oneYear);
		// no immediate amount
		BigDecimal immediateAmount = new BigDecimal(0);
		// current user id
		String userID = user.getUserID();
		// current user's sales branch code
		String salesBranchCode = user.getSalesBranchCode();
		// no payable item to reference
		String productCode = payableItem.getProductCode();
		DirectDebitSchedule newSchedule = new DirectDebitSchedule(ddAuthority, startDate, endDate, amountOutstanding, immediateAmount, payableItemID, clientNumber, userID, salesBranchCode, ss, productCode, "Tfr Receipting. Ref: " + payableItemID);

		DirectDebitAdapter ddAdapter = this.getDirectDebitAdapter();
		Integer newScheduleID = ddAdapter.createSchedule(newSchedule);

		// update existing references for the payable item
		this.updateDirectDebitScheduleID(oldScheduleID, newScheduleID);

		// no ledger changes required
	}

	/**
	 * TRANSACTIONAL
	 * 
	 * Cancel the remaining direct debit deductions on the specified direct debit schedule NOTE: This method updates sub systems. The payment manager must be committed or rolled back.
	 * 
	 * @param directDebitScheduleID Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 */
	public void cancelDirectDebitSchedule(Integer directDebitScheduleID, String userID) throws RemoteException, RollBackException {
		DirectDebitAdapter directDebitAdapter = this.getDirectDebitAdapter();
		directDebitAdapter.cancelSchedule(directDebitScheduleID, userID);
	}

	public void cancelPaymentMethod(PaymentMethod paymentMethod, SourceSystem sourceSystem) throws RemoteException, RollBackException {
		if (paymentMethod == null) {
			throw new SystemException("Payment method is null");
		}
		if (paymentMethod.isReceiptingPaymentMethod()) {
			ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(sourceSystem);
			ReceiptingPaymentMethod recPaymentMethod = (ReceiptingPaymentMethod) paymentMethod;
			receiptingAdapter.cancelPayableFee(paymentMethod);
		} else if (paymentMethod.isDirectDebitPaymentMethod()) {
			DirectDebitSchedule ddSchedule = (DirectDebitSchedule) paymentMethod;
			cancelDirectDebitSchedule(new Integer(paymentMethod.getPaymentMethodID()), paymentMethod.getUserID());
		} else {
			throw new SystemException("Unknown payment method '" + paymentMethod.getDescription() + "'");
		}
	}

	/**
	 * TRANSACTIONAL
	 * 
	 * Add a payable item and creates a record in the receipting system or a direct debit schedule if required.
	 * 
	 * @param PayableItemVO payableItem The details of the payments being recorded.
	 */
	public PayableItemVO addPayableItem(PayableItemVO payableItem) throws RollBackException {

		boolean paymentMethodRequired = false;

		double amountPayable = payableItem.getAmountPayable();
		if (amountPayable > 0) {
			paymentMethodRequired = true;
		}

		return addPayableItem(payableItem, paymentMethodRequired, new DateTime(), null);
	}

	/**
	 * TRANSACTIONAL
	 * 
	 * Add a payable item and creates a record in the receipting system or a direct debit schedule if required.
	 * 
	 * @param PayableItemVO payableItem The details of the payments being recorded.
	 * @param boolean paymentMethodRequird override whether to create DD schedule or Receipting entries.
	 * @param DateTime createDateTime override creation date
	 * @param String paymentSystem override payment system name
	 */
	public PayableItemVO addPayableItem(PayableItemVO payableItem, boolean paymentMethodRequired, DateTime createDateTime, String paymentSystem) throws RollBackException {
		if (payableItem == null) {
			throw new RollBackException("Unable to add payable item as the PayableItem is null.");
		}

		LogUtil.debug(this.getClass(), "addPayableItem=" + payableItem);

		String paymentMethodID = null;
		String repairPaymentMethodID = null;
		PaymentMethod paymentMethod = null;

		LogUtil.debug(this.getClass(), "paymentMethodRequired=" + paymentMethodRequired);

		try {

			// DateTime createDateTime = new DateTime();
			Integer payableItemID = new Integer(PaymentEJBHelper.getPaymentIDMgr().getNextPayableItemID());
			// set id
			payableItem.setPayableItemID(payableItemID);
			// set create date/time
			payableItem.setCreateDateTime(createDateTime);

			LogUtil.debug(this.getClass(), "createDateTime=" + createDateTime);
			LogUtil.debug(this.getClass(), "payableItemID=" + payableItemID);

			paymentMethod = payableItem.getPaymentMethod();

			LogUtil.debug(this.getClass(), "paymentMethod=" + paymentMethod);

			/**
			 * if payment is required
			 */
			if (paymentMethodRequired) {
				/**
				 * Direct debit
				 */
				if (paymentMethod instanceof DirectDebitSchedule) {
					LogUtil.debug(this.getClass(), "Direct Debit");
					// The direct debit authority must be created first and its
					// new ID
					// obtained so that the schedule can be created based on the
					// authority.
					DirectDebitSchedule ddSchedule = (DirectDebitSchedule) paymentMethod;
					ddSchedule.setPayableItemID(payableItemID);
					ddSchedule.setProductCode(payableItem.getProductCode());
					ddSchedule.setScheduleDescription(payableItem.getDescription());

					DirectDebitAuthority ddAuthority = ddSchedule.getDirectDebitAuthority();
					LogUtil.debug(this.getClass(), "ddAuthority=" + ddAuthority);
					DirectDebitAdapter ddAdapter = getDirectDebitAdapter();
					Integer ddAuthorityID = null;
					Hashtable ddTable = ddAdapter.createAuthority(ddAuthority);
					ddAuthorityID = (Integer) ddTable.get(DirectDebitAdapter.DD_AUTHORITY_KEY);
					LogUtil.debug(this.getClass(), "ddAuthorityID=" + ddAuthorityID);
					ddSchedule.setDirectDebitAuthorityID(ddAuthorityID);

					payableItem.setPaymentSystem(ddAdapter.getSystemName());

					// If there is already a payment method ID then
					// the schedule is to be replaced.
					String oldPaymentMethodID = payableItem.getPaymentMethodID();

					// Is the oldPaymentMethodID equals to the paymentMethodId
					// attached to the
					// payment method - ie. schedule.

					/**
					 * @todo payableItem.getPaymentMethodID() may not equal ddSchedule.getDirectDebitScheduleId
					 */

					Integer oldDDScheduleID = oldPaymentMethodID != null ? new Integer(oldPaymentMethodID) : null;
					LogUtil.debug(this.getClass(), "oldDDScheduleID=" + oldDDScheduleID);

					if (oldDDScheduleID == null) {
						// new schedule
						LogUtil.debug(this.getClass(), "creating a schedule" + ddSchedule);
						paymentMethodID = ddAdapter.createSchedule(ddSchedule).toString();
					} else {
						LogUtil.debug(this.getClass(), "isRenewalVersion ddSchedule=" + ddSchedule);
						// if renewal version then activate existing schedule
						if (ddSchedule.isRenewalVersion()) {
							LogUtil.debug(this.getClass(), "activateRenewalSchedule " + ddSchedule);
							ddAdapter.activateRenewalSchedule(oldDDScheduleID, payableItem.getUserID()).toString();
							paymentMethodID = String.valueOf(oldDDScheduleID);
						} else {
							// else this is a schedule replacement
							LogUtil.debug(this.getClass(), "replaceSchedule " + ddSchedule);
							ddSchedule.setPaymentMethodID(oldDDScheduleID.toString());
							paymentMethodID = ddAdapter.replaceSchedule(ddSchedule).toString();
						}
					}
				}
				/**
				 * Receipting
				 */
				else if (paymentMethod instanceof ReceiptingPaymentMethod) {
					LogUtil.debug(this.getClass(), "Receipting");
					// Get a receipting adapter
					ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(payableItem.getSourceSystem());
					payableItem.setPaymentSystem(receiptingAdapter.getSystemName());

					// repair?
					repairPaymentMethodID = payableItem.getRepairPaymentMethodID();

					String oldPaymentMethodID = payableItem.getPaymentMethodID();
					// If there is already a payment method ID then there can only be a previous schedule to be cancelled.
					if (oldPaymentMethodID != null) {
						LogUtil.debug(this.getClass(), "cancelSchedule");
						DirectDebitAdapter ddAdapter = getDirectDebitAdapter();
						ddAdapter.cancelSchedule(new Integer(oldPaymentMethodID), payableItem.getUserID());
						// clear the payment method ID
						payableItem.clearPaymentMethodID();
					}

					if (repairPaymentMethodID == null) {
						LogUtil.debug(this.getClass(), "createPayableFee repairPaymentMethodID null");
						// create receipting payment method normally
						paymentMethodID = receiptingAdapter.createPayableFee(payableItem);
					} else {
						LogUtil.debug(this.getClass(), "createPayableFee repairPaymentMethodID=" + repairPaymentMethodID);
						// don't do anything and just set the id on the payable
						// item
						paymentMethodID = repairPaymentMethodID;
					}
				} else {
					throw new RollBackException("The payment method '" + (paymentMethod != null ? paymentMethod.getDescription() : "null") + "' is not one of the known types!");
				}
			} else {
				LogUtil.debug(this.getClass(), "setPaymentSystem");
				// set the payment system only
				ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(payableItem.getSourceSystem());
				payableItem.setPaymentSystem(receiptingAdapter.getSystemName());
			}

			// override payment system
			if (paymentSystem != null) {
				payableItem.setPaymentSystem(paymentSystem);
			}

			LogUtil.debug(this.getClass(), "before setting payment method id");
			if (paymentMethodID != null) {
				LogUtil.debug(this.getClass(), "setPaymentMethodID");
				// only set it if we have a valid payment method id
				payableItem.setPaymentMethodID(paymentMethodID);
			}

			createPayableItem(payableItem);
			LogUtil.debug(this.getClass(), "after create payable item");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RollBackException(e);
		}

		return payableItem;
	} // addPayableItem

	public void updateComponents(Collection componentList) throws RemoteException {
		PayableItemComponentVO component = null;
		Iterator componentListIterator = componentList.iterator();
		while (componentListIterator.hasNext()) {
			component = (PayableItemComponentVO) componentListIterator.next();
			updatePayableItemComponent(component);
		}
	}

	public void updatePayableItemComponent(PayableItemComponentVO payableItemComponent) throws RemoteException {

		hsession.update(payableItemComponent);
	}

	/**
	 * Receipt an IVR payment.
	 * 
	 * @param ivrPayment IVRPayment
	 * @throws RemoteException
	 * @throws RollBackException
	 * @return String
	 */
	public String receiptIVRPayment(IVRPayment ivrPayment) throws RemoteException, RollBackException {

		LogUtil.debug(this.getClass(), "ivr payment=" + ivrPayment.toString());
		String receiptNumber = null;
		try {
			ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(ivrPayment.getSourceSystem());
			Receipt receipt = receiptingAdapter.receiptIvrPayment(ivrPayment);
			receiptNumber = receipt.getReceiptNumber();
		} catch (Exception e) {
			LogUtil.debug(this.getClass(), "Error receipting IVR payment: " + e.getMessage());
			throw new RollBackException(e.getMessage());
		}
		return receiptNumber;
	}

	public String receiptElectronicPayment(ElectronicPayment electronicPayment) throws RemoteException, RollBackException {
		LogUtil.log(this.getClass(), "receiptElectronicPayment start " + electronicPayment.getCustomerReferenceNumber());
		String receiptNumber = null;
		try {
			LogUtil.debug(this.getClass(), "RLG +++++ RA " + electronicPayment.getSourceSystem().getAbbreviation());
			ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(electronicPayment.getSourceSystem());
			LogUtil.debug(this.getClass(), "RLG +++++ RA receipting " + electronicPayment.getCustomerReferenceNumber() + "/" + electronicPayment.getSourceSystem());
			Receipt r = receiptingAdapter.receiptElectronicPayment(electronicPayment);
			LogUtil.debug(this.getClass(), "RLG +++++ RA receipted get receipt number");
			receiptNumber = r.getReceiptNumber();
			LogUtil.debug(this.getClass(), "RLG +++++ RA receipted " + receiptNumber);
		} catch (Exception e) {
			LogUtil.debug(this.getClass(), "BPAY Insurance Exception " + e.getMessage());
			throw new RollBackException("BPAY " + e.getMessage());
		}
		LogUtil.log(this.getClass(), "receiptElectronicPayment end " + electronicPayment.getCustomerReferenceNumber());
		return receiptNumber;

	}

	public String receiptOnlinePayment(SourceSystem sourceSystem, double amount, String cardNumber, DateTime paymentDate, String cardExpiryDate, String seqNumber, String clientNumber, String userId, String branchCode, String locationCode, String tillCode, String cardType, String cardHolder, String cvv) throws RollBackException {
		Receipt r = null;
		try {
			ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(sourceSystem);
			r = receiptingAdapter.receiptOnlinePayment(sourceSystem.getAbbreviation(), amount, cardNumber, paymentDate, cardExpiryDate, seqNumber, clientNumber, userId, branchCode, locationCode, tillCode, cardType, cardHolder, cvv);
		} catch (Exception e) {
			throw new RollBackException(e);
		}

		return r.getReceiptNumber();
	}

	/**
	 * Extract the Insurance items that are eligible for payment via IVR
	 */
	public String extractInsuranceIVR() throws RemoteException {
		ReceiptingAdapter ra = getReceiptingAdapter(SourceSystem.INSURANCE);
		return ra.extractInsuranceIVR();
	}

	/**
	 * TRANSACTIONAL?
	 * 
	 * Create a pending fee and add the payable amount to the receipting system.
	 */
	public void createPendingFee(Integer clientNumber, String pendingFeeID, DateTime feeEffectiveDate, DateTime feeExpiryDate, String sourceSystem, Integer sourceSystemReferenceNo, String salesBranchCode, String productCode, Long referenceNumber, Long sequenceNumber, String membershipNumber, double amount, String userID, String description) throws RemoteException, PaymentException, RollBackException {
		LogUtil.debug(this.getClass(), "creating payable fee");
		ReceiptingAdapter receiptingAdapter = null;
		String opSequenceNumber = null;

		try {
			LogUtil.debug(this.getClass(), "createPendingFee: client - " + clientNumber + "ss: " + sourceSystem + "salesbcode: " + salesBranchCode + "prodcode: " + productCode + "userid: " + userID + "amount: " + amount + "desc: " + description + "pFeeID: " + pendingFeeID);
			receiptingAdapter = getReceiptingAdapter(SourceSystem.getSourceSystem(sourceSystem));
			opSequenceNumber = receiptingAdapter.createPayableFee(clientNumber, pendingFeeID // becomes
			// ref-no
			// in
			// oi-payable
			, null, productCode, amount, description, sourceSystem, salesBranchCode, userID, null);
			LogUtil.debug(this.getClass(), "created payable fee 1");
			// create the PendingFee record

			LogUtil.debug(this.getClass(), "created payable fee 2");
			PendingFee pendingFee = new PendingFee(pendingFeeID, feeEffectiveDate, feeExpiryDate, sourceSystem, sourceSystemReferenceNo, referenceNumber, sequenceNumber, amount, null, null, null, null, clientNumber, (opSequenceNumber == null) ? null : new Integer(opSequenceNumber), receiptingAdapter.getSystemName());

			hsession.save(pendingFee);
			LogUtil.debug(this.getClass(), "created payable fee 3");
		} catch (Exception e) {
			// Any kind of exception will result in a rollback
			throw new PaymentException("Unable to create pending fees for client " + clientNumber + ".  ", e);
		}
	}

	/**
	 * Clears the amount outstanding on all of the payable items for the client where the payable item was created by the given source system.
	 * 
	 * TRANSACTIONAL (commit is done here!)
	 */
	public int clearAmountOutstandingForClient(SourceSystem sourceSystem, Integer clientNumber) throws RemoteException {

		Collection<PayableItemVO> piList = null;
		int rowsUpdated = 0;
		PaymentMethod paymentMethod = null;
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

		try {
			piList = paymentMgr.getPayableItemsByClient(clientNumber);
		} catch (PaymentException ex) {
			throw new RemoteException("Unable to get payable item list ", ex);
		}
		if (piList != null) {
			for (PayableItemVO piVO : piList) {
				// if sourcesystem is the same
				if (piVO.getSourceSystem().equals(sourceSystem)) {

					paymentMethod = piVO.getPaymentMethod();
					// if receipting
					if (paymentMethod != null) {
						try {
							// set the client number
							paymentMethod.setClientNumber(clientNumber);
							// cancel in the subsystem
							cancelPaymentMethod(paymentMethod, sourceSystem);
							// zero amount in payableItem
							if (paymentMethod.isReceiptingPaymentMethod()) {
								piVO.setAmountOutstanding(0);
								mergePayableItem(piVO);
							}
						} catch (RollBackException ex2) {
							throw new RemoteException("", ex2);
						} catch (PaymentException e) {
							throw new RemoteException("", e);
						}
					}
					rowsUpdated++;
				}
			}
		}

		return rowsUpdated;
	}

	private static final String ACTION_ROLLBACK = "rollback";
	private static final String ACTION_COMMIT = "commit";
	private static final String ACTION_RELEASE = "release";

	/**
	 * Execute a generic adapter action.
	 * 
	 * @param actionType String
	 * @throws SystemException
	 */
	private void executeAdapterAction(String actionType) throws SystemException {
		LogUtil.debug(this.getClass(), "executing actionType=" + actionType);
		TransactionAdapter paymentAdapter = null;
		String key = null;
		String errorMessage = "";
		Enumeration keyList = this.paymentAdapterList.keys();
		if (keyList != null) {
			while (keyList.hasMoreElements()) {
				key = (String) keyList.nextElement();
				paymentAdapter = (TransactionAdapter) this.paymentAdapterList.get(key);
				try {
					if (actionType.equals(ACTION_ROLLBACK)) {
						LogUtil.debug(this.getClass(), "Rolling back adapters");
						paymentAdapter.rollback();
					} else if (actionType.equals(ACTION_COMMIT)) {
						LogUtil.debug(this.getClass(), "Committing adapters");
						paymentAdapter.commit();
					} else if (actionType.equals(ACTION_RELEASE)) {
						LogUtil.debug(this.getClass(), "Releasing adapters");
						paymentAdapter.release();
					} else {
						throw new SystemException("Action '" + actionType + "' may not be processed.");
					}

					// remove the key from the list.
					paymentAdapterList.remove(key);
				} catch (SystemException ex) {
					errorMessage += ex.getMessage();
				}
			}
		}
		if (!errorMessage.equals("")) {
			throw new SystemException("Error executing action '" + actionType + "' on adapter list. " + errorMessage);
		}
	}

	/**
	 * Description of the Method
	 * 
	 * @exception SystemException Description of the Exception
	 */
	public void rollback() throws RemoteException {
		executeAdapterAction(this.ACTION_ROLLBACK);
		// rollback any associated transactions
		sessionContext.setRollbackOnly();
	}

	/**
	 * TRANSACTIONAL?
	 * 
	 * Cancel the remaining direct debit deductions on all schedules relating to the specified direct debit authority NOTE: This method updates sub systems. The payment manager must be committed or rolled back.
	 * 
	 * @param directDebitAuthorityID Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 */
	public void cancelDirectDebitScheduleByDirectDebitAuthority(Integer directDebitAuthorityID, String userID) throws RemoteException, RollBackException {
		DirectDebitAdapter ddAdapter = getDirectDebitAdapter();
		DirectDebitSchedule ddSchedule = null;
		BigDecimal zeroAmount = new BigDecimal(0.0);
		Vector scheduleList = ddAdapter.getScheduleListByAuthority(directDebitAuthorityID);
		for (int i = 0; i < scheduleList.size(); i++) {
			ddSchedule = (DirectDebitSchedule) scheduleList.get(i);
			if (!ddSchedule.getTotalAmountOutstanding().equals(zeroAmount)) {
				ddAdapter.cancelSchedule(ddSchedule.getDirectDebitScheduleID(), userID);
			}
		}

	}

	@Init
	public void ejbCreate() {
		LogUtil.debug(this.getClass(), "ejbCreate=" + this.toString());
		initialiseAdapters();
	}

	@PreDestroy
	public void ejbRemove() throws RemoteException {
		LogUtil.debug(this.getClass(), "ejbRemove=" + this.toString());
		this.paymentAdapterList = null;
	}

	/**
	 * release proxy adapter connections.
	 */
	private void releaseAdapters() throws RemoteException {
		executeAdapterAction(ACTION_RELEASE);
	}

	@PostActivate
	public void ejbActivate() throws RemoteException {
		LogUtil.debug(this.getClass(), "ejbActivate=" + this.toString());
		initialiseAdapters();
	}

	private void initialiseAdapters() {
		paymentAdapterList = new Hashtable();
	}

	@PrePassivate
	public void ejbPassivate() throws RemoteException {
		LogUtil.debug(this.getClass(), "ejbPassivate=" + this.toString());
		this.dataSource = null;
		this.paymentAdapterList = null; // ensure dodgy adapters aren't
		// serialized.
	}

	/**
	 * Recreate pending records in the currently selected receipting system.
	 * 
	 * @param startPayableItemId Integer
	 * @param endPayableItemId Integer
	 * @throws RemoteException
	 * @throws RollBackException
	 */
	public void createPayablesFromPayableItems(Integer startPayableItemId, Integer endPayableItemId) throws RemoteException, RollBackException {
		PayableItemVO payableItem = null;
		final String SQL_PAYABLE_RANGE = "select clientno,paymentmethod,paymentMethodId,payment_system,sourcesystem,sourcesystemreferenceid,salesbranch," + "branchregion,prodcode,logonid,startdate,enddate,description,payableitemid from PUB.pt_payableitem where payableitemid between ? and ?";
		final String SQL_UPDATE_PAYABLE_ITEM = "update PUB.pt_payableitem set paymentmethodid = ?,set payment_system = ? where payableitemid = ?";

		Connection connection = null;
		PreparedStatement payableStatement = null;
		PreparedStatement updatePayableStatement = null;

		Integer clientNumber = null;
		String paymentMethod = null;
		String paymentMethodId = null;
		SourceSystem sourceSystem = null;
		Integer sourceSystemReferenceId = null;
		String salesBranchCode = null;
		Integer clientBranchNumber = null;
		String productCode = null;
		String userId = null;
		DateTime startDate = null;
		DateTime endDate = null;
		String description = null;
		Integer payableItemId = null;
		java.sql.Date tmpDate = null;
		try {
			ReceiptingAdapter receiptingAdapter = this.getReceiptingAdapter(sourceSystem);

			connection = dataSource.getConnection();
			payableStatement = connection.prepareStatement(SQL_PAYABLE_RANGE);
			payableStatement.setInt(1, startPayableItemId.intValue());
			payableStatement.setInt(2, endPayableItemId.intValue());
			ResultSet payableRS = payableStatement.executeQuery();
			int counter = 0;
			while (payableRS.next()) {
				// get details and create payable.
				counter++;

				clientNumber = new Integer(payableRS.getInt(1));
				paymentMethod = payableRS.getString(2);

				// payment method id is assigned

				// paymentSystem = payableRS.getString(4);
				try {
					sourceSystem = SourceSystem.getSourceSystem(payableRS.getString(5));
				} catch (GenericException ex1) {
					// ignore
				}
				sourceSystemReferenceId = new Integer(payableRS.getInt(6));
				salesBranchCode = payableRS.getString(7);
				clientBranchNumber = new Integer(payableRS.getInt(8));
				productCode = payableRS.getString(9);
				userId = payableRS.getString(10);
				tmpDate = payableRS.getDate(11);
				if (tmpDate != null) {
					startDate = new DateTime(tmpDate);
				}
				tmpDate = payableRS.getDate(12);
				if (tmpDate != null) {
					endDate = new DateTime(tmpDate);
				}
				description = payableRS.getString(13);
				payableItemId = new Integer(payableRS.getInt(14));

				try {
					// null paymentMethodId to generate new payable
					payableItem = new PayableItemVO(payableItemId, clientNumber, paymentMethod, paymentMethodId, sourceSystem, sourceSystemReferenceId, salesBranchCode, clientBranchNumber, productCode, userId, startDate, endDate, description);
				} catch (PaymentException ex2) {
					// continue
					LogUtil.fatal(this.getClass(), "Unable to create payable for payable item." + ex2);
				}

				paymentMethodId = receiptingAdapter.createPayableFee(payableItem);

				updatePayableStatement = connection.prepareStatement(SQL_UPDATE_PAYABLE_ITEM);
				updatePayableStatement.setString(1, paymentMethodId);
				updatePayableStatement.setString(2, receiptingAdapter.getSystemName());
				updatePayableStatement.setInt(3, payableItemId.intValue());
				// update payable item with refererence from create payable fee
				// and new receipting system
				updatePayableStatement.executeUpdate();

				if (counter % 250 == 0) {
					LogUtil.debug(this.getClass(), counter + " - " + payableItem.toString());
				}

			}
			LogUtil.log(this.getClass(), counter + " records processed.");
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new RemoteException("Unable to find payable item list. ", ex);
		} finally {
			ConnectionUtil.closeConnection(connection);
		}
	}

	/**
	 * Remove a pending fee AND payable fee
	 * 
	 * NB: would not have worked with OCR since cutover to Receptor in September 05
	 */
	public void removePendingFee(PendingFee pendingFee) throws RollBackException, RemoteException {
		if (pendingFee.getDatePaid() != null) {
			throw new RollBackException("Pending fee '" + pendingFee.getPendingFeeID() + "' has been paid.");
		}
		LogUtil.debug(this.getClass(), "removePendingFee");
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		try {
			Integer clientNumber = pendingFee.getClientNumber();
			String paymentSystem = pendingFee.getPaymentSystem();
			String pendingFeeId = pendingFee.getPendingFeeID();

			String paymentMethodId = null;
			LogUtil.debug(this.getClass(), "paymentSystem=" + paymentSystem);

			// decide between op sequence number and receptor identifier
			if (SourceSystem.RECEPTOR.getAbbreviation().equals(paymentSystem)) {
				paymentMethodId = pendingFee.getPendingFeeID(); // will not work
				// in OCR!!
			} else if (SourceSystem.OCR.getAbbreviation().equals(paymentSystem)) {
				throw new RollBackException("Remove pending fee is not configured for OCR.");
			}

			LogUtil.debug(this.getClass(), "pendingFeeId=" + pendingFeeId);
			LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber);
			LogUtil.debug(this.getClass(), "paymentMethodId=" + paymentMethodId);

			SourceSystem sourceSystem = null;
			try {
				sourceSystem = SourceSystem.getSourceSystem(pendingFee.getSourceSystem());
			} catch (GenericException ex) {
				throw new RollBackException(ex);
			}
			// first remove any pending payment record
			if (clientNumber != null && paymentMethodId != null) {
				LogUtil.debug(this.getClass(), "removePendingFee removePayableFee");
				// receipt may fail but transaction may not complete.
				/**
				 * @todo messy but Receptor does not check the existence of a payable within the delete.
				 */
				Payable payable = null;
				try {
					payable = paymentMgr.getPayable(clientNumber, pendingFee.getOpSequenceNumber().toString(), paymentSystem, sourceSystem);
					if (payable == null) {
						throw new RollBackException("Payable not found.");
					}
				} catch (PaymentException ex1) {
					throw new RollBackException("Unable to get payable", ex1);
				}
				/**
				 * NB: MAY ONLY DELETE PAYABLES FROM THE CURRENT RECEIPTING SYSTEM
				 */
				removePayableFee(clientNumber, paymentMethodId.toString(), sourceSystem, null);
			}

			// remove pending fee record

			hsession.delete(pendingFee);

		} catch (RemoteException re) {
			throw new SystemException("Unable to get the client no or sequence no." + re);
		}
	}

	/**
	 * Commit the data in any sub systems that the payment manager may have a connection to. If the commit fails then be sure to call the rollback method.
	 * 
	 * @exception SystemException Description of the Exception
	 */
	public void commit() throws RemoteException {
		executeAdapterAction(ACTION_COMMIT);
	}

	/**
	 * Get a payment adapter.
	 * 
	 * @return The directDebitAdapter value
	 */
	private TransactionAdapter getPaymentAdapter(String adapterType, SourceSystem sourceSystem) throws RemoteException {
		// construct the key
		String adapterKey = adapterType.concat(sourceSystem == null ? "" : sourceSystem.getAbbreviation());

		TransactionAdapter paymentAdapter = null;
		Object object = this.paymentAdapterList.get(adapterKey);

		if (object == null) {
			// get a new adapter
			if (adapterKey.equals(TransactionAdapter.ADAPTER_DIRECT_DEBIT)) {
				paymentAdapter = PaymentFactory.getDirectDebitAdapter();
			} else if (adapterKey.equals(TransactionAdapter.ADAPTER_FINANCE)) {
				paymentAdapter = PaymentFactory.getFinanceAdapter();
			}
			// adapter type
			else if (adapterType.equals(TransactionAdapter.ADAPTER_RECEIPTING)) {
				paymentAdapter = PaymentFactory.getReceiptingAdapter(sourceSystem);
			} else {
				throw new SystemException("Unknown adapter type or key '" + adapterKey + "'.");
			}
			this.paymentAdapterList.put(adapterKey, paymentAdapter);
		} else {
			if (object instanceof DirectDebitAdapter) {
				paymentAdapter = (DirectDebitAdapter) object;
			} else if (object instanceof FinanceAdapter) {
				paymentAdapter = (FinanceAdapter) object;
			} else if (object instanceof ReceiptingAdapter) {
				paymentAdapter = (ReceiptingAdapter) object;
			} else {
				throw new SystemException("The object type is not available " + object.getClass().getName() + ".");
			}
		}
		return paymentAdapter;
	}

	/**
	 * List of payment adapters.
	 */
	private Hashtable paymentAdapterList;

	/**
	 * Get the receipting adapter.
	 * 
	 * @return The receiptingAdapter value
	 */
	private ReceiptingAdapter getReceiptingAdapter(SourceSystem sourceSystem) throws RemoteException {
		return (ReceiptingAdapter) this.getPaymentAdapter(TransactionAdapter.ADAPTER_RECEIPTING, sourceSystem);
	}

	/**
	 * Get the direct debit adapter.
	 * 
	 * @throws RemoteException
	 * @return DirectDebitAdapter
	 */
	private DirectDebitAdapter getDirectDebitAdapter() throws RemoteException {
		return (DirectDebitAdapter) this.getPaymentAdapter(TransactionAdapter.ADAPTER_DIRECT_DEBIT, null);
	}

	/**
	 * Get the finance adapter
	 * 
	 * @throws RemoteException
	 * @return DirectDebitAdapter
	 */
	private FinanceAdapter getFinanceAdapter() throws RemoteException {
		return (FinanceAdapter) this.getPaymentAdapter(TransactionAdapter.ADAPTER_FINANCE, null);
	}

	/**
	 * Gets the ReceptorReceiptingAdapter so the paid fee information can be passed into RECEPTOR
	 * 
	 */

	public void createPaidTransaction(String username, String branch, String till, String receipt, String customer, String[] products, double[] costs, String[] paymentType, double[] tendered, Properties[] data, String comments, java.util.Date date, String sourceSystem) throws RemoteException, RollBackException {
		try {
			LogUtil.debug(this.getClass(), "createPaidTransaction: " + username + "," + branch + "," + till + "," + receipt + "," + customer + "," + products + "," + costs + "," + paymentType[0] + "," + tendered[0] + "," + data[0] + "," + comments + "," + date + "," + sourceSystem);
			ReceiptingAdapter receiptingAdapter = getReceiptingAdapter(SourceSystem.getSourceSystem(sourceSystem));
			receiptingAdapter.createPaidFee(username, branch, till, receipt, customer, products, costs, paymentType, tendered, data, comments, date, sourceSystem);
		} catch (Exception e) {
			// Any kind of exception will result in a rollback
			e.printStackTrace();
			throw new RemoteException("Unable to create paid fees for receipt " + receipt + ".  " + e);
		}
	}

	/**
	 * Create a payable fee in the receipting system.
	 * 
	 * @param username Description of the Parameter
	 * @param policy Description of the Parameter
	 * @param receipt Description of the Parameter
	 * @param sequence Description of the Parameter
	 * @param pending Description of the Parameter
	 * @param comments Description of the Parameter
	 * @param date Description of the Parameter
	 * @param sourceSystem Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 * @exception RollBackException Description of the Exception
	 */

	public String createPayableFee(Integer clientNumber, String sequenceNumber, String product, Double amountPayable, String description, String sourceSystem, String branch, String userId, Properties[] properties) throws RollBackException {

		String receiptingReturn = null;
		ReceiptingAdapter receiptingAdapter = null;
		try {
			receiptingAdapter = getReceiptingAdapter(SourceSystem.getSourceSystem(sourceSystem));
		} catch (Exception ex) {
			throw new RollBackException(ex);
		}

		LogUtil.debug(this.getClass(), "createPayableFee: " + clientNumber + sequenceNumber + product + amountPayable + description + branch + userId + sourceSystem + properties);

		receiptingReturn = receiptingAdapter.createPayableFee(clientNumber, sequenceNumber, new DateTime(), product, amountPayable.doubleValue(), description, sourceSystem, branch, userId, properties);
		LogUtil.debug(this.getClass(), "ReceiptingAdapter createPayableFee() returned: " + receiptingReturn);
		return receiptingReturn;

	}

	/**
	 * Release OCR (or whatever) addapter
	 */
	public void release() throws RemoteException {
		LogUtil.debug(this.getClass(), "Releasing adapters...");
		releaseAdapters();
	}

	/**
	 * Sends data to finance system for refunds
	 * 
	 * NOT TRANSACTIONAL - COMMITS IN BODY
	 * 
	 * @param payableItem
	 * @param creditDisposal
	 * @throws RemoteException
	 */
	public void performRefund(FinanceVoucher creditDisposal) throws RemoteException {

		LogUtil.log(this.getClass(), "performRefund start");
		LogUtil.log(this.getClass(), "creditDisposal=" + creditDisposal);
		FinanceAdapter financeAdapter = getFinanceAdapter();
		FinancePayee payee = null;
		try {
			payee = creditDisposal.getPayee();
		} catch (GenericException e1) {
			throw new RemoteException(e1.getMessage());
		}
		LogUtil.log(this.getClass(), "payee=" + payee);
		Cheque cheque = creditDisposal.getCheque();
		LogUtil.log(this.getClass(), "cheque=" + cheque);
		try {
			financeAdapter.createVoucher(payee, cheque);
			financeAdapter.commit();
		} catch (Exception e) {
			e.printStackTrace();
			financeAdapter.rollback();
			// rollback jta transaction
			throw new RemoteException("Error creating voucher.", e);
		}
		LogUtil.log(this.getClass(), "performRefund end");
	}

	public void setOCRPayableAmountOutStanding(SourceSystem sourceSystem, Integer clientNumber, String sequenceNumber, BigDecimal amountOutStanding) throws RollBackException

	{
		HistoricalOCRAdapter OCR = PaymentFactory.getHistoricalOCRAdapter();

		try {
			LogUtil.log(this.getClass(), "RLG ++++++++ OCR.setAmountOutStanding ");
			OCR.setAmountOutstanding(clientNumber, sequenceNumber, sourceSystem.getAbbreviation(), amountOutStanding);
		} catch (RemoteException e) {
			throw new RollBackException(e);
		}

	}

}
