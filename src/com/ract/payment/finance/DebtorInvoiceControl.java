package com.ract.payment.finance;

import java.io.Serializable;

/**
 * <p>
 * Control record for the invoice file processing. Prevent multiple debtor files
 * being processed concurrently.
 * </p>
 * 
 * @hibernate.class table="fin_invoice_control" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class DebtorInvoiceControl implements Serializable {
    private String status;

    /**
     * File processing is complete so further processing can continue.
     */
    public static String STATUS_READY = "Ready";

    /**
     * File processing in in progress
     */
    public static String STATUS_IN_PROGRESS = "In Progress";

    public void setStatus(String status) {
	this.status = status;
    }

    /**
     * @hibernate.id column="status" generator-class="assigned"
     * @return String
     */
    public String getStatus() {
	return status;
    }

    public DebtorInvoiceControl() {
    }
}
