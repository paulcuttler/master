package com.ract.payment.finance;

public class DebtorInvoiceReceiptPK implements java.io.Serializable {
    public void setReceiptNumber(String receiptNumber) {
	this.receiptNumber = receiptNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
	this.invoiceNumber = invoiceNumber;
    }

    public DebtorInvoiceReceiptPK() {
    }

    private String receiptNumber;

    private String invoiceNumber;

    /**
     * @hibernate.key-property column="receipt_number"
     */
    public String getReceiptNumber() {
	return receiptNumber;
    }

    /**
     * @hibernate.key-property column="invoice_number"
     */
    public String getInvoiceNumber() {
	return invoiceNumber;
    }

    public boolean equals(Object obj) {
	if (obj instanceof DebtorInvoiceReceiptPK) {
	    DebtorInvoiceReceiptPK that = (DebtorInvoiceReceiptPK) obj;
	    return this.invoiceNumber.equals(that.invoiceNumber) && this.receiptNumber.equals(that.receiptNumber);
	}
	return false;
    }

    public int hashCode() {
	return this.invoiceNumber.hashCode() + this.receiptNumber.hashCode();
    }

    public String toString() {
	return this.invoiceNumber + "/" + this.receiptNumber;
    }

}
