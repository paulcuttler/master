package com.ract.payment.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.ract.payment.receipting.Receipt;
import com.ract.payment.receipting.ReceiptLine;
import com.ract.util.DateTime;

/**
 * <p>
 * A receipt attached to a debtor invoice.
 * </p>
 * 
 * @hibernate.class table="fin_invoice_receipt" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class DebtorInvoiceReceipt implements Receipt, Comparable, Serializable {
    private DebtorInvoiceReceiptPK debtorInvoiceReceiptPK;

    private DateTime createDate;

    private BigDecimal receiptAmount;

    private String logonId;

    private String salesBranch;

    private DateTime backDate;

    private String customerNumber;

    private String narrative;

    /**
     * @hibernate.property
     */
    public String getNarrative() {
	return narrative;
    }

    public void setNarrative(String narrative) {
	this.narrative = narrative;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="effective_date"
     */
    public DateTime getBackDate() {
	return backDate;
    }

    public String getReceiptName() {
	return "Debtor Receipt";
    }

    public String getInvoiceNumber() {
	return this.getDebtorInvoiceReceiptPK().getInvoiceNumber();
    }

    public BigDecimal getChangeAmount() {
	return new BigDecimal(0);
    }

    public BigDecimal getTenderedAmount() {
	return this.getReceiptAmount();
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public void setLogonId(String logonId) {
	this.logonId = logonId;
    }

    public void setReceiptAmount(BigDecimal receiptAmount) {
	this.receiptAmount = receiptAmount;
    }

    public void setSalesBranch(String salesBranch) {
	this.salesBranch = salesBranch;
    }

    public void setBackDate(DateTime backDate) {
	this.backDate = backDate;
    }

    public void setCustomerNumber(String customerNumber) {
	this.customerNumber = customerNumber;
    }

    public void setDebtorInvoiceReceiptPK(DebtorInvoiceReceiptPK debtorInvoiceReceiptPK) {
	this.debtorInvoiceReceiptPK = debtorInvoiceReceiptPK;
    }

    public String getReceiptNumber() {
	return this.getDebtorInvoiceReceiptPK().getReceiptNumber();
    }

    /**
     * @hibernate.property
     * @hibernate.column name="create_date"
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    public String getLogonId() {
	return logonId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="receipt_amount"
     */
    public BigDecimal getReceiptAmount() {
	return receiptAmount;
    }

    public String getSalesBranch() {
	return salesBranch;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="customer_number"
     */
    public String getCustomerNumber() {
	return customerNumber;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public DebtorInvoiceReceiptPK getDebtorInvoiceReceiptPK() {
	return debtorInvoiceReceiptPK;
    }

    public DebtorInvoiceReceipt() {
	// default
    }

    public Integer getBranchNo() {
	return null;
    }

    public ReceiptLine getReceiptLine(int i) {
	return null;
    }

    public ArrayList getReceiptLineList() {
	return null;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(getDebtorInvoiceReceiptPK().getReceiptNumber());
	desc.append("/");
	desc.append(getDebtorInvoiceReceiptPK().getInvoiceNumber());
	desc.append(":");
	desc.append(getCustomerNumber());
	desc.append(" ");
	desc.append(getReceiptAmount());
	desc.append(" ");
	desc.append(getBackDate());
	return desc.toString();
    }

    public int compareTo(Object o) {
	if (o instanceof DebtorInvoiceReceipt) {
	    DebtorInvoiceReceipt dir = (DebtorInvoiceReceipt) o;
	    int recReturnValue = this.getDebtorInvoiceReceiptPK().getReceiptNumber().compareTo(dir.getDebtorInvoiceReceiptPK().getReceiptNumber());
	    if (recReturnValue == 0) {
		return this.getDebtorInvoiceReceiptPK().getInvoiceNumber().compareTo(dir.getDebtorInvoiceReceiptPK().getInvoiceNumber());
	    } else {
		return recReturnValue;
	    }
	} else {
	    return 0;
	}
    }

}
