package com.ract.payment.finance;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ExceptionHelper;
import com.ract.common.MailMgr;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.mail.MailMessage;
import com.ract.payment.FinanceVoucher;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentFactory;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.receipting.Payable;
import com.ract.payment.receipting.ReceiptingAdapter;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileComparator;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

/**
 * Manage finance related information.
 * 
 * @author LewisG, John Holliday
 * @created 1 August 2002
 */

@Stateless
@Remote({ FinanceMgr.class })
@Local({ FinanceMgrLocal.class })
public class FinanceMgrBean
{
    private static final String FIN_AUDIT_DIR = "FIN_AUDIT_DIR";

    @PersistenceContext(unitName = "master")
    Session hsession;

    @PersistenceContext(unitName = "master")
    private EntityManager em;

    @Resource
    private SessionContext sessionContext;

    @Resource(mappedName = "java:/ClientDS")
    private DataSource dataSource;

    private final DecimalFormat FORMATTER_MONTH = new DecimalFormat("00");

    private final DecimalFormat FORMATTER_YEAR = new DecimalFormat("0000");

    private final DecimalFormat FORMATTER_ACCOUNT = new DecimalFormat("0000");

    private final int MAX_EARNINGS_PERIOD = 366;

    private Interval EARNABLE_PERIOD = null;
    {
	try
	{
	    // 2 years and 1 month as we want to account for earning in the
	    // previous month
	    EARNABLE_PERIOD = new Interval(2, 1, 0, 0, 0, 0, 0);
	}
	catch (Exception e)
	{
	    LogUtil.fatal(this.getClass(),
		    "Unable to construct an interval to represent one year."
			    + e);
	}
    }

    /**
     * Array of query components
     */
    private final String EARNINGS_SQL[] = {
	    "SELECT a.accountnumber,a.amount,a.payableitempostingid,a.postat,c.startdate,c.enddate, c.clientno, a.createdate "
	    	+ ", c.payableItemID " 
//rlg
//	    	+ " ,m.membership_id , groupcount =  (select COUNT(*) as groupcount from pub.mem_membership_group gg, "
//	    	+ "( select g.group_Id from PUB.mem_membership_group g where g.membership_id = ( select mx.membership_id from PUB.mem_membership "
//	    	+ "mx where mx.client_number = m.client_number )) a where gg.group_Id = a.group_Id )"
//rlg
		    + // postat
		    "FROM PUB.pt_payableitemposting a, PUB.pt_payableitemcomponent b, PUB.pt_payableitem c "
//		    + ", PUB.mem_membership m "
		    + "WHERE a.payableitemcomponentid = b.payableitemcomponentid "
		    + "AND b.payableitemid = c.payableitemid "
//		    + " and m.client_number = c.clientno "
		    +
		    // only "earnable" accounts
		    "AND (a.incomeaccountnumber IS NOT NULL) and (a.incomeaccountnumber != '') "
		    +
		    // exclude historical earnings postings - we will calculate
		    // them.
		    "AND a.description NOT LIKE 'Transfer of%unearned to earned.' ",
	    // include by post at date
	    "AND a.postAt <= ? " + "AND a.postAt >= ? ",
	    // include by start date
	    "AND c.startdate <= ? " + "AND c.startdate >= ? " };

    /**
     * Constructor
     */
    public FinanceMgrBean()
    {
    }

    /**
     * Create a debtor header record corresponding to a client and also transfer
     * to Finance system
     * 
     * @param debtor
     *            Debtor
     * @throws FinanceException
     */
    public void createDebtor(Debtor debtor) throws FinanceException
    {

	LogUtil.debug(this.getClass(), "get session" + hsession);
	LogUtil.debug(this.getClass(), "saving debtor" + debtor);
	hsession.save(debtor);
	LogUtil.debug(this.getClass(), "calling update debtor");
	FinanceAdapter financeAdapter = PaymentFactory.getFinanceAdapter();
	financeAdapter.updateDebtor(debtor);
    }

    public void processDebtorMasterFiles() throws RemoteException
    {
	LogUtil.debug(this.getClass(), "processDebtorMasterFiles start");
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	String invoiceDir = commonMgr.getSystemParameterValue(
		SystemParameterVO.CATEGORY_PAYMENT,
		SystemParameterVO.FINANCE_INVOICE_DIRECTORY);

	DateTime now = new DateTime();

	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	ClientVO client = null;

	File dir = new File(invoiceDir);

	FilenameFilter diff = new DebtorMasterFilenameFilter();
	// debtor invoice files only (ie. starts with drin
	File[] children = dir.listFiles(diff);

	Arrays.sort(children, new FileComparator());
	LogUtil.debug(this.getClass(), "files=" + children);
	if (children == null)
	{
	    // no files to process
	}
	else
	{
	    // files to process
	    for (int i = 0; i < children.length; i++)
	    {

		// Get filename of file or directory
		File file = children[i];

		Connection connection = null;
		PreparedStatement statement = null;

		try
		{
		    LogUtil.log(this.getClass(), "processDebtorMasterFiles i="
			    + i + ", file=" + file);

		    String elements[] = null;

		    FileInputStream fis = new FileInputStream(file);
		    BufferedReader inputFileReader = new BufferedReader(
			    new InputStreamReader(fis));
		    CSVReader reader = new CSVReader(inputFileReader);

		    int counter = 0;
		    int errors = 0;

		    LogUtil.log(this.getClass(),
			    "Deleting existing debtor master records.");

		    final String SQL_DELETE_DEBTOR_MASTER = "delete from pub.fin_debtor_master";

		    // delete all stored debtor records

		    connection = dataSource.getConnection();
		    statement = connection.prepareStatement(SQL_DELETE_DEBTOR_MASTER);
		    int deleted = statement.executeUpdate();

		    LogUtil.log(this.getClass(),
			    "Deleted existing debtor master records. "
				    + deleted);

		    while ((elements = reader.readNext()) != null)
		    {

			counter++;
			LogUtil.log(this.getClass(), "counter=" + counter);
			try
			{
			    LogUtil.debug(this.getClass(), "elements="
				    + elements.length);
			    LogUtil.debug(this.getClass(), "elements="
				    + elements);
			    // skip the header
			    if (counter > 1)
			    {

				DebtorMaster debtorMaster = new DebtorMaster();
				debtorMaster.setAccountNumber(elements[0]);
				debtorMaster.setSurname(elements[1]);
				debtorMaster.setGivenNames(elements[2]);
				debtorMaster.setPostalName(elements[6]);
				debtorMaster.setAddress1(elements[7]);
				debtorMaster.setAddress2(elements[8]);
				debtorMaster.setAddress3(elements[9]);
				debtorMaster.setCity(elements[10]);
				debtorMaster.setState(elements[11]);
				debtorMaster.setPostcode(elements[12]);
				debtorMaster.setPhoneNumber(elements[13]);
				debtorMaster.setEmailAddress(elements[14]);
				hsession.save(debtorMaster);
				LogUtil.log(this.getClass(),
					"Saved debtor master.");

				Integer clientNumber = new Integer(
					debtorMaster.getAccountNumber());
				Debtor debtor = (Debtor) hsession.get(
					Debtor.class, clientNumber);
				if (debtor == null)
				{
				    // save a new record
				    debtor = new Debtor();
				    debtor.setClientNumber(clientNumber);
				    debtor.setCreateDate(now);
				    hsession.save(debtor);
				    LogUtil.log(this.getClass(),"Created local debtor record.");
				}

				client = clientMgr.getClient(clientNumber);

				boolean valid = false;
				if (client != null)
				{
				    try
				    {
					valid = debtorMaster.equals(client);
				    }
				    catch (Exception ex1)
				    {
					// ignore
					LogUtil.debug(this.getClass(), ex1);
				    }
				}

				LogUtil.log(this.getClass(), "debtorMaster=" + debtorMaster);
				LogUtil.log(this.getClass(), "valid=" + valid);

			    }

			}
			catch (Exception ex)
			{
			    errors++;
			    LogUtil.warn(this.getClass(),
				    "Unable to process debtor master for line "
					    + counter + " " + ex.getMessage());
			}

		    }// end of file

		    file.delete();

		    hsession.flush();

		}// end of files

		catch (IOException ex)
		{
		    LogUtil.warn(
			    this.getClass(),
			    "Unable to process any debtor master files "
				    + ExceptionHelper
					    .getExceptionStackTrace(ex));
		}
		catch (SQLException se)
		{
		    LogUtil.warn(
			    this.getClass(),
			    "Unable to clear debtor master file "
				    + ExceptionHelper
					    .getExceptionStackTrace(se));

		} finally
		{
		    ConnectionUtil.closeConnection(connection, statement);
		}
	    }

	}

	LogUtil.debug(this.getClass(), "processDebtorMasterFiles end");
    }

    /**
     * Maintain the current invoice list.
     * 
     * @throws RemoteException
     */
    public void processDebtorInvoiceFiles() throws RemoteException
    {
	LogUtil.log(this.getClass(), "processDebtorFiles start");
	final int ELEMENT_COUNT = 11; // TECHONE scripts omits NOTE

	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	String invoiceDir = commonMgr.getSystemParameterValue(
		SystemParameterVO.CATEGORY_PAYMENT,
		SystemParameterVO.FINANCE_INVOICE_DIRECTORY);
	LogUtil.log(this.getClass(), "processDebtorFiles " + invoiceDir);
	DateTime now = new DateTime();

	FinanceMgrLocal financeMgrLocal = PaymentEJBHelper.getFinanceMgrLocal();

	// check database entry to see if files are being processed
	boolean alreadyInProgress = financeMgrLocal
		.startDebtorFileProcessing(now);

	if (!alreadyInProgress)
	{
	    File dir = new File(invoiceDir);
	    DebtorInvoice di = null;
	    DebtorInvoicePK diPK = null;
	    FilenameFilter diff = new DebtorInvoiceFilenameFilter();
	    // debtor invoice files only (ie. starts with drin
	    File[] children = dir.listFiles(diff);

	    if (children == null)
	    {
		// no files to process
	    }
	    else
	    {

		Arrays.sort(children, new FileComparator());
		LogUtil.debug(this.getClass(), "files=" + children);

		// files to process
		for (int i = 0; i < children.length; i++)
		{

		    // Get filename of file or directory
		    File file = children[i];

		    LogUtil.log(this.getClass(), "processDebtorInvoiceFiles i="
			    + i + ", file=" + file);

		    // process the first
		    if (i < 1)
		    {
			// prepend the directory name

			try
			{
			    String elements[] = null;

			    FileInputStream fis = new FileInputStream(file);
			    BufferedReader inputFileReader = new BufferedReader(
				    new InputStreamReader(fis));
			    CSVReader reader = new CSVReader(inputFileReader);

			    int counter = 0;
			    int dataLines = 0;
			    int errors = 0;
			    while ((elements = reader.readNext()) != null)
			    {
				counter++;
				LogUtil.log(this.getClass(), "counter="
					+ counter);
				try
				{
				    LogUtil.debug(this.getClass(), "elements="
					    + elements.length);
				    LogUtil.debug(this.getClass(), "elements="
					    + elements);
				    // skip the header
				    if (counter > 1
					    && elements.length == ELEMENT_COUNT)
				    {
					dataLines++;
					LogUtil.debug(this.getClass(),
						"non-header line");
					// get the individual elements
					// 0 - Account Number
					// 1 - Invoice Number
					// 2 - Amount (Outstanding)
					// 3 - Effective Date
					// 4 - Narration
					// 5+ - ignored
					SimpleDateFormat sd = new SimpleDateFormat(
						"dd/MM/yyyy");
					SimpleDateFormat sd2 = new SimpleDateFormat(
						"dd/MM/yyyy hh:mm:ss aaa");
					DateTime effectiveDate = null;
					// two attempts at getting the correct
					// date format
					// (with/without date)
					try
					{
					    effectiveDate = new DateTime(
						    sd.parse(elements[3]));
					}
					catch (ParseException ex1)
					{
					    // ignore for now
					    effectiveDate = new DateTime(
						    sd2.parse(elements[3]));
					    LogUtil.debug(this.getClass(),
						    "effectiveDate="
							    + effectiveDate);
					}

					diPK = new DebtorInvoicePK();
					diPK.setInvoiceNumber(elements[1]);
					diPK.setStatus(DebtorInvoicePK.STATUS_ACTIVE); // should
					// be
					// default
					// anyway
					diPK.setCreateDate(now);
					di = new DebtorInvoice(elements[0],
						new BigDecimal(elements[2]),
						effectiveDate, elements[4]);
					di.setDebtorInvoicePK(diPK);

					LogUtil.debug(this.getClass(), "di="
						+ di.toString());

					// new CMT transaction!
					financeMgrLocal
						.createDebtorInvoiceForPayment(
							di, now);
				    }

				}
				catch (Exception ex)
				{
				    errors++;
				    LogUtil.warn(
					    this.getClass(),
					    "Unable to process debtor invoice for line "
						    + counter
						    + " "
						    + ExceptionHelper
							    .getExceptionStackTrace(ex));
				}
			    } // end of lines

			    LogUtil.log(
				    this.getClass(),
				    "Processed all lines in file "
					    + file.getName());

			    reader.close();

			    LogUtil.log(this.getClass(),
				    "Deleting input file: " + file.getName());

			    // delete file when processed - any errors are
			    // resolved in the
			    // next upload
			    file.delete();

			    LogUtil.log(this.getClass(),
				    "Deleting old debtor invoices");

			    financeMgrLocal.deleteOldDebtorInvoices(now);

			    LogUtil.log(this.getClass(),
				    "errors/dataLines/totalLines=" + errors
					    + "/" + dataLines + "/" + counter);

			}
			catch (Exception ex3)
			{
			    LogUtil.warn(
				    this.getClass(),
				    "Unable to process file "
					    + file
					    + " "
					    + ExceptionHelper
						    .getExceptionStackTrace(ex3));
			}

		    } // end of current file
		    else
		    {
			// delete the other files
			LogUtil.log(
				this.getClass(),
				"deleting "
					+ file
					+ " as only one file per invoice load may be processed.");
			file.delete();
		    }

		} // end of file list

	    } // end of files available

	    // update database entry to finished to allow further processing
	    financeMgrLocal.endDebtorFileProcessing(now);

	} // end of inProgress
	else
	{
	    LogUtil.warn(this.getClass(),
		    "Not processing invoices as the invoice load is already in Progress.");
	}

	LogUtil.log(this.getClass(), "processDebtorFiles end");
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteOldDebtorInvoices(DateTime now) throws FinanceException
    {
	LogUtil.log(this.getClass(), "deleteOldDebtorInvoices start");
	// delete any invoices that are no longer outstanding

	// Active with a last update before now
	List invoiceList = hsession
		.createCriteria(DebtorInvoice.class)
		.add(Expression.lt("lastUpdate", now))
		.add(Expression.eq("debtorInvoicePK.status",
			DebtorInvoicePK.STATUS_ACTIVE)).list();

	LogUtil.debug(
		this.getClass(),
		"Deleting invoices, no longer outstanding="
			+ invoiceList.size());
	FinanceMgrLocal financeMgrLocal = PaymentEJBHelper.getFinanceMgrLocal();
	DebtorInvoice invoice = null;
	for (Iterator it = invoiceList.iterator(); it.hasNext();)
	{
	    invoice = (DebtorInvoice) it.next();
	    // delete debtor invoice - new transaction
	    LogUtil.debug(this.getClass(),
		    "Deleting invoice, no longer outstanding=" + invoice);
	    try
	    {
		// update the create date for the deleted records.

		// NEW TRANSACTION!!!
		financeMgrLocal.deleteDebtorInvoice(null, invoice, now, false);
	    }
	    catch (Exception ex2)
	    {
		LogUtil.warn(this.getClass(), "Unable to delete invoice: "
			+ ex2.getMessage());
	    }
	}
	LogUtil.log(this.getClass(), "deleteOldDebtorInvoices end");
    }

    public void deleteDebtorInvoice(
	    PaymentTransactionMgr paymentTransactionMgr, DebtorInvoice invoice,
	    DateTime now, boolean update) throws RemoteException,
	    RollBackException
    {
	boolean remove = false;
	if (paymentTransactionMgr == null)
	{
	    // created new paymentTransactionMgr
	    paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();
	    remove = true;
	}

	try
	{

	    // delete existings record
	    LogUtil.log(this.getClass(), "deleteDebtorInvoice delete="
		    + invoice + " " + update);
	    hsession.delete(invoice);
	    hsession.flush();

	    // create new one
	    invoice.setLastUpdate(now);
	    invoice.getDebtorInvoicePK().setStatus(
		    DebtorInvoicePK.STATUS_DELETED);
	    invoice.getDebtorInvoicePK().setCreateDate(now);
	    LogUtil.log(this.getClass(), "deleteDebtorInvoice save=" + invoice);
	    hsession.save(invoice);
	    hsession.flush();

	    try
	    {
		paymentTransactionMgr.removePayableFee(
			new Integer(invoice.getCustomerNumber()), invoice
				.getDebtorInvoicePK().getInvoiceNumber(),
			SourceSystem.FIN, INVOICE_USER);
	    }
	    catch (RollBackException ex1)
	    {
		LogUtil.warn(this.getClass(), "Unable to remove payable fee. "
			+ ex1.getMessage());
		if (ex1.getMessage().indexOf(
			ReceiptingAdapter.PAYABLE_EXISTS_RECEIPTS_ATTACHED) != -1)
		{
		    if (update)
		    {
			LogUtil.log(this.getClass(),
				"Ignoring removePayableFee error as we will be updating the payable fee later.");
		    }
		    else
		    {
			LogUtil.log(this.getClass(),
				"Updating payable amount to effectively delete it.");
			// update payable amount outstanding to 0
			// paymentTransactionMgr.updatePayableAmount(SourceSystem.FIN,
			// new Integer(invoice.getCustomerNumber()),
			// invoice.getDebtorInvoicePK().getInvoiceNumber(), new
			// BigDecimal(0));
		    }

		    // commit in the main transaction

		}
		else
		{
		    // rethrow exception
		    throw new Exception(ex1.getMessage());
		}
	    }

	    LogUtil.debug(this.getClass(), "deleteDebtorInvoice commit");
	    paymentTransactionMgr.commit();

	}
	catch (Exception ex)
	{
	    paymentTransactionMgr.rollback();
	    throw new RemoteException("Unable to delete debtor invoice.", ex);
	} finally
	{
	    // remove only if created in this scope otherwise leave it to
	    // calling program to close
	    if (remove)
	    {
		removePaymentTransactionMgr(paymentTransactionMgr);
	    }
	}
    }

    /**
     * Remove the stateful session bean
     * 
     * @param paymentTransactionMgr
     *            PaymentTransactionMgr
     */
    private void removePaymentTransactionMgr(
	    PaymentTransactionMgr paymentTransactionMgr)
    {
	LogUtil.debug(this.getClass(), "removePaymentTransactionMgr="
		+ paymentTransactionMgr);
	if (paymentTransactionMgr != null)
	{
	    try
	    {
		paymentTransactionMgr.release();
	    }
	    catch (Exception ex1)
	    {
		LogUtil.warn(
			this.getClass(),
			"Unable to remove paymentTransactionMgr. "
				+ ex1.getMessage());
	    }
	}
    }

    /**
     * Get a collection of Receptor receipts that are available to be
     * transferred to the Finance system. This is in a new transaction to
     * prevent deadlocks.
     * 
     * @throws FinanceException
     * @return Collection
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Collection getReceptorReceiptsToTransfer() throws FinanceException
    {

	Collection receiptList = new ArrayList();
	Collection receipt = null;
	BigDecimal totalAmount = null;
	DateTime backDate = null;
	String receiptNumber = null;

	// get a list of receipts in Receptor that are attached to purchases
	// with the product code
	// of "INVOICE" and do not exist in fin_invoice_receipts table.
	final String SQL_RECEPTOR_RECEIPTS = "SELECT  DISTINCT pu.RECEIPT_ID, "
		+ "li.TOTAL_COST, " + "pu.POST_DATE "
		+ "FROM daltonc.JALIFE_PRODUCTS pr, "
		+ "     daltonc.JALIFE_SELLABLE se, "
		+ "     daltonc.JALIFE_LINEITEMS li, "
		+ "     daltonc.JALIFE_PURCHASES pu "
		+ "WHERE li.SELLABLE_ID = se.SELLABLE_ID "
		+ "and se.PRODUCT_ID = pr.PRODUCT_ID "
		+ "and se.SELLABLE_ID = li.SELLABLE_ID "
		+ "and li.PURCHASE_ID = pu.PURCHASE_ID "
		+ "and (pr.PRODUCT_CODE = '" + PRODUCT_INVOICE + "') "
		+ "AND (pu.RECEIPT_ID NOT IN "
		+ "   (SELECT     receipt_number "
		+ "    FROM          PUB.fin_invoice_receipt fir)) ";

	LogUtil.log(this.getClass(), "SQL=" + SQL_RECEPTOR_RECEIPTS);

	PreparedStatement receiptStatement = null;
	Connection connection = null;

	try
	{
	    connection = dataSource.getConnection();
	    receiptStatement = connection
		    .prepareStatement(SQL_RECEPTOR_RECEIPTS);
	    //
	    ResultSet rs = receiptStatement.executeQuery();
	    if (rs != null)
	    {
		while (rs.next())
		{
		    // receipt number
		    receiptNumber = rs.getString(1);
		    // amount on receipt (rounded!)
		    totalAmount = rs.getBigDecimal(2);
		    // post date
		    backDate = new DateTime(rs.getDate(3));

		    // add receipt
		    receipt = new ArrayList();
		    receipt.add(receiptNumber); // 0
		    receipt.add(totalAmount); // 1
		    receipt.add(backDate); // 2

		    receiptList.add(receipt);

		}

	    }
	    rs.close(); // close resultset before writing file
	}
	catch (SQLException ex)
	{
	    throw new FinanceException(SQL_RECEPTOR_RECEIPTS, ex);
	} finally
	{
	    ConnectionUtil.closeConnection(connection, receiptStatement);
	}

	return receiptList;

    }

    public DebtorInvoice getInvoice(String invoiceNumber)
    {

	DebtorInvoice debtorInvoice = null;
	List invoiceList = hsession
		.createCriteria(DebtorInvoice.class)
		.add(Expression.eq("debtorInvoicePK.invoiceNumber",
			invoiceNumber)).list();
	if (invoiceList != null)
	{
	    // only select one
	    for (Iterator i = invoiceList.iterator(); i.hasNext()
		    && debtorInvoice == null;)
	    {
		debtorInvoice = (DebtorInvoice) i.next();
	    }
	}
	return debtorInvoice;
    }

    public Collection getInvoicesByClientNumber(String customerNumber)
    {

	DebtorInvoice debtorInvoice = null;
	ArrayList invoiceList = new ArrayList();

	List customerInvoiceList = hsession.createCriteria(DebtorInvoice.class)
		.add(Expression.eq("customerNumber", customerNumber)).list();

	Hashtable invoices = new Hashtable();
	DebtorInvoice tmp = null;
	// only add one record per unique invoice number regardless of status
	for (Iterator i = customerInvoiceList.iterator(); i.hasNext();)
	{
	    debtorInvoice = (DebtorInvoice) i.next();
	    tmp = (DebtorInvoice) invoices.get(debtorInvoice
		    .getDebtorInvoicePK().getInvoiceNumber());
	    if (tmp == null)
	    {
		invoices.put(debtorInvoice.getDebtorInvoicePK()
			.getInvoiceNumber(), debtorInvoice);
		invoiceList.add(debtorInvoice);
	    }
	}
	return invoiceList;
    }

    final String PAYMENT_METHOD_CASH = "CASH";

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Collection getReceptorInvoices(String receiptNumber,
	    DateTime backDate, DateTime createDate) throws Exception
    {
	PreparedStatement invoiceStatement = null;

	// get the invoice details attached to the receipt
	final String SQL_INVOICES = "select pli.reference_id, pli.comments, li.total_cost, org.org_code, pr.product_code, loc.location_code, "
		+ "case when pay.type_id in ('CREDIT','DEBIT') then 'EFT' when pay.type_id in ('CASH','CHEQUE') then 'CC' "
		+ "when pay.type_id = 'AMEX' then 'AMX' when pay.type_id = 'DINERS' then 'DNR' when pay.type_id = 'BPAY' then 'BPY' else pay.type_id end as pay_type, ti.till_code "
		+ "from daltonc.JALIFE_LINEITEMS li "
		+ "left outer join daltonc.JALIFE_PENDING_COMPLETIONS pc on pc.lineitem_id = li.lineitem_id "
		+ "left outer join daltonc.JALIFE_PENDING_LINEITEMS pli on pli.PENDING_ITEM_ID = pc.PENDING_ITEM_ID "
		+ "left outer join daltonc.JALIFE_PENDING_PURCHASES pp on pp.PENDING_ID = pli.PENDING_ID "
		+ "inner join daltonc.JALIFE_PURCHASES pu on li.purchase_id = pu.purchase_id "
		+ "left outer join daltonc.JALIFE_PAYMENTS pay on pu.purchase_id = pay.purchase_id, "
		+ "daltonc.JALIFE_SELLABLE se, "
		+ "daltonc.JALIFE_PRODUCTS pr, "
		+ "daltonc.JALIFE_ORGANISATIONS org, "
		+ "daltonc.JALIFE_TILLS ti, "
		+ "daltonc.JALIFE_LOCATIONS loc "
		+ "WHERE li.SELLABLE_ID = se.SELLABLE_ID "
		+ "and se.PRODUCT_ID = pr.PRODUCT_ID "
		+ "and loc.location_id = pu.location_id "
		+ "and li.customer_id = org.org_id "
		+ "and pu.till_id = ti.till_id "
		+ "and (pay.sequence_id = 1 or pay.sequence_id is null) "
		+ "and pu.receipt_id = ? ";

	String invoiceNumber = null;
	String debtorCustomerNumber = null;
	String invoiceComment = null;
	String productCode = null;
	BigDecimal receiptAmount = new BigDecimal(0);
	BigDecimal invoiceAmount = null;
	DebtorInvoiceReceipt dir = null;
	Collection invoiceReceipts = new ArrayList();
	String location = null;
	String paymentMethod = null;
	String tillId = null;
	String narrative = null;
	ResultSet invoiceRS = null;
	Connection connection = null;
	try
	{
	    connection = dataSource.getConnection();

	    LogUtil.log(this.getClass(), "SQL_INVOICES=" + SQL_INVOICES);
	    invoiceStatement = connection.prepareStatement(SQL_INVOICES);
	    LogUtil.log(this.getClass(), "receiptNumber=" + receiptNumber);
	    invoiceStatement.setString(1, receiptNumber);
	    invoiceRS = invoiceStatement.executeQuery();
	    LogUtil.log(this.getClass(), "invoiceRS=" + invoiceRS);
	    // append each invoice
	    while (invoiceRS.next())
	    {
		invoiceNumber = invoiceRS.getString(1);
		LogUtil.debug(this.getClass(), "invoiceNumber=" + invoiceNumber);
		if (invoiceNumber != null
			&& invoiceNumber.indexOf(REFERENCE_DELIMITER) != -1)
		{
		    LogUtil.debug(this.getClass(), "split");
		    String elements[] = invoiceNumber
			    .split(REFERENCE_DELIMITER);
		    LogUtil.debug(this.getClass(), "elements=" + elements);
		    if (elements.length > 1)
		    {
			invoiceNumber = elements[1];
			if (invoiceNumber == null
				|| invoiceNumber.trim().equals(""))
			{
			    throw new Exception("Invoice number is blank.");
			}
		    }
		    else
		    {
			throw new Exception("Invoice number is not valid.");
		    }
		}
		LogUtil.debug(this.getClass(), "invoiceNumber=" + invoiceNumber);

		invoiceComment = invoiceRS.getString(2);
		LogUtil.debug(this.getClass(), "invoiceComment="
			+ invoiceComment);

		invoiceAmount = invoiceRS.getBigDecimal(3);
		debtorCustomerNumber = invoiceRS.getString(4);
		productCode = invoiceRS.getString(5);

		location = invoiceRS.getString(6);
		if (location.length() > 2)
		{
		    location = location.substring(0, 2);
		}
		paymentMethod = invoiceRS.getString(7);
		// default to cash
		if (invoiceRS.wasNull() || paymentMethod == null
			|| paymentMethod.trim().equals(""))
		{
		    paymentMethod = PAYMENT_METHOD_CASH;
		}

		if (paymentMethod.length() > 2)
		{
		    paymentMethod = paymentMethod.substring(0, 2);
		}

		tillId = invoiceRS.getString(8);
		// eg. HBEFHOBCTR04
		narrative = location + paymentMethod + tillId;

		// pad it out to 8 digits with leading zeros
		debtorCustomerNumber = StringUtil.padNumber(new Integer(
			debtorCustomerNumber), 8);

		invoiceAmount = invoiceAmount.setScale(2,
			BigDecimal.ROUND_HALF_DOWN);
		receiptAmount = receiptAmount.add(invoiceAmount);

		LogUtil.log(this.getClass(), "Invoice=" + invoiceNumber + ":"
			+ invoiceAmount + " " + receiptAmount + " "
			+ debtorCustomerNumber + " " + productCode);

		// only add if it is an invoice line item and has a valid
		// invoice number
		if (invoiceNumber != null && productCode != null
			&& productCode.equals(PRODUCT_INVOICE))
		{
		    LogUtil.debug(this.getClass(), "Adding invoice receipt");
		    dir = new DebtorInvoiceReceipt();
		    // setup primary key
		    DebtorInvoiceReceiptPK debtorInvoiceReceiptPK = new DebtorInvoiceReceiptPK();
		    debtorInvoiceReceiptPK.setInvoiceNumber(invoiceNumber);
		    debtorInvoiceReceiptPK.setReceiptNumber(receiptNumber);
		    // set other debtor receipt attributes
		    dir.setDebtorInvoiceReceiptPK(debtorInvoiceReceiptPK);
		    dir.setCustomerNumber(debtorCustomerNumber);
		    dir.setBackDate(backDate);
		    dir.setCreateDate(createDate);
		    dir.setReceiptAmount(invoiceAmount);
		    dir.setNarrative(narrative);
		    invoiceReceipts.add(dir);
		}

	    } // end of invoice loop
	}
	catch (SQLException ex)
	{
	    ex.printStackTrace();
	    throw new Exception(ex);
	} finally
	{
	    if (invoiceRS != null)
	    {
		invoiceRS.close();
	    }
	    ConnectionUtil.closeConnection(connection, invoiceStatement);
	}

	return invoiceReceipts;

    }

    final String REFERENCE_DELIMITER = "-";

    final String PREFIX_INVOICE = "A";

    final String PREFIX_RECEIPT = "S"; // Sale

    final String PREFIX_CREDIT_NOTE = "C"; // Sale

    /**
     * Transfer the debtor invoice receipts to the finance system.
     */
    public void transferDebtorInvoiceReceipts() throws FinanceException
    {
	LogUtil.log(this.getClass(), "transferDebtorInvoiceReceipts start");
	final DateTime now = new DateTime();
	final String PREFIX_INVOICE_RECEIPTS = "redr____";

	final String YEAR_CURRENT = "C";
	final String YEAR_PREVIOUS = "P";

	final SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
	final String FIN1_HEADER = "\"FORMAT BATCH IMPORT\",\"STANDARD 1.0\"";
	final String FIN1_REDR_HEADING = "\"DOCID\",\"DREF1\",\"DREF2\",\"DDATE1\",\"DDATE2\",\"DPERIOD\",\"LINEID\",\"LACCNBR\",\"LAMOUNT1\",\"LNARR1\",\"LNARR2\",\"LNARR3\",\"LUSERFLD4\",\"YEAR\"";

	String fileName = null;
	Hashtable documentKey = null;
	String voucher = null;
	String timestamp = null;

	try
	{

	    String directory = null;
	    try
	    {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		directory = commonMgr.getSystemParameterValue(
			SystemParameterVO.CATEGORY_PAYMENT,
			SystemParameterVO.FINANCE_RECEIPT_DIRECTORY);
	    }
	    catch (RemoteException ex2)
	    {
		throw new FinanceException(
			"Unable to find the finance receipt directory.", ex2);
	    }

	    FinanceOneFinanceAdapter fofa = null;
	    try
	    {
		fofa = (FinanceOneFinanceAdapter) PaymentFactory
			.getFinanceAdapter(FinanceAdapter.FINANCE_SYSTEM_FINANCE_ONE);
	    }
	    catch (SystemException ex1)
	    {
		throw new FinanceException(ex1);
	    }

	    StringBuffer fileBuffer = new StringBuffer();
	    BigDecimal totalAmount = null;
	    DateTime backDate = null;
	    String receiptNumber = null;

	    FinanceMgrLocal financeMgrLocal = PaymentEJBHelper
		    .getFinanceMgrLocal();
	    // new transaction
	    Collection receiptList = financeMgrLocal
		    .getReceptorReceiptsToTransfer();
	    Iterator receiptListIt = receiptList.iterator();

	    int period = 0;
	    ArrayList receipt = null;
	    int counter = 0;
	    int line = 0;
	    int fileLineCount = 0;
	    int linesWritten = 0;
	    StringBuffer invoiceLineBuffer = null;
	    boolean requestDocId = false;
	    // for each receipt
	    while (receiptListIt.hasNext())
	    {
		receipt = (ArrayList) receiptListIt.next();
		receiptNumber = (String) receipt.get(0);
		totalAmount = (BigDecimal) receipt.get(1);
		backDate = (DateTime) receipt.get(2);
		try
		{
		    requestDocId = true;
		    counter++;

		    // write header line if it is the first record only
		    if (counter == 1)
		    {
			documentKey = fofa.getDocumentKey();
			voucher = (String) documentKey
				.get(FinanceOneFinanceAdapter.KEY_VOUCHER);
			timestamp = (String) documentKey
				.get(FinanceOneFinanceAdapter.KEY_TIMESTAMP);
			LogUtil.debug(this.getClass(), "Header DOCID="
				+ voucher);
			requestDocId = false;

			fileName = directory + PREFIX_INVOICE_RECEIPTS
				+ timestamp + "." + FileUtil.EXTENSION_CSV;

			LogUtil.log(this.getClass(), "fileName=" + fileName);

			fileBuffer.append(FIN1_HEADER);
			fileBuffer.append(FileUtil.NEW_LINE);
			fileBuffer.append(FIN1_REDR_HEADING);
			fileBuffer.append(FileUtil.NEW_LINE);
		    }

		    // write other lines

		    line = 0;

		    // create a new line buffer
		    invoiceLineBuffer = new StringBuffer();

		    // financial period
		    period = FinanceHelper.getPeriod(backDate);

		    // new transaction
		    Collection invoiceReceipts = financeMgrLocal
			    .getReceptorInvoices(receiptNumber, backDate, now);
		    LogUtil.log(this.getClass(), "invoiceReceipts.size()="
			    + invoiceReceipts.size());
		    Collections.sort((ArrayList) invoiceReceipts);

		    String narrative3 = "";
		    DebtorInvoiceReceipt dir = null;

		    // create the receipt lines
		    for (Iterator i = invoiceReceipts.iterator(); i.hasNext();)
		    {
			fileLineCount++;
			line++;

			LogUtil.debug(this.getClass(), "requestDocId="
				+ requestDocId + ", DOCID=" + voucher);

			// assume we have already got it from the initial line.
			// this prevents us from discarding sequence numbers
			// unnecessarily
			if (requestDocId)
			{
			    documentKey = fofa.getDocumentKey();
			}
			// get new document id
			voucher = (String) documentKey
				.get(FinanceOneFinanceAdapter.KEY_VOUCHER);

			LogUtil.debug(this.getClass(), "DOCID=" + voucher);

			dir = (DebtorInvoiceReceipt) i.next();

			LogUtil.debug(this.getClass(), "line=" + line);
			LogUtil.log(this.getClass(), "dir=" + dir);

			// DOCID
			invoiceLineBuffer.append(StringUtil.quote(voucher));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// DREF1
			invoiceLineBuffer.append(StringUtil
				.quote(dir.getDebtorInvoiceReceiptPK()
					.getReceiptNumber()));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// DREF2
			invoiceLineBuffer.append(StringUtil
				.quote(dir.getDebtorInvoiceReceiptPK()
					.getInvoiceNumber()));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// DDATE1
			invoiceLineBuffer.append(sd.format(dir.getBackDate()));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// DDATE2
			invoiceLineBuffer.append(sd.format(now));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// DPERIOD
			invoiceLineBuffer.append(period);
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// LINEID
			invoiceLineBuffer.append(fileLineCount);
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// LACCNBR
			invoiceLineBuffer.append(StringUtil.quote(dir
				.getCustomerNumber()));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// LAMOUNT1

			/*
			 * Rounding amount should be handled by Receptor. It is
			 * currently only handled by SSRE
			 */

			invoiceLineBuffer.append(StringUtil.quote(dir
				.getReceiptAmount().negate().toString())); // reverse
			// sign
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// LNARR1
			invoiceLineBuffer.append(StringUtil.quote(dir
				.getNarrative()));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// LNARR2
			invoiceLineBuffer.append(StringUtil.quote(dir
				.getCustomerNumber()));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// LNARR3
			invoiceLineBuffer.append(StringUtil.quote(narrative3));
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// LUSERFLD4
			invoiceLineBuffer.append(StringUtil.quote(timestamp));

			// EOFY CHANGES
			invoiceLineBuffer.append(FileUtil.SEPARATOR_COMMA);
			// YEAR

			String currentYear = YEAR_CURRENT;

			if (dir.getBackDate() != null
				&& !dir.getBackDate().currentFinancialYear())
			{
			    currentYear = YEAR_PREVIOUS;
			}
			invoiceLineBuffer.append(currentYear);
			invoiceLineBuffer.append(FileUtil.NEW_LINE);

			// save debtor invoice receipt record to the database
			hsession.save(dir);

			linesWritten++;

			// request the next document id for the next loop
			requestDocId = true;

		    }

		    // add invoice lines to file buffer
		    fileBuffer.append(invoiceLineBuffer.toString());

		}
		catch (Exception ex)
		{
		    // ignore
		    LogUtil.warn(
			    this.getClass(),
			    "Unable to write receipt line. "
				    + ExceptionHelper
					    .getExceptionStackTrace(ex));
		}

	    } // end of while loop

	    LogUtil.debug(this.getClass(), "linesWritten=" + linesWritten);
	    // write the file
	    if (linesWritten > 0)
	    {
		FileUtil.writeFile(fileName, fileBuffer.toString());
	    }

	}
	catch (Exception se)
	{
	    // rollback active transactions
	    sessionContext.setRollbackOnly();
	    LogUtil.fatal(this.getClass(),
		    ExceptionHelper.getExceptionStackTrace(se));
	    throw new FinanceException(
		    "Unable to transfer debtor invoice receipts.", se);
	}

	LogUtil.log(this.getClass(), "transferDebtorInvoiceReceipts end");
    }

    /** @todo determine if this is the final product code */
    final String PRODUCT_INVOICE = "INVOICE";
    /** @todo determine if this is the final user name */
    final String INVOICE_USER = "fin";
    /** @todo determine if this is the final branch location */
    final String INVOICE_BRANCH = "hob";

    /**
     * 
     * @return boolean in progress already
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean startDebtorFileProcessing(DateTime now)
    {

	DebtorInvoiceControl debtorInvoiceControl = (DebtorInvoiceControl) hsession
		.get(DebtorInvoiceControl.class,
			DebtorInvoiceControl.STATUS_READY);
	LogUtil.log(this.getClass(), "debtorInvoiceControl="
		+ debtorInvoiceControl);
	// if no record then file processing is not in progress
	if (debtorInvoiceControl != null)
	{
	    // delete any complete
	    try
	    {
		hsession.delete(debtorInvoiceControl);
		hsession.flush();
	    }
	    catch (HibernateException ex)
	    {
		LogUtil.warn(this.getClass(),
			"Unable to delete the control record. " + ex);
	    }

	    // create an in progress record
	    debtorInvoiceControl = new DebtorInvoiceControl();
	    debtorInvoiceControl
		    .setStatus(DebtorInvoiceControl.STATUS_IN_PROGRESS);
	    hsession.save(debtorInvoiceControl);
	    hsession.flush();
	    LogUtil.debug(this.getClass(), "debtorInvoiceControl="
		    + debtorInvoiceControl);
	    LogUtil.log(this.getClass(), "Debtor file processing is starting. "
		    + now);
	    return false;
	}
	else
	{
	    LogUtil.warn(this.getClass(),
		    "Debtor file processing is already in progress or no default record created. "
			    + now);
	    return true;
	}
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void endDebtorFileProcessing(DateTime now)
    {

	// find existing record in progress
	DebtorInvoiceControl debtorInvoiceControl = (DebtorInvoiceControl) hsession
		.get(DebtorInvoiceControl.class,
			DebtorInvoiceControl.STATUS_IN_PROGRESS);
	if (debtorInvoiceControl != null)
	{
	    hsession.delete(debtorInvoiceControl);
	    hsession.flush();
	}

	// create new record with status of complete
	DebtorInvoiceControl newDebtorInvoiceControl = new DebtorInvoiceControl();
	newDebtorInvoiceControl.setStatus(DebtorInvoiceControl.STATUS_READY);
	hsession.save(newDebtorInvoiceControl);
	hsession.flush();
	LogUtil.log(this.getClass(), "Debtor file processing is complete. "
		+ now);
    }

    /**
     * Create a debtor invoice that is available for payment in the receipting
     * system.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void createDebtorInvoiceForPayment(DebtorInvoice invoice,
	    DateTime lastUpdate) throws RemoteException, RollBackException
    {
	LogUtil.debug(this.getClass(), "createDebtorInvoiceForPayment start "
		+ invoice);

	final BigDecimal THRESHOLD_AMOUNT = new BigDecimal("0.02");

	PaymentTransactionMgr paymentTransactionMgr = PaymentEJBHelper
		.getPaymentTransactionMgr();
	PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

	try
	{
	    // always set the lastupdate to now.
	    invoice.setLastUpdate(lastUpdate);

	    Payable payable = null;
	    DebtorInvoice matchingInvoice = null;

	    LogUtil.log(this.getClass(), "invoice=" + invoice.toString());
	    LogUtil.log(this.getClass(), "active="
		    + DebtorInvoicePK.STATUS_ACTIVE);

	    // find any matching invoices by invoice number and status
	    List matchingInvoices = hsession
		    .createCriteria(DebtorInvoice.class)
		    .add(Expression.eq("debtorInvoicePK.invoiceNumber", invoice
			    .getDebtorInvoicePK().getInvoiceNumber()))
		    .add(Expression.eq("debtorInvoicePK.status",
			    DebtorInvoicePK.STATUS_ACTIVE)).list();

	    LogUtil.log(this.getClass(), "matchingInvoices=" + matchingInvoices);
	    LogUtil.log(this.getClass(), "size=" + matchingInvoices.size());
	    if (matchingInvoices.size() > 1)
	    {
		throw new RemoteException(
			"Should only be at most only one matching invoice.");
	    }
	    // find matching invoices
	    try
	    {
		matchingInvoice = (DebtorInvoice) matchingInvoices.iterator()
			.next();
	    }
	    catch (NoSuchElementException nse)
	    {
		LogUtil.warn(this.getClass(), "No matching invoices");
	    }

	    LogUtil.debug(this.getClass(), "matchingInvoice=" + matchingInvoice);

	    // if there is a matching invoice
	    if (matchingInvoice != null
		    && matchingInvoice.getDebtorInvoicePK().isActive())
	    {

		LogUtil.debug(this.getClass(), "matchingInvoice="
			+ matchingInvoice);
		LogUtil.debug(this.getClass(), "invoice=" + invoice);

		// set scales to be the same as BigDecimal is only deemed to be
		// equal if value AND scale are the same
		invoice.setAmount(invoice.getAmount().setScale(2,
			BigDecimal.ROUND_HALF_DOWN));
		matchingInvoice.setAmount(matchingInvoice.getAmount().setScale(
			2, BigDecimal.ROUND_HALF_DOWN));

		// set the last update date of the matched invoice
		matchingInvoice.setLastUpdate(lastUpdate);

		// revised amounts - update
		if (!invoice.getAmount().equals(matchingInvoice.getAmount()))
		{
		    LogUtil.debug(this.getClass(), "delete old invoice="
			    + matchingInvoice);

		    // update old invoice status to deleted
		    try
		    {
			deleteDebtorInvoice(paymentTransactionMgr,
				matchingInvoice, lastUpdate, true);
		    }
		    catch (Exception ex1)
		    {
			LogUtil.warn(
				this.getClass(),
				"Unable to delete debtor invoice: "
					+ ex1.getMessage());
		    }

		    // save new invoice
		    hsession.save(invoice);
		}
		else
		{
		    LogUtil.debug(this.getClass(), "update existing="
			    + matchingInvoice);
		    // update existing
		    hsession.update(matchingInvoice);
		}

		hsession.flush();

		// update payable fee if not a receipt.
		if (!invoice.getDebtorInvoicePK().getInvoiceNumber()
			.startsWith(PREFIX_RECEIPT))
		{

		    LogUtil.debug(this.getClass(),
			    "matchingInvoice matching amounts");

		    // check the state of Receptor payables - TRANSACTIONAL?
		    try
		    {
			LogUtil.debug(this.getClass(), "Getting payable for "
				+ invoice.getCustomerNumber()
				+ "/"
				+ invoice.getDebtorInvoicePK()
					.getInvoiceNumber());
			payable = paymentMgr
				.getPayable(
					new Integer(invoice.getCustomerNumber()),
					invoice.getDebtorInvoicePK()
						.getInvoiceNumber(),
					SourceSystem.RECEPTOR.getAbbreviation(),
					SourceSystem.FIN);
			LogUtil.debug(this.getClass(), "Got payable=" + payable);
		    }
		    catch (PaymentException ex2)
		    {
			// ignore
			LogUtil.warn(this.getClass(), "Unable to find payable."
				+ ex2);
		    }

		    LogUtil.debug(this.getClass(),
			    "matchingInvoice matching amounts");

		    if (payable != null)
		    {
			BigDecimal amountOutstanding = payable
				.getAmountOutstanding().setScale(2,
					BigDecimal.ROUND_HALF_DOWN);
			BigDecimal invoiceAmount = payable.getAmount()
				.setScale(2, BigDecimal.ROUND_HALF_DOWN);
			LogUtil.debug(this.getClass(), "payable invoiceAmount="
				+ invoiceAmount);
			LogUtil.debug(this.getClass(),
				"payable amountOutstanding="
					+ amountOutstanding);
			LogUtil.debug(this.getClass(), "Invoice amount="
				+ invoice.getAmount());
			// New invoice does not match Receptor OR failed to
			// delete record
			if (!invoiceAmount.equals(amountOutstanding))
			{
			    LogUtil.debug(this.getClass(), "Part paid");

			    // otherwise update the existing one to the amount
			    // from Finance One
			    /*
			     * try { paymentTransactionMgr.updatePayableAmount(
			     * SourceSystem.FIN, new
			     * Integer(invoice.getCustomerNumber()),
			     * invoice.getDebtorInvoicePK().getInvoiceNumber(),
			     * invoice.getAmount() ); } catch(RollBackException
			     * ex3) { //ignore paymentTransactionMgr.rollback();
			     * LogUtil.warn(this.getClass(),
			     * "Unable to update invoice:" + ex3.getMessage());
			     * }
			     */

			}
			// else already correct
		    }
		    else
		    {
			LogUtil.debug(this.getClass(), "no payable exists");
			if (invoice.getAmount().compareTo(THRESHOLD_AMOUNT) > 0)
			{
			    try
			    {
				// create a new one as there is not one already
				paymentTransactionMgr
					.createPayableFee(
						new Integer(invoice
							.getCustomerNumber()),
						invoice.getDebtorInvoicePK()
							.getInvoiceNumber(),
						PRODUCT_INVOICE, Double
							.valueOf(invoice
								.getAmount()
								.toString()),
						invoice.getDebtorInvoicePK()
							.getInvoiceNumber(),
						SourceSystem.FIN
							.getAbbreviation(),
						INVOICE_BRANCH, INVOICE_USER,
						null);
				LogUtil.warn(this.getClass(),
					"Created new payable.");
			    }
			    catch (RollBackException ex)
			    {
				// rollback
				paymentTransactionMgr.rollback();
				LogUtil.warn(
					this.getClass(),
					"Unable to create invoice: "
						+ ex.getMessage());
			    }
			}
		    }
		}
	    }
	    else
	    {
		LogUtil.debug(this.getClass(), "new invoice");

		// save invoice to allow full reconciliation of trial balance
		// but only create the payable if it is greater than our
		// allowable threshold
		hsession.save(invoice);
		hsession.flush();

		// create a payable fee if the amount is greater than 2 cents
		// and it is not a receipt. Receipts are allocated
		// to invoices in Finance One not Receptor.
		if (!invoice.getDebtorInvoicePK().getInvoiceNumber()
			.startsWith(PREFIX_RECEIPT))
		{
		    if (invoice.getAmount().compareTo(THRESHOLD_AMOUNT) > 0)
		    {
			try
			{

			    LogUtil.debug(
				    this.getClass(),
				    "createPayableFee="
					    + invoice.getCustomerNumber()
					    + " "
					    + invoice.getDebtorInvoicePK()
						    .getInvoiceNumber() + " "
					    + invoice.getAmount());
			    paymentTransactionMgr.createPayableFee(new Integer(
				    invoice.getCustomerNumber()), invoice
				    .getDebtorInvoicePK().getInvoiceNumber(),
				    PRODUCT_INVOICE, Double.valueOf(invoice
					    .getAmount().toString()), invoice
					    .getDebtorInvoicePK()
					    .getInvoiceNumber(),
				    SourceSystem.FIN.getAbbreviation(),
				    INVOICE_BRANCH, INVOICE_USER, null);

			}
			catch (RollBackException ex)
			{

			    LogUtil.warn(
				    this.getClass(),
				    "Unable to create invoice: "
					    + ex.getMessage());

			    // reinstating a payable after, for example, voiding
			    // a credit note that has written a debt off
			    if (ex.getMessage().indexOf(
				    ReceiptingAdapter.ERROR_TRANSACTION_EXISTS) != -1)
			    {
				LogUtil.warn(this.getClass(),
					"Updating existing transaction.");

				// update payable amount
				/*
				 * paymentTransactionMgr.updatePayableAmount(
				 * SourceSystem.FIN, new
				 * Integer(invoice.getCustomerNumber()),
				 * invoice.
				 * getDebtorInvoicePK().getInvoiceNumber(),
				 * invoice.getAmount() );
				 */
			    }
			    else
			    {
				throw new Exception(ex);
			    }
			}
		    }
		    else
		    {
			LogUtil.log(this.getClass(), "Invoice amount "
				+ invoice.getAmount()
				+ " is below threshold of " + THRESHOLD_AMOUNT
				+ ".");
		    }
		}
		else
		{
		    LogUtil.log(this.getClass(),
			    "Invoice starts with the receipt prefix "
				    + invoice.getDebtorInvoicePK()
					    .getInvoiceNumber() + ".");
		}

	    }

	    // commit any pending transactions
	    paymentTransactionMgr.commit();
	    LogUtil.debug(this.getClass(), "Committed payable");

	}
	catch (Exception ex4)
	{
	    LogUtil.warn(this.getClass(),
		    "Unable to create debtor invoice for payment. " + ex4);
	    // rollback paymentTransactionMgr
	    paymentTransactionMgr.rollback();
	    throw new RemoteException(
		    "Unable to create debtor invoice for payment: ", ex4); // rollback
	    // JTA
	    // transactions
	} finally
	{
	    removePaymentTransactionMgr(paymentTransactionMgr);
	}

	LogUtil.debug(this.getClass(), "createDebtorInvoiceForPayment end");

    }

    public void updateDebtor(Debtor debtor) throws FinanceException
    {
	// fin_debtor record represent initial transfer only.

	FinanceAdapter financeAdapter = PaymentFactory.getFinanceAdapter();
	financeAdapter.updateDebtor(debtor);
    }

    public void updateVoucher(FinanceVoucher voucher)
    {
	em.merge(voucher);
    }

    public void deleteVoucher(FinanceVoucher voucher)
    {
	em.remove(voucher);
    }

    public Debtor getDebtor(Integer clientNumber) throws FinanceException
    {

	Debtor debtor = (Debtor) hsession.get(Debtor.class, clientNumber);
	return debtor;
    }

    /**
     * update postings in the database with journal number, MFG Pro system
     * number and posted date.
     */
    private void updatePostings(Vector journals, FinanceResponse financeResponse)
	    throws FinanceException
    {
	LogUtil.debug(this.getClass(), "updatePostings");
	LogUtil.debug(this.getClass(), "journals=" + journals.size());

	DateTime postedDate = DateUtil.clearTime(new DateTime());
	try
	{

	    // set the MFG Pro system number, journal number and posted date on
	    // the
	    // postings.
	    for (int i = 0; i < journals.size(); i++)
	    {
		Journal journal = (Journal) journals.elementAt(i);
		Vector postings = journal.getPostings();

		for (int j = 0; j < postings.size(); j++)
		{
		    PayableItemPostingVO posting = (PayableItemPostingVO) postings
			    .elementAt(j);

		    // update postat date - note: the create date remains the
		    // same.
		    // this is useful for tracking that a postat date has
		    // changed as a result
		    // of a closed period.
		    posting.setPostAtDate(new DateTime(journal.getPostAtDate()));
		    // the batch key
		    posting.setJournalNumber(financeResponse.getJournalNumber());
		    // date sent to the finance system
		    posting.setPostedDate(postedDate);
		    // the consolidation key within a batch
		    posting.setJournalKey(journal.getJournalKey());

		    hsession.merge(posting);
		}
	    }

	}
	catch (Exception se)
	{
	    throw new FinanceException(
		    "Unable to update postings with journal number and posted date. ",
		    se);
	}
    }

    /**
     * Summarise the postings to be transferred to MFG pro.
     * 
     * @param sourceSystem
     *            the source system to summarise postings of for. Only postings
     *            for that source system will be summarised.
     * @param effectiveDate
     *            the post at date to to restrict range.
     * @param overrideDate
     *            override the post at date of ALL ledgers with this date
     * @return a vector of journal entries to be transferred to MFG pro.
     */
    public Vector createJournals(SourceSystem sourceSystem,
	    DateTime effectiveDate, DateTime overrideDate)
	    throws RemoteException, PaymentException
    {
	Connection connection = null;
	StringBuffer statementText = new StringBuffer();
	PreparedStatement statement = null;
	DateTime now = new DateTime();

	Double totalDebits = 0.0;
	Double totalCredits = 0.0;

	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	String journalAudit = commonMgr.getSystemParameterValue(
		SystemParameterVO.CATEGORY_PAYMENT,
		SystemParameterVO.FINANCE_JOURNAL_TRANSFER_AUDIT_LOC);

	String auditFileName = FileUtil.constructFileName(journalAudit
		+ "/JournalTransferAudit", null, FileUtil.EXTENSION_CSV);
	String errorFileName = FileUtil.constructFileName(journalAudit
		+ "/JournalTransferErrors", null, FileUtil.EXTENSION_CSV);

	StringBuffer auditBuffer = new StringBuffer(
		"ClientNo,PayableID,AmountOutstanding" + FileUtil.NEW_LINE);
	StringBuffer errorBuffer = new StringBuffer(
		"ClientNo,PayableID,PostingID,Message" + FileUtil.NEW_LINE);

	String selectClause = "SELECT c.payableItemPostingID,"
		+ " a.salesBranch, a.clientNo, c.accountNumber,"
		+ " c.amount, a.paymentMethodID, a.paymentMethod, a.payment_system, a.payableItemID "
		+ " , c.payableItemComponentID, a.amountOutstanding ";

	String fromClause = " FROM PUB.pt_payableItemPosting c INNER JOIN PUB.pt_payableItemComponent b ON c.payableItemComponentID = b.payableItemComponentID"
		+ " INNER JOIN PUB.pt_payableItem a ON b.payableItemID = a.payableItemID";

	String whereClause = " WHERE c.posted IS NULL "
		+ " AND a.sourceSystem = ? "
		+ " AND c.postAt <= ? "
		+ " AND a.createDate <= ? "
		+ " AND c.description NOT LIKE 'Transfer of%unearned to earned.'";

	String noOutStanding = " AND (( a.amountOutstanding = 0 and a.paymentMethod = 'Receipting' ) or a.paymentMethod like '%Direct%' )";

	String orderByClause = " order by a.clientNo, a.payableItemID, b.payableItemComponentID, c.payableItemPostingID ";

	String componentIDList = null;

	Hashtable clientsWithOutstanding = new Hashtable();

	Hashtable partPosted = new Hashtable();

	int postingID = 0;

	if (effectiveDate == null)
	{
	    effectiveDate = now;
	}

	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();

	auditBuffer.append(listEligiblePostingsWithOustanding(sourceSystem,
		effectiveDate, selectClause, fromClause, whereClause,
		orderByClause, auditFileName)

	);

	partPosted = getPartPostedPostings(sourceSystem, effectiveDate,
		fromClause, whereClause);

	componentIDList = (String) partPosted.get("componentID");
	// auditBuffer = (StringBuffer) partPosted.get("audit");

	try
	{

	    statementText.append(selectClause);

	    statementText.append(fromClause);

	    // all postings that have not previously been transferred
	    statementText.append(whereClause);

	    statementText.append(noOutStanding);

	    if (componentIDList != null)
	    {
		statementText.append(" AND c.payableItemComponentID not in ( "
			+ componentIDList + ")");
	    }

	    // make sure the records come back in a consistent order
	    statementText.append(orderByClause);

	    LogUtil.info(this.getClass(), statementText.toString());

	    connection = dataSource.getConnection();

	    /**
	     * The posted date of items in the database prior to the system live
	     * date 30/09/2002 has been set to 29/09/2002 and has a journal
	     * number and mfg pro system number.
	     */
	    statement = connection.prepareStatement(statementText.toString());

	    statement.setString(1, sourceSystem.getAbbreviation());
	    statement.setDate(2, effectiveDate.toSQLDate());
	    statement.setDate(3, effectiveDate.toSQLDate());

	    LogUtil.info(this.getClass(), statement.toString());

	    ResultSet resultSet = statement.executeQuery();

	    /*
	     * a hashtable containing the summary information for each account
	     * there are potentially two journal entries for each
	     * account-costcentre-subaccount-salesbranch-company combination,
	     * these being the positive and negative earnings
	     */
	    Hashtable journals = new Hashtable();
	    int counter = 0;
	    String salesBranch = null;
	    Integer clientNumber = null;
	    String accNo = null;
	    double amt = 0;
	    BigDecimal amountOutstanding = null;

	    double amtOut = 0;

	    Payable payable = null;

	    String paymentMethodID = null;
	    String paymentMethod = null;
	    String paymentSystem = null;
	    Integer payableItemID = null;
	    Integer payableItemComponentID = null;

	    Object tmpAmtOs = null;

	    DateTime postAt = null;

	    PayableItemPostingVO posting = null;

	    while (resultSet.next())
	    {
		counter++;
		postingID = resultSet.getInt(1);
		salesBranch = resultSet.getString(2);
		clientNumber = new Integer(resultSet.getInt(3));
		accNo = resultSet.getString(4);
		amt = resultSet.getDouble(5);
		paymentMethodID = resultSet.getString(6);
		paymentMethod = resultSet.getString(7);
		paymentSystem = resultSet.getString(8);
		payableItemID = resultSet.getInt(9);
		payableItemComponentID = resultSet.getInt(10);
		amtOut = resultSet.getDouble(11);
		/*
		 * LogUtil.log("RLG", "counter," + new
		 * Integer(counter).toString() + ",PostingID," + new
		 * Integer(postingID).toString() + ",clientNumber," +
		 * clientNumber.toString() + ",salesBranch," + salesBranch +
		 * ",accountNo," + accNo + ",amount," + new
		 * Double(amt).toString() + ",paymentMethodID," +
		 * paymentMethodID + ",paymentMethod," + paymentMethod +
		 * ",paymentSystem," + paymentSystem + ",payableItemID," + new
		 * Integer(payableItemID).toString() +
		 * ",payableItemComponentID," + new
		 * Integer(payableItemComponentID.toString()) + ",amtOut," + new
		 * Double(amtOut).toString()
		 * 
		 * );
		 */
		// get the posting record
		LogUtil.debug(this.getClass(), "postingID=" + postingID);
		posting = pMgr.getPayableItemPosting(new Integer(postingID));
		try
		{

		    // is there an amount payable in the receipting system
		    // if (PaymentMethod.RECEIPTING.equals(paymentMethod)
		    if (PaymentMethod.RECEIPTING.equals("DO NOT PROCESS"))
		    {
			// has this client already been identified as having
			// outstanding

			/*
			 * rlg tmpAmtOs =
			 * clientsWithOutstanding.get(clientNumber);
			 */
			tmpAmtOs = clientsWithOutstanding.get(payableItemID);
			if (tmpAmtOs == null)
			{
			    // get the payable from the receipting system

			    if (paymentSystem.equals(SourceSystem.ECR
				    .getAbbreviation()))
			    {

				payable = PaymentFactory.getReceiptingAdapter(
					SourceSystem.ECR.getAbbreviation())
					.getPayable(
						payableItemID.toString(),
						SourceSystem.ECR
							.getAbbreviation());

			    }
			    else if (paymentSystem.equals(SourceSystem.RECEPTOR
				    .getAbbreviation()))
			    {

				payable = PaymentFactory
					.getReceiptingAdapter(
						SourceSystem.RECEPTOR
							.getAbbreviation())
					.getPayable(
						payableItemID.toString(),
						SourceSystem.RECEPTOR
							.getAbbreviation());

			    }
			    else
			    {

				payable = pMgr.getPayable(clientNumber,
					paymentMethodID, paymentSystem,
					sourceSystem);
			    }

			    if (payable == null)
			    {
				// skip
				continue;
			    }
			    // get the amount outstanding
			    amountOutstanding = payable.getAmountOutstanding();

			    // if amount outstanding then skip this record
			    if (amountOutstanding != null
				    && amountOutstanding.doubleValue() > 0)
			    {
				LogUtil.log(this.getClass(), "Client number '"
					+ clientNumber + "' has "
					+ amountOutstanding + " outstanding.");
				auditBuffer
					.append(clientNumber + ","
						+ payableItemID + ","
						+ amountOutstanding
						+ FileUtil.NEW_LINE);
				// add it to the list of outstanding clients
				/*
				 * rlg clientsWithOutstanding.put(clientNumber,
				 * amountOutstanding);
				 */
				clientsWithOutstanding.put(payableItemID,
					amountOutstanding);
				continue;
			    }
			}
			else
			{
			    LogUtil.debug(this.getClass(), "Client number '"
				    + clientNumber + "' is in memory and has "
				    + tmpAmtOs + " outstanding.");
			    // any value will mean that the record should be
			    // skipped
			    continue;
			}
		    }
		}
		catch (Exception ex)
		{
		    errorBuffer.append(clientNumber + "," + payableItemID + ","
			    + postingID + "," + '"' + ex.getMessage() + '"'
			    + FileUtil.NEW_LINE);
		    LogUtil.fatal(this.getClass(),
			    "Error getting amount outstanding for client '"
				    + clientNumber + "' with payable ID '"
				    + payableItemID + "' with posting ID '"
				    + postingID + "'. " + ex.getMessage());
		    continue;
		}

		// override the post at date if required
		if (overrideDate == null)
		{
		    // ensure the postAt date is valid
		    postAt = FinanceHelper.validateEffectiveDate(
			    posting.getPostAtDate(), null);
		}
		else
		{
		    postAt = overrideDate;
		}

		String subAccountNumber = posting.getSubAccountNumber();
		if (subAccountNumber == null)
		{
		    subAccountNumber = "";
		}
		String costCentre = posting.getCostCentre();
		if (costCentre == null)
		{
		    costCentre = "";
		}

		String key = posting.getAccountNumber();
		key = key.concat(costCentre);
		key = key.concat(subAccountNumber);
		key = key.concat(salesBranch);
		key = key.concat(posting.getCompany().getCompanyCode());
		key = key.concat(new Long(postAt.getTime()).toString());
		if (posting.getAmount() >= 0)
		{
		    key = key.concat(PayableItemPostingVO.DEBIT);
		    totalDebits += posting.getAmount();
		}
		else
		{
		    key = key.concat(PayableItemPostingVO.CREDIT);
		    totalCredits += posting.getAmount();
		}
		key = key.toUpperCase();

		if (counter % 1000 == 0)
		{
		    LogUtil.log(this.getClass(), " ###Create Journal###"
			    + "  ItemCount = " + counter + " Journal Key "
			    + key + " Total Debits " + totalDebits.toString()
			    + " Total Credits " + totalCredits.toString()

		    );

		    /*
		     * rlg + " posting ID = " + postingID + "  client No = " +
		     * clientNumber + "  amount = " + amt + " amountOs = " +
		     * amountOutstanding + " CC = " + costCentre +
		     * " subAccount = " + subAccountNumber +
		     * " posting amount = " + posting.getAmount() + " postAt = "
		     * + postAt + " posting.accNumber = " +
		     * posting.getAccountNumber());
		     */
		}

		Journal journal = (Journal) journals.get(key);
		if (journal == null)
		{
		    // create journal entry and add to hashtable
		    journal = new Journal(
			    posting.getAmount(),
			    posting.getAccountNumber(),
			    costCentre,
			    subAccountNumber,
			    posting.getCompany(),
			    postAt,
			    (posting.getCreateDate() == null ? posting
				    .getPostAtDate() : posting.getCreateDate()),
			    salesBranch, key, "", key);
		    journal.addPosting(posting);
		    journals.put(key, journal);
		}
		else
		{

		    double amount = journal.getAmount() + posting.getAmount();

		    journal.setAmount(amount);
		    journal.addPosting(posting);
		}

	    }

	    // clear the hashtable
	    if (clientsWithOutstanding != null)
	    {
		clientsWithOutstanding.clear();

	    }

	    auditBuffer.append("Total,Debits," + totalDebits.toString()
		    + FileUtil.NEW_LINE);
	    auditBuffer.append("Total,Credits," + totalCredits.toString()
		    + FileUtil.NEW_LINE);

	    FileUtil.writeFile(auditFileName, auditBuffer.toString());
	    FileUtil.writeFile(errorFileName, errorBuffer.toString());

	    return new Vector(journals.values());

	}
	catch (PaymentException pe)
	{
	    throw new PaymentException("Payment error. " + postingID, pe);
	}
	catch (SQLException e)
	{
	    throw new PaymentException("Error creating journals. " + postingID,
		    e);
	}
	catch (Exception ex)
	{
	    LogUtil.log(this.getClass(), "Exception " + ex.getMessage());
	    throw new RemoteException(ex.getMessage());
	} finally
	{
	    LogUtil.log(this.getClass(), "Closing Connection");
	    ConnectionUtil.closeConnection(connection, statement);
	}

    }

    private StringBuffer listEligiblePostingsWithOustanding(
	    SourceSystem sourceSystem, DateTime effectiveDate,
	    String selectClause, String fromClause, String whereClause,
	    String orderByClause, String fileName)
    {
	Connection connection = null;
	StringBuffer statementText = new StringBuffer();
	PreparedStatement statement = null;

	StringBuffer auditBuffer = new StringBuffer();

	int postingID = 0;
	String salesBranch = null;
	Integer clientNumber = null;
	String accNo = null;
	double amt = 0;
	BigDecimal amountOutstanding = null;
	double amtOut = 0;
	String paymentMethodID = null;
	String paymentMethod = null;
	String paymentSystem = null;
	Integer payableItemID = null;
	Integer payableItemComponentID = null;

	Integer prevPayableItemID = null;

	statementText.append(selectClause);
	statementText.append(fromClause);
	statementText.append(whereClause);
	statementText
		.append(" AND ( a.amountOutstanding <> 0 and a.paymentMethod = 'Receipting' ) ");
	statementText.append(orderByClause);

	LogUtil.log("listEligiblePostingsWithOustanding",
		statementText.toString());

	try
	{
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText.toString());

	    statement.setString(1, sourceSystem.getAbbreviation());
	    statement.setDate(2, effectiveDate.toSQLDate());
	    statement.setDate(3, effectiveDate.toSQLDate());

	    ResultSet resultSet = statement.executeQuery();

	    while (resultSet.next())
	    {
		postingID = resultSet.getInt(1);
		salesBranch = resultSet.getString(2);
		clientNumber = new Integer(resultSet.getInt(3));
		accNo = resultSet.getString(4);
		amt = resultSet.getDouble(5);
		paymentMethodID = resultSet.getString(6);
		paymentMethod = resultSet.getString(7);
		paymentSystem = resultSet.getString(8);
		payableItemID = resultSet.getInt(9);
		payableItemComponentID = resultSet.getInt(10);
		amtOut = resultSet.getDouble(11);

		if (!payableItemID.equals(prevPayableItemID))
		{
		    auditBuffer
			    .append(clientNumber.toString() + ","
				    + payableItemID.toString() + ", "
				    + new Double(amtOut).toString()
				    + FileUtil.NEW_LINE);
		    prevPayableItemID = payableItemID;
		}

	    }
	}
	catch (SQLException SQLEx)
	{

	} finally
	{
	    ConnectionUtil.closeConnection(connection, statement);
	}

	if (auditBuffer.length() > 0)
	{
	    try
	    {
		sendOutstandingEmail(fileName);
	    }
	    catch (Exception rex)
	    {
		LogUtil.log(this.getClass(),
			"Failed to send email " + rex.getMessage());
	    }

	}

	return auditBuffer;
    }

    private Hashtable getPartPostedPostings(SourceSystem sourceSystem,
	    DateTime effectiveDate, String fromClause, String whereClause)
    {
	Connection connection = null;
	StringBuffer partPosted = new StringBuffer();
	PreparedStatement statement = null;

	Hashtable partPostings = new Hashtable();

	StringBuffer auditBuffer = new StringBuffer();

	StringBuffer componentIDList = new StringBuffer();

	Double postingTotal = 0.0;
	String clientNo = null;
	String payableID = null;
	String componentID = null;

	String fileName = FileUtil.constructFileName(
		"/export/home/accounts/partPostings", null, "csv");

	partPosted.append("select * ");
	partPosted.append("from  ");
	partPosted
		.append(" ( SELECT SUM(c.amount) as total, a.clientNo, a.payableItemID, b.payableItemComponentID ");
	partPosted.append(fromClause);
	partPosted.append(whereClause);
	// partPosted.append (" AND a.amountOutstanding = 0 ");
	partPosted
		.append(" group by a.clientNo, a.payableItemID, b.payableItemComponentID ");
	partPosted.append(" ) a ");
	partPosted.append(" where total <> 0 ");

	LogUtil.log("getPartPostedPostings", partPosted.toString());

	try
	{
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(partPosted.toString());

	    statement.setString(1, sourceSystem.getAbbreviation());
	    statement.setDate(2, effectiveDate.toSQLDate());
	    statement.setDate(3, effectiveDate.toSQLDate());

	    ResultSet resultSet = statement.executeQuery();

	    while (resultSet.next())
	    {
		postingTotal = resultSet.getDouble(1);
		clientNo = resultSet.getString(2);
		payableID = resultSet.getString(3);
		componentID = resultSet.getString(4);

		// auditBuffer.append("PartPosted posting for client " +
		// clientNo + " payableid " + payableID + " componentID " +
		// componentID + " amount " + postingTotal.toString() +
		// FileUtil.NEW_LINE );

		// LogUtil.log(this.getClass(), "PartPosted posting for client "
		// + clientNo + " payableid " + payableID + " componentID " +
		// componentID + " amount " + postingTotal.toString());

		if (componentIDList.length() == 0)
		{
		    auditBuffer.append("ClientNo,PayableID,ComponentID,Amount"
			    + FileUtil.NEW_LINE);
		    componentIDList.append(componentID);
		}
		else
		{
		    componentIDList.append("," + componentID);
		}

		auditBuffer.append(clientNo + "," + payableID + ", "
			+ componentID + "," + postingTotal.toString()
			+ FileUtil.NEW_LINE);
	    }
	}
	catch (SQLException SQLEx)
	{

	} finally
	{
	    ConnectionUtil.closeConnection(connection, statement);
	}

	if (auditBuffer.length() > 0)
	{
	    try
	    {
		sendPartPostingList(fileName);
		FileUtil.writeFile(fileName, auditBuffer.toString());
	    }
	    catch (Exception rex)
	    {
		LogUtil.log(this.getClass(),
			"Failed to send email " + rex.getMessage());
	    }

	}

	partPostings.put("componentID", componentIDList.toString());
	partPostings.put(" ", " ");

	return partPostings;
    }

    /**
     * Calculate amounts for all postings for a calculation type and date.
     * 
     * @param calculationDate
     *            calculate as at this date.
     * @param calculationType
     *            the type of calculation
     * @return the hashtable of accounts and totals.
     */
    public Hashtable calculateAmounts(DateTime calculationDate,
	    String calculationType, boolean auditFile, String emailAddress,
	    boolean capDaysToEarn, boolean correctDuration)
	    throws PaymentException, RemoteException
    {
	BufferedOutputStream outputStream = null;
	String auditFileName = null;
	if (auditFile)
	{
	    auditFileName = getFileName(calculationType);
	    try
	    {
		outputStream = new BufferedOutputStream(new FileOutputStream(
			auditFileName));
	    }
	    catch (FileNotFoundException e)
	    {
		throw new PaymentException(e);
	    }
	}

	Hashtable calculatedAmounts = new Hashtable();
	Connection connection = null;
	PreparedStatement statement = null;
	Integer accountNumber = null;
	double amount = 0;
	double amountPerDay = 0;
	double earnedAmount = 0;
	int daysToEarn = 0;
	Integer clientNumber = null;
	DateTime earnFromDate = null;
	DateTime earnToDate = null;
	DateTime createDate = null;
	BigDecimal runningTotal = new BigDecimal(0);
	BigDecimal calculatedAmount = null;
	int earningsDuration = 0;
	Integer payableItemPostingID = null;
	Integer membershipid = null;
	Integer groupcount = null;
	Integer payableID =  null;
	
	int counter = 0;
	try
	{
	    connection = dataSource.getConnection();
	    LogUtil.info(this.getClass(), EARNINGS_SQL.toString());
	    statement = connection.prepareStatement(getDefaultEarningsSQL());
	    statement.setDate(1, calculationDate.toSQLDate());

	    // start from an earnable period before the calc date.
	    DateTime beginDate = null;
	    beginDate = calculationDate.subtract(EARNABLE_PERIOD);
	    LogUtil.log(this.getClass(), "calculationDate " + calculationDate.toString() + " start/beginDate " + beginDate.toString());
	    
	    statement.setDate(2, beginDate.toSQLDate());

	    ResultSet rs = statement.executeQuery();
	    while (rs.next())
	    {
		calculatedAmount = new BigDecimal(0);

		payableItemPostingID = new Integer(rs.getInt(3));

		accountNumber = new Integer(rs.getInt(1));
		// positive amount unless a reversal
		amount = -(rs.getDouble(2));

		// effective from date
		earnFromDate = new DateTime(rs.getDate(5));// validateEarnFromDate(rs.getDate(3),
		// rs.getDate(6));
		// effective to date
		earnToDate = new DateTime(rs.getDate(6));

		if (!correctDuration && earnFromDate.equals(earnToDate))
		{
		    try
		    {
			// subtract 1 year
			earnFromDate = earnFromDate.subtract(new Interval(1, 0, 0, 0, 0, 0, 0));
		    }
		    catch (Exception e)
		    {
			LogUtil.warn(this.getClass(),
				"Unable to correct duration for posting "
					+ payableItemPostingID + ": " + e);
			continue;
		    }
		}
		else
		{
		    // earnings amount is full amount.
		}

		clientNumber = new Integer(rs.getInt(7));

		java.sql.Date tmpCreate = rs.getDate(8);
		if (tmpCreate != null)
		{
		    createDate = new DateTime(tmpCreate);
		}
		else
		{
		    createDate = null;
		}

		// days to earn posting over
		daysToEarn = DateUtil.getDateDiff(earnFromDate, earnToDate,Calendar.DATE);// rs.getInt(4);
		// if more than 366 days then cap
		daysToEarn = validateDaysToEarn(daysToEarn, capDaysToEarn);
		// the amount per day
		amountPerDay = calculateAmountPerDay(amount, daysToEarn);

		if (!calculatedAmounts.isEmpty())
		{
		    runningTotal = (BigDecimal) calculatedAmounts.get(accountNumber);
		    if (runningTotal == null)
		    {
			runningTotal = new BigDecimal(0);
		    }
		}
		// if empty then initialise calculated amount
		else
		{
		    // calculatedAmount
		    runningTotal = new BigDecimal(0);
		}

		// if earn from after calculation date
		if (earnFromDate.onOrAfterDay(calculationDate))
		{
		    // if earned calculation then ignore
		    if (calculationType.equals(FinanceMgr.CALCULATION_TYPE_EARNED))
		    {
			// ignore
			continue;
		    }
		    // if unearned calculation then add the amount per day *
		    // days to earn
		    else if (calculationType.equals(FinanceMgr.CALCULATION_TYPE_UNEARNED))
		    {
			counter++;
			calculatedAmount = new BigDecimal(amountPerDay	* daysToEarn);
			runningTotal = runningTotal.add(calculatedAmount);
		    }
		}
		else
		{
		    counter++;
		    // there is an earned amount
		    earningsDuration = DateUtil.getDateDiff(earnFromDate, calculationDate, Calendar.DATE);
		    // earned is equal to number of days between the effective
		    // day and the calc date
		    // if fully earned then use the original days to earn
		    earningsDuration = validateEarningsDuration(daysToEarn, earningsDuration);

		    // calc the earned amount
		    earnedAmount = amountPerDay * earningsDuration;

		    if (calculationType.equals(FinanceMgr.CALCULATION_TYPE_EARNED))
		    {
			// add the earned amounts
			calculatedAmount = new BigDecimal(earnedAmount);
			runningTotal = runningTotal.add(calculatedAmount);
		    }
		    else if (calculationType.equals(FinanceMgr.CALCULATION_TYPE_UNEARNED))
		    {
			// add the original amount minus the earned amount
			calculatedAmount = new BigDecimal(amount - earnedAmount);
			runningTotal = runningTotal.add(calculatedAmount);
		    }
		}
		writeAuditLine(outputStream, clientNumber,
			payableItemPostingID, calculationDate, null, amount,
			new BigDecimal(earnedAmount), earnFromDate,
			amountPerDay, 0, 0,0,earningsDuration, daysToEarn,
			earnToDate, counter, accountNumber, calculatedAmount,
			createDate, 0,0,0, "calculateAmounts " + calculationType);
		// store in hashtable by key
		calculatedAmounts.put(accountNumber, runningTotal);

	    }

	}
	catch (SQLException sqe)
	{
	    throw new PaymentException("SQL error", sqe);
	} finally
	{
	    if (outputStream != null)
	    {
		try
		{
		    outputStream.close();

		}
		catch (IOException e)
		{
		    LogUtil.fatal(this.getClass(), e);
		}
	    }
	    ConnectionUtil.closeConnection(connection, statement);
	}

	LogUtil.log(this.getClass(), "Processed " + counter + " records.");
	LogUtil.log(this.getClass(), "Calculated amounts " + calculatedAmounts);

	if (auditFile)
	{
	    sendAuditFiles(null, null, calculationDate, calculationType,
		    calculatedAmounts, emailAddress, auditFileName);
	}

	return calculatedAmounts;
    }

    private String getFileName(String calculationType)
    {
	DateTime now = new DateTime();
	String DIR_NAME = null;
	try
	{
	    DIR_NAME = (String) FileUtil.getProperty("master", FIN_AUDIT_DIR);
	}
	catch (Exception e)
	{
	    LogUtil.fatal(this.getClass(), e);
	}
	String fileName = DIR_NAME + Long.toString(now.getTime())
		+ StringUtil.replaceAll(calculationType, " ", "").toLowerCase()
		+ "audit.csv";
	LogUtil.debug(this.getClass(), "fileName=" + fileName);
	return fileName;
    }

    /**
     * Validate the period of earnings to coincide with the days to earn.
     * 
     * @param daysToEarn
     * @param earningsDuration
     * @return
     */
    private int validateEarningsDuration(int daysToEarn, int earningsDuration)
    {
	if (earningsDuration > daysToEarn)
	{
	    earningsDuration = daysToEarn;
	}
	return earningsDuration;
    }

    /**
     * Calculate the amount per day given the amount and days to earn.
     * 
     * @param amount
     * @param daysToEarn
     * @return
     */
    private double calculateAmountPerDay(double amount, int daysToEarn)
    {
	double amountPerDay = 0;
	if (daysToEarn <= 0)
	{
	    amountPerDay = 0; // amount / MAX_EARNINGS_PERIOD
	}
	else
	{
	    amountPerDay = (amount / daysToEarn);
	}
	return amountPerDay;
    }

    /**
     * Calculate earned amounts for all postings between two dates for a
     * specific account number, year and month.
     * 
     * @param accountNumber
     *            the account number.
     * @param startDate
     *            the start of the period.
     * @param endDate
     *            the end of the period. The effective range is restricted for
     *            performance.
     * @return the hashtable of accounts and totals.
     */
    public Hashtable calculateEarnedAmounts(DateTime startDate,
	    DateTime endDate, String mode, boolean auditFile,
	    String emailAddress, boolean capDaysToEarn, boolean correctDuration)
	    throws PaymentException, RemoteException
    {

	BufferedOutputStream outputStream = null;
	String auditFileName = null;
	if (auditFile)
	{
	    auditFileName = getFileName("Earned");
	    try
	    {
		outputStream = new BufferedOutputStream(new FileOutputStream(
			auditFileName));
	    }
	    catch (FileNotFoundException e)
	    {
		throw new PaymentException(e);
	    }
	}

	// construct a one month interval to represent earnings from the
	// previous period that have not been
	// accounted for.
	Interval arrearsPeriod = null;
	try
	{
	    arrearsPeriod = new Interval(0, 1, 0, 0, 0, 0, 0);
	}
	catch (Exception ex)
	{
	    throw new com.ract.common.SystemException(
		    "Unable to construct a one month interval to represent earnings in arrears.",
		    ex);
	}

	Integer payableItemPostingID = null;

	// key
	Integer accountNumber = null;
	// value
	BigDecimal runningTotal = new BigDecimal(0);

	BigDecimal earnedAmount = null;

	// key pattern MMyyyyACCOUNTNUMBER
	// eg. 0220022328
	String earningsKey = "";

	Hashtable earnedAmounts = new Hashtable();

	Connection connection = null;
	PreparedStatement statement = null;

	DateTime earnFromDate = null;
	DateTime createDate = null;
	double amountPerDay = 0;
	double amount = 0;
	// double inArrearsAmount = 0;

	Integer clientNumber = null;

	int endDiff = 0;
	int startDiff = 0;
	int earningsDuration = 0;
	int daysToEarn = 0;
	int arrearsDuration = 0;
	
	Integer membershipid = null;
	Integer groupcount = null;
	Integer payableID =  null;

	DateTime earnToDate = null;

	DateTime earningsYYMM = null; 
	String YYMMearnings;
	
	int counter = 0;
	int skipped = 0;
	int returned = 0;

	try
	{
	    connection = dataSource.getConnection();

	    // get first element
	    String earningsSQL = EARNINGS_SQL[0];

	    // complete SQL based on mode
	    if (FinanceMgr.SELECTION_MODE_POSTAT_DATE.equals(mode))
	    {
		earningsSQL += EARNINGS_SQL[1];
	    }
	    else if (FinanceMgr.SELECTION_MODE_START_DATE.equals(mode))
	    {
		earningsSQL += EARNINGS_SQL[2];
	    }

	    LogUtil.info(this.getClass(), earningsSQL);

	    statement = connection.prepareStatement(earningsSQL);
	    statement.setDate(1, endDate.toSQLDate());

	    DateTime arrearsStart = null;

	    // start from an earnable period before the start date.
	    DateTime beginDate = null;
	    beginDate = startDate.subtract(EARNABLE_PERIOD);
	    LogUtil.info (this.getClass(), "enddate " + endDate.toString() + " begin/start Date " + beginDate.toString());
	    statement.setDate(2, beginDate.toSQLDate());

	    ResultSet rs = statement.executeQuery();

	    while (rs.next())
	    {
		returned += 1;
		earnedAmount = new BigDecimal(0);

		// initialise each record
		arrearsDuration = 0;

		// posting ID
		payableItemPostingID = new Integer(rs.getInt(3));

		// get account number
		accountNumber = new Integer(rs.getInt(1));

		// get the effective from date
		earnFromDate = new DateTime(rs.getDate(5));  // payable item start date
		
		// validateEarnFromDate(rs.getDate(3), rs.getDate(6));

		if (!correctDuration && earnFromDate.equals(earnToDate))
		{
		    try
		    {
			// subtract 1 year
			earnFromDate = earnFromDate.subtract(new Interval(1, 0, 0, 0, 0, 0, 0));
		    }
		    catch (Exception e)
		    {
			LogUtil.warn(this.getClass(),
				"Unable to correct duration for posting "
					+ payableItemPostingID + ": " + e);
			continue;
		    }
		}
		else
		{
		    // earnings amount is full amount.
		}
		
		payableID 	= new Integer(rs.getInt(9));
		membershipid 	= new Integer(0); //rs.getInt(10));
		groupcount	= new Integer(0); //rs.getInt(11));

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(earnFromDate);

		// create the key
		earningsKey = generateEarningsKey(accountNumber, gc);

		// if not empty then fetch by key
		if (!earnedAmounts.isEmpty())
		{
		    runningTotal = (BigDecimal) earnedAmounts.get(earningsKey);
		    if (runningTotal == null)
		    {
			runningTotal = new BigDecimal(0);
		    }
		}
		// if empty then initialise earned amount
		else
		{
		    // earnedAmount
		    runningTotal = new BigDecimal(0);
		}

		// earned to date is the date that the posting has been earned
		// to.
		// get the effective to date
		earnToDate = new DateTime(rs.getDate(6));  //payable item end date

		clientNumber = new Integer(rs.getInt(7));

		java.sql.Date tmpCreate = rs.getDate(8); // posting create date
		if (tmpCreate != null)
		{
		    createDate = new DateTime(tmpCreate);
		}
		else
		{
		    createDate = null;
		}

		// get the amount (change the sign)
		amount = -(rs.getDouble(2));

		// get the difference in days between the start and end date
		// days to earn are days beyond the earned to date yet to earn.
		daysToEarn = DateUtil.getDateDiff(earnFromDate, earnToDate,Calendar.DATE); // rs.getInt(4);

		daysToEarn = validateDaysToEarn(daysToEarn, capDaysToEarn);

		// calculate amount per day - avoid a divide by zero error.
		amountPerDay = calculateAmountPerDay(amount, daysToEarn);

		// eg. 02 2002 account - amount

		// post at = 10/11/2002
		// earnings finish = 10/11/2003
		// start date = 1/11/2002
		// end date = 30/11/2002

		// end date should be after the start date
		if (endDate.after(startDate))
		{
		    // post at date should be on or after the start date
		    if (earnFromDate.onOrAfterDay(startDate))
		    {
			// post at date should be before the end date
			if (earnFromDate.onOrAfterDay(endDate))
			{
			    // ignore if after the end date and break out
			    skipped += 1;
//			    writeAuditLine(outputStream, clientNumber,
//					payableItemPostingID, startDate, endDate, amount,
//					runningTotal, earnFromDate, amountPerDay, endDiff,
//					startDiff, 0, earningsDuration, daysToEarn, earnToDate,
//					skipped, accountNumber, earnedAmount, createDate, "earned " + mode + " skipped");
			    
			    continue;
			}
			else
			{
			    // in example this will be 20
			    endDiff = DateUtil.getDateDiff(earnFromDate,endDate, Calendar.DATE);
			}
		    }
		    else
		    {
			// full period - 30 days
			endDiff = DateUtil.getDateDiff(startDate, endDate,Calendar.DATE);

			// capture the earnings in arrears for last month
			arrearsStart = startDate.subtract(arrearsPeriod);
			if (earnFromDate.afterDay(arrearsStart))
			{

			    // difference between the earn from date and the
			    // start date of the period of interest
			    arrearsDuration = DateUtil.getDateDiff( earnFromDate, startDate, Calendar.DATE);
			    LogUtil.debug(this.getClass(), "In arrears "
				    + payableItemPostingID + " = "
				    + arrearsDuration);
			}

		    }
		}
		else
		{
		    throw new PaymentException(
			    "Start date should be before end date - "
				    + payableItemPostingID + ".");
		}

		// if dte == 0 then what?
		if (earnToDate.onOrBeforeDay(endDate))
		{
		    startDiff = DateUtil.getDateDiff(earnToDate, endDate, Calendar.DATE);
		}
		else
		{
		    // 0 days to subtract as earnings go beyond the end date
		    startDiff = 0;
		}

		// earnings duration is equal to the difference between the
		// start and end date plus any in arrears
		earningsDuration = (endDiff - startDiff) + arrearsDuration;

		if (earningsDuration < 0)
		{
		    skipped += 1;
//		    writeAuditLine(outputStream, clientNumber,
//				payableItemPostingID, startDate, endDate, amount,
//				runningTotal, earnFromDate, amountPerDay, endDiff,
//				startDiff, arrearsDuration, earningsDuration, daysToEarn, earnToDate,
//				skipped, accountNumber, earnedAmount, createDate, "earned " + mode + " skipped");
		    
		    continue;
		}
		else
		{
		    counter++;
		    earnedAmount = new BigDecimal(earningsDuration  * amountPerDay);
		    runningTotal = runningTotal.add(earnedAmount);
		}

		earningsYYMM = startDate.add(new Interval (0,0,earningsDuration,0,0,0,0));
		gc.setTime(earningsYYMM);
		YYMMearnings = generateEarningsKey(accountNumber, gc);
		
		earnedAmounts.put(earningsKey, runningTotal);

		writeAuditLine(outputStream, clientNumber,
			payableItemPostingID, startDate, endDate, amount,
			runningTotal, earnFromDate, amountPerDay, endDiff,
			startDiff, arrearsDuration, earningsDuration, daysToEarn, earnToDate,
			counter, accountNumber, earnedAmount, createDate, 
			payableID, membershipid, groupcount, earningsKey + " " + YYMMearnings + " earned " + mode);

	    }

	}
	catch (SQLException sqe)
	{
	    throw new PaymentException("SQL error", sqe);
	}
	catch (Exception e)
	{
	    throw new PaymentException(payableItemPostingID + " "
		    + earningsDuration + " " + amountPerDay, e);
	} finally
	{
	    if (outputStream != null)
	    {
		try
		{
		    outputStream.close();
		}
		catch (IOException e)
		{
		    LogUtil.fatal(this.getClass(), e);
		}
	    }
	    ConnectionUtil.closeConnection(connection, statement);
	}

	LogUtil.log(this.getClass(), "Processed " + returned + " records.");
	LogUtil.log(this.getClass(), "Accepted  " + counter + " records.");
	LogUtil.log(this.getClass(), "Rejected  " + skipped + " records.");
	LogUtil.log(this.getClass(), "Earned amounts " + earnedAmounts);

	if (auditFile)
	{
	    sendAuditFiles(startDate, endDate, null,
		    FinanceMgr.CALCULATION_TYPE_EARNED_FOR_DATE_RANGE,
		    earnedAmounts, emailAddress, auditFileName);
	}

	return earnedAmounts;

    }

    /**
     * Log key fields for diagnosing earnings calculation problems.
     * 
     * @param startDate
     * @param endDate
     * @param earnedAmount
     * @param earnFromDate
     * @param amountPerDay
     * @param endDiff
     * @param startDiff
     * @param earningsDuration
     * @param daysToEarn
     * @param earningsFinish
     * @param counter
     */
    private void writeAuditLine(OutputStream outputStream,
	    Integer clientNumber, Integer postingId, DateTime startDate,
	    DateTime endDate, double amount, BigDecimal earnedAmount,
	    DateTime earnFromDate, double amountPerDay, int endDiff,
	    int startDiff, int arrearsDuration, int earningsDuration, int daysToEarn,
	    DateTime earnToDate, int counter, Integer accountNumber,
	    BigDecimal total, DateTime createDate, 
	    Integer payableid, Integer membershipid, Integer groupcount, String mode)
    {
	StringBuffer auditLine = new StringBuffer();
	if (outputStream != null)
	{
	    // write heading
	    if (counter == 1)
	    {
		auditLine.append("postingId,clientNumber,startDiff,endDiff,arrearsDuration,startDate,endDate,");
		auditLine.append("amount,counter,earningsDuration,amountPerDay,daysToEarn,earnFromDate,earnToDate,");
		auditLine.append("accountNumber,createDate,total,payableid,membershipid,groupcount,mode");
		auditLine.append(FileUtil.NEW_LINE);
	    }

	    auditLine.append(postingId.toString());
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(clientNumber.toString());
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(startDiff);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(endDiff);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(arrearsDuration);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(startDate != null ? startDate.formatShortDate()
		    : "");
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(endDate != null ? endDate.formatShortDate() : "");
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(String.valueOf(amount));
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(counter);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(earningsDuration);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(String.valueOf(amountPerDay));
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(daysToEarn);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(earnFromDate != null ? earnFromDate
		    .formatShortDate() : "");
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(earnToDate != null ? earnToDate.formatShortDate()
		    : "");
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(accountNumber.toString());
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(createDate != null ? createDate.formatShortDate()
		    : "");
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(total);
	    auditLine.append(FileUtil.SEPARATOR_COMMA); 
	    auditLine.append(payableid);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(membershipid);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(groupcount);
	    auditLine.append(FileUtil.SEPARATOR_COMMA);
	    auditLine.append(mode);
	    auditLine.append(FileUtil.NEW_LINE);

	    try
	    {
		outputStream.write(auditLine.toString().getBytes());
	    }
	    catch (IOException e)
	    {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    /**
     * Temporary logging function to write file information
     * 
     * @param startDate
     *            DateTime
     * @param endDate
     *            DateTime
     * @param effectiveDate
     *            DateTime
     * @param calculationType
     *            String
     * @param calculatedAmounts
     *            Hashtable
     */
    private void sendAuditFiles(DateTime startDate, DateTime endDate,
	    DateTime effectiveDate, String calculationType,
	    Hashtable calculatedAmounts, String emailAddress,
	    String auditFileName) throws RemoteException
    {
	String zipFileName = null;
	if (auditFileName != null)
	{
	    zipFileName = auditFileName
		    .substring(0, auditFileName.length() - 4)
		    + "."
		    + FileUtil.EXTENSION_ZIP;
	    FileUtil.zipFile(zipFileName, auditFileName);
	}

	StringBuffer subject = new StringBuffer();
	DateTime now = new DateTime();
	StringBuffer description = new StringBuffer();
	if (calculationType
		.equals(FinanceMgr.CALCULATION_TYPE_EARNED_FOR_DATE_RANGE))
	{
	    subject.append(calculationType);
	    subject.append(" for period ");
	    subject.append(startDate.formatShortDate());
	    subject.append(" to ");
	    subject.append(endDate.formatShortDate());
	}
	else
	{
	    subject.append(calculationType);
	    subject.append(" as at ");
	    subject.append(effectiveDate.formatShortDate());
	}

	description.append(subject.toString());

	description.append(FileUtil.NEW_LINE);

	description.append("Year,Month,Account Number,Amount"
		+ FileUtil.NEW_LINE);

	BigDecimal totalAmount = new BigDecimal(0);
	TreeMap sortedAmounts = null;

	// create a new treemap with the natural sort order of
	if (calculatedAmounts != null)
	    sortedAmounts = new TreeMap(calculatedAmounts);

	TreeMap totalMap = new TreeMap();

	BigDecimal amount = null;
	Object keys[] = (sortedAmounts.keySet()).toArray();

	String year = "";
	String month = "";
	Integer accountNumber = null;

	BigDecimal accountAmount = new BigDecimal(0);
	Object key = null;
	String stringKey = "";

	// get the detail
	for (int i = 0; i < keys.length; i++)
	{

	    key = keys[i];

	    LogUtil.log(this.getClass(), "key = " + key);

	    if (key instanceof Integer)
	    {
		accountNumber = new Integer(key.toString());
		amount = (BigDecimal) calculatedAmounts.get(key);
	    }
	    else if (key instanceof String)
	    {
		stringKey = key.toString();

		year = stringKey.substring(0, 4);
		month = stringKey.substring(4, 6);
		accountNumber = new Integer(stringKey.substring(6, 10));

		amount = (BigDecimal) calculatedAmounts.get(stringKey);

		if (totalMap.get(accountNumber) == null)
		{
		    totalMap.put(accountNumber, amount);
		}
		else
		{
		    accountAmount = (BigDecimal) totalMap.get(accountNumber);
		    accountAmount = accountAmount.add(amount);
		    totalMap.put(accountNumber, accountAmount);
		}
	    }

	    if (amount != null)
	    {
		totalAmount = totalAmount.add(amount);
	    }
	    description
		    .append(year + "," + month + "," + accountNumber + ",\""
			    + NumberUtil.formatValue(amount) + "\""
			    + FileUtil.NEW_LINE);
	}
	// get the sub totals
	Object totalKeys[] = (totalMap.keySet()).toArray();
	Object totalKey = null;
	BigDecimal totalAccountAmount = null;
	int keyLength = totalKeys.length;
	if (keyLength > 0)
	{
	    // empty
	}
	for (int x = 0; x < keyLength; x++)
	{
	    totalKey = totalKeys[x];
	    totalAccountAmount = (BigDecimal) totalMap.get(totalKey);
	    description.append(",," + totalKey + ",\""
		    + NumberUtil.formatValue(totalAccountAmount) + "\""
		    + FileUtil.NEW_LINE);
	}
	// the grand total
	description.append(",," + "Total,\""
		+ NumberUtil.formatValue(totalAmount) + "\""
		+ FileUtil.NEW_LINE);

	String body = "Run at " + now.formatLongDate() + FileUtil.NEW_LINE;

	description.append(body);

	// change to parameter
	String DIR_NAME = null;
	try
	{
	    DIR_NAME = (String) FileUtil.getProperty("master", FIN_AUDIT_DIR);
	}
	catch (Exception e)
	{
	    LogUtil.fatal(this.getClass(), e);
	}
	String fileName = null;
	fileName = DIR_NAME + Long.toString(now.getTime())
		+ StringUtil.replaceAll(calculationType, " ", "").toLowerCase()
		+ "." + FileUtil.EXTENSION_CSV;
	// write string to file
	try
	{
	    FileUtil.writeFile(fileName, description.toString());
	}
	catch (IOException ex)
	{
	    LogUtil.fatal(this.getClass(), ex);
	}

	Hashtable files = new Hashtable();
	files.put(fileName, fileName);
	if (zipFileName != null)
	{
	    files.put(zipFileName, zipFileName);
	}

	MailMessage message = new MailMessage();
	message.setRecipient(emailAddress);
	message.setSubject(subject.toString());
	message.setMessage(body);
	message.setFiles(files);

	CommonEJBHelper.getMailMgr().sendMail(message);

	if (zipFileName != null)
	{
	    FileUtil.deleteFile(zipFileName);
	}
	FileUtil.deleteFile(fileName);
    }

    /**
     * Validate the period of earnings.
     * 
     * @param daysToEarn
     *            int
     * @param startDate
     *            DateTime
     * @param endDate
     *            DateTime
     * @return int
     */
    private int validateDaysToEarn(int daysToEarn, boolean capDaysToEarn)
    {
	if (daysToEarn > MAX_EARNINGS_PERIOD && capDaysToEarn)
	{
	    daysToEarn = MAX_EARNINGS_PERIOD;
	}
	return daysToEarn;
    }

    /**
     * Generate and earnings key to store in a hashtable
     * 
     * @param accountNumber
     * @param gc
     * @return
     */
    private String generateEarningsKey(Integer accountNumber,
	    GregorianCalendar gc)
    {
	String earningsKey;
	earningsKey 	= 	FORMATTER_YEAR.format(gc.get(Calendar.YEAR))
			+ 	FORMATTER_MONTH.format(gc.get(Calendar.MONTH) + 1)
			+ 	FORMATTER_ACCOUNT.format(accountNumber.longValue());
	return earningsKey;
    }

    public void transferJournals(SourceSystem sourceSystem,
	    boolean doIncomePostings, DateTime effectiveDate,
	    DateTime overrideDate) throws RemoteException, PaymentException
    {
	if (doIncomePostings)
	{
	    throw new PaymentException(
		    "Income postings are derived from a report. The creation of income postings is not available.");
	}
	Vector journalList = createJournals(sourceSystem, effectiveDate,
		overrideDate);
	transferJournals(journalList, sourceSystem);
    }

    /**
     * See remote interface for description of the method
     * 
     * @param sourceSystem
     *            Description of the Parameter
     * @param incomePostings
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     * @exception PaymentException
     *                Description of the Exception
     */
    public void transferJournals(Vector journalList, SourceSystem sourceSystem)
	    throws RemoteException, PaymentException
    {

	if (sourceSystem == null)
	{
	    throw new PaymentException(
		    "Could not transfer journals. SourceSytem is null*");
	}
	String errorMessage = null;
	boolean successful = false;

	FinanceAdapter financeAdapter = PaymentFactory
		.getFinanceAdapter(sourceSystem);
	;

	FinanceResponse financeResponse = null;
	try
	{
	    // 4 hour timeout set in jboss.xml
	    financeResponse = financeAdapter.transferJournals(journalList,
		    sourceSystem, true);

	    // update posting date, journal number and mfg pro screen number
	    updatePostings(journalList, financeResponse);

	    // commit - stateless
	    financeAdapter.commit();
	    successful = true;

	}
	catch (Exception e1)
	{
	    errorMessage = ExceptionHelper.getExceptionStackTrace(e1);

	    LogUtil.warn(this.getClass(), "Error transferring journals="
		    + errorMessage);
	    // rollback finance adapter
	    financeAdapter.rollback();

	    // set transaction to rollback
	    this.sessionContext.setRollbackOnly();

	    // make sure it is marked as failed
	    successful = false;
	}

	// only send on failure
	if (!successful)
	{
	    sendFinanceTransferStatus(journalList, errorMessage,
		    financeResponse, sourceSystem);
	}
    }

    /**
     * Send an email status message to a nominated finance staff member.
     * 
     * @param journalList
     *            Vector
     * @param sw
     *            StringWriter
     * @param financeResponse
     *            FinanceResponse
     * @throws RemoteException
     */
    private void sendFinanceTransferStatus(Vector journalList,
	    String errorMessage, FinanceResponse financeResponse,
	    SourceSystem sourceSystem) throws RemoteException
    {
	LogUtil.debug(this.getClass(), "sendFinanceTransferStatus");
	DateTime transferDate = new DateTime().getDateOnly();
	// email status
	MailMgr mailMgr = CommonEJBHelper.getMailMgr();
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	// get email address
	String financeEmail = commonMgr.getSystemParameterValue(
		SystemParameterVO.CATEGORY_PAYMENT,
		SystemParameterVO.EMAIL_FINANCIAL_TRANSFER);
	StringBuffer subject = new StringBuffer();
	StringBuffer messageBody = new StringBuffer();

	subject.append("LEDGER TRANSFER FAILURE");
	messageBody
		.append("Ledgers have failed to transfer to the finance system."
			+ FileUtil.NEW_LINE);
	messageBody.append("Source system: " + sourceSystem.getSystemName());
	messageBody.append("Error: " + errorMessage);

	// add effective date to subject
	subject.append((transferDate == null ? "" : " "
		+ transferDate.formatShortDate()));

	MailMessage message = new MailMessage();
	message.setRecipient(financeEmail);
	message.setSubject(subject.toString());
	message.setMessage(messageBody.toString());

	mailMgr.sendMail(message);
    }

    private void sendPartPostingList(String fileName) throws RemoteException
    {
	LogUtil.debug(this.getClass(), "sendFinanceTransferStatus");
	DateTime transferDate = new DateTime().getDateOnly();
	// email status
	MailMgr mailMgr = CommonEJBHelper.getMailMgr();
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	// get email address
	String financeEmail = commonMgr.getSystemParameterValue(
		SystemParameterVO.CATEGORY_PAYMENT,
		SystemParameterVO.EMAIL_FINANCIAL_TRANSFER);
	StringBuffer subject = new StringBuffer();
	StringBuffer messageBody = new StringBuffer();

	subject.append("Part Posting List ");
	messageBody
		.append("A list of postings that have been part posted has been created."
			+ FileUtil.NEW_LINE);
	messageBody.append("Please check " + fileName + FileUtil.NEW_LINE);

	// add effective date to subject
	subject.append((transferDate == null ? "" : " "
		+ transferDate.formatShortDate()));

	MailMessage message = new MailMessage();
	message.setRecipient(financeEmail);
	message.setSubject(subject.toString());
	message.setMessage(messageBody.toString());

	mailMgr.sendMail(message);
    }

    private void sendOutstandingEmail(String fileName) throws RemoteException
    {
	LogUtil.debug(this.getClass(), "sendFinanceTransferStatus");
	DateTime transferDate = new DateTime().getDateOnly();
	// email status
	MailMgr mailMgr = CommonEJBHelper.getMailMgr();
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	// get email address
	String financeEmail = commonMgr.getSystemParameterValue(
		SystemParameterVO.CATEGORY_PAYMENT,
		SystemParameterVO.EMAIL_FINANCIAL_TRANSFER);
	StringBuffer subject = new StringBuffer();
	StringBuffer messageBody = new StringBuffer();

	subject.append("Transfer Journal - Oustanding Amounts ");
	messageBody
		.append("A list of eligible postings that have an outstanding amount has been created ."
			+ FileUtil.NEW_LINE);
	messageBody.append("Please check " + fileName + FileUtil.NEW_LINE);

	// add effective date to subject
	subject.append((transferDate == null ? "" : " "
		+ transferDate.formatShortDate()));

	MailMessage message = new MailMessage();
	message.setRecipient(financeEmail);
	message.setSubject(subject.toString());
	message.setMessage(messageBody.toString());

	mailMgr.sendMail(message);
    }

    public void createVoucher(FinanceVoucher voucher)
    {
	// assign disposal id first
	em.persist(voucher);
    }

    /**
     * Retrieve all vouchers relating to a particular source system and
     * reference
     * 
     * @param subSystem
     * @param sourceSystemReferenceId
     * @return Collection of VoucherVO objects
     * @throws RemoteException
     */
    public Collection getVouchersForReference(String sourceSystem,
	    String sourceSystemReference) throws RemoteException
    {
	javax.persistence.Query query = em
		.createQuery("Select e from FinanceVoucher e where sourceSystem = ?1 and sourceSystemReference = ?2");
	query.setParameter(1, sourceSystem);
	query.setParameter(2, sourceSystemReference);
	return query.getResultList();
    }

    public Collection getVouchersForApprovingUser(String approvingUserId)
	    throws RemoteException
    {
	javax.persistence.Query query = em
		.createQuery("Select e from FinanceVoucher e where approvingUserId = ?1");
	query.setParameter(1, approvingUserId);
	return query.getResultList();
    }

    public Collection getVouchersByProcessDate(DateTime startDate,
	    DateTime endDate, String status) throws RemoteException
    {
	javax.persistence.Query query = em
		.createQuery("Select e from FinanceVoucher e where processDate between ?1 and ?2 and status = ?3");
	query.setParameter(1, startDate);
	query.setParameter(2, endDate);
	query.setParameter(3, status);
	return query.getResultList();
    }

    public Cheque getCheque(String voucherId) throws SystemException
    {
	FinanceAdapter financeAdapter = PaymentFactory
		.getFinanceAdapter(FinanceAdapter.FINANCE_SYSTEM_MFGPRO);
	return financeAdapter.getCheque(voucherId);
    }

    public FinancePayee getPayee(String payeeNo) throws SystemException
    {
	FinanceAdapter financeAdapter = PaymentFactory
		.getFinanceAdapter(FinanceAdapter.FINANCE_SYSTEM_MFGPRO);
	return financeAdapter.getPayee(payeeNo);
    }

    /**
     * Retrieves all voucher records relating to a particular payee Any records
     * found are returned in an ArrayList of VoucherVO objects If none are found
     * an empty list is returned.
     * 
     * @param payeeNo
     * @return Collection
     * @throws RemoteException
     */
    public Collection getVouchersForPayee(String payeeNo)
	    throws RemoteException
    {
	javax.persistence.Query query = em
		.createQuery("Select e from FinanceVoucher e where payeeNo = ?1");
	query.setParameter(1, payeeNo);
	return query.getResultList();
    }

    public Collection getVouchersForClient(String clientNumber)
	    throws RemoteException
    {
	javax.persistence.Query query = em
		.createQuery("Select e from FinanceVoucher e where clientNumber = ?1");
	query.setParameter(1, clientNumber);
	return query.getResultList();
    }

    public Collection getVouchersForVoucher(String voucherId)
	    throws RemoteException
    {
	javax.persistence.Query query = em
		.createQuery("Select e from FinanceVoucher e where voucherId = ?1");
	query.setParameter(1, voucherId);
	return query.getResultList();
    }

    public FinanceVoucher getVoucher(Integer disposalId) throws RemoteException
    {
	return em.find(FinanceVoucher.class, disposalId);
    }

    public Collection getSubsystemList() throws FinanceException,
	    RemoteException
    {
	Collection subsystemList = null;

	FinanceAdapter financeAdapter = PaymentFactory
		.getFinanceAdapter(FinanceAdapter.FINANCE_SYSTEM_MFGPRO);

	try
	{
	    subsystemList = financeAdapter.getSubsystemList();
	}
	catch (ValidationException ex)
	{
	    throw new FinanceException(ex);
	}

	return subsystemList;
    }

    public String translateAccounts(String entity, String accountCode,
	    String subAccountCode, String costCentre) throws FinanceException
    {
	String financeOneAccount = "";
	ProgressFinanceAdapter financeAdapter = null;

	// mfg pro only at this stage
	try
	{
	    financeAdapter = (ProgressFinanceAdapter) PaymentFactory
		    .getFinanceAdapter(FinanceAdapter.FINANCE_SYSTEM_MFGPRO);
	}
	catch (Exception ex)
	{
	    throw new FinanceException(
		    "Unable to get MFG/PRO finance adapter.", ex);
	}
	financeOneAccount = financeAdapter.translateAccounts(entity,
		accountCode, subAccountCode, costCentre);

	return financeOneAccount;
    }

    public Collection getGLTransactions(DateTime effectiveStartDate,
	    DateTime effectiveEndDate, DateTime enteredStartDate,
	    DateTime enteredEndDate, String subSystem, String glAccount,
	    String glEntity) throws FinanceException, RemoteException
    {
	Collection glTransactionList = null;

	FinanceAdapter financeAdapter = PaymentFactory.getFinanceAdapter();
	try
	{
	    glTransactionList = financeAdapter.getGLTransactions(
		    effectiveStartDate, effectiveEndDate, enteredStartDate,
		    enteredEndDate, subSystem, glAccount, glEntity);
	}
	catch (ValidationException ex)
	{
	    throw new FinanceException(ex);
	}
	return glTransactionList;
    }

    /**
     * Get the default earnins SQL that uses the post at date.
     * 
     * @return String
     */
    private String getDefaultEarningsSQL()
    {
	String earningsSQL = "";
	earningsSQL += EARNINGS_SQL[0];
	earningsSQL += EARNINGS_SQL[1];
	return earningsSQL;
    }

}
