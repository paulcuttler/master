package com.ract.payment.finance;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;

/**
 * <p>
 * Represents a general ledger transaction from the finance system.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class GLTransaction implements Serializable {
    /**
     * Date entered
     */
    private com.ract.util.DateTime enteredDate;

    /**
     * Date effective
     */
    private com.ract.util.DateTime effectiveDate;
    private String account;
    private String entity;
    private String subAccount;
    private String costCentre;
    private java.math.BigDecimal amount;
    private String subsystem;

    public GLTransaction() {
    }

    private String narration;

    public String getNarration() {
	return narration;
    }

    public void setNarration(String narration) {
	this.narration = narration;
    }

    /**
     * Construct a GL transaction
     */
    public GLTransaction(DateTime effDate, DateTime entDate, String acc, String ent, String subAcc, String cc, BigDecimal amt, String subsys, String narration) {
	this.setEffectiveDate(effDate);
	this.setEnteredDate(entDate);
	this.setAccount(acc);
	this.setEntity(ent);
	this.setSubAccount(subAcc);
	this.setCostCentre(cc);
	this.setAmount(amt);
	this.setSubsystem(subsys);
	this.setNarration(narration);
    }

    public com.ract.util.DateTime getEnteredDate() {
	return enteredDate;
    }

    public void setEnteredDate(com.ract.util.DateTime enteredDate) {
	this.enteredDate = enteredDate;
    }

    public com.ract.util.DateTime getEffectiveDate() {
	return effectiveDate;
    }

    public void setEffectiveDate(com.ract.util.DateTime effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    public String getAccount() {
	return account;
    }

    public void setAccount(String account) {
	this.account = account;
    }

    public String getEntity() {
	return entity;
    }

    public void setEntity(String entity) {
	this.entity = entity;
    }

    public String getSubAccount() {
	return subAccount;
    }

    public void setSubAccount(String subAccount) {
	this.subAccount = subAccount;
    }

    public String getCostCentre() {
	return costCentre;
    }

    public void setCostCentre(String costCentre) {
	this.costCentre = costCentre;
    }

    public java.math.BigDecimal getAmount() {
	return amount;
    }

    public void setAmount(java.math.BigDecimal amount) {
	this.amount = amount;
    }

    public String getSubsystem() {
	return subsystem;
    }

    public void setSubsystem(String subsystem) {
	this.subsystem = subsystem;
    }

    public BigDecimal getCreditAmount() {
	if (isCredit()) {
	    return amount.abs();
	} else {
	    return new BigDecimal(0);
	}
    }

    public BigDecimal getDebitAmount() {
	if (isDebit()) {
	    return amount.abs();
	} else {
	    return new BigDecimal(0);
	}
    }

    /**
     * <p>
     * Two general ledger transactions are equal if they have the same:
     * </p>
     * <ul>
     * <li>Subsystem</li>
     * <li>Effective Date</li>
     * <li>Account</li>
     * <li>Entity</li>
     * <li>Subaccount</li>
     * <li>Cost Centre</li>
     * </ul>
     * 
     * @param thatTrans
     *            Object
     * @return boolean
     */
    public boolean equals(Object thatTrans) {
	// identical
	if (this == thatTrans) {
	    return true;
	}
	// if not a gl transaction
	if (!(thatTrans instanceof GLTransaction)) {
	    return false;
	}

	GLTransaction glTrans = (GLTransaction) thatTrans;
	// LogUtil.log(this.getClass(),glTrans.toString());
	// LogUtil.log(this.getClass(),this.toString());

	String glTransSubsystem = glTrans.getSubsystem() == null ? "" : glTrans.getSubsystem();
	String thisSubsystem = this.getSubsystem() == null ? "" : this.getSubsystem();

	return (thisSubsystem.equals(glTransSubsystem) // source system
		&& this.getEffectiveDate().equals(glTrans.getEffectiveDate()) // effective
									      // date
		&& this.getAccount().equals(glTrans.getAccount()) // account
		&& this.getEntity().equals(glTrans.getEntity()) // entity
		&& this.getSubAccount().equals(glTrans.getSubAccount()) // sub
									// account
	&& this.costCentre.equals(glTrans.getCostCentre()) // cost centre
	);
    }

    public boolean isCredit() {
	if (this.getAmount().compareTo(new BigDecimal(0)) < 0) {
	    return true;
	} else {
	    return false;
	}
    }

    public boolean isDebit() {
	if (this.getAmount().compareTo(new BigDecimal(0)) > 0) {
	    return true;
	} else {
	    return false;
	}

    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("GL Transaction: ");
	desc.append(this.getSubsystem() + " ");
	desc.append(this.getEnteredDate() + " ");
	desc.append(this.getEffectiveDate() + " ");
	desc.append(this.getAccount() + " ");
	desc.append(this.getEntity() + " ");
	desc.append(this.getSubAccount() + " ");
	desc.append(this.getCostCentre() + " ");
	desc.append(CurrencyUtil.formatDollarValue(this.getAmount()));
	return desc.toString();
    }

}
