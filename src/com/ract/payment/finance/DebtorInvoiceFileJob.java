package com.ract.payment.finance;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.payment.PaymentEJBHelper;
import com.ract.util.LogUtil;

/**
 * <p>
 * Scheduled debtor invoice file processing job.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class DebtorInvoiceFileJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
	LogUtil.log(this.getClass(), "execute");
	try {
	    FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	    LogUtil.log(this.getClass(), "processDebtorInvoiceFiles");
	    financeMgr.processDebtorInvoiceFiles();

	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}

    }

    public static void main(String args[]) {
	DebtorInvoiceFileJob dif = new DebtorInvoiceFileJob();
	try {
	    dif.execute(null);
	} catch (JobExecutionException ex) {
	    System.err.println(ex);
	}
    }

}
