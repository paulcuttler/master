package com.ract.payment.finance;

import java.math.BigDecimal;

/**
 * <p>
 * Represents a line on an invoice/order
 * </p>
 * 
 * @author dgk
 * @version 1.0
 */
public class LineItem {
    /**
     * The code for the item sold on this line
     */
    private String stockCode = null;

    /**
     * The amount or quantity being sold
     */
    private BigDecimal quantity = null;

    /**
     * The unit of sale
     */
    private String stockUnit = null;

    /**
     * Price for one unit of the item
     */
    private BigDecimal stockPrice = null;

    /**
     * The discount offered on this line
     */
    private BigDecimal stockDisc = null;

    private boolean taxable;

    /**
     * public constructor
     */
    public LineItem() {
    }

    public BigDecimal getQuantity() {
	return quantity;
    }

    public String getStockCode() {
	return stockCode;
    }

    public BigDecimal getStockDisc() {
	return stockDisc;
    }

    public BigDecimal getStockPrice() {
	return stockPrice;
    }

    public String getStockUnit() {
	return stockUnit;
    }

    public boolean isTaxable() {
	return taxable;
    }

    public void setStockUnit(String stockUnit) {
	this.stockUnit = stockUnit;
    }

    public void setStockPrice(BigDecimal stockPrice) {
	this.stockPrice = stockPrice;
    }

    public void setStockDisc(BigDecimal stockDisc) {
	this.stockDisc = stockDisc;
    }

    public void setStockCode(String stockCode) {
	this.stockCode = stockCode;
    }

    public void setQuantity(BigDecimal quantity) {
	this.quantity = quantity;
    }

    public void setTaxable(boolean taxable) {
	this.taxable = taxable;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(quantity + ",");
	desc.append(stockCode + ",");
	desc.append(stockDisc + ",");
	desc.append(stockPrice + ",");
	desc.append(stockUnit + ",");
	desc.append(taxable);
	return desc.toString();
    }

}
