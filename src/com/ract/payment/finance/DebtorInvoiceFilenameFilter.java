package com.ract.payment.finance;

import java.io.File;
import java.io.FilenameFilter;

/**
 * <p>
 * Debtor invoice files only.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class DebtorInvoiceFilenameFilter implements FilenameFilter {

    public final String FILE_PREFIX = "drin";

    public boolean accept(File dir, String name) {
	// LogUtil.log(this.getClass(),"accept="+name);
	return name.substring(0, 4).equalsIgnoreCase(FILE_PREFIX);
    }

}
