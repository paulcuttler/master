package com.ract.payment.finance;

import java.io.File;
import java.io.FilenameFilter;

public class DebtorMasterFilenameFilter implements FilenameFilter {
    public final String FILE_PREFIX = "drmaster";

    public boolean accept(File dir, String name) {
	// LogUtil.log(this.getClass(),"accept="+name);
	if (name.length() >= 8) {
	    return name.substring(0, 8).equalsIgnoreCase(FILE_PREFIX);
	} else {
	    return false;
	}
    }

}
