package com.ract.payment.finance;

import java.io.IOException;

import com.progress.open4gl.ConnectException;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;
import com.ract.common.CommonConstants;
import com.ract.common.proxy.ProgressProxy;

public class FinanceProgressProxy extends FinanceAdapterProxy implements ProgressProxy {
    public FinanceProgressProxy() throws ConnectException, Open4GLException, SystemErrorException, IOException {
	super(CommonConstants.getProgressAppServerURL(), CommonConstants.getProgressAppserverUser(), CommonConstants.getProgressAppserverPassword(), null);
    }

}
