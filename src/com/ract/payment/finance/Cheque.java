package com.ract.payment.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.GregorianCalendar;

import com.ract.common.SystemException;
import com.ract.util.DateTime;

public class Cheque implements Serializable {
    /**
     * payeeNo is the unique identifier in the finance system (MFGPro) to which
     * the cheque is to be sent.
     */
    private String payeeNo = null;

    private DateTime invoiceDate = null;

    private String subSystem = null;

    /**
     * A = Branch, B = mail, C = Claim supervisor
     */
    private String chqRet = null;

    private BigDecimal grossAmt = null;

    /**
     * Consolidation options: D = daily N = now M = manual E = end of month F =
     * fortnight W = week
     */
    private String chqPrt = "N";

    /**
     * 10 = RACT 20 = RACTi
     */
    private String entity = null;

    /**
     * What the cheque applies to (eg Membership Number 123456)
     */
    private String remark = null;

    /**
     * If true, do not consolidate cheques
     */
    private boolean separateCheque = true;

    /**
     * Cheque particulars (eg Client deceased, refund to poor old widow)
     */
    private String descr = null;

    /**
     * Another description field - what the payment relates to (eg Membership
     * Cancellation)
     */
    private String invoice = null;

    /**
     * Unique identifier for this voucher. A two character prefix plus a
     * numerical sequence (eg RR1234)
     */
    private String voucher = null;

    private DateTime chequeDate = null;

    private String chequeNumber = null;

    private BigDecimal chequeAmount = null;

    /**
     * Default constructor creates a new voucher number
     */
    public Cheque() throws SystemException {
    }

    public Cheque(String voucherNo) {
	this.voucher = voucherNo;
    }

    public String getChqPrt() {
	return chqPrt;
    }

    public String getVoucher() {
	return voucher;
    }

    public String getChqRet() {
	return chqRet;
    }

    public String getDescr() {
	return descr;
    }

    public String getEntity() {
	return entity;
    }

    public BigDecimal getGrossAmt() {
	return grossAmt;
    }

    public String getInvoice() {
	return invoice;
    }

    public DateTime getInvoiceDate() {
	return invoiceDate;
    }

    public GregorianCalendar getInvoiceDateGC() {
	GregorianCalendar gc = new GregorianCalendar();
	gc.setTime(this.invoiceDate);
	return gc;
    }

    public String getPayeeNo() {
	return payeeNo;
    }

    public String getRemark() {
	return remark;
    }

    public boolean isSeparateCheque() {
	return separateCheque;
    }

    public String isSeparateChequeYN() {
	return separateCheque ? "Y" : "N";
    }

    public String getSubSystem() {
	return subSystem;
    }

    public void setChqPrt(String chqPrt) {
	this.chqPrt = chqPrt;
    }

    public void setChqRet(String chqRet) {
	this.chqRet = chqRet;
    }

    public void setDescr(String descr) {
	this.descr = descr;
    }

    public void setEntity(String entity) {
	this.entity = entity;
    }

    public void setGrossAmt(BigDecimal grossAmt) {
	this.grossAmt = grossAmt;
    }

    public void setGrossAmt(double grossAmt) {
	this.grossAmt = new BigDecimal(grossAmt);
    }

    public void setInvoice(String invoice) {
	this.invoice = invoice;
    }

    public void setInvoiceDate(DateTime invoiceDate) {
	this.invoiceDate = invoiceDate;
    }

    public void setPayeeNo(String payeeNo) {
	this.payeeNo = payeeNo;
    }

    public void setRemark(String remark) {
	this.remark = remark;
    }

    public void setSeparateCheque(boolean separateCheque) {
	this.separateCheque = separateCheque;
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    public BigDecimal getChequeAmount() {
	return chequeAmount;
    }

    public void setVoucher(String voucher) {
	this.voucher = voucher;
    }

    public DateTime getChequeDate() {
	return chequeDate;
    }

    public String getChequeNumber() {
	return chequeNumber;
    }

    public void setChequeAmount(BigDecimal chequeAmount) {
	this.chequeAmount = chequeAmount;
    }

    public void setChequeDate(DateTime chequeDate) {
	this.chequeDate = chequeDate;
    }

    public void setChequeNumber(String chequeNumber) {
	this.chequeNumber = chequeNumber;
    }

    @Override
    public String toString() {
	return "Cheque [chequeAmount=" + chequeAmount + ", chequeDate=" + chequeDate + ", chequeNumber=" + chequeNumber + ", chqPrt=" + chqPrt + ", chqRet=" + chqRet + ", descr=" + descr + ", entity=" + entity + ", grossAmt=" + grossAmt + ", invoice=" + invoice + ", invoiceDate=" + invoiceDate + ", payeeNo=" + payeeNo + ", remark=" + remark + ", separateCheque=" + separateCheque + ", subSystem=" + subSystem + ", voucher=" + voucher + "]";
    }

}