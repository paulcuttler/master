package com.ract.payment.finance;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Stock item representing an item of stock from the finance system.
 * 
 * @todo return in validateStockItem
 * 
 * @author jyh
 * @version 1.0
 */

public class StockItem implements Serializable {
    /**
     * Quantity on hand
     */
    private BigDecimal onHand;

    /**
     * Stock identifier
     */
    private String stockCode;

    /**
     * The sell price
     */
    private BigDecimal sellPrice;

    /**
     * The special sell price - discounted prices
     */
    private BigDecimal sellPrice2;

    /**
     * A description of the stock
     */
    private String stockDescription;

    /**
     * The stock profit centre
     */
    private String profitCentre;

    public String getStockDescription() {
	return stockDescription;
    }

    public BigDecimal getSellPrice2() {
	return sellPrice2;
    }

    public BigDecimal getSellPrice() {
	return sellPrice;
    }

    public String getStockCode() {
	return stockCode;
    }

    public void setOnHand(BigDecimal onHand) {
	this.onHand = onHand;
    }

    public void setStockDescription(String stockDescription) {
	this.stockDescription = stockDescription;
    }

    public void setSellPrice2(BigDecimal sellPrice2) {
	this.sellPrice2 = sellPrice2;
    }

    public void setSellPrice(BigDecimal sellPrice) {
	this.sellPrice = sellPrice;
    }

    public void setStockCode(String stockCode) {
	this.stockCode = stockCode;
    }

    public void setProfitCentre(String profitCentre) {
	this.profitCentre = profitCentre;
    }

    public BigDecimal getOnHand() {
	return onHand;
    }

    public String getProfitCentre() {
	return profitCentre;
    }

    /**
     * Construct a stock item.
     * 
     * @param stockCode
     *            String
     * @param onHand
     *            BigDecimal
     * @param sellPrice
     *            BigDecimal
     * @param sellPrice2
     *            BigDecimal
     */
    public StockItem(String stockCode, String profitCentre, BigDecimal onHand, BigDecimal sellPrice, BigDecimal sellPrice2) {
	this.setStockCode(stockCode);
	this.setProfitCentre(profitCentre);
	this.setOnHand(onHand);
	this.setSellPrice(sellPrice);
	this.setSellPrice2(sellPrice2);
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.stockCode + " ");
	desc.append(this.profitCentre + " ");
	desc.append(this.stockDescription + " ");
	desc.append(this.onHand + " ");
	desc.append(this.sellPrice + " ");
	desc.append(this.sellPrice2);
	return desc.toString();
    }
}
