package com.ract.payment.finance;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class DebtorInvoicePK implements java.io.Serializable {

    public void setInvoiceNumber(String invoiceNumber) {
	this.invoiceNumber = invoiceNumber;
    }

    public DebtorInvoicePK() {
    }

    // DOC_REF1
    private String invoiceNumber;

    /**
     * @hibernate.key-property column="invoice_number"
     * @return String
     */
    public String getInvoiceNumber() {
	return invoiceNumber;
    }

    /**
     * @hibernate.key-property column="status"
     */
    public String getStatus() {
	return status;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    /**
     * @hibernate.key-property column="create_date"
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    public static final String STATUS_ACTIVE = "A";
    public static final String STATUS_DELETED = "D";

    private String status = STATUS_ACTIVE;

    public void setStatus(String status) {
	this.status = status;
    }

    private DateTime createDate;

    public boolean isActive() {
	return this.getStatus().equals(STATUS_ACTIVE);
    }

    public boolean equals(Object obj) {
	if (obj instanceof DebtorInvoicePK) {
	    LogUtil.log(this.getClass(), "equals");
	    DebtorInvoicePK that = (DebtorInvoicePK) obj;
	    LogUtil.log(this.getClass(), this + "=" + that);
	    return this.invoiceNumber.equals(that.invoiceNumber) && this.status.equals(that.status) && this.createDate.equals(that.createDate);
	}
	return false;
    }

    public int hashCode() {
	return this.invoiceNumber.hashCode() + this.status.hashCode() + this.createDate.hashCode();
    }

    public String toString() {
	return this.invoiceNumber + "/" + this.status + "/" + this.createDate.formatLongDate();
    }

}
