package com.ract.payment.finance;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;

public class FinanceHelper {

    /**
     * Does the set of journal total 0?
     * 
     * @param journals
     *            Vector
     * @throws FinanceException
     * @return boolean
     */
    public static boolean isBalanced(Vector journals) throws FinanceException 
    {
	BigDecimal journalTotal = new BigDecimal(0);
	boolean balanced = false;
	if (journals != null) 
	{
	    Journal journal = null;
	    for (int i = 0; i < journals.size(); i++) 
	    {
	    	journal = (Journal) journals.get(i);
	    	journalTotal = journalTotal.add(new BigDecimal(journal.getAmount()));
	    	LogUtil.log("FinanceHelper", "journalTotal: " + journalTotal + "     " + journal.toString());
	    }
	    
	    if (journalTotal.intValue() == 0) 
	    {
	    	balanced = true;
	    }
	    
	    LogUtil.log("FinanceHelper", "Journals: " + journals.size());
	}
	
	LogUtil.log("FinanceHelper", "Balanced: " + balanced);
	LogUtil.log("FinanceHelper", "Journal Total: " + journalTotal);
	if (!balanced) 
	{
	    throw new FinanceException("Journals are not balanced. Total is " + CurrencyUtil.formatDollarValue(journalTotal) + ".");
	}
	return balanced;
    }

    public static String formatFinanceOneAccount(String financeOneAccount) {
	final String DELIMITER = ".";
	// fixed length
	String cc = financeOneAccount.substring(0, 2);
	String accountNumber = financeOneAccount.substring(2, 6);
	return cc + DELIMITER + accountNumber;
    }

    public static void logJournalDetails(Vector journals, boolean debug) {
	if (debug) {
	    String journalString = "";
	    journalString += "Effective Date" + ",";
	    journalString += "Account" + ",";
	    journalString += "Sales Branch" + ",";
	    journalString += "Amount" + ",";
	    journalString += "Subaccount" + ",";
	    journalString += "Cost Centre" + ",";
	    journalString += "Company" + "\n";

	    for (int x = 0; x < journals.size(); x++) {
		Journal journal = (Journal) journals.get(x);

		journalString += DateUtil.formatShortDate(journal.getPostAtDate()) + ",";
		journalString += journal.getAccount() + ",";
		journalString += journal.getSalesBranch() + ",";
		journalString += journal.getAmount() + ",";
		journalString += journal.getSubAccount() + ",";
		journalString += journal.getCostCentre() + ",";
		journalString += journal.getCompany() + "\n";
	    }

	    String logFile = null;
	    try {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		logFile = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.FINANCE_TRANSFER_FILE);
		FileOutputStream fos = new FileOutputStream(logFile);
		fos.write(journalString.getBytes());
		fos.flush();
		fos.close();
	    } catch (Exception e) {
		LogUtil.fatal("FinanceHelper", "Unable to write finance transfer log file '" + logFile + "'. " + e);
	    }
	}
    }

    /**
     * Validate the effective date given a number of accounting rules regarding
     * closing financial periods.
     * 
     * @param DateTime
     *            effectiveDate the effective date to validate.
     * @return The validated effective date.
     */
    public static DateTime validateEffectiveDate(DateTime effectiveDate, DateTime posted) {
	// LogUtil.log("validateEffectiveDate","-----------------------");
	// LogUtil.log("validateEffectiveDate","effectiveDate="+new
	// DateTime(effectiveDate.getTime()));
	// Until the threshold for current month (10th) we
	// can back date to the previous month.
	// If the effective date is for next month or before the current
	// month after the threshold then give it today's date.

	final int MAX_DAY_THRESHOLD = 10;
	if (posted == null) {
	    posted = new DateTime();
	}
	// testing try { now = new DateTime("11/7/2003"); }catch(Exception e){}

	int year = posted.getDateYear();
	int month = (posted.getMonthOfYear() + 1);
	int day = posted.getDayOfMonth();
	// LogUtil.log("now",day+" / "+month+" / "+year);
	// LogUtil.log("now",now.toString());

	GregorianCalendar absMinDate = new GregorianCalendar();
	// 1st of last month
	absMinDate.set(year, month - 2, 1);

	// LogUtil.log("validateEffectiveDate","absMinDate="+new
	// DateTime(absMinDate.getTime()));

	GregorianCalendar minDate = null;
	GregorianCalendar maxDate = null;

	GregorianCalendar thresholdDate = new GregorianCalendar();
	// 10 days into the current month.
	thresholdDate.set(year, month - 1, MAX_DAY_THRESHOLD);

	// LogUtil.log("validateEffectiveDate","thresholdDate="+new
	// DateTime(thresholdDate.getTime()));

	if (posted.after(thresholdDate.getTime())) {
	    minDate = new GregorianCalendar();
	    // first of this month
	    minDate.set(year, month - 1, 1);
	    maxDate = new GregorianCalendar();
	    // end of next month
	    maxDate.set(year, month, 1);
	    // max days in month
	    int maxDays = maxDate.getActualMaximum(Calendar.DAY_OF_MONTH);
	    // end of next month
	    maxDate.set(Calendar.DAY_OF_MONTH, maxDays);
	} else if (posted.onOrBeforeDay(thresholdDate.getTime())) {
	    minDate = absMinDate;
	    maxDate = new GregorianCalendar();
	    // end of this month
	    maxDate.set(year, month - 1, 1);
	    int maxDays = maxDate.getActualMaximum(Calendar.DAY_OF_MONTH);
	    maxDate.set(Calendar.DAY_OF_MONTH, maxDays);
	}
	// LogUtil.log("validateEffectiveDate","minDate="+new
	// DateTime(minDate.getTime()));
	// LogUtil.log("validateEffectiveDate","maxDate="+new
	// DateTime(maxDate.getTime()));
	if (effectiveDate.afterDay(maxDate.getTime()) || effectiveDate.beforeDay(minDate.getTime())) {
	    effectiveDate = posted;
	}

	// LogUtil.log("validateEffectiveDate","effectiveDate="+effectiveDate);

	// else use the original value

	return effectiveDate.getDateOnly();
    }

    /**
     * Get the period to post into.
     * 
     * @param effectiveDate
     *            DateTime
     * @return int
     */
    public static int getPeriod(Date effectiveDate) {
	int periods[] = { 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6 };
	GregorianCalendar gc = new GregorianCalendar();
	gc.setTime(effectiveDate);
	int monthOfYear = gc.get(Calendar.MONTH);
	int period = periods[monthOfYear];
	return period;
    }

    public static String getTransactionReference(SourceSystem sourceSystem, String numericReference) {
	String prefix = sourceSystem.getAbbreviation();
	StringBuffer transactionReference = new StringBuffer();
	transactionReference.append(prefix);
	transactionReference.append(numericReference);
	return transactionReference.toString();
    }

}
