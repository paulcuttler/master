/*
 * @(#)FinanceException.java
 *
 */

package com.ract.payment.finance;

import com.ract.payment.PaymentException;

/**
 * Generic exception for Receipting system.
 * 
 * @author Glenn Lewis
 * @version 1.0, 20/5/2002
 */
public class FinanceException extends PaymentException {
    public FinanceException() {
    }

    public FinanceException(String msg) {
	super(msg);
    }

    public FinanceException(Exception chainedException) {
	super(chainedException);
    }

    public FinanceException(String msg, Exception chainedException) {
	super(msg, chainedException);
    }
}