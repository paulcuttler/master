package com.ract.payment.finance;

import java.io.Serializable;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.PostalAddressVO;
import com.ract.common.SystemException;

/**
 * 
 * <p>
 * Title: PayeeVO
 * </p>
 * .
 * <p>
 * Description: Value object for payees
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company: RACT
 * </p>
 * 
 * @author dgk
 * @version 1.0 31/7/2003
 */

public class FinancePayee implements Serializable {
    /**
     * Unique payee identifier (MFGPro uses 8 character field, first character
     * is a letter which identifies what sequence is used. Use C if the payee is
     * an RACT client. The subsequent characters are made up of the client
     * number right justified, zero filled)
     */
    private String payeeNo = null;

    private String payeeName = null;

    private String address1 = null;

    private String address2 = "";

    private String address3 = "";

    private String postcode = "";

    private String suburb = "";

    private String state = "";

    private String abn = "";

    /* the following fields are used if the payment method is by EFT */

    private String bankCode = "";

    private String bsb = "";

    private String accountNo = "";

    /* faxNo or emailAddr required for electronic remittance advice */
    private String faxNo = "";

    private String emailAddr = "";

    /**
     * bankType depends on the type of transaction, so it should not actually be
     * here at all. See MFGPro banks for details.
     */
    private String bankType = null;

    /**
     * If the client number is supplied, create the PayeeVO from the client. In
     * this case only the client bank details, chequeForm and our bank Account
     * need be added if this is an eft client
     * 
     * @param clientNumber
     */
    public FinancePayee(Integer clientNumber) throws SystemException {
	ClientVO client = null;
	try {
	    ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    client = clientMgr.getClient(clientNumber);
	} catch (Exception e) {
	    throw new SystemException("Unable to get client data for client " + clientNumber + ".  " + e);
	}
	payeeName = client.getDisplayName();
	PostalAddressVO addressVO = null;
	try {
	    addressVO = client.getPostalAddress();
	} catch (Exception e) {
	    throw new SystemException("Unable to retrieve postal address for client " + client.getClientNumber());
	}
	address1 = addressVO.getAddressLine1();
	address2 = addressVO.getAddressLine2();
	address3 = "";
	postcode = addressVO.getPostcode();
	suburb = addressVO.getSuburb();
	state = addressVO.getState();
	faxNo = "";
	// faxNo = client.getFaxNumber();
	abn = client.getABN() + "";
	emailAddr = client.getEmailAddress();
    }

    public void setPayeeNo(String payeeNo) {
	this.payeeNo = payeeNo;
    }

    /**
     * Required when reconstructing a payee object from a set of data returned
     * from the finance system
     * 
     * @param payeeNo
     */
    // public FinancePayee(String payeeNo)
    // {
    // this.payeeNo = payeeNo;
    // }

    /**
     * if no clientNumber is supplied, create payee number with standard third
     * party prefix
     */
    public FinancePayee() throws SystemException {

    }

    public String getAbn() {
	return abn;
    }

    public String getAccountNo() {
	return accountNo;
    }

    public String getAddress1() {
	return address1;
    }

    public String getAddress2() {
	return address2;
    }

    @Override
    public String toString() {
	return "FinancePayee [abn=" + abn + ", accountNo=" + accountNo + ", address1=" + address1 + ", address2=" + address2 + ", address3=" + address3 + ", bankCode=" + bankCode + ", bankType=" + bankType + ", bsb=" + bsb + ", emailAddr=" + emailAddr + ", faxNo=" + faxNo + ", payeeName=" + payeeName + ", payeeNo=" + payeeNo + ", postcode=" + postcode + ", state=" + state + ", suburb=" + suburb + "]";
    }

    public String getAddress3() {
	return address3;
    }

    public String getBankCode() {
	return bankCode;
    }

    public String getBsb() {
	return bsb;
    }

    public String getBankType() {
	return bankType;
    }

    public String getFaxNo() {
	return faxNo;
    }

    public String getPayeeName() {
	return payeeName;
    }

    public String getPayeeNo() {
	return payeeNo;
    }

    public String getPostcode() {
	return postcode;
    }

    public String getState() {
	return state;
    }

    public String getSuburb() {
	return suburb;
    }

    public void setSuburb(String suburb) {
	this.suburb = suburb;
    }

    public void setState(String state) {
	this.state = state;
    }

    public void setPostcode(String postcode) {
	this.postcode = postcode;
    }

    public void setPayeeName(String payeeName) {
	this.payeeName = payeeName;
    }

    public void setFaxNo(String faxNo) {
	this.faxNo = faxNo;
    }

    public void setBsb(String bsb) {
	this.bsb = bsb;
    }

    public void setBankType(String bankType) {
	this.bankType = bankType;
    }

    public void setBankCode(String bankCode) {
	this.bankCode = bankCode;
    }

    public void setAddress3(String address3) {
	this.address3 = address3;
    }

    public void setAddress2(String address2) {
	this.address2 = address2;
    }

    public void setAddress1(String address1) {
	this.address1 = address1;
    }

    public void setAccountNo(String accountNo) {
	this.accountNo = accountNo;
    }

    public void setAbn(String abn) {
	this.abn = abn;
    }

    public String getEmailAddr() {
	return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
	this.emailAddr = emailAddr;
    }

}
