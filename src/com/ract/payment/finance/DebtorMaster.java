package com.ract.payment.finance;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.client.ClientVO;
import com.ract.common.AddressVO;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * <p>
 * </p>
 * 
 * @hibernate.class table="fin_debtor_master" lazy="false"
 * 
 * @author not attributable
 * @version 1.0
 */
public class DebtorMaster implements Serializable {
    private String accountNumber;

    private String surname;

    private String givenNames;

    private String postalName;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String state;

    private String postcode;

    private String phoneNumber;

    private String emailAddress;

    private String status;

    /**
     * @hibernate.property
     * @hibernate.column name="DESCR2"
     */
    public String getGivenNames() {
	return givenNames;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="DESCR1"
     */
    public String getSurname() {
	return surname;
    }

    public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
    }

    public DebtorMaster(ClientVO client) {
	this.setAccountNumber(StringUtil.padNumber(client.getClientNumber(), 8));
	this.setGivenNames(client.getGivenNames());
	this.setSurname(client.getSurname());
	AddressVO address = null;
	try {
	    address = client.getPostalAddress();
	} catch (RemoteException ex) {
	    // ignore
	    LogUtil.warn(this.getClass(), ex);
	}
	this.setAddress1(address.getAddressLine1());
	this.setAddress2(address.getAddressLine2());
	// no address 3 field
	// no city field

	this.setState(address.getState());
	this.setPostalName(client.getPostalName());
	this.setPhoneNumber(client.getHomePhone());
	this.setEmailAddress(client.getEmailAddress());
    }

    private boolean equalsDebtorMaster(DebtorMaster debtorMaster) {
	if (debtorMaster.getAccountNumber().equals(this.getAccountNumber()) && debtorMaster.getGivenNames().equals(this.getGivenNames()) && debtorMaster.getSurname().equals(this.getSurname()) && debtorMaster.getAddress1().equals(this.getAddress1()) && debtorMaster.getAddress2().equals(this.getAddress2()) && debtorMaster.getPostcode().equals(this.getPostcode()) && debtorMaster.getPhoneNumber().equals(this.getPhoneNumber()) && debtorMaster.getEmailAddress().equals(this.getEmailAddress())) {
	    return true;
	} else {
	    return false;
	}

    }

    public boolean equals(Object o) {
	LogUtil.log(this.getClass(), "o=" + o.toString());
	LogUtil.log(this.getClass(), "this=" + this.toString());

	DebtorMaster debtorMaster = null;
	if (o instanceof DebtorMaster) {
	    debtorMaster = (DebtorMaster) o;
	    return equalsDebtorMaster(debtorMaster);
	} else if (o instanceof ClientVO) {
	    debtorMaster = new DebtorMaster((ClientVO) o);
	    LogUtil.log(this.getClass(), this.toString());
	    LogUtil.log(this.getClass(), debtorMaster.toString());

	    return equalsDebtorMaster(debtorMaster);
	} else {
	    return false;
	}
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.getAccountNumber());
	desc.append(": ");
	desc.append(this.getGivenNames());
	desc.append(" ");
	desc.append(this.getSurname());
	desc.append(" ");
	desc.append(this.getAddress1());
	desc.append(" ");
	desc.append(this.getAddress2());
	desc.append(" ");
	desc.append(this.getAddress3());
	desc.append(" ");
	desc.append(this.getCity());
	desc.append(" ");
	desc.append(this.getState());
	desc.append(" ");
	desc.append(this.getPhoneNumber());
	desc.append(" ");
	desc.append(this.getEmailAddress());
	desc.append(" ");
	desc.append(this.getPostalName());
	desc.append(" ");
	desc.append(this.getStatus());
	return desc.toString();
    }

    public void setGivenNames(String givenNames) {
	this.givenNames = givenNames;
    }

    public void setSurname(String surname) {
	this.surname = surname;
    }

    public void setPostalName(String postalName) {
	this.postalName = postalName;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
    }

    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }

    public void setPostcode(String postcode) {
	this.postcode = postcode;
    }

    public void setState(String state) {
	this.state = state;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public void setAddress3(String address3) {
	this.address3 = address3;
    }

    public void setAddress2(String address2) {
	this.address2 = address2;
    }

    public void setAddress1(String address1) {
	this.address1 = address1;
    }

    /**
     * @hibernate.id column="ACCNBRI" generator-class="assigned"
     * @return Integer
     */
    public String getAccountNumber() {
	return accountNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="POST_NAME"
     */
    public String getPostalName() {
	return postalName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="STATUS"
     * 
     *                   Not "C"
     */
    public String getStatus() {
	return status;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="EMAIL_ADDR"
     */
    public String getEmailAddress() {
	return emailAddress;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="PHONE_NBR"
     */
    public String getPhoneNumber() {
	return phoneNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="POST_CODE"
     */
    public String getPostcode() {
	return postcode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="STATE"
     */
    public String getState() {
	return state;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="CITY"
     */
    public String getCity() {
	return city;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="ADDR3"
     */
    public String getAddress3() {
	return address3;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="ADDR2"
     */
    public String getAddress2() {
	return address2;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="ADDR1"
     */
    public String getAddress1() {
	return address1;
    }

    public DebtorMaster() {
    }

}
