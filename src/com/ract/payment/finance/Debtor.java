package com.ract.payment.finance;

import java.io.Serializable;

import com.ract.util.DateTime;

/**
 * <p>
 * Represent a debtor in the finance system.
 * </p>
 * 
 * @hibernate.class table="fin_debtor" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class Debtor implements Serializable {

    public Debtor() {
    }

    public Debtor(Integer clientNumber, DateTime createDate) {
	setClientNumber(clientNumber);
	setCreateDate(createDate);
    }

    private Integer clientNumber;

    private DateTime createDate;

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    /**
     * @hibernate.id column="client_number" generator-class="assigned"
     * @return Integer
     */
    public Integer getClientNumber() {
	return clientNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="create_date"
     * @return DateTime
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    public String toString() {
	return clientNumber + " " + createDate;
    }

}
