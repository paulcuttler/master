package com.ract.payment.finance;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.payment.PaymentEJBHelper;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * <p>
 * Reset scheduled debtor invoice file processing job.
 * </p>
 * author gzn
 */
public class ResetDebtorInvoiceFileJob implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
	LogUtil.log(this.getClass(), "starting ResetDebtorInvoiceFileJob...");
	try {
	    FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	    // reset state
	    financeMgr.endDebtorFileProcessing(new DateTime());

	    LogUtil.log(this.getClass(), "finished ResetDebtorInvoiceFileJob.");
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), "ResetDebtorInvoiceFileJob failed! " + e);
	    // throw new JobExecutionException(e, false);
	}
    }

    /*
     * public static void main(String args[]) { ResetDebtorInvoiceFileJob dif =
     * new ResetDebtorInvoiceFileJob(); try { dif.execute(null); } catch
     * (JobExecutionException ex) { System.err.println(ex); } }
     */

}