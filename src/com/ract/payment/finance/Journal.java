/*
 * @(#)Journal.java
 *
 */
package com.ract.payment.finance;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.Company;
import com.ract.common.SalesBranchVO;
import com.ract.payment.PayableItemPostingVO;
import com.ract.util.DateTime;

/**
 * Represent journals to be sent to the finance system
 * 
 * @author Glenn Lewis
 * @version 1.0, 3/6/2002
 * 
 */

public class Journal implements Serializable {

    private static final String DEFAULT_BRANCH = "BRANCH";

    /**
     * The amount of the journal entry. Debits are positive, credits are
     * negative.
     */
    private double amount;

    /** The account for the journal entry (general ledger code) */
    private String account;

    /** The account cost centre of the journal entry */
    private String costCentre;

    /** The account sub account of the journal entry */
    private String subAccount;

    /** The company code for the posting */
    private Company company;

    /**
     * The date the journal is to be posted to the finance system. Only the date
     * is significant. Hours and minutes are ignored.
     */
    private Date postAtDate;

    /**
     * The string representing the sales branch for the posting
     */
    private String salesBranch;

    /**
     * Vector of postings that make up the journal entry
     * 
     * @todo move to posting container?
     */
    Vector postings;

    private String narration;

    private String project;

    private DateTime postedDate;

    public void setNarration(String narration) {
	this.narration = narration;
    }

    public void setProject(String project) {
	this.project = project;
    }

    public String getNarration() {
	return narration;
    }

    public String getProject() {
	return project;
    }

    /**
     * Constructor
     * 
     * @param amount
     *            the amount of the journal. A credit should be a negative
     *            number, a debit positive.
     * @param account
     *            the account number for the journal
     * @param costCentre
     *            the cost centre for the account (<code>null</code> indicates
     *            there no cost centre)
     * @param subAccount
     *            the sub-account for the account <code>null</code> indicates no
     *            sub-account.
     * @param company
     *            the company
     * @param salesBranch
     *            the sales branch for the posting
     */

    public Journal(double amount, String account, String costCentre, String subAccount, Company company, Date postAtDate, Date createDate, String salesBranch, String narration, String project, String journalKey) throws FinanceException {
	setAmount(amount);
	setAccount(account);
	// before costcentre
	setSalesBranch(salesBranch);
	setCostCentre(costCentre);
	setSubAccount(subAccount);
	setCompany(company);
	setPostAtDate(postAtDate);
	setJournalKey(journalKey);
	setNarration(narration);
	setProject(project);

	postings = new Vector();
    }

    private String journalKey;

    // ============ Getter Methods =============================================
    /**
     * Get the amount of the journal
     * 
     * @return the amount of the journal
     */
    public double getAmount() {
	return amount;
    }

    /**
     * Get the account of the journal
     * 
     * @return the account (general ledger code) of the journal
     */
    public String getAccount() {
	return account;
    }

    /**
     * Get the cost centre of the journal
     * 
     * @return the cost centre of the journal, or <code>null</code> if there is
     *         no cost centre
     */
    public String getCostCentre() {
	return costCentre;
    }

    /**
     * Get sub account of the journal
     * 
     * @return the sub account of the journal, or <code>null</code> if there is
     *         no sub account
     */
    public String getSubAccount() {
	return subAccount;
    }

    /** Get the postings that make up this journal entry */
    public Vector getPostings() {
	return postings;
    }

    /**
     * Get the company of the posting
     * 
     * @return the company of the posting
     */
    public Company getCompany() {
	return company;
    }

    /** Get the sales branch */
    public String getSalesBranch() {
	return salesBranch;
    }

    public DateTime getPostedDate() {
	return postedDate;
    }

    public Date getPostAtDate() {
	return postAtDate;
    }

    /**
     * Set the (total) amount of payable item
     * 
     * @param amount
     *            the total amount of this payable item.
     */
    public void setAmount(double amount) throws FinanceException {
	this.amount = amount;
    }

    /**
     * Set the account (general ledger code) of the journal
     * 
     * @param account
     *            the account (general ledger code) to be set
     * @throws <code>FinanceException</code> if the account is <code>null</code>
     *         .
     */
    private void setAccount(String account) throws FinanceException {
	if (account == null) {
	    throw new FinanceException("Setting a null account number on a journal.");
	}
	this.account = account;
    }

    /**
     * Set the cost centre of the journal
     * 
     * @param costCentre
     *            the cost centre
     * @throws <code>FinanceException</code> if the cost centre is null
     */
    private void setCostCentre(String costCentre) throws FinanceException {
	if (costCentre == null) {
	    throw new FinanceException("Setting a null cost centre on a journal.");
	} else {
	    if (costCentre.equalsIgnoreCase(DEFAULT_BRANCH)) {
		try {
		    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		    SalesBranchVO salesBranchVO = commonMgr.getSalesBranch(this.getSalesBranch());
		    costCentre = salesBranchVO.getProfitCentre();
		    if (costCentre == null) {
			throw new FinanceException("Unable to set the cost centre for the DEFAULT_BRANCH");
		    }
		} catch (Exception e) {
		    throw new FinanceException("Error looking up cost centre");
		}

	    }
	}
	this.costCentre = costCentre;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(account + ",");
	desc.append(amount + ",");
	desc.append(company + ",");
	desc.append(costCentre + ",");
	desc.append(narration + ",");
	desc.append(postAtDate + ",");
	desc.append(project + ",");
	desc.append(salesBranch + ",");
	desc.append(subAccount);
	return desc.toString();
    }

    /**
     * Set the sub-account of the journal
     * 
     * @param subAccount
     *            the sub-account
     * @throws <code>FinanceException</code> if the sub-account is null
     */
    private void setSubAccount(String subAccount) throws FinanceException {
	if (subAccount == null) {
	    throw new FinanceException("Setting a sub account to null in a journal.");
	}
	this.subAccount = subAccount;
    }

    /**
     * Set the company of the posting
     * 
     * @param company
     *            the company
     * @throws <code>FinanceException</code> if the company is null
     */
    private void setCompany(Company company) throws FinanceException {
	if (company == null) {
	    throw new FinanceException("Setting a null company on a journal.");
	}
	this.company = company;
    }

    /** Set the sales branch */
    public void setSalesBranch(String salesBranch) throws FinanceException {
	if (salesBranch == null) {
	    throw new FinanceException("Setting the sales branch to null in a journal");
	}
	this.salesBranch = salesBranch;
    }

    public void setPostedDate(DateTime postedDate) {
	this.postedDate = postedDate;
    }

    public void setPostAtDate(Date postAtDate) {
	this.postAtDate = postAtDate;
    }

    /** Add a posting to the set of postings that make up this journal */
    public void addPosting(PayableItemPostingVO posting) {
	postings.addElement(posting);
    }

    public String getJournalKey() {
	return journalKey;
    }

    public void setJournalKey(String journalKey) {
	this.journalKey = journalKey;
    }
}
