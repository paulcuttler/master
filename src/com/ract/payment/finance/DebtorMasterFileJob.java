package com.ract.payment.finance;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.payment.PaymentEJBHelper;
import com.ract.util.LogUtil;

public class DebtorMasterFileJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
	LogUtil.log(this.getClass(), "execute");
	try {
	    FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	    LogUtil.log(this.getClass(), "processDebtorMasterFiles");
	    financeMgr.processDebtorMasterFiles();

	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}

    }

    public static void main(String args[]) {
	DebtorMasterFileJob dif = new DebtorMasterFileJob();
	try {
	    dif.execute(null);
	} catch (JobExecutionException ex) {
	    System.err.println(ex);
	}
    }

}
