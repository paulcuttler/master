package com.ract.payment.finance;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;

import javax.ejb.Remote;

import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.payment.FinanceVoucher;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.util.DateTime;

/**
 * Title: FinanceMgr Description: Handles transfer of journals to MFGpro.
 * Copyright: Copyright (c) 2002 Company: RACT
 * 
 * @author dgk
 * @version 1.0
 */
@Remote
public interface FinanceMgr {

    /**
     * calculation constants
     */
    public static final String CALCULATION_TYPE_EARNED = "Earned";

    public static final String CALCULATION_TYPE_UNEARNED = "Unearned";

    public static final String CALCULATION_TYPE_EARNED_FOR_DATE_RANGE = "Earned Date Range";

    public static final String PAYEE_PREFIX_ROADSIDE = "R";

    public static final String VOUCHER_PREFIX = "RR";

    public static final String SELECTION_MODE_START_DATE = "Start Date";

    public static final String SELECTION_MODE_POSTAT_DATE = "Post at Date";

    public void deleteDebtorInvoice(PaymentTransactionMgr paymentTransactionMgr, DebtorInvoice invoice, DateTime now, boolean update) throws RemoteException, RollBackException;

    public void updateVoucher(FinanceVoucher voucher) throws RemoteException;

    public void deleteVoucher(FinanceVoucher voucher) throws RemoteException;

    public Vector createJournals(SourceSystem sourceSystem, DateTime effectiveDate, DateTime overrideDate) throws RemoteException, PaymentException;

    public void deleteOldDebtorInvoices(DateTime now) throws FinanceException, RemoteException;

    public boolean startDebtorFileProcessing(DateTime now) throws RemoteException;

    public Collection getVouchersForApprovingUser(String approvingUserId) throws RemoteException;

    public void processDebtorMasterFiles() throws RemoteException;

    public void endDebtorFileProcessing(DateTime now) throws RemoteException;

    public Collection getVouchersForClient(String clientNumber) throws RemoteException;

    public String translateAccounts(String entity, String accountCode, String subAccountCode, String costCentre) throws FinanceException, RemoteException;

    public void transferDebtorInvoiceReceipts() throws FinanceException, RemoteException;

    public void createDebtor(Debtor debtor) throws FinanceException, RemoteException;

    public void updateDebtor(Debtor debtor) throws FinanceException, RemoteException;

    public Collection getVouchersByProcessDate(DateTime startDate, DateTime endDate, String status) throws RemoteException;

    public Debtor getDebtor(Integer clientNumber) throws FinanceException, RemoteException;

    public void createDebtorInvoiceForPayment(DebtorInvoice invoice, DateTime lastUpdate) throws RemoteException, RollBackException;

    public void processDebtorInvoiceFiles() throws RemoteException;

    public DebtorInvoice getInvoice(String invoiceNumber) throws RemoteException;

    public Collection getInvoicesByClientNumber(String customerNumber) throws RemoteException;

    /**
     * calculate the earned amounts for a range
     */
    public Hashtable calculateEarnedAmounts(DateTime startDate, DateTime endDate, String mode, boolean auditFile, String emailAddress, boolean capDaysToEarn, boolean correctDuration) throws PaymentException, RemoteException;

    /**
     * calculate the amount for an effective date and calculation type.
     */
    public Hashtable calculateAmounts(DateTime calculationDate, String calculationType, boolean auditFile, String emailAddress, boolean capDaysToEarn, boolean correctDuration) throws PaymentException, RemoteException;

    public void transferJournals(Vector journalList, SourceSystem sourceSystem) throws RemoteException, PaymentException;

    /**
     * Create the journals in the finance system and transfer them to the
     * finance system. Optionally create postings to transfer from unearned to
     * income accounts, and journal these newly created postings.
     * 
     * @param sourceSystem
     *            only postings for this source system will be journalled
     * @param incomePostings
     *            if <code>true</code> then postings will be created to transfer
     *            from unearned to income accounts. These posting will then be
     *            transferred to the finance system.
     */
    public void transferJournals(SourceSystem sourceSystem, boolean incomePostings, DateTime effectiveDate, DateTime overrideDate) throws RemoteException, PaymentException;

    public Collection getVouchersForReference(String sourceSystem, String sourceSystemReference) throws RemoteException;

    public Collection getVouchersForPayee(String payeeNo) throws RemoteException;

    public Collection getVouchersByVoucherId(String voucherId) throws RemoteException;

    public FinanceVoucher getVoucher(Integer disposalId) throws RemoteException;

    public Cheque getCheque(String voucherId) throws SystemException;

    public FinancePayee getPayee(String payeeNo) throws SystemException;

    public Collection getReceptorReceiptsToTransfer() throws RemoteException, FinanceException;

    public Collection getReceptorInvoices(String receiptNumber, DateTime backDate, DateTime createDate) throws RemoteException, Exception;

    public void createVoucher(FinanceVoucher voucher) throws RemoteException;

    /**
     * @see FinanceAdapter
     * @param effectiveStartDate
     * @param effectiveEndDate
     * @param enteredStartDate
     * @param enteredEndDate
     * @param subSystem
     * @param glAccount
     * @return
     * @throws FinanceException
     */
    public Collection getGLTransactions(DateTime effectiveStartDate, DateTime effectiveEndDate, DateTime enteredStartDate, DateTime enteredEndDate, String subSystem, String glAccount, String glEntity) throws FinanceException, RemoteException;

    public Collection getSubsystemList() throws FinanceException, RemoteException;

}
