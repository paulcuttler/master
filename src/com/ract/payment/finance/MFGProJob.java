package com.ract.payment.finance;

import java.util.Hashtable;

import javax.naming.InitialContext;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.SourceSystem;
import com.ract.payment.PaymentEJBHelper;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * MFG Pro scheduler Job.
 * 
 * @author jyh
 */

public class MFGProJob implements Job {

    public MFGProJob() {
    }

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	String instName = context.getJobDetail().getName();
	String instGroup = context.getJobDetail().getGroup();

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();

	try {

	    String providerURL = dataMap.getString("providerURL");
	    String sourceSystemString = dataMap.getString("sourceSystemString");
	    boolean incomePostings = dataMap.getBoolean("incomePostings");
	    DateTime effectiveDate = (DateTime) dataMap.get("effectiveDate");
	    DateTime overrideDate = (DateTime) dataMap.get("overrideDate");

	    SourceSystem sourceSystem = SourceSystem.getSourceSystem(sourceSystemString);

	    String initialContext = "org.jnp.interfaces.NamingContextFactory";
	    Hashtable environment = new Hashtable(2);
	    environment.put(javax.naming.Context.PROVIDER_URL, providerURL);
	    environment.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, initialContext);

	    InitialContext jndiContext = new javax.naming.InitialContext(environment);

	    FinanceMgr mgr = PaymentEJBHelper.getFinanceMgr();
	    mgr.transferJournals(sourceSystem, incomePostings, effectiveDate, overrideDate);

	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }

}