package com.ract.payment.finance;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Vector;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.StringHolder;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentFactory;
import com.ract.payment.PaymentIDMgr;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * <p>
 * The finance one implementation of the finance adapter.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class FinanceOneFinanceAdapter extends ProgressFinanceAdapter {

    private final String IMPORT_FILE_HEADER = "FORMAT BATCH IMPORT" + FileUtil.SEPARATOR_COMMA + "STANDARD 1.0" + FileUtil.NEW_LINE;

    /**
     * Unique document reference
     */
    private final String FIELD_DOCID = "DOCID";

    /**
     * Unique document reference 2. May be the same as DOCID.
     */
    private final String FIELD_DREF1 = "DREF1";

    /**
     * Transaction date
     */
    private final String FIELD_DDATE1 = "DDATE1";

    /**
     * The effective date of the transaction
     */
    private final String FIELD_DDATE2 = "DDATE2";

    private final String FIELD_DPERIOD = "DPERIOD";

    private final String FIELD_LINEID = "LINEID";

    private final String FIELD_LLDGCODE = "LLDGCODE";

    /**
     * Account number
     */
    private final String FIELD_LACCNBR = "LACCNBR";

    private final String FIELD_LAMOUNT1 = "LAMOUNT1";

    private final String FIELD_LNARR1 = "LNARR1";

    /**
     * Dummy debtor code
     */
    private final String FIELD_LNARR3 = "LNARR3";

    /**
     * Project code
     */
    private final String FIELD_LUSERFLD1 = "LUSERFLD1";

    /**
     * Import name for matching to a processing group and processing script.
     */
    private final String FIELD_BIMPNAME = "BIMPNAME";

    /**
     * Code flagging area of the system to affect.
     */
    private final String VALUE_LLDGCODE = "GL";

    public final String DUMMY_DEBTOR_CODE_ROADSIDE = "5400";

    final String BATCH_IDENTIFIER = "ROADSIDE";

    public static final String KEY_VOUCHER = "voucher";
    public static final String KEY_TIMESTAMP = "timestamp";

    public Hashtable getDocumentKey() throws FinanceException {
	StringHolder timestampHolder = new StringHolder();
	StringHolder voucherHolder = new StringHolder();
	FinanceProgressProxy financeProgressProxy = null;
	try {
	    financeProgressProxy = this.getFinanceProgressProxy();
	    financeProgressProxy.getTime(timestampHolder, voucherHolder);
	} catch (Open4GLException ex) {
	    throw new FinanceException("Unable to validate GL Code ", ex);
	} catch (SystemException se) {
	    throw new FinanceException(se);
	} finally {
	    releaseFinancyProgressProxy(financeProgressProxy);
	}

	String timestamp = timestampHolder.getStringValue();
	String voucher = voucherHolder.getStringValue();

	Hashtable time = new Hashtable();
	time.put(KEY_VOUCHER, voucher);
	time.put(KEY_TIMESTAMP, timestamp);

	// this.getPr
	return time;
    }

    public FinanceResponse transferJournalsNatively(Vector journals, SourceSystem sourceSystem, boolean debug) throws FinanceException 
    {
	final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
	final DecimalFormat DF = new DecimalFormat("#########.00");
	// final String ENTITY = "10";
	// final String DOCID = "1";

	String documentReference = null;

	int journalNumber = 0;

	int period = 0;

	// SYSTEM PARAMETER
	final String fileName = "/temp/test.csv";
	// SYSTEM PARAMETER
	
	// create a resultset to pass to progress
	if (journals.size() == 0) 
	{
	    throw new FinanceException("No journals to transfer. FinanceOne");
	}

	// check that journals total 0
	FinanceHelper.isBalanced(journals);

	LogUtil.debug(this.getClass(), "journals=" + journals.size());
	LogUtil.debug(this.getClass(), "sourceSystem=" + sourceSystem);
	LogUtil.debug(this.getClass(), "debug=" + sourceSystem);

	// if debug then write a transaction file to disk
	// for testing only
	// FinanceHelper.logJournalDetails(journals, debug);

	ProgressFinanceAdapter progressFinanceAdapter = null;
	try {
	    progressFinanceAdapter = (ProgressFinanceAdapter) PaymentFactory.getFinanceAdapter(FinanceAdapter.FINANCE_SYSTEM_MFGPRO);
	} catch (SystemException ex1) {
	    throw new FinanceException("Unable to get MFG/Pro adapter.", ex1);
	}

	StringBuffer fileBuffer = new StringBuffer();
	fileBuffer.append(IMPORT_FILE_HEADER);

	// define column headers
	fileBuffer.append(FIELD_DOCID);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_DREF1);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_DDATE1);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_DDATE2);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_DPERIOD);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_LINEID);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_LLDGCODE);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_LACCNBR);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_LAMOUNT1);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_LUSERFLD1);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_LNARR1);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_LNARR3);
	fileBuffer.append(FileUtil.SEPARATOR_COMMA);
	fileBuffer.append(FIELD_BIMPNAME);
	fileBuffer.append(FileUtil.NEW_LINE);

	try {

	    PaymentIDMgr pMgr = PaymentEJBHelper.getPaymentIDMgr();
	    journalNumber = pMgr.getNextJournalNumber();

	    Journal journal = null;
	    // add the records
	    int lineId = 0;
	    for (int i = 0; i < journals.size(); i++) {
		journal = (Journal) journals.get(i);
		lineId++;
		period = FinanceHelper.getPeriod(journal.getPostAtDate());
		documentReference = FinanceHelper.getTransactionReference(sourceSystem, String.valueOf(journalNumber));// Unique
														       // reference
														       // (journal
														       // number?
														       // with
														       // SourceSystem
														       // Prefix)
		fileBuffer.append(documentReference);
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(documentReference);
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(SDF.format(journal.getPostAtDate()));
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(SDF.format(journal.getPostAtDate()));
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(period);
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(lineId);
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(VALUE_LLDGCODE);
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(progressFinanceAdapter.translateAccounts(journal.getCompany().getCompanyCode(), journal.getAccount(), journal.getSubAccount(), journal.getCostCentre())); // use
																							    // translate
																							    // finance
																							    // codes
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(DF.format(journal.getAmount()));
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(journal.getProject());
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(journal.getNarration());
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(DUMMY_DEBTOR_CODE_ROADSIDE);
		fileBuffer.append(FileUtil.SEPARATOR_COMMA);
		fileBuffer.append(BATCH_IDENTIFIER);
		fileBuffer.append(FileUtil.NEW_LINE);

		try 
		{
		    FileUtil.writeFile(fileName, fileBuffer.toString());
		} 
		catch (IOException ex) 
		{
		    throw new RemoteException("Unable to transfer journals to Finance One.", ex);
		}

	    }
	} catch (RemoteException re) {
	    throw new FinanceException("Unable to transfer journals.", re);
	} finally {
	    progressFinanceAdapter.release();
	}

	return new FinanceResponse(new Integer(journalNumber), "");// no
								   // response
    }

    public String getSystemName() {
	return FinanceAdapter.FINANCE_SYSTEM_FINANCE_ONE;
    }

}
