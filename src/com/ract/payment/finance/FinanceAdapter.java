/*
 * @(#)FinanceAdapter.java
 *
 */

package com.ract.payment.finance;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Vector;

import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.util.DateTime;

/**
 * The finance adapter interface.
 * 
 * @author Glenn Lewis, jyh
 * @version 1.0, 20/5/2002
 * 
 */
public interface FinanceAdapter extends TransactionAdapter {

    public static String FINANCE_SYSTEM_MFGPRO = "MFG/PRO";

    public static String FINANCE_SYSTEM_FINANCE_ONE = "Finance One";

    /**
     * Transfer journal entries to the finance system.
     * 
     * @param journals
     *            a vector of journal entries to be transferred to the finance
     *            system
     * @param sourceSystem
     *            the source system that the journals are from
     * @returns FinanceResponse
     */
    public FinanceResponse transferJournals(Vector journals, SourceSystem sourceSystem) throws FinanceException;// ,
														// ValidationException;

    /**
     * Transfer journal entries to the finance system.
     * 
     * @param journals
     *            a vector of journal entries to be transferred to the finance
     *            system
     * @param sourceSystem
     *            the source system that the journals are from
     * @param debug
     *            boolean include debugging information
     * @throws FinanceException
     * @return FinanceResponse
     */
    public FinanceResponse transferJournals(Vector journals, SourceSystem sourceSystem, boolean debug) throws FinanceException;// ,
															       // ValidationException;

    public void createVoucher(FinancePayee payee, Cheque cheque) throws FinanceException;

    /**
     * Commit any pending transactions in the finance sub system. The connection
     * to the finance sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void commit() throws SystemException;

    /**
     * Rollback any pending transactions in the finance sub system. The
     * connection to the finance sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws SystemException;

    /**
     * Release any connections held to the finance sub-system. If a transaction
     * is pending and not committed then the transaction will be rolled back.
     */
    public void release();

    /**
     * Retrieve a payee record from the financial system given the Returns the
     * data as a PayeeVO record, filling as many of the fields for which it can
     * find data. payee number
     * 
     * @param String
     *            payeeNo The payee identifier
     * @return PayeeVO The required payee record
     * @throws SystemException
     */
    public FinancePayee getPayee(String payeeNo) throws SystemException;

    /**
     * Retrieve a payment(refund) voucher record and a cheque record from the
     * financial system given the voucher number. If the voucher has not been
     * loaded the returned cheque vo will be empty. If the cheque has not been
     * created, no cheque details will be supplied
     * 
     * @param String
     *            voucherNo The voucher identifier
     * @return ChequeVO The cheque object
     * @throws SystemException
     */
    public Cheque getCheque(String voucherNo) throws SystemException;

    /**
     * Create Ad-hoc journal in the finance system It is required to give the
     * names of the ledger pair as can be resolved by the finance system.
     * 
     * @param postAtDate
     *            The effective date
     * @param amount
     *            The amounnt
     * @param drAccountName
     *            The debit account
     * @param crAccountName
     *            The credit account
     * @param subSystem
     *            The sub system
     * @throws FinanceException
     */
    // public void createJournal(DateTime postAtDate,
    // BigDecimal amount,
    // String drAccountName,
    // String crAccountName,
    // String subSystem)
    // throws FinanceException;

    /**
     * Create miscellaneous sale record in MFGPro
     * 
     * @param debtorCode
     *            The dummy client NO.
     * @param invoiceNumber
     *            The invoice number allocated by the receipting system
     * @param salesBranch
     *            The sales branch where raised
     * @param postAtDate
     *            The date the transaction was posted
     * @param effDate
     *            The effective date of the transaction
     * @param gst
     *            Whether or not GST was applicable
     * @param totalAmount
     *            The total value of the sale including GST
     * @param receiptNumber
     *            The receipt number issued by the receipting system
     * @param saleLines
     *            A collection of LineItem objects representing the details of
     *            the sale.
     * @throws SystemException
     */
    public void createCustomerOrder(String debtorCode, String invoiceNumber, String salesBranch, DateTime postAtDate, DateTime effDate, boolean gst, BigDecimal totalAmount, String receiptNumber, Collection saleLines) throws SystemException;

    public void updateDebtor(Debtor debtor) throws FinanceException;

    /**
     * Interrogate the finance system to see if a customer code is known to that
     * system
     * 
     * @param customerCode
     *            The customer identifier
     * @return True if already known to finance system
     * @throws FinanceException
     */
    public boolean validateCustomer(String customerCode) throws FinanceException;

    /**
     * Check the finance system for a particular stock item. Returns the
     * quantity on hand for a given profit centre. Returns null if stock item is
     * not found.
     * 
     * @param stockItem
     *            The stock item to validate
     * @param profitCentre
     *            The profit centre to check for stock
     * @return Quantity on hand
     * @throws FinanceException
     */
    public BigDecimal validateStockItem(String stockItem, String profitCentre) throws FinanceException;

    /**
     * Get a stock item from the finance system.
     * 
     * @param stockItemCode
     *            The stock identifier
     * @param profitCentre
     *            The profit centre to retrieve the stock from
     * @throws FinanceException
     * @return StockItem
     */
    public StockItem getStockItem(String stockItemCode, String profitCentre) throws FinanceException;

    /**
     * Obtains a list of valid subsystems.
     * 
     * @return collection of valid subsystems
     * @throws FinanceException
     * @throws ValidationException
     */
    public Collection getSubsystemList() throws FinanceException, ValidationException;

    /**
     * Validate a general ledger code, sub account and cost centre combination.
     * 
     * @param accountCode
     *            The account number
     * @param subAccount
     *            The sub account number
     * @param costCentre
     *            The cost centre
     * @throws FinanceException
     * @return boolean Whether the general ledger code is valid or not.
     */
    public boolean validateGLCode(String accountCode, String subAccount, String costCentre) throws FinanceException;

    /**
     * Validate an invoice number.
     * 
     * @param invoiceNumber
     *            The invoice number
     * @throws FinanceException
     * @return boolean Whether of not the invoice number is valid.
     */
    public boolean validateInvoiceNumber(String invoiceNumber) throws FinanceException;

    /**
     * Gets a list of general ledger transactions.
     * 
     * @param effectiveStartDate
     *            the effective start date
     * @param effectiveEndDate
     *            the effective end date
     * @param enteredStartDate
     *            the entered start date
     * @param enteredEndDate
     *            the entered end date
     * @param subSystem
     *            the Subsystem is identified by a four character string eg.
     *            '5400'.
     * @param glAccount
     *            the GL account may have 'ALL' entered for all accounts or
     *            specify a particular GL account.
     * @return a collection of general ledger transactions
     * @throws FinanceException
     */
    public Collection getGLTransactions(DateTime effectiveStartDate, DateTime effectiveEndDate, DateTime enteredStartDate, DateTime enteredEndDate, String subSystem, String glAccount, String entity) throws FinanceException, ValidationException;

}
