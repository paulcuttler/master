package com.ract.payment.finance;

import java.io.Serializable;

/**
 * <p>
 * Represent a subsystem. This is a finance system concept.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class Subsystem implements Serializable {
    private String subsystemTranslation;
    private String comments;
    private String sourceSystem;

    public Subsystem(String srcSystem, String subsysTransaction, String comm) {
	this.setSourceSystem(srcSystem);
	this.setSubsystemTranslation(subsysTransaction);
	this.setComments(comm);
    }

    public String getSourceSystem() {
	return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public String getSubsystemTranslation() {
	return subsystemTranslation;
    }

    public void setSubsystemTranslation(String subsystemTranslation) {
	this.subsystemTranslation = subsystemTranslation;
    }

    public String getComments() {
	return comments;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

    @Override
    public String toString() {
	return "Subsystem [comments=" + comments + ", sourceSystem=" + sourceSystem + ", subsystemTranslation=" + subsystemTranslation + "]";
    }

}
