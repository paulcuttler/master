package com.ract.payment.finance;

/**
 * Structured response from A finance system. Using MFG-Pro it contains the
 * journal number and the MFG-Pro system number (or screen in MFG-Pro).
 * 
 * @author jyh
 * @version 8/11/2002
 */

public class FinanceResponse {
    public FinanceResponse(Integer newJournalNumber, String newSystemReference) {
	this.setJournalNumber(newJournalNumber);
	this.setSystemReference(newSystemReference);
    }

    private Integer journalNumber;

    private String systemReference;

    public Integer getJournalNumber() {
	return journalNumber;
    }

    public void setJournalNumber(Integer journalNumber) {
	this.journalNumber = journalNumber;
    }

    public void setSystemReference(String systemReference) {
	this.systemReference = systemReference;
    }

    public String getSystemReference() {
	return systemReference;
    }

    public String toString() {
	StringBuffer sb = new StringBuffer();
	if (this.journalNumber != null) {
	    sb.append("Journal Number:   " + this.journalNumber.toString());
	}
	if (this.systemReference != null) {
	    sb.append(", ");
	    sb.append("System Reference: " + this.systemReference);
	}
	return sb.toString();
    }

}
