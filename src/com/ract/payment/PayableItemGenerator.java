package com.ract.payment;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.ract.common.RollBackException;

/**
 * Description: Used to tag a class as being able to produce PayableitemVOs. For
 * example, some of the sub class of the membership TransactionGroup class can
 * generate payable items specific to the transaction being performed.
 * Copyright: Copyright (c) 2002 Company: RACT
 * 
 * @author
 * @version 1.0
 */

public interface PayableItemGenerator {
    /**
     * Generate a list of payable items that need to be sent to the payment
     * manager.
     */
    public ArrayList generatePayableItemList() throws RemoteException, RollBackException;
}
