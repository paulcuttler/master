package com.ract.payment.electronicpayment;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.Remote;

@Remote
public interface EftPaymentMgr {
    public void completEft(EftPayment payment) throws RemoteException;

    public EftPayment getLastTransaction(String tillId) throws RemoteException;

    public EftPayment startEft(EftPayment payment) throws RemoteException;

    public String getPrinter(String tillId) throws RemoteException;

    public EftPayment getTransaction(String txnRef) throws RemoteException;

    public void updateClientEFTPayments(Integer fromClient, Integer toClient) throws RemoteException;

    public Collection<EftPayment> getClientEFTPayments(Integer clientNumber) throws RemoteException;
}
