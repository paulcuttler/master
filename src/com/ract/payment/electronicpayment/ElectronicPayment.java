package com.ract.payment.electronicpayment;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.ParseException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.GenericException;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright:
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Leigh Giles
 * @version 1.0
 */

public class ElectronicPayment implements Serializable {

    private StringBuffer status = new StringBuffer();

    private ElectronicPaymentOperator epo;

    private String customerReferenceNumber;

    public String inputData;

    private DateTime dateOfPayment;

    private String transactionReferenceNumber;

    private DateTime dateReceived;

    private SourceSystem sourceSystem;

    private String clientNumber;

    private String seqNumber;

    private double amount;

    private String ractReceiptNumber;

    private String cardNumber;

    private String referenceId;

    private String payableDescription;

    private double newAmountOutStanding;

    private double originalAmountOutStanding;

    private boolean receptorPurchaseRequiresUncomplete;

    private SourceSystem payableSourceSystem;

    public String getLocation() {
	return epo.getLocation();
    }

    public void setTransactionReferenceNumber(String transactionReferenceNumber) {
	this.transactionReferenceNumber = transactionReferenceNumber;
    }

    public void setInputData(String inputData) {
	this.inputData = inputData;
    }

    public void setCustomerReferenceNumber(String customerReferenceNumber) {
	this.customerReferenceNumber = customerReferenceNumber;
    }

    public void setAmount(double amount) {
	this.amount = amount;
    }

    public void setDateOfPayment(DateTime dateOfPayment) {
	this.dateOfPayment = dateOfPayment;
    }

    public void setDateReceived(DateTime dateReceived) {
	this.dateReceived = dateReceived;
    }

    public void setSourceSystem(SourceSystem sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public void setClientNumber(String clientNumber) {
	this.clientNumber = clientNumber;
    }

    public void setSeqNumber(String seqNumber) {
	this.seqNumber = seqNumber;
    }

    public void setRactReceiptNumber(String ractReceiptNumber) {
	this.ractReceiptNumber = ractReceiptNumber;
    }

    public void setCardNumber(String cardNumber) {
	this.cardNumber = cardNumber;
    }

    public void setReferenceId(String referenceId) {
	this.referenceId = referenceId;
    }

    public String getUserId() {
	return epo.getUserid();
    }

    public String getBranch() {
	return epo.getBranch();
    }

    public String getTransactionReferenceNumber() {
	return transactionReferenceNumber;
    }

    public String getInputData() {
	return inputData;
    }

    public String getCustomerReferenceNumber() {
	return customerReferenceNumber;
    }

    public double getAmount() {
	return amount;
    }

    public String getPaytype() {
	return epo.getPayType();
    }

    public String getTill() {
	return epo.getTill();
    }

    public DateTime getDateOfPayment() {
	return dateOfPayment;
    }

    public DateTime getDateReceived() {
	return dateReceived;
    }

    public SourceSystem getSourceSystem() {
	return sourceSystem;
    }

    public String getClientNumber() {
	return clientNumber;
    }

    public Integer getClientNumberInteger() {
	return new Integer(clientNumber);
    }

    public String getSeqNumber() {
	return seqNumber;
    }

    public String getRactReceiptNumber() {
	return StringUtil.convertNull(ractReceiptNumber);
    }

    public String getCardNumber() {
	return StringUtil.convertNull(cardNumber);
    }

    public String getReferenceId() {
	return referenceId;
    }

    /**
     * Add to the current processing status
     */
    public void addStatus(String stat) {
	if (!StringUtil.isNull(stat)) {
	    this.status.append(stat);
	    this.status.append(":");
	}
    }

    public String getStatus() {
	return status.toString();
    }

    public String getPayableDescription() {
	return payableDescription;
    }

    public void setPayableDescription(String payableDescription) {
	this.payableDescription = payableDescription;
    }

    public double getNewAmountOutStanding() {
	return newAmountOutStanding;
    }

    public void setNewAmountOutStanding(double newAmountOutStanding) {
	this.newAmountOutStanding = newAmountOutStanding;
    }

    public boolean isReceptorPurchaseRequiresUncomplete() {
	return receptorPurchaseRequiresUncomplete;
    }

    public void setReceptorPurchaseRequiresUncomplete(boolean receptorPurchaseRequiresUncomplete) {
	this.receptorPurchaseRequiresUncomplete = receptorPurchaseRequiresUncomplete;
    }

    public SourceSystem getPayableSourceSystem() {
	return payableSourceSystem;
    }

    public void setPayableSourceSystem(SourceSystem payableSourceSystem) {
	this.payableSourceSystem = payableSourceSystem;
    }

    public double getOriginalAmountOutStanding() {
	return originalAmountOutStanding;
    }

    public void setOriginalAmountOutStanding(double originalAmountOutStanding) {
	this.originalAmountOutStanding = originalAmountOutStanding;
    }

    public boolean isOK() {
	return (StringUtil.isNull(this.status.toString()) || this.status.toString().equalsIgnoreCase("null:")) ? true : false;
    }

    public String toString() {
	StringBuffer s = new StringBuffer();
	try {
	    s.append(StringUtil.makeSpaces(this.customerReferenceNumber) + ",");
	    s.append(this.amount + ",");
	    s.append(this.dateOfPayment + ",");
	    s.append(StringUtil.makeSpaces(this.getTransactionReferenceNumber()) + ",");
	    s.append(StringUtil.makeSpaces(this.cardNumber) + ",");
	    s.append(",");
	    s.append(this.dateOfPayment + ",");
	    s.append(StringUtil.makeSpaces(this.sourceSystem.getAbbreviation()) + ",");
	    s.append(this.dateReceived + ",");
	    s.append(StringUtil.makeSpaces(this.clientNumber) + ",");
	    s.append(StringUtil.makeSpaces(this.seqNumber) + ",");
	    s.append(StringUtil.makeSpaces(this.ractReceiptNumber) + ",");
	    s.append((getStatus() == null ? "null:" : StringUtil.replace(getStatus(), ":", "")));
	    // s.append((status == null ? "null:" : status.toString()));
	} catch (Exception ex) {
	    LogUtil.log(this.getClass(), "Electronic payment toString " + ex.getMessage());
	}
	return s.toString();
    }

    /******************************************************************
     * Receipt this payment
     ******************************************************************/
    public void receiptPayment(PaymentTransactionMgrLocal payTxMgrLocal) throws RemoteException, RollBackException {
	LogUtil.log(this.getClass(), "RLG +++++ receipting payment " + this.getCustomerReferenceNumber());
	String receiptNumber = payTxMgrLocal.receiptElectronicPayment((ElectronicPayment) this);

	this.setRactReceiptNumber(receiptNumber);
    }

    public void setElectronicOperatorDetails(String operator) {
	// System.out.println("setElectronicOperatorDetails"+epo);
	try {
	    epo = new ElectronicPaymentOperator(operator);
	} catch (RemoteException ex) {
	    // ex.printStackTrace();
	}
	// System.out.println("setElectronicOperatorDetails"+epo);
    }

    public String lineAsIs() {
	return inputData;
    }

    public String toXML() {
	StringBuffer s = new StringBuffer();

	s.append("<ElectronicPayment>");
	s.append("<Status>" + status + "</Status>");
	s.append("<CustomerReferenceNumber>" + customerReferenceNumber + "</CustomerReferenceNumber>");
	s.append("<DatePaid>" + dateOfPayment + "</DatePaid>");
	s.append("<TransactionReference>" + transactionReferenceNumber + "</TransactionReference>");
	s.append("<Amount>" + amount + "</Amount>");
	s.append("<DateReceived>" + dateReceived + "</DateReceived>");
	s.append("<Client>" + clientNumber + "</Client>");
	s.append("<Sequence>" + seqNumber + "</Sequence>");
	s.append("<RactReceiptNumber>" + (ractReceiptNumber == null ? "unknown" : ractReceiptNumber) + "</RactReceiptNumber>");
	s.append("<SourceSystem>" + (sourceSystem == null ? "UNK" : sourceSystem.getAbbreviation()) + "</SourceSystem>");
	s.append("<InputData>" + inputData + "</InputData>");
	if (epo == null) {
	    epo = new ElectronicPaymentOperator();
	}
	s.append((epo.toXML()));

	s.append("</ElectronicPayment>");

	return s.toString();
    }

    protected void setElectronicPaymentDetails(SourceSystem ss, String epOperator) {
	this.amount = 0.00;
	this.cardNumber = "";
	this.clientNumber = "";
	this.seqNumber = "";
	this.customerReferenceNumber = "";
	this.dateOfPayment = new DateTime();
	this.dateReceived = new DateTime();
	try {
	    this.epo = new ElectronicPaymentOperator(epOperator);
	} catch (RemoteException ex) {
	}
	this.ractReceiptNumber = "";
	this.sourceSystem = ss;
	this.inputData = "";

    }

    public void notifySourceSystemOfPayment() throws RemoteException

    {
	// to be overridden - consider use of abstract class with abstract
	// methods.
    }

    /*
     * public void setPaymentSystem(String paymentSystem) { //
     * this.paymentSystem = paymentSystem; }
     */

    public ElectronicPayment() {
	this.setReceptorPurchaseRequiresUncomplete(false);
	this.setPayableDescription("");
	this.setNewAmountOutStanding(0.0);

	this.status.append("");
    }

    public ElectronicPayment(SourceSystem ss, String epOperator) {
	this.setReceptorPurchaseRequiresUncomplete(false);
	this.setPayableDescription("");
	this.setNewAmountOutStanding(0.0);

	setElectronicPaymentDetails(ss, epOperator);
	this.status.append("");
    }

    public ElectronicPayment(Node epNode) throws SystemException {

	if (epNode == null /*
			    * ||
			    * !epNode.getNodeName().equals(WRITABLE_CLASSNAME)
			    */) {
	    throw new SystemException("Failed to create ElectronicPayment from XML node. The node is not valid.");
	}
	// System.out.println("ElectronicPayment.");
	// Initialise the lists

	NodeList elementList = epNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    LogUtil.debug(this.getClass(), "Loading node 2.");
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("Status".equals(nodeName) && valueText != null) {
		this.status = new StringBuffer(valueText);
	    } else if ("CustomerReferenceNumber".equals(nodeName) && valueText != null) {
		this.customerReferenceNumber = valueText;
	    } else if ("DatePaid".equals(nodeName) && valueText != null) {
		try {
		    this.dateOfPayment = new DateTime(valueText);
		} catch (ParseException ex) {
		    // ignore
		}
	    } else if ("TransactionReference".equals(nodeName) && valueText != null) {
		this.transactionReferenceNumber = valueText;
	    } else if ("Amount".equals(nodeName) && valueText != null) {
		this.amount = Double.parseDouble(valueText);
	    } else if ("DateReceived".equals(nodeName) && valueText != null) {
		try {
		    this.dateReceived = new DateTime(valueText);
		} catch (ParseException ex1) {
		    // ignore
		}
	    } else if ("Client".equals(nodeName) && valueText != null) {
		this.clientNumber = valueText;
	    } else if ("Sequence".equals(nodeName) && valueText != null) {
		this.seqNumber = valueText;
	    } else if ("RactReceiptNumber".equals(nodeName) && valueText != null) {
		this.ractReceiptNumber = valueText;
	    } else if ("SourceSystem".equals(nodeName) && valueText != null) {
		try {
		    this.sourceSystem = SourceSystem.getSourceSystem(valueText);
		} catch (GenericException ex2) {
		    // ignore
		}
	    } else if ("InputData".equals(nodeName) && valueText != null) {
		// System.out.println("####InputData");
		// System.out.println("####value"+valueText);
		this.inputData = valueText;
	    }
	    // System.out.println("nodeName="+nodeName);
	    // System.out.println("valueText="+valueText);
	}
	// hard code due to an error in the xml generation
	this.setElectronicOperatorDetails("IVR");
	// System.out.println("epo="+this.epo);
    }

    public static void main(String[] args) {
	ElectronicPayment electronicPayment1 = new ElectronicPayment();
    }

}
