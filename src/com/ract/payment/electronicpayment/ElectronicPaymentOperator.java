package com.ract.payment.electronicpayment;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;
import com.ract.util.StringUtil;

public class ElectronicPaymentOperator implements Serializable {
    private String userid;

    private String branch;

    private String location;

    private String till;

    private String payType;

    private CommonMgr commonMgr = null;

    public String getUserid() {
	return userid;
    }

    public String getPayType() {
	return payType;
    }

    public String getTill() {
	return till;
    }

    public String getLocation() {
	return location;
    }

    public String getBranch() {
	return branch;
    }

    public ElectronicPaymentOperator() {
    }

    public ElectronicPaymentOperator(String operator) throws RemoteException {
	// System.out.println("ElectronicPaymentOperator");
	try {
	    commonMgr = CommonEJBHelper.getCommonMgr();

	    String opr = operator;
	    //
	    // The operator may come through as INTERNET or IVR, use this to
	    // determine the actual operator from the
	    // electronic payment details table in gn-table.
	    //
	    try {

		opr = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, operator);
		if (opr != null) {
		    opr = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, opr);
		} else {
		    opr = operator;
		}
	    } catch (Exception e) {
		opr = operator;
	    }

	    if (opr.equalsIgnoreCase("bpay")) {
		getBPAYDetails();
	    } else if (opr.equalsIgnoreCase("ivr")) {
		getIVRDetails();
	    } else if (opr.equalsIgnoreCase("web")) {
		getWEBDetails();
	    } else {
		this.userid = "unknown";
		this.branch = "unknown";
		this.location = "unknown";
		this.till = "unknown";
		this.payType = "unknown";
	    }
	} catch (RemoteException ex) {
	    throw new RemoteException("ElectronicPaymentOperator " + ex.getMessage());
	}
	// System.out.println("user="+userid);
	// System.out.println("branch"+branch);
    }

    private void getBPAYDetails() throws RemoteException {
	this.userid = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.BPAY_USER);
	this.branch = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.BPAY_BRANCH);
	this.location = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.BPAY_LOCATION);
	this.till = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.BPAY_TILL);
	this.payType = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.BPAY_PAYTYPE);
    }

    private void getIVRDetails() throws RemoteException {
	this.userid = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.IVR_USER);
	this.branch = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.IVR_BRANCH);
	this.location = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.IVR_LOCATION);
	this.till = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.IVR_TILL);
	this.payType = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.IVR_PAYTYPE);
    }

    private void getWEBDetails() throws RemoteException {
	this.userid = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.WEB_USER);
	this.branch = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.WEB_BRANCH);
	this.location = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.WEB_LOCATION);
	this.till = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.WEB_TILL);
	this.payType = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.WEB_PAYTYPE);
    }

    public String toXML() {
	StringBuffer s = new StringBuffer();

	s.append("<ElectronicPaymentOperator>");
	s.append("<UserId>" + StringUtil.makeSpaces(userid) + "</UserId>");
	// s.append("<Location>" + StringUtil.makeSpaces(location) +
	// "</Location>");
	// s.append("<Branch>" + StringUtil.makeSpaces(branch) + "</Branch>");
	// s.append("<Till>" + StringUtil.makeSpaces(till) + "</Till>");
	// s.append("<PayType>" + StringUtil.makeSpaces(payType) +
	// "</PayType>");
	s.append("</ElectronicPaymentOperator>");

	return s.toString();
    }

}
