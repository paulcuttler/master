package com.ract.payment.electronicpayment;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SequenceMgr;
import com.ract.util.ConnectionUtil;

@Stateless
@Remote({ EftPaymentMgr.class })
public class EftPaymentMgrBean {

    @PersistenceContext(unitName = "master")
    EntityManager em;

    @Resource
    private SessionContext sessionContext;

    @Resource(mappedName = "java:/ClientDS")
    private DataSource dataSource;

    /**
     * create new eft payment record in the database using the (incomplete) data
     * provided. Generate a new transaction reference for this transaction
     * 
     * @param payment
     *            EftPaymentVO
     * @return String Return transaction reference for this transaction
     */
    public EftPayment startEft(EftPayment payment) throws RemoteException {
	// previously used a max(id) method of getting and id which is prone to
	// duplicates where
	// the select is not performed in a separate transaction.
	int noRowsUpdated = 0;
	SequenceMgr sequenceMgr = CommonEJBHelper.getSequenceMgr();
	int txnRef = sequenceMgr.getNextID(SequenceMgr.SEQUENCE_EFT_TRANSACTION); // autounboxed
	if (txnRef == 0) {
	    throw new RemoteException("Unable to obtain the next EFT transaction reference.");
	}
	payment.setTxnRef(txnRef);
	payment.setFormattedTxnRef(padNumber(txnRef, 16));
	em.persist(payment);

	return payment;
    }

    public Collection<EftPayment> getClientEFTPayments(Integer clientNumber) throws RemoteException {
	Query query = em.createQuery("select e from EftPayment e where e.clientNo = ?1 order by e.txnRef desc");
	query.setParameter(1, clientNumber);
	List<EftPayment> transactions = query.getResultList();
	return transactions;
    }

    /**
     * Update eft payments from one client to another
     * 
     * @param fromClient
     * @param toClient
     * @throws RemoteException
     */
    public void updateClientEFTPayments(Integer fromClient, Integer toClient) throws RemoteException {
	EftPayment payment = null;
	Collection<EftPayment> payments = getClientEFTPayments(fromClient);
	if (payments != null && payments.size() > 0) {
	    for (Iterator<EftPayment> i = payments.iterator(); i.hasNext();) {
		payment = i.next();
		payment.setClientNo(toClient);
		updateEftPayment(payment);
	    }
	}

    }

    public EftPayment getLastTransaction(String tillId) throws RemoteException {
	EftPayment lastTrans = null;
	Query query = em.createQuery("select e from EftPayment e where e.tillId = ?1 order by e.txnRef desc");
	query.setParameter(1, tillId);
	List<EftPayment> transactions = query.getResultList();
	if (transactions != null && transactions.size() > 0) {
	    lastTrans = transactions.iterator().next();
	}
	return lastTrans;
    }

    public EftPayment getTransaction(String txnRef) throws RemoteException {
	return em.find(EftPayment.class, new Integer(txnRef));
    }

    public void completEft(EftPayment payment) throws RemoteException {
	updateEftPayment(payment);
    }

    public void updateEftPayment(EftPayment payment) throws RemoteException {
	em.merge(payment);
    }

    // TODO generic
    private String padNumber(int num, int digits) {
	String temp = "" + num;
	for (int x = temp.length(); x < digits; x++) {
	    temp = "0" + temp;
	}
	return temp;
    }

    // TODO relies on Receptor table structures
    public String getPrinter(String tillId) throws RemoteException {
	String sql = "SELECT daltonc.JALIFE_PRINTERS.NAME" + " FROM   daltonc.JALIFE_TILL_PRINTERS INNER JOIN " + "        daltonc.JALIFE_TILLS ON daltonc.JALIFE_TILL_PRINTERS.TILL_ID = daltonc.JALIFE_TILLS.TILL_ID INNER JOIN" + "        daltonc.JALIFE_PRINTERS ON daltonc.JALIFE_TILL_PRINTERS.PRINTER_ID = daltonc.JALIFE_PRINTERS.PRINTER_ID" + " WHERE     (daltonc.JALIFE_TILLS.NAME = ?)";
	Connection connection = null;
	PreparedStatement statement = null;
	String printerName = null;
	try {
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, tillId);
	    ResultSet rs = statement.executeQuery();
	    if (rs.next()) {
		printerName = rs.getString(1);
	    } else {
		throw new RemoteException("Printer not set for " + tillId);
	    }
	    rs.close();
	} catch (SQLException sqle) {
	    throw new RemoteException("Error obtaining printer name " + sqle);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	    return printerName;
	}
    }

}
