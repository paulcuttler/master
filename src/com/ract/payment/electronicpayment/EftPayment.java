package com.ract.payment.electronicpayment;

/**
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * @author not attributable
 * @version 1.0
 */
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ract.common.ValueObject;
import com.ract.util.DateTime;

@Entity
@Table(name = "EftTransaction")
public class EftPayment extends ValueObject {
    public String getFormattedTxnRef() {
	return formattedTxnRef;
    }

    public void setFormattedTxnRef(String formattedTxnRef) {
	this.formattedTxnRef = formattedTxnRef;
    }

    public Integer getTxnRef() {
	return txnRef;
    }

    public void setTxnRef(Integer txnRef) {
	this.txnRef = txnRef;
    }

    public String getTillId() {
	return tillId;
    }

    public void setTillId(String tillId) {
	this.tillId = tillId;
    }

    public String getLogonId() {
	return logonId;
    }

    public void setLogonId(String logonId) {
	this.logonId = logonId;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public DateTime getCompleteDate() {
	return completeDate;
    }

    public void setCompleteDate(DateTime completeDate) {
	this.completeDate = completeDate;
    }

    public String getResultCode() {
	return resultCode;
    }

    public void setResultCode(String resultCode) {
	this.resultCode = resultCode;
    }

    public String getResultText() {
	return resultText;
    }

    public void setResultText(String resultText) {
	this.resultText = resultText;
    }

    public BigDecimal getPurchaseAmt() {
	return purchaseAmt;
    }

    public void setPurchaseAmt(BigDecimal purchaseAmt) {
	this.purchaseAmt = purchaseAmt;
    }

    public BigDecimal getRefundAmt() {
	return refundAmt;
    }

    public void setRefundAmt(BigDecimal refundAmt) {
	this.refundAmt = refundAmt;
    }

    public Integer getClientNo() {
	return clientNo;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    public String getCardNo() {
	return cardNo;
    }

    public void setCardNo(String cardNo) {
	this.cardNo = cardNo;
    }

    public String getExpDate() {
	return expDate;
    }

    public void setExpDate(String expDate) {
	this.expDate = expDate;
    }

    public String getCardType() {
	return cardType;
    }

    public void setCardType(String cardType) {
	this.cardType = cardType;
    }

    public String getTransType() {
	return transType;
    }

    public void setTransType(String transType) {
	this.transType = transType;
    }

    public Boolean getSuccess() {
	return success;
    }

    public void setSuccess(Boolean success) {
	this.success = success;
    }

    @Id
    private Integer txnRef;
    private String tillId;
    private String logonId;
    private DateTime createDate;
    private DateTime completeDate;
    private String resultCode;
    private String resultText;
    private BigDecimal purchaseAmt;
    private BigDecimal refundAmt;
    private Integer clientNo;
    private String cardNo;
    private String expDate;
    private String cardType;
    private String transType;
    private Boolean success;

    @Transient
    private String formattedTxnRef;

    public EftPayment() {
    }

    // public static void main(String[] args)
    // {
    // EftPayment eftPaymentVO1 = new EftPayment();
    // }
    public String toString() {
	return "\n   txnRef               " + txnRef + "\n   tillId               " + tillId + "\n   logonId              " + logonId + "\n   createDate           " + createDate + "\n   completeDate         " + completeDate + "\n   resultCode           " + resultCode + "\n   resultText           " + resultText + "\n   purchaseAmt          " + (purchaseAmt == null ? 0 : purchaseAmt.doubleValue()) + "\n   refundAmt            " + (refundAmt == null ? 0 : refundAmt.doubleValue()) + "\n   clientNo             " + clientNo + "\n   cardNo               " + cardNo + "\n   expDate              " + expDate + "\n   cardType             " + cardType + "\n   transType            " + transType + "\n   success              " + success;
    }

}
