package com.ract.payment.electronicpayment;

import java.rmi.RemoteException;
import java.util.Hashtable;

import com.ract.common.CheckDigit;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;

/**
 * Title: BillpayHelper Description: Holds static methods for billpay functions
 * Copyright: Copyright (c) 2002 Company: RACT
 * 
 * @author dgk 17/12/2002
 * @version 1.0
 */

public class ElectronicPaymentHelper {
    private static Hashtable prefixCache = new Hashtable();

    /**
     * Create an ivrNumber as an eleven digit string in the format prefix + 0
     * filler + number + checkdigit
     * 
     * @param int number The number for which the IVRnumber string is required
     * @param SourceSystem
     *            sourceSystem The source system using the number
     * @return String The formatted ivr number.
     */
    public static String createIvrNumber(int number, SourceSystem sourceSystem) {
	String ivrPrefix = null;
	// only fetch the prefix the first time it is required
	if (prefixCache.containsKey(sourceSystem.getAbbreviation())) {
	    ivrPrefix = (String) prefixCache.get(sourceSystem.getAbbreviation());
	} else {
	    try {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		SystemParameterVO sysVo = commonMgr.getSystemParameter(sourceSystem.getAbbreviation(), "BPPFX");
		ivrPrefix = sysVo.getNumericValue1().toString();
	    } catch (Exception e) {
		ivrPrefix = "9";
	    }
	    prefixCache.put(sourceSystem.getAbbreviation(), ivrPrefix);
	}

	String temp = number + "";
	for (int x = temp.length(); x < 9; x++) {
	    temp = "0" + temp;
	}
	temp = ivrPrefix + temp;
	temp += "" + CheckDigit.getCheckDigit(temp, false);
	return temp;
    }

    public static String getPhonePayNumber(Integer clientNumber, Integer sequenceNumber) {
	return getPhonePayNumber(clientNumber, sequenceNumber, false);
    }

    /**
     * Construct a phone payment number as follows:
     * 
     * ccccccssssd
     * 
     * cccccc client number ssss oi-payable sequence number d check digit using
     * the double-add-double (IBM) check
     */
    public static String getPhonePayNumber(Integer clientNumber, Integer sequenceNumber, boolean dash) {

	final String DASH = "-";

	StringBuffer phonePayNumber = new StringBuffer();

	String cln_str = clientNumber.toString();
	String sln_str = sequenceNumber.toString();

	String tmp_str = "000000".substring(cln_str.length()) + cln_str + "0000".substring(sln_str.length()) + sln_str;

	Integer checkDigit = CheckDigit.getCheckDigit(tmp_str, false);

	phonePayNumber.append(tmp_str);
	if (dash) {
	    phonePayNumber.append(DASH);
	}
	phonePayNumber.append(checkDigit.toString());

	return phonePayNumber.toString();
    }

    public static Integer getDocumentIDfromIVRNumber(String ivrNumber) {
	Integer documentID = null;
	if (ivrNumber != null && ivrNumber.length() == 11) {
	    documentID = new Integer(ivrNumber.substring(4, 10));
	}
	return documentID;
    }

    public static boolean isElectronicPaymentUser(String userId) {

	try {
	    ElectronicPaymentOperator epo = new ElectronicPaymentOperator(userId);

	    if (epo.getUserid().equalsIgnoreCase("unknown")) {
		return false;
	    } else {
		return true;
	    }
	} catch (RemoteException ex) {
	    return false;
	}
    }
}
