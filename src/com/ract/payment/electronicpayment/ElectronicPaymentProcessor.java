package com.ract.payment.electronicpayment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import com.ract.common.CheckDigit;
import com.ract.common.CommonEJBHelper;
import com.ract.common.ExceptionHelper;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.billpay.BillpayHelper;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.receipting.Payable;
import com.ract.payment.receipting.Receipt;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

public abstract class ElectronicPaymentProcessor {

	public DataSource dataSource;

	protected ElectronicPayment processPayment(ElectronicPayment electronicPayment, boolean receiptPayment, boolean processTransaction) throws RemoteException {
		PaymentTransactionMgrLocal payTxMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
		PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();
		PendingFee pf = null;

		try

		{
			LogUtil.log(this.getClass(), "ELECTRONIC PAYMENT Processing start " + electronicPayment.getCustomerReferenceNumber() + " " + new DateTime());

			pf = getRenewalData(electronicPayment.getCustomerReferenceNumber());

			if (pf == null) {
				electronicPayment = nonPendingFeePayment(electronicPayment, payTxMgrLocal, paymentMgrLocal);
			} else {
				boolean processPayment = allowTransactionByDate(pf.getClientNumber().toString(), electronicPayment.getDateOfPayment());

				if (processPayment) // || (receiptPayment &&
				// !processTransaction))
				{

					electronicPayment = pendingFeePayment(electronicPayment, payTxMgrLocal, pf, paymentMgrLocal);
				} else {
					electronicPayment.addStatus("Transaction after payment date. ");
				}
			}
		} catch (Exception onfe) {
			LogUtil.log(this.getClass(), ExceptionHelper.getExceptionStackTrace(onfe));
			String a = onfe.getMessage();
			if (!(a == null)) {
				int i = a.lastIndexOf(":");
				electronicPayment.addStatus(a.substring(i + 1));
			} else {
				electronicPayment.addStatus("Unknown error " + onfe.toString());
			}
		}

		try {
			if (electronicPayment.isOK()) {
				electronicPayment.addStatus("OK");
				payTxMgrLocal.commit();

				adjustOIPayableAndPendingPurchase(electronicPayment, payTxMgrLocal);
			} else {
				LogUtil.log(this.getClass(), "ELECTRONIC PAYMENT attempting rollback " + electronicPayment.getStatus());
				cancelMembershipDocument(pf, electronicPayment, payTxMgrLocal);
				payTxMgrLocal.rollback();
			}
		} catch (Exception e) {
			electronicPayment.addStatus(e.getMessage());
			payTxMgrLocal.rollback();
		}
		LogUtil.log(this.getClass(), "ELECTRONIC PAYMENT Processing end");
		// go back and get the next one
		return electronicPayment;
	}

	protected ElectronicPayment pendingFeePayment(ElectronicPayment ep, PaymentTransactionMgrLocal payTxMgrLocal, PendingFee pf, PaymentMgrLocal paymentMgrLocal) throws RemoteException, RollBackException {

		ep.setReferenceId(pf.getSeqNumber().toString());
		ep.setClientNumber(pf.getClientNumber().toString());
		ep.setSeqNumber(pf.getOpSequenceNumber().toString());

		if (pf.getDatePaid() == null) {
			try {

				// this method may throw an exception
				ep = processThisPayment(ep, payTxMgrLocal, paymentMgrLocal, getReceiptingSystem(ep.getSourceSystem()));

				if (!updatePendingFee(ep.getCustomerReferenceNumber(), ep.getCardNumber(), ep.getTransactionReferenceNumber(), " ")) {

					ep.addStatus("Pending Fee Update Failed");
				}

			} catch (RemoteException re) {
				LogUtil.log(this.getClass(), re.getMessage());
				ep.addStatus(re.getMessage());
			}

		} else {

			String recNo = getPayableItem(ep.getClientNumber(), ep.getSeqNumber(), ep.getSourceSystem(), paymentMgrLocal).getFirstReceipt().getReceiptNumber();

			LogUtil.log(this.getClass(), "ELECTRONIC PAYMENT +++++ " + pf.getPendingFeeID() + " Already paid " + pf.getDatePaid().formatShortDate() + " Receipt No " + recNo);

			ep.setRactReceiptNumber(recNo);
			ep.addStatus("Already paid " + pf.getDatePaid().formatShortDate());
		}
		LogUtil.log(this.getClass(), "pendingFeePayment end " + ep.getCustomerReferenceNumber());
		return ep;
	}

	public ElectronicPayment nonPendingFeePayment(ElectronicPayment ep, PaymentTransactionMgrLocal payTxMgrLocal, PaymentMgrLocal paymentMgrLocal) throws RemoteException, RollBackException {
		LogUtil.log(this.getClass(), "nonPendingFeePayment start " + ep.getCustomerReferenceNumber());

		if ((ep.getClientNumber() == null) || (ep.getSeqNumber() == null)) {
			throw new RemoteException("ELECTRONIC PAYMENT nonPendingFeePayment No OI-Payable. Client or Sequence No is null");
		}

		try {
			ep = processThisPayment(ep, payTxMgrLocal, paymentMgrLocal, getReceiptingSystem(ep.getSourceSystem()));
		} catch (Exception e) {
			ep.addStatus(e.getMessage());

			throw new RemoteException(e.getMessage());
		}
		LogUtil.log(this.getClass(), "nonPendingFeePayment end " + ep.getCustomerReferenceNumber());
		return ep;

	}

	private ElectronicPayment processThisPayment(ElectronicPayment ep, PaymentTransactionMgrLocal payTxMgrLocal, PaymentMgrLocal paymentMgrLocal, String receiptingSystem) throws RemoteException, RollBackException {
		LogUtil.log(this.getClass(), "processThisPayment start " + ep.getCustomerReferenceNumber());

		double payableAmount;

		Payable payable = getPayableItem(ep.getClientNumber(), ep.getSeqNumber(), ep.getSourceSystem(), paymentMgrLocal);

		payableAmount = payable.getAmountOutstanding().doubleValue();

		LogUtil.log(this.getClass(), "RLG ++++++ " + payableAmount + " client " + ep.getCustomerReferenceNumber());
		if (payableAmount == 0) {

			Receipt receipt = null;
			try {
				receipt = paymentMgrLocal.getReceipt(new Integer(ep.getClientNumber()), ep.getSeqNumber(), ep.getSourceSystem());
			} catch (PaymentException ex1) {
				LogUtil.debug(this.getClass(), ExceptionHelper.getExceptionStackTrace(ex1));
			}

			ep = checkPayableForClaimsPayment(payable, ep);
			ep.setRactReceiptNumber(receipt.getReceiptNumber());
			ep.addStatus("Amount Outstanding is zero");

			throw new RemoteException("Amount Outstanding is zero");

		} else if (isPayablePayable(payable, ep.getAmount())) {
			try {
				payThisPayable(ep, payable, receiptingSystem, payTxMgrLocal);
			} catch (RemoteException e) {
				ep = checkPayableForClaimsPayment(payable, ep);
				throw new RemoteException(e.getMessage());
			} catch (RollBackException e) {
				ep = checkPayableForClaimsPayment(payable, ep);
				throw new RollBackException(e.getMessage());
			}

		} else {

			ep.addStatus("Amount payable (" + payableAmount + ") different from amount received (" + ep.getAmount() + "). Source System is " + ep.getSourceSystem().getAbbreviation());
			ep = checkPayableForClaimsPayment(payable, ep);
			throw new RemoteException(ep.getStatus());
		}
		LogUtil.log(this.getClass(), "processThisPayment end " + ep.getCustomerReferenceNumber());

		ep = checkPayableForClaimsPayment(payable, ep);

		return ep;
	}

	private Payable getPayableItem(String clientNumber, String seqNumber, SourceSystem sourceSystem, PaymentMgrLocal paymentMgrLocal) throws RemoteException, RollBackException

	{

		LogUtil.log(this.getClass(), "ELECTRONIC PAYMENT  getPayableItem " + clientNumber + "/" + seqNumber + "/" + sourceSystem.getAbbreviation());
		Payable payable = null;
		try {
			payable = paymentMgrLocal.getPayable(new Integer(clientNumber), seqNumber, null, sourceSystem);
		} catch (Exception ex) {
			// Payment
			LogUtil.log(this.getClass(), "ELECTRONIC PAYMENT getPayable " + ex.getMessage());
		}

		if (payable == null) {
			throw new SystemException("Unable to get a payable (" + seqNumber + ") for client number '" + clientNumber + "'.");
		}

		return payable;
	}

	private boolean updatePendingFee(String billPayNumber, String creditCard, String bpReceipt, String bankReceipt) {

		boolean updatedOK = true;

		StringBuffer sql = new StringBuffer();
		sql.append("update  pub.pt_pending_fee ");
		sql.append(" set  cc_number = ? , ");
		sql.append("      date_paid = ? , ");
		sql.append("      billpay_rec = ? ,");
		sql.append("      bank_rec = ? ");
		sql.append(" where pending_fee_id = ? ");

		Connection connection = null;
		PreparedStatement sqlStatement = null;

		if (creditCard.equalsIgnoreCase("?")) {
			creditCard = null;
		}
		try {
			connection = dataSource.getConnection();
			sqlStatement = connection.prepareStatement(sql.toString());

			sqlStatement.setString(1, creditCard);
			sqlStatement.setDate(2, new DateTime().toSQLDate());
			sqlStatement.setString(3, bpReceipt);
			sqlStatement.setString(4, bankReceipt);
			sqlStatement.setString(5, billPayNumber);

			if (sqlStatement.executeUpdate() < 1) {
				LogUtil.log(this.getClass(), "pending fee table not updated " + billPayNumber + " cc " + creditCard);
				updatedOK = false;
			}

		} catch (Exception e) {
			LogUtil.log(this.getClass(), "UPDATEPENDINGFEE " + billPayNumber + " cc " + creditCard + " " + e.getMessage());
			updatedOK = false;
		} finally {
			try {
				sqlStatement.close();
				connection.close();
			} catch (Exception ee) {
				LogUtil.log(this.getClass(), "UPDATEPENDINGFEE finally " + billPayNumber + " " + ee.getMessage());
				updatedOK = false;
			}
		}
		return updatedOK;
	}

	public String validateReferenceNumber(String referenceNumber) {
		if (referenceNumber.length() != 11 && referenceNumber.length() != 12) // or
		// 12?
		{
			return "reference number has incorrect length " + referenceNumber.length();
		}

		if (!CheckDigit.validateReferenceNumber(referenceNumber)) {
			return "reference number: check digit is incorrect";
		}

		return "ok";
	}

	private boolean allowTransactionByDate(String clientNo, DateTime transDate) {

		boolean afterDate = true;

		StringBuffer sql = new StringBuffer();
		// why use non standard sql to get a string when a date would suffice?
		sql.append("select ");
		sql.append("      convert(char(12),max(mt.transaction_date),103) ");
		sql.append("from  pub.mem_transaction mt ");
		sql.append("      inner join ");
		sql.append("      pub.mem_membership mm on mm.membership_id = mt.membership_id ");
		sql.append("where mm.client_number = ");
		sql.append(clientNo.trim());

		Connection connection = null;
		PreparedStatement sqlStatement = null;
		ResultSet rs = null;

		try {
			connection = dataSource.getConnection();
			sqlStatement = connection.prepareStatement(sql.toString());

			rs = sqlStatement.executeQuery();

			if (rs.next()) {
				DateTime thisDate = new DateTime(rs.getString(1));

				if (thisDate.after(transDate)) {
					afterDate = false;
				}
			}
		} catch (Exception e) {
			LogUtil.log(this.getClass(), "EXCEPTION transactionAfterDate " + e.getMessage());
			afterDate = true;
		} finally {
			try {
				sqlStatement.close();
				connection.close();
			} catch (Exception ee) {
				afterDate = true;
			}
		}

		return afterDate;
	}

	/**
	 * Look for a current (less than three months old) pt_pending_fee record for a particular membership Id
	 * 
	 * @param Integer membershipId
	 * @param noticeTypeList List of notice type definitions, single quoted, comma separated
	 * @return PayableFee
	 */
	public PendingFee getRenewalData(Integer membershipId, String sourceSystem, String noticeTypeList) throws RemoteException {

		String sql1 = "select document_id from PUB.mem_document ";
		sql1 += " where membership_ID = ? ";
		sql1 += " and document_type_code in (" + noticeTypeList + ")";
		sql1 += " and document_date > ?";
		sql1 += " order by document_id desc";

		PendingFee fee = null;

		Connection connection = null;
		PreparedStatement statement = null;

		int documentId;
		String billPayNumber;

		try {
			DateTime today = new DateTime();
			Interval threeMonths = new Interval(0, 3, 0, 0, 0, 0, 0); // TODO
			// use
			// parameters
			DateTime threeMonthsAgo = today.subtract(threeMonths);
			today = today.getDateOnly();

			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql1);

			statement.setInt(1, membershipId.intValue());
			statement.setDate(2, threeMonthsAgo.toSQLDate());

			ResultSet rs = statement.executeQuery();

			if (rs.next()) {
				documentId = rs.getInt(1);
				billPayNumber = BillpayHelper.createIvrNumber(documentId, SourceSystem.getSourceSystem(sourceSystem), false);
				fee = getRenewalData(billPayNumber);
				
				// It may be have been generated with the old 0 concat method
				if (fee == null) {
					billPayNumber = BillpayHelper.createIvrNumber(documentId, SourceSystem.getSourceSystem(sourceSystem), true);
					fee = getRenewalData(billPayNumber);
				}
			}
		} catch (Exception e) {
			throw new RemoteException(e + "");
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (Exception ee) {
				throw new RemoteException("" + ee);
			}
		}
		return fee;

	}

	/**
	 * Alternate version only require pendingFeeId
	 */
	public PendingFee getRenewalData(String pendingFeeId) throws RemoteException {
		LogUtil.log(this.getClass(), "getRenewalData start");
		String sql2 = "select fee_effective_date, fee_expiry_date, source_system,";
		sql2 += "pending_fee_id, cc_number, date_paid, ";
		sql2 += "billpay_rec, bank_rec, ref_number, seq_number, amount, client_number, op_sequence_number ";
		sql2 += " from PUB.pt_pending_fee ";
		sql2 += " where pending_fee_id = ? ";

		PendingFee fee = null;
		Connection connection = null;
		PreparedStatement s2 = null;

		try {
			connection = dataSource.getConnection();

			s2 = connection.prepareStatement(sql2);

			s2.setString(1, pendingFeeId);
			ResultSet rs2 = s2.executeQuery();

			if (rs2.next()) {
				fee = new PendingFee();
				java.sql.Date effDate = rs2.getDate(1);

				if (effDate != null) {
					fee.setFeeEffectiveDate(new DateTime(effDate));
				}

				java.sql.Date expDate = rs2.getDate(2);
				if (expDate != null) {
					fee.setFeeExpiryDate(new DateTime(expDate));
				}
				fee.setSourceSystem(rs2.getString(3));
				fee.setPendingFeeID(rs2.getString(4));
				fee.setCreditCardNumber(rs2.getString(5));

				java.sql.Date pDate = rs2.getDate(6);
				if (pDate != null) {
					fee.setDatePaid(new DateTime(pDate));
				}
				fee.setBillpayReceipt(rs2.getString(7));
				fee.setBankReceipt(rs2.getString(8));
				fee.setRefNumber(new Long(rs2.getLong(9)));
				fee.setSeqNumber(new Long(rs2.getLong(10)));
				fee.setAmount(rs2.getDouble(11));
				fee.setClientNumber(new Integer(rs2.getInt(12)));
				fee.setOpSequenceNumber(new Integer(rs2.getInt(13)));

			}
		} catch (Exception e) {
			LogUtil.log(this.getClass(), "ELECTRONIC PAYMENT getRenewalData  exception ");
			throw new RemoteException("getRenewalData " + e.getMessage() + "");
		} finally {
			try {
				s2.close();
				connection.close();
			} catch (Exception ee) {
				throw new RemoteException("" + ee);
			}
		}
		LogUtil.log(this.getClass(), "getRenewalData end");
		return fee;
	}

	/**
 *
 */
	protected static String formatAmountString(double amount) {
		String f = "000000" + new Integer(new Double(amount * 100).intValue()).toString();
		int l = f.length();
		f = f.substring(l - 6);

		return f;
	}

	public PayableFee getPayableFee(String pendingFeeId) throws RemoteException {
		String sql = "Select * from PUB.pt_pending_fee where pending_fee_id = ?";

		Connection connection = null;
		PreparedStatement statement = null;

		PayableFee fee = null;
		DateTime feeExpiryDate = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setString(1, pendingFeeId);

			ResultSet rs = statement.executeQuery();

			if (rs.next()) {
				feeExpiryDate = new DateTime(rs.getDate("fee_expiry_date"));

				if (!feeExpiryDate.before(new DateTime())) {
					fee = new PayableFee();
					fee.setPaymentCode(rs.getString("pending_fee_id"));
					fee.setAmount(rs.getDouble("amount"));
					Integer checkDigit = CheckDigit.getCheckDigit(fee.getPaymentCode().toString(), false);
					fee.setCheckDigit(checkDigit.intValue());
					fee.setExpiryDate(feeExpiryDate);
				}
			}
		} catch (Exception e) {
			throw new RemoteException("Error get pending payable item: \n" + e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (Exception ee) {
				throw new RemoteException(ee + "");
			}
		}
		return fee;
	}

	private String getReceiptingSystem(SourceSystem sourceSystem) throws RemoteException {

		String receiptingSystem = null;
		try {
			receiptingSystem = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.RECEIPTING_SYSTEM + sourceSystem.getAbbreviation());
		} catch (RemoteException ex) {
			throw new RemoteException("Can't find source system for " + sourceSystem.getAbbreviation() + " - " + ex.getMessage());
		}

		return receiptingSystem;
	}

	private void cancelMembershipDocument(PendingFee pf, ElectronicPayment ep, PaymentTransactionMgrLocal payTxMgrLocal) {
		Integer docID = null;

		if (isDirectDebitError(pf, ep)) {
			// cancel the document
			docID = getDocumentId(pf.getPendingFeeID());
			LogUtil.log(this.getClass(), "Cancel membership document " + docID);

			// try
			// {
			// MembershipMgr membershipMgr =
			// MembershipEJBHelper.getMembershipMgr();
			// membershipMgr.removeRenewalDocument(docID,
			// MembershipMgr.MODE_CANCEL, payTxMgr);
			// }
			// catch(Exception ex)
			// {
			// //ignore errors but log them
			// LogUtil.warn(this.getClass(), ex);
			// }

		}
	}

	private Integer getDocumentId(String pendingFeeID) {
		return new Integer(pendingFeeID.substring(1, pendingFeeID.length() - 1));
	}

	private boolean isDirectDebitError(PendingFee pf, ElectronicPayment ep) {
		String matchStr = "has a Direct Debit";
		int ddStr;
		// boolean isDD;

		LogUtil.debug(this.getClass(), "RLG ++++++++++++++++ dd " + ep.getStatus() + " / " + (pf == null));
		if (pf == null) {
			// do nothing - not a pending fee
			return false;

		}

		ddStr = ep.getStatus().indexOf(matchStr);
		LogUtil.debug(this.getClass(), "RLG ++++++++++++++++ dd " + ep.getStatus() + " / " + matchStr + " / " + ddStr);
		if (ddStr == -1) {
			// do nothing - not a Direct Debit
			return false;
		}

		return true;
	}

	private boolean isThisAClaim(Payable payable) {
		return payable.getProductCode().equalsIgnoreCase("REC");
	}

	private boolean isPayablePayable(Payable payable, double tenderedAmount) {
		// boolean isClaim =
		// ep.getSourceSystem().getAbbreviation().equalsIgnoreCase(SourceSystem.IVR_CLAIMS.getAbbreviation());
		boolean isClaim = isThisAClaim(payable);

		// double tenderedAmount = ep.getAmount();
		double payableAmount = payable.getAmount().doubleValue();
		boolean isPayable;

		isPayable = false;

		if (payableAmount == tenderedAmount) {
			isPayable = true;
		}

		if ((payableAmount > tenderedAmount) && isClaim) {

			isPayable = true;
		}
		LogUtil.log(this.getClass(), "RLG +++ isPayable " + isPayable + " claim " + isClaim + "tendered " + tenderedAmount + " needed " + payableAmount);
		return isPayable;
	}

	private void payThisPayable(ElectronicPayment ep, Payable payable, String receiptingSystem, PaymentTransactionMgrLocal payTxMgrLocal) throws RemoteException, RollBackException {

		if (SourceSystem.RECEPTOR.getAbbreviation().equals(receiptingSystem) && ep.getAmount() != payable.getAmountOutstanding().doubleValue() && isThisAClaim(payable)) {
			adjustOCRPayableAndReceiptPayment(payable, ep, payTxMgrLocal);
		} else {
			receiptThisPayment(ep, payable, receiptingSystem, payTxMgrLocal);
		}

	}

	private void receiptThisPayment(ElectronicPayment ep, Payable payable, String receiptingSystem, PaymentTransactionMgrLocal payTxMgrLocal) throws RemoteException, RollBackException {

		LogUtil.log(this.getClass(), "RLG ++++++ payThisPayable start " + ep.getRactReceiptNumber() + "/" + receiptingSystem);
		if (SourceSystem.OCR.getAbbreviation().equals(receiptingSystem)) {
			LogUtil.log(this.getClass(), "RLG ++++++ OCR");
			try {
				ep.notifySourceSystemOfPayment();
			} catch (Exception e) {
				throw new RemoteException(e.getMessage());
			}
		}

		LogUtil.log(this.getClass(), "RLG ++++++ receiptThisPayment " + ep.getRactReceiptNumber() + "/" + receiptingSystem);
		// Receipt the payment
		// Receptor will send notification to source system itself.

		ep.receiptPayment(payTxMgrLocal);

	}

	private ElectronicPayment checkPayableForClaimsPayment(Payable payable, ElectronicPayment ep) {

		// If the payable description is like CLM.REC
		// then change the source system to CLM

		String payableDesc = payable.getDescription();
		String payableProductCode = payable.getProductCode();

		LogUtil.log(this.getClass(), "RLG ++++ " + payableProductCode + " desc " + payableDesc);
		try {
			if (payableProductCode.equalsIgnoreCase("REC")) {
				ep.setSourceSystem(SourceSystem.IVR_CLAIMS);
			}
		} catch (Exception e) {

		}

		return ep;
	}

	private void adjustOCRPayableAndReceiptPayment(Payable payable, ElectronicPayment ep, PaymentTransactionMgrLocal payTxMgrLocal) throws RemoteException, RollBackException {
		double originalAmountOutStanding = payable.getAmountOutstanding().doubleValue();
		double newAmountOutStanding = originalAmountOutStanding - ep.getAmount();
		String payableDescription = payable.getDescription();

		if (newAmountOutStanding < 0.0) {
			throw new RemoteException("adjustOCRPayableAndReceiptPayment - Payment more than required " + originalAmountOutStanding + " / " + ep.getAmount());
		}
		LogUtil.log(this.getClass(), "About to set OCR amount outstanding to " + ep.getAmount());
		payTxMgrLocal.setOCRPayableAmountOutStanding(payable.getSourceSystem(), ep.getClientNumberInteger(), ep.getSeqNumber(), new BigDecimal(ep.getAmount()));
		LogUtil.log(this.getClass(), "Receipting amount  " + ep.getAmount());

		try {
			ep.setOriginalAmountOutStanding(originalAmountOutStanding);
			ep.setPayableDescription(payableDescription);
			ep.setNewAmountOutStanding(newAmountOutStanding);
			ep.setReceptorPurchaseRequiresUncomplete(true);
			ep.setPayableSourceSystem(payable.getSourceSystem());

			ep.receiptPayment(payTxMgrLocal);

		} catch (Exception e) {
			//
			// Reset the OCR amount outstanding if something goes wrong
			resetOCRPayableAmountOutStanding(ep, payTxMgrLocal);

			ep.setReceptorPurchaseRequiresUncomplete(false);

			throw new RollBackException(e.getMessage());

		}
	}

	public void resetOCRPayableAmountOutStanding(ElectronicPayment ep, PaymentTransactionMgrLocal payTxMgrLocal) {
		try {
			payTxMgrLocal.setOCRPayableAmountOutStanding(ep.getPayableSourceSystem(), ep.getClientNumberInteger(), ep.getSeqNumber(), new BigDecimal(ep.getOriginalAmountOutStanding()));
		} catch (RollBackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void adjustOIPayableAndPendingPurchase(ElectronicPayment ep, PaymentTransactionMgrLocal payTxMgrLocal) throws RemoteException, RollBackException {
		double newAmountOutStanding = ep.getNewAmountOutStanding();
		String payableDescription = ep.getPayableDescription();

		LogUtil.log(this.getClass(), "New amount outstanding is " + newAmountOutStanding);
		if (newAmountOutStanding > 0.0) {
			LogUtil.log(this.getClass(), "About to reset OCR amount outstanding to " + newAmountOutStanding);
			payTxMgrLocal.setOCRPayableAmountOutStanding(ep.getPayableSourceSystem(), ep.getClientNumberInteger(), ep.getSeqNumber(), new BigDecimal(newAmountOutStanding));

			LogUtil.log(this.getClass(), "New amount outstanding is " + newAmountOutStanding + " - " + payableDescription + " : " + ep.getClientNumber() + "/" + ep.getSeqNumber());

			/*
			 * payTxMgrLocal.uncompleteReceptorPendingPurchases ( ep.getClientNumberInteger(), ep.getSeqNumber(), payableDescription );
			 */

			payTxMgrLocal.commit();
		}
	}

	public ElectronicPaymentProcessor() {
	}

	protected class PayableFee implements Serializable {
		private String paymentCode;

		private int checkDigit;

		private double amount;

		private DateTime expiryDate;

		/**
		 * Constructor for the PayableFee object
		 */
		public PayableFee() {
		}

		// Getters and Setters
		public DateTime getExpiryDate() {
			return expiryDate;
		}

		public void setExpiryDate(DateTime expiryDate) {
			this.expiryDate = expiryDate;
		}

		/**
		 * Gets the paymentCode attribute of the PayableFee object
		 * 
		 * @return The paymentCode value
		 */
		public String getPaymentCode() {
			return paymentCode;
		}

		/**
		 * Gets the checkDigit attribute of the PayableFee object
		 * 
		 * @return The checkDigit value
		 */
		public int getCheckDigit() {
			return checkDigit;
		}

		/**
		 * Gets the amount attribute of the PayableFee object
		 * 
		 * @return The amount value
		 */
		public double getAmount() {
			return amount;
		}

		/**
		 * Sets the paymentCode attribute of the PayableFee object
		 * 
		 * @param pc The new paymentCode value
		 */
		public void setPaymentCode(String pc) {
			paymentCode = pc;
		}

		/**
		 * Sets the checkDigit attribute of the PayableFee object
		 * 
		 * @param cd The new checkDigit value
		 */
		public void setCheckDigit(int cd) {
			checkDigit = cd;
		}

		/**
		 * Sets the amount attribute of the PayableFee object
		 * 
		 * @param a The new amount value
		 */
		public void setAmount(double a) {
			amount = a;
		}

		/**
		 * Description of the Method
		 * 
		 * @return Description of the Return Value
		 */
		public String toString() {
			String s = "";

			s += "paymentCode: " + paymentCode + "\n";
			s += "checkDigit: " + checkDigit + "\n";
			s += "amount: " + amount + "\n";

			return s;
		}

	}

}
