package com.ract.payment;

/**
 * Notify a payment to progress.
 * 
 * @todo This should be named something like "PaymentListener"
 * 
 * @deprecated use the integration server instead.
 * 
 * @author John Holliday, Glen Wigg
 * 
 */

public class NotifyPayment
// extends ProgressServlet
{
    //
    // /**Process the HTTP Get request*/
    // public void doGet(HttpServletRequest request, HttpServletResponse
    // response)
    // throws ServletException, IOException
    // {
    // String event = request.getParameter("event");
    // handleEvent(event, request, response);
    // }
    //
    // /**
    // * Notify the payment system of a payment in the receipting system
    // */
    // public void handleEvent_notifyPayment(
    // HttpServletRequest request,
    // HttpServletResponse response)
    // {
    // String responseString = null;
    // try
    // {
    //
    // Integer referenceNo = null;
    // Double amountPaid = null;
    // Double amountOutstanding = null;
    // Integer sequenceNo = null;
    //
    // /** @todo These parameters should be renamed to make them more obvious.
    // */
    // String payReferenceNo = request.getParameter("referenceNo");
    // String sequenceNoStr = request.getParameter("seqNo");
    // String amountPaidStr = request.getParameter("amtPd");
    // String amountOutstandingStr = request.getParameter("amtOs");
    //
    // //These two parameters needed to distinguish the type of action to be
    // taken
    // String description = request.getParameter("desc");
    // String createId = request.getParameter("crtId");
    //
    // String sourceSystemAbbrev = request.getParameter("srcSys");
    // String logonId = request.getParameter("lgnId");
    //
    // try
    // {
    // sequenceNo = new Integer(sequenceNoStr);
    // }
    // catch(Exception e)
    // {
    // throw new SystemException(
    // "Unable to parse integer in NotifyPayment: \n" + e);
    // }
    //
    // try
    // {
    // if(amountPaidStr != null)
    // {
    // amountPaid = new Double(amountPaidStr);
    // }
    // }
    // catch(Exception e)
    // {
    // throw new SystemException("The amountPaid '" + amountPaidStr +
    // "' is not a valid double value.", e);
    // }
    //
    // try
    // {
    // if(amountOutstandingStr != null)
    // {
    // amountOutstanding = new Double(amountOutstandingStr);
    // }
    // }
    // catch(Exception e)
    // {
    // throw new SystemException("The amountOutstanding '" +
    // amountOutstandingStr +
    // "' is not a valid double value.", e);
    // }
    //
    // SourceSystem sourceSystem =
    // SourceSystem.getSourceSystem(sourceSystemAbbrev);
    // // process the payment in a separate thread
    // new ProcessPayment(
    // SourceSystem.OCR, // Only payments done though OCR come through this
    // mechanism.
    // payReferenceNo,
    // sequenceNo,
    // amountPaid.doubleValue(),
    // amountOutstanding.doubleValue(),
    // description,
    // createId,
    // logonId,
    // sourceSystem).start();
    //
    // // send the 'OK' back to the calling program
    // responseString = this.SUCCESS;
    // }
    // catch(Exception se)
    // {
    // LogUtil.fatal(this.getClass(), "handleEvent_notifyPayment():\n" +
    // ExceptionHelper.getExceptionStackTrace(se));
    // responseString = this.FAILURE;
    // }
    // finally
    // {
    // response.setHeader(this.RESPONSE_NAME, responseString);
    // }
    //
    // }
    //
    // /**
    // * This class is used to pass the payment information to the Payment
    // Manager
    // * for processing by the source system. It was separated into its own
    // class to
    // * enable threading to provide asynchronous processing. If the process
    // fails an
    // * administrator will be notified by email so the calling method does not
    // need
    // * to wait for a successful completion.
    // */
    // class ProcessPayment
    // extends Thread
    // {
    // SourceSystem paymentSystem = null;
    //
    // String payReferenceNo = null;
    //
    // Integer sequenceNo = null;
    //
    // double amountPaid;
    //
    // double amountOutstanding;
    //
    // String createId = null;
    //
    // String logonId = null;
    //
    // SourceSystem sourceSystem = null;
    //
    // String description = null;
    //
    // public ProcessPayment(
    // SourceSystem paymentSystem,
    // String payReferenceNo,
    // Integer sequenceNo,
    // double amountPaid,
    // double amountOutstanding,
    // String description,
    // String createId,
    // String logonId,
    // SourceSystem sourceSystem)
    // {
    // this.paymentSystem = paymentSystem;
    // this.payReferenceNo = payReferenceNo;
    // this.sequenceNo = sequenceNo;
    // this.amountPaid = amountPaid;
    // this.amountOutstanding = amountOutstanding;
    // this.description = description;
    // this.createId = createId;
    // this.logonId = logonId;
    // this.sourceSystem = sourceSystem;
    // }
    //
    // public void run()
    // {
    // // create the message text for failure notification
    // StringBuffer message = new StringBuffer();
    // message.append("Payment notification :");
    // message.append("\nPayment System = ").append(this.paymentSystem);
    // message.append("\nReferenceNo = ").append(this.payReferenceNo);
    // message.append("\nseqNo = ").append(this.sequenceNo.toString());
    // message.append("\nAmount Paid = ").append(this.amountPaid);
    // message.append("\nAmount still OS = ").append(this.amountOutstanding);
    // message.append("\nDescription = ").append(this.description);
    // message.append("\nCreated By  = ").append(this.createId);
    // message.append("\nReceipted by = ").append(this.logonId);
    // message.append("\nSource System = ").append(this.sourceSystem);
    //
    // LogUtil.debug(this.getClass(), "run(): " + message.toString());
    //
    // try
    // {
    // // Make sure that OCR is the current receipting system being used
    // String whichReceiptingSystem =
    // CommonEJBHelper.getCommonMgr().getSystemParameterValue(
    // SystemParameterVO.CATEGORY_PAYMENT,
    // SystemParameterVO.RECEIPTING_SYSTEM);
    //
    // if(!SourceSystem.OCR.getAbbreviation().equals(whichReceiptingSystem))
    // {
    // throw new SystemException(
    // "The current receipting system being used is '" +
    // whichReceiptingSystem +
    // "' and not OCR. Payment notifications cannot be accepted by the OCR payment notification mechanism in NotifyPayment.handleEvent_notifyPayment().");
    // }
    //
    // PaymentNotificationMgr paymentNotificationMgr =
    // PaymentEJBHelper.getPaymentNotificationMgr();
    // paymentNotificationMgr.notifyItemPaid(
    // this.paymentSystem,
    // this.payReferenceNo,
    // this.sequenceNo,
    // this.amountPaid,
    // this.amountOutstanding,
    // this.description,
    // this.createId,
    // this.logonId,
    // this.sourceSystem);
    // }
    // catch(Exception e)
    // {
    // String errorStackTrace = ExceptionHelper.getExceptionStackTrace(e);
    // message.append("\nERROR:\n");
    // message.append(errorStackTrace);
    // LogUtil.fatal(this.getClass(), message.toString());
    //
    // //auto rollback
    // // then send an email to the administrator advising of the problem
    // try
    // {
    // String emailAddr =
    // CommonEJBHelper.getCommonMgr().getSystemParameterValue(
    // SystemParameterVO.CATEGORY_MEMBERSHIP,
    // SystemParameterVO.EMAIL_SYS_ADMIN);
    // MailMgr mailMgr = CommonEJBHelper.getMailMgr();
    // mailMgr.sendMail(emailAddr, "Payment Notification Failure",
    // message.toString());
    // }
    // catch(Exception ee)
    // {
    // StringWriter eesw = new StringWriter();
    // eesw.write("\nError emailing sysAdmin:\n");
    // eesw.write("\nEmail message was:\n");
    // eesw.write(errorStackTrace);
    // LogUtil.fatal(this.getClass(), eesw.toString());
    // }
    // }
    // }
    // }
}
