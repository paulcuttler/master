/*
 *  @(#)PaymentException.java
 *
 */
package com.ract.payment;

import com.ract.common.GenericException;

/**
 * Generic exception for Payment system.
 * 
 * @author Glenn Lewis
 * @created 31 July 2002
 * @version 1.0, 6/5/2002
 */

public class PaymentException extends GenericException {
    /**
     * Constructor for the PaymentException object
     */
    public PaymentException() {
    }

    /**
     * Constructor for the PaymentException object
     * 
     * @param msg
     *            Description of the Parameter
     */
    public PaymentException(String msg) {
	super(msg);
    }

    /**
     * Constructor for the PaymentException object
     * 
     * @param chainedException
     *            Description of the Parameter
     */
    public PaymentException(Exception chainedException) {
	super(chainedException);
    }

    /**
     * Constructor for the PaymentException object
     * 
     * @param msg
     *            Description of the Parameter
     * @param chainedException
     *            Description of the Parameter
     */
    public PaymentException(String msg, Exception chainedException) {
	super(msg, chainedException);
    }

}
