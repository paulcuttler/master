/*
 * @(#)PayableItem.java
 *
 */
package com.ract.payment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.BranchVO;
import com.ract.common.ClassWriter;
import com.ract.common.DirectDebitFailedValidationException;
import com.ract.common.ExceptionHelper;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.receipting.Payable;
import com.ract.payment.receipting.Receipt;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.XMLHelper;

/**
 * A item that is due to be paid. An item will remain payable until the
 * amountoutstanding is reduced by the receipting system or the direct debit
 * system.
 * 
 * @hibernate.class table="pt_payableitem" lazy="false"
 * @hibernate.cache usage="read-write"
 * 
 * @author Glenn Lewis created 1 August 2002
 * @version 1.0, 1/5/2002
 */
public class PayableItemVO implements Writable, Serializable {

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((payableItemID == null) ? 0 : payableItemID.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof PayableItemVO))
	    return false;
	PayableItemVO other = (PayableItemVO) obj;
	if (payableItemID == null) {
	    if (other.payableItemID != null)
		return false;
	} else if (!payableItemID.equals(other.payableItemID))
	    return false;
	return true;
    }

    /**
     * Description of the Field
     */
    public final static String WRITABLE_CLASSNAME = "PayableItem";

    /**
     * The ID of the payable item in payment system
     */
    protected Integer payableItemID;

    /**
     * A vector of components that make up the payable item. That is, a vector
     * of <code>PayableItemComponent</code> .
     */
    protected Collection components;

    /**
     * The id of the client associated with this payableItem
     */
    private Integer clientNo = null;

    /**
     * The payment method for this item
     */
    private PaymentMethod paymentMethod;

    /**
     * The source system reference ID. (e.g the transaction number in membership
     * (NB: the current direct debit system expects an integer for this field)
     */
    protected Integer sourceSystemReferenceID;

    /**
     * The id of the sales branch that generated the payable item.
     */
    protected String salesBranchCode;

    /**
     * The branch region. That is, the branch closest to the client's
     * residential address.
     */
    protected BranchVO branchRegion;

    /**
     * The branch region. That is, the branch closest to the client's
     * residential address.
     */
    protected Integer branchNumber;

    /**
     * The product code for the payable item. This is a payments product code.
     * It will be passed on to MFG/PRO. There may be many product codes in the
     * source system that map to a single payments product code.
     */
    protected String productCode;

    /**
     * The logon id of the user who generated this payable item
     */
    protected String userID;

    /**
     * The amount payable
     */
    protected double amountPayable;

    /**
     * The amount outstanding (not yet paid) on this payable item. Initially
     * this is set to be the amount payable.
     */
    // protected double amountOutstanding;

    /**
     * The amount pending on this payable item. This only applies if the item is
     * paid by direct debit. The amount pending is the amount has been sent to
     * the bank, but we are unsure whether the payment was successful (since the
     * bank only informs of unsuccessful payments). After 7 days the amount
     * pending will move to being paid. Set to 0 if there is no amount pending.
     */
    protected double amountPending;

    /**
     * The start date of the product that the item is for item
     */
    protected DateTime startDate;

    /**
     * The end date of the product that the item is for
     */
    protected DateTime endDate;

    /**
     * Description a description of the item
     */
    protected String description;

    /**
     * The minimum length that the product code string can be.
     */
    private static int MIN_PRODUCT_CODE_LENGTH = 1;

    /**
     * The minimum length that the logon id can be.
     */
    private static int MIN_LOGON_ID_LENGTH = 1;

    /**
     * Description of the Field
     */
    protected DateTime createDateTime;

    /**
     * Description of the Field
     */
    protected String paymentMethodID;

    /**
     * Description of the Field
     */
    protected String paymentMethodDescription;

    /**
     * Receipt number.
     */
    private String receiptNo;

    public PayableItemVO() {
    }

    /**
     * Constructor
     * 
     * @param payableItemID
     *            the ID of the payable item. <code>null</code> if the payable
     *            item has not been entered in the payment system.
     * @param clientNo
     *            the id of the client who this payable item is associated with
     * @param paymentMethod
     *            the method via which this item will be (has been) paid
     * @param componentsVector
     *            a vector of the components of this item (i.e a vector of
     *            <code>PayableItemComponent</code>).
     * @param sourceSystem
     *            the source system associated with this payable item
     * @param sourceSystemReferenceID
     *            The source system reference ID (e.g. the transaction ID in the
     *            membership system)
     * @param salesBranch
     *            the sales branch associated with this payable item
     * @param productCode
     *            the product code of the product that this item is for. This is
     *            a payments product code. It will be passed on to MFG/PRO.
     *            There may be many product codes in the source system that map
     *            to a single payments product code.
     * @param startDate
     *            the the start date of the product that this payable item is
     *            for
     * @param endDate
     *            the the end date of the product that this payable item is for
     * @param description
     *            the description of this payable item. A <code>null</code>
     *            value indicates no description.
     * @param clientBranchNumber
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>                  PaymentException</code> if any of the
     *         parameters are invalid.
     */
    public PayableItemVO(Integer payableItemID, Integer clientNo, PaymentMethod paymentMethod, Collection componentsList, SourceSystem sourceSystem, Integer sourceSystemReferenceID, String salesBranch, Integer clientBranchNumber, String productCode, String userID, DateTime startDate, DateTime endDate, String description) throws PaymentException {
	if (payableItemID != null) {
	    setPayableItemID(payableItemID);
	}

	setClientNo(clientNo);
	setPaymentMethod(paymentMethod);
	setComponents(componentsList);
	setSourceSystem(sourceSystem);
	setSourceSystemReferenceID(sourceSystemReferenceID);
	setSalesBranchCode(salesBranch);
	this.setBranchNumber(clientBranchNumber);
	setProductCode(productCode);
	setUserID(userID);
	setStartEndDates(startDate, endDate);
	if (description != null) {
	    setDescription(description);
	}

	// calculate the amount payable from the components
	double amount = 0.0;
	PayableItemComponentVO component = null;
	Iterator compIt = componentsList.iterator();
	while (compIt.hasNext()) {
	    component = (PayableItemComponentVO) compIt.next();
	    amount += component.getAmount().doubleValue();
	}
	LogUtil.debug(this.getClass(), "compAmount" + amount);
	amount = NumberUtil.roundDouble(amount, 2);
	setAmountPayable(amount);

	if (paymentMethod.getDescription().equals(PaymentMethod.RECEIPTING)) {
	    setAmountOutstanding(amount);
	}
	// setAmountPending(0.0);
    }

    public PayableItemVO(Integer payableItemID, Integer clientNo, String paymentMethodDescription, String paymentMethodID, SourceSystem sourceSystem, Integer sourceSystemReferenceID, String salesBranch, Integer clientBranchNumber, String productCode, String userID, DateTime startDate, DateTime endDate, String description) throws PaymentException {
	if (payableItemID != null) {
	    setPayableItemID(payableItemID);
	}

	setClientNo(clientNo);
	this.paymentMethodDescription = paymentMethodDescription;
	this.paymentMethodID = paymentMethodID;
	setSourceSystem(sourceSystem);
	setSourceSystemReferenceID(sourceSystemReferenceID);
	setSalesBranchCode(salesBranch);
	setBranchNumber(clientBranchNumber);
	setProductCode(productCode);
	setUserID(userID);
	setStartEndDates(startDate, endDate);
	if (description != null) {
	    setDescription(description);
	}

    }

    /**
     * Constructor for the PayableItemVO object
     * 
     * @param payableItemID
     *            Description of the Parameter
     * @param clientNo
     *            Description of the Parameter
     * @param payMethodDescription
     *            Description of the Parameter
     * @param payMethodID
     *            Description of the Parameter
     * @param componentsVector
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param sourceSystemReferenceID
     *            Description of the Parameter
     * @param salesBranch
     *            Description of the Parameter
     * @param clientBranchNumber
     *            Description of the Parameter
     * @param productCode
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @param startDate
     *            Description of the Parameter
     * @param endDate
     *            Description of the Parameter
     * @param description
     *            Description of the Parameter
     * @exception PaymentException
     *                Description of the Exception
     */
    public PayableItemVO(Integer payableItemID, Integer clientNo, String payMethodDescription, String payMethodID, Vector componentsVector, SourceSystem sourceSystem, Integer sourceSystemReferenceID, String salesBranch, Integer clientBranchNumber, String productCode, String userID, DateTime startDate, DateTime endDate, String description) throws PaymentException {
	if (payableItemID != null) {
	    setPayableItemID(payableItemID);
	}

	setClientNo(clientNo);
	this.paymentMethodDescription = payMethodDescription;
	this.paymentMethodID = payMethodID;
	setComponents(componentsVector);
	setSourceSystem(sourceSystem);
	setSourceSystemReferenceID(sourceSystemReferenceID);
	setSalesBranchCode(salesBranch);
	setBranchNumber(clientBranchNumber);
	setProductCode(productCode);
	setUserID(userID);
	setStartEndDates(startDate, endDate);
	if (description != null) {
	    setDescription(description);
	}

	// calculate the amount payable from the components
	double amount = 0;
	for (int i = 0; i < componentsVector.size(); i++) {
	    PayableItemComponentVO component = (PayableItemComponentVO) componentsVector.elementAt(i);
	    amount += component.getAmount().doubleValue();
	}
	setAmountPayable(amount);

	if (paymentMethod.getDescription().equals(PaymentMethod.RECEIPTING)) {
	    setAmountOutstanding(amount);
	}
	// setAmountPending(0);
    }

    // ============ Getter Methods =============================================

    /**
     * Get the ID of the payable item in the payment system.
     * 
     * @hibernate.id column="payableitemid" generator-class="assigned"
     * @return the ID of the payable item payment system, or <code>null</code>
     *         if the item has not been entered in the payment system.
     */
    public Integer getPayableItemID() {
	return this.payableItemID;
    }

    /**
     * Get the client associated with this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="clientno"
     * @return the client associated with this payable item
     */
    public Integer getClientNo() {
	return clientNo;
    }

    /**
     * get the client
     * 
     * @return The client value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ClientVO getClient() throws RemoteException {
	ClientVO clientVO = null;
	Integer clientNumber = this.getClientNo();
	if (clientNumber != null) {
	    try {
		ClientMgr cMgr = ClientEJBHelper.getClientMgr();
		clientVO = cMgr.getClient(clientNumber);
	    } catch (ValidationException ve) {
		throw new RemoteException("Unable to get the client for " + clientNumber, ve);
	    }
	}
	return clientVO;
    }

    /**
     * Get the client associated with this payable item
     * 
     * @return the client associated with this payable item
     */
    // public Integer getClientBranchNo()
    // {
    // return this.branchNumber;
    // }

    /**
     * Get the payment method (i.e how this item will be or has been paid) If
     * the payment method is direct debit then adjust the amount outstanding and
     * amount pending as this is not changed on the payable item when a payment
     * by direct debit is processed.
     * 
     * @return the payment method (i.e how this item will be or has been paid)
     * @exception RemoteException
     *                Description of the Exception
     */
    public PaymentMethod getPaymentMethod() throws RemoteException {

	LogUtil.debug(this.getClass(), "paymentMethodID = " + paymentMethodID);
	LogUtil.debug(this.getClass(), "paymentMethod = " + paymentMethod);

	if (this.paymentMethod == null) {
	    LogUtil.debug(this.getClass(), "finding payment method..." + paymentMethodID);

	    if (this.paymentSystem.equals(SourceSystem.ECR.getAbbreviation()) || (!this.paymentSystem.equals(SourceSystem.ECR.getAbbreviation()) && this.paymentMethodID != null)) {
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		this.paymentMethod = paymentMgr.getPaymentMethod(this.paymentMethodDescription, this.paymentMethodID);
	    }
	}
	return this.paymentMethod;
    }

    /**
     * Get the components of this payable item
     * 
     * @return the components of this payable item
     */
    public Collection getComponents() {
	LogUtil.log(this.getClass(), "components a");
	if (this.components == null) {
	    LogUtil.debug(this.getClass(), "payableItemVO");
	    LogUtil.debug(this.getClass(), "srcSystem=" + sourceSystem);
	    LogUtil.debug(this.getClass(), "payableItemID=" + payableItemID);
	    try {
		PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
		LogUtil.log(this.getClass(), "components 1");
		this.components = pMgr.getComponents(this.sourceSystem, this.payableItemID);
		LogUtil.log(this.getClass(), "components 2" + components);
	    } catch (RemoteException ex) {
		LogUtil.fatal(this.getClass(), "Unable to get the components." + ex);
	    }
	}
	return this.components;
    }

    /**
     * Get the set of components that have an unearned posting
     * 
     * @return Collection
     */
    // public Collection getUnearnedComponents()
    // {
    // PayableItemComponentVO payableItemComponent = null;
    // ArrayList unearnedComponentList = new ArrayList();
    // Collection componentList = this.getComponents();
    // Iterator componentIt = componentList.iterator();
    // while (componentIt.hasNext())
    // {
    // payableItemComponent = (PayableItemComponentVO)componentIt.next();
    // if (payableItemComponent.hasUnearnedEarningsPosting())
    // {
    // unearnedComponentList.add(payableItemComponent);
    // }
    // }
    // return unearnedComponentList;
    // }

    /**
     * Get attached component by source system reference.
     * 
     * @param ssRef
     * @return
     */
    public PayableItemComponentVO getComponent(String ssRef) {
	Collection componentList = this.getComponents();
	PayableItemComponentVO pic = null;
	if (componentList != null && componentList.size() > 0) {
	    Iterator compIt = componentList.iterator();
	    while (compIt.hasNext()) {
		pic = (PayableItemComponentVO) compIt.next();
		if (pic.getSourceSystemReference().equals(ssRef)) {
		    return pic;
		}
	    }
	}
	return null;
    }

    private SourceSystem sourceSystem;

    /**
     * Get the source system associated with this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="sourcesystem"
     * @return the source system associated with this payable item
     */
    public SourceSystem getSourceSystem() {
	return this.sourceSystem;
    }

    /**
     * Get the source system reference ID (e.g. the transaction ID in
     * membership)
     * 
     * @hibernate.property
     * @hibernate.column name="sourcesystemreferenceid"
     * @return the source system reference ID (e.g. the transaction ID in
     *         membership)
     */
    public Integer getSourceSystemReferenceID() {
	return this.sourceSystemReferenceID;
    }

    /**
     * Get the sales branch associated with this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="salesbranch"
     * @return the sales branch associated with this payable item
     */
    public String getSalesBranchCode() {
	return this.salesBranchCode;
    }

    /**
     * Get the branch closest to the clients residential address
     * 
     * @return the branch closest to the clients residential address
     */
    public BranchVO getBranchRegionVO() {
	return this.branchRegion;
    }

    /**
     * Get the payments product code of the product that this payable item is
     * for. This is a payments product code. It will be passed on to MFG/PRO.
     * There may be many product codes in the source system that map to a single
     * payments product code.
     * 
     * @hibernate.property
     * @hibernate.column name="prodcode"
     * @return the payments product code of the product that this payable item
     *         is for
     */
    public String getProductCode() {
	return this.productCode;
    }

    /**
     * Get the amount of this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="amountpayable"
     * @return the amount of this payable item
     */
    public double getAmountPayable() {
	return this.amountPayable;
    }

    private BigDecimal amountOutstanding;

    private String repairPaymentMethodID;

    protected String paymentSystem;

    private BigDecimal directAmountOutstanding;

    /**
     * 
     * @hibernate.property
     * @hibernate.column name="amountoutstanding"
     * @return
     */
    public BigDecimal getDirectAmountOutstanding() {
	return this.directAmountOutstanding;
    }

    public void setDirectAmountOutstanding(BigDecimal directAmountOutstanding) {
	this.directAmountOutstanding = directAmountOutstanding;
    }

    /**
     * Get the amount outstanding on this payable item
     * 
     * @return the amount outstanding on this payable item
     */
    public double getAmountOutstanding() throws ValidationException {
	LogUtil.debug(this.getClass(), "getAmountOutstanding =" + this.amountOutstanding);
	// if no direct amt outstanding present, or this is and it's not a
	// Receipting payment
	if (this.getDirectAmountOutstanding() == null || !this.getPaymentMethodDescription().equals(PaymentMethod.RECEIPTING)) {
	    // get from sourcesystem
	    LogUtil.debug(this.getClass(), "amountOutstanding getAmountOutstanding pi=" + this.getPayableItemID());
	    BigDecimal amountOS = new BigDecimal(0);
	    PaymentMethod payMethod = null;
	    try {
		payMethod = this.getPaymentMethod();
	    } catch (RemoteException ex1) {
		throw new ValidationException("Unable to get the payment method for '" + this.payableItemID + "'. " + ex1.getMessage());
	    }
	    LogUtil.debug(this.getClass(), "payMethod null == " + (payMethod == null));
	    if (payMethod != null) {
		String payDesc = paymentMethod.getDescription();
		LogUtil.debug(this.getClass(), "payDesc=" + payDesc);
		if (payDesc.equals(PaymentMethod.DIRECT_DEBIT)) {
		    DirectDebitSchedule ddSched = (DirectDebitSchedule) paymentMethod;
		    if (ddSched.isFailed()) {
			String message = "Direct debit schedule is marked as failed.";
			if (ddSched.getReceiptingAmount().doubleValue() > 0) {
			    message += " There are still amounts in the receipting system.";
			}
			throw new DirectDebitFailedValidationException(message);
		    }
		    LogUtil.debug(this.getClass(), "ddSched=" + ddSched.toString());
		    amountOS = amountOS.add(ddSched.getTotalAmountOutstanding());
		} else if (payDesc.equals(PaymentMethod.RECEIPTING)) {
		    Payable payable = null;
		    try {
			LogUtil.debug(this.getClass(), "getAmountOutstanding rec");
			PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

			payable = paymentMgr.getPayable(this.clientNo, this.paymentMethodID, this.paymentSystem, this.sourceSystem);
			LogUtil.debug(this.getClass(), "clientnumber=" + this.clientNo + ",paymentmethodid=" + this.paymentMethodID);
		    } catch (Exception ex) {
			LogUtil.log(this.getClass(), ex);
			throw new ValidationException("Unable to retrieve outstanding amount on payable for client " + this.clientNo + ", payment method id " + this.paymentMethodID + "." + ExceptionHelper.getExceptionStackTrace(ex));
		    }

		    if (payable != null) {
			// check for partially paid payable item
			amountOS = amountOS.add(payable.getAmountOutstanding());
			if (payable.getAmountOutstanding().compareTo(new BigDecimal(0)) != 0 && // not
												// fully
												// paid
				payable.getAmountOutstanding().compareTo(payable.getAmount()) != 0) // not
												    // fully
												    // unpaid
			{
			    throw new ValidationException("Receipting payables may not be partially paid. ref: " + clientNo + "/" + paymentMethodID);
			}
		    } else { // payable is null
			throw new ValidationException("Unable to retrieve payable for client " + this.clientNo + ", payment method id " + this.paymentMethodID + ".");
		    }

		}
	    }
	    LogUtil.debug(this.getClass(), "getAmountOutstanding amountOS=" + amountOS);
	    this.amountOutstanding = amountOS;
	} else { // if a directly recorded receipting amount
	    this.amountOutstanding = this.getDirectAmountOutstanding();
	}

	// LogUtil.log(this.getClass(), "getAmountOutstanding"+amountOS);
	return this.amountOutstanding.doubleValue();
    }

    /**
     * Return the receipt from the receipting system that was created by this
     * payable item. Return null if the payment method of this payable item is
     * direct debit.
     */
    public Payable getPayable() throws RemoteException {
	if (!isDirectDebit()) {
	    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    Payable payable = null;
	    try {
		payable = paymentMgr.getPayable(this.clientNo, this.paymentMethodID, this.paymentSystem, this.sourceSystem);
	    } catch (PaymentException ex) {
		throw new RemoteException("Unable to get payable for '" + this.getPayableItemID() + "'.", ex);
	    }
	    return payable;

	} else {
	    return null;
	}
    }

    public Receipt getReceipt() throws ValidationException {
	if (!isDirectDebit()) {
	    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    Receipt receipt = null;
	    try {
		receipt = paymentMgr.getReceipt(this.clientNo, this.paymentMethodID, this.sourceSystem);
	    } catch (Exception e) {
		e.printStackTrace();
		throw new ValidationException("Unable to get receipt for '" + this.getPayableItemID() + "'.");
	    }
	    return receipt;
	} else {
	    return null;
	}
    }

    /**
     * Get the start date of the product associated with this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="startdate"
     * @return the start date for this payable item
     */
    public DateTime getStartDate() {
	return this.startDate;
    }

    /**
     * Get the end date of the product associated with this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="enddate"
     * @return the end date for this payable item
     */
    public DateTime getEndDate() {
	return this.endDate;
    }

    /**
     * Get the logon ID of the user who generated this payable item is for
     * 
     * @hibernate.property
     * @hibernate.column name="logonid"
     * @return the logon ID of the user who generated this payable item is for
     */
    public String getUserID() {
	return this.userID;
    }

    /**
     * Get the description of this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="description"
     * @return the description of this payable item
     */
    public String getDescription() {
	return this.description;
    }

    // ============ Setter Methods =============================================

    /**
     * Set the ID of this payable item in the payment system.
     * 
     * @param payableItemID
     *            the ID of this payable item in the payment system.
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if
     *         <code>payableItemID</code> is <code>null</code>
     */
    public void setPayableItemID(Integer payableItemID) throws PaymentException {
	if (payableItemID == null) {
	    throw new PaymentException("Setting payable item ID to null in Payable Item");
	}

	this.payableItemID = payableItemID;
    }

    /**
     * Set the payment method ID. Also set the payment method ID on the attached
     * payment method.
     * 
     * @param payMethodID
     *            The new paymentMethodID value
     * @exception SystemException
     *                Description of the Exception
     */
    public void setPaymentMethodID(String payMethodID) throws SystemException {
	// if(payMethodID == null)
	// {
	// throw new
	// SystemException("Setting payable method ID to null in Payable Item");
	// }

	this.paymentMethodID = payMethodID;

	if (this.paymentMethod != null) {
	    this.paymentMethod.setPaymentMethodID(payMethodID);
	}
	// this.setModified(true);
    }

    public void clearPaymentMethodID() {
	this.paymentMethodID = null;
    }

    /**
     * Sets the paymentMethodDescription attribute of the PayableItemVO object
     * 
     * @param payMethDesc
     *            The new paymentMethodDescription value
     * @exception PaymentException
     *                Description of the Exception
     */
    public void setPaymentMethodDescription(String payMethDesc) throws PaymentException {
	if (!(PaymentMethod.DIRECT_DEBIT.equals(payMethDesc) || PaymentMethod.RECEIPTING.equals(payMethDesc))) {
	    throw new PaymentException("The payment method description '" + payMethDesc + "' is not valid for a payable item.");
	}
	this.paymentMethodDescription = payMethDesc;
    }

    /**
     * Set the client associated with this payable item
     * 
     * @param clientNo
     *            the ID of the client associated with this payable item
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if
     *         <code>clientNo</code> is <code>null</code>
     */
    public void setClientNo(Integer clientNo) throws PaymentException {
	if (clientNo == null) {
	    throw new PaymentException("Setting null client ID in PayableItem");
	}

	// otherwise assume that the clientNo is valid
	this.clientNo = clientNo;
    }

    /**
     * Set the payment method
     * 
     * @param paymentMethod
     *            the method via which this payment will be paid.
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if
     *         <code>paymentMethod</code> is <code>null</code>
     */
    public void setPaymentMethod(PaymentMethod paymentMethod) throws PaymentException {
	if (paymentMethod == null) {
	    throw new PaymentException("Setting null payment method in PayableItem");
	}

	// otherwise assume that the paymentMethod is valid
	this.paymentMethod = paymentMethod;
	this.paymentMethodID = paymentMethod.getPaymentMethodID();
	this.paymentMethodDescription = paymentMethod.getDescription();

	// If a direct debit schedule then adjust the amount outstanding
	// and the amount pending.
	// if (this.paymentMethod.isDirectDebitPaymentMethod()) {
	// DirectDebitSchedule ddSchedule = (DirectDebitSchedule)
	// this.paymentMethod;
	// // Schedule specifications are sent to the dd system, not retrieved
	// from it
	// // so they don't specify the amount outstanding.
	// if (!ddSchedule.isScheduleSpecification()) {
	// this.amountOutstanding =
	// ddSchedule.getTotalAmountOutstanding().doubleValue();
	// this.amountPending =
	// ddSchedule.getTotalAmountPending().doubleValue();
	// }
	// }
    }

    /**
     * Set the components of this payable item
     * 
     * @param componentsVector
     *            the components of this payable item
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if the components
     *         vector does not consist of only valid
     *         <code>PayableItemComponent</code> instances.
     */
    private void setComponents(Collection componentList) throws PaymentException {
	if (componentList == null) {
	    throw new PaymentException("Setting components to null in PayableItem.");
	}

	Iterator compIt = componentList.iterator();
	while (compIt.hasNext()) {
	    Object obj = compIt.next();
	    if (!(obj instanceof PayableItemComponentVO)) {
		throw new PaymentException("Invalid component found when setting components in PayableItem");
	    }

	    PayableItemComponentVO component = (PayableItemComponentVO) obj;
	    if (getPayableItemID() != null) {
		component.setPayableItemID(getPayableItemID());
	    }
	}
	this.components = componentList;
    }

    /**
     * Set the source system
     * 
     * @param sourceSystem
     *            the source system associated with this payable item
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if
     *         <code>sourceSystem</code> is <code>null</code>
     */
    // public void setSourceSystem(SourceSystem sourceSystem)
    // throws PaymentException
    // {
    // if(sourceSystem == null)
    // {
    // throw new
    // PaymentException("Setting source system to null in PayableItem");
    // }
    //
    // this.sourceSystem = sourceSystem;
    // }

    /**
     * Set the tthe source system reference ID
     * 
     * @param sourceSystemReferenceID
     *            the source system reference ID (e.g the transaction ID in
     *            membership).
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>                  PaymentException</code> if
     *         <code>sourceSystemReferenceID</code> is <code>null</code>
     */
    public void setSourceSystemReferenceID(Integer sourceSystemReferenceID) throws PaymentException {
	if (sourceSystemReferenceID == null) {
	    throw new PaymentException("Setting source system reference ID to null in PayableItem");
	}

	this.sourceSystemReferenceID = sourceSystemReferenceID;
    }

    /**
     * Set the sales branch source system
     * 
     * @param salesBranchCode
     *            The new salesBranch value
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if
     *         <code>salesBranch</code> is <code>null</code>
     */
    public void setSalesBranchCode(String salesBranchCode) throws PaymentException {
	if (salesBranchCode == null || salesBranchCode.equals("")) {
	    throw new PaymentException("Setting sales branch to null in PayableItem");
	}

	this.salesBranchCode = salesBranchCode;
    }

    /**
     * Set the branch region system
     * 
     * @param branchNumber
     *            The new branchNumber value
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if
     *         <code>branchRegion</code> is <code>null</code>
     */
    private void setBranchNumber(Integer branchNumber) throws PaymentException {
	this.branchNumber = branchNumber;
    }

    /**
     * Set the payments product code of the product that this item is for. This
     * is a payments product code. It will be passed on to MFG/PRO. There may be
     * many product codes in the source system that map to a single payments
     * product code.
     * 
     * @param productCode
     *            the product code of the product that this item is for
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if:
     *         <p>
     * 
     * 
     *         <OL>
     *         <li> <code>null</code></li>
     *         <li>less than <code>MIN_PRODUCT_CODE_LENGTH</code> characters
     *         long</li>
     *         </OL>
     * 
     */
    public void setProductCode(String productCode) throws PaymentException {
	if (productCode != null) {
	    if (productCode.length() < MIN_PRODUCT_CODE_LENGTH) {
		throw new PaymentException("The product code is not a valid length.");
	    }
	}

	// throw new
	// PaymentException("Setting product code to null in PayableItem");

	this.productCode = productCode;
    }

    /**
     * Set the logon ID of the user who generated the payable item
     * 
     * @param userID
     *            The new userID value
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if
     *         <code>logonID</code> is <code>null</code> or if it is not greater
     *         than the minimum logon id length.
     */
    public void setUserID(String userID) throws PaymentException {
	if (userID == null) {
	    throw new PaymentException("Setting user ID to null in PayableItem");
	}

	if (userID.length() < MIN_LOGON_ID_LENGTH) {
	    throw new PaymentException("The user ID '" + userID + "' is not a valid length.");
	}

	this.userID = userID;
    }

    /**
     * Set the amount payable of payable item
     * 
     * @param amountPayable
     *            the total amount of this payable item. Must be greater than or
     *            equal to 0.
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if the amount is
     *         less than 0
     */
    public void setAmountPayable(double amountPayable) throws PaymentException {
	if (amountPayable < 0.0) {
	    // throw new
	    // PaymentException("The amount must be greater than or equal 0.");
	    LogUtil.debug(this.getClass(), "Amount payable is less than 0." + amountPayable);
	    amountPayable = 0;
	}

	this.amountPayable = amountPayable;
    }

    /**
     * Set the amount pending on payable item
     * 
     * @param amountPending
     *            the amount pending. Must be greater than or equal to 0.
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if the amount
     *         pending is less than 0
     */
    // public void setAmountPending(double amountPending)
    // throws PaymentException
    // {
    // if (amountPending < 0.0) {
    // throw new
    // PaymentException("The amount outstanding cannot be less than zero.");
    // }
    // this.amountPending = amountPending;
    // }

    /**
     * Set the amount outstanding (not yet paid) of payable item
     * 
     * @param amountOutstanding
     *            the amount outstanding. Must be greater than or equal to 0.
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if the amount
     *         outstanding is less than 0
     */
    public void setAmountOutstanding(double amountOutstanding) throws PaymentException {
	if (amountOutstanding < 0.0) {
	    throw new PaymentException("The amount outstanding cannot be less than zero.");
	}

	this.setDirectAmountOutstanding(new BigDecimal(amountOutstanding));
    }

    /**
     * Set the start date of the product that this payable item is for
     * 
     * @param startDate
     *            the the start date of the product that this payable item is
     *            for
     * @param endDate
     *            The new startEndDates value
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if the start date is
     *         <code>null</code>
     */
    private void setStartEndDates(DateTime startDate, DateTime endDate) throws PaymentException {
	if (startDate == null) {
	    throw new PaymentException("Attempting to set the start date of a payable item to null.");
	}

	if (endDate == null) {
	    throw new PaymentException("Attempting to set the end date of a payable item to null.");
	}

	if (startDate.after(endDate)) {
	    throw new PaymentException("Attempting to set start date after the end date in a payable item. Start date is " + startDate + ". End date is " + endDate + ".");
	}

	this.startDate = startDate;
	this.endDate = endDate;
    }

    /**
     * Set the end date of the product that this payable item is for
     * 
     * @param endDate
     *            the the end date of the product that this payable item is for
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if the end date is
     *         <code>null</code>
     */
    private void setEndDate(DateTime endDate) throws PaymentException {
	if (endDate == null) {
	    throw new PaymentException("Attempting to set the end date of a payable item to null.");
	}

	this.endDate = endDate;
    }

    /**
     * Set the description on this payable item
     * 
     * @param description
     *            the description of this payable item
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException</code> if the description
     *         is <code>null</code>
     */
    public void setDescription(String description) throws PaymentException {
	if (description == null) {
	    throw new PaymentException("Attempting to set the description of a payable item to null.");
	}
	this.description = description;
    }

    /**
     * Sets the createDateTime attribute of the PayableItem object
     * 
     * @param createDateTime
     *            The new createDateTime value
     */
    public void setCreateDateTime(DateTime createDateTime) {
	this.createDateTime = createDateTime;
    }

    public void setRepairPaymentMethodID(String repairPaymentMethodID) {
	this.repairPaymentMethodID = repairPaymentMethodID;
    }

    public void setPaymentSystem(String paymentSystem) {
	this.paymentSystem = paymentSystem;
    }

    public void setStartDate(DateTime startDate) {
	this.startDate = startDate;
    }

    public void setSourceSystem(SourceSystem sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    /**
     * @hibernate.property
     * @return
     */
    public String getReceiptNo() {
	return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
	this.receiptNo = receiptNo;
    }

    /**
     * Gets the createDateTime attribute of the PayableItem object
     * 
     * @hibernate.property type="com.ract.util.DateTimeCombined"
     * @hibernate.column name="createdate"
     * @hibernate.column name="createtime"
     * @return The createDateTime value
     */
    public DateTime getCreateDateTime() {
	return createDateTime;
    }

    /**
     * Gets the paymentMethodID attribute of the PayableItem object
     * 
     * @hibernate.property
     * @hibernate.column name="paymentmethodid"
     * @return The paymentMethodID value
     */
    public String getPaymentMethodID() {
	return paymentMethodID;
    }

    /**
     * Gets the paymentMethodDescription attribute of the PayableItemVO object
     * 
     * @hibernate.property
     * @hibernate.column name="paymentmethod"
     * @return The paymentMethodDescription value
     */
    public String getPaymentMethodDescription() {
	return this.paymentMethodDescription;
    }

    public String getRepairPaymentMethodID() {
	return repairPaymentMethodID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="payment_system"
     * @return String
     */
    public String getPaymentSystem() {
	return paymentSystem;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="branchregion"
     * @return Integer
     */
    public Integer getBranchNumber() {
	return branchNumber;
    }

    /**
     * Gets the directDebit attribute of the PayableItemVO object
     * 
     * @return The directDebit value
     */
    public boolean isDirectDebit() {
	return PaymentMethod.DIRECT_DEBIT.equals(this.paymentMethodDescription);
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public boolean postingsHaveBeenTransferred() {
	boolean postingsHaveBeenTransferred = false;
	LogUtil.log(this.getClass(), "postingsHaveBeenTransferred 1");
	Collection componentList = this.getComponents();
	LogUtil.log(this.getClass(), "postingsHaveBeenTransferred 2");
	Iterator componentListIterator = componentList.iterator();
	while (!postingsHaveBeenTransferred && componentListIterator.hasNext()) {
	    PayableItemComponentVO component = (PayableItemComponentVO) componentListIterator.next();
	    LogUtil.log(this.getClass(), "postingsHaveBeenTransferred 3" + component);
	    postingsHaveBeenTransferred = component.postingsHaveBeenTransferred();
	}
	return postingsHaveBeenTransferred;
    }

    /**
     * Return true if the PayableItemVO has any postings that specify an
     * unearned to earned account transfer.
     */
    // public boolean isEarnable()
    // {
    // boolean hasEarningsPosting = false;
    // Collection componentList = this.getComponents();
    // PayableItemComponentVO component = null;
    // Iterator componentListIterator = componentList.iterator();
    // while(!hasEarningsPosting && componentListIterator.hasNext())
    // {
    // component = (PayableItemComponentVO)componentListIterator.next();
    // hasEarningsPosting = component.isEarnable();
    // }
    // return hasEarningsPosting;
    // }

    /**
     * Is the payable item fully unpaid and was there an amount payable.
     * 
     * @return
     */
    public boolean isFullyUnpaid() throws ValidationException {
	boolean unpaid = false;
	if (this.getAmountPayable() > 0 && (this.getAmountOutstanding() == this.getAmountPayable())) {
	    unpaid = true;
	} else {
	    unpaid = false;
	}
	return unpaid;
    }

    /**
     * Return the ratio of the unpaid amount over the total amount
     */
    public BigDecimal getUnpaidRatio() throws RemoteException {
	BigDecimal amountPayable = new BigDecimal(this.getAmountPayable());
	// if there was no amount payable, return 0
	if (amountPayable.compareTo(new BigDecimal(0)) == 0) {
	    return new BigDecimal(0);
	}

	BigDecimal amountOutstanding = null;
	amountOutstanding = new BigDecimal(this.getAmountOutstanding());
	return amountOutstanding.divide(amountPayable, 4, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Return the ratio of the unpaid amount over the total amount
     */
    public BigDecimal getPaidRatio() throws RemoteException {
	return new BigDecimal(1).subtract(this.getUnpaidRatio());
    }

    /*
     * public String toString() { StringBuffer desc = new StringBuffer();
     * desc.append("\n"); desc.append(this.getPayableItemID()+":");
     * desc.append((
     * this.getCreateDateTime()==null?"":this.getCreateDateTime().toString
     * ())+" "); desc.append(this.getPaymentMethodDescription()+" ");
     * desc.append(this.getPaymentSystem()+" ");
     * desc.append(this.getPaymentMethodID()+" ");
     * desc.append(this.getClientNo()+" ");
     * desc.append(this.getSourceSystem()+" ");
     * desc.append(this.getSourceSystemReferenceID()+" ");
     * desc.append((this.getStartDate
     * ()==null?"":this.getStartDate().toString())+" ");
     * desc.append((this.getEndDate
     * ()==null?"":this.getEndDate().toString())+" ");
     * desc.append(this.getAmountPayable()+" ");
     * desc.append(this.getUserID()+" ");
     * desc.append(this.getSalesBranchCode()+" "); // try // { //
     * desc.append(this.getAmountOutstanding() + " "); //from ss // } //
     * catch(ValidationException ex) // { // //do nothing // }
     * desc.append(this.getDescription()+"\n");
     * desc.append("=========================\n");
     * 
     * //don't fetch components and postings
     * 
     * return desc.toString(); }
     */

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public String toXML() throws SystemException {
	ArrayList excludedClassList = new ArrayList();
	return XMLHelper.toXML(excludedClassList, this);
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    /**
     * Description of the Method
     * 
     * @param cw
     *            Description of the Parameter
     * @param deepWrite
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("amountOutstanding", this.getAmountOutstanding());
	cw.writeAttribute("amountPayable", this.amountPayable);
	cw.writeAttribute("amountPending", this.amountPending);
	cw.writeAttribute("branchNumber", this.branchNumber);
	cw.writeAttribute("branchRegion", this.branchRegion);
	cw.writeAttribute("clientNo", this.clientNo);
	cw.writeAttribute("createDateTime", this.createDateTime);
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("endDate", this.endDate);
	cw.writeAttribute("userID", this.userID);
	cw.writeAttribute("payableItemID", this.payableItemID);
	cw.writeAttribute("paymentMethodDescription", this.paymentMethodDescription);
	cw.writeAttribute("paymentMethodID", this.paymentMethodID);
	cw.writeAttribute("productCode", this.productCode);
	cw.writeAttribute("salesBranchCode", this.salesBranchCode);
	cw.writeAttribute("sourceSystem", this.sourceSystem);
	cw.writeAttribute("sourceSystemReferenceID", this.sourceSystemReferenceID);
	cw.writeAttribute("startDate", this.startDate);

	if (deepWrite) {
	    cw.writeAttribute("paymentMethod", this.paymentMethod);

	    if (this.components != null) {
		cw.writeAttributeList("componentList", this.components);
	    }

	}
	cw.endClass();
    }

    @Override
    public String toString() {
	return "PayableItemVO [payableItemID=" + payableItemID + ", components=" + components + ", clientNo=" + clientNo + ", paymentMethod=" + paymentMethod + ", sourceSystemReferenceID=" + sourceSystemReferenceID + ", salesBranchCode=" + salesBranchCode + ", branchRegion=" + branchRegion + ", branchNumber=" + branchNumber + ", productCode=" + productCode + ", userID=" + userID + ", amountPayable=" + amountPayable + ", amountPending=" + amountPending + ", startDate=" + startDate + ", endDate=" + endDate + ", description=" + description + ", createDateTime=" + createDateTime + ", paymentMethodID=" + paymentMethodID + ", paymentMethodDescription=" + paymentMethodDescription + ", sourceSystem=" + sourceSystem + ", amountOutstanding=" + amountOutstanding + ", repairPaymentMethodID=" + repairPaymentMethodID + ", paymentSystem=" + paymentSystem + ", receiptNo=" + receiptNo + "]";
    }

}
