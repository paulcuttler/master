package com.ract.payment;

import java.rmi.RemoteException;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.payment.finance.Cheque;
import com.ract.payment.finance.FinanceMgrLocal;
import com.ract.payment.finance.FinancePayee;

public class ViewVoucherDetailsAction extends ActionSupport {

    // private Voucher voucher;

    private FinancePayee payee; // TODO payment object

    private Cheque cheque;

    private FinanceVoucher voucher;

    @InjectEJB(name = "FinanceMgrBean")
    private FinanceMgrLocal financeMgr;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    public Integer getDisposalId() {
	return disposalId;
    }

    public void setDisposalId(Integer disposalId) {
	this.disposalId = disposalId;
    }

    private Integer disposalId;

    private ClientVO client;

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    /**
     * @return
     */
    public String execute() {

	try {
	    voucher = financeMgr.getVoucher(disposalId);
	} catch (RemoteException e) {
	    addActionError(e.getMessage());
	}

	try {
	    client = clientMgr.getClient(new Integer(voucher.getClientNumber()));
	} catch (Exception e1) {
	    addActionError(e1.getMessage());
	}

	try {
	    payee = financeMgr.getPayee(voucher.getPayeeNo());
	    cheque = financeMgr.getCheque(voucher.getVoucherId());
	} catch (Exception e) {
	    addActionError(e.getMessage());
	}

	return SUCCESS;
    }

    public FinanceVoucher getVoucher() {
	return voucher;
    }

    public void setVoucher(FinanceVoucher creditDisposal) {
	this.voucher = creditDisposal;
    }

    public FinancePayee getPayee() {
	return payee;
    }

    public void setPayee(FinancePayee payee) {
	this.payee = payee;
    }

    public Cheque getCheque() {
	return cheque;
    }

    public void setCheque(Cheque cheque) {
	this.cheque = cheque;
    }
}