package com.ract.payment.notifier;

import java.rmi.RemoteException;

import com.ract.common.GenericException;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.util.LogUtil;

public class PaymentCancellationAdapter extends TransactionAdapterType implements TransactionAdapter {

    private PaymentTransactionMgr paymentTransactionMgr;

    public static final String ERROR_NON_EXISTENT_PAYABLE = "0 state";

    SourceSystem sourceSystem = null;

    Integer clientNumber = null;

    String userId;

    String sequenceNumber;

    public PaymentCancellationAdapter(String clientNumberStr, String sequenceNumberStr, String sourceSystemStr, String userIdStr)

    {
	sequenceNumber = sequenceNumberStr;
	userId = userIdStr;
	try {
	    sourceSystem = SourceSystem.getSourceSystem(sourceSystemStr);
	} catch (GenericException ex) {
	    // ignore
	}
	clientNumber = new Integer(clientNumberStr);

    }

    public void removePayableFee() throws RemoteException {
	try {
	    paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();

	    paymentTransactionMgr.removePayableFee(clientNumber, sequenceNumber, sourceSystem, userId);
	} catch (RollBackException rbe) {
	    if (rbe.toString().indexOf(ERROR_NON_EXISTENT_PAYABLE) > -1) {
		// do nothing - trying to undo a non existent payable. Let it go
		return;
	    } else {
		throw new SystemException(rbe);
	    }
	}

    }

    public void commit() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.commit();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    }
	}
    }

    public void rollback() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.rollback();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    }
	}
    }

    public void release() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.release();
	    } catch (Exception ex) {
		LogUtil.warn(this.getClass(), "Unable to release or remove paymentTransactionMgr. " + ex.getMessage());
	    }
	}
    }

    public String getSystemName() {
	return "Payment";
    }
}
