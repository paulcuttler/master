package com.ract.payment.notifier;

import java.util.Properties;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.SystemException;
import com.ract.common.notifier.NotificationEvent;
import com.ract.util.LogUtil;

/**
 * 
 * <p>
 * Notification from progress that a payment is to be undone in receptor
 * </p>
 * 
 * @author dgk
 * @version 1.0 13/11/2003
 */

public class PaymentCancellationNotificationEvent extends NotificationEvent {
    private transient Node clientNode;
    private String clientXML;

    private String clientNumber;
    private String sequenceNumber;
    private Properties[] properties;

    private static final String UNDO = "Undo";
    private static final String SEQUENCENO = "sequenceNo";
    private static final String CLIENTNO = "clientNo";
    private static final String SOURCESYSTEM = "sourceSystem";
    private static final String USER_NAME = "userName";

    private String sourceSystem;

    private String userName;

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public PaymentCancellationNotificationEvent(Element clientElement) throws SystemException {

	if (clientElement == null) {
	    throw new SystemException("The element passed to PaymentCancellationNotificationEvent event is null.");
	}

	if (!UNDO.equalsIgnoreCase(clientElement.getNodeName())) {
	    throw new SystemException("There are no 'UNDO' elements");
	}

	this.clientNode = clientElement;
	this.clientXML = this.clientNode.toString();

	this.clientNumber = checkNode(clientElement.getElementsByTagName(CLIENTNO), CLIENTNO).trim();
	this.sequenceNumber = checkNode(clientElement.getElementsByTagName(SEQUENCENO), SEQUENCENO).trim();

	this.sourceSystem = checkNode(clientElement.getElementsByTagName(SOURCESYSTEM), SOURCESYSTEM).trim();
	this.userName = checkNode(clientElement.getElementsByTagName(USER_NAME), USER_NAME).trim();

	LogUtil.debug(this.getClass(), "Got values: " + clientNumber + "/" + sequenceNumber);

	try {
	    properties = new Properties[1];
	    properties[0] = new Properties();
	} catch (Exception e) {
	    throw new SystemException("" + e);
	}

	setEventName(NotificationEvent.EVENT_PAYMENT_CANCELLATION);
	LogUtil.debug(this.getClass(), "EVENT_PAYMENT_CANCELLATION set");
    }

    private String checkNode(NodeList nodeList, String tag) throws SystemException {
	LogUtil.debug(this.getClass(), "Checking node: " + tag);
	if (nodeList != null && nodeList.getLength() > 0) {
	    if (nodeList.getLength() > 1) {
		throw new SystemException("The Pending element contains too many " + tag + " tags.");
	    } else {
		String value = null;
		try {

		    value = nodeList.item(0).getFirstChild().getNodeValue();
		    LogUtil.debug(this.getClass(), "" + tag + "= " + value);
		    return value;

		} catch (Exception e) {
		    throw new SystemException("The Pending element contains an invalid " + tag + " tag value (" + value + ").");
		}
	    }
	} else {
	    throw new SystemException("The Pending element does not contain a " + tag + " tag.");
	}
    }

    public String getClientNumber() {
	return this.clientNumber;
    }

    public String getSequenceNumber() {
	return this.sequenceNumber;
    }

    public String getSourceSystem() {
	return sourceSystem;
    }

    public String getUserName() {
	return userName;
    }

}
