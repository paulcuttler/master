package com.ract.payment.notifier;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.SystemException;
import com.ract.common.notifier.NotificationEvent;
import com.ract.util.LogUtil;

/**
 * Holds details about a payment event.
 * 
 * @author Sam McLennan, remodelled by jyh
 * @version 1.0 17 Jul 2003138
 */

public class PaymentNotificationEvent extends NotificationEvent {

    private transient Node paymentNode;
    private String paymentXML;

    private String clientNumber;
    private String reference;
    private Integer sequenceNumber;
    private String description;
    private double amountPaid;
    private double amountOutstanding;
    private String sourceSystem;
    private String receiptedBy;
    private String createdBy;
    private String receiptNo;
    private String branch;
    private String company;
    private String product;
    private String paymentSystem;

    // node name
    private final String PAYMENT = "Payment";

    private final String SEQUENCE_NO = "sequenceNumber";
    private final String CLIENT_NO = "clientNumber";
    private final String REF = "referenceNumber";
    private final String DESC = "description";
    private final String AMOUNT_PAID = "amountPaid";
    private final String AMOUNT_OUTSTANDING = "amountOutstanding";
    private final String SOURCE_SYSTEM = "sourceSystem";
    private final String RECEIPTED_BY = "receiptedBy";
    private final String CREATED_BY = "createdBy";
    private final String RECEIPTNO = "receiptNumber";
    private final String BRANCH = "branch";
    private final String PRODUCT = "product";
    private final String PAYMENT_SYSTEM = "paymentSystem";

    public PaymentNotificationEvent(Element clientElement) throws SystemException {

	if (clientElement == null) {
	    throw new SystemException("The element passed to PaymentDataNotification event is null.");
	}

	if (!PAYMENT.equalsIgnoreCase(clientElement.getNodeName())) {
	    throw new SystemException("There are no '" + PAYMENT + "' elements");
	}

	this.paymentNode = clientElement;
	this.paymentXML = this.paymentNode.toString();

	this.clientNumber = checkNode(clientElement.getElementsByTagName(CLIENT_NO), CLIENT_NO);
	this.reference = checkNode(clientElement.getElementsByTagName(REF), REF);
	this.sequenceNumber = Integer.valueOf(checkNode(clientElement.getElementsByTagName(SEQUENCE_NO), SEQUENCE_NO));
	this.description = checkNode(clientElement.getElementsByTagName(DESC), DESC);
	this.sourceSystem = checkNode(clientElement.getElementsByTagName(SOURCE_SYSTEM), SOURCE_SYSTEM);
	this.amountOutstanding = Double.parseDouble(checkNode(clientElement.getElementsByTagName(AMOUNT_OUTSTANDING), AMOUNT_OUTSTANDING));
	this.amountPaid = Double.parseDouble(checkNode(clientElement.getElementsByTagName(AMOUNT_PAID), AMOUNT_PAID));
	this.receiptedBy = checkNode(clientElement.getElementsByTagName(RECEIPTED_BY), RECEIPTED_BY);
	this.createdBy = checkNode(clientElement.getElementsByTagName(CREATED_BY), CREATED_BY);
	this.branch = checkNode(clientElement.getElementsByTagName(BRANCH), BRANCH);
	this.product = checkNode(clientElement.getElementsByTagName(PRODUCT), PRODUCT);
	this.receiptNo = checkNode(clientElement.getElementsByTagName(RECEIPTNO), RECEIPTNO);
	this.paymentSystem = checkNode(clientElement.getElementsByTagName(PAYMENT_SYSTEM), PAYMENT_SYSTEM);

	setEventName(NotificationEvent.EVENT_PAYMENT_NOTIFICATION);

    }

    /**
     * @todo too generic to be here
     */
    protected String checkNode(NodeList nodeList, String tag) throws SystemException {
	LogUtil.debug(this.getClass(), "Checking node: " + tag);
	if (nodeList != null && nodeList.getLength() > 0) {
	    if (nodeList.getLength() > 1) {
		throw new SystemException("The Payment element contains too many " + tag + " tags.");
	    } else {
		String value = null;
		try {
		    if (nodeList.item(0).hasChildNodes()) {
			value = nodeList.item(0).getFirstChild().getNodeValue();
		    }

		    LogUtil.debug(this.getClass(), "" + tag + "= " + value);
		    return value;

		} catch (Exception e) {
		    throw new SystemException("The Payment element contains an invalid " + tag + " tag value (" + value + ").");
		}
	    }
	} else {
	    throw new SystemException("The Payment element does not contain a " + tag + " tag.");
	}
    }

    public String getBranch() {
	return this.branch;
    }

    public String getClientNo() {
	return this.clientNumber;
    }

    public String getCompany() {
	return this.company;
    }

    public String getCreatedBy() {
	return this.createdBy;
    }

    public String getDescription() {
	return this.description;
    }

    public double getAmountOutstanding() {
	return this.amountOutstanding;
    }

    public double getAmountPaid() {
	return this.amountPaid;
    }

    public String getProduct() {
	return this.product;
    }

    public String getReceiptedBy() {
	return this.receiptedBy;
    }

    public String getReceiptNo() {
	return this.receiptNo;
    }

    public String getReference() {
	return this.reference;
    }

    public String getSourceSystem() {
	return this.sourceSystem;
    }

    public String getPaymentSystem() {
	return this.paymentSystem;
    }

    public Integer getSequenceNumber() {
	return sequenceNumber;
    }

}
