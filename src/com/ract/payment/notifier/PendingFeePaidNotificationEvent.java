package com.ract.payment.notifier;

import com.ract.common.notifier.NotificationEvent;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2003 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class PendingFeePaidNotificationEvent extends NotificationEvent {
}
