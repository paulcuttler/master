package com.ract.payment.notifier;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.SourceSystem;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationEventSubscriber;

/**
 * Process payment notification events. This class is used if the notification
 * is to be asynchronous.
 * 
 * @author Sam McLennan
 * @version 1.0 14/10/03
 */

public class NotificationEventSubscriberPaymentImpl implements NotificationEventSubscriber, Serializable {

    public NotificationEventSubscriberPaymentImpl() {
    }

    public void processNotificationEvent(NotificationEvent event) throws RemoteException {
	// delegate to helper
	PaymentNotificationHelper paymentNotificationHelper = new PaymentNotificationHelper();
	paymentNotificationHelper.processPaymentNotificationEvent(event, null);
    }

    public String getSubscriberName() {
	return SourceSystem.RECEPTOR.getAbbreviation();
    }
}
