package com.ract.payment.notifier;

import java.rmi.RemoteException;
import java.util.Properties;

import com.ract.common.CommonEJBHelper;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.util.LogUtil;

/**
 * <p>
 * 	Manage pending payments.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class PendingTransactionAdapter extends TransactionAdapterType implements TransactionAdapter {

	private PaymentTransactionMgr paymentTransactionMgr;

	Integer clientNumber;
	String sequenceNumber;
	String product;
	Double amountPayable;
	String description;
	String sourceSystem;
	String branchCode;
	String userId;
	Properties[] properties;

	public PendingTransactionAdapter(String clientNumberString, String sequenceNumber, String product, Double amountPayable, String description, String sourceSystem, String branchCode, String userName, Properties[] properties) {
		this.clientNumber = new Integer(clientNumberString);
		this.sequenceNumber = sequenceNumber;
		this.product = product;
		this.amountPayable = amountPayable;
		this.description = description;
		this.sourceSystem = sourceSystem;
		this.branchCode = branchCode;
		this.userId = userName;
	}

	public void createPayableFee() throws RemoteException {
		String receiptingSystem = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.RECEIPTING_SYSTEM + sourceSystem); // PAYRECSYS + eg. MEM

		// Make sure that RECEPTOR is the current receipting system being used
		if (!SourceSystem.RECEPTOR.getAbbreviation().equals(receiptingSystem)) {
			throw new SystemException("The current receipting system being used is '" + receiptingSystem + "' and not RECEPTOR.");
		}

		paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();
		try {
			paymentTransactionMgr.createPayableFee(clientNumber, sequenceNumber, product, amountPayable, description, sourceSystem, branchCode, userId, properties);
		} catch (RollBackException ex) {
			throw new SystemException(ex);
		}
	}

	public void commit() throws SystemException {
		if (paymentTransactionMgr != null) {
			try {
				paymentTransactionMgr.commit();
			} catch (RemoteException ex) {
				throw new SystemException(ex);
			}
		}
	}

	public void rollback() throws SystemException {
		if (paymentTransactionMgr != null) {
			try {
				paymentTransactionMgr.rollback();
			} catch (RemoteException ex) {
				throw new SystemException(ex);
			}
		}
	}

	public void release() throws SystemException {
		if (paymentTransactionMgr != null) {
			try {
				paymentTransactionMgr.release();
			} catch (Exception ex) {
				LogUtil.warn(this.getClass(), "Unable to release or remove paymentTransactionMgr. " + ex.getMessage());
			}
		}
	}

	public String getSystemName() {
		return "Pending";
	}
}
