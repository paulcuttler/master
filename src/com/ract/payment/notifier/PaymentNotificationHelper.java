package com.ract.payment.notifier;

import java.math.BigDecimal;
import java.rmi.RemoteException;

import com.ract.common.GenericException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationRegister;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.common.transaction.TransactionAdapterList;
import com.ract.fleet.FleetAdapter;
import com.ract.fleet.FleetFactory;
import com.ract.insurance.InsuranceAdapter;
import com.ract.insurance.InsuranceFactory;
import com.ract.membership.notifier.MembershipPaymentAdapter;
import com.ract.payment.receipting.ReceiptingAdapter;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.vehicleinspection.VehicleInspectionAdapter;
import com.ract.vehicleinspection.VehicleInspectionFactory;

/**
 * Helper for notification events
 * 
 * @author jyh
 * @version 1.0
 */

public class PaymentNotificationHelper {
    /**
     * Maintain list of discrete adapters.
     */
    // private TransactionAdapterList txAdapterList;

    /**
     * Process payment notification events that will update a source system.
     * 
     * @param event
     *            NotificationEvent
     * @throws RemoteException
     */
    public TransactionAdapter processPaymentNotificationEvent(NotificationEvent event, TransactionAdapterList transactionAdapterList) throws RemoteException {
	LogUtil.debug(this.getClass(), "processPaymentNotificationEvent start");

	LogUtil.debug(this.getClass(), "processPaymentNotificationEvent transactionAdapterList=" + transactionAdapterList.toString());
	LogUtil.debug(this.getClass(), "transactionAdapterList size=" + transactionAdapterList.size());

	TransactionAdapter transactionAdapter = null;
	if (event != null) {

	    LogUtil.debug(this.getClass(), "event=" + event.getEventName());

	    if (event.eventEquals(NotificationEvent.EVENT_PAYMENT_NOTIFICATION)) {

		PaymentNotificationEvent paymentNotificationEvent = (PaymentNotificationEvent) event;

		final String KEY_NOTIFICATION = paymentNotificationEvent.getReceiptNo() + "/" + paymentNotificationEvent.getClientNo() + " - " + (new DateTime()).formatLongDate();

		NotificationRegister.addNotificationKey(KEY_NOTIFICATION);
		LogUtil.debug(this.getClass(), "SourceSystem=" + paymentNotificationEvent.getSourceSystem());

		if (paymentNotificationEvent.getSourceSystem().equalsIgnoreCase(SourceSystem.VI.getAbbreviation())) {
		    transactionAdapter = notifyVehicleInspection(transactionAdapterList, paymentNotificationEvent.getReceiptNo(), paymentNotificationEvent.getClientNo(), getSequence(paymentNotificationEvent.getReference()), paymentNotificationEvent.getAmountPaid(), paymentNotificationEvent.getReceiptedBy(), paymentNotificationEvent.getBranch());
		}
		// accept INS or INS_RAC
		else if (paymentNotificationEvent.getSourceSystem().equalsIgnoreCase(SourceSystem.INSURANCE.getAbbreviation()) || paymentNotificationEvent.getSourceSystem().equalsIgnoreCase(SourceSystem.INSURANCE_RACT.getAbbreviation())) {
		    transactionAdapter = notifyInsurance(transactionAdapterList, paymentNotificationEvent.getReceiptNo(), paymentNotificationEvent.getClientNo(), getSequence(paymentNotificationEvent.getReference()), paymentNotificationEvent.getAmountPaid(), paymentNotificationEvent.getReceiptedBy(), paymentNotificationEvent.getBranch(), SourceSystem.INSURANCE_RACT.getAbbreviation());
		} else if (paymentNotificationEvent.getSourceSystem().equalsIgnoreCase(SourceSystem.FLEET.getAbbreviation())) {
		    transactionAdapter = notifyFleet(transactionAdapterList, paymentNotificationEvent.getReceiptNo(), paymentNotificationEvent.getClientNo(), getSequence(paymentNotificationEvent.getReference()), paymentNotificationEvent.getAmountPaid(), paymentNotificationEvent.getReceiptedBy(), paymentNotificationEvent.getBranch());
		} else if (paymentNotificationEvent.getSourceSystem().equalsIgnoreCase(SourceSystem.MEMBERSHIP.getAbbreviation())) {

		    SourceSystem paymentSystem = null;
		    SourceSystem sourceSystem = null;
		    try {
			paymentSystem = SourceSystem.getSourceSystem(paymentNotificationEvent.getPaymentSystem());
			sourceSystem = SourceSystem.getSourceSystem(paymentNotificationEvent.getSourceSystem());
		    } catch (GenericException ex1) {
			// ignore
		    }
		    LogUtil.debug(this.getClass(), "payment system=" + paymentSystem.getAbbreviation());
		    LogUtil.debug(this.getClass(), "source system=" + sourceSystem.getAbbreviation());

		    transactionAdapter = notifyMembership(paymentNotificationEvent, paymentSystem, sourceSystem);
		} else if (paymentNotificationEvent.getSourceSystem().equalsIgnoreCase(SourceSystem.FIN.getAbbreviation())) {
		    LogUtil.warn(this.getClass(), "No adapter is associated with the finance event.");
		    // ignore
		} else {
		    throw new SystemException("Unexpected source system received from RECEPTOR" + paymentNotificationEvent.getSourceSystem());
		}
		NotificationRegister.removeNotificationKey(KEY_NOTIFICATION);
	    } else if (event.eventEquals(NotificationEvent.EVENT_PAYMENT_CANCELLATION)) {

		PaymentCancellationNotificationEvent pcnEvent = (PaymentCancellationNotificationEvent) event;
		PaymentCancellationAdapter paymentCancellationAdapter = new PaymentCancellationAdapter(pcnEvent.getClientNumber(), pcnEvent.getSequenceNumber(), pcnEvent.getSourceSystem(), pcnEvent.getUserName());
		try {
		    paymentCancellationAdapter.removePayableFee();
		} catch (Exception ex) {
		    paymentCancellationAdapter.setRollbackOnly(ex);
		}
		transactionAdapter = paymentCancellationAdapter;

	    } else {
		throw new SystemException("Unexpected event '" + event + "'.");
	    }

	} else {
	    throw new SystemException("Event is null.");
	}
	LogUtil.debug(this.getClass(), "transactionAdapter=" + (transactionAdapter == null ? "null" : transactionAdapter.getSystemName()));
	LogUtil.debug(this.getClass(), "processPaymentNotificationEvent end");
	return transactionAdapter;
    }

    private MembershipPaymentAdapter notifyMembership(PaymentNotificationEvent paymentNotificationEvent, SourceSystem paymentSystem, SourceSystem sourceSystem) throws RemoteException {
	LogUtil.debug(this.getClass(), "processPaymentNotificationEvent start");
	MembershipPaymentAdapter membershipPaymentAdapter = new MembershipPaymentAdapter(paymentSystem, paymentNotificationEvent.getReference(), paymentNotificationEvent.getClientNo(), paymentNotificationEvent.getSequenceNumber(), paymentNotificationEvent.getAmountPaid(), paymentNotificationEvent.getAmountOutstanding(), paymentNotificationEvent.getDescription(), paymentNotificationEvent.getCreatedBy(), paymentNotificationEvent.getReceiptedBy(), sourceSystem);
	LogUtil.debug(this.getClass(), "processPaymentNotificationEvent end");
	return membershipPaymentAdapter;
    }

    private TransactionAdapter notifyFleet(TransactionAdapterList transactionAdapterList, String receiptNo, String clientNo, Integer sequence, double paid, String receiptedBy, String branch) {
	BigDecimal pay = new BigDecimal(paid);
	LogUtil.debug(this.getClass(), "Notifying fleet of payment:" + receiptNo + clientNo + sequence + pay + receiptedBy + branch);

	FleetAdapter fleetAdapter = (FleetAdapter) getTransactionAdapter(TransactionAdapter.ADAPTER_FLEET, transactionAdapterList);
	try {
	    fleetAdapter.notifyFleetPayment(Integer.valueOf(receiptNo), Integer.valueOf(clientNo), sequence, pay, receiptedBy, branch);
	} catch (Exception ex) {
	    fleetAdapter.setRollbackOnly(ex);
	}
	return fleetAdapter;
    }

    private TransactionAdapter notifyInsurance(TransactionAdapterList transactionAdapterList, String receiptNo, String clientNo, Integer sequence, double paid, String receiptedBy, String branch, String company) {
	BigDecimal amountPaid = new BigDecimal(paid);
	LogUtil.debug(this.getClass(), "receiptNo=" + receiptNo);
	LogUtil.debug(this.getClass(), "clientNo=" + clientNo);
	LogUtil.debug(this.getClass(), "paid=" + paid);
	LogUtil.debug(this.getClass(), "receiptedBy=" + receiptedBy);
	LogUtil.debug(this.getClass(), "branch=" + branch);
	LogUtil.debug(this.getClass(), "company=" + company);
	InsuranceAdapter insuranceAdapter = (InsuranceAdapter) getTransactionAdapter(TransactionAdapter.ADAPTER_INSURANCE, transactionAdapterList);
	try {
	    insuranceAdapter.notifyPremiumPayment(Integer.valueOf(receiptNo), Integer.valueOf(clientNo), sequence, amountPaid, receiptedBy, branch, company);
	} catch (Exception ex) {
	    insuranceAdapter.setRollbackOnly(ex);
	}
	return insuranceAdapter;
    }

    private TransactionAdapter notifyVehicleInspection(TransactionAdapterList transactionAdapterList, String receiptNo, String clientNo, Integer sequence, double paid, String receiptedBy, String branch) {
	BigDecimal amountPaid = new BigDecimal(paid);

	VehicleInspectionAdapter vehicleInsuranceAdapter = (VehicleInspectionAdapter) getTransactionAdapter(TransactionAdapter.ADAPTER_VI, transactionAdapterList);
	try {
	    vehicleInsuranceAdapter.notifyVehicleInspectionPayment(Integer.valueOf(receiptNo), Integer.valueOf(clientNo), sequence, amountPaid, receiptedBy, branch);
	} catch (Exception ex) {
	    vehicleInsuranceAdapter.setRollbackOnly(ex);
	}
	return vehicleInsuranceAdapter;
    }

    private Integer getSequence(String reference) throws SystemException {
	Integer i = Integer.valueOf(reference.substring(reference.indexOf(ReceiptingAdapter.SEPARATOR) + 1));
	return i;
    }

    private TransactionAdapter getTransactionAdapter(String adapterType, TransactionAdapterList transactionAdapterList) {
	// LogUtil.log(this.getClass(),"getTransactionAdapter transactionAdapterList="+transactionAdapterList.toString());
	TransactionAdapter transactionAdapter = transactionAdapterList.getTransactionAdapter(adapterType);

	// create new instances if we don't already have a reference in the list
	// or a list at all
	/**
	 * @todo this should be part of a factory method elsewhere
	 */
	if (transactionAdapter == null) {
	    if (adapterType.equals(TransactionAdapter.ADAPTER_FLEET)) {
		transactionAdapter = FleetFactory.getFleetAdapter();
	    } else if (adapterType.equals(TransactionAdapter.ADAPTER_INSURANCE)) {
		transactionAdapter = InsuranceFactory.getInsuranceAdapter();
	    } else if (adapterType.equals(TransactionAdapter.ADAPTER_VI)) {
		transactionAdapter = VehicleInspectionFactory.getVehicleInspectionAdapter();
	    }
	}
	return transactionAdapter;
    }

}
