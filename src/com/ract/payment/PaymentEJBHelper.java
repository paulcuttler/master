package com.ract.payment;

import java.rmi.RemoteException;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;
import com.ract.payment.billpay.BillPayMgr;
import com.ract.payment.bpay.BPAYMgr;
import com.ract.payment.electronicpayment.EftPaymentMgr;
import com.ract.payment.finance.FinanceMgr;
import com.ract.payment.finance.FinanceMgrLocal;
import com.ract.payment.ocv.OCVTransactionMgr;

/**
 * @author hollidayj
 * @created 31 July 2002
 * @todo Description of the Class
 */
public class PaymentEJBHelper {
    /**
     * The PaymentTransactionMgr session bean is stateful so a reference to it
     * cannot be kept here. A new reference to it must be established every time
     * it is requested. Can keep a reference to the PaymentTransactionMgrHome
     * interface though.
     */

    /**
     * Return a reference to the Payable Item Component Store home interface.
     * 
     * @return The billPayMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static BillPayMgr getBillPayMgr() {
	BillPayMgr billPayMgr = null;
	try {
	    billPayMgr = (BillPayMgr) ServiceLocator.getInstance().getObject("BillPayMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return billPayMgr;
    }

    public static OCVTransactionMgr getOCVTransactionMgr() {
	OCVTransactionMgr ocvTransactionMgr = null;
	try {
	    ocvTransactionMgr = (OCVTransactionMgr) ServiceLocator.getInstance().getObject("OCVTransactionMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return ocvTransactionMgr;
    }

    /**
     * Return a reference to the BPAY Manager home interface.
     * 
     * @return The BPAYMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static BPAYMgr getBPAYMgr() {
	BPAYMgr bPAYMgr = null;
	try {
	    bPAYMgr = (BPAYMgr) ServiceLocator.getInstance().getObject("BPAYMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return bPAYMgr;
    }

    /**
     * Get the remote inteface to the payment transaction mgr
     */
    public static PaymentTransactionMgrLocal getPaymentTransactionMgrLocal() {
	PaymentTransactionMgrLocal paymentTransactionMgrLocal = null;
	try {
	    paymentTransactionMgrLocal = (PaymentTransactionMgrLocal) ServiceLocator.getInstance().getObject("PaymentTransactionMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return paymentTransactionMgrLocal;
    }

    public static PaymentTransactionMgr getPaymentTransactionMgr() {
	PaymentTransactionMgr paymentTransactionMgr = null;
	try {
	    paymentTransactionMgr = (PaymentTransactionMgr) ServiceLocator.getInstance().getObject("PaymentTransactionMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return paymentTransactionMgr;
    }

    /**
     * Return a reference to the Payable Item Component Store home interface.
     * 
     * @return The paymentMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static PaymentMgr getPaymentMgr() {
	PaymentMgr paymentMgr = null;
	try {
	    paymentMgr = (PaymentMgr) ServiceLocator.getInstance().getObject("PaymentMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return paymentMgr;
    }

    public static PaymentMgrLocal getPaymentMgrLocal() {
	PaymentMgrLocal paymentMgrLocal = null;
	try {
	    paymentMgrLocal = (PaymentMgrLocal) ServiceLocator.getInstance().getObject("PaymentMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return paymentMgrLocal;
    }

    /**
     * Return a reference to the Payment ID Manager remote interface.
     * 
     * @return The paymentIDMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static PaymentIDMgr getPaymentIDMgr() {
	PaymentIDMgr paymentIDMgr = null;
	try {
	    paymentIDMgr = (PaymentIDMgr) ServiceLocator.getInstance().getObject("PaymentIDMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return paymentIDMgr;
    }

    /**
     * @return The journalMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static FinanceMgr getFinanceMgr() {
	FinanceMgr financeMgr = null;
	try {
	    financeMgr = (FinanceMgr) ServiceLocator.getInstance().getObject("FinanceMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return financeMgr;
    }

    public static FinanceMgrLocal getFinanceMgrLocal() {
	FinanceMgrLocal financeMgrLocal = null;
	try {
	    financeMgrLocal = (FinanceMgrLocal) ServiceLocator.getInstance().getObject("FinanceMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return financeMgrLocal;
    }

    public static EftPaymentMgr getEftPaymentMgr() {
	EftPaymentMgr eftPaymentMgr = null;
	try {
	    eftPaymentMgr = (EftPaymentMgr) ServiceLocator.getInstance().getObject("EftPaymentMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return eftPaymentMgr;
    }
}
