package com.ract.payment;

import java.io.Serializable;

import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.receipting.ReceiptingPaymentMethod;

/**
 * Abstract class. Concrete subclasses represent various payment methods
 * 
 * @author Terry Bakker
 * @created 21/08/2002
 * @version 1.0
 */

public abstract class PaymentMethod implements Serializable {
    /**
     * The string used to identify a receipting payment method.
     */
    public final static String RECEIPTING = "Receipting";

    /**
     * The string used to identify a direct debit payment method.
     */
    public final static String DIRECT_DEBIT = "Direct Debit";

    protected Integer clientNumber;

    protected String salesBranchCode;

    protected SourceSystem sourceSystem;

    protected Integer sourceSystemReference;

    protected String userID;

    protected PaymentTypeVO paymentType;

    protected boolean validate = true;

    private Exception paymentMethodException;

    public PaymentMethod() {

    }

    public PaymentMethod(Integer cNum, SourceSystem ss) {
	setClientNumber(cNum);
	setSourceSystem(ss);
    }

    public PaymentMethod(Integer clientNumber, String userID, String salesBranchCode, SourceSystem sourceSystem) {
	this.clientNumber = clientNumber;
	this.userID = userID;
	this.salesBranchCode = salesBranchCode;
	this.sourceSystem = sourceSystem;
    }

    public Integer getClientNumber() {
	return this.clientNumber;
    }

    /**
     * The class that extends this one defines what the payment method ID is.
     */
    public abstract String getPaymentMethodID();

    public abstract void setPaymentMethodID(String payMethodID) throws SystemException;

    public String getSalesBranchCode() {
	return this.salesBranchCode;
    }

    public SourceSystem getSourceSystem() {
	return this.sourceSystem;
    }

    public Integer getSourceSystemReference() {
	return this.sourceSystemReference;
    }

    public String getUserID() {
	return this.userID;
    }

    public Exception getPaymentMethodException() {
	return paymentMethodException;
    }

    /**
     * Return true if the payment method has significantly changed. The sub
     * classes (DirectDebitPaymentMethod and ReceiptingPaymentMethod) override
     * this method and provide the real implementation.
     */
    public boolean hasSignificantlyChanged(PaymentMethod paymentMethod) {
	return false;
    }

    public boolean isDirectDebitPaymentMethod() {
	return this instanceof DirectDebitSchedule;
    }

    public boolean isReceiptingPaymentMethod() {
	return this instanceof ReceiptingPaymentMethod;
    }

    /**
     * Return a dscription of the payment type. e.g. "Receipting" or
     * "Direct Debit"
     */
    public abstract String getDescription();

    public void setClientNumber(Integer cNum) {
	this.clientNumber = cNum;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public void setSourceSystem(SourceSystem ss) {
	this.sourceSystem = ss;
    }

    public void setSourceSystemReference(Integer ssr) {
	this.sourceSystemReference = ssr;
    }

    public void setUserID(String userID) {
	this.userID = userID;
    }

    public void setValidate(boolean validate) {
	this.validate = validate;
    }

    public void setPaymentMethodException(Exception paymentMethodException) {
	this.paymentMethodException = paymentMethodException;
    }
}
