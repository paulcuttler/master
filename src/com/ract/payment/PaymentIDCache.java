package com.ract.payment;

import java.rmi.RemoteException;

import org.apache.log4j.Category;

import com.ract.util.LogUtil;

public class PaymentIDCache {

    private static PaymentIDCache instance = new PaymentIDCache();

    private int lastPayableItemID = 0;

    private int lastPayableItemActionID = 0;

    private int lastPayableItemComponentID = 0;

    private int lastPayableItemPostingID = 0;

    private int lastJournalNum = 0;

    /**
     * Return a reference to the one and only instance of this class.
     */
    public static PaymentIDCache getInstance() {
	return instance;
    }

    /**
     * Return the next ID in the Payable Item ID sequence. Must be synchronised
     * so that two threads don't do the initialisation at the same time.
     */
    public synchronized int getNextPayableItemID(PaymentIDMgrBean idMgr) throws RemoteException {
	LogUtil.log(this.getClass(), "idMgr=" + idMgr);
	if (this.lastPayableItemID == 0) {
	    this.lastPayableItemID = idMgr.getLastPayableItemID();
	}
	this.lastPayableItemID++;
	// log("Next payableItemID="+this.lastPayableItemID);
	return this.lastPayableItemID;
    }

    /**
     * Return the next ID in the Payable Item Posting ID sequence. Must be
     * synchronised so that two threads don't do the initialisation at the same
     * time.
     */
    public synchronized int getNextPayableItemPostingID(PaymentIDMgrBean idMgr) throws RemoteException {
	if (this.lastPayableItemPostingID == 0) {
	    this.lastPayableItemPostingID = idMgr.getLastPayableItemPostingID();
	}
	this.lastPayableItemPostingID++;
	// log("Next payableItemPostingID="+this.lastPayableItemPostingID);
	return this.lastPayableItemPostingID;
    }

    /**
     * Return the next ID in the Payable Item Action ID sequence. Must be
     * synchronised so that two threads don't do the initialisation at the same
     * time.
     */
    public synchronized int getNextPayableItemActionID(PaymentIDMgrBean idMgr) throws RemoteException {
	if (this.lastPayableItemActionID == 0) {
	    this.lastPayableItemActionID = idMgr.getLastPayableItemActionID();
	}
	this.lastPayableItemActionID++;
	// log("Next payableItemActionID="+this.lastPayableItemActionID);
	return this.lastPayableItemActionID;
    }

    /**
     * Return the next journal number. Initialise it if it has not already been
     * initialised.
     */
    public synchronized int getNextJournalNumber(PaymentIDMgrBean idMgr) throws RemoteException {
	if (this.lastJournalNum == 0) {
	    this.lastJournalNum = idMgr.getLastJournalNumber();
	}
	this.lastJournalNum++;
	// log("Next journalNumber="+this.lastJournalNum);
	return lastJournalNum;
    }

    /**
     * Return the next ID in the Payable Item ID sequence. Must be synchronised
     * so that two threads don't do the initialisation at the same time.
     */
    public synchronized int getNextPayableItemComponentID(PaymentIDMgrBean idMgr) throws RemoteException {
	if (this.lastPayableItemComponentID == 0) {
	    this.lastPayableItemComponentID = idMgr.getLastPayableItemComponentID();
	}
	this.lastPayableItemComponentID++;
	// log("Next payableItemComponentID="+this.lastPayableItemComponentID);
	return this.lastPayableItemComponentID;
    }

    /**
     * Reset all IDs so that they are initialised again.
     */
    public void reset() {
	this.lastPayableItemID = 0;
	this.lastPayableItemActionID = 0;
	this.lastPayableItemComponentID = 0;
	this.lastPayableItemPostingID = 0;
	this.lastJournalNum = 0;
    }

    private void log(String msg) {
	Category cat = Category.getInstance(this.getClass().getName());
	cat.info(msg);
    }
}