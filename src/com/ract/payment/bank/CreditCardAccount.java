package com.ract.payment.bank;

import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentTypeVO;
import com.ract.util.DateTime;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

public class CreditCardAccount extends BankAccount implements Writable {
    public static final String WRITABLE_CLASSNAME = "CreditCardAccount";

    private String cardTypeDescription;

    private String cardNumber;

    private String cardExpiry;

    private String verificationValue;

    private PaymentTypeVO paymentType;

    /**
     * Constructor
     */
    public CreditCardAccount() {
    }

    public CreditCardAccount(Node creditCardAccountNode) {
	NodeList elementList = creditCardAccountNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("accountName".equals(nodeName) && valueText != null) {
		this.accountName = valueText;
	    } else if ("cardExpiry".equals(nodeName) && valueText != null) {
		this.cardExpiry = valueText;
	    } else if ("cardNumber".equals(nodeName) && valueText != null) {
		this.cardNumber = valueText;
	    } else if ("cardTypeDescription".equals(nodeName) && valueText != null) {
		this.cardTypeDescription = valueText;
	    } else if ("paymentType".equals(nodeName) && writableNode != null) {
		this.paymentType = new PaymentTypeVO(writableNode);
	    } else if ("verificationValue".equals(nodeName) && valueText != null) {
		this.verificationValue = valueText;
	    }

	}

    }

    public String getAccountType() {
	return "Credit card account";
    }

    public String getCardTypeDescription() {
	return this.cardTypeDescription;
    }

    public String getCardNumber() {
	return this.cardNumber;
    }

    public String getCardExpiry() {
	return this.cardExpiry;
    }

    public PaymentTypeVO getPaymentType() {
	return this.paymentType;
    }

    public String getVerificationValue() {
	return this.verificationValue;
    }

    /**
     * Return true if the bank account details have significantly changed. The
     * bank account has significantly changed if the credit card number is
     * different.
     */
    public boolean hasSignificantlyChanged(BankAccount bankAccount) {
	if (bankAccount == null) {
	    return true;
	}

	if (!(bankAccount instanceof CreditCardAccount)) {
	    return true;
	}

	CreditCardAccount cca = (CreditCardAccount) bankAccount;

	if (this.cardNumber != null && !this.cardNumber.equals(cca.getCardNumber())) {
	    return true;
	}

	if (cca.getCardNumber() != null && !cca.getCardNumber().equals(this.cardNumber)) {
	    return true;
	}

	return super.hasSignificantlyChanged(bankAccount);
    }

    public void setPaymentType(PaymentTypeVO payType) throws SystemException {
	if (this.validate) {
	    if (payType == null) {
		throw new SystemException("The payment type cannot be null.");
	    }
	}
	this.paymentType = payType;

	if (this.paymentType != null) {
	    this.cardTypeDescription = this.paymentType.getDescription();
	}
    }

    public void setCardNumber(String cardNumber) throws SystemException, ValidationException {
	if (this.validate) {
	    if (this.paymentType == null) {
		throw new SystemException("A payment type must be specified before the card number.");
	    }

	    if (cardNumber == null) {
		throw new SystemException("The card number cannot be null.");
	    }

	    // trim, replace all spaces and dashes with no spaces
	    cardNumber = cardNumber.trim();
	    cardNumber = StringUtil.replaceAll(cardNumber, " ", "");
	    cardNumber = StringUtil.replaceAll(cardNumber, "-", "");

	    if (cardNumber.length() < 1) {
		throw new ValidationException("The card number must be specified.");
	    }

	    if (this.paymentType.getCardNumberLength() > 0 && cardNumber.length() != this.paymentType.getCardNumberLength()) {
		throw new ValidationException("The card number must contain exactly " + this.paymentType.getCardNumberLength() + " digits.");
	    }

	    // validate number against known types
	    String ccType = null;
	    try {
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		ccType = paymentMgr.validateCreditCardType(cardNumber);
		if (!ccType.equals(paymentType.getPaymentTypeCode())) {
		    throw new ValidationException("The credit card number '" + cardNumber + "' is not valid for a " + cardTypeDescription.toUpperCase() + " credit card.  Please refer to the credit card guidelines.");
		}
	    } catch (RemoteException ex) {
		throw new SystemException(ex.getMessage());
	    }

	}
	this.cardNumber = cardNumber;
    }

    public void setCardExpiry(String cardExpiry) throws SystemException, ValidationException {
	if (this.validate) {
	    // Validate the card expiry date
	    if (cardExpiry == null) {
		throw new SystemException("The card expiry cannot be null.");
	    }

	    cardExpiry = cardExpiry.trim();
	    cardExpiry = StringUtil.replaceAll(cardExpiry, " ", "/");
	    cardExpiry = StringUtil.replaceAll(cardExpiry, "-", "/");
	    if (cardExpiry.length() < 1) {
		throw new ValidationException("A card expiry must be specified.");
	    }

	    String monthStr = null;
	    String yearStr = null;
	    int slashPos = cardExpiry.indexOf("/");
	    if (slashPos < 0) {
		if (cardExpiry.length() == 4) {
		    // Assume the first two characters are the month and the
		    // last two are the year.
		    monthStr = cardExpiry.substring(0, 2);
		    yearStr = cardExpiry.substring(3);
		    StringBuffer sb = new StringBuffer(monthStr);
		    sb.append("/");
		    sb.append(yearStr);
		    cardExpiry = sb.toString();
		} else {
		    throw new ValidationException("The credit card expiry must be in the format \"mm/yy\".");
		}
	    } else {
		monthStr = cardExpiry.substring(0, slashPos);
		yearStr = cardExpiry.substring(slashPos + 1);
	    }

	    int month = 0;
	    try {
		month = Integer.parseInt(monthStr);
	    } catch (Exception e) {
		throw new ValidationException("The credit card expiry month '" + monthStr + "' is not a valid number.");
	    }
	    if (month < 1 || month > 12) {
		throw new ValidationException("The credit card expiry month '" + monthStr + "' must be between 1 and 12.");
	    }

	    int year = 0;
	    try {
		year = Integer.parseInt(yearStr);
	    } catch (Exception e) {
		throw new ValidationException("The credit card expiry year '" + yearStr + "' is not a valid number.");
	    }

	    // Make sure that the cards expiry year is not before the current
	    // year.
	    DateTime today = new DateTime();
	    int todaysYear = today.getDateYear() - 2000; // Only want the two
							 // digit year. This
							 // should work for the
							 // next 98 years!
	    if (year < todaysYear) {
		throw new ValidationException("The credit cards expiry year " + yearStr + " is before the current year. The credit card has expired.");
	    }
	}
	this.cardExpiry = cardExpiry;
    }

    public void setVerificationValue(String verificationValue) {
	this.verificationValue = verificationValue;
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("accountName", this.accountName);
	cw.writeAttribute("cardExpiry", this.cardExpiry);
	cw.writeAttribute("cardNumber", this.cardNumber);
	cw.writeAttribute("cardTypeDescription", this.cardTypeDescription);
	cw.writeAttribute("paymentType", this.paymentType);
	cw.writeAttribute("verificationValue", this.verificationValue);
	cw.endClass();
    }

    public String getCardTypeCode() {
	String cardTypeCode = null;
	if (this.paymentType != null) {
	    cardTypeCode = this.paymentType.getPaymentTypeCode();
	}
	return cardTypeCode;
    }
}
