package com.ract.payment.bank;

import java.io.Serializable;

import com.ract.common.SystemException;
import com.ract.common.ValidationException;

public abstract class BankAccount implements Serializable {

    protected String accountName;

    protected boolean validate = true;

    // /**
    // * Constructor
    // */
    // public BankAccount()
    // {
    //
    // }

    public String getAccountName() {
	return this.accountName;
    }

    /**
     * Return a descriptive name for the type of bank account.
     */
    public abstract String getAccountType();

    /**
     * Return true if the bank account has significantly changed. At this level
     * the account name is the only thing that can change and this is not
     * significant. The sub classes (DebitAccount and CreditCardAccount)
     * override this method and do the real checking.
     */
    public boolean hasSignificantlyChanged(BankAccount bankAccount) {
	return false;
    }

    public void setAccountName(String accName) throws SystemException, ValidationException {
	if (this.validate) {
	    if (accName == null) {
		throw new SystemException("The account name cannot be null.");
	    }

	    accName = accName.trim();

	    if (accName.length() < 1) {
		throw new ValidationException("The account name must be specified.");
	    }
	}
	this.accountName = accName;
    }

    public void setValidate(boolean validate) {
	this.validate = validate;
    }

}