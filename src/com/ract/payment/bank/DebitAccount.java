package com.ract.payment.bank;

import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

public class DebitAccount extends BankAccount implements Writable {
    public static final String WRITABLE_CLASSNAME = "DebitAccount";

    private String bsbNumber;

    private String accountNumber;

    /**
     * Constructor
     */
    public DebitAccount(String bsb, String accNumber, String accName, boolean validate) throws RemoteException, ValidationException {
	this.validate = validate;
	setBsbNumber(bsb);
	setAccountNumber(accNumber);
	setAccountName(accName);
    }

    public DebitAccount(String bsb, String accNumber, String accName) throws RemoteException, ValidationException {
	setBsbNumber(bsb);
	setAccountNumber(accNumber);
	setAccountName(accName);
    }

    public DebitAccount(Node debitAccountNode) {
	NodeList elementList = debitAccountNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("accountName".equals(nodeName) && valueText != null) {
		this.accountName = valueText;
	    } else if ("accountNumber".equals(nodeName) && valueText != null) {
		this.accountNumber = valueText;
	    } else if ("bsbNumber".equals(nodeName) && valueText != null) {
		this.bsbNumber = valueText;
	    }

	}
    }

    public String getAccountType() {
	return "Debit account";
    }

    public String getBsbNumber() {
	return this.bsbNumber;
    }

    public String getAccountNumber() {
	return this.accountNumber;
    }

    /**
     * Return true if the bank account has significantly changed. The debit
     * account has significantly changed if the bsb number or account number is
     * different.
     */
    public boolean hasSignificantlyChanged(BankAccount bankAccount) {
	if (bankAccount == null) {
	    return true;
	}

	if (!(bankAccount instanceof DebitAccount)) {
	    return true;
	}

	DebitAccount da = (DebitAccount) bankAccount;

	if (this.bsbNumber != null && !this.bsbNumber.equals(da.getBsbNumber())) {
	    return true;
	}

	if (da.getBsbNumber() != null && !da.getBsbNumber().equals(this.bsbNumber)) {
	    return true;
	}

	if (this.accountNumber != null && !this.accountNumber.equals(da.getAccountNumber())) {
	    return true;
	}

	if (da.getAccountNumber() != null && !da.getAccountNumber().equals(this.accountNumber)) {
	    return true;
	}

	return super.hasSignificantlyChanged(bankAccount);
    }

    public void setBsbNumber(String bsbNumber) throws RemoteException, ValidationException {
	if (this.validate) {
	    PaymentMgr payMgr = PaymentEJBHelper.getPaymentMgr();
	    try {
		String bankCode = payMgr.validateBsbNumber(bsbNumber);
	    } catch (Exception e) {
		throw new ValidationException(e.getMessage());
	    }
	}
	this.bsbNumber = bsbNumber;
    }

    public void setAccountNumber(String accountNumber) throws SystemException, ValidationException {
	if (this.validate) {
	    if (accountNumber == null) {
		throw new SystemException("The account number cannot be null.");
	    }

	    accountNumber = StringUtil.replaceAll(accountNumber, " ", "");

	    if (accountNumber.length() < 1) {
		throw new ValidationException("The account number must be specified.");
	    }

	    if (accountNumber.length() > 9) {
		throw new ValidationException("The account number cannot be more than 9 digits.");
	    }

	    if (!StringUtil.isInteger(accountNumber)) {
		throw new ValidationException("The account number can only contain digits.");
	    }

	}
	this.accountNumber = accountNumber;
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("accountName", this.accountName);
	cw.writeAttribute("accountNumber", this.accountNumber);
	cw.writeAttribute("bsbNumber", this.bsbNumber);
	cw.endClass();
    }
}
