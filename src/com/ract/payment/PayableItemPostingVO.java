/*
 * @(#)PayableItemPosting.java
 *
 */
package com.ract.payment;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.Account;
import com.ract.common.ClassWriter;
import com.ract.common.Company;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipRefMgr;
import com.ract.payment.finance.FinanceHelper;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

/**
 * A payable item posting corresponds to a line item for a transaction
 * 
 * @hibernate.class table="pt_payableitemposting" lazy="false"
 * @hibernate.cache usage="read-write"
 * 
 * @author Glenn Lewis
 * @version 1.0, 1/5/2002
 * 
 */
public class PayableItemPostingVO implements Writable, Comparable, Serializable {

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((payableItemPostingID == null) ? 0 : payableItemPostingID.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PayableItemPostingVO other = (PayableItemPostingVO) obj;
	if (payableItemPostingID == null) {
	    if (other.payableItemPostingID != null)
		return false;
	} else if (!payableItemPostingID.equals(other.payableItemPostingID))
	    return false;
	return true;
    }

    public final static String WRITABLE_CLASSNAME = "PayableItemPosting";

    /**
     * String representing credit
     */
    public static final String CREDIT = "Credit";

    /**
     * String representing credit
     */
    public static final String DEBIT = "Debit";

    /** The ID of the payable item posting in payment system */
    protected Integer payableItemPostingID;

    /**
     * The amount of the posting. Debits are positive, credits are negative.
     */
    protected double amount;

    /** The account cost centre of the posting */
    protected String costCentre;

    /** The account sub account of the posting */
    protected String subAccountNumber;

    /**
     * If the account is an unearned account then this is the associated income
     * account. Otherwise <code>null</code>.
     */
    protected String incomeAccountNumber;

    /**
     * If the account is an unearned account then this is the associated income
     * cost centre. Otherwise <code>null</code>.
     */
    protected String incomeCostCentre;

    /**
     * If the account is an unearned account then this is the associated income
     * subAccount. Otherwise <code>null</code>.
     */
    protected String incomeSubAccountNumber;

    /** The company code for the posting */
    protected Company company;

    /** The id of the payable item component that this posting belongs to */
    protected Integer payableItemComponentID;

    /**
     * If the account is an unearned account then the amount outstanding is the
     * amount yet still to be transferred to the income account. Otherwise it is
     * <code>null</code>.
     */
    // protected double amountOutstanding;

    /** The journal number from MFG Pro */
    protected Integer journalNumber;

    /**
     * The date the posting is to be posted to the finance system. Only the date
     * is significant. Hours and minutes are ignored.
     */
    protected DateTime postAtDate;

    /**
     * The date the posting was posted to the finance system. <code>null</code>
     * if the posting has not been made.
     */
    protected DateTime postedDate;

    /** Description of the posting */
    protected String description;

    public PayableItemPostingVO() {
    }

    /**
     * Constructor
     * 
     * @param amount
     *            the amount of the posting. A credit should be a negative
     *            number, a debit positive.
     * @param account
     *            the account number for the posting
     * @param costCentre
     *            the cost centre for the account (<code>null</code> indicates
     *            there no cost centre)
     * @param subAccount
     *            the sub-account for the account <code>null</code> indicates no
     *            sub-account.
     * @param isClearingAccount
     *            is the account a clearing account?
     * @param incomeAccount
     *            if the account is an unearned account then income account
     *            number is its associate income account. <code>null</code>
     *            otherwise.
     * @param incomeCostCentre
     *            the cost centre for the account (<code>null</code> indicates
     *            there no income cost centre)
     * @param subAccount
     *            the income sub-account for the posting (<code>null</code>
     *            indicates there no income sub account)
     * @param postAtDate
     *            the date that the posting should be posted to the MFGPro
     *            system
     * @param company
     *            the company
     * @param description
     *            a description for the posting (null value indicates no
     *            description).
     * @param containerID
     *            the id of the component that this posting is for.
     *            <code>null</code> if the container has not been created.
     * @throws <code>PaymentException</code> if a parameter is invalid
     */
    public PayableItemPostingVO(double amount, Account account, Account incomeAccount, DateTime createDate, DateTime postAtDate, Company company, String description, Integer containerID) throws PaymentException {
	setAmount(amount);
	setAccount(account);
	setPostAtDate(postAtDate);
	setCreateDate(createDate);
	if (incomeAccount != null) {
	    setIncomeAccount(incomeAccount);
	}

	setCompany(company);

	if (description != null) {
	    setDescription(description);

	}

	if (containerID != null) {
	    setContainerID(containerID);
	}
    }

    // ============ Getter Methods =============================================

    /**
     * Get the ID of the payable item posting in the payment system.
     * 
     * @return the ID of the payable item posting in the payment system, or
     *         <code>null</code> if the item has not been entered in the payment
     *         system.
     * @hibernate.id column="payableitempostingid" generator-class="assigned"
     */
    public Integer getPayableItemPostingID() {
	return payableItemPostingID;
    }

    /**
     * Get the amount of the posting
     * 
     * @hibernate.property
     * @hibernate.column name="amount"
     * @return the amount of the posting
     */
    public double getAmount() {
	return amount;
    }

    /**
     * The account to post to
     */
    // private Account account;

    private String journalKey;

    private DateTime createDate;

    // protected SourceSystem sourceSystem;

    /**
     * @hibernate.property
     * @hibernate.column name="journalKey"
     */
    public String getJournalKey() {
	return journalKey;
    }

    public void setJournalKey(String journalKey) {
	this.journalKey = journalKey;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="createDate"
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    /**
     * Get the account associated with the account number
     * 
     * @return
     */
    public Account getAccount() {
	try {
	    MembershipRefMgr mRefMgr = MembershipEJBHelper.getMembershipRefMgr();
	    return (Account) mRefMgr.getMembershipAccount(this.accountNumber);
	} catch (Exception ex) {
	    LogUtil.log(this.getClass(), "Unable to get account for " + this.accountNumber + ". " + ex.getMessage());
	}
	return null;
    }

    /**
     * Get the cost centre of the posting
     * 
     * @hibernate.property
     * @hibernate.column name="costcentre"
     * @return the cost centre of the posting, or <code>null</code> if there is
     *         no cost centre
     */
    public String getCostCentre() {
	return this.costCentre;
    }

    /**
     * Get sub account of the posting
     * 
     * @hibernate.property
     * @hibernate.column name="subaccount"
     * @return the sub account of the posting, or <code>null</code> if there is
     *         no sub account
     */
    public String getSubAccountNumber() {
	return this.subAccountNumber;
    }

    /**
     * if this posting has an income account number then it is an earnings
     * posting
     */
    boolean isEarningsPosting() {
	return (this.getIncomeAccountNumber() != null && !this.getIncomeAccountNumber().equals(""));
    }

    public String getJournalKey(String salesBranchCode) throws RemoteException {
	StringBuffer key = new StringBuffer();
	key.append(this.getAccountNumber());
	key.append(this.getCostCentre());
	key.append(this.getSubAccountNumber());
	if (salesBranchCode == null) {
	    key.append(this.getSalesBranchCode());
	}
	key.append(this.getCompany().getCompanyCode());
	key.append(FinanceHelper.validateEffectiveDate(this.getPostAtDate(), this.getPostedDate()).getTime());
	if (this.postedDate != null) {
	    key.append(postedDate.getDateOnly().getTime());
	}
	if (this.getAmount() >= 0) {
	    key.append(PayableItemPostingVO.DEBIT);
	} else {
	    key.append(PayableItemPostingVO.CREDIT);
	}

	return key.toString().toUpperCase();
    }

    private String salesBranchCode = null;

    /**
     * Note: this must link to the payable item to get the sales branch code.
     * 
     * @throws RemoteException
     * @return String
     */
    public String getSalesBranchCode() throws RemoteException {
	if (this.salesBranchCode == null) {
	    try {
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		PayableItemComponentVO payableItemComponent = null;
		if (this.getPayableItemComponentID() != null) {
		    payableItemComponent = paymentMgr.getPayableItemComponent(this.getPayableItemComponentID());
		}

		if (payableItemComponent != null) {
		    this.salesBranchCode = payableItemComponent.getPayableItem().getSalesBranchCode();
		}
	    } catch (PaymentException ex1) {
		// ignore
		throw new RemoteException("Unable to get component or payable item.", ex1);
	    } catch (RemoteException ex2) {
		// ignore
	    }
	}
	return this.salesBranchCode;
    }

    /**
     * If the account is an unearned account, get the income account associated
     * with it.
     * 
     * @hibernate.property
     * @hibernate.column name="incomeaccountnumber"
     * @return the income account (general ledger code) associated with the
     *         unearned account, or <code>null</code> if there is no unearned
     *         account
     */
    public String getIncomeAccountNumber() {
	return this.incomeAccountNumber;
    }

    public Account getIncomeAccount() {
	try {
	    MembershipRefMgr mRefMgr = MembershipEJBHelper.getMembershipRefMgr();
	    return (Account) mRefMgr.getMembershipAccount(this.incomeAccountNumber);
	} catch (Exception ex) {
	    LogUtil.log(this.getClass(), "Unable to get income account for " + this.accountNumber + ". " + ex.getMessage());
	}
	return null;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="incomecostcentre"
     * @return income account cost centre, or <code>null</code> if there is no
     *         unearned account
     */
    public String getIncomeCostCentre() {
	return this.incomeCostCentre;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="incomesubaccount"
     * @return the income account sub account or <code>null</code> if there is
     *         no unearned account
     */
    public String getIncomeSubAccountNumber() {
	return this.incomeSubAccountNumber;
    }

    /**
     * Is this a credit. eg. -10.00
     * 
     * @return
     */
    public boolean isCredit() {
	return !isDebit();
    }

    /**
     * Is this a debit. eg. 10.00
     * 
     * @return
     */
    public boolean isDebit() {
	return (getAmount() > 0 ? true : false);
    }

    /**
     * Get the company of the posting
     * 
     * @hibernate.property
     * @hibernate.column name="companycode"
     * @return the company of the posting
     */
    public Company getCompany() {
	return this.company;
    }

    /**
     * Get the description of the posting
     * 
     * @hibernate.property
     * @hibernate.column name="description"
     * @return the description of the posting or <code>null<\code> if
     * there is no description.
     */
    public String getDescription() {
	return this.description;
    }

    /**
     * Get the payable item component that this posting is for
     * 
     * @hibernate.property
     * @hibernate.column name="payableitemcomponentid"
     * @return the payable item component that this posting is for
     */
    public Integer getPayableItemComponentID() {
	return this.payableItemComponentID;
    }

    /**
     * Get the journal number for this posting in the finance system.
     * 
     * @hibernate.property
     * @hibernate.column name="journalno"
     * @return the journal number for this posting in the finance system, or
     *         <code>null</code> if not posted to the the finance system.
     */
    public Integer getJournalNumber() {
	return this.journalNumber;
    }

    /**
     * Get the date the posting is to be posted to the finance system
     * 
     * @hibernate.property
     * @hibernate.column name="postat"
     * @return the date the posting is to be posted to the finance system.
     */
    public DateTime getPostAtDate() {
	return this.postAtDate;
    }

    /**
     * Get the date the posting was posted to the finance system.
     * <code>null</code> if the posting has not been made.
     * 
     * @hibernate.property
     * @hibernate.column name="posted"
     * @return the date the posting was posted to the finance system
     */
    public DateTime getPostedDate() {
	return this.postedDate;
    }

    /**
     * Set the ID of this payable item posting in the payment system.
     * 
     * @param payableItemPostingID
     *            the ID of this payable item in the payment system.
     * @throws <code>PaymentException<\code> if <code>payableItemID</code> is
     *         <code>null</code>
     */
    public void setPayableItemPostingID(Integer postingID) throws PaymentException {
	if (postingID == null) {
	    throw new PaymentException("Setting payable item posting ID to null in Payable Item Component");
	}

	this.payableItemPostingID = postingID;
    }

    /**
     * Set the (total) amount of payable item
     * 
     * @param amount
     *            the total amount of this payable item.
     */
    public void setAmount(double amount) throws PaymentException {
	this.amount = amount;
    }

    /**
     * Set the account (general ledger code) of the posting
     * 
     * @param account
     *            the account (general ledger code) to be set
     * @throws <code>PaymentException</code> if the account is <code>null</code>
     *         .
     */
    private void setAccount(Account theAccount) throws PaymentException {
	if (theAccount == null) {
	    throw new PaymentException("Setting a null account number on a payableItemPosting.");
	}
	this.setAccountNumber(theAccount.getAccountNumber());
	this.setCostCentre(theAccount.getCostCenter());
	this.setSubAccountNumber(theAccount.getSubAccountNumber());
    }

    /**
     * Set the cost centre of the posting
     * 
     * @param costCentre
     *            the cost centre
     * @throws <code>PaymentException</code> if the cost centre is null
     */
    // private void setCostCentre(String costCentre)
    // throws PaymentException
    // {
    // if(costCentre == null)
    // {
    // throw new
    // PaymentException("Setting a null cost centre on a payableItemPosting.");
    // }
    // this.costCentre = costCentre;
    // }

    /**
     * Set the sub-account of the posting
     * 
     * @param subAccount
     *            the sub-account
     * @throws <code>PaymentException</code> if the sub-account is null
     */
    // private void setSubAccountNumber(String subAccountNumber)
    // throws PaymentException
    // {
    // if(subAccountNumber == null)
    // {
    // throw new
    // PaymentException("Setting a sub account to null in PayableItemPosting.");
    // }
    // this.subAccountNumber = subAccountNumber;
    // }

    // private Account incomeAccount;

    /**
     * set the income account for a posting.
     * 
     * @param incomeAccount
     * @throws PaymentException
     */
    private void setIncomeAccount(Account incomeAccount) throws PaymentException {
	if (incomeAccount == null) {
	    throw new PaymentException("Income account number can't be null.");
	}
	this.setIncomeAccountNumber(incomeAccount.getAccountNumber());
	this.setIncomeCostCentre(incomeAccount.getCostCenter());
	this.setIncomeSubAccountNumber(incomeAccount.getSubAccountNumber());
    }

    /**
     * Set the income account (general ledger code) of the posting. Used if this
     * account is an unearned account. <code>null</code> otherwise.
     * 
     * @param incomeAccount
     *            the income account (general ledger code) to be set
     * @throws <code>PaymentException</code> if the incomeAccount is
     *         <code>null</code>.
     */
    // private void setIncomeAccountNumber(String incomeAccountNumber)
    // throws PaymentException
    // {
    // if(incomeAccountNumber == null)
    // {
    // throw new
    // PaymentException("Setting a null account number on a payableItemPosting.");
    // }
    // this.incomeAccountNumber = incomeAccountNumber;
    // }

    /**
     * Set the cost centre of the income account of posting
     * 
     * @param costCentre
     *            the income cost centre
     * @throws <code>PaymentException</code> if the cost centre is null
     */
    // private void setIncomeCostCentre(String costCentre)
    // throws PaymentException
    // {
    // if(costCentre == null)
    // {
    // throw new
    // PaymentException("Setting a null cost centre on a payableItemPosting.");
    // }
    //
    // this.incomeCostCentre = costCentre;
    // }

    /**
     * Set the sub-account of the income account of the posting
     * 
     * @param subAccount
     *            the income sub-account
     * @throws <code>PaymentException</code> if the sub-account is null
     */
    // private void setIncomeSubAccountNumber(String subAccountNumber)
    // throws PaymentException
    // {
    // if(subAccountNumber == null)
    // {
    // throw new
    // PaymentException("Setting a sub account to null in PayableItemPosting.");
    // }
    //
    // this.incomeSubAccountNumber = subAccountNumber;
    // }

    /**
     * Set the company of the posting
     * 
     * @param company
     *            the company
     * @throws <code>PaymentException</code> if the company is null
     */
    public void setCompany(Company company) throws PaymentException {
	if (company == null) {
	    throw new PaymentException("Setting a null company on a payableItemPosting.");
	}

	this.company = company;
    }

    /**
     * Set the description of the posting
     * 
     * @param description
     *            the posting.
     * @throws <code>PaymentException</code> if the description is null.
     */
    private void setDescription(String desc) throws PaymentException {
	if (desc == null) {
	    throw new PaymentException("Setting a null description on a payableItemPosting.");
	}

	this.description = desc;
    }

    /**
     * Set the payable item component that this posting is for
     * 
     * @param containerID
     *            the ID of the payable item component that this posting is for.
     * @throws <code>PaymentException</code> if the container is null, or if the
     *         parent item has already been set
     */
    public void setContainerID(Integer contID) throws PaymentException {
	if (contID == null) {
	    throw new PaymentException("Setting the container ID to null on a payableItemPosting.");
	}

	if (this.payableItemComponentID != null) {
	    throw new PaymentException("Attempting to set the container for a PayableItemPosting where it has already been set.");
	}

	this.payableItemComponentID = contID;
    }

    /*
     * Set the journal number for this posting in the finance system.
     * 
     * @param journalNumber the journal number to be set
     * 
     * @throws <code>PaymentException</code> if the journal number is null
     */
    public void setJournalNumber(Integer journalNum) throws PaymentException, RemoteException {
	this.journalNumber = journalNum;
    }

    /*
     * Set the date when this posting is to be made in the finance system.
     * 
     * @throws <code>PaymentException</code> if the post at date is null
     */
    public void setPostAtDate(DateTime thePostAtDate) throws PaymentException {
	if (thePostAtDate == null) {
	    throw new PaymentException("Setting the post-at date to null on a payableItemPosting.");
	}

	this.postAtDate = DateUtil.clearTime(thePostAtDate);
    }

    /*
     * Set the date the posting was posted to the finance system.
     * 
     * @throws <code>PaymentException</code> if the posted date is null
     */
    public void setPostedDate(DateTime thePostedDate) throws PaymentException, RemoteException {
	this.postedDate = thePostedDate;
    }

    /**
     * The natural sort order for payable item postings is the post at date then
     * the account number then the cost centre then the sub account number. The
     * post at date is the date that the posting becomes effective.
     */
    public int compareTo(Object obj) {
	if (obj instanceof PayableItemPostingVO) {
	    PayableItemPostingVO postingVO = (PayableItemPostingVO) obj;
	    if (postingVO.getPostAtDate() != null && this.postAtDate != null) {
		int returnValue = this.postAtDate.compareTo(postingVO.getPostAtDate());
		// If both are for the same post at date then use the account
		// number
		// as a secondary sort order
		if (returnValue == 0 && this.getAccountNumber() != null && postingVO.getAccountNumber() != null) {
		    returnValue = this.getAccountNumber().compareTo(postingVO.getAccountNumber());
		    // If the accounts are the same then sort on the cost center
		    if (returnValue == 0 && this.costCentre != null && postingVO.getCostCentre() != null) {
			returnValue = this.costCentre.compareTo(postingVO.getCostCentre());
			// If the cost centers are the same then sort on the sub
			// account
			if (returnValue == 0 && this.subAccountNumber != null && postingVO.getSubAccountNumber() != null) {
			    return this.subAccountNumber.compareTo(postingVO.getSubAccountNumber());
			} else {
			    return returnValue;
			}
		    } else {
			return returnValue;
		    }
		} else {
		    return returnValue;
		}
	    } else {
		return 0;
	    }
	} else {
	    return 0;
	}
    }

    public String toString() {
	StringBuffer str = new StringBuffer();
	str.append(super.toString() + "--");
	str.append(this.payableItemPostingID + " - ");
	str.append(StringUtil.rightPadString(this.getAccountNumber().toString(), 5));
	try {
	    str.append(":" + StringUtil.leftPadString(NumberUtil.formatValue(this.amount), 10));
	} catch (SystemException ex) {
	    // ignore
	}
	str.append(this.postAtDate != null ? " " + this.postAtDate.formatShortDate() : "");
	str.append(" - " + StringUtil.rightPadString(this.description, 100) + " ");
	// str.append(this.getSourceSystem().getAbbreviation()+" ");
	if (this.getAccount() != null) {
	    str.append(this.getAccount().isClearingAccount() ? " (Clearing)" : "");
	    str.append(this.getAccount().isSuspenseAccount() ? " (Suspense)" : "");
	}
	// str.append(this.isReversible() ? "REVERSIBLE" : "NOT REVERSIBLE");
	return str.toString();
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    /**
     * Description of the Method
     * 
     * @param cw
     *            Description of the Parameter
     * @param deepWrite
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("accountNumber", this.getAccountNumber());
	cw.writeAttribute("containerID", this.payableItemComponentID);
	if (this.company != null) {
	    cw.writeAttribute("company", this.company);
	}
	cw.writeAttribute("costCentre", this.costCentre);
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("incomeAccountNumber", this.incomeAccountNumber);
	cw.writeAttribute("incomeCostCentre", this.incomeCostCentre);
	cw.writeAttribute("incomeSubAccountNumber", this.incomeSubAccountNumber);
	cw.writeAttribute("journalNumber", this.journalNumber);
	cw.writeAttribute("payableItemPostingID", this.payableItemPostingID);
	cw.writeAttribute("postAtDate", this.postAtDate);
	cw.writeAttribute("journalKey", this.journalKey);
	cw.writeAttribute("createDate", this.createDate);
	cw.writeAttribute("postedDate", this.postedDate);
	cw.writeAttribute("postingAmount", this.amount);
	cw.writeAttribute("subAccountNumber", this.subAccountNumber);

	cw.endClass();
    }

    protected String accountNumber;

    /**
     * @hibernate.property
     * @hibernate.column name="accountnumber"
     * @return String
     */
    public String getAccountNumber() {
	return this.accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
    }

    public void setCostCentre(String costCentre) {
	this.costCentre = costCentre;
    }

    public void setSubAccountNumber(String subAccountNumber) {
	this.subAccountNumber = subAccountNumber;
    }

    public void setPayableItemComponentID(Integer payableItemComponentID) {
	this.payableItemComponentID = payableItemComponentID;
    }

    public void setIncomeSubAccountNumber(String incomeSubAccountNumber) {
	this.incomeSubAccountNumber = incomeSubAccountNumber;
    }

    public void setIncomeAccountNumber(String incomeAccountNumber) {
	this.incomeAccountNumber = incomeAccountNumber;
    }

    public void setIncomeCostCentre(String incomeCostCentre) {
	this.incomeCostCentre = incomeCostCentre;
    }

}
