package com.ract.payment;

/**
 * PaymentAdapter super interface for commit and rollback
 * 
 * @todo consider use where more than one possibility for commit and rollback.
 * 
 * @author jyh
 */

public interface PaymentAdapter {
    // public static final String ADAPTER_FINANCE = "Finance Adapter";
    // public static final String ADAPTER_RECEIPTING = "Receipting Adapter";
    // public static final String ADAPTER_DIRECT_DEBIT = "Direct Debit Adapter";
    //
    // public void commit()
    // throws SystemException;
    //
    // public void rollback()
    // throws SystemException;
    //
    // public void release()
    // throws SystemException;
}
