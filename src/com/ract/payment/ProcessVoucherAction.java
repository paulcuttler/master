package com.ract.payment;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipTransactionMgrLocal;
import com.ract.membership.MembershipVO;
import com.ract.payment.finance.FinanceMgrLocal;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserMgrLocal;
import com.ract.user.UserSession;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class ProcessVoucherAction extends ActionSupport implements SessionAware, ValidationAware, Preparable {

	private final String PROCESS_APPROVED = "A";

	@InjectEJB(name = "MembershipMgrBean")
	private MembershipMgrLocal membershipMgr;

	@SuppressWarnings("unchecked")
	@Override
	public void validate() {
		if (voucher != null && voucher.getStatus() != null) {
			addActionError("Voucher " + voucher.getDisposalId() + " has already been processed.");
		} else if (this.getProcessAction() != null && this.getProcessAction().equals(PROCESS_APPROVED)) {
			// get membership
			try {
				List<MembershipVO> membershipList = (List<MembershipVO>) membershipMgr.findMembershipByClientNumber(Integer.parseInt(voucher.getClientNumber()));

				if (membershipList.size() > 1) {
					throw new Exception("Membership is not unique");
				} else if (membershipList.size() < 1) {
					throw new Exception("Membership not found");
				}

				MembershipVO memVO = membershipList.get(0);

				// Prevent approval if there is an amount outstanding on the
				// membership.
				double amountOutstanding = memVO.getAmountOutstanding(true, true);
				if (amountOutstanding > 0) {
					this.addActionError("Cannot approve Voucher " + voucher.getDisposalId() + " as membership has amount outstanding of " + CurrencyUtil.formatDollarValue(amountOutstanding));
				}

			} catch (Exception e) {
				LogUtil.log(this.getClass(), "Unable to retrieve unique membership by clientNumber: " + voucher.getClientNumber() + ": " + e);
			}
		}
	}

	private FinanceVoucher voucher;

	@Override
	public void prepare() throws Exception {
		LogUtil.debug(this.getClass(), "processCreditDisposal 1a");

		LogUtil.debug(this.getClass(), "processCreditDisposal 1b" + processAction);

		voucher = financeMgr.getVoucher(disposalId);
		LogUtil.debug(this.getClass(), "processCreditDisposal 1bb" + voucher);

	}

	private Map<String, Object> session = null;

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	private Integer disposalId;

	private String processAction;

	private String approvingUser;

	public String getApprovingUser() {
		return approvingUser;
	}

	public void setApprovingUser(String approvingUser) {
		this.approvingUser = approvingUser;
	}

	private String approvingUserId;

	public String getApprovingUserId() {
		return approvingUserId;
	}

	private Collection<FinanceVoucher> voucherList;

	public void setApprovingUserId(String approvingUserId) {
		this.approvingUserId = approvingUserId;
	}

	@InjectEJB(name = "UserMgrBean")
	private UserMgrLocal userMgr;

	@InjectEJB(name = "FinanceMgrBean")
	private FinanceMgrLocal financeMgr;

	@InjectEJB(name = "MembershipTransactionMgrBean")
	private MembershipTransactionMgrLocal membershipTransactionMgr;

	/**
	 * @return
	 */
	public String execute() {

		UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);
		approvingUserId = userSession.getUserId();

		try {
			approvingUser = userMgr.getUser(approvingUserId).getUsername();
		} catch (RemoteException e1) {
			addActionError(e1.getMessage());
			return ERROR;
		}

		LogUtil.debug(this.getClass(), "processCreditDisposal 1d" + voucherList);

		LogUtil.debug(this.getClass(), "processCreditDisposal 2");
		if (processAction.equals(FinanceVoucher.STATUS_APPROVED)) {
			LogUtil.debug(this.getClass(), "processCreditDisposal 3");
			try {
				membershipTransactionMgr.processVoucher(voucher);
			} catch (RemoteException e) {
				addActionError(e.getMessage());
				return ERROR;
			}
			LogUtil.debug(this.getClass(), "processCreditDisposal 4");
		} else if (processAction.equals(FinanceVoucher.STATUS_REJECTED)) {
			LogUtil.debug(this.getClass(), "processCreditDisposal 5");
		}

		// process credit disposal
		voucher.setStatus(processAction);
		voucher.setProcessDate(new DateTime());

		LogUtil.debug(this.getClass(), "processCreditDisposal 6");
		// transaction safe?
		try {
			financeMgr.updateVoucher(voucher);
		} catch (RemoteException e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		// get credit disposal list with updated status
		LogUtil.debug(this.getClass(), "processCreditDisposal 1c" + approvingUserId);
		try {
			voucherList = financeMgr.getVouchersForApprovingUser(approvingUserId);
		} catch (RemoteException e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}

	public FinanceVoucher getVoucher() {
		return voucher;
	}

	public void setVoucher(FinanceVoucher voucher) {
		this.voucher = voucher;
	}

	public Collection<FinanceVoucher> getVoucherList() {
		return voucherList;
	}

	public void setVoucherList(Collection<FinanceVoucher> voucherList) {
		this.voucherList = voucherList;
	}

	public Integer getDisposalId() {
		return disposalId;
	}

	public void setDisposalId(Integer disposalId) {
		this.disposalId = disposalId;
	}

	public String getProcessAction() {
		return processAction;
	}

	public void setProcessAction(String processAction) {
		this.processAction = processAction;
	}
}