package com.ract.payment;

import java.rmi.RemoteException;
import java.text.ParseException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;

import com.ract.common.CommonEJBHelper;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.payment.finance.DebtorInvoiceFileJob;
import com.ract.payment.finance.DebtorInvoiceReceiptJob;
import com.ract.payment.finance.DebtorMasterFileJob;
import com.ract.payment.finance.ResetDebtorInvoiceFileJob;

/**
 * <p>
 * Utility payment methods.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class PaymentHelper {

    public static void scheduleDebtorInvoiceFileJob(String cronExpression) throws RemoteException {
	JobDetail jobDetail = new JobDetail("DebtorInvoiceFileJob", null, DebtorInvoiceFileJob.class);
	jobDetail.setDescription("Debtor Invoice File Processing");

	CronTrigger trigger = null;
	try {
	    trigger = new CronTrigger("DebtorInvoiceFileTrigger", null, cronExpression);
	} catch (ParseException ex2) {
	    throw new RemoteException("Unable to determine schedule.", ex2);
	}

	trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	}

    }

    private static long getMinuteInterval(int minuteInterval) {
	long interval = minuteInterval * (60L * 1000L); // 60 seconds
	return interval;
    }

    public static void scheduleDebtorInvoiceReceiptJob(String cronExpression) throws RemoteException {
	JobDetail jobDetail = new JobDetail("DebtorInvoiceReceiptJob", null, DebtorInvoiceReceiptJob.class);
	jobDetail.setDescription("Debtor Invoice Receipt Transfer");

	CronTrigger trigger = null;
	try {
	    trigger = new CronTrigger("DebtorInvoiceReceiptTrigger", null, cronExpression);
	} catch (ParseException ex2) {
	    throw new RemoteException("Unable to determine schedule.", ex2);
	}

	trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	}

    }

    public static void scheduleDebtorMasterFileJob(String cronExpression) throws RemoteException {
	JobDetail jobDetail = new JobDetail("DebtorMasterFileJob", null, DebtorMasterFileJob.class);
	jobDetail.setDescription("Debtor Master File Processing");

	// schedule the job
	CronTrigger trigger = null;
	try {
	    trigger = new CronTrigger("DebtorMasterFileTrigger", null, cronExpression);
	} catch (ParseException ex2) {
	    throw new RemoteException("Unable to determine schedule.", ex2);
	}

	trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	}

    }

    public static void scheduleResetDebtorInvoiceFileJob(String cronExpression) throws RemoteException {
	JobDetail jobDetail = new JobDetail("ResetDebtorInvoiceFileJob", null, ResetDebtorInvoiceFileJob.class);
	jobDetail.setDescription("Reset Debtor Invoice File Job");

	// schedule the job
	CronTrigger trigger = null;
	try {
	    trigger = new CronTrigger("ResetDebtorFileTrigger", null, cronExpression);
	} catch (ParseException ex2) {
	    throw new RemoteException("Unable to determine schedule.", ex2);
	}

	trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	}

    }

}
