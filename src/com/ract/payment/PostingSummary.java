package com.ract.payment;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.util.DateTime;

/**
 * A posting summary line for reports
 * 
 * @author jyh
 */
public class PostingSummary implements Serializable {

    private DateTime effectiveDate;

    private DateTime enteredDate;

    private String accountNumber;

    private BigDecimal amount;

    private Boolean posted;

    private String clientNumber;

    private String transactionReference;

    private String narration;

    public String getAccountNumber() {
	return accountNumber;
    }

    public BigDecimal getAmount() {
	return amount;
    }

    public DateTime getEnteredDate() {
	return enteredDate;
    }

    public DateTime getEffectiveDate() {
	return effectiveDate;
    }

    public void setEffectiveDate(DateTime effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    public void setEnteredDate(DateTime enteredDate) {
	this.enteredDate = enteredDate;
    }

    public void setAmount(BigDecimal amount) {
	this.amount = amount;
    }

    public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
    }

    public void setPosted(Boolean posted) {
	this.posted = posted;
    }

    public void setClientNumber(String clientNumber) {
	this.clientNumber = clientNumber;
    }

    public void setTransactionReference(String transactionReference) {
	this.transactionReference = transactionReference;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.getAccountNumber());
	desc.append(" ");
	desc.append(this.getEnteredDate());
	desc.append(" ");
	desc.append(this.getEffectiveDate());
	desc.append(" ");
	desc.append(this.getAmount());
	return desc.toString();
    }

    public void setNarration(String narration) {
	this.narration = narration;
    }

    public Boolean getPosted() {
	return posted;
    }

    public String getKey() {
	return this.getAccountNumber() + this.getEffectiveDate() + this.getEnteredDate();
    }

    public String getClientNumber() {
	return clientNumber;
    }

    public String getTransactionReference() {
	return transactionReference;
    }

    public String getNarration() {
	return narration;
    }

}
