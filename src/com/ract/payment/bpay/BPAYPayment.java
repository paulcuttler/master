package com.ract.payment.bpay;

import java.rmi.RemoteException;

import org.w3c.dom.Node;

import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

public class BPAYPayment extends ElectronicPayment {
    /**
     * A code indicating the type of instruction
     * 
     * 05 Payment 15 Error Correction 25 Reversal
     * 
     */
    private int paymentInstructionType;

    /**
     * The Biller number assigned by BPAY (CIP)
     */
    private String billerCode;

    /**
     * A code indicating the method of payment
     * 
     * 001 debit account 101 VISA 201 MASTERCARD 301 Other Credit Cards
     */
    private int paymentMethod;

    /**
     * A code indicating how the details were captured by the paying institution
     * 
     * 000 undefined 001 key entry by operator 002 touch tone entry by payer 003
     * speech recognition 004 internet/online banking 005 electronic bill
     * presentment 006 batch data entry
     * 
     */
    private int entryMethod;

    private String originalReferenceNumber;

    /**
     * The reason for generating an error
     * 
     * 001 paid twice 002 paid wrong account 003 paid wrong biller 004 paid
     * wrong amount 005 payer did not authorise 400 VISA chargeback 500
     * MASTERCARD chargeback 600 BANKCARD charge back
     * 
     * null for payment or reversal
     */
    private int errorCorrectionReason;

    /* ====================================================================== */

    public int getErrorCorrectionReason() {
	return errorCorrectionReason;
    }

    public String getOriginalReferenceNumber() {
	return originalReferenceNumber;
    }

    public int getEntryMethod() {
	return entryMethod;
    }

    public int getPaymentMethod() {
	return paymentMethod;
    }

    public String getBillerCode() {
	return billerCode;
    }

    public void setPaymentInstructionType(int pit) {
	this.paymentInstructionType = pit;
    }

    public void setErrorCorrectionReason(int ecr) {
	this.errorCorrectionReason = ecr;
    }

    public void setOriginalReferenceNumber(String orn) {
	this.originalReferenceNumber = orn;
    }

    public void setEntryMethod(int em) {
	this.entryMethod = em;
    }

    public void setPaymentMethod(int pm) {
	this.paymentMethod = pm;
    }

    public void setBillerCode(String bc) {
	this.billerCode = bc;
    }

    public int getPaymentInstructionType() {
	return paymentInstructionType;
    }

    public BPAYPayment() {
	super();
	setOperatorDetails();
    }

    public BPAYPayment(Node epNode) throws RemoteException {
	Node writableNode;
	writableNode = XMLHelper.getWritableNode(epNode);
	String nodeName = epNode.getNodeName();
	String valueText = epNode.hasChildNodes() ? epNode.getFirstChild().getNodeValue() : null;

	if ("ElectronicPayment".equals(nodeName) && writableNode != null) {
	    ElectronicPayment ep = new ElectronicPayment(writableNode);
	    // set internal attributes
	    this.setAmount(ep.getAmount());
	    this.setCardNumber(ep.getCardNumber());
	    this.setClientNumber(ep.getClientNumber());
	    this.setCustomerReferenceNumber(ep.getCustomerReferenceNumber());
	    this.setDateOfPayment(ep.getDateOfPayment());
	    this.setDateReceived(ep.getDateReceived());
	    this.setInputData(ep.getInputData());
	    this.setRactReceiptNumber(ep.getRactReceiptNumber());
	    this.setReferenceId(ep.getReferenceId());
	    this.setSeqNumber(ep.getSeqNumber());
	    this.setSourceSystem(ep.getSourceSystem());
	    this.addStatus(ep.getStatus());
	    this.setElectronicOperatorDetails(ep.getUserId());

	}

    }

    private void setOperatorDetails() {
	this.setElectronicOperatorDetails("BPAY");
    }

    public void setPayReferenceNumber(String crn) {
	this.setCustomerReferenceNumber(crn);
    }

    /**
     * @todo review this method
     */
    public String toString() {
	StringBuffer sb = new StringBuffer();

	sb.append(this.paymentInstructionType + ",");
	sb.append(this.billerCode + ",");
	sb.append(this.paymentMethod + ",");
	sb.append(this.entryMethod + ",");
	sb.append(this.originalReferenceNumber + ",");
	sb.append(this.errorCorrectionReason + ",");

	String inputData[] = super.getInputData().split(",");

	sb.append(StringUtil.makeSpaces(this.getCustomerReferenceNumber()) + ",");
	sb.append(this.getDateOfPayment() + ",");
	sb.append(inputData[4] + ",");

	// if (super.getCardNumber().equals("?"))
	// {
	// if(inputData[7] != null)
	// {
	// sb.append(inputData[7]+",");
	// }
	// else
	// {
	// sb.append("?,");
	// }
	// }
	// else
	// {
	// sb.append(super.getCardNumber() + ",");
	// }
	sb.append(StringUtil.makeSpaces(this.getTransactionReferenceNumber()) + ",");
	sb.append(this.getAmount() + ",");

	sb.append(StringUtil.makeSpaces(this.getSourceSystem().getAbbreviation()) + ",");
	sb.append(this.getDateReceived() + ",");
	sb.append(StringUtil.makeSpaces(this.getClientNumber()) + ",");
	sb.append(StringUtil.makeSpaces(this.getSeqNumber()) + ",");
	sb.append(StringUtil.makeSpaces(this.getRactReceiptNumber()) + ",");
	sb.append((getStatus() == null ? "null:" : StringUtil.replace(getStatus(), ":", "")));

	return sb.toString();
    }
}
