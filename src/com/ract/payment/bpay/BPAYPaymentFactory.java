package com.ract.payment.bpay;

/**
 * Title:        Master Project
 * Description:  Return the appropriate BPAYPayment
 * Copyright:    Copyright (c) 2003
 * Company:      RACT
 * @author
 * @version 1.0
 */

import com.ract.common.SourceSystem;

public class BPAYPaymentFactory {
    public static BPAYPayment getBPAYPayment(String sourceSystem) {

	if (sourceSystem.equalsIgnoreCase(SourceSystem.INSURANCE.getAbbreviation())) {
	    return new BPAYInsurancePayment();
	}

	if (sourceSystem.equalsIgnoreCase(SourceSystem.MEMBERSHIP.getAbbreviation())) {
	    return new BPAYMembershipPayment();
	}

	return new BPAYPayment();

    }
}
