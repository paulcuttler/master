package com.ract.payment.bpay;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

import com.ract.payment.billpay.BillPayException;

public interface BPAYMgr extends EJBObject {
    /**
     * Update the memberships paid by BPAY
     * 
     * @param payment_file
     *            The file of payments returned from IVR
     * @param audit_file
     *            The audit trail
     * 
     * @exception RemoteException
     *                Description of the Exception
     * @exception BillPayException
     *                Description of the Exception
     */

    public String processBPAYPayment(String line, boolean receiptPayment, boolean processTransaction) throws RemoteException;

}
