package com.ract.payment.bpay;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Properties;

import com.ract.payment.PaymentEJBHelper;
import com.ract.util.DateTime;

/**
 * Update reader.
 * 
 * @author Levon Kara
 * @created 31 July 2002
 * @version 1.0
 */

public class BPAYUpdater
{
    // BPAYMgrHome BPAYMgrHome = null;

    BPAYMgr bpayMgr = null;

    public BPAYUpdater(String URL, String mode, String inputFile,
	    String auditFile, String receiptPaymentString,
	    String processTransactionString)
    {

	boolean receiptPayment = false;
	boolean processTransaction = false;

	try
	{
	    if (receiptPaymentString == null || receiptPaymentString.equals(""))
	    {
		receiptPayment = true;
	    }
	    else
	    {
		receiptPayment = receiptPaymentString.equals("Y");
	    }

	    if (processTransactionString == null
		    || processTransactionString.equals(""))
	    {
		processTransaction = true;
	    }
	    else
	    {
		processTransaction = processTransactionString.equals("Y");
	    }

	    String providerURL = URL;

	    String initialContext = "org.jnp.interfaces.NamingContextFactory";
	    Properties environment = new Properties();
	    environment.setProperty(javax.naming.Context.PROVIDER_URL,
		    providerURL);
	    environment.setProperty(
		    javax.naming.Context.INITIAL_CONTEXT_FACTORY,
		    initialContext);

	    bpayMgr = PaymentEJBHelper.getBPAYMgr();

	}
	catch (Exception ne)
	{
	    System.out
		    .println("Failed initializing access to BPay Manager home interface: "
			    + ne.getMessage());
	}

	try
	{
	    System.out
		    .println(" ======================================================== ");
	    System.out.println(" Mode                 " + mode);
	    System.out.println(" Remittance file      " + inputFile);
	    System.out.println(" Audit file           " + auditFile);
	    System.out.println(" Receipting enabled   " + receiptPayment);
	    System.out.println(" Process transaction  " + processTransaction);
	    System.out
		    .println(" Starting             " + new Date().toString());
	    System.out
		    .println(" ======================================================== ");

	    if (mode.equalsIgnoreCase("Update")
		    || mode.equalsIgnoreCase("Both"))
	    {
		this.updateBPAYPayments(inputFile, auditFile, receiptPayment,
			processTransaction);
		System.out.println(" Update Finished " + new Date().toString());
	    }

	}
	catch (Exception e)
	{
	    System.out.println("BPayUpdater +++++++ Exception "
		    + e.getMessage());
	    System.out.println("BPayUpdater +++++++ FINISHING");
	}

    }

    private void updateBPAYPayments(String paymentFile, String auditFile,
	    boolean receiptPayment, boolean processPayment)
	    throws RemoteException
    {
	System.out.println("BPAY Audit file ... " + auditFile);
	System.out.println("BPAY Payment file . " + paymentFile);

	BufferedReader in = null;
	PrintWriter audit = null;
	// String printLine = null;

	try
	{
	    // Open the BillPay file from telstra
	    in = new BufferedReader(new FileReader(paymentFile));
	    audit = new PrintWriter(new BufferedWriter(
		    new FileWriter(auditFile)));

	    String line = "";

	    // audit.println(XMLHelper.getXMLHeaderLine());
	    // audit.println("<BPAYPaymentAudit>");
	    // audit.println("<inputFile>" + paymentFile + "</inputFile>");
	    // audit.println("<processDate>" + new DateTime().formatLongDate() +
	    // "</processDate>");

	    // Process the BillPay file line by line
	    while ((line = in.readLine()) != null)
	    {
		System.out.println("updateBPAYPayments processing " + line);
		audit.println(bpayMgr.processBPAYPayment(line, receiptPayment,
			processPayment));
	    }
	    //
	    // ALL OVER RED ROVER
	    //

	}
	catch (Exception e)
	{
	    System.out
		    .println("Fatal error has prevented the completion of the processing the bpay file."
			    + e);
	    throw new RemoteException(
		    "Fatal error has prevented the completion of the processing the bpay file.",
		    e);
	} finally
	{
	    try
	    {
		// audit.println("</BPAYPaymentAudit>");
		audit.close();
		in.close();
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

	System.out.println("BPAY Update Finished "
		+ new DateTime().formatLongDate());

    }

    /**
     * The main program for the BillPayUpdateReader class
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args)
    {

	if (args.length < 6)
	{
	    System.out.println("Not enough command line arguments.");
	    System.out
		    .println("Usage: BPAYUpdater [localhost:1099] [Update] [fromTelstra] [audit] [receipt Y/N] [process transaction Y/N]");
	    return;
	}
	BPAYUpdater bp = new BPAYUpdater(args[0], args[1], args[2], args[3],
		args[4], args[5]);
    }

}
