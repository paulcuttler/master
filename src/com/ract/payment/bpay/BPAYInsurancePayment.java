package com.ract.payment.bpay;

/**
 * Title:        Master Project
 * Description:  Brings all of the projects together into one master project for deployment.
 * Copyright:    Copyright (c) 2003
 * Company:      RACT
 * @author
 * @version 1.0
 */
import java.rmi.RemoteException;

import com.ract.common.SourceSystem;

public class BPAYInsurancePayment extends BPAYPayment {
    public String bpi;

    public BPAYInsurancePayment() {
	this.setSourceSystem(SourceSystem.INSURANCE);
    }

    public void setPayReferenceNumber(String crn) {
	super.setCustomerReferenceNumber(crn);
	//
	// Set the client and sequence numbers for INSURANCE transactions
	//
	if (crn != null && crn.length() == 11) {
	    this.setClientNumber(crn.substring(0, 6));
	    bpi = new Integer(crn.substring(6, 10)).toString();
	    this.setSeqNumber(new Integer(crn.substring(6, 10)).toString());
	}
    }

    public void notifySourceSystemOfPayment() throws RemoteException {
	// no action
    }

}
