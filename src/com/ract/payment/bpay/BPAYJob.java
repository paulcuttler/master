package com.ract.payment.bpay;

import java.io.File;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.extracts.ExtractHelper;
import com.ract.util.LogUtil;

public class BPAYJob implements Job {

    /**
     * Constructor for the BPayRunBatch object
     */
    public BPAYJob() {
    }

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	String instName = context.getJobDetail().getName();
	String instGroup = context.getJobDetail().getGroup();

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();

	try {
	    String inputFileName = dataMap.getString("inputFile");
	    File f = new File(inputFileName);
	    String providerURL = dataMap.getString("providerURL");

	    String auditFileName = f.getParent() + f.separator + "Audit" + ExtractHelper.getSuffixDateTime() + ".txt";
	    BPAYUpdater bp = new BPAYUpdater(providerURL, "update", inputFileName, auditFileName, "Y", "Y");
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }

}
