package com.ract.payment.bpay;

import java.rmi.RemoteException;

import com.ract.common.SourceSystem;
import com.ract.util.LogUtil;

public class BPAYMembershipPayment extends BPAYPayment {
    public BPAYMembershipPayment() {
	LogUtil.log(this.getClass(), "new MEMBERSHIP BPAY PAYMENT");
	this.setSourceSystem(SourceSystem.MEMBERSHIP);
    }

    public void notifySourceSystemOfPayment() throws RemoteException {

	// try
	// {
	// MembershipDocument md =
	// MembershipEJBHelper.getMembershipDocumentHome().findByPrimaryKey(new
	// Integer(getReferenceId()));
	// MembershipDocumentVO mdVO = md.getVO();
	// setReferenceId(mdVO.getMembershipID().toString());
	//
	// MembershipTransactionMgr memTransMgr =
	// MembershipEJBHelper.getMembershipTransactionMgr();
	// memTransMgr..notifyPendingFeePaid(this);
	// }
	// catch(Exception e)
	// {
	// throw new RemoteException(e.getMessage());
	// }
    }

}
