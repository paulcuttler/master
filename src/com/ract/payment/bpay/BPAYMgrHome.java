package com.ract.payment.bpay;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface BPAYMgrHome extends EJBHome {
    public BPAYMgr create() throws CreateException, RemoteException;
}
