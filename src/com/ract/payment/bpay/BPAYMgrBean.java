package com.ract.payment.bpay;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.DataSourceFactory;
import com.ract.common.SourceSystem;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.payment.electronicpayment.ElectronicPaymentProcessor;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

/**
 * BPAY
 * 
 * @author Leigh Giles
 * @created 1 Feb 2005
 */
public class BPAYMgrBean extends ElectronicPaymentProcessor implements SessionBean {
    private SessionContext sessionContext;

    // DataSource dataSource;

    private static boolean firstTime = true;
    private static int MAKE_PAYMENT = 5;
    //
    private static ArrayList billerCodes = new ArrayList();

    /**
     * 
     * @param line
     * @return
     * @throws RemoteException
     */

    public String processBPAYPayment(String line, boolean receiptPayment, boolean processTransaction) throws RemoteException {
	if (firstTime) {
	    firstTime = false;
	    billerCodes = (ArrayList) setBillerCodes();
	}

	String retStr = null;

	BPAYPayment bpay = parseBPAYLine(line);

	if (bpay != null && bpay.isOK()) {
	    bpay = (BPAYPayment) super.processPayment((ElectronicPayment) bpay, receiptPayment, processTransaction);
	}

	// return bpay.toXML();
	return bpay.toString();
    }

    /**
     * parses a line from the BPAY file from telstra. If there were no errors,
     * it returns a BPAYUpdate object, otherwise a BPAYFileFormatException is
     * thrown
     */
    private BPAYPayment parseBPAYLine(String line) {

	String payTypeIndicator = "";
	String customerReferenceNumber = "";
	String billerCode = "";
	String transactionReferenceNumber = "";
	String originalReferenceNumber = "";
	String errorCorrectionReason = "";
	String payMethod = "";
	String sourceSystem = "";
	DateTime paymentDate = null;
	String entryMethod = "";

	double amount = 0.0;
	String token = "";
	String custId = "";
	String seqNo = "";
	StringBuffer parseStatus = new StringBuffer();
	//
	// Parse this Line
	//
	line = StringUtil.replace(line, ",,", ", ,");

	// StringTokenizer lineTokens = new StringTokenizer(line, ",");
	String lineTokens[] = line.split(",");
	if (lineTokens.length != 10) {
	    parseStatus.append("Need 10 fields got " + lineTokens.length);
	} else {
	    payTypeIndicator = lineTokens[0];

	    billerCode = lineTokens[1];

	    if (!checkBillerCode(billerCode)) {
		parseStatus.append("Incorrect biller code");
	    }

	    // CUSTOMER REFERENCE NUMBER
	    customerReferenceNumber = lineTokens[2];
	    String crn = validateReferenceNumber(customerReferenceNumber);
	    if (!crn.equalsIgnoreCase("ok")) {
		parseStatus.append(crn);
	    }

	    // AMOUNT
	    token = lineTokens[3];
	    if (NumberUtil.isNumeric(token)) {
		amount = (new Double(token).doubleValue() / 100);
	    } else {
		parseStatus.append("amount error");
	    }

	    transactionReferenceNumber = lineTokens[4];

	    payMethod = lineTokens[5];

	    entryMethod = lineTokens[6];

	    // PAYMENT DATE
	    token = lineTokens[7];
	    try {
		token = token.substring(6) + "/" + token.substring(4, 6) + "/" + token.substring(0, 4);
		paymentDate = DateUtil.convertStringToDateTime(token);
	    } catch (java.text.ParseException pe) {
		parseStatus.append("Date format error ");
	    }

	    originalReferenceNumber = lineTokens[8];

	    if (lineTokens.length > 9) {
		errorCorrectionReason = lineTokens[9];
	    } else {
		errorCorrectionReason = " ";
	    }

	    if (errorCorrectionReason.length() < 3) {
		errorCorrectionReason = "000";
	    }

	}

	try {

	    sourceSystem = SourceSystem.getElectronicPaymentSourceSystem(customerReferenceNumber);

	    BPAYPayment bpay = BPAYPaymentFactory.getBPAYPayment(sourceSystem);
	    //
	    // populate the BPAYPayment Object
	    //
	    bpay.setInputData(line);
	    bpay.addStatus(parseStatus.toString());

	    // if ( bpay.isOK() )
	    // {

	    bpay.setPaymentInstructionType(new Integer(payTypeIndicator.trim()).intValue());
	    bpay.setOriginalReferenceNumber(originalReferenceNumber);
	    bpay.setErrorCorrectionReason(new Integer(errorCorrectionReason.trim()).intValue());
	    bpay.setEntryMethod(new Integer(entryMethod.trim()).intValue());
	    bpay.setAmount(amount);
	    bpay.setBillerCode(billerCode);
	    bpay.setTransactionReferenceNumber(transactionReferenceNumber);
	    bpay.setPayReferenceNumber(customerReferenceNumber);
	    bpay.setPaymentMethod(new Integer(payMethod.trim()).intValue());
	    bpay.setDateOfPayment(paymentDate);
	    bpay.setDateReceived(new DateTime());

	    // }

	    return bpay;
	} catch (Exception e) {

	    return null;
	}
    }

    /**
     * Check the biller code against the valid biller codes
     * 
     * @param billerCode
     *            String
     * @return boolean
     */
    private boolean checkBillerCode(String billerCode) {
	Iterator it = billerCodes.iterator();
	boolean found;
	found = false;

	while (it.hasNext()) {
	    found = (billerCode.equalsIgnoreCase((String) it.next())) || found;
	}

	return found;
    }

    /**
     * Description of the Method
     */
    public void ejbCreate() {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbRemove() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbActivate() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbPassivate() throws RemoteException {
    }

    /**
     * Sets the sessionContext attribute of the BPAYMgrBean object
     * 
     * @param sessionContext
     *            The new sessionContext value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	} catch (Exception e) {
	    throw new RemoteException("unable to get the datasource");
	}
    }

    private Collection setBillerCodes() throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getBillerCodes();
    }
}
