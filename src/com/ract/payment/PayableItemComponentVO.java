/*
 *  @(#)PayableItemComponent.java
 *
 */
package com.ract.payment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import com.ract.common.ClassWriter;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.util.LogUtil;

/**
 * A component of an item that is due to be paid.
 * 
 * @hibernate.class table="pt_payableitemcomponent" lazy="false"
 * @hibernate.cache usage="read-write"
 * 
 * @author Glenn Lewis created 10 September 2002
 * @version 1.0, 1/5/2002
 */
public class PayableItemComponentVO
// extends ValueObject
	implements Writable, Serializable {
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((payableItemComponentID == null) ? 0 : payableItemComponentID.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PayableItemComponentVO other = (PayableItemComponentVO) obj;
	if (payableItemComponentID == null) {
	    if (other.payableItemComponentID != null)
		return false;
	} else if (!payableItemComponentID.equals(other.payableItemComponentID))
	    return false;
	return true;
    }

    public final static String WRITABLE_CLASSNAME = "PayableItemComponent";

    /**
     * The ID of the payable item component in payment system
     */
    protected Integer payableItemComponentID;

    /**
     * A vector of line items (ledger entries) that make up the component. That
     * is, a vector of <code>PayableItemPosting</code>.
     */
    private PostingContainer postingList;

    /**
     * The ID of the payable item that this component belongs to
     */
    // protected Integer myContainerID;

    /**
     * The amount of the component
     */

    protected BigDecimal amount;

    /**
     * Description a description of the item
     */
    protected String description;

    /**
     * The reference passed from the source system
     */
    protected String sourceSystemReference;

    /**
     * The ID of the payable item to which this component belongs
     */
    protected Integer payableItemID;

    /**
     * The PayableItem to which this component belongs
     */
    protected PayableItemVO payableItem;

    protected SourceSystem sourceSystem;

    /**
     * Indicates that a payable item component can not be supersedable. This is
     * false by default to indicate that the component is not supersedable.
     */

    /**
     * Constructor *******************************
     * 
     * @param postingsVector
     *            a vector of the line items of this component (i.e a vector of
     *            <code>PayableItemPosting</code>).
     * @param amount
     *            the total amount of this payable item component.
     * @param description
     *            the description of this payable item component. A
     *            <code>null</code> value indicates no description.
     * @param payableItemComponentID
     *            Description of the Parameter
     * @param sourceSystemReference
     *            Description of the Parameter
     * @param mode
     *            Description of the Parameter
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>                 PaymentException</code> if any of the
     *         parameters are invalid.
     */
    public PayableItemComponentVO(Vector postingsVector, BigDecimal amount, String description, Integer payableItemComponentID, String sourceSystemReference, String mode) throws PaymentException {
	// try
	// {
	// setMode(mode);
	// }
	// catch(ValidationException e)
	// {
	// LogUtil.fatal(this.getClass(), "Unable to set mode to loading");
	// }

	setAmount(amount);
	if (description != null) {
	    setDescription(description);
	}
	if (payableItemComponentID != null) {
	    setPayableItemComponentID(payableItemComponentID);
	}
	if (sourceSystemReference != null) {
	    setSourceSystemReference(sourceSystemReference);
	}
	try {
	    setPostings(new PostingContainer((Collection) postingsVector));
	} catch (SystemException ex) {
	    throw new PaymentException("Unable to set postings.", ex);
	}
    }

    public PayableItemComponentVO() {
    }

    public PayableItemComponentVO(BigDecimal amount, String description, Integer payableItemComponentID, String sourceSystemReference, String mode) throws PaymentException {
	// try
	// {
	// setMode(mode);
	// }
	// catch(ValidationException e)
	// {
	// LogUtil.fatal(this.getClass(), "Unable to set mode to loading");
	// }
	// LogUtil.log(this.getClass(), "in the pic bit "+description);
	setAmount(amount);
	if (description != null) {
	    setDescription(description);
	}
	if (payableItemComponentID != null) {
	    setPayableItemComponentID(payableItemComponentID);
	}
	if (sourceSystemReference != null) {
	    setSourceSystemReference(sourceSystemReference);
	}
    }

    /**
     * Constructor for the PayableItemComponentVO object with default mode
     */
    public PayableItemComponentVO(Collection postingsList, BigDecimal amount, String description, Integer payableItemComponentID, String sourceSystemReference) throws PaymentException {

	// try
	// {
	// this.setMode(ValueObject.MODE_NORMAL);
	// }
	// catch(ValidationException e)
	// {
	// LogUtil.log(this.getClass(),
	// "unable to set mode of payableitemcomponent to normal");
	// }

	this.setAmount(amount);
	if (description != null) {
	    this.setDescription(description);
	}
	if (payableItemComponentID != null) {
	    this.setPayableItemComponentID(payableItemComponentID);
	}
	if (sourceSystemReference != null) {
	    this.setSourceSystemReference(sourceSystemReference);
	}
	this.setPostings(postingsList);
    }

    // ============ Getter Methods =============================================

    /**
     * Get the ID of the payable item component in the payment system.
     * 
     * @hibernate.id column="payableitemcomponentid" generator-class="assigned"
     * @return the ID of the payable item component payment system, or
     *         <code>null</code> if the item has not been entered in the payment
     *         system.
     */
    public Integer getPayableItemComponentID() {
	return payableItemComponentID;
    }

    /**
     * Gets the payableItemID attribute of the PayableItemComponentVO object
     * 
     * @hibernate.property
     * @hibernate.column name="payableitemid"
     * @return The payableItemID value
     */
    public Integer getPayableItemID() {
	return this.payableItemID;
    }

    /**
     * Gets the payableItem attribute of the PayableItemComponentVO object
     * 
     * @return The payableItem value
     * @exception PaymentException
     *                Description of the Exception
     */
    public PayableItemVO getPayableItem() throws RemoteException {
	if (this.payableItem == null) {
	    try {
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		this.payableItem = paymentMgr.getPayableItem(this.getPayableItemID());
	    } catch (PaymentException pe) {
		throw new SystemException("Error getting the Payable Item for this component.", pe);
	    }
	}
	return this.payableItem;
    }

    /**
     * Get the postings (line items) of this component
     * 
     * @return the postings (line items) of this component
     */
    public PostingContainer getPostings() {
	LogUtil.log(this.getClass(), "getPostings a");
	if (postingList == null) {
	    try {
		PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
		LogUtil.log(this.getClass(), "getPostings 1" + postingList);
		postingList = pMgr.getPostings(this.sourceSystem, this.payableItemComponentID);
		LogUtil.log(this.getClass(), "getPostings 2" + postingList);
	    } catch (Exception ex) {
		LogUtil.log(this.getClass(), "Unable to retrieve postings for " + payableItemComponentID + ". " + ex);
	    }
	}
	return postingList;
    }

    /**
     * Get the amount of this payable item
     * 
     * @hibernate.property
     * @hibernate.column name="amount"
     * @return the amount of this payable item
     */
    public BigDecimal getAmount() {
	return amount;
    }

    /**
     * Get the description of this payable item component
     * 
     * @hibernate.property
     * @hibernate.column name="description"
     * @return the description of this payable item or <code>null</code> if
     *         there is no description
     */
    public String getDescription() {
	return description;
    }

    /**
     * Gets the sourceSystemReference attribute of the PayableItemComponentVO
     * object
     * 
     * @hibernate.property
     * @hibernate.column name="sourcesystemreference"
     * @return The sourceSystemReference value
     */
    public String getSourceSystemReference() {
	return this.sourceSystemReference;
    }

    /**
     * Return the sum of the unearned postings
     */
    // public BigDecimal getUnearnedIncome()
    // throws RemoteException
    // {
    // BigDecimal unearnedAmount = new BigDecimal(0.0);
    // Collection postingList = getUnearnedPostings();
    // Iterator postingListIterator = postingList.iterator();
    // while(postingListIterator.hasNext())
    // {
    // PayableItemPostingVO posting =
    // (PayableItemPostingVO)postingListIterator.next();
    // LogUtil.debug(this.getClass(),"------------->>"+posting.getAmountOutstanding());
    //
    // unearnedAmount = unearnedAmount.add(new
    // BigDecimal(posting.getAmountOutstanding()).setScale(2,
    // BigDecimal.ROUND_HALF_UP));
    //
    // }
    //
    // return unearnedAmount;
    // }

    /**
     * Return the ratio of the unearned postings over the total income
     */
    // public BigDecimal getUnearnedRatio()
    // throws RemoteException
    // {
    // BigDecimal unearnedRatio = null;
    //
    // BigDecimal unearnedAmount = this.getUnearnedIncome();
    // LogUtil.debug(this.getClass(),"unearnedamount = "+unearnedAmount);
    // PayableItemPostingVO originalUnearnedPosting =
    // this.getOriginalUnearnedPosting();
    // LogUtil.debug(this.getClass(),"originalUnearnedPosting = "+originalUnearnedPosting.getAmountOutstanding());
    // if(originalUnearnedPosting != null)
    // {
    // unearnedRatio = unearnedAmount.divide(new
    // BigDecimal(originalUnearnedPosting.
    // getAmountOutstanding()), 4, BigDecimal.ROUND_HALF_UP);
    //
    // }
    // return unearnedRatio;
    // }

    /**
     * Return the ratio of the earned postings over the total income
     */
    // public BigDecimal getEarnedRatio()
    // throws RemoteException
    // {
    // return new BigDecimal(1.0).subtract(this.getUnearnedRatio());
    // }

    /**
     * Returns the original unearned posting for this component.
     */
    // public PayableItemPostingVO getOriginalUnearnedPosting()
    // {
    // PayableItemPostingVO originalUnearnedPosting = null;
    //
    // Collection postingList = this.getPostings();
    // Iterator postingListIterator = postingList.iterator();
    // LogUtil.debug(this.getClass()," size of postings "+postingList.size());
    // PayableItemPostingVO posting = null;
    // while(postingListIterator.hasNext())
    // {
    // posting = (PayableItemPostingVO)postingListIterator.next();
    // LogUtil.debug(this.getClass(),"posting "+posting.toString());
    // if(posting.getAmountOutstanding() > 0.0 && posting.isEarningsPosting())
    // {
    // if(originalUnearnedPosting == null
    // || posting.getPayableItemPostingID().compareTo(originalUnearnedPosting.
    // getPayableItemPostingID()) < 0
    // )
    // {
    // originalUnearnedPosting = posting;
    // }
    // }
    // }
    //
    // return originalUnearnedPosting;
    // }

    /**
     * Gets the unpaidAmount attribute of the PayableItemComponentVO object
     * 
     * @return The unpaidAmount value
     * @exception PaymentException
     *                Description of the Exception
     */
    public BigDecimal getUnpaidAmount() throws RemoteException {
	// get the original amount payable on the component
	// get the payable item for this component
	PayableItemVO payableItem = getPayableItem();

	// // get the amount yet to pay
	// double payableItemUnpaid = 0.0;
	// // if it's a direct debit, the amount is fetched from the DD system
	// if
	// (PaymentMethod.DIRECT_DEBIT.equals(payableItem.getPaymentMethodDescription()))
	// {
	// PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	// DirectDebitSchedule ddSchedule =
	// paymentMgr.getDirectDebitSchedule(payableItem.getPaymentMethodID());
	// if (ddSchedule != null) {
	// payableItemUnpaid =
	// ddSchedule.getTotalAmountOutstanding().doubleValue();
	// }
	// }
	// // otherwise assume the amount outstanding on the payable item is
	// correct
	// else
	// {
	// payableItemUnpaid = payableItem.getAmountOutstanding();
	// }
	//
	// // and calculate the ratio to apply to the component unearned
	// double unpaidProportion = payableItemUnpaid /
	// payableItem.getAmountPayable();
	//
	// // apply the ratio to the component amount
	// return getAmount().multiply(new
	// BigDecimal(unpaidProportion).setScale(2, BigDecimal.ROUND_HALF_UP));
	return getAmount().multiply(payableItem.getUnpaidRatio()).setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Gets the amount of income that has been paid but not earned.
     * 
     * @return The unpaidAmount value
     * @exception PaymentException
     *                Description of the Exception
     */
    // public BigDecimal getPaidUnearnedAmount()
    // throws RemoteException
    // {
    // /**
    // * The unpaid amount all comes off the unearned as any non-earned amounts
    // are
    // * taken to account immediately and any payment has to be greater than the
    // * non-earned amounts.
    // * @todo Review this rule for Insurance etc.
    // */
    // // get the unpaid amount
    // BigDecimal unpaidAmount = this.getUnpaidAmount();
    // // remove GST
    // CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
    // BigDecimal gstRate = new
    // BigDecimal(commonMgr.getCurrentTaxRate().getGstRate());
    // unpaidAmount =
    // CurrencyUtil.calculateGSTExclusiveAmount(unpaidAmount.doubleValue(),
    // gstRate.doubleValue(), 2);
    // // subtract the unpaid from the unearned
    // return this.getUnearnedIncome().subtract(unpaidAmount);
    // }

    /**
     * Get the list of postings with a current unearned amount.
     * 
     * @return The unearnedPostings value
     * @exception PaymentException
     *                Description of the Exception
     */
    // public PostingContainer getXXXUnearnedPostings()
    // throws RemoteException
    // {
    // PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
    // /**
    // * @todo TEST!!!
    // */
    // return
    // paymentMgr.getUnearnedPostingByComponent(this.getPayableItemComponentID());

    // PayableItemPostingStoreHome payableItemPostingStoreHome = null;
    // payableItemPostingStoreHome =
    // PaymentEJBHelper.getPayableItemPostingStoreHome();
    // PayableItemPostingStore postingStore;
    // PayableItemPostingVO posting;
    // Collection postingStoreList = null;
    // try
    // {
    // postingStoreList =
    // payableItemPostingStoreHome.findUnearnedPostingsByComponentID(this.
    // getPayableItemComponentID());
    // }
    // catch(FinderException fe)
    // {
    // throw new SystemException(fe);
    // }
    //
    // Iterator postingStoreListIterator = postingStoreList.iterator();
    // while(postingStoreListIterator.hasNext())
    // {
    // postingStore = (PayableItemPostingStore)postingStoreListIterator.next();
    // try
    // {
    // posting = postingStore.getPayableItemPosting();
    // }
    // catch(PaymentException pe)
    // {
    // throw new SystemException(pe);
    // }
    // unearnedPostingList.add(posting);
    // }
    // return unearnedPostingList;
    // }

    // ============ Setter Methods =============================================

    /**
     * Set the ID of this payable item component in the payment system.
     * 
     * @param payableItemComponentID
     *            the ID of this payable item in the payment system.
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>                 PaymentException<\code> if <code>payableItemID</code>
     *         is <code>null</code>
     */
    public void setPayableItemComponentID(Integer payableItemComponentID) throws PaymentException {
	if (payableItemComponentID == null) {
	    throw new PaymentException("Setting payable item component ID to null in Payable Item Component");
	}

	this.payableItemComponentID = payableItemComponentID;
    }

    /**
     * Sets the payableItemID attribute of the PayableItemComponentVO object
     * 
     * @param payableItemID
     *            The new payableItemID value
     * @exception PaymentException
     *                Description of the Exception
     */
    public void setPayableItemID(Integer payableItemID) throws PaymentException {
	if (payableItemID == null) {
	    throw new PaymentException("Setting payable item ID to null in Payable Item Component");
	}

	this.payableItemID = payableItemID;
    }

    /**
     * Set the postings of this component.
     * 
     * @param postingsVector
     *            the components of this payable item
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException<\code> if the components
     *      vector does not consist of only valid <code>PayableItemPosting</code>
     *         instances.
     */
    private void setPostings(Collection postingList) throws PaymentException {
	if (postingList == null) {
	    throw new PaymentException("Setting components to null in PayableItem");
	}

	PostingContainer postingCont = null;
	try {
	    postingCont = new PostingContainer(postingList);
	} catch (SystemException ex) {
	    throw new PaymentException("Unable to construct posting container.", ex);
	}

	// only validate mode if not isLoading.
	// if(!isLoading())
	// {
	// if(!postingCont.isBalanced())
	// {
	// throw new PaymentException("Postings do not balance: "
	// +postingCont.getPostingTotal()+ ".");
	// }
	// }

	this.postingList = postingCont;
    }

    public boolean doPostingsBalance() throws PaymentException {
	PostingContainer postings = getPostings();
	boolean isBalanced = false;
	if (postings != null) {
	    isBalanced = postings.isBalanced();
	} else {
	    isBalanced = true;
	}
	return isBalanced;
    }

    /**
     * Set the (total) amount of payable item
     * 
     * @param amount
     *            the total amount of this payable item.
     */
    protected void setAmount(BigDecimal amount) {
	// if(amount != null)
	// {
	// LogUtil.log(this.getClass(), "component amount = " + amount);
	// }
	this.amount = amount;
    }

    /**
     * Set the description on this payable item component
     * 
     * @param description
     *            the description of this payable item component
     * @exception PaymentException
     *                Description of the Exception
     * @throws <code>               PaymentException<\code> if the description
     *      is <code>null</code>
     */
    protected void setDescription(String description) throws PaymentException {
	if (description == null) {
	    throw new PaymentException("Attempting to set the description of a payable item component to null.");
	}

	this.description = description;
    }

    /**
     * Sets the sourceSystemReference attribute of the PayableItemComponentVO
     * object
     * 
     * @param sourceSystemReference
     *            The new sourceSystemReference value
     */
    protected void setSourceSystemReference(String sourceSystemReference) {
	this.sourceSystemReference = sourceSystemReference;
    }

    /**
     * Gets the directDebit attribute of the PayableItemComponentVO object
     * 
     * @return The directDebit value
     * @exception RemoteException
     *                Description of the Exception
     * @exception PaymentException
     *                Description of the Exception
     */
    public boolean isDirectDebit() throws RemoteException, PaymentException {
	return getPayableItem().getPaymentMethod().isDirectDebitPaymentMethod();
    }

    /**
     * Return true if the component has any postings that specify an unearned to
     * earned account transfer and has income to be earned.
     */
    // public boolean isEarnable()
    // {
    // boolean hasEarningsPosting = false;
    // Collection postingList = getPostings();
    // Iterator postingListIterator = postingList.iterator();
    // while(!hasEarningsPosting && postingListIterator.hasNext())
    // {
    // PayableItemPostingVO posting =
    // (PayableItemPostingVO)postingListIterator.next();
    // hasEarningsPosting = (posting.isEarningsPosting() &&
    // posting.getAmountOutstanding() > 0.0);
    // }
    // return hasEarningsPosting;
    // }

    /**
     * Return true if the component has any postings that specify an unearned to
     * earned account transfer.
     */
    public boolean hasPostingToUnearnedAccount() {
	boolean hasUnearnedPosting = false;
	Collection postingList = getPostings();
	Iterator postingListIterator = postingList.iterator();
	while (!hasUnearnedPosting && postingListIterator.hasNext()) {
	    PayableItemPostingVO posting = (PayableItemPostingVO) postingListIterator.next();
	    hasUnearnedPosting = (posting.isEarningsPosting());
	}
	return hasUnearnedPosting;
    }

    /**
     * Return true if the component has no supersedingComponentID.
     */
    // public boolean isCurrent()
    // {
    // return(this.getSupersedingComponentID() == null);
    // }

    /**
     * Return true if the component has any postings that specify an unearned to
     * earned account transfer and not all of the amount unearned has been
     * transfered yet.
     */
    // public boolean hasUnearnedEarningsPosting()
    // {
    // boolean hasUnearnedPosting = false;
    // Collection postingList = getPostings();
    // Iterator postingListIterator = postingList.iterator();
    // while(!hasUnearnedPosting && postingListIterator.hasNext())
    // {
    // PayableItemPostingVO posting =
    // (PayableItemPostingVO)postingListIterator.next();
    // hasUnearnedPosting = posting.isEarningsPosting() &&
    // posting.getAmountOutstanding() > 0.0;
    // }
    // return hasUnearnedPosting;
    // }

    /**
     * Has the set of postings been transferred to the finance system.
     * 
     * @return
     */
    public boolean postingsHaveBeenTransferred() {
	boolean postingsHaveBeenTransferred = false;
	LogUtil.log(this.getClass(), "postingsHaveBeenTransferred 1");
	Vector postingList = this.getPostings();
	LogUtil.log(this.getClass(), "postingsHaveBeenTransferred 2" + postingList);
	Iterator postingListIterator = postingList.iterator();
	while (!postingsHaveBeenTransferred && postingListIterator.hasNext()) {
	    PayableItemPostingVO posting = (PayableItemPostingVO) postingListIterator.next();
	    LogUtil.log(this.getClass(), "postingsHaveBeenTransferred 3" + posting);
	    postingsHaveBeenTransferred = posting.getPostedDate() != null;
	}
	return postingsHaveBeenTransferred;
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	if (this.getSourceSystem() != null) {
	    desc.append(this.getSourceSystem().getAbbreviation() + ":");
	}
	desc.append(this.getSourceSystemReference() + ":");
	desc.append(this.getDescription() + "\n");
	// if (this.payableItemComponentID != null)
	// {
	// PostingContainer postings = this.getPostings();
	// if(postings != null)
	// {
	// desc.append(postings.toString());
	// }
	// }
	return desc.toString();
    }

    /**
     * Description of the Method
     * 
     * @param cw
     *            Description of the Parameter
     * @param deepWrite
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("amount", this.amount);
	// cw.writeAttribute("containerID", this.myContainerID);
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("payableItemComponentID", this.payableItemComponentID);
	cw.writeAttribute("payableItemID", this.payableItemID);
	if (deepWrite) {
	    if (this.postingList != null) {
		cw.writeAttributeList("postingList", this.postingList);
	    }
	}
	cw.endClass();
    }

    public SourceSystem getSourceSystem() {
	return sourceSystem;
    }

    public void setSourceSystem(SourceSystem sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

}
