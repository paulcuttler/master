package com.ract.payment;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Properties;

import javax.ejb.ObjectNotFoundException;
import javax.ejb.Remote;
import javax.ejb.RemoveException;

import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.DateTime;

@Remote
public interface PaymentTransactionMgr {

    public static final String ACTION_UNCANCEL = "uncancel";

    /**
     * Add a payableItem to the Payment System.
     * 
     * @param payableItem
     *            The feature to be added to the PayableItem attribute
     * @return the ID of the payable Item in the Payment system
     * @exception RemoteException
     *                Description of the Exception
     * @exception PaymentException
     *                Description of the Exception
     */
    public PayableItemVO addPayableItem(PayableItemVO payableItem) throws RollBackException, PaymentException, RemoteException;

    public PayableItemVO addPayableItem(PayableItemVO payableItem, boolean paymentMethodRequired, DateTime createDateTime, String paymentSystem) throws RollBackException;

    /**
     * Clears the amount outstanding on all of the payable items for the client
     * where the payable item was created by the given source system.
     */
    public int clearAmountOutstandingForClient(SourceSystem sourceSystem, Integer clientNumber) throws RemoteException;

    public void updatePendingFee(PendingFee pendingFee) throws RemoteException;

    // public void removeExpiredPendingFees()
    // throws RollBackException, RemoteException;
    //
    // public void removeExpiredPendingFees(DateTime expiryDate)
    // throws RollBackException, RemoteException;

    public void updatePayableItem(PayableItemVO payableItem) throws RollBackException, PaymentException, RemoteException;

    public void removePayableItem(Integer payableItemID, UserSession userSession) throws RemoteException, ObjectNotFoundException, RemoveException, PaymentException;

    public void removeDirectDebitSchedule(Integer ddScheduleId) throws RemoteException, RollBackException;

    public void transferReceipts(Integer fromCustomerNumber, Integer toCustomerNumber, String userId) throws RollBackException, RemoteException;

    public void createPayablesFromPayableItems(Integer startPayableItemId, Integer endPayableItemId) throws RollBackException, RemoteException;

    public void deleteReceiptingCustomer(String customerNumber, String userId) throws RollBackException, RemoteException;

    /**
     * get to the ReceptorReceiptingAdapter so a the data can be sent to
     * createPaidTransaction()
     * 
     * @param username
     *            Description of the Parameter
     * @param branch
     *            Description of the Parameter
     * @param till
     *            Description of the Parameter
     * @param receipt
     *            Description of the Parameter
     * @param products
     *            Description of the Parameter
     * @param costs
     *            Description of the Parameter
     * @param paymentType
     *            Description of the Parameter
     * @param tendered
     *            Description of the Parameter
     * @param data
     *            Description of the Parameter
     * @param comments
     *            Description of the Parameter
     * @param date
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void createPaidTransaction(String username, String branch, String till, String receipt, String customer, String[] products, double[] costs, String[] paymentType, double[] tendered, Properties[] data, String comments, java.util.Date date, String sourceSystem) throws RemoteException, RollBackException;

    public String createPayableFee(Integer clientNumber, String sequenceNumber, String product, Double amountPayable, String description, String sourceSystem, String branch, String user, Properties[] properties) throws RemoteException, RollBackException;

    public void cancelPaymentMethod(PaymentMethod paymentMethod, SourceSystem sourceSystem) throws RemoteException, RollBackException;

    /**
     * Cancel the remaining direct debit deductions on the specified direct
     * debit schedule NOTE: This method updates sub systems. The payment manager
     * must be committed or rolled back.
     * 
     * @param directDebitScheduleID
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void cancelDirectDebitSchedule(Integer directDebitScheduleID, String userID) throws RemoteException, RollBackException;

    public void createPayableItem(PayableItemVO payableItem) throws RemoteException, PaymentException;

    public void createPayableItemComponent(PayableItemComponentVO payableItemComponent) throws RemoteException, PaymentException;

    public void deletePayableItemComponent(PayableItemComponentVO payableItemComponent) throws RemoteException, PaymentException;

    public void createPayableItemPosting(PayableItemPostingVO payableItemPosting) throws RemoteException, PaymentException;

    public void deletePayableItemPosting(PayableItemPostingVO payableItemPosting) throws RemoteException, PaymentException;

    public void updatePayableItemPosting(PayableItemPostingVO payableItemPosting) throws RemoteException, PaymentException;

    /**
     * Transfer a direct debit schedule that has failed from receipting system
     * into direct debit system.
     * 
     * @param payableItemID
     *            Integer
     * @param user
     *            UserVO
     * @throws RemoteException
     * @throws RollBackException
     */
    public void transferDirectDebitSchedule(Integer payableItemID, User user) throws RemoteException, RollBackException;

    /**
     * Cancel the remaining direct debit deductions on all schedules relating to
     * the specified direct debit authority NOTE: This method updates sub
     * systems. The payment manager must be committed or rolled back.
     * 
     * @param directDebitAuthorityID
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void cancelDirectDebitScheduleByDirectDebitAuthority(Integer directDebitAuthorityID, String userID) throws RemoteException, RollBackException;

    /**
     * Undo a payable item. This means removing (deleting) the payable item and
     * reinstating a previous payable item. If there has been no payment made
     * and the postings have not been transferred to the finance system, remove
     * the payable item and its children (components, postings and actions). If
     * the payment method was direct debit then remove the direct debit schedule
     * in the direct debit sub-system and reinstate the previous direct debit
     * schedule (if any). If the payment method was receipting then remove the
     * payable fee in the receipting system. NOTE: The payment manager must be
     * committed or rolled back.
     * 
     * @param payableItemID
     *            Description of the Parameter
     * @param previousPayableItemID
     *            Description of the Parameter
     * @param userSession
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void undoPayableItem(Integer payableItemID, Integer previousPayableItemID, UserSession userSession) throws RemoteException, ValidationException, RollBackException;

    public void undoPayableItem(Integer payableItemID, Integer previousPayableItemID, String action, UserSession userSession) throws RemoteException, ValidationException, RollBackException;

    /**
     * Description of the Method
     * 
     * @param authority
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Hashtable createDirectDebitAuthority(DirectDebitAuthority authority) throws RemoteException, RollBackException;

    public Integer createDirectDebitSchedule(DirectDebitSchedule ddSchedule) throws RemoteException, RollBackException;

    public Integer createRenewalDirectDebitSchedule(DirectDebitSchedule ddSchedule) throws RemoteException, RollBackException;

    // public int updateDirectDebitScheduleID(Integer oldScheduleID, Integer
    // newScheduleID)
    // throws RemoteException;

    public Integer updateDirectDebitAuthority(DirectDebitAuthority newAuthority, String userID) throws RemoteException, RollBackException;

    public void removePayableFee(Integer clientNo, String paymentMethodId, SourceSystem sourceSystem, String userId) throws RollBackException, RemoteException;

    public void createPendingFee(Integer clientNumber, String pendingFeeID, DateTime feeEffectiveDate, DateTime feeExpiryDate, String sourceSystem, Integer sourceSystemReferenceNo, String salesBranchCode, String productCode, Long referenceNumber, Long sequenceNumber, String membershipNumber, double amount, String userID, String description) throws RemoteException, PaymentException, RollBackException;

    /**
     * Remove the pending fee AND payable fee
     */
    public void removePendingFee(PendingFee pendingFee) throws RollBackException, RemoteException;

    /**
     * Description of the Method
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws RemoteException;

    /**
     * Remove the pending fee record so it will be deleted from IVR. Also delete
     * the payable fee record if one exists.
     */
    public void removePendingFee(String pendingFeeID) throws ObjectNotFoundException, RemoteException, RollBackException;

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void commit() throws RemoteException;

    public String receiptOnlinePayment(SourceSystem sourceSystem, double amount, String cardNumber, DateTime paymentDate, String cardExpiryDate, String seqNumber, String clientNumber, String userId, String branchCode, String locationCode, String tillCode, String cardType, String cardHolder, String cvv) throws RollBackException;

    public String receiptElectronicPayment(ElectronicPayment electronicPayment) throws RemoteException, RollBackException;

    public void updateComponents(Collection componentList) throws RemoteException;

    public void updatePayableData() throws RemoteException;

    /**
     * Extract the Insurance items that are eligible for payment via IVR
     * 
     * @return the pathname of the extract
     */
    public String extractInsuranceIVR() throws RemoteException;

    // public void markScheduleItemAsPaid(Integer ddPayableSeq, Integer
    // ddSchedSeq, BigDecimal amount,
    // Integer receiptNo, String logonId)
    // throws RollBackException, RemoteException;

    /**
     * if the Adapter holds a connection open, then release it. Including any
     * Progress appserver connections
     */
    public void release() throws RemoteException;

    public void performRefund(FinanceVoucher creditDisposal) throws RemoteException;

    // public void setReceptorAmountPayable ( SourceSystem sourceSystem,
    // Integer clientNumber,
    // String paymentMethodId,
    // BigDecimal newAmount
    // )
    // throws RollBackException;

    public void setOCRPayableAmountOutStanding(SourceSystem sourceSystem, Integer clientNumber, String sequenceNumber, BigDecimal amountOutStanding) throws RollBackException;

}
