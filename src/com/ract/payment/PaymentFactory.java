/*
 *  @(#)PaymentFactory.java
 *
 */
package com.ract.payment;

import java.rmi.RemoteException;

import org.w3c.dom.Node;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.payment.directdebit.DirectDebitAdapter;
import com.ract.payment.directdebit.ProgressDDAdapter;
import com.ract.payment.finance.FinanceAdapter;
import com.ract.payment.finance.FinanceOneFinanceAdapter;
import com.ract.payment.finance.ProgressFinanceAdapter;
import com.ract.payment.receipting.ECRAdapter;
import com.ract.payment.receipting.HistoricalOCRAdapter;
import com.ract.payment.receipting.OCRAdapter;
import com.ract.payment.receipting.ReceiptingAdapter;
import com.ract.payment.receipting.ReceptorReceiptingAdapter;
import com.ract.util.LogUtil;

/**
 * Factory for generating payment adaptors. Implemented as a singleton.
 * 
 * @author Glenn Lewis
 * @created 31 July 2002
 * @version 1.0, 1/5/2002
 */

public class PaymentFactory {
    /**
     * The payment factory cannot be constructed. All methods are static.
     */
    private PaymentFactory() {
    }

    /**
     * Get an adaptor that provides an interface to the direct debit system.
     * 
     * @return the Direct Debit adapter
     */
    public static DirectDebitAdapter getDirectDebitAdapter() {
	return (DirectDebitAdapter) new ProgressDDAdapter();
    }

    public static ReceiptingAdapter getReceiptingAdapter(String systemName) throws RemoteException {

	ReceiptingAdapter receiptingAdapter = null;
	if (SourceSystem.OCR.getAbbreviation().equals(systemName)) {
	    LogUtil.debug("PaymentFactory ", "OCR Receipting Adapter");
	    receiptingAdapter = new OCRAdapter();
	} else if (SourceSystem.RECEPTOR.getAbbreviation().equals(systemName)) {
	    LogUtil.debug("PaymentFactory ", "RECEPTOR Receipting Adapter");
	    receiptingAdapter = new ReceptorReceiptingAdapter();
	} else if (SourceSystem.ECR.getAbbreviation().equals(systemName)) {
	    LogUtil.debug("PaymentFactory ", "ECR Receipting Adapter");
	    receiptingAdapter = new ECRAdapter();
	} else {
	    throw new SystemException("Invalid receipting adapter selected '" + systemName + "'.");

	}
	return receiptingAdapter;
    }

    public static ReceiptingAdapter getReceiptingAdapter(SourceSystem sourceSystem) throws RemoteException {
	if (sourceSystem == null) {
	    throw new SystemException("A valid source system has not been provided to retrieve a receipting adapter.");
	}

	String receiptingSystem = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.RECEIPTING_SYSTEM + sourceSystem.getAbbreviation());

	LogUtil.debug("getReceiptingAdapter", "receiptingSystem='" + receiptingSystem + "'.");

	// only Receptor and OCR supported.

	return getReceiptingAdapter(receiptingSystem);

    }

    /**
     * Get an adaptor that provides an interface to the OCR system. THIS ADAPTER
     * CAN ONLY BE USED FOR HISTORICAL PURPOSES. Sam McLennan 17SEP03
     * 
     * @return the historical receipting adapter
     */
    public static HistoricalOCRAdapter getHistoricalOCRAdapter() {
	return (HistoricalOCRAdapter) new HistoricalOCRAdapter();
    }

    /**
     * Get an adaptor that provides an interface to the finance system.
     * 
     * @return the finance adapter
     */
    public static FinanceAdapter getFinanceAdapter() {
	/**
	 * @todo Receptor uses this call. Will need to call alternate method
	 *       passing in source system.
	 */
	return new ProgressFinanceAdapter(); // default
    }

    public static BankAccount getBankAccount(Node bankAccountNode) throws SystemException {
	BankAccount bankAccount = null;
	if (bankAccountNode != null) {
	    String nodeName = bankAccountNode.getNodeName();
	    if (DebitAccount.WRITABLE_CLASSNAME.equals(nodeName)) {
		bankAccount = new DebitAccount(bankAccountNode);
	    } else if (CreditCardAccount.WRITABLE_CLASSNAME.equals(nodeName)) {
		bankAccount = new CreditCardAccount(bankAccountNode);
	    } else {
		// bank account node but now a valid type
		throw new SystemException("Bank account node exists but not a valid type '" + nodeName + "'.");
	    }
	}
	return bankAccount;

    }

    public static FinanceAdapter getFinanceAdapter(String financeSystem) throws SystemException {
	if (FinanceAdapter.FINANCE_SYSTEM_MFGPRO.equals(financeSystem)) {
	    return (FinanceAdapter) new ProgressFinanceAdapter();
	} else if (FinanceAdapter.FINANCE_SYSTEM_FINANCE_ONE.equals(financeSystem)) {
	    return (FinanceAdapter) new FinanceOneFinanceAdapter();
	} else {
	    // return null;
	    throw new SystemException("Finance system '" + financeSystem + "' not recognised.");
	}
    }

    /**
     * Get an adaptor that provides an interface to the finance system. This
     * implementation will need to take in a source system.
     * 
     * @param sourceSystem
     *            SourceSystem
     * @throws SystemException
     * @return FinanceAdapter
     */
    public static FinanceAdapter getFinanceAdapter(SourceSystem sourceSystem) throws RemoteException {
	// system parameter

	// FINANCE_SYSTEM_RACTI
	// FINANCE_SYSTEM_RACT

	System.out.println("ss=" + sourceSystem);

	String financeSystemKey = null;
	// select a finance adapter
	if (sourceSystem != null && SourceSystem.INSURANCE_RACT.equals(sourceSystem)) {
	    financeSystemKey = SystemParameterVO.FINANCE_SYSTEM_RACTI;
	} else {
	    financeSystemKey = SystemParameterVO.FINANCE_SYSTEM_RACT;
	}

	System.out.println("financeSystemKey=" + financeSystemKey);

	// lookup with key
	String financeSystem = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, financeSystemKey);

	return getFinanceAdapter(financeSystem);

    }

}
