package com.ract.payment;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface PaymentIDMgrHome extends EJBHome {
    public PaymentIDMgr create() throws CreateException, RemoteException;
}