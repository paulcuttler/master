package com.ract.payment;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.DataSourceFactory;
import com.ract.common.SequenceMgr;
import com.ract.common.SystemException;
import com.ract.util.LogUtil;

public class PaymentIDMgrBean implements SessionBean {
    private SessionContext sessionContext;

    private DataSource dataSource;

    /**
     * Return the next ID in the Payable Item ID sequence.
     */
    public int getNextPayableItemID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_PAY_ITEM);
	// return PaymentIDCache.getInstance().getNextPayableItemID(this);
    }

    /**
     * @deprecated Return the next ID in the Payable Item ID sequence.
     */
    public int getNextPayableItemActionID() throws RemoteException {
	return PaymentIDCache.getInstance().getNextPayableItemActionID(this);
    }

    /**
     * Return the next ID in the Payable Item Component ID sequence.
     */
    public int getNextPayableItemComponentID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_PAY_COMPONENT);
	// return
	// PaymentIDCache.getInstance().getNextPayableItemComponentID(this);
    }

    /**
     * Return the next ID in the Payable Item Posting ID sequence.
     */
    public int getNextPayableItemPostingID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_PAY_POSTING);
	// return
	// PaymentIDCache.getInstance().getNextPayableItemPostingID(this);
    }

    /**
     * Return the next journal number. Initialise it if it has not already been
     * initialised.
     */
    public int getNextJournalNumber() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_PAY_JOURNAL);
	// return PaymentIDCache.getInstance().getNextJournalNumber(this);
    }

    /**
     * Fetch the last used PayableItemID from the database
     */
    public int getLastPayableItemID() throws RemoteException {
	int maxID = 0;
	Connection connection = null;
	String sql = null;
	PreparedStatement statement = null;
	try {
	    LogUtil.log(this.getClass(), "this=" + this);
	    LogUtil.log(this.getClass(), "getLastPayableItemID 1");
	    DataSource dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    LogUtil.log(this.getClass(), "getLastPayableItemID 2");
	    connection = dataSource.getConnection();
	    LogUtil.log(this.getClass(), "getLastPayableItemID 3");
	    sql = "select max(payableItemID) from PUB.pt_PayableItem";
	    statement = connection.prepareStatement(sql);
	    LogUtil.log(this.getClass(), "getLastPayableItemID 4");
	    ResultSet resultSet = statement.executeQuery();
	    LogUtil.log(this.getClass(), "getLastPayableItemID 5");
	    if (resultSet.next()) {
		maxID = resultSet.getInt(1);
	    }
	    LogUtil.log(this.getClass(), "getLastPayableItemID 6");
	} catch (SQLException e) {
	    throw new SystemException("Error executing SQL " + sql + " : " + e.toString());
	} catch (NamingException ne) {
	    throw new SystemException("Error getting Last Payable Item ID: " + ne.getMessage());
	} finally {
	    closeConnection(connection, statement);
	}

	return maxID;
    } // getLastPayableItemID

    public int getLastPayableItemActionID() throws RemoteException {
	int maxID = 0;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = "select max(payableItemActionID) from PUB.pt_PayableItemAction";
	try {
	    DataSource dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		maxID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new SystemException("Error executing SQL " + statementText + " : " + e.toString());
	} catch (NamingException ne) {
	    throw new SystemException("Error getting Last Payable Item Action ID: " + ne.getMessage());
	} finally {
	    closeConnection(connection, statement);
	}
	return maxID;
    }

    /**
     * Fetch the last used PayableItemPostingID from the database
     */
    public int getLastPayableItemPostingID() throws RemoteException {
	int maxID = 0;
	Connection connection = null;
	String sql = null;
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    connection = dataSource.getConnection();
	    sql = "select max(payableItemPostingID) from PUB.pt_PayableItemPosting";
	    statement = connection.prepareStatement(sql);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		maxID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new SystemException("Error executing SQL " + sql + " : " + e.toString());
	} catch (NamingException ne) {
	    throw new SystemException("Error getting Last Payable Item Posting ID: " + ne.getMessage());
	} finally {
	    closeConnection(connection, statement);
	}

	return maxID;
    } // getLastPayableItemPostingID

    /**
     * Fetch the last used PayableItemComponentID from the database
     */
    public int getLastPayableItemComponentID() throws RemoteException {
	int maxID = 0;
	Connection connection = null;
	String sql = null;
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    connection = dataSource.getConnection();
	    sql = "select max(payableItemComponentID) from PUB.pt_PayableItemComponent";
	    statement = connection.prepareStatement(sql);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		maxID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new SystemException("Error executing SQL " + sql + " : " + e.toString());
	} catch (NamingException ne) {
	    throw new SystemException("Error getting Last Payable Item Component ID: " + ne.getMessage());
	} finally {
	    closeConnection(connection, statement);
	}

	return maxID;
    } // getLastPayableItemComponentID

    public int getLastJournalNumber() throws RemoteException {
	int maxJournalNum = 0;
	Connection connection = null;
	String statementText = null;
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    connection = dataSource.getConnection();
	    statementText = "select max(journalNo) from PUB.pt_PayableItemPosting";
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		maxJournalNum = resultSet.getInt(1);
	    } else {
		// Did not return a record and didn't throw an error
		throw new RemoteException("Failed to get max journal number for PayableItemPosting : No records returned.");
	    }
	} catch (SQLException e) {
	    throw new RemoteException("Error executing SQL " + statementText + " : " + e.toString());
	} catch (NamingException ne) {
	    throw new RemoteException(ne.getMessage(), ne);
	} finally {
	    closeConnection(connection, statement);
	}
	return maxJournalNum;
    }

    /**
     * Reset all of the IDs so that they are initialised again.
     */
    public void reset() {
	PaymentIDCache.getInstance().reset();
    }

    private static void closeConnection(Connection connection, Statement statement) {
	try {
	    if (statement != null) {
		statement.close();
	    }
	} catch (SQLException e) {
	}

	try {
	    if (connection != null) {
		connection.close();
	    }
	} catch (SQLException e) {
	}
    }

    public void ejbCreate() {
    }

    public void ejbRemove() throws RemoteException {
    }

    public void ejbActivate() throws RemoteException {
    }

    public void ejbPassivate() throws RemoteException {
    }

    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
    }

}