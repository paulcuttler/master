/*
 * @(#)PaymentMgrBean.java
 *
 */
package com.ract.payment;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.TransactionGroup;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.receipting.Payable;
import com.ract.payment.receipting.Receipt;
import com.ract.util.DateTime;

/**
 * Define the remote interface for the PaymentMgr session bean. The class
 * PaymentMgrBean implements the remote interface.
 * 
 * @author hollidayj
 * @created 31 July 2002
 */
@Remote
public interface PaymentMgr {
    public static String MEM_AUTO_RENEWAL = "MEM_Auto_Renewal";

    public static String MEM_MANUAL_RENEWAL = "MEM_Manual_Renewal";

    /**
     * Keys to accounts in the finance system:
     */
    public static final String WRITE_OFF_ACCOUNT = "WRITEOFF";

    public static final String CREDIT_ACCOUNT = "CREDIT";

    public static final String DISCRETIONARY_DISCOUNT_COMP_CREDIT = "Disc Discount Comp Credit";

    public static final String DISCRETIONARY_DISCOUNT_ADJUSTMENT = "Disc Discount Adjustment";

    public PayableItemComponentVO createPostingsForDiscretionaryDiscount(Integer transactionReference, TransactionGroup transGroup, String sourceSystemReference, String discType) throws RemoteException, PaymentException;

    public Hashtable getOrphanedDDSchedules(String sourceSystem) throws SystemException;

    /**
     * Retrieves a map of failed DD schedules containing: - clientNo - opSeqNo -
     * amt - payableSeq
     */
    public List getFailedDDSchedules(Integer clientNo, String sourceSystem) throws SystemException;

    /**
     * Retrieve a mapping of client numbers for posting totals for date range.
     * 
     * @param accountNumber
     * @param startDate
     * @param endDate
     * @return
     * @throws RemoteException
     */
    public Map<Integer, BigDecimal> getPostingTotalsForAccount(Integer accountNumber, DateTime startDate, DateTime endDate) throws RemoteException;

    public Map<String, String> getUnlinkedDDSchedules(DateTime startDate);

    public Collection getPayableItems(String paymentMethod, String paymentMethodId);

    public List<PayableItemPostingVO> getPayableItemPostingsByClientAndAccount(Integer clientNumber, Integer accountNumber, DateTime startDate, DateTime endDate) throws RemoteException;

    public BigDecimal getTotalPostings(Integer accountNumber) throws RemoteException;

    public BigDecimal getTotalPostings(Integer accountNumber, DateTime startDate, DateTime endDate) throws RemoteException;

    public BigDecimal getTotalVouchers() throws RemoteException;

    public BigDecimal getTotalVouchers(DateTime startDate, DateTime endDate) throws RemoteException;

    public List<Map<String, Object>> getApprovedVouchers() throws RemoteException;

    /**
     * Return a mapping of client numbers to voucher amounts for a date range.
     * 
     * @param startDate
     * @param endDate
     * @return
     * @throws RemoteException
     */
    public Map<Integer, BigDecimal> getVouchersByRange(DateTime startDate, DateTime endDate) throws RemoteException;

    public Hashtable getJournals(String journalKey) throws RemoteException;

    public Collection getPostingSummaryDetails(String journalKey) throws PaymentException, RemoteException;

    public Collection getPostingSummary(String journalKey) throws PaymentException, RemoteException;

    public Collection getComponents(SourceSystem srcSystem, Integer payableItemID) throws RemoteException;

    public Collection<PaymentTypeVO> getPaymentTypes();

    public PaymentTypeVO getPaymentTypeVO(String paymentTypeCode);

    public void removeExpiredPendingFee(String pendingFeeId) throws RollBackException, RemoteException;

    public Receipt getReceiptByReceiptId(String receiptId) throws PaymentException;

    public Receipt getReceiptByReceiptId(SourceSystem sourceSystem, String receiptId) throws PaymentException;

    public Collection createCancelTypePostings(String referenceNumber, MembershipTransactionVO memTransVO, String description, boolean fullyUnpaid) throws RemoteException, RollBackException;

    public Collection getPoliciesOnAccount(Integer ddAuthorityId) throws RemoteException;

    public PayableItemComponentVO getPayableItemComponent(Integer payableItemComponentID) throws RemoteException, PaymentException;

    public Collection getClientsWithOutstandingAmounts(String paymentMethod) throws RemoteException;

    public PostingContainer getUnearnedPostingByComponent(Integer payableItemComponentID) throws RemoteException;

    public PostingContainer getPostings(SourceSystem srcSystem, Integer payableItemComponentID) throws RemoteException, PaymentException;

    /**
     * get a posting summary for a range.
     * 
     * @param startDate
     *            Description of the Parameter
     * @param endDate
     *            Description of the Parameter
     * @return The postingsSummary value
     * @exception RemoteException
     *                Description of the Exception
     * @exception PaymentException
     *                Description of the Exception
     */
    public Collection getPostingSummary(DateTime entDate, DateTime effDate, String accountNumber) throws PaymentException, RemoteException;

    public Hashtable getJournals(DateTime enteredDate, DateTime effectiveDate, String accountNumber) throws RemoteException;

    public PayableItemPostingVO getPayableItemPosting(Integer payableItemPostingID) throws RemoteException;

    public PayableItemVO getPayableItem(Integer payableItemID) throws RemoteException, PaymentException;

    public Collection<Payable> getReceiptingHistory(Integer clientNo, SourceSystem sourceSystem) throws RemoteException;

    public Vector getDirectDebitScheduleListByDirectDebitAuthority(DirectDebitAuthority ddAuthority) throws RemoteException, PaymentException;

    /**
     * Get the payable item for a given client ID
     * 
     * @param clientNo
     *            Description of the Parameter
     * @return Collection of PayableItem.
     * @exception RemoteException
     *                Description of the Exception
     * @exception PaymentException
     *                Description of the Exception
     */
    public Collection getPayableItemsByClient(Integer clientNumber) throws RemoteException, PaymentException;

    public PayableItemVO getPayableItemByClientAndSource(Integer clientNumber, Integer sourceSystemReference) throws RemoteException, PaymentException;

    public Collection reverseAllPostings(PayableItemComponentVO component, String description) throws RemoteException, PaymentException;

    /*
     * public Collection reverseAllPostings(MembershipTransactionVO memTransVO,
     * String description) throws RemoteException;
     */

    public void resetPaymentTypeCache() throws RemoteException;

    /**
     * Return the payable items for the client that have a payment method of
     * "Receipting" and the specified payment method ID.
     */
    public Collection getPayableItemsByClientReceipt(Integer clientNumber, String paymentMethodID) throws RemoteException;

    public Collection<PayableItemVO> getPayableHistory(Integer clientNumber) throws RemoteException;

    /**
     * Returns a list of PayableItem that have an amount oustanding for the
     * client and were sent from the source system. Returns an empty list if no
     * payable items can be returned.
     * 
     * @param sourceSystem
     *            Description of the Parameter
     * @param clientNumber
     *            Description of the Parameter
     * @return The outstandingPayableItemsForClient value
     * @exception PaymentException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getOutstandingPayableItemsForClient(SourceSystem sourceSystem, Integer clientNumber) throws PaymentException, RemoteException;

    public Collection<PayableItemVO> getOutstandingPayableItemsForClient(SourceSystem sourceSystem, Integer clientNumber, String paymentMethod) throws RemoteException;

    // public Collection getCurrentEarnablePayableItems ( SourceSystem
    // sourceSystem
    // , Integer clientID
    // , String sourceSystemReference
    // )
    // throws RemoteException, PaymentException;

    public Collection<PayableItemVO> getOutstandingPayableItems(SourceSystem sourceSystem, DateTime startDate, DateTime endDate, String paymentMethod) throws RemoteException;

    /**
     * Gets the paymentMethod attribute of the PaymentMgr object
     * 
     * @param paymentMethodDescription
     *            Description of the Parameter
     * @param paymentMethodID
     *            Description of the Parameter
     * @return The paymentMethod value
     * @exception RemoteException
     *                Description of the Exception
     */
    public PaymentMethod getPaymentMethod(String paymentMethodDescription, String paymentMethodID) throws RemoteException;

    /**
     * Gets the paymentMethodByPayableItem attribute of the PaymentMgr object
     * 
     * @param payableItemID
     *            Description of the Parameter
     * @return The paymentMethodByPayableItem value
     * @exception RemoteException
     *                Description of the Exception
     */
    public PaymentMethod getPaymentMethodByPayableItem(Integer payableItemID) throws RemoteException, PaymentException;

    /**
     * Gets the directDebitAuthorityListByClient attribute of the PaymentMgr
     * object
     * 
     * @param clientNumber
     *            Description of the Parameter
     * @return The directDebitAuthorityListByClient value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getDirectDebitAuthorityListByClient(Integer clientNumber) throws RemoteException, PaymentException;

    /**
     * Return a specific payment type.
     * 
     * @param paymentTypeCode
     *            Description of the Parameter
     * @return The paymentType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public PaymentTypeVO getPaymentType(String paymentTypeCode) throws RemoteException;

    /**
     * Return a list of all of the payment types.
     * 
     * @return The paymentTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getPaymentTypeList() throws RemoteException;

    /**
     * Return a list of payment types that can be used for paying memberships.
     * 
     * @return The paymentTypeListForMembership value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getPaymentTypeListForMembership() throws RemoteException;

    /**
     * Cancel a payable item on a given effective date.
     * 
     * @param payableItemID
     *            the id of the payableItem to cancel
     * @param effectiveDate
     *            the date on which the cancellation is to take effect. Must be
     *            on or after today. If the item is paid by direct debit then
     *            the effective date must be the current date since a payable
     *            item cannot be cancelled if there is a pending amount for it
     *            in the direct debit system.
     * @param userID
     *            the ID of the user cancelling the item
     * @param description
     *            a description associated with the cancel. <code>null</code> if
     *            there is no associated description.
     * @exception RemoteException
     *                Description of the Exception
     * @throws PaymentException
     *             if attempting to cancel a payable item with a pending amount
     *             or if the effective date is not valid
     */
    // public void xxxcancelPayableItem(
    // Integer payableItemID,
    // Date effectiveDate,
    // String userID,
    // String description)
    // throws RemoteException, PaymentException, RollBackException;

    /**
     * Synchronise the earnings of all postings on the payable item.
     * 
     * @param payableItemID
     *            the ID of the item to sync earnings for
     * @param effectiveDate
     *            the date to sync at. Amounts for synchronisation will be
     *            calculated at the end of the effective date (12 midnight)
     * @exception RemoteException
     *                Description of the Exception
     * @throws PaymentException
     *             if the effective date is less than the start date of the
     *             product for the payableItem, or greater than the end date of
     *             the product
     */
    // public void synchroniseEarnings(Integer payableItemID, DateTime
    // effectiveDate)
    // throws RemoteException, PaymentException;

    /**
     * Suspend a payable item on a given effective date.
     * 
     * @param payableItemID
     *            the id of the payableItem to suspend
     * @param effectiveDate
     *            the date on which the suspension is to take effect. Must be on
     *            or after today. If the item is paid by direct debit then the
     *            effective date must be the current date since a payable item
     *            cannot be suspended if there is a pending amount for it in the
     *            direct debit system.
     * @param userID
     *            the ID of the user cancelling the item
     * @param description
     *            a description associated with the cancel. <code>null</code> if
     *            there is no associated description.
     * @exception RemoteException
     *                Description of the Exception
     * @throws PaymentException
     *             if attempting to suspend a payable item with a pending amount
     *             or if the effective date is not valid
     */
    // public void suspendEarnings(Integer payableItemID,
    // Date effectiveDate,
    // String userID,
    // String description)
    // throws RemoteException, PaymentException;

    /**
     * Calculates the unearned and paid amount outstanding against a source
     * system reference as at the current date. This method has a <b>side effect
     * </b> of creating the journals for the earned income up to the current
     * date.
     * 
     * @param sourceSystem
     *            Description of the Parameter
     * @param componentReference
     *            Description of the Parameter
     * @return The unearned amount
     * @exception RemoteException
     *                Description of the Exception
     * @exception PaymentException
     *                Description of the Exception
     */
    // public BigDecimal getUnearnedAndPaidAmount(SourceSystem sourceSystem,
    // String componentReference)
    // throws RemoteException, PaymentException;

    /**
     * Description of the Method
     * 
     * @param clientNumber
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public String getDirectDebitStatus(Integer directDebitAuthorityID) throws RemoteException, PaymentException;

    // public PaymentAdapter clearDirectDebits(MembershipVO memVO) throws
    // PaymentException, RemoteException;

    /**
     * Gets the paidRatio attribute of the PaymentMgr object
     * 
     * @param sourceSystem
     *            Description of the Parameter
     * @param componentReference
     *            Description of the Parameter
     * @return The paidRatio value
     * @exception RemoteException
     *                Description of the Exception
     * @exception PaymentException
     *                Description of the Exception
     */
    // public double getPaidRatio(SourceSystem sourceSystem, String
    // componentReference)
    // throws RemoteException, PaymentException;

    /**
     * Description of the Method
     * 
     * @param bsbNumber
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public String validateBsbNumber(String bsbNumber) throws RemoteException;

    /**
     * Gets the amountOutstandingBySourceSystemComponentReference attribute of
     * the PaymentMgr object
     * 
     * @param ss
     *            Description of the Parameter
     * @param ssComponentReference
     *            Description of the Parameter
     * @return The amountOutstandingBySourceSystemComponentReference value
     * @exception RemoteException
     *                Description of the Exception
     */
    // public BigDecimal
    // getAmountOutstandingBySourceSystemComponentReference(SourceSystem ss,
    // String ssComponentReference)
    // throws RemoteException;

    public String validateCreditCardType(String ccNumber) throws RemoteException;

    /**
     * Gets the directDebitAuthority attribute of the PaymentMgr object
     * 
     * @param ddAuthorityID
     *            Description of the Parameter
     * @return The directDebitAuthority value
     * @exception RemoteException
     *                Description of the Exception
     */
    public DirectDebitAuthority getDirectDebitAuthority(Integer ddAuthorityID) throws RemoteException, PaymentException;

    /**
     * Gets the directDebitSchedule attribute of the PaymentMgr object
     * 
     * @param ddScheduleID
     *            Description of the Parameter
     * @return The directDebitSchedule value
     * @exception RemoteException
     *                Description of the Exception
     */
    public DirectDebitSchedule getDirectDebitSchedule(Integer ddScheduleID) throws RemoteException, PaymentException;

    /**
     * Check whether the payment has been received. Currently only checks
     * receipting. Direct debit needs to be added.
     */
    public Payable getPayable(Integer clientNumber, String paymentMethodID, String paymentSystem, SourceSystem sourceSystem) throws RemoteException, PaymentException;

    /**
     * Get the receipt
     * 
     * @param clientNo
     *            Integer
     * @param seqNo
     *            Integer
     * @param sourceSystem
     *            SourceSystem
     * @throws RemoteException
     * @return Receipt
     */
    public Receipt getReceipt(Integer clientNumber, String paymentMethodId, SourceSystem sourceSystem) throws RemoteException, PaymentException;

    /**
     * Gets the provisionalDirectDebitSchedule attribute of the PaymentMgr
     * object
     * 
     * @param scheduleSpec
     *            Description of the Parameter
     * @return The provisionalDirectDebitSchedule value
     * @exception RemoteException
     *                Description of the Exception
     */
    // public DirectDebitSchedule
    // getProvisionalDirectDebitSchedule(DirectDebitSchedule scheduleSpec)
    // throws RemoteException;

    /**
     * Find a pending fee based on a renewal notice document id
     */
    public PendingFee getPendingFee(Integer renewalNoticeDocID, SourceSystem sourceSystem) throws RemoteException, PaymentException;

    public Collection getPendingFeeByClientNumber(Integer clientNumber) throws RemoteException;

    public Collection<PendingFee> getOutstandingPendingFeeByClientNumber(Integer clientNumber) throws RemoteException;

    public PendingFee getPendingFee(String pendingFeeId) throws RemoteException, PaymentException;

    // public Collection getEarnableComponentsBySourceSystemReference
    // (SourceSystem sourceSystem
    // , String sourceSystemReference
    // )
    // throws RemoteException, PaymentException;

    // public Collection getComponentsBySourceSystemReference
    // (String sourceSystem, String sourceSystemReference)
    // throws RemoteException;

    // public Collection createCancelTypePostings
    // (String referenceNumber, PayableItemComponentVO component, String
    // description)
    // throws RemoteException, RollBackException;

    // public Collection
    // createCancelTypePostingsForDiscretionaryDiscount(MembershipTransactionVO
    // oldMemTxVO
    // ,MembershipTransactionVO memTxVO)
    // throws RemoteException;

    // public Collection
    // createCancelTypePostingsForDiscretionaryDiscount(PayableItemVO
    // payableItem,
    // PayableItemComponentVO oldComponent, MembershipTransactionVO memTxVO)
    // throws RemoteException;

    // public Integer getActiveScheduleBySourceSystemReference(SourceSystem
    // sourceSystem,
    // Integer sourceSystemReference)
    // throws PaymentException, RemoteException;

    // don't get existing posting container - use the membership credit.
    // public PostingContainer getPostings(Integer clientNo, Integer transId,
    // DateTime transDate )
    // throws RemoteException, PaymentException;

}
