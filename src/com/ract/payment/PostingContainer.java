package com.ract.payment;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

import com.ract.common.SystemException;
import com.ract.payment.finance.FinanceException;
import com.ract.payment.finance.Journal;
import com.ract.util.LogUtil;

/**
 * Specialised container for carrying postings.
 * 
 * @author jyh
 * @version 1.0
 */
public class PostingContainer extends Vector {

    public PostingContainer() {
	super();
    }

    /**
     * Construct a posting container using the specified list.
     * 
     * @param postingList
     *            The list to add
     * @throws SystemException
     */
    public PostingContainer(Collection postingList) throws SystemException {
	super();
	addAll(postingList, false);
    }

    /**
     * Add all postings in the specified set to the posting container. If the
     * combine postings flag is true then aggregate by account number.
     * 
     * @param postingList
     *            The set of postings to add
     * @param combinePostings
     *            aggregate by account number
     * @return success or failure
     * @throws SystemException
     */
    public boolean addAll(Collection postingList, boolean combinePostings) throws SystemException {
	if (combinePostings) {
	    if (postingList != null) {
		PayableItemPostingVO pipVO = null;
		LogUtil.debug(this.getClass(), "postingList size=" + postingList.size());
		Iterator postingIt = postingList.iterator();
		while (postingIt.hasNext()) {
		    pipVO = (PayableItemPostingVO) postingIt.next();
		    add(pipVO, combinePostings);
		}
	    }
	    return true;
	} else {
	    return this.addAll(postingList);
	}
    }

    /**
     * Add the specified object to the posting container. If the combine
     * postings flag is set then aggregate the postings based on account number.
     * 
     * @param object
     *            The object to add
     * @param combinePostings
     *            Add total by account number
     * @return success or failure
     * @throws SystemException
     */
    public boolean add(Object object, boolean combinePostings) throws SystemException {
	PayableItemPostingVO sourcePipVO = null;

	if (object == null) {
	    return false;
	} else if (object instanceof PayableItemPostingVO) {
	    sourcePipVO = (PayableItemPostingVO) object;
	} else {
	    throw new SystemException("You may only add objects of type PayableItemPostingVO to this container.");
	}

	if (combinePostings) {
	    PayableItemPostingVO oldPVO = getPosting(sourcePipVO.getAccountNumber());
	    PayableItemPostingVO newPVO = (PayableItemPostingVO) object;

	    if (oldPVO != null) {
		// combine amounts only
		try {
		    oldPVO.setAmount(oldPVO.getAmount() + newPVO.getAmount());
		} catch (PaymentException pe) {
		    throw new SystemException(pe);
		}
		return true;
	    } else {
		return this.add(object);
	    }
	} else {
	    return this.add(object);
	}
    }

    /**
     * Return reference to posting for specified account.
     */
    private PayableItemPostingVO getPosting(String accountNumber) throws SystemException {
	if (this != null && !this.isEmpty()) {
	    Iterator it = this.iterator();
	    String targetAcc = null;
	    PayableItemPostingVO pipVO = null;
	    Object obj = null;
	    int counter = 0;
	    // iterate through existing list.
	    while (it.hasNext()) {
		counter++;
		obj = it.next();
		if (obj instanceof PayableItemPostingVO) {
		    pipVO = (PayableItemPostingVO) obj;
		    // if the account numbers are the same
		    targetAcc = pipVO.getAccountNumber();
		    if (targetAcc.equals(accountNumber)) {
			return pipVO;
		    }
		} else {
		    throw new SystemException("not a payable item posting.");
		}
	    }
	}
	return null;
    }

    /**
     * Remove postings with an amount of zero.
     */
    public void removePostingsWithZeroAmount() {
	// finally remove any postings with a zero amount
	ListIterator postingListIterator = this.listIterator();
	PayableItemPostingVO posting = null;
	while (postingListIterator.hasNext()) {
	    posting = (PayableItemPostingVO) postingListIterator.next();
	    if (posting.getAmount() == 0.0) {
		postingListIterator.remove();
	    }
	}
    }

    /**
     * Do the postings balance to zero.
     */
    public boolean isBalanced() {
	double postingTotal = getPostingTotal();
	LogUtil.log(this.getClass(), "postingTotal-->" + postingTotal);
	// why not zero? This is a GW legacy...
	if (postingTotal <= -0.01 || postingTotal >= 0.01) {
	    return false;
	} else {
	    return true;
	}
    }

    private boolean testing;

    /**
     * Get the total of the suspense amount.
     * 
     * @return the suspense amount total
     */
    public double getSuspenseAmountTotal() {
	double suspenseAmountTotal = 0;
	if (this != null && this.size() > 0) {
	    PayableItemPostingVO posting = null;
	    Iterator postingListIterator = this.iterator();
	    while (postingListIterator.hasNext()) {
		posting = (PayableItemPostingVO) postingListIterator.next();
		LogUtil.log(this.getClass(), "postingCont " + posting.toString());
		if (posting.getAccount().isSuspenseAccount()) {
		    LogUtil.log(this.getClass(), "postingCont is suspense");
		    suspenseAmountTotal += posting.getAmount();
		}
	    }
	}
	LogUtil.log(this.getClass(), "suspenseAmountTotal=" + suspenseAmountTotal);
	return suspenseAmountTotal;
    }

    /**
     * Get the total of the postings in the container
     * 
     * @return the total of the postings
     */
    public double getPostingTotal() {
	double postingTotal = 0;
	if (this != null && this.size() > 0) {
	    PayableItemPostingVO posting = null;
	    Iterator postingListIterator = this.iterator();
	    while (postingListIterator.hasNext()) {
		posting = (PayableItemPostingVO) postingListIterator.next();
		postingTotal += posting.getAmount();
	    }
	}
	return postingTotal;
    }

    /**
     * Summarise a collection of postings.
     * 
     * @return Collection
     */
    public Collection getPostingSummary() {
	String key = null;
	PayableItemPostingVO posting = null;
	Hashtable summaries = new Hashtable();
	PostingSummary postingSummary = null;
	PostingSummary tmpSummary = null;
	BigDecimal subTotal = null;
	for (Iterator i = this.iterator(); i.hasNext();) {
	    posting = (PayableItemPostingVO) i.next();

	    // accumulate by account number, post at and posted dates.
	    postingSummary = new PostingSummary();
	    // set required fields to generate a key
	    postingSummary.setAccountNumber(posting.getAccountNumber());
	    postingSummary.setEffectiveDate(posting.getPostAtDate());
	    postingSummary.setEnteredDate(posting.getPostedDate());
	    postingSummary.setAmount(new BigDecimal(posting.getAmount()));

	    tmpSummary = (PostingSummary) summaries.get(postingSummary.getKey());
	    if (tmpSummary == null) {
		// new one
		summaries.put(postingSummary.getKey(), postingSummary);
	    } else {
		// existing so just add amount to existing key
		subTotal = tmpSummary.getAmount();
		System.out.println("subTotal=" + subTotal);
		tmpSummary.setAmount(subTotal.add(postingSummary.getAmount()));
	    }
	}
	// return values only
	return summaries.values();
    }

    /**
     * Dissect a set of postings by journal key and return as a set of journals.
     * 
     * @throws RemoteException
     * @return Hashtable
     */
    public Hashtable dissectByJournalKey() throws RemoteException {
	Hashtable journals = new Hashtable();
	PayableItemPostingVO posting = null;
	String journalKey = null;
	Journal journal = null;
	String salesBranchCode = null;
	String costCentre = null;
	String subAccountNumber = null;
	for (Iterator i = this.iterator(); i.hasNext();) {
	    posting = (PayableItemPostingVO) i.next();
	    journalKey = posting.getJournalKey() == null ? posting.getJournalKey(null) : posting.getJournalKey();
	    journal = (Journal) journals.get(journalKey);
	    try {
		if (journal == null) {
		    salesBranchCode = posting.getSalesBranchCode() == null ? "" : posting.getSalesBranchCode();
		    costCentre = posting.getCostCentre() == null ? "" : posting.getCostCentre();
		    subAccountNumber = posting.getSubAccountNumber() == null ? "" : posting.getSubAccountNumber();
		    journal = new Journal(posting.getAmount(), posting.getAccountNumber(), costCentre, subAccountNumber, posting.getCompany(), posting.getPostAtDate(), posting.getCreateDate(), salesBranchCode, "", "", journalKey);
		    journal.setPostedDate(posting.getPostedDate());
		    journals.put(journalKey, journal);
		} else {
		    double amount = journal.getAmount() + posting.getAmount();
		    journal.setAmount(amount); // set in hashtable
		}
	    } catch (FinanceException ex) {
		// ignore
		LogUtil.warn(this.getClass(), ex);
	    }
	}
	return journals;
    }

    /**
     * String representation of the posting container
     * 
     * @return
     */
    public String toString() {
	StringBuffer postingDesc = new StringBuffer();
	if (this != null) {
	    Iterator it = this.iterator();
	    PayableItemPostingVO pipVO = null;
	    postingDesc.append("\n");
	    while (it.hasNext()) {
		pipVO = (PayableItemPostingVO) it.next();
		postingDesc.append(pipVO.toString() + "\n");
	    }
	}
	return postingDesc.toString();
    }

}
