package com.ract.payment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.ract.common.GenericException;
import com.ract.payment.finance.Cheque;
import com.ract.payment.finance.FinancePayee;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

@Entity
public class FinanceVoucher implements Serializable {

    public final static String TYPE_EFT_ACCOUNT = "EFT to account";

    public final static String TYPE_CHEQUE_THIRD_PARTY = "Cheque to third party";

    public final static String TYPE_CHEQUE_MEMBER = "Cheque to member";

    public final static String TYPE_WRITE_OFF = "Write off";

    public final static String TYPE_TRANSFER_TO_MEMBER = "Transfer to member";

    public final static String PAY_TYPE_EFT = "EFT";

    public final static String PAY_TYPE_CHEQUE = "Cheque";

    public final static String PAYEE_TYPE_TP = "Third party";

    public final static String PAYEE_TYPE_CLIENT = "Client";

    public final static String STATUS_REJECTED = "R";

    public final static String STATUS_APPROVED = "A";

    public final static String MODE_ADHOC = "Adhoc";

    public final static String MODE_CREDIT = "Credit";

    public final static String DISPOSAL_REASON_DECEASED = "deceased";

    @Transient
    private String salesBranch;

    @Transient
    private String userName;

    public String getSalesBranch() {
	return salesBranch;
    }

    public void setSalesBranch(String salesBranch) {
	this.salesBranch = salesBranch;
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    @Id
    private Integer disposalId;

    private String disposalMode;

    public String getDisposalMode() {
	return disposalMode;
    }

    @Transient
    private boolean financeSystem;

    public boolean isFinanceSystem() {
	if (disposalType.equals(TYPE_CHEQUE_MEMBER) || disposalType.equals(TYPE_CHEQUE_THIRD_PARTY) || disposalType.equals(TYPE_EFT_ACCOUNT)) {
	    this.financeSystem = true;
	} else {
	    this.financeSystem = false;
	}
	return this.financeSystem;
    }

    public void setFinanceSystem(boolean financeSystem) {
	this.financeSystem = financeSystem;
    }

    public void setDisposalMode(String disposalMode) {
	this.disposalMode = disposalMode;
    }

    public String getSourceSystem() {
	return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public String getPayeeType() {
	return payeeType;
    }

    public void setPayeeType(String payeeType) {
	this.payeeType = payeeType;
    }

    public Integer getDisposalId() {
	return disposalId;
    }

    public void setDisposalId(Integer disposalId) {
	this.disposalId = disposalId;
    }

    private BigDecimal disposalAmount;

    public BigDecimal getDisposalAmount() {
	return disposalAmount;
    }

    public BigDecimal getTotalAmount() {
	BigDecimal total = new BigDecimal(0);
	LogUtil.debug(this.getClass(), "total=" + total);
	LogUtil.debug(this.getClass(), "disposalAmount=" + disposalAmount);
	LogUtil.debug(this.getClass(), "feeAmount=" + feeAmount);
	total = total.add(disposalAmount).add(feeAmount);
	LogUtil.debug(this.getClass(), "total=" + total);
	return total;
    }

    public void setDisposalAmount(BigDecimal disposalAmount) {
	this.disposalAmount = disposalAmount;
    }

    public BigDecimal getFeeAmount() {
	return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
	this.feeAmount = feeAmount;
    }

    private BigDecimal feeAmount;

    private String particulars;

    private String payeeName;

    private String payeeEmailAddress;

    public String getPayeeEmailAddress() {
	return payeeEmailAddress;
    }

    public void setPayeeEmailAddress(String payeeEmailAddress) {
	this.payeeEmailAddress = payeeEmailAddress;
    }

    private String address1;

    private String address2;

    private String address3;

    public String getAddress3() {
	return address3;
    }

    public void setAddress3(String address3) {
	this.address3 = address3;
    }

    private String transactionReference;

    private DateTime createDate;

    private String voucherId;

    private String disposalType;

    private String approvingUserId;

    private String status;

    private String userId;

    @Transient
    private BigDecimal paidAmount;

    @Transient
    private String paymentReference;

    public BigDecimal getPaidAmount() {
	return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
	this.paidAmount = paidAmount;
    }

    @Transient
    public String getPaymentReference() {
	return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
	this.paymentReference = paymentReference;
    }

    public DateTime getPaymentDate() {
	return paymentDate;
    }

    public void setPaymentDate(DateTime paymentDate) {
	this.paymentDate = paymentDate;
    }

    @Transient
    private DateTime paymentDate;

    public String getTransactionReference() {
	return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
	this.transactionReference = transactionReference;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String getVoucherId() {
	return voucherId;
    }

    public void setVoucherId(String voucherId) {
	this.voucherId = voucherId;
    }

    public String getDisposalType() {
	return disposalType;
    }

    public void setDisposalType(String disposalType) {
	this.disposalType = disposalType;
    }

    public String getApprovingUserId() {
	return approvingUserId;
    }

    public void setApprovingUserId(String approvingUserId) {
	this.approvingUserId = approvingUserId;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public DateTime getProcessDate() {
	return processDate;
    }

    public void setProcessDate(DateTime processDate) {
	this.processDate = processDate;
    }

    public String getPayeeNo() {
	return payeeNo;
    }

    public void setPayeeNo(String payeeNo) {
	this.payeeNo = payeeNo;
    }

    public String getSourceSystemReference() {
	return sourceSystemReference;
    }

    private String sourceSystem;

    public void setSourceSystemReference(String sourceSystemReference) {
	this.sourceSystemReference = sourceSystemReference;
    }

    private DateTime processDate;

    private String payeeType;

    private String payeeNo;

    private String state;

    private String postcode;

    private String bsb;

    private String bankAccountNumber;

    private String writeOffAccountNumber;

    private String disposalReason;

    private String sourceSystemReference;

    private String costCentre;

    public String getCostCentre() {
	return costCentre;
    }

    public void setCostCentre(String costCentre) {
	this.costCentre = costCentre;
    }

    private String transactionTypeCode;

    private String tfrMembershipNumber = null;

    private String clientNumber = null;

    private String bankCode;

    public String getBankCode() {
	return bankCode;
    }

    public void setBankCode(String bankCode) {
	this.bankCode = bankCode;
    }

    /**
     * Refund may be to the member or to a third party. If payment is to the
     * member, name and address fields will be ignored
     */
    private boolean thirdParty;

    public String getTransactionTypeCode() {
	return transactionTypeCode;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
	this.transactionTypeCode = transactionTypeCode;
    }

    /**
     * Refund may be by cheque or EFT If payment is by cheque, the bank details
     * will be ignored
     */
    private boolean eft;

    /**
     * Refund may actually be a write off. If true all other fields will be
     * ignored except the membershipId and amountToDisposeOf
     */
    private boolean writeOff;

    private String payType;

    public String getPayType() {
	return payType;
    }

    public void setPayType(String payType) {
	this.payType = payType;
    }

    public FinanceVoucher() {
    }

    public String getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
	this.clientNumber = clientNumber;
    }

    public String getAddress1() {
	return address1;
    }

    public String getAddress2() {
	return address2;
    }

    public String getBsb() {
	return bsb;
    }

    public String getPayeeName() {
	return payeeName;
    }

    public boolean isEft() {
	return eft;
    }

    public String getParticulars() {
	return particulars;
    }

    public String getPostcode() {
	return postcode;
    }

    public String getDisposalReason() {
	return disposalReason;
    }

    public String getState() {
	return state;
    }

    public boolean isThirdParty() {
	return thirdParty;
    }

    private boolean isApproved() {
	if (STATUS_APPROVED.equals(status)) {
	    return true;
	} else {
	    return false;
	}
    }

    public void setAddress1(String address1) {
	this.address1 = address1;
    }

    public void setAddress2(String address2) {
	this.address2 = address2;
    }

    public void setBsb(String bsb) {
	this.bsb = bsb;
    }

    public void setPayeeName(String chequeName) {
	this.payeeName = chequeName;
    }

    public void setEft(boolean eft) {
	this.eft = eft;
    }

    public void setParticulars(String particulars) {
	this.particulars = particulars;
    }

    public void setPostcode(String postcode) {
	this.postcode = postcode;
    }

    public void setDisposalReason(String disposalReason) {
	this.disposalReason = disposalReason;
    }

    public void setState(String state) {
	this.state = state;
    }

    public void setThirdParty(boolean thirdParty) {
	this.thirdParty = thirdParty;
    }

    public void setWriteOff(boolean writeOff) {
	this.writeOff = writeOff;
    }

    public boolean getWriteOff() {
	return writeOff;
    }

    @Override
    public String toString() {
	return "FinanceVoucher [address1=" + address1 + ", address2=" + address2 + ", approvingUserId=" + approvingUserId + ", bankAccountNumber=" + bankAccountNumber + ", bsb=" + bsb + ", payeeName=" + payeeName + ", clientNumber=" + clientNumber + ", costCentre=" + costCentre + ", createDate=" + createDate + ", disposalAmount=" + disposalAmount + ", disposalId=" + disposalId + ", disposalMode=" + disposalMode + ", disposalReason=" + disposalReason + ", disposalType=" + disposalType + ", eft=" + eft + ", feeAmount=" + feeAmount + ", financeSystem=" + financeSystem + ", particulars=" + particulars + ", payType=" + payType + ", payeeNo=" + payeeNo + ", payeeType=" + payeeType + ", postcode=" + postcode + ", processDate=" + processDate + ", sourceSystem=" + sourceSystem + ", sourceSystemReference=" + sourceSystemReference + ", state=" + state + ", status=" + status + ", tfrMembershipNumber=" + tfrMembershipNumber + ", thirdParty=" + thirdParty + ", transactionReference=" + transactionReference
		+ ", transactionTypeCode=" + transactionTypeCode + ", userId=" + userId + ", voucherId=" + voucherId + ", writeOff=" + writeOff + ", writeOffAccountNumber=" + writeOffAccountNumber + "]";
    }

    public void setTfrMembershipNumber(String tfrMembershipNumber) {
	this.tfrMembershipNumber = tfrMembershipNumber;
    }

    public String getTfrMembershipNumber() {
	return tfrMembershipNumber;
    }

    // public Voucher getVoucher()
    // {
    // Voucher voucher = new Voucher();
    // voucher.setVoucherId(this.voucherId);
    // voucher.setAmount(this.getTotalAmount().doubleValue());
    // voucher.setClientNumber(this.clientNumber.toString());
    // voucher.setCreateDate(this.createDate);
    // voucher.setDescription("Membership");
    // voucher.setParticulars(this.particulars);
    // voucher.setPayeeNo(this.payeeNo);
    // voucher.setReasonCode(this.disposalReason);
    // voucher.setSourceSystemReference(new
    // Integer(this.sourceSystemReference));
    // voucher.setSubSystem(this.sourceSystem);
    // voucher.setTransactionReference(this.transactionReference);
    // voucher.setUserId(this.userId);
    // voucher.setPayeeType(this.payeeType);
    // voucher.setPayType(this.payType);
    // return voucher;
    // }

    public FinancePayee getPayee() throws GenericException {
	FinancePayee payee = null;
	try {
	    payee = new FinancePayee();
	    payee.setPayeeNo(this.payeeNo);

	    payee.setPayeeName(this.payeeName);
	    payee.setAddress1(this.address1);
	    payee.setSuburb(this.getAddress2());
	    payee.setState(this.state);
	    payee.setPostcode(this.postcode);

	    // Finance 1
	    payee.setBankType("F1");

	    if (this.isEft()) {
		payee.setBsb(this.bsb);
		payee.setAccountNo(this.bankAccountNumber);
	    }
	} catch (RemoteException e) {
	    e.printStackTrace();
	    throw new GenericException(e.getMessage());
	}

	return payee;
    }

    public String getBankAccountNumber() {
	return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
	this.bankAccountNumber = bankAccountNumber;
    }

    public String getWriteOffAccountNumber() {
	return writeOffAccountNumber;
    }

    public void setWriteOffAccountNumber(String writeOffAccountNumber) {
	this.writeOffAccountNumber = writeOffAccountNumber;
    }

    public Cheque getCheque() {
	Cheque cheque = new Cheque(this.voucherId);
	cheque.setPayeeNo(this.payeeNo);
	cheque.setChqPrt("D");// ?
	cheque.setChqRet("B");// ?
	cheque.setDescr("Membership");// ?
	cheque.setEntity("10");// ?
	cheque.setGrossAmt(this.getTotalAmount());
	cheque.setInvoice("Clt" + this.getClientNumber());// ?
	cheque.setInvoiceDate(this.createDate);
	cheque.setRemark(this.particulars);
	cheque.setSeparateCheque(true);// ?
	cheque.setSubSystem(this.sourceSystem);
	return cheque;
    }

}
