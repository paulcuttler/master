package com.ract.payment;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.CacheBase;

public class PaymentTypeCache extends CacheBase {

    /**
     * Holds a reference to an instance of this class. The class instance is
     * created when the Class is first referenced.
     */
    private static PaymentTypeCache instance = new PaymentTypeCache();

    /**
     * Implement the initialiseList method of the super class. Initialise the
     * list of cached items.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void initialiseList() throws RemoteException {
	Collection dataList = PaymentEJBHelper.getPaymentMgr().getPaymentTypes();
	this.addAll(dataList);
    }

    /**
     * Return a reference the instance of this class.
     * 
     * @return The instance value
     */
    public static PaymentTypeCache getInstance() {
	return instance;
    }

    /**
     * Return the specified payment type value object.
     * 
     * @param paymentTypeCode
     *            Description of the Parameter
     * @return The paymentType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public PaymentTypeVO getPaymentType(String paymentTypeCode) throws RemoteException {
	return (PaymentTypeVO) getItem(paymentTypeCode);
    }

    /**
     * Return the list of payment types.
     * 
     * @return The paymentTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getPaymentTypeList() throws RemoteException {
	return getItemList();
    }

}
