package com.ract.payment;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface PaymentIDMgr extends EJBObject {
    // public int getLastPayableItemID() throws RemoteException;
    public int getNextPayableItemID() throws RemoteException;

    // public int getLastPayableItemActionID() throws RemoteException;
    public int getNextPayableItemActionID() throws RemoteException;

    // public int getLastPayableItemComponentID() throws RemoteException;
    public int getNextPayableItemComponentID() throws RemoteException;

    public int getNextPayableItemPostingID() throws RemoteException;

    public int getNextJournalNumber() throws RemoteException;

    public void reset() throws RemoteException;

}