package com.ract.payment.directdebit;

import com.ract.util.DateTime;

public class DDMemo {
    Integer ddItemNo;
    Integer ddPayableSeq;
    Integer memoSeq;
    String memoText;
    String createId;
    DateTime createDate;

    public DateTime getCreateDate() {
	return createDate;
    }

    public Integer getDdItemNo() {
	return ddItemNo;
    }

    public String getCreateId() {
	return createId;
    }

    public Integer getDdPayableSeq() {
	return ddPayableSeq;
    }

    public Integer getMemoSeq() {
	return memoSeq;
    }

    public String getMemoText() {
	return memoText;
    }

    public void setMemoText(String memoText) {
	if (memoText == null)
	    this.memoText = "";
	else
	    this.memoText = memoText;

    }

    public void setMemoSeq(Integer memoSeq) {
	this.memoSeq = memoSeq;
    }

    public void setDdPayableSeq(Integer ddPayableSeq) {
	this.ddPayableSeq = ddPayableSeq;
    }

    public void setDdItemNo(Integer ddItemNo) {
	this.ddItemNo = ddItemNo;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String toJSONArray() {
	fixMemoText();
	return "[{\"title\":\"DD Item no\",\"value\":\"" + this.ddItemNo + "\"}," + "{\"title\":\"DD Payable Seq\",\"value\":\"" + this.ddPayableSeq + "\"}," + "{\"title\":\"Memo Seq\",\"value\":\"" + this.memoSeq + "\"}," + "{\"title\":\"Create Id\",\"value\":\"" + this.createId + "\"}," + "{\"title\":\"Create Date\",\"value\":\"" + this.createDate.formatShortDate() + "\"}," + "{\"title\":\"Memo Text\",\"value\":\"" + this.memoText.trim() + "\"}]";

    }

    // replace any carriage return or line feed characters with <br>
    private void fixMemoText() {
	char ch;
	StringBuffer sb = new StringBuffer();
	for (int xx = 0; xx < this.memoText.length(); xx++) {
	    ch = memoText.charAt(xx);
	    if (ch == 10 || ch == 13)
		sb.append("<br>");
	    else if (ch >= 32 && ch <= 127)
		sb.append(ch);
	}
	this.memoText = sb.toString();
    }
}
