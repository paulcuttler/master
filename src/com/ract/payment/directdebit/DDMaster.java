package com.ract.payment.directdebit;

import java.util.ArrayList;

public class DDMaster {

    private String bankAccount;
    private String bsbNumber;
    private String ccExpiryDate;
    private com.ract.util.DateTime changeDate;
    private String changeId;
    private String changeSlsBch;
    private boolean creditCard;
    private Integer ddSeqNo;
    private String drawer;
    private com.ract.util.DateTime madeDate;
    private String madeId;
    private String madeSlsBch;
    private ArrayList oldDrawers;

    public DDMaster() {
    }

    public String getBankAccount() {
	return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
	this.bankAccount = bankAccount;
    }

    public String getBsbNumber() {
	return bsbNumber;
    }

    public void setBsbNumber(String bsbNumber) {
	this.bsbNumber = bsbNumber;
    }

    public String getCcExpiryDate() {
	return ccExpiryDate;
    }

    public void setCcExpiryDate(String ccExpiryDate) {
	this.ccExpiryDate = ccExpiryDate;
    }

    public com.ract.util.DateTime getChangeDate() {
	return changeDate;
    }

    public void setChangeDate(com.ract.util.DateTime changeDate) {
	this.changeDate = changeDate;
    }

    public String getChangeId() {
	return changeId;
    }

    public void setChangeId(String changeId) {
	this.changeId = changeId;
    }

    public String getChangeSlsBch() {
	return changeSlsBch;
    }

    public void setChangeSlsBch(String changeSlsBch) {
	this.changeSlsBch = changeSlsBch;
    }

    public boolean isCreditCard() {
	return creditCard;
    }

    public void setCreditCard(boolean creditCard) {
	this.creditCard = creditCard;
    }

    public Integer getDdSeqNo() {
	return ddSeqNo;
    }

    public void setDdSeqNo(Integer ddSeqNo) {
	this.ddSeqNo = ddSeqNo;
    }

    public String getDrawer() {
	return drawer;
    }

    public void setDrawer(String drawer) {
	this.drawer = drawer;
    }

    public com.ract.util.DateTime getMadeDate() {
	return madeDate;
    }

    public void setMadeDate(com.ract.util.DateTime madeDate) {
	this.madeDate = madeDate;
    }

    public String getMadeId() {
	return madeId;
    }

    public void setMadeId(String madeId) {
	this.madeId = madeId;
    }

    public String getMadeSlsBch() {
	return madeSlsBch;
    }

    public void setMadeSlsBch(String madeSlsBch) {
	this.madeSlsBch = madeSlsBch;
    }

    public ArrayList getOldDrawers() {
	return oldDrawers;
    }

    public void setOldDrawers(ArrayList oldDrawers) {
	this.oldDrawers = oldDrawers;
    }

    public String formatForDisplay() {
	return "  DD Seq No =           " + ddSeqNo + "\n  Bank Account =        " + bankAccount + "\n  BSB Number =          " + bsbNumber + "\n  Credit Card =         " + creditCard + "\n  Expiry Date =         " + ccExpiryDate + "\n  Change Date =         " + changeDate + "\n  Change Id =           " + changeId + "\n  Change Sales Branch = " + changeSlsBch + "\n  Drawer =              " + drawer + "\n  Made Date =           " + madeDate + "\n  Made Id =             " + madeId + "\n  MadeSales Branch =    " + madeSlsBch + "\n  History =             " + oldDrawers;
    }

    public String toJSONArray() {
	String rString = "[{\"title\":\"DD Seq No\",\"value\":\"" + ddSeqNo + "\"}," + "{\"title\":\"Bank Account\",\"value\":\"" + bankAccount + "\"}," + "{\"title\":\"BSB Number\",\"value\":\"" + bsbNumber + "\"}," + "{\"title\":\"Credit Card\",\"value\":\"" + creditCard + "\"}," + "{\"title\":\"Expiry Date\",\"value\":\"" + ccExpiryDate + "\"}," + "{\"title\":\"Change Date\",\"value\":\"" + changeDate + "\"}," + "{\"title\":\"Change Id\",\"value\":\"" + changeId + "\"}," + "{\"title\":\"Change Sales Branch\",\"value\":\"" + changeSlsBch + "\"}," + "{\"title\":\"Drawer\",\"value\":\"" + drawer + "\"}," + "{\"title\":\"Made Date\",\"value\":\"" + madeDate + "\"}," + "{\"title\":\"Made Id\",\"value\":\"" + madeId + "\"}," + "{\"title\":\"MadeSales Branch\",\"value\":\"" + madeSlsBch + "\"}";

	for (int xx = 0; xx < this.oldDrawers.size(); xx++) {
	    String od = (String) oldDrawers.get(xx);
	    if (od != null && !od.trim().equals("")) {
		if (xx == 0)
		    rString += ",{\"title\":\"History\",\"value\":\"" + od + "\"}";
		else
		    rString += ",{\"title\":\"-\",\"value\":\"" + od + "\"}";
	    }
	}
	rString += "]";
	return rString;
    }
}
