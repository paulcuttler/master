/*
 * @(#)DirectDebitScheduleItem.java
 *
 */
package com.ract.payment.directdebit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;

import com.ract.common.ClassWriter;
import com.ract.common.SourceSystem;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * An item in a direct debit schedule (i.e scheduled date, status, etc of a
 * particular payment)
 * 
 * @author Terry Bakker
 * @created 8 January 2003
 * @version 1.0, 10/5/2002
 */

public class DirectDebitScheduleItem implements Serializable, Comparable, Writable {

    /**
     * The date when the money is scheduled to be withdrawn
     */
    private DateTime scheduledDate;

    /**
     * The status of the item.
     */
    private DirectDebitItemStatus ddItemStatus;

    /**
     * The amount
     */
    private BigDecimal scheduledAmount;

    /**
     * The date the item was processed. This date is <code>null</code> if the
     * item has not been processed
     */
    private DateTime processedDate;

    private BigDecimal feeAmount;

    private Integer receiptNumber;

    private Integer scheduledItemId;

    private Integer scheduleId;

    private String description;

    private String productCode;

    private SourceSystem subSystem;

    private DateTime createDate;

    private String createId;

    private Integer transactionNumber;

    private String action;

    private Integer replaceNumber;

    private String returnCode;

    private String authorisationNumber;

    private DateTime returnDate;

    /**
     * Constructor.
     * 
     * @param status
     *            the status of the item
     * @param schedDate
     *            Description of the Parameter
     * @param schedAmount
     *            Description of the Parameter
     * @param procDate
     *            Description of the Parameter
     * @param feeAmt
     *            Description of the Parameter
     * @param receiptNum
     *            Description of the Parameter
     */
    public DirectDebitScheduleItem(DateTime createDate, DateTime schedDate, BigDecimal schedAmount, DirectDebitItemStatus status, DateTime procDate, BigDecimal feeAmt, Integer receiptNum, Integer scheduleItemId, Integer scheduleId, String description, String productCode, String createId, Integer transactionNumber, String action, Integer replaceNumber, String returnCode, String authorisationNumber, DateTime returnDate) {
	setCreateDate(createDate);
	setScheduledDate(schedDate);
	setAmount(schedAmount);
	setStatus(status);
	setProcessedDate(procDate);
	setFeeAmount(feeAmt);
	setReceiptNumber(receiptNum);
	setScheduledItemId(scheduleItemId);
	setScheduleId(scheduleId);
	setDescription(description + " (" + schedDate + ")");
	setProductCode(productCode);
	setCreateId(createId);
	setTransactionNumber(transactionNumber);
	setAction(action);
	setReplaceNumber(replaceNumber);
	setReturnCode(returnCode);
	setAuthorisationNumber(authorisationNumber);
	setReturnDate(returnDate);
    }

    public String toString() {
	StringBuffer itemDesc = new StringBuffer();
	itemDesc.append("\nscheduleId=" + this.scheduleId);
	itemDesc.append("\nscheduleItemId=" + this.scheduledItemId);
	itemDesc.append("\nSched Date=" + this.scheduledDate);
	itemDesc.append("\nAmount=" + this.scheduledAmount);
	itemDesc.append("\nStatus=" + this.ddItemStatus.getDescription());
	itemDesc.append("\nfeeAmount=" + this.feeAmount);
	itemDesc.append("\nprocess Date=" + this.processedDate);
	return itemDesc.toString();
    }

    /**
     * Get the date when the money is scheduled to be withdrawn
     * 
     * @return the date when the money is scheduled to be withdrawn
     */
    public DateTime getScheduledDate() {
	return this.scheduledDate;
    }

    /**
     * Get the status of the item (scheduled, renewal, cancelled, etc)
     * 
     * @return the status of the item
     */
    public DirectDebitItemStatus getStatus() {

	return this.ddItemStatus;
    }

    /**
     * Get the amount of the item
     * 
     * @return the amount of the item
     */
    public BigDecimal getAmount() {
	return this.scheduledAmount;
    }

    /**
     * Get the processed date.
     * 
     * @return the processed date, or <code>null</code> if the item has not been
     *         processed.
     */
    public DateTime getProcessedDate() {
	return this.processedDate;
    }

    /**
     * <p>
     * Should this item be included on a report for the client. Valid item
     * statuses include:
     * </p>
     * <ul>
     * <li>Scheduled
     * <li>Dishonoured
     * <li>Cancelled/Dishonoured
     * <li>Renewal
     * </ul>
     * 
     * @return
     */
    public boolean isDisplayable() {
	if (this.getStatus().equals(DirectDebitItemStatus.SCHEDULED)) {
	    return true;
	}
	// else if(this.getStatus().equals(DirectDebitItemStatus.PAID))
	// {
	// return true;
	// }
	else if (this.getStatus().equals(DirectDebitItemStatus.DISHONOURED)) {
	    return true;
	} else if (this.getStatus().equals(DirectDebitItemStatus.CANCELLED_DISHONOURED)) {
	    return true;
	} else if (this.getStatus().equals(DirectDebitItemStatus.RENEWAL)) {
	    return true;
	}
	return false;
    }

    public String getPrintableStatus() {
	LogUtil.log(this.getClass(), "Status=" + this.getStatus().getDescription());
	return (!this.getStatus().getDescription().equals(DirectDebitItemStatus.SCHEDULED.getDescription()) && !this.getStatus().getDescription().equals(DirectDebitItemStatus.RENEWAL.getDescription())) ? this.getStatus().getDescription() : "";
    }

    public boolean isDishonoured() {
	return (this.getStatus().getDescription().indexOf(DirectDebitItemStatus.DISHONOURED.getDescription()) != -1);
    }

    /**
     * Is this schedule yet to be paid.
     * 
     * @return boolean
     */
    public boolean isPayable() {
	if (this.getStatus().equals(DirectDebitItemStatus.SCHEDULED) || this.isDishonoured()) {
	    return true;
	} else {
	    return false;
	}
    }

    public boolean isScheduled() {
	if (this.getStatus().equals(DirectDebitItemStatus.SCHEDULED)) {
	    return true;
	} else {
	    return false;
	}
    }

    public boolean isRenewal() {
	if (this.getStatus().equals(DirectDebitItemStatus.RENEWAL)) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Has this item had a letter sent?
     * 
     * @return boolean
     */
    public boolean isLetter() {
	if (this.getLetterType() > 0) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Find out the letter number from the 'action', If action = "1" then return
     * 1 if action = "2" then return 2 otherwise return 0
     * 
     * @return int
     */
    public int getLetterType() {
	if (this.action.equalsIgnoreCase("1")) {
	    return 1;
	} else if (this.action.equalsIgnoreCase("2")) {
	    return 2;
	} else {
	    return 0;
	}
    }

    /**
     * Return a copy of this direct debit schedule item
     * 
     * @return Description of the Return Value
     */
    public DirectDebitScheduleItem copy() {
	return new DirectDebitScheduleItem(this.createDate, this.scheduledDate, this.scheduledAmount, this.ddItemStatus, this.processedDate, this.feeAmount, this.receiptNumber, this.scheduledItemId, this.scheduleId, this.description, this.productCode, this.createId, this.transactionNumber, this.action, this.replaceNumber, this.returnCode, this.authorisationNumber, this.returnDate);
    }

    /**
     * Compare the direct debit schedules and order by create date.
     * 
     * @param obj
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int compareTo(Object obj) {
	if (obj != null && obj instanceof DirectDebitScheduleItem) {
	    DirectDebitScheduleItem ddItem = (DirectDebitScheduleItem) obj;
	    if (this.scheduledDate != null && ddItem.getScheduledDate() != null) {
		return this.scheduledDate.compareTo(ddItem.getScheduledDate());
	    }
	}

	return 0;
    }

    /**
     * Set the date when the money is scheduled to be withdrawn
     * 
     * @param scheduledDate
     *            the date when the money is scheduled to be withdrawn
     */
    private void setScheduledDate(DateTime scheduledDate) {
	this.scheduledDate = scheduledDate;
    }

    /**
     * Set the status of the item (scheduled, renewal, cancelled, etc)
     * 
     * @param status
     *            the status of the scheduled item
     */
    private void setStatus(DirectDebitItemStatus status) {
	this.ddItemStatus = status;
    }

    /**
     * Set the amount of the item
     * 
     * @param amount
     *            the amount of this item. Must be greater than 0.
     */
    private void setAmount(BigDecimal amount) {
	this.scheduledAmount = amount;
    }

    /**
     * Set the processed date.
     * 
     * @param processedDate
     *            processed date.
     */
    private void setProcessedDate(DateTime processedDate) {
	this.processedDate = processedDate;
    }

    /**
     * Gets the feeAmount attribute of the DirectDebitScheduleItem object
     * 
     * @return The feeAmount value
     */
    public BigDecimal getFeeAmount() {
	return this.feeAmount;
    }

    /**
     * Sets the feeAmount attribute of the DirectDebitScheduleItem object
     * 
     * @param feeAmount
     *            The new feeAmount value
     */
    private void setFeeAmount(BigDecimal feeAmount) {
	this.feeAmount = feeAmount;
    }

    /**
     * Gets the receiptNumber attribute of the DirectDebitScheduleItem object
     * 
     * @return The receiptNumber value
     */
    public Integer getReceiptNumber() {
	return this.receiptNumber;
    }

    /**
     * Sets the receiptNumber attribute of the DirectDebitScheduleItem object
     * 
     * @param receiptNumber
     *            The new receiptNumber value
     */
    private void setReceiptNumber(Integer receiptNumber) {
	this.receiptNumber = receiptNumber;
    }

    public Integer getScheduleItemId() {
	return this.scheduledItemId;
    }

    public Integer getScheduleId() {
	return this.scheduleId;
    }

    private void setScheduledItemId(Integer scheduledItemId) {
	this.scheduledItemId = scheduledItemId;
    }

    private void setScheduleId(Integer scheduleId) {
	this.scheduleId = scheduleId;
    }

    private void setDescription(String description) {
	this.description = description;
    }

    public String getDescription() {
	return description;
    }

    public String getProductCode() {
	return productCode;
    }

    private void setProductCode(String productCode) {
	this.productCode = productCode;
    }

    public void setSubSystem(SourceSystem subSystem) {
	this.subSystem = subSystem;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public void setReturnDate(DateTime returnDate) {
	this.returnDate = returnDate;
    }

    public void setAuthorisationNumber(String authorisationNumber) {
	this.authorisationNumber = authorisationNumber;
    }

    public void setReturnCode(String returnCode) {
	this.returnCode = returnCode;
    }

    public void setReplaceNumber(Integer replaceNumber) {
	this.replaceNumber = replaceNumber;
    }

    public void setAction(String action) {
	this.action = action;
    }

    public void setTransactionNumber(Integer transactionNumber) {
	this.transactionNumber = transactionNumber;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public SourceSystem getSubSystem() {
	return this.subSystem;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public DateTime getReturnDate() {
	return returnDate;
    }

    public String getAuthorisationNumber() {
	return authorisationNumber;
    }

    public String getReturnCode() {
	return returnCode;
    }

    public Integer getReplaceNumber() {
	return replaceNumber;
    }

    public String getAction() {
	return action;
    }

    public String getActionDescription() {
	if (this.action.equalsIgnoreCase("1")) {
	    return "1st Letter";
	}
	if (this.action.equalsIgnoreCase("2")) {
	    return "2nd Letter";
	}

	return this.action;
    }

    public Integer getTransactionNumber() {
	return transactionNumber;
    }

    public String getCreateId() {
	return createId;
    }

    public void setSourceSystem(SourceSystem subSystem) {
	this.subSystem = subSystem;
    }

    public SourceSystem getSourceSystem() {
	return this.subSystem;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("action", action);
	cw.writeAttribute("authorisationNumber", authorisationNumber);
	cw.writeAttribute("createDate", createDate);
	cw.writeAttribute("createId", createId);
	cw.writeAttribute("ddItemStatus", ddItemStatus);
	cw.writeAttribute("description", description);
	cw.writeAttribute("feeAmount", feeAmount);
	cw.writeAttribute("processedDate", processedDate);
	cw.writeAttribute("productCode", productCode);
	cw.writeAttribute("receiptNumber", receiptNumber);
	cw.writeAttribute("replaceNumber", replaceNumber);
	cw.writeAttribute("returnCode", returnCode);
	cw.writeAttribute("returnDate", returnDate);
	cw.writeAttribute("scheduledAmount", scheduledAmount);
	cw.writeAttribute("scheduledDate", scheduledDate);
	cw.writeAttribute("scheduledItemId", scheduledItemId);
	cw.writeAttribute("scheduleId", scheduleId);
	cw.writeAttribute("subSystem", subSystem);
	cw.writeAttribute("transactionNumber", transactionNumber);
	cw.endClass();

    }

    public static final String WRITABLE_CLASSNAME = "DirectDebitScheduleItem";

    public String getWritableClassName() {
	return WRITABLE_CLASSNAME;
    }

}
