package com.ract.payment.directdebit;

import com.ract.common.SystemException;
import com.ract.payment.PaymentFactory;

public class DDItem {
    private Integer clientNo;
    private boolean currentVer;
    private Integer ddDay;
    private Integer ddItemNo;
    private Integer ddItemSeq;
    private Integer ddSeqNo;
    private String ddType;
    private String frequency;
    private com.ract.util.DateTime madeDate;
    private String madeId;
    private String madeSlsBch;
    private boolean owner;
    private boolean paFlag;
    private com.ract.util.DateTime pdcDate;
    private Integer systemNo;
    private boolean hasPbles;

    public DDItem() {
    }

    public Integer getClientNo() {
	return clientNo;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    public boolean isCurrentVer() {
	return currentVer;
    }

    public void setCurrentVer(boolean currentVer) {
	this.currentVer = currentVer;
    }

    public Integer getDdDay() {
	return ddDay;
    }

    public void setDdDay(Integer ddDay) {
	this.ddDay = ddDay;
    }

    public Integer getDdItemNo() {
	return ddItemNo;
    }

    public void setDdItemNo(Integer ddItemNo) {
	this.ddItemNo = ddItemNo;
    }

    public Integer getDdItemSeq() {
	return ddItemSeq;
    }

    public void setDdItemSeq(Integer ddItemSeq) {
	this.ddItemSeq = ddItemSeq;
    }

    public Integer getDdSeqNo() {
	return ddSeqNo;
    }

    public void setDdSeqNo(Integer ddSeqNo) {
	this.ddSeqNo = ddSeqNo;
    }

    public String getDdType() {
	return ddType;
    }

    public void setDdType(String ddType) {
	this.ddType = ddType;
    }

    public String getFrequency() {
	return frequency;
    }

    public void setFrequency(String frequency) {
	this.frequency = frequency;
    }

    public com.ract.util.DateTime getMadeDate() {
	return madeDate;
    }

    public void setMadeDate(com.ract.util.DateTime madeDate) {
	this.madeDate = madeDate;
    }

    public String getMadeId() {
	return madeId;
    }

    public void setMadeId(String madeId) {
	this.madeId = madeId;
    }

    public String getMadeSlsBch() {
	return madeSlsBch;
    }

    public void setMadeSlsBch(String madeSlsBch) {
	this.madeSlsBch = madeSlsBch;
    }

    public boolean isOwner() {
	return owner;
    }

    public void setOwner(boolean owner) {
	this.owner = owner;
    }

    public boolean isPaFlag() {
	return paFlag;
    }

    public void setPaFlag(boolean paFlag) {
	this.paFlag = paFlag;
    }

    public com.ract.util.DateTime getPdcDate() {
	return pdcDate;
    }

    public void setPdcDate(com.ract.util.DateTime pdcDate) {
	this.pdcDate = pdcDate;
    }

    public Integer getSystemNo() {
	return systemNo;
    }

    public void setSystemNo(Integer systemNo) {
	this.systemNo = systemNo;
    }

    public void setHasPayables(boolean hasPbles) {
	this.hasPbles = hasPbles;
    }

    public boolean hasPayables() {
	return this.hasPbles;
    }

    public String formatForDisplay() {
	return " Current Version =     " + currentVer + "\n DD Seq No =           " + ddSeqNo + "\n Client No =           " + clientNo + "\n Debit Day =           " + ddDay + "\n DD Type =             " + ddType + "\n System No =           " + systemNo + "\n Frequency =           " + frequency + "\n Made Date =           " + madeDate + "\n Made Id =             " + madeId + "\n Made Sales Branch =   " + madeSlsBch + "\n Owner =               " + owner + "\n Processing approved = " + paFlag + "\n PDC Date =            " + pdcDate;

    }

    public String formatAsHTML() {
	return " Current Version =     " + currentVer + "<br> DD Seq No =           " + ddSeqNo + "<br> Client No =           " + clientNo + "<br> Debit Day =           " + ddDay + "<br> DD Type =             " + ddType + "<br> System No =           " + systemNo + "<br> Frequency =           " + frequency + "<br> Made Date =           " + madeDate + "<br> Made Id =             " + madeId + "<br> Made Sales Branch =   " + madeSlsBch + "<br> Owner =               " + owner + "<br> Processing approved = " + paFlag + "<br> PDC Date =            " + pdcDate;

    }

    public String toJSONArray() {
	String os = "[{\"title\":\"Current Version\",\"value\":\"" + currentVer + "\"}," + "{\"title\":\"DD Seq No\",\"value\":\"" + ddSeqNo + "\"}," + " {\"title\":\"Client No\",\"value\":\"" + clientNo + "\"}," + " {\"title\":\"Debit Day\",\"value\":\"" + ddDay + "\"}," + " {\"title\":\"DD Type\",\"value\":\"" + ddType + "\"}," + " {\"title\":\"System No\",\"value\":\"" + systemNo + "\"}," + " {\"title\":\"Frequency\",\"value\":\"" + frequency + "\"}," + " {\"title\":\"Made Date\",\"value\":\"" + madeDate + "\"}," + " {\"title\":\"Made Id\",\"value\":\"" + madeId + "\"}," + " {\"title\":\"Made Sales Branch\",\"value\":\"" + madeSlsBch + "\"}," + " {\"title\":\"Owner\",\"value\":\"" + owner + "\"}," + " {\"title\":\"Processing approved\",\"value\":\"" + paFlag + "\"}," + " {\"title\":\"PDC Date\",\"value\":\"" + pdcDate + "\"}]";
	return os;
    }

    public String getJSONDetail() throws SystemException {
	DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
	DDMaster ddm = ddAdapter.getDDMaster(this.ddSeqNo);
	String ddTypeDesc = null;
	if (this.ddType.equalsIgnoreCase("ins"))
	    ddTypeDesc = "Policy No";
	else if (this.ddType.equalsIgnoreCase("mem"))
	    ddTypeDesc = "Membership No";
	else
	    ddTypeDesc = ddType;
	String bank = null;
	if (ddm.isCreditCard()) {
	    if (ddm.getBsbNumber().equalsIgnoreCase("EV"))
		bank = "VISA";
	    else if (ddm.getBsbNumber().equalsIgnoreCase("EM"))
		bank = "Mastercard";
	    else if (ddm.getBsbNumber().equalsIgnoreCase("EA"))
		bank = "American Express";
	    else if (ddm.getBsbNumber().equalsIgnoreCase("ED"))
		bank = "Diners Club";
	    else if (ddm.getBsbNumber().equalsIgnoreCase("EB"))
		bank = "Bankcard";
	} else
	    bank = ddm.getBsbNumber();
	String expDate = null;
	if (ddm.isCreditCard())
	    expDate = makeEntry("Expiry Date", ddm.getCcExpiryDate() + "") + ",";
	else
	    expDate = "";
	String rs = "[" + makeEntry("Current?", currentVer + "") + "," + makeEntry("Client No", clientNo + "") + "," + makeEntry(ddTypeDesc, this.systemNo + "") + "," + makeEntry("Frequency", this.frequency) + "," + makeEntry("Debit Day", this.ddDay + "") + "," + makeEntry("Made Date", madeDate + "") + "," + makeEntry("Made Id", this.madeId) + "," + makeEntry("Made Sales Branch", this.madeSlsBch) + "," + makeEntry("PDC Date", pdcDate + "") + "," + makeEntry("Bank", bank) + "," + makeEntry("Account", ddm.getBankAccount()) + "," + expDate + makeEntry("Drawer", ddm.getDrawer()) + "," + makeEntry("Change Date", ddm.getChangeDate() + "") + "," + makeEntry("Change Id", ddm.getChangeId());
	for (int xx = 0; xx < ddm.getOldDrawers().size(); xx++) {
	    String od = (String) ddm.getOldDrawers().get(xx);
	    if (od != null && !od.trim().equals("")) {
		if (xx == 0)
		    rs += "," + makeEntry("History", od);
		else
		    rs += "," + makeEntry("", od);
	    }
	}
	rs += "]";

	return rs;
    }

    private String makeEntry(String title, String value) {
	return "{\"title\":\"" + title + "\",\"value\":\"" + value + "\"}";
    }

}
