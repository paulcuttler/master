package com.ract.payment.directdebit;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.ClassWriter;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.receipting.Payable;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * A schedule of direct debit payments and dates.
 * 
 * @author Glenn Lewis,John Holliday
 * @created 15 January 2003
 * @version 1.0, 10/5/2002
 * @version 2.0, 8/1/2003 Severely remodelled. TB. JH.
 */
public class DirectDebitSchedule extends PaymentMethod implements Comparable, Writable {
    /**
     * An internally generated number that uniquely identifies the schedule of
     * direct debit deductions.
     */
    private Integer directDebitScheduleID;

    /**
     * The start date for the period that deductions are to start from. The
     * first actual scheduled deduction will be on the next occurence of the
     * "day in the month to debit day" specified in the direct debit authority
     * that is after the start date.
     */
    private DateTime startDate;

    /**
     * The date that deductions are to be completed by. The last scheduled
     * deduction will not be later than this date.
     */
    private DateTime endDate;

    /**
     * The amount to deduct spread over the period between the start and end
     * dates and at the frequency specified in the direct debit authority.
     */
    private BigDecimal amortisedAmount;

    /**
     * The additional amount to deduct in the first scheduled deduction.
     */
    private BigDecimal immediateAmount;

    /**
     * The payable item in the payment system that the direct debit schedule
     * relates to.
     */
    private Integer payableItemID;

    /**
     * The direct debit authority under which the schedule of direct debit
     * deductions was created.
     */
    private DirectDebitAuthority directDebitAuthority;

    /**
     * The direct debit authority identifier
     */
    private Integer directDebitAuthorityID;

    /**
     * A product code supplied by the source system when the direct debit
     * schedule is created. Allows the reason why a direct debit schedule was
     * created to be more easily assertained.
     */
    private String productCode;

    /**
     * A list of deductions that will be made from the clients bank account.
     */
    private ArrayList scheduleItemList = null;

    private String scheduleDescription;

    private String receiptingSystemID;

    private DateTime createDate;

    /**
     * Construct to use when creating a new schedule specification.
     * 
     * @param authority
     *            Description of the Parameter
     * @param sDate
     *            Description of the Parameter
     * @param eDate
     *            Description of the Parameter
     * @param amortisedAmount
     *            Description of the Parameter
     * @param immediateAmount
     *            Description of the Parameter
     * @param payableItemID
     *            Description of the Parameter
     * @param clientNumber
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @param salesBranchCode
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param prodCode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    public DirectDebitSchedule(DirectDebitAuthority authority, DateTime sDate, DateTime eDate, BigDecimal amortisedAmount, BigDecimal immediateAmount, Integer payableItemID, Integer clientNumber, String userID, String salesBranchCode, SourceSystem sourceSystem, String prodCode, String desc) throws ValidationException {
	super(clientNumber, userID, salesBranchCode, sourceSystem);
	setDirectDebitAuthority(authority);
	setStartDate(sDate);
	setEndDate(eDate);
	setAmortisedAmount(amortisedAmount);
	setImmediateAmount(immediateAmount);
	setPayableItemID(payableItemID);
	setProductCode(prodCode);
	setScheduleDescription(desc);
    }

    /**
     * Construct to use when replacing an existing schedule specification.
     * 
     * @param scheduleID
     *            Description of the Parameter
     * @param authority
     *            Description of the Parameter
     * @param sDate
     *            Description of the Parameter
     * @param eDate
     *            Description of the Parameter
     * @param amortisedAmount
     *            Description of the Parameter
     * @param immediateAmount
     *            Description of the Parameter
     * @param payableItemID
     *            Description of the Parameter
     * @param clientNumber
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @param salesBranchCode
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param prodCode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    public DirectDebitSchedule(Integer scheduleID, DirectDebitAuthority authority, DateTime sDate, DateTime eDate, BigDecimal amortisedAmount, BigDecimal immediateAmount, Integer payableItemID, Integer clientNumber, String userID, String salesBranchCode, SourceSystem sourceSystem, String prodCode) throws ValidationException {
	super(clientNumber, userID, salesBranchCode, sourceSystem);
	setDirectDebitScheduleID(scheduleID);
	setDirectDebitAuthority(authority);
	setStartDate(sDate);
	setEndDate(eDate);
	setAmortisedAmount(amortisedAmount);
	setImmediateAmount(immediateAmount);
	setPayableItemID(payableItemID);
	setProductCode(prodCode);
    }

    /**
     * Construct for use when loading existing schedule data.
     * 
     * @param scheduleID
     *            Description of the Parameter
     * @param authority
     *            Description of the Parameter
     * @param sDate
     *            Description of the Parameter
     * @param eDate
     *            Description of the Parameter
     * @param amortisedAmount
     *            Description of the Parameter
     * @param immediateAmount
     *            Description of the Parameter
     * @param payableItemID
     *            Description of the Parameter
     * @param clientNumber
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @param salesBranchCode
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param itemList
     *            Description of the Parameter
     * @param prodCode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    public DirectDebitSchedule(Integer scheduleID, DateTime createDate, DirectDebitAuthority authority, DateTime sDate, DateTime eDate, BigDecimal amortisedAmount, BigDecimal immediateAmount, Integer payableItemID, Integer clientNumber, String userID, String salesBranchCode, SourceSystem sourceSystem, String prodCode, ArrayList itemList) throws ValidationException {
	super(clientNumber, userID, salesBranchCode, sourceSystem);
	setDirectDebitScheduleID(scheduleID);
	setCreateDate(createDate);
	setDirectDebitAuthority(authority);
	setStartDate(sDate);
	setEndDate(eDate);
	setAmortisedAmount(amortisedAmount);
	setImmediateAmount(immediateAmount);
	setPayableItemID(payableItemID);
	setProductCode(prodCode);
	setScheduleItemList(itemList);
    }

    /**
     * Construct for use when loading existing schedule data.
     * 
     * @param scheduleID
     *            Description of the Parameter
     * @param authority
     *            Description of the Parameter
     * @param sDate
     *            Description of the Parameter
     * @param eDate
     *            Description of the Parameter
     * @param amortisedAmount
     *            Description of the Parameter
     * @param immediateAmount
     *            Description of the Parameter
     * @param payableItemID
     *            Description of the Parameter
     * @param clientNumber
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @param salesBranchCode
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param itemList
     *            Description of the Parameter
     * @param prodCode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    public DirectDebitSchedule(Integer scheduleID, DateTime createDate, DirectDebitAuthority authority, DateTime sDate, DateTime eDate, BigDecimal amortisedAmount, BigDecimal immediateAmount, Integer payableItemID, Integer clientNumber, String userID, String salesBranchCode, SourceSystem sourceSystem, String prodCode, ArrayList itemList, String desc) throws ValidationException {
	super(clientNumber, userID, salesBranchCode, sourceSystem);
	setDirectDebitScheduleID(scheduleID);
	setCreateDate(createDate);
	setDirectDebitAuthority(authority);
	setStartDate(sDate);
	setEndDate(eDate);
	setAmortisedAmount(amortisedAmount);
	setImmediateAmount(immediateAmount);
	setPayableItemID(payableItemID);
	setProductCode(prodCode);
	setScheduleItemList(itemList);
	setScheduleDescription(desc);
    }

    /**
     * Get the payable attached to the schedule. Will only have an amount
     * outstanding if direct debits have failed. <b>This will always refer to
     * the OI payable as this is the implementation in the direct debit
     * system.</b>
     * 
     * @throws RemoteException
     * @return Payable
     */
    public Payable getPayable() throws RemoteException {
	Payable payable = null;
	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	try {
	    if (this.getClientNumber() != null && this.getReceiptingSystemID() != null) {
		payable = pMgr.getPayable(this.getClientNumber(), this.getReceiptingSystemID(), SourceSystem.OCR.getAbbreviation(), this.sourceSystem);
	    }
	} catch (PaymentException ex) {
	    throw new RemoteException("Unable to get payable.", ex);
	}
	return payable;
    }

    /**
     * Order by create date.
     * 
     * @param obj
     *            Object
     * @return int
     */
    public int compareTo(Object obj) {
	LogUtil.debug(this.getClass(), "obj=" + obj);
	LogUtil.debug(this.getClass(), "this" + this);
	if (obj != null && obj instanceof DirectDebitSchedule) {
	    DirectDebitSchedule ddSchedule = (DirectDebitSchedule) obj;
	    if (this.createDate != null && ddSchedule.getCreateDate() != null) {
		return this.createDate.compareTo(ddSchedule.getCreateDate());
	    }
	}
	return 0;
    }

    public DirectDebitSchedule(Integer scheduleId, Exception paymentMethodException) {
	this.directDebitScheduleID = scheduleId;
	this.setPaymentMethodException(paymentMethodException);
    }

    /**
     * Constructor to use when loading a provisional direct debit schedule
     * 
     * @param authority
     *            Description of the Parameter
     * @param sDate
     *            Description of the Parameter
     * @param eDate
     *            Description of the Parameter
     * @param amortisedAmount
     *            Description of the Parameter
     * @param immediateAmount
     *            Description of the Parameter
     * @param itemList
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    public DirectDebitSchedule(DirectDebitAuthority authority, DateTime sDate, DateTime eDate, BigDecimal amortisedAmount, BigDecimal immediateAmount, Integer payableItemID, Integer clientNumber, String userID, String salesBranchCode, SourceSystem sourceSystem, String prodCode, ArrayList itemList, String desc) throws ValidationException {
	super(clientNumber, userID, salesBranchCode, sourceSystem);
	setDirectDebitAuthority(authority);
	setStartDate(sDate);
	setEndDate(eDate);
	setPayableItemID(payableItemID);
	setProductCode(prodCode);
	setAmortisedAmount(amortisedAmount);
	setImmediateAmount(immediateAmount);
	setScheduleItemList(itemList);
	setScheduleDescription(desc);
    }

    /**
     * Gets the amortisedAmount attribute of the DirectDebitSchedule object
     * 
     * @return The amortisedAmount value
     */
    public BigDecimal getAmortisedAmount() {
	return amortisedAmount;
    }

    /**
     * Is the schedule paid?
     * 
     * @return boolean
     */
    public boolean isPaid() {
	boolean paid = false;
	BigDecimal amountOS = this.getTotalAmountOutstanding();
	// if outstanding amount is 0 then it is paid
	if (amountOS.doubleValue() == 0) {
	    paid = true;
	}
	return paid;
    }

    /**
     * Has the first scheduled payment been paid?
     */
    public boolean hasPaidFirstPayment() {
	boolean paidFirstPayment = false;
	DirectDebitScheduleItem ddScheduleItem = this.getLastPaidScheduledItem();
	if (ddScheduleItem != null) {
	    paidFirstPayment = true;
	}
	return paidFirstPayment;
    }

    /**
     * Check that debt has not been transferred to receipting system since
     * failing.
     * 
     * @return boolean
     */
    public boolean isFailed() {
	boolean failed = false;
	Payable payable = null;
	try {
	    payable = this.getPayable();
	    // if outstanding then it has failed
	    if (payable != null && payable.getAmountOutstanding().compareTo(new BigDecimal(0)) > 0) {
		failed = true;
	    }
	} catch (RemoteException ex) {
	    // ignore
	}
	return failed;
    }

    /**
     * Get the default description of the schedule
     */
    public String getDescription() {
	return PaymentMethod.DIRECT_DEBIT;
    }

    /**
     * Gets the directDebitAuthority attribute of the DirectDebitSchedule object
     * 
     * @return The directDebitAuthority value
     * @exception RemoteException
     *                Description of the Exception
     */
    public DirectDebitAuthority getDirectDebitAuthority() throws RemoteException {
	// Lazy load the authority
	if (this.directDebitAuthority == null && this.directDebitAuthorityID != null) {
	    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    try {
		this.directDebitAuthority = paymentMgr.getDirectDebitAuthority(this.directDebitAuthorityID);
	    } catch (PaymentException ex) {
		throw new RemoteException("Unable to get direct debit authority.", ex);
	    }
	}
	return this.directDebitAuthority;
    }

    /**
     * Gets the directDebitAuthorityID attribute of the DirectDebitSchedule
     * object
     * 
     * @return The directDebitAuthorityID value
     */
    public Integer getDirectDebitAuthorityID() {
	return directDebitAuthorityID;
    }

    /**
     * Gets the directDebitScheduleID attribute of the DirectDebitSchedule
     * object
     * 
     * @return The directDebitScheduleID value
     */
    public Integer getDirectDebitScheduleID() {
	return directDebitScheduleID;
    }

    /**
     * Sets the directDebitScheduleID attribute of the DirectDebitSchedule
     * object
     * 
     * @return The endDate value
     */

    /**
     * Gets the endDate attribute of the DirectDebitSchedule object
     * 
     * @return The endDate value
     */
    public DateTime getEndDate() {
	return endDate;
    }

    /**
     * Gets the immediateAmount attribute of the DirectDebitSchedule object
     * 
     * @return The immediateAmount value
     */
    public BigDecimal getImmediateAmount() {
	return immediateAmount;
    }

    /**
     * Gets the payableItemID attribute of the DirectDebitSchedule object
     * 
     * @return The payableItemID value
     */
    public Integer getPayableItemID() {
	return payableItemID;
    }

    /**
     * For a direct debit type of payment method the direct debit schedule ID is
     * the payment method ID.
     * 
     * @return The paymentMethodID value
     */
    public String getPaymentMethodID() {
	return this.directDebitScheduleID != null ? this.directDebitScheduleID.toString() : null;
    }

    /**
     * Sets the paymentMethodID attribute of the DirectDebitSchedule object
     * 
     * @param payMethodID
     *            The new paymentMethodID value
     * @exception SystemException
     *                Description of the Exception
     */
    public void setPaymentMethodID(String payMethodID) throws SystemException {
	this.directDebitScheduleID = new Integer(payMethodID);
    }

    /**
     * Return the number of scheduled items in the list of scheduled items
     * 
     * @return The number of scheduled items in the list
     */
    public int getScheduledItemCount() {
	return scheduleItemList != null ? scheduleItemList.size() : 0;
    }

    /**
     * Return a copy of the specified scheduled item from the list. The index is
     * zero based. i.e. an index of 0 (zero) gets the first scheduled item.
     * 
     * @param index
     *            Description of the Parameter
     * @return The scheduleItem value
     */
    public DirectDebitScheduleItem getScheduleItem(int index) {
	DirectDebitScheduleItem ddItem = null;
	if (this.scheduleItemList != null && 0 <= index && index < this.scheduleItemList.size()) {
	    ddItem = (DirectDebitScheduleItem) this.scheduleItemList.get(index);
	    ddItem = ddItem.copy();
	}
	return ddItem;
    }

    /**
     * Get the scheduled item by the dd sequence reference.
     * 
     * @param scheduledItemId
     *            Integer
     * @return DirectDebitScheduleItem
     */
    public DirectDebitScheduleItem getScheduleItemById(Integer scheduledItemId) {
	DirectDebitScheduleItem targetDDScheduleItem = null;
	DirectDebitScheduleItem ddScheduleItem = null;
	boolean found = false;
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size() && !found; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.getScheduleItemId().equals(scheduledItemId)) {
		    targetDDScheduleItem = ddScheduleItem;
		    found = true;
		}
	    }
	}
	return targetDDScheduleItem;
    }

    /**
     * Gets the startDate attribute of the DirectDebitSchedule object
     * 
     * @return The startDate value
     */
    public DateTime getStartDate() {
	return startDate;
    }

    /**
     * Get the total amount that is/was to be deducted from the customers bank
     * account. This includes amounts already deducted, amounts sent for
     * processing by the bank but not yet confirmed and amounts sent for
     * processing by the bank and confirmed as being deducted.
     * 
     * @return double
     * @roseuid 3E13825D026E
     */
    public BigDecimal getTotalAmount() {
	BigDecimal runningTotal = new BigDecimal(0);
	runningTotal = this.getAmortisedAmount().add(this.getImmediateAmount());
	return runningTotal;
    }

    /**
     * Get the total amount for scheduled items with a status of "Waiting". This
     * means that the amount has been sent to the bank for processing but the
     * bank has not confirmed that the amount was actually deducted.
     * 
     * @return double
     * @roseuid 3E13AD4000DB
     */
    public BigDecimal getTotalAmountWaiting() {
	DirectDebitScheduleItem ddScheduleItem = null;
	BigDecimal runningTotal = new BigDecimal(0);
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size(); i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.getStatus().equals(DirectDebitItemStatus.WAITING)) {
		    runningTotal = runningTotal.add(ddScheduleItem.getAmount());
		}
	    }
	}
	return runningTotal;
    }

    /**
     * Are amounts waiting to be resolved by the bank.
     */
    public boolean containsAmountWaiting() {
	boolean waiting = false;
	if (this.getTotalAmountWaiting().compareTo(new BigDecimal(0)) > 0) {
	    waiting = true;
	}
	return waiting;
    }

    /**
     * Get amount paid on the schedule.
     */
    public BigDecimal getTotalAmountPaid() {
	DirectDebitScheduleItem ddScheduleItem = null;
	BigDecimal totalAmountPaid = new BigDecimal(0);
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size(); i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.getStatus().equals(DirectDebitItemStatus.PAID) || ddScheduleItem.getStatus().equals(DirectDebitItemStatus.WAITING)) {
		    totalAmountPaid = totalAmountPaid.add(ddScheduleItem.getAmount());
		}
	    }
	}
	return totalAmountPaid;
    }

    /**
     * Get the total amount for schedule items with a status of 'S' or starting
     * with 'D'. This means that the amount has not yet been confirmed by the
     * bank as being successfully deducted.
     * 
     * @return double
     * @roseuid 3E13825D0278
     */
    public BigDecimal getTotalAmountOutstanding() {
	DirectDebitScheduleItem ddScheduleItem = null;
	BigDecimal runningTotal = new BigDecimal(0);
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size(); i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.getStatus().equals(DirectDebitItemStatus.SCHEDULED) ||
		// waiting is now considered paid
			ddScheduleItem.isDishonoured()) /**
		 * @todo include
		 *       cancelled/dishonoured
		 */
		{
		    runningTotal = runningTotal.add(ddScheduleItem.getAmount());
		}
	    }
	}

	// add receipting amount
	runningTotal = runningTotal.add(getReceiptingAmount());

	return runningTotal;
    }

    public BigDecimal getReceiptingAmount() {
	BigDecimal receiptingAmount = new BigDecimal(0);
	try {
	    Payable payable = getPayable();
	    if (payable != null) {
		receiptingAmount = receiptingAmount.add(payable.getAmountOutstanding());
	    }
	} catch (RemoteException e) {
	    LogUtil.fatal(this.getClass(), "Unable to get amount outstanding from the receipting system. " + e.getMessage());
	}
	return receiptingAmount;
    }

    /**
     * Use to stop schedule editing.
     */
    public boolean containsUnscheduledDishonouredPayments() {
	DirectDebitScheduleItem ddScheduleItem = null;

	boolean containsUnscheduledDishonouredPayments = false;
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size() && !containsUnscheduledDishonouredPayments; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.isDishonoured() && !isReplaced(ddScheduleItem)) {
		    containsUnscheduledDishonouredPayments = true;
		}
	    }
	}
	return containsUnscheduledDishonouredPayments;
    }

    /**
     * Check if direct debit scheduled item is replaced.
     * 
     * @param scheduledItem
     *            DirectDebitScheduleItem
     * @return boolean
     */
    private boolean isReplaced(DirectDebitScheduleItem scheduledItem) {
	boolean replaced = false;
	DirectDebitScheduleItem ddScheduleItem = null;
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size() && !replaced; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.getReplaceNumber() != null && ddScheduleItem.getReplaceNumber().equals(scheduledItem.getScheduleItemId())) {
		    replaced = true;
		}
	    }
	}
	return replaced;
    }

    /**
     * @return com.ract.payment.directdebit.DirectDebitScheduleItem
     * @todo get the last paid scheduled item. Will always return null if called
     *       from a remaining scheduled list.
     * @roseuid 3E13825D0282
     */
    public DirectDebitScheduleItem getLastPaidScheduledItem() {
	DirectDebitScheduleItem ddScheduleItem = null;
	DirectDebitScheduleItem lastPaidScheduledItem = null;
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size() && lastPaidScheduledItem == null; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		// if dd schedule is paid or waiting
		if (DirectDebitItemStatus.PAID.equals(ddScheduleItem.getStatus()) || DirectDebitItemStatus.WAITING.equals(ddScheduleItem.getStatus())) {
		    lastPaidScheduledItem = ddScheduleItem;
		    LogUtil.debug(this.getClass(), "lastPaidScheduledItem=" + lastPaidScheduledItem);
		}
	    }
	}
	return lastPaidScheduledItem;
    }

    /**
     * Return true if the direct debit schedule instance is a specification for
     * a direct debit schedule only and not an actual generated direct debit
     * schedule.
     * 
     * @return boolean
     * @roseuid 3E13BB170220
     */
    public boolean isSpecification() {
	return this.scheduleItemList == null;
    }

    /**
     * Has the schedule been superseded by another schedule. If any scheduled
     * item is not replaced the whole schedule is considered to not be replaced.
     */
    public boolean isReplaced() {
	boolean replaced = false;
	DirectDebitScheduleItem ddScheduleItem = null;
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size() && !replaced; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (DirectDebitItemStatus.REPLACED.equals(ddScheduleItem.getStatus())) {
		    replaced = true;
		}
	    }
	}
	return replaced;
    }

    /**
     * Is the schedule cancelled.
     * 
     * @return boolean
     */
    public boolean isCancelled() {
	boolean cancelled = false;
	DirectDebitScheduleItem ddScheduleItem = null;
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size() && !cancelled; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (DirectDebitItemStatus.CANCELLED.equals(ddScheduleItem.getStatus())) {
		    cancelled = true;
		}
	    }
	}
	return cancelled;
    }

    /**
     * Sets the amortisedAmount attribute of the DirectDebitSchedule object
     * 
     * @param amortisedAmount
     *            The new amortisedAmount value
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setAmortisedAmount(BigDecimal amortisedAmount) throws ValidationException {
	if (amortisedAmount != null && amortisedAmount.doubleValue() < 0) {
	    throw new ValidationException("The amortised amount on the direct debit schedule '" + amortisedAmount + "' cannot be less than zero.");
	}
	this.amortisedAmount = amortisedAmount;
    }

    /**
     * Sets the directDebitAuthority attribute of the DirectDebitSchedule object
     * 
     * @param authority
     *            The new directDebitAuthority value
     */
    private void setDirectDebitAuthority(DirectDebitAuthority authority) {
	this.directDebitAuthority = authority;
	if (this.directDebitAuthority != null) {
	    this.directDebitAuthorityID = this.directDebitAuthority.getDirectDebitAuthorityID();
	} else {
	    this.directDebitAuthorityID = null;
	}
    }

    /**
     * Sets the directDebitAuthorityID attribute of the DirectDebitSchedule
     * object.
     * 
     * @param directDebitAuthorityID
     *            The new directDebitAuthorityID value
     */
    public void setDirectDebitAuthorityID(Integer directDebitAuthorityID) {
	this.directDebitAuthorityID = directDebitAuthorityID;
    }

    /**
     * Sets the directDebitScheduleID attribute of the DirectDebitSchedule
     * object
     * 
     * @param directDebitScheduleID
     *            The new directDebitScheduleID value
     */
    private void setDirectDebitScheduleID(Integer directDebitScheduleID) {
	this.directDebitScheduleID = directDebitScheduleID;
    }

    /**
     * Sets the endDate attribute of the DirectDebitSchedule object
     * 
     * @param endDate
     *            The new endDate value
     */
    public void setEndDate(DateTime endDate) {
	this.endDate = endDate;
    }

    /**
     * Sets the immediateAmount attribute of the DirectDebitSchedule object
     * 
     * @param immediateAmount
     *            The new immediateAmount value
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setImmediateAmount(BigDecimal immediateAmount) throws ValidationException {
	if (immediateAmount != null && immediateAmount.doubleValue() < 0) {
	    throw new ValidationException("The immediate amount on the direct debit schedule '" + immediateAmount + "' cannot be less than zero.");
	}
	this.immediateAmount = immediateAmount;
    }

    /**
     * Sets the payableItemID attribute of the DirectDebitSchedule object
     * 
     * @param payableItemID
     *            The new payableItemID value
     */
    public void setPayableItemID(Integer payableItemID) {
	this.payableItemID = payableItemID;
    }

    /**
     * Sets the scheduleItemList attribute of the DirectDebitSchedule object
     * 
     * @param itemList
     *            The new scheduleItemList value
     */
    private void setScheduleItemList(ArrayList itemList) {
	LogUtil.debug(this.getClass(), "setScheduleItemList=" + itemList);
	this.scheduleItemList = itemList;
    }

    /**
     * Sets the startDate attribute of the DirectDebitSchedule object
     * 
     * @param startDate
     *            The new startDate value
     */
    private void setStartDate(DateTime startDate) {
	this.startDate = startDate;
    }

    /**
     * Gets the productCode attribute of the DirectDebitSchedule object
     * 
     * @return The productCode value
     */
    public String getProductCode() {
	LogUtil.debug(this.getClass(), "productCode=" + productCode);
	return productCode;
    }

    /**
     * Sets the productCode attribute of the DirectDebitSchedule object
     * 
     * @param productCode
     *            The new productCode value
     */
    public void setProductCode(String productCode) {
	this.productCode = productCode;
    }

    /**
     * Return a collection of payments.
     * 
     * @return the collecton of scheduled items.
     */
    public Collection getScheduleItemList() {
	return scheduleItemList;
    }

    public String getScheduleDescription() {
	return scheduleDescription;
    }

    public String getReceiptingSystemID() {
	return receiptingSystemID;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public void setScheduleDescription(String scheduleDescription) {
	this.scheduleDescription = scheduleDescription;
    }

    public void setReceiptingSystemID(String receiptingSystemID) {
	this.receiptingSystemID = receiptingSystemID;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    /**
     * Determines whether a schedule is current. The definition of current in
     * this sense is that there is still at least one scheduled payment.
     */
    public boolean isCurrent() {
	boolean current = false;
	DirectDebitScheduleItem ddScheduleItem = null;
	if (this.scheduleItemList != null) {
	    for (int i = 0; i < this.scheduleItemList.size() && !current; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.getStatus().equals(DirectDebitItemStatus.SCHEDULED)) {
		    current = true;
		}
	    }
	}
	return current;
    }

    // public boolean isProposed()
    // {
    // boolean current = false;
    // DirectDebitScheduleItem ddScheduleItem = null;
    // if(this.scheduleItemList != null)
    // {
    // for(int i = 0; i < this.scheduleItemList.size() && !current; i++)
    // {
    // ddScheduleItem = (DirectDebitScheduleItem)this.scheduleItemList.get(i);
    // if(ddScheduleItem.getStatus().equals(DirectDebitItemStatus.NOT_SCHEDULED))
    // {
    // current = true;
    // }
    // }
    // }
    // return current;
    // }

    private boolean renewalVersion = false;

    /**
     * Is a renewal version if any scheduled items are marked as renewal or if
     * it has been set explicitly.
     * 
     * @return boolean
     */
    public boolean isRenewalVersion() {
	DirectDebitScheduleItem ddScheduleItem = null;
	if (this.scheduleItemList != null && !renewalVersion) {
	    for (int i = 0; i < this.scheduleItemList.size() && !renewalVersion; i++) {
		ddScheduleItem = (DirectDebitScheduleItem) this.scheduleItemList.get(i);
		if (ddScheduleItem.getStatus().equals(DirectDebitItemStatus.RENEWAL)) {
		    renewalVersion = true;
		}
	    }
	}
	return this.renewalVersion;
    }

    public void setRenewalVersion(boolean renewalVersion) {
	this.renewalVersion = renewalVersion;
    }

    @Override
    public String toString() {
	return "DirectDebitSchedule [amortisedAmount=" + amortisedAmount + ", createDate=" + createDate + ", directDebitAuthority=" + directDebitAuthority + ", directDebitAuthorityID=" + directDebitAuthorityID + ", directDebitScheduleID=" + directDebitScheduleID + ", endDate=" + endDate + ", immediateAmount=" + immediateAmount + ", payableItemID=" + payableItemID + ", productCode=" + productCode + ", receiptingSystemID=" + receiptingSystemID + ", renewalVersion=" + renewalVersion + ", scheduleDescription=" + scheduleDescription + ", scheduleItemList=" + scheduleItemList + ", startDate=" + startDate + "]";
    }

    public static final String WRITABLE_CLASSNAME = "DirectDebitSchedule";

    public String getWritableClassName() {
	return WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("amortisedAmount", amortisedAmount);
	cw.writeAttribute("createDate", createDate);
	cw.writeAttribute("directDebitAuthorityId", directDebitAuthorityID);
	cw.writeAttribute("directDebitScheduleId", directDebitScheduleID);
	cw.writeAttribute("endDate", endDate);
	cw.writeAttribute("immediateAmount", immediateAmount);
	cw.writeAttribute("payableItemId", payableItemID);
	cw.writeAttribute("productCode", productCode);
	cw.writeAttribute("receiptingSystemId", receiptingSystemID);
	cw.writeAttribute("scheduleDescription", scheduleDescription);
	cw.writeAttribute("startDate", startDate);
	cw.endClass();
    }

}
