package com.ract.payment.directdebit;

import java.rmi.RemoteException;
import java.text.ParseException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;

import com.opensymphony.xwork2.ActionSupport;
import com.ract.common.CommonEJBHelper;
import com.ract.common.schedule.ScheduleMgr;

public class ScheduleDDAuditAction extends ActionSupport {

    public String getCronExpression() {
	return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
	this.cronExpression = cronExpression;
    }

    private String cronExpression;

    @Override
    public String execute() throws Exception {

	JobDetail jobDetail = new JobDetail("DDAuditJob", Scheduler.DEFAULT_GROUP, DirectDebitAuditJob.class);

	jobDetail.setDescription("Direct Debit audit");

	// schedule the job
	CronTrigger trigger = null;
	try {
	    trigger = new CronTrigger("DDAuditTrigger", Scheduler.DEFAULT_GROUP, cronExpression);
	} catch (ParseException ex2) {
	    throw new RemoteException("Unable to determine schedule.", ex2);
	}

	trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	}

	return SUCCESS;

    }

}
