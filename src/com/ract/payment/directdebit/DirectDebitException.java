/*
 * @(#)DirectDebitException.java
 *
 */

package com.ract.payment.directdebit;

import com.ract.payment.PaymentException;

/**
 * Generic exception for DirectDebit system.
 * 
 * @author Glenn Lewis
 * @version 1.0, 6/5/2002
 */
public class DirectDebitException extends PaymentException {
    public DirectDebitException() {
    }

    public DirectDebitException(String msg) {
	super(msg);
    }

    public DirectDebitException(Exception chainedException) {
	super(chainedException);
    }

    public DirectDebitException(String msg, Exception chainedException) {
	super(msg, chainedException);
    }
}