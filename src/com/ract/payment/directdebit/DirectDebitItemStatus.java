/*
 * @(#)DirectDebitItemStatus.java
 *
 */
package com.ract.payment.directdebit;

import java.io.Serializable;

/**
 * Represent the status of an item in the direct debit schedule
 * 
 * @author Glenn Lewis
 * @since 8 January 2003
 */
public final class DirectDebitItemStatus implements Serializable {

    /**
     * A description of the status.
     */
    private String description = null;

    /**
     * Constructor. (Private constructor ensures that this class can only be
     * constructed by member methods.)
     * 
     * @param description
     *            a description of the status.
     */
    private DirectDebitItemStatus(String description) {
	this.description = description;
    }

    public boolean equals(Object obj) {
	if (obj != null && obj instanceof DirectDebitItemStatus) {
	    DirectDebitItemStatus ddItemStatus = (DirectDebitItemStatus) obj;
	    return this.getDescription().equals(ddItemStatus.getDescription());
	} else {
	    return false;
	}

    }

    /**
     * The item is cancelled
     */
    public final static DirectDebitItemStatus CANCELLED = new DirectDebitItemStatus("Cancelled");

    /**
     * The item is dishonoured
     */
    public final static DirectDebitItemStatus DISHONOURED = new DirectDebitItemStatus("Dishonoured");

    /**
     * The item is cancelled and dishonoured
     */
    public final static DirectDebitItemStatus CANCELLED_DISHONOURED = new DirectDebitItemStatus("Cancelled/Dishonoured");

    /**
     * The item was scheduled and then cancelled
     */
    public final static DirectDebitItemStatus CANCELLED_SCHEDULED = new DirectDebitItemStatus("Schedule Cancelled");

    /**
     * The item has failed to be deducted.
     */
    public final static DirectDebitItemStatus FAILURE = new DirectDebitItemStatus("Failure");

    /**
     * The item is not scheduled to be paid.
     */
    // public final static DirectDebitItemStatus NOT_SCHEDULED =
    // new DirectDebitItemStatus("Not scheduled");

    /**
     * The item is paid
     */
    public final static DirectDebitItemStatus PAID = new DirectDebitItemStatus("Paid");

    /**
     * The item is replaced
     */
    public final static DirectDebitItemStatus REPLACED = new DirectDebitItemStatus("Replaced");

    /**
     * The item is scheduled to be paid
     */
    public final static DirectDebitItemStatus SCHEDULED = new DirectDebitItemStatus("Scheduled");

    /**
     * The item has been sent to the bank to be deducted but the bank has not
     * confirmed that the amount has been deducted.
     */
    public final static DirectDebitItemStatus WAITING = new DirectDebitItemStatus("Waiting");

    public final static DirectDebitItemStatus UNKNOWN = new DirectDebitItemStatus("Unknown");

    public final static DirectDebitItemStatus RENEWAL = new DirectDebitItemStatus("Renewal");

    /**
     * Get a description of the status
     * 
     * @return a string description of the status
     */
    public String getDescription() {
	return description;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public String toString() {
	return this.description;
    }

}
