package com.ract.payment.directdebit.ui;

import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ract.common.ApplicationServlet;
import com.ract.common.ServiceLocator;
import com.ract.payment.PaymentFactory;
import com.ract.payment.directdebit.DDItem;
import com.ract.payment.directdebit.DDMaster;
import com.ract.payment.directdebit.DDMemo;
import com.ract.payment.directdebit.DDPayable;
import com.ract.payment.directdebit.DDSched;
import com.ract.payment.directdebit.DirectDebitAdapter;
import com.ract.util.CurrencyUtil;

public class DDUIC extends ApplicationServlet {
    private static final String CONTENT_TYPE = "text/xml";
    /** @todo set DTD */
    private static final String DOC_TYPE = null;

    public void handleEvent_loadDDTree(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	try {
	    String ddType = request.getParameter("ddType");
	    String sysNoString = request.getParameter("systemNo");
	    Integer sysNo = new Integer(sysNoString);
	    JSONObject jso = new JSONObject(request.getParameter("data"));
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
	    JSONObject node = jso.getJSONObject("node");
	    String objectId = node.getString("objectId");
	    String fieldName = "";
	    String fieldValue = null;
	    String fieldValue2 = null;
	    String tString = "";
	    String res = "";
	    JSONArray jsOut = new JSONArray();
	    JSONObject js = null;
	    ArrayList ddList = null;
	    ArrayList memoList = null;
	    String bank = null;
	    int xx;
	    if (objectId.equals("")) // is root
	    {
		ddList = ddAdapter.getDDItems(sysNo, ddType);
		for (xx = 0; xx < ddList.size(); xx++) {
		    DDItem ddItem = (DDItem) ddList.get(xx);
		    js = new JSONObject();
		    DDMaster ddm = ddAdapter.getDDMaster(ddItem.getDdSeqNo());
		    if (ddm.isCreditCard()) {
			if (ddm.getBsbNumber().equalsIgnoreCase("EV"))
			    bank = "VISA ";
			else if (ddm.getBsbNumber().equalsIgnoreCase("EM"))
			    bank = "Mastercard ";
			else if (ddm.getBsbNumber().equalsIgnoreCase("EA"))
			    bank = "AmEx ";
			else if (ddm.getBsbNumber().equalsIgnoreCase("ED"))
			    bank = "Diners Club ";
			else if (ddm.getBsbNumber().equalsIgnoreCase("EB"))
			    bank = "Bankcard ";
		    } else
			bank = ddm.getBsbNumber() + "/";
		    js.put("title", ddItem.getMadeDate().toString() + "  " + ddItem.getFrequency() + "/" + ddItem.getDdDay() + " " + bank + ddm.getBankAccount());
		    js.put("isFolder", ddItem.hasPayables());
		    js.put("objectId", "ddItem_" + ddItem.getDdItemNo() + "_" + ddItem.getDdItemSeq());
		    jsOut.put(js);
		}
		// add node for memos if there are any
		memoList = ddAdapter.getDDMemos(sysNo, ddType);
		if (memoList.size() > 0) {
		    js = new JSONObject();
		    js.put("title", "Memo");
		    js.put("isFolder", true);
		    js.put("objectId", "itemMemo_" + sysNo + "_ _" + ddType);
		    jsOut.put(js);
		}
	    } else {
		String[] entries = objectId.split("_");
		fieldName = entries[0];
		Integer key1 = new Integer(entries[1]);
		Integer key2;
		if (entries.length == 3)
		    key2 = new Integer(entries[2]);
		else
		    key2 = null;

		if (fieldName.equals("ddItem")) {
		    ddList = ddAdapter.getDDPayables(key1, key2);
		    for (xx = 0; xx < ddList.size(); xx++) {
			DDPayable ddp = (DDPayable) ddList.get(xx);
			js = new JSONObject();
			tString = ddp.getDdPayableSeq() + " " + ddp.getDescription() + " " + (ddp.getStartDate() == null ? "" : ddp.getStartDate().formatShortDate());
			if (tString == null || tString.trim().equals(""))
			    tString = "*** Error ***";
			js.put("title", tString);
			js.put("isFolder", ddp.HasSchedules());
			js.put("objectId", "ddPayable_" + ddp.getDdPayableSeq() + "_");
			jsOut.put(js);

			memoList = ddAdapter.getDDMemos(ddp.getDdPayableSeq(), "");
			if (memoList.size() > 0) {
			    js = new JSONObject();
			    js.put("title", "Memo");
			    js.put("isFolder", true);
			    js.put("objectId", "itemMemo_" + ddp.getDdPayableSeq() + "_ _" + " ");
			    jsOut.put(js);
			}
		    }
		} else if (fieldName.equals("ddPayable"))// fieldName must be
							 // ddPayable
		{
		    ddList = ddAdapter.getDDSchedules(key1);
		    for (xx = 0; xx < ddList.size(); xx++) {
			DDSched dds = (DDSched) ddList.get(xx);
			js = new JSONObject();
			js.put("title", dds.getSchedDate().formatShortDate() + " " + CurrencyUtil.formatDollarValue(dds.getAmount()) + " " + dds.getTranStatus() + (dds.getAction().equals("") ? "" : " (" + dds.getAction() + ")"));
			js.put("isFolder", false);
			js.put("objectId", "ddSched_" + key1 + "_" + dds.getDdSchedSeq());
			jsOut.put(js);
		    }
		    /* add memos if there are any */
		} else if (fieldName.equals("itemMemo")) {
		    memoList = ddAdapter.getDDMemos(key1, entries[3]);
		    for (xx = 0; xx < memoList.size(); xx++) {
			DDMemo ddMemo = (DDMemo) memoList.get(xx);
			js = new JSONObject();
			js.put("title", "Memo " + ddMemo.getCreateDate().formatShortDate() + " " + ddMemo.getCreateId());
			js.put("isFolder", false);
			js.put("objectId", "ddMemo_" + ddMemo.getDdItemNo() + "_" + ddMemo.getMemoSeq());
			jsOut.put(js);

		    }
		}
	    }
	    // System.out.println(jsOut.toString());
	    response.getWriter().write(jsOut.toString());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException(ex);
	}
    }

    public void handleEvent_getDDDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	// response.setContentType("text/xml");
	// response.setHeader("Cache-Control", "no-cache");
	// System.out.println("handleEvent_getDDDetail");
	// System.out.println(" tableName >>------>> " +
	// request.getParameter("tableName"));
	// System.out.println(" key1 >>------->" + request.getParameter("key1")
	// + "<");
	// System.out.println(" key2 >>----->" + request.getParameter("key2") +
	// "<");

	String retString = null;
	try {
	    String tableName = request.getParameter("tableName");
	    String key1Str = request.getParameter("key1").trim();
	    String key2Str = request.getParameter("key2").trim();
	    Integer key1 = null;
	    if (key1Str != null && !key1Str.equals("")) {
		key1 = new Integer(key1Str);
	    }
	    Integer key2 = null;
	    if (key2Str != null && !key2Str.equals("")) {
		key2 = new Integer(key2Str);
	    }

	    DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
	    if (tableName.equalsIgnoreCase("ddItem")) {
		retString = ddAdapter.getDDItem(key1, key2).getJSONDetail();
	    } else if (tableName.equalsIgnoreCase("ddPayable")) {
		retString = ddAdapter.getDDPayable(key1).toJSONArray();
	    } else if (tableName.equalsIgnoreCase("ddSched")) {
		retString = ddAdapter.getDDSched(key1, key2).toJSONArray();
	    } else if (tableName.equalsIgnoreCase("ddMaster")) {
		retString = ddAdapter.getDDMaster(key1).toJSONArray();
	    } else if (tableName.equalsIgnoreCase("ddMemo")) {
		DDMemo ddMemo = ddAdapter.getDDMemo(key1, key2);
		retString = ddMemo.toJSONArray();
	    } else {
		// do nothing - dont't display details
		// throw new ServletException("Unknown table name: " + tableName
		// );
	    }

	    if (retString != null)
		response.getWriter().write(retString);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException("" + ex);
	}
    }

}
