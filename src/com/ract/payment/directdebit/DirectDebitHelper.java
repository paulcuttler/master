package com.ract.payment.directdebit;

import java.util.ArrayList;
import java.util.Collections;

import com.ract.common.SystemException;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * Class to assist with assembling direct debit related data.
 * 
 * @author jyh
 * @version 1.0
 */

public class DirectDebitHelper {

    public static String getDirectDebitItemSummary(DirectDebitSchedule ddSchedule) {
	LogUtil.debug("getDirectDebitItemSummary", ddSchedule.toString());
	double amount = 0;
	double schedAmount = 0;
	DirectDebitScheduleItem ddItem = null;
	StringBuffer directDebitItemDescription = new StringBuffer();

	if (ddSchedule.isCurrent() || ddSchedule.isRenewalVersion()) // was
								     // proposed
	{
	    ArrayList scheduledItemList = (ArrayList) ddSchedule.getScheduleItemList();
	    Collections.sort(scheduledItemList);
	    for (int y = 0; y < scheduledItemList.size(); y++) {
		ddItem = (DirectDebitScheduleItem) scheduledItemList.get(y);

		LogUtil.debug("scheduled item ", ddItem.toString());

		if (ddItem.isDisplayable()) {
		    schedAmount = ddItem.getAmount().doubleValue() + ddItem.getFeeAmount().doubleValue();

		    amount += schedAmount;

		    directDebitItemDescription.append(formatScheduleItemLine(ddItem.getScheduledDate(), schedAmount, ddItem.getStatus().getDescription()));
		}
	    }
	}
	return directDebitItemDescription.toString();

    }

    private static String formatScheduleItemLine(DateTime scheduleDate, double amount, String description) {
	final String PAD_SPACE = "  ";
	final int maxLength = 8;
	StringBuffer schedItemLine = new StringBuffer();
	if (scheduleDate != null) {
	    schedItemLine.append(scheduleDate.formatShortDate());
	    schedItemLine.append(PAD_SPACE);
	}
	String amountString = CurrencyUtil.formatDollarValue(amount);
	String paddedAmountString = "";

	try {
	    paddedAmountString = StringUtil.leftPadString(amountString, maxLength);
	} catch (SystemException ex) {
	    // no error
	}

	schedItemLine.append(paddedAmountString);
	schedItemLine.append(PAD_SPACE);
	schedItemLine.append(description);
	// always a new line?
	schedItemLine.append("\n");
	LogUtil.debug("DirectDebitHelper", "schedItemLine = " + schedItemLine.toString());

	// right justify amount to x number of spaces
	return schedItemLine.toString();
    }

}
