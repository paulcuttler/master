package com.ract.payment.directdebit;

import com.ract.util.DateTime;

public class DDPayable {
    private Integer canSubSeq;
    private Integer clientNo;
    private Integer ddItemNo;
    private Integer ddItemSeq;
    private Integer ddPayableSeq;
    private String ddStatus;
    private com.ract.util.DateTime endDate;
    private Integer opSeqNo;
    private Integer opSubSeq;
    private DateTime startDate;
    private java.math.BigDecimal amortisedAmount;
    private java.math.BigDecimal immediateAmount;
    private String description;
    private com.ract.util.DateTime createDate;
    private String createId;
    private com.ract.util.DateTime cancDate;
    private String cancId;
    private Integer payableItemNo;
    private boolean hasChildren;

    public DDPayable() {
    }

    public Integer getCanSubSeq() {
	return canSubSeq;
    }

    public void setCanSubSeq(Integer canSubSeq) {
	this.canSubSeq = canSubSeq;
    }

    public Integer getClientNo() {
	return clientNo;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    public Integer getDdItemNo() {
	return ddItemNo;
    }

    public void setDdItemNo(Integer ddItemNo) {
	this.ddItemNo = ddItemNo;
    }

    public Integer getDdItemSeq() {
	return ddItemSeq;
    }

    public void setDdItemSeq(Integer ddItemSeq) {
	this.ddItemSeq = ddItemSeq;
    }

    public Integer getDdPayableSeq() {
	return ddPayableSeq;
    }

    public void setDdPayableSeq(Integer ddPayableSeq) {
	this.ddPayableSeq = ddPayableSeq;
    }

    public String getDdStatus() {
	return ddStatus;
    }

    public void setDdStatus(String ddStatus) {
	this.ddStatus = ddStatus;
    }

    public DateTime getEndDate() {
	return endDate;
    }

    public void setEndDate(DateTime endDate) {
	this.endDate = endDate;
    }

    public Integer getOpSeqNo() {
	return opSeqNo;
    }

    public void setOpSeqNo(Integer opSeqNo) {
	this.opSeqNo = opSeqNo;
    }

    public Integer getOpSubSeq() {
	return opSubSeq;
    }

    public void setOpSubSeq(Integer opSubSeq) {
	this.opSubSeq = opSubSeq;
    }

    public DateTime getStartDate() {
	return startDate;
    }

    public void setStartDate(DateTime startDate) {
	this.startDate = startDate;
    }

    public java.math.BigDecimal getAmortisedAmount() {
	return amortisedAmount;
    }

    public void setAmortisedAmount(java.math.BigDecimal amortisedAmount) {
	this.amortisedAmount = amortisedAmount;
    }

    public java.math.BigDecimal getImmediateAmount() {
	return immediateAmount;
    }

    public void setImmediateAmount(java.math.BigDecimal immediateAmount) {
	this.immediateAmount = immediateAmount;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public com.ract.util.DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(com.ract.util.DateTime createDate) {
	this.createDate = createDate;
    }

    public String getCreateId() {
	return createId;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public com.ract.util.DateTime getCancDate() {
	return cancDate;
    }

    public void setCancDate(com.ract.util.DateTime cancDate) {
	this.cancDate = cancDate;
    }

    public String getCancId() {
	return cancId;
    }

    public void setCancId(String cancId) {
	this.cancId = cancId;
    }

    public String formatForDisplay() {
	return "DD Payable Seq =    " + ddPayableSeq + "\n DD Item No =        " + ddItemNo + "\n DD Item Seq =       " + ddItemSeq + "\n Client No =         " + clientNo + "\n Op Seq No =         " + opSeqNo + "\n Op Sub Seq =        " + opSubSeq + "\n Start Date =        " + startDate + "\n End Date =          " + endDate + "\n Create Date =       " + createDate + "\n Create Id =         " + createId + "\n Status =            " + ddStatus + "\n Amortised Amount =  " + com.ract.util.CurrencyUtil.formatDollarValue(amortisedAmount) + "\n Immediate Amount =  " + com.ract.util.CurrencyUtil.formatDollarValue(immediateAmount) + "\n Description =       " + description + "\n Cancellation Date = " + cancDate + "\n Cancelled By =      " + cancId + "\n Canc Sub Seq =      " + canSubSeq;
    }

    public Integer getPayableItemNo() {
	return payableItemNo;
    }

    public void setPayableItemNo(Integer payableItemNo) {
	this.payableItemNo = payableItemNo;
    }

    public boolean HasSchedules() {
	return hasChildren;
    }

    public void setHasPayables(boolean hasChildren) {
	this.hasChildren = hasChildren;
    }

    public String formatAsHTML() {
	return "DD Payable Seq =    " + ddPayableSeq + "<br> DD Item No =        " + ddItemNo + "<br> DD Item Seq =       " + ddItemSeq + "<br> Client No =         " + clientNo + "<br> Op Seq No =         " + opSeqNo + "<br> Op Sub Seq =        " + opSubSeq + "<br> Start Date =        " + startDate + "<br> End Date =          " + endDate + "<br> Create Date =       " + createDate + "<br> Create Id =         " + createId + "<br> Status =            " + ddStatus + "<br> Amortised Amount =  " + com.ract.util.CurrencyUtil.formatDollarValue(amortisedAmount) + "<br> Immediate Amount =  " + com.ract.util.CurrencyUtil.formatDollarValue(immediateAmount) + "<br> Description =       " + description + "<br> Cancellation Date = " + cancDate + "<br> Cancelled By =      " + cancId + "<br> Canc Sub Seq =      " + canSubSeq;
    }

    public String toJSONArray() {
	String os = "[{\"title\":\"DD Payable Seq    \",\"value\":\"" + ddPayableSeq + "\"}," + "{\"title\":\"DD Item No        \",\"value\":\"" + ddItemNo + "\"}," + "{\"title\":\"DD Item Seq       \",\"value\":\"" + ddItemSeq + "\"}," + "{\"title\":\"Client No         \",\"value\":\"" + clientNo + "\"}," + "{\"title\":\"Op Seq No         \",\"value\":\"" + opSeqNo + "\"}," + "{\"title\":\"Op Sub Seq        \",\"value\":\"" + opSubSeq + "\"}," + "{\"title\":\"Start Date        \",\"value\":\"" + startDate + "\"}," + "{\"title\":\"End Date          \",\"value\":\"" + endDate + "\"}," + "{\"title\":\"Create Date       \",\"value\":\"" + createDate + "\"}," + "{\"title\":\"Create Id         \",\"value\":\"" + createId + "\"}," + "{\"title\":\"Status            \",\"value\":\"" + ddStatus + "\"}," + "{\"title\":\"Amortised Amount  \",\"value\":\"" + com.ract.util.CurrencyUtil.formatDollarValue(amortisedAmount) + "\"}," + "{\"title\":\"Immediate Amount  \",\"value\":\""
		+ com.ract.util.CurrencyUtil.formatDollarValue(immediateAmount) + "\"}," + "{\"title\":\"Description       \",\"value\":\"" + description + "\"}," + "{\"title\":\"Cancellation Date \",\"value\":\"" + cancDate + "\"}," + "{\"title\":\"Cancelled By      \",\"value\":\"" + cancId + "\"}," + "{\"title\":\"Canc Sub Seq      \",\"value\":\"" + canSubSeq + "\"}]";
	return os;
    }

}
