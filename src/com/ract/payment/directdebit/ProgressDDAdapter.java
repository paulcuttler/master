package com.ract.payment.directdebit;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.progress.open4gl.BigDecimalHolder;
import com.progress.open4gl.BooleanHolder;
import com.progress.open4gl.DateHolder;
import com.progress.open4gl.IntHolder;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentTypeVO;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * Adapter for the Direct Debit system written in Progress at the RACT.
 * 
 * A progress proxy class which can be used to retrieve data from the Progress
 * direct debit system.
 * 
 * <h3>PROGRESS APPSERVER LIMITATION!!!</h3>
 * 
 * Only allowed to return one set of data at a time from an AppObject. This
 * means that trying to use a cached proxy will not work where the following is
 * attempted:
 * 
 * <ul>
 * <li>Iterate through the data returning ids
 * <li>Use id to load the record (this fails as you are already iterating
 * through the records using the initial appobject)
 * 
 * Workaround: open a connection and close in the scope of a proxy method. @see
 * getAuthority
 * </ul>
 * 
 * @author Terry Bakker
 * @created 8 January 2003
 * @version 2.0, 7/1/2003
 */
public class ProgressDDAdapter extends TransactionAdapterType implements
		DirectDebitAdapter {

	public final static String SYSTEM_PROGRESS_DIRECT_DEBIT = "ProgressDirectDebit";

	/**
	 * Strings returned for status info from the dd system
	 */
	private final static String DD_STATUS_ALL = "*";

	private final static String DD_STATUS_CANCELLED = "Cancelled";

	private final static String DD_STATUS_CANCEL_CODE = "C";

	private final static String DD_STATUS_DISHONOURED = "D";

	private final static String DD_STATUS_FAILURE = "B";

	private final static String DD_STATUS_PAID = "P"; // actually means
	// processed

	private final static String DD_STATUS_PAID_OCR = "O";

	private final static String DD_STATUS_RENEWAL = "R";

	private final static String DD_STATUS_SCHEDULED = "S";

	private final static String DD_STATUS_WAITING = "W";

	private final static String DD_STATUS_CANCELLED_DISHONOURED = "CD";

	private final static String DD_STATUS_REPLACED = "Replaced";

	private final static String DD_STATUS_PAID_DISHONOURED = "OD";

	private final static String DD_STATUS_CANCELLED_RENEWAL = "CR";

	private final static String DD_STATUS_CANCELLED_SCHEDULED = "CS";
	/**
	 * Strings used to describe fequency in the dd system
	 */
	private final static String DD_FREQUENCY_ANNUALLY = "A";

	private final static String DD_FREQUENCY_HALF_YEARLY = "H";

	private final static String DD_FREQUENCY_QUARTERLY = "Q";

	private final static String DD_FREQUENCY_MONTHLY = "M";

	private final static String DD_FREQUENCY_FORTNIGHTLY = "F";

	/**
	 * An instance of a progress transaction proxy class which can be used to
	 * update data in the Progress direct debit system.
	 */
	private ProgressDDTransactionProxy progressDDTransactionProxy;

	private DDProgressProxy progressProxy;

	/**
	 * Construct the direct debit adapter for the progress-based direct debit
	 * system.
	 */
	public ProgressDDAdapter() {
	}

	/**
	 * Create an new authority to direct debit a clients bank account. If a
	 * direct debit authority for the client does not exist then create a new
	 * direct debit authority and return its ID. Otherwise if any of the
	 * following details: BSB number, Account number, Credit card number,
	 * Deduction frequency, Day to debit, DDR Date on the existing direct debit
	 * authority are different to the new direct debit authority details then
	 * create a new version of the direct debit authority and return its ID.
	 * Otherwise update the existing authority.
	 * 
	 * Do not create or change the schedule of deductions.
	 * 
	 * @param ddAuthority
	 *            Description of the Parameter
	 * @return A hashtable containing the ID of the direct debit authority
	 *         created/updated and the new direct debit schedule id if
	 *         applicable.
	 * @exception RollBackException
	 *                Description of the Exception
	 * @throws ValidationException
	 */
	public Hashtable createAuthority(DirectDebitAuthority ddAuthority)
			throws RollBackException {
		Hashtable ddTable = new Hashtable();
		if (ddAuthority == null) {
			throw new RollBackException(
					"The direct debit authority cannot be null when creating the authority in the ProgressDDAdapter.");
		}
		LogUtil.debug(this.getClass(), "createAuthority " + ddAuthority);

		// A zero authority ID indicates to the progress DD system to create a
		// new authority
		// rather than update an existing authority.
		IntHolder authorityIDHolder = new IntHolder();
		IntHolder oldScheduleIDHolder = new IntHolder();
		IntHolder newScheduleIDHolder = new IntHolder();

		authorityIDHolder
				.setIntValue(ddAuthority.getDirectDebitAuthorityID() == null ? 0
						: ddAuthority.getDirectDebitAuthorityID().intValue());
		String bsbNumber = "";
		String accountNumber = "";
		String ccNumber = "";
		String ccExpiry = "";
		String ccVerification = "";
		BankAccount bankAccount = ddAuthority.getBankAccount();
		String accountName = bankAccount.getAccountName();
		if (bankAccount instanceof CreditCardAccount) {
			CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;

			ccNumber = ccAccount.getCardNumber();
			// strip the "/" from the credit card expiry date
			ccExpiry = ccAccount.getCardExpiry();
			if ((ccExpiry.length() != 5) || (ccExpiry.charAt(2) != '/')) {
				throw new RollBackException("Credit card expiry date '"
						+ ccExpiry + "' is not in the format (mm/yy) required.");
			}

			String ccExpiryMonth = ccExpiry.substring(0, 2);
			String ccExpiryYear = ccExpiry.substring(3, 5);
			ccExpiry = ccExpiryMonth.concat(ccExpiryYear);
			ccVerification = ccAccount.getVerificationValue();
			bsbNumber = ccAccount.getCardTypeCode();
		} else // otherwise a debit account
		{
			DebitAccount debitAccount = (DebitAccount) bankAccount;
			bsbNumber = debitAccount.getBsbNumber();
			accountNumber = debitAccount.getAccountNumber();
		}
		Integer clientNumber = ddAuthority.getClientNumber();
		if (clientNumber == null) {
			throw new RollBackException(
					"The client number on the direct debit authority cannot be null.");
		}
		SourceSystem sourceSystem = ddAuthority.getSourceSystem();
		if (sourceSystem == null) {
			throw new RollBackException(
					"The source system on the direct debit authority cannot be null.");
		}
		int ssReference = ddAuthority.getSourceSystemReference() == null ? 0
				: ddAuthority.getSourceSystemReference().intValue();
		DateTime ddrDate = ddAuthority.getDdrDate();
		if (ddrDate == null) {
			throw new RollBackException(
					"The DDR date on the direct debit authority cannot be null.");
		}
		GregorianCalendar gcDDRDate = new GregorianCalendar();
		gcDDRDate.setTime(ddrDate);
		boolean isOwner = ddAuthority.isClientIsOwner();
		String userID = ddAuthority.getUserID();
		if (userID == null) {
			throw new RollBackException(
					"The user ID on the direct debit authority cannot be null.");
		}
		String salesBranchCode = ddAuthority.getSalesBranchCode();
		if (salesBranchCode == null) {
			throw new RollBackException(
					"The sales branch code on the direct debit authority cannot be null.");
		}

		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.makeAuthority(authorityIDHolder,
					clientNumber.intValue(), sourceSystem.getAbbreviation(),
					ssReference,
					mapFrequency(ddAuthority.getDeductionFrequency()),
					ddAuthority.getDayToDebit(), gcDDRDate, isOwner, bsbNumber,
					accountNumber, ccNumber, ccExpiry, ccVerification,
					accountName, userID, salesBranchCode, newScheduleIDHolder,
					oldScheduleIDHolder);

			handleTransactionProxyReturnString();

			if (authorityIDHolder.isNull()
					|| authorityIDHolder.getIntValue() < 1) {
				throw new RollBackException(
						"Failed to create direct debit authority in direct debit system.  Returned ID ["
								+ authorityIDHolder + "] is invalid");
			}
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
		LogUtil.debug(this.getClass(), "authorityIDHolder  ="
				+ authorityIDHolder.getIntValue());
		// LogUtil.log(this.getClass(), "newScheduleIDHolder=" +
		// newScheduleIDHolder.getIntValue());
		// LogUtil.log(this.getClass(), "oldScheduleIDHolder=" +
		// oldScheduleIDHolder.getIntValue());
		// setup table with returned values
		ddTable.put(DirectDebitAdapter.DD_AUTHORITY_KEY,
				authorityIDHolder.getValue());
		// ddTable.put(DirectDebitAdapter.DD_NEW_SCHEDULE_KEY,
		// newScheduleIDHolder.getValue());
		// ddTable.put(DirectDebitAdapter.DD_OLD_SCHEDULE_KEY,
		// oldScheduleIDHolder.getValue());
		return ddTable;
	}

	/**
	 * Remove an existing direct debit authority. If any scheduled items
	 * associated to any version of the authority have been processed then throw
	 * an exception. The authority cannot be removed. Delete all schedules and
	 * scheduled items associated with all versions of the authority. Delete all
	 * versions of the authority.
	 * 
	 * @param ddAuthorityID
	 * @exception RollBackException
	 *                Description of the Exception
	 * @throws SystemException
	 * @throws ValidationException
	 */
	public void removeAuthority(Integer ddAuthorityID) throws RollBackException {
		LogUtil.debug(this.getClass(), "removeAuthority=" + ddAuthorityID);
		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.removeAuthority(ddAuthorityID.intValue());
			handleTransactionProxyReturnString();
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
	}

	public Integer createRenewalSchedule(DirectDebitSchedule ddSchedule)
			throws RollBackException {
		LogUtil.log(this.getClass(), "createRenewalSchedule");
		return createSchedule(ddSchedule);
	}

	/**
	 * Create a new schedule of deductions given a schedule specification. The
	 * schedule specification consists of a direct debit authority, the start
	 * and end dates of the period over which deductions are to be made, the
	 * amount to amortize over the period of deduction and the additional amount
	 * to take with the first deduction. Always create a new schedule of
	 * deductions even if there is an existing schedule for the same direct
	 * debit authority.
	 * 
	 * @param ddSchedule
	 *            - The direct debit schedule specification that the schedule is
	 *            to be created from. The schedule specification must contain as
	 *            a minimum: an existing direct debit authority ID start date
	 *            end date amortised amount product code
	 * @return int
	 * @exception RollBackException
	 *                Description of the Exception
	 * @throws SystemException
	 * @throws ValidationException
	 */
	public Integer createSchedule(DirectDebitSchedule ddSchedule)
			throws RollBackException {
		if (ddSchedule == null) {
			throw new RollBackException(
					"The direct debit schedule specification cannot be null when creating the schedule in the ProgressDDAdapter.");
		}

		LogUtil.debug(this.getClass(), "createSchedule " + ddSchedule);

		int authorityID = ddSchedule.getDirectDebitAuthorityID() == null ? 0
				: ddSchedule.getDirectDebitAuthorityID().intValue();
		if (authorityID < 1) {
			throw new RollBackException(
					"A valid direct debit authority ID ("
							+ authorityID
							+ ") on the direct debit schedule specification must be given when creating a new direct debit schedule.");
		}

		DateTime startDate = ddSchedule.getStartDate();
		if (startDate == null) {
			throw new RollBackException(
					"The start date on the direct debit schedule specification cannot be null.");
		}
		GregorianCalendar gcStartDate = new GregorianCalendar();
		gcStartDate.setTime(startDate);

		DateTime endDate = ddSchedule.getEndDate();
		if (endDate == null) {
			throw new RollBackException(
					"The end date on the direct debit schedule specification cannot be null.");
		}
		GregorianCalendar gcEndDate = new GregorianCalendar();
		gcEndDate.setTime(endDate);

		BigDecimal amortisedAmount = ddSchedule.getAmortisedAmount() == null ? new BigDecimal(
				0.0) : ddSchedule.getAmortisedAmount();

		BigDecimal immediateAmount = ddSchedule.getImmediateAmount() == null ? new BigDecimal(
				0.0) : ddSchedule.getImmediateAmount();

		String userID = ddSchedule.getUserID();
		if (userID == null) {
			throw new RollBackException(
					"The user ID on the direct debit schedule cannot be null.");
		}

		String salesBranchCode = ddSchedule.getSalesBranchCode();
		if (salesBranchCode == null) {
			throw new RollBackException(
					"The sales branch code on the direct debit schedule cannot be null.");
		}

		int payableItemID = ddSchedule.getPayableItemID() == null ? 0
				: ddSchedule.getPayableItemID().intValue();

		String productCode = ddSchedule.getProductCode();

		IntHolder scheduleIDHolder = new IntHolder();

		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			// check if it is a renewal version
			if (ddSchedule.isRenewalVersion()) {
				transactionProxy.createRenewalSchedule(authorityID,
						gcStartDate, gcEndDate, amortisedAmount,
						immediateAmount, productCode,
						ddSchedule.getScheduleDescription(), userID,
						salesBranchCode, payableItemID, scheduleIDHolder);
			} else {
				transactionProxy.createSchedule(authorityID, gcStartDate,
						gcEndDate, amortisedAmount, immediateAmount,
						productCode, ddSchedule.getScheduleDescription(),
						userID, salesBranchCode, payableItemID,
						scheduleIDHolder);
			}

			handleTransactionProxyReturnString();

			if (scheduleIDHolder.isNull() || scheduleIDHolder.getIntValue() < 1) {
				throw new RollBackException(
						"Failed to create direct debit schedule in direct debit system.  Returned ID ["
								+ scheduleIDHolder.getIntValue()
								+ "] is invalid");
			}
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(
					"Unable to create the direct debit schedule.", gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
		return new Integer(scheduleIDHolder.getIntValue());
	}

	/**
	 * Change the status of all of the remaining scheduled items to "Replaced",
	 * create a new version of the direct debit schedule using the new schedule
	 * specification details and generate new scheduled items.
	 * 
	 * @param ddSchdule
	 *            - A new specification for an existing direct debit schedule.
	 *            The specification must contain the ID of an existing direct
	 *            debit schedule.
	 * @return int
	 * @exception RollBackException
	 *                Description of the Exception
	 * @throws SystemException
	 * @throws ValidationException
	 */
	public Integer replaceSchedule(DirectDebitSchedule ddSchedule)
			throws RollBackException {
		if (ddSchedule == null) {
			throw new RollBackException(
					"The direct debit schedule specification cannot be null when replacing the schedule in the ProgressDDAdapter.");
		}
		LogUtil.debug(this.getClass(), "replaceSchedule=" + ddSchedule);

		int scheduleID = ddSchedule.getDirectDebitScheduleID() == null ? 0
				: ddSchedule.getDirectDebitScheduleID().intValue();
		if (scheduleID < 1) {
			throw new RollBackException(
					"A valid direct debit schedule ID on the direct debit schedule specification must be given when replacing a new direct debit schedule.");
		}

		DateTime startDate = ddSchedule.getStartDate();
		if (startDate == null) {
			throw new RollBackException(
					"The start date on the direct debit schedule specification cannot be null.");
		}
		GregorianCalendar gcStartDate = new GregorianCalendar();
		gcStartDate.setTime(startDate);

		DateTime endDate = ddSchedule.getEndDate();
		if (endDate == null) {
			throw new RollBackException(
					"The end date on the direct debit schedule specification cannot be null.");
		}
		GregorianCalendar gcEndDate = new GregorianCalendar();
		gcEndDate.setTime(endDate);

		BigDecimal amortisedAmount = ddSchedule.getAmortisedAmount() == null ? new BigDecimal(
				0.0) : ddSchedule.getAmortisedAmount();

		BigDecimal immediateAmount = ddSchedule.getImmediateAmount() == null ? new BigDecimal(
				0.0) : ddSchedule.getImmediateAmount();

		String userID = ddSchedule.getUserID();
		if (userID == null) {
			throw new RollBackException(
					"The user ID on the direct debit schedule cannot be null.");
		}

		String salesBranchCode = ddSchedule.getSalesBranchCode();
		if (salesBranchCode == null) {
			throw new RollBackException(
					"The sales branch code on the direct debit schedule cannot be null.");
		}

		String productCode = ddSchedule.getProductCode();
		if (productCode == null) {
			productCode = "";
		}

		IntHolder scheduleIDHolder = new IntHolder();
		int payableItemID = ddSchedule.getPayableItemID() == null ? 0
				: ddSchedule.getPayableItemID().intValue();

		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.replaceSchedule(scheduleID, gcStartDate,
					gcEndDate, amortisedAmount, immediateAmount, productCode,
					ddSchedule.getScheduleDescription(), userID,
					salesBranchCode, payableItemID, scheduleIDHolder);

			handleTransactionProxyReturnString();

			if (scheduleIDHolder.isNull()) {
				throw new RollBackException(
						"Failed to replace direct debit schedule in direct debit system as the returned. Returned ID waas null.");
			}
			if (scheduleIDHolder.getIntValue() < 1) {
				throw new RollBackException(
						"Failed to replace direct debit schedule in direct debit system.  Returned ID ["
								+ scheduleIDHolder.getIntValue()
								+ "] is invalid");
			}
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(
					"Unable to replace the direct debit schedule.", gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
		return new Integer(scheduleIDHolder.getIntValue());
	}

	/**
	 * Returns the bank code.
	 * 
	 * @param bsbNumber
	 * @return
	 * @throws SystemException
	 */
	public String validateBSBNumber(String bsbNumber) throws SystemException {
		if (bsbNumber == null) {
			throw new SystemException("BSB number may not be null.");
		}
		LogUtil.debug(this.getClass(), "validateBSBNumber=" + bsbNumber);
		DDProgressProxy ddProxy = getDDProgressProxy();
		String bankCode = null;
		String invalidReason = null;

		StringHolder invalidReasonHolder = new StringHolder();
		StringHolder bankCodeHolder = new StringHolder();
		try {
			ddProxy.validateBSB(bsbNumber, bankCodeHolder, invalidReasonHolder);

			bankCode = bankCodeHolder.getStringValue();
			invalidReason = invalidReasonHolder.getStringValue();

			if (!invalidReason.equals("")) {
				throw new SystemException(invalidReason);
			}

			if (bankCode == null) {
				throw new SystemException("Bank code is null.");
			}

			handleProxyReturnString(ddProxy);

		} catch (Open4GLException o4gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(o4gle);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}

		return bankCode;
	}

	/**
	 * 
	 * @param ccNumber
	 *            String
	 * @throws SystemException
	 * @return String
	 */
	public String validateCreditCardType(String ccNumber)
			throws SystemException {
		if (ccNumber == null) {
			throw new SystemException("Credit card number may not be null.");
		}
		LogUtil.debug(this.getClass(), "validateCreditCardType=" + ccNumber);
		DDProgressProxy ddProxy = getDDProgressProxy();
		String invalidReason = null;
		String ccType = null;

		StringHolder creditCardType = new StringHolder();
		try {
			ddProxy.validateCCType(ccNumber, creditCardType);

			ccType = creditCardType.getStringValue();

			// ccType should now be one of ED,EA,EM,EV,EB

			if (ccType == null || ccType.equals("")) {
				throw new SystemException("Credit card type may not be null.");
			}
			handleProxyReturnString(ddProxy);

		} catch (Open4GLException o4gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(o4gle);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}

		return ccType;
	}

	/**
	 * @see DirectDebitAdapter
	 * @param directDebitScheduleId
	 *            Integer
	 * @param userId
	 *            String
	 * @throws RollBackException
	 * @return Integer
	 */
	public Integer activateRenewalSchedule(Integer ddScheduleID, String userId)
			throws RollBackException {
		LogUtil.debug(this.getClass(), "activateRenewalSchedule "
				+ ddScheduleID + " " + userId);
		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.activateRenewalSchedule(ddScheduleID.intValue(),
					userId);
			handleTransactionProxyReturnString();
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(
					"Unable to activate the direct debit schedule '"
							+ ddScheduleID + "'.", gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
		return ddScheduleID;
	}

	/**
	 * Change the status of all un-processed scheduled items to "Cancelled".
	 * 
	 * @param ddScheduleID
	 *            - The ID of the direct debit schedule to cancel.
	 * @exception RollBackException
	 *                Description of the Exception
	 * @throws SystemException
	 * @throws ValidationException
	 */
	public void cancelSchedule(Integer ddScheduleID, String userId)
			throws RollBackException {
		LogUtil.debug(this.getClass(), "cancelSchedule " + ddScheduleID + " "
				+ userId);
		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.cancelSchedule(ddScheduleID.intValue(), userId);
			handleTransactionProxyReturnString();
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(
					"Unable to cancel the direct debit schedule.", gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
	}

	/**
	 * Delete all of the scheduled items for the schedule and delete the
	 * schedule. If any of the scheduled items for the schedule have been
	 * processed or have a status of "Replaced" then throw an exception.
	 * 
	 * @param ddScheduleID
	 *            - The ID of the direct debit schedule to remove.
	 * @exception RollBackException
	 *                Description of the Exception
	 * @throws SystemException
	 * @throws ValidationException
	 */
	public void removeSchedule(Integer ddScheduleID) throws RollBackException {
		LogUtil.debug(this.getClass(), "removeSchedule=" + ddScheduleID);
		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.removeSchedule(ddScheduleID.intValue());
			handleTransactionProxyReturnString();
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException("Unable to remove the schedule.", gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
	}

	/**
	 * If any of the scheduled items have a status of "Cancelled" then it is
	 * assumed that the canceling of the schedule is being undone. Change the
	 * status of all cancelled items to "Scheduled". Otherwise, delete all of
	 * the scheduled items for the schedule to be undone and delete the schedule
	 * to be undone. Find the previous schedule, if one was specified, and for
	 * each scheduled item with a status of "Replaced", change the status to
	 * "Scheduled". If any of the scheduled items for the schedule to be undone
	 * have been processed or have a status of "Replaced" then throw an
	 * exception. If the status of the scheduled items is "Replaced" then the
	 * schedule that replaced it must be undone first.
	 * 
	 * @param undoScheduleID
	 *            - The ID of the direct debit schedule to undo.
	 * @param reinstateScheduleID
	 *            - The ID of the direct debit schedule to reinstate. Set to
	 *            zero if no direct debit schedule is being reinstated.
	 * @exception RollBackException
	 *                Description of the Exception
	 * @throws SystemException
	 * @throws ValidationException
	 */
	public void undoSchedule(Integer undoScheduleID,
			Integer reinstateScheduleID, String userID)
			throws RollBackException {
		LogUtil.debug(this.getClass(), "undoSchedule");
		LogUtil.debug(this.getClass(), "undoScheduleID=" + undoScheduleID);
		LogUtil.debug(this.getClass(), "reinstateScheduleID="
				+ reinstateScheduleID);
		LogUtil.debug(this.getClass(), "userID=" + userID);
		// A zero for the reinstate schedule ID indicates to the Progress DD
		// system that there is no previous schedule to reinstate.
		int reinstateID = reinstateScheduleID == null ? 0 : reinstateScheduleID
				.intValue();
		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.undoSchedule(undoScheduleID.intValue(),
					reinstateID, userID);
			handleTransactionProxyReturnString();
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(
					"Unable to undo the direct debit schedule.", gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
	}

	/**
	 * Uncancel a schedule
	 */
	public void uncancelSchedule(Integer unCancelScheduleID)
			throws RollBackException {
		LogUtil.debug(this.getClass(), "uncancelSchedule=" + unCancelScheduleID);
		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.unCancel(unCancelScheduleID.intValue(),
					DD_STATUS_CANCELLED);
			handleTransactionProxyReturnString();
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(
					"Unable to uncancel the direct debit schedule.", gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
	}

	/**
	 * Given a direct debit authority ID return the direct debit authority
	 * details.
	 * 
	 * @param ddAuthorityID
	 * @return com.ract.payment.directdebit.DirectDebitAuthority
	 * @throws SystemException
	 */
	public DirectDebitAuthority getAuthority(Integer ddAuthorityID)
			throws SystemException {
		LogUtil.log(this.getClass(), "getAuthority=" + ddAuthorityID);

		DirectDebitAuthority ddAuthority = null;

		IntHolder clientNoHolder = new IntHolder();
		StringHolder systemHolder = new StringHolder();
		IntHolder systemReferenceHolder = new IntHolder();
		StringHolder frequencyHolder = new StringHolder();
		IntHolder debitDayHolder = new IntHolder();
		DateHolder ddrDateHolder = new DateHolder();
		BooleanHolder isOwnerHolder = new BooleanHolder();
		StringHolder bsbNumberHolder = new StringHolder();
		StringHolder accountNumberHolder = new StringHolder();
		StringHolder ccNumberHolder = new StringHolder();
		StringHolder ccExpiryDateHolder = new StringHolder();
		StringHolder ccVerificationHolder = new StringHolder();
		StringHolder ccTypeHolder = new StringHolder();
		StringHolder accountNameHolder = new StringHolder();
		BooleanHolder approvedHolder = new BooleanHolder();
		StringHolder userIDHolder = new StringHolder();
		StringHolder salesBranchHolder = new StringHolder();
		DateHolder createDateHolder = new DateHolder();

		DDProgressProxy ddProxy = getDDProgressProxy();

		try {
			ddProxy.getAuthority(ddAuthorityID.intValue(), clientNoHolder,
					systemHolder, systemReferenceHolder, frequencyHolder,
					debitDayHolder, ddrDateHolder, isOwnerHolder,
					bsbNumberHolder, accountNumberHolder, ccNumberHolder,
					ccExpiryDateHolder, ccVerificationHolder, ccTypeHolder,
					accountNameHolder, approvedHolder, userIDHolder,
					salesBranchHolder, createDateHolder);
			handleProxyReturnString(ddProxy);

		} catch (Open4GLException o4gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(o4gle);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}

		String freq = frequencyHolder.getStringValue();
		DirectDebitFrequency ddFrequency = mapFrequency(freq);

		BankAccount bankAccount = null;
		if (ccNumberHolder != null && !ccNumberHolder.isNull()
				&& ccNumberHolder.getStringValue().length() > 0) {
			PaymentTypeVO payTypeVO = PaymentEJBHelper.getPaymentMgr()
					.getPaymentTypeVO(ccTypeHolder.getStringValue());

			try {
				CreditCardAccount ccAccount = new CreditCardAccount();
				ccAccount.setValidate(false);
				ccAccount.setPaymentType(payTypeVO);
				ccAccount.setAccountName(accountNameHolder.getStringValue());
				String cardExpiry = ccExpiryDateHolder.getStringValue();
				ccAccount.setCardExpiry(cardExpiry.substring(0, 2) + "/"
						+ cardExpiry.substring(2));
				ccAccount.setCardNumber(ccNumberHolder.getStringValue());
				ccAccount.setVerificationValue(ccVerificationHolder
						.getStringValue());
				bankAccount = ccAccount;
			} catch (ValidationException ve) {
				throw new SystemException(ve);
			}
		} else {
			try {
				DebitAccount debitAccount = new DebitAccount(
						bsbNumberHolder.getStringValue(),
						accountNumberHolder.getStringValue(),
						accountNameHolder.getStringValue(), false);
				// Don't validate
				bankAccount = debitAccount;
			} catch (RemoteException se) {
				throw new SystemException(se);
			}
		}

		SourceSystem sourceSystem = null;
		try {
			sourceSystem = SourceSystem.getSourceSystem(systemHolder
					.getStringValue());
		} catch (GenericException ge) {
			sourceSystem = null;
		}

		if (createDateHolder.getValue() == null) {
			throw new SystemException(
					"The create date not been provided for authority '"
							+ ddAuthorityID + "'.");
		}

		if (ddrDateHolder.getValue() == null) {
			throw new SystemException(
					"The first date of debit has not been provided for authority '"
							+ ddAuthorityID + "'.");
		}

		if (clientNoHolder.getValue() == null) {
			throw new SystemException(
					"The client number is not available for authority '"
							+ ddAuthorityID + "'.");
		}

		if (systemReferenceHolder.getValue() == null) {
			throw new SystemException(
					"The source system reference has not been provided for authority '"
							+ ddAuthorityID + "'.");
		}

		ddAuthority = new DirectDebitAuthority(ddAuthorityID, new Integer(
				clientNoHolder.getIntValue()), userIDHolder.getStringValue(),
				salesBranchHolder.getStringValue(), new DateTime(
						createDateHolder.getDateValue().getTime()),
				isOwnerHolder.getBooleanValue(), debitDayHolder.getIntValue(),
				approvedHolder.getBooleanValue(), new DateTime(ddrDateHolder
						.getDateValue().getTime()), ddFrequency, bankAccount,
				sourceSystem, new Integer(systemReferenceHolder.getIntValue()));

		return ddAuthority;
	}

	// private void logObj(com.progress.open4gl.Holder o)
	// {
	// LogUtil.log(this.getClass(), o.getClass().getName() + " " +
	// o.getValue());
	//
	// }

	/**
	 * Given a client number return a list of all of the DirectDebitAuthority
	 * that belong to the client.
	 * 
	 * @param clientNumber
	 * @return java.util.Vector
	 * @throws SystemException
	 */
	public Vector getAuthorityList(Integer clientNumber) throws SystemException {
		LogUtil.log(this.getClass(), "getAuthorityList=" + clientNumber);
		Vector authorityList = new Vector();
		ArrayList ddAuthorityIdList = new ArrayList();
		Integer authorityId = null;
		DDProgressProxy ddProxy = getDDProgressProxy();
		try {
			ResultSetHolder authorityIDListHolder = new ResultSetHolder();

			ddProxy.getAuthorityList(clientNumber.intValue(),
					authorityIDListHolder);

			handleProxyReturnString(ddProxy);

			ResultSet resultSet = authorityIDListHolder.getResultSetValue();
			while (resultSet.next()) {
				ddAuthorityIdList.add(new Integer(resultSet.getInt(1)));
			}
			resultSet.close();

			for (int i = 0; i < ddAuthorityIdList.size(); i++) {
				authorityId = (Integer) ddAuthorityIdList.get(i);
				authorityList.add(getAuthority(authorityId));
			}
		} catch (Open4GLException gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(gle);
		} catch (SQLException sqle) {
			throw new SystemException(sqle);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return authorityList;
	}

	/**
	 * Given a direct debit schedule ID return the details of the direct debit
	 * schedule including the specification and the scheduled items.
	 * 
	 * @param ddScheduleID
	 * @return com.ract.payment.directdebit.DirectDebitSchedule
	 * @throws SystemException
	 */
	public DirectDebitSchedule getSchedule(Integer ddScheduleID)
			throws SystemException {
		LogUtil.debug(this.getClass(), "getSchedule=" + ddScheduleID);
		// must use a different proxy (connection) to return data
		// if used within a list (ie. from get ScheduleListByXXX).
		DDProgressProxy ddProxy = getDDProgressProxy();
		DirectDebitSchedule ddSchedule = null;
		try {
			DateHolder startDateHolder = new DateHolder();
			DateHolder endDateHolder = new DateHolder();
			BigDecimalHolder amortisedAmountHolder = new BigDecimalHolder();
			BigDecimalHolder immediateAmountHolder = new BigDecimalHolder();
			StringHolder productCodeHolder = new StringHolder();
			StringHolder descriptionHolder = new StringHolder();
			IntHolder ddAuthorityIDHolder = new IntHolder();
			StringHolder userIDHolder = new StringHolder();
			StringHolder salesBranchHolder = new StringHolder();
			IntHolder payableItemIDHolder = new IntHolder();
			StringHolder subSystemHolder = new StringHolder();
			StringHolder receiptingSystemID = new StringHolder();
			DateHolder createDateHolder = new DateHolder();
			ResultSetHolder itemListHolder = new ResultSetHolder();

			ddProxy.getSchedule(ddScheduleID.intValue(), startDateHolder,
					endDateHolder, amortisedAmountHolder,
					immediateAmountHolder, productCodeHolder,
					descriptionHolder, ddAuthorityIDHolder, userIDHolder,
					salesBranchHolder, payableItemIDHolder, subSystemHolder,
					receiptingSystemID, createDateHolder, itemListHolder);

			handleProxyReturnString(ddProxy);

			// Get the direct debit authority that the schedule refers to.
			DirectDebitAuthority ddAuthority = getAuthority(new Integer(
					ddAuthorityIDHolder.getIntValue()));
			if (ddAuthority == null) {
				throw new SystemException(
						"Failed to get direct debit authority with ID '"
								+ ddAuthorityIDHolder.getIntValue()
								+ "' for direct debit schedule with ID '"
								+ ddScheduleID + "'");
			}

			ArrayList itemList = new ArrayList();
			DateTime scheduledDate;
			DateTime itemCreateDate;
			DirectDebitItemStatus itemStatus;
			Object obj;
			BigDecimal scheduledAmount;
			BigDecimal feeAmount;
			DateTime processedDate = null;
			Integer transactionNumber = null;
			Integer replaceNumber = null;
			String createId = null;
			String action = null;
			String returnCode = null;
			String authorisationNumber = null;
			DateTime returnDate = null;
			java.sql.Date tmpReturnDate = null;
			java.sql.Date tmpProcessedDate = null;
			Integer receiptNumber;
			Integer scheduleSequence;
			DirectDebitScheduleItem scheduleItem;
			ResultSet resultSet = itemListHolder.getResultSetValue();
			SourceSystem subSystem = null;
			try {
				subSystem = SourceSystem.getSourceSystem(subSystemHolder
						.getStringValue());
			} catch (Exception e) {
			}
			while (resultSet.next()) {
				scheduledDate = new DateTime(resultSet.getDate(1));

				obj = resultSet.getObject(2);
				if (obj instanceof BigDecimal) {
					scheduledAmount = (BigDecimal) obj;
				} else {
					throw new SystemException(
							"The scheduled amount returned from the direct debit system is not a BigDecimal.");
				}

				obj = resultSet.getObject(3);
				if (!resultSet.wasNull() && obj != null) {
					if (obj instanceof BigDecimal) {
						feeAmount = (BigDecimal) obj;
					} else {
						throw new SystemException(
								"The fee amount returned from the direct debit system is not a BigDecimal.");
					}
				} else {
					feeAmount = new BigDecimal(0);
				}

				itemStatus = mapStatusString(resultSet.getString(4));

				tmpProcessedDate = resultSet.getDate(5);
				if (tmpProcessedDate != null) {
					processedDate = new DateTime(tmpProcessedDate);
				} else {
					processedDate = null;
				}

				receiptNumber = new Integer(resultSet.getInt(6));
				scheduleSequence = new Integer(resultSet.getInt(7));

				itemCreateDate = new DateTime(resultSet.getDate(8));

				createId = resultSet.getString(9);// create id

				transactionNumber = new Integer(resultSet.getInt(10));// transaction
				// number
				action = resultSet.getString(11); // action
				replaceNumber = new Integer(resultSet.getInt(12)); // replace
				// number
				returnCode = resultSet.getString(13); // return code
				authorisationNumber = resultSet.getString(14); // authorisation
				// number

				tmpReturnDate = resultSet.getDate(15);
				if (tmpReturnDate != null) {
					returnDate = new DateTime(tmpReturnDate);
				} else {
					returnDate = null;
				}

				// return date

				scheduleItem = new DirectDebitScheduleItem(itemCreateDate,
						scheduledDate, scheduledAmount, itemStatus,
						processedDate,
						feeAmount,
						receiptNumber,
						scheduleSequence,
						ddScheduleID,
						// get description and product code from the direct
						// debit
						// schedule
						descriptionHolder.getStringValue(),
						productCodeHolder.getStringValue(), createId,
						transactionNumber, action, replaceNumber, returnCode,
						authorisationNumber, returnDate);
				scheduleItem.setSourceSystem(subSystem);
				itemList.add(scheduleItem);
			}
			resultSet.close();

			ddSchedule = new DirectDebitSchedule(
					ddScheduleID,
					new DateTime(createDateHolder.getDateValue().getTime()),
					ddAuthority,
					new DateTime(startDateHolder.getDateValue().getTime()),
					new DateTime(endDateHolder.getDateValue().getTime()),
					amortisedAmountHolder.getBigDecimalValue(),
					immediateAmountHolder.getBigDecimalValue(),
					payableItemIDHolder == null ? null : new Integer(
							payableItemIDHolder.getIntValue()),
					ddAuthority.getClientNumber(),
					// The schedule should be for the same client that the
					// authority is
					// for.
					userIDHolder.getStringValue(),
					salesBranchHolder.getStringValue(),
					ddAuthority.getSourceSystem(),
					// Assume that the same source system that created the
					// authority also created the schedule.
					productCodeHolder == null ? null : productCodeHolder
							.getStringValue(), itemList,
					descriptionHolder.getStringValue());
			ddSchedule.setSourceSystem(subSystem);
			ddSchedule.setReceiptingSystemID(receiptingSystemID
					.getStringValue());

		} catch (Open4GLException gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(gle);
		} catch (SQLException sqle) {
			throw new SystemException(sqle);
		} catch (ValidationException ve) {
			// Since we trust the DD system to have correct data then a
			// ValidationException
			// thrown by the DirectDebitSchedule constructor can be considered a
			// system exception.
			throw new SystemException(ve);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return ddSchedule;
	}

	/**
	 * Given a direct debit authority number return a list of all of the direct
	 * debit schedules associated to the authority.
	 * 
	 * @param ddAuthorityID
	 *            Description of the Parameter
	 * @return java.util.Vector
	 * @throws SystemException
	 */
	public Vector getScheduleListByAuthority(Integer ddAuthorityID)
			throws SystemException {
		LogUtil.debug(this.getClass(), "getScheduleListByAuthority="
				+ ddAuthorityID);
		Vector scheduleList = new Vector();
		Integer scheduleId = null;
		DDProgressProxy ddProxy = getDDProgressProxy();
		try {
			ResultSetHolder scheduleIDListHolder = new ResultSetHolder();

			ddProxy.getScheduleListByAuthority(ddAuthorityID.intValue(),
					scheduleIDListHolder);

			handleProxyReturnString(ddProxy);

			ResultSet resultSet = scheduleIDListHolder.getResultSetValue();

			ArrayList ddScheduleIDList = new ArrayList();
			while (resultSet.next()) {
				ddScheduleIDList.add(new Integer(resultSet.getInt(1)));
			}
			resultSet.close();// close result set

			for (int i = 0; i < ddScheduleIDList.size(); i++) {
				scheduleId = (Integer) ddScheduleIDList.get(i);
				scheduleList.add(getSchedule(scheduleId));
			}

		} catch (Open4GLException gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(gle);
		} catch (SQLException sqle) {
			throw new SystemException(sqle);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return scheduleList;
	}

	private Vector getScheduleListByClientNumber(Integer clientNumber,
			boolean outstandingOnly) throws SystemException {
		// current date
		DateTime endDate = new DateTime();

		Interval effectiveRange = new Interval();
		try {
			// get and set the history limit in years.
			CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
			int historyLimit = Integer.parseInt(commonMgr
					.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,
							SystemParameterVO.PROGRESS_HISTORY_LIMIT_YEARS));
			effectiveRange.setYears(historyLimit);
		} catch (Exception ex) {
			throw new SystemException(ex);
		}

		// five years from current date
		DateTime startDate = endDate.subtract(effectiveRange);
		endDate = endDate.add(effectiveRange);
		return getScheduleListByClientNumber(clientNumber, startDate, endDate,
				outstandingOnly);

	}

	public Vector getScheduleListByClientNumber(Integer clientNumber)
			throws SystemException {
		return getScheduleListByClientNumber(clientNumber, false);
	}

	/**
	 * Given a client number return a list of all of the direct debit schedules
	 * belonging to the client.
	 * 
	 * @param clientNumber
	 * @return java.util.Vector
	 * @throws SystemException
	 */
	public Vector getScheduleListByClientNumber(Integer clientNumber,
			DateTime startDate, DateTime endDate, boolean outstandingOnly)
			throws SystemException {
		LogUtil.debug(this.getClass(), "getScheduleListByClientNumber");
		LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber);
		LogUtil.debug(this.getClass(), "startDate=" + startDate);
		LogUtil.debug(this.getClass(), "endDate=" + endDate);
		LogUtil.debug(this.getClass(), "outstanding=" + outstandingOnly);

		Vector scheduleList = new Vector();
		DDProgressProxy ddProxy = getDDProgressProxy();
		GregorianCalendar gregStartDate = new GregorianCalendar();
		gregStartDate.setTime(startDate);
		GregorianCalendar gregEndDate = new GregorianCalendar();
		gregEndDate.setTime(endDate);
		ArrayList ddScheduleIdList = new ArrayList();

		try {
			ResultSetHolder scheduleIDListHolder = new ResultSetHolder();

			ddProxy.getScheduleList(clientNumber.intValue(), gregStartDate,
					gregEndDate, outstandingOnly, scheduleIDListHolder);

			handleProxyReturnString(ddProxy);

			ResultSet resultSet = scheduleIDListHolder.getResultSetValue();
			DirectDebitSchedule ddSchedule = null;
			Integer scheduleId = null;
			while (resultSet.next()) {
				ddScheduleIdList.add(new Integer(resultSet.getInt(1)));
			}
			resultSet.close();

			for (int i = 0; i < ddScheduleIdList.size(); i++) {
				scheduleId = (Integer) ddScheduleIdList.get(i);
				try {
					ddSchedule = getSchedule(scheduleId);
				} catch (Exception ex) {
					// default
					ddSchedule = new DirectDebitSchedule(scheduleId, ex);
				}
				scheduleList.add(ddSchedule);
			}

		} catch (Open4GLException gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(gle);
		} catch (SQLException sqle) {
			throw new SystemException(sqle);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return scheduleList;
	}

	public String getDDStatus(Integer ddNumber) throws SystemException {
		LogUtil.debug(this.getClass(), "getDDStatus=" + ddNumber);
		String returnString = "";
		DDProgressProxy proxy = this.getDDProgressProxy();
		StringHolder retStrHolder = new StringHolder();
		BooleanHolder okHolder = new BooleanHolder();
		try {
			proxy.isPaidUpToDate(ddNumber.intValue(), okHolder, retStrHolder);
		} catch (Open4GLException ex) {
			throw new SystemException("Error in DDProxy: " + ex);
		} finally {
			this.releaseDDProgressProxy(proxy);
		}
		if (okHolder.getBooleanValue()) {
			returnString = "";
		} else {
			returnString = retStrHolder.getStringValue();
			LogUtil.debug(this.getClass(), "dd status returnString = "
					+ returnString);
		}
		return returnString;
	}

	public String getSystemName() {
		return SYSTEM_PROGRESS_DIRECT_DEBIT;
	}

	/**
	 * Given a deduction frequency, day in the month to debit, start date, end
	 * date, amortized amount and immediate amount return a provisional direct
	 * debit schedule. Do not make any changes to data in the direct debit
	 * system.
	 * 
	 * @param ddSchedule
	 *            - A specification for a new direct debit schedule.
	 * @return com.ract.payment.directdebit.DirectDebitSchedule
	 * @throws SystemException
	 */
	public DirectDebitSchedule getProvisionalSchedule(
			DirectDebitSchedule ddSchedule) throws SystemException {
		LogUtil.debug(this.getClass(), "getProvisionalSchedule=" + ddSchedule);
		DDProgressProxy ddProxy = getDDProgressProxy();
		DirectDebitSchedule newSchedule = null;

		Integer ddAuthorityID = ddSchedule.getDirectDebitAuthorityID();
		if (ddAuthorityID == null) {
			throw new SystemException(
					"The direct debit authority ID must be set on the direct debit schedule specification when creating a provisional schedule.");
		}

		// Get the direct debit authority that the schedule refers to.
		DirectDebitAuthority ddAuthority = getAuthority(ddAuthorityID);
		if (ddAuthority == null) {
			throw new SystemException(
					"Failed to get direct debit authority with ID '"
							+ ddAuthorityID
							+ "'  when creating a provisional schedule.");
		}

		DateTime startDate = ddSchedule.getStartDate();
		if (startDate == null) {
			throw new SystemException(
					"The start date on the direct debit schedule specification cannot be null when creating a provisional schedule.");
		}
		GregorianCalendar gcStartDate = new GregorianCalendar();
		gcStartDate.setTime(startDate);

		DateTime endDate = ddSchedule.getEndDate();
		if (endDate == null) {
			throw new SystemException(
					"The end date on the direct debit schedule specification cannot be null when creating a provisional schedule.");
		}
		GregorianCalendar gcEndDate = new GregorianCalendar();
		gcEndDate.setTime(endDate);

		ResultSetHolder itemListHolder = new ResultSetHolder();

		try {
			ddProxy.getProvisionalSchedule(ddAuthorityID.intValue(),
					ddSchedule.getAmortisedAmount(),
					ddSchedule.getImmediateAmount(), gcStartDate, gcEndDate,
					itemListHolder);

			handleProxyReturnString(ddProxy);

			ArrayList itemList = new ArrayList();
			DateTime scheduledDate;
			DirectDebitItemStatus itemStatus;
			Object obj;
			BigDecimal scheduledAmount;
			BigDecimal feeAmount;
			DateTime processedDate;
			Integer receiptNumber;
			Integer scheduleSequence;
			DirectDebitScheduleItem scheduleItem;
			ResultSet resultSet = itemListHolder.getResultSetValue();
			LogUtil.debug(this.getClass(), "get item list");
			while (resultSet.next()) {
				scheduledDate = new DateTime(resultSet.getDate(1));
				LogUtil.debug(this.getClass(), "resultSet");
				obj = resultSet.getObject(2);
				if (obj instanceof BigDecimal) {
					scheduledAmount = (BigDecimal) obj;
				} else {
					throw new SystemException(
							"The scheduled amount returned from the direct debit system is not a BigDecimal.");
				}
				/*
				 * there is never a fee on a provisional schedule. This will
				 * just break the program
				 */
				// obj = resultSet.getObject(3);
				// if (!resultSet.wasNull()
				// && obj != null) {
				// if (obj instanceof BigDecimal) {
				// feeAmount = (BigDecimal) obj;
				// }
				// else {
				// throw new
				// SystemException("The fee amount returned from the direct debit system is not a BigDecimal.");
				// }
				// }
				// else {
				feeAmount = new BigDecimal(0);
				// }
				//
				// resultSet.getObject(3);
				//
				// itemStatus = mapStatusString(resultSet.getString(4));
				// processedDate = new DateTime(resultSet.getDate(5));
				// receiptNumber = new Integer(resultSet.getInt(6));
				// scheduleSequence = new Integer(resultSet.getInt(7));
				//
				//
				// scheduleItem =
				// new DirectDebitScheduleItem(
				// scheduledDate,
				// scheduledAmount,
				// itemStatus,
				// processedDate,
				// feeAmount,
				// receiptNumber,
				// scheduleSequence,
				// null,
				// "",
				// ""); //not required for a dummy schedule
				//

				itemStatus = mapStatusString("R"); // renewal version
				processedDate = null;
				receiptNumber = null;
				scheduleSequence = null;

				scheduleItem = new DirectDebitScheduleItem(
						null, // create date
						// give by
						// direct debit
						// syste,
						scheduledDate, scheduledAmount, itemStatus, null,
						feeAmount, receiptNumber, scheduleSequence, null, "",
						"", "", null, "", null, "", "", null);
				LogUtil.debug(this.getClass(), "scheduleItem=" + itemList);
				itemList.add(scheduleItem);
				// scheduleItem.write(System.out);
			}
			resultSet.close();

			LogUtil.debug(this.getClass(), "constructing new schedule"
					+ itemList);

			newSchedule = new DirectDebitSchedule(ddAuthority, startDate,
					endDate, ddSchedule.getAmortisedAmount(),
					ddSchedule.getImmediateAmount(), null,
					ddAuthority.getClientNumber(), ddSchedule.getUserID(),
					ddSchedule.getSalesBranchCode(),
					ddSchedule.getSourceSystem(), ddSchedule.getProductCode(),
					itemList, ddSchedule.getScheduleDescription());

		} catch (Open4GLException gle) {
			handleProxyReturnString(ddProxy);
			throw new SystemException(gle);
		} catch (SQLException sqle) {
			throw new SystemException(sqle);
		} catch (ValidationException ve) {
			// Since we trust the DD system to have correct data then a
			// ValidationException
			// thrown by the DirectDebitSchedule constructor can be considered a
			// system exception.
			throw new SystemException(ve);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		LogUtil.debug(this.getClass(), "newsSchedule" + newSchedule.toString());
		return newSchedule;
	}

	/**
	 * Commit any pending transactions in the direct debit sub system. The
	 * connection to the direct debit sub-system will also be released.
	 * 
	 * @exception SystemException
	 *                Description of the Exception
	 */
	public void commit() throws SystemException {
		LogUtil.debug(this.getClass(), "committing direct debit adapter");
		if (this.progressDDTransactionProxy != null) {
			try {
				this.progressDDTransactionProxy.commit();
			} catch (Open4GLException gle) {
				throw new SystemException(gle);
			} finally {
				release();
			}
		}
	}

	/**
	 * Rollback any pending transactions in the direct debit sub system. The
	 * connection to the direct debit sub-system will also be released.
	 * 
	 * @exception SystemException
	 *                Description of the Exception
	 */
	public void rollback() throws SystemException {
		LogUtil.debug(this.getClass(), "rollback()");
		try {
			if (this.progressDDTransactionProxy != null) {
				this.progressDDTransactionProxy.rollback();
			}

		} catch (Open4GLException gle) {
			throw new SystemException(gle);
		} finally {
			release();
		}
	}

	/**
	 * Release any connections held to the direct debit sub-system. If a
	 * transaction is pending and not committed then the transaction will be
	 * rolled back.
	 */
	public synchronized void release() {
		LogUtil.debug(this.getClass(), "release()");
		releaseTransactionProxy();
	}

	/**
	 * Gets the dDProxy attribute of the ProgressDDAdapter object
	 * 
	 * @return The dDProxy value
	 * @exception SystemException
	 *                Description of the Exception
	 */
	private DDProgressProxy getDDProgressProxy() throws SystemException {
		DDProgressProxy progressDDProxy = null;

		try {
			progressDDProxy = (DDProgressProxy) ProgressProxyObjectPool
					.getInstance().borrowObject(
							ProgressProxyObjectFactory.KEY_DD_PROXY);
		} catch (Exception ex) {
			throw new SystemException(ex);
		}

		return progressDDProxy;
	}

	/**
	 * Gets the dDTransactionProxy attribute of the ProgressDDAdapter object AND
	 * sets the global variable
	 * 
	 * @return The dDTransactionProxy value
	 * @exception SystemException
	 *                Description of the Exception
	 */
	private ProgressDDTransactionProxy getDDTransactionProxy()
			throws SystemException {
		if (this.progressDDTransactionProxy == null) {
			progressProxy = getDDProgressProxy();
			try {
				this.progressDDTransactionProxy = progressProxy
						.createPO_ProgressDDTransactionProxy();
			} catch (Open4GLException o4gle) {
				throw new SystemException(o4gle);
			}
		}
		return this.progressDDTransactionProxy;
	}

	/**
	 * Retrieves a map of failed DD schedules containing: - clientNo - opSeqNo -
	 * amt - payableSeq
	 */
	public List<Map<String, Object>> getFailedDDSchedules(Integer clientNo,
			String sourceSystem) throws SystemException {
		DDProgressProxy ddProxy = getDDProgressProxy();
		ResultSetHolder ddRsh = new ResultSetHolder();
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		try {
			ddProxy.getFailedMemberships(clientNo, sourceSystem, ddRsh);

			if (ddRsh != null) {
				ResultSet ddResultSet = ddRsh.getResultSetValue();

				while (ddResultSet.next()) {
					Map<String, Object> record = new HashMap<String, Object>();
					record.put("clientNo", ddResultSet.getInt(1));
					record.put("oiPayableSeq", ddResultSet.getInt(2));
					record.put("amt", ddResultSet.getDouble(3));
					record.put("payableSeq", ddResultSet.getInt(4));
					record.put("ddPayableSeq", ddResultSet.getInt(5));

					results.add(record);
				}
			}

		} catch (Exception e) {
			throw new SystemException(e);
		}

		return results;
	}

	public Hashtable<Integer, String> getOrphanedDDSchedules(String sourceSystem)
			throws SystemException {
		LogUtil.debug(this.getClass(), "sourceSystem=" + sourceSystem);
		Hashtable<Integer, String> orphanedDDSchedules = new Hashtable<Integer, String>();
		DDProgressProxy ddProxy = getDDProgressProxy();
		ResultSetHolder ddRsh = new ResultSetHolder();
		try {
			ddProxy.listLiveDD(sourceSystem, ddRsh);
			PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
			if (ddRsh != null) {
				ResultSet ddResultSet = ddRsh.getResultSetValue();
				int ddPayableSeq = 0;
				int clientNumber = 0;
				String error = "";
				Collection payableItems = null;
				int counter = 0;
				while (ddResultSet.next()) {
					counter++;
					LogUtil.debug(this.getClass(), "counter=" + counter);
					ddPayableSeq = ddResultSet.getInt(1);
					clientNumber = ddResultSet.getInt(2);
					LogUtil.debug(this.getClass(), "ddPayableSeq=" + ddPayableSeq);
					try {
						// reinitialise
						error = "";
						payableItems = paymentMgr.getPayableItems(PaymentMethod.DIRECT_DEBIT, String.valueOf(ddPayableSeq));
					} catch (Exception e) {
						error = e.getMessage();
						LogUtil.warn(this.getClass(), e);
					}
					LogUtil.debug(this.getClass(), "payableItems="
							+ payableItems);
					if (payableItems.size() == 0) {
						orphanedDDSchedules.put(ddPayableSeq, clientNumber + error);
					}
				}
			}
		} catch (Exception e) {
			throw new SystemException(e);
		}
		return orphanedDDSchedules;
	}

	/**
	 * Description of the Method
	 * 
	 * @exception SystemException
	 *                Description of the Exception
	 */
	private void handleProxyReturnString(DDProgressProxy progressDDProxy)
			throws SystemException {
		if (progressDDProxy != null) {
			try {
				String returnString = progressDDProxy._getProcReturnString();
				if (returnString != null && returnString.length() > 0) {
					throw new SystemException(returnString);
				}
			} catch (Open4GLException gle) {
				throw new SystemException(gle);
			}
		}
	}

	/**
	 * Description of the Method
	 * 
	 * @exception SystemException
	 *                Description of the Exception
	 */
	private void handleTransactionProxyReturnString() throws RollBackException {
		if (this.progressDDTransactionProxy != null) {
			try {
				String returnString = this.progressDDTransactionProxy
						._getProcReturnString();
				if (returnString != null && returnString.length() > 0) {
					throw new RollBackException(new ValidationException(
							returnString));
				}
			} catch (Open4GLException gle) {
				throw new RollBackException(gle);
			}
		}
	}

	/**
	 * Description of the Method
	 */
	private void releaseDDProgressProxy(DDProgressProxy progressDDProxy) {

		try {
			ProgressProxyObjectPool.getInstance().returnObject(
					ProgressProxyObjectFactory.KEY_DD_PROXY, progressDDProxy);
		} catch (Exception ex) {
			LogUtil.fatal(this.getClass(), ex);
		} finally {
			progressDDProxy = null;
		}
	}

	/**
	 * Release the transaction proxy
	 */
	private void releaseTransactionProxy() {
		if (this.progressDDTransactionProxy != null) {
			try {

				this.progressDDTransactionProxy._release();

			} catch (Open4GLException gle) {
				LogUtil.fatal(this.getClass(), gle.getMessage());
			} finally {
				this.progressDDTransactionProxy = null;
			}
		}
		releaseDDProgressProxy(this.progressProxy);
		this.progressProxy = null;
	}

	/**
	 * Map the status string returned by the direct debit system to a
	 * DirectDebitItemStatus
	 * 
	 * @param statusString
	 *            the status returned by the direct debit system
	 * @return the DirectDebitItemStatus corresponding to the status string
	 * @exception SystemException
	 *                Description of the Exception
	 * @throws DirectDebitException
	 *             if the statusString does not correspond to any
	 *             DirectDebitItemStatus@throws
	 *             Payment_lv.directdebit.DirectDebitException
	 */
	private DirectDebitItemStatus mapStatusString(String statusString) {
		DirectDebitItemStatus status = null;
		if (statusString.startsWith(DD_STATUS_CANCEL_CODE)) {
			status = DirectDebitItemStatus.CANCELLED;
		} else if (statusString.startsWith(DD_STATUS_DISHONOURED)) {
			status = DirectDebitItemStatus.DISHONOURED;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_FAILURE)) {
			status = DirectDebitItemStatus.FAILURE;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_SCHEDULED)) {
			status = DirectDebitItemStatus.SCHEDULED;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_PAID)) {
			status = DirectDebitItemStatus.PAID;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_PAID_OCR)) {
			status = DirectDebitItemStatus.PAID;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_RENEWAL)) {
			status = DirectDebitItemStatus.RENEWAL;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_WAITING)) {
			status = DirectDebitItemStatus.WAITING;
		} else if (statusString
				.equalsIgnoreCase(DD_STATUS_CANCELLED_DISHONOURED)) {
			status = DirectDebitItemStatus.CANCELLED_DISHONOURED;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_REPLACED)) {
			status = DirectDebitItemStatus.REPLACED;
		} else if (statusString.equalsIgnoreCase(DD_STATUS_PAID_DISHONOURED)) {
			status = DirectDebitItemStatus.PAID;
		} else {
			// set an unknown status rather than crashing it
			status = DirectDebitItemStatus.UNKNOWN;
		}
		return status;
	}

	/**
	 * Map frequency to the string required for the frequency in the direct
	 * debit system
	 * 
	 * @param ddFrequency
	 * @return the string required for the frequency in the dd system
	 * @exception SystemException
	 *                Description of the Exception
	 * @throws DirectDebitException
	 *             if the the frequency is not known in the direct debit
	 *             system@throws Payment_lv.directdebit.DirectDebitException
	 */
	private String mapFrequency(DirectDebitFrequency ddFrequency)
			throws SystemException {
		if (ddFrequency.equals(DirectDebitFrequency.ANNUALLY)) {
			return DD_FREQUENCY_ANNUALLY;
		} else if (ddFrequency.equals(DirectDebitFrequency.HALF_YEARLY)) {
			return DD_FREQUENCY_HALF_YEARLY;
		} else if (ddFrequency.equals(DirectDebitFrequency.QUARTERLY)) {
			return DD_FREQUENCY_QUARTERLY;
		} else if (ddFrequency.equals(DirectDebitFrequency.MONTHLY)) {
			return DD_FREQUENCY_MONTHLY;
		} else if (ddFrequency.equals(DirectDebitFrequency.FORTNIGHTLY)) {
			return DD_FREQUENCY_FORTNIGHTLY;
		} else {
			throw new SystemException("Unknown frequency '"
					+ ddFrequency.toString()
					+ "' in progress direct debit adapter");
		}
	}

	/**
	 * Map frequency from string from the dd system to the frequency in the
	 * payments system
	 * 
	 * @param frequency
	 *            the frequency (in the payment system)
	 * @return the string required for the frequency in the dd system
	 * @exception SystemException
	 *                Description of the Exception
	 * @throws DirectDebitException
	 *             if the the frequency is not known in the direct debit
	 *             system@throws Payment_lv.directdebit.DirectDebitException
	 */
	private DirectDebitFrequency mapFrequency(String frequency)
			throws SystemException {
		if (frequency.equalsIgnoreCase(DD_FREQUENCY_ANNUALLY)) {
			return DirectDebitFrequency.ANNUALLY;
		} else if (frequency.equalsIgnoreCase(DD_FREQUENCY_HALF_YEARLY)) {
			return DirectDebitFrequency.HALF_YEARLY;
		} else if (frequency.equalsIgnoreCase(DD_FREQUENCY_QUARTERLY)) {
			return DirectDebitFrequency.QUARTERLY;
		} else if (frequency.equalsIgnoreCase(DD_FREQUENCY_MONTHLY)) {
			return DirectDebitFrequency.MONTHLY;
		} else if (frequency.equalsIgnoreCase(DD_FREQUENCY_FORTNIGHTLY)) {
			return DirectDebitFrequency.FORTNIGHTLY;
		} else {
			throw new SystemException("Unknown frequency '" + frequency
					+ "'from progress direct debit system");
		}
	}

	public void markScheduleItemAsPaid(Integer ddPayableSeq,
			Integer ddSchedSeq, BigDecimal amount, Integer receiptNo,
			String logonId) throws RollBackException {
		LogUtil.debug(this.getClass(), "markScheduleItemAsPaid");
		LogUtil.debug(this.getClass(), "ddPayableSeq=" + ddPayableSeq);
		LogUtil.debug(this.getClass(), "ddSchedSeq=" + ddSchedSeq);
		LogUtil.debug(this.getClass(), "amount=" + amount);
		LogUtil.debug(this.getClass(), "receiptNo=" + receiptNo);
		LogUtil.debug(this.getClass(), "logonId=" + logonId);

		try {
			ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();
			transactionProxy.markScheduleAsPaid(ddPayableSeq.intValue(),
					ddSchedSeq.intValue(), amount, receiptNo.intValue(),
					logonId);
		} catch (Open4GLException gle) {
			handleTransactionProxyReturnString();
			throw new RollBackException(
					"Unable to mark the direct debit scheduled item as paid.",
					gle);
		} catch (SystemException se) {
			throw new RollBackException(se);
		}
	}

	/**
	 * Returns a vector of DirectDebitScheduleItem object which are payable,
	 * given a client number
	 * 
	 * @param clientNo
	 * @return
	 * @throws SystemException
	 */
	public Vector getPayableScheduledItemsForClient(Integer clientNo)
			throws SystemException {
		LogUtil.log(this.getClass(), "getPayableScheduledItemsForClient="
				+ clientNo);
		Vector ddScheduleItemList = new Vector();
		DirectDebitSchedule ddSchedule = null;
		DirectDebitScheduleItem ddScheduleItem = null;

		// payable schedule items only.
		Vector tempList = this.getScheduleListByClientNumber(clientNo, true);

		LogUtil.debug(this.getClass(), "schedule list size = "
				+ (tempList == null ? 0 : tempList.size()));
		ArrayList tempItemList = null;
		DirectDebitItemStatus status = null;
		for (int x = 0; x < tempList.size(); x++) {
			ddSchedule = (DirectDebitSchedule) tempList.get(x);
			LogUtil.debug(this.getClass(),
					"ddSchedule=" + ddSchedule.toString());
			tempItemList = (ArrayList) ddSchedule.getScheduleItemList();
			// if invalid schedule then there will be no scheduled items
			// attached.
			if (tempItemList != null) {
				for (int y = 0; y < tempItemList.size(); y++) {
					ddScheduleItem = (DirectDebitScheduleItem) tempItemList
							.get(y);
					LogUtil.debug(this.getClass(),
							"item=" + ddScheduleItem.toString());
					LogUtil.debug(this.getClass(), y + ": item status="
							+ status);
					if (ddScheduleItem.isPayable()) // add if payable
					{
						ddScheduleItemList.add(ddScheduleItem);
					}
				}
			}
		}
		LogUtil.debug(this.getClass(), "ddScheduleItemList size="
				+ ddScheduleItemList.size());
		return ddScheduleItemList;
	}

	public DDMaster getDDMaster(Integer ddSequenceNo) throws SystemException {
		DDMaster ddMaster = new DDMaster();
		DDProgressProxy ddProxy = getDDProgressProxy();
		StringHolder hbsbNumber = new StringHolder();
		StringHolder hbankAccount = new StringHolder();
		StringHolder hdrawer = new StringHolder();
		StringHolder hmadeId = new StringHolder();
		DateHolder hmadeDate = new DateHolder();
		StringHolder hmadeSalesBranch = new StringHolder();
		DateHolder hchangeDate = new DateHolder();
		StringHolder hchangeId = new StringHolder();
		StringHolder hchangeSlsbch = new StringHolder();
		IntHolder hchangeTime = new IntHolder();
		BooleanHolder hcreditCard = new BooleanHolder();
		StringHolder hccExpiryDate = new StringHolder();
		StringHolder holdDrawers1 = new StringHolder();
		StringHolder holdDrawers2 = new StringHolder();
		StringHolder holdDrawers3 = new StringHolder();
		StringHolder holdDrawers4 = new StringHolder();
		StringHolder holdDrawers5 = new StringHolder();
		StringHolder holdDrawers6 = new StringHolder();
		StringHolder holdDrawers7 = new StringHolder();
		StringHolder holdDrawers8 = new StringHolder();
		StringHolder holdDrawers9 = new StringHolder();
		StringHolder holdDrawers10 = new StringHolder();
		ArrayList oldDrawers = new ArrayList();

		try {
			ddProxy.getDDMaster(ddSequenceNo.intValue(), hbsbNumber,
					hbankAccount, hdrawer, hmadeId, hmadeDate,
					hmadeSalesBranch, hchangeDate, hchangeId, hchangeSlsbch,
					hchangeTime, hcreditCard, hccExpiryDate, holdDrawers1,
					holdDrawers2, holdDrawers3, holdDrawers4, holdDrawers5,
					holdDrawers6, holdDrawers7, holdDrawers8, holdDrawers9,
					holdDrawers10);
			ddMaster.setDdSeqNo(ddSequenceNo);
			ddMaster.setBankAccount(hbankAccount.getStringValue());
			ddMaster.setBsbNumber(hbsbNumber.getStringValue());
			ddMaster.setDrawer(hdrawer.getStringValue());
			ddMaster.setMadeId(hmadeId.getStringValue());
			GregorianCalendar gc = hmadeDate.getDateValue();
			if (gc != null)
				ddMaster.setMadeDate(new DateTime(gc.getTime()));
			ddMaster.setMadeSlsBch(hmadeSalesBranch.getStringValue());
			ddMaster.setChangeId(hchangeId.getStringValue());
			ddMaster.setChangeSlsBch(hchangeSlsbch.getStringValue());
			ddMaster.setChangeDate(convertDateAndTime(hchangeDate, hchangeTime));
			ddMaster.setCreditCard(hcreditCard.getBooleanValue());
			ddMaster.setCcExpiryDate(hccExpiryDate.getStringValue());
			oldDrawers.add(holdDrawers1.getStringValue());
			oldDrawers.add(holdDrawers2.getStringValue());
			oldDrawers.add(holdDrawers3.getStringValue());
			oldDrawers.add(holdDrawers4.getStringValue());
			oldDrawers.add(holdDrawers5.getStringValue());
			oldDrawers.add(holdDrawers6.getStringValue());
			oldDrawers.add(holdDrawers7.getStringValue());
			oldDrawers.add(holdDrawers8.getStringValue());
			oldDrawers.add(holdDrawers9.getStringValue());
			oldDrawers.add(holdDrawers10.getStringValue());
			ddMaster.setOldDrawers(oldDrawers);
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return ddMaster;
	}

	public ArrayList getDDItems(Integer systemNo, String ddType)
			throws SystemException {
		ArrayList itemList = new ArrayList();
		DDProgressProxy ddProxy = getDDProgressProxy();
		ResultSetHolder rsh = new ResultSetHolder();
		ResultSet rs = null;
		DDItem ddItem = null;
		java.sql.Date tDate = null;
		int count = 0;
		try {
			ddProxy.getDDItems(systemNo.intValue(), ddType, rsh);
			rs = rsh.getResultSetValue();
			while (rs.next()) {
				ddItem = new DDItem();
				ddItem.setClientNo(new Integer(rs.getInt(1)));
				ddItem.setDdSeqNo(new Integer(rs.getInt(2)));
				ddItem.setDdItemNo(new Integer(rs.getInt(3)));
				ddItem.setDdItemSeq(new Integer(rs.getInt(4)));
				ddItem.setDdType(rs.getString(5));
				ddItem.setSystemNo(new Integer(rs.getInt(6)));
				tDate = rs.getDate(7);
				if (tDate != null)
					ddItem.setMadeDate(new DateTime(tDate));
				ddItem.setMadeId(rs.getString(8));
				ddItem.setMadeSlsBch(rs.getString(9));
				ddItem.setOwner(rs.getBoolean(10));
				ddItem.setFrequency(rs.getString(11));
				ddItem.setCurrentVer(rs.getBoolean(12));
				ddItem.setPaFlag(rs.getBoolean(13));
				tDate = rs.getDate(14);
				if (tDate != null)
					ddItem.setPdcDate(new DateTime(tDate));
				ddItem.setDdDay(new Integer(rs.getInt(15)));
				ddItem.setHasPayables(rs.getBoolean(16));
				itemList.add(ddItem);
			}
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}

		return itemList;
	}

	public ArrayList getDDPayables(Integer ddItemNo, Integer ddItemSeq)
			throws SystemException {
		ArrayList pList = new ArrayList();
		DDProgressProxy ddProxy = getDDProgressProxy();
		ResultSetHolder rsh = new ResultSetHolder();
		ResultSet rs = null;
		DDPayable ddp = null;
		java.sql.Date tDate = null;
		int count = 0;
		Object obj = null;
		try {
			ddProxy.getDDPayables(ddItemNo.intValue(), ddItemSeq.intValue(),
					rsh);
			rs = rsh.getResultSetValue();
			while (rs.next()) {
				ddp = new DDPayable();
				ddp.setDdItemNo(ddItemNo);
				ddp.setDdItemSeq(ddItemSeq);
				ddp.setDdPayableSeq(new Integer(rs.getInt(3)));
				ddp.setClientNo(new Integer(rs.getInt(4)));
				ddp.setOpSeqNo(new Integer(rs.getInt(5)));
				ddp.setOpSubSeq(new Integer(rs.getInt(6)));
				ddp.setDdStatus(rs.getString(7));
				tDate = rs.getDate(8);
				if (tDate != null)
					ddp.setStartDate(new DateTime(tDate));
				tDate = rs.getDate(9);
				if (tDate != null)
					ddp.setEndDate(new DateTime(tDate));
				obj = rs.getObject(10);
				if (obj != null)
					ddp.setCanSubSeq(new Integer(obj.toString()));
				obj = rs.getObject(11);
				if (obj != null)
					ddp.setPayableItemNo(new Integer(obj.toString()));
				obj = rs.getObject(12);
				if (obj != null)
					ddp.setImmediateAmount(new BigDecimal(obj.toString()));
				obj = rs.getObject(13);
				if (obj != null)
					ddp.setAmortisedAmount(new BigDecimal(obj.toString()));
				tDate = rs.getDate(14);
				if (tDate != null)
					ddp.setCreateDate(new DateTime(tDate));
				ddp.setCreateId(rs.getString(15));
				tDate = rs.getDate(16);
				if (tDate != null)
					ddp.setCancDate(new DateTime(tDate));
				ddp.setCancId(rs.getString(17));
				ddp.setDescription(rs.getString(18));
				ddp.setHasPayables(rs.getBoolean(19));
				count++;
				pList.add(ddp);
			}
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return pList;
	}

	public ArrayList getDDSchedules(Integer ddPayableSeq)
			throws SystemException {
		ArrayList sList = new ArrayList();
		DDProgressProxy ddProxy = getDDProgressProxy();
		ResultSetHolder rsh = new ResultSetHolder();
		ResultSet rs = null;
		DDSched dds = null;
		java.sql.Date tDate = null;
		int count = 0;
		Object obj = null;
		try {
			ddProxy.getDDSchedules(ddPayableSeq.intValue(), rsh);
			rs = rsh.getResultSetValue();
			while (rs.next()) {
				dds = new DDSched();
				dds.setDdPayableSeq(ddPayableSeq);
				dds.setDdSchedSeq(new Integer(rs.getInt(2)));
				dds.setAmount(new BigDecimal(rs.getDouble(3)));
				dds.setFee(new BigDecimal(rs.getDouble(4)));
				tDate = rs.getDate(5);
				if (tDate != null)
					dds.setCreateDate(new DateTime(tDate));
				dds.setCreateId(rs.getString(6));
				dds.setCreateSalesBranch(rs.getString(7));
				tDate = rs.getDate(8);
				if (tDate != null)
					dds.setSchedDate(new DateTime(tDate));
				tDate = rs.getDate(9);
				if (tDate != null)
					dds.setProcessDate(new DateTime(tDate));
				obj = rs.getObject(10);
				if (obj != null)
					dds.setReceiptNo(new Integer(obj.toString()));
				dds.setTranNo(new Integer(rs.getInt(11)));
				dds.setTranStatus(rs.getString(12));
				dds.setAction(rs.getString(13));
				dds.setReplaceNo(new Integer(rs.getInt(14)));
				dds.setReturnCode(rs.getString(15));
				tDate = rs.getDate(17);
				if (tDate != null)
					dds.setReturnDate(new DateTime(tDate));

				count++;
				sList.add(dds);
			}
		} catch (Exception ex) {
			throw new SystemException(ex);
		}
		return sList;
	}

	private DateTime convertDateAndTime(DateHolder dh, IntHolder th) {
		DateTime dt = null;
		if (!dh.isNull() && !th.isNull()) {
			GregorianCalendar gc = DateUtil.clearTime(dh.getDateValue());
			dt = new DateTime(gc.getTime().getTime() + th.getIntValue());
		}
		return dt;
	}

	private DateTime convertDate(DateHolder dh) {
		DateTime dt = null;
		GregorianCalendar gc = null;
		gc = dh.getDateValue();
		if (gc == null)
			return null;
		else {
			dt = new DateTime(dh.getDateValue().getTime());
		}
		return dt;
	}

	private Integer convertIntHolder(IntHolder ih) {
		Integer in = null;
		if (ih == null || ih.isNull())
			return null;
		else {
			in = new Integer(ih.getIntValue());
		}
		return in;
	}

	public DDItem getDDItem(Integer ddItemNo, Integer ddItemSeq)
			throws SystemException {
		DDItem ddItem = null;
		DDProgressProxy ddProxy = getDDProgressProxy();
		IntHolder clientNo = new IntHolder();
		IntHolder ddDay = new IntHolder();
		StringHolder freq = new StringHolder();
		IntHolder ddSeqNo = new IntHolder();
		StringHolder ddType = new StringHolder();
		IntHolder systemNo = new IntHolder();
		DateHolder madeDate = new DateHolder();
		StringHolder madeId = new StringHolder();
		StringHolder madeSlsbch = new StringHolder();
		BooleanHolder owner = new BooleanHolder();
		BooleanHolder paFlag = new BooleanHolder();
		DateHolder pdcDate = new DateHolder();
		BooleanHolder currentVer = new BooleanHolder();
		BooleanHolder hasPayables = new BooleanHolder();

		try {
			ddProxy.getDDItem(ddItemNo.intValue(), ddItemSeq.intValue(),
					currentVer, clientNo, ddDay, freq, ddSeqNo, ddType,
					systemNo, madeDate, madeId, madeSlsbch, owner, paFlag,
					pdcDate, hasPayables);

			ddItem = new DDItem();
			ddItem.setClientNo(new Integer(clientNo.getIntValue()));
			ddItem.setCurrentVer(currentVer.getBooleanValue());
			ddItem.setDdDay(new Integer(ddDay.getIntValue()));
			ddItem.setDdItemNo(ddItemNo);
			ddItem.setDdItemSeq(ddItemSeq);
			ddItem.setDdSeqNo(new Integer(ddSeqNo.getIntValue()));
			ddItem.setDdType(ddType.getStringValue());
			ddItem.setFrequency(freq.getStringValue());
			ddItem.setMadeDate(new DateTime(madeDate.getDateValue().getTime()));
			ddItem.setHasPayables(hasPayables.getBooleanValue());
			ddItem.setMadeId(madeId.getStringValue());
			ddItem.setMadeSlsBch(madeSlsbch.getStringValue());
			ddItem.setOwner(owner.getBooleanValue());
			ddItem.setPaFlag(paFlag.getBooleanValue());
			ddItem.setPdcDate(new DateTime(pdcDate.getDateValue().getTime()));
			ddItem.setSystemNo(new Integer(systemNo.getIntValue()));
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return ddItem;
	}

	public DDPayable getDDPayable(Integer ddPayableSeq) throws SystemException {
		DDPayable ddp = null;
		DDProgressProxy ddProxy = getDDProgressProxy();
		IntHolder ddItemNo = new IntHolder();
		IntHolder ddItemSeq = new IntHolder();
		IntHolder clientNo = new IntHolder();
		IntHolder opSeqNo = new IntHolder();
		IntHolder opSubSeq = new IntHolder();
		StringHolder ddStatus = new StringHolder();
		DateHolder startDate = new DateHolder();
		DateHolder endDate = new DateHolder();
		IntHolder cancSubSeq = new IntHolder();
		IntHolder payableItemNo = new IntHolder();
		BigDecimalHolder iAmount = new BigDecimalHolder();
		BigDecimalHolder aAmount = new BigDecimalHolder();
		DateHolder createDate = new DateHolder();
		StringHolder createId = new StringHolder();
		DateHolder cancDate = new DateHolder();
		StringHolder cancId = new StringHolder();
		StringHolder descr = new StringHolder();
		BooleanHolder hasSchedules = new BooleanHolder();

		try {
			ddProxy.getDDPayable(ddPayableSeq.intValue(), ddItemNo, ddItemSeq,
					clientNo, opSeqNo, opSubSeq, ddStatus, startDate, endDate,
					cancSubSeq, payableItemNo, iAmount, aAmount, createDate,
					createId, cancDate, cancId, descr, hasSchedules);

			ddp = new DDPayable();
			ddp.setAmortisedAmount(aAmount.getBigDecimalValue());
			ddp.setImmediateAmount(iAmount.getBigDecimalValue());
			ddp.setCancDate(convertDate(cancDate));
			ddp.setCancId(cancId.getStringValue());
			ddp.setCanSubSeq(this.convertIntHolder(cancSubSeq));
			ddp.setClientNo(convertIntHolder(clientNo));
			ddp.setCreateDate(this.convertDate(createDate));
			ddp.setCreateId(createId.getStringValue());
			ddp.setDdItemNo(convertIntHolder(ddItemNo));
			ddp.setDdItemSeq(convertIntHolder(ddItemSeq));
			ddp.setDdPayableSeq(ddPayableSeq);
			ddp.setDdStatus(ddStatus.getStringValue());
			ddp.setDescription(descr.getStringValue());
			ddp.setEndDate(convertDate(endDate));
			ddp.setStartDate(convertDate(startDate));
			ddp.setHasPayables(hasSchedules.getBooleanValue());
			ddp.setOpSeqNo(convertIntHolder(opSeqNo));
			ddp.setOpSubSeq(convertIntHolder(opSubSeq));
			ddp.setPayableItemNo(convertIntHolder(payableItemNo));
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return ddp;
	}

	public Collection getPoliciesOnAccount(Integer ddAuthorityId)
			throws SystemException {
		LogUtil.log(this.getClass(), "getPoliciesOnAccount 1");
		LogUtil.log(this.getClass(), "ddAuthorityId=" + ddAuthorityId);

		Collection policyList = new ArrayList();
		DDProgressProxy ddProxy = getDDProgressProxy();
		StringHolder polList = new StringHolder();
		StringHolder memList = new StringHolder();
		try {

			ddProxy.otherDdForThisClient(ddAuthorityId, polList, memList);
			LogUtil.log(this.getClass(), "polList=" + polList);
			if (polList != null) {
				LogUtil.log(this.getClass(), "polList not null");
				String[] policies = polList.getStringValue().split(
						FileUtil.SEPARATOR_COMMA, -1); // split
				// line
				// by
				// comma
				for (int i = 0; i < policies.length; i++) {
					String tmp = policies[i];
					if (tmp.trim().length() > 0) {
						policyList.add(tmp);
					}
				}
				LogUtil.log(this.getClass(), "policyList=" + policyList);
			}
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		LogUtil.log(this.getClass(), "getPoliciesOnAccount 2");
		return policyList;
	}

	public DDSched getDDSched(Integer ddPayableSeq, Integer ddSchedSeq)
			throws SystemException {
		DDSched dds = null;
		DDProgressProxy ddProxy = getDDProgressProxy();
		BigDecimalHolder amount = new BigDecimalHolder();
		BigDecimalHolder fee = new BigDecimalHolder();
		DateHolder createDate = new DateHolder();
		StringHolder createId = new StringHolder();
		StringHolder createSlsbch = new StringHolder();
		DateHolder schedDate = new DateHolder();
		DateHolder processDate = new DateHolder();
		IntHolder receiptNo = new IntHolder();
		IntHolder tranNo = new IntHolder();
		StringHolder tranStatus = new StringHolder();
		StringHolder action = new StringHolder();
		IntHolder replaceNo = new IntHolder();
		StringHolder retCode = new StringHolder();
		StringHolder authNo = new StringHolder();
		DateHolder returnDate = new DateHolder();

		try {
			ddProxy.getDDSched(ddPayableSeq.intValue(), ddSchedSeq.intValue(),
					amount, fee, createDate, createId, createSlsbch, schedDate,
					processDate, receiptNo, tranNo, tranStatus, action,
					replaceNo, retCode, authNo, returnDate);

			dds = new DDSched();
			dds.setAction(action.getStringValue());
			dds.setAmount(amount.getBigDecimalValue());
			dds.setFee(fee.getBigDecimalValue());
			dds.setCreateDate(convertDate(createDate));
			dds.setCreateId(createId.getStringValue());
			dds.setCreateSalesBranch(createSlsbch.getStringValue());
			dds.setDdPayableSeq(ddPayableSeq);
			dds.setDdSchedSeq(ddSchedSeq);
			dds.setProcessDate(convertDate(processDate));
			dds.setReceiptNo(convertIntHolder(receiptNo));
			dds.setReplaceNo(convertIntHolder(replaceNo));
			dds.setReturnCode(retCode.getStringValue());
			dds.setReturnDate(convertDate(returnDate));
			dds.setSchedDate(convertDate(schedDate));
			dds.setTranNo(convertIntHolder(tranNo));
			dds.setTranStatus(tranStatus.getStringValue());

		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		return dds;
	}

	public DDMaster getDDMaster(Integer ddItemNo, Integer ddItemSeq)
			throws SystemException {
		DDItem ddItem = this.getDDItem(ddItemNo, ddItemSeq);
		DDMaster ddm = this.getDDMaster(ddItem.getDdSeqNo());
		return ddm;
	}

	public ArrayList getDDMemos(Integer id, String type) throws SystemException {
		ArrayList itemList = new ArrayList();
		DDProgressProxy ddProxy = getDDProgressProxy();
		ResultSetHolder memoRsH = new ResultSetHolder();
		DDMemo ddMemo = null;
		java.sql.Date tDate = null;
		try {
			ddProxy.getDDMemos(id.intValue(), type, memoRsH);
			ResultSet rs = memoRsH.getResultSetValue();
			while (rs.next()) {
				ddMemo = new DDMemo();
				ddMemo.setDdItemNo(new Integer(rs.getInt(1)));
				ddMemo.setMemoSeq(new Integer(rs.getInt(2)));
				ddMemo.setDdPayableSeq(new Integer(rs.getInt(3)));
				ddMemo.setMemoText(rs.getString(4));
				ddMemo.setCreateId(rs.getString(5));
				tDate = rs.getDate(6);
				if (tDate != null)
					ddMemo.setCreateDate(new DateTime(tDate));
				itemList.add(ddMemo);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}

		return itemList;

	}

	public DDMemo getDDMemo(Integer ddItemNo, Integer memoSeq)
			throws SystemException {
		DDMemo ddMemo = null;
		DDProgressProxy ddProxy = getDDProgressProxy();
		IntHolder ddPayableHolder = new IntHolder();
		StringHolder createIdHolder = new StringHolder();
		DateHolder createDateHolder = new DateHolder();
		StringHolder memoTextHolder = new StringHolder();
		try {
			ddProxy.getDDMemo(ddItemNo.intValue(), memoSeq.intValue(),
					ddPayableHolder, createIdHolder, createDateHolder,
					memoTextHolder);
			ddMemo = new DDMemo();
			ddMemo.setDdItemNo(ddItemNo);
			ddMemo.setMemoSeq(memoSeq);
			ddMemo.setDdPayableSeq(new Integer(ddPayableHolder.getIntValue()));
			ddMemo.setCreateId(createIdHolder.getStringValue());
			ddMemo.setCreateDate(convertDate(createDateHolder));
			ddMemo.setMemoText(memoTextHolder.getStringValue());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SystemException(ex);
		} finally {
			this.releaseDDProgressProxy(ddProxy);
		}
		// LogUtil.log(this.getClass(),ddMemo.getMemoText());
		return ddMemo;
	}

	public void clearFailedStatus(Integer clientNo, Integer seqNo,
			BigDecimal amount) throws Exception {
		ProgressDDTransactionProxy transactionProxy = getDDTransactionProxy();

		BooleanHolder okHolder = new BooleanHolder();
		try {
			transactionProxy.clearFailedStatus(clientNo.intValue(),
					seqNo.intValue(), amount, okHolder);
			if (!okHolder.getBooleanValue()) {
				throw new Exception(
						"Unspecified error in ProgressDDAdapter.clearFailedStatus");
			}
		} catch (Exception ex) {
			throw ex;
		} finally {

		}
	}

}
