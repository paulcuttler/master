package com.ract.payment.directdebit;

public class DDSched {
    private String action;
    private java.math.BigDecimal amount;
    private java.math.BigDecimal fee;
    private com.ract.util.DateTime createDate;
    private String createId;
    private String createSalesBranch;
    private Integer ddPayableSeq;
    private Integer ddSchedSeq;
    private com.ract.util.DateTime processDate;
    private Integer receiptNo;
    private Integer replaceNo;
    private String returnCode;
    private com.ract.util.DateTime schedDate;
    private Integer tranNo;
    private String tranStatus;
    private com.ract.util.DateTime returnDate;

    public DDSched() {
    }

    public String getAction() {
	return action;
    }

    public void setAction(String action) {
	this.action = action;
    }

    public java.math.BigDecimal getAmount() {
	return amount;
    }

    public void setAmount(java.math.BigDecimal amount) {
	this.amount = amount;
    }

    public java.math.BigDecimal getFee() {
	return fee;
    }

    public void setFee(java.math.BigDecimal fee) {
	this.fee = fee;
    }

    public com.ract.util.DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(com.ract.util.DateTime createDate) {
	this.createDate = createDate;
    }

    public String getCreateId() {
	return createId;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public String getCreateSalesBranch() {
	return createSalesBranch;
    }

    public void setCreateSalesBranch(String createSalesBranch) {
	this.createSalesBranch = createSalesBranch;
    }

    public Integer getDdPayableSeq() {
	return ddPayableSeq;
    }

    public void setDdPayableSeq(Integer ddPayableSeq) {
	this.ddPayableSeq = ddPayableSeq;
    }

    public Integer getDdSchedSeq() {
	return ddSchedSeq;
    }

    public void setDdSchedSeq(Integer ddSchedSeq) {
	this.ddSchedSeq = ddSchedSeq;
    }

    public com.ract.util.DateTime getProcessDate() {
	return processDate;
    }

    public void setProcessDate(com.ract.util.DateTime processDate) {
	this.processDate = processDate;
    }

    public Integer getReceiptNo() {
	return receiptNo;
    }

    public void setReceiptNo(Integer receiptNo) {
	this.receiptNo = receiptNo;
    }

    public Integer getReplaceNo() {
	return replaceNo;
    }

    public void setReplaceNo(Integer replaceNo) {
	this.replaceNo = replaceNo;
    }

    public String getReturnCode() {
	return returnCode;
    }

    public void setReturnCode(String returnCode) {
	this.returnCode = returnCode;
    }

    public com.ract.util.DateTime getSchedDate() {
	return schedDate;
    }

    public void setSchedDate(com.ract.util.DateTime schedDate) {
	this.schedDate = schedDate;
    }

    public Integer getTranNo() {
	return tranNo;
    }

    public void setTranNo(Integer tranNo) {
	this.tranNo = tranNo;
    }

    public String getTranStatus() {
	return tranStatus;
    }

    public void setTranStatus(String tranStatus) {
	this.tranStatus = tranStatus;
    }

    public com.ract.util.DateTime getReturnDate() {
	return returnDate;
    }

    public void setReturnDate(com.ract.util.DateTime returnDate) {
	this.returnDate = returnDate;
    }

    public String formatForDisplay() {
	return " Sequence =            " + ddSchedSeq + "\n Replaces Sequence =   " + replaceNo + "\n DD Payable Seq =      " + ddPayableSeq + "\n Scheduled Date =      " + schedDate + "\n Amount =              " + com.ract.util.CurrencyUtil.formatDollarValue(amount) + "\n Fee =                 " + com.ract.util.CurrencyUtil.formatDollarValue(fee) + "\n Status =              " + tranStatus + "\n Date Processed =      " + processDate + "\n Batch No =            " + tranNo + "\n Return Code =         " + returnCode + "\n Return Date =         " + returnDate + "\n Action =              " + action + "\n Create Date =         " + createDate + "\n Create Id =           " + createId + "\n Create Sales Branch = " + createSalesBranch + "\n Receipt No =          " + receiptNo;

    }

    public String formatAsHTML() {
	return " Sequence =            " + ddSchedSeq + "<br> Replaces Sequence =   " + replaceNo + "<br> DD Payable Seq =      " + ddPayableSeq + "<br> Scheduled Date =      " + schedDate + "<br> Amount =              " + com.ract.util.CurrencyUtil.formatDollarValue(amount) + "<br> Fee =                 " + com.ract.util.CurrencyUtil.formatDollarValue(fee) + "<br> Status =              " + tranStatus + "<br> Date Processed =      " + processDate + "<br> Batch No =            " + tranNo + "<br> Return Code =         " + returnCode + "<br> Return Date =         " + returnDate + "<br> Action =              " + action + "<br> Create Date =         " + createDate + "<br> Create Id =           " + createId + "<br> Create Sales Branch = " + createSalesBranch + "<br> Receipt No =          " + receiptNo;

    }

    public String toJSONArray() {
	return "[{\"title\":\"Sequence           \",\"value\":\"" + ddSchedSeq + "\"}," + "{\"title\":\"Replaces Sequence  \",\"value\":\"" + replaceNo + "\"}," + "{\"title\":\"DD Payable Seq     \",\"value\":\"" + ddPayableSeq + "\"}," + "{\"title\":\"Scheduled Date     \",\"value\":\"" + schedDate + "\"}," + "{\"title\":\"Amount             \",\"value\":\"" + com.ract.util.CurrencyUtil.formatDollarValue(amount) + "\"}," + "{\"title\":\"Fee                \",\"value\":\"" + com.ract.util.CurrencyUtil.formatDollarValue(fee) + "\"}," + "{\"title\":\"Status             \",\"value\":\"" + tranStatus + "\"}," + "{\"title\":\"Date Processed     \",\"value\":\"" + processDate + "\"}," + "{\"title\":\"Batch No           \",\"value\":\"" + tranNo + "\"}," + "{\"title\":\"Return Code        \",\"value\":\"" + returnCode + "\"}," + "{\"title\":\"Return Date        \",\"value\":\"" + returnDate + "\"}," + "{\"title\":\"Action             \",\"value\":\"" + action + "\"},"
		+ "{\"title\":\"Create Date        \",\"value\":\"" + createDate + "\"}," + "{\"title\":\"Create Id          \",\"value\":\"" + createId + "\"}," + "{\"title\":\"Create Sales Branch\",\"value\":\"" + createSalesBranch + "\"}," + "{\"title\":\"Receipt No         \",\"value\":\"" + receiptNo + "\"}]";

    }

}
