package com.ract.payment.directdebit;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Collection;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.GenericException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentFactory;
import com.ract.payment.PaymentMgr;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

/**
 * Holds details about a direct debit authorisation. The direct debit
 * authorisation specifies the bank account belonging to a client that can be
 * used to deduct amounts from and the frequency and day in the month that the
 * amounts can be deducted.
 * 
 * @author Terry Bakker
 * @created 8 January 2003
 * @version 1.0, 2/1/2003
 */
public class DirectDebitAuthority implements Serializable, Writable, Comparable {
    public static final String WRITABLE_CLASSNAME = "DirectDebitAuthority";

    /**
     * A system generated number to uniquely identify the direct debit
     * authority.
     */
    private Integer directDebitAuthorityID;

    /**
     * The unique identifier for the client that the direct debit authority
     * belongs to.
     */
    private Integer clientNumber;

    /**
     * The login name of the user who created the direct debit authority.
     */
    private String userID;

    /**
     * The sales branch responsible for creating the direct debit authority.
     */
    private String salesBranchCode;

    /**
     * The date the direct debit authority was created.
     */
    private DateTime createDate;

    /**
     * Indicates if the client that the direct debit belongs to is the owner of
     * the bank account specified by the direct debit authority.
     */
    private boolean clientIsOwner;

    /**
     * The day in the month on which the deduction from the clients bank account
     * is to be made.
     */
    private int dayToDebit;

    /**
     * Indicates if the direct debit authority has been approved.
     */
    private boolean approved;

    /**
     * The date after which the first direct debit may be taken. Typically set
     * to two weeks after the direct debit authority was created to give time
     * for the paper work to be returned.
     */
    private DateTime ddrDate;

    /**
     * How often the direct debit is allowed to be taken.
     */
    private DirectDebitFrequency deductionFrequency;

    private BankAccount bankAccount;

    private SourceSystem sourceSystem;

    private Integer sourceSystemReference;

    /**
     * Constructor for the DirectDebitAuthority object
     */
    public DirectDebitAuthority() {
    }

    public DirectDebitAuthority(Node directDebitNode) {
	NodeList elementList = directDebitNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    // load attributes

	    if ("approved".equals(nodeName) && valueText != null) {
		this.approved = Boolean.valueOf(valueText).booleanValue();
	    } else if ("bankAccount".equals(nodeName) && writableNode != null) {
		try {
		    this.bankAccount = PaymentFactory.getBankAccount(writableNode);
		} catch (SystemException ex3) {
		    LogUtil.warn(this.getClass(), "Unable to get bank account. " + ex3);
		    this.bankAccount = null;
		}
	    } else if ("clientIsOwner".equals(nodeName) && valueText != null) {
		this.clientIsOwner = Boolean.valueOf(valueText).booleanValue();
	    } else if ("clientNumber".equals(nodeName) && valueText != null) {
		this.clientNumber = new Integer(valueText);
	    } else if ("dayToDebit".equals(nodeName) && valueText != null) {
		this.dayToDebit = Integer.parseInt(valueText);
	    } else if ("ddrDate".equals(nodeName) && valueText != null) {
		try {
		    this.ddrDate = new DateTime(valueText);
		} catch (ParseException ex) {
		    LogUtil.warn(this.getClass(), ex);
		    this.ddrDate = null;
		}
	    } else if ("deductionFrequency".equals(nodeName) && valueText != null) {
		try {
		    this.deductionFrequency = DirectDebitFrequency.getFrequency(valueText);
		} catch (SystemException ex1) {
		    LogUtil.warn(this.getClass(), ex1);
		    this.deductionFrequency = null;
		}
	    } else if ("directDebitAuthorityID".equals(nodeName) && valueText != null) {
		this.directDebitAuthorityID = new Integer(valueText);
	    } else if ("salesBranchCode".equals(nodeName) && valueText != null) {
		this.salesBranchCode = valueText;
	    } else if ("sourceSystem".equals(nodeName) && writableNode != null) {
		try {
		    this.sourceSystem = new SourceSystem(writableNode);
		} catch (GenericException ex2) {
		    LogUtil.warn(this.getClass(), ex2);
		    this.sourceSystem = null;
		}
	    } else if ("sourceSystemReference".equals(nodeName) && valueText != null) {
		this.sourceSystemReference = new Integer(valueText);
	    } else if ("userID".equals(nodeName) && valueText != null) {
		this.userID = valueText;
	    }

	}

    }

    /**
     * Constructor for the DirectDebitAuthority object
     * 
     * @param ddAuthorityID
     *            Description of the Parameter
     * @param clientNumber
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @param salesBranchCode
     *            Description of the Parameter
     * @param createDate
     *            Description of the Parameter
     * @param clientIsOwner
     *            Description of the Parameter
     * @param dayToDebit
     *            Description of the Parameter
     * @param approved
     *            Description of the Parameter
     * @param ddrDate
     *            Description of the Parameter
     * @param frequency
     *            Description of the Parameter
     * @param bankAccount
     *            Description of the Parameter
     * @param ss
     *            Description of the Parameter
     * @param ssReference
     *            Description of the Parameter
     */
    public DirectDebitAuthority(Integer ddAuthorityID, Integer clientNumber, String userID, String salesBranchCode, DateTime createDate, boolean clientIsOwner, int dayToDebit, boolean approved, DateTime ddrDate, DirectDebitFrequency frequency, BankAccount bankAccount, SourceSystem ss, Integer ssReference) {
	setDirectDebitAuthorityID(ddAuthorityID);
	setClientNumber(clientNumber);
	setUserID(userID);
	setSalesBranchCode(salesBranchCode);
	setCreateDate(createDate);
	setClientIsOwner(clientIsOwner);
	setDayToDebit(dayToDebit);
	setApproved(approved);
	setDdrDate(ddrDate);
	setDeductionFrequency(frequency);
	setBankAccount(bankAccount);
	setSourceSystem(ss);
	setSourceSystemReference(ssReference);
    }

    /**
     * Gets the approved attribute of the DirectDebitAuthority object
     * 
     * @return The approved value
     */
    public boolean isApproved() {
	return approved;
    }

    public String toString() {
	StringBuffer directDebitAuthorityDetails = new StringBuffer();
	directDebitAuthorityDetails.append("directDebitAuthorityId=" + this.getDirectDebitAuthorityID() + FileUtil.NEW_LINE);
	directDebitAuthorityDetails.append("clientNumber=" + this.getClientNumber() + FileUtil.NEW_LINE);
	directDebitAuthorityDetails.append("createDate=" + this.getCreateDate() + FileUtil.NEW_LINE);
	directDebitAuthorityDetails.append("dayToDebit=" + this.getDayToDebit() + FileUtil.NEW_LINE);
	directDebitAuthorityDetails.append("ddrDate=" + this.getDdrDate() + FileUtil.NEW_LINE);
	directDebitAuthorityDetails.append("frequency=" + this.getDeductionFrequency().getDescription() + FileUtil.NEW_LINE);
	directDebitAuthorityDetails.append("sourceSystem=" + this.getSourceSystem() + FileUtil.NEW_LINE);
	directDebitAuthorityDetails.append("userId=" + this.getUserID() + FileUtil.NEW_LINE);
	return directDebitAuthorityDetails.toString();
    }

    public int compareTo(Object obj) {
	DirectDebitAuthority compAuth = null;
	if (obj != null && obj instanceof DirectDebitAuthority) {
	    compAuth = (DirectDebitAuthority) obj;
	    if (this.createDate != null && compAuth.getCreateDate() != null) {
		return this.createDate.compareTo(compAuth.getCreateDate());
	    }
	}
	return 0;
    }

    /**
     * Sets the approved attribute of the DirectDebitAuthority object
     * 
     * @param approved
     *            The new approved value
     */
    public void setApproved(boolean approved) {
	this.approved = approved;
    }

    /**
     * Gets the bankAccount attribute of the DirectDebitAuthority object
     * 
     * @return The bankAccount value
     */
    public BankAccount getBankAccount() {
	return bankAccount;
    }

    /**
     * Sets the bankAccount attribute of the DirectDebitAuthority object
     * 
     * @param bankAccount
     *            The new bankAccount value
     */
    public void setBankAccount(BankAccount bankAccount) {
	this.bankAccount = bankAccount;
    }

    /**
     * Gets the clientIsOwner attribute of the DirectDebitAuthority object
     * 
     * @return The clientIsOwner value
     */
    public boolean isClientIsOwner() {
	return clientIsOwner;
    }

    /**
     * Sets the clientIsOwner attribute of the DirectDebitAuthority object
     * 
     * @param clientIsOwner
     *            The new clientIsOwner value
     */
    public void setClientIsOwner(boolean clientIsOwner) {
	this.clientIsOwner = clientIsOwner;
    }

    /**
     * Gets the clientNumber attribute of the DirectDebitAuthority object
     * 
     * @return The clientNumber value
     */
    public Integer getClientNumber() {
	return clientNumber;
    }

    /**
     * Sets the clientNumber attribute of the DirectDebitAuthority object
     * 
     * @param clientNumber
     *            The new clientNumber value
     */
    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    /**
     * Gets the createDate attribute of the DirectDebitAuthority object
     * 
     * @return The createDate value
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    /**
     * Sets the createDate attribute of the DirectDebitAuthority object
     * 
     * @param createDate
     *            The new createDate value
     */
    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    /**
     * Gets the dayToDebit attribute of the DirectDebitAuthority object
     * 
     * @return The dayToDebit value
     */
    public int getDayToDebit() {
	return dayToDebit;
    }

    /**
     * Sets the dayToDebit attribute of the DirectDebitAuthority object
     * 
     * @param dayToDebit
     *            The new dayToDebit value
     */
    public void setDayToDebit(int dayToDebit) {
	this.dayToDebit = dayToDebit;
    }

    /**
     * Gets the ddrDate attribute of the DirectDebitAuthority object
     * 
     * @return The ddrDate value
     */
    public DateTime getDdrDate() {
	return ddrDate;
    }

    /**
     * Sets the ddrDate attribute of the DirectDebitAuthority object
     * 
     * @param ddrDate
     *            The new ddrDate value
     */
    public void setDdrDate(DateTime ddrDate) {
	this.ddrDate = ddrDate;
    }

    /**
     * Gets the deductionFrequency attribute of the DirectDebitAuthority object
     * 
     * @return The deductionFrequency value
     */
    public DirectDebitFrequency getDeductionFrequency() {
	return deductionFrequency;
    }

    /**
     * Sets the deductionFrequency attribute of the DirectDebitAuthority object
     * 
     * @param deductionFrequency
     *            The new deductionFrequency value
     */
    public void setDeductionFrequency(DirectDebitFrequency deductionFrequency) {
	this.deductionFrequency = deductionFrequency;
    }

    /**
     * Gets the directDebitAuthorityID attribute of the DirectDebitAuthority
     * object
     * 
     * @return The directDebitAuthorityID value
     */
    public Integer getDirectDebitAuthorityID() {
	return directDebitAuthorityID;
    }

    /**
     * Sets the directDebitAuthorityID attribute of the DirectDebitAuthority
     * object
     * 
     * @param directDebitAuthorityID
     *            The new directDebitAuthorityID value
     */
    public void setDirectDebitAuthorityID(Integer directDebitAuthorityID) {
	this.directDebitAuthorityID = directDebitAuthorityID;
    }

    /**
     * Gets the salesBranchCode attribute of the DirectDebitAuthority object
     * 
     * @return The salesBranchCode value
     */
    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    /**
     * Sets the salesBranchCode attribute of the DirectDebitAuthority object
     * 
     * @param salesBranchCode
     *            The new salesBranchCode value
     */
    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    /**
     * Gets the userID attribute of the DirectDebitAuthority object
     * 
     * @return The userID value
     */
    public String getUserID() {
	return userID;
    }

    /**
     * Sets the userID attribute of the DirectDebitAuthority object
     * 
     * @param userID
     *            The new userID value
     */
    public void setUserID(String userID) {
	this.userID = userID;
    }

    /**
     * Gets the sourceSystem attribute of the DirectDebitAuthority object
     * 
     * @return The sourceSystem value
     */
    public SourceSystem getSourceSystem() {
	return sourceSystem;
    }

    /**
     * Sets the sourceSystem attribute of the DirectDebitAuthority object
     * 
     * @param sourceSystem
     *            The new sourceSystem value
     */
    public void setSourceSystem(SourceSystem sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    /**
     * Gets the sourceSystemReference attribute of the DirectDebitAuthority
     * object
     * 
     * @return The sourceSystemReference value
     */
    public Integer getSourceSystemReference() {
	return sourceSystemReference;
    }

    /**
     * Get list of schedules attached to the authority. Note: lazy fetch
     * 
     * @return
     * @throws RemoteException
     */
    public Collection getScheduleList() throws RemoteException {
	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	Collection scheduleList = null;
	try {
	    scheduleList = pMgr.getDirectDebitScheduleListByDirectDebitAuthority(this);
	} catch (PaymentException ex) {
	    throw new RemoteException("Unable to get direct debit schedule list by authority.", ex);
	}
	return scheduleList;
    }

    /**
     * Sets the sourceSystemReference attribute of the DirectDebitAuthority
     * object
     * 
     * @param sourceSystemReference
     *            The new sourceSystemReference value
     */
    public void setSourceSystemReference(Integer sourceSystemReference) {
	this.sourceSystemReference = sourceSystemReference;
    }

    /**
     * Return true if the direct debit authority has significantly changed. The
     * authority has significantly changed (meaning a new direct debity
     * authority is required) if the debit day or frequency or bank account
     * details are different.
     * 
     * @param authority
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean hasSignificantlyChanged(DirectDebitAuthority authority) {
	if (authority == null) {
	    return true;
	}

	if (this.dayToDebit != authority.getDayToDebit()) {
	    return true;
	}

	if (this.deductionFrequency == null && authority.getDeductionFrequency() != null) {
	    return true;
	}

	if (this.deductionFrequency != null && authority.getDeductionFrequency() == null) {
	    return true;
	}

	if (this.deductionFrequency != null && authority.getDeductionFrequency() != null && !this.deductionFrequency.equals(authority.getDeductionFrequency())) {
	    return true;
	}

	if (this.bankAccount == null && authority.getBankAccount() != null) {
	    return true;
	}

	if (this.bankAccount != null && authority.getBankAccount() == null) {
	    return true;
	}

	if (this.bankAccount != null && authority.getBankAccount() != null && this.bankAccount.hasSignificantlyChanged(authority.getBankAccount())) {
	    return true;
	}

	return false;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public boolean hasCreditCardAccount() {
	return this.bankAccount instanceof CreditCardAccount;
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("approved", this.approved);
	cw.writeAttribute("bankAccount", this.bankAccount);
	cw.writeAttribute("clientIsOwner", this.clientIsOwner);
	cw.writeAttribute("clientNumber", this.clientNumber);
	cw.writeAttribute("createDate", this.createDate);
	cw.writeAttribute("dayToDebit", this.dayToDebit);
	cw.writeAttribute("ddrDate", this.ddrDate);
	cw.writeAttribute("deductionFrequency", this.deductionFrequency);
	cw.writeAttribute("directDebitAuthorityID", this.directDebitAuthorityID);
	cw.writeAttribute("salesBranchCode", this.salesBranchCode);
	cw.writeAttribute("sourceSystem", this.sourceSystem);
	cw.writeAttribute("sourceSystemReference", this.sourceSystemReference);
	cw.writeAttribute("userID", this.userID);
	cw.endClass();
    }

    public String getDirectDebitProblemReason() throws RemoteException {
	if (this.inArrearsReason == null && !this.checkedArrears) {
	    LogUtil.debug(this.getClass(), "inArrearsReason=" + inArrearsReason);
	    PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	    try {
		this.inArrearsReason = pMgr.getDirectDebitStatus(directDebitAuthorityID);
	    } catch (PaymentException ex) {
		throw new RemoteException("Unable to get the direct debit status.", ex);
	    }
	}
	this.checkedArrears = true;
	return this.inArrearsReason;
    }

    // internal use only
    private String inArrearsReason;

    // internal use only
    private boolean checkedArrears;

}
