/*
 * @(#)DirectDebitFrequency.java
 *
 */
package com.ract.payment.directdebit;

import java.io.Serializable;

import com.ract.common.SystemException;

/**
 * Represent the different frequencies or withdrawals possible in direct debit.
 * 
 * NOTE: This class does not need to implement Writable as an instance of it
 * only has one attribute which is returned by the toString method.
 * 
 * @author Glenn Lewis
 * @version 1.0, 15/5/2002
 * 
 */
public final class DirectDebitFrequency implements Serializable {
    private String myDescription = null;

    /**
     * Constructor. (Private constructor ensures that this class can only be
     * constructed by member methods.)
     * 
     * @param description
     *            a description of the frequency
     */
    private DirectDebitFrequency(String description) {
	myDescription = description;
    }

    /** Instance representing annual frequency */
    public static final DirectDebitFrequency ANNUALLY = new DirectDebitFrequency("Annually");

    /** Instance representing half yearly frequency */
    public static final DirectDebitFrequency HALF_YEARLY = new DirectDebitFrequency("Half Yearly");

    /** Instance representing quaterly frequency */
    public static final DirectDebitFrequency QUARTERLY = new DirectDebitFrequency("Quarterly");

    /** Instance representing monthly frequency */
    public static final DirectDebitFrequency MONTHLY = new DirectDebitFrequency("Monthly");

    /** Instance representing monthly frequency */
    public static final DirectDebitFrequency FORTNIGHTLY = new DirectDebitFrequency("Fortnightly");

    /** All frequencies */
    private static DirectDebitFrequency[] allFrequencies = { ANNUALLY, HALF_YEARLY, QUARTERLY, MONTHLY, FORTNIGHTLY };

    /** Get the DirectDebitFrequency for a given description */
    public static DirectDebitFrequency getFrequency(String description) throws SystemException {
	for (int i = 0; i < allFrequencies.length; i++) {
	    DirectDebitFrequency frequency = allFrequencies[i];
	    if (frequency.getDescription().equalsIgnoreCase(description)) {
		return frequency;
	    }
	}
	throw new SystemException("Unknown frequency [" + description + "]");
    }

    /**
     * Get the description of the frequency
     * 
     * @return the description of the frequency
     */
    public String getDescription() {
	return myDescription;
    }

    // override the equals method
    public boolean equals(Object otherObject) {
	if (!(otherObject instanceof DirectDebitFrequency)) {
	    return false;
	} else {
	    String otherDescription = ((DirectDebitFrequency) otherObject).getDescription();
	    return myDescription.equals(otherDescription);
	}
    }

    public String toString() {
	if (this.myDescription != null) {
	    return this.myDescription;
	} else {
	    return "DirectDebitFrequency is undefined!";
	}
    }

}
