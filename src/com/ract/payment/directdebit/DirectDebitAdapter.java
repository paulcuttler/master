//Source file: C:\\src\\master\\src\\com\\ract\\payment\\directdebit\\DirectDebitAdapter.java

package com.ract.payment.directdebit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.ract.common.RollBackException;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.util.DateTime;

/**
 * The direct debit adapter interface. Provides basic CRUD (create, read,
 * update, and delete) operations on the direct debit system.
 * 
 * @author Glenn Lewis
 * @created 14 January 2003
 * @version 1.0, 1/5/2002
 */
public interface DirectDebitAdapter extends TransactionAdapter {

    /**
     * String representing authority not approved.
     */
    public final static String DD_STATUS_AUTHORITY_NOT_APPROVED = "DD Authority not approved";

    /**
     * String representing unresolved dishonour payment.
     */
    public final static String DD_STATUS_UNRESOLVED_PAYMENT = "Unresolved dishonoured payment";

    /**
     * String representing payment in arrears.
     */
    public final static String DD_STATUS_PAYMENT_IN_ARREARS = "Payment at least 14 days in arrears";

    /**
     * String representing failed direct debits
     */
    public final static String DD_STATUS_FAILED = "Direct Debits Failed";

    /**
     * key to get the direct debit authority id
     */
    public final static String DD_AUTHORITY_KEY = "DDA";

    /**
     * key to get the old direct debit schedule id
     */
    public final static String DD_OLD_SCHEDULE_KEY = "DDOS";

    /**
     * key to get the new direct debit schedule id
     */
    public final static String DD_NEW_SCHEDULE_KEY = "DDNS";

    /**
     * Validate the BSB number externally.
     * 
     * @param bsbNumber
     * @return
     * @throws SystemException
     */
    public String validateBSBNumber(String bsbNumber) throws SystemException;

    /**
     * Active a renewal version of a direct debit schedule.
     * 
     * @param directDebitScheduleId
     *            Integer
     * @throws SystemException
     */
    public Integer activateRenewalSchedule(Integer directDebitScheduleId, String userId) throws RollBackException;

    /**
     * Create an new authority to direct debit a clients bank account. If a
     * direct debit authority for the client does not exist then create a new
     * direct debit authority and return its ID. Otherwise if any of the
     * following details: BSB number, Account number, Credit card number,
     * Deduction frequency, Day to debit, DDR Date on the existing direct debit
     * authority are different to the new direct debit authority details then
     * create a new version of the direct debit authority and return its ID.
     * Otherwise update the existing authority. Do not create or change the
     * schedule of deductions.
     * 
     * @param ddAuthority
     *            Description of the Parameter
     * @return The ID of the direct debit authority created/updated.@throws
     *         SystemException
     * @exception SystemException
     *                Description of the Exception
     * @throws ValidationException
     */
    public Hashtable createAuthority(DirectDebitAuthority ddAuthority) throws RollBackException;

    public Collection getPoliciesOnAccount(Integer ddAuthorityId) throws SystemException;

    public Hashtable<Integer, String> getOrphanedDDSchedules(String sourceSystem) throws SystemException;

    public List<Map<String, Object>> getFailedDDSchedules(Integer clientNo, String sourceSystem) throws SystemException;

    /**
     * Remove an existing direct debit authority. If any scheduled items
     * associated to any version of the authority have been processed then throw
     * an exception. The authority cannot be removed. Delete all schedules and
     * scheduled items associated with all versions of the authority. Delete all
     * versions of the authority.
     * 
     * @param ddAuthorityID
     * @throws SystemException
     * @throws ValidationException
     */
    public void removeAuthority(Integer ddAuthorityID) throws RollBackException;

    /**
     * Create a new schedule of deductions given a schedule specification. The
     * schedule specification consists of a direct debit authority, the start
     * and end dates of the period over which deductions are to be made, the
     * amount to amortize over the period of deduction and the additional amount
     * to take with the first deduction. Always create a new schedule of
     * deductions even if there is an existing schedule for the same direct
     * debit authority.
     * 
     * @param ddSchedule
     *            - The direct debit schedule specification that the schedule is
     *            to be created from. The schedule specification must contain as
     *            a minimum: an existing direct debit authority ID start date
     *            end date amortised amount product code
     * @return int
     * @throws SystemException
     * @throws ValidationException
     */
    public Integer createSchedule(DirectDebitSchedule ddSchedule) throws RollBackException;

    public Integer createRenewalSchedule(DirectDebitSchedule ddSchedule) throws RollBackException;

    /**
     * Change the status of all of the remaining scheduled items to "Replaced",
     * create a new version of the direct debit schedule using the new schedule
     * specification details and generate new scheduled items.
     * 
     * @param ddSchdule
     *            - A new specification for an existing direct debit schedule.
     *            The specification must contain the ID of an existing direct
     *            debit schedule.
     * @return int
     * @throws SystemException
     * @throws ValidationException
     */
    public Integer replaceSchedule(DirectDebitSchedule ddSchdule) throws RollBackException;

    /**
     * Change the status of all un-processed scheduled items to "Cancelled".
     * 
     * @param ddScheduleID
     *            - The ID of the direct debit schedule to cancel.
     * @throws SystemException
     * @throws ValidationException
     */
    public void cancelSchedule(Integer ddScheduleID, String userID) throws RollBackException;

    /**
     * Delete all of the scheduled items for the schedule and delete the
     * schedule. If any of the scheduled items for the schedule have been
     * processed or have a status of "Replaced" then throw an exception.
     * 
     * @param ddScheduleID
     *            - The ID of the direct debit schedule to remove.
     * @throws SystemException
     * @throws ValidationException
     */
    public void removeSchedule(Integer ddScheduleID) throws RollBackException;

    /**
     * If any of the scheduled items have a status of "Cancelled" then it is
     * assumed that the canceling of the schedule is being undone. Change the
     * status of all cancelled items to "Scheduled". Otherwise, delete all of
     * the scheduled items for the schedule to be undone and delete the schedule
     * to be undone. Find the previous schedule, if one was specified, and for
     * each scheduled item with a status of "Replaced", change the status to
     * "Scheduled". If any of the scheduled items for the schedule to be undone
     * have been processed or have a status of "Replaced" then throw an
     * exception. If the status of the scheduled items is "Replaced" then the
     * schedule that replaced it must be undone first.
     * 
     * @param undoScheduleID
     *            - The ID of the direct debit schedule to undo.
     * @param reinstateScheduleID
     *            - The ID of the direct debit schedule to reinstate. Set to
     *            zero if no direct debit schedule is being reinstated.
     * @throws SystemException
     * @throws ValidationException
     */
    public void undoSchedule(Integer undoScheduleID, Integer reinstateScheduleID, String userID) throws RollBackException;

    /**
     * Reset the cancel flag to undo the cancel direct debit operation.
     * 
     * @param uncancelSchedule
     * @throws RollBackException
     */
    public void uncancelSchedule(Integer uncancelSchedule) throws RollBackException;

    /**
     * Given a direct debit authority ID return the direct debit authority
     * details.
     * 
     * @param ddAuthorityID
     * @return com.ract.payment.directdebit.DirectDebitAuthority
     * @throws SystemException
     */
    public DirectDebitAuthority getAuthority(Integer ddAuthorityID) throws SystemException;

    /**
     * Given a client number return a list of all of the DirectDebitAuthority
     * that belong to the client.
     * 
     * @param clientNumber
     * @return java.util.Vector
     * @throws SystemException
     */
    public Vector getAuthorityList(Integer clientNumber) throws SystemException;

    /**
     * Given a direct debit schedule ID return the details of the direct debit
     * schedule including the specification and the scheduled items.
     * 
     * @param ddScheduleID
     * @return com.ract.payment.directdebit.DirectDebitSchedule
     * @throws SystemException
     */
    public DirectDebitSchedule getSchedule(Integer ddScheduleID) throws SystemException;

    /**
     * Given a direct debit authority number return a list of all of the direct
     * debit schedules associated to the authority.
     * 
     * @param ddAuthorityID
     *            Description of the Parameter
     * @return java.util.Vector
     * @throws SystemException
     */
    public Vector getScheduleListByAuthority(Integer ddAuthorityID) throws SystemException;

    /**
     * Given a client number return a list of all of the direct debit schedules
     * belonging to the client.
     * 
     * @param clientNumber
     * @return java.util.Vector
     * @throws SystemException
     */
    // public Vector getScheduleListByClientNumber(Integer clientNumber)
    // throws SystemException;

    public Vector getScheduleListByClientNumber(Integer clientNumber, DateTime startDate, DateTime endDate, boolean outstandingOnly) throws SystemException;

    /**
     * Given a deduction frequency, day in the month to debit, start date, end
     * date, amortized amount and immediate amount return a provisional direct
     * debit schedule. Do not make any changes to data in the direct debit
     * system.
     * 
     * @param ddSchedule
     *            Description of the Parameter
     * @return com.ract.payment.directdebit.DirectDebitSchedule
     * @exception SystemException
     *                Description of the Exception
     */
    // public DirectDebitSchedule getProvisionalSchedule(DirectDebitSchedule
    // ddSchedule)
    // throws SystemException;

    /**
     * Commit any pending transactions in the direct debit sub system. The
     * connection to the direct debit sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */

    public void commit() throws SystemException;

    /**
     * Rollback any pending transactions in the direct debit sub system. The
     * connection to the direct debit sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws SystemException;

    /**
     * Release any connections held to the direct debit sub-system. If a
     * transaction is pending and not committed then the transaction will be
     * rolled back.
     */
    public void release();

    /**
     * Sends a message to the direct debit system that an identified scheduled
     * payment has been paid through the receipting system.
     * 
     * @param Integer
     *            ddPayableSeq ddPayableSeq and ddSchedSeq provide the key to
     *            the direct debit system
     * @param Integer
     *            ddSchedSeq
     * @param BigDecimal
     *            amount The amount that has been receipted (should be the same
     *            as the scheduled amount)
     * @param receiptNo
     *            The receipt number for the payment
     * @param logonId
     *            Identifies the user who takes the payment
     * @throws RollBackException
     *             An exception will be thrown if any of the required records in
     *             the dd system do not match the returned data, or have
     *             changed.
     */
    public void markScheduleItemAsPaid(Integer scheduleID, Integer scheduledItemID, BigDecimal amount, Integer receiptNo, String logonId) throws RollBackException;

    /**
     * Returns a Vector of DirectDebitItem objects which can be paid, that is,
     * that are scheduled, or have been dishonoured, but not re-scheduled If
     * there are no payable items, then returns an empty Vector
     * 
     * @param clientNo
     * @return
     * @throws SystemException
     */
    public Vector getPayableScheduledItemsForClient(Integer clientNo) throws SystemException;

    /**
     * Validate a credit card number.
     * 
     * @param ccNumber
     *            String
     * @throws SystemException
     * @return String
     */
    public String validateCreditCardType(String ccNumber) throws SystemException;

    /**
     * checks the direct debit system to make sure that any scheduled direct
     * debits are paid up to date, that there are no unresolved dishonours or
     * failed items. Also checks that direct debit authorities have been
     * approved.
     * 
     * @param ddNumber
     *            The key into the direct debit system.
     * @return An empty string if everything is OK. OTherwise, the reason for
     *         failure
     * @throws SystemException
     */
    public String getDDStatus(Integer ddAuthorityID) throws SystemException;

    public ArrayList getDDItems(Integer systemNo, String ddType) throws SystemException;

    public DDMaster getDDMaster(Integer ddSequenceNo) throws SystemException;

    public ArrayList getDDPayables(Integer ddItemNo, Integer ddItemSeq) throws SystemException;

    public ArrayList getDDSchedules(Integer ddPayableSeq) throws SystemException;

    public DDSched getDDSched(Integer ddPayableSeq, Integer ddSchedSeq) throws SystemException;

    public DDPayable getDDPayable(Integer ddPayableSeq) throws SystemException;

    public DDItem getDDItem(Integer ddItemNo, Integer ddItemSeq) throws SystemException;

    public DDMaster getDDMaster(Integer ddItemNo, Integer ddItemSeq) throws SystemException;

    public ArrayList getDDMemos(Integer id, String type) throws SystemException;

    public DDMemo getDDMemo(Integer ddItemNo, Integer memoSeq) throws SystemException;

    public void clearFailedStatus(Integer clientNo, Integer seqNo, BigDecimal amount) throws Exception;
}
