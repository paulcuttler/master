package com.ract.payment.directdebit;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.MailMgr;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

public class DirectDebitAuditJob implements Job {

    private static final String DEFAULT_PROPERTIES = "master";

    @SuppressWarnings("unchecked")
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {

	LogUtil.log(getClass(), "Commencing DD Audit...");

	try {
	    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    // MembershipMgr membershipMgr =
	    // MembershipEJBHelper.getMembershipMgr();

	    LogUtil.log(getClass(), "DD Audit - retrieving orphaned DD Schedules...");
	    Hashtable<Integer, String> orphanedDDSchedules = new Hashtable<Integer, String>();
	    try {
		orphanedDDSchedules = paymentMgr.getOrphanedDDSchedules(SourceSystem.MEMBERSHIP.getAbbreviation());
	    } catch (Exception e) {
		LogUtil.fatal(getClass(), "DD Audit - failed to retrieve orphaned DD Schedules.");
	    }

	    LogUtil.log(getClass(), "DD Audit - retrieving unlinked DD Schedules...");
	    Map<String, String> unlinkedDDSchedules = new HashMap<String, String>();
	    try {
		unlinkedDDSchedules = paymentMgr.getUnlinkedDDSchedules(new DateTime());
	    } catch (Exception e) {
		LogUtil.fatal(getClass(), "DD Audit - failed to retrieve unlinked DD Schedules.");
	    }

	    // LogUtil.log(getClass(),
	    // "DD Audit - retrieving renewal advices without DD Schedules...");
	    Map<String, String> advicesWithoutSchedules = new HashMap<String, String>();
	    /*
	     * try { advicesWithoutSchedules =
	     * membershipMgr.getRenewalAdvicesWithoutDDSchedules(new
	     * DateTime()); } catch (Exception e) { LogUtil.fatal(getClass(),
	     * "DD Audit - failed to retrieve renewal advices without DD Schedules."
	     * ); }
	     */

	    LogUtil.log(getClass(), "DD Audit - finished retrieving lists.");

	    if (!orphanedDDSchedules.isEmpty() || !unlinkedDDSchedules.isEmpty() || !advicesWithoutSchedules.isEmpty()) {
		final int COLUMNWIDTH = 20;
		final int COLUMNCOUNT = 2;

		String systemName = "Unknown";
		try {
		    systemName = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);
		} catch (Exception r) {
		}

		final String SUBJECT = systemName + ": Orphaned/Unlinked direct debit schedules for " + SourceSystem.MEMBERSHIP.getSystemName();
		StringBuffer messageText = new StringBuffer();

		if (!orphanedDDSchedules.isEmpty()) {
		    messageText.append("The following records have an active direct debit schedule but no reference in the payable item table.");
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append(StringUtil.rightPadString("DD Payable Sequence", COLUMNWIDTH));
		    messageText.append(StringUtil.rightPadString("Client Number", COLUMNWIDTH));
		    messageText.append(FileUtil.NEW_LINE);
		    for (int i = 0; i < (COLUMNWIDTH * COLUMNCOUNT); i++) {
			messageText.append("-");
		    }
		    messageText.append(FileUtil.NEW_LINE);
		    String clientNumber = null;
		    for (Integer ddPayableSeq : orphanedDDSchedules.keySet()) {
			clientNumber = orphanedDDSchedules.get(ddPayableSeq);
			messageText.append(StringUtil.rightPadString(ddPayableSeq.toString(), COLUMNWIDTH));
			messageText.append(StringUtil.rightPadString(clientNumber.toString(), COLUMNWIDTH));
			messageText.append(FileUtil.NEW_LINE);
		    }
		}

		if (!unlinkedDDSchedules.isEmpty()) {
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append("The following records have a missing direct debit schedule.");
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append(StringUtil.rightPadString("DD Payable Sequence", COLUMNWIDTH));
		    messageText.append(StringUtil.rightPadString("Client Number", COLUMNWIDTH));
		    messageText.append(FileUtil.NEW_LINE);
		    for (int i = 0; i < (COLUMNWIDTH * COLUMNCOUNT); i++) {
			messageText.append("-");
		    }
		    messageText.append(FileUtil.NEW_LINE);
		    String clientNumber = null;
		    for (String ddPayableSeq : unlinkedDDSchedules.keySet()) {
			clientNumber = unlinkedDDSchedules.get(ddPayableSeq);
			messageText.append(StringUtil.rightPadString(ddPayableSeq, COLUMNWIDTH));
			messageText.append(StringUtil.rightPadString(clientNumber, COLUMNWIDTH));
			messageText.append(FileUtil.NEW_LINE);
		    }
		}

		if (!advicesWithoutSchedules.isEmpty()) {
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append("The following records have a renewal document missing a renewal direct debit schedule.");
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append(FileUtil.NEW_LINE);
		    messageText.append(StringUtil.rightPadString("DD Payable Sequence", COLUMNWIDTH));
		    messageText.append(StringUtil.rightPadString("Client Number", COLUMNWIDTH));
		    messageText.append(FileUtil.NEW_LINE);
		    for (int i = 0; i < (COLUMNWIDTH * COLUMNCOUNT); i++) {
			messageText.append("-");
		    }
		    messageText.append(FileUtil.NEW_LINE);
		    String clientNumber = null;
		    for (String ddPayableSeq : advicesWithoutSchedules.keySet()) {
			clientNumber = advicesWithoutSchedules.get(ddPayableSeq);
			messageText.append(StringUtil.rightPadString(ddPayableSeq, COLUMNWIDTH));
			messageText.append(StringUtil.rightPadString(clientNumber, COLUMNWIDTH));
			messageText.append(FileUtil.NEW_LINE);
		    }
		}

		String emailAddress = FileUtil.getProperty(DEFAULT_PROPERTIES, SystemParameterVO.EMAIL_DIRECT_DEBIT_AUDIT);

		MailMessage message = new MailMessage();
		message.setRecipient(emailAddress);
		message.setSubject(SUBJECT);
		message.setMessage(messageText.toString());

		MailMgr mailMgr = CommonEJBHelper.getMailMgr();
		mailMgr.sendMail(message);
	    }

	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}

	LogUtil.log(getClass(), "Finished DD Audit.");
    }

}
