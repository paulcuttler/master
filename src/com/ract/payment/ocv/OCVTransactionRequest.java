package com.ract.payment.ocv;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.StringUtil;

public class OCVTransactionRequest implements Serializable {

    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String getSourceSystem() {
	return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public String getSourceSystemReference() {
	return sourceSystemReference;
    }

    public void setSourceSystemReference(String sourceSystemReference) {
	this.sourceSystemReference = sourceSystemReference;
    }

    public static final String CLIENT_TYPE_INTERNET = "0";
    public static final String CLIENT_TYPE_TELEPHONE_ORDER = "1";
    public static final String CLIENT_TYPE_MAIL_ORDER = "2";
    public static final String CLIENT_TYPE_CUSTOMER_PRESENT = "3";
    public static final String CLIENT_TYPE_RECURRING = "4";
    public static final String CLIENT_TYPE_INSTALLMENT = "5";

    public String getStartFlag() {
	return startFlag;
    }

    public void setStartFlag(String startFlag) {
	this.startFlag = startFlag;
    }

    public String getLength() {
	return length;
    }

    public void setLength(String length) {
	this.length = length;
    }

    public String getTransactionType() {
	return transactionType;
    }

    public void setTransactionType(String transactionType) {
	this.transactionType = transactionType;
    }

    public String getClientId() {
	return clientId;
    }

    public void setClientId(String clientId) {
	this.clientId = clientId;
    }

    public String getTransactionReference() {
	return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
	this.transactionReference = transactionReference;
    }

    public String getPolledMode() {
	return polledMode;
    }

    public void setPolledMode(String polledMode) {
	this.polledMode = polledMode;
    }

    public String getAccountNumber() {
	return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
    }

    public String getCardData() {
	return cardData;
    }

    public void setCardData(String cardData) {
	this.cardData = cardData;
    }

    public String getCardExpiryDate() {
	return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
	this.cardExpiryDate = cardExpiryDate;
    }

    public BigDecimal getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
	this.totalAmount = totalAmount;
    }

    public String getClientType() {
	return clientType;
    }

    public void setClientType(String clientType) {
	this.clientType = clientType;
    }

    public String getAuthorisationNumber() {
	return authorisationNumber;
    }

    public void setAuthorisationNumber(String authorisationNumber) {
	this.authorisationNumber = authorisationNumber;
    }

    public String getCvv() {
	return cvv;
    }

    public void setCvv(String cvv) {
	this.cvv = cvv;
    }

    public static String TRANSACTION_TYPE_PURCHASE = "1";
    public static String TRANSACTION_TYPE_STATUS_REQUEST = "2";
    public static String TRANSACTION_TYPE_REFUND = "4";
    public static String TRANSACTION_TYPE_PREAUTH_REQUEST = "7";
    public static String TRANSACTION_TYPE_COMPLETION_REQUEST = "8";

    private String startFlag = "#";

    private String length = "90";

    private String transactionType = "";

    private String clientId = "";

    private String transactionReference = "";;

    private String polledMode = "0";

    private String accountNumber = "";

    private String cardData;

    private String cardExpiryDate;

    private BigDecimal totalAmount;

    private String clientType = "";;

    private String authorisationNumber = "";

    private String cvv = "";;

    private String sourceSystem;

    private String sourceSystemReference;

    private String userId;

    private DateTime createDate;

    public String toRequestString() throws GenericException {
	// 1 - start flag
	// 4 - length
	// 1 - transaction type
	// 8 - client id
	// 16 - transaction reference
	// 1 - polled mode
	// 4 - account number
	// 20 - card data
	// 4 - card expiry date
	// 12 - total amount
	// 1 - clientType
	// 12 - authorisation number
	// 6 - cvv field
	StringBuffer transactionRequest = new StringBuffer();
	try {
	    transactionRequest.append(startFlag);
	    transactionRequest.append(StringUtil.leftPadString(length, 4));
	    transactionRequest.append(transactionType);
	    transactionRequest.append(StringUtil.rightPadString(clientId, 8));
	    transactionRequest.append(StringUtil.rightPadString(transactionReference, 16));
	    transactionRequest.append(polledMode);
	    transactionRequest.append(StringUtil.rightPadString(accountNumber, 4));
	    transactionRequest.append(StringUtil.rightPadString(cardData, 20));
	    transactionRequest.append(StringUtil.rightPadString(cardExpiryDate, 4));

	    String totalAmountString = "";

	    DecimalFormat myFormatter = new DecimalFormat("########0.00");
	    String temp = myFormatter.format(totalAmount.doubleValue());

	    // String temp = totalAmount.toString();
	    temp = StringUtil.replace(temp, ".", "");
	    System.out.println(temp);
	    final int LENGTH = 12;
	    for (int x = 0; x < (LENGTH - temp.length()); x++) {
		totalAmountString += "0";
	    }
	    totalAmountString += temp;
	    if (totalAmountString.length() != LENGTH) {
		throw new GenericException("Length of amount (" + totalAmountString.length() + ") does not equal " + LENGTH + ".");
	    }

	    transactionRequest.append(totalAmountString);

	    transactionRequest.append(clientType);
	    transactionRequest.append(StringUtil.rightPadString(authorisationNumber, 12));
	    transactionRequest.append(StringUtil.rightPadString(cvv, 6));

	    final int LENGTH_TRANSACTION_REQUEST = 90;
	    if (transactionRequest.toString().length() != LENGTH_TRANSACTION_REQUEST) {
		throw new GenericException("Length of transaction request (" + transactionRequest.toString().length() + ") does not equal " + LENGTH_TRANSACTION_REQUEST + ".");
	    }
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return transactionRequest.toString();

    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("startFlag=" + startFlag + FileUtil.NEW_LINE);
	desc.append("length=" + length + FileUtil.NEW_LINE);
	desc.append("transactionType=" + transactionType + FileUtil.NEW_LINE);
	desc.append("clientId=" + clientId + FileUtil.NEW_LINE);
	desc.append("transactionReference=" + transactionReference + FileUtil.NEW_LINE);
	desc.append("polled=" + polledMode + FileUtil.NEW_LINE);
	desc.append("accountNumber=" + accountNumber + FileUtil.NEW_LINE);
	desc.append("cardData=" + cardData + FileUtil.NEW_LINE);
	desc.append("cardExpiryDate=" + cardExpiryDate + FileUtil.NEW_LINE);
	desc.append("totalAmount=" + totalAmount + FileUtil.NEW_LINE);
	desc.append("clientType=" + clientType + FileUtil.NEW_LINE);
	desc.append("authorisationNumber=" + authorisationNumber + FileUtil.NEW_LINE);
	desc.append("cvv=" + cvv);
	return desc.toString();
    }

}
