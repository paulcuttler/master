package com.ract.payment.ocv;

public class OCVHelper {

    public static boolean isOCVApproved(String responseCode) {
	boolean transactionApproved = false;
	// String responseCode = this.getResponseCode();
	for (int x = 0; x < OCVTransactionResponse.RESPONSE_CODES_APPROVED.length && !transactionApproved; x++) {
	    transactionApproved = OCVTransactionResponse.RESPONSE_CODES_APPROVED[x].equals(responseCode);
	}
	return transactionApproved;
    }

}
