package com.ract.payment.ocv;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.Remote;

import com.ract.common.GenericException;

@Remote
public interface OCVTransactionMgr {
    public OCVTransactionResponse createPurchase(OCVTransactionRequest transactionRequest) throws RemoteException, GenericException;

    public OCVTransactionResponse sendTransactionRequest(OCVTransactionRequest transactionRequest) throws RemoteException, GenericException;

    public OCVTransaction updateOCVTransaction(OCVTransaction ocvTransaction);

    public OCVTransaction getOCVTransaction(String transactionId);

    public Collection<OCVTransaction> getOCVTransactionsBySSRef(String sourceSystemRef);
}
