package com.ract.payment.ocv;

import java.io.Serializable;

import com.ract.util.FileUtil;

public class OCVTransactionResponse implements Serializable {

    public String getTransactionId() {
	return transactionId;
    }

    public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
    }

    public final static String[] RESPONSE_CODES_APPROVED = { "00", "08", "77", "97" };

    public final static String[] RESPONSE_CODES_INCOMPLETE = { "X1", "U9", "V9", "AE", "B2" };

    public final static String RESPONSE_CODE_RECORD_NOT_FOUND = "AF";

    public final static String RESULT_TRANSACTION_APPROVED = "0";

    public final static String RESULT_TRANSACTION_DECLINED = "1";

    public final static String RESULT_TRANSACTION_IN_PROGRESS = "2";

    public final static String RESULT_TRANSACTION_NOT_FOUND = "3";

    public String getResponseString() {
	return responseString;
    }

    public void setResponseString(String responseString) {
	this.responseString = responseString;
    }

    private String responseString;

    public OCVTransactionResponse(String response) {
	setResponseString(response);
	setStartFlag(response.substring(0, 1));
	setLength(response.substring(1, 5));
	setTransationType(response.substring(5, 6));
	setClientId(response.substring(6, 14));
	setTransactionReference(response.substring(14, 30));
	setResult(response.substring(30, 31));
	setResponseCode(response.substring(31, 33));
	setResponseText(response.substring(33, 53));
	setAccount(response.substring(53, 57));
	setStan(response.substring(57, 63));
	setAuthorisationCode(response.substring(63, 69));
	setTerminalId(response.substring(69, 77));
	setMerchantId(response.substring(77, 92));
	setRetry(response.substring(92, 93));
	setSettlementDate(response.substring(93, 101));
	setCardBinNumber(response.substring(101, 103));
	setCardDescription(response.substring(103, 123));
	setPreauthNumber(response.substring(123, 135));
	setAcquirerId(response.substring(135, 136));

    }

    public String getStartFlag() {
	return startFlag;
    }

    public void setStartFlag(String startFlag) {
	this.startFlag = startFlag;
    }

    public String getLength() {
	return length;
    }

    public void setLength(String length) {
	this.length = length;
    }

    public String getTransationType() {
	return transationType;
    }

    public void setTransationType(String transationType) {
	this.transationType = transationType;
    }

    public String getClientId() {
	return clientId;
    }

    public void setClientId(String clientId) {
	this.clientId = clientId;
    }

    public String getTransactionReference() {
	return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
	this.transactionReference = transactionReference;
    }

    public String getResult() {
	return result;
    }

    public void setResult(String result) {
	this.result = result;
    }

    public String getResponseCode() {
	return responseCode;
    }

    public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
    }

    public String getResponseText() {
	return responseText;
    }

    public void setResponseText(String responseText) {
	this.responseText = responseText;
    }

    public String getAccount() {
	return account;
    }

    public void setAccount(String account) {
	this.account = account;
    }

    public String getStan() {
	return stan;
    }

    public void setStan(String stan) {
	this.stan = stan;
    }

    public String getAuthorisationCode() {
	return authorisationCode;
    }

    public void setAuthorisationCode(String authorisationCode) {
	this.authorisationCode = authorisationCode;
    }

    public String getTerminalId() {
	return terminalId;
    }

    public void setTerminalId(String terminalId) {
	this.terminalId = terminalId;
    }

    public String getMerchantId() {
	return merchantId;
    }

    public void setMerchantId(String merchantId) {
	this.merchantId = merchantId;
    }

    public String getRetry() {
	return retry;
    }

    public boolean isApproved() {
	return OCVHelper.isOCVApproved(this.getResponseCode());
    }

    public boolean isIncomplete() {
	boolean transactionIncomplete = false;
	String responseCode = this.getResponseCode();
	for (int x = 0; x < OCVTransactionResponse.RESPONSE_CODES_INCOMPLETE.length && !transactionIncomplete; x++) {
	    transactionIncomplete = OCVTransactionResponse.RESPONSE_CODES_INCOMPLETE[x].equals(responseCode);
	}
	return transactionIncomplete;
    }

    public void setRetry(String retry) {
	this.retry = retry;
    }

    public String getSettlementDate() {
	return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
	this.settlementDate = settlementDate;
    }

    public String getCardBinNumber() {
	return cardBinNumber;
    }

    public void setCardBinNumber(String cardBinNumber) {
	this.cardBinNumber = cardBinNumber;
    }

    public String getCardDescription() {
	return cardDescription;
    }

    public void setCardDescription(String cardDescription) {
	this.cardDescription = cardDescription;
    }

    public String getPreauthNumber() {
	return preauthNumber;
    }

    public void setPreauthNumber(String preauthNumber) {
	this.preauthNumber = preauthNumber;
    }

    public String getAcquirerId() {
	return acquirerId;
    }

    public void setAcquirerId(String acquirerId) {
	this.acquirerId = acquirerId;
    }

    private String startFlag;

    private String length;

    private String transationType;

    private String clientId;

    private String transactionReference;

    private String result;

    private String responseCode;

    private String responseText;

    private String account;

    private String stan;

    private String authorisationCode;

    private String terminalId;

    private String transactionId;

    private String merchantId;

    private String retry;

    private String settlementDate;

    private String cardBinNumber;

    private String cardDescription;

    private String preauthNumber;

    private String acquirerId;

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("startFlag=" + startFlag + FileUtil.NEW_LINE);
	desc.append("length=" + length + FileUtil.NEW_LINE);
	desc.append("transationType=" + transationType + FileUtil.NEW_LINE);
	desc.append("clientId=" + clientId + FileUtil.NEW_LINE);
	desc.append("transactionReference=" + transactionReference + FileUtil.NEW_LINE);
	desc.append("result=" + result + FileUtil.NEW_LINE);
	desc.append("responseCode=" + responseCode + FileUtil.NEW_LINE);
	desc.append("responseText=" + responseText + FileUtil.NEW_LINE);
	desc.append("account=" + account + FileUtil.NEW_LINE);
	desc.append("STAN=" + stan + FileUtil.NEW_LINE);
	desc.append("authorisationCode=" + authorisationCode + FileUtil.NEW_LINE);
	desc.append("terminalId=" + terminalId + FileUtil.NEW_LINE);
	desc.append("merchantId=" + merchantId + FileUtil.NEW_LINE);
	desc.append("retry=" + retry + FileUtil.NEW_LINE);
	desc.append("settlementDate=" + settlementDate + FileUtil.NEW_LINE);
	desc.append("cardBinNumber=" + cardBinNumber + FileUtil.NEW_LINE);
	desc.append("cardDescription=" + cardDescription + FileUtil.NEW_LINE);
	desc.append("preauthNumber=" + preauthNumber + FileUtil.NEW_LINE);
	desc.append("acquirerId=" + acquirerId);
	return desc.toString();
    }

}
