package com.ract.payment.ocv;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

@Entity
public class OCVTransaction {

    public String getReceiptReference() {
	return receiptReference;
    }

    public void setReceiptReference(String receiptReference) {
	this.receiptReference = receiptReference;
    }

    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String getSourceSystem() {
	return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public String getSourceSystemReference() {
	return sourceSystemReference;
    }

    public void setSourceSystemReference(String sourceSystemReference) {
	this.sourceSystemReference = sourceSystemReference;
    }

    public String getTransactionId() {
	return transactionId;
    }

    public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
    }

    public String getTransactionReference() {
	return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
	this.transactionReference = transactionReference;
    }

    private String sourceSystem;

    private String sourceSystemReference;

    public String getTransactionType() {
	return transactionType;
    }

    public void setTransactionType(String transactionType) {
	this.transactionType = transactionType;
    }

    public String getClientId() {
	return clientId;
    }

    public void setClientId(String clientId) {
	this.clientId = clientId;
    }

    public String getAccountNumber() {
	return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
    }

    public String getCardData() {
	return cardData;
    }

    public void setCardData(String cardData) {
	this.cardData = cardData;
    }

    public String getCardExpiryDate() {
	return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
	this.cardExpiryDate = cardExpiryDate;
    }

    public BigDecimal getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
	this.totalAmount = totalAmount;
    }

    public String getClientType() {
	return clientType;
    }

    public void setClientType(String clientType) {
	this.clientType = clientType;
    }

    public String getAuthorisationNumber() {
	return authorisationNumber;
    }

    public void setAuthorisationNumber(String authorisationNumber) {
	this.authorisationNumber = authorisationNumber;
    }

    public String getCvv() {
	return cvv;
    }

    public void setCvv(String cvv) {
	this.cvv = cvv;
    }

    public String getResult() {
	return result;
    }

    public void setResult(String result) {
	this.result = result;
    }

    public String getResponseCode() {
	return responseCode;
    }

    public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
    }

    public String getResponseText() {
	return responseText;
    }

    public void setResponseText(String responseText) {
	this.responseText = responseText;
    }

    @Id
    private String transactionId;

    private String transactionReference;

    private String transactionType;

    private String clientId;

    private String accountNumber;

    private String cardData;

    private String cardExpiryDate;

    private BigDecimal totalAmount;

    private DateTime createDate;

    private String userId;

    private String clientType;

    private String authorisationNumber;

    private String cvv;

    private String result;

    private String responseCode;

    private String responseText;

    private String receiptReference;

    @Override
    public String toString() {
	return "OCVTransaction [accountNumber=" + accountNumber + ", authorisationNumber=" + authorisationNumber + ", cardData=" + cardData + ", cardExpiryDate=" + cardExpiryDate + ", clientId=" + clientId + ", clientType=" + clientType + ", createDate=" + createDate + ", cvv=" + cvv + ", receiptReference=" + receiptReference + ", responseCode=" + responseCode + ", responseText=" + responseText + ", result=" + result + ", sourceSystem=" + sourceSystem + ", sourceSystemReference=" + sourceSystemReference + ", totalAmount=" + totalAmount + ", transactionId=" + transactionId + ", transactionReference=" + transactionReference + ", transactionType=" + transactionType + ", userId=" + userId + "]";
    }

}
