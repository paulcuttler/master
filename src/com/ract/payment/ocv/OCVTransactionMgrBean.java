package com.ract.payment.ocv;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ract.common.GenericException;
import com.ract.common.SequenceMgrLocal;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

@Stateless
@Remote({ OCVTransactionMgr.class })
@Local({ OCVTransactionMgrLocal.class })
public class OCVTransactionMgrBean {

    @PersistenceContext(unitName = "master")
    EntityManager em;

    @EJB
    SequenceMgrLocal sequenceMgrLocal;

    private static final String OCV_PORT = "OCV_PORT";
    private static final String OCV_SERVER = "OCV_SERVER";

    private OCVTransaction createOCVTransaction(OCVTransactionRequest transactionRequest) throws GenericException {
	LogUtil.log(this.getClass(), "createOCVTransaction");

	// sequence
	Integer transactionId = null;
	try {
	    transactionId = sequenceMgrLocal.getNextID("OCV_TRANSACTION");
	} catch (RemoteException e) {
	    throw new GenericException(e);
	}

	OCVTransaction ocvTransaction = new OCVTransaction();
	ocvTransaction.setCreateDate(transactionRequest.getCreateDate());
	ocvTransaction.setUserId(transactionRequest.getUserId());
	ocvTransaction.setTransactionId(transactionId.toString());
	ocvTransaction.setTransactionType(transactionRequest.getTransactionType());
	ocvTransaction.setClientId(transactionRequest.getClientId());
	ocvTransaction.setTransactionReference(transactionRequest.getTransactionReference());
	ocvTransaction.setAccountNumber(transactionRequest.getAccountNumber());
	ocvTransaction.setCardData(transactionRequest.getCardData());
	ocvTransaction.setCardExpiryDate(transactionRequest.getCardExpiryDate());
	ocvTransaction.setTotalAmount(transactionRequest.getTotalAmount());
	ocvTransaction.setClientType(transactionRequest.getClientType());
	ocvTransaction.setAuthorisationNumber(transactionRequest.getAuthorisationNumber());
	ocvTransaction.setCvv(transactionRequest.getCvv());
	ocvTransaction.setSourceSystem(transactionRequest.getSourceSystem());
	ocvTransaction.setSourceSystemReference(transactionRequest.getSourceSystemReference());

	LogUtil.log(this.getClass(), "ocvTransaction=" + ocvTransaction);

	em.persist(ocvTransaction);

	return ocvTransaction;

    }

    public OCVTransaction updateOCVTransaction(OCVTransaction ocvTransaction) {
	return em.merge(ocvTransaction);
    }

    public OCVTransaction getOCVTransaction(String transactionId) {
	return em.find(OCVTransaction.class, transactionId);
    }

    public Collection<OCVTransaction> getOCVTransactionsBySSRef(String sourceSystemRef) {
	Query query = em.createQuery("from OCVTransaction e where e.sourceSystemReference = ?1");
	query.setParameter(1, sourceSystemRef);
	List<OCVTransaction> r = query.getResultList();
	return r;
    }

    private OCVTransaction updateOCVTransaction(OCVTransaction ocvTransaction, OCVTransactionResponse transactionResponse) throws GenericException {
	LogUtil.log(this.getClass(), "updateOCVTransaction");

	ocvTransaction.setResult(transactionResponse.getResult());
	ocvTransaction.setResponseCode(transactionResponse.getResponseCode());
	ocvTransaction.setResponseText(transactionResponse.getResponseText());

	LogUtil.log(this.getClass(), "ocvTransaction=" + ocvTransaction);

	return em.merge(ocvTransaction);

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OCVTransactionResponse createPurchase(OCVTransactionRequest transactionRequest) throws GenericException {
	if (!transactionRequest.getTransactionType().equals(OCVTransactionRequest.TRANSACTION_TYPE_PURCHASE)) {
	    throw new GenericException("Only the purchase transaction type is supported - " + transactionRequest.getTransactionType() + ".");
	}

	OCVTransaction ocvTransaction = createOCVTransaction(transactionRequest);
	OCVTransactionResponse transactionResponse = sendTransactionRequest(transactionRequest);
	transactionResponse.setTransactionId(ocvTransaction.getTransactionId());// unique
										// id
										// for
										// ocv
										// transaction
	updateOCVTransaction(ocvTransaction, transactionResponse);
	LogUtil.log(this.getClass(), "transactionResponse=" + transactionResponse);
	if (transactionResponse.isApproved()) {
	    return transactionResponse;
	} else if (transactionResponse.isIncomplete()) {
	    // send a status request
	    transactionRequest.setTransactionType(transactionRequest.TRANSACTION_TYPE_STATUS_REQUEST);
	    transactionResponse = sendTransactionRequest(transactionRequest);
	    updateOCVTransaction(ocvTransaction, transactionResponse);
	    if (transactionResponse.isApproved()) {
		return transactionResponse;
	    } else {
		// try again
		if (transactionResponse.getResponseCode().equals(OCVTransactionResponse.RESPONSE_CODE_RECORD_NOT_FOUND)) {
		    transactionRequest.setTransactionType(transactionRequest.TRANSACTION_TYPE_PURCHASE);
		    transactionResponse = sendTransactionRequest(transactionRequest);
		    updateOCVTransaction(ocvTransaction, transactionResponse);
		    if (transactionResponse.isApproved()) {
			return transactionResponse;
		    } else {
			throw new GenericException("Unable to record transaction - " + transactionResponse.getResponseCode() + ": " + transactionResponse.getResponseText());
		    }
		} else {
		    throw new GenericException("Unable to record transaction - " + transactionResponse.getResponseCode() + ": " + transactionResponse.getResponseText());
		}
	    }
	} else {
	    throw new GenericException("Unable to record transaction - " + transactionResponse.getResponseCode() + ": " + transactionResponse.getResponseText());
	}
    }

    public OCVTransactionResponse sendTransactionRequest(OCVTransactionRequest transactionRequest) throws GenericException {

	StringBuffer response = new StringBuffer();
	String server;
	String port;
	try {
	    server = FileUtil.getProperty("master", OCV_SERVER);
	    port = FileUtil.getProperty("master", OCV_PORT);
	} catch (Exception e1) {
	    throw new GenericException(e1);
	}
	try {
	    LogUtil.log(this.getClass(), "server=" + server);
	    LogUtil.log(this.getClass(), "port=" + port);
	    // properties file
	    Socket socket = new Socket(server, Integer.parseInt(port));
	    OutputStream output = socket.getOutputStream();
	    InputStream input = socket.getInputStream();
	    String transactionRequestString = transactionRequest.toRequestString();
	    System.out.println("transactionRequestString=" + transactionRequestString);

	    output.write(transactionRequestString.getBytes());
	    output.flush();
	    int c;
	    String tmp = "";
	    final int RESPONSE_LENGTH = 136;
	    int length = 0;
	    boolean read = true;
	    while (read) {
		c = input.read();
		length++;
		response.append((char) c);
		if (input.available() == 0) {
		    read = false;
		}
	    }
	    input.close();
	    output.close();
	    socket.close();
	} catch (UnknownHostException e) {
	    throw new GenericException(e);
	} catch (IOException e) {
	    throw new GenericException(e);
	}
	OCVTransactionResponse transactionResponse = new OCVTransactionResponse(response.toString());
	return transactionResponse;
    }

}
