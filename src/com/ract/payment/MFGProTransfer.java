/*
 *  @(#)MFGProTransfer.java
 *
 */
package com.ract.payment;

import java.rmi.RemoteException;

import com.ract.common.SourceSystem;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.payment.finance.FinanceMgr;
import com.ract.util.DateTime;

/**
 * Cron job calls this program to transfer a (summary of) postings to MFGPro
 * 
 * @author lewisg
 * @created 1 August 2002
 */
public class MFGProTransfer {

    /**
     * <p>
     * Main method. Expects an array of three parameters:
     * </p>
     * 
     * <ol>
     * <li>jboss server URL
     * <li>source system -- a string representing the source system. "MEM"
     * should be used for membership, "INS" for insurance only postings for this
     * source system will be transferred
     * <li>incomePostings -- a boolean indicating whether postings should
     * created to transfer from uneared to income. If true these posting will be
     * created and journalled after the new postings have been journalled.
     * </ol>
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args) {
	System.out.println("MFGPro Transfer initiated " + new DateTime().formatLongDate());
	try {

	    // check that we get the expected arguments
	    if (args.length < 3) {
		throw new PaymentException("Wrong number of arguments to MFGProTransfer");
	    }
	    String providerURL = args[0];
	    String sourceSystemString = args[1];
	    boolean incomePostings = (Boolean.valueOf(args[2])).booleanValue();
	    DateTime effectiveDate = null;
	    DateTime overrideDate = null;
	    if (args.length == 4) {
		if (args[3] != null) {
		    effectiveDate = new DateTime(args[3]);
		}
	    }
	    if (args.length == 5) {
		if (args[4] != null) {
		    overrideDate = new DateTime(args[4]);
		}
	    }

	    SourceSystem sourceSystem = SourceSystem.getSourceSystem(sourceSystemString);
	    // explicitly set the initial context
	    String url[] = providerURL.split(":");
	    RACTPropertiesProvider.setContextURL(url[0], Integer.parseInt(url[1]));
	    FinanceMgr mgr = PaymentEJBHelper.getFinanceMgr();

	    System.out.println("sourceSystem = " + sourceSystem);
	    System.out.println("incomePostings = " + incomePostings);
	    System.out.println("effectiveDate = " + effectiveDate);
	    System.out.println("overrideDate = " + overrideDate);

	    try {
		mgr.transferJournals(sourceSystem, incomePostings, effectiveDate, overrideDate);

	    } catch (RemoteException e) {
		e.printStackTrace();
		System.err.println("Payment System RemoteException " + e.getMessage());
	    } catch (PaymentException e) {
		e.printStackTrace();
		System.err.println("Payment System PaymentException " + e.getMessage());
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    System.err.println("Payment System Exception " + e.getMessage());
	}
    }

}
