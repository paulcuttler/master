package com.ract.payment;

import java.text.SimpleDateFormat;

import com.opensymphony.xwork2.ActionSupport;
import com.ract.util.DateTime;
import com.ract.util.Interval;

public class ShowSearchApprovedVouchersAction extends ActionSupport {

    private String startDate;

    public String getStartDate() {
	return startDate;
    }

    public void setStartDate(String startDate) {
	this.startDate = startDate;
    }

    public String getEndDate() {
	return endDate;
    }

    public void setEndDate(String endDate) {
	this.endDate = endDate;
    }

    private boolean unpaid;

    public boolean isUnpaid() {
	return unpaid;
    }

    public void setUnpaid(boolean unpaid) {
	this.unpaid = unpaid;
    }

    private String endDate;

    /**
     * @return
     */
    public String execute() {
	DateTime sDate = new DateTime().getDateOnly();
	DateTime eDate = null;
	try {
	    eDate = sDate.add(new Interval(0, 0, 1, 0, 0, 0, 0));
	} catch (Exception e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	startDate = format.format(sDate);
	endDate = format.format(eDate);
	setUnpaid(true);
	return SUCCESS;
    }
}