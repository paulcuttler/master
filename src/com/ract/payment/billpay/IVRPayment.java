package com.ract.payment.billpay;

import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

/**
 * IVR Payment
 * 
 * @author Levon Kara
 * @created 31 July 2002
 * @version 1.0
 */
// public abstract class IVRPayment
public class IVRPayment extends ElectronicPayment {
    // Some constants for Receptor
    public final static String IVR_NAME = "IVR";
    public final static String IVR_LOCATION = "IVR";
    public final static String IVR_BRANCH = "IVR";
    public final static String IVR_TILL = "OL1";
    public final static String IVR_PAYTYPE = "IVR";

    private String ivrReceiptNumber;

    private String bankReceiptNumber;

    // private StringBuffer status = new StringBuffer();

    private String referenceId;

    private String paymentSystem;

    /**
     * Constructor for the IVRPayment object
     */
    public IVRPayment() {
	super();
	super.addStatus("");
    }

    public IVRPayment(String ppn, DateTime rcvd, double amt, String ir, String br, String cc) {
	super.setCustomerReferenceNumber(ppn);
	super.setDateReceived(rcvd);
	this.ivrReceiptNumber = ir;
	this.bankReceiptNumber = br;
	super.setAmount(amt);
	super.setCardNumber(cc);
	super.setInputData(null);
	super.addStatus("");
	this.referenceId = null;

    }

    public IVRPayment(String l) {
	this.inputData = l;

    }

    public IVRPayment(Node epNode) throws SystemException {

	if (epNode == null /*
			    * ||
			    * !epNode.getNodeName().equals(WRITABLE_CLASSNAME)
			    */) {
	    throw new SystemException("Failed to create ElectronicPayment from XML node. The node is not valid.");
	}
	LogUtil.debug(this.getClass(), "Loading transaction node 1.");
	// Initialise the lists

	NodeList elementList = epNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    LogUtil.debug(this.getClass(), "Loading node 2.");
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("ElectronicPayment".equals(nodeName) && writableNode != null) {
		ElectronicPayment ep = new ElectronicPayment(writableNode);
		// set internal attributes
		this.setAmount(ep.getAmount());
		this.setCardNumber(ep.getCardNumber());
		this.setClientNumber(ep.getClientNumber());
		this.setCustomerReferenceNumber(ep.getCustomerReferenceNumber());
		this.setDateOfPayment(ep.getDateOfPayment());
		this.setDateReceived(ep.getDateReceived());
		this.setInputData(ep.getInputData());
		this.setRactReceiptNumber(ep.getRactReceiptNumber());
		this.setReferenceId(ep.getReferenceId());
		this.setSeqNumber(ep.getSeqNumber());
		this.setSourceSystem(ep.getSourceSystem());
		this.setStatus(ep.getStatus());
		this.setElectronicOperatorDetails(ep.getUserId());

	    } else if ("IvrExtraData".equals(nodeName) && writableNode != null) {
		// next level down
		NodeList selectList = elementNode.getChildNodes();
		// System.out.println("exData 1");
		if (selectList != null) {
		    for (int j = 0; j < selectList.getLength(); j++) {
			Node node = selectList.item(j);
			String nName = node.getNodeName();
			String vText = node.hasChildNodes() ? node.getFirstChild().getNodeValue() : null;
			// System.out.println("exData 2"+nName);
			if ("BankReceipt".equals(nName) && vText != null) {
			    this.bankReceiptNumber = vText;
			} else if ("IvrReceipt".equals(nName) && vText != null) {
			    this.ivrReceiptNumber = vText;
			}

		    }
		}

	    }
	}
    }

    //
    // Getters and Setters
    //
    // =============== GETTERS ====================//

    /**
     * Gets the phonePayNumber attribute of the IVRPayment object
     * 
     * @return The phonePayNumber value
     */
    public String getPhonePayNumber() {
	return super.getCustomerReferenceNumber();
    }

    /**
     * Gets the date received attribute of the IVRPayment object
     * 
     * @return The dateReceived value
     */
    public DateTime getDateReceived() {
	return super.getDateReceived();
    }

    /**
     * Gets the datePaid attribute of the IVRPayment object
     * 
     * @return The datePaid value
     */
    public DateTime getDatePaid() {
	return super.getDateOfPayment();
    }

    /**
     * Gets the bankReceiptNumber attribute of the IVRPayment object
     * 
     * @return The bankReceiptNumber value
     */
    public String getBankReceiptNumber() {
	return bankReceiptNumber;
    }

    /**
     * Gets the ivrReceiptNumber attribute of the IVRPayment object
     * 
     * @return The ivrReceiptNumber value
     */
    public String getIvrReceiptNumber() {
	return ivrReceiptNumber;
    }

    /**
     * Gets the creditCardNumber attribute of the IVRPayment object
     * 
     * @return The cCNumber value
     */
    public String getCreditCardNumber() {
	return super.getCardNumber();
    }

    /**
     * Gets the referenceId attribute of the IVRPayment object
     * 
     * @return The referenceId value
     */
    public String getInputLine() {
	return super.getInputData();
    }

    /**
     * Gets the source system abbreviation of the IVRPayment object
     * 
     * @return The source system abbreviation
     */
    public String getSourceSystemAbbreviation() {
	return super.getSourceSystem().getAbbreviation();
    }

    /**
     * get the OI Payable Client Number
     * 
     * @return opClientNUmber
     */
    public Integer getOpClientNumber() {
	return new Integer(super.getClientNumber());
    }

    /**
     * get the OI Payable Sequence Number
     * 
     * @return opSequenceNumber
     */
    public Integer getOpSequenceNumber() {
	return new Integer(super.getSeqNumber());
    }

    public String getPaymentSystem() {
	return paymentSystem;
    }

    //
    // =============== SETTERS ====================//
    /**
     * Sets the phonePayNumber attribute of the IVRPayment object
     * 
     * @param pc
     *            The new phonePayNumber value
     */
    public void setPhonePayNumber(String pc) {
	super.setCustomerReferenceNumber(pc);
    }

    /**
     * Sets the datePaid attribute of the IVRPayment object
     * 
     * @param dp
     *            The new datePaid value
     */
    public void setDatePaid(DateTime dp) {
	super.setDateOfPayment(dp);
    }

    /**
     * Sets the bankReceiptNumber attribute of the IVRPayment object
     * 
     * @param bpr
     *            The new bankReceiptNumber value
     */
    public void setBankReceiptNumber(String bpr) {
	bankReceiptNumber = bpr;
    }

    /**
     * Sets the ivrReceiptNumber attribute of the IVRPayment object
     * 
     * @param br
     *            The new ivrReceiptNumber value
     */
    public void setIvrReceiptNumber(String br) {
	ivrReceiptNumber = br;
    }

    /**
     * Sets the creditCardNumber attribute of the IVRPayment object
     * 
     * @param ccn
     *            The new creditCardNumber value
     */
    public void setCreditCardNumber(String ccn) {
	super.setCardNumber(ccn);
    }

    /**
     * Sets the status attribute of the IVRPayment object
     * 
     */
    public void setStatus(String st) {
	addStatus(st);
    }

    /**
     * Sets the referenceId attribute of the IVRPayment object
     * 
     */
    public void setInputLine(String ref) {
	super.setInputData(ref);
    }

    /**
     * Set the source system
     */
    public void setSourceSystem(String ss) {
	try {
	    super.setSourceSystem(SourceSystem.getSourceSystem(ss));
	} catch (Exception e) {
	    addStatus(e.getMessage());
	}
    }

    /**
     * set the OI Payable Client Number
     */
    public void setOpClientNumber(Integer clientNo) {
	super.setClientNumber(clientNo.toString());
    }

    /**
     * set the OI Payable Sequence Number
     */
    public void setOpSequenceNumber(Integer seqNumber) {
	super.setSeqNumber(seqNumber.toString());
    }

    public void setPaymentSystem(String paymentSystem) {
	this.paymentSystem = paymentSystem;
    }

    /**
     * set the appropriate IVR fields with data from the Pending Fee
     */
    public void setIVRWithPendingFee(PendingFee pf) {
	try {
	    if (super.getSourceSystem() == null) {
		super.setSourceSystem(SourceSystem.getSourceSystem(pf.getSourceSystem()));
	    } else if (!this.getSourceSystemAbbreviation().equalsIgnoreCase(pf.getSourceSystem())) {
		this.addStatus("Source System conflict. IVR " + this.getSourceSystemAbbreviation() + ". Pending Fee " + pf.getSourceSystem());
	    }
	    setReferenceId(pf.getSeqNumber().toString());
	    this.setOpClientNumber(pf.getClientNumber());
	    this.setOpSequenceNumber(pf.getOpSequenceNumber());
	} catch (Exception e) {
	    this.addStatus("Pending Fee add");
	}
    }

    /**
     * Description of the Method
     * 
     * @todo review this method
     * @return Description of the Return Value
     */
    public String toString() {
	StringBuffer s = new StringBuffer();

	SourceSystem ss = super.getSourceSystem();
	String sourceSystem = (ss == null ? "UNK" : ss.getAbbreviation());

	s.append(super.getCustomerReferenceNumber() + ",");
	s.append(super.getDateOfPayment() + ",");
	s.append(bankReceiptNumber + ",");
	s.append(ivrReceiptNumber + ",");
	if (super.getCardNumber().equals("?")) {
	    String iData = super.getInputData();
	    String inputData[] = iData.split(",");
	    if (inputData[7] != null) {
		s.append(inputData[7] + ",");
	    } else {
		s.append("?,");
	    }
	} else {
	    s.append(super.getCardNumber() + ",");
	}
	s.append(super.getAmount() + ",");
	s.append(StringUtil.makeSpaces(referenceId) + ",");
	s.append(super.getDateReceived() + ",");
	s.append(super.getClientNumber() + ",");
	s.append(super.getSeqNumber() + ",");
	s.append(sourceSystem + ",");
	// report written to ignore this column
	// s.append(getUserId() + ",");
	s.append(StringUtil.makeSpaces(super.getRactReceiptNumber()) + ",");
	s.append((getStatus() == null ? "null:" : StringUtil.replace(getStatus(), ":", "")));
	// String x = this.toString();
	return s.toString();
    }

    public void constructTransactionReferenceNumber() {
	setTransactionReferenceNumber(this.bankReceiptNumber + "/" + this.ivrReceiptNumber);
    }

    public String toXML() {
	StringBuffer s = new StringBuffer();
	/**
	 * @todo change to DOM
	 */
	s.append("<IvrPayment>");
	s.append(super.toXML());
	s.append("<IvrExtraData>");
	s.append("<BankReceipt>" + bankReceiptNumber + "</BankReceipt>");
	s.append("<IvrReceipt>" + ivrReceiptNumber + "</IvrReceipt>");
	s.append("</IvrExtraData>");
	s.append("</IvrPayment>");

	return s.toString();
    }

    public String lineAsIs() {
	return super.lineAsIs() + "," + this.getStatus();
    }

    public String getColumnNames() {
	StringBuffer cn = new StringBuffer();
	cn.append("\"PhonePay Number\"" + ",");
	cn.append("\"Date Received\"" + ",");
	cn.append("\"Bank Receipt\"" + ",");
	cn.append("\"IVR Receipt\"" + ",");
	cn.append("\"Credit Card\"" + ",");
	cn.append("\"Amount\"" + ",");
	cn.append("\"Membership Id\"" + ",");
	cn.append("\"Date Paid\"" + ",");
	cn.append("\"Client Number\"" + ",");
	cn.append("\"Sequence Number\"" + ",");
	cn.append("\"Source System\"" + ",");
	cn.append("\"RACT Receipt\"" + ",");
	cn.append("\"Status\"" + "\n");

	return cn.toString();

    }

    /**
     * Add to the current processing status
     */
    public void addStatus(String stat) {
	if (!StringUtil.isNull(stat)) {
	    super.addStatus(stat);
	}
    }

    /******************************************************************
     * Notify
     ******************************************************************/
    // public abstract void notifySourceSystemOfPayment()
    public void notifySourceSystemOfPayment() throws RemoteException

    {

    }

}
