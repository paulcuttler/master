package com.ract.payment.billpay;

public class BillPayFileFormatException extends Exception {
    public BillPayFileFormatException(String s) {
	super(s);
    }
}