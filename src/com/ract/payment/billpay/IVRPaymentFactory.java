package com.ract.payment.billpay;

/**
 * Title:        Master Project
 * Description:  Brings all of the projects together into one master project for deployment.
 * Copyright:    Copyright (c) 2003
 * Company:      RACT
 * @author
 * @version 1.0
 */

import com.ract.common.SourceSystem;

public class IVRPaymentFactory {
    public static IVRPayment getIVRPayment(String sourceSystem) throws BillPayException {

	if (sourceSystem.equalsIgnoreCase(SourceSystem.IVR_MEMBERSHIP.getAbbreviation())) {
	    return new IVRMembershipPayment();
	} else if (sourceSystem.equalsIgnoreCase(SourceSystem.IVR_INSURANCE.getAbbreviation())) {
	    return new IVRInsurancePayment();
	} else if (sourceSystem.equalsIgnoreCase("BASE")) {
	    return new IVRPayment();
	}

	else {
	    System.out.println("IVRPaymentFactory - Invalid Source System " + sourceSystem);
	    throw new BillPayException("Invalid Source System " + sourceSystem);
	}
    }
}
