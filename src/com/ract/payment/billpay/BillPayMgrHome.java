package com.ract.payment.billpay;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface BillPayMgrHome extends EJBHome {
    public BillPayMgr create() throws CreateException, RemoteException;
}
