package com.ract.payment.billpay;

/**
 * Title:        Master Project
 * Description:  Brings all of the projects together into one master project for deployment.
 * Copyright:    Copyright (c) 2003
 * Company:      RACT
 * @author
 * @version 1.0
 */
import com.ract.common.SourceSystem;

public class IVRInsurancePayment extends IVRPayment {

    public IVRInsurancePayment() {
	super();
	super.setSourceSystem(SourceSystem.INSURANCE);
	super.setReferenceId("0000");
    }

    public void setPhonePayNumber(String pc) {
	super.setPhonePayNumber(pc);
	//
	// Set the client and sequence numbers for INSURANCE transactions
	//
	if (pc != null) {
	    if (pc.length() == 11) {
		this.setClientNumber(pc.substring(0, 6));
		this.setSeqNumber(pc.substring(6, 10));
	    }
	    if (pc.length() == 12) {
		this.setClientNumber(pc.substring(0, 7));
		this.setSeqNumber(pc.substring(7, 10));
	    }

	    this.setOpClientNumber(new Integer(this.getClientNumber()));
	    this.setOpSequenceNumber(new Integer(this.getSeqNumber()));
	}
    }

    // public void notifySourceSystemOfPayment()
    // throws RemoteException
    // {
    // //no action
    // }

    // public void receiptPayment(PaymentTransactionMgr payTxMgr)
    // throws RemoteException, RollBackException
    // {
    // String receiptNumber = payTxMgr.receiptIVRPayment(this);
    // this.setRactReceiptNumber(receiptNumber);
    // }

    // public String toXML ()
    // {
    // StringBuffer s = new StringBuffer();
    //
    // s.append("<IVRInsurancePayment>");
    // s.append(super.toXML());
    // s.append("</IVRInsurancePayment>");
    //
    // return s.toString();
    // }

}
