package com.ract.payment.billpay;

public class BillPayConstants {
    public final static String BillpayFTPHeaderIndicator = "1";
    public final static String BillpayFTPTrailerIndicator = "9";
    public final static String BillpayFTPDetailIndicator = "2";

    public final static String BillpayFTPFileDescriptor = "ACCOUNT";
    public final static String BillpayFTPBillType = "0";
    public final static String BillpayFTPFileVersion = "001";

    public final static String NewLine = "\n";
    public final static String FieldDelimiter = ",";
    // public BillPayConstants()
    // {
    //
    // }
}
