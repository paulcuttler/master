package com.ract.payment.billpay;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.Date;

import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.payment.PaymentEJBHelper;
import com.ract.util.DateTime;

/**
 * Update reader.
 * 
 * @author Levon Kara
 * @created 31 July 2002
 * @version 1.0
 */

public class BillPayUpdateReader {
    // BillPayMgrHome billPayMgrHome = null;
    //
    // BillPayMgr billPayMgr = null;

    private BillPayUpdateReader(String host, String port, String mode, String inputFile, String auditFile, String outputFile, String tmpFile, String receiptPaymentString, String processTransactionString) {
	boolean receiptPayment = false;
	boolean processTransaction = false;
	BillPayMgr billPayMgr = null;
	try {
	    if (receiptPaymentString == null || receiptPaymentString.equals("")) {
		receiptPayment = true;
	    } else {
		receiptPayment = receiptPaymentString.equalsIgnoreCase("Y");
	    }
	    if (processTransactionString == null || processTransactionString.equals("")) {
		processTransaction = true;
	    } else {
		processTransaction = processTransactionString.equalsIgnoreCase("Y");
	    }

	    RACTPropertiesProvider.setContextURL(host, Integer.parseInt(port));
	    billPayMgr = PaymentEJBHelper.getBillPayMgr();

	} catch (Exception ne) {
	    ne.printStackTrace();
	    System.out.println("Failed initializing access to Bill Pay Manager home interface: " + ne.getMessage());

	}

	try {
	    System.out.println(" ======================================================== ");
	    System.out.println(" Host                 " + host);
	    System.out.println(" Port                 " + port);
	    System.out.println(" Mode                 " + mode);
	    System.out.println(" Remittance file      " + inputFile);
	    System.out.println(" Audit file           " + auditFile);
	    System.out.println(" Extract file         " + outputFile);
	    System.out.println(" Temporary file       " + tmpFile);
	    System.out.println(" Receipting enabled   " + receiptPayment);
	    System.out.println(" Process transaction  " + processTransaction);
	    System.out.println(" Starting             " + new Date().toString());
	    System.out.println(" ======================================================== ");

	    if (mode.equalsIgnoreCase("Update") || mode.equalsIgnoreCase("Both")) {
		this.updateIvrPayments(inputFile, auditFile, receiptPayment, processTransaction, billPayMgr);
		System.out.println(" Update Finished " + new Date().toString());
	    }

	    if (mode.equalsIgnoreCase("Extract") || mode.equalsIgnoreCase("Both")) {
		billPayMgr.extractForIvr(outputFile, tmpFile);
		System.out.println(" Extract Finished " + new Date().toString());
	    }

	} catch (Exception e) {
	    System.out.println("BillPayUpdateReader +++++++ Exception " + e.getMessage());
	    System.out.println("BillPayUpdateReader +++++++ FINISHING");
	}

    }

    public void updateIvrPayments(String paymentFile, String auditFile, boolean receiptPayment, boolean processPayment, BillPayMgr billPayMgr) throws RemoteException {
	System.out.println("BILLPAY Audit file ... " + auditFile);
	System.out.println("BILLPAY Payment file . " + paymentFile);

	BufferedReader in = null;
	PrintWriter audit = null;
	// String printLine = null;

	try {
	    // Open the BillPay file from telstra
	    in = new BufferedReader(new FileReader(paymentFile));
	    audit = new PrintWriter(new BufferedWriter(new FileWriter(auditFile)));

	    String line = "";

	    // audit.println(XMLHelper.getXMLHeaderLine());
	    // audit.println("<IvrPaymentAudit>");
	    // audit.println("<inputFile>" + paymentFile + "</inputFile>");
	    // audit.println("<processDate>" + new DateTime().formatLongDate() +
	    // "</processDate>");

	    System.out.println("" + billPayMgr);
	    System.out.println("" + audit);

	    // Process the BillPay file line by line
	    while ((line = in.readLine()) != null) {
		System.out.println("BILLPAY  processing " + line + "/" + receiptPayment + "/" + processPayment);
		audit.println(billPayMgr.processIVRPayment(line, receiptPayment, processPayment));
	    }
	    //
	    // ALL OVER RED ROVER
	    //

	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println("Fatal error has prevented the completion of the processing the billpay file. " + e);
	    throw new RemoteException("Fatal error has prevented the completion of the processing the billpay file.", e);
	} finally {
	    try {
		// audit.println("</IvrPaymentAudit>");
		audit.close();
		in.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	System.out.println("BILLPAY Update Finished " + new DateTime().formatLongDate());

    }

    /**
     * The main program for the BillPayUpdateReader class
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args) {

	if (args.length < 8) {
	    System.out.println("Not enough command line arguments.");
	    System.out.println("Usage: BillPayUpdateReader [localhost] [1099] [Update|Extract|Both] [fromTelstra] [audit] [toTelstra] [temporary] [receipt Y/N] [process transaction Y/N]");
	    return;
	}
	BillPayUpdateReader bp = new BillPayUpdateReader(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
    }

}
