package com.ract.payment.billpay;

import com.ract.common.GenericException;

/**
 * Bill pay exception
 * 
 * @author Levon Kara
 * @version 1.0
 */

public class BillPayException extends GenericException {
    public BillPayException() {
	super();
    }

    public BillPayException(String s) {
	super(s);
    }

    public BillPayException(Exception e) {
	super(e);
    }

    public BillPayException(String s, Exception e) {
	super(s, e);
    }
}