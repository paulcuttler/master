package com.ract.payment.billpay;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

/**
 * Description of the Interface
 * 
 * @author Levon Kara
 * @created 1 August 2002
 */
public interface BillPayMgr extends EJBObject {
    /**
     * Update the memberships paid by IVR
     * 
     * @param payment_file
     *            The file of payments returned from IVR
     * @param audit_file
     *            The audit trail
     * 
     * @exception RemoteException
     *                Description of the Exception
     * @exception BillPayException
     *                Description of the Exception
     */
    public String processIVRPayment(String line, boolean receiptPayment, boolean processTransaction) throws RemoteException;

    /**
     * Extract the memberships that are to be sent to IVR for possible payment
     * 
     * @param ivr_file
     *            The text file of payments to be sent to IVR
     * 
     * @exception RemoteException
     *                Description of the Exception
     * @exception BillPayException
     *                Description of the Exception
     */
    public void extractForIvr(String ivrFile, String tmpFile) throws RemoteException;

    /**
     * Find the pending fee
     * 
     * @param pfId
     *            The pending fee ide
     * 
     * @exception RemoteException
     *                Description of the Exception
     * @exception BillPayException
     *                Description of the Exception
     */

    public PendingFee getRenewalData(Integer membershipId, String sourceSystem, String noticeTypeList) throws RemoteException;

    // public PendingFee getRenewalData(String billPayNumber)
    // throws RemoteException;

}
