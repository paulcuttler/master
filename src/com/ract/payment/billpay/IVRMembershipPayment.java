package com.ract.payment.billpay;

/**
 * Title:        Master Project
 * Description:  Brings all of the projects together into one master project for deployment.
 * Copyright:    Copyright (c) 2003
 * Company:      RACT
 * @author
 * @version 1.0
 */

import java.rmi.RemoteException;

import com.ract.common.SourceSystem;
import com.ract.membership.MembershipDocument;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipTransactionMgr;

public class IVRMembershipPayment extends IVRPayment {

    public IVRMembershipPayment() {
	this.setSourceSystem(SourceSystem.MEMBERSHIP);
    }

    public void notifySourceSystemOfPayment() throws RemoteException {

	try {
	    MembershipDocument mdVO = MembershipEJBHelper.getMembershipMgr().getMembershipDocument(new Integer(getReferenceId()));
	    setReferenceId(mdVO.getMembershipId().toString());

	    MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
	    memTransMgr.notifyPendingFeePaid(this);
	} catch (Exception e) {
	    throw new RemoteException(e.getMessage());
	}
    }

}
