package com.ract.payment.billpay;

import java.io.Serializable;

import com.ract.util.DateTime;

/**
 * A fee that is yet to be paid.
 * 
 * @hibernate.class table="pt_pending_fee" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */

public class PendingFee implements Serializable {
    private String pendingFeeID;

    private DateTime feeEffectiveDate = null;

    private DateTime feeExpiryDate = null;

    private String sourceSystem = null;

    @Override
    public String toString() {
	return "PendingFee [amount=" + amount + ", bankReceipt=" + bankReceipt + ", billpayReceipt=" + billpayReceipt + ", clientNumber=" + clientNumber + ", creditCardNumber=" + creditCardNumber + ", datePaid=" + datePaid + ", feeEffectiveDate=" + feeEffectiveDate + ", feeExpiryDate=" + feeExpiryDate + ", opSequenceNumber=" + opSequenceNumber + ", paymentSystem=" + paymentSystem + ", pendingFeeID=" + pendingFeeID + ", refNumber=" + refNumber + ", seqNumber=" + seqNumber + ", sourceSystem=" + sourceSystem + ", sourceSystemReferenceNo=" + sourceSystemReferenceNo + "]";
    }

    // the following two parameters are poorly named
    // usage: in membership system refNumber = 9000 - prefix
    // seqNumber = documentId of associated renewal document -
    // transactionReference
    private Long refNumber;

    private Long seqNumber;

    private double amount;

    private DateTime datePaid = null;

    private String billpayReceipt = null;

    private String bankReceipt = null;

    private String creditCardNumber = null;

    private Integer sourceSystemReferenceNo = null;

    /**
     * Keys into OCR receipting system clientNumber = oi-payable.client-no
     * sequenceNo = oi-payable.opsequenceNo
     */
    private Integer clientNumber = null;

    private Integer opSequenceNumber = null;

    private String paymentSystem;

    public PendingFee() {
    }

    public PendingFee(String pfId, DateTime fEffDate, DateTime fExpDate, String ss, Integer sourceSystemReferenceNo, Long rn, Long sn, double amt, DateTime dp, String bpReceipt, String bankReceipt, String ccNum, Integer clientNumber, Integer opSequenceNumber, String paymentSystem) {

	this.pendingFeeID = pfId;
	this.feeEffectiveDate = fEffDate;
	this.feeExpiryDate = fExpDate;
	this.sourceSystem = ss;
	this.refNumber = rn;
	this.seqNumber = sn;
	this.amount = amt;
	this.datePaid = dp;
	this.billpayReceipt = bpReceipt;
	this.bankReceipt = bankReceipt;
	this.creditCardNumber = ccNum;
	this.sourceSystemReferenceNo = sourceSystemReferenceNo;
	this.clientNumber = clientNumber;
	this.opSequenceNumber = opSequenceNumber;
	this.setPaymentSystem(paymentSystem);
    }

    public boolean isPaid() {
	return (this.getDatePaid() != null);
    }

    public PendingFee copy() {
	PendingFee pfVO = new PendingFee(this.pendingFeeID, this.feeEffectiveDate, this.feeExpiryDate, this.sourceSystem, this.sourceSystemReferenceNo, this.refNumber, this.seqNumber, this.amount, this.datePaid, this.billpayReceipt, this.bankReceipt, this.creditCardNumber, this.clientNumber, this.opSequenceNumber, this.paymentSystem);

	// pfVO.setReadOnly(false);
	return pfVO;
    }

    /*************************** Getter methods *****************************/
    /**
     * @hibernate.property
     * @hibernate.column name="client_number"
     */
    public Integer getClientNumber() {
	return this.clientNumber;
    }

    /**
     * This is the oi-payable sequence number in OCR and the pending_item_id in
     * Receptor.
     * 
     * @hibernate.property
     * @hibernate.column name="op_sequence_number"
     */
    public Integer getOpSequenceNumber() {
	return this.opSequenceNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="amount"
     */
    public double getAmount() {
	return amount;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="bank_rec"
     */
    public String getBankReceipt() {
	return bankReceipt;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="billpay_rec"
     */
    public String getBillpayReceipt() {
	return billpayReceipt;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="cc_number"
     */
    public String getCreditCardNumber() {
	return creditCardNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="date_paid"
     */
    public DateTime getDatePaid() {
	return datePaid;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="fee_effective_date"
     */
    public DateTime getFeeEffectiveDate() {
	return feeEffectiveDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="fee_expiry_date"
     */
    public DateTime getFeeExpiryDate() {
	return feeExpiryDate;
    }

    /**
     * @hibernate.id column="pending_fee_id" generator-class="assigned"
     * 
     * @return String
     */
    public String getPendingFeeID() {
	return pendingFeeID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="ref_number"
     */
    public Long getRefNumber() {
	return refNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="seq_number"
     */
    public Long getSeqNumber() {
	return seqNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="source_system"
     */
    public String getSourceSystem() {
	return sourceSystem;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="source_system_ref"
     */
    public Integer getSourceSystemReferenceNo() {
	return this.sourceSystemReferenceNo;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="payment_system"
     */
    public String getPaymentSystem() {
	return paymentSystem;
    }

    /*************************** Setter methods *****************************/
    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public void setOpSequenceNumber(Integer opSequenceNumber) {
	this.opSequenceNumber = opSequenceNumber;
    }

    public void setSourceSystemReferenceNo(Integer sourceSystemReferenceNo) {
	this.sourceSystemReferenceNo = sourceSystemReferenceNo;
    }

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public void setSeqNumber(Long seqNumber) {
	this.seqNumber = seqNumber;
    }

    public void setRefNumber(Long refNumber) {
	this.refNumber = refNumber;
    }

    public void setPendingFeeID(String pendingFeeId) {
	this.pendingFeeID = pendingFeeId;
    }

    public void setFeeExpiryDate(DateTime feeExpiryDate) {
	this.feeExpiryDate = feeExpiryDate;
    }

    public void setFeeEffectiveDate(DateTime feeEffectiveDate) {
	this.feeEffectiveDate = feeEffectiveDate;
    }

    public void setDatePaid(DateTime datePaid) {
	this.datePaid = datePaid;
    }

    public void setCreditCardNumber(String creditCardNumber) {
	this.creditCardNumber = creditCardNumber;
    }

    public void setBillpayReceipt(String billPayReceipt) {
	this.billpayReceipt = billPayReceipt;
    }

    public void setBankReceipt(String bankReceipt) {
	this.bankReceipt = bankReceipt;
    }

    public void setAmount(double amount) {
	this.amount = amount;
    }

    public void setPaymentSystem(String paymentSystem) {
	this.paymentSystem = paymentSystem;
    }

}
