package com.ract.payment.billpay;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.ract.common.CommonConstants;
import com.ract.common.DataSourceFactory;
import com.ract.common.SourceSystem;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.payment.electronicpayment.ElectronicPaymentProcessor;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

/**
 * Bill pay
 * 
 * @author Levon Kara
 * @created 1 August 2002
 */
public class BillPayMgrBean extends ElectronicPaymentProcessor implements SessionBean {
    private SessionContext sessionContext;

    /**
     * 
     * @param line
     * @return
     * @throws RemoteException
     */

    public String processIVRPayment(String line, boolean receiptPayment, boolean processTransaction) throws RemoteException {

	String retStr = null;

	IVRPayment ivr = parseInputLine(line);

	if (ivr != null && ivr.isOK())

	{
	    ivr = (IVRPayment) super.processPayment((ElectronicPayment) ivr, receiptPayment, processTransaction);
	    retStr = ivr.toString();
	} else {
	    retStr = headerOrTotalLine(ivr.getStatus()) ? ivr.lineAsIs() : ivr.toString();
	}

	return retStr;

    }

    /**
     * extractForIvr
     * 
     * Copy the existing billpay.dat file upto the trailer record and then
     * insert the Roadside records from the pending fee table
     */

    public void extractForIvr(String ivrFile, String tmpFile) throws RemoteException {
	LogUtil.log(this.getClass(), "BILLPAY Insurance Extract Started " + new DateTime().formatLongDate());
	PaymentTransactionMgr payTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
	String ivrUnixFile = payTxMgr.extractInsuranceIVR();
	LogUtil.log(this.getClass(), "BILLPAY Insurance Extract Finished " + new DateTime().formatLongDate());

	LogUtil.log(this.getClass(), "BILLPAY Using " + tmpFile + " Unix file is " + ivrUnixFile);

	String inLine = "";
	// BillPayMgr mgr = PaymentEJBHelper.getBillPayMgr();

	String trailerRecord = "";

	try {
	    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(tmpFile)));
	    BufferedReader in = new BufferedReader(new FileReader(ivrFile));

	    LogUtil.log(this.getClass(), "BILLPAY Copy billpay.dat: " + ivrUnixFile);
	    int cnt = 0;
	    try {

		while ((inLine = in.readLine()) != null) {
		    cnt++;

		    if ((cnt % 1000) == 0) {
			LogUtil.log(this.getClass(), "BILLPAY ++++ " + cnt + " processed ");
		    }

		    // look for trailer record
		    if (inLine.startsWith("2")) {
			// set the trailer record
			trailerRecord = inLine;
		    } else {
			// write record to file
			out.println(inLine);
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    LogUtil.log(this.getClass(), cnt + " lines copied");
	    //
	    // Now add the RoadSide stuff
	    //
	    System.out.println("BILLPAY Extracting IVR Payments");
	    Vector payableFees = this.getPayableFeeList();
	    Iterator i = payableFees.iterator();
	    cnt = 0;

	    while (i.hasNext()) {
		PayableFee pf = (PayableFee) i.next();

		// Add the payable fee to the file
		out.print("1");
		out.print(pf.getPaymentCode());
		out.print(formatAmountString(pf.getAmount()));
		out.println();
		cnt++;
	    }
	    //
	    // Write the trailer record
	    //
	    out.print(trailerRecord);
	    //
	    // Close the temporary file
	    //
	    out.close();
	    in.close();
	    DateTime d = new DateTime();
	    LogUtil.log(this.getClass(), "BILLPAY Roadside extract finished at " + d.formatLongDate());
	    LogUtil.log(this.getClass(), "BILLPAY " + cnt + " records extracted");

	} catch (Exception e) {
	    e.printStackTrace();
	}
	//
	// Now copy it to the right file
	//
	int cnt = 0;

	LogUtil.log(this.getClass(), "BILLPAY Creating Billpay.dat " + new DateTime().formatLongDate());
	try {
	    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(ivrFile)));
	    BufferedReader in = new BufferedReader(new FileReader(tmpFile));

	    while ((inLine = in.readLine()) != null) {
		out.println(inLine);
		cnt++;
	    }

	    LogUtil.log(this.getClass(), "BILLPAY finished " + new DateTime().formatShortDate());
	    //
	    // Close the temporary file
	    //
	    out.close();
	    in.close();

	    // New format for the
	    LogUtil.log(this.getClass(), "BILLPAY IVR extract finished " + cnt + " records " + new DateTime().formatLongDate());
	    //
	    // Now copy it to the right file
	    //

	} catch (Exception e) {
	    e.printStackTrace();
	}

	// Create the file in the FTP format
	//
	cnt = 0;

	LogUtil.log(this.getClass(), "BILLPAY Creating lookup0.csv " + new DateTime().formatLongDate());
	try {
	    File f = new File(ivrFile);
	    File ftpIVRFile = new File(f.getParent(), "lookup0.csv");

	    StringBuffer sb = new StringBuffer();

	    BufferedReader in = new BufferedReader(new FileReader(f.getPath()));

	    String now = DateUtil.formatDate(new java.util.Date(), "yyyyMMddHHmmss");

	    // Header
	    sb.append(BillPayConstants.BillpayFTPHeaderIndicator + BillPayConstants.FieldDelimiter + BillPayConstants.BillpayFTPFileDescriptor + BillPayConstants.FieldDelimiter + BillPayConstants.BillpayFTPFileVersion + BillPayConstants.FieldDelimiter);

	    sb.append(now + BillPayConstants.NewLine);

	    while ((inLine = in.readLine()) != null) {
		if (inLine.startsWith("1")) {
		    sb.append(BillPayConstants.BillpayFTPDetailIndicator);
		    sb.append(BillPayConstants.FieldDelimiter);

		    sb.append(inLine.substring(1, 12)); // pay reference number

		    sb.append(BillPayConstants.FieldDelimiter);
		    sb.append(BillPayConstants.BillpayFTPBillType); // Bill_type_number
		    sb.append(BillPayConstants.FieldDelimiter);

		    sb.append(inLine.substring(12)); // amount

		    sb.append(BillPayConstants.NewLine);
		    cnt++;
		}
	    }

	    // Trailer
	    sb.append(BillPayConstants.BillpayFTPTrailerIndicator + BillPayConstants.FieldDelimiter + BillPayConstants.BillpayFTPFileDescriptor + BillPayConstants.FieldDelimiter + BillPayConstants.BillpayFTPFileVersion + BillPayConstants.FieldDelimiter);

	    sb.append(now);
	    sb.append(BillPayConstants.FieldDelimiter);
	    sb.append(cnt); // record count

	    FileUtil.writeFile(ftpIVRFile.getPath(), sb.toString());

	    LogUtil.log(this.getClass(), "BILLPAY finished " + new DateTime().formatShortDate());
	    //
	    // Close the temporary file
	    //
	    in.close();

	    // New format for the
	    LogUtil.log(this.getClass(), "BILLPAY FTP IVR extract finished " + cnt + " records " + new DateTime().formatLongDate());

	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    /**
     * Gets the payableFeeList attribute of the BillPayMgrBean object
     * 
     * @return The payableFeeList value
     * @exception RemoteException
     *                Description of the Exception
     * @exception BillPayException
     *                Description of the Exception
     */
    private Vector getPayableFeeList() throws RemoteException, BillPayException {
	Vector feeList = new Vector();
	DateTime d = new DateTime().getDateOnly();

	String sql = "SELECT pending_fee_id,ref_number,seq_number,amount";
	sql += " FROM PUB.\"pt_pending_fee\"";
	sql += " where ? <= ";
	sql += " fee_expiry_date and date_paid is null";
	sql += " and len(pending_fee_id) = 11"; // required
	Connection connection = null;
	PreparedStatement statement = null;
	String pendingFeeID = null;

	try {
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    statement.setDate(1, d.toSQLDate());
	    ResultSet rs = statement.executeQuery();

	    int count = 0;
	    while (rs.next()) {

		// new payable fee object
		PayableFee payF = new PayableFee();

		pendingFeeID = rs.getString(1);
		payF.setPaymentCode(rs.getString(1));
		Double amount = new Double(rs.getString(4));
		payF.setAmount(NumberUtil.roundDouble(amount.doubleValue(), 2));

		feeList.add(payF);

	    }
	} catch (Exception e) {

	    throw new BillPayException(e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return feeList;
    }

    // /**
    // * return a pending fee remote interface
    // */
    // public PendingFee getPendingFee(String pendingFeeId)
    // throws BillPayException
    // {
    // PendingFee pf = null;
    //
    // try
    // {
    // PendingFeeHome home = PaymentEJBHelper.getPendingFeeHome();
    // pf = home.findByPrimaryKey(pendingFeeId);
    // }
    // catch(ObjectNotFoundException o)
    // {
    // LogUtil.log(this.getClass(), "PendingFee getPendingFee ++++ " +
    // o.getMessage());
    // pf = null;
    // }
    // catch(Exception e)
    // {
    // throw new BillPayException(e);
    // }
    //
    // return pf;
    // }

    /**
     * parses a line from the BillPay file from telstra. If there were no
     * errors, it returns a BillPayUpdate object, otherwise a
     * BillPayFileFormatException is thrown
     */
    private IVRPayment parseInputLine(String line) {

	String lineTypeIndicator = "";
	String phonePayNumber = "";
	String bankReceipt = "";
	String ivrReceipt = "";
	String creditCardNumber = "";
	String sourceSystem = "";
	DateTime paymentDate = null;
	double amount = 0.0;
	String token = "";
	String custId = "";
	String seqNo = "";
	String paymentService = "";
	String paymentTime = "";

	boolean controlCharacter;

	StringBuffer parseStatus = new StringBuffer();
	//
	// Parse this Line
	//
	line = StringUtil.replace(line, ",,", ", ,");

	// StringTokenizer lineTokens = new StringTokenizer(line, ",");
	String lineTokens[] = line.split(",");

	if (lineTokens.length > 0) {
	    lineTypeIndicator = lineTokens[0];
	    //
	    // a "2" in the first character of the line indicates a payment line
	    //
	    controlCharacter = true;
	    try {
		controlCharacter = Character.isISOControl(lineTypeIndicator.charAt(0));
	    } catch (Exception ex) {
		controlCharacter = true;
		parseStatus.append(ex.getMessage());
	    }

	    if (controlCharacter) {
		parseStatus.append("Header Line");
	    } else if (lineTypeIndicator.equals("2")) {
		// Payment Line
		// Get date
		if (lineTokens.length > 1) {
		    token = lineTokens[1];
		    try {
			paymentDate = DateUtil.convertStringToDateTime(token);
		    } catch (java.text.ParseException pe) {
			parseStatus.append("Date format error ");
		    }
		} else {
		    parseStatus.append("No date field");
		}

		// Phone Payment Number
		if (lineTokens.length > 2) {
		    // Recalculate the PhonePayNumber to see if it is correct
		    phonePayNumber = lineTokens[2];
		    String ppn = validateReferenceNumber(phonePayNumber);
		    if (!ppn.equalsIgnoreCase("ok")) {
			parseStatus.append(ppn);
		    }
		} else {
		    parseStatus.append("No Phone Pay Number");
		}

		if (lineTokens.length > 3) {
		    token = lineTokens[3];
		} else {
		    parseStatus.append("No unknown empty field");
		}

		// Get amount
		if (lineTokens.length > 4) {
		    token = lineTokens[4];
		    if (NumberUtil.isNumeric(token)) {
			amount = (new Double(token).doubleValue() / 100);
		    } else {
			parseStatus.append("amount error");
		    }
		} else {
		    parseStatus.append("No amount field");
		}

		// Get Bank Receipt
		if (lineTokens.length > 5) {
		    bankReceipt = lineTokens[5];
		} else {
		    parseStatus.append("No bank receipt field");
		}

		// Get telstra receipt
		if (lineTokens.length > 6) {
		    ivrReceipt = lineTokens[6];
		} else {
		    parseStatus.append("No telstra receipt field");
		}

		// Get credit card number
		if (lineTokens.length > 7) {
		    creditCardNumber = lineTokens[7];
		} else {
		    parseStatus.append("No credit card field");
		}

		// get payment service used
		if (lineTokens.length > 8) {
		    paymentService = lineTokens[8];
		} else {
		    parseStatus.append("No payment service field");
		}

		// get time of payment
		if (lineTokens.length > 9) {
		    paymentTime = lineTokens[9];
		} else {
		    parseStatus.append("No payment time field");
		}

	    } else if (lineTypeIndicator.equals("3")) {
		// Total Line
		parseStatus.append("Total Line");
		token = lineTokens[1];
		if (NumberUtil.isNumeric(token)) {
		    amount = (new Double(token).doubleValue() / 100);
		} else {
		    parseStatus.append("total amount error");
		}

	    } else {
		parseStatus.append("Not a payment line");
		phonePayNumber = null;
	    }
	} else {
	    parseStatus.append("Not a payment Line");
	    phonePayNumber = null;
	}

	try {
	    sourceSystem = SourceSystem.getElectronicPaymentSourceSystem(phonePayNumber);

	    IVRPayment ivr = IVRPaymentFactory.getIVRPayment(sourceSystem);
	    //
	    // populate the IVRPayment Object
	    //
	    ivr.setInputLine(line);
	    ivr.addStatus(parseStatus.toString());

	    if (!headerOrTotalLine(ivr.getStatus())) {
		ivr.setAmount(amount);
		ivr.setBankReceiptNumber(bankReceipt);
		ivr.setIvrReceiptNumber(ivrReceipt);
		ivr.setPhonePayNumber(phonePayNumber);
		ivr.setCreditCardNumber(creditCardNumber);
		ivr.setDatePaid(paymentDate);
		ivr.setDateReceived(new DateTime());
		ivr.constructTransactionReferenceNumber();
		ivr.setElectronicOperatorDetails(paymentService);
	    }

	    return ivr;
	} catch (BillPayException b) {
	    return null;
	}
    }

    private boolean headerOrTotalLine(String status) {

	return status.equalsIgnoreCase("Header Line:") || status.equalsIgnoreCase("Total Line:") || status.equalsIgnoreCase("Not a payment Line:");

    }

    /**
     * Description of the Method
     */
    public void ejbCreate() {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbRemove() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbActivate() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbPassivate() throws RemoteException {
    }

    /**
     * Sets the sessionContext attribute of the BillPayMgrBean object
     * 
     * @param sessionContext
     *            The new sessionContext value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	} catch (Exception e) {
	    throw new RemoteException("unable to get the datasource");
	}
    }

}
