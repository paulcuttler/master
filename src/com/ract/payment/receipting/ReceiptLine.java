package com.ract.payment.receipting;

import java.math.BigDecimal;
import java.rmi.RemoteException;

import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentTypeVO;

/**
 * <p>
 * A class representing an individual component to a receipt.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class ReceiptLine {

    public ReceiptLine() {

    }

    public ReceiptLine(Integer lineNumber, String paymentTypeCode, BigDecimal amountPaid) {
	setLineNumber(lineNumber);
	setPaymentTypeCode(paymentTypeCode);
	setAmountPaid(amountPaid);
    }

    private String paymentTypeCode;

    private BigDecimal amountPaid;

    private Integer lineNumber;

    /**
     * Using the payment type code get the payment type details.
     * 
     * @todo Receptor will require a mapping class between RACT payment type
     *       codes and Receptor payment type codes.
     * 
     * @throws RemoteException
     * @return PaymentTypeVO
     */
    public PaymentTypeVO getPaymentType() throws RemoteException {
	PaymentTypeVO payTypeVO = PaymentEJBHelper.getPaymentMgr().getPaymentTypeVO(paymentTypeCode);
	return payTypeVO;
    }

    public void setLineNumber(Integer lineNumber) {
	this.lineNumber = lineNumber;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
	this.amountPaid = amountPaid;
    }

    public void setPaymentTypeCode(String paymentTypeCode) {
	this.paymentTypeCode = paymentTypeCode;
    }

    public Integer getLineNumber() {
	return lineNumber;
    }

    public BigDecimal getAmountPaid() {
	return amountPaid;
    }

    public String getPaymentTypeCode() {
	return paymentTypeCode;
    }

}
