package com.ract.payment.receipting;

import java.rmi.RemoteException;
import java.text.ParseException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;

import com.opensymphony.xwork2.ActionSupport;
import com.ract.common.CommonEJBHelper;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.web.common.PaymentFileGeneratorJob;

public class ScheduleWebPaymentsFileAction extends ActionSupport {

    private String cronExpression;

    @Override
    public String execute() throws Exception {

	JobDetail jobDetail = new JobDetail("WebPaymentFileGeneratorJob", Scheduler.DEFAULT_GROUP, PaymentFileGeneratorJob.class);

	jobDetail.setDescription("Web Payment File Generator");

	// schedule the job
	CronTrigger trigger = null;
	try {
	    trigger = new CronTrigger("WebPaymentFileGeneratorTrigger", Scheduler.DEFAULT_GROUP, cronExpression);
	} catch (ParseException ex2) {
	    throw new RemoteException("Unable to determine schedule.", ex2);
	}

	trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);

	try {
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	}
	return SUCCESS;
    }

    public String getCronExpression() {
	return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
	this.cronExpression = cronExpression;
    }

}
