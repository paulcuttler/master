package com.ract.payment.receipting;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemDataException;
import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapterDelegate;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.billpay.IVRPayment;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class ECRAdapter implements ReceiptingAdapter {

    public ECRAdapter() {
    }

    PaymentTransactionMgr paymentTransactionMgr = null;
    private TransactionAdapterDelegate transactionAdapterDelegate = new TransactionAdapterDelegate();

    @Override
    public void commit() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.commit();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    }
	}
    }

    @Override
    public void rollback() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.rollback();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    }
	}

    }

    @Override
    public void release() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.release();
	    } catch (RemoteException ex) {
		LogUtil.warn(getClass(), "Unable to release paymentTransactionMgr: " + ex);
	    }
	}
    }

    @Override
    public String getSystemName() {
	return SourceSystem.ECR.getAbbreviation();
    }

    @Override
    public boolean getRollbackOnly() {
	return transactionAdapterDelegate.getRollbackOnly();
    }

    @Override
    public void setRollbackOnly(Exception exception) {
	transactionAdapterDelegate.setRollbackOnly(exception);
    }

    @Override
    public Exception getRollbackException() {
	return transactionAdapterDelegate.getRollbackException();
    }

    @Override
    public String createPayableFee(PayableItemVO payableItem) throws RollBackException {

	return null;
    }

    @Override
    public void transferReceipts(Integer fromClientNumber, Integer toClientNumber, String userId) throws RollBackException {

    }

    @Override
    public void deleteReceiptingCustomer(Integer clientNumber, String userId) throws RollBackException {

    }

    @Override
    public String createPayableFee(Integer clientNumber, String sequenceNumber, DateTime createDate, String product, double amountPayable, String description, String sourceSystem, String branch, String user, Properties[] properties) throws RollBackException {

	return null;
    }

    @Override
    public Integer createPaidFee(String username, String branch, String till, String receipt, String clientNumber, String[] products, double[] costs, String[] paymentType, double[] tendered, Properties[] data, String comments, Date date, String sourceSystem) throws RemoteException, RollBackException {

	return null;
    }

    @Override
    public void cancelPayableFee(PaymentMethod paymentMethod) throws RollBackException {

    }

    @Override
    public Payable getPayable(String uniqueID, String sourceSystem) throws RemoteException, SystemDataException {

	ECRPayable payable = null;

	try {
	    Integer payableItemID = Integer.parseInt(uniqueID);
	    PayableItemVO payableItemVO = PaymentEJBHelper.getPaymentMgr().getPayableItem(payableItemID);

	    if (payableItemVO.getPaymentSystem().equals(SourceSystem.ECR.getAbbreviation())) {
		payable = new ECRPayable(payableItemVO);
	    }

	} catch (Exception e) {
	    LogUtil.fatal(getClass(), "Unable to retrieve payable by ID: " + uniqueID + ", source: " + sourceSystem);
	}

	return payable;
    }

    @Override
    public void removePayableFee(PayableItemVO payableItem) throws RollBackException {

    }

    @Override
    public void removePayableFee(Integer clientNumber, String paymentMethodID, String system, String userId) throws RollBackException {

    }

    @Override
    public Receipt receiptOnlinePayment(String sourceSystem, double amount, String cardNumber, DateTime paymentDate, String cardExpiryDate, String seqNumber, String clientNumber, String userId, String branchCode, String locationCode, String tillCode, String cardType, String cardHolder, String cvv) throws RollBackException {

	return null;
    }

    @Override
    public Receipt receiptIvrPayment(IVRPayment ivr) throws RollBackException {

	return null;
    }

    @Override
    public Receipt receiptElectronicPayment(ElectronicPayment ep) throws RollBackException {

	return null;
    }

    @Override
    public Receipt getReceiptByReceiptId(String receiptId) {

	return null;
    }

    @Override
    public String extractInsuranceIVR() throws RemoteException {

	return null;
    }

    @Override
    public Receipt getReceipt(String uniqueID, SourceSystem sourceSystem) throws RemoteException, SystemDataException {

	return null;
    }

    @Override
    public Collection<Payable> getPayableHistory(Integer clientNo) throws RemoteException {
	Collection<PayableItemVO> payableItems = PaymentEJBHelper.getPaymentMgr().getPayableHistory(clientNo);
	Collection<Payable> results = new ArrayList<Payable>();

	for (PayableItemVO payableItem : payableItems) {
	    if (payableItem.getPaymentSystem().equals(SourceSystem.ECR.getAbbreviation())) {
		ECRPayable payable = new ECRPayable(payableItem);
		results.add(payable);
	    }
	}

	return results;
    }

    public Collection<PayableItemVO> getOutstandingPayables(DateTime startDate, DateTime endDate) throws RemoteException {

	Collection<PayableItemVO> payableItems = PaymentEJBHelper.getPaymentMgr().getOutstandingPayableItems(SourceSystem.MEMBERSHIP, startDate, endDate, PaymentMethod.RECEIPTING);
	Collection<PayableItemVO> results = new ArrayList<PayableItemVO>();

	for (PayableItemVO payableItem : payableItems) {
	    if (payableItem.getPaymentSystem().equals(SourceSystem.ECR.getAbbreviation())) {
		results.add(payableItem);
	    }
	}

	return results;
    }

    @Override
    public Collection getPayableHistory(Integer clientNumber, DateTime startDate, DateTime endDate, boolean outstandingOnly) throws RemoteException {

	return null;
    }

    @Override
    public Collection getOutstandingPayables(Integer clientNumber) throws RemoteException {

	return null;
    }

    @Override
    public Collection getReceiptingHistory(Date start, Date end) throws RemoteException {

	return null;
    }

    @Override
    public void uncancelPayableFee(String uniqueID) throws RollBackException {

    }

}
