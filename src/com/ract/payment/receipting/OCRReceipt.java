package com.ract.payment.receipting;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Represents an oi receipt.
 * 
 * @author Leigh Giles
 * @version 1.0
 */

public class OCRReceipt implements Receipt {

    public static final String RECEIPT_NUMBER = "number";
    public static final String RECEIPT_NAME = "name";
    public static final String RECEIPT_CREATE_DATE = "createDate";
    public static final String RECEIPT_CREATE_TIME = "createTime";
    public static final String RECEIPT_AMOUNT = "amount";
    public static final String RECEIPT_LOGON_ID = "logonId";
    public static final String RECEIPT_SALES_BRANCH = "salesBranch";
    public static final String RECEIPT_BRANCH_NUMBER = "branchNo";
    public static final String RECEIPT_BACK_DATE = "backDate";
    public static final String RECEIPT_CHANGE_AMOUNT = "changeAmount";

    private HashMap receipt = new HashMap();
    private ArrayList receiptLineList;

    public void setReceiptLineList(ArrayList receiptLineList) {
	this.receiptLineList = receiptLineList;
    }

    public OCRReceipt() {
    }

    public OCRReceipt(String receiptNumber, BigDecimal amountOutstanding) {
	receipt.put(RECEIPT_NUMBER, receiptNumber);
	receipt.put(RECEIPT_AMOUNT, amountOutstanding);
    }

    public OCRReceipt(HashMap r) {
	this.receipt = r;
	this.setReceiptLineList((ArrayList) r.get("receiptLineList"));
    }

    /**
   *
   */
    public DateTime getBackDate() {
	DateTime bd = null;
	Object obj = receipt.get(RECEIPT_BACK_DATE);
	try {
	    bd = new DateTime((String) obj);
	} catch (Exception e) {
	}
	return bd;
    }

    /**
   *
   */
    public BigDecimal getChangeAmount() {
	Object obj = receipt.get(RECEIPT_CHANGE_AMOUNT);
	try {
	    return new BigDecimal((String) obj);
	} catch (Exception e) {
	}
	return null;
    }

    /**
     * the date and time of the receipt
     */
    public DateTime getCreateDate() {
	DateTime createDate = null;
	Object obj = null;
	String dateString = "", timeString = "";
	try {
	    obj = this.receipt.get(RECEIPT_CREATE_DATE);
	    if (obj == null) {
		throw new Exception(RECEIPT_CREATE_DATE + " null");
	    }
	    dateString = (String) obj;
	    createDate = new DateTime((String) obj);
	    obj = this.receipt.get(RECEIPT_CREATE_TIME);
	    timeString = (String) obj;
	    int createTime = Integer.parseInt(timeString.trim());
	    createDate = new DateTime(createDate.getTime() + createTime * 1000);
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), "Unable to get create date. " + dateString + "  " + timeString);
	}
	return createDate;

    }

    // private Object getAttribute(String attrName)
    // {
    // return this.receipt.get(attrName);
    // }

    /**
     * the logon id of the user
     */
    public String getLogonId() {
	Object obj = this.receipt.get(RECEIPT_LOGON_ID);
	if (obj == null) {
	    return null;
	}
	return (String) obj;
    }

    /**
     * Get the actual receipted amount
     * 
     * @return
     */
    public BigDecimal getReceiptAmount() {
	BigDecimal tenderedAmount = getTenderedAmount();
	BigDecimal changeAmount = getChangeAmount();
	return tenderedAmount.subtract(changeAmount);
    }

    /**
     * the amount paid
     */
    public BigDecimal getTenderedAmount() {
	Object obj = receipt.get(RECEIPT_AMOUNT);
	if (obj == null) {
	    return null;
	}
	return new BigDecimal(((String) obj).trim());
    }

    /**
     * get the name on the receipt
     */
    public String getReceiptName() {
	Object obj = receipt.get(RECEIPT_NAME);
	if (obj == null) {
	    return null;
	}
	return (String) obj;
    }

    /**
     * the receipt number
     */
    public String getReceiptNumber() {
	Object obj = receipt.get(RECEIPT_NUMBER);
	if (obj == null) {
	    return null;
	}
	return (String) obj;
    }

    /**
     * the Sales Branch
     */
    public String getSalesBranch() {
	Object obj = receipt.get(RECEIPT_SALES_BRANCH);
	if (obj == null) {
	    return null;
	}
	return (String) obj;
    }

    /**
     * the Branch Number
     */
    public Integer getBranchNo() {
	Object obj = receipt.get(RECEIPT_BRANCH_NUMBER);
	if (obj == null) {
	    return null;
	}
	return new Integer((String) obj);
    }

    public ReceiptLine getReceiptLine(int lineNumber) {
	if (receiptLineList != null && receiptLineList.size() > 0) {
	    return (ReceiptLine) this.receiptLineList.get(lineNumber);
	} else {
	    return null;
	}
    }

    public ArrayList getReceiptLineList() {
	return this.receiptLineList;
    }

}
