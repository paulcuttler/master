package com.ract.payment.receipting;

import java.io.Serializable;

import com.ract.common.GenericException;
import com.ract.common.SystemException;

/**
 * <p>
 * A unique identifier for a payable or a receipt.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class ReceiptingIdentifier implements Serializable {

    /**
     * The delimiter between the client number and the payment method ID.
     */
    public static String DELIMITER_SLASH = "/";

    public static String DELIMITER_DASH = "-";

    /**
     * The client number.
     */
    private Integer clientNumber;

    /**
     * The payment method ID.
     */
    private String paymentMethodID;

    /**
     * Constructor with client number and payment method ID.
     * 
     * @param clientNumber
     * @param paymentMethodID
     */
    public ReceiptingIdentifier(Integer clientNumber, String paymentMethodID) {
	this.clientNumber = clientNumber;
	this.paymentMethodID = paymentMethodID;
    }

    /**
     * Constructor with unique ID.
     * 
     * @param uniqueID
     */
    public ReceiptingIdentifier(String uniqueID) {
	this.setClientNumber(uniqueID);
	this.setPaymentMethodID(uniqueID);
    }

    public String getUniqueID() throws GenericException {
	return getUniqueID(DELIMITER_SLASH);
    }

    /**
     * Get the unique ID.
     * 
     * @return
     * @throws SystemException
     */
    public String getUniqueID(String delimiter) throws GenericException {
	if (clientNumber == null) {
	    throw new GenericException("Client number may not be null.");
	}

	if (paymentMethodID == null) {
	    throw new GenericException("Payment method id may not be null.");
	}
	StringBuffer uniqueID = new StringBuffer();
	uniqueID.append(clientNumber);
	uniqueID.append(delimiter);
	uniqueID.append(paymentMethodID);
	return uniqueID.toString();
    }

    private void setClientNumber(String uniqueID) {
	int end = uniqueID.indexOf(DELIMITER_SLASH);
	Integer clientNumber = new Integer(uniqueID.substring(0, end));
	this.clientNumber = clientNumber;
    }

    private void setPaymentMethodID(String uniqueID) {
	int start = uniqueID.indexOf(DELIMITER_SLASH);
	String paymentMethodID = uniqueID.substring(start + 1, uniqueID.length());
	this.paymentMethodID = paymentMethodID;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public String getPaymentMethodID() {
	return paymentMethodID;
    }

    public String toString() {
	String description = "";
	try {
	    description = getUniqueID();
	} catch (GenericException ex) {
	    // ignore
	}
	return description;
    }

}
