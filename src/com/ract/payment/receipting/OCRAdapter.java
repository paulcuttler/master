/*
 * @(#)OCRAdapter.java
 *
 */
package com.ract.payment.receipting;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.progress.open4gl.BigDecimalHolder;
import com.progress.open4gl.BooleanHolder;
import com.progress.open4gl.DateHolder;
import com.progress.open4gl.IntHolder;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemDataException;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentMethod;
import com.ract.payment.billpay.IVRPayment;
import com.ract.payment.electronicpayment.ElectronicPayment;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * Adapter for the OCR system written in Progress at the RACT. This
 * implementation allows commit and rollback of changes to Progress.
 * 
 * @author Glenn Lewis
 * @created 2 August 2002
 * @version 1.0, 1/5/2002
 * @version 2.0, 7/7/2003 Sam McLennan (Added createPaidFee)
 */

public class OCRAdapter extends TransactionAdapterType implements ReceiptingAdapter {

    @Override
    public Receipt getReceiptByReceiptId(String receiptId) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Receipt receiptOnlinePayment(String sourceSystem, double amount, String cardNumber, DateTime paymentDate, String cardExpiryDate, String seqNumber, String clientNumber, String userId, String branchCode, String locationCode, String tillCode, String cardType, String cardHolder, String cvv) throws RollBackException {
	throw new RollBackException("Not implemented yet.");
    }

    private boolean readOnly;

    public OCRAdapter(boolean readOnly) {
	this.readOnly = false;
    }

    public OCRAdapter() {

    }

    public void checkReadOnly() throws SystemException {
	if (this.readOnly) {
	    throw new SystemException("Unable to call method as it is read only.");
	}
    }

    // private OCRAdapterProxy progressOCRAdapterProxy = null;
    private ProgressOCRTransactionProxy ocrTransactionProxy = null;

    /**
     * Create a paid fee in OCR
     * 
     * @param username
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param branch
     *            Description of the Parameter
     * @param till
     *            Description of the Parameter
     * @param receipt
     *            Description of the Parameter
     * @param products
     *            Description of the Parameter
     * @param paymentType
     *            Description of the Parameter
     * @param costs
     *            Description of the Parameter
     * @param tendered
     *            Description of the Parameter
     * @param data
     *            Description of the Parameter
     * @param date
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception SystemException
     *                Description of the Exception
     */
    public Integer createPaidFee(String username, String branch, String till, String receipt, String customerNumber, String[] products, double[] costs, String[] paymentType, double[] tendered, Properties[] data, String comments, java.util.Date date, String sourceSystem) throws RemoteException, RollBackException {
	throw new RemoteException("Not implemented yet.");
    }

    public void transferReceipts(Integer fromClientNumber, Integer toClientNumber, String userId) throws RollBackException {
	throw new RollBackException("Not implemented yet.");
    }

    public void deleteReceiptingCustomer(Integer clientNumber, String userId) throws RollBackException {
	throw new RollBackException("Not implemented yet.");
    }

    public String getSystemName() {
	return SourceSystem.OCR.getAbbreviation();
    }

    /**
     * Create a payable fee in OCR
     * 
     * @param payableItem
     *            the payable item to be created
     */
    public String createPayableFee(PayableItemVO payableItem) throws RollBackException {
	String paymentMethodID = payableItem.getPaymentMethodID();
	// String returnString = null;
	if (paymentMethodID != null) {
	    throw new RollBackException("Attempting to create a payable fee in the receipting system with an existing receipting ID");
	}

	Integer payableItemID = payableItem.getPayableItemID();
	if (payableItemID == null) {
	    throw new RollBackException("Attempting to create a payable fee in the receipting system for a payble item with null ID");
	}

	return createPayableFee(payableItem.getClientNo(), payableItemID.toString(), null, payableItem.getProductCode(), payableItem.getAmountPayable(), payableItem.getDescription(), payableItem.getSourceSystem().getAbbreviation(), payableItem.getSalesBranchCode(), payableItem.getUserID(), null);

    }

    /**
     * Create a payable fee.
     * 
     * @param clientNumber
     *            Integer
     * @param reference
     *            String
     * @param productCode
     *            String
     * @param amountPayable
     *            double
     * @param description
     *            String
     * @param sourceSystem
     *            String
     * @param salesBranchCode
     *            String
     * @param userID
     *            String
     * @param properties
     *            Properties[]
     * @throws RollBackException
     * @return String
     */
    public String createPayableFee(Integer clientNumber, String reference, DateTime createDate, String productCode, double amountPayable, String description, String sourceSystem, String salesBranchCode, String userID, Properties[] properties) throws RollBackException {
	String paymentMethodID = null;
	// Get a progress transaction controller for creating OI-Payable records
	BigDecimal wrappedTotalAmount = new BigDecimal(amountPayable);
	IntHolder sequenceNumber = new IntHolder();
	try {
	    if (sourceSystem.equalsIgnoreCase(SourceSystem.MEMBERSHIP.getAbbreviation())) {
		ProgressOCRTransactionProxy ocrTransactionProxy = this.getOCRTransactionProxy();
		ocrTransactionProxy.createOiPayable(clientNumber.intValue(), sourceSystem, salesBranchCode, productCode, null // transaction
															      // type
															      // is
															      // null
			, userID, wrappedTotalAmount, wrappedTotalAmount, description, reference, sequenceNumber);

		handleProxyReturnString();
	    } else {
		return null;
	    }
	} catch (Open4GLException gle) {
	    handleProxyReturnString();
	    throw new RollBackException(gle);
	} catch (SystemException se) {
	    throw new RollBackException("Failed to create oi-payable in OCR. See chained SystemException.", se);
	}

	if (sequenceNumber == null) {
	    throw new RollBackException("Failed to create payable fee in OCR. Returned sequence number is null.");
	}

	paymentMethodID = Integer.toString(sequenceNumber.getIntValue());
	if (paymentMethodID.equals("0")) {
	    throw new RollBackException("Failed to create payable fee in OCR. Returned sequence number is zero.");
	}
	return paymentMethodID;

    }

    /**
     * Cancels the receipting payment method by setting the outstanding amount
     * to zero.
     * 
     * @param payableItem
     *            Description of the Parameter
     * @exception SystemException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void cancelPayableFee(PaymentMethod paymentMethod) throws RollBackException {

	// removed 4/2/2002 JH - to be re-implemented inside the persistant
	// procedure.

	if (paymentMethod.getClientNumber() == null) {
	    throw new RollBackException("Attempting to cancel a payment instance with null client number");
	}

	if (paymentMethod.getPaymentMethodID() == null) {
	    throw new RollBackException("Attempting to cancel a payment instance with null receipting ID");
	}

	int clientNo = ((ReceiptingPaymentMethod) paymentMethod).getClientNumber().intValue();
	int sequenceNo = Integer.parseInt(((ReceiptingPaymentMethod) paymentMethod).getPaymentMethodID());

	cancelAmountOutstanding(clientNo, sequenceNo);
    }

    /**
     * Setting the outstanding amount to zero.
     * 
     * @exception SystemException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void cancelAmountOutstanding(Integer clientNo, Integer sequenceNo) throws RollBackException {

	ProgressOCRTransactionProxy ocrTransactionProxy = null;
	try {
	    ocrTransactionProxy = this.getOCRTransactionProxy();
	    BooleanHolder result = new BooleanHolder();

	    ocrTransactionProxy.setAmountOutstandingToZero(clientNo, sequenceNo, result);
	    if (!result.getBooleanValue()) {
		throw new SystemException("Could not find the payable item with client No " + clientNo + " and sequence number " + sequenceNo + " in the receipting system");
	    }
	} catch (Open4GLException gle) {
	    try {
		handleProxyReturnString();
	    } catch (Exception se) {
		throw new RollBackException(se);
	    }
	    throw new RollBackException(gle);
	} catch (SystemException se) {
	    throw new RollBackException("Failed to cancel the payable fee in OCR. See chained SystemException.", se);
	}
    }

    /**
     * Recreate Receptor payables using the data held in the OCR payable.
     * 
     * @param startDate
     *            DateTime
     * @param endDate
     *            DateTime
     * @param outstandingOnly
     *            boolean
     * @param subsystemList
     *            String MEM, TRV, INS, FIN or ALL
     * @throws RollBackException
     * @throws RemoteException
     */
    public Collection getPayables(DateTime startDate, DateTime endDate, boolean outstandingOnly, String subsystemList) throws RemoteException {
	ArrayList updatePayableList = new ArrayList();
	PayableItemVO payableItem = null;

	// temporary variables
	Integer clientNumber = null;
	Integer sequenceNumber = null;
	// not used
	DateTime createDate = null;
	String productCode = null;
	double amountPayable = 0;
	double amountOutstanding = 0;
	String description = null;
	String sourceSystemString = null;
	SourceSystem sourceSystem = null;
	String salesBranchCode = null;
	String userId = null;
	String reference = null;
	Properties[] properties = null;

	// returned
	String paymentMethodId = null;

	GregorianCalendar gregStartDate = new GregorianCalendar();
	gregStartDate.setTime(startDate);
	GregorianCalendar gregEndDate = new GregorianCalendar();
	gregEndDate.setTime(endDate);

	ResultSetHolder payableList = new ResultSetHolder();

	OCRProgressProxy ocrProgressProxy = this.getOCRProgressProxy();
	try {
	    // get oi-payables between the given dates for the given subsystem
	    ocrProgressProxy.getOiPayables(gregStartDate, gregEndDate, outstandingOnly, subsystemList, payableList);

	    ResultSet payableRS = payableList.getResultSetValue();
	    int successCounter = 0;
	    int recordCounter = 0;
	    while (payableRS.next()) {
		recordCounter++;
		clientNumber = new Integer(payableRS.getInt(1));
		sequenceNumber = new Integer(payableRS.getInt(2)); // sequence
								   // number
		createDate = new DateTime(payableRS.getDate(3));
		amountPayable = payableRS.getDouble(4);
		amountOutstanding = payableRS.getDouble(5);
		userId = payableRS.getString(6);
		salesBranchCode = payableRS.getString(7);
		// ignore branch number
		description = payableRS.getString(9);
		sourceSystemString = payableRS.getString(10);
		productCode = payableRS.getString(11);
		// ignore product qualifier
		reference = payableRS.getString(13);

		try {
		    sourceSystem = SourceSystem.getSourceSystem(sourceSystemString);
		} catch (GenericException ex1) {
		    // ignore
		}

		// translate INS to INS_RAC when converting Insurance data.
		if (sourceSystem.equals(SourceSystem.INSURANCE)) {
		    sourceSystem = SourceSystem.INSURANCE_RACT;
		}

		try {
		    // are duplicates catered for?

		    // add it to list so calling program can update references.
		    payableItem = new PayableItemVO();
		    payableItem.setClientNo(clientNumber);

		    // payable item id serves as the unique identifier for the
		    // payable
		    if (sourceSystem.equals(SourceSystem.MEMBERSHIP)) {
			payableItem.setPayableItemID(new Integer(reference)); // payable
									      // item
									      // id
									      // or
									      // pending
									      // fee
									      // id
		    } else if (sourceSystem.equals(SourceSystem.INSURANCE_RACT)) {
			payableItem.setPayableItemID(sequenceNumber); // sequence
								      // number
		    } else {
			throw new RemoteException("Unknown source system.");
		    }
		    payableItem.setCreateDateTime(createDate);
		    payableItem.setPaymentSystem(this.getSystemName()); // OCR
		    payableItem.setPaymentMethodDescription(PaymentMethod.RECEIPTING);
		    payableItem.setProductCode(productCode);
		    payableItem.setAmountPayable(amountPayable);
		    payableItem.setDescription(description);
		    payableItem.setSourceSystem(sourceSystem);
		    payableItem.setSalesBranchCode(salesBranchCode);
		    payableItem.setUserID(userId);

		    LogUtil.debug(this.getClass(), "payableItem=" + payableItem);

		    // other payable item details
		    updatePayableList.add(payableItem);

		    successCounter++;
		} catch (Exception ex) {

		    // ignore and log
		    LogUtil.warn(this.getClass(), "Unable to create  for client number '" + clientNumber + "' and reference '" + payableItem.getPayableItemID() + "'. " + ex.getMessage());
		}

		// output every 1000+

		if (recordCounter % 1000 == 0 || recordCounter == 1) {
		    LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber);
		    LogUtil.debug(this.getClass(), "sequenceNumber=" + sequenceNumber); // sequence
											// number
		    LogUtil.debug(this.getClass(), "createDate=" + createDate);
		    LogUtil.debug(this.getClass(), "amountPayable=" + amountPayable);
		    LogUtil.debug(this.getClass(), "amountOutstanding=" + amountOutstanding);
		    LogUtil.debug(this.getClass(), "userId=" + userId);
		    LogUtil.debug(this.getClass(), "salesBranchCode=" + salesBranchCode);
		    LogUtil.debug(this.getClass(), "description=" + description);
		    LogUtil.debug(this.getClass(), "sourceSystem=" + sourceSystem);
		    LogUtil.debug(this.getClass(), "productCode=" + productCode);
		    LogUtil.debug(this.getClass(), "reference=" + reference); // sequence
									      // number
		}
	    }
	    payableRS.close();

	    LogUtil.debug(this.getClass(), "Processed " + recordCounter + " oi-payables.");
	    LogUtil.debug(this.getClass(), successCounter + " oi-payables were successfully inserted in Receptor.");
	    LogUtil.debug(this.getClass(), (recordCounter - successCounter) + " oi-payables failed.");
	} catch (Open4GLException gle) {
	    // handleProxyReturnString();
	    throw new RemoteException(gle.getMessage());
	} catch (SQLException se) {
	    throw new RemoteException(se.getMessage());
	} finally {
	    // release connection
	    releaseOCRProgressProxy(ocrProgressProxy);
	}

	return updatePayableList;

    }

    /**
     * Receipt an IVR payment in the Progress System and return the payable.
     */
    public Receipt receiptIvrPayment(IVRPayment ivr) throws RollBackException {
	Integer subSeq;

	BigDecimalHolder amountOutstandingHolder = new BigDecimalHolder();
	StringHolder receiptNumberHolder = new StringHolder();
	IntHolder subSequenceHolder = new IntHolder();

	BigDecimal amount = NumberUtil.roundHalfUp(ivr.getAmount(), 2);

	GregorianCalendar paidDate = new GregorianCalendar();
	paidDate.setTime(ivr.getDatePaid());

	try {
	    ProgressOCRTransactionProxy ocrTransactionProxy = this.getOCRTransactionProxy();
	    ocrTransactionProxy.receiptOiPayable(ivr.getOpClientNumber().intValue(), ivr.getOpSequenceNumber().intValue(), ivr.getSourceSystemAbbreviation(), amount, paidDate, ivr.getBankReceiptNumber(), ivr.getIvrReceiptNumber(), ivr.getCreditCardNumber(), CommonConstants.USER_IVR, receiptNumberHolder, amountOutstandingHolder, subSequenceHolder);
	    handleProxyReturnString();

	    if (subSequenceHolder.isNull()) {
		subSeq = new Integer(0);
	    } else {
		subSeq = new Integer(subSequenceHolder.getIntValue());
	    }
	    // ReceiptingIdentifier ri = new ReceiptingIdentifier(
	    // ivr.getOpClientNumber(),ivr.getOpSequenceNumber().toString());
	    OCRReceipt receipt = new OCRReceipt(receiptNumberHolder.getStringValue(), amountOutstandingHolder.getBigDecimalValue());
	    return receipt;
	} catch (Open4GLException gle) {
	    handleProxyReturnString();
	    throw new RollBackException(gle.getMessage());
	} catch (SystemException se) {
	    throw new RollBackException("Failed to receipt IVR payment. See chained SystemException.", se);
	} catch (Exception e) {
	    throw new RollBackException(e.getMessage());
	}
    }

    public Receipt receiptElectronicPayment(ElectronicPayment ep) throws RollBackException {
	return receiptElectronicPayment(new Integer(ep.getClientNumber()).intValue(), new Integer(ep.getSeqNumber()).intValue(), ep.getSourceSystem().getAbbreviation(), ep.getAmount(), ep.getDateOfPayment(), " ", ep.getTransactionReferenceNumber(), " ", ep.getUserId());
    }

    private Receipt receiptElectronicPayment(int clientNumber, int sequenceNumber, String sourceSystem, double amount, DateTime datePaid, String bankReceiptNumber, String transactionReceiptNumber, String cardNumber, String userID) throws RollBackException {

	Integer subSeq;
	LogUtil.debug(this.getClass(), "RLG +++++ going to progress ");
	BigDecimalHolder amountOutstandingHolder = new BigDecimalHolder();
	StringHolder receiptNumberHolder = new StringHolder();
	IntHolder subSequenceHolder = new IntHolder();

	BigDecimal amt = NumberUtil.roundHalfUp(amount, 2);

	GregorianCalendar paidDate = new GregorianCalendar();
	paidDate.setTime(datePaid);

	try {
	    ProgressOCRTransactionProxy ocrTransactionProxy = this.getOCRTransactionProxy();
	    LogUtil.debug(this.getClass(), "RLG +++++ going to progress proxy receiptOiPayable");
	    ocrTransactionProxy.receiptOiPayable(clientNumber, sequenceNumber, sourceSystem, amt, paidDate, bankReceiptNumber, transactionReceiptNumber, cardNumber, userID, receiptNumberHolder, amountOutstandingHolder, subSequenceHolder);
	    handleProxyReturnString();

	    if (subSequenceHolder.isNull()) {
		subSeq = new Integer(0);
	    } else {
		subSeq = new Integer(subSequenceHolder.getIntValue());
	    }
	    // ReceiptingIdentifier ri = new ReceiptingIdentifier(
	    // ivr.getOpClientNumber(),ivr.getOpSequenceNumber().toString());
	    OCRReceipt receipt = new OCRReceipt(receiptNumberHolder.getStringValue(), amountOutstandingHolder.getBigDecimalValue());
	    return receipt;
	} catch (Open4GLException gle) {
	    handleProxyReturnString();
	    throw new RollBackException(gle.getMessage());
	} catch (SystemException se) {
	    throw new RollBackException("Failed to receipt " + userID + " payment. See chained SystemException.", se);
	} catch (Exception e) {
	    throw new RollBackException(e.getMessage());
	}

    }

    /**
     * TRANSACTIONAL Return an oi-receipt from the OCR receipting system.
     * 
     * @todo return receipt
     */
    public Receipt getReceipt(String uniqueID, SourceSystem sourceSystem) throws RemoteException, SystemDataException {
	LogUtil.log(this.getClass(), "RLG ++++++ getReceipt ");
	Payable payable = this.getPayable(uniqueID);
	return payable.getFirstReceipt();
    }

    public void uncancelPayableFee(String uniqueID) throws RollBackException {
	if (uniqueID == null) {
	    throw new RollBackException("The  unique ID cannot be null when uncancelling an oi-payable.");
	}

	ReceiptingIdentifier ri = new ReceiptingIdentifier(uniqueID);
	Integer clientNumber = ri.getClientNumber();
	Integer paymentMethodID = new Integer(ri.getPaymentMethodID());

	ProgressOCRTransactionProxy ocrTransactionProxy = null;
	try {
	    ocrTransactionProxy = this.getOCRTransactionProxy();
	    // BooleanHolder result = new BooleanHolder();

	    ocrTransactionProxy.setAmountOutstandingToAmountPayable(clientNumber.intValue(), paymentMethodID.intValue());
	    // if (!result.getBooleanValue())
	    // throw new
	    // SystemException("Unable to uncancel for client "+clientNumber+", seq "+paymentMethodID);
	} catch (Open4GLException gle) {
	    handleProxyReturnString();
	    throw new RollBackException(gle);
	} catch (SystemException se) {
	    throw new RollBackException("Failed to uncancel payable fee with client number " + clientNumber + " and sequence number " + paymentMethodID + ". " + se);
	}
    }

    /**
     * Remove a payable fee. This method is transaction safe. The adapter must
     * be committed or rolled back by the caller.
     */
    public void removePayableFee(Integer clientNumber, String paymentMethodID, String sourceSystem, String userId) throws RollBackException {
	if (clientNumber == null) {
	    throw new RollBackException("The client number cannot be null when removing an oi-payable.");
	}

	if (paymentMethodID == null) {
	    throw new RollBackException("The payment method ID cannot be null when removing an oi-payable.");
	}

	try {
	    ProgressOCRTransactionProxy ocrTransactionProxy = this.getOCRTransactionProxy();
	    ocrTransactionProxy.removeOiPayable(clientNumber.intValue(), Integer.parseInt(paymentMethodID));
	    handleProxyReturnString();
	} catch (Open4GLException gle) {
	    handleProxyReturnString();
	    throw new RollBackException(gle);
	} catch (SystemException se) {
	    throw new RollBackException("Failed to remove payable fee. See chained SystemException.", se);
	}
    }

    /**
     * Remove a payable fee from OCR
     */
    public void removePayableFee(PayableItemVO payableItem) throws RollBackException {
	if (payableItem == null) {
	    throw new RollBackException("The payable item cannot be null when removing an oi-payable.");
	}

	Integer clientNumber = payableItem.getClientNo();
	if (clientNumber == null) {
	    throw new RollBackException("The client number on payable item '" + payableItem.getPayableItemID() + "' cannot be null when removing an oi-payable.");
	}

	String paymentMethodID = payableItem.getPaymentMethodID();
	if (paymentMethodID == null) {
	    throw new RollBackException("The payment method ID on payable item '" + payableItem.getPayableItemID() + "' cannot be null when removing an oi-payable.");
	}
	String userId = payableItem.getUserID();
	String sourceSystem = payableItem.getSourceSystem().getAbbreviation();
	removePayableFee(clientNumber, paymentMethodID, sourceSystem, userId);
    }

    public String extractInsuranceIVR() throws RemoteException {
	StringHolder fileNameHolder = new StringHolder();
	OCRProgressProxy progressOCRProxy = null;
	try {
	    progressOCRProxy = getOCRProgressProxy();
	    progressOCRProxy.bpayext(fileNameHolder);
	} catch (Exception e) {
	    throw new RemoteException(e.getMessage());
	} finally {
	    this.releaseOCRProgressProxy(progressOCRProxy);
	}
	return fileNameHolder.getStringValue();
    }

    /**
     * source system is an implementation of OCR. eg. ISCU, RACT, etc.
     * 
     * @param uniqueID
     * @param sourceSystem
     * @return
     * @throws RemoteException
     */

    public Payable getPayable(String uniqueID, String sourceSystemString) throws RemoteException, SystemDataException {

	// Note: source system string will not change for ocr implementation.

	BigDecimalHolder amountHolder = new BigDecimalHolder();
	BigDecimalHolder amountOutstandingHolder = new BigDecimalHolder();
	IntHolder branchIDHolder = new IntHolder();
	DateHolder createDateHolder = new DateHolder();
	IntHolder createTimeHolder = new IntHolder();
	StringHolder createUserIDHolder = new StringHolder();
	StringHolder descriptionHolder = new StringHolder();
	StringHolder refNoHolder = new StringHolder();
	StringHolder productCodeHolder = new StringHolder();
	StringHolder salesBranchCodeHolder = new StringHolder();
	StringHolder subSystemHolder = new StringHolder();
	StringHolder transactionCodeHolder = new StringHolder();
	StringHolder receiptXMLHolder = new StringHolder();

	ReceiptingIdentifier ri = new ReceiptingIdentifier(uniqueID);
	Integer clientNumber = ri.getClientNumber();
	Integer sequenceNumber = new Integer(ri.getPaymentMethodID());

	LogUtil.debug(this.getClass(), ri.toString());

	if (clientNumber == null) {
	    throw new SystemException("Client number cannot be null - " + clientNumber);
	}

	if (sequenceNumber == null) {
	    throw new SystemException("Sequence number cannot be null - " + sequenceNumber);
	}

	OCRProgressProxy ocrProgressProxy = this.getOCRProgressProxy();
	try {
	    ocrProgressProxy.getOiPayable(clientNumber.intValue(), sequenceNumber.intValue(), refNoHolder, createDateHolder, createTimeHolder, amountHolder, amountOutstandingHolder, productCodeHolder, transactionCodeHolder, createUserIDHolder, salesBranchCodeHolder, descriptionHolder, branchIDHolder, subSystemHolder, receiptXMLHolder);

	    handleProxyReturnString(ocrProgressProxy);

	} catch (Open4GLException gle) {
	    String returnString = "";
	    try {
		returnString = ocrProgressProxy._getProcReturnString();
	    } catch (Exception re) {
	    }
	    throw new SystemException(returnString, gle);
	} finally {
	    releaseOCRProgressProxy(ocrProgressProxy);
	}

	DateTime createDate = null;
	if (!createDateHolder.isNull() && !createTimeHolder.isNull()) {
	    GregorianCalendar gc = DateUtil.clearTime(createDateHolder.getDateValue());
	    createDate = new DateTime(gc.getTime().getTime() + createTimeHolder.getIntValue());
	}

	SourceSystem sourceSystem = null;
	if (!subSystemHolder.isNull()) {
	    try {
		sourceSystem = SourceSystem.getSourceSystem(subSystemHolder.getStringValue());
	    } catch (GenericException ge) {
		throw new SystemException(ge);
	    }
	}

	// The implementation for returning these attributes has not been done
	// yet.
	DateTime receiptDate = null;
	String receiptUserID = null;
	String receiptBranchCode = null;
	Collection receiptList = null;

	if (!receiptXMLHolder.isNull()) {
	    String myXML = receiptXMLHolder.getStringValue();
	    LogUtil.debug(this.getClass(), "RLG +++++ myXML\n" + myXML);
	    receiptList = this.getOCRReceiptListFromXML(myXML);
	}
	OCRPayable payable = new OCRPayable(clientNumber, sequenceNumber, createDate, amountHolder.isNull() ? null : amountHolder.getBigDecimalValue(), amountOutstandingHolder.isNull() ? null : amountOutstandingHolder.getBigDecimalValue(), createUserIDHolder.isNull() ? null : createUserIDHolder.getStringValue(), salesBranchCodeHolder.isNull() ? null : salesBranchCodeHolder.getStringValue(), branchIDHolder.isNull() ? null : new Integer(branchIDHolder.getIntValue()), productCodeHolder.isNull() ? null : productCodeHolder.getStringValue(), descriptionHolder.isNull() ? null : descriptionHolder.getStringValue(), sourceSystem, refNoHolder.isNull() ? null : refNoHolder.getStringValue(), receiptDate, receiptUserID, receiptBranchCode, receiptList);
	return payable;

    }

    /**
     * Return an oi-payable from the OCR receipting system.
     */
    public Payable getPayable(String uniqueID) throws RemoteException, SystemDataException {
	return getPayable(uniqueID, null);
    }

    // private boolean errorSaysNotFound(Exception e)
    // {
    // String errorMessage = e.getMessage();
    // if(errorMessage.indexOf("not found") > 0)
    // {
    // return true;
    // }
    // return false;
    // }

    public void commit() throws SystemException {
	LogUtil.debug(this.getClass(), "committing receipting adapter");
	if (this.ocrTransactionProxy != null) {
	    try {
		this.ocrTransactionProxy.commit();
	    } catch (Open4GLException gle) {
		throw new SystemException(gle);
	    } finally {
		release();
	    }
	}
    }

    /**
     * Description of the Method
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws SystemException {
	LogUtil.debug(this.getClass(), "rolling back receipting adapter");
	try {
	    if (this.ocrTransactionProxy != null) {
		this.ocrTransactionProxy.rollback();
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	} finally {
	    release();
	}
    }

    /**
     * Release any OCR transaction proxy object and the OCR adapter proxy object
     * then set the references to null so that they are re-initialised on the
     * next usage of the recepting adapter.
     */
    public void release() {
	releaseOCRTransactionProxy();
    }

    private void releaseOCRTransactionProxy() {
	if (this.ocrTransactionProxy != null) {
	    LogUtil.debug(this.getClass(), "releasing ocrTransactionProxy");
	    try {
		this.ocrTransactionProxy._release();
	    } catch (Exception gle1) {
		gle1.printStackTrace();
	    } finally {
		this.ocrTransactionProxy = null;
	    }
	}
	releaseOCRProgressProxy(this.transactionOCRProgressProxy);
	this.transactionOCRProgressProxy = null;
    }

    /**
     * Gets the crTransactionProxy attribute of the OCRAdapter object
     * 
     * @return The crTransactionProxy value
     * @exception SystemException
     *                Description of the Exception
     */
    private ProgressOCRTransactionProxy getOCRTransactionProxy() throws SystemException {
	if (this.ocrTransactionProxy == null) {
	    this.transactionOCRProgressProxy = getOCRProgressProxy();
	    try {
		this.ocrTransactionProxy = transactionOCRProgressProxy.createPO_ProgressOCRTransactionProxy();
	    } catch (Open4GLException gle) {
		throw new SystemException("Failed to get CreateOiPayable transaction proxy.", gle);
	    }
	}
	return this.ocrTransactionProxy;
    }

    /**
     * proxy with transaction running
     */
    private OCRProgressProxy transactionOCRProgressProxy = null;

    /**
     * Get the OCR progress proxy.
     * 
     * @throws SystemException
     * @return OCRProgressProxy
     */
    private OCRProgressProxy getOCRProgressProxy() throws SystemException {
	OCRProgressProxy progressOCRProxy = null;
	try {
	    progressOCRProxy = (OCRProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_OCR_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return progressOCRProxy;
    }

    private void handleProxyReturnString(OCRProgressProxy ocrProgressProxy) throws SystemException, SystemDataException {
	try {
	    if (ocrProgressProxy != null) {
		String returnString = ocrProgressProxy._getProcReturnString();
		LogUtil.debug(this.getClass(), "returnString=" + returnString);
		if (returnString != null && returnString.length() > 0) {
		    if (returnString.indexOf("Oi-payable for client-no") >= 0 && returnString.indexOf("not found in aps/gen/getAmountOutstanding.p") >= 0) {
			throw new SystemDataException(returnString);
		    } else {
			throw new SystemException(returnString);
		    }
		}
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	}
    }

    /**
     * handle the return string for the transaction proxy
     */
    private void handleProxyReturnString() throws RollBackException {
	try {
	    if (this.ocrTransactionProxy != null) {
		String returnString = this.ocrTransactionProxy._getProcReturnString();
		if (returnString != null && returnString.length() > 0) {
		    throw new RollBackException(new ValidationException(returnString));
		}
	    }
	} catch (Open4GLException gle) {
	    throw new RollBackException(gle);
	}
    }

    public Collection getReceiptingHistory(java.util.Date start, java.util.Date end) throws RemoteException {
	// this is a null implementation for OCR
	//
	throw new RemoteException("Not implemented yet.");
    }

    /**
     * Get amount outstanding for a client and sequence number.
     * 
     * @param clientNumber
     *            Integer
     * @param sequenceNumber
     *            String
     * @param sourceSystem
     *            String
     * @throws RemoteException
     * @return BigDecimal
     */
    public BigDecimal getAmountOutstanding(Integer clientNumber, String sequenceNumber, String sourceSystem) throws RemoteException, SystemDataException {
	BigDecimalHolder amountOutstanding = new BigDecimalHolder();
	OCRProgressProxy ocrProgressProxy = null;
	try {
	    ocrProgressProxy = this.getOCRProgressProxy();
	    ocrProgressProxy.getAmountOutstanding(clientNumber.intValue(), Integer.parseInt(sequenceNumber), sourceSystem, amountOutstanding);
	    handleProxyReturnString(ocrProgressProxy);
	} catch (Exception ex) {
	    handleProxyReturnString(ocrProgressProxy);
	    throw new SystemException("Unable to get amount outstanding for client '" + clientNumber + "'. " + ex.getMessage());
	} finally {
	    this.releaseOCRProgressProxy(ocrProgressProxy);
	}
	return amountOutstanding.getBigDecimalValue();
    }

    public void setAmountOutstanding(Integer clientNumber, String sequenceNumber, String sourceSystem, BigDecimal amountOutStanding) throws RemoteException

    {
	// BigDecimalHolder amountOutstanding = new BigDecimalHolder();
	OCRProgressProxy ocrProgressProxy = null;
	try {
	    ocrProgressProxy = this.getOCRProgressProxy();
	    ocrProgressProxy.setAmountOutstanding(clientNumber.intValue(), Integer.parseInt(sequenceNumber), sourceSystem, amountOutStanding);
	    handleProxyReturnString(ocrProgressProxy);
	} catch (Exception ex) {
	    try {
		handleProxyReturnString(ocrProgressProxy);
	    } catch (SystemDataException e) {

	    }

	    throw new SystemException("Unable to get amount outstanding for client '" + clientNumber + "'. " + ex.getMessage());
	} finally {
	    this.releaseOCRProgressProxy(ocrProgressProxy);
	}

    }

    /**
     * Retrieve a collection of oi-payable records by client number.
     * 
     * @param clientNumber
     *            Integer
     * @throws RemoteException
     * @return Collection
     */
    public Collection getPayableHistory(Integer clientNumber) throws RemoteException {
	return getPayableHistory(clientNumber, false);
    }

    private Collection getPayableHistory(Integer clientNumber, boolean outstandingOnly) throws RemoteException {
	// current date
	DateTime endDate = new DateTime();

	Interval fiveYearPeriod = new Interval();
	try {
	    // get and set the history limit in years.
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    int historyLimit = Integer.parseInt(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.PROGRESS_HISTORY_LIMIT_YEARS));
	    fiveYearPeriod.setYears(historyLimit);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}

	// five years from current date
	DateTime startDate = endDate.subtract(fiveYearPeriod);

	return getPayableHistory(clientNumber, startDate, endDate, outstandingOnly);
    }

    public Collection getOutstandingPayables(Integer clientNumber) throws RemoteException {
	return getPayableHistory(clientNumber, true);
    }

    /**
     * Retrieve a collection of oi-payable records and associated receipts for
     * given client, date range and whether outstanding only or all records.
     * 
     * @param clientNumber
     *            Integer
     * @param startDate
     *            DateTime
     * @param endDate
     *            DateTime
     * @param outstandingOnly
     *            boolean
     * @throws RemoteException
     * @return Collection
     */
    public Collection getPayableHistory(Integer clientNumber, DateTime startDate, DateTime endDate, boolean outstandingOnly) throws RemoteException {
	LogUtil.log(this.getClass(), "getPayableHistory(" + clientNumber + ", " + startDate + ", " + endDate + " ," + outstandingOnly + ") start");
	ArrayList payableList = new ArrayList();
	Payable oiPayable = null;

	ResultSetHolder rsHolder = new ResultSetHolder();
	ArrayList payableKeyList = new ArrayList();
	int rCclientNo;
	int rSeqNo;
	GregorianCalendar gregStartDate = new GregorianCalendar();
	gregStartDate.setTime(startDate);
	GregorianCalendar gregEndDate = new GregorianCalendar();
	gregEndDate.setTime(endDate);

	ReceiptingIdentifier receiptingIdentifier = null;
	OCRProgressProxy progressOCRProxy = getOCRProgressProxy();
	try {
	    // System.out.print("outstandingOnly="+outstandingOnly);
	    LogUtil.log(this.getClass(), "before getOiPayablesForClient");
	    progressOCRProxy.getOiPayablesForClient(clientNumber.intValue(), gregStartDate, gregEndDate, outstandingOnly, rsHolder);
	    LogUtil.log(this.getClass(), "after getOiPayablesForClient");
	    ResultSet rs = rsHolder.getResultSetValue();

	    while (rs.next()) {
		try {
		    rCclientNo = rs.getInt(1);
		    rSeqNo = rs.getInt(2);
		    receiptingIdentifier = new ReceiptingIdentifier(new Integer(rCclientNo), String.valueOf(rSeqNo));
		    payableKeyList.add(receiptingIdentifier);

		} catch (Exception ex) {
		    LogUtil.warn(this.getClass(), "Error creating receiptingIdentifier object for client no " + clientNumber + "\n " + ex.getMessage());
		    ex.printStackTrace();
		}
	    }
	    rs.close();

	    // using the key populate the list
	    for (int i = 0; i < payableKeyList.size(); i++) {
		receiptingIdentifier = (ReceiptingIdentifier) payableKeyList.get(i);
		LogUtil.log(this.getClass(), "clientNumber=" + receiptingIdentifier.getClientNumber() + ",sequenceNumber=" + receiptingIdentifier.getPaymentMethodID());
		try {
		    oiPayable = getPayable(receiptingIdentifier.getUniqueID());
		    LogUtil.log(this.getClass(), oiPayable.toString());
		} catch (RemoteException ex1) {
		    LogUtil.fatal(this.getClass(), ex1);
		    oiPayable = new OCRPayable(receiptingIdentifier, ex1);
		}
		payableList.add(oiPayable);
	    }

	} catch (Exception sqle) {
	    LogUtil.fatal(this.getClass(), sqle);
	    sqle.printStackTrace();
	    throw new RemoteException("Error getting oi-payable for " + clientNumber + ": " + sqle);
	} finally {
	    this.releaseOCRProgressProxy(progressOCRProxy);
	}

	LogUtil.log(this.getClass(), "getPayableHistory end");

	return payableList;
    }

    /**
     * Get a collection of OCRReceipt objects given an xml string.
     * 
     * @param xmlString
     * @return
     * @throws SystemException
     */
    private Collection getOCRReceiptListFromXML(String xmlString) throws SystemException {
	ArrayList receiptList = new ArrayList();

	if (xmlString == null) {
	    return receiptList;
	}

	Element docRoot = null;
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	factory.setValidating(false);
	DocumentBuilder builder = null;
	Document document = null;

	try {
	    builder = factory.newDocumentBuilder();
	    StringReader reader = new StringReader(xmlString);
	    InputSource source = new InputSource(reader);
	    document = builder.parse(source);
	} catch (IOException ex) {
	    throw new SystemException("Unable to get receipt xml to parse.", ex);
	} catch (SAXException ex) {
	    throw new SystemException("Unable to parse receipt xml. " + xmlString, ex);
	} catch (ParserConfigurationException ex) {
	    throw new SystemException(ex);
	}

	docRoot = document.getDocumentElement();
	NodeList elementList = docRoot.getChildNodes();

	for (int x = 0; x < elementList.getLength(); x++) {
	    receiptList.add(getReceiptFromXML(elementList.item(x)));
	}

	return receiptList;

    }

    /**
     * Create an OCR receipt from xml.
     * 
     * @param rNode
     * @return
     * @throws java.lang.Exception
     */
    private OCRReceipt getReceiptFromXML(Node rNode) {
	OCRReceipt receipt = null;

	HashMap recMap = new HashMap();

	NodeList elementList = null;
	NodeList childElems = null;
	Node elementNode = null;

	String nodeName = null;
	String valueText = null;

	CDATASection cData = null;

	elementList = rNode.getChildNodes();

	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();
	    LogUtil.debug(this.getClass(), "RLG ++++ xxx elementNode = " + elementNode.getNodeType() + " / " + nodeName + " / " + elementNode.hasChildNodes());

	    if (nodeName.equalsIgnoreCase("receiptPaymentTypeList")) {
		childElems = elementNode.getChildNodes();
		LogUtil.debug(this.getClass(), "RLG ++++ xxx child elements " + childElems.getLength());
		recMap.put("receiptLineList", this.getReceiptLineListFromXML(elementNode));
	    } else {
		if (elementNode.hasChildNodes()) {
		    valueText = elementNode.getFirstChild().getNodeValue();
		} else {
		    if (elementNode.getNodeType() == Node.CDATA_SECTION_NODE) {
			LogUtil.debug(this.getClass(), "RLG ++++ CDATA");
			cData = (CDATASection) elementNode;
			nodeName = cData.getNodeName();
			valueText = cData.getNodeValue();

		    }
		}
		LogUtil.debug(this.getClass(), "RLG ++++ nodeName = " + nodeName);
		LogUtil.debug(this.getClass(), "RLG ++++ valueText = " + valueText);

		recMap.put(nodeName, valueText);
	    }

	}

	receipt = new OCRReceipt(recMap);
	return receipt;

    }

    /**
     * Get a collection of ReceiptLine objects given an xml string.
     * 
     * @param xmlString
     * @return
     * @throws SystemException
     */
    private Collection getReceiptLineListFromXML(Node rNode) {

	ArrayList receiptLineList = new ArrayList();

	NodeList elementList = null;

	Node rlNode = null;
	elementList = rNode.getChildNodes();

	for (int i = 0; i < elementList.getLength(); i++) {
	    rlNode = elementList.item(i);
	    LogUtil.debug(this.getClass(), "RLG +++++ " + rlNode.getNodeName());
	    receiptLineList.add(getReceiptLineFromXML(elementList.item(i)));
	}

	return receiptLineList;

    }

    /**
     * Get a ReceiptLine object given an xml string.
     * 
     * @param xmlString
     * @return
     * @throws SystemException
     */

    private ReceiptLine getReceiptLineFromXML(Node rNode) {

	ReceiptLine receiptLine = new ReceiptLine();

	Integer lineNumber;
	String paymentTypeCode;
	BigDecimal amount;

	NodeList elementList = null;
	String nodeValue;
	Node rlNode = null;
	elementList = rNode.getChildNodes();

	for (int i = 0; i < elementList.getLength(); i++) {
	    rlNode = elementList.item(i);
	    nodeValue = rlNode.getFirstChild().getNodeValue();

	    LogUtil.debug(this.getClass(), "RLG +++++ grlfx " + rlNode.getNodeName() + " / " + nodeValue);

	    if (rlNode.getNodeName().equalsIgnoreCase("paymentNumber")) {
		receiptLine.setLineNumber(new Integer(nodeValue));
	    } else if (rlNode.getNodeName().equalsIgnoreCase("amount")) {
		receiptLine.setAmountPaid(new BigDecimal(nodeValue));
	    } else if (rlNode.getNodeName().equalsIgnoreCase("paymentTypeCode")) {
		receiptLine.setPaymentTypeCode(nodeValue);
	    }

	}

	return receiptLine;

    }

    public int doesPayableFeeExist(String clientNumber, String sequenceNumber, String sourceSystem) throws RemoteException {
	return 0;
    }

    /**
     * release the proxy
     */
    private void releaseOCRProgressProxy(OCRProgressProxy progressOCRProxy) {
	// return to pool
	LogUtil.debug(this.getClass(), "releasing progressOCRProxy");
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_OCR_PROXY, progressOCRProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), ex);
	}
    }

}
