package com.ract.payment.receipting;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;

import com.ract.common.SourceSystem;
import com.ract.util.DateTime;

/**
 * The interface to implementations of a payable object.
 * 
 * redesigned 15/03/04 jyh
 * 
 * @author J. Holliday
 * @version 1.0
 */

public interface Payable {

    /**
     * The amount payable.
     * 
     * @return
     */
    public BigDecimal getAmount();

    /**
     * The amount outstanding.
     * 
     * @return
     */
    public BigDecimal getAmountOutstanding();

    /**
     * The branch identifier.
     * 
     * @return
     */
    public Integer getBranchID();

    /**
     * The client number.
     * 
     * @return
     */
    public Integer getClientNumber();

    /**
     * The date and time that the payable was created.
     * 
     * @return
     */
    public DateTime getCreateDate();

    /**
     * The user ID who created the payable.
     * 
     * @return
     */
    public String getCreateUserID();

    /**
     * The description of the payable.
     * 
     * @return
     */
    public String getDescription();

    /**
     * The product code of the product attached to the payable.
     * 
     * @return
     */
    public String getProductCode();

    /**
     * ???
     * 
     * @return
     */
    public String getProductQualifier();

    /**
     * The sales branch
     * 
     * @return
     */
    public String getSalesBranchCode();

    /**
     * The sequence number of the payable
     * 
     * @return
     */
    public Integer getSequenceNumber();

    /**
     * The source system of the payable
     * 
     * @return
     */
    public SourceSystem getSourceSystem();

    /**
     * The receipt list attached to the payable. There is typically only one
     * receipt per payable but can be more.
     * 
     * @return
     */
    public Collection getReceiptList();

    /**
     * Add a receipt to the receipt list.
     * 
     * @param r
     */
    public void addReceipt(Receipt receipt);

    /**
     * Get the first receipt and return the identifier.
     * 
     * @return
     * @throws RemoteException
     */
    public Receipt getFirstReceipt() throws RemoteException;

    public Exception getPayableException();

    /**
     * Get the name of the system that the payable originated from.
     * 
     * @return String
     */
    public String getSystemName();

    /**
     * The source system reference
     * 
     * @return String
     */
    public String getSourceSystemReferenceId();

    public String getReceiptNo();
}
