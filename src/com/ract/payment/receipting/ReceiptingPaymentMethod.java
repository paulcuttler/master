package com.ract.payment.receipting;

import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.payment.PaymentMethod;

/**
 * Receipting payment method.
 * 
 * @author Terry Bakker
 * @version 1.0, 23/08/2002
 */
public class ReceiptingPaymentMethod extends PaymentMethod {
    private String receiptingPaymentMethodID;

    public ReceiptingPaymentMethod(Integer cNum, SourceSystem ss) {
	super(cNum, ss);
    }

    public ReceiptingPaymentMethod(String paymentMethodID, Integer cNum, SourceSystem ss) throws SystemException {
	super(cNum, ss);
	setPaymentMethodID(paymentMethodID);
	// setReceiptingSequenceNumber(seqNumber);
    }

    public String getDescription() {
	return PaymentMethod.RECEIPTING;
    }

    /**
     * For a receipting type of payment method the payment method ID is the
     * sequence number for the clients payment in the receipting system.
     */
    public String getPaymentMethodID() {
	return this.receiptingPaymentMethodID;
    }

    public void setPaymentMethodID(String payMethodID) throws SystemException {
	this.receiptingPaymentMethodID = payMethodID;
    }

    /**
     * Return true if the payment method has been significantly changed. For
     * receipting payment methods the only significance is of the payment method
     * being compared to is different to this one.
     */
    public boolean hasSignificantlyChanged(PaymentMethod paymentMethod) {
	if (paymentMethod == null) {
	    return true;
	}

	if (!(paymentMethod instanceof ReceiptingPaymentMethod)) {
	    return true;
	}

	return super.hasSignificantlyChanged(paymentMethod);
    }

    /** @todo Should be getSequenceNumber. */
    // public Integer getReceiptingSequenceNumber()
    // {
    // return this.receiptingSequenceNumber;
    // }
    //
    // public void setReceiptingSequenceNumber(Integer seqNumber)
    // throws SystemException
    // {
    // this.paymentMethodId = seqNumber;
    // }
}