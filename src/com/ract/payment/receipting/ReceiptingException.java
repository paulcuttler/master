/*
 * @(#)ReceiptingException.java
 *
 */

package com.ract.payment.receipting;

import com.ract.payment.PaymentException;

/**
 * Generic exception for Receipting system.
 * 
 * @author Glenn Lewis
 * @version 1.0, 6/5/2002
 */
public class ReceiptingException extends PaymentException {
    public ReceiptingException() {
    }

    public ReceiptingException(String msg) {
	super(msg);
    }

    public ReceiptingException(Exception chainedException) {
	super(chainedException);
    }

    public ReceiptingException(String msg, Exception chainedException) {
	super(msg, chainedException);
    }
}