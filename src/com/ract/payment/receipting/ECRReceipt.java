package com.ract.payment.receipting;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.ract.util.DateTime;

public class ECRReceipt implements Receipt {

    private BigDecimal receiptAmount;
    private BigDecimal tenderedAmount;
    private BigDecimal changeAmount;
    private String logonId;
    private String salesBranch;
    private String receiptName;
    private String receiptNumber;
    private Integer branchNo;
    private DateTime backDate;
    private DateTime createDate;
    private ArrayList receiptLineList;

    public ECRReceipt() {
	super();
    }

    /**
     * @return the receiptAmount
     */
    public BigDecimal getReceiptAmount() {
	return receiptAmount;
    }

    /**
     * @param receiptAmount
     *            the receiptAmount to set
     */
    public void setReceiptAmount(BigDecimal receiptAmount) {
	this.receiptAmount = receiptAmount;
    }

    /**
     * @return the tenderedAmount
     */
    public BigDecimal getTenderedAmount() {
	return tenderedAmount;
    }

    /**
     * @param tenderedAmount
     *            the tenderedAmount to set
     */
    public void setTenderedAmount(BigDecimal tenderedAmount) {
	this.tenderedAmount = tenderedAmount;
    }

    /**
     * @return the changeAmount
     */
    public BigDecimal getChangeAmount() {
	return changeAmount;
    }

    /**
     * @param changeAmount
     *            the changeAmount to set
     */
    public void setChangeAmount(BigDecimal changeAmount) {
	this.changeAmount = changeAmount;
    }

    /**
     * @return the logonID
     */
    public String getLogonId() {
	return logonId;
    }

    /**
     * @param logonID
     *            the logonID to set
     */
    public void setLogonId(String logonID) {
	this.logonId = logonID;
    }

    /**
     * @return the salesBranch
     */
    public String getSalesBranch() {
	return salesBranch;
    }

    /**
     * @param salesBranch
     *            the salesBranch to set
     */
    public void setSalesBranch(String salesBranch) {
	this.salesBranch = salesBranch;
    }

    /**
     * @return the receiptName
     */
    public String getReceiptName() {
	return receiptName;
    }

    /**
     * @param receiptName
     *            the receiptName to set
     */
    public void setReceiptName(String receiptName) {
	this.receiptName = receiptName;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
	return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(String receiptNumber) {
	this.receiptNumber = receiptNumber;
    }

    /**
     * @return the branchNo
     */
    public Integer getBranchNo() {
	return branchNo;
    }

    /**
     * @param branchNo
     *            the branchNo to set
     */
    public void setBranchNo(Integer branchNo) {
	this.branchNo = branchNo;
    }

    /**
     * @return the backDate
     */
    public DateTime getBackDate() {
	return backDate;
    }

    /**
     * @param backDate
     *            the backDate to set
     */
    public void setBackDate(DateTime backDate) {
	this.backDate = backDate;
    }

    /**
     * @return the createDate
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    /**
     * @param createDate
     *            the createDate to set
     */
    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    /**
     * @return the receiptLineList
     */
    public ArrayList getReceiptLineList() {
	return receiptLineList;
    }

    /**
     * @param receiptLineList
     *            the receiptLineList to set
     */
    public void setReceiptLineList(ArrayList receiptLineList) {
	this.receiptLineList = receiptLineList;
    }

    @Override
    public ReceiptLine getReceiptLine(int lineNumber) {
	// TODO Auto-generated method stub
	return null;
    }

}
