package com.ract.payment.receipting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.SourceSystem;
import com.ract.util.DateTime;

/**
 * Represents an oi-payable
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class OCRPayable implements Payable, Serializable {
    private Integer clientNumber;

    private Integer sequenceNumber;

    private DateTime createDate;

    private BigDecimal amount;

    private BigDecimal amountOutstanding;

    private String createUserID;

    private String salesBranchCode;

    private Integer branchID;

    private String description;

    private SourceSystem sourceSystem;

    private String productCode;

    private String productQualifier;

    private DateTime receiptDate;

    private String receiptUserID;

    private String receiptBranchCode;

    /**
     * A list of the OI-Receipt receipts
     */
    private ArrayList receiptList;

    /**
     * A list of distributions to accounts that were made when the amount
     * payable was paid. Will be empty if the amout payable has not been paid.
     */
    // private ArrayList distributionList;

    private Exception payableException;

    private String sourceSystemReferenceId;

    public void setSourceSystemReferenceId(String sourceSystemReferenceId) {
	this.sourceSystemReferenceId = sourceSystemReferenceId;
    }

    public void setPaymentException(Exception exception) {
	this.payableException = exception;
    }

    /*
     * public OCRPayable( Integer clientNumber, Integer sequenceNumber, DateTime
     * createDate, BigDecimal amount, BigDecimal amountOutstanding, String
     * createUserID, String salesBranchCode, Integer branchID, String
     * description, SourceSystem sourceSystem, DateTime receiptDate, String
     * receiptUserID, String receiptBranchCode ) { this.clientNumber =
     * clientNumber; this.sequenceNumber = sequenceNumber; this.createDate =
     * createDate; this.amount = amount; this.amountOutstanding =
     * amountOutstanding; this.createUserID = createUserID; this.salesBranchCode
     * = salesBranchCode; this.branchID = branchID; this.description =
     * description; this.sourceSystem = sourceSystem; this.receiptDate =
     * receiptDate; this.receiptUserID = receiptUserID; this.receiptBranchCode =
     * receiptBranchCode; // this.distributionList = distributionList;
     * this.receiptList = null; this.distributionList = null; }
     */

    public OCRPayable(Integer clientNumber, Integer sequenceNumber, DateTime createDate, BigDecimal amount, BigDecimal amountOutstanding, String createUserID, String salesBranchCode, Integer branchID, String prodCode, String description, SourceSystem sourceSystem, String sourceSystemReferenceId, DateTime receiptDate, String receiptUserID, // user
																																										     // id
	    String receiptBranchCode, // sales branch
	    Collection receiptList) {
	this.clientNumber = clientNumber;
	this.sequenceNumber = sequenceNumber;
	this.createDate = createDate;
	this.amount = amount;
	this.amountOutstanding = amountOutstanding;
	this.createUserID = createUserID;
	this.salesBranchCode = salesBranchCode;
	this.branchID = branchID;
	this.description = description;
	this.sourceSystem = sourceSystem;
	this.sourceSystemReferenceId = sourceSystemReferenceId;
	this.receiptDate = receiptDate;
	this.receiptUserID = receiptUserID;
	this.receiptBranchCode = receiptBranchCode;
	this.receiptList = (ArrayList) receiptList;
	this.productCode = prodCode;
    }

    /*
     * public OCRPayable( Integer clientNumber, Integer sequenceNumber, DateTime
     * createDate, BigDecimal amount, BigDecimal amountOutstanding, String
     * createUserID, String salesBranchCode, Integer branchID, String
     * description, SourceSystem sourceSystem, String prodCode, String prodQual,
     * DateTime receiptDate, String receiptUserID, String receiptBranchCode,
     * Collection receiptList ) { this.clientNumber = clientNumber;
     * this.sequenceNumber = sequenceNumber; this.createDate = createDate;
     * this.amount = amount; this.amountOutstanding = amountOutstanding;
     * this.createUserID = createUserID; this.salesBranchCode = salesBranchCode;
     * this.branchID = branchID; this.description = description;
     * this.sourceSystem = sourceSystem; this.productCode = prodCode;
     * this.productQualifier = prodQual; this.receiptDate = receiptDate;
     * this.receiptUserID = receiptUserID; this.receiptBranchCode =
     * receiptBranchCode; this.receiptList = (ArrayList)receiptList;
     * this.distributionList = null; }
     */

    public BigDecimal getAmount() {
	return amount;
    }

    public BigDecimal getAmountOutstanding() {
	return amountOutstanding;
    }

    public Integer getBranchID() {
	return branchID;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public String getDescription() {
	return description;
    }

    public String getProductCode() {
	return productCode;
    }

    public String getProductQualifier() {
	return productQualifier;
    }

    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    public Integer getSequenceNumber() {
	return sequenceNumber;
    }

    public SourceSystem getSourceSystem() {
	return sourceSystem;
    }

    public String getCreateUserID() {
	return createUserID;
    }

    public Exception getPayableException() {
	return payableException;
    }

    public String getSourceSystemReferenceId() {
	return sourceSystemReferenceId;
    }

    public String getSystemName() {
	return SourceSystem.OCR.getAbbreviation();
    }

    public synchronized void addReceipt(Receipt rec) {
	if (this.receiptList == null) {
	    receiptList = new ArrayList();
	}

	receiptList.add(rec);
    }

    public Collection getReceiptList() {
	return receiptList;
    }

    public Receipt getFirstReceipt() throws RemoteException {
	OCRReceipt receipt = null;

	if (receiptList != null && !receiptList.isEmpty()) {
	    receipt = ((OCRReceipt) receiptList.iterator().next());
	} else {
	    throw new RemoteException("No OI Receipt Found");
	}

	return receipt;
    }

    public OCRPayable(ReceiptingIdentifier uniqueIdentifier, Exception exception) {
	this.clientNumber = uniqueIdentifier.getClientNumber();
	this.sequenceNumber = new Integer(uniqueIdentifier.getPaymentMethodID());
	this.payableException = exception;
    }

    public String toString() {
	return "clientNo=" + this.clientNumber + ", sequenceNo=" + this.sequenceNumber + ", sourceSystem " + this.sourceSystem.getAbbreviation() + ", productCode=" + this.productCode + ", sourceSystemReferenceId=" + this.sourceSystemReferenceId;
    }

    @Override
    public String getReceiptNo() {
	// TODO Auto-generated method stub
	return null;
    }

    // public void write(PrintStream outStream)
    // {
    // OCRReceipt receipt = null;
    // outStream.println("OCR Payable:"
    // + "\n Client no =   " + this.clientNumber
    // + "\n Sequence no = " + this.sequenceNumber
    // + "\n Description = " + this.description
    // + "\n Create Date = " + this.createDate
    // + "\n Sub-system =  " + this.sourceSystem
    // + "\n Prod-code =   " + this.productCode
    // + "\n Amount =      " + this.amount
    // + "\n Outstanding = " + this.amountOutstanding
    // + "\n Receipt ID =  " + this.receiptUserID
    // + "\n Receipt Branch = " + this.receiptBranchCode
    // + "\n Receipt Date = " + this.receiptDate);
    //
    //
    // if(receiptList != null && receiptList.size() > 0)
    // {
    // Iterator it = receiptList.iterator();
    // while(it.hasNext())
    // {
    // receipt = (OCRReceipt)it.next();
    // outStream.println("   ReceiptNo = " + receipt.getReceiptNumber()
    // + "  Name: " + receipt.getReceiptName()
    // + "  Logon Id: " + receipt.getLogonId()
    // + "  Date: " + receipt.getCreateDate().formatLongDate()
    // + "  Amount: " + receipt.getReceiptAmount());
    //
    // }
    // }
    //
    // }

}
