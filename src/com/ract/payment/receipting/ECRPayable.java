package com.ract.payment.receipting;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;

import com.ract.common.SourceSystem;
import com.ract.common.ValidationException;
import com.ract.payment.PayableItemVO;
import com.ract.util.DateTime;

/**
 * Placeholder object to allow a PayableItemVO with a receiptNo to be
 * represented as an ECR Receipt.
 * 
 * @author newtong
 * 
 */
public class ECRPayable implements Payable {

    private Integer clientNumber;

    private Integer sequenceNumber;

    private BigDecimal amount;

    private BigDecimal amountOutstanding;

    private String createUserID;

    private String salesBranchCode;

    private Integer branchID;

    private String description;

    private SourceSystem sourceSystem;

    private DateTime createDate;

    private String productCode;

    private String productQualifier;

    private ArrayList receiptList = new ArrayList();

    private Exception PaymentException;

    private String sourceSystemReferenceId;

    private String receiptNo;

    public ECRPayable() {

    }

    /**
     * Constructor based on the associated payableItem. The only data that we
     * can retrieve from ECR is the receiptNo, so all other values that are set
     * are pulled straight from the payableItem.
     * 
     * @param payableItem
     */
    public ECRPayable(PayableItemVO payableItem) {
	super();
	this.setClientNumber(payableItem.getClientNo());
	this.setAmount(new BigDecimal(payableItem.getAmountPayable()));
	try {
	    this.setAmountOutstanding(new BigDecimal(payableItem.getAmountOutstanding()));
	} catch (ValidationException e) {

	}
	this.setCreateDate(null);
	this.setDescription(payableItem.getDescription());
	this.setProductCode(payableItem.getProductCode());
	this.setSourceSystem(payableItem.getSourceSystem());
	try {
	    this.setSequenceNumber(Integer.parseInt(payableItem.getReceiptNo()));
	} catch (NumberFormatException e) {

	}
	this.setReceiptNo(payableItem.getReceiptNo());
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public Integer getSequenceNumber() {
	return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
	this.sequenceNumber = sequenceNumber;
    }

    public BigDecimal getAmount() {
	return amount;
    }

    public void setAmount(BigDecimal amount) {
	this.amount = amount;
    }

    public BigDecimal getAmountOutstanding() {
	return amountOutstanding;
    }

    public void setAmountOutstanding(BigDecimal amountOutstanding) {
	this.amountOutstanding = amountOutstanding;
    }

    public String getCreateUserID() {
	return createUserID;
    }

    public void setCreateUserID(String createUserID) {
	this.createUserID = createUserID;
    }

    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public Integer getBranchID() {
	return branchID;
    }

    public void setBranchID(Integer branchID) {
	this.branchID = branchID;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public SourceSystem getSourceSystem() {
	return sourceSystem;
    }

    public void setSourceSystem(SourceSystem sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String getProductCode() {
	return productCode;
    }

    public void setProductCode(String productCode) {
	this.productCode = productCode;
    }

    public String getProductQualifier() {
	return productQualifier;
    }

    public void setProductQualifier(String productQualifier) {
	this.productQualifier = productQualifier;
    }

    public ArrayList getReceiptList() {
	return receiptList;
    }

    public void setReceiptList(ArrayList receiptList) {
	this.receiptList = receiptList;
    }

    public Exception getPaymentException() {
	return PaymentException;
    }

    public void setPaymentException(Exception paymentException) {
	PaymentException = paymentException;
    }

    public String getSourceSystemReferenceId() {
	return sourceSystemReferenceId;
    }

    public void setSourceSystemReferenceId(String sourceSystemReferenceId) {
	this.sourceSystemReferenceId = sourceSystemReferenceId;
    }

    @Override
    public void addReceipt(Receipt receipt) {
	// TODO Auto-generated method stub

    }

    @Override
    public Receipt getFirstReceipt() throws RemoteException {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Exception getPayableException() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getSystemName() {
	return SourceSystem.ECR.getAbbreviation();
    }

    public void setReceiptNo(String receiptNo) {
	this.receiptNo = receiptNo;
    }

    @Override
    public String getReceiptNo() {
	return this.receiptNo;
    }

}
