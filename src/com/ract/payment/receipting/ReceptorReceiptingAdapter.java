package com.ract.payment.receipting;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.SourceSystem;
import com.ract.common.SystemDataException;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.util.LogUtil;

/**
 * Read-only version of Receptor Receipting Adapter.
 * 
 * @author newtong
 * 
 */
public class ReceptorReceiptingAdapter extends ECRAdapter implements ReceiptingAdapter {

    public ReceptorReceiptingAdapter() {
    }

    @Override
    public String getSystemName() {
	return SourceSystem.RECEPTOR.getAbbreviation();
    }

    @Override
    public Collection<Payable> getPayableHistory(Integer clientNo) throws RemoteException {
	Collection<PayableItemVO> payableItems = PaymentEJBHelper.getPaymentMgr().getPayableHistory(clientNo);
	Collection<Payable> results = new ArrayList<Payable>();

	for (PayableItemVO payableItem : payableItems) {
	    if (payableItem.getPaymentSystem().equals(SourceSystem.RECEPTOR.getAbbreviation())) {
		ReceptorPayable payable = new ReceptorPayable(payableItem);
		results.add(payable);
	    }
	}

	return results;
    }

    @Override
    public Payable getPayable(String uniqueID, String sourceSystem) throws RemoteException, SystemDataException {

	ReceptorPayable payable = null;

	try {
	    Integer payableItemID = Integer.parseInt(uniqueID);
	    PayableItemVO payableItemVO = PaymentEJBHelper.getPaymentMgr().getPayableItem(payableItemID);

	    if (payableItemVO.getPaymentSystem().equals(SourceSystem.RECEPTOR.getAbbreviation())) {
		payable = new ReceptorPayable(payableItemVO);
	    }

	} catch (Exception e) {
	    LogUtil.fatal(getClass(), "Unable to retrieve payable by ID: " + uniqueID + ", source: " + sourceSystem);
	}

	return payable;
    }
}
