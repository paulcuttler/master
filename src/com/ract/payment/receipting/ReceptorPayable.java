package com.ract.payment.receipting;

import com.ract.common.SourceSystem;
import com.ract.payment.PayableItemVO;

/**
 * Placeholder object to allow a PayableItemVO with a receiptNo to be
 * represented as an Receptor Receipt.
 * 
 * @author newtong
 * 
 */
public class ReceptorPayable extends ECRPayable {

    public ReceptorPayable() {
    }

    /**
     * Constructor based on the associated payableItem. The only data that we
     * can retrieve from ECR is the receiptNo, so all other values that are set
     * are pulled straight from the payableItem.
     * 
     * @param payableItem
     */
    public ReceptorPayable(PayableItemVO payableItem) {
	super(payableItem);
    }

    @Override
    public String getSystemName() {
	return SourceSystem.RECEPTOR.getAbbreviation();
    }

}
