package com.ract.payment.receipting;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.ract.util.DateTime;

/**
 * <p>
 * Represent a receipt interface.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public interface Receipt {

    /**
     * Get the effective date of the receipt
     */
    public DateTime getBackDate();

    /**
     * Get the change amount
     */
    public BigDecimal getChangeAmount();

    /**
     * The date and time of the receipt
     */
    public DateTime getCreateDate();

    /**
     * The logon id of the user
     */
    public String getLogonId();

    /**
     * the amount paid
     */
    public BigDecimal getReceiptAmount();

    /**
     * the amount paid
     */
    public BigDecimal getTenderedAmount();

    /**
     * get the name on the receipt
     */
    public String getReceiptName();

    /**
     * the receipt number
     */
    public String getReceiptNumber();

    /**
     * the Sales Branch
     */
    public String getSalesBranch();

    /**
     * the Branch Number
     */
    public Integer getBranchNo();

    public ReceiptLine getReceiptLine(int lineNumber);

    public ArrayList getReceiptLineList();

}
