/*
 * @(#)HistoricalOCRAdapter.java
 *
 */
package com.ract.payment.receipting;

import java.rmi.RemoteException;
import java.util.Properties;

import com.ract.common.RollBackException;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentMethod;
import com.ract.payment.billpay.IVRPayment;

/**
 * Adaptor for the OCR system written in Progress at the RACT. This
 * implementation ONLY allows historical data to be pushed into RECEPTOR.
 * 
 * @author Sam McLennan
 * @created 17 Sep 2003
 * @version 1.0, 17/9/2003
 */

public class HistoricalReceiptingAdapter extends OCRAdapter {

    // private OCRAdapterProxy ocrAdapterProxy = null;
    private ProgressOCRTransactionProxy ocrTransactionProxy = null;

    /**
     * Create a paid fee in OCR
     * 
     * @param username
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param branch
     *            Description of the Parameter
     * @param till
     *            Description of the Parameter
     * @param receipt
     *            Description of the Parameter
     * @param products
     *            Description of the Parameter
     * @param paymentType
     *            Description of the Parameter
     * @param costs
     *            Description of the Parameter
     * @param tendered
     *            Description of the Parameter
     * @param data
     *            Description of the Parameter
     * @param date
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception SystemException
     *                Description of the Exception
     */

    public String createPaidFee(String username, String branch, String till, String receipt, String[] products, double[] costs, String[] paymentType, double[] tendered, Properties[] data, String comments, java.util.Date date, String sourceSystem) throws RemoteException, RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Create a payable fee in OCR
     * 
     * @param payableItem
     *            the payable item to be created
     */
    public String createPayableFee(PayableItemVO payableItem) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Description of the Method
     * 
     * @param clientNo
     *            Description of the Parameter
     * @param sourceSystem
     *            Description of the Parameter
     * @param salesBranchCode
     *            Description of the Parameter
     * @param productCode
     *            Description of the Parameter
     * @param userID
     *            Description of the Parameter
     * @param amountPayable
     *            Description of the Parameter
     * @param description
     *            Description of the Parameter
     * @param reference
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception SystemException
     *                Description of the Exception
     */
    public String createPayableFee(Integer clientNumber, String sourceSystem, String salesBranchCode, String productCode, String userID, double amountPayable, String description, String reference) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    public String createPayableFee(String clientNumber, String sequenceNumber, String product, double amountPayable, String description, String sourceSystem, String branch, String user, Properties[] properties) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Cancels the receipting payment method by setting the outstanding amount
     * to zero.
     * 
     * @param payableItem
     *            Description of the Parameter
     * @exception SystemException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void cancelPayableFee(PaymentMethod paymentMethod) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Receipt an IVR payment in the Progress System
     */
    public Receipt receiptIvrPayment(IVRPayment ivr) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Return an oi-receipt from the OCR receipting system.
     */
    public Payable getIVRReceipt(Integer clientNumber, Integer sequenceNumber, Integer subSequenceNumber) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Remove a payable fee. This method is transaction safe. The adapter must
     * be committed or rolled back by the caller.
     */
    public void removePayableFee(Integer clientNumber, Integer paymentMethodID) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Remove a payable fee from OCR
     */
    public void removePayableFee(PayableItemVO payableItem) throws RollBackException {
	throw new RollBackException("HistoricalOCRAdapter does not implement this method");
    }

    public String extractInsuranceIVR() throws RemoteException {
	throw new RemoteException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Commits the current transactional proxy
     */
    public void commit() throws SystemException {
	throw new SystemException("HistoricalOCRAdapter does not implement this method");
    }

    public void commit(boolean releaseConnection) throws SystemException {
	throw new SystemException("HistoricalOCRAdapter does not implement this method");
    }

    /**
     * Description of the Method
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws SystemException {
	throw new SystemException("HistoricalOCRAdapter does not implement this method");
    }

}
