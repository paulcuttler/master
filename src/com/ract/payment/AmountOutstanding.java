package com.ract.payment;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;

/**
 * Represent an amount outstanding for a client transaction.
 * 
 * @author jyh
 * @version 1.0
 */

public class AmountOutstanding implements Serializable {
    private BigDecimal amountOutstanding;

    private boolean fullyUnpaid;

    private String clientNumber;

    private String paymentMethodID;

    private String paymentMethodDescription;

    private BigDecimal amountPayable;

    private DateTime createDate;

    private String payableReferenceId;

    public boolean isFullyUnpaid() {
	return fullyUnpaid;
    }

    public BigDecimal getAmountOutstanding() {
	return amountOutstanding;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public BigDecimal getAmountPayable() {
	return amountPayable;
    }

    public String toString() {
	StringBuffer description = new StringBuffer();
	description.append(payableReferenceId);
	description.append(": ");
	description.append(clientNumber);
	description.append(" ");
	description.append(paymentMethodDescription);
	description.append(" ");
	description.append(amountOutstanding);
	return description.toString();
    }

    public String getPaymentMethodDescription() {
	return paymentMethodDescription;
    }

    public String getPaymentMethodID() {
	return paymentMethodID;
    }

    public String getClientNumber() {
	return clientNumber;
    }

    public String getPayableReferenceId() {
	return payableReferenceId;
    }

    public void setFullyUnpaid(boolean fullyUnpaid) {
	this.fullyUnpaid = fullyUnpaid;
    }

    public void setAmountOutstanding(BigDecimal amountOutstanding) {
	this.amountOutstanding = amountOutstanding;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public void setAmountPayable(BigDecimal amountPayable) {
	this.amountPayable = amountPayable;
    }

    public void setPaymentMethodDescription(String paymentMethodDescription) {
	this.paymentMethodDescription = paymentMethodDescription;
    }

    public void setPaymentMethodID(String paymentMethodID) {
	this.paymentMethodID = paymentMethodID;
    }

    public void setClientNumber(String clientNumber) {
	this.clientNumber = clientNumber;
    }

    public void setPayableReferenceId(String payableReferenceId) {
	this.payableReferenceId = payableReferenceId;
    }

    public String formatForDisplay() {
	StringBuffer amountOs = new StringBuffer();
	amountOs.append(CurrencyUtil.formatDollarValue(this.getAmountOutstanding()));
	return amountOs.toString();
    }

    public AmountOutstanding() {
	// initialise variables
	this.amountOutstanding = new BigDecimal(0);
    }
}
