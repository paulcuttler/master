package com.ract.payment.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.ApplicationServlet;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.RollBackException;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.admin.AdminCodeDescription;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.common.ui.CommonUIConstants;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.ui.MembershipUIConstants;
import com.ract.payment.PayableItemComponentVO;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentHelper;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.bpay.BPAYJob;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.finance.DebtorInvoice;
import com.ract.payment.finance.FinanceMgr;
import com.ract.payment.receipting.Payable;
import com.ract.security.Privilege;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

/**
 * Payment UI Controller.
 * 
 * @author John Holliday
 * @created 2 August 2002
 * @version 1.0
 */

public class PaymentUIC extends ApplicationServlet {

	/**
	 * @todo global variables in servlet can result in inconsistent results
	 */
	HashMap ivrLines = null;

	public void handleEvent_scheduleDebtorInvoiceFileJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String cronExpression = request.getParameter("cronExpression");
		LogUtil.log(this.getClass(), "cronExpression=" + cronExpression);
		PaymentHelper.scheduleDebtorInvoiceFileJob(cronExpression);
		forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_resetDebtorInvoiceFileProcessing(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
		// reset state
		financeMgr.endDebtorFileProcessing(new DateTime());
		forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_scheduleDebtorInvoiceReceiptJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String cronExpression = request.getParameter("cronExpression");
		LogUtil.log(this.getClass(), "cronExpression=" + cronExpression);
		PaymentHelper.scheduleDebtorInvoiceReceiptJob(cronExpression);
		forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_scheduleDebtorMasterFileJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String cronExpression = request.getParameter("cronExpression");
		LogUtil.log(this.getClass(), "cronExpression=" + cronExpression);
		PaymentHelper.scheduleDebtorMasterFileJob(cronExpression);
		forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_scheduleResetDebtorInvoiceFileJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String cronExpression = request.getParameter("cronExpression");
		LogUtil.log(this.getClass(), "cronExpression=" + cronExpression);
		PaymentHelper.scheduleResetDebtorInvoiceFileJob(cronExpression);
		forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	/**
	 * Handle the selectViewBPAY event
	 * 
	 */
	public void handleEvent_selectScheduleBPAY(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String pathName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.BPAY_AUDIT);

		File f = new File(pathName);

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith("bpay");
			}
		};

		File files[] = f.listFiles(filter);

		SortedSet s = null;
		if (files != null) {
			s = new TreeSet();
			int maxLen = 0;

			for (int i = 0; i < files.length; i++) {
				s.add(new AdminCodeDescription(files[i].getAbsolutePath(), files[i].getName(), 1));
			}
		}
		//
		// Add some things to the request;
		//
		request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
		request.getSession().setAttribute("pageTitle", "BPAY FILES");
		request.getSession().setAttribute("buttonMessage", "This will not work.");
		request.getSession().setAttribute("createEvent", "");
		request.getSession().setAttribute("selectEvent", "BPAY_GetJobSchedule");
		request.getSession().setAttribute("codeName", "inputFileName");
		request.getSession().setAttribute("UICPage", PaymentUIConstants.PAGE_PaymentUIC);
		request.getSession().setAttribute("origCode", "");

		forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
	}

	public void handleEvent_BPAY_GetJobSchedule(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String inputFileName = (String) request.getParameter("inputFileName");

		request.setAttribute("inputFileName", inputFileName);

		this.forwardRequest(request, response, PaymentUIConstants.Page_BPAYUpdateJob);

	}

	/**
	 * construct the BPAY Update
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_BPAY_DoUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {

		CommonMgr cm = CommonEJBHelper.getCommonMgr();

		String rmiPort = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_PORT);
		String rmiHost = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_HOST);

		String dateString = request.getParameter("runDate");
		String hoursString = request.getParameter("runHours");
		String minsString = request.getParameter("runMins");
		DateTime runTime = new DateTime(dateString);

		GregorianCalendar gc = new GregorianCalendar();
		gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(hoursString), Integer.parseInt(minsString), 0);

		ScheduleMgr sMgr = CommonEJBHelper.getScheduleMgr();

		JobDetail jobDetail = new JobDetail("BPAY", Scheduler.DEFAULT_GROUP, BPAYJob.class);

		jobDetail.setDescription("BPAY Update");

		SimpleTrigger trigger = new SimpleTrigger("BPAYtrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);

		JobDataMap dataMap = jobDetail.getJobDataMap();

		dataMap.put("inputFile", request.getParameter("inputFileName"));
		dataMap.put("createDateTime", DateUtil.formatLongDate(new DateTime()));
		String userId = this.getUserSession(request).getUserId();
		dataMap.put("userId", userId);
		String messageText = null;

		String providerURL = rmiHost + ":" + rmiPort;

		dataMap.put("providerURL", providerURL);

		try {

			sMgr.scheduleJob(jobDetail, trigger);

			request.setAttribute("heading", "BPAY Update Scheduled");
			messageText = "The BPAY Update will run at" + "<br/>" + (new DateTime(gc.getTime())).formatLongDate();
			request.setAttribute("message", messageText);
		} catch (Exception e) {
			request.setAttribute("heading", "WARNING: BPAY Update not scheduled");
			messageText = "The system was unable to schedule the BPAY Update: <br/>" + e;
			request.setAttribute("message", messageText);
		}
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	public void handleEvent_MotorNews_Cancel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setAttribute("heading", "Motor News Extract Cancelled");
		String cancelText = "You have chosen to cancel the Motor News Extract. \n" + "You may now continue with any other process.";
		request.setAttribute("message", cancelText);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	/**
	 * Handle the selectViewBPAY event
	 * 
	 */
	public void handleEvent_selectViewBPAY(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String pathName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS, SystemParameterVO.BPAY_AUDIT);
		File f = new File(pathName);

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith("Audi");
			}
		};

		File files[] = f.listFiles(filter);

		SortedSet s = null;
		if (files != null) {
			s = new TreeSet();
			int maxLen = 0;

			for (int i = 0; i < files.length; i++) {
				s.add(new AdminCodeDescription(files[i].getAbsolutePath(), files[i].getName(), 1));
			}
		}
		//
		// Add some things to the request;
		//
		request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
		request.getSession().setAttribute("pageTitle", "BPAY FILES");
		request.getSession().setAttribute("buttonMessage", "This will not work.");
		request.getSession().setAttribute("createEvent", "");
		request.getSession().setAttribute("selectEvent", "viewBPAYReport");
		request.getSession().setAttribute("codeName", "inputFilename");
		request.getSession().setAttribute("UICPage", PaymentUIConstants.PAGE_PaymentUIC);
		request.getSession().setAttribute("origCode", "");

		forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
	}

	public void handleEvent_viewThisBPAYLine(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String lineToView = (String) request.getParameter("listCode");
		ArrayList fields = (ArrayList) ivrLines.get(lineToView);
		String payment_file = (String) request.getParameter("inputFilename");

		request.setAttribute("filename", payment_file);
		request.setAttribute("lineToShow", fields);

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewFullBPAYLine);
	}

	/**
     *
    */
	public void handleEvent_viewBPAYReport(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		int amountField = 10;
		int[] showFields = { 7, 6, 10, 12, 13, 11, 15 };
		int errorField = 16;
		String toPage = PaymentUIConstants.PAGE_ViewBPAY;
		String audit = "BPAY";

		createElectronicReport(request, response, amountField, showFields, errorField, audit, toPage);

	}

	/**
	 * should be a helper method. html should not be generated here. html should reside in the jsp and data is sent to it.
	 * 
	 * @param fields ArrayList
	 * @return boolean
	 */
	private void createElectronicReport(HttpServletRequest request, HttpServletResponse response, int amountField, int[] showFields, int errorField, String audit, String toPage) throws ServletException {

		String payment_file = (String) request.getParameter("inputFilename");
		String viewAll = request.getParameter("listCode");
		request.setAttribute("filename", StringUtil.replaceAll(payment_file, "\\", "/"));

		String paidDate = null;
		StringBuffer sb = new StringBuffer();
		BufferedReader inp = null;

		String viewLineMethod = "'viewThis" + audit + "Line'";
		ArrayList fields = null;
		String bkColor = null;

		String errorColour = "dataValue";

		double thisAmount = 0.0;
		double totalAmount = 0.0;
		int totalCount = 0;
		double fileAmount = 0.0;
		int fileCount = 0;
		double successAmount = 0.0;
		int successCount = 0;
		double errorAmount = 0.0;
		int errorCount = 0;
		int fldNo = 0;

		boolean displayLine = false;
		int lineNo = 0;
		String lineKey = null;

		ivrLines = new HashMap();

		try {
			// Open the file
			inp = new BufferedReader(new FileReader(payment_file));
			String line = "";

			// Process the file line by line
			while ((line = inp.readLine()) != null) {
				// Parse the line
				fields = new ArrayList(errorField + 1);
				String fieldArray[] = line.split(",");
				for (int i = 0; i < fieldArray.length; i++) {
					fields.add(fieldArray[i]);
				}
				fileAmount += Double.parseDouble(fieldArray[10]);
				if (fieldArray[7] != null) {
					paidDate = " " + fieldArray[7];
				}
				// put the lines in a hashmap so we can diplay the full line if
				// requested
				lineKey = new Integer(lineNo).toString();
				ivrLines.put(lineKey, fields);
				lineNo++;

				// Payment Line - only print the errors

				StringBuffer wb = new StringBuffer();
				if (!(viewAll == null) && viewAll.equalsIgnoreCase("YES")) {
					displayLine = true;
				} else {
					displayLine = isThisAnErrorLine(errorField, fields);
				}
				bkColor = "#FFFFFF";
				// parse payment line
				wb.append("<tr bgColor =\"" + bkColor + "\" ");
				wb.append("onMouseOver=\"bgColor = '#CDD7E1'\" ");
				wb.append("onMouseOut=\"bgColor = '" + bkColor + "'\" ");
				wb.append("\" id=\"");
				wb.append(lineKey);
				wb.append("\" ondblclick=\"setListCodeAndSubmitForm(" + viewLineMethod + ",'");
				wb.append(lineKey + "')\" >");

				for (int i = 0; i < showFields.length; i++) {
					fldNo = showFields[i];
					String f = (String) fields.get(fldNo);
					wb.append("<td class=\"datavalue\" align=\"right\">");
					if (fldNo == amountField) {
						thisAmount = new Double(f).doubleValue();
						totalAmount += thisAmount;
						totalCount += 1;
						wb.append(NumberUtil.formatValue(thisAmount));
					} else {
						wb.append(f);
					}

					wb.append("</td>");
				}

				wb.append("</tr>");

				if (displayLine) {
					if (isThisAnErrorLine(errorField, fields)) {
						wb.append("<tr bgcolor=\"" + bkColor + "\"><td class=\"dataValue\" colspan=\"11\" align=\"right\">");
						wb.append(fields.get(errorField));
						wb.append("</td></tr>");
					}
					// Put this line in the buffer that goes back to the JSP

					sb.append(wb.toString());
				}

				if (isThisAnErrorLine(errorField, fields)) {
					errorAmount += thisAmount;
					errorCount += 1;
				} else {
					successAmount += thisAmount;
					successCount += 1;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inp.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		errorColour = "dataValue";
		if (successAmount != fileAmount) {
			errorColour = "dataValueRed";
		}

		if (sb.length() == 0) {
			sb.append("<tr><td class=\"dataValue\" align=\"center\" colspan=\"12\">&nbsp</td></tr>");
			sb.append("<tr><td class=\"dataValue\" align=\"center\" colspan=\"12\">==== NO ERRORS ====</td></tr>");
		}

		StringBuffer tb = new StringBuffer();

		tb.append("<tr>");
		tb.append("<td colspan=\"4\"> <table width=\"70%\" align=\"center\"><tr>");
		tb.append("<td>" + audit.toUpperCase() + " AUDIT </td><td>" + payment_file + "</td>");
		tb.append("</tr>");
		tb.append("<tr>");
		tb.append("<td>Payments made</td><td>" + paidDate + "</td>");
		tb.append("</tr>");
		tb.append("<tr>");
		tb.append("<td>Amount in file</td><td class=\"" + errorColour + "\">" + NumberUtil.formatValue(fileAmount) + "</td>");
		tb.append("</tr></table></td>");
		tb.append("</tr>");

		tb.append("<tr><td colspan=\"4\">&nbsp</td></tr>");

		tb.append("<tr align=\"right\"><td width=\"35%\">Processed</td>");
		tb.append("<td width=\"15%\" class=\"label\">Amount</td>");
		tb.append("<td  width=\"15%\"class=\"label\">Count</td>");
		tb.append("<td width=\"35%\">&nbsp</td></tr>");

		tb.append("<tr align=\"right\" >");
		tb.append("<td width=\"35%\" class=\"label\">Successful</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + NumberUtil.formatValue(successAmount) + "</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + successCount + "</td>");
		tb.append("<td width=\"35%\">&nbsp</td>");
		tb.append("</tr>");

		tb.append("<tr align=\"right\" >");
		tb.append("<td width=\"35%\" class=\"label\">Error</td>");
		tb.append("<td width=\"15%\" class=\"" + errorColour + "\">" + NumberUtil.formatValue(errorAmount) + "</td>");
		tb.append("<td width=\"15%\" class=\"" + errorColour + "\">" + errorCount + "</td>");
		tb.append("<td width=\"35%\">&nbsp</td>");
		tb.append("</tr>");

		tb.append("<tr><td align=\"center\" colspan=\"4\">&nbsp</td></tr>");

		tb.append("<tr align=\"right\">");
		tb.append("<td width=\"35%\" class=\"label\">Total</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + NumberUtil.formatValue(totalAmount) + "</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + totalCount + "</td>");
		tb.append("<td width=\"35%\">&nbsp</td>");
		tb.append("</tr>");

		request.setAttribute("tableBody", sb.toString());
		request.setAttribute("totals", tb.toString());

		forwardRequest(request, response, toPage);
	}

	/**
	 * Handle the selectViewIVR event
	 * 
	 */
	public void handleEvent_selectViewIVR(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String pathName = commonMgr.getSystemParameterValue("BPY", "AUDIT");

		File f = new File(pathName);

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith("Audit");
			}
		};

		File files[] = f.listFiles(filter);

		SortedSet s = null;
		if (files != null) {
			s = new TreeSet();
			int maxLen = 0;

			for (int i = 0; i < files.length; i++) {
				s.add(new AdminCodeDescription(files[i].getAbsolutePath(), files[i].getName(), 1));
			}
		}
		//
		// Add some things to the request;
		//
		request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
		request.getSession().setAttribute("pageTitle", "IVR FILES");
		request.getSession().setAttribute("buttonMessage", "This will not work.");
		request.getSession().setAttribute("createEvent", "");
		request.getSession().setAttribute("selectEvent", "viewIVRReport");
		request.getSession().setAttribute("codeName", "ivrFilename");
		request.getSession().setAttribute("UICPage", PaymentUIConstants.PAGE_PaymentUIC);
		request.getSession().setAttribute("origCode", "");

		forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
	}

	public void handleEvent_viewThisIvrLine(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String lineToView = (String) request.getParameter("listCode");
		ArrayList fields = (ArrayList) ivrLines.get(lineToView);
		String payment_file = (String) request.getParameter("ivrFilename");

		request.setAttribute("filename", payment_file);
		request.setAttribute("lineToShow", fields);

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewFullIVRLine);
	}

	/**
	 * @todo HTML should not generated here. This is a controller and should only control process flow. This should be delegated to a helper for processing
	 */
	public void handleEvent_viewIVRReport(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payment_file = (String) request.getParameter("ivrFilename");
		String viewAll = request.getParameter("listCode");
		request.setAttribute("filename", StringUtil.replaceAll(payment_file, "\\", "/"));

		String[] paidDate = { " ", " ", " ", " ", " " };
		int pdDateNdx = 0;
		StringBuffer sb = new StringBuffer();
		BufferedReader inp = null;

		ArrayList fields = null;
		String bkColor = null;
		String errorColour = "dataValue";
		String errorLabel = "label";

		double thisAmount = 0.0;
		double totalAmount = 0.0;
		int totalCount = 0;
		double fileAmount = 0.0;
		int fileCount = 0;
		double successAmount = 0.0;
		int successCount = 0;
		double errorAmount = 0.0;
		int errorCount = 0;
		int[] showFields = { 1, 4, 5, 7, 8, 10, 11 };

		boolean displayLine = false;
		int lineNo = 0;
		int cDateNdx = 0;

		String lineKey = null;

		ivrLines = new HashMap();

		try {
			// Open the BillPay file from telstra
			inp = new BufferedReader(new FileReader(payment_file));
			String line = "";

			// Process the BillPay file line by line
			while ((line = inp.readLine()) != null) {
				// Discard the header information
				if (!line.endsWith("Not a payment line:") && !line.startsWith("\"")) {
					// Parse the line
					fields = new ArrayList();
					String lineTokens[] = line.split(",");
					int numFields = lineTokens.length;
					for (int i = 0; i < numFields; i++) {
						fields.add(lineTokens[i]);
					}

					// if(line.endsWith("Header Line:"))
					// {
					// // parse header line
					// try
					// {
					// paidDate = (String)fields.get(2);
					// }
					// catch(Exception e)
					// {
					// paidDate = new DateTime().formatShortDate();
					// }
					// }
					// else
					if (line.endsWith("Total Line:")) {
						// parse total line
						try {
							String f = (String) fields.get(1);
							fileAmount = new Double(f).doubleValue() / 100;
						} catch (Exception e) {
							fileAmount = 0.0;
						}
					} else {
						// put the lines in a hashmap so we can diplay the full
						// line if requested
						lineKey = new Integer(lineNo).toString();
						ivrLines.put(lineKey, fields);
						lineNo++;

						// Payment Line - only print the errors

						StringBuffer wb = new StringBuffer();
						if (!(viewAll == null) && viewAll.equalsIgnoreCase("YES")) {
							displayLine = true;
						} else {
							displayLine = isThisAnErrorLine(fields);
						}

						bkColor = "#FFFFFF";
						// parse payment line
						wb.append("<tr bgColor =\"" + bkColor + "\" ");
						wb.append("onMouseOver=\"bgColor = '#CDD7E1'\" ");
						wb.append("onMouseOut=\"bgColor = '" + bkColor + "'\" ");
						wb.append("\" id=\"");
						wb.append(lineKey);
						wb.append("\" ondblclick=\"setListCodeAndSubmitForm('viewThisIvrLine','");
						wb.append(lineKey + "')\" >");

						for (int i = 0; i < numFields - 1; i++) {
							String f = (String) fields.get(i);
							LogUtil.log(this.getClass(), "RLG ++++ " + f + " - " + i);
							if (i == 1) {

								cDateNdx = pdDateNdx == 0 ? 0 : pdDateNdx - 1;

								if (!f.equalsIgnoreCase(paidDate[cDateNdx])) {
									paidDate[pdDateNdx] = f;
									pdDateNdx++;
								}
							}

							if (useThisField(i)) {
								wb.append("<td class=\"datavalue\" align=\"right\">");
								if (i == 5) {
									thisAmount = new Double(f).doubleValue();
									totalAmount += thisAmount;
									totalCount += 1;
									wb.append(NumberUtil.formatValue(thisAmount));
								} else {
									wb.append(f);
								}
								wb.append("</td>");
							}
						}
						wb.append("</tr>");

						if (displayLine) {
							if (isThisAnErrorLine(fields)) {
								wb.append("<tr bgcolor=\"" + bkColor + "\"><td class=\"dataValue\" colspan=\"11\" align=\"right\">");
								wb.append(fields.get(12));
								wb.append("</td></tr>");
							}
							// Put this line in the buffer that goes back to the
							// JSP

							sb.append(wb.toString());
						}

						if (isThisAnErrorLine(fields)) {
							errorAmount += thisAmount;
							errorCount += 1;
						} else {
							successAmount += thisAmount;
							successCount += 1;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inp.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		errorColour = "dataValue";
		errorLabel = "label";

		if (NumberUtil.roundDouble(successAmount, 2) != NumberUtil.roundDouble(fileAmount, 2)) {
			errorColour = "dataValueRed";
			errorLabel = "dataValueRed";
		}

		if (sb.length() == 0) {
			sb.append("<tr><td class=\"dataValue\" align=\"center\" colspan=\"12\">&nbsp</td></tr>");
			sb.append("<tr><td class=\"dataValue\" align=\"center\" colspan=\"12\">==== NO ERRORS ====</td></tr>");
		}

		StringBuffer tb = new StringBuffer();
		StringBuffer pd = new StringBuffer();

		pd.append(paidDate[0]);
		for (int i = 1; i < pdDateNdx; i++) {
			pd.append(" , ");
			pd.append(paidDate[i]);
		}

		tb.append("<tr>");
		tb.append("<td colspan=\"4\"> <table width=\"70%\" align=\"center\"><tr>");
		tb.append("<td>IVR AUDIT </td><td>" + payment_file + "</td>");
		tb.append("</tr>");
		tb.append("<tr>");
		tb.append("<td>Payments made</td><td>" + pd.toString() + "</td>");
		tb.append("</tr>");
		tb.append("<tr>");
		tb.append("<td class=\"" + errorLabel + "\">Amount in file</td>");
		tb.append("<td class=\"" + errorColour + "\">" + NumberUtil.formatValue(fileAmount) + "</td>");
		tb.append("</tr></table></td>");
		tb.append("</tr>");

		tb.append("<tr><td colspan=\"4\">&nbsp</td></tr>");

		tb.append("<tr align=\"right\"><td width=\"35%\">Processed</td>");
		tb.append("<td width=\"15%\" class=\"label\">Amount</td>");
		tb.append("<td  width=\"15%\"class=\"label\">Count</td>");
		tb.append("<td width=\"35%\">&nbsp</td></tr>");

		tb.append("<tr align=\"right\" >");
		tb.append("<td width=\"35%\" class=\"label\">Successful</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + NumberUtil.formatValue(successAmount) + "</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + successCount + "</td>");
		tb.append("<td width=\"35%\">&nbsp</td>");
		tb.append("</tr>");

		tb.append("<tr align=\"right\" >");
		tb.append("<td width=\"35%\" class=\"" + errorLabel + "\">Error</td>");
		tb.append("<td width=\"15%\" class=\"" + errorColour + "\">" + NumberUtil.formatValue(errorAmount) + "</td>");
		tb.append("<td width=\"15%\" class=\"" + errorColour + "\">" + errorCount + "</td>");
		tb.append("<td width=\"35%\">&nbsp</td>");
		tb.append("</tr>");

		tb.append("<tr><td align=\"center\" colspan=\"4\">&nbsp</td></tr>");

		tb.append("<tr align=\"right\">");
		tb.append("<td width=\"35%\" class=\"label\">Total</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + NumberUtil.formatValue(totalAmount) + "</td>");
		tb.append("<td width=\"15%\" class=\"dataValue\">" + totalCount + "</td>");
		tb.append("<td width=\"35%\">&nbsp</td>");
		tb.append("</tr>");

		request.setAttribute("tableBody", sb.toString());
		request.setAttribute("totals", tb.toString());

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewIVR);
	}

	/**
	 * should be a property of an ivr line or be a helper method
	 * 
	 * @param fields ArrayList
	 * @return boolean
	 */
	private boolean useThisField(int i) {
		if (i == 1 || i == 4 || i == 5 || i == 7 || i == 8 || i == 10 || i == 11) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * should be a property of an ivr line or be a helper method
	 * 
	 * @param fields ArrayList
	 * @return boolean
	 */
	private boolean displayThisfield(int i, int[] flds) {
		boolean use;
		use = false;

		for (int y = 0; y < flds.length && use == false; y++) {
			if (flds[y] == i) {
				use = true;
			}
		}
		return use;
	}

	/**
	 * should be a property of an ivr line or be a helper method
	 * 
	 * @param fields ArrayList
	 * @return boolean
	 */
	private boolean isThisAnErrorLine(ArrayList fields) {
		return isThisAnErrorLine(12, fields);
	}

	/**
	 * should be a property of an ivr line or be a helper method
	 * 
	 * @param fields ArrayList
	 * @return boolean
	 */
	private boolean isThisAnErrorLine(int fldNo, ArrayList fields) {
		if (((String) fields.get(fldNo)).indexOf("OK") != -1) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * view the direct debit schedule
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewDirectDebitSchedule(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String directDebitPaymentMethodId = request.getParameter("directDebitPaymentMethodID");
		if (directDebitPaymentMethodId != null) {
			try {
				PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
				DirectDebitSchedule ddSchedule = pMgr.getDirectDebitSchedule(new Integer(directDebitPaymentMethodId));

				ClientVO clientVO = getClientVO(ddSchedule.getClientNumber());

				request.setAttribute("ddSchedule", ddSchedule);
				request.setAttribute("client", clientVO);
			} catch (Exception re) {
				throw new ServletException("Unable to get the direct debit schedule for payment method ID " + directDebitPaymentMethodId);
			}
		}
		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewDirectDebitSchedule);
	}

	public void handleEvent_updatePayableData(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		PaymentTransactionMgr paymentMgr = null;
		try {
			paymentMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			paymentMgr.updatePayableData();
		} catch (RemoteException ex) {
		}
	}

	/**
	 * Display a list of PayableItems for the client.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItemList(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		Integer clientNumber = new Integer(request.getParameter("clientNumber"));

		ClientVO clientVO = getClientVO(clientNumber);
		request.setAttribute("client", clientVO);

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

		try {
			Collection payableItems = paymentMgr.getPayableItemsByClient(clientNumber);
			request.setAttribute("payableItems", payableItems);
		} catch (PaymentException ex) {
			throw new SystemException("Unable to get payable item list for client '" + clientNumber + "'.");
		}

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewPayableItemList);
	}

	/**
	 * Display the detail of a PayableItem for the client.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPendingFees(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String clientNumberString = request.getParameter("clientNumber");
		Integer clientNumber = new Integer(clientNumberString);
		PendingFee pendingFee = null;
		// Add the membership to the request so that the view pages can use it.
		try {
			PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
			Collection pendingFeeList = paymentMgr.getPendingFeeByClientNumber(clientNumber);

			request.setAttribute("pendingFeeList", pendingFeeList);
			request.setAttribute("clientNumber", clientNumber);
		} catch (Exception e) {
			throw new ServletException("Unable to find pending fees for client " + clientNumberString + "." + e);
		}

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewPendingFees);
	}

	/**
	 * Display the detail of a PayableItem for the client.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItem_viewDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payableItemIDString = request.getParameter("payableItemID");
		LogUtil.debug(this.getClass(), "payableItemID = " + payableItemIDString);

		// Add the membership to the request so that the view pages can use it.
		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			PayableItemVO pItem = pMgr.getPayableItem(new Integer(payableItemIDString));
			LogUtil.debug(this.getClass(), "setting pItem = " + pItem.toString());
			request.setAttribute("payableItem", pItem);
		} catch (Exception e) {
			throw new ServletException("Unable to find payable item " + payableItemIDString + "." + e);
		}

		String pageName = PaymentUIConstants.PAGE_ViewPayableItemDetail;
		forwardRequest(request, response, pageName);
	}

	/**
	 * Display the details of a PayableItem Component for the PayableItem.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItem_viewComponents(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		Integer payableItemID = new Integer(request.getParameter("payableItemID"));
		request.setAttribute("payableItemID", payableItemID);
		LogUtil.log(this.getClass(), "payableItemID=" + payableItemID);

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		PayableItemVO payableItem = null;

		try {
			payableItem = paymentMgr.getPayableItem(payableItemID);
			request.setAttribute("client", payableItem.getClient());
		} catch (PaymentException ex) {
			throw new RemoteException("Unable to get payable item '" + payableItemID + "'.");
		}

		Collection payableItemComponents = payableItem.getComponents();
		request.setAttribute("payableItemComponents", payableItemComponents);

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewPayableItemComponent);
	}

	/**
	 * View a set of postings that have been selected by subsystem, effective date, entered date and account number
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPostingSummaryResults(HttpServletRequest request, HttpServletResponse response) throws SystemException, ServletException {
		String journalKey = request.getParameter("journalKey");

		LogUtil.debug(this.getClass(), "journalKey=" + journalKey);

		Collection postingsSet = null;
		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			postingsSet = pMgr.getPostingSummary(journalKey);
		} catch (Exception e) {
			throw new SystemException("Unable to get the posting summary.", e);
		}

		request.setAttribute("postingsSet", postingsSet);

		this.forwardRequest(request, response, PaymentUIConstants.PAGE_ViewPostingSummary);

	}

	/**
	 * View a set of postings that have been selected by subsystem, effective date, entered date and account number
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPostingDetails(HttpServletRequest request, HttpServletResponse response) throws SystemException, ServletException {
		String journalKey = request.getParameter("journalKey");

		LogUtil.debug(this.getClass(), "journalKey=" + journalKey);

		Collection postingsSet = null;

		LogUtil.debug(this.getClass(), "1");

		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			postingsSet = pMgr.getPostingSummaryDetails(journalKey);
		} catch (Exception e) {
			throw new SystemException("Unable to get the posting summary details.", e);
		}

		LogUtil.debug(this.getClass(), "2");

		request.setAttribute("postingsSet", postingsSet);
		request.setAttribute("journalKey", journalKey);

		LogUtil.debug(this.getClass(), "3");

		this.forwardRequest(request, response, "/payment/ViewPostingSummary.jsp");

	}

	/**
	 * Display the details of a PayableItem for the client.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItem_viewActionItems(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payableItemIDString = request.getParameter("payableItemID");
	}

	/**
	 * Display the details of a PayableItem for the client.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItem_viewSourceSystem(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payableItemIDString = request.getParameter("payableItemID");
	}

	/**
	 * Display the details of a PayableItem for the client.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItem_viewPayment(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payableItemIDString = request.getParameter("payableItemID");
	}

	/**
	 * Handle the event of clicking the "Posting summary" link on the "View payable item" page. Forward to the ViewPayableItemPostingSummary page.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItem_postingSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		// String payableItemIDString = request.getParameter("payableItemID");
		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewPayableItemPostingSummary);
	}

	/**
	 * Display the details of a PayableItem for the client.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_viewPayableItemComponent_viewPosting(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		// PayableItemVO payableItem = null;
		PayableItemComponentVO payableItemComponent = null;

		Integer payableItemComponentID = new Integer(request.getParameter("payableItemComponentID"));
		// check
		if (payableItemComponentID == null) {
			throw new ServletException("There is no component ID available for postings.");
		}

		// Got to get the payableItem, then a list of the Components. From the
		// list of components pick the right one
		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			payableItemComponent = pMgr.getPayableItemComponent(payableItemComponentID);

			request.setAttribute("client", payableItemComponent.getPayableItem().getClient());
		} catch (Exception e) {
			throw new ServletException("Unable to get Payment Mgr!", e);
		}

		if (payableItemComponent == null) {
			throw new ServletException("There is no component available for postings.");
		}
		request.setAttribute("payableItemComponent", payableItemComponent);
		request.setAttribute("payableItemPostings", payableItemComponent.getPostings());

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewPayableItemPosting);
	}

	/**
	 * Get a client value object for the specified client id.
	 * 
	 * @param clientNumberText Description of the Parameter
	 * @return The clientVO value
	 * @exception ServletException Description of the Exception
	 */
	private ClientVO getClientVO(Integer clientNumber) throws ServletException {
		ClientVO clientVO = null;
		if (clientNumber != null) {
			// Convert the client number to an integer.
			try {
				ClientMgr cMgr = ClientEJBHelper.getClientMgr();
				clientVO = cMgr.getClient(clientNumber);
			} catch (Exception e) {
				throw new ServletException("Unable to get clientVO. Client number was " + clientNumber + "." + e);
			}
		}
		return clientVO;
	}

	/**
	 * View a list of GL transactions.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 */
	public void handleEvent_viewGLTransactionList(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		String effStartDateString = request.getParameter("effStartDate");
		String effEndDateString = request.getParameter("effEndDate");
		String entStartDateString = request.getParameter("entStartDate");
		String entEndDateString = request.getParameter("entEndDate");
		String account = request.getParameter("account");
		String subsystem = request.getParameter("subsystem");
		String entity = request.getParameter("entity");
		String mode = request.getParameter("mode");
		LogUtil.debug(this.getClass(), "mode=" + mode);
		DateTime effStartDate = null;
		try {
			if (effStartDateString != null)
				effStartDate = new DateTime(effStartDateString);
			DateTime effEndDate = null;
			effEndDate = new DateTime(effEndDateString);
			DateTime entStartDate = null;
			entStartDate = new DateTime(entStartDateString);
			DateTime entEndDate = null;
			entEndDate = new DateTime(entEndDateString);

			FinanceMgr fMgr = PaymentEJBHelper.getFinanceMgr();
			Collection glTransactionList = fMgr.getGLTransactions(effStartDate, effEndDate, entStartDate, entEndDate, subsystem, account, entity);
			request.setAttribute("glTransactionList", glTransactionList);
			request.setAttribute("effStartDate", effStartDate);
			request.setAttribute("effEndDate", effEndDate);
			request.setAttribute("entStartDate", entStartDate);
			request.setAttribute("entEndDate", entEndDate);
			request.setAttribute("account", account);
			request.setAttribute("subsystem", subsystem);
			request.setAttribute("entity", entity);
			request.setAttribute("mode", mode);
		} catch (Exception ex) {
			throw new ServletException(ex);

		}

		this.forwardRequest(request, response, PaymentUIConstants.PAGE_VIEW_GL_TRANSACTION_LIST);
	}

	/**
	 * Enter the query parameters
	 */
	public void handleEvent_enterGLTransactionParameters(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		this.forwardRequest(request, response, PaymentUIConstants.PAGE_ENTER_GL_TRANSACTION_PARAMETERS);
	}

	public void handleEvent_transferDirectDebitSchedule(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		this.forwardRequest(request, response, PaymentUIConstants.PAGE_TRANSFER_DIRECT_DEBIT_SCHEDULE);
	}

	public void handleEvent_viewPayable_transferDirectDebitSchedule(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		transferDirectDebitScheduleGetPayable(request, response);
	}

	/**
	 * Get the payable item by payable item id.
	 */
	public void handleEvent_transferDirectDebitSchedule_getPayable(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		transferDirectDebitScheduleGetPayable(request, response);
	}

	/**
	 * Get the payable item by payable item id.
	 */
	private void transferDirectDebitScheduleGetPayable(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		String payableItemIDString = request.getParameter("payableItemID");
		if (payableItemIDString == null || payableItemIDString.equals("")) {
			throw new ValidationException("No payable item Id has been entered.");
		}
		Integer payableItemID = new Integer(payableItemIDString);

		LogUtil.debug(this.getClass(), "payableItemIDString=" + payableItemIDString);

		PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
		PayableItemVO payableItem = null;
		try {
			payableItem = pMgr.getPayableItem(payableItemID);
		} catch (PaymentException ex) {
			throw new ServletException();
		}

		PaymentMethod paymentMethod = payableItem.getPaymentMethod();

		if (!(paymentMethod instanceof DirectDebitSchedule)) {
			throw new SystemException("Payable item '" + payableItemIDString + "' is not attached to a schedule.");
		}

		DirectDebitSchedule ddSchedule = (DirectDebitSchedule) paymentMethod;

		Payable payable = ddSchedule.getPayable();

		if (payable.getAmountOutstanding().compareTo(new BigDecimal(0)) <= 0) {
			throw new SystemException("The direct debit schedule selected does not have an amount outstanding in the receipting system.");
		}

		request.setAttribute("payableItem", payableItem);

		this.forwardRequest(request, response, PaymentUIConstants.PAGE_TRANSFER_DIRECT_DEBIT_SCHEDULE);
	}

	/**
	 * Transfer schedules from receipting system into direct debit system.
	 */
	public void handleEvent_transferDirectDebitSchedule_transferSchedule(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		String payableItemIDString = request.getParameter("payableItemID");
		Integer payableItemID = new Integer(payableItemIDString);
		UserSession userSession = this.getUserSession(request);

		PaymentTransactionMgr pTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
		try {
			pTxMgr.transferDirectDebitSchedule(payableItemID, userSession.getUser());
		} catch (RollBackException ex) {
			pTxMgr.rollback();
			throw new SystemException("Unable to transfer direct debit schedule.", ex);
		}
		pTxMgr.commit();

		request.setAttribute("heading", "Transfer Direct Debit Schedule Complete");
		request.setAttribute("message", "The receipting system payable resulting from direct debit failure associated with payable item id '" + payableItemIDString + "' " + "has been scheduled in the direct debit system.");

		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

	}

	/**
	 * View calculated amounts for different financial calculation types.
	 */
	public void handleEvent_viewCalculateAmounts(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		this.forwardRequest(request, response, PaymentUIConstants.PAGE_CalculateAmounts);
	}

	public void handleEvent_editPayableItem_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		// test
		String payableItemIDString = request.getParameter("payableItemID");
		String paymentMethodIDString = request.getParameter("paymentMethodID");

		LogUtil.debug(this.getClass(), "" + payableItemIDString);
		LogUtil.debug(this.getClass(), "" + paymentMethodIDString);

		PaymentMgr paymentMgr = null;
		PaymentTransactionMgr paymentTxMgr = null;
		PayableItemVO payableItem = null;
		try {
			paymentMgr = PaymentEJBHelper.getPaymentMgr();
			paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			payableItem = paymentMgr.getPayableItem(new Integer(payableItemIDString));
			payableItem.setPaymentMethodID(paymentMethodIDString);
			paymentTxMgr.updatePayableItem(payableItem);
		} catch (Exception ex) {
			throw new ServletException("Unable to update payable item.", ex);
		}

		request.setAttribute("payableItem", payableItem);

		forwardRequest(request, response, PaymentUIConstants.PAGE_ViewPayableItemDetail);
	}

	/**
	 * View receipting history
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 */
	public void handleEvent_viewReceiptingHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String clientNumberString = request.getParameter("clientNumber");
		request.setAttribute("clientNumber", new Integer(clientNumberString));
		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			Collection receiptingHistoryList = pMgr.getReceiptingHistory(new Integer(clientNumberString), null);

			request.setAttribute("receiptingHistoryList", receiptingHistoryList);
		} catch (Exception e) {
			// ignore errors
			LogUtil.debug(this.getClass(), new Exception("Unable to find receipting history for client '" + clientNumberString + "'. ", e));
			// throw new
			// ServletException("Unable to find receipting history for client '"
			// + clientNumberString + "'.",e);
		}

		forwardRequest(request, response, PaymentUIConstants.PAGE_VIEW_RECEIPTING_HISTORY);
	}

	public void handleEvent_viewPayableItem_editPayableItem(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payableItemIDString = request.getParameter("payableItemID");

		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			PayableItemVO pItem = pMgr.getPayableItem(new Integer(payableItemIDString));
			request.setAttribute("payableItem", pItem);
		} catch (Exception e) {
			throw new ServletException("Unable to find payable item " + payableItemIDString + "." + e);
		}

		forwardRequest(request, response, PaymentUIConstants.PAGE_EditPayableItem);
	}

	public void handleEvent_reverseComponent(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		String payableItemComponentIDString = request.getParameter("componentID");
		Integer payableItemComponentID = new Integer(payableItemComponentIDString);
		try {

			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			PaymentTransactionMgr pTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			PayableItemComponentVO pic = pMgr.getPayableItemComponent(new Integer(payableItemComponentIDString));
			PayableItemPostingVO pipVO = null;
			//
			Collection reversePostings = pMgr.reverseAllPostings(pic, "Reverse amounts for correction. Ref: ");
			if (reversePostings != null && reversePostings.size() > 0) {
				Iterator it = reversePostings.iterator();
				while (it.hasNext()) {
					pipVO = (PayableItemPostingVO) it.next();
					pipVO.setContainerID(payableItemComponentID);
					pTxMgr.createPayableItemPosting(pipVO);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ServletException(ex);
		}
		handleEvent_viewPayableItem_viewComponents(request, response);
	}

	public void handleEvent_removePosting(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payableItemPostingIDString = request.getParameter("postingID");
		Integer payableItemPostingID = new Integer(payableItemPostingIDString);
		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			PayableItemPostingVO pip = pMgr.getPayableItemPosting(payableItemPostingID);
			// delete it
			paymentTxMgr.deletePayableItemPosting(pip);
		} catch (Exception re) {
			throw new ServletException(re);
		}

		handleEvent_viewPayableItemComponent_viewPosting(request, response);
	}

	public void handleEvent_getInvoice(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String invoiceNumber = request.getParameter("invoiceNumber");
		String searchType = request.getParameter("searchType");
		try {
			FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
			DebtorInvoice invoice = financeMgr.getInvoice(invoiceNumber);
			request.setAttribute("invoice", invoice);
			request.setAttribute("searchType", searchType);
			request.setAttribute("searchTerm", invoiceNumber);
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		forwardRequest(request, response, PaymentUIConstants.PAGE_SEARCH_INVOICES);
	}

	public void handleEvent_getInvoicesByClientNumber(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String customerNumber = request.getParameter("customerNumber");
		String searchType = request.getParameter("searchType");

		try {
			FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
			Collection invoiceList = financeMgr.getInvoicesByClientNumber(customerNumber);
			request.setAttribute("invoiceList", invoiceList);
			request.setAttribute("searchType", searchType);
			request.setAttribute("searchTerm", customerNumber);
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		forwardRequest(request, response, PaymentUIConstants.PAGE_SEARCH_INVOICES);
	}

	public void handleEvent_copyPosting(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String payableItemPostingIDString = request.getParameter("postingID");
		Integer payableItemPostingID = new Integer(payableItemPostingIDString);
		try {
			PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
			PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			PayableItemPostingVO pip = pMgr.getPayableItemPosting(payableItemPostingID);
			// create a copy
			paymentTxMgr.createPayableItemPosting(pip);
		} catch (Exception re) {
			throw new ServletException(re);
		}

		handleEvent_viewPayableItemComponent_viewPosting(request, response);
	}

	public void handleEvent_translateAccountCodes(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String entity = request.getParameter("entity");
		String accountCode = request.getParameter("accountCode");
		String subAccountCode = request.getParameter("subAccountCode");
		String costCentre = request.getParameter("costCentre");

		String financeOneAccount = null;

		try {
			FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
			financeOneAccount = financeMgr.translateAccounts(entity, accountCode, subAccountCode, costCentre);
		} catch (Exception re) {
			throw new ServletException(re);
		}

		request.setAttribute("financeOneAccount", financeOneAccount);
		// request.setAttribute("financeOneCostCentre", financeOneCostCentre);

		forwardRequest(request, response, PaymentUIConstants.PAGE_TRANSLATE_FINANCE_CODES);
	}

	/**
	 * View calculated amounts for different financial calculation types.
	 */
	// public void handleEvent_viewPostingSummary(HttpServletRequest request,
	// HttpServletResponse response)
	// throws ServletException
	// {
	// this.forwardRequest(request, response,
	// PaymentUIConstants.PAGE_ViewPostingsSummary);
	// }

	/**
	 * View calculated amounts for different financial calculation types.
	 */
	public void handleEvent_calculateAmounts(HttpServletRequest request, HttpServletResponse response) throws SystemException, ServletException, ValidationException {
		String calculationType = request.getParameter("calcType");

		String startDateString = request.getParameter("startDate");
		String endDateString = request.getParameter("endDate");
		String effectiveDateString = request.getParameter("effectiveDate");
		String selectionModeString = request.getParameter("selectionMode");
		String audit = request.getParameter("audit");
		String capDaysToEarn = request.getParameter("capDaysToEarn");
		String correctDuration = request.getParameter("correctDuration");

		String emailAddress = request.getParameter("emailAddress");

		LogUtil.debug(this.getClass(), "calculationType=" + calculationType);
		LogUtil.debug(this.getClass(), "startDate=" + startDateString);
		LogUtil.debug(this.getClass(), "endDate=" + endDateString);
		LogUtil.debug(this.getClass(), "effectiveDate=" + effectiveDateString);
		LogUtil.debug(this.getClass(), "selectionModeString=" + selectionModeString);
		LogUtil.debug(this.getClass(), "audit=" + audit);
		LogUtil.debug(this.getClass(), "capDaysToEarn=" + capDaysToEarn);
		LogUtil.debug(this.getClass(), "correctDuration=" + correctDuration);
		LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);

		boolean auditFile = audit != null ? audit.equals("on") : false;
		boolean capDTE = capDaysToEarn != null ? capDaysToEarn.equals("on") : false;
		boolean correctDur = correctDuration != null ? correctDuration.equals("on") : false;

		LogUtil.debug(this.getClass(), "auditFile=" + auditFile);
		LogUtil.debug(this.getClass(), "capDTE=" + capDTE);

		String selectionMode = (selectionModeString != null && selectionModeString.equals("on")) ? FinanceMgr.SELECTION_MODE_START_DATE : FinanceMgr.SELECTION_MODE_POSTAT_DATE;
		DateTime startDate = null;
		DateTime endDate = null;
		DateTime effectiveDate = null;

		if (auditFile && (emailAddress == null || emailAddress.trim().length() == 0)) {
			throw new ValidationException("An email address must be entered if generating an audit file.");
		}

		Hashtable calculatedAmounts = null;

		try {
			if (FinanceMgr.CALCULATION_TYPE_EARNED_FOR_DATE_RANGE.equals(calculationType)) {
				startDate = new DateTime(startDateString);
				endDate = new DateTime(endDateString);

				FinanceMgr fMgr = PaymentEJBHelper.getFinanceMgr();
				calculatedAmounts = fMgr.calculateEarnedAmounts(startDate, endDate, selectionMode, auditFile, emailAddress, capDTE, correctDur);
			} else if (FinanceMgr.CALCULATION_TYPE_EARNED.equals(calculationType) || FinanceMgr.CALCULATION_TYPE_UNEARNED.equals(calculationType)) {
				effectiveDate = new DateTime(effectiveDateString);

				FinanceMgr fMgr = PaymentEJBHelper.getFinanceMgr();
				calculatedAmounts = fMgr.calculateAmounts(effectiveDate, calculationType, auditFile, emailAddress, capDTE, correctDur);
			} else {
				throw new SystemException("Unknown calculation type: " + calculationType + ".");
			}
		} catch (RemoteException e) {
			throw new SystemException(e);
		} catch (ParseException pe) {
			throw new SystemException(pe);
		} catch (PaymentException paye) {
			throw new SystemException(paye);
		}

		request.setAttribute("calculatedAmounts", calculatedAmounts);

		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("effectiveDate", effectiveDate);
		request.setAttribute("calculationType", calculationType);

		request.setAttribute("selectionMode", selectionMode);

		request.setAttribute("audit", auditFile);
		request.setAttribute("capDaysToEarn", capDTE);
		request.setAttribute("correctDuration", correctDur);
		request.setAttribute("emailAddress", emailAddress);

		this.forwardRequest(request, response, PaymentUIConstants.PAGE_CalculateAmounts);
	}

	/**
	 * Cancel a DD schedule.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws RemoteException
	 * @throws PaymentException
	 */
	public void handleEvent_cancelDirectDebitSchedule(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, PaymentException {
		UserSession userSession = this.getUserSession(request);
		userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CANCEL_DD);
		if (userSession == null || (userSession != null && !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CANCEL_DD))) {
			throw new ServletException("Your account does not have the required privileges to perform this action");
		}

		// pass membership on
		String membershipIDStr = request.getParameter("membershipID");
		Integer membershipID = Integer.parseInt(membershipIDStr);
		MembershipMgrLocal membershipMgr = MembershipEJBHelper.getMembershipMgrLocal();
		request.setAttribute("membershipVO", membershipMgr.getMembership(membershipID));

		// cancel DD schedule
		String ddScheduleIDStr = request.getParameter("ddScheduleID");
		Integer ddScheduleID = Integer.parseInt(ddScheduleIDStr);

		PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();
		DirectDebitSchedule sched = paymentMgrLocal.getDirectDebitSchedule(ddScheduleID);
		PaymentTransactionMgrLocal paymentTxnMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();

		if (sched.isRenewalVersion()) { // remove
			try {
				paymentTxnMgrLocal.removeDirectDebitSchedule(ddScheduleID);
				paymentTxnMgrLocal.commit();
			} catch (Exception re) {
				paymentTxnMgrLocal.rollback();
				throw new ServletException(re);
			}
		} else { // cancel
			try {
				paymentTxnMgrLocal.cancelDirectDebitSchedule(ddScheduleID, "COM");
				paymentTxnMgrLocal.commit();
			} catch (Exception re) {
				paymentTxnMgrLocal.rollback();
				throw new ServletException(re);
			}
		}

		this.forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPaymentMethod);
	}

}
