package com.ract.payment.ui;

import com.ract.common.CommonConstants;

/**
 * Payment Constants
 * 
 * @author John Holliday
 * @version 1.0
 */

public class PaymentUIConstants {
    // These constants define the names of the JSP pages.
    // CommonConstants.PAYMENT_DIR +
    public static final String PAGE_PaymentUIC = "/PaymentUIC";

    public static final String PAGE_ViewPayableItemList = CommonConstants.DIR_PAYMENT + "/ViewPayableItemList.jsp";

    public static final String PAGE_ViewPayableItemDetail = CommonConstants.DIR_PAYMENT + "/ViewPayableItemDetail.jsp";

    public static final String PAGE_ViewPayableItemComponent = CommonConstants.DIR_PAYMENT + "/ViewPayableItemComponent.jsp";

    public static final String PAGE_ViewPayableItemPosting = CommonConstants.DIR_PAYMENT + "/ViewPayableItemPosting.jsp";

    public static final String PAGE_ViewPendingFees = CommonConstants.DIR_PAYMENT + "/ViewPendingFeeList.jsp";

    public static final String PAGE_ViewPayableItemPostingSummary = CommonConstants.DIR_PAYMENT + "/ViewPayableItemPostingSummary.jsp";

    public static final String PAGE_ViewPostingSummary = CommonConstants.DIR_PAYMENT + "/ViewPostingSummary.jsp";

    public static final String PAGE_ViewDirectDebitSchedule = CommonConstants.DIR_PAYMENT + "/ViewDirectDebitSchedule.jsp";

    public static final String PAGE_CalculateAmounts = CommonConstants.DIR_PAYMENT + "/CalculateAmounts.jsp";

    public static final String PAGE_TRANSFER_DIRECT_DEBIT_SCHEDULE = CommonConstants.DIR_PAYMENT + "/TransferDirectDebitSchedule.jsp";

    public static final String PAGE_View_Cheque = CommonConstants.DIR_PAYMENT + "/ViewCheque.jsp";

    public static final String PAGE_EditPayableItem = CommonConstants.DIR_PAYMENT + "/EditPayableItem.jsp";

    public static final String PAGE_SEARCH_INVOICES = CommonConstants.DIR_PAYMENT + "/searchInvoices.jsp";

    public static final String PAGE_VIEW_GL_TRANSACTION_LIST = CommonConstants.DIR_PAYMENT + "/ViewGLTransactionList.jsp";

    public static final String PAGE_ENTER_GL_TRANSACTION_PARAMETERS = CommonConstants.DIR_PAYMENT + "/EnterGLTransactionParameters.jsp";

    public static final String PAGE_VIEW_RECEIPTING_HISTORY = CommonConstants.DIR_PAYMENT + "/ViewReceiptingHistory.jsp";

    public static final String PAGE_TRANSLATE_FINANCE_CODES = "/TranslateFinanceCodes.jsp";

    /* IVR */
    public static final String PAGE_ViewIVR = CommonConstants.DIR_PAYMENT + "/ViewIVR.jsp";
    public static final String PAGE_ViewFullIVRLine = CommonConstants.DIR_PAYMENT + "/ViewFullIVRLine.jsp";

    /* BPAY */
    public static final String PAGE_ViewBPAY = CommonConstants.DIR_PAYMENT + "/ViewBPAY.jsp";
    public static final String PAGE_ViewFullBPAYLine = CommonConstants.DIR_PAYMENT + "/ViewFullBPAYLine.jsp";
    public static final String Page_BPAYUpdateJob = CommonConstants.DIR_PAYMENT + "/BPAYUpdateJob.jsp";

}
