package com.ract.payment.integration;

//import com.ract.common.integration.IntegrationDocument;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Properties;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.SystemException;
import com.ract.common.integration.IntegrationDocument;
import com.ract.payment.receipting.ReceiptingAdapter;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * <p>
 * A data carrier holding payment type data
 * </p>
 * 
 * @author John Holliday
 * @version 1.0
 */

public class PaymentDocument extends IntegrationDocument implements Serializable {
    /**
     * breakdown of all of the payment types that constitute the receipt.
     */
    private Properties[] paymentBreakdown;

    private String clientNumber;

    private String referenceDoc;

    private String sequenceNumber;

    private String description;

    private String product;

    private DateTime expiryDate;

    private String sourceSystem;

    private String branch;

    // private String effectiveDate;

    private String userName;

    private String total;

    private String receiptNumber;

    private String till = null;

    private DateTime createDate;

    private String[] products;

    // private String comments;

    private double[] costs;

    private String[] paymentType;

    private Properties[] propData;

    private double[] tendered;

    // CONSTANTS

    private static final String REFERENCE = "reference";

    private static final String SEQUENCE_NUMBER = "sequenceNo";

    private static final String CLIENT_NUMBER = "clientNo";

    private static final String DESCRIPTION = "description";

    private static final String EXPIRY_DATE = "expiryDate";

    // private static final String EFFECTIVE_DATE = "effectiveDate";

    private static final String SOURCE_SYSTEM = "sourceSystem";

    private static final String AMOUNT_PAYABLE = "payable";

    private static final String BRANCH = "branch";

    private static final String PRODUCT = "product";

    private static final String RECEIPT_NUMBER = "receiptNumber";

    private static final String CLIENT = "clientNo";

    private static final String PRODUCTS = "products";

    private static final String USERNAME = "userName";

    private static final String TOTAL = "total";

    private static final String TILL = "till";

    private static final String CREATE_DATE = "createDate";

    private static final String COSTS = "costs";

    /**
     * the nodelist label
     */
    private static final String PAY = "pay";

    private static final String TYPE = "type";

    private static final String AMOUNT_PAID = "amount";

    private static final String BSB_NO = "bsbno";

    private static final String BANK_ACCOUNT = "bankaccount";

    private static final String DRAWER = "drawer";

    private static final String CHEQUE_NUMBER = "chequeno";

    private static final String CARD_NUMBER = "cardno";

    private static final String CARD_EXPIRY = "cardexpiry";

    private static final String CARD_HOLDER = "cardholder";

    private Double amountPaid;

    private Double amountPayable;

    private PaymentDocument() {
    }

    /**
     * Construct the payment document object.
     * 
     * @param payDataType
     * @param dataElement
     * @throws SystemException
     */
    public PaymentDocument(String payDataType, Element dataElement) throws SystemException {

	if (dataElement == null) {
	    throw new SystemException("The element passed to " + this.getClass() + " is null.");
	}

	this.integrationDataNode = dataElement;

	// the type of payment data it represents
	this.paymentDataType = payDataType;
	this.referenceDoc = getNodeValue(REFERENCE);
	this.product = getNodeValue(PRODUCT);
	this.clientNumber = getNodeValue(CLIENT_NUMBER);
	this.sequenceNumber = getNodeValue(SEQUENCE_NUMBER);
	this.description = getNodeValue(DESCRIPTION);
	this.sourceSystem = getNodeValue(SOURCE_SYSTEM);
	String amountPayableStr = getNodeValue(AMOUNT_PAYABLE);
	if (amountPayableStr != null) {
	    amountPayable = new Double(amountPayableStr);
	}

	this.expiryDate = null;

	String tmpExpiryDate = getNodeValue(EXPIRY_DATE);
	try {
	    if (tmpExpiryDate != null) {
		expiryDate = new DateTime(tmpExpiryDate);
	    }
	} catch (ParseException ex) {
	    throw new SystemException("Unable to parse expiry date " + tmpExpiryDate + ".", ex);
	}

	this.createDate = null;
	String tmpCreateDate = getNodeValue(CREATE_DATE);
	try {
	    if (tmpCreateDate != null) {
		createDate = new DateTime(tmpCreateDate);
	    }
	} catch (ParseException ex) {
	    throw new SystemException("Unable to parse create date " + tmpCreateDate + ".", ex);
	}

	this.branch = getNodeValue(BRANCH);
	this.userName = getNodeValue(USERNAME);

	try {
	    propData = new Properties[1];
	    propData[0] = new Properties();
	    if (expiryDate != null) {
		propData[0].put(ReceiptingAdapter.EXPIRY_DATE, expiryDate);
	    }
	    if (expiryDate != null) {
		propData[0].put(ReceiptingAdapter.REFERENCE_DOC, referenceDoc);
	    }
	} catch (Exception e) {
	    throw new SystemException("", e);
	}

	this.total = getNodeValue(TOTAL);
	this.receiptNumber = getNodeValue(RECEIPT_NUMBER);
	this.till = getNodeValue(TILL);

	// check for any pay tags
	this.processPayTags();

    }

    public String paymentDataType;

    public String getPaymentDataType() {
	return paymentDataType;
    }

    public String getBranch() {
	return this.branch;
    }

    public String getClientNumber() {
	return this.clientNumber;
    }

    public String getDescription() {
	return this.description;
    }

    public String getProduct() {
	return product;
    }

    public String getReferenceDoc() {
	return this.referenceDoc;
    }

    public String getSequenceNumber() {
	return this.sequenceNumber;
    }

    public String getSourceSystem() {
	return this.sourceSystem;
    }

    public String getUserName() {
	return this.userName;
    }

    public double[] getCosts() {
	return costs;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public DateTime getExpiryDate() {
	return expiryDate;
    }

    public String[] getProducts() {
	return products;
    }

    public Properties[] getPropData() {
	return propData;
    }

    public Double getAmountPayable() {
	return amountPayable;
    }

    public Double getAmountPaid() {
	return amountPaid;
    }

    /**
     * Get the pay node list and populate the arrays.
     * 
     * @throws SystemException
     */
    private void processPayTags() throws SystemException {
	final double NEGATIVE = -1;

	NodeList payList = integrationDataNode.getElementsByTagName(PAY);

	if (payList == null) {
	    // is this actually an exception?
	    return;
	}

	int payLength = payList.getLength();

	// initialise the arrays
	this.tendered = new double[payLength];
	this.paymentType = new String[payLength];
	this.paymentBreakdown = new Properties[payLength];

	for (int g = 0; g < payLength; g++) {
	    if (payList.item(g).hasChildNodes()) {
		// get the node
		Node pay = payList.item(g);
		NodeList nl = pay.getChildNodes();

		Element ty = ((Element) nl.item(0));
		Element py = ((Element) nl.item(1));

		String type = getElementValue(ty).trim();
		String amt = getElementValue(py).trim();
		LogUtil.debug(this.getClass(), "Got " + type + " of " + amt + "for pay tag " + g);
		DecimalFormat nf = (DecimalFormat) NumberFormat.getNumberInstance();
		nf.applyPattern("##########.00-");
		Number n = null;

		try {
		    n = nf.parse(amt);
		    n = (Number) new Double((n.doubleValue() * NEGATIVE));
		} catch (ParseException ex) {
		    nf.applyPattern("##########.00");
		    try {
			n = nf.parse(amt);
		    } catch (ParseException ex1) {
		    }
		}

		tendered[g] = n.doubleValue();

		if (type.equalsIgnoreCase(ReceiptingAdapter.CASH)) {
		    LogUtil.debug(this.getClass(), "Got cash type" + type);
		    paymentType[g] = ReceiptingAdapter.CASH;
		    paymentBreakdown[g] = new Properties();
		    paymentBreakdown[g] = null;
		} else if (type.equalsIgnoreCase(ReceiptingAdapter.CHEQUE)) {
		    LogUtil.debug(this.getClass(), "Got cheque type" + type);
		    Element bsb = ((Element) nl.item(2));
		    Element accnt = ((Element) nl.item(3));
		    Element drawer = ((Element) nl.item(4));
		    Element no = ((Element) nl.item(5));
		    Element bankname = ((Element) nl.item(6));

		    if (bsb.hasChildNodes() && accnt.hasChildNodes() && drawer.hasChildNodes() && no.hasChildNodes()) {
			String num = "" + getElementValue(bsb) + "-" + getElementValue(accnt) + "-" + getElementValue(no);
			paymentType[g] = ReceiptingAdapter.CHEQUE;
			paymentBreakdown[g] = new Properties();
			paymentBreakdown[g].put(ReceiptingAdapter.CHEQUE_NO, num);
			paymentBreakdown[g].put(ReceiptingAdapter.CHEQUE_DRAWER, getElementValue(drawer).trim());
			paymentBreakdown[g].put(ReceiptingAdapter.CHEQUE_BANK, getElementValue(bankname).trim());

			LogUtil.debug(this.getClass(), "Cheque parsed: " + num);
		    } else {
			throw new SystemException("A <cheque> tag contains no child elements");
		    }
		} else if (type.equalsIgnoreCase(ReceiptingAdapter.EFTPOS) || type.equalsIgnoreCase(ReceiptingAdapter.AMEX) || type.equalsIgnoreCase(ReceiptingAdapter.DINERS) || type.equalsIgnoreCase(ReceiptingAdapter.MASTERCARD) || type.equalsIgnoreCase(ReceiptingAdapter.BANKCARD) || type.equalsIgnoreCase(ReceiptingAdapter.VISA)) {
		    LogUtil.debug(this.getClass(), "Got card type" + type);
		    if (type.equalsIgnoreCase(ReceiptingAdapter.EFTPOS)) {
			paymentType[g] = ReceiptingAdapter.EFTPOS;
		    }
		    if (type.equalsIgnoreCase(ReceiptingAdapter.AMEX)) {
			paymentType[g] = ReceiptingAdapter.AMEX;
		    }
		    if (type.equalsIgnoreCase(ReceiptingAdapter.DINERS)) {
			paymentType[g] = ReceiptingAdapter.DINERS;
		    }
		    if (type.equalsIgnoreCase(ReceiptingAdapter.MASTERCARD)) {
			paymentType[g] = ReceiptingAdapter.MASTERCARD;
		    }
		    if (type.equalsIgnoreCase(ReceiptingAdapter.VISA)) {
			paymentType[g] = ReceiptingAdapter.VISA;
		    }
		    if (type.equalsIgnoreCase(ReceiptingAdapter.BANKCARD)) {
			paymentType[g] = ReceiptingAdapter.BANKCARD;

		    }

		    Element no = ((Element) nl.item(2));
		    Element hol = ((Element) nl.item(3));
		    Element exp = ((Element) nl.item(4));

		    if (no.hasChildNodes() && hol.hasChildNodes() && exp.hasChildNodes()) {

			paymentBreakdown[g] = new Properties();
			paymentBreakdown[g].put(ReceiptingAdapter.CARD_NO, getElementValue(no).trim());
			paymentBreakdown[g].put(ReceiptingAdapter.CARD_HOLDER, getElementValue(hol).trim());
			paymentBreakdown[g].put(ReceiptingAdapter.CARD_EXPIRY, getElementValue(exp).trim());
			paymentBreakdown[g].put(ReceiptingAdapter.CARD_TYPE, paymentType[g]);
			LogUtil.debug(this.getClass(), "Processed card details" + paymentType[g] + paymentBreakdown[g]);

		    } else {
			throw new SystemException("A <card> tag contains no child elements");
		    }
		} else if (type.equalsIgnoreCase(ReceiptingAdapter.ROUNDING)) {
		    paymentType[g] = null;
		    paymentBreakdown[g] = new Properties();
		    paymentBreakdown[g] = null;
		    tendered[g] = 0.00;
		    LogUtil.debug(this.getClass(), "Omitting rounding details");

		} else {
		    throw new SystemException("The <pay> element contains an unknown pay type: " + type);
		}
	    } else {
		throw new SystemException("The payList contains no child tags.");
	    }
	}
    }

    public String getReceiptNumber() {
	return receiptNumber;
    }

    public String[] getPaymentType() {
	return paymentType;
    }

    public Properties[] getPaymentBreakdown() {
	return paymentBreakdown;
    }

    public double[] getTendered() {
	return tendered;
    }

    public String getTotal() {
	return total;
    }

    public String getTill() {
	return till;
    }

}
