package com.ract.payment;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.ObjectNotFoundException;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

import com.ract.common.Account;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.Company;
import com.ract.common.DataSourceFactory;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.membership.AdjustmentTypeVO;
import com.ract.membership.ComplimentaryCredit;
import com.ract.membership.FeeTypeVO;
import com.ract.membership.MembershipAccountVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipHistory;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipTransactionAdjustment;
import com.ract.membership.MembershipTransactionDiscount;
import com.ract.membership.MembershipTransactionFee;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.TransactionCredit;
import com.ract.membership.TransactionGroup;
import com.ract.payment.billpay.BillpayHelper;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.directdebit.DirectDebitAdapter;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.receipting.ECRReceipt;
import com.ract.payment.receipting.Payable;
import com.ract.payment.receipting.Receipt;
import com.ract.payment.receipting.ReceiptingAdapter;
import com.ract.payment.receipting.ReceiptingIdentifier;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.util.ConnectionUtil;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * Manage the transfer of payments to and from enterprise systems. (Implemented as a stateless session bean.)
 * 
 * @author LewisG
 * @created 1 August 2002
 */
@Stateless
@Remote({ PaymentMgr.class })
@Local({ PaymentMgrLocal.class })
public class PaymentMgrBean {

	@PersistenceContext(unitName = "master")
	EntityManager em;

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource dataSource;

	@PersistenceContext(unitName = "master")
	Session hsession;

	/**
	 * String representing credit
	 */
	private String CREDIT = "Credit";

	/**
	 * String representing debit
	 */
	private String DEBIT = "Debit";

	/**
	 * Constructor
	 */
	public PaymentMgrBean() {
	}

	// =================== Implement remote interface =========================

	/**
	 * Find the payable items for a specified client
	 * 
	 */
	// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection getPayableItemsByClient(Integer clientNumber) throws RemoteException, PaymentException {

		List payableItemList = hsession.createCriteria(PayableItemVO.class).add(Expression.eq("clientNo", clientNumber)).list();
		return payableItemList;
	}

	public PayableItemVO getPayableItemByClientAndSource(Integer clientNumber, Integer sourceSystemReference) throws RemoteException, PaymentException {

		PayableItemVO result = (PayableItemVO) hsession.createCriteria(PayableItemVO.class).add(Expression.eq("clientNo", clientNumber)).add(Expression.eq("sourceSystemReferenceID", sourceSystemReference)).uniqueResult();

		return result;
	}

	public BigDecimal getTotalPostings(Integer accountNumber) throws RemoteException {
		return getTotalPostings(accountNumber, null, null);
	}

	/**
	 * Retrieve the sum of postings for an account over an optional date range.
	 * 
	 * @param accountNumber
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws RemoteException
	 */
	public BigDecimal getTotalPostings(Integer accountNumber, DateTime startDate, DateTime endDate) throws RemoteException {
		PreparedStatement statement = null;
		BigDecimal total = BigDecimal.ZERO;

		String statementText = "SELECT SUM(pub.pt_payableItemPosting.amount) FROM [PUB].[pt_payableItemPosting]\n" + "INNER JOIN pub.pt_payableItemComponent ON PUB.pt_payableItemPosting.payableItemComponentID = PUB.pt_payableItemComponent.payableItemComponentID\n" + "INNER JOIN pub.pt_payableItem ON PUB.pt_payableItemComponent.payableItemID = PUB.pt_payableItem.payableItemID\n" + "INNER JOIN pub.mem_membership ON pub.mem_membership.client_number = PUB.pt_payableItem.clientNo\n" + "WHERE accountNumber = ? and posted is not null\n";

		if (startDate != null) {
			statementText += " and postAt >= ?";
		}
		if (endDate != null) {
			statementText += " and postAt < ?";
		}

		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			statement.setInt(1, accountNumber);
			int nextIndex = 2;

			if (startDate != null) {
				statement.setDate(nextIndex, startDate.toSQLDate());
				nextIndex++;
			}
			if (endDate != null) {
				statement.setDate(nextIndex, endDate.toSQLDate());
			}

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				total = resultSet.getBigDecimal(1);
			}

			// sanity check
			if (total == null) {
				total = BigDecimal.ZERO;
			}
		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving total postings for account.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return total;
	}

	public BigDecimal getTotalVouchers() throws RemoteException {
		return getTotalVouchers(null, null);
	}

	/**
	 * Retrieve the sum of vouchers for an optional date range.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws RemoteException
	 */
	public BigDecimal getTotalVouchers(DateTime startDate, DateTime endDate) throws RemoteException {
		PreparedStatement statement = null;
		BigDecimal total = BigDecimal.ZERO;

		String statementText = "SELECT sum([disposalAmount]) FROM [dbo].[FinanceVoucher] WHERE status = 'A' and processDate is not null";
		if (startDate != null) {
			statementText += " and processDate >= ?";
		}
		if (endDate != null) {
			statementText += " and processDate < ?";
		}

		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			int nextIndex = 1;
			if (startDate != null) {
				statement.setDate(nextIndex, startDate.toSQLDate());
				nextIndex++;
			}
			if (endDate != null) {
				statement.setDate(nextIndex, endDate.toSQLDate());
			}

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				total = resultSet.getBigDecimal(1);
			}

			// sanity check
			if (total == null) {
				total = BigDecimal.ZERO;
			}
		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving total of vouchers.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return total;
	}

	private final String SQL_SELECT_POSTINGS_FOR_ACCOUNT = "SELECT p.amount, clientNo\n" + "FROM (\n" + "		SELECT SUM(PUB.pt_payableItemPosting.amount) AS amount, clientNo\n" + "		FROM [PUB].[pt_payableItemPosting]\n" + "		INNER JOIN pub.pt_payableItemComponent\n" + "		ON PUB.pt_payableItemPosting.payableItemComponentID = PUB.pt_payableItemComponent.payableItemComponentID\n" + "		INNER JOIN pub.pt_payableItem\n" + "		ON pub.pt_payableItem.payableItemID = pub.pt_payableItemComponent.payableItemID\n" + "		WHERE accountNumber = ?\n" + "		AND PUB.pt_payableItemPosting.postAt >= ? AND PUB.pt_payableItemPosting.postAt < ?\n" + "		GROUP BY clientNo\n" + ") AS p\n" + "WHERE p.amount <> 0";

	/**
	 * Retrieve a mapping of client numbers for posting totals for date range.
	 * 
	 * @param accountNumber
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws RemoteException
	 */
	public Map<Integer, BigDecimal> getPostingTotalsForAccount(Integer accountNumber, DateTime startDate, DateTime endDate) throws RemoteException {

		Map<Integer, BigDecimal> results = new HashMap<Integer, BigDecimal>();
		PreparedStatement statement = null;

		String statementText = SQL_SELECT_POSTINGS_FOR_ACCOUNT;

		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			statement.setInt(1, accountNumber);
			statement.setDate(2, startDate.toSQLDate());
			statement.setDate(3, endDate.toSQLDate());

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				BigDecimal amount = resultSet.getBigDecimal(1);
				Integer clientNumber = resultSet.getInt(2);

				results.put(clientNumber, amount);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving Postings Totals for account.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}

	private final String SQL_SELECT_POSTINGS_FOR_CLIENT = "SELECT [PUB].[pt_payableItemPosting].payableItemPostingID\n" + "FROM [PUB].[pt_payableItemPosting]\n" + "INNER JOIN pub.pt_payableItemComponent\n" + "ON PUB.pt_payableItemPosting.payableItemComponentID = PUB.pt_payableItemComponent.payableItemComponentID\n" + "INNER JOIN pub.pt_payableItem\n" + "ON pub.pt_payableItem.payableItemID = pub.pt_payableItemComponent.payableItemID\n" + "WHERE accountNumber = ? and posted is not null\n";

	/**
	 * Find the postings against a specified account for a specified client in a date range.
	 * 
	 * @param clientNumber
	 * @param accountNumber
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws RemoteException
	 */
	public List<PayableItemPostingVO> getPayableItemPostingsByClientAndAccount(Integer clientNumber, Integer accountNumber, DateTime startDate, DateTime endDate) throws RemoteException {
		List<PayableItemPostingVO> results = new ArrayList<PayableItemPostingVO>();

		PreparedStatement statement = null;

		String statementText = SQL_SELECT_POSTINGS_FOR_CLIENT;

		if (startDate != null) {
			statementText += "AND PUB.pt_payableItemPosting.postAt >= ?\n";
		}
		if (endDate != null) {
			statementText += "AND PUB.pt_payableItemPosting.postAt < ?\n";
		}

		if (clientNumber != null) {
			statementText += "AND clientNo = ?\n";
		}

		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			int idx = 1;
			statement.setInt(idx, accountNumber);
			idx++;

			if (startDate != null) {
				statement.setDate(idx, startDate.toSQLDate());
				idx++;
			}
			if (endDate != null) {
				statement.setDate(idx, endDate.toSQLDate());
				idx++;
			}
			if (clientNumber != null) {
				statement.setInt(idx, clientNumber);
				idx++;
			}

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer payableItemPostingID = resultSet.getInt(1);
				results.add(this.getPayableItemPosting(payableItemPostingID));
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving Payable Item Postings with for client/account.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}

	/**
	 * Return a mapping of client numbers to voucher amounts for a date range.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws RemoteException
	 */
	public Map<Integer, BigDecimal> getVouchersByRange(DateTime startDate, DateTime endDate) throws RemoteException {
		String statementText = "SELECT clientNumber, disposalAmount FROM [dbo].[FinanceVoucher] WHERE status = 'A'";
		if (startDate != null) {
			statementText += " and processDate >= ?";
		}
		if (endDate != null) {
			statementText += " and processDate < ?";
		}

		Map<Integer, BigDecimal> results = new HashMap<Integer, BigDecimal>();

		PreparedStatement statement = null;

		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			if (startDate != null) {
				statement.setDate(1, startDate.toSQLDate());
			}
			if (endDate != null) {
				statement.setDate(2, endDate.toSQLDate());
			}
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer clientNumber = resultSet.getInt(1);
				BigDecimal amount = resultSet.getBigDecimal(2);
				results.put(clientNumber, amount);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving Vouchers by date range.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}

	private final static String SQL_SELECT_APPROVED_VOUCHERS = "SELECT clientNumber, membership_id, disposalAmount, processDate\n" + "FROM [dbo].[FinanceVoucher] \n" + "INNER JOIN pub.mem_membership\n" + "ON dbo.FinanceVoucher.clientNumber = pub.mem_membership.client_number\n" + "WHERE status = 'A'\n" + "ORDER BY processDate asc";

	/**
	 * Retrieve a list of mapped fields relating to approved vouchers.
	 * 
	 * @return Map containing clientNumber, membershipId, disposalAmount, processDate.
	 * @throws RemoteException
	 */
	public List<Map<String, Object>> getApprovedVouchers() throws RemoteException {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		Map<String, Object> result;

		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = SQL_SELECT_APPROVED_VOUCHERS;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer clientNo = resultSet.getInt(1);
				Integer membershipId = resultSet.getInt(2);
				BigDecimal disposalAmount = resultSet.getBigDecimal(3);
				Date processDate = resultSet.getDate(4);

				result = new HashMap<String, Object>();
				result.put("clientNumber", clientNo);
				result.put("membershipId", membershipId);
				result.put("disposalAmount", disposalAmount);
				result.put("processDate", processDate);

				results.add(result);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving approved vouchers.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}

	/**
	 * Find the payable items for a specified client
	 */
	public Collection getPayableItemsByClientReceipt(Integer clientNumber, String paymentMethodID) throws RemoteException {

		List payableItemList = hsession.createCriteria(PayableItemVO.class).add(Expression.eq("clientNo", clientNumber)).add(Expression.eq("paymentMethodDescription", PaymentMethod.RECEIPTING)).add(Expression.eq("paymentMethodID", paymentMethodID)).list();
		return payableItemList;
	}

	public Collection<PayableItemVO> getPayableHistory(Integer clientNumber) throws RemoteException {
		List payableItemList = hsession.createCriteria(PayableItemVO.class).add(Expression.eq("clientNo", clientNumber)).add(Expression.isNotNull("receiptNo")).list();
		return payableItemList;
	}

	public final static String MODE_CREATE = "create";

	public Receipt getReceiptByReceiptId(String receiptId) throws PaymentException {
		Receipt receipt = null;
		try {
			ReceiptingAdapter receiptingAdapter = null;
			receiptingAdapter = PaymentFactory.getReceiptingAdapter(SourceSystem.RECEPTOR.getAbbreviation());
			receipt = receiptingAdapter.getReceiptByReceiptId(receiptId);
		} catch (Exception e) {
			throw new PaymentException(e.getMessage());
		}
		return receipt;
	}

	public Receipt getReceiptByReceiptId(SourceSystem sourceSystem, String receiptId) throws PaymentException {
		Receipt receipt = null;

		List<PayableItemVO> payableItemList = hsession.createCriteria(PayableItemVO.class).add(Expression.eq("receiptNo", receiptId)).add(Expression.eq("paymentSystem", sourceSystem.getAbbreviation())).list();

		for (PayableItemVO p : payableItemList) {
			if (sourceSystem.equals(SourceSystem.ECR)) {
				receipt = new ECRReceipt();
				((ECRReceipt) receipt).setReceiptNumber(p.getReceiptNo());
				break;
			}
		}

		return receipt;
	}

	public Collection<PaymentTypeVO> getPaymentTypes() {
		// LogUtil.log(this.getClass(), "XXXXXXXXXXXXXXXXX");
		javax.persistence.Query paymentTypeQuery = em.createQuery("select e from PaymentTypeVO e");
		return paymentTypeQuery.getResultList();
	}

	public PaymentTypeVO getPaymentTypeVO(String paymentTypeCode) {
		return em.find(PaymentTypeVO.class, paymentTypeCode);
	}

	/**
	 * Remove and commit a pending fee and associated payable fee.
	 * 
	 * @todo In non-transactional bean as commit and rollback is done directly in the method.
	 * 
	 * @param receiptingAdapter ReceiptingAdapter
	 * @param paymentSystem String
	 * @param paymentMethodId String
	 * @param pendingFee PendingFee
	 * @param hsession Session
	 * @throws HibernateException
	 * @throws RollBackException
	 * @throws RemoteException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void removeExpiredPendingFee(String pendingFeeId) throws RollBackException, RemoteException {
		PaymentTransactionMgr paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();
		try {
			paymentTransactionMgr.removePendingFee(pendingFeeId);
			paymentTransactionMgr.commit();
		} catch (ObjectNotFoundException ex) {
			paymentTransactionMgr.rollback();
			throw new RollBackException(ex.getMessage());
		}
	}

	/**
	 * Create the new set of postings reversing the original postings as no payment was received.
	 */
	public Collection reverseAllPostings(PayableItemComponentVO component, String description) throws RemoteException, PaymentException {
		PayableItemPostingVO oldPosting = null;
		PayableItemPostingVO newPosting = null;
		PostingContainer newPostingList = new PostingContainer();
		String postingDescription = description + " " + component.getSourceSystemReference() + ".";
		DateTime transactionDate = new DateTime().getDateOnly();
		ArrayList oldPostingList = new ArrayList(component.getPostings());
		Iterator oldPostingListIterator = oldPostingList.iterator();
		Account incomeAccount = null;
		Account account = null;
		while (oldPostingListIterator.hasNext()) {
			oldPosting = (PayableItemPostingVO) oldPostingListIterator.next();

			account = new MembershipAccountVO(oldPosting.getAccountNumber(), oldPosting.getCostCentre(), oldPosting.getSubAccountNumber());
			incomeAccount = new MembershipAccountVO(oldPosting.getIncomeAccountNumber(), oldPosting.getIncomeCostCentre(), oldPosting.getIncomeSubAccountNumber());

			newPosting = new PayableItemPostingVO(-oldPosting.getAmount(), account // default
			// branch
			// allocation
			, incomeAccount, transactionDate, transactionDate, Company.RACT, postingDescription, null);
			newPostingList.add(newPosting);

		}
		return newPostingList;
	}

	/**
	 * Get the payable item posting value object given the posting id.
	 * 
	 * @param payableItemPostingID
	 * @return
	 * @throws RemoteException
	 */
	public PayableItemPostingVO getPayableItemPosting(Integer payableItemPostingID) {

		PayableItemPostingVO payableItemPosting = (PayableItemPostingVO) hsession.get(PayableItemPostingVO.class, payableItemPostingID);
		return payableItemPosting;
	}

	/**
	 * Gets the payable item value object given the payable item id.
	 * 
	 * @param payableItemID Description of the Parameter
	 * @return The payableItem value
	 * @exception RemoteException Description of the Exception
	 */
	public PayableItemVO getPayableItem(Integer payableItemID) throws RemoteException, PaymentException {

		PayableItemVO payableItem = (PayableItemVO) hsession.get(PayableItemVO.class, payableItemID);
		return payableItem;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Hashtable getOrphanedDDSchedules(String sourceSystem) throws SystemException {
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		return ddAdapter.getOrphanedDDSchedules(sourceSystem);
	}

	/**
	 * Retrieves a map of failed DD schedules containing: - clientNo - opSeqNo - amt - payableSeq
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List getFailedDDSchedules(Integer clientNo, String sourceSystem) throws SystemException {
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		return ddAdapter.getFailedDDSchedules(clientNo, sourceSystem);
	}

	// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection getPayableItems(String paymentMethod, String paymentMethodId) throws RemoteException, PaymentException {
		LogUtil.debug(this.getClass(), "getPayableItems start");
		LogUtil.debug(this.getClass(), "paymentMethod=" + paymentMethod);
		LogUtil.debug(this.getClass(), "paymentMethodId=" + paymentMethodId);
		Collection<PayableItemVO> payableItems = new ArrayList<PayableItemVO>();

		List<PayableItemVO> payableItemList = hsession.createCriteria(PayableItemVO.class).add(Expression.eq("paymentMethodDescription", paymentMethod)).add(Expression.eq("paymentMethodID", paymentMethodId)).list();
		if (payableItemList != null) {
			if (payableItemList.size() > 0) {
				for (Iterator<PayableItemVO> i = payableItemList.iterator(); i.hasNext();) {
					payableItems.add(i.next());
				}
			}
		}
		LogUtil.debug(this.getClass(), "getPayableItems end");
		return payableItems;
	}

	/**
	 * Retrieves a list of all PayableItems having payment method of Direct Debit starting from start date. This could return a large amount of results and should be used with care.
	 * 
	 * @param startDate
	 * @return
	 * @throws RemoteException
	 * @throws PaymentException
	 */
	@SuppressWarnings("unchecked")
	private Collection<PayableItemVO> getDirectDebitPayableItemsByDate(DateTime startDate) {
		Collection<PayableItemVO> payableItems = hsession.createCriteria(PayableItemVO.class).add(Expression.eq("paymentMethodDescription", PaymentMethod.DIRECT_DEBIT)).add(Expression.isNotNull("paymentMethodID")).add(Expression.ge("startDate", startDate)).list();

		return payableItems;
	}

	/**
	 * Return a list of Payable Items with non-existant DD schedules;
	 * 
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> getUnlinkedDDSchedules(DateTime startDate) {
		Map<String, String> result = new LinkedHashMap<String, String>();
		List<PayableItemVO> payableItems = (List<PayableItemVO>) getDirectDebitPayableItemsByDate(startDate);

		for (PayableItemVO p : payableItems) {
			try {
				this.getDirectDebitSchedule(Integer.parseInt(p.getPaymentMethodID()));
			} catch (Exception e) {
				result.put(p.getPaymentMethodID(), p.getClientNo().toString());
			}
		}

		return result;
	}

	public PayableItemComponentVO getPayableItemComponent(Integer payableItemComponentId) {

		PayableItemComponentVO payableItemComponent = (PayableItemComponentVO) hsession.get(PayableItemComponentVO.class, payableItemComponentId);
		return payableItemComponent;
	}

	/**
	 * Returns the payment method for the given payment method description and ID. If the payment method description matches a receipting payment method then a ReceiptingPaymentMethod is returned and the payment ID is ignored. If the payment method description matches a direct debit payment method then the DirectDebitPaymentMethod is fetched from the direct debit system.
	 * 
	 * @param paymentMethodDescription Description of the Parameter
	 * @param paymentMethodID Description of the Parameter
	 * @return The paymentMethod value
	 * @exception RemoteException Description of the Exception
	 */
	public PaymentMethod getPaymentMethod(String paymentMethodDescription, String paymentMethodID) throws RemoteException {

		LogUtil.debug(this.getClass(), "paymentMethodDescription = " + paymentMethodDescription);
		LogUtil.debug(this.getClass(), "paymentMethodID = " + paymentMethodID);

		PaymentMethod paymentMethod = null;
		if (PaymentMethod.RECEIPTING.equals(paymentMethodDescription)) {
			Integer clientNumber = null;
			SourceSystem sourceSystem = null;
			paymentMethod = new ReceiptingPaymentMethod(paymentMethodID, clientNumber, sourceSystem);
		} else if (PaymentMethod.DIRECT_DEBIT.equals(paymentMethodDescription)) {
			DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
			if (paymentMethodID != null) {
				paymentMethod = ddAdapter.getSchedule(new Integer(paymentMethodID));
			}
		} else {
			throw new SystemException("Failed to get payment method. Payment method description '" + paymentMethodDescription + "' is not known.");
		}
		return paymentMethod;
	}

	public Collection getPoliciesOnAccount(Integer ddAuthorityId) throws RemoteException {
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		return ddAdapter.getPoliciesOnAccount(ddAuthorityId);
	}

	/**
	 * Return the payment method specified by the payable item.
	 * 
	 * @param payableItemID Description of the Parameter
	 * @return The paymentMethodByPayableItem value
	 * @exception RemoteException Description of the Exception
	 */
	// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public PaymentMethod getPaymentMethodByPayableItem(Integer payableItemID) throws RemoteException, PaymentException {
		PayableItemVO pItem = getPayableItem(payableItemID);
		if (pItem != null) {
			return pItem.getPaymentMethod();
		} else {
			throw new SystemException("Unable to find payable item.");
		}
	}

	/**
	 * Gets the directDebitAuthorityListByClient attribute of the PaymentMgrBean object
	 * 
	 * @param clientNumber Description of the Parameter
	 * @return The directDebitAuthorityListByClient value
	 * @exception RemoteException Description of the Exception
	 */
	public Collection getDirectDebitAuthorityListByClient(Integer clientNumber) throws RemoteException, PaymentException {
		Collection authorityList = null;
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		try {
			authorityList = ddAdapter.getAuthorityList(clientNumber);
		} catch (RemoteException ex) {
			throw new PaymentException(ex);
		}
		return authorityList;
	}

	/**
	 * Return a specific payment type.
	 * 
	 * @param paymentTypeCode Description of the Parameter
	 * @return The paymentType value
	 * @exception RemoteException Description of the Exception
	 */
	public PaymentTypeVO getPaymentType(String paymentTypeCode) throws RemoteException {
		return PaymentTypeCache.getInstance().getPaymentType(paymentTypeCode);
	}

	public void resetPaymentTypeCache() {
		PaymentTypeCache.getInstance().reset();
	}

	/**
	 * Return a list of all of the payment types.
	 * 
	 * @return The paymentTypeList value
	 * @exception RemoteException Description of the Exception
	 */
	public Collection getPaymentTypeList() throws RemoteException {
		return PaymentTypeCache.getInstance().getPaymentTypeList();
	}

	/**
	 * Return a list of payment types that can be used for paying memberships.
	 * 
	 * @return The paymentTypeListForMembership value
	 * @exception RemoteException Description of the Exception
	 */
	// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection getPaymentTypeListForMembership() throws RemoteException {
		ArrayList memTypesList = new ArrayList();
		Collection allTypesList = PaymentTypeCache.getInstance().getPaymentTypeList();
		if (allTypesList != null) {
			PaymentTypeVO paymentTypeVO;
			Iterator allTypesListIterator = allTypesList.iterator();
			while (allTypesListIterator.hasNext()) {
				paymentTypeVO = (PaymentTypeVO) allTypesListIterator.next();
				if (paymentTypeVO.isMembershipPaymentType()) {
					memTypesList.add(paymentTypeVO);
				}
			}
		}
		return memTypesList;
	}

	/**
	 * Return the specified direct debit authority
	 * 
	 * @param ddAuthorityID Description of the Parameter
	 * @return The directDebitAuthority value
	 * @exception RemoteException Description of the Exception
	 */
	public DirectDebitAuthority getDirectDebitAuthority(Integer ddAuthorityID) throws RemoteException, PaymentException {
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		DirectDebitAuthority ddAuthority = null;
		try {
			ddAuthority = ddAdapter.getAuthority(ddAuthorityID);
		} catch (RemoteException ex) {
			throw new PaymentException(ex);
		}
		return ddAuthority;
	}

	/**
	 * Return a direct debit schedule given the dd schedule id.
	 * 
	 * @param ddScheduleID Description of the Parameter
	 * @return The directDebitSchedule value
	 * @exception RemoteException Description of the Exception
	 */
	public DirectDebitSchedule getDirectDebitSchedule(Integer ddScheduleID) throws RemoteException, PaymentException {
		// get the direct debit schedule
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		DirectDebitSchedule ddSchedule = null;
		try {
			ddSchedule = ddAdapter.getSchedule(ddScheduleID);
		} catch (RemoteException ex) {
			throw new PaymentException(ex);
		}
		return ddSchedule;
	}

	/**
	 * Get the direct debit schedule by dd authority.
	 * 
	 */
	public Vector getDirectDebitScheduleListByDirectDebitAuthority(DirectDebitAuthority ddAuthority) throws RemoteException, PaymentException {

		Integer ddAuthorityID = ddAuthority.getDirectDebitAuthorityID();

		// get the direct debit schedule
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		Vector ddScheduleList = null;
		try {
			ddScheduleList = ddAdapter.getScheduleListByAuthority(ddAuthorityID);
		} catch (RemoteException ex) {
			throw new PaymentException(ex);
		}
		return ddScheduleList;
	}

	/**
	 * Create postings for discretionary discounts This will normally not be done inside each transaction, but only for the payer (prime addressee) transaction, even if there is no payable amount relating to that member.
	 * 
	 * NOT TRANSACTIONAL
	 * 
	 * <ul>
	 * <li>create payable Item component and return it
	 * <li>create postings for discountAmount - debit discount account<br/>
	 * -1 * discountAmount - credit control account
	 * <li>add postings to payableItemComponent and return it to the calling program
	 * <li>discount amount is stored (temporarily) in the transactionGroup object
	 * </ul>
	 * 
	 */
	public PayableItemComponentVO createPostingsForDiscretionaryDiscount(Integer transactionReference, // PA
			// transaction
			// ID
			TransactionGroup transGroup, String sourceSystemReference, // membershipNumber/clientNumber
			String discType) throws RemoteException, PaymentException {
		LogUtil.log(this.getClass(), "createPostingsForDiscretionaryDiscount " + transactionReference + " " + sourceSystemReference + " " + discType);
		PayableItemComponentVO component = null;

		double postingTotal = 0;
		double gstRate = transGroup.getGstRate();
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		PostingContainer postingList = new PostingContainer();

		// effective date of posting
		DateTime postAtDate = new DateTime();
		Company company = Company.RACT;
		String description = "Membership Disc Discount. Ref: " + transactionReference;
		Integer containerID = null;

		BigDecimal discountAmount = null;
		Account account = null;
		String costCentre = transGroup.getUser().getSalesBranch().getProfitCentre();

		if (PaymentMgr.DISCRETIONARY_DISCOUNT_ADJUSTMENT.equals(discType)) {

			MembershipTransactionAdjustment memTransAdjustment = transGroup.getAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT);

			if (memTransAdjustment == null) {
				throw new SystemException("Attached discretionary discount adjustment should not be null.");
			}

			AdjustmentTypeVO adjTypeVO = memTransAdjustment.getAdjustmentTypeVO();
			if (adjTypeVO == null) {
				throw new SystemException("Failed to get adjustment type '" + AdjustmentTypeVO.DISCRETIONARY_DISCOUNT + "'.");
			}
			MembershipAccountVO discountAccount = adjTypeVO.getAdjustmentAccount();
			if (discountAccount == null) {
				throw new SystemException("Failed to get discretionary discount account.");
			}

			discountAmount = memTransAdjustment.getAdjustmentAmount();
			// JH 20081230 fixed bug that set cost centre on account in the
			// cache - copy object first.
			discountAccount = discountAccount.copy();
			discountAccount.setCostCenter(costCentre);
			account = discountAccount;
		} else if (PaymentMgr.DISCRETIONARY_DISCOUNT_COMP_CREDIT.equals(discType)) {
			MembershipTransactionVO memTxVO = transGroup.getMembershipTransactionForClient(new Integer(sourceSystemReference));
			MembershipVO newMemVO = memTxVO.getNewMembership();
			ComplimentaryCredit compCredit = (ComplimentaryCredit) newMemVO.getTransactionCredit(TransactionCredit.CREDIT_COMPLIMENTARY);
			MembershipAccountVO compAcc = MembershipEJBHelper.getMembershipRefMgr().getMembershipAccountByTitle(MembershipAccountVO.MEMBERSHIP_MISC_ACCOUNT);
			account = compAcc;
			discountAmount = compCredit.getTransactionCreditAmount();
			if (discountAmount != null) {
				LogUtil.log(this.getClass(), "discountAmount = " + discountAmount.doubleValue());
			}
		}

		if (discountAmount == null) {
			throw new SystemException("The discount amount is null.");
		}

		double gstAmount = 0;
		double discountNet = 0;
		gstAmount = CurrencyUtil.calculateGSTincluded(discountAmount.doubleValue(), gstRate);
		gstAmount = NumberUtil.roundDouble(gstAmount, 2);
		// Deduct the GST component
		discountNet = discountAmount.doubleValue() - gstAmount;

		PayableItemPostingVO posting = null;
		try {
			posting = new PayableItemPostingVO(discountNet, account, null, postAtDate, postAtDate, company, "Discretionary discount on membership. Ref: " + transactionReference, containerID);
		} catch (PaymentException e) {
			throw new SystemException("Error creating discretionary discount posting : " + e.getMessage(), e);
		}
		postingList.add(posting);
		postingTotal += discountAmount.doubleValue();
		// discretionaryPosting = posting;
		// add clearing account posting for the discretionary discount

		MembershipAccountVO memClearingAccountVO = null;
		memClearingAccountVO = refMgr.getMembershipClearingAccount();
		Account clearingAccount = memClearingAccountVO;
		// Note: the fees are GST inclusive.

		MembershipAccountVO memGSTAccountVO = refMgr.getMembershipGSTAccount();
		account = memGSTAccountVO;
		try {
			posting = new PayableItemPostingVO(gstAmount, account, null, postAtDate, postAtDate, company, "GST for discount. Ref: " + transactionReference, containerID);
		} catch (PaymentException e) {
			throw new SystemException("Error creating posting to GST Account : " + e.getMessage(), e);
		}
		postingList.add(posting);
		postingTotal += gstAmount;

		double netAmount = discountAmount.negate().doubleValue();

		// then do the clearing posting
		try {
			posting = new PayableItemPostingVO(netAmount, clearingAccount, null, postAtDate, postAtDate, company, "Balancing post for membership. Ref: " + transactionReference, containerID);
		} catch (PaymentException e) {
			throw new SystemException("Error creating posting for Clearing Account : " + e.getMessage(), e);
		}

		postingList.add(posting);
		// done the income and GST postings
		// now create the component
		description = "Discretionary Discount. Ref: " + transactionReference;
		// This is set by the payment system.
		// Create a component for each Membership Transaction.
		Integer componentID = null;
		discountAmount = discountAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
		component = new PayableItemComponentVO(postingList, discountAmount.negate(), description, componentID, sourceSystemReference, null // supersedingComponentID
		);
		return component;
	}

	// /**
	// * Return the direct debit schedule ID if there is an active schedule for
	// that source
	// * system reference and today's date.
	// *
	// * @todo Assumes only one schedule for a source system reference. This is
	// currently true for
	// * membership but may not be for other source systems.
	// */
	// public Integer getActiveScheduleBySourceSystemReference(SourceSystem
	// sourceSystem,
	// Integer sourceSystemReference)
	// throws PaymentException
	// {
	// Integer ddScheduleID = null;
	//
	// //date of the current transaction
	// DateTime now = new DateTime();
	// DateTime nowDateOnly = now.getDateOnly();
	//
	// PayableItemStoreHome piStoreHome = null;
	//
	// try
	// {
	// piStoreHome = PaymentEJBHelper.getPayableItemStoreHome();
	// }
	// catch(RemoteException re)
	// {
	// throw new PaymentException(re);
	// }
	//
	// Collection payableItemList = null;
	// try
	// {
	// payableItemList = piStoreHome.findBySourceSystemReference(sourceSystem,
	// sourceSystemReference);
	// }
	// catch(FinderException fe)
	// {
	// throw new
	// PaymentException("Unable to find payable items by source system reference.",
	// fe);
	// }
	// catch(RemoteException reme)
	// {
	// throw new PaymentException(reme);
	// }
	// if(payableItemList != null && payableItemList.size() > 0)
	// {
	// PayableItemStore piStore = null;
	// PayableItemVO piVO = null;
	// DirectDebitSchedule ddSchedule = null;
	// Iterator piIt = payableItemList.iterator();
	// //should only be one
	// while(piIt.hasNext())
	// {
	// piStore = (PayableItemStore)piIt.next();
	// try
	// {
	// piVO = piStore.getPayableItem();
	// }
	// catch(RemoteException re)
	// {
	// throw new PaymentException(re);
	// }
	//
	// //must be direct debit
	// if(PaymentMethod.DIRECT_DEBIT.equals(piVO.getPaymentMethodDescription()))
	// {
	// //is the payable item current
	// if(nowDateOnly.onOrAfterDay(piVO.getStartDate()) &&
	// nowDateOnly.onOrBeforeDay(piVO.getEndDate()))
	// {
	//
	// //if it is then get the schedule
	// //payment method in the case of dd
	// try
	// {
	// ddSchedule = (DirectDebitSchedule)piVO.getPaymentMethod();
	// }
	// catch(RemoteException dde)
	// {
	// throw new
	// PaymentException("Unable to get direct debit schedule for payable item "
	// + piVO.getPayableItemID() + ".", dde);
	// }
	//
	// //if outstanding then dd is active
	// if(ddSchedule.getTotalAmountOutstanding().doubleValue() > 0)
	// {
	// ddScheduleID = new Integer(ddSchedule.getPaymentMethodID());
	// }
	// }
	// }
	// }
	// }
	// return ddScheduleID;
	// }

	/**
	 * Return the set of payable item components for the source system reference.
	 * 
	 * @todo fix source system
	 */
	public Collection getComponentsBySourceSystemReference(String sourceSystem, String sourceSystemReference) throws RemoteException {

		List componentList = hsession.createCriteria(PayableItemComponentVO.class).add(Expression.eq("sourceSystemReference", sourceSystemReference)).list();
		return componentList;

		// PayableItemComponentStoreHome componentHome =
		// PaymentEJBHelper.getPayableItemComponentStoreHome();
		// PayableItemComponentStore componentStore = null;
		// ArrayList componentList = null, componentVOList = new ArrayList();
		// try
		// {
		// componentList = new
		// ArrayList(componentHome.findBySourceSystemReference(sourceSystem,
		// sourceSystemReference));
		// }
		// catch(FinderException e)
		// {
		// throw new SystemException("Error getting payable item components : "
		// + e.getMessage(), e);
		// }
		//
		// Iterator componentListIterator = componentList.iterator();
		// while(componentListIterator.hasNext())
		// {
		// componentStore =
		// (PayableItemComponentStore)componentListIterator.next();
		// try
		// {
		// componentVOList.add(componentStore.getPayableItemComponent());
		// }
		// catch(PaymentException e)
		// {
		// throw new SystemException("Error getting payable item component : " +
		// e.getMessage(), e);
		// }
		// }
		// return componentVOList;
	}

	/**
	 * Check whether their are payments in arrears for the direct debit authority
	 * 
	 * @param authority Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException Description of the Exception
	 */
	public String getDirectDebitStatus(Integer directDebitAuthorityID) throws RemoteException, PaymentException {
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		String statusString = null;
		try {
			statusString = ddAdapter.getDDStatus(directDebitAuthorityID);
		} catch (RemoteException ex) {
			throw new PaymentException(ex);
		}
		return statusString;
	}

	/**
	 * Returns a list of PayableItem that have an amount oustanding for the client and were sent from the source system. Returns an empty list if no payable items can be returned.
	 * 
	 * @param sourceSystem Description of the Parameter
	 * @param clientNumber Description of the Parameter
	 * @return The outstandingPayableItemsForClient value
	 * @exception PaymentException Description of the Exception
	 * @exception RemoteException Description of the Exception
	 */
	// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection getOutstandingPayableItemsForClient(SourceSystem sourceSystem, Integer clientNumber) throws PaymentException, RemoteException {
		ArrayList payableItemList = new ArrayList();
		try {
			Collection allPayableItemList = getPayableItemsByClient(clientNumber);
			double amountOS = 0;
			if (allPayableItemList != null) {
				PayableItemVO payableItemVO = null;
				Iterator iterator = allPayableItemList.iterator();
				while (iterator.hasNext()) {
					payableItemVO = (PayableItemVO) iterator.next();
					// Was the payable item sent from the source system
					if (payableItemVO.getSourceSystem().equals(sourceSystem)) {
						// Does the payable item have an amount outstanding
						amountOS = payableItemVO.getAmountOutstanding();
						if (amountOS > 0.0) {
							payableItemList.add(payableItemVO);
						}
					}
				}
			}
		} catch (RemoteException ex) {
			throw new PaymentException(ex);
		}
		return payableItemList;
	}

	/**
	 * Returns a list of PayableItem that have a direct (non-derived) amount outstanding for the client and were sent from the source system. Returns an empty list if no payable items can be returned.
	 * 
	 * @param sourceSystem
	 * @param clientNumber
	 * @param paymentMethod
	 * @return
	 * @throws RemoteException
	 * @throws PaymentException
	 */
	public Collection<PayableItemVO> getOutstandingPayableItemsForClient(SourceSystem sourceSystem, Integer clientNumber, String paymentMethod) throws RemoteException {
		Criteria criteria = hsession.createCriteria(PayableItemVO.class).add(Expression.eq("clientNo", clientNumber)).add(Expression.eq("sourceSystem", sourceSystem)).add(Expression.eq("paymentMethodDescription", paymentMethod)).add(Expression.isNotNull("directAmountOutstanding")).add(Expression.gt("directAmountOutstanding", BigDecimal.ZERO));

		List<PayableItemVO> payableItemList = criteria.list();

		return payableItemList;
	}

	/**
	 * Returns a list of PayableItem that have a direct (non-derived) amount outstanding for the date range and were sent from the source system. Returns an empty list if no payable items can be returned.
	 * 
	 * @param sourceSystem
	 * @param startDate
	 * @param endDate
	 * @param paymentMethod
	 * @return
	 * @throws RemoteException
	 */
	public Collection<PayableItemVO> getOutstandingPayableItems(SourceSystem sourceSystem, DateTime startDate, DateTime endDate, String paymentMethod) throws RemoteException {

		final String OUTSTANDING_PAYABLES_SQL = "select * FROM [PUB].[pt_payableItem]\n" + "WHERE createDate >= :startDate AND createDate <= :endDate\n" + "AND sourceSystem = :sourceSystem\n" + "AND amountOutstanding IS NOT null AND amountOutstanding > 0\n" + "AND paymentMethod = :paymentMethod";

		Query query = hsession.createSQLQuery(OUTSTANDING_PAYABLES_SQL).addEntity(PayableItemVO.class);
		query.setDate("startDate", startDate);
		query.setDate("endDate", endDate);
		query.setString("sourceSystem", sourceSystem.getAbbreviation());
		query.setString("paymentMethod", paymentMethod);

		/*
		 * Doesn't work due to composite type createDate...
		 * 
		 * Criteria criteria = hsession.createCriteria(PayableItemVO.class) .add(Expression.ge("createDateTime", startDate)) .add(Expression.le("createDateTime", endDate)) .add(Expression.eq("sourceSystem", sourceSystem)) .add(Expression.eq("paymentMethodDescription", paymentMethod)) .add(Expression.isNotNull("directAmountOutstanding")) .add(Expression.gt("directAmountOutstanding", BigDecimal.ZERO));
		 */

		List<PayableItemVO> payableItemList = (List<PayableItemVO>) query.list();

		return payableItemList;
	}

	/**
	 * Returns the list of payable items that are still active. These are defined by their having a component with an unearned posting and the payable item end date after today and no outstanding
	 */
	/*
	 * public Collection getEarnableComponentsBySourceSystemReference (SourceSystem sourceSystem , String sourceSystemReference ) throws RemoteException, PaymentException { if(sourceSystem == null) { throw new SystemException("No source system given!"); } if(sourceSystemReference == null || sourceSystemReference.length() == 0) { throw new SystemException("No source system reference given!"); }
	 * 
	 * ArrayList unearnedComponentList = new ArrayList(); DateTime transactionDate = new DateTime().getDateOnly(); PayableItemVO payableItem = null; //get the components by source system reference Collection componentList = this.getComponentsBySourceSystemReference(sourceSystem. getAbbreviation(), sourceSystemReference); Iterator componentListIterator = componentList.iterator(); PayableItemComponentVO component = null;
	 * 
	 * // Integer payableItemID = null; while(componentListIterator.hasNext()) { component = (PayableItemComponentVO)componentListIterator.next();
	 * 
	 * //avoid duplicates payableItem = component.getPayableItem();
	 * 
	 * //add if the payable item end date is after now - current if(payableItem.endDate.after(transactionDate) && component.isEarnable()) { unearnedComponentList.add(component); }
	 * 
	 * } return unearnedComponentList; }
	 */
	/**
	 * get the history of receipting transactions for a client number from all receipting systems.
	 * 
	 * @param clientNo
	 * @sourceSystem the source system to return the receipts for
	 * @return
	 * @throws RemoteException
	 */
	public Collection<Payable> getReceiptingHistory(Integer clientNo, SourceSystem sourceSystem) throws RemoteException {
		// source system is used for filtering further down the track

		String receiptingAdapters[] = { SourceSystem.ECR.getAbbreviation(), SourceSystem.RECEPTOR.getAbbreviation(), SourceSystem.OCR.getAbbreviation() };
		Collection<Payable> currentReceiptingList = new ArrayList<Payable>();
		ReceiptingAdapter receiptingAdapter = null;
		for (int i = 0; i < receiptingAdapters.length; i++) {
			LogUtil.debug(this.getClass(), "clientNo=" + clientNo);
			try {
				receiptingAdapter = PaymentFactory.getReceiptingAdapter(receiptingAdapters[i]);
				currentReceiptingList.addAll(receiptingAdapter.getPayableHistory(clientNo));
			} catch (Exception ex) {
				// ignore all exceptions for display purposes
				LogUtil.warn(this.getClass(), "Unable to payable history for receipting adapter '" + receiptingAdapters[i] + "'.");
				LogUtil.warn(this.getClass(), ex);
			}
		}
		LogUtil.debug(this.getClass(), "getReceiptingHistory size=" + currentReceiptingList.size());
		return currentReceiptingList;
	}

	/**
	 * Create a set of cancel type ledgers.
	 * 
	 * NOT TRANSACTIONAL
	 * 
	 * @param memTransVO MembershipTransactionVO The transaction to reverse
	 * @throws RemoteException
	 * @throws RollBackException
	 * @return Collection
	 */
	public Collection createCancelTypePostings(String referenceNumber, MembershipTransactionVO memTransVO, String description, boolean fullyUnpaid) throws RemoteException, RollBackException {
		LogUtil.log(this.getClass(), "createCancelTypePostings " + referenceNumber + " " + memTransVO + " " + description + " " + fullyUnpaid);
		PostingContainer newPostingList = new PostingContainer();

		// keep a running total of the postings to 2 decimal places
		BigDecimal postingTotal = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);

		// utility objects
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

		Account gstAccount = refMgr.getMembershipGSTAccount();
		// Account suspenseAccount = refMgr.getMembershipSuspenseAccount();

		// get the current gst rate
		BigDecimal gstRate = new BigDecimal(commonMgr.getCurrentTaxRate().getGstRate());

		FeeTypeVO feeType = null;
		// placeholder postings
		PayableItemPostingVO newPosting = null;
		PayableItemPostingVO unearnedPosting = null;
		PayableItemPostingVO earnedPosting = null;
		PayableItemPostingVO gstPosting = null;
		PayableItemPostingVO suspensePosting = null;

		Account earnedAccount = null;

		DateTime transactionDate = new DateTime().getDateOnly();
		String postingDescription = null;
		BigDecimal unearnedAmount = new BigDecimal(0);
		BigDecimal unearnedAmountFee = null;
		BigDecimal unearnedRatio = null;
		BigDecimal gstExUnearnedAmount = null;
		BigDecimal gstOnUnearnedAmount = null;
		BigDecimal originalUnearnedAmount = null;
		// first remove any outstanding value remaining in the unearned income
		// account
		boolean hasUnearnedAmount = false;

		// reverse earned immediately fees
		BigDecimal feeAmount = null;
		BigDecimal gstExFeeAmount = null;
		BigDecimal gstOnFeeAmount = null;

		Account unearnedAccount = null;

		// historical membership
		MembershipHistory oldMem = memTransVO.getMembershipHistory();

		// int memTermYears = 0;

		Collection transactionFeeList = memTransVO.getTransactionFeeList();
		if (transactionFeeList != null) {
			LogUtil.debug(this.getClass(), "transactionFeeList" + transactionFeeList.size() + "    " + transactionFeeList);
			MembershipTransactionFee mtFee = null;
			Iterator feeIt = transactionFeeList.iterator();
			while (feeIt.hasNext()) {
				mtFee = (MembershipTransactionFee) feeIt.next();
				LogUtil.debug(this.getClass(), "mtFee.getEarnedIncomeAmount() " + mtFee.getEarnedIncomeAmount());
				LogUtil.debug(this.getClass(), "mtFee.getUnearnedIncomeAmount() " + mtFee.getUnearnedIncomeAmount());
				LogUtil.debug(this.getClass(), "mtFee " + mtFee);
				// unearned fees
				if ((mtFee.getEarnedIncomeAmount() > 0 || mtFee.isValueFee()) // contains
						// a
						// value
						// fee
						&& !hasUnearnedAmount) {

					hasUnearnedAmount = true;

					// get the fee type to get the account
					feeType = mtFee.getFeeSpecificationVO().getFeeType();

					LogUtil.debug(this.getClass(), "feeType" + feeType.getDescription());

					// get the unearned account
					unearnedAccount = feeType.getUnearnedAccount();

					earnedAccount = feeType.getEarnedAccount();
					LogUtil.debug(this.getClass(), "earnedAccount" + earnedAccount == null ? "" : earnedAccount.toString());
					LogUtil.debug(this.getClass(), "unearnedAccount" + unearnedAccount == null ? "" : unearnedAccount.toString());

					if (fullyUnpaid) {
						// full amount to reverse
						unearnedRatio = new BigDecimal(1);
					} else {
						// if unpaid it is total days of transaction divided by
						// maximum potential
						unearnedRatio = MembershipHelper.getUnearnedRatio(memTransVO.getEffectiveRange(transactionDate).getTotalDays(), memTransVO.getMaximumPotentialRange().getTotalDays());
					}

					LogUtil.debug(this.getClass(), "unearnedRatio " + unearnedRatio);

					// the original value at the time of the transaction * terms
					originalUnearnedAmount = oldMem.getMembershipValue(true, true);

					LogUtil.debug(this.getClass(), "originalUnearnedAmount " + originalUnearnedAmount);

					// calculate the amount as unearned ratio multiplied by
					// amount
					unearnedAmountFee = unearnedRatio.multiply(originalUnearnedAmount);

					unearnedAmount = unearnedAmount.add(unearnedAmountFee);
					LogUtil.debug(this.getClass(), "unearnedAmountFee " + unearnedAmountFee);
					LogUtil.debug(this.getClass(), "unearnedAmount " + unearnedAmount);

					// gst exclusive
					gstExUnearnedAmount = CurrencyUtil.calculateGSTExclusiveAmount(unearnedAmountFee.doubleValue(), gstRate.doubleValue(), 2);
					// gst component
					gstOnUnearnedAmount = CurrencyUtil.calculateGSTincluded(unearnedAmountFee.doubleValue(), gstRate.doubleValue(), 2);
					LogUtil.debug(this.getClass(), "gstExUnearnedAmount " + gstExUnearnedAmount);

					postingDescription = "Remove unearned amount on " + description + ". Ref: " + referenceNumber + ".";
					try {
						newPosting = new PayableItemPostingVO(gstExUnearnedAmount.doubleValue(), unearnedAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
						newPostingList.add(newPosting);
						unearnedPosting = newPosting;
						// offset below in gst if partially/fully paid
						postingTotal = postingTotal.add(gstExUnearnedAmount);
						LogUtil.debug(this.getClass(), "postingTotal " + postingTotal);
					} catch (PaymentException e) {
						throw new SystemException("Error creating unearned posting : " + e.getMessage(), e);
					}

					postingDescription = "Remove GST amount on " + description + ". Ref: " + referenceNumber + ".";
					try {
						newPosting = new PayableItemPostingVO(gstOnUnearnedAmount.doubleValue(), gstAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
						newPostingList.add(newPosting);
						postingTotal = postingTotal.add(gstOnUnearnedAmount);
					} catch (PaymentException e) {
						throw new SystemException("Error creating unearned posting : " + e.getMessage(), e);
					}

				}
				// fees earned immediately
				if (mtFee.getUnearnedIncomeAmount() > 0) {
					LogUtil.debug(this.getClass(), "fullyUnpaid" + fullyUnpaid);
					// fees that are earned immediately are only reversed when
					// nothing is paid.
					if (fullyUnpaid) {
						feeType = mtFee.getFeeSpecificationVO().getFeeType();

						LogUtil.debug(this.getClass(), "feeType" + feeType.getDescription());

						// get the unearned account
						earnedAccount = feeType.getEarnedAccount();

						feeAmount = new BigDecimal(mtFee.getGrossFee());

						LogUtil.debug(this.getClass(), "feeAmount b4 immediate " + feeAmount);
						LogUtil.debug(this.getClass(), "unearnedAmount b4 immediate " + unearnedAmount);

						// add the immediate fees as they are considered
						// unearned if nothing is paid
						unearnedAmount = unearnedAmount.add(feeAmount);
						LogUtil.debug(this.getClass(), "unearnedAmount of immediate " + unearnedAmount);
						postingDescription = "Remove unearned amount on " + description + ". Ref: " + referenceNumber + ".";

						gstExFeeAmount = CurrencyUtil.calculateGSTExclusiveAmount(feeAmount.doubleValue(), gstRate.doubleValue(), 2);
						gstOnFeeAmount = CurrencyUtil.calculateGSTincluded(feeAmount.doubleValue(), gstRate.doubleValue(), 2);
						// debit fee amount
						try {
							newPosting = new PayableItemPostingVO(gstExFeeAmount.doubleValue(), earnedAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
							newPostingList.add(newPosting);
						} catch (PaymentException e) {
							throw new SystemException("Error creating unearned posting : " + e.getMessage(), e);
						}

						postingDescription = "Remove GST amount on " + description + ". Ref: " + referenceNumber + ".";

						// debit gst amount
						try {
							newPosting = new PayableItemPostingVO(gstOnFeeAmount.doubleValue(), gstAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
							newPostingList.add(newPosting);

						} catch (PaymentException e) {
							throw new SystemException("Error creating unearned posting : " + e.getMessage(), e);
						}

						postingTotal = postingTotal.add(feeAmount); // new
						// amount
						LogUtil.debug(this.getClass(), "postingTotal " + postingTotal);
					}
				}
			}
		}
		LogUtil.debug(this.getClass(), "unearnedAmount of immediate " + unearnedAmount);

		LogUtil.debug(this.getClass(), "1 - " + newPostingList);

		LogUtil.debug(this.getClass(), "1 - " + postingTotal);

		// add the discretionary postings
		PostingContainer discretionaryDiscountPostings = (PostingContainer) createCancelTypePostingsForDiscretionaryDiscount(memTransVO, description, referenceNumber);
		newPostingList.addAll(discretionaryDiscountPostings);

		LogUtil.debug(this.getClass(), "5r- " + postingTotal);

		// BigDecimal amountPayable = new
		// BigDecimal(memTransVO.getNetTransactionFee());
		BigDecimal paidRatio = memTransVO.getPaidRatio();
		// BigDecimal unpaidUnearnedAmount =
		// memTransVO.getUnearnedAmountOutstanding().getAmountOutstanding();
		BigDecimal unpaidAmount = memTransVO.getAmountOutstanding().getAmountOutstanding();

		// if fully unpaid
		if (fullyUnpaid) {
			LogUtil.debug(this.getClass(), "1a - adj" + memTransVO.getAdjustmentTotal());
			LogUtil.debug(this.getClass(), "1b - postingTotal" + postingTotal);
			BigDecimal adjustmentTotal = memTransVO.getAdjustmentTotal();
			LogUtil.debug(this.getClass(), "5p- " + postingTotal);
			// unearnedAmount = unearnedAmount.add(adjustmentTotal);
			unpaidAmount = unpaidAmount.subtract(adjustmentTotal);
			LogUtil.debug(this.getClass(), "unearnedAmount after  " + unearnedAmount);
		}

		// determine if a discount refund is applicable
		if (hasUnearnedAmount) {
			BigDecimal totalDiscountRefund = new BigDecimal(0);
			MembershipTransactionFee txFee = null;
			Iterator discountListIterator = null;
			Collection discountList = null;
			MembershipTransactionDiscount txDiscount = null;

			BigDecimal discountAmount = null;
			BigDecimal gstExDiscountRefund = null;
			BigDecimal gstOnDiscountRefund = null;

			MembershipAccountVO memAccount = null;
			Iterator feeIt2 = transactionFeeList.iterator();
			while (feeIt2.hasNext()) {
				txFee = (MembershipTransactionFee) feeIt2.next();

				LogUtil.debug(this.getClass(), "2a");

				if (!txFee.getFeeSpecificationVO().isEarned()) // if not earned
				{
					LogUtil.debug(this.getClass(), "3");
					discountList = txFee.getDiscountList();
					LogUtil.debug(this.getClass(), "3a");
					discountListIterator = discountList.iterator();
					while (discountListIterator.hasNext()) {
						txDiscount = (MembershipTransactionDiscount) discountListIterator.next();

						LogUtil.debug(this.getClass(), "4");

						memAccount = txDiscount.getDiscountTypeVO().getDiscountAccount();

						discountAmount = new BigDecimal(txDiscount.getDiscountAmount());

						// offset discounts
						if (memAccount != null) {

							// discountRefund = discountRefund.setScale(2,
							// BigDecimal.ROUND_HALF_UP);
							LogUtil.debug(this.getClass(), "4aa " + discountAmount);

							gstExDiscountRefund = CurrencyUtil.calculateGSTExclusiveAmount(discountAmount.doubleValue(), gstRate.doubleValue(), 2);
							gstOnDiscountRefund = CurrencyUtil.calculateGSTincluded(discountAmount.doubleValue(), gstRate.doubleValue(), 2);

							LogUtil.debug(this.getClass(), "4aa");

							// ledgers are only created when fully unpaid
							if (fullyUnpaid) {
								LogUtil.debug(this.getClass(), "fully unpaid");
								// reverse gst comp
								try {
									postingDescription = description + " of GST portion of discount and other alternative income.";
									newPosting = new PayableItemPostingVO(gstOnDiscountRefund.negate().doubleValue(), gstAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
									LogUtil.debug(this.getClass(), "newPosting" + newPosting);
									newPostingList.add(newPosting);
								} catch (PaymentException e) {
									throw new SystemException("Error creating GST posting : " + e.getMessage(), e);
								}

								// reverse discount
								try {
									postingDescription = description + " of discount and other alternative income.";
									newPosting = new PayableItemPostingVO(gstExDiscountRefund.negate().doubleValue(), memAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
									LogUtil.debug(this.getClass(), "newPosting" + newPosting);
									newPostingList.add(newPosting);
								} catch (PaymentException e) {
									throw new SystemException("Error creating GST posting : " + e.getMessage(), e);
								}

								LogUtil.debug(this.getClass(), "postingTotal");
							}

						}
						// non-offset ledgers
						else {
							// it should affect the total only
							LogUtil.debug(this.getClass(), "non-offset ledgers");
							// unearned credit and credit - funded from suspense
							// account
						}

						// total discount
						totalDiscountRefund = totalDiscountRefund.add(discountAmount);

					}

					// add it to the total
					postingTotal = postingTotal.subtract(totalDiscountRefund);

					LogUtil.debug(this.getClass(), " >>------> after discount refunds: " + newPostingList);

					LogUtil.debug(this.getClass(), "postingTotal " + postingTotal);
				}
			}

			LogUtil.debug(this.getClass(), " >>------> after discount refunds: " + newPostingList);

			BigDecimal earnedRatio = new BigDecimal(1).subtract(unearnedRatio);
			LogUtil.debug(this.getClass(), " >>------> earnedRatio: " + earnedRatio);
			LogUtil.debug(this.getClass(), " >>------> paidRatio: " + paidRatio);

			// if earned more than paid
			if (earnedRatio.compareTo(paidRatio) > 0) // was >=
			{
				LogUtil.debug(this.getClass(), "5t- " + postingTotal);

				// reverse earned

				// Remove the amount earned but unpaid from the earned account.
				// This is the earned total less the GST exclusive paid amount.
				LogUtil.debug(this.getClass(), ">>>>>>>>>>originalUnearnedAmount=" + originalUnearnedAmount);
				BigDecimal earnedAmount = originalUnearnedAmount.subtract(unearnedAmount);
				LogUtil.debug(this.getClass(), "earnedAmount=" + earnedAmount);
				LogUtil.debug(this.getClass(), "originalUnearnedAmount=" + originalUnearnedAmount);
				LogUtil.debug(this.getClass(), "paidRatio=" + paidRatio);
				BigDecimal paidAmount = originalUnearnedAmount.multiply(paidRatio);
				LogUtil.debug(this.getClass(), "paidAmount=" + paidAmount);
				BigDecimal earnedButUnpaidAmount = earnedAmount.subtract(paidAmount);
				BigDecimal gstExEarnedButUnpaidAmount = CurrencyUtil.calculateGSTExclusiveAmount(earnedButUnpaidAmount.doubleValue(), gstRate.doubleValue(), 2);
				LogUtil.debug(this.getClass(), "earnedButUnpaidAmount=" + earnedButUnpaidAmount);

				if (unearnedAccount == null) {
					throw new SystemException("Unable to determine the unearned account for reversing earned.");
				}

				postingDescription = "Remove earned but unpaid amount on " + description + " . Ref: " + referenceNumber + ".";
				try {
					newPosting = new PayableItemPostingVO(gstExEarnedButUnpaidAmount.doubleValue(), unearnedAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
					newPostingList.add(newPosting);
					LogUtil.debug(this.getClass(), "3 - " + postingTotal);
				} catch (PaymentException e) {
					throw new SystemException("Error creating unearned posting : " + e.getMessage(), e);
				}

				// The posting to take money out of the GST account will
				// be the total amount unpaid times the GST rate.
				BigDecimal gstAmount = CurrencyUtil.calculateGSTincluded(earnedButUnpaidAmount.doubleValue(), gstRate.doubleValue(), 2);
				LogUtil.debug(this.getClass(), "gstAmount=" + gstAmount);
				postingDescription = "Remove GST on earned but unpaid amount on " + description + " . Ref: " + referenceNumber + ".";
				try {
					newPosting = new PayableItemPostingVO(gstAmount.doubleValue(), gstAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
					newPostingList.add(newPosting);
					LogUtil.debug(this.getClass(), "4 - " + postingTotal);
				} catch (PaymentException e) {
					throw new SystemException("Error creating GST posting : " + e.getMessage(), e);
				}
				LogUtil.debug(this.getClass(), "unearnedAmount 1 " + unearnedAmount);
				// add to unearned amount
				unearnedAmount = unearnedAmount.add(earnedButUnpaidAmount);
				LogUtil.debug(this.getClass(), "unearnedAmount 2 " + unearnedAmount);
				// add to the posting total
				postingTotal = postingTotal.add(earnedButUnpaidAmount);

			} // end of if earned

			LogUtil.debug(this.getClass(), "5x- " + postingTotal);
			LogUtil.debug(this.getClass(), "5x-unearnedAmount " + unearnedAmount);
			LogUtil.debug(this.getClass(), "5x-unpaidAmount " + unpaidAmount);

			// the posting to put money into the suspense account will be
			// the amount paid, less the GST inclusive amount earned and
			// other income sources.
			BigDecimal suspenseAmount = unearnedAmount.subtract(unpaidAmount); // unpaid
			// unearned
			// scale to two decimal places
			suspenseAmount = suspenseAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
			LogUtil.debug(this.getClass(), "suspenseAmount=" + suspenseAmount);
			// test that the figure is positive
			if (suspenseAmount.doubleValue() < 0) {
				throw new SystemException("Unable to " + description + " as the unpaid amount '" + unpaidAmount + "' is greater than the earnable fee '" + unearnedAmount + "'. Suspense amount is '" + suspenseAmount + "'.");
			}

			/** @todo Rounding errors going into suspense! */

			// refund if fully unpaid
			if (fullyUnpaid) {

				// affiliated club credits?
				double affiliatedClubCredit = memTransVO.getTransferCreditApplied();

				if (affiliatedClubCredit > 0) {

					// reverse the original discount debit and suspense credit

					double affiliatedClubCreditExGST = CurrencyUtil.calculateGSTExclusiveAmount(affiliatedClubCredit, gstRate.doubleValue());
					double affiliatedClubCreditGST = CurrencyUtil.calculateGSTincluded(affiliatedClubCredit, gstRate.doubleValue());

					refMgr = MembershipEJBHelper.getMembershipRefMgr();
					Account discretionaryDiscountAccount = refMgr.getMembershipAccountByTitle(MembershipAccountVO.MEMBERSHIP_TRANSFER_ACCOUNT);

					// suspense debit
					try {
						newPosting = new PayableItemPostingVO(NumberUtil.roundDouble(affiliatedClubCreditExGST, 2), refMgr.getMembershipSuspenseAccount(), null, transactionDate, transactionDate, Company.RACT, "Refund membership suspense account for affiliated club credit. Ref: " + referenceNumber, null);
					} catch (PaymentException e) {
						throw new SystemException("Error creating posting to Suspense Account : ", e);
					}

					// add ledger
					newPostingList.add(newPosting);

					// discretionary discount
					try {
						newPosting = new PayableItemPostingVO(-NumberUtil.roundDouble(affiliatedClubCreditExGST, 2), discretionaryDiscountAccount, null, transactionDate, transactionDate, Company.RACT, "Refund discretionary discount account to fund affiliated club credit. Ref: " + referenceNumber, null);
					} catch (PaymentException e) {
						throw new SystemException("Error creating posting to Discretionary Discount Account : ", e);
					}
					// add ledger
					newPostingList.add(newPosting);

					// previous two postings should net to zero

					postingDescription = "Transfer the GST exclusive credit amount on " + description + " . Ref: " + referenceNumber + " to the suspense account.";
					try {
						newPosting = new PayableItemPostingVO(-NumberUtil.roundDouble(affiliatedClubCreditExGST, 2), refMgr.getMembershipSuspenseAccount(), null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
						newPostingList.add(newPosting, false);
						// may need to uncomment this?
						// suspensePosting = newPosting;
						postingTotal = postingTotal.subtract(new BigDecimal(affiliatedClubCreditExGST));
						LogUtil.debug(this.getClass(), "5y- " + postingTotal);

					} catch (PaymentException e) {
						throw new SystemException("Error creating suspense posting for affiliated club: ", e);
					}

					// gst
					postingDescription = "Transfer the GST refunded in affiliated club transfer on " + description + " . Ref: " + referenceNumber + " to the suspense account.";
					try {
						newPosting = new PayableItemPostingVO(-NumberUtil.roundDouble(affiliatedClubCreditGST, 2), gstAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
						newPostingList.add(newPosting, false);
						postingTotal = postingTotal.subtract(new BigDecimal(affiliatedClubCreditGST));
						LogUtil.debug(this.getClass(), "5z- " + postingTotal);

					} catch (PaymentException e) {
						throw new SystemException("Error creating gst posting for affiliated club: ", e);
					}

					// subtract affiliated club credit from suspense amount
					suspenseAmount = suspenseAmount.subtract(new BigDecimal(affiliatedClubCredit));
				}

			}

			// subtract any discount refunds
			// suspenseAmount = suspenseAmount.subtract(totalDiscountRefund);

			// if suspense is still positive after removing affiliated club
			// credit
			if (suspenseAmount.compareTo(new BigDecimal(0)) > 0) {
				postingDescription = "Transfer the GST inclusive unearned and paid amount on " + description + " . Ref: " + referenceNumber + " to the suspense account.";
				try {
					newPosting = new PayableItemPostingVO(suspenseAmount.negate().doubleValue(), refMgr.getMembershipSuspenseAccount(), null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
					newPostingList.add(newPosting, true);
					suspensePosting = newPosting;
					postingTotal = postingTotal.subtract(suspenseAmount);
					LogUtil.debug(this.getClass(), "6 - " + postingTotal);

				} catch (PaymentException e) {
					throw new SystemException("Error creating suspense posting : ", e);
				}
			}
		}

		LogUtil.debug(this.getClass(), "6z- " + postingTotal);

		// clearing
		BigDecimal clearingAmount = unpaidAmount; // unpaid amount with
		// immediate fees
		if (clearingAmount.doubleValue() > 0) {
			// clearingAmount = clearingAmount.multiply(unpaidRatio);
			MembershipAccountVO memClearingAccount = refMgr.getMembershipClearingAccount();
			String clearingAccountNumber = memClearingAccount.getAccountNumber();
			postingDescription = "Reverse unpaid amount on " + description + " . Ref: " + referenceNumber + ".";
			try {
				newPosting = new PayableItemPostingVO(-clearingAmount.doubleValue(), memClearingAccount, null, transactionDate, transactionDate, Company.RACT, postingDescription, null);
				newPostingList.add(newPosting);
				postingTotal = postingTotal.subtract(clearingAmount);
				LogUtil.debug(this.getClass(), "7 - " + postingTotal);
			} catch (PaymentException e) {
				throw new SystemException("Error creating clearing posting : " + e.getMessage(), e);
			}
		}

		// rounding
		BigDecimal maxRoundingError = new BigDecimal(0);
		postingTotal = postingTotal.setScale(2, BigDecimal.ROUND_HALF_UP);
		for (int i = 0; i < newPostingList.size(); i++) {
			// maximum of 1 cent per posting
			maxRoundingError = maxRoundingError.add(new BigDecimal(0.01));
		}

		LogUtil.debug(this.getClass(), "maxRoundingError = " + maxRoundingError);
		LogUtil.debug(this.getClass(), "postingTotal = " + postingTotal.abs());

		// check maximum rounding error allowance +/- 0.01 cent per posting
		if (postingTotal.abs().compareTo(maxRoundingError) < 0) // if posting
		// total < max
		// rounding
		// allowance
		{
			try {
				if (gstPosting != null) {
					gstPosting.setAmount(gstPosting.getAmount() - postingTotal.doubleValue());
				} else if (unearnedPosting != null) {
					unearnedPosting.setAmount(unearnedPosting.getAmount() - postingTotal.doubleValue());
				} else if (earnedPosting != null) {
					earnedPosting.setAmount(earnedPosting.getAmount() - postingTotal.doubleValue());
				} else if (suspensePosting != null) {
					suspensePosting.setAmount(suspensePosting.getAmount() - postingTotal.doubleValue());
				} else {
					throw new SystemException("Unable to make postings balance.  Posting total is " + postingTotal.toString() + "/" + maxRoundingError + " (postings: " + newPostingList + ").");
				}
			} catch (PaymentException e) {
				throw new SystemException("Error adjusting postings : " + e.getMessage(), e);
			}
		} else if (postingTotal.compareTo(maxRoundingError) > 0) {
			throw new SystemException("Postings do not balance.  Posting total is " + postingTotal.toString() + " (postings: " + newPostingList + ").");
		}

		// remove any postings with zero amount to prevent posting to the
		// finance system.
		newPostingList.removePostingsWithZeroAmount();

		// testing

		return newPostingList;
	}

	/**
	 * <p>
	 * Get a list of clients with outstanding amounts.
	 * </p>
	 * 
	 * @throws RemoteException
	 * @return Collection
	 */
	public Collection getClientsWithOutstandingAmounts(String paymentMethod) throws RemoteException {
		DateTime startTime = new DateTime();
		DateTime inceptionDate = null;
		try {
			inceptionDate = new DateTime("30/09/2002");
		} catch (ParseException ex1) {
			throw new RemoteException("Unable to determine the inception date for the Roadside system.");
		}

		ArrayList clientList = new ArrayList();
		final String sql = "SELECT clientno, paymentmethod, paymentmethodid, createdate, amountpayable, payment_system, payableitemid FROM pub.pt_payableitem WHERE paymentmethod = ? " + "AND createdate > ?";
		Connection connection = null;
		PreparedStatement statement = null;
		String clientNumber = null;
		String paymentMethodID = null;
		String paymentSystem = null;
		String payableItemId = null;

		BigDecimal amountPayable = null;
		DateTime createDate = null;
		BigDecimal amountOutstanding = null;

		DirectDebitSchedule tmpSchedule = null;
		Payable tmpPayable = null;

		AmountOutstanding amountOs = null;
		DirectDebitAdapter directDebitAdaper = PaymentFactory.getDirectDebitAdapter();
		int counter = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setString(1, paymentMethod);
			statement.setDate(2, inceptionDate.toSQLDate());
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				counter++;
				if (counter % 1000 == 0) {
					LogUtil.debug(this.getClass(), "Record count = " + counter);
				}
				try {
					clientNumber = rs.getString(1);
					paymentMethodID = rs.getString(3);
					createDate = new DateTime(rs.getDate(4));
					amountPayable = new BigDecimal(rs.getDouble(5));
					paymentSystem = rs.getString(6);
					payableItemId = rs.getString(7);

					if (PaymentMethod.DIRECT_DEBIT.equals(paymentMethod)) {
						tmpSchedule = directDebitAdaper.getSchedule(new Integer(paymentMethodID)); // slow
						amountOutstanding = tmpSchedule.getTotalAmountOutstanding();
					} else if (PaymentMethod.RECEIPTING.equals(paymentMethod)) {
						// get payable
						tmpPayable = getPayable(new Integer(clientNumber), paymentMethodID, paymentSystem, SourceSystem.MEMBERSHIP);
						if (tmpPayable == null) {
							LogUtil.warn(this.getClass(), "No payable found for client number " + clientNumber + ", payment method id " + paymentMethodID + " in payment system " + paymentSystem);
							continue;
						}
						amountOutstanding = tmpPayable.getAmountOutstanding();
					} else {
						// log an error
						throw new SystemException("Payment method '" + paymentMethod + "' is unknown for payable item " + payableItemId);
					}

					if (amountOutstanding != null && amountOutstanding.compareTo(new BigDecimal(0)) > 0) {
						amountOs = new AmountOutstanding();
						amountOs.setClientNumber(clientNumber);
						amountOs.setPaymentMethodDescription(paymentMethod);
						amountOs.setCreateDate(createDate);
						amountOs.setPaymentMethodID(paymentMethodID);
						amountOs.setAmountPayable(amountPayable);
						amountOs.setAmountOutstanding(amountOutstanding);
						amountOs.setPayableReferenceId(payableItemId);
						clientList.add(amountOs);
					}
				} catch (Exception ex) {
					// ignore
					LogUtil.fatal(this.getClass(), "Payable item " + payableItemId + " has no payable. Client " + clientNumber + ". " + ex.toString());
				}

			}
		} catch (SQLException se) {
			throw new RemoteException("Error finding clients with outstanding amounts: ", se);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		DateTime endTime = new DateTime();
		long duration = endTime.getTime() - startTime.getTime();
		duration = duration / 1000; // seconds

		LogUtil.debug(this.getClass(), "Processed " + counter + " records in " + duration + " seconds.");
		return clientList;
	}

	public PostingContainer getUnearnedPostingByComponent(Integer payableItemComponentID) throws RemoteException {
		PostingContainer postings = new PostingContainer();

		Query query = hsession.createQuery("select max(payableItemPostingId) from PayableItemPosting p where payableItemComponentID = :payableitemcomponentid and (incomeAccountNumber != '' and amountOutstanding > 0) ");
		query.setParameter("payableitemcomponentid", payableItemComponentID);

		Collection result = query.list();
		PayableItemPostingVO payableItemPosting = null;
		for (Iterator it = result.iterator(); it.hasNext();) {
			Object[] objects = (Object[]) it.next();
			payableItemPosting = getPayableItemPosting((Integer) objects[0]);
			postings.add(payableItemPosting);
		}

		return postings;

	}

	/**
	 * Get the postings for this component. If no postings are found it returns an empty list.
	 */
	public PostingContainer getPostings(SourceSystem srcSystem, Integer payableItemComponentID) throws RemoteException, PaymentException {
		PostingContainer postings = new PostingContainer();

		List postingList = hsession.createCriteria(PayableItemPostingVO.class).add(Expression.eq("payableItemComponentID", payableItemComponentID)).list();
		LogUtil.log(this.getClass(), "adding all 1 " + postingList);
		postings.addAll(postingList);
		LogUtil.log(this.getClass(), "adding all 2");
		return postings;

	}

	public PostingContainer getPostings(Integer clientNo, Integer transId, DateTime transDate) throws RemoteException, PaymentException {

		PostingContainer pc = null;

		try {

			PayableItemVO piVO = getPayableItem(transId);

			PayableItemComponentVO picVO = null;

			SourceSystem srcSystem = null;
			Integer payableItemComponentID = null;
			Integer payableItemID = null;
			Collection components = null;
			components = piVO.getComponents();

			if (components == null) {
				throw new RemoteException();
			}

			Iterator compIT = components.iterator();
			while (compIT.hasNext()) {
				picVO = (PayableItemComponentVO) compIT.next();
				pc = picVO.getPostings();
			}
		} catch (Exception ex) {
			LogUtil.log(this.getClass(), ex.getMessage());
			ex.printStackTrace();
			pc = null;
		}

		return pc;
	}

	/**
	 * Get a list of components for a payable item.
	 * 
	 * @todo use SourceSystem parameter
	 * @param payableItemID
	 * @return
	 * @throws RemoteException
	 */
	public Collection getComponents(SourceSystem srcSystem, Integer payableItemID) throws RemoteException {
		LogUtil.debug(this.getClass(), "getComponents 1");
		LogUtil.debug(this.getClass(), "srcSystem=" + srcSystem);
		LogUtil.debug(this.getClass(), "payableItemID=" + payableItemID);

		LogUtil.debug(this.getClass(), "getComponents 1a");
		List componentList = hsession.createCriteria(PayableItemComponentVO.class).add(Expression.eq("payableItemID", payableItemID)).list();
		LogUtil.debug(this.getClass(), "getComponents 2");
		return componentList;
	}

	/**
	 * Return a payable amount from the receipting system.
	 */
	public Payable getPayable(Integer clientNumber, String paymentMethodID, String paymentSystem, SourceSystem sourceSystem) throws PaymentException {
		LogUtil.log(this.getClass(), "getPayable start");

		LogUtil.log(this.getClass(), "clientNumber=" + clientNumber);
		LogUtil.log(this.getClass(), "paymentMethodID=" + paymentMethodID);
		LogUtil.log(this.getClass(), "paymentSystem=" + paymentSystem);
		LogUtil.log(this.getClass(), "sourceSystem=" + sourceSystem);
		Payable payable = null;
		try {
			ReceiptingAdapter receiptingAdapter = null;
			if (paymentSystem != null) {
				receiptingAdapter = PaymentFactory.getReceiptingAdapter(paymentSystem);
			} else {
				receiptingAdapter = PaymentFactory.getReceiptingAdapter(sourceSystem);
			}
			ReceiptingIdentifier ri = new ReceiptingIdentifier(clientNumber, paymentMethodID);
			LogUtil.debug(this.getClass(), "ri=" + ri);
			payable = receiptingAdapter.getPayable(ri.getUniqueID(), sourceSystem.getAbbreviation());
		} catch (Exception e) {
			// system level exceptions (ie. java.rmi.RemoteException) thrown
			// will cause a transaction rollback
			throw new PaymentException(e);
		}
		LogUtil.log(this.getClass(), "getPayable end");
		return payable;
	}

	/**
	 * Get the actual receipt
	 * 
	 * @param clientNo Integer
	 * @param seqNo Integer
	 * @throws RemoteException
	 * @throws RollBackException
	 * @return Receipt
	 */
	public Receipt getReceipt(Integer clientNumber, String paymentMethodId, SourceSystem sourceSystem) throws PaymentException {
		LogUtil.log(this.getClass(), "getReceipt start");

		LogUtil.log(this.getClass(), "clientNumber=" + clientNumber);
		LogUtil.log(this.getClass(), "paymentMethodId=" + paymentMethodId);
		LogUtil.log(this.getClass(), "sourceSystem=" + sourceSystem);

		Receipt receipt = null;
		try {
			ReceiptingAdapter receiptingAdapter = PaymentFactory.getReceiptingAdapter(sourceSystem);
			LogUtil.log(this.getClass(), "receiptingAdapter=" + receiptingAdapter.getSystemName());

			if (receiptingAdapter.getSystemName().equals(SourceSystem.ECR.getAbbreviation())) {
				LogUtil.log(this.getClass(), "Current receipting system does not store pendings");
				return null;
			}

			ReceiptingIdentifier ri = new ReceiptingIdentifier(clientNumber, paymentMethodId);
			receipt = receiptingAdapter.getReceipt(ri.getUniqueID(), sourceSystem);
		} catch (Exception e) {
			throw new PaymentException(e.getMessage());
		}
		LogUtil.log(this.getClass(), "getReceipt end");
		return receipt;
	}

	/**
	 * Validate the bsb number
	 * 
	 * @param bsbNumber Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException Description of the Exception
	 */
	public String validateBsbNumber(String bsbNumber) throws RemoteException {
		String bankCode = null;
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		bankCode = ddAdapter.validateBSBNumber(bsbNumber);
		return bankCode;
	}

	/**
	 * Validate a credit card number.
	 * 
	 * @throws RemoteException
	 * @return String
	 */
	public String validateCreditCardType(String ccNumber) throws RemoteException {
		String ccType = null;
		DirectDebitAdapter ddAdapter = PaymentFactory.getDirectDebitAdapter();
		ccType = ddAdapter.validateCreditCardType(ccNumber);
		return ccType;
	}

	/**
	 * Gets the dataSource attribute of the PaymentMgrBean object
	 * 
	 * @param dsName Description of the Parameter
	 * @return The dataSource value
	 * @exception RemoteException Description of the Exception
	 */
	private void setDataSource(String dsName) throws RemoteException {
		try {
			dataSource = DataSourceFactory.getDataSource(dsName);
		} catch (NamingException ex) {
			throw new RemoteException(ex.toString());
		}
	}

	/**
	 * Get a summary of postings within a current period of time.
	 * 
	 * Only posted items.
	 * 
	 * @param startDate Description of the Parameter
	 * @param endDate Description of the Parameter
	 * @return The postingsSummary value
	 * @exception PaymentException Description of the Exception
	 */
	public Collection getPostingSummary(DateTime enteredDate, DateTime effectiveDate, String accountNumber) throws PaymentException, RemoteException {
		PostingContainer postings = getPostingContainer(enteredDate, effectiveDate, accountNumber);
		return postings.getPostingSummary();
	}

	public Collection getPostingSummary(String journalKey) throws PaymentException, RemoteException {
		PostingContainer postings = getPostingContainer(journalKey);
		return postings.getPostingSummary();
	}

	public Hashtable getJournals(DateTime enteredDate, DateTime effectiveDate, String accountNumber) throws RemoteException {
		PostingContainer postings = getPostingContainer(enteredDate, effectiveDate, accountNumber);
		return postings.dissectByJournalKey();
	}

	public Hashtable getJournals(String journalKey) throws RemoteException {
		PostingContainer postings = getPostingContainer(journalKey);
		return postings.dissectByJournalKey();
	}

	private PostingContainer getPostingContainer(DateTime enteredDate, DateTime effectiveDate, String accountNumber) throws HibernateException {
		LogUtil.log(this.getClass(), "enteredDate=" + enteredDate);
		LogUtil.log(this.getClass(), "effectiveDate=" + effectiveDate);
		LogUtil.log(this.getClass(), "accountNumber=" + accountNumber);
		PostingContainer postings = new PostingContainer();

		Criteria crit = hsession.createCriteria(PayableItemPostingVO.class);
		if (enteredDate != null) {
			crit = crit.add(Expression.eq("postedDate", enteredDate));
		}
		if (effectiveDate != null) {
			crit = crit.add(Expression.eq("postAtDate", effectiveDate));
		}
		if (accountNumber != null) {
			crit = crit.add(Expression.eq("accountNumber", accountNumber));
		}
		LogUtil.log(this.getClass(), "crit=" + crit.toString());
		List postingList = crit.list();
		LogUtil.log(this.getClass(), "postingList size=" + postingList.size());
		postings.addAll(postingList);
		return postings;
	}

	private PostingContainer getPostingContainer(String journalKey) throws HibernateException {
		LogUtil.log(this.getClass(), "journalKey=" + journalKey);
		PostingContainer postings = new PostingContainer();

		Criteria crit = hsession.createCriteria(PayableItemPostingVO.class);
		if (journalKey != null) {
			crit = crit.add(Expression.eq("journalKey", journalKey));
		}
		LogUtil.log(this.getClass(), "crit=" + crit.toString());
		List postingList = crit.list();
		LogUtil.log(this.getClass(), "postingList size=" + postingList.size());
		postings.addAll(postingList);
		return postings;
	}

	public Collection getPostingSummaryDetails(String journalKey) throws PaymentException, RemoteException {
		Connection connection = null;
		String sql = null;
		PreparedStatement statement = null;
		PostingContainer postings = new PostingContainer();
		PostingSummary ps = null;
		// entered date may not be equal to effective date
		try {
			connection = dataSource.getConnection();

			sql = " SELECT a.posted, a.postAt, a.accountNumber, a.amount, c.clientno, c.sourcesystemreferenceid, c.description ";
			sql += "FROM PUB.pt_payableItemPosting a, PUB.pt_payableitemcomponent b, PUB.pt_payableitem c ";
			// effective date
			sql += "WHERE c.payableitemid = b.payableitemid ";
			sql += "AND b.payableitemcomponentid = a.payableitemcomponentid ";
			sql += "AND a.journalKey = ? ";

			statement = connection.prepareStatement(sql);
			statement.setString(1, journalKey);

			ResultSet postingsSet = statement.executeQuery();
			while (postingsSet.next()) {
				ps = new PostingSummary();
				ps.setEnteredDate(new DateTime(postingsSet.getDate(1)));
				ps.setEffectiveDate(new DateTime(postingsSet.getDate(2)));
				ps.setAccountNumber(postingsSet.getString(3));
				ps.setAmount(postingsSet.getBigDecimal(4));
				ps.setClientNumber(postingsSet.getString(5));
				ps.setTransactionReference(postingsSet.getString(6));
				ps.setNarration(postingsSet.getString(7));
				postings.add(ps);
			}

		} catch (Exception e) {
			throw new PaymentException("Error getting postings detail. " + sql, e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return postings;
	}

	public Collection getPendingFeeByClientNumber(Integer clientNumber) throws RemoteException {

		// restrict by client number
		List pendingFeeList = hsession.createCriteria(PendingFee.class).add(Expression.eq("clientNumber", clientNumber)).list();
		return pendingFeeList;
	}

	public Collection<PendingFee> getOutstandingPendingFeeByClientNumber(Integer clientNumber) throws RemoteException {
		// restrict by client number, not-expired and not paid
		Collection<PendingFee> pendingFeeList = hsession.createCriteria(PendingFee.class).add(Expression.eq("clientNumber", clientNumber)).add(Expression.ge("feeExpiryDate", new DateTime())).add(Expression.isNull("datePaid")).list();
		return pendingFeeList;
	}

	/**
	 * Find a pending fee based on a membership renewal notice document id.
	 */
	public PendingFee getPendingFee(Integer renewalNoticeDocID, SourceSystem sourceSystem) throws RemoteException, PaymentException {
		LogUtil.debug(this.getClass(), "getPendingFee");
		LogUtil.debug(this.getClass(), "renewalNoticeDocID=" + renewalNoticeDocID);
		// membership specific
		if (!SourceSystem.MEMBERSHIP.equals(sourceSystem)) {
			throw new RemoteException("Unable to get pending fees for source system '" + sourceSystem + "'.");
		}
		String pendingFeeId = BillpayHelper.createIvrNumber(renewalNoticeDocID.intValue(), sourceSystem, false);
		LogUtil.debug(this.getClass(), "pendingFeeId=" + pendingFeeId);
		PendingFee pendingFee = getPendingFee(pendingFeeId);
		
		// If not found look for a number using the old concat 0 check digit method
		if (pendingFee == null) {
			pendingFeeId = BillpayHelper.createIvrNumber(renewalNoticeDocID.intValue(), sourceSystem, true);
			pendingFee = getPendingFee(pendingFeeId);
		}
		return pendingFee;
	}

	/**
	 * Find a pending fee based on a membership renewal notice document id.
	 */
	public PendingFee getPendingFee(String pendingFeeId) throws RemoteException, PaymentException {

		PendingFee pendingFee = null;
		try {
			pendingFee = (PendingFee) hsession.get(PendingFee.class, pendingFeeId);
		} catch (HibernateException ex) {
			throw new PaymentException(ex);
		}
		return pendingFee;
	}

	/**
	 * Create the cancel type postings for discretionary discounts.
	 * 
	 * @param oldMemTxVO MembershipTransactionVO
	 * @param transactionTypeCode String
	 * @param membershipNumber String
	 * @throws RemoteException
	 * @return Collection
	 */
	private Collection createCancelTypePostingsForDiscretionaryDiscount(MembershipTransactionVO oldMemTxVO, String transactionTypeCode, String membershipNumber) throws RemoteException {
		PostingContainer postingList = new PostingContainer();
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		MembershipAccountVO memAccount = null;
		PayableItemVO payableItem = oldMemTxVO.getPayableItemForPA();
		// assume fully paid
		boolean fullyUnpaid = payableItem == null ? false : payableItem.isFullyUnpaid();

		DateTime transactionDate = new DateTime().getDateOnly();

		LogUtil.debug(this.getClass(), "discretionary discount");
		// get adjustment list from old mem transaction
		Collection adjustmentList = oldMemTxVO.getAdjustmentList();

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		BigDecimal gstRate = new BigDecimal(commonMgr.getCurrentTaxRate().getGstRate());

		// the accounts
		MembershipAccountVO memClearingAccount = refMgr.getMembershipClearingAccount();
		Account suspenseAccount = refMgr.getMembershipSuspenseAccount();
		Account gstAccount = refMgr.getMembershipGSTAccount();

		Iterator adjustmentListIterator = adjustmentList.iterator();
		while (adjustmentListIterator.hasNext()) {
			MembershipTransactionAdjustment adjustment = (MembershipTransactionAdjustment) adjustmentListIterator.next();
			// locate the discretionary discount
			if (AdjustmentTypeVO.DISCRETIONARY_DISCOUNT.equals(adjustment.getAdjustmentTypeCode())) {

				BigDecimal discountAmount = adjustment.getAdjustmentAmount();
				LogUtil.debug(this.getClass(), "discAmt " + discountAmount);

				/**
				 * KH/GH 03/06/2004 Discretionary discounts are only "refunded" if there are no amounts paid.
				 * 
				 * @todo given this rule look at the historical impact
				 */
				if (fullyUnpaid) {

					BigDecimal gstOnDiscountAmount = CurrencyUtil.calculateGSTincluded(discountAmount.doubleValue(), gstRate.doubleValue(), 2);
					BigDecimal gstExDiscountAmount = CurrencyUtil.calculateGSTExclusiveAmount(discountAmount.doubleValue(), gstRate.doubleValue(), 2);
					LogUtil.debug(this.getClass(), "gstExDiscountAmount=" + gstExDiscountAmount);
					LogUtil.debug(this.getClass(), "gstOnDiscountAmount=" + gstOnDiscountAmount);

					// Discretionary discounts are now being posted to the
					// membership misc account
					memAccount = refMgr.getMembershipAccountByTitle(MembershipAccountVO.MEMBERSHIP_MISC_ACCOUNT);
					try {
						PayableItemPostingVO newPosting = new PayableItemPostingVO(gstExDiscountAmount.negate().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(), memAccount, null, transactionDate, transactionDate, Company.RACT, "Rebate of discretionary discount on " + transactionTypeCode + " of membership " + membershipNumber, null);
						postingList.add(newPosting);
					} catch (PaymentException e) {
						throw new SystemException("Error creating discretionary discount posting : " + e.getMessage(), e);
					}

					Iterator postingListIterator = postingList.iterator();
					PayableItemPostingVO posting = null;

					// create a posting to the clearing account
					try {
						PayableItemPostingVO newPosting = new PayableItemPostingVO(discountAmount.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(), memClearingAccount, null, transactionDate, transactionDate, Company.RACT, "Rebate of discretionary discount on " + transactionTypeCode + " of membership " + membershipNumber, null);
						postingList.add(newPosting);
					} catch (PaymentException e) {
						throw new SystemException("Error creating clearing posting : " + e.getMessage(), e);
					}

					// create a new one
					try {
						PayableItemPostingVO newPosting = new PayableItemPostingVO(gstOnDiscountAmount.negate().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(), gstAccount, null, transactionDate, transactionDate, Company.RACT, "GST on rebate of discretionary discount on " + transactionTypeCode + " of membership " + membershipNumber, null);
						postingList.add(newPosting);
					} catch (PaymentException e) {
						throw new SystemException("Error creating GST posting : " + e.getMessage(), e);
					}
				}
			}

			LogUtil.debug(this.getClass(), "After gst " + postingList.toString());

		}

		return postingList;
	}

}
