package com.ract.payment;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.payment.finance.Cheque;
import com.ract.payment.finance.FinanceMgrLocal;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.util.DateTime;

public class SearchApprovedVouchersAction extends ActionSupport {

    @InjectEJB(name = "FinanceMgrBean")
    private FinanceMgrLocal financeMgr;

    private DateTime startDate;

    private DateTime endDate;

    public DateTime getStartDate() {
	return startDate;
    }

    @InjectEJB(name = "UserMgrBean")
    private UserMgrLocal userMgr;

    public void setStartDate(DateTime startDate) {
	this.startDate = startDate;
    }

    public DateTime getEndDate() {
	return endDate;
    }

    public void setEndDate(DateTime endDate) {
	this.endDate = endDate;
    }

    private Collection voucherList;

    private BigDecimal totalAmount;

    private BigDecimal paidAmount;

    private BigDecimal feeAmount;

    private boolean unpaid;

    public boolean isUnpaid() {
	return unpaid;
    }

    public void setUnpaid(boolean unpaid) {
	this.unpaid = unpaid;
    }

    /**
     * @return
     */
    public String execute() {
	Cheque invoice = null;
	FinanceVoucher voucher = null;
	totalAmount = new BigDecimal(0);
	paidAmount = new BigDecimal(0);
	feeAmount = new BigDecimal(0);
	User user = null;
	String userName = null;
	String salesBranch = null;
	try {
	    voucherList = financeMgr.getVouchersByProcessDate(startDate, endDate, FinanceVoucher.STATUS_APPROVED);
	    for (Iterator<FinanceVoucher> i = voucherList.iterator(); i.hasNext();) {
		voucher = i.next();
		if (!voucher.isFinanceSystem()) {
		    i.remove();
		    continue;
		}
		totalAmount = totalAmount.add(voucher.getDisposalAmount());
		feeAmount = feeAmount.add(voucher.getFeeAmount());
		invoice = financeMgr.getCheque(voucher.getVoucherId());
		// setting the payment details on the voucher reference
		voucher.setPaidAmount(invoice.getChequeAmount());
		voucher.setPaymentDate(invoice.getChequeDate());
		voucher.setPaymentReference(invoice.getChequeNumber());
		user = userMgr.getUser(voucher.getUserId());
		voucher.setUserName(user.getUsername());
		voucher.setSalesBranch(user.getSalesBranch().getSalesBranchName());

		if (voucher.getPaidAmount() != null) {
		    paidAmount = paidAmount.add(voucher.getPaidAmount());
		}

		if (unpaid && voucher.getDisposalAmount().subtract(voucher.getPaidAmount()).compareTo(new BigDecimal(0)) == 0) // paid
		{
		    i.remove();
		    totalAmount = totalAmount.subtract(voucher.getDisposalAmount());
		    feeAmount = feeAmount.subtract(voucher.getFeeAmount());
		    paidAmount = paidAmount.subtract(voucher.getPaidAmount());
		    continue;
		}
	    }
	} catch (RemoteException e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	// iterate through vouchers and get payment details!

	return SUCCESS;
    }

    public BigDecimal getFeeAmount() {
	return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
	this.feeAmount = feeAmount;
    }

    public BigDecimal getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
	this.totalAmount = totalAmount;
    }

    public BigDecimal getPaidAmount() {
	return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
	this.paidAmount = paidAmount;
    }

    public Collection getVoucherList() {
	return voucherList;
    }

    public void setVoucherList(Collection voucherList) {
	this.voucherList = voucherList;
    }
}