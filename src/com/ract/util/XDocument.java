package com.ract.util;

import java.io.StringWriter;

import org.apache.util.DOMWriter;
import org.w3c.dom.Node;

/**
 * <p>
 * Title: XDocument
 * </p>
 * <p>
 * Description: Extends org.apache.crimson.tree.XmlDocument with wrapped methods
 * for adding element nodes adding leaf nodes. Enables ready construction of an
 * XML document from any arbitrary data.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2006
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author dgk 01/09/06
 * @version 1.0
 */
public class XDocument extends org.apache.xerces.dom.DocumentImpl {

    public static String getXMLHeaderLine() {
	return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    }

    public static String getXSLTagLine(String href) {
	return "<?xml-stylesheet href=\"" + href + "\" type=\"text/xsl\"?>";
    }

    public void addLeaf(Node parent, String leafName, String leafContent, boolean preserveContents) {
	LogUtil.debug(this.getClass(), "leafName=" + leafName);
	LogUtil.debug(this.getClass(), "leafContent=" + leafContent);
	LogUtil.debug(this.getClass(), "preserveContents=" + preserveContents);
	Node el = (Node) this.createElement(leafName);
	if (preserveContents) {
	    el.appendChild(this.createTextNode(leafContent));
	} else {
	    el.appendChild(this.createCDATASection(leafContent));
	}
	parent.appendChild(el);
    }

    public void addLeaf(Node parent, String leafName, String leafContent) {
	addLeaf(parent, leafName, leafContent, false);
    }

    public void addLeaf(Node parent, String leafName, int leafContent) {
	addLeaf(parent, leafName, new Integer(leafContent).toString(), false);
    }

    public void addLeaf(Node parent, String leafName, long leafContent) {
	addLeaf(parent, leafName, new Long(leafContent).toString(), false);
    }

    public void addLeaf(Node parent, String leafName, boolean leafContent) {
	addLeaf(parent, leafName, new Boolean(leafContent).toString(), false);
    }

    public Node addNode(Node parent, String nodeName) {
	Node nn = this.createElement(nodeName);
	parent.appendChild(nn);
	return nn;
    }

    public static String toXMLString(Node node) {
	StringWriter sw = new StringWriter();
	DOMWriter dw = new DOMWriter(sw, true);
	dw.print(node);
	String xmlString = sw.toString();
	return xmlString;
    }

    public String toXMLString() {
	StringWriter sw = new StringWriter();
	DOMWriter dw = new DOMWriter(sw, true);
	dw.print(this.getFirstChild());
	return getXMLHeaderLine() + sw.toString();
    }

    public Node getPrintNode(String dataType, String printGroup, String imageDir) {
	Node n = this.createElement("Integration");
	this.appendChild(n);
	Node branch = null;
	Node item = addNode(n, "Item");
	branch = addNode(item, "Control");
	addLeaf(branch, "DataType", dataType);
	addLeaf(branch, "Action", "Print");
	addLeaf(branch, "ClassName", "com.ract.common.reporting.PrintIntegrator");
	branch = addNode(item, "Data");
	branch = addNode(branch, "Print");
	addLeaf(branch, "printGroup", printGroup);
	addLeaf(branch, "imageLocation", imageDir);
	return branch;
    }

}
