package com.ract.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Utility class for manipulating currency.
 * 
 * @author John Holliday
 * @created 1 August 2002
 * @version 1.0, 5/6/2002
 */

public class CurrencyUtil {

    private static final DecimalFormat DEFAULT_FORMAT = new DecimalFormat("\u00A4###,###,##0.00");

    public static final int DEFAULT_SCALE = 2;

    /**
     * Return a string representing an absolute dollar value for the provided
     * amount. eg. $23.50 DR
     */
    public static String formatAbsDollarValue(double amount) {
	final String DEBIT = "DR";
	final String CREDIT = "CR";
	final String SPACE = " ";

	String output = DEFAULT_FORMAT.format(Math.abs(amount));
	if (amount < 0) {
	    output += SPACE + CREDIT;
	} else {
	    output += SPACE + DEBIT;
	}
	return output;
    }

    public static String formatAccountingDollarValue(double amount) {
	String output = DEFAULT_FORMAT.format(Math.abs(amount));
	if (amount < 0) {
	    output = "(" + output + ")";
	}
	return output;
    }

    public static String formatDollarValue(BigDecimal amount) {
	if (amount != null) {
	    return formatDollarValue(amount.doubleValue());
	} else {
	    return "";
	}
    }

    /**
     * Return a string representing a dollar value for the provided amount.
     * 
     * @param amount
     *            the double amount
     * @return the string representing the dollar amount eg. 23.3433 becomes
     *         $23.34
     */
    public static String formatDollarValue(double amount) {
	String output = DEFAULT_FORMAT.format(amount);
	return output;
    }

    /**
     * Given a GST inclusive amount, calculate the amount of GST it contains
     * 
     * @param gstInclusiveAmount
     *            Description of the Parameter
     * @param gstRate
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static double calculateGSTincluded(double gstInclusiveAmount, double gstRate) {
	return NumberUtil.roundDouble((gstInclusiveAmount * gstRate) / (1 + gstRate), DEFAULT_SCALE);
    }

    public static BigDecimal calculateGSTincluded(double gstInclusiveAmount, double gstRate, int scale) {
	BigDecimal gstAmount = new BigDecimal(gstInclusiveAmount);
	gstAmount = gstAmount.setScale(scale, BigDecimal.ROUND_HALF_UP);
	gstAmount = gstAmount.multiply(new BigDecimal(gstRate));
	gstAmount = gstAmount.divide(new BigDecimal(1 + gstRate), scale, BigDecimal.ROUND_HALF_UP);
	return gstAmount;
    }

    public static BigDecimal calculateGSTInclusiveAmount(BigDecimal amount, double gstRate) {
	BigDecimal gstInclusiveAmount = null;
	BigDecimal gst = new BigDecimal(gstRate).multiply(amount);
	gstInclusiveAmount = gst.add(amount);
	return gstInclusiveAmount;
    }

    /**
     * Given a GST inclusive amount, calculate the GST exclusive amount
     * 
     * @param gstInclusiveAmount
     *            Description of the Parameter
     * @param gstRate
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static double calculateGSTExclusiveAmount(double gstInclusiveAmount, double gstRate) {
	return gstInclusiveAmount - calculateGSTincluded(gstInclusiveAmount, gstRate);
    }

    public static BigDecimal calculateGSTExclusiveAmount(double gstInclusiveAmount, double gstRate, int scale) {
	BigDecimal gst = calculateGSTincluded(gstInclusiveAmount, gstRate, scale);
	return new BigDecimal(gstInclusiveAmount).subtract(gst);
    }

}
