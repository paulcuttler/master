package com.ract.util;

public class EnvVarUtil {

    public static String replaceEnvironmentVariable(String dirName) {
	String pathDelimiter = "/";
	String urlIndicator = "\\";
	String variableIndicator = "$";

	String pathParts[] = dirName.split(pathDelimiter);
	StringBuffer newDirName = new StringBuffer();

	for (int i = 0; i < pathParts.length; i++) {

	    if (pathParts[i].startsWith(variableIndicator)) {
		pathParts[i] = getEnvironmentVariableValue(pathParts[i].substring(1));
	    }

	    if (!pathParts[i].startsWith(pathDelimiter) && !pathParts[i].startsWith(urlIndicator)) {
		newDirName.append(pathDelimiter);
	    }

	    newDirName.append(pathParts[i]);

	}

	return newDirName.toString();
    }

    private static String getEnvironmentVariableValue(String varName) {
	return getEnvironmentVariable(varName);
    }

    private static String getEnvironmentVariable(String varName) {
	// return em.find(EnvironmentVariable.class,varName);
	String varValue = "";

	try {
	    varValue = FileUtil.getProperty("unix", varName);
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    LogUtil.log("EnvVarUtil", "Can't find value for " + varName + " - " + e.getMessage());
	}

	return varValue;
    }
}
