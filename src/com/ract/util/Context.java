package com.ract.util;

import java.util.UUID;

public class Context {
	
	private String correlationId;
	
	public Context() {
		this.correlationId = UUID.randomUUID().toString();
	}
	
	public Context(String id) {
		this.correlationId = id;
	}

	public String Get() {
		return this.correlationId;
	}
	
}
