package com.ract.util;

public class Milestone {
    private Integer milestoneCount = 0;

    private Integer milestone = 1000;

    private String messageString = null;

    private long startTime = 0;

    private long currentTime = 0;

    private long processTime = 0;

    public Milestone() {
	milestoneCount = 0;
	messageString = "%1$d records processed.";
	startTime = System.currentTimeMillis();
	currentTime = System.currentTimeMillis();
	currentTime = System.currentTimeMillis();
    }

    public String getMilestoneCountMessage() {
	milestoneCount++;

	return ((milestoneCount % milestone) == 0) ? formatMessage(milestoneCount) : "";
    }

    public void setMilestone(Integer milestone) {
	this.milestone = milestone;

    }

    private String formatMessage(Integer count) {
	return String.format(messageString, count);
    }

    public String milestoneEndMessage() {
	return formatMessage(milestoneCount);
    }

    public Integer getMilestoneCount() {
	return milestoneCount;
    }

    public void setMessageString(String messageString) {
	this.messageString = messageString;
    }
}
