package com.ract.util;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

/**
 * <p>
 * Adds additional functionality to the standard java Date class.
 * </p>
 * <p>
 * It is defined as a Hibernate UserType as by default the java.util.Date
 * Hibernate mapping will not bring back timestamp detail.
 * </p>
 * 
 * @author Terry Bakker, John Holliday
 * @version 1.0,26/2/2001
 */

public class DateTime extends java.util.Date implements UserType {

    public final String DATE_MESSAGE = "Error: a date must not be null if it is part of the DateTime constructor.";

    public final int MILLISECONDS = 1000;

    /**************************** constructors ******************************/

    /**
     * Default constructor creates an instance initialised to the current date
     * and time.
     */
    public DateTime() {
	super();
    }

    /**
     * Initialise the date from the specified date.
     */
    public DateTime(long date) {
	super(date);
    }

    /**
     * Initialise the date by parsing the specified date.
     */
    public DateTime(String date) throws ParseException {
	super(DateUtil.parseDate(date).getTime());
    }

    /**
     * Initialise the DateTime with the value from a java.util.Date class
     */
    public DateTime(java.util.Date date) {
	super();
	if (date == null) {
	    throw new IllegalArgumentException(DATE_MESSAGE);
	} else {
	    setDateTime(date.getTime());
	}
    }

    public boolean equals(Object object) {
	// LogUtil.log(this.getClass(), "this="+this.formatLongDate());
	// LogUtil.log(this.getClass(),
	// "object="+((DateTime)object).formatLongDate());
	return super.equals(object);
    }

    public int hashCode() {
	return super.hashCode();
    }

    /**
     * Return the current date time stripped of hours/mins/secs. ie, a date only
     * 
     * @return DateTime A DateTime object having the same date as the current
     *         object.
     */
    public DateTime getDateOnly() {
	return DateUtil.clearTime(this);
    }

    /**************************** functions *******************************/

    /**
     * Return a new date time with the interval added to this date time.
     */
    public DateTime add(Interval interval) {
	GregorianCalendar gc = new GregorianCalendar();
	gc.setTime(this);
	if (interval != null) {
	    gc.add(GregorianCalendar.YEAR, interval.getYears());
	    gc.add(GregorianCalendar.MONTH, interval.getMonths());
	    gc.add(GregorianCalendar.DAY_OF_MONTH, interval.getDays());
	    gc.add(GregorianCalendar.HOUR_OF_DAY, interval.getHours());
	    gc.add(GregorianCalendar.MINUTE, interval.getMinutes());
	    gc.add(GregorianCalendar.SECOND, interval.getSeconds());
	    gc.add(GregorianCalendar.MILLISECOND, interval.getMilliseconds());
	}
	return new DateTime(gc.getTime());

	// long time = this.getTime();
	// if (interval != null)
	// {
	// time += interval.getTotalMilliseconds();
	// }
	// return new DateTime(time);
    }

    /**
     * Based on Australian fiscal year.
     * 
     * @return boolean
     */
    public boolean currentFinancialYear() // 1/1/2008
    {
	boolean currentFinancialYear = false;
	SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

	DateTime now = new DateTime();
	// previous year
	final String START_DAY = "01";
	final String START_MONTH = "07";
	final String END_DAY = "30";
	final String END_MONTH = "06";
	final String DATE_DELIMITER = "/";

	int year = now.getDateYear(); // 2008
	int startYear = 0;
	int endYear = 0;

	DateTime currentFinYearStart = null;
	DateTime currentFinYearEnd = null;
	try {
	    // pivot date
	    DateTime tmp = new DateTime(START_DAY + DATE_DELIMITER + START_MONTH + DATE_DELIMITER + (year));
	    // System.out.println("tmp="+tmp);
	    if (now.onOrAfterDay(tmp)) {
		startYear = now.getDateYear();
		endYear = now.getDateYear() + 1;
	    } else {
		startYear = now.getDateYear() - 1;
		endYear = now.getDateYear();
	    }
	    // System.out.println("startYear="+startYear);
	    // System.out.println("endYear="+endYear);
	    currentFinYearStart = new DateTime(START_DAY + DATE_DELIMITER + START_MONTH + DATE_DELIMITER + startYear);
	    currentFinYearEnd = new DateTime(END_DAY + DATE_DELIMITER + END_MONTH + DATE_DELIMITER + endYear);
	} catch (ParseException ex) {
	    // ignore
	}

	// System.out.println("currentFinYearStart="+this);
	// System.out.println("currentFinYearStart="+currentFinYearStart);
	// System.out.println("currentFinYearEnd="+currentFinYearEnd);

	if (this.onOrAfterDay(currentFinYearStart) && this.onOrBeforeDay(currentFinYearEnd)) {
	    currentFinancialYear = true;
	}
	// System.out.println("currentFinancialYear="+currentFinYearEnd);
	return currentFinancialYear;

    }

    /**
     * Return a new date time with the interval subtracted from this date time.
     */
    public DateTime subtract(Interval interval) {
	GregorianCalendar gc = new GregorianCalendar();
	gc.setTime(this);
	if (interval != null) {
	    gc.add(GregorianCalendar.YEAR, -interval.getYears());
	    gc.add(GregorianCalendar.MONTH, -interval.getMonths());
	    gc.add(GregorianCalendar.DAY_OF_MONTH, -interval.getDays());
	    gc.add(GregorianCalendar.HOUR_OF_DAY, -interval.getHours());
	    gc.add(GregorianCalendar.MINUTE, -interval.getMinutes());
	    gc.add(GregorianCalendar.SECOND, -interval.getSeconds());
	    gc.add(GregorianCalendar.MILLISECOND, -interval.getMilliseconds());
	}
	return new DateTime(gc.getTime());

    }

    /**
     * Return a copy of the date time.
     */
    public DateTime copy() {
	return new DateTime(this.getTime());
    }

    /**
     * toString date format
     */
    public String toString() {
	return this.formatShortDate();
    }

    // Return the number of milliseconds from 12 am midnight.
    public int getMillisecondsFromMidnight() {
	GregorianCalendar gc = new GregorianCalendar();
	gc.setTime(this);
	gc = DateUtil.clearTime(gc);
	return (int) (this.getTime() - gc.getTime().getTime());
    }

    // Return the number of seconds from 12 am midnight. (Progress Time)
    public int getSecondsFromMidnight() {
	return (int) (this.getMillisecondsFromMidnight() / MILLISECONDS);
    }

    private void setDateTime(long dateTime) {
	this.setTime(dateTime);
    }

    public java.sql.Date toSQLDate() {
	// what if this is null?
	return new java.sql.Date(this.getTime());
    }

    public java.sql.Timestamp toSQLTimestamp() {
	// what if this is null?
	return new java.sql.Timestamp(this.getTime());
    }

    public String formatShortDate() {
	return DateUtil.formatShortDate(this);
    }

    // no concept of time (as part of the date) in progress
    public String formatLongDate() {
	return DateUtil.formatLongDate(this);
    }

    // no concept of time (as part of the date) in progress
    public String formatMediumDate() {
	return DateUtil.formatMediumDate(this);
    }

    /**
     * Return true if the passed date if before the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates. Convert the DateTime to a GrgorianCalendar and call the
     * actual implementation.
     */
    public boolean beforeDay(DateTime effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);
	return beforeDay(gcDate);
    }

    /**
     * Return true if the passed date if on or before the instance date
     * otherwise false. Return false if the passed date is null. Ignore the time
     * component of the dates. Convert the DateTime to a GregorianCalendar and
     * call the actual implementation.
     */
    public boolean onOrBeforeDay(DateTime effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);

	if (!beforeDay(gcDate) && !afterDay(gcDate)) {
	    return true;
	} else {
	    return beforeDay(gcDate);
	}
    }

    /**
     * Return true if the passed date if before the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates. Convert the sql.Date to a GrgorianCalendar and call the
     * actual implementation.
     */
    public boolean beforeDay(java.util.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);
	return beforeDay(gcDate);
    }

    /**
     * Return true if the passed date if on or before the instance date
     * otherwise false. Return false if the passed date is null. Ignore the time
     * component of the dates. Convert the DateTime to a GregorianCalendar and
     * call the actual implementation.
     */
    public boolean onOrBeforeDay(java.util.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);

	if (!beforeDay(gcDate) && !afterDay(gcDate)) {
	    return true;
	} else {
	    return beforeDay(gcDate);
	}
    }

    /**
     * Return true if the passed date if before the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates. Convert the sql.Date to a GrgorianCalendar and call the
     * actual implementation.
     */
    public boolean beforeDay(java.sql.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);
	return beforeDay(gcDate);
    }

    /**
     * Return true if the passed date if on or before the instance date
     * otherwise false. Return false if the passed date is null. Ignore the time
     * component of the dates. Convert the DateTime to a GregorianCalendar and
     * call the actual implementation.
     */
    public boolean onOrBeforeDay(java.sql.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);

	if (!beforeDay(gcDate) && !afterDay(gcDate)) {
	    return true;
	} else {
	    return beforeDay(gcDate);
	}
    }

    /**
     * Return true if the passed date if before the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates.
     */
    public boolean beforeDay(GregorianCalendar effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	// Clear the time fields of the passed date
	effectiveDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
	effectiveDate.set(GregorianCalendar.MINUTE, 0);
	effectiveDate.set(GregorianCalendar.SECOND, 0);
	effectiveDate.set(GregorianCalendar.MILLISECOND, 0);

	// Clear the time fields of the instance date
	GregorianCalendar thisDate = new GregorianCalendar();
	thisDate.setTime(this);
	thisDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
	thisDate.set(GregorianCalendar.MINUTE, 0);
	thisDate.set(GregorianCalendar.SECOND, 0);
	thisDate.set(GregorianCalendar.MILLISECOND, 0);

	return thisDate.before(effectiveDate);
    }

    /**
     * Return true if the passed date if after the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates. Convert the DateTime to a GrgorianCalendar and call the
     * actual implementation.
     */
    public boolean afterDay(DateTime effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);
	return afterDay(gcDate);
    }

    /**
     * Return true if the passed date if on or before the instance date
     * otherwise false. Return false if the passed date is null. Ignore the time
     * component of the dates. Convert the DateTime to a GregorianCalendar and
     * call the actual implementation.
     */
    public boolean onOrAfterDay(DateTime effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);

	if (!beforeDay(gcDate) && !afterDay(gcDate)) {
	    return true;
	} else {
	    return afterDay(gcDate);
	}
    }

    /**
     * Return true if the passed date if after the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates. Convert the util.Date to a GrgorianCalendar and call the
     * actual implementation.
     */
    public boolean afterDay(java.util.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);
	return afterDay(gcDate);
    }

    /**
     * Return true if the passed date if on or before the instance date
     * otherwise false. Return false if the passed date is null. Ignore the time
     * component of the dates. Convert the java.util.date to a GregorianCalendar
     * and call the actual implementation.
     */
    public boolean onOrAfterDay(java.util.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);

	if (!beforeDay(gcDate) && !afterDay(gcDate)) {
	    return true;
	} else {
	    return afterDay(gcDate);
	}
    }

    /**
     * Return true if the passed date if after the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates. Convert the sql.Date to a GrgorianCalendar and call the
     * actual implementation.
     */
    public boolean afterDay(java.sql.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);
	return afterDay(gcDate);
    }

    /**
     * Return true if the passed date if on or before the instance date
     * otherwise false. Return false if the passed date is null. Ignore the time
     * component of the dates. Convert the java.sql.Date to a GregorianCalendar
     * and call the actual implementation.
     */
    public boolean onOrAfterDay(java.sql.Date effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}

	GregorianCalendar gcDate = new GregorianCalendar();
	gcDate.setTime(effectiveDate);

	if (!beforeDay(gcDate) && !afterDay(gcDate)) {
	    return true;
	} else {
	    return afterDay(gcDate);
	}
    }

    /**
     * Return true if the passed date is after the instance date otherwise
     * false. Return false if the passed date is null. Ignore the time component
     * of the dates.
     */
    public boolean afterDay(GregorianCalendar effectiveDate) {
	if (effectiveDate == null) {
	    return false;
	}
	// Clear the time fields of the passed date
	effectiveDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
	effectiveDate.set(GregorianCalendar.MINUTE, 0);
	effectiveDate.set(GregorianCalendar.SECOND, 0);
	effectiveDate.set(GregorianCalendar.MILLISECOND, 0);

	// Clear the time fields of the instance date
	GregorianCalendar thisDate = new GregorianCalendar();
	thisDate.setTime(this);
	thisDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
	thisDate.set(GregorianCalendar.MINUTE, 0);
	thisDate.set(GregorianCalendar.SECOND, 0);
	thisDate.set(GregorianCalendar.MILLISECOND, 0);

	return thisDate.after(effectiveDate);
    }

    public int getDayOfMonth() {
	return DateUtil.getDayOfMonth(this);
    }

    public int getMonthOfYear() {
	return DateUtil.getMonthOfYear(this);
    }

    public int getDateYear() {
	return DateUtil.getYear(this);
    }

    public int getDayOfYear() {
	return DateUtil.getDayOfYear(this);
    }

    public int getWeekOfYear() {
	return DateUtil.getWeekOfYear(this);
    }

    public int getWeekOfMonth() {
	return DateUtil.getWeekOfMonth(this);
    }

    // hibernate user type interface methods

    int TYPES[] = { Types.TIMESTAMP };

    public int[] sqlTypes() {
	return TYPES;
    }

    public Integer getYearsBetween(DateTime effectiveDate) {
	if (this.getDateOnly() == null || effectiveDate == null) {
	    return 0;
	} else {
	    return this.getYear() - effectiveDate.getYear();
	}

    }

    public Class returnedClass() {
	return com.ract.util.DateTime.class;
    }

    public int hashCode(Object object) throws HibernateException {
	return object.hashCode();
    }

    public boolean equals(Object x, Object y) throws HibernateException {
	if (x == y) {
	    return true;
	}
	if (x == null) {
	    return false;
	}
	return x.equals(y);
    }

    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException {
	// LogUtil.debug(this.getClass(),"nullSafeGet"+names[0] );
	java.sql.Timestamp dateTime = resultSet.getTimestamp(names[0]);
	// LogUtil.debug(this.getClass(),"nullSafeGet"+names[0]+" "+dateTime );
	if (dateTime != null) {
	    // LogUtil.debug(this.getClass(),"testttt"+new
	    // DateTime(dateTime.getTime()) );
	    return new DateTime(dateTime.getTime());
	} else {
	    return null;
	}
    }

    public void nullSafeSet(PreparedStatement statement, Object value, int index) throws HibernateException, SQLException {
	if (value == null) {
	    statement.setNull(index, Types.TIMESTAMP);
	} else {
	    DateTime dateTime = (DateTime) value;

	    // clear milliseconds

	    statement.setTimestamp(index, dateTime.toSQLTimestamp());
	}
    }

    public Object deepCopy(Object value) throws HibernateException {
	if (value == null) {
	    return null;
	} else {
	    DateTime date = (DateTime) value;
	    return new DateTime(date.getTime());
	}
    }

    public boolean isMutable() {
	return true;
    }

    public Serializable disassemble(Object value) throws HibernateException {
	return (Serializable) value;
    }

    public Object assemble(Serializable cached, Object owner) throws HibernateException {
	return cached;
    }

    public Object replace(Object original, Object target, Object owner) throws HibernateException {
	return deepCopy(original);
    }

}
