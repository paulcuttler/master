package com.ract.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import com.ract.common.PrintForm;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;

/**
 * <p>
 * Set of printing/print utilities.
 * </p>
 * 
 * @author John Holliday
 * @created 31 July 2002
 * @version 1.0
 */

public class PrintUtil {

	/**
	 * <p>
	 * Write a file into a server location for printing.
	 * </p>
	 * <p>
	 * The process for printing is as follows:
	 * </p>
	 * <ol>
	 * <li>Construct a file name consisting of:
	 * <ul>
	 * <li>The server location.</li>
	 * <li>The name of the printer (according to predefined standards).</li>
	 * <li>The source system abbreviation.</li>
	 * <li>The form options consisting of two character tray instructions.</li>
	 * <li>The time of printing.</li>
	 * <li>The file extension.</li>
	 * </ul>
	 * </li>
	 * <li>Write file to server.</li>
	 * <li>Watch Directory on the remote server listens on the directory where the file is written.</li>
	 * <li>The file is converted to pcl with ghostscript 7.04.</li>
	 * <li>The file has the required pcl tray codes inserted into the pcl document via PERL.</li>
	 * <li>The file is printed using the lpr command.</li>
	 * </ol>
	 * 
	 * <p>
	 * This approach results in only a small file being transferred across the network.
	 * </p>
	 * 
	 * @param printForm The printer and form options
	 * @param sourceSystem The source system initiating the process
	 * @param contents The binary contents of the file
	 * @throws SystemException
	 */
	public static void writeFileToServerForPrinting(PrintForm printForm, SourceSystem sourceSystem, byte[] contents) throws SystemException {
		String timeStamp = getUniqueFileTime();

		// file name
		// PRINTERNAME TYPE TRAYS
		// eg. HOBISYBO1 INS X XXXXXX.pdf
		String fileName = printForm.getPrinterOptions() // printer directory
				// path
				+ printForm.getPrinterName() // the name of the printer
				+ sourceSystem.getAbbreviation() // the source system
				// abbreviation
				+ printForm.getFormOptions() // tray in form X
				+ timeStamp // time stamp down to the millisecond
				+ "." + FileUtil.EXTENSION_PDF; // pdf extension
		try {
			FileUtil.writeFile(fileName, contents, true);
		} catch (IOException ex) {
			throw new SystemException("Unable to write file '" + fileName + "' for printing.", ex);
		}

	}

	// public static void writeFileToServerForPrinting(PrintForm printForm,
	// SourceSystem sourceSystem, OutputStream oStream)
	// throws SystemException
	// {
	// String timeStamp = getUniqueFileTime();
	//
	// //file name
	// //PRINTERNAME TYPE TRAYS
	// //eg. HOBISYBO1 INS X XXXXXX.pdf
	// String fileName = printForm.getPrinterOptions() //printer directory path
	// + printForm.getPrinterName() //the name of the printer
	// + sourceSystem.getAbbreviation() //the source system abbreviation
	// + printForm.getFormOptions() //tray in form X
	// + timeStamp //time stamp down to the millisecond
	// + "." + FileUtil.EXTENSION_PDF; //pdf extension
	// try
	// {
	// FileUtil.writeFile(fileName, contents, true);
	// }
	// catch(IOException ex)
	// {
	// throw new
	// SystemException("Unable to write file '"+fileName+"' for printing.",ex);
	// }
	//
	// }

	public static String getFileNameForPrinting(PrintForm printForm, SourceSystem sourceSystem) throws SystemException {
		String timeStamp = getUniqueFileTime();

		// file name
		// PRINTERNAME TYPE TRAYS
		// eg. HOBISYBO1 INS X XXXXXX.pdf
		String fileName = printForm.getPrinterOptions() // printer directory path
				+ printForm.getPrinterName() // the name of the printer
				+ sourceSystem.getAbbreviation() // the source system
				// abbreviation
				+ printForm.getFormOptions() // tray in form X
				+ timeStamp // time stamp down to the millisecond
				+ "." + FileUtil.EXTENSION_PDF; // pdf extension
		LogUtil.debug("getFileNameForPrinting", "fileName=" + fileName);
		System.out.println("Membership advice filename = " + fileName);
		return fileName;
	}

	/**
	 * Get a unique file name using the current time. Synchronize this block so that the name can be guaranteed to be unique.
	 * 
	 * @return String
	 */
	private synchronized static String getUniqueFileTime() {
		DateTime printDateTime = new DateTime();
		String timeStamp = Long.toString(printDateTime.getTime());
		return timeStamp;
	}

	/**
	 * writeToPrinter Streams a string of text directly to a printer
	 * 
	 * @param printerURL String
	 * @param textToPrint String
	 */
	public static void writeToPrinter(String printerURL, String textToPrint) throws FileNotFoundException {
		FileOutputStream os = new FileOutputStream(printerURL);
		// wrap stream in "friendly" PrintStream
		PrintStream ps = new PrintStream(os);
		// print text here
		ps.println(textToPrint);

		// form feed -- this is important
		// Without the form feed, the text
		// will simply sit in the print
		// buffer until something else
		// gets printed.
		ps.print("\f");

		// flush buffer and close
		ps.close();
	}

}
