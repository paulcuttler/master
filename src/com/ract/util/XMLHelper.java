package com.ract.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.util.DOMWriter;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ract.common.ClassWriter;
import com.ract.common.CommonConstants;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.common.XMLClassWriter;

/**
 * Provides static helper methods for formating XML documents.
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class XMLHelper {

    private static DocumentBuilder docBuilder = null;
    public static final String XSL_MEMBERSHIP_QUOTE_VIEW = CommonConstants.DIR_MEMBERSHIP + "/xsl/MembershipQuoteView.xsl";

    public static final String XSL_MEMBERSHIP_DOCUMENT_VIEW = CommonConstants.DIR_MEMBERSHIP + "/xsl/MembershipDocumentView.xsl";

    private static final CharSequence[] cdataCharList = { "&", "<", ">", "!", "'" };

    private static final String cdataStart = "<![CDATA[";
    private static final String cdataEnd = "]]>";

    /**
     * @todo should take a DTD as a parameter
     */
    public static String getXMLHeaderLine() {
	return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + FileUtil.NEW_LINE;
    }

    public static String getXSLTagLine(String href) {
	return "<?xml-stylesheet href=\"" + href + "\" type=\"text/xsl\"?>" + FileUtil.NEW_LINE;
    }

    public static String getXMLEncodingForMidas() {
	return "<?xml version=\"1.0\" encoding=\"windows-1252\" ?>" + FileUtil.NEW_LINE;
    }

    public static String getXMLHeaderForMidas() {
	return "<TABLE xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:od=\"urn:schemas-microsoft-com:officedata\" xsi:noNamespaceSchemaLocation=\"RACT_Import_Schema.xsd\">" + FileUtil.NEW_LINE;
    }

    public static String getXMLFooterForMidas() {
	return "</TABLE>" + FileUtil.NEW_LINE;
    }

    public static String getRACTImportStartTag() {
	return "<RACT.IMPORT>";
    }

    public static String getRACTImportEndTag() {
	return "</RACT.IMPORT>";
    }

    /**
     * Return a value given the following structure:
     * 
     * @param the
     *            list of value to seach through <#text>value</#text>
     */
    public static String returnNodeValue(NodeList values) {
	String value = "";
	for (int x = 0; x < values.getLength(); x++) {
	    Node subNode = values.item(x);
	    if (subNode.getNodeType() == Node.TEXT_NODE) {
		value = subNode.getNodeValue();
	    }
	}
	return value;
    }

    /**
     * Convert characters in the string that give XML problems. Convert "&" to
     * "&amp;"
     */
    public static String encode(String theString) {
	// This is all it does for the moment.
	return replaceAmp(theString);
    }

    /**
     * Convert "&" to "&amp;"
     */
    private static String replaceAmp(String theString) {
	/*
	 * char[] charArray = theString.toCharArray(); StringBuffer resultBuffer
	 * = new StringBuffer(); int stringLen = theString.length(); int
	 * startIndex = 0; int endIndex = 0; for (int i = 0; i < stringLen ;
	 * i++) { if (charArray[i] == '&') {
	 * resultBuffer.append(charArray,startIndex,i - startIndex);
	 * resultBuffer.append("&amp;"); startIndex = i + 1; } } // Append any
	 * remaining characters
	 * resultBuffer.append(charArray,startIndex,stringLen - startIndex);
	 * return resultBuffer.toString();
	 */
	return StringUtil.replaceAll(theString, "&", "&amp;");
    }

    /**
     * Return true if the node has an attribute named 'type' with a value of
     * 'Writable'. This indicates that the node is using the newer style of
     * constructing a Writable object as an attribute of another object rather
     * than the old style.
     */
    public static boolean isWritable(Node node) {
	boolean writable = false;
	NamedNodeMap attributes = node.getAttributes();
	// System.out.println("XMLHelper: " + node.getNodeName() +
	// " attributes are "+(attributes == null ? "":"NOT")+" null");
	if (attributes != null) {

	    // for (int i=0;i<attributes.getLength();i++){
	    // System.out.println("XMLHelper: attribute " + i +
	    // " = "+attributes.item(i).getNodeName());
	    // }

	    Node typeNode = attributes.getNamedItem("type");
	    // System.out.println("XMLHelper: typeNode is "+(typeNode == null ?
	    // "":"NOT")+" null");
	    if (typeNode != null) {
		// System.out.println("XMLHelper: typeNode.name="+typeNode.getNodeName()+", typeNode.value="+typeNode.getNodeValue());
		writable = typeNode.getNodeValue().equals("Writable");
	    }
	}
	// System.out.println("XMLHelper: writable = " + writable);
	return writable;
    }

    /**
     * If the node has the attribute named 'type' with a value of 'Writable'
     * then get the child of the node that is an ELEMENT type. Otherwise return
     * the passed node if the passed node has children. Otherwise return null.
     */
    public static Node getWritableNode(Node node) {
	Node writableNode = null;
	if (node.hasChildNodes()) {
	    if (isWritable(node)) {
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength() && writableNode == null; i++) {
		    if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
			writableNode = nodeList.item(i);
		    }
		}
	    } else {
		writableNode = node;
	    }
	}
	return writableNode;
    }

    public static Hashtable getResultSetAttributes(String xmlDoc) {
	boolean validating = false;
	Hashtable attr = new Hashtable();
	// System.out.println("getting atts");
	try {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setValidating(validating);
	    DocumentBuilder builder = factory.newDocumentBuilder();

	    InputSource inStream = new InputSource();
	    inStream.setCharacterStream(new StringReader(xmlDoc));

	    Document document = builder.parse(inStream);
	    // Processing the document as a DOM tree

	    Node resultSet = document.getFirstChild();
	    System.out.println("resultSet=" + resultSet.getNodeName());

	    NamedNodeMap attributes = resultSet.getAttributes();

	    String attributeName = null;
	    Integer attributeValue = null;
	    if (attributes != null) {
		for (int x = 0; x < attributes.getLength(); x++) {
		    Node node = attributes.item(x);
		    attributeName = node.getNodeName();
		    attributeValue = new Integer(node.getNodeValue());

		    System.out.println("attributeName " + attributeName);
		    System.out.println("attributeValue " + attributeValue);
		    attr.put(attributeName, attributeValue);
		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println("Error " + e.toString());
	}
	return attr;
    }

    /**
     * Generate XML string from a class.
     * 
     * @param excludedClassList
     *            ArrayList
     * @param writable
     *            Writable
     * @return String
     */
    public static String toXML(ArrayList excludedClassList, Writable writable) throws SystemException {
	StringWriter writer = null;
	try {
	    writer = new StringWriter();
	    XMLClassWriter xmlClassWriter = new XMLClassWriter(writer, null, excludedClassList);
	    ClassWriter classWriter = (ClassWriter) xmlClassWriter;
	    writable.write(classWriter);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException("Unable to write XML.", ex);
	    // LogUtil.warn("XMLHelper","Unable to write XML."+ex.getMessage());
	}
	return writer.toString();
    }

    public static String toXML(Node node) {
	StringWriter sw = new StringWriter();
	DOMWriter dw = new DOMWriter(sw, true);
	dw.print(node);
	String xmlString = sw.toString();
	return xmlString;
    }

    public static String cDATA(String xmlValue) {

	for (CharSequence cs : cdataCharList) {
	    if (xmlValue.contains(cs)) {
		xmlValue = cdataStart + xmlValue.trim() + cdataEnd;
		break;
	    }
	}

	return xmlValue;
    }

    public static String xmlElement(String elementName, String elementValue) {
	return "<" + elementName.trim() + ">" + cDATA(StringUtil.convertNull(elementValue)) + "</" + elementName.trim() + ">";
    }

    public static String xmlElement(String elementName, String elementValue, int elementLength) {
	if (elementValue.length() > elementLength) {
	    elementValue = elementValue.substring(0, elementLength);
	}
	String tempVal = cDATA(StringUtil.convertNull(elementValue));

	return "<" + elementName.trim() + ">" + tempVal + "</" + elementName.trim() + ">";
    }

    public static String optionalXMLElement(String elementName, String elementValue) {
	if (StringUtil.isBlank(elementValue) || StringUtil.isNull(elementValue)) {
	    return "";
	} else {
	    return xmlElement(elementName, elementValue);
	}

    }

    public static String optionalXMLElement(String elementName, String elementValue, int elementLength) {
	if (StringUtil.isBlank(elementValue) || StringUtil.isNull(elementValue)) {
	    return "";
	} else {
	    return xmlElement(elementName, elementValue, elementLength);
	}

    }
}
