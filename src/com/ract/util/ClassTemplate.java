/*************************************************
 * Java Coding Standards by example
 **************************************************
 * Draft release 1.0
 **************************************************
 * Copyright RACT 2001
 *************************************************/

//package current class belongs to
//eg. com.ract.util

package com.ract.util;

//import specific class(es) required if possible. For large packages importing * is acceptable.

/**
 * Short description of the functionality of the class:
 * 
 * eg. Template for Java class design for RACT.
 * 
 * <b>Note: this class is not intended for use in any application</b>
 * 
 * Supplementary information including:
 * 
 * @author John Holliday
 * @version 1.0, 12/12/1999
 */

// the following javadoc is used as an example of how to javadoc classes,
// methods and attributes.

// implicit extension of Object. Do not include "extends Object".
public class ClassTemplate {

    // attribute comments are a description of what the attribute is.

    /**
     * The id of the class. Declared as private and manipulated and retrieved
     * via get and set methods.
     */
    private int classTemplateId;

    /**
     * The name of the class template.
     */
    private static final String classTemplateName = "Template1";

    // methods have a return value or void. Methods may also have parameters and
    // throw
    //

    /**
     * Retrieves a class template id.
     * 
     * @return int numerical id of the template.
     * @throws Exception
     *             generic exception.
     */
    public int getClassTemplateId() throws Exception {
	return classTemplateId;
    }

    /**
     * Retrieves the class template name.
     * 
     * @return String name of the template class.
     * @throws Exception
     *             generic exception.
     */
    public String getClassTemplateName() throws Exception {

	return classTemplateName;
    }

    /**
     * Sets the value of classTemplateId with a value.
     * 
     * @param x
     *            int value.
     */
    public void setClassTemplateId(int x) {
	classTemplateId = x;
    }

    /**
     * Constructor for creating a new template.
     */
    public void ClassTemplate() {
	classTemplateId = 0;
    }

    /**
     * Test harness to test ClassTemplate class. If the main method will not be
     * referenced then include a test harness to test the class.
     * 
     * @param args
     *            array of arguments to the method.
     */
    public static void main(String[] args) {
	System.out.println("Test harness");
	ClassTemplate cl = new ClassTemplate();
	cl.setClassTemplateId(3);
	try {
	    // getClassTemplateId and getClassTemplateName throw Exception.
	    System.out.println("Class id = " + cl.getClassTemplateId());
	    System.out.println("Class name = " + cl.getClassTemplateName());
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}
    }

}
