package com.ract.util;

//import java.util.regex.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.ejb.FinderException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.jboss.util.xml.DOMWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.archive.ArchiveEJBHelper;
import com.ract.archive.ArchiveMgr;
import com.ract.archive.ArchivedClient;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.Company;
import com.ract.common.ExceptionHelper;
import com.ract.common.PrintForm;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.ValidationException;
import com.ract.help.HelpMgr;
import com.ract.help.HelpPage;
import com.ract.membership.MembershipCardVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipGroupDetailVO;
import com.ract.membership.MembershipGroupVO;
import com.ract.membership.MembershipHistory;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipRenewalMgr;
import com.ract.membership.MembershipTransactionMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.RenewalMgr;
import com.ract.membership.TransactionGroup;
import com.ract.membership.TransactionMgr;
import com.ract.payment.AmountOutstanding;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.billpay.IVRPayment;
import com.ract.payment.bpay.BPAYPayment;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.finance.FinanceException;
import com.ract.payment.finance.FinanceMgr;
import com.ract.payment.finance.Journal;
import com.ract.security.SecurityHelper;
import com.ract.travel.TramadaClientTransaction;
import com.ract.user.User;

/**
 * Test harness for Util classes
 * 
 * @author John Holliday
 * @version 1.0,2/2/02
 */

public class TestUtil {

    // test harness
    public static void main(String[] args) {

	DateTime startTime = new DateTime();
	try {
	    // jndi.properties
	    org.apache.log4j.BasicConfigurator.configure();

	    // check cache

	    // MembershipVO mem2 = membershipMgr.getMembership(memId);
	    // System.out.println(MethodCounter.outputCounts());

	    // DateTime effectiveDate = new DateTime("01/04/2008");
	    // membershipMgr.produceGMVoucherLetters(effectiveDate);

	    // membershipMgr.undoAccessMembership(new DateTime(),false,new
	    // Integer(212998));
	    // membershipMgr.createAccessMembership(new Integer(478326),null,new
	    // DateTime());

	    // RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	    // renewalMgr.processAutomaticRenewals(new
	    // DateTime("28/02/2007"),new DateTime("02/03/2007"));

	    // PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    // Hashtable journals = paymentMgr.getJournals(null,new
	    // DateTime("01/05/2007"),"1340");
	    //
	    // String key = null;
	    // Journal journal = null;
	    // for (Iterator i = journals.keySet().iterator();i.hasNext();)
	    // {
	    // key = (String)i.next();
	    // journal = (Journal)journals.get(key);
	    //
	    // System.out.println(key+" "+journal);
	    // }

	    // PayableItemVO payableItem = paymentMgr.getPayableItem(new
	    // Integer(103781));

	    // fixAccessMemberAddresses("/temp/accessmember20070424.CSV");

	    // ExtractFormatter ef = new
	    // ExtractFormatter(ExtractFormatter.FORMAT_CLIENT_FUTURE_MEDIUM_EMAIL,"Itchy Feet");
	    // System.out.println(ef.toString());
	    // String[] test = {ef.toString(),",","12"};
	    // XMLToCSV x = new XMLToCSV(test);
	    // System.out.println(x.getCSV());
	    //
	    // clientMgr.get

	    //
	    // MembershipVO mem =
	    // (MembershipVO)membershipMgr.findMembershipByClientNumber(new
	    // Integer(430741)).iterator().next();
	    // ArrayList memList = new ArrayList();
	    // memList.add(mem);
	    //
	    // RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	    // renewalMgr.constructSMSRenewalReminders(memList);

	    // removeCardRequestTransactions("/temp/cardreqremove.csv");

	    // with new equals/hashcode
	    // equals 1 ? true
	    // equals 2 ? true
	    // equals 2 ? false
	    // equals 3 ? true
	    // equals 3 ? false
	    // equals 3 ? 1509442 true
	    // equals 4 ? false

	    // without
	    // equals 1 ? false
	    // equals 2 ? false
	    // equals 2 ? false
	    // equals 3 ? true
	    // equals 3 ? false
	    // equals 3 ? 1509442 false
	    // equals 4 ? false

	    // fixMembershipGroup(new Integer(1169727));
	    // fixMembershipStateFromHistory("/temp/mem_exp20060220.CSV");

	    // final int limit = -1;
	    // DateTime effectiveDate = new DateTime().getDateOnly();

	    // membershipMgr.undoAccessMemberships(effectiveDate,false);

	    // ArrayList eligibleClientList =
	    // membershipMgr.getEligibleAccessMembershipClients(effectiveDate,
	    // limit);

	    // membershipMgr.createEligibleAccessMembershipClients(effectiveDate,
	    // true, limit);

	    // ClientVO client = ClientEJBHelper.getClientMgr().getClient(new
	    // Integer(22611));

	    // String inputFileName = "/temp/AccessCardFile.txt";
	    // String outputFileName = "/temp/accesscard20060714.txt";
	    // convertCardFileForPresortCodes(inputFileName,outputFileName);
	    // Collection transList = memMgr.getTransactionList(new Integer());
	    // convertCardFile();

	    // String inputFile1 = "/temp/AccessCardFile.txt";
	    // String inputFile2 = "/temp/accesscard20060714_Sort.txt";
	    // String outputFile = "/temp/accesscard20060717_sorted.txt";
	    // sequenceCardFile(inputFile1,inputFile2,outputFile);
	    // undoAccessMembershipList();

	    // String region = "South";
	    // String category = null;
	    // String type = null;
	    // String payeeName = null;
	    // String payeeClass = null;
	    // String ABN = null;
	    // String sortBy = null;
	    // int elementSize = 300;
	    // int pageSize = 25;
	    // int startPage = 1;
	    // System.out.println(InsuranceUIHelper.getPayeeNavigationBar(startPage,pageSize,elementSize,1,12,
	    // payeeName,ABN,region,payeeClass,category,type,sortBy));

	    // Integer clientNumber = new Integer(0);

	    // ClientVO client = new ClientVO();
	    // client.setClientNumber(new Integer(430741));
	    // client.setInitials("A J & P F");
	    // client.setSurname("HOLLIDAY");
	    // System.out.println(TramadaClientHelper.getTramadaProfileCode(client));
	    //
	    // client = new ClientVO();
	    // client.setClientNumber(new Integer(41));
	    // client.setInitials("A J K L & P F");
	    // client.setSurname("HOLLIDAYESKIAVICICALENSKI");
	    // System.out.println(TramadaClientHelper.getTramadaProfileCode(client));
	    //
	    // client = new ClientVO();
	    // client.setClientNumber(new Integer(415224));
	    // client.setInitials("");
	    // client.setSurname("GLENORCHY CITY COUNCIL COUNCIL COUCIL");
	    // System.out.println(TramadaClientHelper.getTramadaProfileCode(client));
	    //
	    // client = new ClientVO();
	    // client.setClientNumber(new Integer(125448));
	    // client.setInitials("J");
	    // client.setSurname("SMITH");
	    // System.out.println(TramadaClientHelper.getTramadaProfileCode(client));

	    // DateTime loadDate = new DateTime("31/10/2006");

	    // CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    // com.ract.util.test.TestDateTimeCombined td = new
	    // com.ract.util.test.TestDateTimeCombined();
	    // td.setTheDate(startTime);
	    // td.setTheKey(String.valueOf(startTime.getTime()));
	    // commonMgr.testDateTimeCombined(td);
	    // StreetSuburbVO ss = commonMgr.getStreetSuburb(new Integer(1111));
	    // ClientMgr cMgr = ClientEJBHelper.getClientMgr();
	    // ClientVO client = cMgr.getClient(clientNumber);
	    // client.setGivenNames("x");
	    // System.out.println("type = "+client.getClass().getName()+"  "+client.getClientType());

	    // File f = new File("/ract/prd/tmp/tram20061129.csv");
	    // System.out.println("ftest = "+commonMgr.testDateTimeCombined("1164695612870").getTheDate().formatLongDate()+" was 28/11/2006 17:33:32.870");
	    // System.out.println("ss = "+startTime.formatLongDate());
	    //
	    // DateTime loadDate = new DateTime("14/12/2006");
	    // cMgr.loadTramadaClientTransactions(f,loadDate);

	    // explicity add a client to tramada
	    // cMgr.transferTramadaClientXML(Client.TRAMADA_MODE_EXPLICIT,TramadaClientProfileImporter.TRAMADA_ACTION_DELETE,"jyh","hob",client);
	    // cMgr.deleteTramadaClientTransactions(clientNumber);
	    // removeTramadaGroupClients("/temp/grp_tramada20060202.CSV");
	    // convert the RACT client to Tramada format in XML using Progress
	    // implementation
	    // ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
	    // String xml =
	    // clientAdapter.getTramadaClientXML(TramadaClientProfileImporter.TRAMADA_ACTION_UPDATE,
	    // client);
	    // System.out.println(xml);

	    // String xml =
	    // "<client><agency>RACV</agency><employeeno>33</employeeno></client>";

	    // TramadaClientProfileImporter tcpi = new
	    // TramadaClientProfileImporter();
	    // tcpi.sendTramadaClientXML(xml);

	    // String xml =
	    // "<client><action>A</action><agency>dataconv</agency><company_auth>tuesday1234</company_auth>"
	    // +
	    // "<travellertype>T</travellertype><employeeno>8819</employeeno><password>test</password><branch>HOB</branch>"
	    // +
	    // "<companyid>RACT</companyid><surname>Kely</surname><firstname>Ian</firstname><title>Mr</title>"
	    // +
	    // "<emailaddress>j.j@j.com</emailaddress></client>";

	    // TramadaClientProfileImport tcpi=new TramadaClientProfileImport();
	    // tcpi.sendTramadaClientXML(xml);

	    // ClientMgr cMgr = ClientEJBHelper.getClientMgr();
	    // ClientVO client = cMgr.getClient(clientNumber);
	    // cMgr.transferTramadaClientXML(client);

	    // System.out.println("memVO="+memVO);
	    // System.out.println("memGroupVO="+memVO.getMembershipGroup());

	    // release 12 - done as at 20060116
	    // memMgr.updateMembershipQuotesWithUserDetails();

	    // notifyMembershipPayment();
	    // final String FILENAME = "/temp/transactionsfix.txt";
	    // fixMembershipHistoryWithGroupErrors(FILENAME);

	    // ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    // clientMgr.updateDeceasedClientStatus();

	    // DateTime effectiveDate = new DateTime("01/06/2007");

	    // SourceSystem sourceSystem = SourceSystem.FIN;
	    // Integer clientNumber = new Integer(429947);
	    // String paymentMethodId = "726443";
	    // double amount = 238.00;
	    // ReceiptingIdentifier ri = new ReceiptingIdentifier(clientNumber,
	    // paymentMethodId);
	    // PaymentTransactionMgr paymentTransactionMgr =
	    // PaymentEJBHelper.getPaymentTransactionMgr();
	    // paymentTransactionMgr.removeDirectDebitSchedule(393467);
	    // paymentTransactionMgr.commit();
	    // paymentTransactionMgr.updatePayableAmount(sourceSystem,
	    // clientNumber, paymentMethodId, amount);
	    // paymentTransactionMgr.commit();

	    // paymentTransactionMgr.createPayableFee(clientNumber,paymentMethodId,ProductVO.PRODUCT_ULTIMATE,new
	    // Double(amount),paymentMethodId,SourceSystem.MEMBERSHIP.getAbbreviation(),"HML","hml",null);
	    // paymentTransactionMgr.commit();

	    // Payable payable =
	    // paymentMgr.getPayable(clientNumber,paymentMethodId, null,
	    // sourceSystem);
	    // System.out.println("payable="+payable);

	    // paymentTransactionMgr.removePayableFee(clientNumber,
	    // paymentMethodId, SourceSystem.FIN, "fin");
	    // paymentTransactionMgr.commit();

	    // FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	    // financeMgr.processDebtorInvoiceFiles();

	    // financeMgr.transferDebtorInvoiceReceipts();
	    // financeMgr.processDebtorMasterFiles();

	    // DateTime targetDate = new DateTime("09/12/2007");
	    // financeMgr.transferJournals(SourceSystem.MEMBERSHIP,false,targetDate,null);
	    // Vector journals =
	    // financeMgr.createJournals(SourceSystem.MEMBERSHIP,targetDate,null);
	    // System.out.println("isBalanced="+FinanceHelper.isBalanced(journals));

	    // ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    // clientMgr.deleteObsoleteClientsAndBusiness("/temp/testlist1000.txt");

	    // String file = "/temp/unsubscribed-mar08.csv";
	    // SubscriptionsMgr subMgr =
	    // SubscriptionsEJBHelper.getSubscriptionsMgr();
	    // subMgr.processUnsubscribeFile(file,"13/02/2008");

	    // String file = "/temp/clientpurge20080413.csv";
	    // Integer clientNumber = new Integer(507351);
	    // ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    // clientMgr.deleteClientAndBusiness(clientNumber);

	    // String x =
	    // "002|0264175|Renewal Unfinancial Group Member Advice|11 RENNIE ST||WEST HOBART|TAS|7000|MRS|DJ|DENISE|BOWER|ACTIVE|ROADSIDE ADVANTAGE|Personal|22/07/2007||||||||||";
	    // String y = "003|0264175|0264175|||||||||||||";
	    // String z =
	    // "002|0309386|Renewal Final Notice|Flat 8|28 WATERWORKS RD|DYNNYRNE|TAS|7005|MR|C|CRAIG|BROOKES|ACTIVE|ROADSIDE ULTIMATE|Personal|11/08/2007|11/08/2008|$130.00||$0.00|$0.00|$130.00|$11.82|90009152142||FALSE||||";
	    // String w =
	    // "003|0309379|0309379|MR DENIS MCCARTHY|ACTIVE||||||1|Member since 1994. ||||";
	    //

	    // final String cardNumber = "3084070100000096";
	    //
	    // ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    // ClientVO client =
	    // clientMgr.getClientByMembershipCardNumber(cardNumber);
	    // System.out.println("client="+client);

	    // final String fromCustomerNumber = "3";
	    // final String toCustomerNumber = "430742";
	    // final String userId = "jyh";
	    //
	    // PaymentTransactionMgr paymentTxMgr =
	    // PaymentEJBHelper.getPaymentTransactionMgr();
	    // paymentTxMgr.transferReceptorReceipts(fromCustomerNumber,
	    // toCustomerNumber, userId);
	    // paymentTxMgr.deleteReceptorCustomer(fromCustomerNumber, userId);
	    // paymentTxMgr.commit();

	    // System.out.println("tr");
	    // OCVTransactionRequest tr = new OCVTransactionRequest();
	    // TimeStamp: 29/04/2011 9:28:07 AM
	    // Txn Type: Purchase [1]
	    // Client ID: ractINS
	    // Txn Reference: 0755164006
	    // Polled Mode: No
	    // Account No: 2
	    // Card Data: 5353161304599675
	    // Expiry: 0911
	    // Amount: 000000009085
	    // Client Type: Recurring [4]
	    // Auth Number:
	    // CVV:
	    // Net Indicator: [NOT PRESENT]

	    // tr.setTransactionType(OCVTransactionRequest.TRANSACTION_TYPE_PURCHASE);
	    // tr.setClientId("ractINS");
	    // tr.setTransactionReference("0755164006");
	    // tr.setTransactionReference("755164006");
	    // tr.setAccountNumber("2");
	    // tr.setCardData("5353161304599675");
	    // tr.setCardData("5353161304599675");
	    // tr.setCardExpiryDate("0911");
	    // tr.setTotalAmount(new BigDecimal("80"));
	    // tr.setClientType("4");
	    // tr.setCvv("065");

	    // tr.setTransactionType(OCVTransactionRequest.TRANSACTION_TYPE_PURCHASE);
	    // tr.setClientId("jyh");
	    // tr.setTransactionReference("123312339945");
	    // tr.setAccountNumber("1");
	    // tr.setCardData("4506060100798151");
	    // tr.setCardExpiryDate("0312");
	    // tr.setTotalAmount(new BigDecimal("155.22"));
	    // tr.setClientType("0");
	    // tr.setCvv("065");
	    // System.out.println(tr.toString());
	    // OCVTransactionMgr ocvTransactionMgr =
	    // PaymentEJBHelper.getOCVTransactionMgr();
	    // OCVTransactionResponse response =
	    // ocvTransactionMgr.createPurchase(tr);
	    // System.out.println("response="+response);

	    // Thread.currentThread().sleep(5000);

	    // PaymentTransactionMgr paymentTxMgr =
	    // PaymentEJBHelper.getPaymentTransactionMgr();
	    // paymentTxMgr.receiptOnlinePayment(SourceSystem.MEMBERSHIP, 102.0,
	    // "4502263225633652", new DateTime(), "01/11",
	    // "4063", "430741", "MWEB","WEB","WEB","HOBISY01", "VISA",
	    // "John Smith","");
	    // paymentTxMgr.rollback();

	    // PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    // Receipt receipt = null;
	    // try
	    // {
	    // receipt = paymentMgr.getReceipt(new Integer(469333), "4201",
	    // SourceSystem.MEMBERSHIP);
	    // LogUtil.log("TestUtil","receipt="+receipt);
	    // }
	    // catch (Exception e) {e.printStackTrace();}
	    // try
	    // {
	    // receipt = paymentMgr.getReceipt(new Integer(408788), "8" ,
	    // SourceSystem.INSURANCE);
	    // LogUtil.log("TestUtil","receipt="+receipt);
	    // }
	    // catch (Exception e) {e.printStackTrace();}
	    // try
	    // {
	    // receipt = paymentMgr.getReceipt(new Integer(430741), "25",
	    // SourceSystem.INSURANCE);
	    // LogUtil.log("TestUtil","receipt="+receipt);
	    // }
	    // catch (Exception e) {e.printStackTrace();}
	    // try
	    // {
	    // receipt = paymentMgr.getReceipt(new Integer(516756), "1617",
	    // SourceSystem.MEMBERSHIP);
	    // LogUtil.log("TestUtil","receipt="+receipt);
	    // }
	    // catch (Exception e) {e.printStackTrace();}
	    //
	    // try
	    // {
	    // //430741-A001237
	    // receipt = paymentMgr.getReceipt(new Integer(430741), "A001237" ,
	    // SourceSystem.INSURANCE);
	    // LogUtil.log("TestUtil","receipt="+receipt);
	    // }
	    // catch (Exception e) {e.printStackTrace();}

	    // PaymentTransactionMgr paymentTxMgr =
	    // PaymentEJBHelper.getPaymentTransactionMgr();
	    // paymentTxMgr.createPayableFee(493055, "727476", "ULTIMATE",
	    // 5135.44, "", "MEM", "HOB", "jyh", null);
	    // paymentTxMgr.commit();

	    // RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	    // renewalMgr.emailRenewal(new
	    // Integer(305452),"j.holliday@ract.com.au");
	    // renewalMgr.emailRenewal(new
	    // Integer(264165),"j.holliday@ract.com.au");
	    // renewalMgr.emailRenewal(new
	    // Integer(214939),"j.holliday@ract.com.au");

	    // renewalMgr.emailRenewal(new
	    // Integer(217674),"j.holliday@ract.com.au");
	    // renewalMgr.emailRenewal(new
	    // Integer(83015),"j.holliday@ract.com.au");
	    // renewalMgr.emailRenewal(new
	    // Integer(70503),"j.holliday@ract.com.au");

	    testSocketSend();

	} catch (Exception e) {
	    System.err.println(ExceptionHelper.getExceptionStackTrace(e));
	}

	DateTime endTime = new DateTime();

	long longTime = (endTime.getTime() - startTime.getTime());

	System.out.println("Completed in " + longTime + " ms (" + (longTime / 1000) + " secs) @ " + endTime.formatLongDate());

    }

    private static void convertProgressTime() throws ParseException {
	final long x = 45961; // seconds
	DateTime d = new DateTime("2/01/2008");
	long y = d.getTime() + (x * 1000);
	SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	System.out.println("y=" + sd.format(new DateTime(y)));
    }

    private static void removeOrphanedPayables() throws Exception {
	final String inputFile = "/temp/orph_pending20080507.csv";

	CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));
	String[] line = null;

	String clientNumber = null;
	String paymentMethodId = null;
	final SourceSystem sourceSystem = SourceSystem.MEMBERSHIP;
	final String userId = "jyh";
	int counter = 0;

	PaymentTransactionMgr paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();

	while ((line = reader.readNext()) != null) {
	    counter++;
	    try {
		clientNumber = line[0];
		paymentMethodId = line[1];
		paymentTransactionMgr.removePayableFee(new Integer(clientNumber), paymentMethodId, sourceSystem, userId);
		paymentTransactionMgr.commit();

	    } catch (Exception ex) {
		System.err.println("clientNumber=" + clientNumber + ": " + ExceptionHelper.getExceptionStackTrace(ex));
		paymentTransactionMgr.rollback();
	    }
	}
	System.out.println("counter=" + counter);
    }

    private static void spamProblem() throws Exception {
	final String INPUT_FILE = "/temp/dnsbl.txt";
	final String PATH = "/temp/spam/";

	final String[] DEVONPORT = { "f.raymond-crawn", "j.cutts", "j.roberts", "r.bassett", "r.sims", "traveldevonport" };
	final String[] BURNIE = { "a.dowling", "k.kelly", "m.wade", "travelburnie" };
	final String[] LAUNCESTON = { "a.harrison", "c.stokes", "m.matanle", "r.ravani", "r.keith", "travellaunceston" };
	final String[] ULVERSTONE = { "b.catlin", "c.fielding", "k.smith", "k.endelmanis", "r.sinfield", "s.lyons", "travelulverstone" };
	final String[] KINGSTON = { "c.benbow", "j.spencer", "n.marchioli", "travelkingston" };
	final String[] ROSNY = { "h.coleman", "k.morgan", "p.higgins", "s.lyeontyeva", "travelrosny" };
	final String[] GLENORCHY = { "a.storer", "j.fulton", "travelglenorchy" };
	final String[] HOBART = { "a.cerutty", "e.vailas", "e.roberts", "j.patterson", "j.mason", "l.penfold", "r.evans", "t.giffard", "travelhobart" };
	final String[] COLLINSST = { "b.may", "e.lorkin", "l.horne", "m.pigden", "r.jacker", "t.graham", "travelhobartcollins" };

	Hashtable ht = new Hashtable();
	ht.put("Devonport", DEVONPORT);
	ht.put("Burnie", BURNIE);
	ht.put("Launceston", LAUNCESTON);
	ht.put("Ulverstone", ULVERSTONE);
	ht.put("Kingston", KINGSTON);
	ht.put("Rosny", ROSNY);
	ht.put("Glenorchy", GLENORCHY);
	ht.put("Hobart", HOBART);
	ht.put("Hobart_Collins_St", COLLINSST);

	int count = 0;
	String branch = null;
	String[] line = null;
	String[] userList = null;
	String user = null;
	FileWriter fw = null;
	// 05/08/08 14:11:38
	SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yy HH:mm:SS");
	SimpleDateFormat display = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
	DateTime date = null;
	// for each branch
	Enumeration en = ht.keys();
	while (en.hasMoreElements()) {
	    // branch
	    branch = (String) en.nextElement();
	    // System.out.println("branch="+branch);
	    userList = (String[]) ht.get(branch);
	    for (int i = 0; i < userList.length; i++) {
		count++;

		// user
		user = userList[i];
		// System.out.println("user="+user);

		// new file per user/branch combination
		fw = new FileWriter(PATH + branch + "_" + user + ".csv");
		fw.write("Date,From,To,Subject" + FileUtil.NEW_LINE);

		CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(INPUT_FILE)));
		while ((line = reader.readNext()) != null) {
		    // System.out.println(line[3]+" "+line[5].equals("Deleted"));
		    String email = line[3].split("@")[0];
		    // add a line for any matching user
		    if (user.equalsIgnoreCase(email) && line[5].equals("Deleted")) {
			date = new DateTime(sd.parse(line[0]));
			fw.write(display.format(date) + FileUtil.SEPARATOR_COMMA + line[2] + FileUtil.SEPARATOR_COMMA + line[3] + FileUtil.SEPARATOR_COMMA + line[4] + FileUtil.NEW_LINE);
		    }
		}

		fw.flush();
		fw.close();
		reader.close();

	    }
	}
	ht.clear();
	System.out.println("Processed " + count + " users.");
    }

    private static void loadArchiveList() throws Exception {
	final String inputFile = "/temp/clientpurge20050305.csv";
	final DateTime archiveDate = new DateTime("05/03/2005");
	ArchiveMgr archiveMgr = ArchiveEJBHelper.getArchiveMgr();

	CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));
	String[] line = null;
	ArchivedClient client = null;
	String clientNumber = null;
	while ((line = reader.readNext()) != null) {
	    try {
		client = new ArchivedClient();
		clientNumber = line[0];
		client.setClientNo(new Integer(clientNumber));
		client.setArchiveDate(archiveDate);
		archiveMgr.createArchivedClient(client);
	    } catch (Exception ex) {
		System.err.println("clientNumber=" + clientNumber + ": " + ex.getMessage());
	    }
	}
    }

    private static void updateArchivedClients() throws Exception {
	final String inputFile = "/temp/clprogress20080414";
	final SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS"); // 2006-04-13
										 // 00:00:00

	CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));
	ArchiveMgr archiveMgr = ArchiveEJBHelper.getArchiveMgr();
	String[] line = null;
	ArchivedClient client = null;
	String clientNumber = null;
	String status = null;

	double ins = 0;
	double roa = 0;
	double tra = 0;
	String business = null;
	String createDate = null;
	String lastUpdate = null;
	String logon = null;

	System.out.println("starting update");

	int updateCount = 0;
	int recordCount = 0;

	while ((line = reader.readNext()) != null) {
	    recordCount++;
	    business = "";
	    clientNumber = line[0];
	    // System.out.println("client "+clientNumber);
	    client = archiveMgr.getArchivedClient(new Integer(clientNumber));
	    if (client != null) {
		try {
		    updateCount++;
		    createDate = line[1];
		    lastUpdate = line[2];
		    status = line[3];

		    roa = Double.parseDouble(line[4]);
		    ins = Double.parseDouble(line[5]);
		    tra = Double.parseDouble(line[6]);

		    logon = line[7];

		    if (ins > 0) {
			business += "I";
		    }
		    if (roa > 0) {
			business += "R";
		    }
		    if (tra > 0) {
			business += "T";
		    }

		    if (status != null && !status.trim().equals("")) {
			client.setStatus(status.toUpperCase());
		    }

		    if (createDate != null && !createDate.equals("")) {
			client.setCreateDate(new DateTime(sd.parse(createDate)));
		    }
		    if (lastUpdate != null && !lastUpdate.equals("")) {
			client.setLastUpdate(new DateTime(sd.parse(lastUpdate)));
		    }
		    client.setCreateLogonId(logon);
		    client.setBusiness(business);

		    archiveMgr.updateArchivedClient(client);
		    if (updateCount % 1000 == 0) {
			System.out.println(client.toString());
		    }
		} catch (Exception ex) {
		    System.err.println(clientNumber + ": " + ex.getMessage());
		}

		// update

	    }
	}
	System.out.println("recordCount=" + recordCount);
	System.out.println("updateCount=" + updateCount);
    }

    private static void fixClientMarketingFlag() throws Exception {
	String fileName = "/temp/marketable.txt";
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));
	String line = null;
	String elements[] = null;
	ClientVO client = null;
	int counter = 0;
	Integer clientNumber = null;
	while ((line = inputFileReader.readLine()) != null) {
	    counter++;
	    elements = line.split(",");
	    try {
		clientNumber = new Integer(elements[0]);
		client = clientMgr.getClient(clientNumber);
		if (client != null && !client.getMarket().booleanValue()) {
		    client.setMarket(Boolean.valueOf(true));
		    clientMgr.updateClient(client, "dgk", "Marketing", "hob", "", "");
		}

		if (counter % 1000 == 0) {
		    System.out.println(counter + " " + elements[0] + " ");
		    System.out.println(client.getClientNumber());
		    System.out.println(client.getSurname());
		}
	    } catch (Exception ex) {
		System.err.println(clientNumber + ": " + ex);
	    }
	}
	System.out.println("record count=" + counter);
    }

    private static void updateCompetitionEmails() throws Exception {
	String fileName = "/temp/petr_comp20080317.csv";
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));
	String line = null;
	DateTime now = new DateTime().getDateOnly();
	String emailAddress = "";
	String elements[] = null;
	ClientVO client = null;
	int counter = 0;
	Integer clientNumber = null;
	while ((line = inputFileReader.readLine()) != null) {
	    counter++;
	    if (counter > 2) {
		elements = line.split(",");
		try {
		    clientNumber = new Integer(elements[8]);
		    emailAddress = elements[9];
		    client = clientMgr.getClient(clientNumber);
		    if (client != null) {
			// client.setEmailAddress(emailAddress.toLowerCase());
			client.setLastUpdate(now);
			clientMgr.updateClient(client, "jyh", "Petrusma competition email harvest", "hob", "", "");
		    } else {
			throw new Exception("client does not exist.");
		    }

		    System.out.println(client.getClientNumber() + ": " + client.getEmailAddress());

		} catch (Exception ex) {
		    System.err.println(clientNumber + ": " + ex);
		}
	    }
	    System.out.println("record count=" + counter);
	}
    }

    private static void fixMembershipHistoryWithGroupErrors(String fileName) throws Exception {
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	String line = null;
	User user = SecurityHelper.getSystemUser();
	MembershipTransactionVO memTrans = null;
	int counter = 0;
	int errCounter = 0;
	Integer membershipId = null;
	MembershipVO mem = null;
	MembershipHistory memHist = null;
	String transactionId = null;
	String clientNumber = null;
	String elements[] = null;
	Collection transList = null;
	MembershipTransactionVO tx = null;
	Iterator transIt = null;
	while ((line = inputFileReader.readLine()) != null) {
	    elements = line.split(",");
	    transactionId = elements[1];
	    counter++;
	    memTrans = membershipMgr.getMembershipTransaction(new Integer(transactionId));

	    mem = membershipMgr.getMembership(memTrans.getMembershipID()); // current
									   // state
	    memHist = memTrans.getMembershipHistory();
	    if (memHist != null && mem != null) {

		if (mem.getMembershipGroup() != null && memHist.getMembershipGroup() != null && (memHist.getMembershipGroup().getMemberCount() != mem.getMembershipGroup().getMemberCount())) // history
																							      // group
																							      // count
																							      // !=
																							      // current
																							      // count
		{

		    // set the last transaction with the current XML
		    memTrans.setMembershipXML(mem.toXML());

		    // update transaction in the database
		    membershipMgr.updateMembershipTransaction(memTrans);

		    errCounter++;
		    // if(errCounter % 100 == 0)
		    // {
		    System.err.println(errCounter + " - " + mem.getClientNumber() + ": " + memHist.getMembershipGroup().getMemberCount() + "/" + mem.getMembershipGroup().getMemberCount());
		    // }

		} else {
		    // group has changed or group count is correct
		    System.out.println(counter + " - " + mem.getClientNumber() + ": " + (memHist.getMembershipGroup() == null) + " " + (mem.getMembershipGroup() == null));
		    // individual? ie. 1==1
		}
	    }
	}
	System.out.println("record count=" + counter);
	System.out.println("error count=" + errCounter);
    }

    private static void fixMembershipStateFromHistory(String fileName) throws Exception {
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));
	FileWriter fw = new FileWriter("/temp/memerrors.csv");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	String line = null;

	MembershipTransactionVO memTrans = null;
	int counter = 0;
	int errCounter = 0;
	MembershipVO mem = null;
	MembershipHistory memHist = null;
	String transactionId = null;
	String membershipId = null;
	String elements[] = null;
	boolean fixed = false;
	int fixCounter = 0;
	while ((line = inputFileReader.readLine()) != null) {
	    elements = line.split(",");
	    transactionId = elements[0];
	    membershipId = elements[1];
	    counter++;
	    memTrans = membershipMgr.getMembershipTransaction(new Integer(transactionId));

	    if (memTrans != null) {
		try {
		    mem = membershipMgr.getMembership(new Integer(membershipId)); // current
										  // state
		    memHist = memTrans.getMembershipHistory();
		    if (memHist != null && mem != null) {
			// compare join date, product, status and expiry
			// difference
			if (!mem.getJoinDate().equals(memHist.getJoinDate()) || (mem.getProductCode() != null && memHist.getProductCode() != null && !mem.getProductCode().equals(memHist.getProductCode())) || !mem.getBaseStatus().equals(memHist.getBaseStatus()) ||
			// greater than 3 month difference between expiries
				DateUtil.getDateDiff(mem.getExpiryDate(), memHist.getExpiryDate(), Calendar.MONTH) > 3) {
			    if (memTrans.getTransactionGroupTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL) || memTrans.getTransactionGroupTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP)) {
				membershipMgr.updateMembership((MembershipVO) memHist);
				fixCounter++;
				fixed = true;
			    } else {
				fixed = false;
			    }
			    fw.write(mem.getClientNumber() + "," + memTrans.getTransactionID() + "," + memTrans.getTransactionDate() + "," + memTrans.getTransactionTypeCode() + "," + memTrans.getTransactionGroupTypeCode() + "," + memTrans.getUsername() + "," + memTrans.getTransactionGroupID() + "," + fixed + FileUtil.NEW_LINE);
			    fw.flush();
			    errCounter++;
			}
		    } else {
			System.out.println("transactionId=" + transactionId);
		    }

		    if (counter % 500 == 0) {
			System.out.println("count = " + counter + " " + errCounter);
		    }

		} catch (Exception ex) {
		    // parsing error
		    System.out.println(membershipId + ": " + ex);
		}

	    }

	}
	fw.close();
	System.out.println("record count=" + counter);
	System.out.println("error count=" + errCounter);
	System.out.println("fix count=" + fixCounter);

	System.out.println("error percentage=" + NumberUtil.formatPercentageValue(errCounter, counter));
	System.out.println("fix percentage=" + NumberUtil.formatPercentageValue(fixCounter, errCounter));
	System.out.println("fix remaining=" + (errCounter - fixCounter));
    }

    private static void fixMembershipGroup(Integer transactionId) throws Exception {

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	MembershipTransactionVO membershipTransaction = membershipMgr.getMembershipTransaction(transactionId);
	int counter = 0;
	if (membershipTransaction != null) {

	    MembershipVO memHist = (MembershipVO) membershipTransaction.getMembershipHistory();
	    MembershipGroupVO memGroup = memHist.getMembershipGroup();
	    MembershipGroupDetailVO groupDetail = null;
	    Collection groupDetailList = memGroup.getMembershipGroupDetailList();
	    if (groupDetailList != null) {

		Iterator groupIt = groupDetailList.iterator();
		while (groupIt.hasNext()) {
		    counter++;
		    groupDetail = (MembershipGroupDetailVO) groupIt.next();
		    System.out.println(groupDetail.getMembership().getClientNumber() + ": " + groupDetail.isPrimeAddressee());
		    // membershipMgr.createMembershipGroupDetail(groupDetail);
		}

	    }
	}

	System.out.println("count: " + counter);

    }

    private static void removeCardRequestTransactions(String fileName) throws Exception {
	System.out.println("removeCardRequestTransactions");
	// get list of card request transactions only
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));

	MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();

	// transaction id, transaction_type_code
	String line = null;
	String elements[] = null;
	String transactionTypeCode = null;
	Integer transactionId = null;
	Collection transList = null;
	Iterator transIt = null;
	MembershipTransactionVO trans = null;
	MembershipTransactionVO tmpTrans = null;
	Collection cardList = null;
	Iterator cardIt = null;
	MembershipCardVO memCard = null;
	int counter = 0;
	int errCounter = 0;
	System.out.println("Reading file...");
	while ((line = inputFileReader.readLine()) != null) {
	    elements = line.split(",");

	    transactionId = new Integer(elements[0]);
	    System.out.println("Removing card requests for transaction " + transactionId + "...");
	    transactionTypeCode = elements[1];

	    if (!transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST)) {
		throw new Exception("May only remove card requests and associated transactions.");
	    }

	    trans = memMgr.getMembershipTransaction(transactionId);
	    if (trans != null) {
		System.out.println(trans.getMembership().getClientNumber());
		System.out.println("Getting associated transactions...");
		// get the transaction list for the group - remove all
		// associated transactions
		transList = trans.getTransactionListForGroup();
		if (transList != null && transList.size() > 0) {
		    System.out.println("Retrieved transactions " + transList.size());
		    transIt = transList.iterator();
		    while (transIt.hasNext()) {
			counter++;
			tmpTrans = (MembershipTransactionVO) transIt.next();
			System.out.println("Retrieved transaction " + tmpTrans);
			if (!transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST) && !transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP)) {
			    System.err.println(transactionTypeCode + " should not be removed.");
			    errCounter++;
			    continue;
			} else {
			    cardList = memMgr.getMembershipCardList(tmpTrans.getMembershipID());
			    if (cardList != null) {
				cardIt = cardList.iterator();
				while (cardIt.hasNext()) {
				    memCard = (MembershipCardVO) cardIt.next();
				    // if transaction date the same as the card
				    // request date
				    // then update card record with the user id
				    // of the transaction
				    if (tmpTrans.getTransactionDate().getDateOnly().equals(memCard.getRequestDate().getDateOnly())) {
					System.out.println("Updating card and removing transaction...");
					// set the transaction user id
					memCard.setUserId(tmpTrans.getUsername());

					// update card record
					memMgr.updateMembershipCard(memCard);

					// remove the transaction
					memMgr.removeMembershipTransaction(tmpTrans);

				    } else {
					// errCounter++;
					System.err.println("No cards matching transaction date");
				    }
				} // end while
			    } else {
				errCounter++;
				System.err.println("No cards to update");
			    }

			}
		    } // end trans list
		}
	    }// end of tran
	    else {
		System.err.println("Transaction " + transactionId + " not found.");
	    }

	} // end of line

	System.out.println("record count=" + counter);
	System.out.println("error count=" + errCounter);
	System.out.println("");

    }

    private static void removeTramadaGroupClients(String fileName) throws Exception {
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	String line = null;
	User user = SecurityHelper.getSystemUser();
	ClientVO client = null;
	// String deleteSQL = "";
	int counter = 0;
	int errCounter = 0;
	int clientNumber = 0;
	String errString = "";
	while ((line = inputFileReader.readLine()) != null) {
	    counter++;
	    client = clientMgr.getClient(new Integer(line));
	    clientNumber = client.getClientNumber().intValue();

	    // trigger a delete
	    try {
		// System.out.println("processing client=" + clientNumber);
		// clientMgr.deleteTramadaClientTransactions(new
		// Integer(clientNumber));
		// if (client.getInitials() == null ||
		// client.getInitials().trim().equals(""))
		// {
		// client.setInitials("X");
		// }
		// System.out.print("insert into PUB.tr_client_transaction (client_number,Tramada_profile_code,action,modified) values ");
		// System.out.println("("+clientNumber+",'"+TramadaClientHelper.getTramadaProfileCode(client)+"','A','"+new
		// DateTime().formatShortDate()+"')");
		System.out.println(client.getSurname() + "              " + client.getInitials() + " " + client.getClientNumber());
		if (false)
		    throw new RemoteException();

		// clientMgr.transferTramadaClientXML(Client.TRAMADA_MODE_EXPLICIT,TramadaClientProfileImporter.TRAMADA_ACTION_DELETE,
		// user.getUserID(),
		// user.getSalesBranchCode(), client);
	    } catch (RemoteException ex) {
		errString += (clientNumber + ": " + ex.toString()) + "\n";
		// deleteSQL +=
		// "delete from PUB.tr_client_transaction where client_number = "+clientNumber+"\n";
		errCounter++;
	    }
	}
	System.out.println("record count=" + counter);
	System.out.println("error count=" + errCounter);
	System.out.println("");
	System.out.println(errString);
	// System.out.println("");
	// System.out.println(deleteSQL);
    }

    private static void fixAccessMemberAddresses(String fileName) throws Exception {
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));

	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	String line = null;
	Integer clientNumber = null;
	int counter = 0;
	ClientVO client = null;
	while ((line = inputFileReader.readLine()) != null && counter < 1) {
	    clientNumber = new Integer(line);
	    client = clientMgr.getClient(clientNumber);

	    System.out.println("client=" + client.toString());
	    counter++;
	    // client.setStatus(Client.STATUS_ADDRESS_UNKNOWN);
	    // clientMgr.updateClient(client,"jyh","Access member update","hob","","RSS");

	}

	System.out.println("processed " + counter);
    }

    private static void fixTramadaSalesBranches(String fileName) throws Exception {
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	String line = null;
	TramadaClientTransaction tct = null;
	// User user = UserSession.getSystemUser().getUser();
	ClientVO client = null;
	int counter = 0;
	int errCounter = 0;
	int clientNumber = 0;
	while ((line = inputFileReader.readLine()) != null && counter < 1) {
	    counter++;
	    client = clientMgr.getClient(new Integer(line));
	    clientNumber = client.getClientNumber().intValue();

	    try {
		// get list of client transactions
		tct = clientMgr.getLastTramadaClientTransaction(client.getClientNumber());

		// clientMgr.transferTramadaClientXML(Client.TRAMADA_MODE_EXPLICIT,
		// TramadaClientProfileImporter.TRAMADA_ACTION_UPDATE, "",
		// "HBC", client); //explicit update to update Branch
	    } catch (RemoteException ex) {
		System.err.println(clientNumber + ": " + ex.toString());
		errCounter++;
	    }

	}
	System.out.println("record count=" + counter);
	System.out.println("error count=" + errCounter);
    }

    private static void undoAccessMembershipList() throws Exception {
	// file in format client_number,membership_id
	String fileName = "/temp/undo_access_list.txt";
	BufferedReader inputFileReader = new BufferedReader(new FileReader(fileName));
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	String line = null;
	DateTime effectiveDate = new DateTime("13/7/2006");
	String lineArray[] = null;
	Integer clientNumber = null;
	boolean testMode = false;
	Integer membershipId = null;
	int failCounter = 0;
	int successCounter = 0;
	// MembershipVO membershipVO = null;
	while ((line = inputFileReader.readLine()) != null) {
	    lineArray = line.split(FileUtil.SEPARATOR_COMMA);
	    clientNumber = new Integer(lineArray[0]);
	    membershipId = new Integer(lineArray[1]);
	    try {
		membershipMgr.undoAccessMembership(effectiveDate, testMode, membershipId);
		// membershipVO = membershipMgr.getMembershipVO(membershipId);
		// if (!membershipVO.getClientNumber().equals(clientNumber))
		// {
		// throw new Exception("Client number does not match");
		// }
		// if
		// (!membershipVO.getProductCode().equals(Product.PRODUCT_ACCESS))
		// {
		// throw new Exception("Membership is not 'Access'");
		// }
		successCounter++;
	    } catch (Exception ex) {
		System.err.print("Unable to undo access membership '" + membershipId + "'. ");
		System.err.println(ex);
		failCounter++;
	    }
	    System.out.println(clientNumber + " " + membershipId);
	}
	inputFileReader.close();
	System.out.println("Undo - Successful:" + successCounter + ", Failed:" + failCounter); // should
											       // be
											       // ~
											       // 350
    }

    public static String buildSQLInList(Collection listItems) {
	StringBuffer inList = new StringBuffer();
	Iterator searchList = listItems.iterator();
	int count = 0;
	while (searchList.hasNext()) {
	    count++;
	    inList.append("'" + searchList.next() + "'");
	    if (count < listItems.size()) {
		inList.append(FileUtil.SEPARATOR_COMMA);
	    }
	}
	return inList.toString(); // cl.substring(0,cl.length()-2);
    }

    private static void convertCardFile() throws NumberFormatException, IOException {
	boolean sequenceMode = true;

	final String dir = "/temp/";
	final String inputFileName = dir + "testdata20060626.txt"; // enter
								   // input
	final String outputFileName = dir + "testdata20060705.txt";

	DateTime currentDate = new DateTime();
	SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");

	final String austpostFileName = dir + "ract_test_200607.txt";
	final String finalOutputFileName = dir + "cardfile" + sd.format(currentDate) + "_presorted.txt";

	// one or the other
	if (!sequenceMode) {
	    convertCardFileForPresortCodes(inputFileName, outputFileName);
	} else {
	    // get file back from aust post with pre sort codes added
	    sequenceCardFile(inputFileName, austpostFileName, finalOutputFileName);
	}
    }

    private static void sequenceCardFile(String unsorted, String sorted, String sortedOutputFile) throws NumberFormatException, FileNotFoundException, IOException {
	Hashtable cardFile = new Hashtable();
	Hashtable keyedFile = new Hashtable();

	File cardFileUnsorted = new File(unsorted);
	File cardFileSortedAndAppended = new File(sorted);
	File outputFile = new File(sortedOutputFile);

	String delimiter = FileUtil.SEPARATOR_PIPE;

	BufferedReader inputFileReader1 = new BufferedReader(new FileReader(cardFileUnsorted));
	BufferedReader inputFileReader2 = new BufferedReader(new FileReader(cardFileSortedAndAppended));
	BufferedWriter outputFileWriter1 = new BufferedWriter(new FileWriter(outputFile));

	String line = null;
	String lineArray[] = null;
	while ((line = inputFileReader1.readLine()) != null) {
	    if (!line.startsWith("End of File.")) {
		line = line + delimiter; // make it consistent with orig card
					 // file
		lineArray = line.split("[|]");
		cardFile.put(new Integer(lineArray[8]), line);
	    }
	}
	System.out.println("after card file 1");
	Integer clientNumber = null;
	StringBuffer tmpLine = null;
	while ((line = inputFileReader2.readLine()) != null) {
	    if (!line.startsWith("SeqNbr")) {
		lineArray = line.split("[|]");
		clientNumber = new Integer(lineArray[10]);
		tmpLine = new StringBuffer((String) cardFile.get(clientNumber));
		for (int i = 0; i < 10; i++) {
		    if (i > 0) {
			tmpLine.append(delimiter);
		    }
		    tmpLine.append(lineArray[i]);
		}
		tmpLine.append(delimiter);
		tmpLine.append(lineArray[17]);
		// tmpLine.append(delimiter);
		// tmpLine.append(lineArray[0]);
		keyedFile.put(new Integer(lineArray[0]), tmpLine);
	    }
	}
	System.out.println("after card file 2");
	// sort it
	ArrayList keys = new ArrayList(keyedFile.keySet());
	Collections.sort(keys);
	Iterator it = keys.iterator();
	while (it.hasNext()) {
	    Integer element = (Integer) it.next();
	    // System.out.println(keyedFile.get(element).toString());
	    outputFileWriter1.write(keyedFile.get(element).toString() + FileUtil.NEW_LINE);
	}

	// auto flush
	outputFileWriter1.close();
    }

    private static void convertCardFileForPresortCodes(String inputFileName, String outputFileName) throws IOException {
	File inputFile = new File(inputFileName);
	File outputFile = new File(outputFileName);

	FileWriter fileWriter = new FileWriter(outputFile);

	BufferedReader fileReader = null;
	String delimiter = FileUtil.SEPARATOR_PIPE;
	String line = null;
	String lineArray[] = null;
	fileReader = new BufferedReader(new FileReader(inputFile));

	fileWriter.write("membership_number");
	fileWriter.write(delimiter);
	fileWriter.write("property");
	fileWriter.write(delimiter);
	fileWriter.write("street_number");
	fileWriter.write(delimiter);
	fileWriter.write("street");
	fileWriter.write(delimiter);
	fileWriter.write("suburb");
	fileWriter.write(delimiter);
	fileWriter.write("state");
	fileWriter.write(delimiter);
	fileWriter.write("postcode");
	fileWriter.write(FileUtil.NEW_LINE);

	String street[] = null;
	String add1[] = null;
	String add2[] = null;
	int c1 = 0;
	int c2 = 0;
	while ((line = fileReader.readLine()) != null) {
	    c1++;
	    if (line.indexOf("End of File.") == -1) {
		c2++;
		lineArray = line.split("[|]"); // reg exp
		// System.out.println("line="+line+" "+lineArray.length);
		fileWriter.write(lineArray[8]);
		fileWriter.write(delimiter);
		fileWriter.write(lineArray[3]);
		fileWriter.write(delimiter);
		// System.out.println(lineArray[4]);
		street = lineArray[4].split(" ");
		// System.out.println(street[0]+" "+lineArray[4]+" "+street.length);
		if (street.length > 1) {
		    fileWriter.write(street[0]); // street number
		    fileWriter.write(delimiter);
		    fileWriter.write(street[1]); // street
		    if (street.length == 3) {
			fileWriter.write(" ");
			fileWriter.write(street[2]); // street type
		    }
		} else {
		    fileWriter.write(delimiter);
		}
		fileWriter.write(delimiter);
		// System.out.println(lineArray[5]);
		add1 = lineArray[5].split("   ");
		// System.out.println(add1);
		add2 = add1[1].split(" ");
		// System.out.println(add2[0]);
		// System.out.println(add2[1]);
		fileWriter.write(add1[0]); // suburb
		fileWriter.write(delimiter);
		fileWriter.write(add2[0]); // state
		fileWriter.write(delimiter);
		fileWriter.write(add2[1]); // postcode
		fileWriter.write(FileUtil.NEW_LINE);
		fileWriter.flush();
	    }
	    // break;

	}
	System.out.println("counters " + c1 + " " + c2);
	fileWriter.flush();
	fileWriter.close();

	fileReader.close();
    }

    private static void testBillPayXML() throws NumberFormatException, SAXException, ParserConfigurationException, FactoryConfigurationError, IOException {
	BufferedReader fileReader = null;

	FileWriter fileWriter = new FileWriter(new File("/temp/Audit.txt"));

	final String FILE_NAMES[] = { "/temp/Audit060620.txt", "/temp/Audit_20060620_112500618.txt", "/temp/Audit_20060620_111500200.txt" };
	File file = null;
	String line = null;
	for (int x = 0; x < FILE_NAMES.length; x++) {
	    file = new File(FILE_NAMES[x]);
	    fileWriter.write(file.getName() + FileUtil.NEW_LINE);
	    fileWriter.write("" + FileUtil.NEW_LINE);
	    fileWriter.write("" + FileUtil.NEW_LINE);
	    fileReader = new BufferedReader(new FileReader(file));
	    while ((line = fileReader.readLine()) != null) {
		if (line.startsWith("<IvrPayment") || line.startsWith("<ElectronicPayment")) {
		    // System.out.println("line: "+line);
		    // cater for error in xml

		    if (line.startsWith("<ElectronicPayment>")) {
			String search = "</ElectronicPayment>";
			line = line.substring(0, line.indexOf(search) + search.length());
		    }
		    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		    factory.setValidating(false);
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    Document document = builder.parse(new InputSource(new StringReader(line)));
		    // Should only be one TransactionGroup node in the document.
		    Node node = document.getFirstChild();

		    if (node.getNodeName().equals("IvrPayment")) {
			IVRPayment ivr = new IVRPayment(node);
			fileWriter.write(ivr.toString() + FileUtil.NEW_LINE);
		    } else if (node.getNodeName().equals("ElectronicPayment")) {
			BPAYPayment ep = new BPAYPayment(node);
			ep.setPaymentInstructionType(5);
			if (ep.getSourceSystem().equals(SourceSystem.MEMBERSHIP)) {
			    ep.setBillerCode("403451");
			} else {
			    ep.setBillerCode("727842");
			}
			String inputArray[] = ep.getInputData().split(",");
			ep.setPaymentMethod(Integer.parseInt(inputArray[5]));
			ep.setEntryMethod(Integer.parseInt(inputArray[6]));
			ep.setOriginalReferenceNumber(" ");
			ep.setErrorCorrectionReason(0);
			fileWriter.write(ep.toString() + FileUtil.NEW_LINE);

		    }
		}
	    }
	}

	fileWriter.flush();
	fileWriter.close();
    }

    private static void dumpSystemProperties() {
	Properties prop = System.getProperties();
	Set keys = prop.keySet();
	Iterator keyIt = keys.iterator();

	while (keyIt.hasNext()) {
	    String key = (String) keyIt.next();
	    String value = (String) prop.get(key);
	    System.out.println(key + ": " + value);
	}

    }

    private static void testFinanceOneAdapter() throws FinanceException, RemoteException, PaymentException, ParseException {
	SourceSystem ss = SourceSystem.MEMBERSHIP;
	Vector journalList = new Vector();
	Journal j1 = new Journal(33.00, "1340", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");
	Journal j2 = new Journal(-33.00, "2150", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");
	Journal j3 = new Journal(77.00, "1340", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");
	Journal j4 = new Journal(-77.00, "2150", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");
	Journal j5 = new Journal(150.00, "1340", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");
	Journal j6 = new Journal(-150.00, "2150", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");
	Journal j7 = new Journal(77.00, "1340", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");
	Journal j8 = new Journal(-77.00, "2150", "", "", Company.RACT, new DateTime(), new DateTime(), "HOB", "test", "", "xx");

	journalList.add(j1);
	journalList.add(j2);
	journalList.add(j3);
	journalList.add(j4);
	journalList.add(j5);
	journalList.add(j6);
	journalList.add(j7);
	journalList.add(j8);

	FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	financeMgr.transferJournals(journalList, ss);
    }

    private static void testMembershipTier(DateTime joinDate) throws ValidationException, RemoteException {
	MembershipVO mem = new MembershipVO();
	mem.setJoinDate("1/1/1957");
	mem.setExpiryDate("1/2/2007");
	System.out.println("joinDate=" + mem.getJoinDate());
	System.out.println("memYears=" + mem.getMembershipYears().getTotalYears());
	System.out.println(mem.getMembershipTier());
	System.out.println(mem.getMembershipTier().isEligibleForLoyaltyScheme());
	System.out.println("renewal tier=" + mem.getRenewalMembershipTier());

    }

    private static void fixPolicy() {
	// InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	// insAdapter.notifyPremiumPayment(new Integer(68007),
	// new Integer(37540),
	// new Integer(21),
	// new BigDecimal("769.42"),
	// "edv",
	// "hob",
	// SourceSystem.INSURANCE_RACT.getAbbreviation());
	// insAdapter.commit();

    }

    private static void testGetPayable() throws PaymentException, RemoteException {
	String paymentMethodId = "33";
	PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	// Payable p = paymentMgr.getReceptorPayable(new Integer(00000003),
	// "000066",
	// SourceSystem.FIN, false);
	// System.out.println("Receipt Number: " +
	// p.getFirstReceipt().getReceiptNumber());
	// System.out.println("Receipt Details: " +
	// p.getFirstReceipt().getCreateDate());
	System.out.println("Payment method id: " + paymentMethodId);
	// System.out.println("Client: " + p.getClientNumber());
	// System.out.println("Amount: " + p.getAmount());
	// System.out.println("Outstanding: " + p.getAmountOutstanding());
	// System.out.println("Product: " + p.getProductCode());
    }

    private static void testDirectDebitScheduleByReceptorOrgId() throws NumberFormatException, PaymentException, RemoteException {
	String clientCode = "S1_40146";
	String clientNumber = "539004";
	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	Collection authList = pMgr.getDirectDebitAuthorityListByClient(new Integer(clientNumber));
	DirectDebitAuthority ddAuthority = null;
	DirectDebitSchedule ddSchedule = null;
	Collection schedList = null;
	if (authList != null) {
	    Iterator authIt = authList.iterator();
	    while (authIt.hasNext()) {
		ddAuthority = (DirectDebitAuthority) authIt.next();
		System.out.println("Authority = " + ddAuthority.toString());
		if (ddAuthority != null) {
		    // schedList =
		    // pMgr.getDirectDebitScheduleListByDirectDebitAuthority(ddAuthority);
		    schedList = ddAuthority.getScheduleList();
		    if (schedList != null) {
			Iterator schedIt = schedList.iterator();
			while (schedIt.hasNext()) {
			    ddSchedule = (DirectDebitSchedule) schedIt.next();
			    System.out.println("Schedule = " + ddSchedule.toString());
			}

		    }
		}
	    }
	}
    }

    private static void generateIVRSummary() throws NumberFormatException, IOException, ParseException {
	String ivrNumber = null; // 0
	DateTime paymentDate = null; // 1
	double amount = 0; // 5
	String clientNumber = null; // 8
	String sourceSystem = null; // 10
	boolean error = false; // 12

	final String STATUS_OK = "OK:";

	final String SYSTEM_INS = "INS"; // 0
	final String SYSTEM_MEM = "MEM"; // 1

	final int TOTAL_INS = 0; // 0
	final int TOTAL_MEM = 1; // 1

	double totals[] = { 0, 0 };

	DateTime startDate = new DateTime("1/9/2005");
	DateTime endDate = new DateTime("30/9/2005");

	BufferedReader fileReader = null;
	String dirName = "/temp/ivr";

	File outputFile = new File("/temp/ivr_sept05.csv");
	FileWriter outputWriter = new FileWriter(outputFile);

	File dir = new File(dirName);
	final String DELIMITER = ",";
	final String LINE = "\n";
	final int DELIMITER_INDEX = 12;
	File files[] = dir.listFiles();
	StringBuffer writeLine = null;
	File file = null;
	String line = null;
	String lineElements[] = null;
	int lineCount = 0;
	int fileLineCount = 0;
	int processedFileLineCount = 0;
	for (int i = 0; i < files.length; i++) {
	    if (lineCount == 0) {
		writeLine = new StringBuffer();
		writeLine.append("IVR Number");
		writeLine.append(DELIMITER);
		writeLine.append("Payment Date");
		writeLine.append(DELIMITER);
		writeLine.append("Amount");
		writeLine.append(DELIMITER);
		writeLine.append("Client Number");
		writeLine.append(DELIMITER);
		writeLine.append("Source System");
		writeLine.append(DELIMITER);
		writeLine.append("Error");
		writeLine.append(LINE);
		outputWriter.write(writeLine.toString());
		outputWriter.flush();

	    }
	    fileLineCount = 0;
	    processedFileLineCount = 0;
	    file = files[i];
	    System.out.println("fileName=" + file.getName());
	    fileReader = new BufferedReader(new FileReader(file));
	    while ((line = fileReader.readLine()) != null) {
		lineCount++;
		fileLineCount++;
		// process line
		// System.out.println("line"+line);
		// System.out.println("line lenght"+line.length());
		if (line.length() > DELIMITER_INDEX && line.substring(11, 12).equals(DELIMITER)) {
		    // write the line
		    lineElements = line.split(DELIMITER);
		    ivrNumber = lineElements[0];
		    paymentDate = new DateTime(lineElements[1]);
		    amount = Double.parseDouble(lineElements[5]);
		    clientNumber = lineElements[8];
		    sourceSystem = lineElements[10].substring(3);
		    error = !lineElements[12].equals(STATUS_OK);

		    // System.out.println(line);

		    if (paymentDate.onOrAfterDay(startDate) && paymentDate.onOrBeforeDay(endDate)) {

			processedFileLineCount++;

			if (!error) {
			    if (sourceSystem.equals(SYSTEM_INS)) {
				totals[TOTAL_INS] += amount;
			    } else if (sourceSystem.equals(SYSTEM_MEM)) {
				totals[TOTAL_MEM] += amount;
			    }
			}

			writeLine = new StringBuffer();
			writeLine.append(ivrNumber);
			writeLine.append(DELIMITER);
			writeLine.append(paymentDate);
			writeLine.append(DELIMITER);
			writeLine.append(amount);
			writeLine.append(DELIMITER);
			writeLine.append(clientNumber);
			writeLine.append(DELIMITER);
			writeLine.append(sourceSystem);
			writeLine.append(DELIMITER);
			writeLine.append(error ? "Yes" : "No");
			writeLine.append(LINE);

			// System.out.println(writeLine);

			outputWriter.write(writeLine.toString());
			outputWriter.flush();

		    }

		}

	    }

	    System.out.println("fileLineCount=" + fileLineCount);
	    System.out.println("processedFileLineCount=" + processedFileLineCount);

	}

	outputWriter.close();

	System.out.println("lineCount=" + lineCount);
	System.out.println("files=" + files.length);

	System.out.println("INS Totals=" + CurrencyUtil.formatDollarValue(totals[TOTAL_INS]));
	System.out.println("MEM Totals=" + CurrencyUtil.formatDollarValue(totals[TOTAL_MEM]));
    }

    private static void testTierCode() throws NumberFormatException, FinderException, RemoteException {
	String clientNumber = "2001";
	MembershipVO memVO = null;

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection membershipList = membershipMgr.findMembershipByClientNumber(new Integer(clientNumber));
	Iterator memIt = membershipList.iterator();
	memVO = (MembershipVO) memIt.next();
	System.out.println("Membership tier = " + memVO.getMembershipTier());
    }

    private static void testCreateReceptorPayablesFromOCR() throws RollBackException, RemoteException, ParseException {
	DateTime startDate = new DateTime("1/2/2005");
	DateTime endDate = new DateTime("1/3/2005");
	String subsystemList = "INS";
	PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
	// paymentTxMgr.createReceptorPayablesFromOCR(startDate, endDate, true,
	// subsystemList);
    }

    private static void testCreatePayablesFromPendingFees() throws RollBackException, RemoteException, ParseException {
	DateTime startDate = new DateTime("1/2/2005");
	DateTime endDate = new DateTime("12/2/2005");
	String subsystemList = "MEM";
	PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
	// paymentTxMgr.createReceptorPayablesFromPendingFees(startDate,
	// endDate, SourceSystem.MEMBERSHIP);
	paymentTxMgr.commit();
    }

    private static void notifyMembershipPayment() throws IOException {
	// file containing failed notifications
	String fileName = "/temp/rctr_failed_notifications20080425.csv";
	BufferedReader fileReader = null;
	fileReader = new BufferedReader(new FileReader(fileName));
	String line = null;
	String[] columns = null;

	SourceSystem paymentSystem = SourceSystem.RECEPTOR;
	SourceSystem sourceSystem = SourceSystem.MEMBERSHIP;

	String referenceNumber = null;
	String userId = null;
	Integer sequenceNumber = null;
	String tmpSeq = null;
	Integer clientNumber = null;
	String description = null;
	double amountPaid = 0;
	double amountOutstanding = 0;
	int counter = 0;

	MembershipTransactionMgr membershipTransactionMgr = MembershipEJBHelper.getMembershipTransactionMgr();

	while ((line = fileReader.readLine()) != null) {
	    counter++;

	    columns = line.split(",");

	    clientNumber = new Integer(columns[0]); // not required

	    description = columns[1];
	    amountPaid = Double.parseDouble(columns[3]);

	    userId = columns[6];
	    tmpSeq = columns[9];
	    tmpSeq = tmpSeq.substring((tmpSeq.indexOf("_") + 1), tmpSeq.length());
	    sequenceNumber = new Integer(tmpSeq);
	    referenceNumber = columns[5]; // pending fee id

	    try {
		TransactionMgr transactionMgr = MembershipEJBHelper.getTransactionMgr();

		transactionMgr.notifyPendingFeePaid(referenceNumber, sequenceNumber, amountPaid, amountOutstanding, description, userId, null);

	    } catch (Exception ex) {
		System.out.println("Error committing " + referenceNumber + ". " + ex.getMessage());
	    }

	    System.out.println("-------------------------------------------------------");
	    System.out.println("paymentSystem=" + paymentSystem);
	    System.out.println("referenceNumber=" + referenceNumber);
	    System.out.println("sequenceNumber=" + sequenceNumber);
	    System.out.println("amountPaid=" + amountPaid);
	    System.out.println("amountOutstanding=" + amountOutstanding);
	    System.out.println("description=" + description);
	    System.out.println("createdBy=" + userId);
	    System.out.println("receiptedBy=" + userId);
	    System.out.println("sourceSystem=" + sourceSystem);
	    System.out.println("clientNumber=" + clientNumber);
	    // get the columns
	}

	System.out.println("-------------------------------------------------------");
	System.out.println("Total: " + counter);
	System.out.println("-------------------------------------------------------");

    }

    private static void testHelpPages() throws RemoteException {

	HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
	ArrayList helpList = (ArrayList) helpMgr.searchHelpPages("cance");
	HelpPage helpPage = null;
	for (int i = 0; i < helpList.size(); i++) {
	    helpPage = (HelpPage) helpList.get(i);
	    System.out.println("help page = " + helpPage);
	}
    }

    private static void testAutomaticRenewals() throws ParseException, RemoteException {
	RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	DateTime startDate = new DateTime("6/7/2007");
	DateTime endDate = new DateTime("1/8/2007");
	ArrayList autoRenewables = (ArrayList) renewalMgr.getAutoRenewables(startDate, endDate);
	for (int i = 0; i < autoRenewables.size(); i++) {
	    System.out.println((i + 1) + " " + autoRenewables.get(i).toString());
	}
	renewalMgr.processAutomaticRenewals(startDate, endDate);
    }

    private static void testSplitString() {
	String list = "test,test2,,,x,,x,test4,r,,x,x,ds,d,df,,dfd,f,d,ffddf5";
	String textArray[] = {};
	textArray = list.split(",");

	System.out.println(textArray.length);
	for (int x = 0; x < textArray.length; x++) {
	    System.out.println("item = " + textArray[x]);
	}
    }

    private static void testGetGLTransactions() throws ParseException, FinanceException, RemoteException {
	FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();

	ArrayList glTransactions = (ArrayList) financeMgr.getGLTransactions(new DateTime("1/12/2003"), new DateTime("31/12/2003"), new DateTime("1/11/2003"), new DateTime("31/12/2003"), "5400", "1340", "10");

	for (int i = 0; i < glTransactions.size(); i++) {
	    System.out.println(glTransactions.get(i).toString());
	}
    }

    private static void testMergeClient() throws IOException, ParserConfigurationException, SAXException {
	// String documentStr = null;
	String fileName = "/temp/prosper_ract_products.txt";
	FileInputStream fileInputStream = new FileInputStream(new File(fileName));

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	factory.setValidating(false);
	DocumentBuilder builder = factory.newDocumentBuilder();

	// expect well formed xml with any illegal characters enclosed in CDATA
	// sections
	Document document = builder.parse(fileInputStream);
	Element documentElement = document.getDocumentElement();
	// String elementName = documentElement.getNodeName();

	StringWriter sw = new StringWriter();
	DOMWriter dw = new DOMWriter(sw);
	dw.print(documentElement);
	String xml = sw.toString();
	// xml = StringUtil.replace(xml,"&#10;","\n");

	System.out.println(xml);

	// send to the integration server.
	testSocketSend();

    }

    private static void testConstructRenewalDirectDebitDetails() throws Exception {
	final Integer CLIENT_NUMBER = new Integer(124889); // 590215

	MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
	MembershipVO memVO = memMgr.getMembership(CLIENT_NUMBER);
	User user = SecurityHelper.getSystemUser();
	MembershipRenewalMgr renewalMgr = MembershipEJBHelper.getMembershipRenewalMgr();
	TransactionGroup transGroup = TransactionGroup.getRenewalTransactionGroup(memVO, user, memVO.getExpiryDate());
	// transGroup.createCreditDiscounts();
	// transGroup.calculateTransactionFee();
	renewalMgr.produceIndividualRenewalNotice(memVO, transGroup, user, null);

    }

    private static void testAutoCancelAllowToLapse() throws RemoteException {
	MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
	memMgr.autoCancelAllowToLapse();
    }

    private static void testTravelEmail() throws IOException {
	// EOT
	final char EOT = (char) 4;

	String testXML = "<?xml version=\"1.0\"?><Integration><Item><Control><DataType>com.ract.travel.reporting.TaxInvoice</DataType><Action>print</Action><ClassName>com.ract.common.reporting.PrintIntegrator</ClassName></Control><Data><print><printGroup>it-dev</printGroup><imageLocation>/reports/images/</imageLocation><emailAddress>r.pearson@ract.com.au</emailAddress><emailAddressFrom>j.holliday@ract.com.au</emailAddressFrom><emailSubject>RACT Travelworld Ref: TEST01</emailSubject><emailMessageBody>Please find files attached.</emailMessageBody><TaxInv><header><ABN>ABN 62 009 475 861</ABN><clientName>MR. RORY BIGGS</clientName><clientNo>5</clientNo><clientAddr1>5/12, MILFORD ST</clientAddr1>"
		+ "<clientAddr2>LINDISFARNE, TAS 7015.</clientAddr2><clientABN>NA</clientABN><refNo>TEST01</refNo><invNo>TEST01-1101</invNo><invDate>13/10/05</invDate><invType>TAX INVOICE DUPLICATE</invType><invDescription>19TH AVENUE ON THE BEACH - Bkg.No:BOOKING#4</invDescription><invDetails>Date:16/12/02</invDetails><bookingName>MR. RONALD ABBOTT</bookingName><netAmount>90.91</netAmount><gstAmount>9.09</gstAmount><grossAmount>100</grossAmount><notes></notes><serviceFee></serviceFee></header></TaxInv></print></Data></Item></Integration>";

	Socket x = new Socket("localhost", 5300);
	OutputStream sos = x.getOutputStream();

	InputStream is = x.getInputStream();

	// send message 1

	System.out.println("writing " + testXML);
	sos.write(testXML.getBytes());
	sos.flush();
	int tmp = 0;
	System.out.println("response 1a");
	// while ((tmp = is.read()) != 4)
	// {
	// get response
	// System.out.print((char)tmp);
	// System.out.print(tmp);
	// }
	System.out.println("response 1b" + x.isClosed());
	System.out.println("response 2b");

	sos.close();
	x.close();
    }

    private static void testDeleteObsoleteClients() throws RemoteException {
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	clientMgr.deleteObsoleteClientsAndBusiness("/temp/clientDelete.txt");
    }

    private static void testSocketSend() throws IOException {
	// EOT
	char c = (char) 4;

	String[] actions = { "rollback", "commit" };
	String action = actions[0];

	String string1 = "<Integration keepsocketopen=\"false\"><Item><Control><DataType></DataType><Action></Action></Control></Item></Integration>" + c;
	String string2 = "<Integration><Item><Control><DataType>IntegrationData</DataType><Action>" + action + "</Action></Control></Item></Integration>" + c;

	Socket x = new Socket("localhost", 5300);
	OutputStream sos = x.getOutputStream();

	InputStream is = x.getInputStream();

	// send message 1

	System.out.println("writing " + string1);
	sos.write(string1.getBytes());
	sos.flush();
	int tmp = 0;
	System.out.println("response 1a");
	while ((tmp = is.read()) != 4) {
	    // get response
	    // System.out.print((char)tmp);
	    System.out.print(tmp);
	}
	System.out.println("response 1b" + x.isClosed());

	/*
	 * //send message 2 System.out.println("writing "+string2);
	 * sos.write(string2.getBytes()); sos.flush();
	 * System.out.println("response 2a"); while ((tmp = is.read()) != 4) {
	 * //get response // System.out.print((char)tmp); //
	 * System.out.print(tmp); }
	 */

	System.out.println("response 2b");

	sos.close();
	x.close();
    }

    /*
     * private static void testObjectPool() throws Exception {
     * 
     * //singleton to manage pool GenericKeyedObjectPool pool = new
     * GenericKeyedObjectPool(new ProgressProxyObjectFactory(),
     * 10,GenericKeyedObjectPool
     * .WHEN_EXHAUSTED_GROW,GenericKeyedObjectPool.DEFAULT_MAX_WAIT);
     * pool.setTestOnReturn(true); OCRProgressProxy ocrProxy = null;
     * 
     * ResultSetHolder rs = new ResultSetHolder(); ProgressOCRProxy
     * progressOCRProxy = null;
     * 
     * DateTime startTime = new DateTime(); for (int i = 0; i <= 10; i++) {
     * System.out.println("i="+i);
     * System.out.println("number in pool = "+pool.getNumActive
     * (ProgressProxyObjectFactory.KEY_OCR_PROXY)); ocrProxy =
     * (OCRProgressProxy)
     * pool.borrowObject(ProgressProxyObjectFactory.KEY_OCR_PROXY);
     * System.out.println(ocrProxy._getConnectionId().toString() + " succeeded."
     * + new DateTime().formatLongDate());
     * 
     * // System.out.println("s"+ocrProxy._isStreaming());
     * ocrProxy.getOiPayablesForClient(430471, rs);
     * System.out.println(ocrProxy._isStreaming()); System.out.println(i
     * +" returning object"); //need to close rs here to close resource
     * 
     * System.out.println("test on borrow? "+pool.getTestOnBorrow()); //without
     * this line it has to create a new one. // rs.getResultSetValue().close();
     * 
     * pool.returnObject(ProgressProxyObjectFactory.KEY_OCR_PROXY,ocrProxy);
     * System.out.println(i
     * +" number in pool = "+pool.getNumIdle(ProgressProxyObjectFactory
     * .KEY_OCR_PROXY)); } DateTime endTime = new DateTime();
     * 
     * System.out.println("time taken (in S) = "+(endTime.getTime()-startTime.
     * getTime())/1000);
     * 
     * pool.close(); }
     */

    private static void testGetClientsWithOutstanding() throws RemoteException {
	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	Collection clients = pMgr.getClientsWithOutstandingAmounts(PaymentMethod.RECEIPTING);
	AmountOutstanding amountOs = null;
	if (clients != null) {
	    Iterator clientIt = clients.iterator();
	    while (clientIt.hasNext()) {
		amountOs = (AmountOutstanding) clientIt.next();
		System.out.println(amountOs.toString());
	    }
	}

    }

    private static void testUpdatePayable() throws RemoteException {
	PaymentTransactionMgr pMgr = PaymentEJBHelper.getPaymentTransactionMgr();
	pMgr.updatePayableData();
    }

    private static void testPrinting() throws FileNotFoundException, IOException {
	File f = new File("c:\\queue_pdf\\test.pdf");
	InputStream is = new FileInputStream(f);
	String line = "";

	long length = f.length();
	byte[] contents = new byte[(int) length];
	// Read in the bytes
	int offset = 0;
	int numRead = 0;
	while (offset < contents.length && (numRead = is.read(contents, offset, contents.length - offset)) >= 0) {
	    offset += numRead;
	}

	// rewrite it
	PrintForm pf = new PrintForm();
	pf.setPrinterName("HOBISYBO1");
	// pf.setPrinterTray("1");
	pf.setPrinterType("HP4200TN");
	pf.setPrinterOptions("smb://ferrari/queue_pdf/");
	pf.setFormOptions("12");

	PrintUtil.writeFileToServerForPrinting(pf, SourceSystem.INSURANCE, contents);
    }

    private static void outputParams(ArrayList array) {
	for (int x = 0; x < array.size(); x++) {
	    System.out.println(x + ". " + array.get(x).toString());
	}

    }

}
