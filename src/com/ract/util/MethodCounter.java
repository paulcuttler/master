package com.ract.util;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

/**
 * <p>
 * Allow debugging of methods. One instance per JVM.
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class MethodCounter {

    /**
     * Hastable of method counts
     * */
    private static Hashtable methodCounts = new Hashtable();

    public synchronized static void addToCounter(String context, String category) {
	// key
	Counter counter = new Counter(context, category);

	if (methodCounts.get(counter) == null) {
	    // add it
	    methodCounts.put(counter, new Integer(1));
	} else {
	    // add one and replace it
	    Integer count = (Integer) methodCounts.get(counter);
	    count = new Integer(count.intValue() + 1);
	    methodCounts.put(counter, count);
	}
    }

    public static void resetCounters() {
	// clear existing counts
	methodCounts.clear();
    }

    public static String outputCounts() {
	StringBuffer desc = new StringBuffer();
	// output counts
	Set keys = methodCounts.keySet();
	Counter counter = null;
	if (keys != null) {
	    desc.append("Method Counts:" + FileUtil.NEW_LINE);
	    for (Iterator keysIt = keys.iterator(); keysIt.hasNext();) {
		counter = (Counter) keysIt.next();
		desc.append(counter.toString() + "=" + methodCounts.get(counter) + FileUtil.NEW_LINE);
	    }
	}
	return desc.toString();
    }

}

class Counter {
    public String getCategory() {
	return category;
    }

    public void setContext(String context) {
	this.context = context;
    }

    public void setCategory(String category) {
	this.category = category;
    }

    public String getContext() {
	return context;
    }

    public Counter(String context, String category) {
	this.context = context;
	this.category = category;
    }

    private String context;
    private String category;

    public boolean equals(Object o) {
	if (o instanceof Counter) {
	    Counter counter = (Counter) o;
	    if (context != null && category != null && context.equals(counter.getContext()) && category.equals(counter.getCategory())) {
		return true;
	    }
	}
	return false;
    }

    public int hashCode() {
	return context.hashCode() + category.hashCode();
    }

    public String toString() {
	return context + "/" + category;
    }

}
