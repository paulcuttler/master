package com.ract.util;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.SortedSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.CommonConstants;
import com.ract.common.CommonMgrBean;
import com.ract.common.SalesBranchVO;
import com.ract.common.SystemException;
import com.ract.common.admin.AdminCodeDescription;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;

/**
 * Utility methods for dealing with HTML formatting.
 * 
 * @author John Holliday
 * @version 1.0, 5/6/2002
 */

public class HTMLUtil {

    public static final String ANCHOR_NEW_LINE = "&#10;";

    private static final String ASTERISK = "*";

    private static final String NEW_LINE = "\n";

    private static final String SPACE = "&nbsp;";

    public static String stringValue(Object o, String methodName) {
	if (o != null) {
	    Class c = o.getClass();
	    Method method[] = c.getMethods();
	    Object ret = null;
	    for (int i = 0; i < method.length; i++) {
		if (method[i].getName().equals(methodName)) {
		    try {
			ret = method[i].invoke(o, null);
		    } catch (Exception e) {
			try {
			    LogUtil.warn(Class.forName("com.ract.util.HTMLUtil"), e);
			} catch (Exception e1) {
			}
		    }
		    return stringValue(ret);
		}
	    }
	}
	return "";
    }

    /**
     * Format the username with a link.
     */
    public static String formatUsername(User user) {
	StringBuffer userString = new StringBuffer();
	SalesBranchVO svo = null;
	if (user != null) {
	    try {
		svo = user.getSalesBranch();
	    } catch (RemoteException re) {
	    }

	    userString.append("<a href=\"" + LoginUIConstants.PAGE_SecurityUIC + "?event=viewUser&userId=" + user.getUserID() + "\"");
	    if (svo != null) {
		userString.append(" title=\"" + user.getUserID() + " " + user.getUsername() + " " + svo.getSalesBranchName() + " " + user.getPrinterGroup() + "\">");
	    } else {
		userString.append(">");
	    }
	    userString.append(user.getUsername());
	    userString.append("</a>");
	}

	return userString.toString();
    }

    /**
     * returns string of an object
     */
    private static String stringValue(Object o) {
	String returnString = "";

	if (o != null) {
	    if (o instanceof DateTime) {
		returnString += ((DateTime) o).formatShortDate();
	    } else if (o instanceof java.util.Date) {
		returnString += DateUtil.formatShortDate((Date) o);
	    } else {
		returnString += o.toString();
	    }
	}

	return returnString;
    }

    /**
     * Return a cleaned copy of the given string that can be used as HTML tag
     * IDs and names that can be referenced in JavaScript. Hyphens, spaces and
     * illegal characters are replaced with underscores.
     */
    public static String cleanString(String str) {
	String tmp;
	// Replace hyphens, spaces and ampersands (&).
	tmp = str.replace('-', '_').replace(' ', '_').replace('&', '_');
	return tmp;
    }

    public static String appendURLParameters(Hashtable queryParameters) {
	StringBuffer urlParameters = new StringBuffer();
	Enumeration keys = queryParameters.keys();
	String key;
	Object value;
	while (keys.hasMoreElements()) {
	    key = (String) keys.nextElement();
	    value = (Object) queryParameters.get(key);
	    urlParameters.append("&");
	    urlParameters.append(key);
	    urlParameters.append("=");
	    try {
		urlParameters.append(URLEncoder.encode(value.toString(), "UTF-8"));
	    } catch (Exception e) {
		LogUtil.debug(HTMLUtil.class, "Unable to encode value= " + value + ", " + e);
	    }
	}
	return urlParameters.toString();
    }

    /**
     * get the row type html given a count
     */
    public static String getRowType(int x) {
	String html = "";
	if (x % 2 == 0) {
	    html += "evenRow";
	} else {
	    html += "oddRow";
	}
	return html;
    }

    public static String getHighlightRowClass() {
	return "highlightRow";
    }

    /**
     * Adds the no cache tag to a html tag
     * 
     * @return the no cache html tags.
     */
    public static String noCacheTag() {
	StringBuffer sb = new StringBuffer();
	sb.append(appendLine("<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">"));

	sb.append("<META HTTP-EQUIV=\"Expires\" CONTENT=\"-1\">");
	// sb.append("<META HTTP-EQUIV=\"Expires\" CONTENT=\"");
	// sb.append(DateUtil.formatDate(new
	// DateTime(),"EEE, dd MMM yyyy HH:mm:ss") + " GMT");
	// sb.append("0");
	// sb.append("\">");
	sb.append(NEW_LINE);
	return sb.toString();
    }

    /**
     * return the html required to include a js file
     * 
     * @note replace includeJavascriptFile as using JSPWriter was not feasible.
     */
    public static String includeJS(String relativeFile) {
	return "<script language=\"Javascript\" src=\"" + relativeFile + "\"></script>";
    }

    /**
     * Append a new line to a string
     */
    private static String appendLine(String text) {
	return text += NEW_LINE;
    }

    /**
     * constant to represent selected item in a select list.
     */
    private static final String LIST_SELECTED = "selected";

    /**
     * return a string representing an option in a selected list.
     * 
     * @param value
     *            the current value.
     * @param target
     *            the target value to match to the current value.
     * @return the option specifying whether it is selected or not.
     */
    public static String option(int value, int target) {
	String select = "<option ";
	if (value == target) {
	    select += LIST_SELECTED;
	}
	select += " value=\"" + value + "\">" + NEW_LINE;
	return select;
    }

    public static String option(int value, int target, String label) {
	StringBuffer select = new StringBuffer("<option ");
	if (value == target) {
	    select.append(LIST_SELECTED);
	}
	select.append(" value=\"");
	select.append(value);
	select.append("\">");
	select.append(label);
	select.append("</option>");
	select.append(NEW_LINE);
	return select.toString();
    }

    /**
     * Return a string representing an option in a selected list.
     * 
     * @param value
     *            the current value.
     * @param target
     *            the target value to match to the current value.
     * @return the option specifying whether it is selected or not.
     */
    public static String option(String value, String target) {
	String select = "<option ";
	if (value.equals(target)) {
	    select += LIST_SELECTED;
	}
	select += " value=\"" + value + "\">" + value + NEW_LINE;
	return select;
    }

    private static final String CHECKED = "checked";

    /**
     * get html for check box items
     */
    public static String getChecked(Boolean source, boolean target) {
	String value = "";
	if (source != null) {
	    if ((source.booleanValue() == target)) {
		value = CHECKED;
	    }
	}
	return value;
    }

    /**
     * Return a string representing an option in a selected list.
     * 
     * @param value
     *            the current value.
     * @param text
     *            the display text.
     * @param target
     *            the target value to match to the current value.
     * @return the option specifying whether it is selected or not.
     */
    public static String option(String value, String text, String target) {
	String select = "<option ";
	if (value.equals(target)) {
	    select += LIST_SELECTED;
	}
	select += " value=\"" + value + "\">" + text + NEW_LINE;
	return select;
    }

    /**
     * return a string rendering html for a red asterisk indicating a mandatory
     * item.
     */
    public static String mandatoryItem() {
	StringBuffer sb = new StringBuffer();
	sb.append("<span class=\"helpTextSmall\">");
	sb.append(ASTERISK);
	sb.append("</span>");
	return sb.toString();
    }

    /**
     * Replace < and > signs with &lt; and &gt; respectively also replace
     * carriage returns with </br>
     */
    public static String showTags(String htmlString) {
	StringBuffer temp = new StringBuffer();
	String ch = null;
	if (htmlString == null || htmlString.length() == 0) {
	    return "";
	}
	for (int x = 0; x < htmlString.length(); x++) {
	    ch = htmlString.substring(x, x + 1);
	    if (ch.equals(">")) {
		temp.append("&gt;");
	    } else if (ch.equals("<")) {
		temp.append("&lt;");
	    } else if (ch.equals("\n")) {
		temp.append("</br>");
	    } else {
		temp.append(ch);
	    }
	}
	return temp.toString();
    }

    /**
     * Place the save and delete buttons and the status message in a table row
     * 
     * @params
     */
    public static String buttonSaveAndDelete(String saveEvent, String deleteEvent, String code, String statMsg) {
	StringBuffer sb = new StringBuffer();

	sb.append("<table border=\"0\" cellspacing=\"3\" cellpadding=\"3\" height=\"94%\" width=\"100%\" >" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);
	sb.append("<td valign=\"top\" width=\"10%\">" + NEW_LINE);

	sb.append(buttonSave(saveEvent, code) + NEW_LINE);

	sb.append("</td>" + NEW_LINE);
	sb.append("<td valign=\"top\" width=\"10%\">" + NEW_LINE);

	sb.append(buttonDelete(deleteEvent, code) + NEW_LINE);

	sb.append("</td>" + NEW_LINE);
	sb.append("<td width=\"10%\">&nbsp;</td>" + NEW_LINE);
	sb.append("<td >" + statusMessage(statMsg) + "</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("</table>" + NEW_LINE);

	return sb.toString();
    }

    public static String statusMessage(String statMsg) {
	StringBuffer sb = new StringBuffer();

	sb.append("<span class=\"adminInfoText\" valign=\"top\" width=\"80%\">" + statMsg + "</span>");

	return sb.toString();
    }

    public static String buttonSave(String saveEvent, String code) {
	String display = "Save this " + code + ".";
	return button("Save", submitFormEvent(saveEvent), code);
    }

    public static String buttonDelete(String deleteEvent, String code) {
	String display = "Delete this " + code + ".";
	return button("Delete", submitFormEvent(deleteEvent), code);
    }

    public static String buttonNext(String nextEvent, String code) {
	String display = "Proceed to " + code + ".";
	return button("Next", submitFormEvent(nextEvent), code);
    }

    public static String buttonBack(String backEvent, String code) {
	String display = "Back to " + code + ".";
	return button("Back", backEvent, code);
    }

    public static String buttonEdit(String editEvent, String code) {
	String display = "Edit this " + code + ".";
	return button("Edit", submitFormEvent(editEvent), code);
    }

    public static String buttonPrintRightFrame() {
	return button("Print", "printRightFrame();return true;", "Print the right hand frame");
    }

    public static String buttonOk(String okEvent, String code) {
	return button("Ok", submitFormEvent(okEvent), code);
    }

    public static String buttonCancel(String event, String code) {
	return button("Cancel", submitFormEvent(event), code);
    }

    public static String buttonRemove(String event, String code) {
	return button("Remove", submitFormEvent(event), code);
    }

    public static String buttonViewAll(String event, String code) {
	return button("ViewAll", event, code);
    }

    public static String button(String buttonName, String event, String display) {
	StringBuffer sb = new StringBuffer();
	String buttonImageName = "btn" + buttonName + new Long(System.currentTimeMillis()).toString();

	sb.append("<span>");
	sb.append("<a  onclick=\"" + event + "\"" + NEW_LINE);
	sb.append("onMouseOut=\"MM_swapImgRestore();\"" + NEW_LINE);
	sb.append("onMouseOver=\"MM_displayStatusMsg('" + display + ".')");
	sb.append(";MM_swapImage('" + buttonImageName + "','','");
	sb.append(CommonConstants.DIR_IMAGES + "/" + buttonName + "Button_over.gif',1);");
	sb.append("return document.MM_returnValue\" >" + NEW_LINE);
	sb.append("<img  name=\"" + buttonImageName + "\"" + NEW_LINE);
	sb.append("src=\"" + CommonConstants.DIR_IMAGES + "/" + buttonName + "Button.gif\"" + NEW_LINE);
	sb.append("border=\"0\"" + NEW_LINE);
	sb.append("alt=\"" + display + ".\"" + NEW_LINE);
	sb.append("></a>");
	sb.append("</span>");

	return sb.toString();
    }

    private static String submitFormEvent(String event) {
	return "submitForm('" + event + "');return true;";
    }

    public static String codeDescriptionTable(String tableHeading, SortedSet codeList, String dblClickEvent, String dblClickMsg, String noneSelectedMsg) {
	StringBuffer sb = new StringBuffer();

	sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);
	sb.append("<td class=\"label\" width=\"40%\"><b>" + tableHeading + "</b></td>" + NEW_LINE);
	sb.append("<td class=\"helpText\" width=\"60%\" align=\"right\">Double Click highlighted row.</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);
	sb.append("<td colspan=\"2\">" + NEW_LINE);
	sb.append("<table width=\"100%\" border=\"0\">" + NEW_LINE);
	sb.append("<tr class=\"headerRow\">" + NEW_LINE);
	sb.append("<td class=\"listHeadingPadded\" width=\"30%\">Code</td>" + NEW_LINE);
	sb.append("<td class=\"listHeadingPadded\" width=\"70%\">Description</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("</table>" + NEW_LINE);
	sb.append("</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);
	sb.append("<td colspan=\"2\">" + NEW_LINE);
	sb.append("<div style=\" height:200;overflow:auto; border-width: 1; border-style: solid;\">" + NEW_LINE);
	sb.append("<table width=\"100%\" border=\"0\">" + NEW_LINE);
	//

	if (!(codeList == null)) {
	    if (codeList.size() > 0) {
		Iterator it = codeList.iterator();
		while (it.hasNext()) {
		    AdminCodeDescription ad = (AdminCodeDescription) it.next();
		    sb.append("<tr name=\"" + ad.getCode().trim() + "\"" + NEW_LINE);
		    sb.append("onMouseOver=\"bgColor = '#CDD7E1';MM_displayStatusMsg('" + dblClickMsg + "');\"" + NEW_LINE);
		    sb.append("onMouseOut=\"bgColor = '#FFFFFF'\"" + NEW_LINE);
		    sb.append("bgcolor=\"#FFFFFF\"" + NEW_LINE);
		    sb.append("ondblclick=\"setCodeAndSubmitForm('" + dblClickEvent + "',this);return true;\"" + NEW_LINE);
		    sb.append(">" + NEW_LINE);
		    sb.append("<td valign='top' class=\"dataValue\" width=\"30%\" >" + ad.getCode().trim() + "</td>" + NEW_LINE);
		    sb.append("<td valign='top' class=\"dataValue\" width=\"70%\">" + ad.getDescription().trim() + "</td>" + NEW_LINE);
		    sb.append("</tr>" + NEW_LINE);
		}
	    } else {
		sb.append("<tr><td colspan=\"2\"><b>" + noneSelectedMsg + "</b></td></tr>" + NEW_LINE);
	    }
	} else {
	    sb.append("<tr><td colspan=\"2\"><b>" + noneSelectedMsg + "</b></td></tr>" + NEW_LINE);
	}
	sb.append("</table>" + NEW_LINE);
	sb.append("</div>" + NEW_LINE);
	sb.append("</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("</table>" + NEW_LINE);

	return sb.toString();
    }

    public static String getSelectList(int queryType, String searchTerm, Collection list, String name, String javascript) {
	String optionValue = "";
	String html = "<select name=\"" + name + "\" onChange=\"" + javascript + "\"/>";
	LogUtil.log("", "list=" + list);
	if (list != null) {
	    Iterator listIt = list.iterator();
	    Object tmp = null;
	    String optionDesc = null;
	    while (listIt.hasNext()) {
		tmp = listIt.next();
		LogUtil.debug("", "test" + tmp);
		optionValue = (String) tmp;
		optionDesc = new String(optionValue); // new instance

		if (queryType == CommonMgrBean.CRITERIA_RESULTS) {
		    // if of form "10 results" then get the number only
		    optionValue = optionValue.split(" ")[0]; // get first
							     // element only
		}

		html += "<option value=\"" + optionValue + "\"";
		if (searchTerm != null && optionValue.equals(searchTerm)) {
		    html += " selected";
		}
		html += ">" + optionDesc;
	    }
	}
	html += "</select>";
	return html;
    }

    public static String selectList(String listName, Object selectFrom, String event) {
	return selectList(listName, (SortedSet) selectFrom, "ALL", "ALL", "ALL", true, event);
    }

    public static String selectList(String listName, Object selectFrom) {
	return selectList(listName, (SortedSet) selectFrom, "ALL", "ALL", "ALL", true, "");
    }

    public static String selectList(String listName, SortedSet selectFrom, String selectedValue, String defaultValue, String defaultDesc) {
	return selectList(listName, selectFrom, selectedValue, defaultValue, defaultDesc, false, "");
    }

    public static String selectList(String listName, SortedSet selectFrom, String selectedValue, String defaultValue, String defaultDesc, String event) {
	return selectList(listName, selectFrom, selectedValue, defaultValue, defaultDesc, false, event);
    }

    public static String selectList(String listName, SortedSet selectFrom, String selectedValue, String defaultValue, String defaultDesc, boolean multipleSelect) {
	return selectList(listName, selectFrom, selectedValue, defaultValue, defaultDesc, multipleSelect, "");
    }

    public static String selectList(String listName, SortedSet selectFrom, String selectedValue, String defaultValue, String defaultDesc, boolean multipleSelect, String event) {
	StringBuffer sb = new StringBuffer();

	String sv = StringUtil.isNull(selectedValue) ? "" : selectedValue.trim();

	sb.append("<select name=\"" + listName + "\"");
	if (multipleSelect) {
	    sb.append(" size=\"6\" multiple ");
	}
	if (event != null) {
	    sb.append(event);
	    sb.append(" ");
	}
	sb.append(">" + NEW_LINE);

	if (defaultValue != null) {
	    sb.append("<option value='" + defaultValue + "'");
	    if (defaultValue.trim().equalsIgnoreCase(sv)) {
		sb.append(" selected ");
	    }
	    sb.append(">" + defaultDesc + "</option>" + NEW_LINE);
	}
	if (!(selectFrom == null)) {
	    Iterator it = selectFrom.iterator();
	    while (it.hasNext()) {
		AdminCodeDescription ad = (AdminCodeDescription) it.next();
		sb.append("<option value='" + ad.getCode() + "'");

		if (ad.getCode().trim().equalsIgnoreCase(sv)) {
		    sb.append("selected");
		}
		sb.append(">" + ad.getDescription() + "</option>" + NEW_LINE);
	    }
	}

	sb.append("</select>" + NEW_LINE);

	return sb.toString();
    }

    public static String inputOrReadOnly(String name, String value) {
	return inputOrReadOnly(name, value, "");
    }

    public static String inputOrReadOnly(String name, String value, String event) {
	StringBuffer sb = new StringBuffer();

	sb.append("<input type=\"text\" name=\"" + name + "\" value=\"" + StringUtil.makeSpaces(value) + "\" ");
	if (!StringUtil.isNull(value)) {
	    if (!(value.equalsIgnoreCase((new DateTime()).formatShortDate()))) {
		sb.append(" readonly ");
	    }
	} else {
	    if (!StringUtil.isNull(event)) {
		sb.append(event);
	    }
	}
	sb.append(">" + NEW_LINE);

	return sb.toString();
    }

    //
    public static String inputTextBox(String name, String value, String event) {
	return StringUtil.replace(inputOrReadOnly(name, value, event), "readonly ", " ");
    }

    public static String formatShortDate(DateTime d) {
	String fd = DateUtil.formatShortDateTime(d);
	if (fd == null || fd.length() < 1) {
	    fd = "&nbsp;";
	}
	return fd;
    }

    public static String dataTable(String tableHeading, ArrayList columnHeadings, ArrayList rowList, String dblClickEvent, String dblClickMsg, String noneSelectedMsg) {
	StringBuffer sb = new StringBuffer();
	int cols = columnHeadings.size();
	sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);
	sb.append("<td class=\"label\" width=\"40%\"><b>" + tableHeading + "</b></td>" + NEW_LINE);
	sb.append("<td class=\"helpText\" width=\"60%\" align=\"right\">Double Click highlighted row to edit.</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);
	sb.append("<td colspan=\"" + cols + "\">" + NEW_LINE);
	sb.append("<table width=\"100%\" border=\"0\">" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);

	Iterator hdIt = columnHeadings.iterator();
	while (hdIt.hasNext()) {
	    HashMap cell = (HashMap) hdIt.next();
	    String val = (String) cell.get("value");
	    String algn = (String) cell.get("align");
	    String wdth = (String) cell.get("width");

	    sb.append("<td bgColor=\"#CDD7E1\" ");
	    if (!(algn == null)) {
		sb.append("align=\"" + algn + "\" ");
	    }
	    if (!(wdth == null)) {
		sb.append("width=\"" + wdth + "\" ");
	    }
	    sb.append("class=\"label\" >");
	    sb.append(val + "</td>" + NEW_LINE);
	}
	sb.append("</tr>" + NEW_LINE);
	sb.append("</table>" + NEW_LINE);
	sb.append("</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("<tr>" + NEW_LINE);
	sb.append("<td colspan=\"" + cols + "\">" + NEW_LINE);
	sb.append("<div style=\" height:90;overflow:auto; border-width: 1; border-style: solid;\">" + NEW_LINE);
	sb.append("<table width=\"100%\" border=\"0\">" + NEW_LINE);
	//

	if (!(rowList == null)) {
	    if (rowList.size() > 0) {
		int rowNum = 0;
		Iterator it = rowList.iterator();
		while (it.hasNext()) {
		    rowNum += 1;

		    ArrayList colList = (ArrayList) it.next();
		    Iterator colIt = colList.iterator();

		    HashMap cell = (HashMap) colIt.next();
		    String rowId = (String) cell.get("rowId");
		    // String algn = (String) cell.get("align");
		    // String wdth = (String) cell.get("width");

		    sb.append("<tr name=\"row" + rowId + "\"" + NEW_LINE);
		    sb.append("onMouseOver=\"bgColor='#CDD7E1';MM_displayStatusMsg('" + dblClickMsg + "');\"" + NEW_LINE);
		    sb.append("onMouseOut=\"bgColor='#FFFFFF'\"" + NEW_LINE);
		    sb.append("bgcolor=\"#FFFFFF\"" + NEW_LINE);
		    sb.append("ondblclick=\"moveToEditRow(this);return true;\"" + NEW_LINE);
		    sb.append(">" + NEW_LINE);

		    while (colIt.hasNext()) {
			cell = (HashMap) colIt.next();
			String val = (String) cell.get("value");
			String algn = (String) cell.get("align");
			String wdth = (String) cell.get("width");

			sb.append("<td valign='top' ");
			if (!(algn == null)) {
			    sb.append("align=\"" + algn + "\" ");
			}
			if (!(wdth == null)) {
			    sb.append("width=\"" + wdth + "\" ");
			}
			sb.append("class=\"dataValue\" >");
			sb.append(val + "</td>" + NEW_LINE);
			// sb.append("<td valign='top' class=\"dataValue\" width=\"30%\" >"
			// + ad.getCode().trim() + "</td>" + NEW_LINE);
			// sb.append("<td valign='top' class=\"dataValue\" width=\"70%\">"
			// + ad.getDescription().trim() + "</td>" + NEW_LINE);
		    }
		    sb.append("</tr>" + NEW_LINE);
		}
	    } else {
		sb.append("<tr><td colspan=\"" + cols + "\"><b>" + noneSelectedMsg + "</b></td></tr>" + NEW_LINE);
	    }
	} else {
	    sb.append("<tr><td colspan=\"" + cols + "\"><b>" + noneSelectedMsg + "</b></td></tr>" + NEW_LINE);
	}
	sb.append("</table>" + NEW_LINE);
	sb.append("</div>" + NEW_LINE);
	sb.append("</td>" + NEW_LINE);
	sb.append("</tr>" + NEW_LINE);
	sb.append("</table>" + NEW_LINE);

	return sb.toString();
    }

    public static String OkButton(String okEvent, String code) {
	StringBuffer sb = new StringBuffer();

	sb.append("<span>");
	sb.append("<a  onclick=\"submitForm('" + okEvent + "');return true;\"" + NEW_LINE);
	sb.append("onMouseOut=\"MM_swapImgRestore();\"" + NEW_LINE);
	sb.append("onMouseOver=\"MM_displayStatusMsg('" + code + ".')");
	sb.append(";MM_swapImage('btnOK','','");
	sb.append(CommonConstants.DIR_IMAGES + "/OkButton_over.gif',1);");
	sb.append("return document.MM_returnValue\" >" + NEW_LINE);
	sb.append("<img  name=\"btnOK\"" + NEW_LINE);
	sb.append("src=\"" + CommonConstants.DIR_IMAGES + "/OkButton.gif\"" + NEW_LINE);
	sb.append("border=\"0\"" + NEW_LINE);
	sb.append("alt=\"" + code + ".\"" + NEW_LINE);
	sb.append("></a>");
	sb.append("</span>");

	return sb.toString();
    }

    /**
     * Process the xml document and convert to html results.
     * 
     * @param xmlString
     * @return
     * @throws SystemException
     */
    public static String buildHTMLResults(String xmlString) throws SystemException {
	StringBuffer htmlResults = new StringBuffer();
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	factory.setValidating(false);

	Document doc = null;

	try {
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    ByteArrayInputStream xmlDocStream = new ByteArrayInputStream(xmlString.getBytes());
	    doc = builder.parse(xmlDocStream);

	    // at result set level
	    Node resultset = doc.getFirstChild(); // row

	    // get rows
	    NodeList rows = resultset.getChildNodes();
	    NodeList values = null;

	    Node elementNode = null;
	    Node item = null;
	    String nodeName = null;
	    String valueText = null;
	    Node tmpValue = null;
	    // for each row

	    StringBuffer header = new StringBuffer();
	    StringBuffer rowValues = new StringBuffer();

	    int elementCounter = 0;

	    for (int i = 0; i < rows.getLength(); i++) {
		// get a node
		elementNode = rows.item(i);

		nodeName = elementNode.getNodeName();

		// System.out.println("nodeName"+nodeName+" "+i);
		// get attributes of row

		if (elementNode.getNodeType() == Node.ELEMENT_NODE) {
		    // row
		    NodeList row = elementNode.getChildNodes();

		    // System.out.println("is element");
		    elementCounter++;
		    // get attributes
		    rowValues.append("<tr class=\"" + HTMLUtil.getRowType(elementCounter) + "\">");
		    // for each element
		    for (int x = 0; x < row.getLength(); x++) {
			// System.out.println("x="+x);

			item = row.item(x);

			// item name
			if (item.getNodeType() == Node.ELEMENT_NODE) {
			    String itemName = item.getNodeName();
			    // System.out.println("itemName"+itemName);
			    String value = "";

			    values = item.getChildNodes();

			    for (int z = 0; z < values.getLength(); z++) {
				tmpValue = values.item(z);
				if (tmpValue.getNodeType() == Node.TEXT_NODE) {
				    // value
				    value = tmpValue.getNodeValue();
				    // System.out.println("value"+value);
				}
			    }
			    // write the heading - first time only!
			    if (elementCounter == 1) {
				header.append("<td>");
				header.append("<b>" + itemName + "</b>");
				header.append("</td>");
			    }
			    rowValues.append("<td>" + value + "</td>");
			}
		    }
		    rowValues.append("</tr>");
		}
	    }
	    if (rowValues.length() == 0) {
		rowValues.append("<td class=\"helpText\">No results found.</td>");
	    }
	    // construct table of results
	    htmlResults.append("<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\">");
	    htmlResults.append("<tr class=\"headerRow\">");
	    htmlResults.append(header.toString());
	    htmlResults.append("</tr>");
	    htmlResults.append(rowValues.toString());
	    htmlResults.append("</table>");
	} catch (Exception e) {
	    throw new SystemException(e);
	}
	return htmlResults.toString();
    }

}
