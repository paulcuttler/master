package com.ract.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

/**
 * <p>
 * Composite user type to combine the date and time elements together to return
 * as a DateTime attribute. This is a legacy of Progress where date and time
 * fields are stored in discrete columns.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class DateTimeCombined extends DateTime implements CompositeUserType {

    private static final int[] TYPES = { Types.DATE, Types.NUMERIC };

    public int[] sqlTypes() {
	return TYPES;
    }

    public Class returnedClass() {
	return DateTime.class;
    }

    public int hashCode(Object object) throws HibernateException {
	return object.hashCode();
    }

    public boolean equals(Object x, Object y) {
	LogUtil.debug(this.getClass(), "x=" + x);
	LogUtil.debug(this.getClass(), "y=" + y);
	if (x == y)
	    return true;
	if ((x == null) || (y == null))
	    return false;
	return x.equals(y);
    }

    public Object deepCopy(Object x) {
	LogUtil.debug(this.getClass(), "deepCopy=" + x);
	if (x == null)
	    return null;
	return new DateTime(((java.util.Date) x).getTime());
    }

    public boolean isMutable() {
	return true;
    }

    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
	// must not have a time component associated with the date
	java.sql.Date date = (java.sql.Date) Hibernate.DATE.nullSafeGet(rs, names[0]);
	BigDecimal time = (BigDecimal) Hibernate.BIG_DECIMAL.nullSafeGet(rs, names[1]);
	LogUtil.debug(this.getClass(), "date=" + date);
	LogUtil.debug(this.getClass(), "time=" + time);
	if (date == null && time == null) {
	    return null;
	}
	if (date == null) {
	    return new java.util.Date(time.longValue());
	}
	DateTime dt = new DateTime(date.getTime() + time.longValue());
	LogUtil.debug(this.getClass(), "dt=" + dt);
	return dt;
    }

    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
	java.util.Date dateTime = (java.util.Date) value;
	LogUtil.debug(this.getClass(), "dateTime=" + dateTime);
	// clear time component
	if (value != null) {
	    DateTime clearedDateTime = new DateTime(DateUtil.clearTime(dateTime));
	    LogUtil.debug(this.getClass(), "clearedDateTime=" + clearedDateTime);
	    java.sql.Date sd = clearedDateTime.toSQLDate();
	    Hibernate.DATE.nullSafeSet(st, sd, index);
	    LogUtil.debug(this.getClass(), "sd=" + sd);
	    // non cleared
	    BigDecimal hours = new BigDecimal(new DateTime(dateTime).getMillisecondsFromMidnight());
	    LogUtil.debug(this.getClass(), "hours=" + hours);
	    Hibernate.BIG_DECIMAL.nullSafeSet(st, hours, index + 1);
	}
    }

    public String[] getPropertyNames() {
	return new String[] { "date", "time" };
    }

    public Type[] getPropertyTypes() {
	return new Type[] { Hibernate.DATE, Hibernate.BIG_DECIMAL };
    }

    public Object getPropertyValue(Object component, int property) {
	if (property == 0) {
	    DateTime clearedDateTime = new DateTime(DateUtil.clearTime((java.util.Date) component));
	    LogUtil.debug(this.getClass(), "gpv clearedDateTime=" + clearedDateTime);
	    return clearedDateTime.toSQLDate();
	}
	if (property == 1) {
	    DateTime clearedDateTime = new DateTime((java.util.Date) component);
	    BigDecimal time = new BigDecimal(clearedDateTime.getMillisecondsFromMidnight());
	    LogUtil.debug(this.getClass(), "gpv time=" + time);

	    return time;
	}
	return null;
    }

    public void setPropertyValue(Object component, int property, Object value) {
	LogUtil.log(this.getClass(), "setPropertyValue = " + component.getClass());
	DateTime dateTime = new DateTime((java.util.Date) component);
	if (property == 0) {
	    dateTime.setTime(dateTime.getTime() + ((java.sql.Date) value).getTime());
	} else if (property == 1) {
	    dateTime.setTime(dateTime.getTime() + ((BigDecimal) value).longValue());
	}
    }

    public Object assemble(Serializable cached, SessionImplementor session, Object owner) {
	return deepCopy(cached);
    }

    public Serializable disassemble(Object value, SessionImplementor session) {
	return (Serializable) deepCopy(value);
    }

    public Object replace(Object object, Object object1, SessionImplementor session, Object object2) throws HibernateException {
	return deepCopy(object);
    }

}
