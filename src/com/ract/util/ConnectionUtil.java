package com.ract.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Static connection utility methods.
 * 
 * @author John Holliday
 * @version 1.0
 */

public class ConnectionUtil {
    private static final int TRANSACTION_ISOLATION_DEFAULT = Connection.TRANSACTION_READ_COMMITTED;

    public static void closeConnection(Connection connection, boolean defaultIsolation) {
	try {
	    if (connection != null) {
		if (defaultIsolation) {
		    connection.setTransactionIsolation(TRANSACTION_ISOLATION_DEFAULT);
		}
		connection.close();
	    }
	} catch (SQLException e) {
	    LogUtil.log("ConnectionUtil", "Error closing connection. " + e);
	}
    }

    public static void closeStatement(Statement statement) {
	try {
	    if (statement != null) {
		statement.close();
	    }
	} catch (SQLException e) {
	    LogUtil.log("ConnectionUtil", "Error closing statement. " + e);
	}
    }

    public static void closeConnection(Connection connection, Statement statement) {
	closeStatement(statement);
	closeConnection(connection);
    }

    public static void closeConnection(Connection connection) {
	closeConnection(connection, false);
    }

}
