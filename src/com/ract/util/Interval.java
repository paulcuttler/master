package com.ract.util;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import com.ract.common.SystemException;

/**
 * Specifies an interval of time in years, months, days, hours, minutes,
 * seconds, milliseconds. This class can be used to specify the difference in
 * time between two dates.
 * 
 * @author Terry Bakker, John Holliday
 * @version 1.0 3/3/2002
 */

public class Interval implements Serializable, Comparable, UserType {
    public int hashCode() {
	return (int) this.getTotalMilliseconds();
    }

    /**
     * Two intervals are equivalent if the total number of milliseconds of each
     * interval is equal
     */
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Interval))
	    return false;
	Interval other = (Interval) obj;
	if (other.getTotalMilliseconds() != this.getTotalMilliseconds()) {
	    return false;
	}
	return true;
    }

    public static final String DELIMITER = ":";

    // These constants define fixed calculation amounts
    public static final long MONTHS_IN_YEAR = 12;

    public static final long DAYS_IN_YEAR = 365; // Not dealing with leap years
						 // as we have no starting
						 // reference date.

    public static final long HOURS_IN_YEAR = DAYS_IN_YEAR * 24;

    public static final long MINUTES_IN_YEAR = HOURS_IN_YEAR * 60;

    public static final long SECONDS_IN_YEAR = MINUTES_IN_YEAR * 60;

    public static final long MILLISECONDS_IN_YEAR = SECONDS_IN_YEAR * 1000;

    public static final long MILLISECONDS_IN_SECOND = 1000;

    public static final long MILLISECONDS_IN_MINUTE = MILLISECONDS_IN_SECOND * 60;

    public static final long MILLISECONDS_IN_HOUR = MILLISECONDS_IN_MINUTE * 60;

    public static final long MILLISECONDS_IN_DAY = MILLISECONDS_IN_HOUR * 24;

    public static final long MILLISECONDS_IN_MONTH = MILLISECONDS_IN_YEAR / MONTHS_IN_YEAR;

    // These constants indicate how to format the interval for display
    public static final short FORMAT_YEAR = 0;

    public static final short FORMAT_MONTH = 1;

    public static final short FORMAT_DAY = 2;

    public static final short FORMAT_HOUR = 3;

    public static final short FORMAT_MINUTE = 4;

    public static final short FORMAT_SECOND = 5;

    public static final short FORMAT_MILLISECOND = 6;

    // These are the attributes that the interval is held in.
    private long years = 0;

    private long months = 0;

    private long days = 0;

    private long hours = 0;

    private long minutes = 0;

    private long seconds = 0;

    private long milliseconds = 0;

    private DateTime fromDate;

    private DateTime toDate;

    /***************************** Constructors ******************************/
    /**
     * @param intervalSpec
     *            - A specification for an interval. Use the format
     *            "years:months:days:hours:minutes:seconds:milliseconds".
     */
    public Interval(String intervalSpec) throws ParseException {
	setInterval(intervalSpec);
    }

    /**
     * explicitly set the interval
     */
    public Interval(int y, int mnth, int d, int h, int min, int s, int ms) throws Exception {
	setInterval(y, mnth, d, h, min, s, ms);
    }

    /**
     * Return true if the Interval has been specified with a from and to date
     * and the period defined by these two dates contains an extra day due to a
     * leap year.
     * 
     * @return boolean
     */
    public boolean containsLeapYear() {
	boolean leapYear = false;

	if (this.fromDate != null && this.toDate != null) {
	    GregorianCalendar leapDay = null;
	    boolean startLeap = DateUtil.isLeapYear(this.fromDate);
	    if (startLeap) {
		//
		leapDay = new GregorianCalendar(this.fromDate.getDateYear(), 1, 29);
		// LogUtil.log(this.getClass(),"from="+new
		// DateTime(leapDay.getTime()));
		if (fromDate.onOrBeforeDay(leapDay.getTime())) {
		    leapYear = true;
		}
	    }
	    boolean endLeap = DateUtil.isLeapYear(toDate);
	    if (endLeap) {
		leapDay = new GregorianCalendar(this.toDate.getDateYear(), 1, 29);
		// LogUtil.log(this.getClass(),"to="+new
		// DateTime(leapDay.getTime()));
		if (this.toDate.onOrAfterDay(leapDay.getTime())) {
		    leapYear = true;
		}
	    }
	    // LogUtil.log(this.getClass(),"fromDate="+fromDate+", toDate="+toDate+", leapYear="+leapYear);
	}
	return leapYear;
    }

    /**
     * Return the number of days in a year. If the interval has been specified
     * using a from and to date and the period defined by these two dates
     * contains an extra day due to a leap year then 366 is returned otherwise
     * 365 is returned.
     * 
     * @return the number of days in a year.
     */
    public long getDaysInYear() {
	long daysInYear = this.DAYS_IN_YEAR;
	// adjust for leap year - the term of membership is a year not
	// 365 days.
	if (containsLeapYear()) {
	    daysInYear++;
	}
	return daysInYear;
    }

    private void setDateRange(DateTime fromDate, DateTime toDate) {
	this.fromDate = fromDate;
	this.toDate = toDate;
    }

    /**
     * calculates years,months and days between two dates
     */
    public Interval(DateTime fromDate, DateTime toDate) throws Exception {
	// setIntervalDays(fromDate,toDate);
	setDateRange(fromDate, toDate);
    }

    /**
     * assumes todate is current date
     */
    public Interval(DateTime fromDate) throws Exception {
	DateTime now = (new DateTime()).getDateOnly();
	// setIntervalDays(fromDate,now);
	setDateRange(fromDate, now);
    }

    /**
     * Construct this interval as a copy of the passed interval.
     */
    public Interval(Interval interval) throws Exception {
	if (interval != null) {
	    this.years = interval.getYears();
	    this.months = interval.getMonths();
	    this.days = interval.getDays();
	    this.hours = interval.getHours();
	    this.minutes = interval.getMinutes();
	    this.seconds = interval.getSeconds();
	    this.milliseconds = interval.getMilliseconds();
	} else {
	    throw new Exception("Could not construct interval, the passed interval was null.");
	}
    }

    /**
     * Default constructor. Initialises all of the interval components to zero.
     */
    public Interval() {
    }

    /**************************** Getter methods *****************************/

    /**
     * Return the value of the years component
     * 
     * @return int
     */
    public int getYears() {
	return (int) this.years;
    }

    /**
     * Return the value of the months component
     * 
     * @return int
     */
    public int getMonths() {
	return (int) this.months;
    }

    /**
     * Return the value of the days component
     * 
     * @return int
     */
    public int getDays() {
	return (int) this.days;
    }

    /**
     * Return the value of the hours component
     * 
     * @return int
     */
    public int getHours() {
	return (int) this.hours;
    }

    /**
     * Return the value of the minutes component
     * 
     * @return int
     */
    public int getMinutes() {
	return (int) this.minutes;
    }

    /**
     * Return the value of the seconds component
     * 
     * @return int
     */
    public int getSeconds() {
	return (int) this.seconds;
    }

    /**
     * Return the value of the milliseconds component.
     * 
     * @return int
     */
    public int getMilliseconds() {
	return (int) this.milliseconds;
    }

    /**
     * Return the total years by rolling up the months, days, hours, seconds and
     * milliseconds
     * 
     * @return int
     */
    public int getTotalYears() {
	if (isDateRangeBased()) {
	    return DateUtil.getDateDiff(fromDate, toDate, Calendar.YEAR);
	} else {
	    return (int) (getTotalMilliseconds() / this.MILLISECONDS_IN_YEAR);
	}
    }

    /**
     * Return the total months.
     */
    public int getTotalMonths() {
	if (isDateRangeBased()) {
	    return DateUtil.getDateDiff(fromDate, toDate, Calendar.MONTH);
	} else {
	    return (int) (getTotalMilliseconds() / this.MILLISECONDS_IN_MONTH);
	}
    }

    /**
     * Return the total days.
     */
    public int getTotalDays() {
	if (isDateRangeBased()) {
	    return DateUtil.getDateDiff(fromDate, toDate, Calendar.DATE);
	} else {
	    return (int) (getTotalMilliseconds() / this.MILLISECONDS_IN_DAY);
	}
    }

    /**
     * Return the total hours.
     */
    public int getTotalHours() {
	return (int) (getTotalMilliseconds() / this.MILLISECONDS_IN_HOUR);
    }

    /**
     * Return the total minutes.
     */
    public int getTotalMinutes() {
	return (int) (getTotalMilliseconds() / this.MILLISECONDS_IN_MINUTE);
    }

    /**
     * Return the total seconds.
     */
    public long getTotalSeconds() {
	return getTotalMilliseconds() / this.MILLISECONDS_IN_SECOND;
    }

    /**
     * Return the total milliseconds by rolling up the years, months, days,
     * hours, seconds and milliseconds
     */
    public long getTotalMilliseconds() {
	long total = this.milliseconds;
	total += this.years * MILLISECONDS_IN_YEAR;
	total += this.months * MILLISECONDS_IN_MONTH;
	total += this.days * MILLISECONDS_IN_DAY;
	total += this.hours * MILLISECONDS_IN_HOUR;
	total += this.minutes * MILLISECONDS_IN_MINUTE;
	total += this.seconds * MILLISECONDS_IN_SECOND;
	return total;
    }

    /**
     * Get the interval represented as a double value for the given precision.
     * Use the FORMAT_* constants to specify the precision.
     */
    public double doubleValue(short precision) {
	double dVal = 0.0;
	double totalMS = (double) getTotalMilliseconds();
	if (precision == this.FORMAT_YEAR) {
	    dVal = totalMS / (double) this.MILLISECONDS_IN_YEAR;
	} else if (precision == this.FORMAT_MONTH) {
	    dVal = totalMS / (double) this.MILLISECONDS_IN_MONTH;
	} else if (precision == this.FORMAT_DAY) {
	    dVal = totalMS / (double) this.MILLISECONDS_IN_DAY;
	} else if (precision == this.FORMAT_HOUR) {
	    dVal = totalMS / (double) this.MILLISECONDS_IN_HOUR;
	} else if (precision == this.FORMAT_MINUTE) {
	    dVal = totalMS / (double) this.MILLISECONDS_IN_MINUTE;
	} else if (precision == this.FORMAT_YEAR) {
	    dVal = totalMS / (double) this.MILLISECONDS_IN_YEAR;
	} else {
	    dVal = totalMS;

	}
	return dVal;
    }

    /**************************** Setter methods *****************************/

    public void setInterval(String intervalSpec) throws ParseException {
	if (intervalSpec == null || intervalSpec.length() < 1) {
	    throw new ParseException("The interval specification cannot be empty.", 0);
	}

	// The interval spec may not be complete so initialise all components
	// to zero.
	int[] spec = { 0, 0, 0, 0, 0, 0, 0 };
	String[] errComponent = { "years", "months", "days", "hours", "seconds", "milliseconds" };

	// Get the components from the interval specification and convert them
	// to integers.
	// StringTokenizer st = new StringTokenizer(intervalSpec, DELIMITER);
	String st[] = intervalSpec.split(DELIMITER);
	// int count = 0;

	for (int i = 0; i < st.length && i < spec.length; i++) {
	    // while(st.hasMoreTokens() && count < 7)
	    // {
	    String token = st[i];
	    try {
		spec[i] = Integer.parseInt(token);
		// count++;
	    } catch (Exception e) {
		throw new ParseException("The " + errComponent[i] + " component (" + token + ") of the interval specification '" + intervalSpec + "' is not a valid integer.", i * 2);
	    }
	}

	// Set the component values
	try {
	    setInterval(spec[0], spec[1], spec[2], spec[3], spec[4], spec[5], spec[6]);
	} catch (Exception e) {
	    throw new ParseException("The interval specification '" + intervalSpec + "' is not valid : " + e.getMessage(), 0);
	}

    }

    /**
     * explicitly set the interval
     */
    public void setInterval(int y, int mnth, int d, int h, int min, int s, int ms) throws Exception {
	setYears(y);
	setMonths(mnth);
	setDays(d);
	setHours(h);
	setMinutes(min);
	setSeconds(s);
	setMilliseconds(ms);
    }

    private boolean isDateRangeBased() {
	if (this.toDate != null && this.fromDate != null) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Sets an interval between two dates. Is only accurate to the day. Counts
     * days from the end of the from date to the end of the to date.
     */
    public void setIntervalDays(DateTime fromDate, DateTime toDate) throws Exception {
	if (fromDate == null) {
	    throw new SystemException("The fromDate cannot be null.");
	}

	if (toDate == null) {
	    throw new SystemException("The toDate cannot be null.");
	}

	long fromDateTime = DateUtil.clearTime(fromDate).getTime();
	long toDateTime = DateUtil.clearTime(toDate).getTime();

	if (toDateTime < fromDateTime) {
	    throw new SystemException("The from date '" + fromDate + "' must be before the to date '" + toDate + "'");
	}

	int daysBetween = (int) ((toDateTime - fromDateTime) / MILLISECONDS_IN_DAY);
	if (daysBetween >= 0) {
	    this.setDays(daysBetween);
	}
    }

    /**
     * set year component
     */
    public void setYears(int y) throws Exception {
	if (y < 0) {
	    throw new Exception("Failed to set Interval years. The years cannot be negative.");
	}
	this.years = y;
    }

    /**
     * set month component
     */
    public void setMonths(int mnth) throws Exception {
	if (mnth < 0) {
	    throw new Exception("Failed to set Interval months. The months cannot be negative.");
	}
	this.months = mnth;
    }

    /**
     * set day component
     */
    public void setDays(int d) throws Exception {
	if (d < 0) {
	    throw new Exception("Failed to set Interval days. The days cannot be negative.");
	}
	this.days = d;
    }

    /**
     * set hour component
     */
    public void setHours(int h) throws Exception {
	if (h < 0) {
	    throw new Exception("Failed to set Interval hours. The hours cannot be negative.");
	}
	this.hours = h;
    }

    /**
     * set minute component
     */
    public void setMinutes(int min) throws Exception {
	if (min < 0) {
	    throw new Exception("Failed to set Interval minutes. The minutes cannot be negative.");
	}
	this.minutes = min;
    }

    /**
     * set seconds component
     */
    public void setSeconds(int s) throws Exception {
	if (s < 0) {
	    throw new Exception("Failed to set Interval seconds. The seconds cannot be negative.");
	}
	this.seconds = s;
    }

    /**
     * set milliseconds component
     */
    public void setMilliseconds(int ms) throws Exception {
	if (ms < 0) {
	    throw new Exception("Failed to set Interval milliseconds. The milliseconds cannot be negative.");
	}
	this.milliseconds = ms;
    }

    /************************** Processing methods ***************************/

    /**
     * Add an interval to this interval.
     * 
     * @return A reference to this interval.
     */
    public Interval add(Interval interval) {
	if (interval != null) {
	    this.milliseconds += interval.getMilliseconds();
	    this.seconds += interval.getSeconds();
	    this.minutes += interval.getMinutes();
	    this.days += interval.getDays();
	    this.months += interval.getMonths();
	    this.years += interval.getYears();
	}
	return this;
    }

    /**
     * Add the specified years to the interval
     * 
     * @return A reference to this interval.
     */
    public Interval addYears(int yrs) {
	this.years += yrs;
	return this;
    }

    /**
     * Add the specified months to the interval
     * 
     * @return A reference to this interval.
     */
    public Interval addMonths(int mnths) {
	this.months += mnths;
	return this;
    }

    /**
     * Add the specified days to the interval
     * 
     * @return A reference to this interval.
     */
    public Interval addDays(int days) {
	this.days += days;
	return this;
    }

    /**
     * Add the specified hours to the interval
     * 
     * @return A reference to this interval.
     */
    public Interval addHours(int hrs) {
	this.hours += hrs;
	return this;
    }

    /**
     * Add the specified minutes to the interval
     * 
     * @return A reference to this interval.
     */
    public Interval addMinutes(int min) {
	this.minutes += min;
	return this;
    }

    /**
     * Add the specified seconds to the interval
     * 
     * @return A reference to this interval.
     */
    public Interval addSeconds(int sec) {
	this.seconds += sec;
	return this;
    }

    /**
     * Add the specified milliseconds to the interval
     * 
     * @return A reference to this interval.
     */
    public Interval addMilliseconds(int ms) {
	this.milliseconds += ms;
	return this;
    }

    /**
     * Return -1 if the passed interval is less than the objects interval.
     * Return 0 if the passed interval is the same as the objects interval.
     * Return 1 if the passed interval is greater than the objects interval.
     * 
     * @return int
     */
    public int compareTo(Object obj) {
	if (obj == null) {
	    return 0;
	}

	if (obj instanceof Interval) {
	    Interval that = (Interval) obj;
	    long thisTotalMilliseconds = this.getTotalMilliseconds();
	    long thatTotalMilliseconds = that.getTotalMilliseconds();
	    if (thisTotalMilliseconds > thatTotalMilliseconds) {
		return 1;
	    } else if (thisTotalMilliseconds < thatTotalMilliseconds) {
		return -1;
	    } else {
		return 0;
	    }
	} else {
	    return 0;
	}
    }

    /**
     * Return a copy of this interval
     */
    public Interval copy() throws Exception {
	return new Interval(this);
    }

    /*************************** Formatting methods **************************/

    /**
     * Return a string representation of the interval. Has the format,
     * "years:months:days:hours:minutes:seconds:milliseconds".
     */
    public String toString() {
	StringBuffer str = new StringBuffer();
	str.append(this.years);
	str.append(DELIMITER);
	str.append(this.months);
	str.append(DELIMITER);
	str.append(this.days);
	str.append(DELIMITER);
	str.append(this.hours);
	str.append(DELIMITER);
	str.append(this.minutes);
	str.append(DELIMITER);
	str.append(this.seconds);
	str.append(DELIMITER);
	str.append(this.milliseconds);
	return str.toString();
    }

    public String formatForDisplay(short startComponent, short endComponent) {
	StringBuffer output = new StringBuffer();
	long totalMilliseconds = getTotalMilliseconds();
	int yrs = 0;
	int mnths = 0;
	int dys = 0;
	int hrs = 0;
	int min = 0;
	int sec = 0;
	long msec = 0;

	if (isDateRangeBased()) {
	    switch (startComponent) {
	    case 0:
		// this is total years
		yrs = this.getTotalYears();
	    case 1:
		// this is total months (not remainder months)
		mnths = this.getTotalMonths();
	    case 2:
		// this is total days (not remainder days)
		dys = this.getTotalDays();
	    }
	} else {
	    switch (startComponent) {
	    case 0: // this.FORMAT_YEAR
		yrs = (int) (totalMilliseconds / MILLISECONDS_IN_YEAR);
		totalMilliseconds -= yrs * MILLISECONDS_IN_YEAR;
	    case 1: // this.FORMAT_MONTH
		mnths = (int) (totalMilliseconds / MILLISECONDS_IN_MONTH);
		totalMilliseconds -= mnths * MILLISECONDS_IN_MONTH;
	    case 2: // this.FORMAT_DAY
		dys = (int) (totalMilliseconds / MILLISECONDS_IN_DAY);
		totalMilliseconds -= dys * MILLISECONDS_IN_DAY;
	    case 3: // this.FORMAT_HOUR
		hrs = (int) (totalMilliseconds / MILLISECONDS_IN_HOUR);
		totalMilliseconds -= hrs * MILLISECONDS_IN_HOUR;
	    case 4: // this.FORMAT_MINUTE
		min = (int) (totalMilliseconds / MILLISECONDS_IN_MINUTE);
		totalMilliseconds -= min * MILLISECONDS_IN_MINUTE;
	    case 5: // this.FORMAT_SECOND
		sec = (int) (totalMilliseconds / MILLISECONDS_IN_SECOND);
		totalMilliseconds -= sec * MILLISECONDS_IN_SECOND;
	    case 6: // this.FORMAT_MILLISECOND
		msec = totalMilliseconds;
	    }
	}

	if (yrs > 0) {
	    output.append(yrs);
	    output.append(" year");
	    if (yrs > 1) {
		output.append('s');
	    }
	}

	if (mnths > 0 && endComponent >= this.FORMAT_MONTH) {
	    if (output.length() > 0) {
		output.append(", ");
	    }
	    output.append(mnths);
	    output.append(" month");
	    if (mnths > 1) {
		output.append('s');
	    }
	}

	if (dys > 0 && endComponent >= this.FORMAT_DAY) {
	    if (output.length() > 0) {
		output.append(", ");
	    }
	    output.append(dys);
	    output.append(" day");
	    if (dys > 1) {
		output.append('s');
	    }
	}

	if (hrs > 0 && endComponent >= this.FORMAT_HOUR) {
	    if (output.length() > 0) {
		output.append(", ");
	    }
	    output.append(hrs);
	    output.append(" hour");
	    if (hrs > 1) {
		output.append('s');
	    }
	}

	if (min > 0 && endComponent >= this.FORMAT_MINUTE) {
	    if (output.length() > 0) {
		output.append(", ");
	    }
	    output.append(min);
	    output.append(" minute");
	    if (min > 1) {
		output.append('s');
	    }
	}

	if (sec > 0 && endComponent >= this.FORMAT_SECOND) {
	    if (output.length() > 0) {
		output.append(", ");
	    }
	    output.append(sec);
	    output.append(" second");
	    if (sec > 1) {
		output.append('s');
	    }
	}

	if (msec > 0 && endComponent >= this.FORMAT_MILLISECOND) {
	    if (output.length() > 0) {
		output.append(", ");
	    }
	    output.append(msec);
	    output.append(" millisecond");
	    if (msec > 1) {
		output.append('s');
	    }
	}

	return output.toString();
    }

    int TYPES[] = { Types.VARCHAR };

    public int[] sqlTypes() {
	return TYPES;
    }

    public Class returnedClass() {
	return com.ract.util.Interval.class;
    }

    public int hashCode(Object object) throws HibernateException {
	return object.hashCode();
    }

    public boolean equals(Object x, Object y) throws HibernateException {
	if (x == y) {
	    return true;
	}
	if (x == null) {
	    return false;
	}
	return x.equals(y);
    }

    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException {
	LogUtil.debug(this.getClass(), "nullSafeGet " + names[0] + " " + owner);

	String interval = resultSet.getString(names[0]);

	LogUtil.debug(this.getClass(), "nullSafeGet" + names[0] + " " + interval);
	if (interval != null) {

	    try {
		return new Interval(interval);
	    } catch (ParseException ex) {
		LogUtil.log(this.getClass(), "Unable to parse interval." + interval + "," + ex);
		return null;
	    }
	} else {
	    return null;
	}
    }

    public void nullSafeSet(PreparedStatement statement, Object value, int index) throws HibernateException, SQLException {
	LogUtil.debug(this.getClass(), "nullSafeSet " + index + ", " + value);

	if (value == null) {
	    statement.setNull(index, Types.VARCHAR);
	} else {
	    Interval interval = (Interval) value;

	    // clear milliseconds

	    statement.setString(index, interval.toString());
	}
    }

    public Object deepCopy(Object value) throws HibernateException {
	LogUtil.debug(this.getClass(), "deepCopy " + value);
	if (value == null) {
	    return null;
	} else {
	    Interval interval = (Interval) value;
	    try {
		return new Interval(interval.toString());
	    } catch (ParseException ex) {
		LogUtil.log(this.getClass(), "Unable to parse interval." + interval + "," + ex);
		return null;
	    }
	}
    }

    public boolean isMutable() {
	return true;
    }

    public Serializable disassemble(Object value) throws HibernateException {
	LogUtil.debug(this.getClass(), "disassemble");
	return (Serializable) value;
    }

    public Object assemble(Serializable cached, Object owner) throws HibernateException {
	LogUtil.debug(this.getClass(), "assemble");
	return cached;
    }

    public Object replace(Object object, Object object1, Object object2) throws HibernateException {
	LogUtil.debug(this.getClass(), "replace");
	return deepCopy(object);
    }

}
