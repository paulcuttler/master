package com.ract.util;

public class AddressUtil {

    public static Integer getStreetNumberFromStreetChar(String streetChar) {
	Integer streetNumber;

	/*
	 * if property qualifier is not null then manipulate it to get the
	 * actual number.
	 */

	streetNumber = null;

	if ((streetChar != null) && (!streetChar.trim().equals(""))) {
	    int lastNumberPos = lastPos(streetChar);
	    if (lastNumberPos >= 0) {
		int firstNumberPos = firstPos(streetChar, lastNumberPos);

		try {
		    streetNumber = new Integer(streetChar.substring(firstNumberPos, (lastNumberPos + 1)));
		} catch (Exception ex) {
		    LogUtil.fatal("AddressUtil ", "Error parsing street number for " + streetChar);
		}
	    }
	}

	return streetNumber;
    }

    /**
     * Locate the last position by iterating through the characters in the
     * string in reverse order and finding the first number encountered.
     * 
     * @param s
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    private static int lastPos(String s) {
	char c[] = s.toCharArray();

	for (int i = (c.length - 1); i >= 0; i--) {
	    if ((c[i] >= '0') && (c[i] <= '9')) {
		// System.out.println("s: "+ c[ i ]+", "+i);
		return i;
	    }
	}

	return -1;
    }

    /**
     * Locate the first position by iterating through the characters in the
     * string in reverse order (from the last position) and finding the first
     * number encountered.
     * 
     * @param s
     *            Description of the Parameter
     * @param n
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    private static int firstPos(String s, int n) {
	char c[] = s.toCharArray();

	for (int i = n; i >= 0; i--) {
	    if ((c[i] < '0') || (c[i] > '9')) {
		// System.out.println("e: "+ c[ i ]+", "+i);
		return ++i;
	    }
	}

	return 0;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	// TODO Auto-generated method stub

    }

}
