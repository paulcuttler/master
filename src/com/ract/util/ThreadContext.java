package com.ract.util;

public class ThreadContext {
	
	public static final ThreadLocal<Context> threadContext = new ThreadLocal<Context>();
	
	
	public static void set(Context ctx) {
		threadContext.set(ctx);
	}

	public static void unset() {
		threadContext.remove();
	}

	public static Context get() {
		return threadContext.get();
	}
	
}
