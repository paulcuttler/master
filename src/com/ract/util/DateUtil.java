package com.ract.util;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Class with static methods to format and parse java dates
 * 
 * @author John Holliday
 * @version 1.0, 14/2/2001
 */

public class DateUtil {

    public static String[] getMonthStrings() {
	String[] months = new DateFormatSymbols().getMonths();
	int lastIndex = months.length - 1;

	if (months[lastIndex] == null || months[lastIndex].length() <= 0) { // last
									    // item
									    // empty
	    String[] monthStrings = new String[lastIndex];
	    System.arraycopy(months, 0, monthStrings, 0, lastIndex);
	    return monthStrings;
	} else { // last item not empty
	    return months;
	}
    }

    public static boolean isLeapYear(DateTime date) {
	GregorianCalendar gc1 = new GregorianCalendar();
	return gc1.isLeapYear(date.getDateYear());
    }

    public static DateTime nullOrDate(String thisDate) {
	DateTime newDate = null;

	if (!StringUtil.isNull(thisDate) && !(thisDate.length() < 1)) {

	    try {
		newDate = new DateTime(DateUtil.convertStringToDateTime(thisDate));
	    } catch (Exception p) {
		newDate = null;
	    }
	} else {
	    newDate = null;
	}

	return newDate;
    }

    /**
     * return a datestamp as a long
     */
    public String getDateStamp() {
	return String.valueOf((new Date()).getTime());
    }

    /**
     * Short date format
     */
    public static final String DATE_FORMAT_SHORT = "dd/MM/yyyy";

    /**
     * Medium date format
     */
    public static final String DATE_FORMAT_MEDIUM = "dd/MM/yyyy HH:mm:ss";

    /**
     * Long date format
     */
    public static final String DATE_FORMAT_LONG = "dd/MM/yyyy HH:mm:ss.SSS";
    public static final String DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT_FULL = "yyyyMMddHHmmss";

    /**
     * The date pattern list defines the list of allowed date format patterns
     * that users may type dates as. Make sure that the data formats with 2
     * digit years are specified first otherwise the parser will fill in 00 for
     * the century and convert 02 to 0002.
     */
    public static final String[] DATE_PATTERN_LIST = { DATE_FORMAT_LONG, DATE_FORMAT_MEDIUM, DATE_FORMAT_SHORT, DATE_FORMAT_ISO, DATE_FORMAT_FULL, "dd/MM/yy", "dd-MM-yy", "dd.MM.yy", "d/M/yy", "d-M-yy", "d.M.yy", "d MMM yy", "dd MMM yy", "dd-MM-yyyy", "dd.MM.yyyy", "d/M/yyyy", "d-M-yyyy", "d.M.yyyy", "d MMM yyyy", "dd MMM yyyy" };

    /**
     * figure out if the day should be suffixed with st, nd, rd, or th as in
     * 1st,2nd, 3rd etc
     * 
     * @param day
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static String getDaySuffix(int day) {
	int lastDigit;
	int tens;
	final int TEN = 10;
	lastDigit = day % TEN;
	tens = (day / TEN) % TEN;
	if (tens == 1) {
	    return "th";
	} else if (lastDigit == 1) {
	    return "st";
	} else if (lastDigit == 2) {
	    return "nd";
	} else if (lastDigit == 3) {
	    return "rd";
	} else {
	    return "th";
	}
    }

    /**
     * Returns a date given an int day, month and year
     * 
     * @param dd
     *            day of the month
     * @param MM
     *            month of the year
     * @param yyyy
     *            the year
     * @return converted date
     */
    public static Date getDate(int dd, int MM, int yyyy) {
	GregorianCalendar c = new GregorianCalendar(yyyy, (MM - 1), dd);
	return c.getTime();
    }

    /**
     * Returns a short string representation for the given date
     * 
     * @param date
     *            the string date
     * @return the string representation
     */
    public static String formatShortDateTime(DateTime date) {
	if (date != null) {
	    return date.toString();
	} else {
	    return "";
	}
    }

    /**
     * Returns a short string representation for the given date
     * 
     * @param date
     *            the string date
     * @return the string representation
     */
    public static String formatShortDate(Date date) {
	String thisDate = "";
	if (date != null) {
	    thisDate = formatDate(date, DATE_FORMAT_SHORT);
	}
	return thisDate;
    }

    /**
     * Return the current date / time in the long date format
     */
    public static String formatLongDate() {
	return formatDate(new Date(), DATE_FORMAT_LONG);
    }

    public static String formatMediumDate(DateTime date) {
	String thisDate = "";
	if (date != null) {
	    thisDate = formatDate(date, DATE_FORMAT_MEDIUM);
	}
	return thisDate;
    }

    /**
     * Formats a long string representation for the given date
     * 
     * @param date
     *            the string date
     * @return the string representation
     */
    public static String formatLongDate(Date date) {
	return formatDate(date, DATE_FORMAT_LONG);
    }

    /**
     * Formats a string representation for the given date according to the given
     * format mask. Returns an empty string if the passed date or the format
     * mask is null.
     * 
     * @param date
     *            . The date to format.
     * @param formatMask
     *            . The mask to use when formating the date.
     * @return the string representation of the date
     */
    public static String formatDate(Date date, String formatMask) {
	if (date == null || formatMask == null) {
	    return null;
	}

	SimpleDateFormat ldf = new SimpleDateFormat(formatMask);
	return ldf.format(date);
    }

    /**
     * Converts a string to date time
     * 
     * @param date
     *            the string date to be converted
     * @return the converted date
     */
    public static DateTime convertStringToDateTime(String date) throws ParseException {
	// /DateFormat d = DateFormat.getDateTimeInstance();
	Date dd = null;
	// dd = d.parse(date);
	dd = parseDate(date);
	// dd = d.parse(date);
	return new DateTime(dd);
    }

    /**
     * Converts a string to date time
     * 
     * @param date
     *            the string date to be converted
     * @return the converted date
     */
    public static Date convertStringToDate(String date) throws ParseException {
	// DateFormat d = DateFormat.getDateInstance();
	Date dd = null;
	// dd = d.parse(date);
	dd = parseDate(date);
	return dd;
    }

    /**
     * Parse a string to a date by applying the set of know date format patterns
     * to the string until a valid date is produced.
     * 
     * @param date
     *            the string date
     * @return the parsed date
     */
    public static Date parseDate(String dateStr) throws ParseException {
	Date dd = null;
	SimpleDateFormat sdf = new SimpleDateFormat();
	sdf.setLenient(false);
	int listLength = DATE_PATTERN_LIST.length;
	for (int i = 0; dd == null && i < listLength; i++) {
	    try {
		sdf.applyPattern(DATE_PATTERN_LIST[i]);
		dd = sdf.parse(dateStr);
		// System.out.println(DATE_PATTERN_LIST[i]+" "+dd.toString());
	    } catch (Exception e) {
	    } // Ignore parse exceptions
	}
	return dd;
    }

    /**
     * Get the number of days between the two dates.
     */
    public static int getDays(Date d1, Date d2) {
	GregorianCalendar g1 = new GregorianCalendar();
	GregorianCalendar g2 = new GregorianCalendar();
	g1.setTime(d1);
	g2.setTime(d2);
	return getDays(g1, g2);
    }

    /**
     * Returns the number of days between two calendar instances.
     * 
     * @param g1
     *            calendar instance 1
     * @param g2
     *            calendar instance 2
     * @return int number of days between the two calendar instances
     */
    public static int getDays(GregorianCalendar g1, GregorianCalendar g2) {
	int elapsed = 0;
	GregorianCalendar gc1, gc2;

	if (g2.after(g1)) {
	    gc2 = (GregorianCalendar) g2.clone();
	    gc1 = (GregorianCalendar) g1.clone();
	} else {
	    gc2 = (GregorianCalendar) g1.clone();
	    gc1 = (GregorianCalendar) g2.clone();
	}

	gc1 = clearTime(gc1);
	gc1 = clearTime(gc1);

	// include the actual days
	// if gc1 before gc2 then add a day
	if (gc1.before(gc2)) {
	    elapsed++;
	}

	gc1.add(Calendar.DATE, 1);
	while (gc1.before(gc2)) {
	    gc1.add(Calendar.DATE, 1);
	    elapsed++;
	}
	return elapsed;
    }

    /**
     * Return the last day of the month in the given year. Need the year as the
     * last day of February changes depending on the year.
     * 
     * @param int month The month of the year to return the last day on. Use the
     *        GregorianCalendar month constants.
     * @param int year The year in which to return the month.
     */
    public static int getLastDayOfMonth(int month, int year) {
	GregorianCalendar gc = new GregorianCalendar(year, month, 1);
	return gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
    }

    public static int getLastDayOfMonth(DateTime d) {
	return getLastDayOfMonth(d.getMonthOfYear(), d.getDateYear());
    }

    /**
     * get the time difference in second between now and target date.
     */
    public static int getTimeDiff(long created) {
	long current = System.currentTimeMillis();
	int totalMins = (int) (current - created) / 60000;
	return totalMins;
    }

    /**
     * Calculate the number of days between two dates. Subtracts the first date
     * from the second. That is if the second date is after the first date,
     * returns a positive number, If the second date is before the first,
     * returns a negative number
     * 
     * @param Date
     *            d1 The first date
     * @param Date
     *            d2 The second date
     * @param int datePart The Calendar element that we want.
     * @return int the number of days between (signed)
     */
    public static int getDateDiff(Date d1, Date d2, int datePart) {
	if (datePart == Calendar.DATE) {
	    return getDays(d1, d2);
	} else if (datePart == Calendar.MONTH) {
	    return getMonths(d1, d2);
	} else if (datePart == Calendar.YEAR) {
	    return getYears(d1, d2);
	} else {
	    return 0;
	}
    }

    /**
     * Get the number of months between the two dates.
     */
    public static int getMonths(Date d1, Date d2) {
	GregorianCalendar g1 = new GregorianCalendar();
	GregorianCalendar g2 = new GregorianCalendar();
	g1.setTime(d1);
	g2.setTime(d2);
	return getMonths(g1, g2);
    }

    /**
     * Returns the number of months between two calendar instances.
     * 
     * @param g1
     *            calendar instance 1
     * @param g2
     *            calendar instance 2
     * @return int number of months between the two calendar instances
     */
    public static int getMonths(GregorianCalendar g1, GregorianCalendar g2) {
	int elapsed = 0;
	GregorianCalendar gc1, gc2;

	if (g2.after(g1)) {
	    gc2 = (GregorianCalendar) g2.clone();
	    gc1 = (GregorianCalendar) g1.clone();
	} else {
	    gc2 = (GregorianCalendar) g1.clone();
	    gc1 = (GregorianCalendar) g2.clone();
	}

	gc1 = clearTime(gc1);
	gc1.set(Calendar.DAY_OF_MONTH, 0);

	gc2 = clearTime(gc2);
	gc2.set(Calendar.DAY_OF_MONTH, 0);

	gc1.add(Calendar.MONTH, 1);
	// LogUtil.log("DateUtil","m1="+gc1.get(Calendar.MONTH)+",y="+gc1.get(Calendar.YEAR));
	// LogUtil.log("DateUtil","m2="+gc2.get(Calendar.MONTH)+",y="+gc2.get(Calendar.YEAR));
	while (gc1.before(gc2)) {
	    // LogUtil.log("DateUtil","am1="+gc1.get(Calendar.MONTH)+",y="+gc1.get(Calendar.YEAR));
	    // LogUtil.log("DateUtil","am2="+gc2.get(Calendar.MONTH)+",y="+gc2.get(Calendar.YEAR));
	    gc1.add(Calendar.MONTH, 1);
	    // LogUtil.log("DateUtil","bm1="+gc1.get(Calendar.MONTH)+",y="+gc1.get(Calendar.YEAR));
	    // LogUtil.log("DateUtil","bm2="+gc2.get(Calendar.MONTH)+",y="+gc2.get(Calendar.YEAR));
	    elapsed++;
	}
	return elapsed;
    }

    /**
     * Get the number of years between the two dates.
     */
    public static int getYears(Date d1, Date d2) {
	GregorianCalendar g1 = new GregorianCalendar();
	GregorianCalendar g2 = new GregorianCalendar();
	g1.setTime(d1);
	g2.setTime(d2);
	return getYears(g1, g2);
    }

    /**
     * Returns the number of years between two calendar instances.
     * 
     * @param g1
     *            calendar instance 1
     * @param g2
     *            calendar instance 2
     * @return int number of years between the two calendar instances
     */
    public static int getYears(GregorianCalendar g1, GregorianCalendar g2) {
	int elapsed = 0;
	GregorianCalendar gc1, gc2;

	if (g2.after(g1)) {
	    gc2 = (GregorianCalendar) g2.clone();
	    gc1 = (GregorianCalendar) g1.clone();
	} else {
	    gc2 = (GregorianCalendar) g1.clone();
	    gc1 = (GregorianCalendar) g2.clone();
	}

	gc1 = clearTime(gc1);
	gc2 = clearTime(gc2);

	gc1.add(Calendar.YEAR, 1);
	while (gc2.after(gc1) || gc1.equals(gc2)) {
	    gc1.add(Calendar.YEAR, 1);
	    elapsed++;
	}
	return elapsed;
    }

    public static int getSecondsFromMidnight(GregorianCalendar gc) {
	DateTime aDate = new DateTime(gc.getTime());
	return aDate.getSecondsFromMidnight();
    }

    /**
     * Return a new DateTime instance with the time fields cleared. Returns null
     * if the passed date is null.
     */
    public static DateTime clearTime(DateTime d) {
	if (d != null) {
	    GregorianCalendar gc = new GregorianCalendar();
	    gc.setTime(d);
	    gc = clearTime(gc);
	    if (gc != null) {
		return new DateTime(gc.getTime());
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    /**
     * Return a new Date instance with the time fields set to zero. Returns null
     * if the passed date is null.
     */
    public static Date clearTime(Date d) {
	if (d != null) {
	    GregorianCalendar gc = new GregorianCalendar();
	    gc.setTime(d);
	    gc = clearTime(gc);
	    if (gc != null) {
		return gc.getTime();
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    /**
     * Return a the GregorianCalendar instance with the time fields set to zero.
     * Returns null if the passed calendar is null.
     */
    public static GregorianCalendar clearTime(GregorianCalendar gc) {
	if (gc != null) {
	    gc.set(GregorianCalendar.MILLISECOND, 0);
	    gc.set(GregorianCalendar.SECOND, 0);
	    gc.set(GregorianCalendar.MINUTE, 0);
	    gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
	    return gc;
	} else {
	    return null;
	}
    }

    /**
     * Retrieves a required part of a date from a date object
     * 
     * @param Date
     *            d The date from which the part is to be extracted
     * @param int datePart The specified part to be extracted as defined in
     *        GregorianCalendar (static attributes)
     * @return int The number of the required part
     */
    private static int getDatePart(Date d, int datePart) {
	GregorianCalendar g1 = new GregorianCalendar();
	g1.setTime(d);
	return g1.get(datePart);
    }

    /**
     * Retrieves the Day part of a date as an integer - the day of the month.
     * 
     * @param Date
     *            d The date from which the day of the month is required
     * @return int The day of the month
     */
    public static int getDayOfMonth(Date d) {
	return getDatePart(d, GregorianCalendar.DAY_OF_MONTH);
    }

    /**
     * Returns the month part of a date as in integer. Not zero referenced. That
     * is January = 1, Feb = 2 etc
     * 
     * @param Date
     *            d The date from which the month is to be extracted
     * @return int The number of the month
     */
    public static int getMonthOfYear(Date d) {
	return getDatePart(d, GregorianCalendar.MONTH);
    }

    /**
     * Returns the year part of a date object
     * 
     * @param Date
     *            d The date from which the year is required
     * @return int The year in normal calendar format
     */
    public static int getYear(Date d) {
	return getDatePart(d, GregorianCalendar.YEAR);
    }

    /**
     * Returns the day of the year
     * 
     * @param Date
     *            d The date from which the year is required
     * @return int The day in normal calendar format
     */
    public static int getDayOfYear(Date d) {
	return getDatePart(d, GregorianCalendar.DAY_OF_YEAR);
    }

    /**
     * Returns the week of the year
     * 
     * @param Date
     *            d The date from which the year is required
     * @return int The week of the year in normal calendar format
     */
    public static int getWeekOfYear(Date d) {
	return getDatePart(d, GregorianCalendar.WEEK_OF_YEAR);
    }

    /**
     * Returns the week of the month
     * 
     * @param Date
     *            d The date from which the year is required
     * @return int The week in normal calendar format
     */
    public static int getWeekOfMonth(Date d) {
	return getDatePart(d, GregorianCalendar.WEEK_OF_MONTH);
    }

    /**
     * Returns an integer representation of year and month in the format yyyymm
     * The date must not be null.
     * 
     * @param Date
     *            date The date for which the yearMonth is required
     * @return int The year and month in an integer format
     */
    public static int yearMonth(Date date) {
	DateTime d = new DateTime(date);
	int month = d.getMonthOfYear();
	int year = d.getDateYear();
	return year * 100 + month;
    }

    /**
     * Returns a string representing a date in the form YYYYMMDD
     * 
     * @param DateTime
     *            date The date for which the string is required
     * @return String The string representation of the date
     */
    public static String formatYYYYMMDD(DateTime date) {
	long dateInt = date.getDateYear() * 10000 + (date.getMonthOfYear() + 1) * 100 + date.getDayOfMonth();
	return dateInt + "";
    }

    public static DateTime nextDay() {
	return nextDay(new DateTime());
    }

    public static DateTime nextDay(DateTime fromDate) {
	try {
	    return new DateTime(fromDate.add(new Interval(0, 0, 1, 0, 0, 0, 0)));
	} catch (Exception ex) {
	    return null;
	}
    }
}
