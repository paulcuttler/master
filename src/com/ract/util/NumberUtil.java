package com.ract.util;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DecimalFormat;

import com.ract.common.ValidationException;

/**
 * Number utility Classes
 * 
 * @author dgk
 * @version 1.0
 */

public class NumberUtil {

    public static final String PHONE_TYPE_HOME = "Home";

    public static final String PHONE_TYPE_MOBILE = "Mobile";

    public static final String PHONE_TYPE_WORK = "Work";

    private static final int PHONE_LENGTH_HOME = 10;

    private static final int PHONE_LENGTH_MOBILE = 10;

    private static final int PHONE_LENGTH_WORK = 10;

    /**
     * Rounds double numbers to any desired number of decimal places
     * 
     * @param double toBeRounded The number to be rounded
     * @param int fractionDigits The number of decimal places required
     * @return double The rounded number
     */
    public static double roundDouble(double toBeRounded, int fractionDigits) {
	BigDecimal aNumber = new BigDecimal(toBeRounded);
	aNumber = aNumber.setScale(fractionDigits, BigDecimal.ROUND_HALF_UP);
	return aNumber.doubleValue();
    }

    /**
     * Round a double value to the specified scale using the round half up
     * method and return the result as a BigDecimal.
     */
    public static BigDecimal roundHalfUp(double value, int scale) {
	return new BigDecimal(roundDouble(value, scale));
    }

    /**
     * format a double to two places for display
     */
    public static String formatValue(double amount) {
	DecimalFormat myFormatter = new DecimalFormat("###,###,##0.00");
	String output = myFormatter.format(amount);
	// LogUtil.log("xxx", "xxout" + output);
	return output;
    }

    public static String formatValue(BigDecimal amount) {
	// LogUtil.log("xxx", "xx" + amount);
	double amt = 0.0;
	if (amount != null) {
	    amt = amount.doubleValue();
	}
	return formatValue(amt);
    }

    /**
     * Format value as a percentage.
     * 
     * @param amount
     *            double
     * @return String
     */
    public static String formatPercentageValue(int amount1, int amount2) {
	double amount = getRatio(amount1, amount2);
	// System.out.println("amount "+amount);
	// amount = amount * 100;
	// System.out.println("amount "+amount);
	DecimalFormat myFormatter = new DecimalFormat("##0.00%");
	return myFormatter.format(amount);
    }

    /**
     * format a double to two places for display
     */
    public static String formatValue(double amount, String mask) {
	DecimalFormat myFormatter = new DecimalFormat(mask);
	String output = myFormatter.format(amount);
	return output;
    }

    /**
     * validation a phone number and return it in an acceptable format
     * 
     * @return the formatted phone number in the format 0418262255 or 0362456454
     */
    public static String validatePhoneNumber(String type, String phoneNumber) throws RemoteException, ValidationException {

	phoneNumber = phoneNumber.trim();

	if (phoneNumber != null && !phoneNumber.equals("")) {

	    // allow different validation
	    try {
		if (PHONE_TYPE_HOME.equals(type)) {
		    Long.parseLong(phoneNumber);
		    if (phoneNumber.length() != PHONE_LENGTH_HOME) {
			throw new ValidationException("Home phone number should have " + PHONE_LENGTH_HOME + " numbers.");
		    }
		} else if (PHONE_TYPE_MOBILE.equals(type)) {
		    Long.parseLong(phoneNumber);
		    if (phoneNumber.length() != PHONE_LENGTH_MOBILE) {
			throw new ValidationException("Mobile phone number should have " + PHONE_TYPE_MOBILE + " numbers.");
		    }
		} else if (PHONE_TYPE_WORK.equals(type)) {
		    Long.parseLong(phoneNumber);
		    if (phoneNumber.length() != PHONE_LENGTH_WORK) {
			throw new ValidationException(" Work phone number should have " + PHONE_LENGTH_WORK + " numbers.");
		    }
		} else {
		    throw new ValidationException("Not a known phone number type " + type + ".");
		}
	    } catch (NumberFormatException e) {
		throw new ValidationException(type + " phone number should be a valid number and not contain \"-\" or \"(\"." + e);
	    }
	}
	return phoneNumber;
    }

    /**
     * Checks if a string contains only valid numeric characters
     */
    public static boolean isNumeric(String str) {
	try {
	    long n = Long.parseLong(str);
	    return true;
	} catch (NumberFormatException nfe) {
	    return false;
	}
    }

    public static Integer reqInteger(String strInt) {
	return (StringUtil.isBlank(strInt)) ? new Integer("0") : new Integer(strInt);
    }

    /**
     * Generic ratio function
     * 
     * @param firstPart
     * @param secondPart
     * @return
     */
    public static double getRatio(int firstPart, int secondPart) {
	double ratio = (double) firstPart / (double) secondPart;
	return ratio;
    }

}
