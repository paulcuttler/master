package com.ract.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class SqlUtil {

    /**
     * Print to the system console the name and type of each column in the
     * result set and the order they appear in the result set.
     */
    public static void printColumns(ResultSet rs) {
	try {
	    ResultSetMetaData metaData = rs.getMetaData();
	    for (int i = 1; i <= metaData.getColumnCount(); i++) {
		System.out.println("Column " + i + " : " + metaData.getColumnName(i) + ", " + metaData.getColumnType(i));
	    }
	} catch (SQLException sqle) {
	    sqle.printStackTrace();
	}
    }

}
