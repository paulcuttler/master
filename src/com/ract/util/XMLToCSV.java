package com.ract.util;

/**
 * Convert an XML file to CSV
 * 
 * @author jyh
 */
public class XMLToCSV extends XMLHandler {

    private StringBuffer csv;
    private StringBuffer header;
    private int headercounter;

    public String getContent() {
	return csv.toString();
    }

    public static void main(String args[]) {
	if (args.length != 4) {
	    System.out.println("java XMLToCSV [source] [delimiter] [number of fields] [exclude header row?]");
	    System.exit(1);
	}
	new XMLToCSV(args);
    }

    public XMLToCSV(String args[]) {
	csv = new StringBuffer();
	nfields = Integer.parseInt(args[2]);
	delim = args[1];
	excludeHeader = Boolean.parseBoolean(args[3]);
	parse(args[0]);

	String output = csv.substring(0, csv.length() - delim.length() - 1);
    }

    // DocumentHandler methods

    public void startDocument() {
	fieldcounter = 0;
	header = new StringBuffer();
	System.out.println("start document");
    }

    public void endElement(String uri, String local, String qname) {
	if (inleaf) {
	    // collate headers
	    if (!qname.equals("resultset") && !qname.equals("row")) {
		if (!headerEntries.contains(qname)) {
		    header.append(qname);
		    headerEntries.add(qname);
		    if (++headercounter % nfields == 0) {
			header.append(FileUtil.NEW_LINE);
		    } else {
			header.append(delim);
		    }
		}
	    }

	    csv.append(elementcontent);
	    if (++fieldcounter % nfields == 0) {
		csv.append(FileUtil.NEW_LINE);
	    } else {
		csv.append(delim);
	    }
	}
	inleaf = false;
    }

    public void endDocument() {
	if (!excludeHeader) {
	    csv.insert(0, header.toString());
	}
    }

}
