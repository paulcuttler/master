package com.ract.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;

import com.ract.common.CommonEJBHelper;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportGenerator;
import com.ract.common.reporting.ReportRegister;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.membership.reporting.MembershipRenewalAdviceRequest;

public class ReportUtil {
	final static String KEY_URL = "URL";

	/**
	 * Print an Elixir report
	 */
	public static void printReport(ReportRequestBase reportRequest) throws GenericException {
		LogUtil.log(com.ract.util.ReportUtil.class, "printReport start");
		LogUtil.log(com.ract.util.ReportUtil.class, "reportRequest=" + reportRequest);
		try {
			OutputStream fStream = FileUtil.openStreamForWriting(reportRequest.getDestinationFileName(), true);
			reportRequest.setOutputStream(fStream);
			reportRequest.setReportKeyName(ReportRegister.addReport(reportRequest));
			reportRequest.constructURL();
			reportRequest.addParameter(KEY_URL, reportRequest.getReportDataURL());
			new ReportGenerator().runReport(reportRequest);
			ReportRegister.removeReport(reportRequest.getReportKeyName());
		} catch (Exception e) {
			throw new GenericException(e);
		}
		LogUtil.log(com.ract.util.ReportUtil.class, "printReport end");
	}

	/**
	 * Email an elixir report
	 */
	public static void emailReport(ReportRequestBase reportRequest, boolean smbMode, MailMessage message) throws ReportException {

		LogUtil.log(com.ract.util.ReportUtil.class, "emailReport start");

		// Only generate the report if we have a non null request
		if (reportRequest != null) {
			String reportKeyName = ReportRegister.addReport(reportRequest); // add
			// to
			// register
			reportRequest.setReportKeyName(reportKeyName);
			try {
				reportRequest.constructURL();
			} catch (Exception e1) {
				throw new ReportException(e1);
			}
			// encode url
			reportRequest.addParameter("URL", reportRequest.getReportDataURL());
	
			OutputStream fStream;
			try {
				fStream = FileUtil.openStreamForWriting(reportRequest.getDestinationFileName(), smbMode);
			} catch (IOException e) {
				throw new ReportException(e);
			}
			reportRequest.setOutputStream(fStream);
			reportRequest.addParameter("URL", reportRequest.getReportDataURL());
			LogUtil.debug(com.ract.util.ReportUtil.class, "reportRequest=" + reportRequest);
	
			new ReportGenerator().runReport(reportRequest); // generate
	
			ReportRegister.removeReport(reportKeyName); // remove from register
	
			// LogUtil.log(com.ract.util.ReportUtil.class, "message=" + message);
		}

		try {
			Hashtable<String, String> files = message.getFiles();
			if (files == null)
				files = new Hashtable<String, String>();

			// Only attach the report if it was generated
			if (reportRequest != null) {
				String attachmentName = reportRequest.getReportAttachmentName() == null ? reportRequest.getReportName() : reportRequest.getReportAttachmentName();
				files.put(attachmentName, reportRequest.getDestinationFileName());
			}
			
			// attach privacy info - REMOVED
			// if (reportRequest instanceof MembershipRenewalAdviceRequest) {
			// files.put(((MembershipRenewalAdviceRequest) reportRequest).getPrivacyAttachmentName(), ((MembershipRenewalAdviceRequest) reportRequest).getPrivacyAttachmentFile());
			// }
			message.setFiles(files);

			MailMgr mailMgr = CommonEJBHelper.getMailMgr();
			mailMgr.sendMail(message);
//		} catch (IOException ex) {
//			throw new ReportException(ex);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		} finally {
			LogUtil.log(com.ract.util.ReportUtil.class, "reportRequest remove file" + reportRequest.isRemoveFile()); // remove
			// file
			if (reportRequest.isRemoveFile()) {
				// Clean up the temporary file once the email is on its way
				FileUtil.deleteFile(reportRequest.getDestinationFileName());
			}
		}
		LogUtil.log(com.ract.util.ReportUtil.class, "emailReport end"); // remove
		// file
	}

}
