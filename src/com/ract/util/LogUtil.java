package com.ract.util;

import java.util.Date;

import org.apache.log4j.Category;

import com.ract.common.ExceptionHelper;

/**
 * Log utilities class
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class LogUtil {

    private static boolean debugEnabled = true;

    private static boolean loggingEnabled = true;

    private static boolean timeStampingEnabled = true;

    public static void debug(Class c, Exception e) {
	debug(c, ExceptionHelper.getExceptionStackTrace(e));
    }

    public static void debug(Class c, String msg) {
	if (c != null && debugEnabled) {
	    Category cat = Category.getInstance(c.getName());
	    cat.debug(msg);
	}
    }

    public static void debug(String className, String msg) {
	if (className != null && loggingEnabled) {
	    Category cat = Category.getInstance(className);
	    cat.debug(msg);
	}
    }

    public static void fatal(Class c, Exception e) {
	fatal(c, ExceptionHelper.getExceptionStackTrace(e));
    }

    public static void fatal(Class c, String msg) {
	if (c != null) {
	    Category cat = Category.getInstance(c.getName());
	    cat.fatal(msg);
	}
    }

    public static void fatal(String className, String msg) {
	if (className != null) {
	    Category cat = Category.getInstance(className);
	    cat.fatal(msg);
	}
    }

    public static void info(Class c, Exception e) {
	info(c, ExceptionHelper.getExceptionStackTrace(e));
    }

    public static void info(Class c, String msg) {
	if (c != null && loggingEnabled) {
	    Category cat = Category.getInstance(c.getName());
	    cat.info(msg);
	}
    }

    public static void log(Class c, Exception e) {
	log(c, ExceptionHelper.getExceptionStackTrace(e));
    }

    public static void log(Class c, String msg) {
	if (c != null && loggingEnabled) {
	    Category cat = Category.getInstance(c.getName());
	    cat.info(msg);
	}
    }

    public static void log(String className, String msg) {
	if (className != null && loggingEnabled) {
	    Category cat = Category.getInstance(className);
	    cat.info(msg);
	}
    }

    public static void timeStamp(Class c, String msg) {
	if (c != null && timeStampingEnabled) {
	    log(c, DateUtil.formatDate(new Date(), "dd/MM/yyyy HH:mm:ss:SS") + " : " + msg);
	}
    }

    public static void warn(Class c, Exception e) {
	warn(c, ExceptionHelper.getExceptionStackTrace(e));
    }

    public static void warn(Class c, String msg) {
	if (c != null) {
	    Category cat = Category.getInstance(c.getName());
	    cat.warn(msg);
	}
    }

    public static void warn(String cName, String msg) {
	if (cName != null) {
	    Category cat = Category.getInstance(cName);
	    cat.warn(msg);
	}
    }

    // private static String getExceptionTrace(Exception e)
    // {
    // StringWriter sw = new StringWriter();
    // PrintWriter pw = new PrintWriter(sw);
    // e.printStackTrace(pw);
    // return sw.toString();
    // }

}
