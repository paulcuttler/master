package com.ract.util;

import java.io.File;
import java.util.Comparator;

/**
 * <p>
 * Sort files on date in ascending order
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class FileComparator implements Comparator {

    public FileComparator() {
    }

    public int compare(Object o1, Object o2) {
	if (o1 != null && o1 instanceof File && o2 != null && o2 instanceof File) {
	    File file1 = (File) o1;
	    LogUtil.debug(this.getClass(), "file1=" + file1.lastModified());
	    File file2 = (File) o2;
	    LogUtil.debug(this.getClass(), "file2=" + file2.lastModified());
	    if (file1.lastModified() < file2.lastModified()) // reverse order
	    {
		return 1;
	    } else {
		return -1;
	    }
	} else {
	    return 0;
	}
    }

}
