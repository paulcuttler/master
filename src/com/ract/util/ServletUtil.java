package com.ract.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.rmi.RemoteException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.common.ExceptionHelper;
import com.ract.common.SystemParameterVO;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserSession;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @deprecated use Struts 2 instead.
 * @author
 * @version 1.0
 */
public class ServletUtil {

    public static UserSession getUserSession(HttpServletRequest request, boolean securityEnabled) throws RemoteException {
	LogUtil.debug("getUserSession", "getUserSession");
	User user = null;
	UserSession userSession = null;
	if (!securityEnabled) {
	    LogUtil.debug("getUserSession", "Secury not enabled!");

	    try {
		// try the user session
		userSession = UserSession.getUserSession(request.getSession());
		LogUtil.debug("getUserSession", "1 userSession=" + userSession);
		if (userSession == null) {
		    String userId = FileUtil.getProperty("master", SystemParameterVO.DEFAULT_USER_ID);
		    LogUtil.debug("getUserSession", "userId=" + userId);
		    // if null in the user session - get from the database
		    user = UserEJBHelper.getUserMgr().getUser(userId);
		    LogUtil.debug("getUserSession", "user=" + user);
		    String systemName = FileUtil.getProperty("master", SystemParameterVO.TYPE_SYSTEM);
		    LogUtil.debug("getUserSession", "2a" + systemName);
		    userSession = new UserSession(user, request.getSession().getId(), request.getRemoteAddr(), systemName, new DateTime());

		    LogUtil.debug("getUserSession", "2 userSession=" + userSession);

		    request.getSession().setAttribute(LoginUIConstants.USER_SESSION, userSession);

		}
	    } catch (Exception e) {
		LogUtil.debug("ServletUtil", "Unable to get the default user. " + ExceptionHelper.getExceptionStackTrace(e));
	    }
	} else {
	    userSession = UserSession.getUserSession(request.getSession());
	}
	LogUtil.debug("getUserSession", "3 userSession=" + userSession);
	return userSession;
    }

    public static String getStringParam(String name, HttpServletRequest request) {
	return request.getParameter(name);
    }

    public static Integer getIntegerParam(String name, HttpServletRequest request) {
	return NumberUtil.reqInteger(getName(request.getParameter(name)));
    }

    // public static Integer getIntegerParam ( String name )
    // {
    // return NumberUtil.reqInteger(getName(req.getParameter(name)));
    // }

    // public static int getIntParam ( String name )
    // {
    // return getIntegerParam ( name ).intValue();
    // }

    public static int getIntParam(String name, HttpServletRequest request) {
	return getIntegerParam(name, request).intValue();
    }

    public static boolean getBooleanParam(String name, HttpServletRequest request) {
	return Boolean.valueOf(getName(request.getParameter(name))).booleanValue();
    }

    public static double getDoubleParam(String name, HttpServletRequest request) {
	String t = getName(request.getParameter(name));
	if (t.length() < 1) {
	    t = "0.0";
	}
	return new Double(t).doubleValue();
    }

    public static BigDecimal getBigDecimalParam(String name, HttpServletRequest request) {
	return new BigDecimal(getDoubleParam(name, request));
    }

    public static DateTime getDateParam(String name, HttpServletRequest request) {
	return DateUtil.nullOrDate(getName(request.getParameter(name)));
    }

    private static String getName(String s) {
	return StringUtil.makeSpaces(s);
    }

    public static void ajaxXMLResponse(HttpServletResponse response, String responseXML) {

	response.setContentType("text/xml");
	response.setDateHeader("Expires", 0);

	LogUtil.debug("ServletUtil ajaxResponse", "PRE " + responseXML);
	PrintWriter out = null;
	try {
	    out = response.getWriter();
	} catch (IOException ex2) {
	    LogUtil.log("ServletUtil ajaxResponse ", "Unable to get PrintWriter " + ex2.getMessage());
	    return;
	}

	out.println(responseXML);
	out.flush();
	out.close();
	LogUtil.log("ServletUtil ajaxResponse ", "Responding with " + responseXML);

    }
}
