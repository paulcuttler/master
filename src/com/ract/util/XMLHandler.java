package com.ract.util;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

public class XMLHandler extends DefaultHandler {

    protected StringBuffer elementcontent;
    protected boolean inleaf;
    protected boolean excludeHeader;
    protected int nfields;
    protected int fieldcounter;
    protected String delim;
    protected List<String> headerEntries = new ArrayList<String>();

    public Object getContent() {
	return null;
    }

    public void parse(String xmlDoc) {
	try {
	    // Create a JAXP SAXParser
	    SAXParserFactory saxfactory = SAXParserFactory.newInstance();
	    SAXParser saxparser = saxfactory.newSAXParser();
	    org.xml.sax.XMLReader xmlreader = saxparser.getXMLReader();

	    xmlreader.setContentHandler(this);
	    xmlreader.setErrorHandler(this);
	    xmlreader.setFeature("http://xml.org/sax/features/validation", true);
	    InputSource inStream = new InputSource();
	    inStream.setCharacterStream(new StringReader(xmlDoc));
	    xmlreader.parse(inStream);
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    public void startElement(String uri, String local, String qname, Attributes attrs) {
	elementcontent = new StringBuffer();
	inleaf = true;
    }

    public void characters(char ch[], int start, int length) {
	elementcontent.append(ch, start, length);
    }
}
