package com.ract.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

/**
 * <p>
 * Manipulate files.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class FileUtil {

    public final static String NEW_LINE = "\n";

    public final static String TAB = "\t";

    public static final String SEPARATOR_PIPE = "|";

    public static final String SEPARATOR_COMMA = ",";

    // well known file extensions
    public static final String EXTENSION_DATA = "dat";

    public static final String EXTENSION_CSV = "csv";

    public static final String EXTENSION_TXT = "txt";

    public static final String EXTENSION_ZIP = "zip";

    public static final String EXTENSION_PDF = "pdf";

    public static final String EXTENSION_XML = "xml";

    /**
     * Default file name prefix with date. eg. 20050323.
     */
    public static final String FILE_NAME_DATE_PREFIX = "yyyyMMdd";
    public static final String FILEUTIL_yyMMddHHmm = "yyMMddHHmm";
    public static final String FILEUTIL_yyMMHHmm = "yyMMHHmm";
    public static final String FILEUTIL_MMddHHmm = "MMddHHmm";

    /**
     * write a file to disk
     * 
     * @param fileName
     * @param contents
     */
    public static File writeFile(String fileName, String contents) throws IOException {
	return writeFile(fileName, contents.getBytes(), false);
    }

    /**
     * Construct a default file name given default components.
     * 
     * @param outputDirectory
     *            String
     * @param suffix
     *            String
     * @param extension
     *            String
     * @return String
     */
    public static String constructFileName(String outputDirectory, String suffix, String extension) {
	return outputDirectory + DateUtil.formatDate(new DateTime(), FileUtil.FILE_NAME_DATE_PREFIX) + (suffix == null ? "" : suffix) + "." + extension;
    }

    public static String constructFileName(String baseFileName, String dateFormat, String suffix, String extension) {
	return baseFileName + (dateFormat == null ? "" : DateUtil.formatDate(new DateTime(), dateFormat)) + (suffix == null ? "" : suffix) + "." + extension;

    }

    /**
     * Write a binary file
     * 
     * @param fileName
     *            String
     * @param contents
     *            byte[]
     * @param smbMode
     *            use samba mode that will allow a file to be written to the
     *            windows file system using details stored in jcifs.properties.
     * @throws IOException
     */
    public static File writeFile(String fileName, byte[] contents, boolean smbMode) throws IOException {
	BufferedOutputStream out = null;
	// LogUtil.log("writeFile","1");
	File file = null;
	if (smbMode) {

	    SmbFile smbfile = new SmbFile(fileName);
	    // LogUtil.log("writeFile","2a");
	    out = new BufferedOutputStream(new SmbFileOutputStream(smbfile));
	    // LogUtil.log("writeFile","2b");
	} else {
	    // native mode
	    file = new File(fileName);
	    out = new BufferedOutputStream(new FileOutputStream(file));
	}
	out.write(contents);
	// LogUtil.log("writeFile","3");
	out.flush();
	out.close();
	// LogUtil.log("writeFile","4");
	// no file to return
	return file;
    }

    public static void zipFile(String destFileName, String sourceFileName) {
	final int BUFFER = 2048;
	try {
	    BufferedInputStream origin = null;
	    FileOutputStream dest = new FileOutputStream(destFileName);
	    ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
	    // out.setMethod(ZipOutputStream.DEFLATED);
	    byte data[] = new byte[BUFFER];

	    FileInputStream fi = new FileInputStream(sourceFileName);
	    origin = new BufferedInputStream(fi, BUFFER);
	    ZipEntry entry = new ZipEntry(sourceFileName);
	    out.putNextEntry(entry);
	    int count;
	    while ((count = origin.read(data, 0, BUFFER)) != -1) {
		out.write(data, 0, count);
	    }
	    origin.close();
	    out.close();
	    // delete if zip successful
	    deleteFile(sourceFileName);
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    public static BufferedOutputStream openStreamForWriting(String fileName, boolean smbMode) throws IOException {
	BufferedOutputStream out = null;
	if (smbMode) {
	    // SMB mode
	    SmbFile smbfile = new SmbFile(fileName);
	    out = new BufferedOutputStream(new SmbFileOutputStream(smbfile));
	} else {
	    // native mode
	    File file = new File(fileName);
	    out = new BufferedOutputStream(new FileOutputStream(file));
	}
	return out;
    }

    /**
     * Prompt the browser to save the contents to a file.
     * 
     * @param response
     * @param content
     * @param fileName
     */
    public static void downloadContentsAsFile(HttpServletResponse response, OutputStream out, Object content, String fileName) {

	// Set the headers.
	response.setContentType("application/x-download");
	response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

	// Send the file.
	if (content instanceof String) {
	    try {
		PrintStream writer = new PrintStream(out);
		writer.print((String) content);
		writer.flush();
		writer.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	} else if (content instanceof PdfPTable) {
	    Document doc = new Document(PageSize.A4.rotate()); // landscape
	    try {
		PdfWriter.getInstance(doc, out);
		doc.open();
		doc.add((PdfPTable) content);
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    doc.close();
	}
    }

    public static void deleteFile(String fileName) {
	File file = new File(fileName);
	file.delete();
    }

    public static void renameFile(String oldFileName, String newFileName) {
	File file = new File(oldFileName);
	file.renameTo(new File(newFileName));
    }

    public static List<File> getSortedFileNameList(String dirName, String filter) {
	File file = new File(dirName);
	File[] files = file.listFiles(new FileNameFilter(filter));

	try {
	    Arrays.sort(files, new FileNameComparator());
	    return Arrays.asList(files);
	} catch (NullPointerException n) {
	    // do nothing on null pointer
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return null;

    }

    @SuppressWarnings("serial")
    static class FileNameComparator implements Comparator<File>, Serializable {
	public int compare(File file1, File file2) {
	    return file1.getName().compareTo(file2.getName());
	}

    }

    static class FileNameFilter implements FileFilter

    {
	private String filePrefix = null;

	FileNameFilter() {
	}

	FileNameFilter(String filePrefix) {
	    this.filePrefix = filePrefix;
	}

	public boolean accept(File fileName) {
	    if (fileName.getName().startsWith(this.filePrefix)) {
		return true;
	    }

	    return false;
	}
    }

    public static String getFilePathSeparator() {
	try {
	    return FileUtil.getProperty("system", "file.separator");
	} catch (Exception e) {
	    return "\\";
	}
    }

    /**
     * Clear a cached properties file so that it can be loaded again.
     * 
     * @param name
     *            Name of the properties file
     */
    public static void clearPropertiesFileCache(String name) {
	ResourceBundle bundle = PropertyResourceBundle.getBundle(name);
	bundle.clearCache();
    }

    public static String getProperty(String filename, String key) throws Exception {
	ResourceBundle bundle = PropertyResourceBundle.getBundle(filename);
	return bundle.getString(key);
    }

}
