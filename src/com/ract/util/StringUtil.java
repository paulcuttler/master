package com.ract.util;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.common.SystemException;

/**
 * Class with static methods to convert and manipulate Strings
 * 
 * @author John Holliday, Terry Bakker
 * @created 8 August 2002
 * @version 1.0,2/2/02
 */

public class StringUtil {

    private final static char[] wordDividerList = { ' ', '&', '-', ':', ',', '/' };

    private final static int wordDividerListSize = 6;

    public static String quote(String target) {
	return "\"" + target + "\"";
    }

    /**
     * Convert vector of vector data to csv
     * 
     * @param rows
     *            Vector of rows (vector)
     * @return CSV string
     */
    public static String convertToCSV(Vector rows) {
	int rowSize = rows.size();
	int colSize = 0;
	Vector columns = null;

	StringBuffer csvString = new StringBuffer();

	for (int i = 0; i < rowSize; i++) {
	    columns = (Vector) rows.get(i);

	    colSize = columns.size();
	    for (int z = 0; z < colSize; z++) {
		csvString.append((String) columns.get(z));

		// if last column do not add a delimiter
		if (z != (colSize - 1)) {
		    csvString.append(FileUtil.SEPARATOR_COMMA);
		}

	    }
	    // add a new line to the end
	    csvString.append(FileUtil.NEW_LINE);

	}

	return csvString.toString();
    }

    /**
     * Join a list of Strings with a delimiter.
     * 
     * @param list
     * @param delim
     * @return
     */
    public static String join(List<String> list, char delim) {
	StringBuilder sb = new StringBuilder();
	boolean first = true;

	for (String s : list) {
	    if (!first) {
		sb.append(delim);
	    }
	    sb.append(s);
	    first = false;
	}

	return sb.toString();
    }

    /**
     * Convert int to a string
     * 
     * @param x
     *            the int to convert
     * @return the converted string
     */
    public static String convertIntToString(int x) {
	return String.valueOf(x);
    }

    public static String convertBooleanValue(boolean item) {
	return item ? "Y" : "N";
    }

    /**
     * Convert int to a string
     * 
     * @param string
     *            the string to convert
     * @return the converted string
     */
    public static int convertStringToInt(String string) {
	return Integer.parseInt(string);
    }

    /**
     * Add space to end of a string
     * 
     * @param target
     *            the string to add space after
     * @param fieldLength
     *            the amount of space to add
     * @return the string with space added
     */
    public static String rightPadString(String target, int fieldLength) {
	StringBuffer sb = new StringBuffer(target);
	for (int i = target.length(); i < fieldLength; i++) {
	    sb.append(" ");
	}
	// System.out.println("size "+sb.toString().length());
	return sb.toString();
    }

    /**
     * Recursive method to remove commas from a string.
     */
    public static String removeCommas(String fromString) {
	String commaLess = replaceAll(fromString, ",", "");
	return commaLess;
    } // removeCommas

    public static String removeWhitespace(String fromString) {
	return replaceAll(fromString, " ", "");
    }

    /**
     * Format a credit card number for display.
     * 
     * @param creditCardNumber
     *            String
     * @return String
     */
    public static String formatCreditCardNumber(String creditCardNumber) {
	final int DIVISION = 4;
	final String SPACE = " ";
	String currentChar;
	StringBuffer formattedCreditCardNumber = new StringBuffer();
	for (int i = 0; i < creditCardNumber.length(); i++) {
	    currentChar = creditCardNumber.substring(i, i + 1);
	    if ((i + 1) % DIVISION == 0 && i != (creditCardNumber.length() - 1)) {
		formattedCreditCardNumber.append(currentChar + SPACE);
	    } else {
		formattedCreditCardNumber.append(currentChar);
	    }
	}
	return formattedCreditCardNumber.toString();
    }

    public static String obscureFormattedCreditCardNumber(String creditCardNumber) {
	StringBuffer obscuredCreditCardNumber = null;
	if (creditCardNumber != null && creditCardNumber.length() > 9) {
	    obscuredCreditCardNumber = new StringBuffer(formatCreditCardNumber(creditCardNumber));
	    obscuredCreditCardNumber.setCharAt(5, '#');
	    obscuredCreditCardNumber.setCharAt(6, '#');
	    obscuredCreditCardNumber.setCharAt(7, '#');
	    obscuredCreditCardNumber.setCharAt(8, '#');

	    obscuredCreditCardNumber.setCharAt(10, '#');
	    obscuredCreditCardNumber.setCharAt(11, '#');
	    obscuredCreditCardNumber.setCharAt(12, '#');
	    obscuredCreditCardNumber.setCharAt(13, '#');
	    return obscuredCreditCardNumber.toString();

	} else {
	    return creditCardNumber;
	}
    }

    public static String obscureBankAccountNumber(String accountNumber) {
	StringBuffer obscuredBankAccountNumber = new StringBuffer(accountNumber);

	for (int i = 1; i < accountNumber.length() - 2; i++) {
	    obscuredBankAccountNumber.setCharAt(i, '#');
	}
	return obscuredBankAccountNumber.toString();
    }

    /**
     * Replace AN occurrence of a string within a string
     * 
     * @param source
     *            the source string
     * @param find
     *            the string to find
     * @param replace
     *            the replace string
     * @return the initial string with replacements made
     */
    public static String replace(String source, String find, String replace) {
	String result = "";
	StringBuffer sb = new StringBuffer(source);
	int pos = source.indexOf(find);
	if (pos > -1) {
	    sb.replace(pos, pos + find.length(), replace);
	    result = sb.toString();
	}
	// not found!
	else {
	    result = source;
	}
	return result;
    }

    /**
     * Replace ALL occurrences of a string within a string
     * 
     * @param source
     *            the source string
     * @param find
     *            the find string
     * @param replace
     *            the replace string
     * @return the initial string with replacements made
     */
    public static String replaceAll(String source, String find, String replace) {

	if (find == null || find.equals("")) {
	    // nothing to find
	    return source;
	}

	char[] sourceCh = source.toCharArray();
	char[] findCh = find.toCharArray();
	int stringLen = source.length();
	int findLen = find.length();

	StringBuffer resultBuffer = new StringBuffer();

	for (int i = 0; i < stringLen; i++) {
	    // if current character equal to the first character of the search
	    // term
	    if (sourceCh[i] == findCh[0]) {
		// check if subsequent chars the same
		boolean match = true;
		int matchCount = 0;
		// while there is a match check the next characters
		for (int x = 0; (x < findCh.length) && (match); x++) {
		    // make sure we don't exceed the limit of the string
		    if ((i + x) < stringLen) {
			// if a match, count it
			if (sourceCh[i + x] == findCh[x]) {
			    matchCount++;
			} else {
			    // no match
			    match = false;
			}
		    }
		}
		// if matched count equals the find term's length then add
		// replace string
		if (matchCount == findLen) {
		    // if len is zero then it would have returned at the top
		    // move index
		    i = i + (findLen - 1);
		    resultBuffer.append(replace);
		}
		// if no match then just append the current character
		else {
		    resultBuffer.append(sourceCh[i]);
		}
	    }
	    // if no match at all then just append the next character
	    else {
		resultBuffer.append(sourceCh[i]);
	    }
	}
	return resultBuffer.toString();
    }

    public static int getSizeInKB(String string) {
	int size = 0;
	final int kilobits = 1024;
	size = string.getBytes().length / kilobits;
	return size;
    }

    /**
     * Return true if the passed in string can be converted to an integer.
     */
    public static boolean isInteger(String str) {
	boolean ok = true;
	try {
	    int n = Integer.parseInt(str);
	} catch (NumberFormatException nfe) {
	    ok = false;
	}
	return ok;
    }

    public static boolean isDouble(String str) {
	boolean ok = true;
	try {
	    double n = Double.parseDouble(str);
	} catch (NumberFormatException nfe) {
	    ok = false;
	}
	return ok;
    }

    public static boolean isFloat(String str) {
	boolean ok = true;
	try {
	    float n = Float.parseFloat(str);
	} catch (NumberFormatException nfe) {
	    ok = false;
	}
	return ok;
    }

    /**
     * insert spaces between the characters in inString
     * 
     * "abc" becomes "a b c"
     * 
     * @param inString
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static String insertSpaces(String inString) {
	if (inString == null) {
	    return "";
	}

	StringBuffer outString = new StringBuffer();

	char ch;

	inString = inString.trim();

	for (int x = 0; x < inString.length(); x++) {
	    ch = inString.charAt(x);

	    if (ch != ' ') {
		outString.append(ch);
		outString.append(" ");
	    }
	}

	return outString.toString().trim();
    }

    /**
     * Returns the string with all characters converted to lower case and the
     * first character of each word converted to upper case.
     * 
     * @param theString
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static String toProperCase(String theString) {
	return toProperCase(theString, true);
    }

    /**
     * Returns the string with the first character of each word converted to
     * upper case. If the 'forceLowerCase' flag is true then all of the other
     * characters will be converted to lower case otherwise they will be left
     * unchanged. Does not strip extra spaces from between words or from the
     * start or end of the string. Words are separated by one of the characters
     * in the 'dividerList' array. Returns an empty string if the passed string
     * is null.
     * 
     * @param theString
     *            Description of the Parameter
     * @param forceLowerCase
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static String toProperCase(String theString, boolean forceLowerCase) {
	if (theString != null) {
	    // Convert all characters to lower case if required.
	    if (forceLowerCase) {
		theString = theString.toLowerCase();
	    }

	    // Now convert the first character in each word to upper case.
	    char[] theChars = theString.toCharArray();
	    boolean doNext = true;
	    for (int i = 0; i < theString.length(); i++) {
		if (doNext) {
		    // If the character is lower case it must
		    // be in the range a - z.
		    if (Character.isLowerCase(theChars[i])) {
			theChars[i] = Character.toUpperCase(theChars[i]);
			doNext = false;
		    }
		} else {
		    // Is the character a word divider
		    if (isWordDivider(theChars[i])) {
			doNext = true;
		    }
		}
	    }
	    return String.copyValueOf(theChars);
	} else {
	    return "";
	}
    }

    public static String makeSpaces(String s) {
	return (isNull(s)) ? "" : s;
    }

    public static boolean isBlank(String s) {
	return (isNull(s) || " ".equals(s));
    }

    public static boolean isNull(String s) {
	return (s == null || s.length() == 0 || "null".equals(s));
    }

    /**
     * Return true if the specified character is contained in the list of
     * recognised word divider characters.
     * 
     * @param ch
     *            Description of the Parameter
     * @return The wordDivider value
     */
    private static boolean isWordDivider(char ch) {
	boolean inList = false;
	for (int i = 0; i < wordDividerListSize && !inList; i++) {
	    if (ch == wordDividerList[i]) {
		inList = true;
	    }
	}
	return inList;
    }

    /**
     * Search through the string and look for a tag. If found, recursively
     * search the balance of the string Tags which are found are replaced from a
     * Hashtable of value. The Hashtable holds tagname-value pairs.
     * 
     * @param String
     *            inString The string which might contain tags.
     * @param String
     *            opener The character string which marks the beginning of a
     *            tag.
     * @param String
     *            closer The character string which marks the end of a tag.
     * @param Hashtable
     *            tagList A Hashtable of tag name - value pairs.
     * @return String The string with the tags replaced.
     */
    public static String replaceTag(String inString, String opener, String closer, Hashtable<String, String> tagList) {
	int openAt = -1;
	int closeAt = -1;
	boolean found = false;
	String tagName = null;
	String temp = inString;
	openAt = inString.indexOf(opener);
	if (openAt > -1) {
	    closeAt = inString.indexOf(closer, openAt);
	    if (closeAt > -1) {
		found = true;
		tagName = inString.substring(openAt + opener.length(), closeAt);
		temp = inString.substring(0, openAt);
		if (tagList.containsKey(tagName)) {
		    temp += tagList.get(tagName);
		} else {
		    temp = inString.substring(0, closeAt + closer.length());
		}
		temp += replaceTag(inString.substring(closeAt + closer.length()), opener, closer, tagList);
	    }
	}
	return temp;
    }

    /**
     * Removes blank lines from an array of strings within the given range
     * 
     * @param address
     *            Description of the Parameter
     * @param first
     *            Description of the Parameter
     */
    public static String[] removeBlankLines(String[] stringArray, int first) {
	String[] newStringArray = stringArray;
	int x = first;
	if (first < newStringArray.length - 1) {
	    if (newStringArray[first].trim().equals("")) {
		while (x < newStringArray.length - 1 && newStringArray[x].trim().equals("")) {
		    x++;
		}
		newStringArray[first] = newStringArray[x];
		newStringArray[x] = "";
	    }
	    removeBlankLines(newStringArray, first + 1);
	}
	return newStringArray;
    }

    /**
     * @todo This should use formatting. pad out string to a number of digits
     * 
     * @param num
     *            Description of the Parameter
     * @param digits
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static String padNumber(Integer num, int digits) {
	String temp = num.toString();
	for (int x = temp.length(); x < digits; x++) {
	    temp = "0" + temp;
	}
	return temp;
    }

    public static String leftPadString(String stringToPad, int maxLength) throws SystemException {
	StringBuffer paddedString = new StringBuffer();
	int padding = maxLength - stringToPad.length(); // 3
	if (padding < 0) {
	    throw new SystemException("String to pad (" + stringToPad + ") is too large.");
	}
	for (int i = 0; i < padding; i++) {
	    paddedString.append(" ");
	}
	paddedString.append(stringToPad);
	return paddedString.toString();
    }

    /**
     * Check if null before passing to Progress
     * 
     * @param a
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static String convertNull(String targetString) {
	if (targetString == null) {
	    targetString = "";
	}
	return targetString.trim();
    }

    /*
     * format a telephone number as 03 1234 5678
     */
    public static String formatPhoneNo(String phoneNo) {
	String outString = "";
	char ch;
	int yy = 0;
	for (int xx = 0; xx < phoneNo.length(); xx++) {
	    ch = phoneNo.charAt(xx);
	    if (ch != 32) {
		yy++;
		outString += ch;
		if (yy == 2 || yy == 6)
		    outString += " ";
	    }
	}
	return outString;
    }

    public static String formatMobileNo(String phoneNo) {
	String outString = "";
	char ch;
	int yy = 0;
	for (int xx = 0; xx < phoneNo.length(); xx++) {
	    ch = phoneNo.charAt(xx);
	    if (ch != 32) {
		yy++;
		outString += ch;
		if (yy == 4 || yy == 7)
		    outString += " ";
	    }
	}
	return outString;
    }

    public static String validateEmail(String emailAddress, boolean allowFieldStatusValue) throws GenericException {
	if (emailAddress != null && emailAddress.trim().length() > 0) {
	    if (emailAddress.equals(ClientVO.FIELD_STATUS_NOT_APPLICABLE) || emailAddress.equals(ClientVO.FIELD_STATUS_REFUSED) || emailAddress.equals(ClientVO.FIELD_STATUS_TO_FOLLOW)) {
		if (allowFieldStatusValue) {
		    return emailAddress;
		} else {
		    throw new GenericException("Email address '" + emailAddress + "' contains a field status of " + emailAddress + ".");
		}
	    }
	    int index = emailAddress.indexOf("@");
	    if (index == -1) {
		throw new GenericException("Email address '" + emailAddress + "' does not contain an '@' and does not have a value of Not Applicable, Refused or To Follow.!");
	    } else {
		if (emailAddress.indexOf(".", index + 2) == -1 || emailAddress.substring(emailAddress.length() - 1, emailAddress.length()).equals(".")) {
		    throw new GenericException("Email address '" + emailAddress + "' either does not contain a '.' or it has a '.' at the end!");
		}
	    }
	}
	return emailAddress;
    }

    public static String stripHTML(String input) {
	if (input != null) {
	    input = Pattern.compile("<(.*?)>").matcher(input).replaceAll("");
	}

	return input;
    }

}
