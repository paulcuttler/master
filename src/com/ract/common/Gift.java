package com.ract.common;

/**
 * <p>
 * A generic gift.
 * </p>
 * 
 * @hibernate.class table="gn_gift" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class Gift {

    public final static String TYPE_GIFT_GOLD_MEDALLION = "Gold Medallion";
    public final static String TYPE_GIFT_FREE_MEMBERSHIP = "Free Membership";
    public final static String TYPE_GIFT_ACCOMMODATION_GUIDE = "Accommodation";

    public final static String STATUS_GIFT_ACTIVE = "A";
    public final static String STATUS_GIFT_RETURNED = "R";
    public final static String STATUS_GIFT_DELETED = "D";

    private String giftCode;

    private String giftName;

    private String giftDescription;

    private String giftType;

    private Boolean returnable;

    /**
     * @hibernate.property
     * @hibernate.column name="gift_description"
     */
    public String getGiftDescription() {
	return giftDescription;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="gift_name"
     */

    public String getGiftName() {
	return giftName;
    }

    public void setGiftCode(String giftCode) {
	this.giftCode = giftCode;
    }

    public void setGiftDescription(String giftDescription) {
	this.giftDescription = giftDescription;
    }

    public void setGiftName(String giftName) {
	this.giftName = giftName;
    }

    public void setReturnable(Boolean returnable) {
	this.returnable = returnable;
    }

    public void setGiftType(String giftType) {
	this.giftType = giftType;
    }

    /**
     * @hibernate.id column="gift_code" generator-class="assigned"
     * @return String
     */
    public String getGiftCode() {
	return giftCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="returnable"
     */

    public Boolean isReturnable() {
	return returnable;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="gift_type"
     */
    public String getGiftType() {
	return giftType;
    }

    public Gift() {

    }

}
