package com.ract.common;

import java.lang.reflect.Method;
import java.util.Hashtable;

import javax.ejb.EJBHome;
import javax.ejb.EJBObject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.sql.DataSource;

import com.ract.util.LogUtil;

public class ServiceLocator {
    private static ServiceLocator serviceLocator;

    InitialContext context = null;

    private ServiceLocator() throws ServiceLocatorException {
	try {
	    context = new InitialContext();
	    LogUtil.debug(this.getClass(), "context=" + context.getEnvironment());
	} catch (NamingException ne) {
	    throw new ServiceLocatorException(ne);
	}
    }

    // Returns the instance of ServiceLocator class
    public static ServiceLocator getInstance() throws ServiceLocatorException {
	if (serviceLocator == null) {
	    serviceLocator = new ServiceLocator();
	}
	return serviceLocator;
    }

    /**
     * Cast to appropriate object. This method is used to access EJB3 services
     * 
     * @param jndiName
     * @return
     * @throws ServiceLocatorException
     */
    public Object getObject(String jndiName) throws ServiceLocatorException {
	if (jndiName == null) {
	    throw new ServiceLocatorException("JNDI name is null.");
	}
	try {
	    LogUtil.debug(this.getClass(), "getObject, JNDI name is " + jndiName);
	    Object object = context.lookup(jndiName);
	    LogUtil.debug(this.getClass(), "getObject, Class returned is " + object.getClass().getName());
	    return object;
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServiceLocatorException(ex);
	}
    }

    /**
     * Gets an EJBObject by jndi name
     * 
     * @param jndiName
     *            String
     * @throws ServiceLocatorException
     * @return EJBObject
     */
    @Deprecated
    public EJBObject getService(String jndiName) throws ServiceLocatorException {
	LogUtil.debug(this.getClass(), "getService " + jndiName);
	if (jndiName == null) {
	    throw new ServiceLocatorException("JNDI name is null");
	}
	try {
	    Object object = context.lookup(jndiName);

	    // Need not type cast home object to appropriate Home Interface
	    EJBHome home = (EJBHome) PortableRemoteObject.narrow(object, Class.forName(CLASS_EJB_HOME));

	    // Need not type cast home object to appropriate Remote Interface
	    Class homeBase = home.getClass();

	    Method m = homeBase.getMethod(METHOD_CREATE, null);
	    EJBObject remoteObject = (EJBObject) m.invoke(home, null);
	    return remoteObject;

	}

	catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServiceLocatorException(ex);
	}
    }

    final String METHOD_CREATE = "create";

    final String CLASS_EJB_HOME = "javax.ejb.EJBHome";

    /**
     * Returns the EJBHome object for requested service name. Throws
     * ServiceLocatorException If Any Error occurs in lookup.
     * 
     * @param jndiHomeName
     *            String
     * @throws ServiceLocatorException
     * @return EJBHome
     */
    @Deprecated
    public EJBHome getHome(String jndiHomeName) throws ServiceLocatorException {
	try {
	    Object objref = context.lookup(jndiHomeName);
	    // Need not type cast home object to appropriate Home Interface
	    EJBHome home = (EJBHome) PortableRemoteObject.narrow(objref, Class.forName(CLASS_EJB_HOME));
	    return home;
	} catch (Exception ex) {
	    throw new ServiceLocatorException(ex);
	}
    }

    private static Hashtable datasourceCache = new Hashtable();

    /**
     * Gets the connection attribute of the ConnectionFactory class
     * 
     * @param jndiDataSource
     *            Description of the Parameter
     * @return The connection value
     * @exception NamingException
     *                Description of the Exception
     */
    public DataSource getDataSource(String jndiName) throws NamingException {
	if (jndiName == null) {
	    throw new NamingException("No JNDI data source name provided.");
	}

	// Try and get the data source from the cache first
	DataSource dataSource = (DataSource) datasourceCache.get(jndiName);

	// Not in the cache so look it up.
	if (dataSource == null) {
	    dataSource = lookupDataSource(jndiName);
	}

	// Still null so not valid.
	if (dataSource == null) {
	    throw new NamingException("Unable to find JNDI Datasource " + jndiName + ".");
	}

	return dataSource;
    }

    /**
     * Gets the dataSource attribute of the ConnectionFactory class
     * 
     * @param jndiDataSource
     *            Description of the Parameter
     * @return The dataSource value
     */
    private DataSource lookupDataSource(String jndiDataSource) throws NamingException {
	DataSource dataSource = (DataSource) context.lookup(jndiDataSource);
	datasourceCache.put(jndiDataSource, dataSource);
	return dataSource;
    }

}
