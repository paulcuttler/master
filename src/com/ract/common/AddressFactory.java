package com.ract.common;

/**
 * AddressFactory to return an instance of an address of the correct type.
 *
 * @author John Holliday
 * @version 1.0
 */

import java.rmi.RemoteException;

import org.w3c.dom.Node;

public class AddressFactory {

    public static AddressVO getAddressVO(String type, String property, String propertyQualifier, Integer streetSuburbID) throws RemoteException {

	if (type.equals(AddressVO.ADDRESS_TYPE_POSTAL)) {
	    return (AddressVO) getPostalAddressVO(property, propertyQualifier, streetSuburbID);
	} else if (type.equals(AddressVO.ADDRESS_TYPE_STREET)) {
	    return (AddressVO) getStreetAddressVO(property, propertyQualifier, streetSuburbID);
	}
	return null;
    }

    public static AddressVO getAddressVO(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid) throws RemoteException {

	if (type.equals(AddressVO.ADDRESS_TYPE_POSTAL)) {
	    return (AddressVO) getPostalAddressVO(property, propertyQualifier, streetSuburbID, dpid);
	} else if (type.equals(AddressVO.ADDRESS_TYPE_STREET)) {
	    return (AddressVO) getStreetAddressVO(property, propertyQualifier, streetSuburbID, dpid);
	}
	return null;

    }

    public static AddressVO getAddressVO(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String ausbar) throws RemoteException {

	if (type.equals(AddressVO.ADDRESS_TYPE_POSTAL)) {
	    return (AddressVO) getPostalAddressVO(property, propertyQualifier, streetSuburbID, dpid, ausbar);
	}
	return null;

    }

    public static AddressVO getAddressVO(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf) throws RemoteException {

	if (type.equals(AddressVO.ADDRESS_TYPE_STREET)) {
	    return (AddressVO) getStreetAddressVO(property, propertyQualifier, streetSuburbID, dpid, latitude, longitude, gnaf);
	}
	return null;

    }

    public static AddressVO getAddressVO(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf, String ausbar) throws RemoteException {

	if (type.equals(AddressVO.ADDRESS_TYPE_STREET)) {
	    return (AddressVO) getStreetAddressVO(property, propertyQualifier, streetSuburbID, dpid, latitude, longitude, gnaf, ausbar);

	} else if (type.equals(AddressVO.ADDRESS_TYPE_POSTAL)) {
	    return (AddressVO) getPostalAddressVO(property, propertyQualifier, streetSuburbID, dpid, latitude, longitude, gnaf, ausbar);
	}

	return null;
    }

    /**
     * @todo String propertyQualifier
     */
    public static ResidentialAddressVO getStreetAddressVO(String property, String propertyQualifier, Integer streetSuburbID) throws RemoteException {
	ResidentialAddressVO svo = null;
	if (streetSuburbID != null) {
	    svo = new ResidentialAddressVO();
	    if (property != null) {
		svo.setProperty(property);
	    }
	    if (propertyQualifier != null) {
		svo.setPropertyQualifier(propertyQualifier);
	    }
	    svo.setStreetSuburbID(streetSuburbID);
	    svo.setStreetSuburb(getStreetSuburbVO(streetSuburbID));
	    svo.setDpid("");
	}
	return svo;
    }

    public static ResidentialAddressVO getStreetAddressVO(String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf, String ausbar) throws RemoteException {
	ResidentialAddressVO svo = getStreetAddressVO(property, propertyQualifier, streetSuburbID);
	if (svo != null) {
	    svo.setDpid(dpid);
	    svo.setLatitude(latitude);
	    svo.setLongitude(longitude);
	    svo.setGnaf(gnaf);
	    svo.setAusbar(ausbar);
	}

	return svo;
    }

    public static ResidentialAddressVO getStreetAddressVO(String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf) throws RemoteException {
	ResidentialAddressVO svo = getStreetAddressVO(property, propertyQualifier, streetSuburbID);
	if (svo != null) {
	    svo.setDpid(dpid);
	    svo.setLatitude(latitude);
	    svo.setLongitude(longitude);
	    svo.setGnaf(gnaf);
	}

	return svo;
    }

    public static ResidentialAddressVO getStreetAddressVO(String property, String propertyQualifier, Integer streetSuburbID, String dpid) throws RemoteException {
	ResidentialAddressVO svo = getStreetAddressVO(property, propertyQualifier, streetSuburbID);
	if (svo != null) {
	    svo.setDpid(dpid);
	}

	return svo;
    }

    /**
     * Return the address filled with the data from the XML DOM node. Set the
     * mode to "History" and set the value object to read only.
     */
    public static ResidentialAddressVO getStreetAddressVO(Node addressNode) throws SystemException {
	ResidentialAddressVO svo = new ResidentialAddressVO(addressNode);
	return svo;
    }

    public static PostalAddressVO getPostalAddressVO(String property, String propertyQualifier, Integer streetSuburbID) throws RemoteException {
	PostalAddressVO pvo = null;
	if (streetSuburbID != null) {
	    pvo = new PostalAddressVO();
	    if (property != null) {
		pvo.setProperty(property);
	    }
	    if (propertyQualifier != null) {
		pvo.setPropertyQualifier(propertyQualifier);
	    }
	    pvo.setStreetSuburbID(streetSuburbID);
	    pvo.setStreetSuburb(getStreetSuburbVO(streetSuburbID));
	    pvo.setDpid("");
	}
	return pvo;
    }

    public static PostalAddressVO getPostalAddressVO(String property, String propertyQualifier, Integer streetSuburbID, String dpid) throws RemoteException {
	PostalAddressVO pvo = getPostalAddressVO(property, propertyQualifier, streetSuburbID);
	if (pvo != null) {
	    pvo.setDpid(dpid);
	}

	return pvo;
    }

    public static PostalAddressVO getPostalAddressVO(String property, String propertyQualifier, Integer streetSuburbID, String dpid, String ausbar) throws RemoteException {
	PostalAddressVO pvo = getPostalAddressVO(property, propertyQualifier, streetSuburbID);
	if (pvo != null) {
	    pvo.setDpid(dpid);
	    pvo.setAusbar(ausbar);
	}

	return pvo;
    }

    public static PostalAddressVO getPostalAddressVO(String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf, String ausbar) throws RemoteException {
	PostalAddressVO svo = getPostalAddressVO(property, propertyQualifier, streetSuburbID);
	if (svo != null) {
	    svo.setDpid(dpid);
	    svo.setLatitude(latitude);
	    svo.setLongitude(longitude);
	    svo.setGnaf(gnaf);
	    svo.setAusbar(ausbar);
	}

	return svo;
    }

    /**
     * Return the address filled with the data from the XML DOM node. Set the
     * mode to "History" and set the value object to read only.
     */
    public static PostalAddressVO getPostalAddressVO(Node addressNode) throws SystemException {
	PostalAddressVO svo = new PostalAddressVO(addressNode);
	return svo;
    }

    private static StreetSuburbVO getStreetSuburbVO(Integer stsubid) throws RemoteException {
	CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	return cMgr.getStreetSuburb(stsubid);
    }

}
