package com.ract.common.hibernate;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;

import com.ract.util.LogUtil;

/**
 * @author jyh
 */
public class HibernatePage implements Serializable {

    private int startPage;
    private int pageSize;
    private int totalElementSize;
    private List elements;

    public List getElements() {
	return elements;
    }

    public int getPageSize() {
	LogUtil.debug(this.getClass(), "getPageSize() =" + pageSize);
	return pageSize;
    }

    public int getEndPage() {
	int endPage = (getStartPage() + getPageSize()) - 1;
	LogUtil.debug(this.getClass(), "endPage=" + endPage);
	LogUtil.debug(this.getClass(), "getTotalPages()=" + getTotalPages());
	if (endPage > getTotalElementSize()) {
	    endPage = getTotalElementSize();
	}
	LogUtil.debug(this.getClass(), "endPage=" + endPage);
	return endPage;
    }

    public int getCurrentPage() {
	// get startPage
	int startIndex = 0;
	int endIndex = 0;
	int pageNum = 0;
	boolean foundCurrentPage = false;
	for (int i = 0; i < getTotalPages() && !foundCurrentPage; i++) {
	    pageNum = (i + 1);
	    startIndex = (i * pageSize) + 1;
	    endIndex = pageNum * pageSize;
	    if (startPage >= startIndex && startPage <= endIndex) {
		foundCurrentPage = true;
	    }
	}
	LogUtil.debug(this.getClass(), "getCurrentPage() pageNum=" + pageNum);
	return pageNum;
    }

    public int getTotalPages() {
	double totalPages = 0; // eg. 11/10 = 1 + 1

	double eSize = getTotalElementSize();
	double pSize = getPageSize();
	totalPages = Math.ceil(eSize / pSize);

	LogUtil.debug(this.getClass(), "totalPages=" + totalPages);
	return (int) totalPages;
    }

    public int getStartPage() {
	return startPage;
    }

    public int getTotalElementSize() {
	return totalElementSize;
    }

    public HibernatePage(Criteria crit, int startPage, int pageSize) {
	// set the total size before pagination
	totalElementSize = crit.list().size();
	LogUtil.debug(this.getClass(), "totalElementSize=" + totalElementSize);

	LogUtil.debug(this.getClass(), "pageSize=" + pageSize);

	this.startPage = startPage;
	LogUtil.debug(this.getClass(), "startPage=" + startPage);
	crit.setFirstResult(startPage - 1); // 0 based index
	LogUtil.debug(this.getClass(), "setFirstResult startPage=" + startPage);
	if (pageSize > totalElementSize) {
	    pageSize = totalElementSize; // ie. less than a page
	}
	this.pageSize = pageSize;
	LogUtil.debug(this.getClass(), "pageSize=" + pageSize);
	crit.setMaxResults(pageSize);

	elements = crit.list();
	LogUtil.debug(this.getClass(), "elements=" + elements);
    }

}
