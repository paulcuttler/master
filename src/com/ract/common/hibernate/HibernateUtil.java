package com.ract.common.hibernate;

import org.hibernate.Session;

import com.ract.util.LogUtil;

/**
 * <p>
 * Expose services for hibernate.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class HibernateUtil {

    /**
     * Threadsafe shared constant
     */
    // private static final SessionFactory sessionFactory;

    /**
     * Initialise the session factory with the session factory deployed to J2EE
     * app server
     */
    // static
    // {
    // /**
    // * @todo only supports one session factory at the moment that is
    // associated with the ClientDS
    // * datasource
    // */
    // final String JNDI_HIBERNATE_SESSION_FACTORY =
    // "java:/hibernate/SessionFactory";
    // try
    // {
    // InitialContext ctx = new InitialContext();
    //
    // sessionFactory = (SessionFactory)
    // ctx.lookup(JNDI_HIBERNATE_SESSION_FACTORY);
    //
    // }
    // catch(NamingException ex)
    // {
    // throw new ExceptionInInitializerError(ex);
    // }
    // }

    /**
     * Get a hibernate session associated with the session factory.
     * 
     * @throws HibernateException
     * @return Session
     */
    // public static Session openSession() throws HibernateException
    // {
    // Session session = sessionFactory.openSession();
    // LogUtil.debug("openSession", "session=" + session);
    // return session;
    // }

    /**
     * Get a hibernate session associated with any current transactions.
     * 
     * @throws HibernateException
     * @return Session
     */
    // public static Session getCurrentSessionX() throws HibernateException
    // {
    // Session session = sessionFactory.getCurrentSession();
    //
    // LogUtil.debug("getCurrentSession", "session=" + session);
    // return session;
    // }

    /**
     * Close the hibernate session if it not null and open.
     * 
     * @param session
     *            Session
     */
    // public static void closeSession(Session session)
    // {
    // if (session != null &&
    // session.isOpen())
    // {
    // LogUtil.debug("","closeSession="+session);
    // //do not explicitly close session when invoked via CMT/BMT.
    // session.close();
    // }
    // }

    public static void flushSession(Session session) {
	if (session != null && session.isOpen()) {
	    LogUtil.debug("", "flushSession=" + session);
	    session.flush();
	}

    }

    /**
     * Output the session open and close stats.
     */
    // public static void outputStatistics()
    // {
    // Statistics statistics = sessionFactory.getStatistics();
    // statistics.logSummary();
    // }

}
