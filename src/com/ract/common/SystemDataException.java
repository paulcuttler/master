package com.ract.common;

/**
 * <p>
 * Flag that a system component has provided invalid data.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class SystemDataException extends GenericException {
    public SystemDataException() {
    }

    public SystemDataException(String msg) {
	super(msg);
    }

    public SystemDataException(Throwable t) {
	super("", t);
    }

    public SystemDataException(String msg, Throwable t) {
	super(msg, t);
    }

}
