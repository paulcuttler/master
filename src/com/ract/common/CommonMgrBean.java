package com.ract.common;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.FinderException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.ract.common.admin.AdminCodeDescription;
import com.ract.common.menu.MenuItem;
import com.ract.travel.TravelBranch;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;

@Stateless
@Remote({ CommonMgr.class })
@Local({ CommonMgrLocal.class })
public class CommonMgrBean {

	@PersistenceContext(unitName = "master")
	EntityManager em;

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource dataSource;

	@PersistenceContext(unitName = "master")
	Session hsession;

	public Gift getGift(String giftCode) throws RemoteException {
		LogUtil.debug(this.getClass(), "giftCode " + giftCode);
		Gift gift = null;
		try {
			gift = (Gift) hsession.get(Gift.class, giftCode);
			LogUtil.debug(this.getClass(), "gift " + gift);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to load gift.", ex);
		}
		return gift;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void startExtract(String extractName, String userId) throws GenericException {

		ExtractControl ec = new ExtractControl();
		ec.setExtractName(extractName);
		ec.setStartDate(new DateTime());
		ec.setUserId(userId);

		ExtractControl tmp = (ExtractControl) get(ExtractControl.class, ec.getExtractName());
		if (tmp != null) {
			throw new GenericException("Extract already started at " + tmp.getStartDate().formatMediumDate() + " by " + tmp.getUserId() + ".");
		} else {
			// create control record
			create(ec);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void endExtract(String extractName) throws GenericException {
		ExtractControl ec = (ExtractControl) get(ExtractControl.class, extractName);
		if (ec == null) {
			throw new GenericException("Extract not started.");
		} else {
			// create control record
			delete(ec);
		}
	}

	public Object update(Object o) {
		return em.merge(o);
	}

	public Collection getMenuList() {
		List menuList = hsession.createCriteria(MenuItem.class).add(Restrictions.eq("menuItemPK.menuItemName", "")).list();
		return menuList;
	}

	public void delete(Object o) {
		o = em.merge(o);
		em.remove(o);
	}

	public void create(Object o) {
		em.persist(o);
	}

	public Object get(Class entityClass, Object key) {
		return em.find(entityClass, key);
	}

	public Collection getMenuItemList(String menuName) {
		List menuList = hsession.createCriteria(MenuItem.class).add(Restrictions.eq("menuItemPK.menuName", menuName)).list();
		return menuList;
	}

	public Collection<BusinessType> getBusinessTypes() {
		Query businessTypeQuery = em.createQuery("select e from BusinessType e");
		return businessTypeQuery.getResultList();
	}

	public ReferenceDataVO getReferenceData(String type, String code) {
		ReferenceDataVO refData = em.find(ReferenceDataVO.class, new ReferenceDataPK(code, type));
		return refData;
	}

	public MemoVO getMemo(Integer memoNumber) {
		MemoVO memo = em.find(MemoVO.class, memoNumber);
		return memo;
	}

	public void createMemo(MemoVO memo) {
		em.persist(memo);
	}

	public Collection getTaxRates() {
		Query taxQuery = em.createQuery("select e from TaxRateVO e");
		return taxQuery.getResultList();
	}

	public void deleteMemo(MemoVO memo) {
		em.remove(memo);
	}

	public MemoVO updateMemo(MemoVO memo) {
		return em.merge(memo);
	}

	public ArrayList getStringResultArray(String sql) throws RemoteException {
		LogUtil.debug(this.getClass(), "getStringResultArray start");
		LogUtil.debug(this.getClass(), "sql=" + sql);
		Connection connection = null;
		ArrayList resultArray = new ArrayList();
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			String value = null;
			// add a blank
			resultArray.add("");
			while (rs.next()) {
				value = rs.getString(1);
				if (value != null && !value.trim().equals("")) {
					resultArray.add(value);
				}
			}
		} catch (SQLException se) {
			throw new RemoteException("Unable to get result set array. " + sql, se);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		LogUtil.debug(this.getClass(), "getStringResultArray end");
		return resultArray;
	}

	public Collection getGiftList() throws RemoteException {
		Collection giftList = null;
		try {
			giftList = new ArrayList(hsession.createCriteria(Gift.class).list());
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}
		return giftList;
	}

	public Collection getGiftListByType(String type) throws RemoteException {
		Collection giftList = null;
		try {
			giftList = new ArrayList(hsession.createCriteria(Gift.class).add(Restrictions.eq("giftType", type)).list());
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}
		return giftList;
	}

	// 1164695612870

	public Collection getTravelBranchList() throws RemoteException {
		Collection travelBranchList = null;
		try {
			travelBranchList = new ArrayList(hsession.createCriteria(TravelBranch.class).list());
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}
		// finally
		// {
		// HibernateUtil.closeSession(hsession);
		// }
		return travelBranchList;
	}

	public Collection getPublicationList() throws RemoteException {
		Collection publicationList = null;
		try {
			publicationList = new ArrayList(hsession.createCriteria(Publication.class).add(Restrictions.eq("systemManaged", false)).list());
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}
		return publicationList;
	}

	public Publication getPublication(String publicationCode) {
		return (Publication) hsession.get(Publication.class, publicationCode);
	}

	public static final int CRITERIA_RESULTS = 10;

	public Collection getResultsPerPage() throws RemoteException {
		ArrayList sorts = new ArrayList();
		// sorts.add("10 results");
		sorts.add("20 results");
		sorts.add("30 results");
		sorts.add("50 results");
		sorts.add("100 results");
		return sorts;

	}

	/**
	 * Count the records produced in the result.
	 * 
	 * @param sql String
	 * @throws ObjectNotFoundException
	 * @throws RemoteException
	 * @return int
	 */
	public int countRecords(String sql) throws RemoteException {
		Connection connection = null;
		PreparedStatement statement = null;
		int recordCount = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				recordCount++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RemoteException("Error: " + sql + " " + e.getMessage());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return recordCount;
	}

	/**
	 * Get the printform object for the form ,print group and tray specified
	 * 
	 * @param formType the form type associated with the report
	 * @param printGroup the print group associated with the user
	 * @return the printer object
	 */
	public PrintForm getPrintForm(String formType, String printerGroup) throws RemoteException {
		LogUtil.debug(this.getClass(), "getPrintForm start");
		LogUtil.debug(this.getClass(), "formType=" + formType);
		LogUtil.debug(this.getClass(), "printerGroup=" + printerGroup);
		String sql = "select  a.pr_printer, a.pr_prtype, a.pr_options, b.pf_form_options ";
		sql += "from PUB.x_printer a, ";
		sql += "PUB.x_prnt_form b where a.pr_printer = b.pf_printer ";
		sql += "and b.pf_form_type = ? ";
		sql += "and b.pf_print_grp = ? ";

		LogUtil.debug(this.getClass(), "sql=" + sql);

		Connection connection = null;
		PreparedStatement statement = null;
		PrintForm printForm = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setString(1, formType.trim());
			statement.setString(2, printerGroup.trim());

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				printForm = new PrintForm();
				printForm.setPrinterName(rs.getString(1));
				printForm.setPrinterType(rs.getString(2));
				printForm.setPrinterOptions(rs.getString(3));
				printForm.setFormOptions(rs.getString(4));
			} else {
				throw new RemoteException("Unable to determine print job for form type (" + formType + ") and printer group (" + printerGroup + ") combination.");
			}
		} catch (SQLException ex) {
			throw new RemoteException("Unable to locate print job.", ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		LogUtil.debug(this.getClass(), "getPrintForm end");
		return printForm;
	}

	/**
	 * Return a list of all of the reference data for the type. Returns an empty list if reference data of the specified type could not be found.
	 */
	public Collection getReferenceDataListByType(String type) throws RemoteException {
		Query refDataQuery = em.createQuery("select e from ReferenceDataVO e where e.referenceDataPK.referenceType = ?1");
		refDataQuery.setParameter(1, type);
		return refDataQuery.getResultList();
	}

	/**
	 * Return the current tax rate.
	 * 
	 * @todo fetch the current tax rate instead of the latest one.
	 */
	public TaxRateVO getCurrentTaxRate() throws RemoteException {
		return TaxRateCache.getInstance().getCurrentTaxRate();
	}

	/**
	 * Return the branch value object for the specified sales branch code. Returns null if the sales branch code could not be found.
	 */
	public BranchVO getBranch(int branchNumber) throws RemoteException {
		return em.find(BranchVO.class, branchNumber);
	}

	public Collection getBranchList() throws RemoteException, FinderException {
		Query branchQuery = em.createQuery("select e from BranchVO e");
		return branchQuery.getResultList();
	}

	public SortedSet getBranchRegionsAndDescriptions() throws RemoteException {
		Collection b = null;

		try {
			b = getBranchList();
		} catch (Exception e) {
		}
		SortedSet br = new TreeSet();

		Iterator it = b.iterator();

		while (!(b == null) & it.hasNext()) {
			BranchVO branch = (BranchVO) it.next();
			br.add(new AdminCodeDescription(new Integer(branch.getBranchNumber()).toString(), branch.getDescription(), branch.getDescription()));
		}

		return br;
	}

	public Collection getPrinterGroups() throws RemoteException {
		String sql = "SELECT \"pg_prgrp\" FROM PUB.\"x_print-grp\"";
		Collection salesBranchCodes = this.getCodeList(sql);
		return salesBranchCodes;
	}

	public Hashtable getExtractParameters(String extractName) throws RemoteException {
		Hashtable extractParams = new Hashtable();
		String sql = "SELECT [tab-key], [tab-desc] from PUB.[gn-table] where [tab-name] = ? order by [tab-key]";
		PreparedStatement statement = null;
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, extractName.trim());
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				String key = rs.getString(1);
				String value = rs.getString(2);

				extractParams.put(key, value);

				//
				// Put value(name), key for this parameter into the hashtable
				// It'll make things easier later on
				//
				int n = key.indexOf("_NAME");
				if (n > -1) {
					extractParams.put(value, key.substring(0, n));
				}

			}
		} catch (SQLException e) {
			throw new RemoteException("Error getting code list. " + e + "\n" + sql);
		} finally {
			ConnectionUtil.closeConnection(conn, statement);
		}
		return extractParams;
	}

	public SortedSet getAllRoadSideExtracts() throws RemoteException {
		return this.getAllExtracts("ROADSIDE_EXTRACTS");
	}

	public SortedSet getAllExtracts(String extractName) throws RemoteException {
		String sql = "SELECT [tab-key], [tab-desc], [tab-dec0]  from PUB.[gn-table] where [tab-name] = '" + extractName + "' order by [tab-dec0], [tab-key]";

		return this.getCodesAndDescriptions(sql);

	}

	public SortedSet getPrinterCodesAndDescriptions() throws RemoteException {
		String sql = "SELECT pg_prgrp,pg_desc FROM PUB.\"x_print-grp\"";
		return this.getCodeAndDescriptionList(sql);
	}

	public SortedSet getSalesBranchCodesAndDescriptions() throws RemoteException {
		String sql = "SELECT \"sales-branch\",description FROM PUB.\"gn-slsbch\"";
		return this.getCodeAndDescriptionList(sql);
	}

	public SortedSet getUpperSalesBranchCodesAndDescriptions() throws RemoteException {
		String sql = "SELECT upper(\"sales-branch\"),description FROM PUB.\"gn-slsbch\"";
		return this.getCodeAndDescriptionList(sql);
	}

	public Collection getSalesBranchCodes() throws RemoteException {
		String sql = "SELECT \"sales-branch\" FROM PUB.\"gn-slsbch\"";
		Collection printerGroups = getCodeList(sql);
		return printerGroups;
	}

	private Collection getCodeList(String sql) throws RemoteException {
		Collection codeList = new Vector();
		PreparedStatement statement = null;
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				codeList.add(rs.getString(1));
			}
		} catch (SQLException e) {
			throw new RemoteException("Error getting code list. " + e + "\n" + sql);
		} finally {
			ConnectionUtil.closeConnection(conn, statement);
		}
		return codeList;
	}

	private SortedSet getCodeAndDescriptionList(String sql) throws RemoteException {
		SortedSet codeList = new TreeSet();
		PreparedStatement statement = null;
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				String c = rs.getString(1);
				String d = rs.getString(2);
				String dc = d.trim() + "  (" + c.trim() + ")";
				codeList.add(new AdminCodeDescription(c, dc));
			}
		} catch (SQLException e) {
			throw new RemoteException("Error getting code list. " + e + "\n" + sql);
		} finally {
			ConnectionUtil.closeConnection(conn, statement);
		}
		return codeList;
	}

	private SortedSet getCodesAndDescriptions(String sql) throws RemoteException {
		SortedSet codeList = new TreeSet();
		PreparedStatement statement = null;
		String sortKey = null;

		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				String c = rs.getString(1);
				String d = rs.getString(2);
				try {
					sortKey = rs.getString(3);
				} catch (Exception e) {
				}
				if (sortKey == null) {
					sortKey = c;
				}
				codeList.add(new AdminCodeDescription(c.trim(), d.trim(), sortKey));
			}
		} catch (SQLException e) {
			throw new RemoteException("Error getting code list. " + e + "\n" + sql);
		} finally {
			ConnectionUtil.closeConnection(conn, statement);
		}
		return codeList;
	}

	/**
	 * reset the sales branch cache
	 */
	public void resetSalesBranchCache() {
		SalesBranchCache.getInstance().reset();
	}

	public Collection getSalesBranchList() {
		Query salesBranchQuery = em.createQuery("select e from SalesBranchVO e order by e.salesBranchName");
		return salesBranchQuery.getResultList();
	}

	public SalesBranchVO getSalesBranch(String salesBranchCode) {
		return em.find(SalesBranchVO.class, salesBranchCode);
	}

	/**
	 * Return the sales branch value object for the specified sales branch code. Returns null if the sales branch code could not be found.
	 */
	public SalesBranchVO getCachedSalesBranch(String salesBranchCode) throws RemoteException {
		return SalesBranchCache.getInstance().getSalesBranch(salesBranchCode);
	}

	public ArrayList getCachedSalesBranchList() throws RemoteException {
		return SalesBranchCache.getInstance().getSalesBranchList();
	}

	public void createStreetSuburb(StreetSuburbVO streetSuburb) throws RemoteException {
		hsession.save(streetSuburb);
	}

	public void updateStreetSuburb(StreetSuburbVO streetSuburb) throws RemoteException {
		hsession.merge(streetSuburb);
	}

	public void deleteStreetSuburb(StreetSuburbVO streetSuburb) throws RemoteException {
		hsession.delete(streetSuburb);
	}

	/**
	 * Get street suburb object with data populated for a street suburb id.
	 */
	public StreetSuburbVO getStreetSuburb(Integer stSubID) throws RemoteException {
		LogUtil.debug(this.getClass(), "stSubID " + stSubID);
		StreetSuburbVO streetSuburb = null;
		try {
			streetSuburb = (StreetSuburbVO) hsession.get(StreetSuburbVO.class, stSubID);
			LogUtil.debug(this.getClass(), "streetSuburb " + streetSuburb);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to load streetSuburb.", ex);
		}
		return streetSuburb;

	}

	public Collection getStreetSuburbList(String streetName, String suburbName) throws RemoteException {
		Collection titleList = null;
		try {
			titleList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.eq("street", streetName)).add(Restrictions.eq("suburb", suburbName)).add(Restrictions.isNull("deleteDate")).list());

		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find Street/Suburbs.", ex);
		}
		return titleList;

	}

	public List<StreetSuburbVO> getStreetSuburbList(String streetName, String suburbName, String state) throws RemoteException {
		List<StreetSuburbVO> suburbList = new ArrayList<StreetSuburbVO>();
		try {
			suburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.eq("street", streetName)).add(Restrictions.eq("suburb", suburbName)).add(Restrictions.eq("state", state)).add(Restrictions.isNull("deleteDate")).list());

		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find Suburbs.", ex);
		}
		return suburbList;
	}

	public List<StreetSuburbVO> getSuburbList(String suburbName, String state) throws RemoteException {
		List<StreetSuburbVO> suburbList = new ArrayList<StreetSuburbVO>();
		try {
			suburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.eq("suburb", suburbName)).add(Restrictions.eq("state", state)).add(Restrictions.isNull("deleteDate")).list());

		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find Suburbs.", ex);
		}
		return suburbList;
	}

	/**
	 * Return the address value object
	 * 
	 * @param type The type of address
	 * @param property The property title
	 * @param streetNumber the number of the street
	 * @param propertyQualifier the fully qualified property identifier
	 * @param stsubid the street suburb id to retrieve the street suburb record.
	 */
	public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID) throws RemoteException {
		return AddressFactory.getAddressVO(type, property, propertyQualifier, streetSuburbID);
	}

	public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid) throws RemoteException {
		return AddressFactory.getAddressVO(type, property, propertyQualifier, streetSuburbID, dpid);
	}

	public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String ausbar) throws RemoteException {
		return AddressFactory.getAddressVO(type, property, propertyQualifier, streetSuburbID, dpid, ausbar);
	}

	public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf) throws RemoteException {
		return AddressFactory.getAddressVO(type, property, propertyQualifier, streetSuburbID, dpid, latitude, longitude, gnaf);
	}

	public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf, String ausbar) throws RemoteException {
		return AddressFactory.getAddressVO(type, property, propertyQualifier, streetSuburbID, dpid, latitude, longitude, gnaf, ausbar);
	}

	/**
	 * Return the value of a specified system parameter based on the parameter category and parameter name. If the parameter cound not be found then return null.
	 */
	public String getSystemParameterValue(String category, String param) throws RemoteException {
		return SystemParameterCache.getInstance().getParameterValue(category, param);
	}

	public SystemParameterVO getSystemParameter(String category, String param) {
		return em.find(SystemParameterVO.class, new SystemParameterPK(category, param));
	}

	@SuppressWarnings("unchecked")
	public Collection<SystemParameterVO> getSystemParameterList(String category, String param) {
		Collection<SystemParameterVO> allSystemParameters = null;
		try {
			allSystemParameters = getSystemParameterList();
		} catch (RemoteException e) {
			LogUtil.log(this.getClass(), "Can't find MIDAS EXPORT files");
			e.printStackTrace();
		}

		List<SystemParameterVO> requiredSystemParameters = new ArrayList();

		for (Iterator i = allSystemParameters.iterator(); i.hasNext();) {
			SystemParameterVO svo = (SystemParameterVO) i.next();

			if (svo.getCategory().equalsIgnoreCase(category) && svo.getParameter().startsWith(param)) {
				requiredSystemParameters.add(svo);
			}
		}
		return requiredSystemParameters;
	}

	public void setSystemParameterValue(String category, String param, String value) throws RemoteException {
		SystemParameterCache.getInstance().setSystemParameterValue(category, param, value);
	}

	/**
	 * Return a list of system parameters, as SystemParameterVO, held in the system parameter cache.
	 */
	public Collection getCachedSystemParameterList() throws RemoteException {
		return SystemParameterCache.getInstance().getItemList();
	}

	public void resetSystemParameterCache() {
		SystemParameterCache.getInstance().reset();
	}

	/**
	 * retrieve letter data from gn-letter table.
	 */
	public LetterStruct getLetter(String subSystem, String key) throws RemoteException {
		LetterStruct letter = null;
		StringBuffer sql = new StringBuffer();
		sql.append("Select \"type-text\",\"heading-text\",\"body-text\",\"body-text2\"");
		sql.append(" from PUB.\"gn-letter\"");
		sql.append(" where \"sub-sys\" = ? and \"type\" = ?");
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
			statement = connection.prepareStatement(sql.toString());

			statement.setString(1, subSystem);
			statement.setString(2, key);

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				letter = new LetterStruct();
				letter.setSubSystem(subSystem);
				letter.setType(key);
				letter.setTypeText(rs.getString(1));
				letter.setHeading(rs.getString(2));
				letter.setBody1(rs.getString(3));
				letter.setBody2(rs.getString(4));
			} else {
				throw new RemoteException("Letter text not found for " + subSystem + "/" + key);
			}
		} catch (Exception e) {
			throw new RemoteException("Error in CommonMgrBean.getLetter: " + "\n" + sql.toString() + "\nsubSystem: " + subSystem + "\n     type: " + key + "\n" + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return letter;
	}

	/**
	 * get the text data for general text
	 */
	public String getGnText(String subSys, String type, String name) throws RemoteException {
		LogUtil.debug(this.getClass(), "subSys=" + subSys);
		LogUtil.debug(this.getClass(), "type=" + type);
		LogUtil.debug(this.getClass(), "name=" + name);
		String textStr = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select text_data from PUB.gn_text ");
		sql.append(" where sub_sys = ? and text_type = ? and text_name = ?");
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql.toString());
			statement.setString(1, subSys);
			statement.setString(2, type);
			statement.setString(3, name);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				textStr = rs.getString(1);
			} else {
				throw new RemoteException("Unable to get gn_text for " + subSys + "/" + type + "/" + name);
			}
		} catch (SQLException e) {
			throw new RemoteException("Unable to get gn_text for " + subSys + "/" + type + "/" + name + "\n" + e + "");
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return textStr;
	}

	/*
	 * Routines for management of the gn_text table
	 */

	public GnTextVO getGnTextVO(String subSystem, String textType, String textName) throws RemoteException {

		GnTextVO textVO = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select text_data,notes from PUB.gn_text ");
		sql.append(" where sub_sys = ? and text_type = ? and text_name = ?");
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql.toString());
			statement.setString(1, subSystem);
			statement.setString(2, textType);
			statement.setString(3, textName);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				textVO = new GnTextVO();
				textVO.setSubSystem(subSystem);
				textVO.setTextName(textName);
				textVO.setTextType(textType);
				textVO.setTextData(rs.getString(1));
				textVO.setNotes(rs.getString(2));
			} else {
				throw new RemoteException("Unable to get gn_text for " + subSystem + "/" + textType + "/" + textName);
			}
		} catch (SQLException e) {
			throw new RemoteException("Unable to get gn_text for " + subSystem + "/" + textType + "/" + textName + "\n" + e + "");
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return textVO;
	}

	public void setGnTextFromVO(GnTextVO gnTextVO) throws RemoteException {
		String sql = "select * from PUB.gn_text where sub_sys = ? and text_type = ? and text_name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setString(1, gnTextVO.getSubSystem());
			statement.setString(2, gnTextVO.getTextType());
			statement.setString(3, gnTextVO.getTextName());
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				rs.close();
				updateGnText(gnTextVO);
			} else {
				rs.close();
				insertGnText(gnTextVO);
			}
		} catch (Exception sqlE) {
			throw new RemoteException("Unable to store gn-text data");
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	private void updateGnText(GnTextVO gnTextVO) throws SQLException {
		String sql = "update pub.gn_text set text_data = ?, notes = ? " + " where sub_sys = ? and text_type = ? and text_name = ?";
		Connection connection = null;
		PreparedStatement statement = null;

		connection = dataSource.getConnection();
		statement = connection.prepareStatement(sql);
		statement.setString(1, gnTextVO.getTextData());
		statement.setString(2, gnTextVO.getNotes());
		statement.setString(3, gnTextVO.getSubSystem());
		statement.setString(4, gnTextVO.getTextType());
		statement.setString(5, gnTextVO.getTextName());
		try {

			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error updating gnText in updateGnText: " + e);
			throw e;
		}
		ConnectionUtil.closeConnection(connection, statement);
	}

	private void insertGnText(GnTextVO gnTextVO) throws SQLException {
		String sql = "insert into pub.gn_text (text_data, notes, sub_sys, text_type, text_name) values(?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement statement = null;

		connection = dataSource.getConnection();
		statement = connection.prepareStatement(sql);
		statement.setString(1, gnTextVO.getTextData());
		statement.setString(2, gnTextVO.getNotes());
		statement.setString(3, gnTextVO.getSubSystem());
		statement.setString(4, gnTextVO.getTextType());
		statement.setString(5, gnTextVO.getTextName());
		try {
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error inserting gnText in insert GnText: " + e);
			throw e;
		}
		ConnectionUtil.closeConnection(connection, statement);
	}

	/**
	 * returns an array list of GnTextVO objects which match the criteria provided. If the textType field is null, returns all for the given subSystem If the sybsystem is null then returns all
	 */
	public Collection getGnTextList(String subSystem, String textType) throws RemoteException {
		GnTextVO textVO = null;
		ArrayList textList = new ArrayList();
		String sql = "select sub_sys, text_type, text_name, text_data, notes from PUB.gn_text ";
		if (subSystem != null) {
			sql += " where sub_sys = ?";
			if (textType != null) {
				sql += " and text_type = ?";
			}
		}
		sql += " order by sub_sys, text_type, text_name";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			if (subSystem != null) {
				statement.setString(1, subSystem);
				if (textType != null) {
					statement.setString(2, textType);
				}
			}
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				textVO = new GnTextVO();
				textVO.setSubSystem(rs.getString(1));
				textVO.setTextType(rs.getString(2));
				textVO.setTextName(rs.getString(3));
				textVO.setTextData(rs.getString(4));
				textVO.setNotes(rs.getString(5));
				textList.add(textVO);
			}
		} catch (Exception e) {
			throw new RemoteException("Unable to retrieve gnText data: " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return textList;
	}

	public void updateSystemParameter(SystemParameterVO systemParameter) throws RemoteException {
		em.merge(systemParameter);
	}

	public void createSystemParameter(SystemParameterVO systemParameter) throws SQLException {
		em.persist(systemParameter);
	}

	public Collection getSystemParameterList() throws RemoteException {
		Query systemParameterQuery = em.createQuery("select e from SystemParameterVO e");
		return systemParameterQuery.getResultList();
	}

	/**
	 * Returns a collection of ReferenceDataVO objects Only sets the key fields: refType, refCode
	 */
	public Collection getReferenceDataList() {
		Query refDataQuery = em.createQuery("select e from ReferenceDataVO e");
		return refDataQuery.getResultList();
	}

	public void updateReferenceData(ReferenceDataVO referenceData) {
		em.merge(referenceData);
	}

	/**
	 * get the biller codes from gn-table
	 * 
	 * @throws RemoteException
	 */
	public Collection getBillerCodes() throws RemoteException {
		String sql = "select [tab-desc] from pub.[gn-table] ";
		sql += "where [tab-name] = ? "; // BILLER CODE
		sql += "and [tab-key] like ? ";
		LogUtil.debug(this.getClass(), "sql=" + sql);
		Connection connection = null;
		PreparedStatement statement = null;
		ArrayList billerCodes = new ArrayList();
		// int i = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setString(1, SystemParameterVO.CATEGORY_ELECTRONIC_PAYMENT_DETAILS);
			statement.setString(2, SystemParameterVO.BPAY_BILLER_CODE + CommonConstants.WILDCARD);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				billerCodes.add(rs.getString(1));
			}
		} catch (SQLException ex) {
			throw new RemoteException("Unable to retrieve biller codes.", ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return billerCodes;
	}

	public Collection getMotorNewsAddSurnameKeyEqualList() throws RemoteException {

		return getMotorNewsAddSurnameList(SystemParameterVO.MN_ADD_SURNAME_EQUAL + CommonConstants.WILDCARD);

	}

	public Collection getMotorNewsAddSurnameKeyLikeList() throws RemoteException {

		return getMotorNewsAddSurnameList(SystemParameterVO.MN_ADD_SURNAME_LIKE + CommonConstants.WILDCARD);

	}

	public Collection getMotorNewsAddSurnameList(String listType) throws RemoteException {
		String sql = "select [tab-desc] from pub.[gn-table] ";
		sql += "where [tab-name] = ? "; // Motor News
		sql += "and [tab-key] like ? ";
		LogUtil.debug(this.getClass(), "getMotorNewsAddSurnameList sql=" + sql);
		LogUtil.debug(this.getClass(), "getMotorNewsAddSurnameList sql=" + SystemParameterVO.CATEGORY_MOTOR_NEWS);
		LogUtil.debug(this.getClass(), "getMotorNewsAddSurnameList sql=" + listType);
		Connection connection = null;
		PreparedStatement statement = null;
		ArrayList surnameAddList = new ArrayList();
		String val;
		int i = 0;
		int y = 0;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setString(1, SystemParameterVO.CATEGORY_MOTOR_NEWS);
			statement.setString(2, listType);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				i++;
				val = rs.getString(1);
				y = val.indexOf(" ");
				if (y < 0) {
					y = val.length();
				}

				surnameAddList.add(val.substring(0, y));
			}
		} catch (SQLException ex) {
			surnameAddList = null;
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		LogUtil.debug(this.getClass(), "getMotorNewsAddSurnameList sql=" + i);
		return surnameAddList;
	}

	public int getCadExtractMembershipCutOff() throws RemoteException {

		return getCadParameter(SystemParameterVO.CAD_EXTRACT_CUTOFF, 1460);
	}

	public int getCadDeleteFlagCutOff() throws RemoteException {

		return getCadParameter(SystemParameterVO.CAD_DELETE_FLAG, 90);
	}

	private int getCadParameter(String param, int defaultValue) {
		String sql = "select [tab-dec0] from pub.[gn-table] ";
		sql += "where [tab-name] = ? ";
		sql += "and [tab-key] like ? ";
		LogUtil.debug(this.getClass(), "sql=" + sql);
		Connection connection = null;
		PreparedStatement statement = null;

		int returnValue = defaultValue;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setString(1, SystemParameterVO.CATEGORY_CAD);
			statement.setString(2, param);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				returnValue = new Integer((rs.getString(1))).intValue();
			}
		} catch (Exception ex) {
			LogUtil.log(this.getClass(), "Unable to retrieve cut off days. Using 1460 days");
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return returnValue;
	}

	/**
	 * Initialise the list of server sockets and start them up so that they are waiting to receive on their ports.
	 */
	public void initialiseSocketServers() throws RemoteException {
		SocketServerCache.getInstance().initialiseList();
	}

	/**
	 * Return a list (String) of server socket keys. The keys can be used to query details of a particular server socket or to manage the state of a particular server socket.
	 */
	public ArrayList getSocketServerKeyList() throws RemoteException {
		return SocketServerCache.getInstance().getSocketServerKeyList();
	}

	/**
	 * Get the active thread count.
	 */
	public int getSocketServerThreadCount(String socketServerKey) throws RemoteException {
		return SocketServerCache.getInstance().getSocketServerThreadCount(socketServerKey);
	}

	/**
	 * Stop the specified server socket.
	 */
	public void stopSocketServer(String socketServerKey) throws RemoteException {
		SocketServerCache.getInstance().stopSocketServer(socketServerKey);
	}

	/**
	 * Stop and re-establish the specified server socket.
	 */
	public void resetSocketServer(String socketServerKey) throws RemoteException {
		SocketServerCache.getInstance().resetSocketServer(socketServerKey);
	}

	/**
	 * Start the specified server socket.
	 */
	public void startSocketServer(String socketServerKey) throws RemoteException {
		SocketServerCache.getInstance().startSocketServer(socketServerKey);
	}

	/**
	 * Start all of the server sockets
	 */
	public void startAllSocketServers() throws RemoteException {
		SocketServerCache.getInstance().startAll();
	}

	/**
	 * Stop all of the server sockets
	 */
	public void stopAllSocketServers() throws RemoteException {
		SocketServerCache.getInstance().stopAll();
	}

	/**
	 * Stop and re-establish all of the server sockets
	 */
	public void resetAllSocketServers() throws RemoteException {
		SocketServerCache.getInstance().resetAll();
	}

	/**
	 * Return the port number used by the specified server socket.
	 */
	public Exception getSocketServerException(String socketServerKey) throws RemoteException {
		return SocketServerCache.getInstance().getSocketServerException(socketServerKey);
	}

	/**
	 * Return the state of the specified server socket.
	 */
	public String getSocketServerState(String socketServerKey) throws RemoteException {
		return SocketServerCache.getInstance().getSocketServerState(socketServerKey);
	}

	/**
	 * Return the port number used by the specified server socket.
	 */
	public int getSocketServerPort(String socketServerKey) throws RemoteException {
		return SocketServerCache.getInstance().getSocketServerPort(socketServerKey);
	}

	public String getPeriodStartDate() {
		return DateUtil.formatShortDate(new java.util.Date());
	}

	public String getPeriodEndDate() {
		return DateUtil.formatShortDate(new java.util.Date());
	}
}
