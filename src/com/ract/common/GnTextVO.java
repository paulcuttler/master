package com.ract.common;

/**
 * <p>
 * A gn-text value object.
 * </p>
 * 
 * @author
 * @version 1.0
 */

public class GnTextVO extends ValueObject {

    private String subSystem = null;

    private String textType = null;

    private String textName = null;

    private String textData = null;

    private String notes = null;

    public GnTextVO() {
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    public String getSubSystem() {
	return this.subSystem;
    }

    public void setTextType(String textType) {
	this.textType = textType;
    }

    public String getTextType() {
	return this.textType;
    }

    public void setTextName(String textName) {
	this.textName = textName;
    }

    public String getTextName() {
	return this.textName;
    }

    public void setTextData(String textData) {
	this.textData = textData;
    }

    public String getTextData() {
	return this.textData;
    }

    public void setNotes(String notes) {
	this.notes = notes;
    }

    public String getNotes() {
	return this.notes;
    }
}