package com.ract.common;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

/**
 * Business areas. Not the same as source system.
 * 
 * @author hollidayj
 * 
 */

@Entity
public class BusinessType implements Auditable {

    public DateTime getLastUpdate() {
	return lastUpdate;
    }

    public void setLastUpdate(DateTime lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateId() {
	return lastUpdateId;
    }

    public void setLastUpdateId(String lastUpdateId) {
	this.lastUpdateId = lastUpdateId;
    }

    public static final String BUSINESS_TYPE_MEMBERSHIP = "MEM";

    public String getTypeCode() {
	return typeCode;
    }

    public void setTypeCode(String typeCode) {
	this.typeCode = typeCode;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    @Id
    private String typeCode;

    private String description;

    private DateTime lastUpdate;

    private String lastUpdateId;
}
