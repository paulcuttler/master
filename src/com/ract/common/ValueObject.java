package com.ract.common;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Vector;

import com.ract.util.LogUtil;

/**
 * Class used to extend for value objects (and indirectly for EJBs) it provides
 * a logging interface and the ability to enhance EJB performance via the
 * mutable class.
 * 
 * @author John Holliday
 * @created 31 July 2002
 * @version 1.0
 */

public abstract class ValueObject
// extends Mutable
	implements Serializable, Cloneable {

    /**
     * The normal mode means that the value object is not in one of the other
     * modes.
     */
    public final static String MODE_NORMAL = "Normal";

    /**
     * Loading means the value object is loading data from an entity bean. Some
     * business rules can be relaxed in this circumstance so that data from the
     * database which is no longer valid may still be loaded.
     */
    public final static String MODE_LOADING = "Loading";

    /**
     * History means that the value object contains historical data that should
     * not be modified.
     */
    public final static String MODE_HISTORY = "History";

    /**
     * Create means the value object is being created. This implies that various
     * actions like doing lazy fetches on related data should not be done.
     */
    public final static String MODE_CREATE = "Create";

    /**
     * Update means that the value object is being updated.
     */
    public final static String MODE_UPDATE = "Update";

    /**
     * The mode of the value object
     */
    private String mode = MODE_NORMAL;

    /**
     * Indicates if this value object is readonly. Used to disable the setter
     * methods in the super class if it implements read only functionality.
     */
    private boolean readOnly = false;

    private final int DEFAULT_LOG_LEVEL = 1;

    /**
     * A list of exceptions, as anything that extends Exception, that apply to
     * the state of the value object. This is used when errors are encountered
     * during the load of the value object values and the load must continue.
     * For an example see XML node constructor on MembershipVO.
     */
    private Vector exceptionList = new Vector();

    /**
     * Adds a warning type of exception to the exception list with the given
     * message.
     */
    protected void addWarning(String msg) {
	WarningException we = new WarningException(msg);
	addException(we);
    }

    /**
     * Add a message to the warning list.
     */
    protected void addException(Exception e) {
	this.exceptionList.add(e);
    }

    /**
     * Description of the Method
     * 
     * @param message
     *            Description of the Parameter
     */
    public void log(String message) {
	LogUtil.log(this.getClass(), message);
    }

    /**
     * Sets the readOnly attribute of the ValueObject object
     * 
     * @param isReadOnly
     *            The new readOnly value
     */
    public void setReadOnly(boolean isReadOnly) {
	// LogUtil.log(this.getClass(),"Setting readOnly to " + isReadOnly);

	this.readOnly = isReadOnly;
    }

    /**
     * Gets the mode attribute of the ValueObject object
     * 
     * @return The mode value
     */
    public String getMode() {
	return this.mode;
    }

    /**
     * Return the list of exceptions. Should never be null but may be empty.
     */
    public Vector getExceptionList() {
	return this.exceptionList;
    }

    /**
     * Set the value object mode.
     * 
     * @param newMode
     *            The new mode value
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setMode(String newMode) throws ValidationException {
	validateMode(newMode);
	this.mode = newMode;
    }

    /**
     * Validate the mode. Sub-classes may override this method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
	if (!MODE_NORMAL.equals(newMode) && !MODE_CREATE.equals(newMode) && !MODE_UPDATE.equals(newMode) && !MODE_LOADING.equals(newMode) && !MODE_HISTORY.equals(newMode)) {
	    throw new ValidationException("The new mode '" + newMode + "' is not valid.");
	}
    }

    /**
     * is value object immutable?
     * 
     * @return The readOnly value
     */
    public boolean isReadOnly() {
	return this.readOnly;
    }

    /**
     * Return true if the current mode of the value object is loading. This can
     * be used to relax business validation rules when loading a value object
     * from an entity bean.
     * 
     * @return The loading value
     */
    public boolean isLoading() {
	return this.MODE_LOADING.equals(this.mode);
    }

    /**
     * Return true if the current mode of the value object is NOT loading. This
     * can be used to relax business validation rules when loading a value
     * object from an entity bean.
     * 
     * @return The notLoading value
     */
    public boolean isNotLoading() {
	return !this.MODE_LOADING.equals(this.mode);
    }

    /**
     * Return true if the current mode of the value object is History. This can
     * be used to disable functionality.
     * 
     * @return The history value
     */
    public boolean isHistory() {
	return this.MODE_HISTORY.equals(this.mode);
    }

    /**
     * Return true if the current mode of the value object is not History. This
     * can be used to disable functionality.
     * 
     * @return The notHistory value
     */
    public boolean isNotHistory() {
	return !this.MODE_HISTORY.equals(this.mode);
    }

    /**
     * Return true if exceptions have been raised about the state of the value
     * object.
     */
    public boolean hasException() {
	return !this.exceptionList.isEmpty();
    }

    /**
     * Throws a remote exception if the value object is set to read only.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    protected void checkReadOnly() throws RemoteException {
	if (isReadOnly()) {
	    throw new SystemException("Attempting to update a read only value object : " + this.getClass().getName());
	}
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    protected Object clone() {
	return this.clone();
	// new UnsupportedOperationException("Error: unable to clone object.");
    }
}
