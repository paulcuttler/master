package com.ract.common;

public class ServiceLocatorException extends GenericException {
    public ServiceLocatorException(String message) {
	super(message);
    }

    public ServiceLocatorException(Throwable exception) {
	super(exception);
    }

}
