package com.ract.common;

//import com.sun.org.apache.bcel.internal.classfile.JavaClass;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import com.ract.util.LogUtil;

/**
 * Implements a threaded class that manages a ServerSocket. The runnable thread
 * of this class creates an instance of a ServerSocket and waits on it to
 * receive a message. When a message is received the message is passed of to a
 * threaded worker class to do the message processing while the main thread goes
 * back to waiting on the server socket for another message. The worker class
 * does a call back to an abstract method on the manager class to do the actual
 * processing of the message received. Subclasses must override
 * ServerSocketManager.processClientSocket() to process the message.
 * 
 * @author T. Bakker
 * @version 1.0
 */

public abstract class ServerSocketManager extends Thread implements CachedItem {

    public static final int SIGNAL_STOP = 0;

    public static final int SIGNAL_START = 1;

    public static final int SIGNAL_RESET = 2;

    /**
     * The port number that the server socket wil listen on.
     */
    private int port = 0;

    /**
     * The server socket itself.
     */
    private ServerSocket server = null;

    /**
     * A signal sent to the runnable thread. Allowable values are defined in
     * GenericSocketServer
     */
    private int signal = SIGNAL_START;

    /**
     * The last serious exception raised
     */
    private Exception exception;

    public ServerSocketManager() {
    }

    /**
     * This is the method that executes when the thread is started.
     */
    public void run() {
	LogUtil.log(this.getClass(), this.currentThread().getName() + "-run()...");
	ServerSocket serverSocket = null;
	this.signal = SIGNAL_START;
	Socket client = null;
	// Continuously wait for client connections and process data
	while (true) {
	    LogUtil.debug(this.getClass(), this.currentThread().getName() + "-run(): while(): signal=" + this.signal);
	    if (this.signal == SIGNAL_START) {
		// Accept a client connection, spawn a thread for it
		// and go back to listening for other clients
		try {
		    // Try and establish a server socket if required.
		    if (serverSocket == null) {
			serverSocket = getServerSocket();
			if (serverSocket == null) {
			    LogUtil.fatal(this.getClass(), "Failed to establish server socket.  Stopping...");
			    this.signal = SIGNAL_STOP;
			    client = null;
			}
		    }
		    if (serverSocket != null) {
			client = serverSocket.accept();
		    }
		} catch (Exception e) {
		    client = null;
		    // See why the socket threw an exception. If we have been
		    // signalled
		    // to stop or reset then the exception is not something we
		    // need to worry about.
		    LogUtil.debug(this.getClass(), this.currentThread().getName() + "-run(): while(): exception signal=" + this.signal);
		    if (this.signal == SIGNAL_START) {
			// The signal hasn't changed so it realy is an error we
			// need to worry about.
			// Log it and try a reset.
			this.exception = e;
			LogUtil.fatal(this.getClass(), e);
			this.signal = SIGNAL_RESET;
		    }
		}

		// Get the worker to process the client socket if we have one.
		try {
		    if (client != null) {
			LogUtil.debug(this.getClass(), this.currentThread().getName() + "-run(): while(): starting worker");
			new SocketWorker(this, client).start();
		    }
		} catch (Exception e) {
		    // Don't terminate if the worker throws an exception
		    LogUtil.fatal(this.getClass(), e);
		    try {
			if (client != null) {
			    client.close();
			}
		    } catch (Exception ex) {
		    }
		}
	    } else if (this.signal == SIGNAL_STOP) {
		LogUtil.debug(this.getClass(), this.currentThread().getName() + "-run(): while(): stopping...");
		// We have been signaled to stop. Cause the thread to wait
		// until it has been signalled to start again.
		stopAndWait();
	    } else if (this.signal == SIGNAL_RESET) {
		LogUtil.debug(this.getClass(), this.currentThread().getName() + "-run(): while(): resetting...");
		// Reset the server socket if we can.
		serverSocket = getServerSocket();
		if (serverSocket != null) {
		    this.signal = SIGNAL_START;
		} else {
		    // Couldn't get a new server socket so go into the stop
		    // state.
		    this.signal = SIGNAL_STOP;
		}
	    }
	} // while ()...
    }

    /**
     * Close the socket server and cause the thread to wait. This method must be
     * synchronized so that it has ownership of the objects monitor.
     */
    private synchronized void stopAndWait() {
	closeServer();
	this.signal = SIGNAL_STOP;
	try {
	    this.wait();
	    this.signal = SIGNAL_START;
	} catch (InterruptedException ie) {
	    // This thread has been woken up. Go back into the start state.
	    this.exception = ie;
	    LogUtil.warn(this.getClass(), ie);
	    this.signal = SIGNAL_START;
	} catch (IllegalMonitorStateException imse) {
	    this.exception = imse;
	    LogUtil.warn(this.getClass(), imse);
	    this.signal = SIGNAL_RESET;
	}
    }

    /**
     * Wake up the thread from waiting and start processing again. This method
     * must be synchronized so that it has ownership of the objects monitor.
     */
    private synchronized void startProcessing() {
	try {
	    closeServer(); // Just in case it isn't
	    this.notify(); // Wake the thread up.
	} catch (IllegalMonitorStateException imse) {
	    LogUtil.debug(this.getClass(), imse);
	    this.exception = imse;
	    this.signal = SIGNAL_RESET;
	}
    }

    /**
     * Return a new ServerSocket. If we already have one then close it first. JH
     * 17/7/2008 added synchrnonized keyword
     */
    private synchronized ServerSocket getServerSocket() {
	try {
	    if (this.server != null) {
		LogUtil.warn(this.getClass(), "Closing server socket...");
		closeServer();
	    }
	    this.server = new ServerSocket(this.port);
	    this.exception = null;
	    LogUtil.info(this.getClass(), this.server.toString());
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), "Failed to establish SocketServer: " + ex);
	    this.exception = ex;
	    this.server = null;
	}
	return this.server;
    }

    // public void finalize()
    // {
    // LogUtil.log(this.getClass(),"finalizing server socket");
    // }

    /**
     * Signal the thread to take some action.
     */
    public void sendSignal(int sig) {
	// Process the signal depending on the state that the thread is already
	// in.
	if (sig == SIGNAL_START) {
	    LogUtil.debug(this.getClass(), this.currentThread().getName() + "-sendSignal(START)");
	    // Tell the thread to start
	    switch (this.signal) {
	    case SIGNAL_START:

		// Already running. Do nothing.
		break;
	    case SIGNAL_RESET:
		// It Shouldn't be in this state. Do the same as if it were
		// stopped.
	    case SIGNAL_STOP:

		// The thread should be waiting. Wake it up by sending the
		// thread
		// a notify signal.
		startProcessing();
	    }
	} else if (sig == SIGNAL_STOP) {
	    LogUtil.debug(this.getClass(), this.currentThread().getName() + "-sendSignal(STOP)");
	    // Tell the thread to stop
	    switch (this.signal) {
	    case SIGNAL_STOP:

		// Already stopped. Do nothing.
		break;
	    case SIGNAL_RESET:
		// It Shouldn't be in this state. Do the same as if it were
		// started.
	    case SIGNAL_START:

		// The thread should be sitting on the socket.
		// Signal it to stop and then close the socket to cause it to
		// process the stop signal.
		this.signal = SIGNAL_STOP;
		closeServer();
		break;
	    }
	} else if (sig == SIGNAL_RESET) {
	    LogUtil.debug(this.getClass(), this.currentThread().getName() + "-sendSignal(RESET)");
	    // Tell the thread to reset
	    switch (this.signal) {
	    case SIGNAL_RESET:
		// It Shouldn't be in this state. Do the same as if it were
		// started.
	    case SIGNAL_START:

		// The thread should be sitting on the socket.
		// Signal it to reset and then close the socket to cause it to
		// process the signal.
		this.signal = SIGNAL_RESET;
		closeServer();
		break;
	    case SIGNAL_STOP:

		// The thread should be waiting. Wake it up by sending the
		// thread
		// a notify signal.
		startProcessing();
		break;
	    }
	}
    }

    /**
     * Return the exception that was last raised.
     */
    public Exception getException() {
	return this.exception;
    }

    /**
     * Set the exception that was last raised.
     */
    protected void setException(Exception anException) {
	this.exception = anException;
    }

    protected String readInputStream(InputStream in) throws IOException {
	// As a sanity check limit the number of characters read so that
	// the worker does not get flooded, use up all the available server
	// memory
	// and then crash the server.
	LogUtil.debug(this.getClass(), "reading input stream: " + this.currentThread().getName() + "-processClientSocket()");
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

	// once initialised will read from memory anyway.
	int maxSize = Integer.parseInt(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.MAX_SOCKET_BYTE_SIZE));
	// this is actually character count not byte count.
	int byteCount = 0;

	// We don't expect more than about 1Kb of data to be sent.
	StringBuffer sb = new StringBuffer(1024);
	if (in != null) {
	    char ch;

	    boolean readNext = true;
	    while (readNext) {
		ch = (char) in.read();
		byteCount++;

		if (ch == (char) 4 // End of transmission (EOT) terminator
			|| ch == (char) -1) // End of file (EOF) terminator
		{
		    readNext = false;
		} else if (byteCount > maxSize) {
		    throw new IOException("Number of bytes allowed exceeded. Maximum byte size is " + maxSize + ".");
		} else {
		    sb.append(ch);
		}
	    }
	}
	LogUtil.log(this.getClass(), "Length of transmission=" + sb.length());
	return sb.toString();
    }

    protected void closeSocketResources(Socket socket, PrintStream out, InputStream in) {
	try {
	    LogUtil.debug(this.getClass(), "Closing input stream.");
	    in.close();
	    LogUtil.debug(this.getClass(), "Closing output stream.");
	    out.close();
	    LogUtil.debug(this.getClass(), "Closing socket.");
	    socket.close();
	} catch (Exception ex1) {
	    LogUtil.warn(this.getClass(), "Error closing socket. " + ex1.getMessage());
	}
    }

    /**
     * Return the port used by the SocketServer.
     */
    public int getPort() {
	return this.port;
    }

    public int getClientSocketCount() {
	return clientSocketCount;
    }

    /**
     * Set the port used by the SocketServer.
     */
    public void setPort(int aPort) {
	this.port = aPort;
	LogUtil.info(this.getClass(), "Configured for port " + this.port);
    }

    /**
     * Return the state of the thread.
     */
    public String getServerSocketState() {
	String state = null;
	switch (this.signal) {
	case SIGNAL_START:
	    state = "Running";
	    break;
	case SIGNAL_STOP:
	    state = "Stopped";
	    break;
	case SIGNAL_RESET:
	    state = "Resetting";
	    break;
	default:
	    state = "Unknown";
	    break;
	}

	return state;
    }

    private int clientSocketCount;

    public synchronized void addClientSocket() {
	clientSocketCount++;
    }

    public synchronized void subtractClientSocket() {
	clientSocketCount--;
    }

    /**
     * Close the SocketServer and ignore any errors
     */
    private void closeServer() {
	try {
	    if (this.server != null) {
		this.server.close();
	    }
	} catch (Exception e) {
	} finally {
	    this.server = null;
	}
    }

    /**
     * Make sure that the SocketServer is closed to release the port number
     * before garbage collecting this class instance.
     */
    // protected void finalize()
    // throws Throwable
    // {
    // closeServer();
    // }

    public abstract void processClientSocket(Socket socket);

    /********************** CachedItem interface methods ********************/

    /**
     * The key for the cache is the class name as we only want one
     * IntegrationSocketServer in the cache.
     */
    public String getKey() {
	return this.getClass().getName();
    }

    public boolean keyEquals(String key) {
	if (getKey().equals(key)) {
	    return true;
	} else {
	    return false;
	}
    }

}

/**
 * Worker class to do the actual processing of the client socket in another
 * thread. The worker does a call back to a method on the manager class which is
 * implemented by a sub class to do the actual work of processing the socket.
 * 
 * @author T. Bakker
 * @created 31 March 2003
 */
class SocketWorker extends Thread {
    private Socket socket = null;

    private ServerSocketManager manager = null;

    public SocketWorker(ServerSocketManager myManager, Socket mySocket) {
	this.manager = myManager;
	this.socket = mySocket;
    }

    /**
     * This method is invoked when running in a thread.
     */
    public void run() {
	LogUtil.debug(this.getClass(), this.currentThread().getName() + "-run(1)...");

	this.manager.processClientSocket(this.socket);
	LogUtil.debug(this.getClass(), this.currentThread().getName() + "-run(2)...");
    }
}
