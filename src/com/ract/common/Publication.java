package com.ract.common;

import java.io.Serializable;

/**
 * @hibernate.class table="gn_publication" lazy="false"
 */
public class Publication implements Serializable {
    /**
     * @hibernate.property
     * @return
     */
    public boolean isMembershipDependent() {
	return membershipDependent;
    }

    public static String RACT_NEWSLETTER = "General";

    public static String ROADSIDE_EMAIL_RENEWAL = "Roadside Email Renewal";

    public void setMembershipDependent(boolean membershipDependent) {
	this.membershipDependent = membershipDependent;
    }

    private String publicationCode;

    private String description;

    private boolean customerDefault;

    public void setDescription(String description) {
	this.description = description;
    }

    public void setPublicationCode(String publicationCode) {
	this.publicationCode = publicationCode;
    }

    public void setCustomerDefault(boolean customerDefault) {
	this.customerDefault = customerDefault;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="description"
     */
    public String getDescription() {
	return description;
    }

    /**
     * @hibernate.id column="publication_code" generator-class="assigned"
     */
    public String getPublicationCode() {
	return publicationCode;
    }

    /**
     * Does publication require membership
     */
    private boolean membershipDependent;

    /**
     * System managed
     */
    private boolean systemManaged;

    /**
     * @hibernate.property
     * @hibernate.column name="customer_default"
     */
    public boolean isCustomerDefault() {
	return customerDefault;
    }

    /**
     * @hibernate.property
     * @return
     */
    public boolean isSystemManaged() {
	return systemManaged;
    }

    public void setSystemManaged(boolean systemManaged) {
	this.systemManaged = systemManaged;
    }

    public Publication() {
    }
}
