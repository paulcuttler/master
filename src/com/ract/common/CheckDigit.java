package com.ract.common;

/**
 * <pre>
 * Construct a phone payment number as follows
 *  	ccccccssssd
 * 
 * 	where 	cccccc 	is a client number
 * 		ssss	is an OI-Payable sequence number
 * 	and	d	is the check digit calculated using the
 * 			double-add-double (IBM) check
 * 
 *  IBM Check
 * 
 *  The "IBM check", which is used by MasterCard, VISA, and most other
 *  credit card companies is an even/odd weighted code.
 * 
 *  The digits in the even positions (numbering from the right) are multiplied by 2,
 *  then reduced to a single digit (if > 9) by "casting out nines" (subtracting 9,
 *  which is equivalent to adding the digits .
 * 
 *  All digits are then summed and a check digit added to make the result evenly divisible by 10.
 * 
 *  For example, given the number
 * 
 *      6   1   8   2        0   9   2   3        1   5   5   3
 * 
 *  the leading 6 is doubled, giving 12, which is then reduced to 3 by adding the digits of 12
 *  together; similarly, the 8 becomes 16 and then 7; the 0 is impervious to doubling; the 2
 *  becomes 4; the 1 becomes 2; and the 5 in the second-last position becomes 10 and thus 1.
 * 
 *  Thus the check equation is
 * 
 *    6#2 + 1 + 8#2 + 2 + 0#2 + 9 + 2#2 + 3 + 1#2 + 5 + 5#2 + 3 mod 10 = 0
 * 
 *  where '#' represents multiplication with casting out nines, giving
 * 
 *      3 + 1 + 7 + 2 + 0 + 9 + 4+ 3 + 2 + 5 + 1 + 3 mod 10 = 40 mod 10 = 0
 * 
 *  This scheme catches all single errors and most adjacent transpositions,
 *  but not jump transpositions (such as 553 becoming 355) or 09 becoming 90.
 * </pre>
 * 
 * @author Leigh Giles
 * @version 1.0, 1/2/2002 Modified 9/12/2002 TB. Change to a static class method
 */

public class CheckDigit {

	/**
	 * Standard check digit implementation. This implementation is incorrect as it counts from the right but starts at the length rather than 1.
	 * 
	 * @param sNum String
	 * @return Integer
	 */
	public static Integer getExternalCheckDigit(String sNum) {
		Integer checkDigit;
		if (sNum != null) {
			int cd = 0;
			int i = sNum.length() - 1; // 0 based substring
			while (i >= 0) {
				// get check digit
				cd += sumDigits(sNum.substring(i, i + 1), i);
				i--;
			}
			int remainder = (10 - (cd % 10));
			if (remainder > 9) {
				remainder = 0;
			}
			checkDigit = new Integer(remainder);
		} else {
			checkDigit = new Integer(0);
		}
		return checkDigit;
	}

	/**
	 * Correct IBM check implementation. It will return the same result as getInternalCheckDigit for 10 digit numbers but card numbers will now change to the correct check digit. Had same bug as getExternalCheckDigit but for 10 digit number it worked.
	 * #### Used only for IVR and PhonePay numbers ####
	 * 
	 * @param sNum String
	 * @return Integer
	 */
	public static Integer getCheckDigit(String referenceNumber, boolean useZero) {
		referenceNumber = referenceNumber.concat("0"); // assumes not already added
		
		Integer checkDigit;
		if (referenceNumber != null) {
			int cd = 0;
			StringBuffer reverseReferenceNumber = new StringBuffer(referenceNumber);
			referenceNumber = reverseReferenceNumber.reverse().toString();
			for (int i = 0; i < referenceNumber.length(); i++) {
				cd += sumDigits(referenceNumber.substring(i, i + 1), (i + 1));
			}

			int remainder = (10 - (cd % 10));
			if (remainder > 9) {
				remainder = 0;
			}
			checkDigit = new Integer(remainder);
		} else {
			checkDigit = new Integer(0);
		}
		return checkDigit;
	}

	/**
	 * Calculate the multiplier for this digit 1 - for odd positioned digits 2 - for even positioned digits
	 * 
	 * @param digit String
	 * @param i int
	 * @return int
	 */
	private static int sumDigits(String digit, int position) {
		int digitSum = 0;
		String stringDigit = "";
		int multiplier = 0;
		// position++;
		// System.out.println("pos="+position);
		int sum = new Integer(digit).intValue();
		if (position % 2 == 0) {
			multiplier = 2;
		} else {
			multiplier = 1;
		}
		sum *= multiplier;
		stringDigit = String.valueOf(sum);
		if (sum < 10) {
			digitSum = Integer.parseInt(stringDigit);
		} else {
			for (int x = 0; x < stringDigit.length(); x++) {
				digitSum += Integer.parseInt(stringDigit.substring(x, x + 1));
			}
		}
		// System.out.println(position+"          "+digit+" * "+multiplier+" = "+stringDigit+", digits added together = "+digitSum);
		return digitSum;
	}

	public static boolean validateCardNumber(String cardNumber) {
		boolean valid = false;
		int cardNumberLength = cardNumber.length();
		Integer checkDigit = new Integer(cardNumber.substring(cardNumberLength - 1, cardNumberLength));
		String rawNumber = cardNumber.substring(0, cardNumberLength - 1);
		Integer calculatedCheckDigit = getCheckDigit(rawNumber, false);
		// System.out.println("calculatedCheckDigit="+calculatedCheckDigit);
		if (checkDigit.equals(calculatedCheckDigit)) {
			valid = true;
		}
		return valid;
	}

	/**
	 * Allow backward compatibility.
	 */

	/**
	 * Calculate the multiplier for this digit 1 - for odd positioned digits 2 - for even positioned digits note: an error with this code results in non-standard check digits eg. if n = 4 n = 4 * (1 + (1 * (6 % 2))) n = 4 * (1 + (0)) //even should be 2 n = 5
	 */
	// private static int calcAdd(String sN, int i)
	// {
	//
	// int n = new Integer(sN).intValue();
	// n = n * (1 + (1 * (i % 2)));
	// if(n > 9)
	// {
	// n = n - 9;
	// }
	// return n;
	// }

	/**
	 * Generate an internal check digit. This check digit is not using the correct digit to character mapping as defined in the class description.
	 * 
	 * @param sNum String
	 * @return Integer
	 */
	// public static Integer getInternalCheckDigit(String sNum)
	// {
	// Integer checkDigit;
	// if(sNum != null)
	// {
	// int cd = 0;
	// int i = sNum.length() - 1;
	//
	// while(i >= 0)
	// {
	// cd = cd + calcAdd(sNum.substring(i, i + 1), i);
	// i--;
	// }
	// checkDigit = new Integer(10 - (cd % 10));
	//
	// if((checkDigit.compareTo(new Integer(10))) == 0)
	// {
	// checkDigit = new Integer(0);
	// }
	// }
	// else
	// {
	// checkDigit = new Integer(0);
	// }
	//
	// return checkDigit;
	// }
	/**
	 * Validate the reference number. Use both check digit calculations
	 * 
	 * @param referenceNumber String
	 * @return boolean
	 */

	public static boolean validateReferenceNumber(String referenceNumber) {
		boolean result;

		result = false;
		String cd = referenceNumber.substring(referenceNumber.length() - 1, referenceNumber.length());
		String rn = referenceNumber.substring(0, referenceNumber.length() - 1);

		Integer cdn = new Integer(cd);
		Integer cdi = getCheckDigit(rn, false);
		Integer cde = getExternalCheckDigit(rn); // to be phased out

		if (cdn.equals(cdi) || cdn.equals(cde)) {
			result = true;
		}

		return result;

	}
}
