package com.ract.common;

/**
 * Item object that stores one string. Could become more complex later on.
 * 
 * @todo add more complex structure to allow caching of more complex data. It is
 *       a simple string element at this stage.
 * 
 * @author John Holliday
 * @version 1.0, 18/4/2002
 */

public class Item implements CachedItem {

    public Item(String item) {
	this.itemName = item;
    }

    private String itemName;

    public String getItemName() {
	return itemName;
    }

    public void setItemName(String itemName) {
	this.itemName = itemName;
    }

    public String getKey() {
	return this.getItemName();
    }

    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    public String toString() {
	return this.getItemName();
    }

}
