package com.ract.common;

/**
 * A specific type of ValidationException relating to a Direct Debit schedule
 * that has been dishonoured twice and marked as "Failed".
 * 
 * @author newtong
 * 
 */
public class DirectDebitFailedValidationException extends ValidationException {
    /**
     * Constructor for the DirectDebitFailedValidationException object
     */
    public DirectDebitFailedValidationException() {
	super("Direct Debit Failed Validation Error: undefined.");
    }

    /**
     * Constructor for the DirectDebitFailedValidationException object
     * 
     * @param message
     *            Description of the Parameter
     */
    public DirectDebitFailedValidationException(String message) {
	super(message);
    }

    public DirectDebitFailedValidationException(Throwable t) {
	super("", t);
    }

    /**
     * Constructor for the DirectDebitFailedValidationException object
     * 
     * @param message
     *            Description of the Parameter
     * @param e
     *            Description of the Parameter
     */
    public DirectDebitFailedValidationException(String message, Throwable t) {
	super(message, t);
    }
}
