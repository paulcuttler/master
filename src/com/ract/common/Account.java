package com.ract.common;

import java.rmi.RemoteException;

/**
 * Holds details about an account in the finance system that the payment system
 * needs to know.
 * 
 */
public interface Account
// implements Serializable
{

    public String getAccountNumber();

    public String getCostCenter();

    public String getSubAccountNumber();

    public boolean isClearingAccount();

    public boolean isGstAccount();

    public boolean isSuspenseAccount();

    public boolean isTaxable();

    public void setClearingAccount(boolean clearingAccount) throws RemoteException;

    public void setSuspenseAccount(boolean suspenseAccount) throws RemoteException;

    public void setGstAccount(boolean gstAccount) throws RemoteException;

    public void setTaxable(boolean taxable) throws RemoteException;

}
