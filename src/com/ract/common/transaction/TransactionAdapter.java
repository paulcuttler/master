package com.ract.common.transaction;

import com.ract.common.SystemException;

/**
 * TransactionAdapter super interface for commit, rollback and release
 * 
 * @author jyh
 */
public interface TransactionAdapter {
    // core adapters
    public static final String ADAPTER_FINANCE = "Finance Adapter";
    public static final String ADAPTER_RECEIPTING = "Receipting Adapter";
    public static final String ADAPTER_DIRECT_DEBIT = "Direct Debit Adapter";
    public static final String ADAPTER_FLEET = "Fleet Adapter";
    public static final String ADAPTER_VI = "VI Adapter";
    public static final String ADAPTER_INSURANCE = "Insurance Adapter";
    public static final String ADAPTER_CLIENT = "Client Adapter";

    public void commit() throws SystemException;

    public void rollback() throws SystemException;

    public void release() throws SystemException;

    /**
     * Get specific system name
     * 
     * @return String
     */
    public String getSystemName();

    public boolean getRollbackOnly();

    public void setRollbackOnly(Exception exception);

    public Exception getRollbackException();
}
