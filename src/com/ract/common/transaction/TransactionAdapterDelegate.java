package com.ract.common.transaction;

/**
 * <p>
 * Implement transaction adapter with rollback flag.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class TransactionAdapterDelegate {

    public boolean getRollbackOnly() {
	return this.rollbackOnly;
    }

    private Exception rollbackException;

    private boolean rollbackOnly;

    public void setRollbackOnly(Exception exception) {
	this.rollbackException = exception;
	this.rollbackOnly = true;
    }

    public Exception getRollbackException() {
	return this.rollbackException;
    }

}
