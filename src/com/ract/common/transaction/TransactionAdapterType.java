package com.ract.common.transaction;

/**
 * <p>
 * Abstract transaction adapter.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public abstract class TransactionAdapterType implements TransactionAdapter {

    private TransactionAdapterDelegate transactionAdapterDelegate = new TransactionAdapterDelegate();

    public void setRollbackOnly(Exception ex) {
	transactionAdapterDelegate.setRollbackOnly(ex);
    }

    public boolean getRollbackOnly() {
	return transactionAdapterDelegate.getRollbackOnly();
    }

    public Exception getRollbackException() {
	return transactionAdapterDelegate.getRollbackException();
    }

}
