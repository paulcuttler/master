package com.ract.common.transaction;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.ract.common.SystemException;
import com.ract.fleet.FleetAdapter;
import com.ract.insurance.InsuranceAdapter;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.vehicleinspection.VehicleInspectionAdapter;

/**
 * </p>Maintain a list of transaction adapters</p>
 * 
 * @todo move receipting/direct debit functionality from payment transaction mgr
 *       to this class.
 * 
 * @author jyh
 * @version 1.0
 */
public class TransactionAdapterList extends ArrayList {
    public TransactionAdapterList() {
	super();
    }

    public void addTransactionAdapter(TransactionAdapter transactionAdapter) throws SystemException {
	boolean added = this.add(transactionAdapter);
	// failed to add
	if (!added) {
	    LogUtil.debug(this.getClass(), "failed to add");
	    if (isRollbackOnly()) {
		LogUtil.debug(this.getClass(), "is Rollback only");
		// rollback this adapter
		transactionAdapter.rollback();
		// if already marked for rollback then get reason
		Exception rollbackException = getFirstRollbackException();
		throw new SystemException(rollbackException.getMessage());
	    }
	    LogUtil.debug(this.getClass(), "else already exists in transaction adapter list. forces the type.");
	}
	LogUtil.debug(this.getClass(), "added successfully");
    }

    public boolean add(Object o) {
	// if a transaction adapter instance and not already in the list and not
	// marked for rollback
	// then add it.
	if (o instanceof TransactionAdapter && !this.contains(o)) {
	    LogUtil.debug(this.getClass(), "Adding object. " + ((TransactionAdapter) o).getSystemName());
	    super.add(o);
	    return true;
	} else {
	    LogUtil.debug(this.getClass(), "Unable to add object as already exists or of the wrong type. " + this.size() + " " + o.getClass());
	    return false;
	}
    }

    public TransactionAdapter getTransactionAdapter(String adapterType) {
	TransactionAdapter transactionAdapter = null;
	LogUtil.log(this.getClass(), "getTransactionAdapter adapterType=" + adapterType);
	if (this != null) {
	    TransactionAdapter tmpTransactionAdapter = null;
	    LogUtil.log(this.getClass(), "TransactionAdapterList size=" + this.size());
	    for (int x = 0; x < this.size(); x++) {
		tmpTransactionAdapter = (TransactionAdapter) this.get(x);
		LogUtil.log(this.getClass(), "adapter class" + tmpTransactionAdapter.getSystemName());
		LogUtil.log(this.getClass(), "adapter class" + tmpTransactionAdapter.getClass().getName());
		if (adapterType.equals(TransactionAdapter.ADAPTER_FLEET) && tmpTransactionAdapter instanceof FleetAdapter) {
		    transactionAdapter = tmpTransactionAdapter;
		} else if (adapterType.equals(TransactionAdapter.ADAPTER_INSURANCE) && tmpTransactionAdapter instanceof InsuranceAdapter) {
		    transactionAdapter = tmpTransactionAdapter;
		} else if (adapterType.equals(TransactionAdapter.ADAPTER_VI) && tmpTransactionAdapter instanceof VehicleInspectionAdapter) {
		    transactionAdapter = tmpTransactionAdapter;
		} else {
		    LogUtil.log(this.getClass(), "unknown type." + adapterType + " " + TransactionAdapter.ADAPTER_VI + (tmpTransactionAdapter instanceof VehicleInspectionAdapter));
		}
		// other adapter types
	    }
	}
	LogUtil.log(this.getClass(), "transactionAdapter null?" + (transactionAdapter == null));
	return transactionAdapter;
    }

    public String toString() {
	StringBuffer description = new StringBuffer();
	if (this != null) {
	    TransactionAdapter transactionAdapter = null;
	    for (int i = 0; i < this.size(); i++) {
		transactionAdapter = (TransactionAdapter) this.get(i);
		if (transactionAdapter != null) {
		    description.append(FileUtil.NEW_LINE + (i + 1) + ". " + transactionAdapter.getSystemName() + " " + transactionAdapter.getRollbackOnly() + " " + transactionAdapter.getRollbackException());
		}
	    }
	}
	return description.toString();
    }

    /**
     * Rollback all adapters.
     * 
     * @throws RemoteException
     */
    public void rollbackTransactionAdapterList() throws RemoteException {
	if (this != null) {
	    LogUtil.debug(this.getClass(), "Adapters to rollback: " + this.toString());
	    TransactionAdapter transactionAdapter = null;
	    for (int i = 0; i < this.size(); i++) {
		transactionAdapter = (TransactionAdapter) this.get(i);
		transactionAdapter.rollback();
	    }
	}
    }

    public Exception getFirstRollbackException() {
	Exception rollbackException = null;
	if (this != null) {
	    LogUtil.debug(this.getClass(), "Adapters to locate rollback exception: " + this.toString());
	    TransactionAdapter transactionAdapter = null;
	    for (int i = 0; i < this.size() && (rollbackException == null); i++) {
		transactionAdapter = (TransactionAdapter) this.get(i);
		rollbackException = transactionAdapter.getRollbackException();
	    }
	}
	return rollbackException;
    }

    /**
     * Commit all adapters.
     * 
     * @throws RemoteException
     */
    public void commitTransactionAdapterList() throws RemoteException {
	if (this != null) {
	    LogUtil.debug(this.getClass(), "Adapters to commit: " + this.toString());
	    TransactionAdapter transactionAdapter = null;
	    for (int i = 0; i < this.size(); i++) {
		transactionAdapter = (TransactionAdapter) this.get(i);
		transactionAdapter.commit();
	    }
	}
    }

    /**
     * Are any transaction adapters in the list marked to rollback only?
     * 
     * @return boolean
     */
    public boolean isRollbackOnly() {
	boolean rollbackOnly = false;
	if (this != null) {
	    TransactionAdapter transactionAdapter = null;
	    for (int i = 0; i < this.size() && !rollbackOnly; i++) {
		transactionAdapter = (TransactionAdapter) this.get(i);
		// is the transaction adapter marked for rollback only.
		rollbackOnly = transactionAdapter.getRollbackOnly();
	    }
	}
	return rollbackOnly;
    }

    /**
     * Release all adapters.
     * 
     * @throws RemoteException
     */
    public void releaseTransactionAdapterList() throws RemoteException {
	if (this != null) {
	    LogUtil.debug(this.getClass(), "Adapters to release: " + this.toString());
	    TransactionAdapter transactionAdapter = null;
	    for (int i = 0; i < this.size(); i++) {
		transactionAdapter = (TransactionAdapter) this.get(i);
		transactionAdapter.release();
	    }
	}
    }

}
