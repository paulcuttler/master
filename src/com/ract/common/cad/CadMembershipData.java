package com.ract.common.cad;

import java.rmi.RemoteException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemParameterVO;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

/**
 * <p>
 * cad membership
 * </p>
 * 
 * @author rlg
 * @version 1.0
 */

public class CadMembershipData {

    private String membershipCancelled;
    private String membershipFleet;
    private String membershipVehicleMake;
    private String membershipVehicleModel;
    private String membershipVehicleReg;
    private String membershipVehicleVin;
    private String membershipVehicleYear;
    private String membershipExpiry;
    private String membershipJoined;
    private String membershipLevel;
    private String membershipNumber;
    private String membershipPlusExpiry;
    private String membershipPlusJoined;
    private String membershipType;
    private String membershipYears;
    private String membershipXML;

    private CadCommonMgrLocal ccm = null;

    public CadMembershipData() {

    }

    public CadMembershipData(String XML) {
	this.membershipXML = XML;
    }

    public CadMembershipData(MembershipVO memVO) {
	try {
	    LogUtil.log(this.getClass(), "RLG ++++++ " + memVO.getClientNumber() + " - " + memVO.getStatus() + " " + memVO.isActive());
	} catch (RemoteException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	if (memVO.getMembershipTypeCode().equalsIgnoreCase(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL)) {
	    try {

		setPersonalMembershipAttributes(memVO.getExpiryDate(), memVO.getJoinDate(), memVO.getCurrentProductCode(), memVO.getMembershipNumber(), memVO.getMembershipYears().getTotalYears(), memVO.getBaseStatus());

		if (memVO.getProduct().isVehicleBased()) {
		    this.membershipVehicleMake = memVO.getMake();
		    this.membershipVehicleModel = memVO.getModel();
		    this.membershipVehicleReg = memVO.getRego();
		    this.membershipVehicleVin = memVO.getVin();
		    this.membershipVehicleYear = memVO.getYear();

		    // override level with profile code
		    if (memVO.getProductCode().equals(ProductVO.PRODUCT_CMO)) {
			this.membershipLevel = memVO.getMembershipProfileCode();
		    }
		}
	    } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    public CadMembershipData(DateTime membershipExpiry, DateTime membershipJoined, String membershipLevel, Integer membershipNumber, DateTime membershipPlusExpiry, DateTime membershipPlusJoined) {

    }

    /**
     * @param membershipCancelled
     * @param membershipFleet
     * @param membershipVehicleMake
     * @param membershipVehicleModel
     * @param membershipVehicleReg
     * @param membershipVehicleYear
     * @param membershipExpiry
     * @param membershipJoined
     * @param membershipLevel
     * @param membershipNumber
     * @param membershipPlusExpiry
     * @param membershipPlusJoined
     * @param membershipType
     * @param membershipYears
     */
    public CadMembershipData(String membershipCancelled, String membershipFleet, String membershipVehicleMake, String membershipVehicleModel, String membershipVehicleReg, String membershipVehicleYear, String membershipExpiry, String membershipJoined, String membershipLevel, String membershipNumber, String membershipPlusExpiry, String membershipPlusJoined, String membershipType, String membershipYears) {
	this.membershipCancelled = membershipCancelled;
	this.membershipFleet = membershipFleet;
	this.membershipVehicleMake = membershipVehicleMake;
	this.membershipVehicleModel = membershipVehicleModel;
	this.membershipVehicleReg = membershipVehicleReg;
	this.membershipVehicleYear = membershipVehicleYear;
	this.membershipExpiry = membershipExpiry;
	this.membershipJoined = membershipJoined;
	this.membershipLevel = membershipLevel;
	this.membershipNumber = membershipNumber;
	this.membershipPlusExpiry = membershipPlusExpiry;
	this.membershipPlusJoined = membershipPlusJoined;
	this.membershipType = membershipType;
	this.membershipYears = membershipYears;
    }

    private void setPersonalMembershipAttributes(DateTime membershipExpiry, DateTime membershipJoined, String membershipLevel, String membershipNumber, int membershipYears, String membershipStatus) {
	this.setAttributesToNull();

	this.setMembershipNumber(membershipNumber);
	this.setMembershipExpiry(membershipExpiry);
	this.setMembershipJoined(membershipJoined);
	this.setMembershipLevel(getMembershipLevelAbbreviation(membershipLevel));
	this.setMembershipType(getMembershipTypeAbbreviation(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL));
	this.setMembershipYears(new Integer(membershipYears).toString());
	this.setMembershipCancelled(calcCancelled(membershipExpiry, membershipStatus));

    }

    public void setAttributesToNull() {
	this.membershipCancelled = null;
	this.membershipExpiry = null;
	this.membershipFleet = null;
	this.membershipJoined = null;
	this.membershipLevel = null;
	this.membershipNumber = null;
	this.membershipPlusExpiry = null;
	this.membershipPlusJoined = null;
	this.membershipType = null;
	this.membershipVehicleMake = null;
	this.membershipVehicleModel = null;
	this.membershipVehicleReg = null;
	this.membershipVehicleVin = null;
	this.membershipVehicleYear = null;
	this.membershipYears = null;
	this.membershipXML = null;

    }

    public String getMembershipCancelled() {
	return this.membershipCancelled;
    }

    public void setMembershipCancelled(String membershipCancelled) {
	this.membershipCancelled = membershipCancelled;
    }

    public String getMembershipFleet() {
	return this.membershipFleet;
    }

    public void setMembershipFleet(String membershipFleet) {
	this.membershipFleet = membershipFleet;
    }

    public String getMembershipVehicleMake() {

	return this.membershipVehicleMake;
    }

    public void setMembershipVehicleMake(String membershipVehicleMake) {
	this.membershipVehicleMake = membershipVehicleMake;
    }

    public String getMembershipVehicleModel() {
	return this.membershipVehicleModel;
    }

    public void setMembershipVehicleModel(String membershipVehicleModel) {
	this.membershipVehicleModel = membershipVehicleModel;
    }

    public String getMembershipVehicleReg() {
	return this.membershipVehicleReg;
    }

    public void setMembershipVehicleReg(String membershipVehicleReg) {
	this.membershipVehicleReg = membershipVehicleReg;
    }

    public String getMembershipVehicleYear() {
	return this.membershipVehicleYear;
    }

    public void setMembershipVehicleYear(String membershipVehicleYear) {
	this.membershipVehicleYear = membershipVehicleYear;
    }

    public String getMembershipExpiry() {
	return this.membershipExpiry;
    }

    public void setMembershipExpiry(String membershipExpiry) {

	try {
	    this.setMembershipExpiry(new DateTime(membershipExpiry));
	} catch (Exception e) {
	    // do nothing
	}
    }

    public void setMembershipExpiry(DateTime membershipExpiry) {
	this.membershipExpiry = DateUtil.formatShortDateTime(membershipExpiry);
    }

    public String getMembershipJoined() {
	return this.membershipJoined;
    }

    public void setMembershipJoined(String membershipJoined) {
	try {
	    setMembershipJoined(new DateTime(membershipJoined));
	} catch (Exception e) {
	    // Do Nothing
	}
    }

    public void setMembershipJoined(DateTime membershipJoined) {
	this.membershipJoined = DateUtil.formatShortDateTime(membershipJoined);
    }

    public String getMembershipLevel() {
	return this.membershipLevel;
    }

    public void setMembershipLevel(String membershipLevel) {
	this.membershipLevel = membershipLevel;
    }

    public String getMembershipNumber() {
	return this.membershipNumber;
    }

    public void setMembershipNumber(String membershipNumber) {
	this.membershipNumber = membershipNumber;
    }

    public String getMembershipPlusExpiry() {
	return this.membershipPlusExpiry;
    }

    public void setMembershipPlusExpiry(String membershipPlusExpiry) {
	try {
	    setMembershipPlusExpiry(new DateTime(membershipPlusExpiry));
	} catch (Exception e) {
	    // Do Nothing

	}
    }

    public void setMembershipPlusExpiry(DateTime membershipPlusExpiry) {
	this.membershipPlusExpiry = DateUtil.formatShortDateTime(membershipPlusExpiry);
    }

    public String getMembershipPlusJoined() {
	return this.membershipPlusJoined;
    }

    public void setMembershipPlusJoined(String membershipPlusJoined) {
	try {
	    setMembershipPlusJoined(new DateTime(membershipPlusJoined));
	} catch (Exception e) {
	    // Do Nothing
	}
    }

    public void setMembershipPlusJoined(DateTime membershipPlusJoined) {
	this.membershipPlusJoined = DateUtil.formatShortDateTime(membershipPlusJoined);
    }

    public String getMembershipType() {
	return this.membershipType;
    }

    public void setMembershipType(String membershipType) {
	this.membershipType = membershipType;
    }

    public String getMembershipYears() {
	return this.membershipYears;
    }

    public void setMembershipYears(String membershipYears) {
	this.membershipYears = membershipYears;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("CadMembershipData [membershipCancelled=");
	builder.append(membershipCancelled);
	builder.append(", membershipExpiry=");
	builder.append(membershipExpiry);
	builder.append(", membershipFleet=");
	builder.append(membershipFleet);
	builder.append(", membershipJoined=");
	builder.append(membershipJoined);
	builder.append(", membershipLevel=");
	builder.append(membershipLevel);
	builder.append(", membershipNumber=");
	builder.append(membershipNumber);
	builder.append(", membershipPlusExpiry=");
	builder.append(membershipPlusExpiry);
	builder.append(", membershipPlusJoined=");
	builder.append(membershipPlusJoined);
	builder.append(", membershipType=");
	builder.append(membershipType);
	builder.append(", membershipVehicleMake=");
	builder.append(membershipVehicleMake);
	builder.append(", membershipVehicleModel=");
	builder.append(membershipVehicleModel);
	builder.append(", membershipVehicleReg=");
	builder.append(membershipVehicleReg);
	builder.append(", membershipVehicleVin=");
	builder.append(membershipVehicleVin);
	builder.append(", membershipVehicleYear=");
	builder.append(membershipVehicleYear);
	builder.append(", membershipYears=");
	builder.append(membershipYears);
	builder.append("]");
	return builder.toString();
    }

    private String createXML() {
	StringBuilder builder = new StringBuilder();

	builder.append("<membership>");

	builder.append(XMLHelper.optionalXMLElement("membershipNumber", this.getMembershipNumber(), 12));
	builder.append(XMLHelper.optionalXMLElement("membershipType", this.getMembershipType(), 1));
	builder.append(XMLHelper.optionalXMLElement("membershipLevel", this.getMembershipLevel(), 4));
	builder.append(XMLHelper.optionalXMLElement("membershipJoined", this.getMembershipJoined(), 15));
	builder.append(XMLHelper.optionalXMLElement("membershipExpiry", this.getMembershipExpiry(), 15));
	builder.append(XMLHelper.optionalXMLElement("membershipPlusJoined", this.getMembershipPlusJoined(), 15));
	builder.append(XMLHelper.optionalXMLElement("membershipPlusExpiry", this.getMembershipPlusExpiry(), 15));
	builder.append(XMLHelper.optionalXMLElement("membershipYears", this.getMembershipYears(), 4));
	builder.append(XMLHelper.optionalXMLElement("membershipVehicleReg", this.getMembershipVehicleReg(), 10));
	builder.append(XMLHelper.optionalXMLElement("membershipVehicleVin", this.getMembershipVehicleVin(), 20));
	builder.append(XMLHelper.optionalXMLElement("membershipVehicleMake", this.getMembershipVehicleMake(), 20));
	builder.append(XMLHelper.optionalXMLElement("membershipVehicleModel", this.getMembershipVehicleModel(), 20));
	builder.append(XMLHelper.optionalXMLElement("membershipVehicleYear", this.getMembershipVehicleYear(), 4));
	builder.append(XMLHelper.optionalXMLElement("membershipFleet", this.getMembershipFleet(), 10));
	builder.append(XMLHelper.optionalXMLElement("membershipCancelled", this.getMembershipCancelled(), 1));

	builder.append("</membership>");

	return builder.toString();
    }

    public String getXML() {
	if (this.membershipXML == null) {
	    this.membershipXML = this.createXML();
	}

	return membershipXML;
    }

    private String getMembershipLevelAbbreviation(String memLevel) {
	String level = getCadCommonMgr().getParameter(SystemParameterVO.CATEGORY_CAD, SystemParameterVO.CAD_MEMLEVEL_ABBREV + memLevel.trim());
	if (level.equalsIgnoreCase("")) {
	    if (memLevel.length() > 3) {
		level = memLevel.substring(0, 4).toUpperCase();
	    } else {
		level = memLevel.toUpperCase();
	    }
	}

	return level;

    }

    private String getMembershipTypeAbbreviation(String memType) {
	String level = getCadCommonMgr().getParameter(SystemParameterVO.CATEGORY_CAD, SystemParameterVO.CAD_MEMTYPE_ABBREV + memType.trim());
	if (level.equalsIgnoreCase("")) {
	    level = memType.substring(0, 1).toUpperCase();
	}

	return level;

    }

    private String calcCancelled(DateTime membershipExpiry, String membershipStatus) {
	if (membershipExpiry.afterDay(CadHelper.getCadDeleteCutOffDate()) && isMembershipActive(membershipStatus)) {
	    return "N";
	} else {
	    return "Y";
	}
    }

    private boolean isMembershipActive(String memStatus) {
	if (memStatus.equalsIgnoreCase(MembershipVO.STATUS_ACTIVE) || memStatus.equalsIgnoreCase(MembershipVO.STATUS_ONHOLD)) {
	    return true;
	}

	return false;
    }

    private CadCommonMgrLocal getCadCommonMgr() {
	if (ccm == null) {
	    ccm = CommonEJBHelper.getCadCommonMgrLocal();

	}

	return ccm;
    }

    public String getMembershipVehicleVin() {
	return membershipVehicleVin;
    }

    public void setMembershipVehicleVin(String membershipVehicleVin) {
	this.membershipVehicleVin = membershipVehicleVin;
    }
}
