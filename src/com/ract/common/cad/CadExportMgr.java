package com.ract.common.cad;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.ejb.Remote;

import com.ract.common.ValidationException;
import com.ract.membership.MembershipVO;
import com.ract.util.DateTime;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */
@Remote
public interface CadExportMgr {

    /**
     * Send membership changes to cad-export table
     */
    public void notifyMembershipChangeToCAD(MembershipVO membershipVO, DateTime effectiveDate) throws RemoteException, ValidationException;

    public void createCADRecord(CadExportVO vo) throws RemoteException, ValidationException;

    /**
     * Look for membership records which have exceeded their service date
     * (Currently expiry date + 3 months), and create cad-export records to pass
     * this data to the CARES system.
     * 
     * @param DateTime
     *            date The date for which the extract is to be done
     */
    public void sendClientsToCAD(String sql) throws RemoteException;

    public void sendExpiredMembershipsToCAD(DateTime date) throws RemoteException;

    public void sendAddressChangesToCAD(DateTime date) throws RemoteException;

    public void refreshFromRoadside() throws RemoteException, ValidationException;

    public void nightlyRefreshMembership() throws RemoteException;

    /**
     * Remove superseded records from the cad-export table
     * 
     * @param DateTime
     *            asAt Records which are older than this date and have been
     *            processed are to be removed
     */
    public void removeOldCADs(DateTime asAt) throws RemoteException;

    /**
     * Return a list of CadExportControlVO representing the data from the
     * cad-export-ctrl table in the Progress CAD export system.
     */
    public ArrayList getCadExportControlList() throws RemoteException;

    /**
     * Return a list of CadExportVO representing the data from the cad-export
     * table in the Progress CAD export system.
     */
    public ArrayList getCadExportList(Integer clientNumber) throws RemoteException;

    public RemoteException getRefreshMembershipException() throws RemoteException;

    public int getRefreshMembershipRecordCount() throws RemoteException;

    public String getRefreshMembershipStatus() throws RemoteException;

    public void cancelMembershipRefresh() throws RemoteException;

    public void compareRAAwithRACT(String action) throws RemoteException;

    /**
     * Change the status of the worker to IDLE.
     */
    public void resetWorker() throws RemoteException;

}
