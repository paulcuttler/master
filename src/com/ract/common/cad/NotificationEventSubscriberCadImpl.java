package com.ract.common.cad;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationEventSubscriber;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipVO;
import com.ract.membership.notifier.MembershipChangeNotificationEvent;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class NotificationEventSubscriberCadImpl implements NotificationEventSubscriber, Serializable {

    public NotificationEventSubscriberCadImpl() {
    }

    /**
     * Handle the notification of a change to a membership. Whatever the change
     * is, send the membership details to CARES.
     */
    public void processNotificationEvent(NotificationEvent event) throws RemoteException {
	if (event != null && event.eventEquals(NotificationEvent.EVENT_MEMBERSHIP_CHANGE)) {
	    MembershipChangeNotificationEvent memChangeEvent = (MembershipChangeNotificationEvent) event;
	    MembershipVO memVO = memChangeEvent.getMembershipVO();

	    // If no membership VO supplied then look it ip.
	    if (memVO == null) {
		LogUtil.debug(this.getClass(), "Fetching membership ID " + memChangeEvent.getMembershipID());
		memVO = MembershipEJBHelper.getMembershipMgr().getMembership(memChangeEvent.getMembershipID());
	    }

	    if (memVO == null) {
		throw new SystemException("Failed to notify CARES of change to membership ID " + memChangeEvent.getMembershipID() + ".  Unable to get membership VO.");
	    } else {
		CadExportMgr cadExportMgr = CommonEJBHelper.getCadExportMgr();
		cadExportMgr.notifyMembershipChangeToCAD(memVO, new DateTime());
	    }
	}
    }

    public String getSubscriberName() {
	return SourceSystem.CAD.getAbbreviation();
    }
}
