package com.ract.common.cad;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.CommonEJBHelper;
import com.ract.util.LogUtil;

public class CadMembershipRefreshJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {

	LogUtil.log(this.getClass(), "CadMembershipRefreshJob");

	try {

	    CadExportMgr exportMgr = CommonEJBHelper.getCadExportMgr();
	    LogUtil.log(this.getClass(), "Midas nightly refresh");
	    exportMgr.nightlyRefreshMembership();

	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}

    }

    public static void main(String args[]) {
	CadMembershipRefreshJob cmrj = new CadMembershipRefreshJob();
	try {
	    cmrj.execute(null);
	} catch (JobExecutionException ex) {
	    System.err.println(ex);
	}
    }

}
