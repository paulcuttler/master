package com.ract.common.cad;

import com.ract.common.CommonEJBHelper;
import com.ract.util.DateTime;
import com.ract.util.Interval;

/**
 * Title: CadExpire Description: Routine for notifying CARES of personal
 * memberships which have exceeded their expiry date by three months. This class
 * has a main method and must be run daily, as only memberships expiring on the
 * previous day are collected. (This could become a problem, but there is no way
 * to flag whether or not a membership has already been removed.) It is expected
 * to be run by a UNIX CRON task
 * 
 * Also deletes old cad-export records (Question: Why aren't they deleted as
 * soon as they are uploaded to CARES?) Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class CadExpire {
    private static String HOST = null;

    private static DateTime memExpiryDate = null;

    private static DateTime stSubChgDate = null;

    private static DateTime removeCADDate = null;

    private static Integer memDays = new Integer(0);

    private static Integer stSubDays = new Integer(0);

    private static Integer cadDays = new Integer(0);

    public CadExpire() {
	try {

	    // jnp host/port no longer required. Require a jndi.properties file
	    // in the classpath
	    CadExportMgr cadMgr = CommonEJBHelper.getCadExportMgr();

	    System.out.println("Expire memberships started......" + new DateTime().formatLongDate());
	    System.out.println("Memberships expiring ..........." + memExpiryDate.formatShortDate());
	    cadMgr.sendExpiredMembershipsToCAD(memExpiryDate);

	    System.out.println("Street Suburb Change started...." + new DateTime().formatLongDate());
	    System.out.println("Street Suburb Changes..........." + stSubChgDate.formatShortDate());
	    cadMgr.sendAddressChangesToCAD(stSubChgDate);

	    System.out.println("Remove old CAD started.........." + new DateTime().formatLongDate());
	    System.out.println("Old CAD's ......................" + removeCADDate.formatShortDate());
	    cadMgr.removeOldCADs(removeCADDate);

	    System.out.println("Memberships expired and old records successfully purged " + new DateTime().formatLongDate());
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    public static void main(String[] args) {
	if (args.length != 4) {
	    System.out.println("Usage: CadExpire [localhost:1099] [Membership Expiry Days before] [Street/Suburb Change Days before]] [Remove From CAD Days before]]");
	    System.out.println("Days before in format -n ");
	    return;
	}

	HOST = args[0];
	try {
	    memDays = new Integer(args[1]);
	} catch (NumberFormatException ex1) {
	    System.out.println("Unable to parse Membership Expiry Days Before " + args[1]);
	    return;
	}

	memExpiryDate = constructDate(memDays);

	try {
	    stSubDays = new Integer(args[2]);
	} catch (NumberFormatException ex) {
	    System.out.println("Unable to parse Street/Suburb Change Days Before " + args[2]);
	    return;
	}
	stSubChgDate = constructDate(stSubDays);

	try {
	    cadDays = new Integer(args[3]);
	} catch (NumberFormatException ex) {
	    System.out.println("Unable to parse Remove From CAD Days Before " + args[3]);
	    return;
	}
	removeCADDate = constructDate(cadDays);

	System.out.println("Run commenced: " + (new DateTime()).formatLongDate());
	new CadExpire();
	System.out.println("Run Completed: " + (new DateTime()).formatLongDate());

    }

    private static DateTime constructDate(Integer numDays) {
	return (new DateTime().add(new Interval().addDays(numDays.intValue()))).getDateOnly();
    }

}
