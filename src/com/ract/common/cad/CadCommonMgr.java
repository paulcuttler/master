package com.ract.common.cad;

import java.util.Collection;

import javax.ejb.Remote;

import com.ract.common.cad.dapfleet.DAPFleet;
import com.ract.util.DateTime;

@Remote
public interface CadCommonMgr {

    public Collection<DAPFleet> getDAPFleetMembers();

    public String getParameter(String category, String keyValue);

    public Integer getIntegerParameter(String category, String keyValue);

    public DateTime getDateParameter(String category, String keyValue);
}
