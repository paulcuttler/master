package com.ract.common.cad;

public class AgentPK implements java.io.Serializable {
    public void setAgentId(String agentId) {
	this.agentId = agentId;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public AgentPK() {
    }

    /**
     * @hibernate.key-property column="location"
     */
    public String getLocation() {
	return location;
    }

    private String location;

    /**
     * @hibernate.key-property column="agent_id"
     * @return String
     */
    public String getAgentId() {
	return agentId;
    }

    private String agentId;

    public boolean equals(Object obj) {
	if (obj instanceof AgentPK) {
	    AgentPK that = (AgentPK) obj;
	    return this.agentId.equals(that.agentId) && this.location.equals(that.location);
	}
	return false;
    }

    public int hashCode() {
	return this.agentId.hashCode() + this.location.hashCode();
    }

    public String toString() {
	return this.agentId + "/" + this.location;
    }

}
