package com.ract.common.cad;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemParameterVO;
import com.ract.common.cad.dapfleet.DAPFleet;
import com.ract.util.DateTime;
import com.ract.util.EnvVarUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

@Stateless
@Remote({ CadCommonMgr.class })
@Local({ CadCommonMgrLocal.class })
public class CadCommonMgrBean {

    // private static final long serialVersionUID = -4977622166779282521L;

    @PersistenceContext(unitName = "master")
    EntityManager em;

    @Resource
    private SessionContext sessionContext;

    @Resource(mappedName = "java:/ClientDS")
    private DataSource dataSource;

    public Collection<DAPFleet> getDAPFleetMembers() {
	/*
	 * This will get a record from gn-table CAD DAPFLEET. The description
	 * will contain a comma separated list of Keys for the dapfleet members
	 * e.g. TOYOTA,TASGOV These keys will be used to look up the client
	 * numbers from the CAD table in gn-table i.e CAD TOYOTA will have the
	 * client number for the toyota dapfleet.
	 */
	Collection<DAPFleet> dapFleetMembers = new ArrayList<DAPFleet>();

	DAPFleet df = null;

	String dapFleetList = getParameter(SystemParameterVO.CATEGORY_CAD, SystemParameterVO.CAD_DAPFLEETLIST);
	LogUtil.log(this.getClass(), "RLG +++++ " + dapFleetList);
	String dapFleetMemberString[] = dapFleetList.split(",");
	LogUtil.log(this.getClass(), "RLG +++++ " + dapFleetMemberString.length);
	for (int i = 0; i < dapFleetMemberString.length; i++) {
	    String dapFleetKey = dapFleetMemberString[i];
	    LogUtil.log(this.getClass(), "RLG +++++ " + dapFleetKey);
	    Integer clientNo = getIntegerParameter(SystemParameterVO.CATEGORY_CAD, dapFleetMemberString[i]);
	    LogUtil.log(this.getClass(), "RLG +++++ " + clientNo.toString());
	    String filePathName = getOutputDirectory(SystemParameterVO.CATEGORY_EXTRACT_DIRS, dapFleetKey);
	    LogUtil.log(this.getClass(), "RLG +++++ " + filePathName);
	    String filePrefix = getParameter(SystemParameterVO.CATEGORY_CAD, SystemParameterVO.CAD_DAPFLEET_FILE_PREFIX + dapFleetKey);
	    LogUtil.log(this.getClass(), "RLG +++++ " + filePrefix);
	    DateTime cutOffDate = getCutOffDate(dapFleetKey);
	    LogUtil.log(this.getClass(), "RLG +++++ " + cutOffDate.formatLongDate());

	    String vehMake = getParameter(SystemParameterVO.CATEGORY_CAD, SystemParameterVO.CAD_DAPFLEET_MAKE + dapFleetKey);

	    String schemeCode = getParameter(SystemParameterVO.CATEGORY_CAD, SystemParameterVO.CAD_DAPFLEET_SCHEME + dapFleetKey);

	    DateTime joinedDate = getDateParameter(SystemParameterVO.CATEGORY_CAD, SystemParameterVO.CAD_DAPFLEET_JOINED);

	    df = new DAPFleet(dapFleetKey, clientNo, filePathName, filePrefix, cutOffDate, joinedDate, vehMake, schemeCode);

	    dapFleetMembers.add(df);

	}

	return dapFleetMembers;

    }

    private DateTime getCutOffDate(String keyValue) {
	Interval i;
	try {
	    i = new Interval(getParameter(SystemParameterVO.CATEGORY_CAD, (SystemParameterVO.CAD_FLEET_EXPIRY_CUTOFF + keyValue).trim()));
	} catch (ParseException e) {
	    i = new Interval();
	    try {
		i.setYears(4);
	    } catch (Exception e1) {
		// should never be thrown
	    }
	}

	return new DateTime().subtract(i);

    }

    public DateTime getDateParameter(String category, String keyValue) {
	try {
	    return new DateTime(getParameter(category, keyValue));
	} catch (Exception e) {
	    return new DateTime();
	}
    }

    public String getParameter(String category, String keyValue) {
	try {

	    return CommonEJBHelper.getCommonMgr().getSystemParameter(category, keyValue).getValue();
	} catch (Exception e) {
	    LogUtil.log(this.getClass(), "RLG ++++ Unable to get " + category + "/" + keyValue + " " + e.getMessage());
	    return "";
	}
    }

    public Integer getIntegerParameter(String category, String keyValue) {
	return new Integer(getParameter(category, keyValue));
    }

    private String getOutputDirectory(String tableName, String tableKey) {
	String dirName;
	try {
	    dirName = getParameter(tableName, tableKey);

	    /*
	     * Path name has a Unix environment variable in it e.g.
	     * $CAD_DIR/specials This will be /ract/prd/import/cad/specials or
	     * /ract/dev/import/cad/specials depending on the enviroment -
	     * prd,tst,dev etc
	     */
	    LogUtil.log(this.getClass(), "RLG ++++ pre " + dirName);
	    dirName = EnvVarUtil.replaceEnvironmentVariable(dirName);
	    LogUtil.log(this.getClass(), "RLG ++++ post " + dirName);
	} catch (Exception e) {
	    LogUtil.log(this.getClass(), "No table entry found for " + tableName + "/" + tableKey);
	    dirName = null;
	}

	return dirName;
    }

}
