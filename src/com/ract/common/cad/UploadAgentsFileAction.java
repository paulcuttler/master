package com.ract.common.cad;

import java.io.File;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.membership.MembershipMgrLocal;

public class UploadAgentsFileAction extends ActionSupport {

    public Double getFileSize() {
	return fileSize;
    }

    public void setFileSize(Double fileSize) {
	this.fileSize = fileSize;
    }

    public File getUpfile() {
	return upfile;
    }

    public void setUpfile(File upfile) {
	this.upfile = upfile;
    }

    public Integer getRecordCount() {
	return recordCount;
    }

    public void setRecordCount(Integer recordCount) {
	this.recordCount = recordCount;
    }

    public String getUpfileFileName() {
	return upfileFileName;
    }

    public void setUpfileFileName(String upfileFileName) {
	this.upfileFileName = upfileFileName;
    }

    private Double fileSize;

    private File upfile;

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    private Integer recordCount;

    private String upfileFileName;

    @Override
    public String execute() throws Exception {
	recordCount = membershipMgr.uploadAgentsFile(upfile);
	fileSize = new Double(upfile.length() / 1024);
	return SUCCESS;
    }

}