package com.ract.common.cad;

import java.io.IOException;

import com.progress.open4gl.ConnectException;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;
import com.ract.common.CommonConstants;
import com.ract.common.proxy.ProgressProxy;

/**
 * CAD progress proxy implementation.
 * 
 * @author jyh
 * @version 1.0
 */

public class CADProgressProxy extends CadProxy implements ProgressProxy {
    public CADProgressProxy() throws ConnectException, Open4GLException, SystemErrorException, IOException {
	super(CommonConstants.getProgressAppServerURL(), CommonConstants.getProgressAppserverUser(), CommonConstants.getProgressAppserverPassword(), null);

    }
}
