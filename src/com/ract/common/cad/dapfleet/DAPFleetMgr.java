package com.ract.common.cad.dapfleet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.ejb.Stateless;

import com.ract.common.CommonEJBHelper;
import com.ract.common.cad.CadCommonMgr;
import com.ract.common.cad.CadExport;
import com.ract.common.cad.CadExportMgr;
import com.ract.common.cad.CadHelper;
import com.ract.common.cad.CadMembershipData;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

@SuppressWarnings("serial")
@Stateless
// @Remote({DAPFleetMgr.class})
// @Local({DAPFleetMgrLocal.class})
public class DAPFleetMgr implements Serializable, DAPFleetMgrLocal, DAPFleetMgrRemote {
    private final int MembershipId = 0;
    private final int ExpiryDate = 4;
    private final int RegistrationNumber = 10;
    private final int Model = 24;
    private final int Make = 25;

    private Collection<DAPFleet> dapFleets = null;

    CadCommonMgr ccm = null;
    CadExportMgr cem = null;

    public String processAllDAPFleets() {
	// LogUtil.log(this.getClass(),"RLG +++++ Starting DAPFLEET Update");
	System.out.println("RLG +++++ Starting DAPFLEET Update");
	dapFleets = initDAPFleetUpdater();
	System.out.println("RLG +++++ " + dapFleets.size());
	// LogUtil.log(this.getClass(),"RLG +++++ " + dapFleets.size() );
	processDAPFleets(dapFleets);

	terminateDAPFleetUpdater();

	return new String();

    }

    private Collection<DAPFleet> initDAPFleetUpdater() {
	ccm = CommonEJBHelper.getCadCommonMgr();
	cem = CommonEJBHelper.getCadExportMgr();

	return ccm.getDAPFleetMembers();
    }

    private void terminateDAPFleetUpdater() {

    }

    /*
 * 
 */

    private void processDAPFleets(Collection<DAPFleet> dapFleets) {

	for (DAPFleet dapFleet : dapFleets) {
	    processThisDAPFleet(dapFleet);
	}

    }

    /*
  *    
  */

    private void processThisDAPFleet(DAPFleet dapFleet) {
	// int dapProcessed = 0;
	// dapFleet= initThisDAPFleet ( dapFleet );
	System.out.println("RLG +++++ processing " + dapFleet.getDapFleetKey());
	for (File file : dapFleet.getFileNameList()) {
	    this.processThisDAPFleetFile(file, dapFleet);
	}

    }

    private void processThisDAPFleetFile(File inputFileName, DAPFleet dapFleet) {
	BufferedReader inputReader = null;
	String fleetMembershipRecord = null;
	CadMembershipData cmd = new CadMembershipData();
	try {
	    inputReader = new BufferedReader(new FileReader(inputFileName));

	    while ((fleetMembershipRecord = inputReader.readLine()) != null) {
		dapFleet.incrementSeqNo();
		processThisLine(dapFleet, cmd, fleetMembershipRecord);

	    }
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} finally {
	    terminateProcessThisDAPFleetFile(inputReader);
	}

	System.out.println("RLG ++++++ " + inputFileName.getName());

    }

    private void terminateProcessThisDAPFleetFile(BufferedReader inputReader) {
	try {
	    inputReader.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private void processThisLine(DAPFleet dapFleet, CadMembershipData cmd, String fleetMembershipRecord) {
	LogUtil.log(this.getClass(), "RLG ++++++ " + fleetMembershipRecord);
	String fleetData[] = fleetMembershipRecord.split(",");

	if (validFleetRecord(dapFleet.getClientId(), fleetData[MembershipId], dapFleet.getCutOffDate(), fleetData[ExpiryDate])) {
	    LogUtil.log(this.getClass(), "RLG ++++++ process " + fleetMembershipRecord);

	    dapFleet.incrementSeqNo();

	    String fleetXML = XMLHelper.getRACTImportStartTag() + dapFleet.getClientXML(dapFleet.getSeqNo()) + this.creatFleetXML(dapFleet, cmd, fleetData) + XMLHelper.getRACTImportEndTag();
	    LogUtil.log(this.getClass(), "RLG ++++++ " + fleetXML);

	    CadExport ce = new CadExport(dapFleet.getDapFleetKey(), cmd.getMembershipNumber(), fleetXML);

	    // try
	    // {
	    // // cem.createCadExport( ce );
	    // }
	    // catch (RemoteException e)
	    // {
	    // // TODO Auto-generated catch block
	    // e.printStackTrace();
	    // }
	}

    }

    private String creatFleetXML(DAPFleet dapFleet, CadMembershipData cmd, String[] fleetData) {
	cmd.setAttributesToNull();

	cmd.setMembershipCancelled("N");
	cmd.setMembershipYears("0");
	cmd.setMembershipType("F");
	cmd.setMembershipLevel("ADV");
	cmd.setMembershipNumber(CadHelper.formatMembershipNumber(dapFleet.getClientId(), dapFleet.getSchemeCode(), dapFleet.getSeqNo()));
	cmd.setMembershipJoined(dapFleet.getJoinedDate().formatShortDate());
	cmd.setMembershipVehicleMake(dapFleet.getMake());
	cmd.setMembershipVehicleModel(fleetData[Model]);
	cmd.setMembershipVehicleReg(fleetData[RegistrationNumber]);

	return cmd.getXML();

    }

    private boolean validFleetRecord(Integer referenceClientNo, String thisClientNo, DateTime referenceExpiryDate, String thisExpiryDate) {

	try {
	    Integer clientNo = new Integer(thisClientNo);

	    if (!referenceClientNo.equals(clientNo)) {
		return false;
	    }
	} catch (Exception e) {
	    return false;
	}

	try {
	    Date vehExpiryDate = DateUtil.parseDate(thisExpiryDate);

	    if (referenceExpiryDate.onOrAfterDay(vehExpiryDate)) {
		return false;
	    }

	} catch (Exception e) {

	    return false;
	}

	return true;
    }

}
