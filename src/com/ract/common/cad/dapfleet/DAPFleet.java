package com.ract.common.cad.dapfleet;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.ract.common.cad.fleet.FleetClientData;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;

@SuppressWarnings("serial")
public class DAPFleet implements Serializable {

    private String dapFleetKey;

    private Integer clientId;

    private String filePrefix;

    // private String newFileName;

    private String filePathName;

    // private String fileListName;

    private FleetClientData clientData;

    private List<File> filesToProcess;

    private int seqNo;

    private DateTime cutOffDate;

    private String schemeCode;

    private DateTime joinedDate;

    private String make;

    /*
     * ====================================
     */

    public DAPFleet() {

	clientData = null;
	dapFleetKey = null;
	clientId = null;
	filePrefix = null;
	filesToProcess = null;

    }

    public DAPFleet(String fleetKey, Integer clientNo, String filePathName, String filePrefix, DateTime expiryCutoffDate, DateTime joinedDate, String make, String schemeCode) {
	this.setDapFleetKey(fleetKey);
	this.setClientId(clientNo);
	this.setFilePathName(filePathName);
	this.setFilePrefix(filePrefix);
	this.setCutOffDate(expiryCutoffDate);
	this.setJoinedDate(joinedDate);
	this.setMake(make);
	this.setSchemeCode(schemeCode);

	this.setClientData(clientNo);

	this.initializeSeqNo();

	this.changeNewFileName();
	this.setFileNameList();
    }

    /*
     * ====================================
     */

    private void changeNewFileName() {

	/*
	 * When new dapfleet vehicles are to be added to MIDAS a file called
	 * Toyota.csv or Tasgov.csv is created in the dapfleet directory. This
	 * changes the file to Toyota_YYYYMMDD.csv or Tasgov_YYYYMMDD.csv
	 */
	String baseFileName = this.getFilePathName() + "/" + this.getFilePrefix().trim();

	String oldFileName = baseFileName + ".csv";

	String newFileName = FileUtil.constructFileName(baseFileName + "_", null, FileUtil.EXTENSION_CSV);

	System.out.println("RLG +++++ old " + oldFileName);
	System.out.println("RLG +++++ new " + newFileName);
	try {
	    FileUtil.renameFile(oldFileName, newFileName);
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public String getSchemeCode() {
	return schemeCode;
    }

    public void setSchemeCode(String schemeCode) {
	this.schemeCode = schemeCode;
    }

    public DateTime getJoinedDate() {
	return joinedDate;
    }

    public void setJoinedDate(DateTime joinedDate) {
	this.joinedDate = joinedDate;
    }

    public String getMake() {
	return make;
    }

    public void setMake(String make) {
	this.make = make;
    }

    private void setFileNameList() {
	this.filesToProcess = FileUtil.getSortedFileNameList(this.getFilePathName(), (this.getFilePrefix().trim() + "_"));
    }

    public List<File> getFileNameList() {
	if (this.filesToProcess == null) {
	    this.setFileNameList();
	}

	return this.filesToProcess;
    }

    public String getDapFleetKey() {
	return dapFleetKey;
    }

    public void setDapFleetKey(String dapFleetKey) {
	this.dapFleetKey = dapFleetKey;
    }

    public Integer getClientId() {
	return clientId;
    }

    public void setClientId(Integer clientId) {
	this.clientId = clientId;
    }

    public String getFilePrefix() {
	return filePrefix;
    }

    public void setFilePrefix(String filePrefix) {
	this.filePrefix = filePrefix;
    }

    public String getFilePathName() {
	return filePathName;
    }

    public void setFilePathName(String filePathName) {
	this.filePathName = filePathName;
    }

    public String getClientXML() {
	return this.clientData.getClientXML();
    }

    public String getClientXML(int seqNo) {
	return this.clientData.createClientXML(seqNo);
    }

    public FleetClientData getClientData() {
	return clientData;
    }

    public void setClientData(Integer clientNo) {
	this.clientData = new FleetClientData(clientNo);
    }

    public int getSeqNo() {
	return seqNo;
    }

    public void setSeqNo(int seqNo) {
	this.seqNo = seqNo;
    }

    public void initializeSeqNo() {
	this.seqNo = 0;
    }

    public void incrementSeqNo() {
	this.seqNo++;
    }

    public DateTime getCutOffDate() {
	return cutOffDate;
    }

    public void setCutOffDate(DateTime cutOffDate) {
	this.cutOffDate = cutOffDate;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	// TODO Auto-generated method stub

    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();

	builder.append("DAPFleet [clientId=");
	builder.append(clientId);
	builder.append(", dapFleetKey=");
	builder.append(dapFleetKey);
	builder.append(", filePathName=");
	builder.append(filePathName);
	builder.append(", filePrefix=");
	builder.append(filePrefix);
	builder.append("]");

	return builder.toString();
    }
}
