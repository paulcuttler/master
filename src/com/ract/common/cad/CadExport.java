/**
 * 
 */
package com.ract.common.cad;

/**
 * @author gilesl
 */

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

@Entity
public class CadExport implements Serializable {
    @Id
    private Integer cadExportKey;
    private String sourceSystem;
    private String sourceSystemRef;
    private String exportXml;
    private DateTime created;
    private DateTime exported;

    /*
 * 
 */
    public CadExport(Integer cadExportKey, String sourceSystem, String sourceSystemRef, String exportXml) {
	this.cadExportKey = cadExportKey;
	this.sourceSystem = sourceSystem;
	this.sourceSystemRef = sourceSystemRef;
	this.exportXml = exportXml;
	this.created = new DateTime();
	this.exported = null;
    }

    public CadExport(String sourceSystem, String sourceSystemRef, String exportXml) {

	this.sourceSystem = sourceSystem;
	this.sourceSystemRef = sourceSystemRef;
	this.exportXml = exportXml;
	this.created = new DateTime();
	this.exported = null;
    }

    /**
 * 
 */
    public CadExport() {
    }

    /*
 * 
 */
    public String getSourceSystem() {
	return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public String getSourceSystemRef() {
	return sourceSystemRef;
    }

    public void setSourceSystemRef(String sourceSystemRef) {
	this.sourceSystemRef = sourceSystemRef;
    }

    public String getExportXml() {
	return exportXml;
    }

    public String getExportXmlNewLine() {
	return getExportXml() + "\n";
    }

    public void setExportXml(String exportXml) {
	this.exportXml = exportXml;
    }

    public DateTime getCreated() {
	return created;
    }

    public void setCreated(DateTime created) {
	this.created = created;
    }

    public Integer getCadExportKey() {
	return cadExportKey;
    }

    public void setCadExportKey(Integer cadExportKey) {
	this.cadExportKey = cadExportKey;
    }

    public DateTime getExported() {
	return exported;
    }

    public void setExported(DateTime exported) {
	this.exported = exported;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("CadExport [cadExportKey=");
	builder.append(cadExportKey);
	builder.append(", created=");
	builder.append(created);
	builder.append(", exportXml=");
	builder.append(exportXml);
	builder.append(", exported=");
	builder.append(exported);
	builder.append(", sourceSystem=");
	builder.append(sourceSystem);
	builder.append(", sourceSystemRef=");
	builder.append(sourceSystemRef);
	builder.append("]");
	return builder.toString();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cadExportKey == null) ? 0 : cadExportKey.hashCode());
	result = prime * result + ((created == null) ? 0 : created.hashCode());
	result = prime * result + ((exportXml == null) ? 0 : exportXml.hashCode());
	result = prime * result + ((exported == null) ? 0 : exported.hashCode());
	result = prime * result + ((sourceSystem == null) ? 0 : sourceSystem.hashCode());
	result = prime * result + ((sourceSystemRef == null) ? 0 : sourceSystemRef.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	CadExport other = (CadExport) obj;
	if (cadExportKey == null) {
	    if (other.cadExportKey != null) {
		return false;
	    }
	} else if (!cadExportKey.equals(other.cadExportKey)) {
	    return false;
	}
	if (created == null) {
	    if (other.created != null) {
		return false;
	    }
	} else if (!created.equals(other.created)) {
	    return false;
	}
	if (exportXml == null) {
	    if (other.exportXml != null) {
		return false;
	    }
	} else if (!exportXml.equals(other.exportXml)) {
	    return false;
	}
	if (exported == null) {
	    if (other.exported != null) {
		return false;
	    }
	} else if (!exported.equals(other.exported)) {
	    return false;
	}
	if (sourceSystem == null) {
	    if (other.sourceSystem != null) {
		return false;
	    }
	} else if (!sourceSystem.equals(other.sourceSystem)) {
	    return false;
	}
	if (sourceSystemRef == null) {
	    if (other.sourceSystemRef != null) {
		return false;
	    }
	} else if (!sourceSystemRef.equals(other.sourceSystemRef)) {
	    return false;
	}
	return true;
    }

}
