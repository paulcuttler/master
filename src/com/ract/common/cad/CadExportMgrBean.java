package com.ract.common.cad;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import com.progress.open4gl.IntHolder;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.ract.client.ClientVO;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ExceptionHelper;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.cad.raa.RaaRactRunner;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.proxy.ProgressProxy;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipVO;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * cad export mgr bean
 * 
 * @author dgk,jyh
 * @created 14 February 2003
 * @version 1.0
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@Remote({ CadExportMgr.class })
@Local({ CadExportMgrLocal.class })
public class CadExportMgrBean {

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource dataSource;

	/**
	 * Run the RAA - RACT compare
	 * 
	 * @param action String specifies which of the jobs to run, all, load, extract send or a specific job name
	 * 
	 * @throws RemoteException
	 */
	public void compareRAAwithRACT(String action) throws RemoteException {
		RaaRactRunner rrr = new RaaRactRunner();
		rrr.runTheExtracts(action, "RAA_RACT_COMPARE");
	}

	/**
	 * create a cad record
	 * 
	 * @param vo Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 */
	public void createCADRecord(CadExportVO vo) throws RemoteException, ValidationException {
		createCADRecord(vo.getExportType(), vo.getClientNo(), vo.getSurname(), vo.getGivenNames(), vo.getClientTitle(), vo.getProperty(), vo.getJoinDate(), vo.getMemberType(), vo.getBirthDate(), vo.getExpiryDate(), vo.getPlusCommenceDate(), vo.getMembershipYears(), vo.getDeleted(), vo.getRegNo(), vo.getMapCode(), vo.getPlusFlag(), vo.getMembershipHold(), vo.getRemoveRecord(), vo.getManModel(), vo.getManCover(), vo.getHomePhone(), vo.getPlusExpiry(), vo.getAddress(), vo.getCadNumber(), vo.getAddress2(), vo.getTranNo(), vo.getCreateDateTime(), vo.getMapNo(), vo.getServiceLevel(), vo.getSeqNo(), vo.getEffectiveDate());
	}

	/**
	 * Description of the Method
	 * 
	 * @param exportType Description of the Parameter
	 * @param clientNo Description of the Parameter
	 * @param surname Description of the Parameter
	 * @param givenNames Description of the Parameter
	 * @param clientTitle Description of the Parameter
	 * @param property Description of the Parameter
	 * @param joinDate Description of the Parameter
	 * @param memberType Description of the Parameter
	 * @param birthDate Description of the Parameter
	 * @param expiryDate Description of the Parameter
	 * @param plusCommenceDate Description of the Parameter
	 * @param membershipYears Description of the Parameter
	 * @param deleted Description of the Parameter
	 * @param regNo Description of the Parameter
	 * @param mapCode Description of the Parameter
	 * @param plusFlag Description of the Parameter
	 * @param membershipHold Description of the Parameter
	 * @param removeRecord Description of the Parameter
	 * @param manModel Description of the Parameter
	 * @param manCover Description of the Parameter
	 * @param homePhone Description of the Parameter
	 * @param plusExpiry Description of the Parameter
	 * @param address Description of the Parameter
	 * @param cadNumber Description of the Parameter
	 * @param address2 Description of the Parameter
	 * @param tranNo Description of the Parameter
	 * @param createDateTime Description of the Parameter
	 * @param mapNo Description of the Parameter
	 * @param serviceLevel Description of the Parameter
	 * @param seqNo Description of the Parameter
	 * @param effectiveDate Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 */
	private void createCADRecord(String exportType, Integer clientNo, String surname, String givenNames, String clientTitle, String property, String joinDate, String memberType, String birthDate, String expiryDate, String plusCommenceDate, Integer membershipYears, String deleted, String regNo, String mapCode, String plusFlag, String membershipHold, String removeRecord, String manModel, String manCover, String homePhone, String plusExpiry, String address, String cadNumber, String address2, Integer tranNo, DateTime createDateTime, String mapNo, String serviceLevel, String seqNo, DateTime effectiveDate) throws RemoteException, ValidationException {
		/*
		 * LogUtil.info(this.getClass(), "clientNo="+clientNo); LogUtil.info(this.getClass(), "surname="+surname); LogUtil.info(this.getClass(), "givenNames="+givenNames); LogUtil.info(this.getClass(), "clientTitle="+clientTitle); LogUtil.info(this.getClass(), "property="+property); LogUtil.info(this.getClass(), "membershipYears="+membershipYears); LogUtil.info(this.getClass(), "address="+address); LogUtil.info(this.getClass(), "address2="+address2); LogUtil.info(this.getClass(), "createDateTime="+createDateTime);
		 */
		// Check to see if a refresh is underway first.
		if (!RefreshMembershipWorker.STATUS_IDLE.equals(getRefreshMembershipStatus())) {
			throw new ValidationException("A refresh of the ROADSIDE data in CARES is currently being done. No transactions are permitted until the ROADSIDE data has been extracted.");
		}

		ProgressCadTransactionProxy cadTransProxy = null;
		CADProgressProxy cadProxy = CadExportUtil.getCADProxy();
		try {
			cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(createDateTime.getDateOnly());
			GregorianCalendar effDate = new GregorianCalendar();
			effDate.setTime(effectiveDate);

			cadTransProxy.notifyCares(exportType, clientNo.intValue(), surname.trim(), givenNames.trim(), clientTitle.trim(), property.trim(), joinDate, memberType, birthDate, expiryDate, plusCommenceDate, membershipYears.intValue(), deleted, regNo, mapCode, plusFlag, membershipHold, removeRecord, manModel, manCover, homePhone, plusExpiry, address.trim(), cadNumber, address2.trim(), gc, mapNo, createDateTime.getSecondsFromMidnight(), serviceLevel, seqNo, effDate, 0); // No
			// control
			// ID
			// when
			// doing
			// single
			// updates

			cadTransProxy.commit();
		} catch (Exception e) {
			String errorMessage = CadExportUtil.getProxyErrorMessage(cadTransProxy, e);
			try {
				cadTransProxy.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			throw new SystemException("Error in cad proxy: " + errorMessage, e);
		} finally {
			try {
				cadTransProxy._release();
				cadTransProxy = null;
			} catch (Exception ee) {
				ee.printStackTrace();
			}
			CadExportUtil.releaseCADProxy(cadProxy);
		}
	}

	/**
	 * Description of the Method
	 * 
	 * @param date Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 */
	public void sendExpiredMembershipsToCAD(DateTime memExpiryDate) throws RemoteException {
		if (!RefreshMembershipWorker.STATUS_IDLE.equals(getRefreshMembershipStatus())) {
			throw new RemoteException("A refresh of the ROADSIDE data in CARES is currently being done. Another refresh is not permitted until the current one is finished.");
		}

		// Hashtable productList = CadExportUtil.getProductList(dataSource);

		System.out.println("Extracting Memberships expiring " + memExpiryDate.formatShortDate());

		StringBuffer sql = new StringBuffer();
		sql.append("\n Select client_number,surname,\"given-names\",\"client-title\",");
		sql.append("\n \"birth-date\",\"home-phone\",\"resi-street-char\",street,\"resi-property\",");
		sql.append("\n suburb,postcode,mem.product_code,expiry_date,join_date, sort_order, 'Y','Y' ");
		sql.append("\n from PUB.mem_membership mem, PUB.\"cl-master\" clt, PUB.\"gn-stsub\" stsub,");
		sql.append("\n PUB.mem_product mp ");
		sql.append("\n where clt.\"client-no\" = mem.client_number");
		sql.append("\n and stsub.\"stsubid\" = clt.\"resi-stsubid\"");
		sql.append("\n and mp.notifiable = 1 ");
		sql.append("\n and mp.product_code = mem.product_code ");
		// sql.append("\n and mem.expiry_date = ?");
		sql.append("\n and mem.expiry_date = ");
		sql.append("'" + memExpiryDate.toSQLDate() + "'");

		updateCADRecords(sql.toString());
	}

	public void sendAddressChangesToCAD(DateTime addressChangeDate) throws RemoteException {
		if (!RefreshMembershipWorker.STATUS_IDLE.equals(getRefreshMembershipStatus())) {
			throw new RemoteException("A refresh of the ROADSIDE data in CARES is currently being done. Another refresh is not permitted until the current one is finished.");
		}

		System.out.println("Extracting Address changes " + addressChangeDate.formatShortDate());

		StringBuffer sql = new StringBuffer();
		sql.append("\n Select client_number,surname,\"given-names\",\"client-title\",");
		sql.append("\n \"birth-date\",\"home-phone\",\"resi-street-char\",street,\"resi-property\",");
		sql.append("\n suburb,postcode,mem.product_code,expiry_date,join_date, sort_order, 'N', 'N' ");
		sql.append("\n from PUB.mem_membership mem, PUB.\"cl-master\" clt, PUB.\"gn-stsub\" stsub,");
		sql.append("\n PUB.mem_product mp ");
		sql.append("\n where clt.\"client-no\" = mem.client_number");
		sql.append("\n and stsub.\"stsubid\" = clt.\"resi-stsubid\"");
		sql.append("\n and mp.notifiable = 1 ");
		sql.append("\n and mp.product_code = mem.product_code ");
		sql.append("\n and stsub.[last-update] = ");
		sql.append("'" + addressChangeDate.toSQLDate() + "'");

		updateCADRecords(sql.toString());
	}

	public void sendClientsToCAD(String sql) throws RemoteException {
		System.out.println("sendClientsToCAD");
		this.updateCADRecords(sql);
	}

	// private void insertCADCandidates ()
	// {
	//
	// }

	/**
	 * 
	 * @param sql String
	 * @throws RemoteException
	 */
	private void updateCADRecords(String sql) throws RemoteException {

		Connection connection = null;
		PreparedStatement statement = null;
		DateTime today = new DateTime();

		int clientNumber = 0;
		String surname = null;
		String givenNames = null;
		String clientTitle = null;
		// java.sql.Date birthDate = null;
		String homePhone = null;
		String addressLine1 = null;
		String addressLine2 = null;

		String membershipType = null;
		String resiProperty = null;

		String deleted = null;
		String removed = null;
		String productCode = null;
		String birthDateString = "";
		String expiryDateString = "";
		String joinDateString = "";
		java.sql.Date expiryDate = null;
		java.sql.Date joinDate = null;
		int count = 0;
		int errors = 0;
		int controlId = 0;

		// StringBuffer howFar = new StringBuffer();

		GregorianCalendar createDateGC = new GregorianCalendar();
		createDateGC.setTime(today.getDateOnly());
		GregorianCalendar effDateGC = new GregorianCalendar();
		ProgressCadTransactionProxy cadTransProxy = null;
		CADProgressProxy cadProxy = CadExportUtil.getCADProxy();

		try {

			cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();

			connection = dataSource.getConnection();

			statement = connection.prepareStatement(sql);

			ResultSet rs = statement.executeQuery();

			int memYears = 0;
			int productLevel = 0;

			audit("CADEXPORT Beginning scan ");

			while (rs.next()) {

				count++;

				if ((count % 1000) == 0) {
					/*
					 * Committing now will shorten the transaction in Progress. This will prevent the Lock Table being overloaded. A new transaction will be started with the call to cadTransProxy.notifyCares below
					 */
					audit("updateCADRecords : Committing Progress " + new Integer(count).toString());
					cadTransProxy.commit();
				}

				clientNumber = rs.getInt(1);

				surname = rs.getString(2).trim();

				givenNames = rs.getString(3) == null ? "" : rs.getString(3).trim();

				clientTitle = rs.getString(4) == null ? "" : rs.getString(4).trim();

				homePhone = rs.getString(6) == null ? "" : rs.getString(6).trim();

				resiProperty = rs.getString(9) == null ? "" : rs.getString(9).trim();

				try {
					controlId = rs.getInt("ControlId");
				} catch (Exception ex) {
					controlId = 0; // No control ID when expiring memberships
				}

				try {
					membershipType = rs.getString("MemType");
				} catch (Exception ex) {
					membershipType = SourceSystem.MEMBERSHIP.getAbbreviation();
				}

				try {

					addressLine1 = CadExportUtil.getAddressLine1(rs.getString(9), rs.getString(7), rs.getString(8));

					addressLine2 = CadExportUtil.getAddressLine2(rs.getString(10), rs.getInt(11));

					productCode = rs.getString(12) == null ? " " : rs.getString(12);

					expiryDate = rs.getDate(13);

					joinDate = rs.getDate(14);

					productLevel = rs.getInt(15);

					deleted = rs.getString(16) == null ? "" : rs.getString(16);

					removed = rs.getString(17) == null ? "" : rs.getString(17);

					if (expiryDate != null && joinDate != null) {
						memYears = (new DateTime(expiryDate).getDateYear()) * 10000 + (new DateTime(joinDate).getDateYear());
					} else {
						memYears = 0;
					}

					effDateGC.setTime(new DateTime(expiryDate));

					if (givenNames != null) {
						givenNames = givenNames.trim();
					} else {
						givenNames = "";
					}

					try {
						birthDateString = (new DateTime(rs.getDate(5)).formatShortDate());
					} catch (Exception ee) { // do nothing. Leave
						// birthDateString empty
						birthDateString = "";
					}

					if (joinDate != null) {
						joinDateString = (new DateTime(joinDate)).formatShortDate();
					} else {
						joinDateString = "";
					}

					if (expiryDate != null) {
						expiryDateString = (new DateTime(expiryDate)).formatShortDate();
					} else {
						expiryDateString = "";
					}

					cadTransProxy.notifyCares(membershipType, clientNumber, surname, givenNames, clientTitle, resiProperty, joinDateString, productCode, birthDateString, expiryDateString, "", // plus
							// commence
							// date
							memYears, // membership Years
							deleted, // delete flag
							"", // reg No
							"", // map code
							(productLevel == 0 ? "+" : ""), "N", // should not
							// be on hold
							removed, // remove record
							"", // manModel
							"", // manCover
							homePhone, "", // plusExpiry
							addressLine1, "", // cadNo
							addressLine2, createDateGC, "", // map no
							today.getSecondsFromMidnight(), "", // service level
							"", // seq no
							effDateGC, controlId);

				} catch (Exception dataExc) {
					errors++;
					LogUtil.warn(this.getClass(), "Faulty data for client " + clientNumber + " " + dataExc.getMessage());
					LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(dataExc));
					continue;
				}

			}
			rs.close();
			// while
			cadTransProxy.commit();
		} catch (Exception e) {
			String errorMessage = CadExportUtil.getProxyErrorMessage(cadTransProxy, e);
			try {
				cadTransProxy.rollback();
			} catch (Exception e1) {
			}

			LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
			throw new RemoteException("Error in getCadRecords : " + errorMessage, e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
			try {
				cadTransProxy._release();
				cadTransProxy = null;
			} catch (Exception ee) {
			}
			CadExportUtil.releaseCADProxy(cadProxy);
		}

		audit(count + " CAD Records processed " + errors + " errors " + new DateTime().formatLongDate());
	}

	/**
	 * Return a list of CadExportControlVO representing the data from the cad-export-ctrl table in the Progress CAD export system.
	 */
	public ArrayList getCadExportControlList() throws RemoteException {
		ArrayList controlList = new ArrayList();
		CADProgressProxy cadProxy = CadExportUtil.getCADProxy();
		ResultSetHolder rsh = new ResultSetHolder();
		try {
			cadProxy.getCadExportCtrl(rsh);
			if (!rsh.isNull()) {
				ResultSet rs = rsh.getResultSetValue();
				CadExportControlVO controlVO;
				int controlID = 0;
				DateTime startDate;
				DateTime endDate;
				DateTime exportDate;
				java.sql.Date sqlDate;

				while (rs.next()) {
					controlID = rs.getInt("ctrlId");
					sqlDate = rs.getDate("buildStartDate");
					startDate = sqlDate == null || rs.wasNull() ? null : new DateTime(sqlDate.getTime() + rs.getInt("buildStartTime"));
					sqlDate = rs.getDate("buildEndDate");
					endDate = sqlDate == null || rs.wasNull() ? null : new DateTime(sqlDate.getTime() + rs.getInt("buildEndTime"));
					sqlDate = rs.getDate("exportDate");
					exportDate = sqlDate == null || rs.wasNull() ? null : new DateTime(sqlDate.getTime() + rs.getInt("exportTime"));
					controlVO = new CadExportControlVO(controlID, startDate, endDate, exportDate);
					controlVO.setReadOnly(true);
					controlList.add(controlVO);
				}
			}
		} catch (Open4GLException gle) {
			throw new SystemException("Error fetching CAD export control : " + CadExportUtil.getProxyErrorMessage(cadProxy, gle), gle);
		} catch (SQLException sqle) {
			throw new SystemException("Error fetching CAD export control : " + sqle.getMessage(), sqle);
		} finally {
			CadExportUtil.releaseCADProxy(cadProxy);
		}

		return controlList;
	}

	/**
	 * Return a list of CadExportControlVO representing the data from the cad-export-ctrl table in the Progress CAD export system.
	 */
	public ArrayList getCadExportList(Integer clientNumber) throws RemoteException {
		ArrayList cadList = new ArrayList();
		CADProgressProxy cadProxy = CadExportUtil.getCADProxy();
		ResultSetHolder rsh = new ResultSetHolder();
		try {
			cadProxy.getCadExportForClient(clientNumber.intValue(), rsh);
			if (!rsh.isNull()) {
				ResultSet rs = rsh.getResultSetValue();
				CadExportVO cadVO;
				java.sql.Date sqlDate;
				long time = 0;
				String streetChar;

				while (rs.next()) {
					cadVO = new CadExportVO();
					cadVO.setExportType(rs.getString(1));
					cadVO.setClientNo(new Integer(rs.getInt(2)));
					cadVO.setSurname(rs.getString(3));
					cadVO.setGivenNames(rs.getString(4));
					cadVO.setClientTitle(rs.getString(5));
					cadVO.setProperty(rs.getString(6));
					streetChar = rs.getString(7);
					cadVO.setMemberType(rs.getString(8));
					cadVO.setMembershipYears(rs.getInt(9));
					cadVO.setRegNo(rs.getString(10));
					cadVO.setMapCode(rs.getString(11));
					cadVO.setCadNumber(rs.getString(12));
					cadVO.setTranNo(new Integer(rs.getInt(13)));
					cadVO.setBirthDate(rs.getString(14));
					cadVO.setExpiryDate(rs.getString(15));
					cadVO.setPlusCommenceDate(rs.getString(16));
					cadVO.setDeleted(rs.getString(17));
					cadVO.setPlusFlag(rs.getString(18));
					cadVO.setMembershipHold(rs.getString(19));
					cadVO.setRemoveRecord(rs.getString(20));
					cadVO.setManModel(rs.getString(21));
					cadVO.setHomePhone(rs.getString(22));
					cadVO.setPlusExpiry(rs.getString(23));
					cadVO.setAddress(rs.getString(24));
					cadVO.setAddress2(rs.getString(25));
					sqlDate = rs.getDate(26);
					cadVO.setMapNo(rs.getString(27));
					time = rs.getLong(28);
					if (sqlDate != null) {
						cadVO.setCreateDateTime(new DateTime((sqlDate.getTime() + time)));
					}
					cadVO.setServiceLevel(rs.getString(29));
					cadVO.setManCover(rs.getString(30));
					cadVO.setSeqNo(rs.getString(31));
					sqlDate = rs.getDate(32);
					if (sqlDate != null) {
						cadVO.setEffectiveDate(new DateTime(sqlDate.getTime()));
					}
					cadVO.setControlID(rs.getInt(33));

					cadList.add(cadVO);
				}
			}
		} catch (Open4GLException gle) {
			throw new SystemException("Error fetching CAD export (Open4GLException) : " + CadExportUtil.getProxyErrorMessage(cadProxy, gle), gle);
		} catch (SQLException sqle) {
			throw new SystemException("Error fetching CAD export (SQLException) : " + sqle.getMessage(), sqle);
		} finally {
			CadExportUtil.releaseCADProxy(cadProxy);
		}

		return cadList;
	}

	/**
	 * Description of the Method
	 * 
	 * @param membershipVO Description of the Parameter
	 * @param effectiveDate Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 * @exception ValidationException Description of the Exception
	 */
	public void notifyMembershipChangeToCAD(MembershipVO membershipVO, DateTime effectiveDate) throws RemoteException, ValidationException {

		/*
		 * LogUtil.log(this.getClass(), "notifyMembershipChangeToCAD," + membershipVO.getMembershipID() + "," + effectiveDate.formatLongDate() );
		 */
		String productCode = membershipVO.getCurrentProductCode();

		if (productCode == null) {
			LogUtil.log(this.getClass(), "No product code specified for " + membershipVO.getMembershipID() + ".");
			return;
		}

		CadExportVO cadVO = new CadExportVO();
		Hashtable productList = null;
		// assume this is a personal membership
		try {
			productList = CadExportUtil.getProductList(dataSource);
		} catch (RemoteException e) {
			throw new RemoteException("Unable to get valid list of products." + e);
		}
		ClientVO clientVO = membershipVO.getClient();
		ResidentialAddressVO streetAddress = clientVO.getResidentialAddress();
		String address1 = streetAddress.getAddressLine1() + " " + streetAddress.getAddressLine2();
		address1 = address1.trim();
		String address2 = streetAddress.getSuburb() + " " + streetAddress.getPostcode();
		cadVO.setAddress(address1);
		cadVO.setAddress2(address2);

		if (clientVO.getBirthDate() == null) {
			cadVO.setBirthDate("");
		} else {
			cadVO.setBirthDate(clientVO.getBirthDate().formatShortDate());
		}

		cadVO.setClientNo(clientVO.getClientNumber());
		cadVO.setClientTitle(clientVO.getTitle());
		cadVO.setCreateDateTime(new DateTime());

		String membershipStatus = membershipVO.getStatus();
		if (MembershipVO.STATUS_CANCELLED.equals(membershipStatus) || MembershipVO.STATUS_UNDONE.equals(membershipStatus)) {
			cadVO.setDeleted("Y");
		} else {
			cadVO.setDeleted("N");
		}

		cadVO.setExpiryDate(membershipVO.getExpiryDate().formatShortDate());
		int numchars = membershipVO.getMembershipTypeCode().length();
		if (numchars > 3) {
			numchars = 3;
		}
		cadVO.setExportType(membershipVO.getMembershipTypeCode().substring(0, numchars).toUpperCase());
		cadVO.setGivenNames(clientVO.getGivenNames());
		cadVO.setHomePhone(clientVO.getHomePhone());
		numchars = productCode.length();
		if (numchars > 3) {
			numchars = 3;
		}
		cadVO.setMemberType(productCode.substring(0, numchars).toUpperCase());
		cadVO.setMembershipHold((membershipVO.getStatus().equalsIgnoreCase(MembershipVO.STATUS_ONHOLD)) ? "Y" : "");
		cadVO.setMembershipYears(membershipVO.getJoinDate(), membershipVO.getExpiryDate());
		cadVO.setJoinDate(membershipVO.getJoinDate());

		if (productList.containsKey(productCode)) {
			int productLevel = ((Integer) productList.get(productCode)).intValue();
			cadVO.setPlusFlag(productLevel == 0 ? "+" : "");
		} else {
			cadVO.setPlusFlag("");
			cadVO.setDeleted("Y");
		}

		cadVO.setRemoveRecord(cadVO.getDeleted());
		cadVO.setSurname(clientVO.getSurname());
		cadVO.setTranNo(null);
		cadVO.setEffectiveDate(effectiveDate);

		createCADRecord(cadVO);
	}

	/**
	 * Clear obsolete records from the cad-export table
	 * 
	 * @param asAt Description of the Parameter
	 * @exception RemoteException Description of the Exception
	 */
	public void removeOldCADs(DateTime asAt) throws RemoteException {
		// Check to see if a refresh is underway first.
		if (!RefreshMembershipWorker.STATUS_IDLE.equals(getRefreshMembershipStatus())) {
			throw new RemoteException("A refresh of the ROADSIDE data in CARES is currently being done. No transactions are permitted until the ROADSIDE data has been extracted.");
		}

		ProgressCadTransactionProxy cadTransProxy = null;
		CADProgressProxy cadProxy = CadExportUtil.getCADProxy();
		GregorianCalendar asAtGC = new GregorianCalendar();
		asAtGC.setTime(asAt);

		try {
			cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();
			cadTransProxy.clearOldCad(asAtGC);
			cadTransProxy.commit();
		} catch (Exception e) {
			String errorMessage = CadExportUtil.getProxyErrorMessage(cadTransProxy, e);
			try {
				cadTransProxy.rollback();
			} catch (Exception e1) {
			}

			e.printStackTrace();
			throw new SystemException("Error in removeOldCADs: " + errorMessage);
		} finally {
			try {
				cadTransProxy._release();
				cadTransProxy = null;
			} catch (Exception ee) {
				// do nothing
			}
			CadExportUtil.releaseCADProxy(cadProxy);
		}
	}

	/**
	 * Refresh all Roadside membership data in CARES.
	 */
	public void refreshFromRoadside() throws RemoteException, ValidationException {
		if (this.refreshMembershipWorker != null) {
			// Check to see if a refresh is underway first.
			if (!RefreshMembershipWorker.STATUS_IDLE.equals(getRefreshMembershipStatus())) {
				throw new ValidationException("A refresh of the ROADSIDE data in CARES is currently being done. Another refresh is not permitted until the current one is finished.");
			}

			// Cause a new worker to be created
			this.refreshMembershipWorker = null;
		}
		RefreshMembershipWorker worker = getRefreshMembershipWorker();
		LogUtil.debug(this.getClass(), "refreshFromROADSIDE() : worker.start()");
		worker.start();
	}

	/**
	 * Reset the status of the worker to idle.
	 */
	public void resetWorker() {
		RefreshMembershipWorker worker = getRefreshMembershipWorker();
		worker.resetStatus();
	}

	public int getRefreshMembershipRecordCount() {
		RefreshMembershipWorker worker = getRefreshMembershipWorker();
		return worker.getRecordCount();
	}

	public String getRefreshMembershipStatus() {
		RefreshMembershipWorker worker = getRefreshMembershipWorker();
		return worker.getStatus();
	}

	public RemoteException getRefreshMembershipException() {
		RefreshMembershipWorker worker = getRefreshMembershipWorker();
		return worker.getException();
	}

	/**
	 * If the membership refresh worker is currently processing then send a signal to it to stop processing. Don't wait for its status to change.
	 */
	public void cancelMembershipRefresh() {
		RefreshMembershipWorker worker = getRefreshMembershipWorker();
		if (!RefreshMembershipWorker.STATUS_IDLE.equals(getRefreshMembershipStatus())) {
			worker.cancelRefresh();
		}
	}

	private static RefreshMembershipWorker refreshMembershipWorker = null;

	/**
	 * Return the worker that does the refresh of the membership data in CARES. Its a static reference so there should only be one for this instance of the VM.
	 */
	private RefreshMembershipWorker getRefreshMembershipWorker() {
		// Initialise a worker if this is the first call.
		if (this.refreshMembershipWorker == null) {
			this.refreshMembershipWorker = new RefreshMembershipWorker();
		}
		return this.refreshMembershipWorker;
	}

	/**
	 * Sets the sessionContext attribute of the CadExportMgrBean object
	 * 
	 * @param sessionContext The new sessionContext value
	 * @exception RemoteException Description of the Exception
	 */
	public void setSessionContext(SessionContext sessionContext) throws RemoteException {
		this.sessionContext = sessionContext;
		try {
			javax.naming.Context context = new javax.naming.InitialContext();
			try {
				dataSource = (DataSource) context.lookup(CommonConstants.DATASOURCE_CLIENT);
			} catch (Exception e) {
				throw new EJBException("Error looking up dataSource: " + e.toString());
			}
		} catch (Exception e) {
			throw new EJBException("Error initializing context:" + e.toString());
		}
	}

	private void audit(String message) {
		LogUtil.timeStamp(this.getClass(), message.trim());
	}

	public void nightlyRefreshMembership() throws RemoteException {
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();

		audit("nightlyRefreshMembership() : start ");

		audit("nightlyRefreshMembership() : clearing Cad");
		this.clearCad();

		audit("nightlyRefreshMembership() : refresh fleet");
		CadCounts cc = refreshFleet();

		StringBuffer sql = new StringBuffer();
		/*
		 * Some dates for this job
		 */
		int ExtractCutOffDays = cMgr.getCadExtractMembershipCutOff();
		int DeleteCutOffDays = cMgr.getCadDeleteFlagCutOff();

		Interval ExtractCutOffDaysInterval = null;
		DateTime ExtractCutOffDate = null;

		Interval DeleteCutOffDaysInterval = null;
		DateTime DeleteCutOffDate = null;

		try {
			ExtractCutOffDaysInterval = new Interval(0, 0, ExtractCutOffDays, 0, 0, 0, 0);
			DeleteCutOffDaysInterval = new Interval(0, 0, DeleteCutOffDays, 0, 0, 0, 0);
		} catch (Exception e2) {
			throw new RemoteException("nightlyRefreshMembership - unable to create NinetyDay Interval");
		}

		ExtractCutOffDate = new DateTime().subtract(ExtractCutOffDaysInterval);
		DeleteCutOffDate = new DateTime().subtract(DeleteCutOffDaysInterval);

		audit("nightlyRefreshMembership() : Extract Cutoff     " + ExtractCutOffDate.formatShortDate());
		audit("nightlyRefreshMembership() : Delete Flag Cutoff " + DeleteCutOffDate.formatShortDate());

		sql.append("SELECT mm.client_number,"); // 1
		sql.append("       cl.surname, "); // 2
		sql.append("       cl.[given-names] AS given_names, "); // 3
		sql.append("       cl.[client-title] AS client_title, "); // 4
		sql.append("       cl.[birth-date] AS birth_date, "); // 5
		sql.append("       cl.[home-phone] AS home_phone, "); // 6
		sql.append("       cl.[resi-street-char] AS street_number, "); // 7
		sql.append("       st.street, "); // 8
		sql.append("       cl.[resi-property] AS property, "); // 9
		sql.append("       st.suburb, "); // 10
		sql.append("       st.postcode, "); // 11
		sql.append("       mm.product_code, "); // 12
		sql.append("       mm.expiry_date, "); // 13
		sql.append("       mm.join_date, "); // 14
		sql.append("       mp.sort_order as productLevel, "); // 15
		/*
		 * Deleted flag set to Y if < today - 90 days
		 */
		sql.append("       case ");
		sql.append("           when mm.expiry_date < '" + DeleteCutOffDate.toSQLDate() + "' then 'Y' ");
		sql.append("           when mm.membership_status not in ('active','on hold') then 'Y' ");
		sql.append("           else 'N'  "); // 16
		sql.append("        end as deleted, ");
		sql.append("       'N', "); // 17
		sql.append("       '" + cc.getControlId() + "' as ControlId, ");
		sql.append("       '" + SourceSystem.MEMBERSHIP.getAbbreviation() + "' as MemType ");

		sql.append("FROM PUB.[cl-master] cl");
		sql.append("    INNER JOIN PUB.[gn-stsub] st ON cl.[resi-stsubid] = st.stsubid ");
		sql.append("    INNER JOIN PUB.mem_membership mm ON cl.[client-no] = mm.client_number ");
		sql.append("    INNER JOIN pub.mem_product mp on mp.product_code = mm.product_code ");

		sql.append("WHERE   mm.expiry_date >  '" + ExtractCutOffDate.toSQLDate() + "' ");

		audit("nightlyRefreshMembership() : SQL is " + sql.toString());

		audit("nightlyRefreshMembership() : refresh Membership");
		this.updateCADRecords(sql.toString());

		audit("nightlyRefreshMembership() : finish Data Set Build");
		this.finishCadDataSetBuild(cc.getControlId());

		audit(" nightlyRefreshMembership() : Exporting to MIDAS");
		this.exportToMidas(cc.getControlId());

		audit(" nightlyRefreshMembership() : clearing CAD");
		this.clearCad();

		audit(" nightlyRefreshMembership() : finished ");
	}

	private void exportToMidas(int controlID) throws RemoteException {
		CADProgressProxy cadProxy = null;
		ProgressCadTransactionProxy cadTransProxy = null;

		try {
			cadProxy = CadExportUtil.getCADProxy();
			cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();

			cadTransProxy.refreshMidas(controlID);

			cadTransProxy.commit();
		} catch (Exception e) {
			try {
				cadTransProxy.rollback();
			} catch (Exception e1) {
			}

			throw new RemoteException("Error in exportToMidas: " + e.getMessage());
		} finally {
			try {
				cadTransProxy._release();
				cadTransProxy = null;
			} catch (Exception ee) {
			}

			CadExportUtil.releaseCADProxy(cadProxy);
		}

	}

	private void clearCad() throws RemoteException {

		CADProgressProxy cadProxy = null;
		ProgressCadTransactionProxy cadTransProxy = null;

		try {
			// get the transaction proxy to the Progress CAD system
			cadProxy = CadExportUtil.getCADProxy();
			cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();
			/*
			 * Remove all the rows in the cad-export table
			 */
			cadTransProxy.clearCad();

			cadTransProxy.commit();

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = CadExportUtil.getProxyErrorMessage(cadTransProxy, e);
			try {
				cadTransProxy.rollback();
			} catch (Exception e1) {
			}

			throw new SystemException("Error in clearCAD: " + errorMessage, e);
		} finally {
			try {
				cadTransProxy._release();
				cadTransProxy = null;
			} catch (Exception ee) {
			}

			CadExportUtil.releaseCADProxy(cadProxy);
		}

	}

	private void finishCadDataSetBuild(int controlID) throws RemoteException {
		CADProgressProxy cadProxy = null;
		ProgressCadTransactionProxy cadTransProxy = null;

		try {
			cadProxy = CadExportUtil.getCADProxy();
			cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();

			cadTransProxy.endDataSetBuild(controlID);

			cadTransProxy.commit();
		} catch (Exception e) {
			try {
				cadTransProxy.rollback();
			} catch (Exception e1) {
			}

			throw new RemoteException("Error in finishCadDataSetBuild: " + e.getMessage());
		} finally {
			try {
				cadTransProxy._release();
				cadTransProxy = null;
			} catch (Exception ee) {
			}

			CadExportUtil.releaseCADProxy(cadProxy);
		}

	}

	private CadCounts refreshFleet() throws RemoteException {

		CadCounts cc = new CadCounts();
		IntHolder controlID = new IntHolder();
		IntHolder fleetCount = new IntHolder();

		CADProgressProxy cadProxy = null;
		ProgressCadTransactionProxy cadTransProxy = null;

		cc.setControlId(0);
		cc.setFleetCount(0);

		try {
			// get the transaction proxy to the Progress CAD system
			cadProxy = CadExportUtil.getCADProxy();
			cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();
			/*
			 * Start a new dataset build and get the control ID.
			 */
			cadTransProxy.startDataSetBuild(controlID);
			if (controlID.isNull()) {
				throw new SystemException("The control ID returned by the Progress CAD system is null.");
			}
			audit("nightlyRefreshMembership() : control id is " + new Integer(controlID.getIntValue()).toString());
			/*
			 * Add the Fleet Memberships
			 */
			audit("nightlyRefreshMembership() : starting fleet refresh");
			cadTransProxy.refreshMAPNVI(controlID.getIntValue(), fleetCount);

			cc.setControlId(controlID.getIntValue());
			cc.setFleetCount(fleetCount.getIntValue());
			audit("nightlyRefreshMembership() : fleet refreshed " + new Integer(fleetCount.getIntValue()).toString());

			cadTransProxy.commit();

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = CadExportUtil.getProxyErrorMessage(cadTransProxy, e);
			try {
				cadTransProxy.rollback();
			} catch (Exception e1) {
			}

			throw new SystemException("Error in refreshFromROADSIDE: " + errorMessage, e);
		} finally {
			try {
				cadTransProxy._release();
				cadTransProxy = null;
			} catch (Exception ee) {
			}

			CadExportUtil.releaseCADProxy(cadProxy);
		}
		return cc;
	}

	private class CadCounts {
		int controlId;
		int fleetCount;

		public CadCounts() {
			this.setControlId(0);
			this.setFleetCount(0);
		}

		public int getControlId() {
			return controlId;
		}

		public void setControlId(int controlId) {
			this.controlId = controlId;
		}

		public int getFleetCount() {
			return fleetCount;
		}

		public void setFleetCount(int fleetCount) {
			this.fleetCount = fleetCount;
		}
	}

	/*
	 * ========================================================================== ====
	 */

	private class RefreshMembershipWorker extends Thread {
		public static final String STATUS_IDLE = "Idle";

		public static final String STATUS_STARTING_BUILD = "Starting build";

		public static final String STATUS_PROCESSING = "Processing";

		public static final String STATUS_ENDING_BUILD = "Ending build";

		public static final String SIGNAL_CANCEL = "Cancel";

		private String status = STATUS_IDLE;

		private RemoteException exception;

		private String signal;

		private int recordCount = 0;

		public RefreshMembershipWorker() {
			this.signal = null;
		}

		public void run() {
			this.signal = null;
			try {
				refreshMembership();
			} catch (RemoteException re) {
				this.exception = re;
			}
		}

		public String getStatus() {
			return this.status;
		}

		public RemoteException getException() {
			return this.exception;
		}

		public int getRecordCount() {
			return this.recordCount;
		}

		public void cancelRefresh() {
			this.signal = this.SIGNAL_CANCEL;
		}

		public void resetStatus() {
			this.status = this.STATUS_IDLE;
		}

		private void refreshMembership() throws RemoteException {
			LogUtil.debug(this.getClass(), "refreshMembership() : start");
			this.status = this.STATUS_STARTING_BUILD;
			StringBuffer sql = new StringBuffer();

			sql.append("SELECT PUB.\"cl-master\".surname, ");
			sql.append("       PUB.\"cl-master\".\"given-names\" AS given_names, ");
			sql.append("       PUB.\"cl-master\".\"client-title\" AS client_title, ");
			sql.append("       PUB.\"cl-master\".\"home-phone\" AS home_phone, ");
			sql.append("       PUB.\"cl-master\".\"birth-date\" AS birth_date, ");
			sql.append("       PUB.\"cl-master\".\"resi-property\" AS property, ");
			sql.append("       PUB.\"cl-master\".\"resi-street-char\" AS street_number, ");
			sql.append("       PUB.\"gn-stsub\".street, ");
			sql.append("       PUB.\"gn-stsub\".suburb, ");
			sql.append("       PUB.\"gn-stsub\".postcode, ");
			sql.append("       PUB.mem_membership.membership_id, ");
			sql.append("       PUB.mem_membership.membership_number, ");
			sql.append("       PUB.mem_membership.membership_type_code, ");
			sql.append("       PUB.mem_membership.membership_status, ");
			sql.append("       PUB.mem_membership.join_date, ");
			sql.append("       PUB.mem_membership.expiry_date, ");
			sql.append("       PUB.mem_membership.product_code, ");
			sql.append("       PUB.mem_membership.product_date ");
			sql.append("FROM PUB.\"cl-master\" INNER JOIN PUB.\"gn-stsub\" ON PUB.\"cl-master\".\"resi-stsubid\" = PUB.\"gn-stsub\".stsubid ");
			sql.append("INNER JOIN PUB.mem_membership ON PUB.\"cl-master\".\"client-no\" = PUB.mem_membership.client_number ");

			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			CADProgressProxy cadProxy = null;
			ProgressCadTransactionProxy cadTransProxy = null;
			Connection connection = null;
			PreparedStatement statement = null;
			try {
				// get the transaction proxy to the Progress CAD system
				cadProxy = CadExportUtil.getCADProxy();
				cadTransProxy = cadProxy.createPO_ProgressCadTransactionProxy();

				// Start a new dataset build and get the control ID.
				IntHolder controlID = new IntHolder();
				cadTransProxy.startDataSetBuild(controlID);
				if (controlID.isNull()) {
					throw new SystemException("Failed to start a new dataset build.  The control ID returned by the Progress CAD system is null.");
				}

				GregorianCalendar createDate = DateUtil.clearTime(new GregorianCalendar());
				int createTime = new DateTime().getSecondsFromMidnight();
				GregorianCalendar effectiveDate = new GregorianCalendar();
				String addressLine1 = null;
				String addressLine2 = null;
				int memberYears = 0;
				GregorianCalendar gc = new GregorianCalendar();
				Hashtable productList = CadExportUtil.getProductList(dataSource);
				String productCode;
				String plusFlag;
				String deletedFlag;
				java.sql.Date productEffectiveDate;
				java.util.Date today = new java.util.Date();
				MembershipVO memVO;

				// Query all of the membership data
				connection = dataSource.getConnection();
				statement = connection.prepareStatement(sql.toString());
				ResultSet rs = statement.executeQuery();
				this.recordCount = 0;
				LogUtil.debug(this.getClass(), "Refreshing CARES: started at " + new DateTime().formatLongDate());
				this.status = this.STATUS_PROCESSING;
				try {
					while (rs.next()) {
						this.recordCount++;
						if (this.signal != null) {
							throw new CancelException();
						}

						// LogUtil.debug(this.getClass(),"Record " + recCount);

						addressLine1 = CadExportUtil.getAddressLine1(rs.getString("property"), rs.getString("street_number"), rs.getString("street"));
						addressLine2 = CadExportUtil.getAddressLine2(rs.getString("suburb"), rs.getInt("postcode"));

						// Show cancelled or undone memberships as deleted on
						// CARES
						deletedFlag = (rs.getString("membership_status").equals(MembershipVO.STATUS_CANCELLED) || rs.getString("membership_status").equals(MembershipVO.STATUS_UNDONE) ? "Y" : "N");

						// Get the corresponding product number to the product
						// code from the product list.
						// Use this to determine the plus flag.
						// If the product is not yet effective then lookup the
						// current product from the memberships history
						productEffectiveDate = rs.getDate("product_date");
						productCode = rs.getString("product_code");
						if (productEffectiveDate.after(today)) {
							LogUtil.debug(this.getClass(), "refreshMembership() : Fetching current product code for membership " + rs.getInt("membership_id") + ". Effective date for product " + productCode + " was " + productEffectiveDate);
							// This is a bit of an overhead but hopefully there
							// should not be too many to fetch like this
							memVO = membershipMgr.getMembership(new Integer(rs.getInt("membership_id")));
							productCode = memVO.getCurrentProductCode();
							if (productCode == null) {
								// So error occurred. Use the one we have.
								productCode = rs.getString("product_code");
							}
						}

						if (productList.containsKey(productCode)) {
							plusFlag = (((Integer) productList.get(productCode)).intValue() == 0 ? "+" : "");
						} else {
							// If the membership has an unknown product then
							// show as deleted on CARES
							plusFlag = "";
							deletedFlag = "Y";
						}

						// Format the membership years as <expiry date
						// year><join date year> in the format YYYYYYYY.
						gc.setTime(rs.getDate("expiry_date"));
						memberYears = gc.get(GregorianCalendar.YEAR) * 10000;
						gc.setTime(rs.getDate("join_date"));
						memberYears += gc.get(GregorianCalendar.YEAR);

						cadTransProxy.notifyCares(
						// Export type is first three characters of membership
						// type. e.g. PER
						rs.getString("membership_type_code").substring(0, 3).toUpperCase(), Integer.parseInt(rs.getString("membership_number")), rs.getString("surname").trim().toUpperCase(), rs.getString("given_names").trim().toUpperCase(), rs.getString("client_title").trim().toUpperCase(), rs.getString("property").trim().toUpperCase(), DateUtil.formatShortDate(rs.getDate("join_date")),
						// member type is the first three charaters of
						// the product code. e.g. ULT, ADV
						productCode.substring(0, 3).toUpperCase(), DateUtil.formatShortDate(rs.getDate("birth_date")), DateUtil.formatShortDate(rs.getDate("expiry_date")),
						// plus commence date not used
						"", memberYears, deletedFlag,
						// regno not used
						"",
						// mapno not used
						"", plusFlag, (rs.getString("membership_status").equals(MembershipVO.STATUS_ONHOLD) ? "Y" : ""),
						// Only set the remove flag if the membership
						// was undone after it was created
						(rs.getString("membership_status").equals(MembershipVO.STATUS_UNDONE) ? "Y" : ""), "", "", rs.getString("home_phone").trim(),
						// No plus expiry date
						"", addressLine1,
						// No cad number
						"", addressLine2, createDate, "", createTime,
						// No service level
						"",
						// No sequence number for ROADSIDE
						"", effectiveDate, controlID.getIntValue());

						cadTransProxy.commit();
					}
				} catch (CancelException ce) {
					LogUtil.warn(this.getClass(), "Cancelling CAD refresh at " + new DateTime().formatLongDate() + " with " + this.recordCount + " records sent.");
				}

				LogUtil.debug(this.getClass(), "Finished CAD refresh at " + new DateTime().formatLongDate() + " with " + this.recordCount + " records sent.");
				this.status = this.STATUS_ENDING_BUILD;
				cadTransProxy.endDataSetBuild(controlID.getIntValue());
				cadTransProxy.commit();
			} catch (Exception e) {
				String errorMessage = CadExportUtil.getProxyErrorMessage(cadTransProxy, e);
				try {
					cadTransProxy.rollback();
				} catch (Exception e1) {
				}

				throw new SystemException("Error in refreshFromROADSIDE: " + errorMessage, e);
			} finally {
				this.status = this.STATUS_IDLE;
				ConnectionUtil.closeConnection(connection, statement);
				try {
					cadTransProxy._release();
					cadTransProxy = null;
				} catch (Exception ee) {
				}
				CadExportUtil.releaseCADProxy(cadProxy);
			}
		}

		/**
		 * This private class is used to signal a cancelation of the refresh processing
		 */
		private class CancelException extends Exception {
			public CancelException() {
			}
		}

	}
}

class CadExportUtil {

	public static CADProgressProxy getCADProxy() throws RemoteException {
		CADProgressProxy cadProxy = null;
		try {
			cadProxy = (CADProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_CAD_PROXY);
		} catch (Exception ex) {
			throw new SystemException("Failed to get object.", ex);
		}
		return cadProxy;
	}

	public static void releaseCADProxy(ProgressProxy cadProxy) throws RemoteException {
		try {
			ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_CAD_PROXY, cadProxy);
		} catch (Exception ex) {
			throw new SystemException("Failed to return object.", ex);
		}
		cadProxy = null;
	}

	public static String getProxyErrorMessage(ProgressCadTransactionProxy proxy, Exception e) {
		String errorMessage = null;
		if (e instanceof Open4GLException) {
			try {
				if (proxy != null) {
					errorMessage = "Progress error : " + proxy._getProcReturnString();
				}
			} catch (Exception e1) {
			}
		}
		if (errorMessage == null || errorMessage.length() < 0) {
			errorMessage = e.getMessage();
		}
		return errorMessage;
	}

	public static String getProxyErrorMessage(CADProgressProxy proxy, Exception e) {
		String errorMessage = null;
		if (e instanceof Open4GLException) {
			try {
				if (proxy != null) {
					errorMessage = "Progress error : " + proxy._getProcReturnString();
				}
			} catch (Exception e1) {
			}
		}
		if (errorMessage == null || errorMessage.length() < 0) {
			errorMessage = e.getMessage();
		}
		return errorMessage;
	}

	/**
	 * retrieve product types, Names, and service levels Returns a Hashtable of product sort levels keyed with the product_code This allows easy determination of whether a membership is "+" from the product code, irrespective of the name given to the product.
	 * 
	 * @return The productList value
	 * @exception RemoteException Description of the Exception
	 */
	public static Hashtable getProductList(DataSource dataSource) throws RemoteException {
		Hashtable productTable = new Hashtable();
		String sql = "select product_code, sort_order from PUB.mem_product where notifiable = 1";
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = dataSource.getConnection();
			st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				productTable.put(rs.getString(1), new Integer(rs.getInt(2)));
			}
		} catch (Exception ex) {
			throw new RemoteException("Error getting product list in CadExportMgrBean.getProductList \n" + ex);
		} finally {
			ConnectionUtil.closeConnection(conn, st);
		}
		return productTable;
	}

	private static final int MAX_ADDRESS_LINE_LENGTH = 29;

	/**
	 * Return a formatted string for the first address line using the street number, street and property values. Limit the length of the returned string to 29 characters
	 */
	public static String getAddressLine1(String property, String streetNumber, String street) {
		StringBuffer addressLine1 = new StringBuffer();
		if (property != null) {
			addressLine1.append(property.trim());
			addressLine1.append(" ");
		}

		if (streetNumber != null) {
			addressLine1.append(streetNumber.trim());
			addressLine1.append(" ");
		}

		if (street != null) {
			addressLine1.append(street.trim());
		}

		// Add the property to the address line if there is room left and there
		// is something in the property. Use as much of the property string as
		// will fit into the remaining space leaving room for two brackets.
		// if(addressLine1.length() < MAX_ADDRESS_LINE_LENGTH - 2 && property !=
		// null
		// && property.length() > 0)
		// {
		// // Get the amount of room left in the address line
		// int len = MAX_ADDRESS_LINE_LENGTH - 2 - addressLine1.length();
		// // Limit the length of the property string to use by the length of
		// the property string
		// len = len > property.length() ? property.length() : len;
		// String propertySubstr = property.substring(0, len);
		//
		// if(propertySubstr != null && propertySubstr.length() > 0)
		// {
		// addressLine1.append("(");
		// addressLine1.append(propertySubstr);
		// addressLine1.append(")");
		// }
		// }
		if (addressLine1.length() > MAX_ADDRESS_LINE_LENGTH) {
			return addressLine1.toString().substring(0, MAX_ADDRESS_LINE_LENGTH).toUpperCase();
		} else {
			return StringUtil.replaceAll(addressLine1.toString().toUpperCase(), "  ", " ");
		}
	}

	/**
	 * Return the second address line formatted using the suburb and postcode. Limit the size of the returned string to 29 characters
	 */
	public static String getAddressLine2(String suburb, int postcode) {
		StringBuffer addressLine2 = new StringBuffer();
		if (suburb != null && suburb.trim().length() > 0) {
			addressLine2.append(suburb.trim());
		}
		if (postcode != 0) {
			if (addressLine2.length() > 0) {
				addressLine2.append(" ");
			}
			addressLine2.append(postcode);
		}
		if (addressLine2.length() > MAX_ADDRESS_LINE_LENGTH) {
			return addressLine2.toString().substring(0, MAX_ADDRESS_LINE_LENGTH).toUpperCase();
		} else {
			return addressLine2.toString().toUpperCase();
		}
	}

	/**
	 * get the years between two dates
	 * 
	 * @param effectiveDate Description of the Parameter
	 * @param joinDate Description of the Parameter
	 * @return The membershipYears value
	 * @todo move to a generic function NOT in this class.
	 */
	public static int getMembershipYears(DateTime effectiveDate, DateTime joinDate) {
		int years = 0;
		if (joinDate != null && effectiveDate != null) {
			if (joinDate.onOrBeforeDay(effectiveDate)) {
				years = DateUtil.getYears(joinDate, effectiveDate) + 1;
			}
		}
		return years;
	}

}
