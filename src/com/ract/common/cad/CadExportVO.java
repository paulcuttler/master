package com.ract.common.cad;

import com.ract.common.ValueObject;
import com.ract.util.DateTime;

/**
 * CAD Export value object.
 * 
 * @author dgk
 * @created 1 August 2002
 * @version 1.0
 */

public class CadExportVO extends ValueObject {
    /**
     * The type of road service. Valid values: MEM NVI MAP CLT (?)
     */
    private String exportType = "";

    /**
     * Description of the Field
     */
    private Integer clientNo;

    /**
     * Description of the Field
     */
    private String surname = "";

    /**
     * Description of the Field
     */
    private String givenNames = "";

    /**
     * Description of the Field
     */
    private String clientTitle = "";

    /**
     * Description of the Field
     */
    private String property = "";

    /**
     * Date the client first became a member
     */
    private String joinDate = "";

    /**
     * Description of the Field
     */
    private String memberType = "";

    /**
     * Description of the Field
     */
    private String birthDate = "";

    /**
     * Description of the Field
     */
    private String expiryDate = "";

    /**
     * Description of the Field
     */
    private String plusCommenceDate = "";

    /**
     * Membership years in the form of concatenated last and first year eg
     * 20021989
     */
    private Integer membershipYears = null;

    /**
     * Description of the Field
     */
    private String deleted = "";

    /**
     * Description of the Field
     */
    private String regNo = "";

    /**
     * Description of the Field
     */
    private String mapCode = "";

    /**
     * Description of the Field
     */
    private String plusFlag = "";

    /**
     * Description of the Field
     */
    private String membershipHold = "";

    /**
     * Description of the Field
     */
    private String removeRecord = "";

    /**
     * Description of the Field
     */
    private String manModel = "";

    /**
     * Description of the Field
     */
    private String manCover = "";

    /**
     * Description of the Field
     */
    private String homePhone = "";

    /**
     * Description of the Field
     */
    private String plusExpiry = "";

    /**
     * Description of the Field
     */
    private String address = "";

    /**
     * Description of the Field
     */
    private String cadNumber = "";

    /**
     * Description of the Field
     */
    private String address2 = "";

    /**
     * Description of the Field
     */
    private Integer tranNo;

    /**
     * Description of the Field
     */
    private DateTime createDateTime;

    /**
     * Description of the Field
     */
    private String mapNo = "";

    /**
     * Description of the Field
     */
    private String serviceLevel = "";

    /**
     * Description of the Field
     */
    private String seqNo = "";

    /**
     * Effective Date: The date on which the related change becomes effective Do
     * not move this change to the CARES system before this date
     */
    private DateTime effectiveDate = null;

    private int controlID;

    /**
     * Constructor for the CadExportVO object
     */
    public CadExportVO() {
    }

    public DateTime getEffectiveDate() {
	return effectiveDate;
    }

    public void setEffectiveDate(DateTime effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    /**
     * Gets the exportType attribute of the CadExportVO object
     * 
     * @return The exportType value
     */
    public String getExportType() {
	return exportType;
    }

    /**
     * Sets the exportType attribute of the CadExportVO object
     * 
     * @param exportType
     *            The new exportType value
     */
    public void setExportType(String exportType) {
	this.exportType = exportType;
    }

    /**
     * Gets the clientNo attribute of the CadExportVO object
     * 
     * @return The clientNo value
     */
    public Integer getClientNo() {
	return clientNo;
    }

    /**
     * Sets the clientNo attribute of the CadExportVO object
     * 
     * @param clientNo
     *            The new clientNo value
     */
    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    /**
     * Gets the surname attribute of the CadExportVO object
     * 
     * @return The surname value
     */
    public String getSurname() {
	return this.surname;
    }

    /**
     * Sets the surname attribute of the CadExportVO object
     * 
     * @param surname
     *            The new surname value
     */
    public void setSurname(String surname) {
	this.surname = surname;
    }

    /**
     * Gets the givenNames attribute of the CadExportVO object
     * 
     * @return The givenNames value
     */
    public String getGivenNames() {
	return this.givenNames;
    }

    /**
     * Sets the givenNames attribute of the CadExportVO object
     * 
     * @param givenNames
     *            The new givenNames value
     */
    public void setGivenNames(String givenNames) {
	this.givenNames = givenNames;
    }

    /**
     * Gets the clientTitle attribute of the CadExportVO object
     * 
     * @return The clientTitle value
     */
    public String getClientTitle() {
	return this.clientTitle;
    }

    /**
     * Sets the clientTitle attribute of the CadExportVO object
     * 
     * @param clientTitle
     *            The new clientTitle value
     */
    public void setClientTitle(String clientTitle) {
	this.clientTitle = clientTitle;
    }

    /**
     * Gets the property attribute of the CadExportVO object
     * 
     * @return The property value
     */
    public String getProperty() {
	return this.property;
    }

    /**
     * Sets the property attribute of the CadExportVO object
     * 
     * @param property
     *            The new property value
     */
    public void setProperty(String property) {
	this.property = property;
    }

    /**
     * Gets the streetChar attribute of the CadExportVO object
     * 
     * @return The streetChar value
     */
    public String getJoinDate() {
	return this.joinDate;
    }

    /**
     * Sets the joinDate attribute of the CadExportVO object
     * 
     * @param String
     *            /DateTime joinDate The new joinDate value
     */
    public void setJoinDate(String joinDate) {
	this.joinDate = joinDate;
    }

    public void setJoinDate(DateTime joinDate) {
	this.joinDate = joinDate.formatShortDate();
    }

    /**
     * Gets the memberType attribute of the CadExportVO object
     * 
     * @return The memberType value
     */
    public String getMemberType() {
	return this.memberType;
    }

    /**
     * Sets the memberType attribute of the CadExportVO object
     * 
     * @param memberType
     *            The new memberType value
     */
    public void setMemberType(String memberType) {
	this.memberType = memberType;
    }

    /**
     * Gets the address attribute of the CadExportVO object
     * 
     * @return The address value
     */
    public String getAddress() {
	return address;
    }

    /**
     * Gets the address2 attribute of the CadExportVO object
     * 
     * @return The address2 value
     */
    public String getAddress2() {
	return address2;
    }

    /**
     * Gets the birthDate attribute of the CadExportVO object
     * 
     * @return The birthDate value
     */
    public String getBirthDate() {
	return birthDate;
    }

    /**
     * Gets the cadNumber attribute of the CadExportVO object
     * 
     * @return The cadNumber value
     */
    public String getCadNumber() {
	return cadNumber;
    }

    /**
     * Gets the createDateTime attribute of the CadExportVO object
     * 
     * @return The createDateTime value
     */
    public DateTime getCreateDateTime() {
	return createDateTime;
    }

    /**
     * Sets the createDateTime attribute of the CadExportVO object
     * 
     * @param createDateTime
     *            The new createDateTime value
     */
    public void setCreateDateTime(DateTime createDateTime) {
	this.createDateTime = createDateTime;
    }

    /**
     * Sets the cadNumber attribute of the CadExportVO object
     * 
     * @param cadNumber
     *            The new cadNumber value
     */
    public void setCadNumber(String cadNumber) {
	this.cadNumber = cadNumber;
    }

    /**
     * Sets the birthDate attribute of the CadExportVO object
     * 
     * @param birthDate
     *            The new birthDate value
     */
    public void setBirthDate(String birthDate) {
	this.birthDate = birthDate;
    }

    /**
     * Sets the address2 attribute of the CadExportVO object
     * 
     * @param address2
     *            The new address2 value
     */
    public void setAddress2(String address2) {
	this.address2 = address2;
    }

    /**
     * Sets the address attribute of the CadExportVO object
     * 
     * @param address
     *            The new address value
     */
    public void setAddress(String address) {
	this.address = address;
    }

    /**
     * Gets the deleted attribute of the CadExportVO object
     * 
     * @return The deleted value
     */
    public String getDeleted() {
	return deleted;
    }

    /**
     * Sets the deleted attribute of the CadExportVO object
     * 
     * @param deleted
     *            The new deleted value
     */
    public void setDeleted(String deleted) {
	this.deleted = deleted;
    }

    /**
     * Sets the expiryDate attribute of the CadExportVO object
     * 
     * @param expiryDate
     *            The new expiryDate value
     */
    public void setExpiryDate(String expiryDate) {
	this.expiryDate = expiryDate;
    }

    /**
     * Gets the expiryDate attribute of the CadExportVO object
     * 
     * @return The expiryDate value
     */
    public String getExpiryDate() {
	return expiryDate;
    }

    /**
     * Gets the homePhone attribute of the CadExportVO object
     * 
     * @return The homePhone value
     */
    public String getHomePhone() {
	return homePhone;
    }

    /**
     * Sets the homePhone attribute of the CadExportVO object
     * 
     * @param homePhone
     *            The new homePhone value
     */
    public void setHomePhone(String homePhone) {
	this.homePhone = homePhone;
    }

    /**
     * Gets the manCover attribute of the CadExportVO object
     * 
     * @return The manCover value
     */
    public String getManCover() {
	return manCover;
    }

    /**
     * Sets the manCover attribute of the CadExportVO object
     * 
     * @param manCover
     *            The new manCover value
     */
    public void setManCover(String manCover) {
	this.manCover = manCover;
    }

    /**
     * Gets the manModel attribute of the CadExportVO object
     * 
     * @return The manModel value
     */
    public String getManModel() {
	return manModel;
    }

    /**
     * Sets the manModel attribute of the CadExportVO object
     * 
     * @param manModel
     *            The new manModel value
     */
    public void setManModel(String manModel) {
	this.manModel = manModel;
    }

    /**
     * Gets the mapCode attribute of the CadExportVO object
     * 
     * @return The mapCode value
     */
    public String getMapCode() {
	return mapCode;
    }

    /**
     * Sets the mapCode attribute of the CadExportVO object
     * 
     * @param mapCode
     *            The new mapCode value
     */
    public void setMapCode(String mapCode) {
	this.mapCode = mapCode;
    }

    /**
     * Gets the mapNo attribute of the CadExportVO object
     * 
     * @return The mapNo value
     */
    public String getMapNo() {
	return mapNo;
    }

    /**
     * Sets the mapNo attribute of the CadExportVO object
     * 
     * @param mapNo
     *            The new mapNo value
     */
    public void setMapNo(String mapNo) {
	this.mapNo = mapNo;
    }

    /**
     * Sets the membershipHold attribute of the CadExportVO object
     * 
     * @param membershipHold
     *            The new membershipHold value
     */
    public void setMembershipHold(String membershipHold) {
	this.membershipHold = membershipHold;
    }

    /**
     * Gets the membershipHold attribute of the CadExportVO object
     * 
     * @return The membershipHold value
     */
    public String getMembershipHold() {
	return membershipHold;
    }

    /**
     * Gets the membershipYears attribute of the CadExportVO object
     * 
     * @return The membershipYears value
     */
    public Integer getMembershipYears() {
	return membershipYears;
    }

    /**
     * Sets the membershipYears attribute of the CadExportVO object
     * 
     * @param membershipYears
     *            The new membershipYears value
     */
    public void setMembershipYears(Integer membershipYears) {
	this.membershipYears = membershipYears;
    }

    /**
     * Sets the membershipYears attribute of the CadExportVO object
     * 
     * @param membershipYears
     *            The new membershipYears value
     */
    public void setMembershipYears(int membershipYears) {
	this.membershipYears = new Integer(membershipYears);
    }

    public void setMembershipYears(DateTime join, DateTime expire) {
	this.membershipYears = new Integer(expire.getDateYear() * 10000 + join.getDateYear());
    }

    /**
     * Gets the plusCommenceDate attribute of the CadExportVO object
     * 
     * @return The plusCommenceDate value
     */
    public String getPlusCommenceDate() {
	return plusCommenceDate;
    }

    /**
     * Sets the plusCommenceDate attribute of the CadExportVO object
     * 
     * @param plusCommenceDate
     *            The new plusCommenceDate value
     */
    public void setPlusCommenceDate(String plusCommenceDate) {
	this.plusCommenceDate = plusCommenceDate;
    }

    /**
     * Gets the plusExpiry attribute of the CadExportVO object
     * 
     * @return The plusExpiry value
     */
    public String getPlusExpiry() {
	return plusExpiry;
    }

    /**
     * Sets the plusExpiry attribute of the CadExportVO object
     * 
     * @param plusExpiry
     *            The new plusExpiry value
     */
    public void setPlusExpiry(String plusExpiry) {
	this.plusExpiry = plusExpiry;
    }

    /**
     * Gets the plusFlag attribute of the CadExportVO object
     * 
     * @return The plusFlag value
     */
    public String getPlusFlag() {
	return plusFlag;
    }

    /**
     * Sets the plusFlag attribute of the CadExportVO object
     * 
     * @param plusFlag
     *            The new plusFlag value
     */
    public void setPlusFlag(String plusFlag) {
	this.plusFlag = plusFlag;
    }

    /**
     * Gets the regNo attribute of the CadExportVO object
     * 
     * @return The regNo value
     */
    public String getRegNo() {
	return regNo;
    }

    /**
     * Sets the regNo attribute of the CadExportVO object
     * 
     * @param regNo
     *            The new regNo value
     */
    public void setRegNo(String regNo) {
	this.regNo = regNo;
    }

    /**
     * Gets the removeRecord attribute of the CadExportVO object
     * 
     * @return The removeRecord value
     */
    public String getRemoveRecord() {
	return removeRecord;
    }

    /**
     * Sets the removeRecord attribute of the CadExportVO object
     * 
     * @param removeRecord
     *            The new removeRecord value
     */
    public void setRemoveRecord(String removeRecord) {
	this.removeRecord = removeRecord;
    }

    /**
     * Gets the seqNo attribute of the CadExportVO object
     * 
     * @return The seqNo value
     */
    public String getSeqNo() {
	return seqNo;
    }

    /**
     * Sets the seqNo attribute of the CadExportVO object
     * 
     * @param seqNo
     *            The new seqNo value
     */
    public void setSeqNo(String seqNo) {
	this.seqNo = seqNo;
    }

    /**
     * Gets the serviceLevel attribute of the CadExportVO object
     * 
     * @return The serviceLevel value
     */
    public String getServiceLevel() {
	return serviceLevel;
    }

    /**
     * Sets the serviceLevel attribute of the CadExportVO object
     * 
     * @param serviceLevel
     *            The new serviceLevel value
     */
    public void setServiceLevel(String serviceLevel) {
	this.serviceLevel = serviceLevel;
    }

    /**
     * Gets the tranNo attribute of the CadExportVO object
     * 
     * @return The tranNo value
     */
    public Integer getTranNo() {
	return tranNo;
    }

    /**
     * Sets the tranNo attribute of the CadExportVO object
     * 
     * @param tranNo
     *            The new tranNo value
     */
    public void setTranNo(Integer tranNo) {
	this.tranNo = tranNo;
    }

    public int getControlID() {
	return controlID;
    }

    public void setControlID(int controlID) {
	this.controlID = controlID;
    }

    public String toString() {
	StringBuffer str = new StringBuffer();
	str.append("address=");
	str.append(address);
	str.append(", address2=");
	str.append(address2);
	str.append(", birthDate=");
	str.append(birthDate);
	str.append(", cadNumber=");
	str.append(cadNumber);
	str.append(", clientNo=");
	str.append(clientNo);
	str.append(", clientTitle=");
	str.append(clientTitle);
	str.append(", controlID=");
	str.append(controlID);
	str.append(", createDateTime=");
	str.append(createDateTime);
	str.append(", deleted=");
	str.append(deleted);
	str.append(", effectiveDate=");
	str.append(effectiveDate);
	str.append(", expiryDate=");
	str.append(expiryDate);
	str.append(", exportType=");
	str.append(exportType);
	str.append(", givenNames=");
	str.append(givenNames);
	str.append(", homePhone=");
	str.append(homePhone);
	str.append(", joinDate=");
	str.append(joinDate);
	str.append(", manCover=");
	str.append(manCover);
	str.append(", manModel=");
	str.append(manModel);
	str.append(", mapCode=");
	str.append(mapCode);
	str.append(", mapNo=");
	str.append(mapNo);
	str.append(", membershipHold=");
	str.append(membershipHold);
	str.append(", membershipYears=");
	str.append(membershipYears);
	str.append(", memberType=");
	str.append(memberType);
	str.append(", plusCommenceDate=");
	str.append(plusCommenceDate);
	str.append(", plusExpiry=");
	str.append(plusExpiry);
	str.append(", plusFlag=");
	str.append(plusFlag);
	str.append(", property=");
	str.append(property);
	str.append(", regNo=");
	str.append(regNo);
	str.append(", removeRecord=");
	str.append(removeRecord);
	str.append(", seqNo=");
	str.append(seqNo);
	str.append(", serviceLevel=");
	str.append(serviceLevel);
	str.append(", surname=");
	str.append(surname);
	str.append(", tranNo=");
	str.append(tranNo);

	return str.toString();
    }

}
