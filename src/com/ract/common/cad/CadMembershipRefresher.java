package com.ract.common.cad;

import java.util.Date;

import com.ract.common.CommonEJBHelper;
import com.ract.common.integration.RACTPropertiesProvider;

/**
 * Refresh Midas Personal Membership Fleet Membserhisp
 * 
 * @author RLG
 * @created 24 November 2008
 * @version 1.0
 */

public class CadMembershipRefresher {

    private CadMembershipRefresher(String host, String port) {

	CadExportMgr cadExportMgr = null;
	try {
	    System.out.println(" ======================================================== ");
	    System.out.println(" Host                 " + host);
	    System.out.println(" Port                 " + port);

	    RACTPropertiesProvider.setContextURL(host, Integer.parseInt(port));
	    cadExportMgr = CommonEJBHelper.getCadExportMgr();
	} catch (Exception ex) {
	    ex.printStackTrace();
	    System.out.println("Failed initializing access to CadExportMgr home interface: " + ex.getMessage());

	}

	try {

	    System.out.println(" Starting             " + new Date().toString());
	    cadExportMgr.nightlyRefreshMembership();

	    System.out.println(" Extract Finished " + new Date().toString());
	    System.out.println(" ======================================================== ");

	} catch (Exception e) {
	    System.out.println("CadMembershipRefresher +++++++ Exception " + e.getMessage());
	    System.out.println("CadMembershipRefresher +++++++ FINISHING");
	}

    }

    /**
     * The main program for the CadMembershipRefresher class
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args) {

	if (args.length < 2) {
	    System.out.println("Not enough command line arguments.");
	    System.out.println("Usage: CadMembershipRefresher [localhost] [1099] ");
	    return;
	}

	CadMembershipRefresher CMR = new CadMembershipRefresher(args[0], args[1]);
    }

}
