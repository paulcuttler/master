package com.ract.common.cad;

import com.ract.common.CommonEJBHelper;
import com.ract.util.DateTime;

/**
 * Title: CadCompare Description: Routine for notifying CARES of personal
 * memberships which have exceeded their expiry date by three months. This class
 * has a main method and must be run daily, as only memberships expiring on the
 * previous day are collected. (This could become a problem, but there is no way
 * to flag whether or not a membership has already been removed.) It is expected
 * to be run by a UNIX CRON task
 * 
 * Also deletes old cad-export records (Question: Why aren't they deleted as
 * soon as they are uploaded to CARES?) Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class CadCompare {
    private static String HOST = null;

    public CadCompare(String host, String action) {
	try {
	    // jnp host/port no longer required. Require a jndi.properties file
	    // in the classpath
	    CadExportMgr cadMgr = CommonEJBHelper.getCadExportMgr();
	    System.out.println("Compare memberships started......" + new DateTime().formatLongDate());
	    cadMgr.compareRAAwithRACT(action);

	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    public static void main(String[] args) {
	if (args.length != 2) {
	    System.out.println("Not enough command line arguments. Found " + args.length + " need 2 ");
	    System.out.println("Usage: CadCompare [localhost:1099]  [action LOAD|EXTRACT|ALL] ");

	    return;
	}

	HOST = args[0];

	System.out.println("Run commenced: " + (new DateTime()).formatLongDate() + " Action is " + args[1]);
	new CadCompare(HOST, args[1]);
	System.out.println("Run Completed: " + (new DateTime()).formatLongDate());

    }

}
