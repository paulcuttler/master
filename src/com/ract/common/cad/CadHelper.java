package com.ract.common.cad;

import java.rmi.RemoteException;
import java.text.ParseException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * <p>
 * Utility cad methods.
 * </p>
 * 
 * @author rlg
 * @version 1.0
 */
public class CadHelper {

	public static void scheduleCadMembershipRefreshJob(String cronExpression) throws RemoteException {
		JobDetail jobDetail = new JobDetail("CadMembershipRefreshJob", null, CadMembershipRefreshJob.class);

		jobDetail.setDescription("Refresh Membership for MIDAS");

		// schedule the job
		CronTrigger trigger = null;
		try {
			trigger = new CronTrigger("CadMembershipRefreshTrigger", null, cronExpression);
		} catch (ParseException ex2) {
			throw new RemoteException("Unable to determine schedule.", ex2);
		}

		trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);

		try {
			ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
			scheduleMgr.scheduleJob(jobDetail, trigger);
		} catch (SchedulerException ex) {
			throw new RemoteException("Unable to schedule job.", ex);
		}

	}

	public static String formatMembershipNumber(Integer clientNo, String schemeCode, int seqNo) {
		StringBuilder builder = new StringBuilder();

		builder.append(StringUtil.padNumber(clientNo, 6));
		builder.append(schemeCode);
		builder.append(StringUtil.padNumber(new Integer(seqNo), 4));

		return builder.toString();
	}

	public static String formatClientCode(Integer clientNo, Integer seqNo) {
		StringBuilder builder = new StringBuilder();

		builder.append(StringUtil.padNumber(clientNo, 6));
		builder.append(StringUtil.padNumber(seqNo, 4));

		return builder.toString();
	}

	public static DateTime getCadDeleteCutOffDate() {
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		int DeleteCutOffDays = 90;
		try {
			DeleteCutOffDays = cMgr.getCadDeleteFlagCutOff();
		} catch (RemoteException e) {
			// do nothing
		}

		return calcCutOffDate(DeleteCutOffDays);
	}

	public static DateTime getCadExtractMembershipCutOff() {
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		int DeleteCutOffDays = 1460;
		try {
			DeleteCutOffDays = cMgr.getCadExtractMembershipCutOff();
		} catch (RemoteException e) {
			// do nothing
		}

		return calcCutOffDate(DeleteCutOffDays);
	}

	private static DateTime calcCutOffDate(int daysPrior) {
		Interval cutOffDaysInterval = null;

		try {

			cutOffDaysInterval = new Interval(0, 0, daysPrior, 0, 0, 0, 0);
		} catch (Exception e2) {
			LogUtil.log("calcCutOffDate", "Unable to create interval");
		}

		return new DateTime().subtract(cutOffDaysInterval);

	}
}
