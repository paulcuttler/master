package com.ract.common.cad.raa;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.SortedSet;

import com.ract.common.CommonEJBHelper;
import com.ract.common.admin.AdminCodeDescription;
import com.ract.common.mail.MailMessage;
import com.ract.util.LogUtil;

/**
 * RaaRactRunner
 * 
 * @author rlg
 * @created 28 September 2004
 * @version 1.0
 */

public class RaaRactRunner {

    /**
     * Get all the records from GN-TABLE with a tab-name of theseExtracts
     * 
     * @param action
     *            String
     * @param theseExtracts
     *            String
     * @throws RemoteException
     */
    public void runTheExtracts(String action, String theseExtracts) throws RemoteException {

	SortedSet extracts = CommonEJBHelper.getCommonMgr().getAllExtracts(theseExtracts);

	Iterator it = extracts.iterator();

	while (it.hasNext()) {
	    AdminCodeDescription acd = (AdminCodeDescription) it.next();
	    runThisExtract(action, acd.getCode(), acd.getDescription());
	}
    }

    /**
     * Get the parameters for the extract from the GN-TABLE with a tab-name
     * extractName One of these parameters is called RUN_KEY, the value of this
     * is compared with the parameter ACTION If there is a match then
     * extractName is executed
     * 
     * @param action
     *            String
     * @param extractName
     *            String
     * @param description
     *            String
     * @throws RemoteException
     */
    private void runThisExtract(String action, String extractName, String description) throws RemoteException {
	Hashtable extractParams = CommonEJBHelper.getCommonMgr().getExtractParameters(extractName);

	String runKeys = (String) extractParams.get("RUN_KEY");
	LogUtil.log(this.getClass(), "Extract " + extractName + " runkeys " + runKeys + " - " + action);

	if (isSpecifiedToRun(action, runKeys)) {
	    LogUtil.log(this.getClass(), "Running " + extractName);
	    runExtract(extractParams, extractName, description);
	}
    }

    /**
     * Compares the value of action to runKeys and returns true if there is a
     * match
     * 
     * @param action
     *            String
     * @param runKeys
     *            String
     * @return boolean
     */
    private boolean isSpecifiedToRun(String action, String runKeys) {
	boolean okToRun = false;

	if (runKeys == null) {
	    return okToRun;
	}
	// StringTokenizer lineTokens = new StringTokenizer(runKeys, ",");
	String lineTokens[] = runKeys.split(",");
	for (int i = 0; i < lineTokens.length && okToRun == false; i++) {
	    String thisKey = lineTokens[i];
	    if (action.indexOf(thisKey) > -1) {
		okToRun = true;
	    }
	}

	return okToRun;
    }

    /**
     * Execute the extractName
     * 
     * There are two parameters PackageName and ClassName - these are combined
     * to get the class to run. If ClassName is not specified the the
     * PackageName and extractName are combined.
     * 
     * @param extractParams
     *            Hashtable
     * @param extractName
     *            String
     * @param description
     *            String
     * @throws RemoteException
     */
    private void runExtract(Hashtable extractParams, String extractName, String description) throws RemoteException {

	// find the class to run
	String packageName = (String) extractParams.get("PackageName");
	String className = (String) extractParams.get("ClassName");
	String runClassName = null;

	if (className == null) {
	    runClassName = packageName + "." + extractName;
	} else {
	    runClassName = packageName + "." + className;
	}

	RaaRactBase runRaaRact = null;
	try {
	    runRaaRact = (RaaRactBase) Class.forName(runClassName).newInstance();
	} catch (Exception ex) {
	    throw new RemoteException(ex.getMessage());
	}

	Hashtable resultSet = runRaaRact.doExecute(extractParams);

	// mail the results
	String resultStr = resultSet.get("extractTo") + ". Records " + resultSet.get("recordsExtracted");

	String mailTo = (String) extractParams.get("EMAIL");
	if (mailTo == null) {
	    mailTo = "l.giles@ract.com.au";
	}

	MailMessage message = new MailMessage();
	message.setRecipient(mailTo);
	message.setSubject(description.trim());
	message.setMessage(resultStr);

	CommonEJBHelper.getMailMgr().sendMail(message);
    }

    public RaaRactRunner() {
    }
}
