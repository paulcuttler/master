package com.ract.common.cad.raa;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Hashtable;

import javax.sql.DataSource;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.DataSourceFactory;
import com.ract.common.extracts.ExtractHelper;
import com.ract.util.ConnectionUtil;
import com.ract.util.LogUtil;

/**
 * This uses RaaTOCadExtract to populate the CADEXPORT table. It runs at times
 * specified by the RUN_AT parameter and sends BATCH_SIZE records at a time
 * 
 * @author rlg
 * @version 1.0
 */

public class RaaDataSender extends RaaRactBase {

    // private SessionContext sessionContext;

    private DataSource dataSource = null;

    public RaaDataSender() {

    }

    public static void main(String[] args) {
	RaaDataSender sendUpdatesToRAA1 = new RaaDataSender();
    }

    public Hashtable doExecute(Hashtable params) throws RemoteException {
	this.setParameterList(params);
	Connection connection = null;

	// PreparedStatement statement = null;

	Hashtable paramList = this.getParameterList();
	String rt = (String) paramList.get("RUN_AT");

	RunTimer runTimes = new RunTimer(rt);

	String sqlCmdFile = (String) paramList.get("BASE_DIR") + (String) paramList.get("SQL_DIR") + (String) paramList.get("SQL_FILE");

	int batchSize = new Integer((String) paramList.get("BATCH_SIZE")).intValue();

	boolean keepGoing = true;

	while (keepGoing == true) {
	    int recsLeft = populateCadExport(sqlCmdFile, batchSize);
	    if (recsLeft < 1) {
		keepGoing = false;
	    } else {
		runTimes.nextRun();
	    }
	}

	return new Hashtable();

    }

    private int populateCadExport(String sqlCmdFile, int batchSize) {

	String rowNum = null;
	final String sql = "select min(row_Num) from pub.Raa_To_Cad_Extract ";

	Connection connection = null;
	int minRowNum = 0;
	Statement statement = null;

	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    connection = dataSource.getConnection();

	    statement = connection.createStatement();
	    ResultSet rs = statement.executeQuery(sql);

	    if (rs.next()) {
		minRowNum = new Integer(rs.getString(1)).intValue();
	    }
	    // close the resultset
	    rs.close();
	    if (minRowNum > 0) {
		LogUtil.log(this.getClass(), "Processing rows " + minRowNum + " to " + (minRowNum + batchSize));
		rowNum = new Integer(minRowNum + batchSize).toString();
		LogUtil.log(this.getClass(), "Processing rows " + minRowNum + " to " + (minRowNum + batchSize) + "/" + rowNum);

		this.sendToCad(sqlCmdFile, rowNum);

		final String delSql = "delete from pub.Raa_To_Cad_Extract where row_Num <= " + rowNum; // use
												       // prepared
												       // statements
												       // and
												       // set
												       // attributes
		// statement = connection.prepareStatement(delSql);
		int rd = statement.executeUpdate(delSql);
		LogUtil.log(this.getClass(), rd + "/" + delSql);
	    }

	} catch (Exception e) {
	    LogUtil.log(this.getClass(), e.getMessage());
	    minRowNum = 0;
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return countRemainingRows();
    }

    private int countRemainingRows() {

	int remainingRows = 0;

	final String sql = "select count(*) from pub.Raa_To_Cad_Extract ";

	Connection connection = null;
	Statement statement = null;

	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    connection = dataSource.getConnection();
	    // Only prepare if the statement has attributes and is not final
	    // statement = connection.prepareStatement(sql);
	    statement = connection.createStatement();
	    ResultSet rs = statement.executeQuery(sql);
	    if (rs.next()) {
		remainingRows = new Integer(rs.getString(1)).intValue();
	    }
	} catch (Exception e) {
	    LogUtil.log(this.getClass(), e.getMessage());
	    remainingRows = 0;
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return remainingRows;
    }

    private void sendToCad(String sqlCmdFile, String rowNum) throws RemoteException {
	HashMap sqlParams = new HashMap();
	sqlParams.put("1", rowNum);
	String sql = ExtractHelper.getSQLStatement(sqlCmdFile, sqlParams, ExtractHelper.getSuffixDateTime());
	LogUtil.log(this.getClass(), "sendToCad 1");
	CommonEJBHelper.getCadExportMgr().sendClientsToCAD(sql);
	LogUtil.log(this.getClass(), "sendToCad 2");
    }

}
