package com.ract.common.cad.raa;

import java.io.BufferedReader;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Hashtable;

import javax.ejb.EJBException;
import javax.sql.DataSource;

import com.ract.common.CommonConstants;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * 
 * Loads the extract files from the RAA into a SQL server table called
 * RACTMemDump
 * 
 * @author rlg
 * @version 1.0
 */

public class RaaDataLoader extends RaaRactBase {

    private DataSource dataSource = null;
    private Hashtable parameterList = new Hashtable();

    public RaaDataLoader() {
	try {
	    javax.naming.Context context = new javax.naming.InitialContext();
	    try {
		dataSource = (DataSource) context.lookup(CommonConstants.DATASOURCE_CLIENT);
	    } catch (Exception e) {
		throw new EJBException("Error looking up dataSource: " + e.toString());
	    }
	} catch (Exception e) {
	    throw new EJBException("Error initializing context:" + e.toString());
	}

    }

    public void setParameterList(Hashtable parameterList) {
	this.parameterList = parameterList;
    }

    public Hashtable getParameterList() {
	return this.parameterList;
    }

    public Hashtable doExecute(Hashtable paramList) throws RemoteException {
	this.setParameterList(paramList);
	return doExecute();
    }

    public Hashtable doExecute() throws RemoteException {
	LogUtil.log(this.getClass(), "Loading  RAA Compare Table (RACTMemDump) " + new DateTime().formatLongDate());

	String inputFile = (String) parameterList.get("INPUT_FILE");
	Hashtable results = new Hashtable();

	BufferedReader in = null;
	String printLine = null;
	StringBuffer insertPart = new StringBuffer("insert into pub.RACT_Mem_Dump ( ");

	char tabChar = '\t';
	String tabStr = "\t";

	Connection connection = null;
	Statement statement = null;
	int i = 0;

	try {
	    in = new BufferedReader(new FileReader(inputFile));

	    String line = "";
	    // the first line has the column names in
	    line = in.readLine();
	    insertPart.append(line.replace(tabChar, ','));
	    insertPart.append(" ) values ( ");

	    int st = 0;
	    connection = dataSource.getConnection();
	    // was defined per line
	    StringBuffer sql = null;
	    while ((line = in.readLine()) != null) {
		sql = new StringBuffer();

		sql.append(insertPart.toString());
		sql.append("'");
		// date column handling may not be platform independent
		sql.append(StringUtil.replaceAll(StringUtil.replaceAll(line, "'", "''"), tabStr, "','"));
		sql.append("')");

		/**
		 * Note: use of a PreparedStatement in this context will cache
		 * all statements (permanently using the jtds driver
		 * implementation) as every SQL statement is different. The
		 * ideal solution is to use a generic SQL statement and
		 * substitute the parameters ( for complete database
		 * independence) or use a simple Statement to avoid caching.
		 */
		statement = connection.createStatement(); // was
							  // connection.prepareStatement(sql.toString());
		try {
		    st = statement.executeUpdate(sql.toString());
		} catch (Exception e) {
		    LogUtil.warn(this.getClass(), e.getMessage() + sql.toString());
		}

		i++;
		if ((i % 10000) == 0) {
		    LogUtil.log(this.getClass(), "RaaDataLoader ++++ " + i + " processed " + new DateTime().formatLongDate());
		}
		// close statement per loop
		ConnectionUtil.closeStatement(statement);
		sql.setLength(0);
		sql = null;

	    }
	    LogUtil.log(this.getClass(), "RaaDataLoader  ++++ " + i + " records loaded. " + new DateTime().formatLongDate());
	    in.close();
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new RemoteException("Unable to load the RAA data.", e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	results.put("extractTo", inputFile);
	results.put("recordsExtracted", new Integer(i).toString());

	return results;

    }

    public static void main(String[] args) {
	RaaDataLoader loadRACTMemDump1 = new RaaDataLoader();
    }

}
