package com.ract.common.cad.raa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

/**
 * Gets a string of numbers separated by commas (minute of the hour to run) . It
 * finds the one closest to the current minute of the hour It works out how long
 * it has to wait, waits and the returns.
 * 
 * @author not attributable
 * @version 1.0
 */
public class RunTimer {
    ArrayList runTimes = new ArrayList();
    private int tCnt;

    public RunTimer() {
    }

    public RunTimer(String times) {
	// StringTokenizer lineTokens = new StringTokenizer(times, ",");
	String lineTokens[] = times.split(",");
	tCnt = 0;

	for (int i = 0; i < lineTokens.length; i++) {
	    runTimes.add(tCnt, new Integer(lineTokens[i]));
	    tCnt++;
	}

    }

    public void nextRun() {

	int mm = Calendar.getInstance().get(Calendar.MINUTE);

	Iterator it = runTimes.iterator();
	int runTime = 0;
	int firstTime = 0;

	while (it.hasNext()) {
	    int nextTime = ((Integer) it.next()).intValue();

	    if (firstTime == 0) {
		firstTime = nextTime;
	    }
	    if (mm < nextTime && runTime == 0) {
		runTime = nextTime;
	    }
	}

	if (runTime == 0) {
	    runTime = 60 + firstTime;
	}

	long delay = (long) ((runTime - mm) * 60 * 1000);

	try {
	    java.lang.Thread.sleep(delay);
	} catch (Exception e) {

	}
    }

    public static void main(String[] args) {
	RunTimer runTimer1 = new RunTimer();
    }

}
