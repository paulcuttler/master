package com.ract.common.cad.raa;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Hashtable;

import javax.sql.DataSource;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.DataSourceFactory;
import com.ract.common.SystemException;
import com.ract.common.extracts.ExtractHelper;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * The base class for the RAA RACT compare. It would probably be better in the
 * extracts package
 * 
 * It executes SQL that produces no output, xml or csv output. It also puts the
 * output into a table called RaaToCadExtract. The rows from this table are used
 * to populate the CADEXPORT table (in progress).
 * 
 * @author Leigh Giles
 * @version 1.0
 */

public class RaaRactBase {

    private DataSource dataSource = null;

    private Hashtable parameterList = new Hashtable();

    public RaaRactBase() {

    }

    public RaaRactBase(Hashtable paramList) {

	this.setParameterList(paramList);
    }

    public void setParameterList(Hashtable parameterList) {
	this.parameterList = parameterList;
    }

    public Hashtable getParameterList() {
	return this.parameterList;
    }

    public Hashtable doExecute(Hashtable paramList) throws RemoteException {
	this.setParameterList(paramList);
	return doExecute();
    }

    public Hashtable doExecute() throws RemoteException {
	String suffixDateTime = ExtractHelper.getSuffixDateTime();

	String extractFile = (String) parameterList.get("BASE_DIR") + (String) parameterList.get("EXT_DIR") + (String) parameterList.get("EXT_FILE");

	String sqlCmdFile = (String) parameterList.get("BASE_DIR") + (String) parameterList.get("SQL_DIR") + (String) parameterList.get("SQL_FILE");

	HashMap sqlParams = (HashMap) parameterList.get("sqlParams");

	String extractType = (String) parameterList.get("EXT_FILE_TYPE");
	if (extractType == null) {
	    extractType = "none";
	}

	String sqlCmd = ExtractHelper.getSQLStatement(sqlCmdFile, sqlParams, suffixDateTime);

	String extractTo = null;

	// find out where to put the extract
	if (!extractType.equalsIgnoreCase("none")) {
	    extractTo = ExtractHelper.getExtractFilename(extractFile, suffixDateTime, "." + extractType);
	}

	Hashtable results = new Hashtable();
	String recs = null;
	String tmp = null;

	if (extractType.equalsIgnoreCase("xml")) {
	    tmp = CommonEJBHelper.getReportMgr().getXMLResultSet(sqlCmd, CommonConstants.DATASOURCE_CLIENT);

	    try {
		FileUtil.writeFile(extractTo, tmp);
	    } catch (IOException ex) {
		throw new SystemException("Unable to write extract file " + extractTo + ". " + ex.getMessage());
	    }

	    // extract the number of records from the returned string
	    int i = tmp.lastIndexOf("row id=") + 8;
	    recs = tmp.substring(i);
	    i = recs.indexOf("\"");
	    recs = recs.substring(0, i);
	} else if (extractType.equalsIgnoreCase("csv")) {
	    recs = CommonEJBHelper.getReportMgr().getCSVResultSet(sqlCmd, extractTo);
	} else if (extractType.equalsIgnoreCase("none")) {
	    /**
	     * @todo this allows insert, update and deletes via external sql.
	     *       This is a security risk. eg. delete from mem_membership.
	     */
	    CommonEJBHelper.getExtractMgr().executeSQL(sqlCmd);
	    extractTo = "SQL completed ";
	    recs = " completed ";
	}

	results.put("extractTo", extractTo);
	results.put("recordsExtracted", recs);

	if (((String) parameterList.get("RESULTS_TO_CAD")) != null) {
	    loadToRaaToCadExtract(extractTo);
	}

	return results;
    }

    private void loadToRaaToCadExtract(String inputFile) throws RemoteException {
	LogUtil.log(this.getClass(), "Loading  RaaToCadExtract " + inputFile + " " + new DateTime().formatLongDate());

	BufferedReader in = null;
	// String printLine = null;
	StringBuffer insertPart = new StringBuffer("insert into pub.Raa_To_Cad_Extract ( row_Num, client_No, remove_From_Raa, delete_From_Raa) values ( ");

	Connection connection = null;
	Statement statement = null;

	ResultSet rs = null;

	String line = null;
	int i = 0;
	int st = 0;
	int rowNum = 0;

	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    in = new BufferedReader(new FileReader(inputFile));

	    connection = dataSource.getConnection();

	    String mSql = "select max(row_Num) from pub.Raa_To_Cad_Extract";
	    statement = connection.createStatement(); // prepareStatement(mSql);
	    try {
		rs = statement.executeQuery(mSql);
		if (rs.next()) { // only one result
		    line = rs.getString(1);
		}
		rowNum = new Integer(line).intValue();

	    } catch (Exception e) {
		LogUtil.log(this.getClass(), "Exception " + e.getMessage() + mSql);
		rowNum = 0;
	    }

	    LogUtil.log(this.getClass(), "Starting load");
	    // ignore the first line
	    line = in.readLine();

	    while ((line = in.readLine()) != null) {
		rowNum++;

		StringBuffer sql = new StringBuffer();
		String tk[] = line.split(",");
		sql.append(insertPart.toString());
		sql.append(rowNum);
		sql.append(",");
		sql.append("'");
		sql.append(tk[0]);
		sql.append("','");
		sql.append(tk[1]);
		sql.append("','");
		sql.append(tk[2]);

		sql.append("')");

		// statement = connection.prepareStatement();
		statement = connection.createStatement();
		try {
		    st = statement.executeUpdate(StringUtil.replaceAll(sql.toString(), "\"", ""));
		} catch (Exception e) {
		    LogUtil.log(this.getClass(), e.getMessage() + sql.toString());
		}

		i++;
		if ((i % 10000) == 0) {
		    LogUtil.log(this.getClass(), "RAACompare RaaToCadExtract ++++ " + i + " processed " + new DateTime().formatLongDate());
		}

	    }

	    LogUtil.log(this.getClass(), "RAACompare RaaToCadExtract ++++ " + i + " records loaded. " + new DateTime().formatLongDate());
	    in.close();
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new RemoteException(e.getMessage());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

    }

}
