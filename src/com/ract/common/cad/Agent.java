package com.ract.common.cad;

import com.ract.util.FileUtil;

/**
 * Represent a roadside agent
 * 
 * @hibernate.class table="rs_agent" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class Agent implements java.io.Serializable {

    private int priority;

    private String agentName;

    private String contact1Name;

    private String contact1Phone;

    private String contact2Name;

    private String contact2Phone;

    private String businessPhone;

    private String mobilePhone;

    private boolean roadside;

    private boolean towing;

    private String towingType;

    private String notes;

    private String homePhone;

    private String preferredContactMethod;

    /**
     * @hibernate.property
     * @hibernate.column name="notes"
     */
    public String getNotes() {
	return notes;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="towing_type"
     */
    public String getTowingType() {
	return towingType;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="towing"
     */
    public boolean isTowing() {
	return towing;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="roadside"
     */
    public boolean isRoadside() {
	return roadside;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="mobile_phone"
     */
    public String getMobilePhone() {
	return mobilePhone;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="business_phone"
     */
    public String getBusinessPhone() {
	return businessPhone;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="contact_2_phone"
     */
    public String getContact2Phone() {
	return contact2Phone;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="contact_2_name"
     */
    public String getContact2Name() {
	return contact2Name;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="contact_1_phone"
     */
    public String getContact1Phone() {
	return contact1Phone;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="contact_1_name"
     */
    public String getContact1Name() {
	return contact1Name;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="agent_name"
     */
    public String getAgentName() {
	return agentName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="priority"
     */
    public int getPriority() {
	return priority;
    }

    public void setNotes(String notes) {
	this.notes = notes;
    }

    public void setTowingType(String towingType) {
	this.towingType = towingType;
    }

    public void setTowing(boolean towing) {
	this.towing = towing;
    }

    public void setRoadside(boolean roadside) {
	this.roadside = roadside;
    }

    public void setMobilePhone(String mobilePhone) {
	this.mobilePhone = mobilePhone;
    }

    public void setBusinessPhone(String businessPhone) {
	this.businessPhone = businessPhone;
    }

    public void setContact2Phone(String contact2Phone) {
	this.contact2Phone = contact2Phone;
    }

    public void setContact2Name(String contact2Name) {
	this.contact2Name = contact2Name;
    }

    public void setContact1Phone(String contact1Phone) {
	this.contact1Phone = contact1Phone;
    }

    public void setContact1Name(String contact1Name) {
	this.contact1Name = contact1Name;
    }

    public void setAgentName(String agentName) {
	this.agentName = agentName;
    }

    public void setPriority(int priority) {
	this.priority = priority;
    }

    public void setHomePhone(String homePhone) {
	this.homePhone = homePhone;
    }

    public void setPreferredContactMethod(String preferredContactMethod) {
	this.preferredContactMethod = preferredContactMethod;
    }

    public void setAgentPK(AgentPK agentPK) {
	this.agentPK = agentPK;
    }

    public void setBattery(boolean battery) {
	this.battery = battery;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="home_phone"
     */
    public String getHomePhone() {
	return homePhone;
    }

    public static final String CONTACT_METHOD_BUSINESS = "BUS";

    public static final String CONTACT_METHOD_HOME = "HOME";

    public static final String CONTACT_METHOD_MOBILE = "MOB";

    private AgentPK agentPK;

    private boolean battery;

    /**
     * @hibernate.property
     * @hibernate.column name="preferred_contact_method"
     */
    public String getPreferredContactMethod() {
	return preferredContactMethod;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public AgentPK getAgentPK() {
	return agentPK;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="battery"
     */
    public boolean isBattery() {
	return battery;
    }

    public Agent() {
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(agentName);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(agentPK.getAgentId());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(agentPK.getLocation());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(businessPhone);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(contact1Name);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(contact1Phone);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(contact2Name);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(contact2Phone);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(homePhone);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(mobilePhone);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(notes);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(preferredContactMethod);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(notes);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(priority);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(roadside);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(towing);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(towingType);

	return desc.toString();

    }

}
