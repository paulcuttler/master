package com.ract.common.cad.fleet;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.AddressVO;
import com.ract.common.cad.CadHelper;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

public class FleetClientData implements Serializable {
    /**
     * 
     */

    private String clientNo;

    private String surname;
    private String givenNames;
    private String title;
    private DateTime birthDate;

    private String workPhone;
    private String mobilePhone;
    private String homePhone;

    private String resiProperty;
    private String resiStreetChar;
    private String resiStreet;
    private String resiSuburb;
    private String resiPostCode;

    private String postProperty;
    private String postStreetChar;
    private String postStreet;
    private String postSuburb;
    private String postPostCode;

    private String clientXML = null;

    private String nullString = null;

    /**
     * @param clientNo
     */
    public FleetClientData(Integer clientNo) {
	this.setClientNo(clientNo);

	ClientVO client = getClient(clientNo);

	if (client == null) {
	    this.setAttributesToNull();
	} else {
	    this.setAttributes(client);
	}

	this.clientXML = this.createClientXML(nullString);
    }

    private void setAttributes(ClientVO client) {

	this.setSurname(client.getSurname());
	this.setGivenNames(client.getGivenNames());
	this.setBirthDate(client.getBirthDate());
	this.setTitle(client.getTitle());

	this.setHomePhone(client.getHomePhone());
	this.setWorkPhone(client.getWorkPhone());
	this.setMobilePhone(client.getMobilePhone());

	this.setResiProperty(client.getResiProperty());
	this.setResiStreetChar(client.getResiStreetChar());

	try {
	    AddressVO address = client.getResidentialAddress();

	    this.setResiStreet(address.getStreet());
	    this.setResiSuburb(address.getSuburb());
	    this.setResiPostCode(address.getPostcode());
	} catch (RemoteException e) {
	    this.setResiStreet(null);
	    this.setResiSuburb(null);
	    this.setResiPostCode(null);
	}

	this.setPostProperty(client.getPostProperty());
	this.setPostStreetChar(client.getPostStreetChar());

	try {
	    AddressVO address = client.getPostalAddress();

	    this.setPostStreet(address.getStreet());
	    this.setPostSuburb(address.getSuburb());
	    this.setPostPostCode(address.getPostcode());
	} catch (RemoteException e) {
	    this.setPostStreet(null);
	    this.setPostSuburb(null);
	    this.setPostPostCode(null);
	}

    }

    private void setAttributesToNull() {
	this.surname = null;
	this.givenNames = null;
	this.title = null;
	this.birthDate = null;
	this.workPhone = null;
	this.mobilePhone = null;
	this.homePhone = null;
	this.resiProperty = null;
	this.resiStreetChar = null;
	this.resiStreet = null;
	this.resiSuburb = null;
	this.resiPostCode = null;
	this.postProperty = null;
	this.postStreetChar = null;
	this.postStreet = null;
	this.postSuburb = null;
	this.postPostCode = null;

    }

    private ClientVO getClient(Integer clientNo) {

	ClientVO clientVO = null;
	if (clientNo != null) {
	    try {
		ClientMgr cMgr = ClientEJBHelper.getClientMgr();
		clientVO = cMgr.getClient(clientNo);
	    } catch (Exception e) {
		LogUtil.log(this.getClass(), "Unable to retrieve client details for " + clientNo.toString());
	    }
	}

	return clientVO;
    }

    public String getClientNo() {
	return clientNo;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo.toString();
    }

    public String getSurname() {
	return surname;
    }

    public void setSurname(String surname) {
	this.surname = surname;
    }

    public String getGivenNames() {
	return givenNames;
    }

    public void setGivenNames(String givenNames) {
	this.givenNames = givenNames;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public DateTime getBirthDate() {
	return birthDate;
    }

    public void setBirthDate(DateTime birthDate) {
	try {
	    this.birthDate = birthDate;
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public String getWorkPhone() {
	return workPhone;
    }

    public void setWorkPhone(String workPhone) {
	this.workPhone = workPhone;
    }

    public String getMobilePhone() {
	return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
	this.mobilePhone = mobilePhone;
    }

    public String getHomePhone() {
	return homePhone;
    }

    public void setHomePhone(String homePhone) {
	this.homePhone = homePhone;
    }

    public String getResiProperty() {
	return resiProperty;
    }

    public void setResiProperty(String resiProperty) {
	this.resiProperty = resiProperty;
    }

    public String getResiStreetChar() {
	return resiStreetChar;
    }

    public void setResiStreetChar(String resiStreetChar) {
	this.resiStreetChar = resiStreetChar;
    }

    public String getResiStreet() {
	return resiStreet;
    }

    public void setResiStreet(String resiStreet) {
	this.resiStreet = resiStreet;
    }

    public String getResiSuburb() {
	return resiSuburb;
    }

    public void setResiSuburb(String resiSuburb) {
	this.resiSuburb = resiSuburb;
    }

    public String getResiPostCode() {
	return resiPostCode;
    }

    public void setResiPostCode(String resiPostCode) {
	this.resiPostCode = resiPostCode;
    }

    public String getPostProperty() {
	return postProperty;
    }

    public void setPostProperty(String postProperty) {
	this.postProperty = postProperty;
    }

    public String getPostStreetChar() {
	return postStreetChar;
    }

    public void setPostStreetChar(String postStreetChar) {
	this.postStreetChar = postStreetChar;
    }

    public String getPostStreet() {
	return postStreet;
    }

    public void setPostStreet(String postStreet) {
	this.postStreet = postStreet;
    }

    public String getPostSuburb() {
	return postSuburb;
    }

    public void setPostSuburb(String postSuburb) {
	this.postSuburb = postSuburb;
    }

    public String getPostPostCode() {
	return postPostCode;
    }

    public void setPostPostCode(String postPostCode) {
	this.postPostCode = postPostCode;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("FleetClientData [birthDate=");
	builder.append(birthDate);
	builder.append(", clientNo=");
	builder.append(clientNo);
	builder.append(", givenNames=");
	builder.append(givenNames);
	builder.append(", homePhone=");
	builder.append(homePhone);
	builder.append(", mobilePhone=");
	builder.append(mobilePhone);
	builder.append(", postPostCode=");
	builder.append(postPostCode);
	builder.append(", postProperty=");
	builder.append(postProperty);
	builder.append(", postStreet=");
	builder.append(postStreet);
	builder.append(", postStreetChar=");
	builder.append(postStreetChar);
	builder.append(", postSuburb=");
	builder.append(postSuburb);
	builder.append(", resiPostCode=");
	builder.append(resiPostCode);
	builder.append(", resiProperty=");
	builder.append(resiProperty);
	builder.append(", resiStreet=");
	builder.append(resiStreet);
	builder.append(", resiStreetChar=");
	builder.append(resiStreetChar);
	builder.append(", resiSuburb=");
	builder.append(resiSuburb);
	builder.append(", surname=");
	builder.append(surname);
	builder.append(", title=");
	builder.append(title);
	builder.append(", workPhone=");
	builder.append(workPhone);
	builder.append("]");
	return builder.toString();
    }

    public String createClientXML(int seqNo) {

	return this.createClientXML(CadHelper.formatClientCode(new Integer(this.getClientNo()), new Integer(seqNo)));
    }

    private String createClientXML(String clientCode) {
	StringBuilder builder = new StringBuilder();

	builder.append("<client>");

	if (clientCode == null) {
	    builder.append(XMLHelper.xmlElement("clientcode", this.getClientNo()));
	} else {
	    builder.append(XMLHelper.xmlElement("clientcode", clientCode, 10));
	}
	try {
	    builder.append(XMLHelper.xmlElement("dateofbirth", this.getBirthDate().formatShortDate(), 15));
	} catch (NullPointerException n) {
	    /* Do nothing - not required in XML */
	} catch (Exception e) {
	    e.printStackTrace();
	}

	builder.append(XMLHelper.optionalXMLElement("surname", this.getSurname(), 50));
	builder.append(XMLHelper.optionalXMLElement("givennames", this.getGivenNames(), 50));

	builder.append(XMLHelper.optionalXMLElement("title", this.getTitle(), 20));

	builder.append(createAddressXML("residential", this.getResiProperty(), this.getResiStreetChar(), this.getResiStreet(), this.getResiSuburb(), this.getResiPostCode()));

	builder.append(createAddressXML("postal", this.getPostProperty(), this.getPostStreetChar(), this.getPostStreet(), this.getPostSuburb(), this.getPostPostCode()));

	builder.append(createPhoneXML());

	builder.append("</client>");

	return builder.toString();
    }

    private String createPhoneXML() {
	StringBuilder builder = new StringBuilder();

	builder.append("<phonenumbers>");

	builder.append(addPhoneXML("HOME", this.getHomePhone()));
	builder.append(addPhoneXML("WORK", this.getWorkPhone()));
	builder.append(addPhoneXML("MOBILE", this.getMobilePhone()));

	builder.append("</phonenumbers>");

	return builder.toString();
    }

    private String addPhoneXML(String phoneType, String phoneNumber) {
	StringBuilder phoneXML = new StringBuilder();

	if (StringUtil.isNull(phoneNumber) || phoneNumber.equalsIgnoreCase("na")) {
	    return "";
	} else {
	    phoneXML.append("<phoneDetails>");
	    phoneXML.append(XMLHelper.optionalXMLElement("phoneType", phoneType, 10));
	    phoneXML.append(XMLHelper.optionalXMLElement("phoneNumber", phoneNumber, 20));
	    phoneXML.append("</phoneDetails>");

	    return phoneXML.toString();

	}
    }

    private String createAddressXML(String addressType, String property, String streetChar, String streetName, String suburbName, String postCode) {
	StringBuilder builder = new StringBuilder();

	StringBuilder addr2 = new StringBuilder();

	try {
	    addr2.append(streetChar.trim());
	    addr2.append(" ");
	} catch (NullPointerException n) {
	    /* do nothing */
	} catch (Exception e) {

	    e.printStackTrace();
	}

	try {
	    addr2.append(streetName.trim());
	} catch (NullPointerException n) {
	    /* do nothing */
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	builder.append(XMLHelper.optionalXMLElement(addressType + "Address1", property, 50));
	builder.append(XMLHelper.optionalXMLElement(addressType + "Address2", addr2.toString(), 50));
	builder.append(XMLHelper.optionalXMLElement(addressType + "Suburb", suburbName, 50));
	builder.append(XMLHelper.optionalXMLElement(addressType + "Postcode", postCode, 5));

	return builder.toString();
    }

    public String getClientXML() {
	if (clientXML == null) {

	    clientXML = this.createClientXML(nullString);
	}

	return clientXML;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	// TODO Auto-generated method stub

    }

}
