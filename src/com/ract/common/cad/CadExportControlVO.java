package com.ract.common.cad;

/**
 * This class holds the CAD export control data from the CAD export system.
 * It is only intended for querying the data and not changing it  which is why
 * all of the data must be passed to the constructor and there are not setter
 * methods.
 */

import com.ract.common.ValueObject;
import com.ract.util.DateTime;

public class CadExportControlVO extends ValueObject implements Comparable {
    private int controlID = 0;

    private DateTime startDate;

    private DateTime endDate;

    private DateTime extractDate;

    public CadExportControlVO(int controlID, DateTime startDate, DateTime endDate, DateTime extractDate) {
	this.controlID = controlID;
	this.startDate = startDate;
	this.endDate = endDate;
	this.extractDate = extractDate;
	this.setReadOnly(true);
    }

    public int getControlID() {
	return controlID;
    }

    public DateTime getEndDate() {
	return endDate;
    }

    public DateTime getExtractDate() {
	return extractDate;
    }

    public DateTime getStartDate() {
	return startDate;
    }

    public int compareTo(Object obj) throws ClassCastException {
	if (obj instanceof CadExportControlVO) {
	    CadExportControlVO that = (CadExportControlVO) obj;
	    if (this.startDate != null && that.getStartDate() != null) {
		if (this.startDate.getTime() < that.getStartDate().getTime()) {
		    return -1;
		} else if (this.startDate.getTime() > that.getStartDate().getTime()) {
		    return 1;
		} else {
		    return 0;
		}
	    } else {
		return 0;
	    }
	} else {
	    throw new ClassCastException("Cannot compare a " + this.getClass() + " to a " + obj.getClass());
	}
    }

}