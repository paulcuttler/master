package com.ract.common.cad;

import java.io.Serializable;

import com.ract.util.DateTime;

/**
 * @author hollidayj
 * @created 1 August 2002
 * @todo Description of the Class
 */
public class CadExportPK implements Serializable {

    /**
     * Description of the Field
     */
    public Integer tranNo;

    /**
     * Description of the Field
     */
    public DateTime createDateTime;

    /**
     * Constructor for the CadExportPK object
     */
    public CadExportPK() {
    }

    /**
     * Constructor for the CadExportPK object
     * 
     * @param tranNo
     *            Description of the Parameter
     * @param createDateTime
     *            Description of the Parameter
     */
    public CadExportPK(Integer tranNo, DateTime createDateTime) {
	this.tranNo = tranNo;
	this.createDateTime = createDateTime;

    }

    /**
     * Description of the Method
     * 
     * @param obj
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean equals(Object obj) {
	if (this.getClass().equals(obj.getClass())) {
	    CadExportPK that = (CadExportPK) obj;
	    return this.tranNo.equals(that.tranNo) && this.createDateTime.equals(that.createDateTime);
	}
	return false;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public int hashCode() {
	return (tranNo + "" + createDateTime).hashCode();
    }
}
