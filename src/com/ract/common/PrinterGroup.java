package com.ract.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * Title: Master Project
 * </p>
 * <p>
 * Description: Brings all of the projects together into one master project for
 * deployment.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: RACT
 * </p>
 * 
 * @author John Holliday
 * @version 1.0
 */
@Entity
@Table(name = "[x_print-grp]")
public class PrinterGroup {
    public String getPrinterGroup() {
	return printerGroup;
    }

    public void setPrinterGroup(String printerGroup) {
	this.printerGroup = printerGroup;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getBranchNumber() {
	return branchNumber;
    }

    public void setBranchNumber(String branchNumber) {
	this.branchNumber = branchNumber;
    }

    public String getPrinted() {
	return printed;
    }

    public void setPrinted(String printed) {
	this.printed = printed;
    }

    @Id
    @Column(name = "pg_prgrp")
    private String printerGroup;

    @Column(name = "pg_desc")
    private String description;

    @Column(name = "[branch-no]")
    private String branchNumber;

    private String printed;
}