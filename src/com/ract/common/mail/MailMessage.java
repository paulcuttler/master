package com.ract.common.mail;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * Represent an email message.
 * 
 * @author hollidayj
 * 
 */
public class MailMessage implements Serializable {

    // String recipient, String bccTo,String subject, String messageText,String
    // fromAddress, Hashtable files, boolean specifyEnvironment, boolean
    // htmlFormat
    private String recipient;

    private String bcc;

    private String message;

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    private String subject;

    private String from;

    private Hashtable files;

    private boolean specifyEnvironment;

    private boolean html;

    @Override
    public String toString() {
	return "MailMessage [bcc=" + bcc + ", files=" + files + ", from=" + from + ", html=" + html + ", message=" + message + ", recipient=" + recipient + ", specifyEnvironment=" + specifyEnvironment + ", subject=" + subject + "]";
    }

    public String getRecipient() {
	return recipient;
    }

    public void setRecipient(String recipient) {
	this.recipient = recipient;
    }

    public String getBcc() {
	return bcc;
    }

    public void setBcc(String bcc) {
	this.bcc = bcc;
    }

    public String getSubject() {
	return subject;
    }

    public void setSubject(String subject) {
	this.subject = subject;
    }

    public String getFrom() {
	return from;
    }

    public void setFrom(String from) {
	this.from = from;
    }

    public Hashtable getFiles() {
	return files;
    }

    public void setFiles(Hashtable files) {
	this.files = files;
    }

    public boolean isSpecifyEnvironment() {
	return specifyEnvironment;
    }

    public void setSpecifyEnvironment(boolean specifyEnvironment) {
	this.specifyEnvironment = specifyEnvironment;
    }

    public boolean isHtml() {
	return html;
    }

    public void setHtml(boolean html) {
	this.html = html;
    }

}
