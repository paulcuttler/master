package com.ract.common;

import java.rmi.RemoteException;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @todo Description of the Class
 * 
 * @author hollidayj
 * @created 1 August 2002
 */
@Entity
@Table(name = "[gn_reference]")
public class ReferenceDataVO extends ValueObject implements Writable, CachedItem {

    public ReferenceDataPK getReferenceDataPK() {
	return referenceDataPK;
    }

    public void setReferenceDataPK(ReferenceDataPK referenceDataPK) {
	this.referenceDataPK = referenceDataPK;
    }

    public static final boolean STATUS_Active = true;

    public static final boolean STATUS_Inactive = false;

    public static final String REF_TYPE_JOIN_REASON = "MEMJOIN";

    public static final String REF_TYPE_REJOIN_REASON = "MEMREJOIN";

    // public static final String REF_TYPE_AFFILIATED_MOTOR_CLUB = "MEMAMC";

    public static final String REF_TYPE_MEMBERSHIP_CARD_REQUEST = "MEMCARDREQ";

    public static final String REF_TYPE_REJOIN_TYPE = "MEMREJTYPE";

    public static final String REF_TYPE_CANCEL_REASON = "MEMCANCEL";

    /**
     * These reference codes apply to the reference type
     * REF_TYPE_MEMBERSHIP_CARD_REQUEST
     */
    public static final String CARD_REQUEST_CHANGE_JOIN_DATE = "Change Join Date";

    public static final String CARD_REQUEST_CHANGE_PRODUCT = "Change Product";

    public static final String CARD_REQUEST_CHANGE_NAME = "NAME";

    public static final String CARD_REQUEST_CONVERT = "Convert";

    public static final String CARD_REQUEST_DAMAGED = "Damaged";

    public static final String CARD_REQUEST_LOST = "Lost";

    public static final String CARD_REQUEST_NEW_MEMBERSHIP = "New Membership";

    public static final String CARD_REQUEST_REJOIN_MEMBERSHIP = "Rejoin";

    public static final String CARD_REQUEST_STOLEN = "Stolen";

    public static final String CARD_REQUEST_EXPIRED = "Expired";

    public static final String CARD_REQUEST_ISSUE_ERROR = "Issue error";

    public static final String CARD_REQUEST_TIER_CHANGE = "Tier change";

    public static final String CARD_REQUEST_RENEW = "Renew";

    public static final String CARD_REQUEST_CHANGE_REGO = "ChangeRego";

    // The new rejoin type.
    public static final String REJOIN_TYPE_NEW = "New";

    public static final String REJOIN_TYPE_RESET_EXPIRY = "ResetExpiry";

    // allow reasons to apply discounts
    public static final String REJOIN_REASON_ON_ROAD = "OR";

    public static final String JOIN_REASON_ON_ROAD = "OR";

    public static final String REASON_ACCESS_TRAVEL = "ACCTRV";

    public static final String REASON_ACCESS_INSURANCE = "ACCINS";

    public static final String REASON_CMO = "CMO";

    // A membership cancellation reason of deceased.
    public static final String MEMCANCELREASON_DECEASED = "DEC";
    public static final String MEMCANCELREASON_ALLOW_TO_LAPSE = "COO";
    public static final String MEMCANCELREASON_UNFINANCIAL = "UNF";
    public static final String MEMCANCELREASON_ADDRESS_UNKNOWN = "ADK";

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "ReferenceData";

    /**
     * Hard code to prevent users changing it.
     */
    public final static ReferenceDataVO TRANSFER_FINANCIAL = new ReferenceDataVO(REF_TYPE_JOIN_REASON, "TF", "Transfer Financial");

    /**
     * Flag to change the join date only or not.
     */
    public final static String REF_REJOIN_TYPE_RESET_EXPIRY = "ResetExpiry";

    /**
     * Description of the Field
     */
    protected String description;

    @EmbeddedId
    private ReferenceDataPK referenceDataPK;

    /**
     * Used to determine if the code is still "Active" can it be displayed in
     * the selection list
     */
    @Column(name = "ref_active")
    protected boolean referenceActive;

    /**
     * Used to determine the display order in select lists
     */
    @Column(name = "sort_order")
    protected int sortOrder;

    /**
     * Constructors ****************************
     */

    public ReferenceDataVO() {
    }

    /**
     * Constructor for the ReferenceDataVO object
     * 
     * @param rType
     *            Description of the Parameter
     * @param rCode
     *            Description of the Parameter
     * @param desc
     *            Description of the Parameter
     * 
     */
    public ReferenceDataVO(String rType, String rCode, String desc) {
	this.setReferenceDataPK(new ReferenceDataPK(rCode, rType));
	this.description = desc;
	this.referenceActive = true;
    }

    /**
     * Constructor for the ReferenceDataVO object
     * 
     * @param rType
     *            Description of the Parameter
     * @param rCode
     *            Description of the Parameter
     * @param desc
     *            Description of the Parameter
     * @param rActive
     *            Is this rCode active
     */
    public ReferenceDataVO(String rType, String rCode, String desc, boolean rActive) {
	this.setReferenceDataPK(new ReferenceDataPK(rCode, rType));
	this.description = desc;
	this.referenceActive = rActive;
    }

    /**
     * Constructor for the ReferenceDataVO object
     * 
     * @param rType
     *            Description of the Parameter
     * @param rCode
     *            Description of the Parameter
     * @param desc
     *            Description of the Parameter
     * @param rActive
     *            Is this rCode active
     * @param rSortOrder
     *            SortOrder for display in lists
     */
    public ReferenceDataVO(String rType, String rCode, String desc, boolean rActive, int rSortOrder) {
	this.setReferenceDataPK(new ReferenceDataPK(rCode, rType));
	this.description = desc;
	this.referenceActive = rActive;
	this.sortOrder = rSortOrder;
    }

    public ReferenceDataVO(Node refNode) throws RemoteException {
	if (refNode == null || !refNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
	    throw new SystemException("Failed to create ReferenceDataVO from XML node. The node is not valid.");
	}

	this.referenceDataPK = new ReferenceDataPK();

	NodeList elementList = refNode.getChildNodes();
	Node elementNode = null;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();
	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;
	    }

	    if ("referenceType".equals(nodeName) && valueText != null) {
		this.referenceDataPK.setReferenceType(valueText);
	    } else if ("referenceCode".equals(nodeName) && valueText != null) {
		this.referenceDataPK.setReferenceCode(valueText);
	    } else if ("description".equals(nodeName)) {
		this.description = valueText;
	    } else if ("referenceActive".equals(nodeName) && valueText != null) {
		this.referenceActive = Boolean.valueOf(valueText).booleanValue();
	    } else if ("referenceSortOrder".equals(nodeName) && valueText != null) {
		this.sortOrder = Integer.parseInt(valueText);
	    }
	}
    }

    /**
     * Gets the description attribute of the ReferenceDataVO object
     * 
     * @return The description value
     */
    public String getDescription() {
	return this.description;
    }

    /**
     * return the Active statuus of this ReferenceDataVO
     * 
     * @return the referenceActive value
     */
    public boolean isActive() {
	return this.referenceActive;
    }

    /**
     * Sets the description attribute of the ReferenceDataVO object
     * 
     * @param description
     *            The new description value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    public int getSortOrder() {
	return this.sortOrder;
    }

    public void setSortOrder(int sortOrder) throws RemoteException {
	checkReadOnly();
	this.sortOrder = sortOrder;
    }

    /**
     * Sets the referenceActive attribute of the ReferenceDataVO object
     * 
     * @param referenceActive
     *            The new referenceActive value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setActive(boolean refActive) throws RemoteException {
	checkReadOnly();
	this.referenceActive = refActive;
    }

    /**
     * Description of the Method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
    }

    /**
     * CachedItem interface methods ***************
     * 
     * @return The key value
     */

    /**
     * Return the reference type and code as the cached item key.
     * 
     * @return The key value
     */
    public String getKey() {
	return this.getReferenceDataPK().getReferenceType() + this.getReferenceDataPK().getReferenceCode();
    }

    /**
     * Return true if the specified key matches this items key.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if both keys are null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {

	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("referenceType", this.referenceDataPK.getReferenceType());
	cw.writeAttribute("referenceCode", this.referenceDataPK.getReferenceCode());
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("referenceActive", this.referenceActive);
	cw.writeAttribute("referenceSortOrder", this.sortOrder);
	cw.endClass();
    }

}
