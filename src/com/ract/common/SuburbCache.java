package com.ract.common;

/**
 * Singleton to store suburb data in memory. It is reset on change of state.
 * Assumption is that 99% are tasmanian.
 * 
 * @author John Holliday
 * @created 2 April 2003
 * @version 1.0
 */

public class SuburbCache
// extends CacheBase
{
    //
    // /**
    // * the state filter of suburbs
    // */
    // private static String state;
    //
    // //initialise the singleton
    // private static SuburbCache instance = new SuburbCache();
    //
    // /**
    // * Implement the getItemList method of
    // * the super class. Return the list of
    // * cached items. Add items to the list
    // * if it is empty.
    // *
    // *@exception RemoteException Description
    // * of the Exception
    // */
    // public void initialiseList()
    // throws RemoteException
    // {
    // if(this.isEmpty())
    // {
    // StreetSuburbMgr streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
    // Collection dataList = null;
    //
    // try
    // {
    // dataList = streetSuburbMgr.getSuburbs(state);
    // if(dataList != null)
    // {
    // Iterator it = dataList.iterator();
    // while(it.hasNext())
    // {
    // Item suburb = new Item((String)it.next());
    // this.add(suburb);
    // }
    // }
    // }
    // catch(Exception e)
    // {
    // System.out.println("Failed to get list of states:\n");
    // e.printStackTrace();
    // }
    // }
    // }
    //
    // /**
    // * Return a reference the instance of
    // * this class.
    // *
    // *@return The instance value
    // */
    // public static SuburbCache getInstance()
    // {
    // return instance;
    // }
    //
    // /**
    // * Return the list of products. Use the
    // * getList() method to make sure the list
    // * has been initialised. A null or an
    // * empty list may be returned.
    // *
    // *@param state Description
    // * of the Parameter
    // *@return The suburbList
    // * value
    // *@exception RemoteException Description
    // * of the Exception
    // */
    // public ArrayList getSuburbList(String state)
    // throws RemoteException
    // {
    // if(this.state != state)
    // {
    // //reset the list
    // this.reset();
    // //set the new values
    // this.state = state;
    // }
    // return this.getItemList();
    // }

}
