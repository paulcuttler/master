package com.ract.common.qas;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.ract.common.qas.ui.QASUIConstants;
import com.ract.util.LogUtil;

public class QASNotifier {

    public QASNotifier() {
    }

    public static void NotifyQAS(String ipAddress, String layout) throws UnknownHostException, IOException {
	// EOT
	char c = (char) 4;
	LogUtil.log("QASNotifier ", "getting socket " + " " + ipAddress);
	// open the socket
	InetAddress host = InetAddress.getByName(ipAddress);
	LogUtil.log("QASNotifier ", "getting socket " + " " + ipAddress + " / " + host);
	Socket socket = null;
	try {
	    socket = new Socket(host, QASUIConstants.qasPort);
	} catch (IOException ex) {
	    LogUtil.log("QASNotifier ", " Unable to create socket " + ex.getMessage());
	    return;
	}
	LogUtil.log("QASNotifier ", "socket " + socket.getInetAddress().getHostName() + " " + ipAddress);
	System.out.println("QASNotifier socket " + socket.getInetAddress().getHostName() + " " + ipAddress);
	OutputStream sos = socket.getOutputStream();

	// Notify QAS
	String thisLayout = "com.ract.clientlistener.QASPopUp " + layout.trim();
	LogUtil.debug("QASNotifier ", "writing " + (thisLayout));
	System.out.println("QASNotifier writing " + (thisLayout));
	sos.write((thisLayout + " " + c).getBytes());
	sos.flush();
	sos.close();

	// close the socket
	socket.close();
    }

    public static void main(String[] args) {
	QASNotifier q = new QASNotifier();
	try {
	    q.NotifyQAS("203.20.20.164", "CustomerResidentialAuto");
	} catch (UnknownHostException ex) {
	    System.out.println("Unknown Host " + ex.getMessage());
	} catch (IOException ex) {
	    System.out.println("IO Exception " + ex.getMessage());
	}
    }
}
