package com.ract.common.qas.ui;

import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.common.ApplicationServlet;
import com.ract.common.qas.QASAddress;
import com.ract.common.qas.QASNotifier;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;

public class QASUIC extends ApplicationServlet {
    private final static String CONTENT_TYPE = "text/html";

    public void init() throws ServletException {

    }

    public void destroy() {
    }

    public void handleEvent_notifyQAS(HttpServletRequest request, HttpServletResponse response) {

	LogUtil.debug("RLG + handleEvent_notifyQAS ", " QASUIC entered ");
	String reply = "";
	try {
	    // if(!this.getUserSession(request).getUser().isPrivilegedUser("qaspopup"))
	    // {
	    // LogUtil.info(this.getClass(),
	    // "User does not have the qaspopup privilege");
	    // throw new Exception();
	    // }

	    String ipAddress = null;
	    ipAddress = this.getUserSession(request).getIp();

	    String layout = (String) request.getParameter("layout");

	    LogUtil.debug("QASUIC", ipAddress + " - " + layout);

	    QASNotifier.NotifyQAS(ipAddress, layout);
	    reply = "successful";

	} catch (Exception ex) {
	    LogUtil.debug("QASUIC Exception ", ex.getMessage());
	    reply = "unsuccessful";
	}

	ServletUtil.ajaxXMLResponse(response, "<QAS><response>" + reply + "</response></QAS>");

    }

    public void handleEvent_processQASAddress(HttpServletRequest request, HttpServletResponse response) {
	this.outputRequestAttributes(request);
	this.outputRequestParameters(request);

	String qasSuburb = request.getParameter("qasSuburb").toUpperCase();
	String qasStreet = request.getParameter("street").toUpperCase();
	String qasProperty = request.getParameter("property").toUpperCase();
	String qasPropertyQualifier = request.getParameter("propertyQualifier").toUpperCase();

	String userid = null;
	try {
	    userid = "<QAS>-AUTO-" + this.getUserSession(request).getUserId();
	} catch (RemoteException ex) {
	    LogUtil.log(this.getClass(), "Unable to get user session");
	}

	QASAddress qadd = new QASAddress(qasSuburb, qasStreet, qasProperty, qasPropertyQualifier, userid);

	ServletUtil.ajaxXMLResponse(response, qadd.toXML());
    }
}
