package com.ract.common.qas.ui;

import com.ract.common.CommonConstants;

public class QASUIConstants {
    public final static String DIR_COMMON = CommonConstants.DIR_COMMON;
    /**
     * URL pattern for Client UIC
     */
    public static final String PAGE_QASUIC = "/QASUIC";

    /**
     * QAS port
     */
    public final static int qasPort = 38724;

}
