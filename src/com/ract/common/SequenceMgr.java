package com.ract.common;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface SequenceMgr extends EJBObject {
    public static final String SEQUENCE_PAY_PAYEE = "TP_PAYEE";

    public static final String SEQUENCE_PAY_CREDIT_DISPOSAL = "PAY_DISPOSAL";

    public static final String SEQUENCE_PAY_VOUCHER = "VOUCHERNO";

    public static final String SEQUENCE_HELP = "HELP";

    public static final String SEQUENCE_CLI_OCCUPATION = "CLI_OCCUPATION";

    public static final String SEQUENCE_MEM_ACCOUNT = "MEM_ACCOUNT";

    public static final String SEQUENCE_MEM_DOCUMENT = "MEM_DOCUMENT";

    public static final String SEQUENCE_MEM_MEMBERSHIP = "MEM_MEMBERSHIP";

    public static final String SEQUENCE_MEM_GROUP = "MEM_GROUP";

    public final static String SEQUENCE_MEM_CARD = "MEM_CARD";

    public static final String SEQUENCE_MEM_QUOTE = "MEM_QUOTE";

    public static final String SEQUENCE_MEM_TRANSACTION_GROUP = "MEM_TRANSACTION_GROUP";

    public static final String SEQUENCE_MEM_TRANSACTION = "MEM_TRANSACTION";

    public static final String SEQUENCE_PAY_JOURNAL = "PAY_JOURNAL";

    public static final String SEQUENCE_PAY_ITEM = "PAY_ITEM";

    public static final String SEQUENCE_CLIENT_NOTE = "CLI_NOTE";

    public static final String SEQUENCE_PAYEE_NAME = "TP_PAYEE";

    public static final String SEQUENCE_PAY_COMPONENT = "PAY_COMPONENT";

    public static final String SEQUENCE_PAY_POSTING = "PAY_POSTING";

    public static final String SEQUENCE_MEM_FEE_SPECIFICATION = "MEM_FEE_SPECIFICATION";

    public static final String SEQUENCE_EFT_TRANSACTION = "EFT_TRANSACTION";

    public static final String SEQUENCE_CAD_EXPORT = "CADEXPORT";

    public void resetTableId(String tableName, Integer Id) throws RemoteException;

    /**
     * Must in be in new transaction.
     * 
     * @param tableName
     *            String
     * @throws RemoteException
     * @return Integer
     */
    public Integer getNextID(String tableName) throws RemoteException;

}
