package com.ract.common;

import java.io.Serializable;

/**
 * A print form object holding printer and form information.
 * 
 * @author Nigel Whitley
 */

public class PrintForm implements Serializable {

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public void setPrinterType(String printerType) {
		this.printerType = printerType;
	}

	public void setPrinterOptions(String printerOptions) {
		this.printerOptions = printerOptions;
	}

	public void setFormOptions(String formOptions) {
		this.formOptions = formOptions;
	}

	public String getFormOptions() {
		return formOptions;
	}

	public String getPrinterOptions() {
		return printerOptions;
	}

	public String getPrinterType() {
		return printerType;
	}

	public String getPrinterName() {
		return printerName;
	}

	private String printerName;

	private String printerType;

	private String printerOptions;

	private String formOptions;

	public String toString() {
		StringBuffer desc = new StringBuffer();
		desc.append(this.printerName + ",");
		desc.append(this.printerType + ",");
		desc.append(this.printerOptions + ",");
		desc.append(this.formOptions);
		return desc.toString();
	}

}
