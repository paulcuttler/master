package com.ract.common.menu;

import java.io.Serializable;

public class MenuItemPK implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 6807182278920300147L;
    private String menuName;
    private String menuItemName;

    /**
     * @hibernate.key-property position="1"
     */
    public String getMenuName() {
	return menuName;
    }

    public void setMenuName(String menuName) {
	this.menuName = menuName;
    }

    /**
     * @hibernate.key-property position="2"
     */
    public String getMenuItemName() {
	return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
	this.menuItemName = menuItemName;
    }

    public boolean equals(Object obj) {
	if (obj instanceof MenuItemPK) {
	    MenuItemPK that = (MenuItemPK) obj;
	    return this.menuName.equals(that.menuName) && this.menuItemName.equals(that.menuItemName);
	}
	return false;
    }

    public int hashCode() {
	return this.menuName.hashCode() + this.menuItemName.hashCode();
    }

    public String toString() {
	return this.menuName + "/" + this.menuItemName;
    }

}
