package com.ract.common.menu;

import java.io.Serializable;

/**
 * @hibernate.class lazy="false"
 */
public class MenuItem implements Serializable {

    private boolean disabled;

    /**
	 * 
	 */
    private static final long serialVersionUID = 5255967998352602732L;

    private MenuItemPK menuItemPK;

    private String menuItemTitle;

    private String target;

    private String location;

    private String privilegeId;

    /**
     * @hibernate.property
     */
    public String getPrivilegeId() {
	return privilegeId;
    }

    public void setPrivilegeId(String privilegeId) {
	this.privilegeId = privilegeId;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public MenuItemPK getMenuItemPK() {
	return menuItemPK;
    }

    public void setMenuItemPK(MenuItemPK menuItemPK) {
	this.menuItemPK = menuItemPK;
    }

    /**
     * @hibernate.property
     */
    public String getMenuItemTitle() {
	return menuItemTitle;
    }

    public void setMenuItemTitle(String menuItemTitle) {
	this.menuItemTitle = menuItemTitle;
    }

    /**
     * @hibernate.property
     */
    public String getTarget() {
	return target;
    }

    public void setTarget(String target) {
	this.target = target;
    }

    /**
     * @hibernate.property
     */
    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(menuItemPK);
	desc.append(",");
	desc.append(menuItemTitle);
	desc.append(",");
	desc.append(location);
	desc.append(",");
	desc.append(target);
	desc.append(",");
	desc.append(privilegeId);
	return desc.toString();
    }

    /**
     * @hibernate.property
     */
    public boolean isDisabled() {
	return disabled;
    }

    public void setDisabled(boolean disabled) {
	this.disabled = disabled;
    }

}
