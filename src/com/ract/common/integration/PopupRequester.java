package com.ract.common.integration;

import java.rmi.RemoteException;

import org.w3c.dom.Node;

import com.ract.common.CommonEJBHelper;

public class PopupRequester implements Integrator {

    PopupRequest pr = null;
    String response = null;

    public void processXML(String dataType, String action, Node dataNode) throws RemoteException {
	// what always happens here is that the request number is extracted,
	// written to the popupRequest table with the balance of the xml

	try {
	    // LogUtil.log(this.getClass(),"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
	    // + "\n" + PopupRequest.getXMLString(dataNode)
	    // + "\n-----------------------------------");

	    pr = new PopupRequest(dataNode);
	    IntegrationMgr imgr = CommonEJBHelper.getIntegrationMgr();
	    imgr.setPopupRequest(pr);
	    this.response = "Success";
	} catch (Exception ex) {
	    this.response = "Failure";
	    ex.printStackTrace();
	}
    }

    public String getResponse() {
	return this.response;
    }

    public static PopupRequest getPopupRequest(Integer requestNo) throws Exception {
	PopupRequest pr = null;
	IntegrationMgr imgr = CommonEJBHelper.getIntegrationMgr();
	pr = imgr.getPopupRequest(requestNo);
	return pr;
    }

}
