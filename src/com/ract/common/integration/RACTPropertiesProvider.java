package com.ract.common.integration;

import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.ract.util.LogUtil;

/**
 * <p>
 * Provides initial properties for ejb initial context.
 * </p>
 * 
 * @deprecated
 * @author Sam McLennan, jyh
 * @created 30 Jul 2003
 */
@Deprecated
public class RACTPropertiesProvider extends Hashtable {

    private static RACTPropertiesProvider propertiesProvider;

    private static int port;

    private static String host;

    private RACTPropertiesProvider() {
	LogUtil.log("RACTPropertiesProvider", "Replaced by jndi.properties method.");
	if (getContextURL() == null) {
	    InitialContext initialContext = null;
	    try {
		LogUtil.log(this.getClass(), "initialContext=" + host + ":" + port);
		initialContext = new InitialContext();
		LogUtil.log(this.getClass(), "environment=" + initialContext.getEnvironment());
		this.putAll(initialContext.getEnvironment());
	    } catch (NamingException ex) {
		LogUtil.fatal(this.getClass(), "Unable to get initial context. " + ex.getMessage());
	    }
	}
    }

    public synchronized static RACTPropertiesProvider getInstance() {
	LogUtil.log("RACTPropertiesProvider", "Replaced by jndi.properties method.");
	if (propertiesProvider == null) {
	    propertiesProvider = new RACTPropertiesProvider();
	}
	return propertiesProvider;
    }

    public static boolean setContextURL(String newHost, int newPort) {
	LogUtil.log("RACTPropertiesProvider", "Replaced by jndi.properties method.");
	propertiesProvider = null;
	host = newHost;
	port = newPort;
	return true;
    }

    public static String getContextURL() {
	LogUtil.log("RACTPropertiesProvider", "Replaced by jndi.properties method.");
	String contextURL = null;
	if (host != null && port != 0) {
	    contextURL = host + ":" + port;
	}
	return contextURL;
    }

}
