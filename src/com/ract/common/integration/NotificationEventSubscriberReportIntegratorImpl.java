package com.ract.common.integration;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.PrintForm;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationEventSubscriber;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.PrintUtil;
import com.ract.util.ReportUtil;

/**
 * <p>
 * Primary subscriber to the report integrator event. Process the report in one
 * of the following ways:
 * </p>
 * <ul>
 * <li>Print</li>
 * <li>File</li>
 * <li>Email</li>
 * </ul>
 * 
 * @author jyh
 * @version 1.0
 */
public class NotificationEventSubscriberReportIntegratorImpl implements NotificationEventSubscriber, Serializable {

    private final String ELEMENT_PRINT_GROUP = "printGroup";
    private final String ELEMENT_IMAGE_LOCATION = "imageLocation";
    private final String ELEMENT_FILE_NAME = "fileName";
    private final String ELEMENT_EMAIL_ADDRESS_TO = "emailAddress";
    private final String ELEMENT_EMAIL_ADDRESS_FROM = "emailAddressFrom";
    private final String ELEMENT_EMAIL_MESSAGE_BODY = "emailMessageBody";
    private final String ELEMENT_EMAIL_SUBJECT = "emailSubject";
    private final String ELEMENT_DEBUG = "debug"; // add environment to subject
    // private final String ELEMENT_FORM_NAME = "formName";
    private final String ELEMENT_TEMPLATE_NAME = "templateName";
    private final String ELEMENT_EMAIL_BCC = "bccAddress";

    /**
     * Prefix of the key to use to look up the system parameter.
     */
    private final String KEY_DIRECTORY_PREFIX = "DIR_";
    final String FORM_FILE = "file";
    final String FORM_EMAIL = "email";

    public void processNotificationEvent(NotificationEvent event) throws RemoteException {
	// do the work
	ReportIntegratorNotificationEvent reportIntegratorNotificationEvent = (ReportIntegratorNotificationEvent) event;
	Node dataNode = reportIntegratorNotificationEvent.getDataNode();
	String dataType = reportIntegratorNotificationEvent.getDataType();
	Node printNode = null;

	printNode = dataNode.getFirstChild();

	try {
	    // construct the report
	    this.constructReportFromXML(dataType, printNode);

	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
    }

    public String getSubscriberName() {
	return "ReportIntegrator";
    }

    private void constructReportFromXML(String reportType, Node printNode) throws ReportException {
	XMLReportRequest myReport = null;
	String formName = null;
	String printGroup = null;
	String imageLocation = null;
	NodeList itemList = null;
	Node printerNode = null;
	Node imageNode = null;
	Element printDataNode = null;
	Element fileNameNode = null;
	String fileNameString = null;
	String toAddressString = null;
	String fromAddressString = null;
	String messageBody = null;
	boolean specifyEnvironment = false;
	String subject = null;
	String templateName = null;
	boolean smbMode = false;
	// String reportKeyName = null;
	String reportCache = null;
	String reportName = null;
	String bccAddressString = null;

	itemList = ((Element) printNode).getElementsByTagName(ELEMENT_PRINT_GROUP);
	printerNode = itemList.item(0);

	imageLocation = this.getChildValue(printNode, ELEMENT_IMAGE_LOCATION);
	fileNameString = this.getChildValue(printNode, ELEMENT_FILE_NAME);
	toAddressString = this.getChildValue(printNode, ELEMENT_EMAIL_ADDRESS_TO);
	fromAddressString = this.getChildValue(printNode, ELEMENT_EMAIL_ADDRESS_FROM);
	bccAddressString = this.getChildValue(printNode, ELEMENT_EMAIL_BCC);
	messageBody = this.getChildValue(printNode, ELEMENT_EMAIL_MESSAGE_BODY);
	reportName = this.getChildValue(printNode, "reportName");
	subject = this.getChildValue(printNode, ELEMENT_EMAIL_SUBJECT);
	printGroup = this.getChildValue(printNode, "printGroup");
	formName = this.getChildValue(printNode, "formName");
	templateName = this.getChildValue(printNode, ELEMENT_TEMPLATE_NAME);

	printDataNode = (Element) printNode.getLastChild();

	itemList = ((Element) printNode).getElementsByTagName(ELEMENT_DEBUG);
	if (itemList.getLength() > 0) {
	    fileNameNode = (Element) itemList.item(0);
	    specifyEnvironment = Boolean.valueOf(fileNameNode.getFirstChild().getNodeValue()).booleanValue();
	}

	if (printGroup == null) {
	    throw new ReportException("printGroup is null in " + this.getClass() + ".constructReportFromXML");
	}
	if (formName == null) {
	    throw new ReportException("formName is null in " + this.getClass() + ".constructReportFromXML");
	}
	if (templateName == null) {
	    throw new ReportException("templateName is null in " + this.getClass() + ".constructReportFromXML");
	}
	try {
	    CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
	    reportCache = comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_REPORT_CACHE);
	} catch (Exception rex) {
	    rex.printStackTrace();
	    throw new ReportException(rex);
	}

	myReport = new XMLReportRequest();
	try {
	    myReport.initialise(printGroup, formName, reportName, templateName, CommonConstants.REPORT_REPOSITORY, // system
														   // parameter
		    (Element) printDataNode);
	    ReportDeliveryFormat deliveryFormat = ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF);
	    myReport.setDeliveryFormat(deliveryFormat);

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ReportException("ERROR INITIALISING XMLReportRequest \n" + ex);
	}
	// formName = myReport.getFormName();
	if (formName.equals(FORM_FILE)) {
	    if (fileNameString != null) {
		myReport.setDestinationFileName("smb:" + fileNameString);
		smbMode = true;
	    } else {
		smbMode = false;
		String fileName = reportName + (new java.util.Date().getTime() + "").substring(0, 8) + ".pdf";
		myReport.setDestinationFileName(reportCache + "/" + fileName);
		// myReport.setDestinationFileName(getFileNameForOutput(myReport));
	    }
	} else if (formName.equals(this.FORM_EMAIL)) {
	    /* write the file to the report cache directory */
	    myReport.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
	    try {
		String fileName = reportName + (new java.util.Date().getTime() + "").substring(0, 8) + ".pdf";
		myReport.setDestinationFileName(reportCache + "/" + fileName);
		LogUtil.log(this.getClass(), "cache filename = " + myReport.getDestinationFileName());
		smbMode = false;
	    } catch (Exception rex) {
		rex.printStackTrace();
		throw new ReportException(rex);
	    }
	} else {
	    myReport.setDestinationFileName(this.getFileNameForPrinting(myReport));
	    smbMode = true;
	}
	if (myReport.getDestinationFileName() == null) {
	    throw new ReportException("Unable to create output filename");
	}

	try {

	    LogUtil.debug(this.getClass(), "\n----------------------------------------------------" + "\n       printGroup = " + printGroup + "\n       formName   = " + formName + "\n       template   = " + templateName + "\n       reportName = " + reportName + "\n   image location = " + imageLocation + "\n   fileNameString = " + fileNameString + "\n  toAddressString = " + toAddressString + "\nfromAddressString = " + fromAddressString + "\n       bccAddress = " + bccAddressString + "\n          subject = " + subject + "\n          smbMode = " + smbMode + "\n         FileName = " + myReport.getDestinationFileName() + "\n              URL = " + myReport.getReportDataURL() + "\n          smbMode = " + smbMode + "\n----------------------------------------------------");

	    // OutputStream fStream =
	    // FileUtil.openStreamForWriting(myReport.getDestinationFileName(),smbMode);
	    // myReport.setOutputStream(fStream);
	    // myReport.addParameter("URL",myReport.getReportDataURL());
	    // LogUtil.debug(this.getClass(),"myReport="+myReport);
	    // new ReportGenerator().runReport(myReport);
	    // ReportRegister.removeReport(reportKeyName);

	    if (formName.equals(FORM_EMAIL)) {

		MailMessage message = new MailMessage();
		message.setSubject(subject);
		message.setRecipient(toAddressString);
		message.setBcc(bccAddressString);
		message.setFrom(fromAddressString);
		message.setMessage(messageBody);
		message.setSpecifyEnvironment(specifyEnvironment);

		myReport.setReportAttachmentName(reportName + "." + FileUtil.EXTENSION_PDF);

		ReportUtil.emailReport(myReport, smbMode, message);// TODO
								   // report
								   // name and
								   // dest
								   // filename

		// emailReport(subject,reportName + "."+FileUtil.EXTENSION_PDF,
		// myReport.getDestinationFileName(),
		// toAddressString,bccAddressString, fromAddressString,
		// messageBody, specifyEnvironment);
	    } else {
		ReportUtil.printReport(myReport);
	    }
	} catch (Exception ex1) {
	    ex1.printStackTrace();
	    throw new ReportException(ex1);
	}
    }

    /**
     * Standard report suffix to append to the class name.
     */
    private final String REPORT_SUFFIX = "Request";

    /**
     * Print the report request.
     * 
     * @param myReport
     *            ReportRequestBase
     * @param report
     *            ReportRequest
     * @param formName
     *            String
     * @param printerGroup
     *            String
     * @throws ReportException
     */
    private String getFileNameForPrinting(ReportRequestBase myReport) throws ReportException {

	String fileName;
	try {
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    PrintForm printForm = cMgr.getPrintForm(myReport.getFormName(), myReport.getPrinterGroup());
	    fileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.INSURANCE);
	} catch (RemoteException ex) {
	    throw new ReportException(ex);
	}
	return fileName;
    }

    // private String getFileNameForOutput(ReportRequestBase myReport)throws
    // ReportException
    // {
    // String dirReportKey = KEY_DIRECTORY_PREFIX +
    // myReport.getReportName().toUpperCase(); //eg. DIR_DIRECTDEBITSCHEDULEEXT
    // String directoryName = null;
    // try
    // {
    // CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
    // directoryName =
    // commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,
    // dirReportKey);
    // }
    // catch(RemoteException ex)
    // {
    // throw new ReportException(ex);
    // }
    // return directoryName + myReport.getReportKeyName() + "." +
    // FileUtil.EXTENSION_PDF;
    //
    // }

    private String getChildValue(Node thisNode, String childName) {
	NodeList itemList = null;
	Node childNode = null;
	String value = null;

	itemList = ((Element) thisNode).getElementsByTagName(childName);
	if (itemList.getLength() > 0) {
	    childNode = (Element) itemList.item(0);
	    value = childNode.getFirstChild().getNodeValue();
	}
	return value;
    }

}
