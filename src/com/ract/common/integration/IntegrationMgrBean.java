package com.ract.common.integration;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Init;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.client.notifier.ProgressClientChangeNotificationEvent;
import com.ract.common.CommonEJBHelper;
import com.ract.common.ExceptionHelper;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationMgrLocal;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.common.transaction.TransactionAdapterList;
import com.ract.payment.integration.PaymentDocument;
import com.ract.payment.notifier.PaymentCancellationAdapter;
import com.ract.payment.notifier.PaymentNotificationEvent;
import com.ract.payment.notifier.PaymentNotificationHelper;
import com.ract.payment.notifier.PendingTransactionAdapter;
import com.ract.travel.notifier.TravelPaymentAdapter;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * <p>
 * The session bean that manages all of the integration between RACT systems.
 * The integration takes place via a suite of XML documents that contain
 * elements that instruct how the data is to be processed.
 * </p>
 * 
 * <p>
 * The integration XML schema definition is contained in a Word document in the
 * (at the time of writing) "l:\information technology\projects\system
 * integration\" directory.
 * 
 * @todo The schema definition should be formalised into a XML schema for
 *       validation so that all documents that do not conform to the standard
 *       are rejected.
 *       </p>
 * 
 * @author John Holliday
 * @version 1.0
 */
@Stateful
@Remote({ IntegrationMgr.class })
public class IntegrationMgrBean {
    @Resource
    private SessionContext sessionContext;

    @PersistenceContext(unitName = "master")
    Session hsession;

    // supported data types
    public final static String DATATYPE_PROGRESS_CLIENT = "ProgressClientData";

    public final static String DATATYPE_PROGRESS_STREET_SUBURB = "ProgressStreetSuburbData";

    public final static String DATATYPE_MEMBERSHIP = "MembershipData";

    public final static String DATATYPE_TRAVEL = "TravelData";

    public final static String DATATYPE_INSURANCE = "InsuranceData";

    public final static String DATATYPE_FLEET = "FleetData";

    public final static String DATATYPE_VEHICLE_INSPECTION = "VehicleInspectionData";

    public final static String DATATYPE_INTEGRATION = "IntegrationData";

    /**
     * Generic payment data.
     */

    public final static String DATATYPE_PAYMENT = "PaymentData";

    // supported actions

    public final static String ACTION_PAYMENT = "payment";

    public final static String ACTION_UPDATE = "update";

    public final static String ACTION_CREATE = "create";

    public final static String ACTION_DELETE = "delete";

    public final static String ACTION_PRINT = "print";

    public final static String ACTION_ROLLBACK = "rollback";

    public final static String ACTION_COMMIT = "commit";

    // the label of the "data" node

    public static final String DATA_PROGRESS_CLIENT = "ProgressClient";

    public final static String DATA_PAYMENT = "Payment";

    public final static String DATA_PENDING = "Pending";

    public final static String DATA_PROGRESS_STREET_SUBURB = "ProgressStreetSuburb";

    public final static String DATA_UNDO = "Undo";

    public final static String TAG_DATA = "Data";

    // private global variables to hold the action and document data type.

    private String action;

    private String dataType;

    /**
     * Maintain a list of TransactionAdapters to commit, rollback or release.
     * There should be a maximum of one instance of each transactionadapter.
     */
    private TransactionAdapterList transactionAdapterList;

    /**
     * Take an xml document as input and parse and respond according to the
     * structure. The xml document will consist of one or more integration
     * items.
     * 
     * @param documentElement
     * @throws RemoteException
     */
    public String processIntegrationDocument(Element documentElement) throws RemoteException, IntegrationException {
	LogUtil.log(this.getClass(), "processIntegrationDocument start");
	LogUtil.debug(this.getClass(), "processIntegrationDocument transactionAdapterList=" + transactionAdapterList);

	// get the keepsocketopen attribute, if false then commit at the end.
	boolean keepSocketOpen = IntegrationServerSocket.keepSocketOpen(documentElement);
	LogUtil.debug(this.getClass(), "keepSocketOpen=" + keepSocketOpen);

	String response = null;

	try {
	    NodeList itemList;
	    Node itemNode;
	    itemList = documentElement.getChildNodes();

	    if (itemList == null || itemList.getLength() == 0) {
		throw new RemoteException("Integration XML contains no items: " + documentElement.toString());
	    }

	    String responseItem = "";
	    // for each integration item
	    for (int x = 0; x < itemList.getLength(); x++) {
		itemNode = itemList.item(x);
		if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
		    // get single response item
		    responseItem = processIntegrationItem((Element) itemNode);
		    if (responseItem != null) {
			// initialise variable
			if (response == null) {
			    response = new String();
			}
			response += responseItem;
		    }
		}
		if (transactionAdapterList.isRollbackOnly()) {
		    LogUtil.debug(this.getClass(), "marked for rollback");
		    // error
		    throw new Exception(transactionAdapterList.getFirstRollbackException().getMessage());
		}
	    }
	} catch (Exception ex) // catch all exceptions
	{
	    // if not closing the socket otherwise wait for the next document
	    if (!keepSocketOpen) {
		rollback();
	    }
	    LogUtil.fatal(this.getClass(), "Exception=" + ExceptionHelper.getExceptionStackTrace(ex) + "/" + keepSocketOpen);
	    throw new IntegrationException(ex.getMessage());
	}

	// if not closing the socket
	if (!keepSocketOpen) {
	    commit();
	}
	LogUtil.debug(this.getClass(), "response=" + response);
	LogUtil.log(this.getClass(), "processIntegrationDocument end");
	return response;
    }

    private void rollback() throws RemoteException {
	LogUtil.debug(this.getClass(), "rollback start");
	LogUtil.debug(this.getClass(), "rollback() transactionAdapterList=" + transactionAdapterList);
	if (transactionAdapterList != null && transactionAdapterList.size() > 0) {
	    // rollback
	    LogUtil.log(this.getClass(), "rollback transactionAdapterList=" + transactionAdapterList.toString());
	    try {
		transactionAdapterList.rollbackTransactionAdapterList();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    } finally {
		transactionAdapterList.clear();

		// rollback all CMTs
		sessionContext.setRollbackOnly();
	    }
	}
	LogUtil.debug(this.getClass(), "rollback end");
    }

    private void commit() throws RemoteException {
	LogUtil.debug(this.getClass(), "commit start");
	LogUtil.debug(this.getClass(), "commit() transactionAdapterList=" + transactionAdapterList);
	if (transactionAdapterList != null && transactionAdapterList.size() > 0) {
	    try {
		// commit
		LogUtil.log(this.getClass(), "commit transactionAdapterList=" + transactionAdapterList.toString());
		transactionAdapterList.commitTransactionAdapterList();
	    } catch (RemoteException ex) {
		// rethrow
		throw new SystemException(ex);
	    } finally {
		transactionAdapterList.clear();
	    }
	}
	LogUtil.debug(this.getClass(), "commit end");
    }

    // The control section tags.

    public static final String CONTROL_DATATYPE = "DataType";

    public static final String CONTROL_ACTION = "Action";

    public static final String CONTROL_CLASSNAME = "ClassName";

    /**
     * <p>
     * Handle an individual integration item. Multiple integration items make up
     * an integration document.
     * </p>
     * 
     * <p>
     * NB: any new methods can be handled by creating a new class which
     * implements the Integrator interface and including a "ClassName" tag in
     * the XML (alongside "DataType" and "Action") which holds the fully
     * qualified name of the class to be instantiated to handle the command.
     * </p>
     * 
     * <p>
     * This class should implement the Integrator interface. The XML data node
     * is passed to the setXML method of the Integrator object, along with the
     * dataType and action.
     * </p>
     * 
     * @param documentElement
     *            the integration item element
     * @throws RemoteException
     */
    private String processIntegrationItem(Element documentElement) throws RemoteException {
	LogUtil.debug(this.getClass(), "processIntegrationItem start");

	String response = null;
	action = null;
	dataType = null;
	Node dataTypeNode, actionNode, classNameNode;
	NodeList nodeList, actionList, classNameList;
	String classNameToRun = "";

	// Find the data type from the control section of the document
	nodeList = documentElement.getElementsByTagName(CONTROL_DATATYPE);
	actionList = documentElement.getElementsByTagName(CONTROL_ACTION);
	classNameList = documentElement.getElementsByTagName(CONTROL_CLASSNAME);

	if (nodeList == null || nodeList.getLength() != 1) {
	    throw new SystemException("'handleIntegration - Integration' XML document does not contain a '" + CONTROL_DATATYPE + "' element. " + documentElement.toString());
	}

	if (actionList == null || actionList.getLength() != 1) {
	    throw new SystemException("'handleIntegration - Integration' XML document does not contain an '" + CONTROL_ACTION + "' element. " + documentElement.toString());
	}

	dataTypeNode = nodeList.item(0);
	actionNode = actionList.item(0);
	// use reflection to run the class
	if (classNameList != null && classNameList.getLength() > 0) {
	    classNameNode = classNameList.item(0);
	    if (classNameNode.hasChildNodes()) {
		classNameToRun = classNameNode.getFirstChild().getNodeValue();
	    }
	}
	if (dataTypeNode.hasChildNodes()) {
	    dataType = dataTypeNode.getFirstChild().getNodeValue();
	}

	if (actionNode.hasChildNodes()) {
	    action = actionNode.getFirstChild().getNodeValue();
	}

	LogUtil.debug(this.getClass(), "\n  >>-----------> Action =   " + action + "\n  >>-----------> Data type = " + dataType + "\n  >>-----------> The class to instantiate = " + classNameToRun);

	if (!classNameToRun.equals("")) {
	    response = processIntegrator(documentElement, response, classNameToRun);
	}
	// INTEGRATION TRANSACTION CONTROL
	else if (DATATYPE_INTEGRATION.equalsIgnoreCase(dataType)) {
	    if (ACTION_COMMIT.equalsIgnoreCase(action)) {
		commit();
	    } else if (ACTION_ROLLBACK.equalsIgnoreCase(action)) {
		rollback();
	    }
	}
	// PAYMENT
	else if (DATATYPE_PAYMENT.equalsIgnoreCase(dataType)) {
	    if (ACTION_DELETE.equalsIgnoreCase(action)) {
		handleDeletePayable(action, documentElement);
	    } else if (ACTION_UPDATE.equalsIgnoreCase(action)) {
		handlePaymentData(documentElement);
	    } else {
		throw new SystemException("Integration '" + DATATYPE_PAYMENT + "' document contains an unknown '" + CONTROL_ACTION + "' type: " + action);
	    }
	}
	// PROGRESS PROFILE_CLIENT
	else if (DATATYPE_PROGRESS_CLIENT.equalsIgnoreCase(dataType)) {
	    try {
		handleProgressClientData(documentElement, action);
	    } catch (RollBackException ex1) {
		throw new SystemException(ex1);
	    }
	}
	// TRAVEL
	else if (DATATYPE_TRAVEL.equalsIgnoreCase(dataType)) {
	    // LogUtil.debug(this.getClass(), "Got '"+dataType+"' DataType");
	    if (action.equalsIgnoreCase(ACTION_PAYMENT)) {
		handleTravelPaymentData(documentElement);
	    } else {
		throw new SystemException("Integration travel document contains an unknown '" + CONTROL_ACTION + "' type: " + action);
	    }
	}
	// PENDING
	else if (isPendingDatatype(dataType)) {
	    LogUtil.debug(this.getClass(), "Got '" + dataType + "' xml DataType");

	    if (action.equalsIgnoreCase(ACTION_PAYMENT)) {
		handlePendingData(dataType, documentElement);
	    } else {
		throw new SystemException("Integration  XML document contains an unknown '" + CONTROL_ACTION + "' type: " + action);
	    }
	} else {
	    throw new SystemException("'Integration' XML document contains an unknown  " + CONTROL_DATATYPE + " ' of " + dataType + "'.");
	}
	LogUtil.debug(this.getClass(), "processIntegrationItem end");
	LogUtil.debug(this.getClass(), "response=" + response);
	return response;
    }

    /**
     * Process an integrator. If it is a print integrator then process as an
     * event.
     * 
     * @param documentElement
     *            Element
     * @param response
     *            String
     * @param classNameToRun
     *            String
     * @throws SystemException
     * @return String
     */
    private String processIntegrator(Element documentElement, String response, String classNameToRun) throws RemoteException {
	LogUtil.debug(this.getClass(), "Attempting to process Integrator " + classNameToRun);
	Class integratorClass = null;
	Integrator integrator = null;

	NodeList dataList = documentElement.getElementsByTagName(TAG_DATA);
	Node dataNode = dataList.item(0);
	try {
	    integratorClass = Class.forName(classNameToRun);
	    LogUtil.debug(this.getClass(), "Starting report generation");
	    integrator = (Integrator) integratorClass.newInstance(); // instantiate
								     // object
	    integrator.processXML(dataType, action, dataNode);
	    // get a response code after waiting for the integrator to finish
	    // its work.
	    response = integrator.getResponse();
	    LogUtil.debug(this.getClass(), "Finished report generation");
	} catch (Exception ex) {
	    throw new SystemException("Error running " + classNameToRun, ex);
	}

	LogUtil.debug(this.getClass(), "response=" + response);

	return response;
    }

    /**
     * Determine if the datatype is pending data. It could be fleet, insurance,
     * vehicle inspection of membership.
     * 
     * @param dataType
     * @return
     */
    private boolean isPendingDatatype(String dataType) {
	boolean pendingType = false;
	if (DATATYPE_FLEET.equalsIgnoreCase(dataType) || DATATYPE_INSURANCE.equalsIgnoreCase(dataType) || DATATYPE_VEHICLE_INSPECTION.equalsIgnoreCase(dataType) || DATATYPE_MEMBERSHIP.equals(dataType)) {
	    pendingType = true;
	}
	return pendingType;
    }

    /**
     * The Integration xml document may contain one or more 'Payment' elements.
     * Send each one to the notification manager as a paid change notification
     * event.
     */
    private void handlePaymentData(Element documentElement) throws RemoteException {
	LogUtil.debug(this.getClass(), "handlePaymentData start");
	NodeList paymentList = null;
	Node paymentNode = null;
	Element paymentElement = null;
	int paymentCount = 0;

	TransactionAdapter transactionAdapter = null;

	PaymentNotificationEvent paidDataEvent = null;

	LogUtil.debug(this.getClass(), "Payment data: getting elements by tag name");

	paymentList = documentElement.getElementsByTagName(DATA_PAYMENT);
	paymentCount = paymentList.getLength();

	String receiptingSystem = null;

	if ((paymentList != null) && (paymentCount != 0)) {
	    LogUtil.debug(this.getClass(), "Element has " + paymentCount + "payment tags");
	    for (int j = 0; j < paymentList.getLength(); j++) {
		paymentNode = paymentList.item(j);
		if (paymentNode.getNodeType() == Node.ELEMENT_NODE) {
		    paymentElement = (Element) paymentNode;
		    paidDataEvent = new PaymentNotificationEvent(paymentElement);

		    receiptingSystem = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.RECEIPTING_SYSTEM + paidDataEvent.getSourceSystem()); // PAYRECSYS
																									  // +
																									  // eg.
																									  // MEM

		    // Make sure that RECEPTOR is the current receipting system
		    // being used
		    if (!SourceSystem.RECEPTOR.getAbbreviation().equals(receiptingSystem)) {
			throw new SystemException("The current receipting system being used is '" + receiptingSystem + "' and not RECEPTOR. Payment notifications cannot be accepted by the RECEPTOR payment notification mechanism in IntegrationMgrBean.handlePaymentData().");
		    }

		    // Process notification sequentially rather than through the
		    // notification manager.
		    try {
			PaymentNotificationHelper paymentNotificationHelper = new PaymentNotificationHelper();
			// handle multiple notification types
			transactionAdapter = paymentNotificationHelper.processPaymentNotificationEvent(paidDataEvent, transactionAdapterList);
			LogUtil.log(this.getClass(), "adding transaction adapter to list. " + transactionAdapterList.size());
			// add transaction adapter to the list
			if (transactionAdapter != null) {
			    transactionAdapterList.addTransactionAdapter(transactionAdapter);
			} else {
			    LogUtil.warn(this.getClass(), "The event does not have an associated transaction adapter.");
			}
			LogUtil.log(this.getClass(), "transactionAdapterList size=" + transactionAdapterList.size());
		    } catch (Exception ex) {
			throw new SystemException(ex.getMessage()); // throw message
		    }

		} else {
		    throw new SystemException("The " + paymentCount + " node is not an xml 'Pending' type.");
		}
	    }
	} else {
	    throw new SystemException("The Integration xml document specified a '" + DATA_PAYMENT + "' data type but no '" + DATA_PAYMENT + "' elements could be found.");
	}
	LogUtil.debug(this.getClass(), "handlePaymentData end");
    }

    /**
     * Handle pending type data
     * 
     * @see isPendingType
     * @param pendingDataType
     * @param documentElement
     * @throws RemoteException
     * 
     */
    private void handlePendingData(String pendingDataType, Element documentElement) throws RemoteException {
	NodeList pendingList = null;
	Node pendingNode = null;
	Element pendingElement = null;
	int clientCount = 0;

	PaymentDocument paymentDocument = null;

	LogUtil.debug(this.getClass(), "Pending data: getting elements by tag name");

	pendingList = documentElement.getElementsByTagName(DATA_PENDING);// "Pending"
	clientCount = pendingList.getLength();

	if ((pendingList != null) && (clientCount != 0)) {
	    LogUtil.debug(this.getClass(), "Element has " + pendingList.getLength() + "pending tags");

	    for (int j = 0; j < pendingList.getLength(); j++) {
		pendingNode = pendingList.item(j);
		if (pendingNode.getNodeType() == Node.ELEMENT_NODE) {
		    pendingElement = (Element) pendingNode;
		    paymentDocument = new PaymentDocument(pendingDataType, pendingElement);
		    try {
			LogUtil.debug(this.getClass(), "notifying receptor");

			PendingTransactionAdapter transactionAdapter = new PendingTransactionAdapter(paymentDocument.getClientNumber(), paymentDocument.getSequenceNumber(), paymentDocument.getProduct(), paymentDocument.getAmountPayable(), paymentDocument.getDescription(), paymentDocument.getSourceSystem(), paymentDocument.getBranch(), paymentDocument.getUserName(), paymentDocument.getPropData());
			try {
			    transactionAdapter.createPayableFee();
			} catch (Exception ex) {
			    transactionAdapter.setRollbackOnly(ex);
			}
			// add to the transaction list.
			transactionAdapterList.addTransactionAdapter(transactionAdapter);
		    } catch (Exception e) {
			throw new SystemException(e);
		    }
		} else {
		    throw new SystemException("The " + clientCount + " node is not an xml 'Pending' type.");
		}
	    }
	} else {
	    throw new SystemException("The Integration xml document specified a '" + DATA_PENDING + "' data type but no '" + DATA_PENDING + "' elements could be found.");
	}

    }

    /**
     * The xml document may contain one or more 'Payment' elements. Send each
     * one to the notification manager as a paid change notification event.
     * 
     * @todo move to the helper
     */
    private void handleTravelPaymentData(Element documentElement) throws RemoteException {
	NodeList paymentList = null;
	Node paymentNode = null;
	int paymentCount = 0;

	LogUtil.debug(this.getClass(), "Travel data: getting elements by tag name : '" + DATA_PAYMENT + "'");

	paymentList = documentElement.getElementsByTagName(DATA_PAYMENT);
	paymentCount = paymentList.getLength();

	LogUtil.debug(this.getClass(), "Travel data: found nodes  " + paymentCount);
	PaymentDocument travelPaymentDocument = null;

	if ((paymentList != null) && (paymentCount != 0)) {
	    LogUtil.debug(this.getClass(), "Element has " + paymentList.getLength() + " Payment tags");
	    TravelPaymentAdapter transactionAdapter = null;
	    for (int j = 0; j < paymentList.getLength(); j++) {
		paymentNode = paymentList.item(j);
		if (paymentNode.getNodeType() == Node.ELEMENT_NODE) {
		    travelPaymentDocument = new PaymentDocument(action, (Element) paymentNode);
		    transactionAdapter = new TravelPaymentAdapter(travelPaymentDocument.getUserName(), travelPaymentDocument.getBranch(), travelPaymentDocument.getTill(), travelPaymentDocument.getReceiptNumber(), travelPaymentDocument.getClientNumber(), travelPaymentDocument.getProducts(), travelPaymentDocument.getCosts(), travelPaymentDocument.getPaymentType(), travelPaymentDocument.getTendered(), travelPaymentDocument.getPaymentBreakdown(), "", new DateTime(travelPaymentDocument.getCreateDate()), travelPaymentDocument.getSourceSystem());
		    try {
			transactionAdapter.createPaidTransaction();
		    } catch (RemoteException ex) {
			transactionAdapter.setRollbackOnly(ex);
		    }
		    // add to the transaction adapter list
		    transactionAdapterList.addTransactionAdapter(transactionAdapter);
		} else {
		    throw new SystemException("Node " + paymentNode.getNodeName() + " is not an xml '" + DATA_PAYMENT + "' type.");
		}
	    }
	} else {
	    throw new SystemException("The Integration xml document specified a '" + DATA_PAYMENT + "' data type but no elements could be found. " + documentElement.toString());
	}
    }

    /**
     * Handle undo type transactions, typically undo a renewal.
     * 
     * @param documentElement
     *            XML element containing the relevant data
     * @throws RemoteException
     */
    private void handleDeletePayable(String action, Element documentElement) throws RemoteException {
	NodeList undoList = null;
	Node undoNode = null;
	Element undoElement = null;
	int undoCount = 0;

	undoList = documentElement.getElementsByTagName(DATA_UNDO);
	undoCount = undoList.getLength();
	PaymentDocument ud = null;

	if ((undoList != null) && (undoCount != 0)) {
	    LogUtil.debug(this.getClass(), "Element has " + undoCount + " Undo tags");
	    PaymentCancellationAdapter transactionAdapter = null;
	    for (int j = 0; j < undoList.getLength(); j++) {
		undoNode = undoList.item(j);
		if (undoNode.getNodeType() == Node.ELEMENT_NODE) {
		    undoElement = (Element) undoNode;
		    ud = new PaymentDocument(action, undoElement);
		    transactionAdapter = new PaymentCancellationAdapter(ud.getClientNumber(), ud.getSequenceNumber(), ud.getSourceSystem(), ud.getUserName());
		    try {
			transactionAdapter.removePayableFee();
		    } catch (Exception ex) {
			transactionAdapter.setRollbackOnly(ex);
		    }
		    // add to transaction adapter list
		    transactionAdapterList.addTransactionAdapter(transactionAdapter);
		} else {
		    throw new SystemException("The " + undoNode.getNodeName() + " node is not an xml '" + DATA_UNDO + "' type.");
		}
	    }
	} else {
	    throw new SystemException("The Integration xml document specified an '" + DATA_UNDO + "' data type but no matching elements could be found.");
	}
    }

    /**
     * The xml document will contain one or more 'ProgressClient' elements. Send
     * each one to the notification manager as a progress client change
     * notification event.
     * 
     * //TODO allow commit/rollback
     */
    private void handleProgressClientData(Element documentElement, String action) throws RemoteException, RollBackException {
	NodeList clientList = null;
	Node clientNode = null;
	Element clientElement = null;
	int clientCount = 0;

	/*
	 * Change NotificationMgr to a local reference due to changes in JBoss
	 * between 4.0.3SP1 and 4.2.2 In 4.0.3SP1, it appears that by default
	 * all calls via remote and local interfaces were pass by reference.
	 * 4.2.2 follows the spec more closely and remote interfaces pass by
	 * value and local interfaces pass by reference. This has implications
	 * for serialization as objects passed via the remote interface must
	 * either support serialization or be omitted via the transient keyword.
	 * eg. Element in ProgressClientChangeNotificationEvent is marked as
	 * transient so it will be null when passed via the remote interface.
	 */
	NotificationMgrLocal notificationMgrLocal = CommonEJBHelper.getNotificationMgrLocal();
	/* ProgressClientChange */NotificationEvent progClientEvent = null;

	clientList = documentElement.getElementsByTagName(DATA_PROGRESS_CLIENT);
	if (clientList != null) {
	    for (int i = 0; i < clientList.getLength(); i++) {
		clientNode = clientList.item(i);
		clientCount++;
		if (clientNode.getNodeType() == Node.ELEMENT_NODE) {
		    clientElement = (Element) clientNode;
		    LogUtil.log(this.getClass(), "clientElement=" + clientElement);
		    progClientEvent = new ProgressClientChangeNotificationEvent(clientElement, action);
		    try {
			notificationMgrLocal.notifyEvent(progClientEvent);
		    } catch (RollBackException rbe) {
			if (notificationMgrLocal != null) {
			    notificationMgrLocal.rollback();
			}
			throw new SystemException(rbe);
		    }
		} else {
		    throw new SystemException("The " + clientCount + " '" + DATA_PROGRESS_CLIENT + "' node is not an xml 'Element' type.");
		}
	    }
	}
	if (clientCount < 1) {
	    throw new SystemException("The Integration xml document specified a ProgressClientData data type but no 'ProgressClient' elements could be found.");
	}

	notificationMgrLocal.commit();
    }

    @Init
    public void ejbCreate() {
	LogUtil.debug(this.getClass(), "ejbCreate" + this.hashCode());
	transactionAdapterList = new TransactionAdapterList();
    }

    @PreDestroy
    public void ejbRemove() throws RemoteException {
	LogUtil.debug(this.getClass(), "ejbRemove" + this.hashCode());
	transactionAdapterList.clear();
	transactionAdapterList = null;
    }

    @PostActivate
    public void ejbActivate() throws RemoteException {
	LogUtil.debug(this.getClass(), "ejbActivate" + this.hashCode());
    }

    @PrePassivate
    public void ejbPassivate() throws RemoteException {
	LogUtil.debug(this.getClass(), "ejbPassivate" + this.hashCode());
    }

    public void setPopupRequest(PopupRequest pr) throws RemoteException {
	try {
	    hsession.save(pr);
	} catch (HibernateException hex) {
	    throw new RemoteException("Unable to store popupRequest: ", hex);
	}
    }

    public PopupRequest getPopupRequest(Integer requestNo) throws RemoteException {
	ArrayList fList = new ArrayList();
	PopupRequest pr = null;
	try {
	    Criteria c = hsession.createCriteria(PopupRequest.class);
	    c.add(Restrictions.eq("requestNo", requestNo));
	    fList = new ArrayList(c.list());
	    if (fList.size() != 0) {
		pr = (PopupRequest) fList.get(0); // should be exactly one
						  // result
		hsession.delete(pr);
	    }
	} catch (HibernateException ex) {
	    throw new RemoteException("ERROR retrieving PopuRequest\n", ex);
	}
	return pr;
    }

    /**
     * getPopupRequests Returns an array list of popupRequest objects
     * representing popup requests which have not been serviced
     * 
     * @param requestType
     *            String
     * @param salesBranch
     *            String
     * @param logonId
     *            String
     * @param rDate
     *            DateTime
     * @throws Exception
     * @return ArrayList
     */
    public ArrayList getPopupRequests(String requestType, String salesBranch, DateTime fDate, DateTime tDate) throws RemoteException {
	// LogUtil.log(this.getClass()+".getPopupRequests",
	// "\nrequestType = " + requestType
	// +"\nsalesBranch = " + salesBranch
	// +"\nfDate =       " + fDate
	// +"\ntDate =       " + tDate);
	ArrayList fList = new ArrayList();
	try {
	    Criteria c = hsession.createCriteria(PopupRequest.class);
	    c.add(Restrictions.eq("requestType", requestType));
	    if (salesBranch != null && !salesBranch.equals("") && !salesBranch.equalsIgnoreCase("all"))
		c.add(Restrictions.eq("salesBranch", salesBranch));
	    // if(logonId!=null && !logonId.equals(""))
	    // c.add(Restrictions.eq("logonId",logonId));
	    if (fDate != null)
		c.add(Restrictions.ge("requestDate", fDate));
	    if (tDate != null)
		c.add(Restrictions.le("requestDate", tDate));
	    fList = new ArrayList(c.list());
	} catch (HibernateException ex) {
	    throw new RemoteException("Error retrieving PopupRequests\n", ex);
	}
	return fList;
    }

}
