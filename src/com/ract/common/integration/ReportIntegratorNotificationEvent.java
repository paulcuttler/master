package com.ract.common.integration;

import org.w3c.dom.Node;

import com.ract.common.SystemException;
import com.ract.common.notifier.NotificationEvent;

/**
 * Allow Print Integrators to be treated as events.
 * 
 * @author jyh
 * @version 1.0
 */
public class ReportIntegratorNotificationEvent extends NotificationEvent {

    private String dataType;

    private Node dataNode;

    private String action;

    public Node getDataNode() {
	return dataNode;
    }

    public String getDataType() {
	return dataType;
    }

    public String getAction() {
	return action;
    }

    public ReportIntegratorNotificationEvent(String dataType, String action, Node dataNode) throws SystemException {
	// set up the data
	this.dataType = dataType;
	this.action = action;
	this.dataNode = dataNode;
	this.setEventName(NotificationEvent.EVENT_PRINT_INTEGRATOR);
	this.setEventMode(NotificationEvent.MODE_ASYNCHRONOUS);
    }

}
