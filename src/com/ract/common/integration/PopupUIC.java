package com.ract.common.integration;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.rmi.RemoteException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ract.client.ChangedAddress;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * UIC for admin work.
 * 
 * @author dgk
 * @version 1.0 16/05/2007
 */

public class PopupUIC extends HttpServlet {

    protected final static String CONTENT_TYPE_HTML = "text/html";
    protected final static String CONTENT_TYPE_XML = "text/xml";
    private final String METHOD_PREFIX = "handleEvent_";
    public static final String PARAMETER_EVENT = "event";
    UserSession us = null;

    public void init() throws ServletException {
    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

    protected void forwardRequest(HttpServletRequest request, HttpServletResponse response, String pageName) throws ServletException {
	try {
	    // LogUtil.debug(this.getClass(),"pageName="+pageName);
	    RequestDispatcher rd = this.getServletContext().getRequestDispatcher(pageName);
	    rd.forward(request, response);
	} catch (IOException e) {
	    handleException(request, response, e);
	}
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
	try {
	    response.setContentType(CONTENT_TYPE_HTML);
	    String event = request.getParameter(PARAMETER_EVENT);
	    String userId = request.getParameter("userid");
	    Integer requestNo = new Integer(request.getParameter("requestSeq"));
	    PopupRequest pr = PopupRequester.getPopupRequest(requestNo);
	    // if request no is not valid, don't create user session, and don't
	    // do anything else
	    if (pr == null)
		throw new Exception("Invalid request number: request not found");

	    // Establish user session
	    HttpSession session = request.getSession();
	    // failure here will also scuttle the creation of a user session
	    User user = this.getUser(userId);
	    String sessionId = session.getId();
	    DateTime createTime = new DateTime(session.getCreationTime());
	    String ipAddress = request.getRemoteAddr();
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String systemName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);
	    us = (UserSession) session.getAttribute(LoginUIConstants.USER_SESSION);
	    if (us == null) {
		us = new UserSession(user, sessionId, ipAddress, systemName, createTime);
		session.setAttribute(LoginUIConstants.USER_SESSION, us);
	    }
	    if (event == null) {
		throw new Exception("No event specified");
	    }
	    handleEvent(event, pr, request, response);
	} catch (Exception e) {
	    // forward to error page
	    e.printStackTrace();
	    handleException(request, response, e);
	}
    }

    protected void handleEvent(String event, PopupRequest pr, HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Document document = null;
	try {

	    String xmlString = pr.getData();
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    document = builder.parse(new InputSource(new StringReader(xmlString)));
	} catch (Exception ex) {
	    handleException(request, response, ex);
	}
	Class classTarget = this.getClass();
	String methodName = METHOD_PREFIX + event;
	Method methodTarget;
	try {
	    Class[] parameterTypes = new Class[] { HttpServletRequest.class, HttpServletResponse.class, Document.class };
	    methodTarget = classTarget.getMethod(methodName, parameterTypes);
	    Object[] arguments = new Object[] { request, response, document };
	    methodTarget.invoke(this, arguments);
	} catch (NoSuchMethodException nsme) {
	    RemoteException re = new RemoteException("The event handling method '" + methodName + "' in class '" + classTarget.getName() + "' does not exist!", nsme);
	    handleException(request, response, re);
	} catch (Exception e) {
	    e.printStackTrace();
	    handleException(request, response, e);
	}
    }

    protected void handleException(HttpServletRequest request, HttpServletResponse response, Exception e) {
	LogUtil.log(this.getClass(), e + "");
    }

    public void handleEvent_showDDHistory(HttpServletRequest request, HttpServletResponse response, Document doc) throws ServletException, RemoteException, ValidationException {
	String subSystem = getNodeValue("subSystem", doc);
	String systemNo = getNodeValue("systemNo", doc);
	String ddType = null;
	request.setAttribute("subSystem", subSystem);
	request.setAttribute("systemNo", systemNo);
	if (subSystem.equalsIgnoreCase("ins"))
	    ddType = "Policy Number";
	else if (subSystem.equalsIgnoreCase("mem"))
	    ddType = "Membership Number";
	else
	    ddType = subSystem;
	request.setAttribute("ddType", ddType);
	LogUtil.log(this.getClass(), "\nshowDDHistory:" + "\nsubsystem = " + subSystem + "\nsystemNo  = " + systemNo + "\nddType    = " + ddType);
	forwardRequest(request, response, "/payment/viewDDHistory.jsp");
    }

    public void handleEvent_showAddressUpdater(HttpServletRequest request, HttpServletResponse response, Document doc) throws ServletException, RemoteException, ValidationException {
	// LogUtil.log(this.getClass(),"handleEvent_showAddressUpdater >---> "
	// + "\n" + this.getXMLString(doc));
	ChangedAddress ca = new ChangedAddress();
	ca.setClientNo(new Integer(getNodeValue("clientNo", doc)));
	ca.setPostProperty(getNodeValue("oldPostProperty", doc));
	ca.setPostStreetChar(getNodeValue("oldPostStreetChar", doc));
	ca.setPostStsubid(new Integer(getNodeValue("oldPostStsubid", doc)));
	ca.setResiProperty(getNodeValue("oldResiProperty", doc));
	ca.setResiStreetChar(getNodeValue("oldResiStreetChar", doc));
	ca.setResiStsubid(new Integer(getNodeValue("oldResiStsubid", doc)));
	ca.setHomePhone(getNodeValue("oldHomePhone", doc));
	request.setAttribute("userId", getNodeValue("userid", doc));
	request.setAttribute("slsbch", getNodeValue("slsbch", doc));
	request.setAttribute("changedAddress", ca);
	forwardRequest(request, response, "/client/AddressUpdater.jsp");
    }

    private String getNodeValue(String nodeName, Document doc) {
	NodeList nl = doc.getElementsByTagName(nodeName);
	String value = "";
	if (nl.getLength() > 0) {
	    Node fn = nl.item(0);
	    if (fn.hasChildNodes()) {
		Node cn = fn.getFirstChild();
		value = cn.getNodeValue();
	    }
	}
	return value;
    }

    private User getUser(String userId) throws ServletException {
	UserMgr uMgr = null;
	User user = null;
	try {
	    uMgr = UserEJBHelper.getUserMgr();
	    user = uMgr.getUser(userId);
	} catch (Exception ex) {
	    throw new ServletException("Unable to find EJB Home.", ex);
	}
	return user;
    }

    public void handleEvent_showMarketOptions(HttpServletRequest request, HttpServletResponse response, Document doc) throws ServletException, RemoteException {
	String clientNo = getNodeValue("clientNo", doc);
	request.setAttribute("clientNo", clientNo);
	forwardRequest(request, response, "/client/MarketOptions.jsp");
    }

    public void handleEvent_insuranceExcesses(HttpServletRequest request, HttpServletResponse response, Document doc) throws ServletException, RemoteException {
	forwardRequest(request, response, "/insurance/InsExcesses.jsp");
    }
}
