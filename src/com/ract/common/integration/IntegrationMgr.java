package com.ract.common.integration;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.ejb.Remote;

import org.w3c.dom.Element;

import com.ract.util.DateTime;

/**
 * Integration Mgr
 * 
 * @author John Holliday
 * @version 1.0
 */
@Remote
public interface IntegrationMgr {

    public String processIntegrationDocument(Element documentElement) throws RemoteException, IntegrationException;

    public void setPopupRequest(PopupRequest pr) throws RemoteException;

    public PopupRequest getPopupRequest(Integer requestNo) throws RemoteException;

    public ArrayList getPopupRequests(String requestType, String salesBranch, DateTime fDate, DateTime tDate) throws RemoteException;

}
