package com.ract.common.integration.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import junit.framework.TestCase;

import com.ract.util.LogUtil;

/**
 * Test processing of xml integration documents.
 * 
 * @author jyh
 * @version 1.0
 */
public class TestSystemIntegration extends TestCase {

    protected void setUp() throws Exception {
	super.setUp();
	/** @todo verify the constructors */
    }

    final static String DIR = "c:\\temp\\test.xml";

    final static String HOST = "localhost";

    final static int PORT = 5300;

    final static String READ_SUCCESS = "ReadSuccessful";

    /**
     * test the vehicle inspection integration.
     */
    public void testVehicleInspection() {
	String returnString = "";

	// create the document
	StringBuffer xmlDoc = new StringBuffer();
	xmlDoc.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	xmlDoc.append("<Integration>");
	xmlDoc.append("<Item><Control><DataType>VehicleInspectionData</DataType><Action>Payment</Action></Control><Data><Pending><clientNo>137</clientNo><sequenceNo>13</sequenceNo><reference>0014503</reference><product>INSP</product><payable>110</payable><description>VI14503/0/KDSGJKL/sdkljsgjsadg/sdkjflskdgjsd</description><expiryDate>04/02/2004</expiryDate><sourceSystem>VI</sourceSystem><branch>HOB</branch><userName>rlg</userName></Pending></Data></Item>");
	xmlDoc.append("</Integration>");
	returnString = sendXMLDocument(xmlDoc);
	// ie. ReadSuccessful == ReadSuccessful
	assertEquals(READ_SUCCESS, returnString);
    }

    /**
     * Send an xml document to a socket
     * 
     * @return
     */
    private String sendXMLDocument(StringBuffer xmlDoc) {
	String returnString = "";
	try {
	    Socket socket = new Socket(HOST, PORT);
	    OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());
	    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

	    // control character
	    char spec = (char) 4;
	    xmlDoc.append(spec);

	    out.write(xmlDoc.toString());
	    out.flush();

	    StringBuffer sb = new StringBuffer();

	    char ch;

	    String responseLine = "";

	    while ((responseLine = in.readLine()) != null) {
		sb.append(responseLine);
	    }

	    returnString = sb.toString();

	    in.close();
	    socket.close();

	} catch (IOException ex) {
	    LogUtil.log(this.getClass(), ex);
	}

	return returnString;
    }

    protected void tearDown() throws Exception {

	super.tearDown();
    }

}
