package com.ract.common.integration;

import java.io.Serializable;
import java.io.StringWriter;

import org.jboss.util.xml.DOMWriter;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.util.DateTime;

/**
 * 
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * 
 * @author dgk
 * @version 1.0 28/05/07
 * @hibernate.class table="popupRequest" lazy="false"
 */
public class PopupRequest implements Serializable {
    private Integer requestNo = null;
    private String data = null;
    private String requestType;
    private DateTime requestDate;
    private String salesBranch;
    private String logonId;

    public PopupRequest() {

    }

    public PopupRequest(Integer rNo, String data) {
	this.requestNo = rNo;
	this.data = data;
    }

    public PopupRequest(Node dNode) throws Exception {
	try {
	    NodeList children = dNode.getChildNodes();
	    // should be at least 2 children
	    // the first should be the request no.
	    Node item = (Node) children.item(0);
	    String rnString = item.getFirstChild().getNodeValue();
	    this.requestNo = new Integer(rnString);
	    Node xmlNode = (Node) children.item(1);
	    this.data = getXMLString(xmlNode);

	    /*
	     * could be requestType, salesBranch and userid. if present set the
	     * relevant values
	     */
	    for (int xx = 2; xx < children.getLength(); xx++) {
		Node aNode = children.item(xx);
		if (aNode.getNodeName().equalsIgnoreCase("userId")) {
		    this.setLogonId(aNode.getFirstChild().getNodeValue());
		} else if (aNode.getNodeName().equalsIgnoreCase("slsbch")) {
		    this.setSalesBranch(aNode.getFirstChild().getNodeValue());
		} else if (aNode.getNodeName().equalsIgnoreCase("requestType")) {
		    this.setRequestType(aNode.getFirstChild().getNodeValue());
		}
		this.setRequestDate(new DateTime().getDateOnly());
	    }
	    // LogUtil.log(this.getClass(),"\nrequestNo = " + this.requestNo
	    // + "\nXML       = " + this.data);
	} catch (Exception ex) {
	    throw new Exception("Error writing PopupRequest: \n" + ex);
	}
    }

    /**
     * @hibernate.property
     * @hibernate.column name="data"
     */
    public String getData() {
	return data;
    }

    /**
     * @hibernate.id column="requestNo" generator-class="assigned"
     */

    public Integer getRequestNo() {
	return requestNo;
    }

    public void setData(String data) {
	this.data = data;
    }

    public void setRequestNo(Integer requestNo) {
	this.requestNo = requestNo;
    }

    public static String getXMLString(Node node) {
	StringWriter sw = new StringWriter();
	DOMWriter dw = new DOMWriter(sw);
	dw.print(node);
	String xml = sw.toString();
	return xml;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="requestType"
     */
    public String getRequestType() {
	return requestType;
    }

    public void setRequestType(String requestType) {
	this.requestType = requestType;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="requestDate"
     */
    public com.ract.util.DateTime getRequestDate() {
	return requestDate;
    }

    public void setRequestDate(com.ract.util.DateTime requestDate) {
	this.requestDate = requestDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="salesBranch"
     */
    public String getSalesBranch() {
	return salesBranch;
    }

    public void setSalesBranch(String salesBranch) {
	this.salesBranch = salesBranch;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="logonId"
     */
    public String getLogonId() {
	return logonId;
    }

    public void setLogonId(String logonId) {
	this.logonId = logonId;
    }

    /*
     * extract an integer value from the xmlString in 'data' There will be no
     * repeats, so first appearance of the tag will do.
     */
    public Integer getInt(String fieldName) throws Exception {
	return new Integer(getString(fieldName));
    }

    public String getString(String fieldName) throws Exception {
	int start = data.indexOf("<" + fieldName + ">");
	int end = data.indexOf("</" + fieldName + ">");
	return data.substring(start + fieldName.length() + 2, end);

    }
}
