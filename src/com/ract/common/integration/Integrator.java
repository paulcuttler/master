package com.ract.common.integration;

import java.rmi.RemoteException;

import org.w3c.dom.Node;

import com.ract.common.SystemException;

/**
 * <p>
 * Interface for processing an integration document.
 * </p>
 * <p>
 * Does not maintain a transactional context.
 * </p>
 * 
 * @author: dgk 4/5/04
 * @version 1.0
 */
public interface Integrator {
    /**
     * Process the xml sent and act on it.
     * 
     * @param dataType
     *            String
     * @param action
     *            String
     * @param dataNode
     *            Node
     * @throws SystemException
     */
    public void processXML(String dataType, String action, Node dataNode) throws RemoteException;

    /**
     * Return a string response.
     * 
     * @todo return an xml document response
     * @return String
     */
    public String getResponse();

}
