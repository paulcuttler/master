package com.ract.common.integration;

import com.ract.common.GenericException;

/**
 * <p>
 * Flag an integration exception.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class IntegrationException extends GenericException {
    public IntegrationException() {
	super();
    }

    public IntegrationException(Exception chainedException) {
	super(chainedException);
    }

    public IntegrationException(String message) {
	super(message);
    }

}
