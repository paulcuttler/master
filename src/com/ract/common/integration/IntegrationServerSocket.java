package com.ract.common.integration;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.Socket;
import java.rmi.RemoteException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ServerSocketManager;
import com.ract.common.SystemParameterVO;
import com.ract.util.LogUtil;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2003 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class IntegrationServerSocket extends ServerSocketManager {

    public IntegrationServerSocket() {

	LogUtil.debug(this.getClass(), this.currentThread().getName() + "-Initialising port.");
	try {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String portStr = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.NOTIFICATION_LISTENER_SOCKET_PORT);
	    setPort(Integer.parseInt(portStr));
	} catch (RemoteException re) {
	    setException(re);
	    LogUtil.fatal(this.getClass(), "Failed to initialise port number from system parameters using '" + SystemParameterVO.CATEGORY_COMMON + "," + SystemParameterVO.NOTIFICATION_LISTENER_SOCKET_PORT + "'. : " + re);
	} catch (NumberFormatException nfe) {
	    setException(nfe);
	    LogUtil.fatal(this.getClass(), "Failed to initialise port number from system parameters using '" + SystemParameterVO.CATEGORY_COMMON + "," + SystemParameterVO.NOTIFICATION_LISTENER_SOCKET_PORT + "'. :" + nfe);
	}
    }

    private final String ELEMENT_INTEGRATION = "Integration";

    private static final String ATTRIBUTE_INTEGRATION_KEEP_SOCKET_OPEN = "keepsocketopen";

    private final String RESPONSE_DELIMITER = " | ";

    private final String RESPONSE_FAILURE = "ReadFailed";

    private final String RESPONSE_SUCCESS = "ReadSuccessful";

    // prevent the client socket from waiting indefinitely
    private final int MESSAGE_LIMIT = 2;

    public static final char EOT = (char) 4;

    public void processClientSocket(Socket socket) {
	try {
	    LogUtil.debug(this.getClass(), "Host local address=" + socket.getLocalAddress().getHostAddress());
	    LogUtil.debug(this.getClass(), "Host local port=" + socket.getLocalPort());
	    LogUtil.debug(this.getClass(), "Host remote address=" + socket.getInetAddress());
	    LogUtil.debug(this.getClass(), "Host remote port=" + socket.getPort());
	    LogUtil.debug(this.getClass(), "TCP No delay=" + socket.getTcpNoDelay());
	    LogUtil.debug(this.getClass(), "Socket keep alive=" + socket.getKeepAlive());
	    LogUtil.debug(this.getClass(), "Socket send buffer size=" + socket.getSendBufferSize());
	    LogUtil.debug(this.getClass(), "Socket receive buffer size=" + socket.getReceiveBufferSize());
	} catch (Exception e) {
	    LogUtil.warn(this.getClass(), "Unable to log socket details. " + e.getMessage());
	}

	String response = null;
	String documentString = "";
	String integrationDocumentResponse = null;
	IntegrationMgr integrationMgr = null;

	// Integration element
	Element documentElement = null;

	// initialise input and output streams
	PrintStream out = null;
	InputStream in = null;
	try {
	    out = new PrintStream(socket.getOutputStream());
	    in = socket.getInputStream();
	} catch (IOException ioe) {
	    closeSocketResources(socket, out, in);
	    // unrecoverable error
	    LogUtil.fatal(this.getClass(), "Failed to get input or output stream. " + ioe.getMessage());
	    return;
	}

	integrationMgr = CommonEJBHelper.getIntegrationMgr();

	/*
	 * Determine whether the socket is to remain open
	 */
	boolean keepSocketOpen = true;
	int messageCount = 0;
	while (keepSocketOpen && messageCount < MESSAGE_LIMIT) {
	    // initialise the response
	    response = RESPONSE_SUCCESS;

	    messageCount++;
	    LogUtil.debug(this.getClass(), "Processing message " + messageCount + "...");
	    try {
		// increment counter
		addClientSocket();
		LogUtil.debug(this.getClass(), "Process client socket: " + this.currentThread().getName() + "-processClientSocket() - " + this.getClientSocketCount());

		// Parse the XML document in the receiveBuffer into an XML
		// document object model.
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		documentString = readInputStream(in);
		LogUtil.debug(this.getClass(), documentString);
		// expect well formed xml with any illegal characters enclosed
		// in CDATA sections
		Document document = builder.parse(new InputSource(new StringReader(documentString)));

		documentElement = document.getDocumentElement();
		String elementName = documentElement.getNodeName();
		// LogUtil.log(this.getClass(),
		// "Processing client socket, element name:" + elementName);
		// What sort of xml document is it?
		if (ELEMENT_INTEGRATION.equalsIgnoreCase(elementName)) {

		    integrationDocumentResponse = integrationMgr.processIntegrationDocument(documentElement);
		    // append an additional response string
		    if (integrationDocumentResponse != null && !integrationDocumentResponse.equals("")) {
			/**
			 * @todo return response in XML rather than plain text.
			 */
			response += RESPONSE_DELIMITER + integrationDocumentResponse;
		    }
		} else {
		    response = "ReadFailed " + RESPONSE_DELIMITER + "Unknown XML document type " + document.getNodeName() + "\n" + documentString;
		    LogUtil.fatal(this.getClass(), "Unknown XML document type " + document.getNodeName() + "\n" + documentString);
		}

	    } catch (Exception e) {
		// catch all exceptions
		response = RESPONSE_FAILURE + RESPONSE_DELIMITER + e.getMessage();
		LogUtil.fatal(this.getClass(), e);
		LogUtil.fatal(this.getClass(), documentString);
	    } finally {
		// determine if socket is to remain open
		keepSocketOpen = keepSocketOpen(documentElement);
		LogUtil.debug(this.getClass(), "keepSocketOpen=" + keepSocketOpen);

		// write message to client
		try {
		    // add EOT based on value of keep socket open
		    response += (keepSocketOpen ? String.valueOf(EOT) : "");
		    LogUtil.debug(this.getClass(), "Sending Integration document response='" + response + "'.");
		    out.write(response.getBytes());
		    out.flush();
		    // don't close yet
		} catch (IOException ioe) {
		    LogUtil.fatal(this.getClass(), "Failed to send return status to client.");
		}
		subtractClientSocket();
	    }

	}// end while

	// close socket and streams
	closeSocketResources(socket, out, in);

    }// end

    /**
     * Keep socket open for additional messages
     * 
     * @param documentElement
     *            Element
     * @return boolean
     */
    public static boolean keepSocketOpen(Element documentElement) {
	boolean keepSocketOpen;
	// process keepsocketopen attribute - in the absence of the attribute
	// assume socket is to be closed to allow support of legacy
	// implementations.
	String keepSocketOpenAttributeValue = null;
	if (documentElement != null) {
	    keepSocketOpenAttributeValue = documentElement.getAttribute(ATTRIBUTE_INTEGRATION_KEEP_SOCKET_OPEN); // if
														 // null?
	    // LogUtil.debug(this.getClass(),"keepSocketOpenAttributeValue="+keepSocketOpenAttributeValue);
	    if ("true".equals(keepSocketOpenAttributeValue)) {
		keepSocketOpen = true;
	    } else {
		keepSocketOpen = false;
	    }
	} else {
	    // end socket conversation.
	    keepSocketOpen = false;
	}
	// LogUtil.debug(this.getClass(),"keepSocketOpenAttributeValue="+keepSocketOpenAttributeValue);
	return keepSocketOpen;
    }

}
