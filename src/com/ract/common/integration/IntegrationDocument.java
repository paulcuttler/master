package com.ract.common.integration;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.SystemException;
import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

public abstract class IntegrationDocument {
    protected transient Element integrationDataNode;

    protected String integrationDataXML;

    protected String getNodeValue(String nodeName) throws SystemException {
	return getNodeByTag(integrationDataNode.getElementsByTagName(nodeName), nodeName);
    }

    private String getNodeByTag(NodeList nodeList, String tag) throws SystemException {

	// Node myNode = null;
	LogUtil.log(this.getClass(), "Checking node: " + tag);
	if (nodeList != null && nodeList.getLength() > 0) {
	    if (nodeList.getLength() > 1) {
		throw new SystemException("The integration element contains too many " + tag + "  tags.");
	    } else {
		String value = null;
		try {
		    value = nodeList.item(0).getFirstChild().getNodeValue();
		    LogUtil.log(this.getClass(), "" + tag + "= " + value);
		    return value;
		} catch (Exception e) {
		    throw new SystemException("The integration element contains an invalid " + tag + " tag value (" + value + ").");
		}
	    }
	} else {
	    return null;
	    // throw new
	    // SystemException("The integration element does not contain a " +
	    // tag + " tag.");
	}
    }

    protected String getElementValue(Element e) throws SystemException {
	if (e.hasChildNodes()) {
	    NodeList noList = e.getChildNodes();
	    Node noNode = noList.item(0);

	    LogUtil.log(this.getClass(), " got element value" + noNode.getNodeValue());

	    return noNode.getNodeValue();

	} else {
	    return null;
	}
    }

    protected String[] createStringArray(NodeList nl) throws SystemException {
	String[] values = null;

	if (nl.getLength() < 1) {
	    throw new SystemException("The element contains no tags.");
	} else {
	    if (nl.item(0).hasChildNodes()) {
		Node nl2 = nl.item(0);
		NodeList n = nl2.getChildNodes();
		int listLength = n.getLength();

		values = new String[listLength];

		for (int i = 0; i < listLength; i++) {
		    Element product = ((Element) n.item(i));
		    NodeList nList = product.getChildNodes();
		    // eg. ticket
		    String value = XMLHelper.returnNodeValue(nList);

		    // set in array
		    values[i] = value;

		}
		return values;
	    } else {
		throw new SystemException("There are no child nodes");
	    }
	}
    }

}