package com.ract.common;

/**
 * Defines the interface used by the cache base classes to manipulate cached
 * items.
 * 
 * @author Terry Bakker
 * @created 31 July 2002
 * @version 1.0
 */

public interface CachedItem {
    /**
     * Gets the key attribute of the CachedItem object
     * 
     * @return The key value
     */
    public String getKey();

    /**
     * Description of the Method
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key);
}
