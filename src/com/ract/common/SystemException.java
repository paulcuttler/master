package com.ract.common;

import java.rmi.RemoteException;

/**
 * A class to tag system generated exceptions so that they can be fetched from
 * an exception stack as the underlying cause.
 * 
 * @author Terry Bakker
 */

public class SystemException extends RemoteException {

    public SystemException() {
    }

    public SystemException(String msg) {
	super(msg);
    }

    public SystemException(Throwable t) {
	super("", t);
    }

    public SystemException(String msg, Throwable t) {
	super(msg, t);
    }
}
