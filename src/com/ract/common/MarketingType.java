package com.ract.common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Marketing type class
 * 
 * @author John Holliday
 * @version 1.0
 */

public class MarketingType implements Serializable {

    public MarketingType() {
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
	ois.defaultReadObject();
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
	oos.defaultWriteObject();
    }

    public void setMarketingId(Integer marketingId) {
	this.marketingId = marketingId;
    }

    public Integer getMarketingId() {
	return marketingId;
    }

    public void setMarketingType(String marketingType) {
	this.marketingType = marketingType;
    }

    public String getMarketingType() {
	return marketingType;
    }

    public void setMarketingTypeDesc(String marketingTypeDesc) {
	this.marketingTypeDesc = marketingTypeDesc;
    }

    public String getMarketingTypeDesc() {
	return marketingTypeDesc;
    }

    private Integer marketingId;

    private String marketingType;

    private String marketingTypeDesc;
}