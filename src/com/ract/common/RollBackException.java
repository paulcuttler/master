package com.ract.common;

/**
 * This exception is thrown whenever a component must be rolled back. For
 * example, calling a method on the payment manager may update the direct debit
 * system which needs to be explicitly committed or rolled back when the source
 * system commits or rolls back.
 * 
 * @author T. Bakker
 * @created 17 January 2003
 * @version 1.0 15/01/2003
 */

public class RollBackException extends GenericException {

    /**
     * Constructor for the RollBackException object
     */
    public RollBackException() {
	super();
    }

    /**
     * Constructor for the RollBackException object
     * 
     * @param msg
     *            Description of the Parameter
     */
    public RollBackException(String msg) {
	super(msg);
    }

    /**
     * Constructor for the RollBackException object
     * 
     * @param chainedException
     *            Description of the Parameter
     */
    public RollBackException(Throwable chainedException) {
	super(chainedException);
    }

    /**
     * Constructor for the RollBackException object
     * 
     * @param msg
     *            Description of the Parameter
     * @param chainedException
     *            Description of the Parameter
     */
    public RollBackException(String msg, Throwable chainedException) {
	super(msg, chainedException);
    }

}
