package com.ract.common;

import java.io.Serializable;

/**
 * Printer object representing a physical printer
 * 
 * @todo replaced by printjob - should be deleted
 * 
 * @author John Holliday
 * @version 1.0
 */

public class Printer implements Serializable {

    public Printer() {
    }

    private String printerName;

    private String printerType;

    private String printerDescription;

    private String printerCommand;

    private String printerOptions;

    public String getPrinterName() {
	return printerName;
    }

    public void setPrinterName(String printerName) {
	this.printerName = printerName;
    }

    public void setPrinterType(String printerType) {
	this.printerType = printerType;
    }

    public String getPrinterType() {
	return printerType;
    }

    public void setPrinterDescription(String printerDescription) {
	this.printerDescription = printerDescription;
    }

    public String getPrinterDescription() {
	return printerDescription;
    }

    public void setPrinterCommand(String printerCommand) {
	this.printerCommand = printerCommand;
    }

    public String getPrinterCommand() {
	return printerCommand;
    }

    public void setPrinterOptions(String printerOptions) {
	this.printerOptions = printerOptions;
    }

    public String getPrinterOptions() {
	return printerOptions;
    }

}