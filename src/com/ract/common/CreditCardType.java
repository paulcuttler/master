/*
 * @(#)CreditCardType.java
 *
 */
package com.ract.common;

import java.io.Serializable;

/**
 * Represent the different types of credit cards.
 * 
 * @author Glenn Lewis
 * @version 1.0, 13/5/2002
 * 
 */
public final class CreditCardType implements Serializable {
    /** A description of the credit card */
    private String myDescription = null;

    /**
     * Constructor. (Private constructor ensures that this class can only be
     * constructed by member methods.)
     * 
     * @param description
     *            a description of the credit card.
     */
    private CreditCardType(String description) {
	myDescription = description;
    }

    /** Instance representing an Americal Express Card */
    public static final CreditCardType AMERICAN_EXPRESS_CARD = new CreditCardType("American Express");

    /** Instance representing a Bank Card */
    public static final CreditCardType BANK_CARD = new CreditCardType("Bank Card");

    /** Instance representing a Diners Card */
    public static final CreditCardType DINERS_CLUB_CARD = new CreditCardType("Diners Club");

    /** Instance representing a Master Card */
    public static final CreditCardType MASTER_CARD = new CreditCardType("MasterCard");

    /** Instance representing a Diners Card */
    public static final CreditCardType VISA_CARD = new CreditCardType("Visa");

    /** All credit cards */
    private static CreditCardType[] allCardTypes = { AMERICAN_EXPRESS_CARD, BANK_CARD, DINERS_CLUB_CARD, MASTER_CARD, VISA_CARD };

    /**
     * Get a description of the credit card type
     * 
     * @return a string description of the credit card type
     */
    public String getDescription() {
	return myDescription;
    }

    /** Get the CreditCardType for a given description */
    public static CreditCardType getCardType(String description) throws ValidationException {
	for (int i = 0; i < allCardTypes.length; i++) {
	    CreditCardType cardType = allCardTypes[i];
	    if (cardType.getDescription().equals(description)) {
		return cardType;
	    }
	}
	Exception detail = new Exception("CreditCardType.getCardType('" + description + "').");
	throw new ValidationException("Unknown credit card type '" + description + "'.", detail);
    }

    // override the toString method
    public String toString() {
	return myDescription;
    }

    // override the equals method
    public boolean equals(Object otherObject) {
	if (otherObject == null) {
	    return false;
	}

	if (!(otherObject instanceof CreditCardType)) {
	    return false;
	} else {
	    String otherDescription = ((CreditCardType) otherObject).getDescription();
	    return myDescription.equals(otherDescription);
	}
    }
}
