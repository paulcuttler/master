package com.ract.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Value object to represent the different branches.
 * 
 * @author Glenn Lewis
 * @created 31 July 2002
 * @version 1.0
 */
@Entity
@Table(name = "[gn-branch]")
public class BranchVO extends ValueObject {
    public String getProfitCentre() {
	return profitCentre;
    }

    public void setProfitCentre(String profitCentre) {
	this.profitCentre = profitCentre;
    }

    /**
     * Description of the Field
     */
    @Id
    @Column(name = "[branch-no]")
    protected int branchNumber;

    /**
     * Description of the Field
     */
    protected String description;

    @Column(name = "[profit-centre]")
    protected String profitCentre;

    /**
     * Constructor
     */
    public BranchVO() {
    }

    /**
     * Constructor for the BranchVO object
     * 
     * @param branchNumber
     *            Description of the Parameter
     */
    public BranchVO(int branchNumber) {
	this.branchNumber = branchNumber;
    }

    /**
     * Constructor for the BranchVO object
     * 
     * @param branchNumber
     *            Description of the Parameter
     * @param description
     *            Description of the Parameter
     */
    public BranchVO(int branchNumber, String description) {
	this.branchNumber = branchNumber;
	this.description = description;
    }

    /**
     * ================= Getter Methods =======================
     * 
     * @return The branchNumber value
     */

    public int getBranchNumber() {
	return branchNumber;
    }

    /**
     * Gets the description attribute of the BranchVO object
     * 
     * @return The description value
     */
    public String getDescription() {
	return description;
    }

    /**
     * ================= Setter Methods =======================
     * 
     * @param description
     *            The new description value
     */

    public void setDescription(String description) {
	this.description = description;
    }

    /**
     * Description of the Method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
    }

    /**
     * Sets the branchNumber attribute of the BranchVO object
     * 
     * @param branchNumber
     *            The new branchNumber value
     */
    public void setBranchNumber(int branchNumber) {
	this.branchNumber = branchNumber;
    }

}
