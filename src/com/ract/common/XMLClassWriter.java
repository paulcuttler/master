package com.ract.common;

import java.io.IOException;
import java.io.Writer;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;

import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

/**
 * <p>
 * Implements a class writter that writes class data as an XML document. The
 * startClass() method must be called before the write methods and endClass
 * method. The endClass() method must be called last.
 * </p>
 * <p>
 * The class must be instantiated by passing a writer object. The writer is not
 * closed by the endClass() method but the reference to it is set to null so
 * that no more writting can be done.
 * </p>
 * <p>
 * The class can be instantiated with a list of classes to exclude from writing.
 * The class of each non primitive object passed to the writeAttribute() method
 * (e.g. not one of : int, float, double, boolean and String) will be checked
 * against the excluded class list and will only be written if it is not found
 * in the list. The excluded class list is a collection of classes obtained as
 * follows:
 * </p>
 * 
 * <pre>
 * ArrayList excludedClassList = new ArrayList();
 * excludedClassList.add(Class.forName(&quot;com.ract.membership.ProductVO&quot;));
 * </pre>
 * <p>
 * NOTE: This class is not serializable. You must pass it to a value objects
 * write method and not to an entity beans write method.
 * </p>
 * <p>
 * Some methods take an optional deepWrite flag. This flag is passed back to
 * attributes that implement writable so that they can decide how far down the
 * object map they will go when writing their output. The XML document will have
 * the following format. Values supplied to the write methods are depicted with
 * quotes. If the elements in an attribute list are not writable then the value
 * (as return by the toString() method of each element in the list will be
 * specified with the <value> </value> tag names.
 * </p>
 * 
 * <pre>
 *   <"className">
 *   <"attributeName">attributeValue</"attributeName">
 *   // if the element is not writable
 *   <"className">
 *   // if the element is writable
 *   <"attributeName">attributeValue</"attributeName">
 *   <"attributeName">attributeValue</"attributeName">
 *   <"attributeListName">
 *     <value>attributeValue</value>
 *     <value>attributeValue</value>
 *  </"attributeListName">
 *  </"className">
 *  <"attributeListName"> <value>attributeValue</value>
 *  // if the list element is not writable
 *  <value>attributeValue</value> </"attributeListName">
 *  <"attributeListName">
 *    <"className">
 *    // if the list element is writable
 *    <"attributeName">attributeValue</"attributeName">
 *    <"attributeListName">
 *      <value>attributeValue</value>
 *      <value>attributeValue</value>
 *    </"attributeListName">
 *    </"className">
 *  </"attributeListName">
 *  </"className">
 * </pre>
 * 
 * @author T. Bakker
 * @created 1 August 2002
 * @version 1.0
 */

public class XMLClassWriter extends ClassWriterBase {
    /**
     * The writer to use when outputing the classes data.
     */
    Writer writer;

    /**
     * The name of the class that is being written out.
     */

    String className;

    /**
     * A list of classes that are to be excluded from the output. If an object
     * passed to the writeAttribute method has the same class name as one of the
     * classes in this list then it will not be written out.
     */

    Collection classExclusionList;

    /**
     * Constructor **************************
     * 
     * @param writer
     *            Description of the Parameter
     * @param className
     *            Description of the Parameter
     * @param classList
     *            Description of the Parameter
     * @exception CreateException
     *                Description of the Exception
     */
    /**
     * Construct the class writer with a writer to output the result to. The
     * writer must be specified. A class name may be specified. If it is
     * specified it will override the class name passed to the startClass()
     * method. A list of excluded classes may be specified. If a non-primitive
     * class is being written and the class in the excluded class list then it
     * wont be written.
     * 
     * @param writer
     *            Description of the Parameter
     * @param className
     *            Description of the Parameter
     * @param classList
     *            Description of the Parameter
     * @exception CreateException
     *                Description of the Exception
     */

    public XMLClassWriter(Writer writer, String className, Collection classList) throws CreateException {
	if (writer == null) {
	    throw new CreateException("A writer must be specified.");
	}

	this.writer = writer;
	this.className = className;
	this.classExclusionList = classList;
    }

    /**
     * Methods ****************************
     * 
     * @exception RemoteException
     *                Description of the Exception
     */

    /**
     * Start writing to the writer. For XML documents this means writing the
     * first line of the document That identifies the document as containing XML
     * content.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void startDocument() throws RemoteException {
	write(XMLHelper.getXMLHeaderLine());
    }

    /**
     * Write the class name. The class name cannot be null. The endClass()
     * method cannot have been called. Use the class name that the writer was
     * constructed with if it is not null otherwise use the given class name.
     * 
     * @param className
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void startClass(String className) throws RemoteException {
	if (this.className == null) {
	    this.className = className;
	}

	checkClass();

	StringBuffer sb = new StringBuffer("<");
	sb.append(this.className);
	sb.append(">\n");
	write(sb.toString());
    }

    /**
     * Write the closing tag for the class. The writeClass() method must have
     * been called first. The endClass() method cannot have been called
     * previously. Once the method completes no more writting will be permitted
     * on the class writer. The refernce to the writer is set to null.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void endClass() throws RemoteException {
	checkClass();
	StringBuffer sb = new StringBuffer("</");
	sb.append(this.className);
	sb.append(">\n");
	write(sb.toString());
	this.className = null;
	this.writer = null;
    }

    /**
     * Write a string attribute. Default the attribute name to "value" if it is
     * not specified. The "value" attribute name should only be used for objects
     * in a list that do not implement the Writable interface.
     * 
     * @param attributeName
     *            Description of the Parameter
     * @param attribute
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void writeAttribute(String attributeName, String attribute) throws RemoteException {
	checkClass();
	if (attributeName == null || attributeName.length() < 1) {
	    attributeName = "value";
	}

	StringBuffer sb = new StringBuffer("<");
	sb.append(attributeName);
	sb.append(">");
	if (attribute != null) {
	    attribute = attribute.trim();
	    sb.append(XMLHelper.encode(attribute));
	}
	sb.append("</");
	sb.append(attributeName);
	sb.append(">\n");
	write(sb.toString());
    }

    /**
     * Write an attribute. The startClass() method must have been called first.
     * The attribute name cannot be null. The endClass() method cannot have been
     * called. If the attribute does not implement writable then it will be
     * written to the writer otherwise the attributes write() method will be
     * called to write the attributes values to the writer. If the attribute is
     * null then an empty tag will be written.
     * 
     * @param attributeName
     *            Description of the Parameter
     * @param obj
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void writeAttribute(String attributeName, Object obj) throws RemoteException {
	if (obj == null) {
	    String str = null;
	    // Need a null string to call the writeAttribute method that takes a
	    // string attribute.
	    writeAttribute(attributeName, str);
	} else if (obj instanceof Collection) {
	    // Handle lists sent to this method by mistake.
	    Collection list = (Collection) obj;
	    writeAttributeList(attributeName, list);
	} else if (classNotExcluded(obj)) {
	    if (obj instanceof Writable) {
		try {
		    write("<");
		    write(attributeName);
		    write(" type=\"Writable\">\n");

		    // Write the writable object using a new xml class writer
		    // sending the
		    // output to this classes writer.
		    Writable writableObj = (Writable) obj;
		    XMLClassWriter newWriter = new XMLClassWriter(this.writer, null, this.classExclusionList);
		    writableObj.write(newWriter);

		    write("</");
		    write(attributeName);
		    write(">\n");
		} catch (CreateException ce) {
		    throw new SystemException(ce);
		}
		// Don't obscure the exception
		// catch (RemoteException re){
		// // Log the error and continue.
		// LogUtil.log(this.getClass(),"Failed to write class attribute '"
		// + attributeName + "' : " + re.getMessage());
		// }
	    } else {
		// Use the attributes toString() method to write out its value.
		writeAttribute(attributeName, obj.toString());
	    }
	}
    }

    /**
     * Write an attribute list. The startClass() method must have been called
     * first. The attribute list name cannot be null or empty. The endClass()
     * method cannot have been called. If an element in the attribute list does
     * not implement writable then it will be written to the writer otherwise
     * the elements write() method will be called to write its attribute values
     * to the writer.
     * 
     * @param listName
     *            Description of the Parameter
     * @param list
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void writeAttributeList(String listName, Collection list) throws RemoteException {
	checkClass();
	if (listName == null || listName.length() < 1) {
	    throw new RemoteException("An attribute list name must be specified.");
	}

	// Write the start list element tag
	StringBuffer sb = new StringBuffer("<");
	sb.append(listName);
	sb.append(">\n");
	write(sb.toString());

	// Write the list elements
	if (list != null && !list.isEmpty()) {
	    Iterator listIT = list.iterator();
	    while (listIT.hasNext()) {
		writeListItem(listIT.next());
	    }
	}

	// Write the end list element tag
	sb = new StringBuffer("</");
	sb.append(listName);
	sb.append(">\n");
	write(sb.toString());
    }

    private void writeListItem(Object obj) throws RemoteException {
	if (obj == null) {
	    String str = null;
	    // Need a null string to call the writeAttribute method that takes a
	    // string attribute.
	    writeAttribute("value", str);
	} else if (obj instanceof Collection) {
	    // Handle lists sent to this method by mistake.
	    Collection list = (Collection) obj;
	    writeAttributeList("list", list);
	} else if (classNotExcluded(obj)) {
	    if (obj instanceof Writable) {
		Writable writableObj = (Writable) obj;
		try {
		    // Write the writable object using a new xml class writer
		    // sending the
		    // output to this classes writer.
		    XMLClassWriter newWriter = new XMLClassWriter(this.writer, null, this.classExclusionList);
		    writableObj.write(newWriter);
		} catch (CreateException ce) {
		    throw new SystemException(ce);
		} catch (RemoteException re) {
		    // Log the error and continue.
		    LogUtil.log(this.getClass(), "Failed to write class attribute '" + writableObj.getWritableClassName() + "' : " + re.getMessage());
		}
	    } else {
		// Use the attributes toString() method to write out its value.
		writeAttribute("value", obj.toString());
	    }
	}
    }

    /**
     * Throw and exception if the writer has been closed.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    private void checkWriter() throws RemoteException {
	if (this.writer == null) {
	    throw new RemoteException("Write sequence error. The class writer can no longer be used. The endWrite() method has been called.");
	}
    }

    /**
     * Throw and exception if writing to the class has ended.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    private void checkClass() throws RemoteException {
	if (this.className == null || this.className.length() < 1) {
	    throw new RemoteException("Write sequence error. The class name must be written first.");
	}
    }

    /**
     * Send the string to the writer.
     * 
     * @param str
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    private void write(String str) throws RemoteException {
	checkWriter();
	try {
	    this.writer.write(str);
	} catch (IOException ioe) {
	    throw new RemoteException(ioe.getMessage());
	}
    }

    /**
     * Search the excluded class list for the objects class. If found return
     * false otherwise true.
     * 
     * @param obj
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    private boolean classNotExcluded(Object obj) {
	// LogUtil.debug(this.getClass(),"classNotExcluded("+ (obj == null ?
	// "null" : obj.getClass().getName())+")");
	boolean notExcluded = true;
	if (obj != null) {
	    Class theClass = obj.getClass();
	    if (this.classExclusionList != null) {
		Class aClass = null;
		Iterator listIT = this.classExclusionList.iterator();
		while (listIT.hasNext() && notExcluded) {
		    aClass = (Class) listIT.next();
		    if (aClass.equals(theClass)) {
			notExcluded = false;
		    }
		    // LogUtil.debug(this.getClass(),"excluded class "+
		    // aClass.getName()+". notExcluded="+notExcluded);
		}
	    }
	}
	return notExcluded;
    }
}
