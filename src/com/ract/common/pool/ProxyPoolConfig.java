package com.ract.common.pool;

import org.apache.commons.pool.impl.GenericKeyedObjectPool.Config;
import org.w3c.dom.Node;

import com.ract.util.XDocument;

public class ProxyPoolConfig extends Config {

    XDocument doc;

    public ProxyPoolConfig() {
    }

    public String toXML() {

	if (doc == null) {
	    doc = createXML();
	}

	return doc.toXMLString();
    }

    private XDocument createXML() {
	XDocument doc = new XDocument();
	Node docRoot = doc.addNode(doc, "config");

	doc.addLeaf(docRoot, "maxActive", this.maxActive);
	doc.addLeaf(docRoot, "maxIdle", this.maxIdle);
	doc.addLeaf(docRoot, "maxTotal", this.maxTotal);
	doc.addLeaf(docRoot, "minIdle", this.minIdle);
	doc.addLeaf(docRoot, "numTestsPerEvictionRun", this.numTestsPerEvictionRun);
	doc.addLeaf(docRoot, "lifo", this.lifo);
	doc.addLeaf(docRoot, "testOnBorrow", this.testOnBorrow);
	doc.addLeaf(docRoot, "testOnReturn", this.testOnReturn);
	doc.addLeaf(docRoot, "testWhileIdle", this.testWhileIdle);
	doc.addLeaf(docRoot, "maxWait", this.maxWait);
	doc.addLeaf(docRoot, "minEvictableIdleTimeMillis", this.minEvictableIdleTimeMillis);
	doc.addLeaf(docRoot, "timeBetweenEvictionRunsMillis", this.timeBetweenEvictionRunsMillis);
	doc.addLeaf(docRoot, "whenExhaustedAction", this.whenExhaustedAction);

	return doc;
    }
}
