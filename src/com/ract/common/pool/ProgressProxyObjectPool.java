package com.ract.common.pool;

import org.apache.commons.pool.impl.GenericKeyedObjectPool;

import com.ract.common.SystemParameterVO;
import com.ract.util.LogUtil;

/**
 * Singleton class to provide access to a keyed object pool consisting of proxy
 * objects.
 * 
 * @author jyh
 * @version 1.0
 */

public class ProgressProxyObjectPool extends ProxyObjectPool {

    private ProgressProxyObjectPool() {
	// null
    }

    private static String proxyPoolKey = SystemParameterVO.CATEGORY_PROXY_POOL_RDS;

    private static GenericKeyedObjectPool progressProxyObjectPool;

    public synchronized static GenericKeyedObjectPool getInstance() {
	if (progressProxyObjectPool == null) {

	    GenericKeyedObjectPool.Config conf = new ProgressProxyObjectPool().getProxyPoolConfig();

	    log("ProgressProxyObjectPool ", "creating pool ");
	    progressProxyObjectPool = new GenericKeyedObjectPool(new ProgressProxyObjectFactory(), conf);

	}

	log("ProgressProxyObjectPool ", toString(progressProxyObjectPool));

	return progressProxyObjectPool;
    }

    public static void removeCurrentObjectPool() {
	if (progressProxyObjectPool != null) {
	    progressProxyObjectPool.clear();
	    try {
		progressProxyObjectPool.close();
	    } catch (Exception ex) {
		LogUtil.log("ProgressProxyObjectPool", "Unable to close the object pool. " + ex.getMessage());
	    }
	    progressProxyObjectPool = null;
	}
    }

    protected String getKey() {
	return proxyPoolKey;
    }
}
