package com.ract.common.pool;

import org.apache.commons.pool.impl.GenericKeyedObjectPool;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;
import com.ract.util.LogUtil;

public abstract class ProxyObjectPool {
    private static CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
    private static ProxyPoolConfig conf;

    protected abstract String getKey();

    protected ProxyPoolConfig getProxyPoolConfig() {
	if (conf == null) {
	    conf = createProxyPoolConfig();
	    LogUtil.log(this.getClass(), "Pool config for " + getKey() + " is " + conf.toXML());
	}

	return conf;
    }

    private ProxyPoolConfig createProxyPoolConfig() {

	String param;

	conf = new ProxyPoolConfig();

	param = getParameterValue(SystemParameterVO.PROXY_POOL_MAX_ACTIVE);
	if (param.equalsIgnoreCase("-1")) {
	    param = new Integer(GenericKeyedObjectPool.DEFAULT_MAX_ACTIVE).toString();
	}
	conf.maxActive = new Integer(param).intValue();

	param = getParameterValue(SystemParameterVO.PROXY_POOL_MAX_IDLE);
	if (param.equalsIgnoreCase("-1")) {
	    param = new Integer(GenericKeyedObjectPool.DEFAULT_MAX_IDLE).toString();
	}
	conf.maxIdle = new Integer(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_MAX_TOTAL);
	if (param.equalsIgnoreCase("-1")) {
	    param = new Integer(GenericKeyedObjectPool.DEFAULT_MAX_TOTAL).toString();
	}
	conf.maxTotal = new Integer(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_MAX_WAIT);
	if (param.equalsIgnoreCase("-1")) {
	    param = new Long(GenericKeyedObjectPool.DEFAULT_MAX_WAIT).toString();
	}
	conf.maxWait = new Long(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_TEST_ON_BORROW);
	conf.testOnBorrow = new Boolean(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_TEST_ON_RETURN);
	conf.testOnReturn = new Boolean(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_TEST_WHILE_IDLE);
	conf.testWhileIdle = new Boolean(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_WHEN_EXHAUSTED_ACTION);
	if (param.equalsIgnoreCase("FAIL")) {
	    param = new Long(GenericKeyedObjectPool.WHEN_EXHAUSTED_FAIL).toString();
	} else if (param.equalsIgnoreCase("BLOCK")) {
	    param = new Long(GenericKeyedObjectPool.WHEN_EXHAUSTED_BLOCK).toString();
	} else {
	    param = new Long(GenericKeyedObjectPool.WHEN_EXHAUSTED_GROW).toString();
	}
	conf.whenExhaustedAction = new Byte(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_MIN_EVICTABLE_IDLE_TIME_MILLIS);
	if (param.equalsIgnoreCase("-1")) {
	    param = new Long(GenericKeyedObjectPool.DEFAULT_MIN_EVICTABLE_IDLE_TIME_MILLIS).toString();
	}
	conf.minEvictableIdleTimeMillis = new Long(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_NUM_TESTS_PER_EVICTION_RUN);
	if (param.equalsIgnoreCase("-1")) {
	    param = new Integer(GenericKeyedObjectPool.DEFAULT_NUM_TESTS_PER_EVICTION_RUN).toString();
	}
	conf.numTestsPerEvictionRun = new Integer(param);

	param = getParameterValue(SystemParameterVO.PROXY_POOL_TIME_BETWEEN_EVICTION_RUNS_MILLIS);
	if (param.equalsIgnoreCase("-1")) {
	    param = new Long(GenericKeyedObjectPool.DEFAULT_TIME_BETWEEN_EVICTION_RUNS_MILLIS).toString();
	}
	conf.timeBetweenEvictionRunsMillis = new Long(param);

	return conf;
    }

    private String getParameterValue(String key) {
	return cMgr.getSystemParameter(getKey(), key).getValue();
    }

    protected static String toString(GenericKeyedObjectPool pool) {
	return "returning pool - " + pool.getClass().getName() + " Active " + pool.getNumActive() + " Idle " + pool.getNumIdle() + " pool " + pool.toString();
    }

    protected static void log(String poolName, String message) {
	LogUtil.log("POOL +++++ " + poolName, message);
    }

}
