package com.ract.common.pool;

import org.apache.commons.pool.KeyedPoolableObjectFactory;
import org.apache.commons.pool.impl.GenericKeyedObjectPool;

import com.progress.open4gl.Open4GLException;
import com.ract.client.ClientProgressProxy;
import com.ract.common.SourceSystem;
import com.ract.common.cad.CADProgressProxy;
import com.ract.common.proxy.ProgressProxy;
import com.ract.common.streetsuburb.StreetSuburbProgressProxy;
import com.ract.fleet.FleetProgressProxy;
import com.ract.insurance.InsuranceProgressProxy;
import com.ract.payment.directdebit.DDProgressProxy;
import com.ract.payment.finance.FinanceProgressProxy;
import com.ract.payment.receipting.OCRProgressProxy;
import com.ract.travel.TravelProgressProxy;
import com.ract.util.LogUtil;
import com.ract.vehicleinspection.VIProgressProxy;

/**
 * A pool factory for creating and managing Progress Proxy objects to avoid
 * costly connections to the Progress Application Server.
 * 
 * @author jyh
 * @version 1.0
 */

public class ProgressProxyObjectFactory implements KeyedPoolableObjectFactory {

    public final static String KEY_OCR_PROXY = SourceSystem.OCR.getAbbreviation();// "OCR"
    public final static String KEY_DD_PROXY = SourceSystem.DD.getAbbreviation();// "DD";

    // Insurance - retest with INS prefixes
    public final static String KEY_INSURANCE_RACT_PROXY = SourceSystem.INSURANCE_RACT.getAbbreviation();// "INS_RAC"

    public final static String KEY_FINANCE_PROXY = SourceSystem.FIN.getAbbreviation(); // FIN
    public final static String KEY_CLIENT_PROXY = SourceSystem.CLIENT.getAbbreviation(); // PROFILE_CLIENT

    // not implemented yet
    public final static String KEY_CAD_PROXY = SourceSystem.CAD.getAbbreviation();// CAD
    public final static String KEY_FLEET_PROXY = SourceSystem.FLEET.getAbbreviation();// FLEET
    public final static String KEY_VI_PROXY = SourceSystem.VI.getAbbreviation(); // VI

    public final static String KEY_TRAVEL_PROXY = SourceSystem.TRAVEL.getAbbreviation(); // VI
    public final static String KEY_SECURITY_PROXY = SourceSystem.SECURITY.getAbbreviation();
    public static final String KEY_OCV_PROXY = SourceSystem.OCV.getAbbreviation();

    public final static String KEY_STREETSUBURB_PROXY = SourceSystem.STREETSUBURB.getAbbreviation();

    public ProgressProxyObjectFactory() {
	// no action
    }

    /**
     * Make a progress proxy given a particular key.
     * 
     * @param key
     *            Object
     * @throws Exception
     * @return Object
     */
    public Object makeObject(Object key) throws java.lang.Exception {

	if (key == null) {
	    throw new Exception("Attempting to make an object with a null key.");
	}

	getPoolInfo(ProgressProxyObjectPool.getInstance(), key);

	if (key.toString().equals(KEY_OCR_PROXY)) {
	    return new OCRProgressProxy();
	} else if (key.toString().equals(KEY_DD_PROXY)) {
	    return new DDProgressProxy();
	} else if (key.toString().equals(KEY_CAD_PROXY)) {
	    return new CADProgressProxy();
	} else if (key.toString().equals(KEY_CLIENT_PROXY)) {
	    return new ClientProgressProxy();
	} else if (key.toString().equals(KEY_INSURANCE_RACT_PROXY)) {
	    // LogUtil.debug(this.getClass(),"systemName="+SourceSystem.INSURANCE_RACT.getSystemName());
	    return new InsuranceProgressProxy(KEY_INSURANCE_RACT_PROXY);
	    // return ((ProgressProxy)new
	    // InsuranceProgressProxy(SourceSystem.INSURANCE_RACT.getSystemName()));
	    // //system name = RACT
	} else if (key.toString().equals(KEY_FINANCE_PROXY)) {
	    return new FinanceProgressProxy();
	} else if (key.toString().equals(KEY_FLEET_PROXY)) {
	    return new FleetProgressProxy();
	} else if (key.toString().equals(KEY_VI_PROXY)) {
	    return new VIProgressProxy();
	} else if (key.toString().equals(KEY_TRAVEL_PROXY)) {
	    return new TravelProgressProxy();
	} else if (key.toString().equals(KEY_SECURITY_PROXY)) {
	    return new com.ract.security.SecurityProgressProxy();
	} else if (key.toString().equals(KEY_STREETSUBURB_PROXY)) {
	    return new StreetSuburbProgressProxy();
	} else {
	    throw new Exception("The object pool can not make objects with key '" + key.toString() + "'.");
	}
    }

    /**
     * Destroy a progress proxy object. Release the connection to the app
     * server.
     * 
     * @param key
     *            Object
     * @param object
     *            Object
     * @throws Exception
     */
    public void destroyObject(Object key, Object object) throws java.lang.Exception {
	ProgressProxy proxy = (ProgressProxy) object;
	LogUtil.debug(this.getClass(), "destroyObject getting progressProxyPool");
	GenericKeyedObjectPool progressProxyPool = ProgressProxyObjectPool.getInstance();
	if (proxy == null) {
	    LogUtil.debug(this.getClass(), "Destroying object but already null.");
	    return;
	}

	LogUtil.debug(this.getClass(), "Destroying object '" + key + "'. Object: " + object);
	LogUtil.debug(this.getClass(), "Before Total Active " + progressProxyPool.getNumActive() + " Idle " + progressProxyPool.getNumIdle());
	LogUtil.debug(this.getClass(), "Before Key Active " + progressProxyPool.getNumActive(key.toString()) + " Idle " + progressProxyPool.getNumIdle(key.toString()));
	try {
	    proxy._release();
	} catch (Open4GLException ex) {
	    LogUtil.fatal(this.getClass(), "Unable to release proxy. " + ex.getMessage());
	}

	LogUtil.debug(this.getClass(), "After Release Total Active " + progressProxyPool.getNumActive() + " Idle " + progressProxyPool.getNumIdle());
	LogUtil.debug(this.getClass(), "After Release Key Active " + progressProxyPool.getNumActive(key.toString()) + " Idle " + progressProxyPool.getNumIdle(key.toString()));

	// nullify object - the pool should do that for us?
	proxy = null;
	LogUtil.debug(this.getClass(), "After NULL Total Active " + progressProxyPool.getNumActive() + " Idle " + progressProxyPool.getNumIdle());
	LogUtil.debug(this.getClass(), "After NULL Key Active " + progressProxyPool.getNumActive(key.toString()) + " Idle " + progressProxyPool.getNumIdle(key.toString()));

    }

    /**
     * Validate a progress proxy object. If the object is still streaming (ie.
     * has activity associated with a connection) then the object is invalid.
     * Any exception when using the proxy will also result in an invalid object
     * flag.
     * 
     * @param key
     *            Object
     * @param object
     *            Object
     * @return boolean
     */
    public boolean validateObject(Object key, Object object) {
	LogUtil.debug(this.getClass(), "Validating object '" + key + "'. Object: " + object);
	LogUtil.debug(this.getClass(), "validateObject getting progressProxyPool");
	GenericKeyedObjectPool progressProxyPool = ProgressProxyObjectPool.getInstance();

	LogUtil.debug(this.getClass(), "Before validate Total Active " + progressProxyPool.getNumActive() + " Idle " + progressProxyPool.getNumIdle());
	LogUtil.debug(this.getClass(), "Before validate Key Active " + progressProxyPool.getNumActive(key.toString()) + " Idle " + progressProxyPool.getNumIdle(key.toString()));

	ProgressProxy proxy = (ProgressProxy) object;
	if (proxy == null) {
	    LogUtil.debug(this.getClass(), "Validating object but already null.");
	    return false;
	}

	Object connection = null;
	try {
	    connection = proxy._getConnectionId();
	    if (connection == null) {
		LogUtil.debug(this.getClass(), "Connection is null");
		return false;
	    }

	    LogUtil.debug(this.getClass(), "ConnectionId=" + connection.toString());
	} catch (Open4GLException ex1) {
	    LogUtil.warn(this.getClass(), "Connection error=" + ex1.toString());
	    return false;
	}

	boolean valid = true;
	String returnString = null;
	try {
	    valid = !proxy._isStreaming();
	    LogUtil.debug(this.getClass(), "Proxy not streaming=" + valid);
	    if (valid) {
		returnString = proxy._getProcReturnString();
		if (returnString != null && returnString.length() > 0) {
		    LogUtil.debug(this.getClass(), "Proxy return string=" + returnString);
		    valid = false;
		}
	    }
	} catch (Open4GLException ex) {
	    LogUtil.fatal(this.getClass(), "Validating object '" + key + "'. Exception: " + ex.getMessage());
	    // should not give out an invalid connection
	    valid = false;
	}
	LogUtil.debug(this.getClass(), "valid=" + valid);

	LogUtil.debug(this.getClass(), "Validated Total Active " + progressProxyPool.getNumActive() + " Idle " + progressProxyPool.getNumIdle());
	LogUtil.debug(this.getClass(), "Validated Key Active " + progressProxyPool.getNumActive(key.toString()) + " Idle " + progressProxyPool.getNumIdle(key.toString()));

	return valid;
    }

    /**
     * Get a progress proxy from the pool. There are no special requirements for
     * preparing the object as it is already connected.
     * 
     * @param key
     *            Object
     * @param object
     *            Object
     * @throws Exception
     */
    public void activateObject(Object key, Object object) throws java.lang.Exception {
	LogUtil.debug(this.getClass(), "Activating object '" + key + "'. Object: " + object);
    }

    /**
     * Return a progress object to the pool. All pending requests are cancelled.
     * 
     * @param key
     *            Object
     * @param object
     *            Object
     * @throws Exception
     */
    public void passivateObject(Object key, Object object) throws java.lang.Exception {
	ProgressProxy proxy = (ProgressProxy) object;
	LogUtil.debug(this.getClass(), "passivateObject getting progressProxyPool");
	GenericKeyedObjectPool progressProxyPool = ProgressProxyObjectPool.getInstance();

	if (proxy == null) {
	    LogUtil.debug(this.getClass(), "Passivating object but already null.");
	    return;
	}

	LogUtil.debug(this.getClass(), "Passivating object '" + key + "'. Object: " + object);
	LogUtil.debug(this.getClass(), "Passivate before Total Active " + progressProxyPool.getNumActive() + " Idle " + progressProxyPool.getNumIdle());
	LogUtil.debug(this.getClass(), "Passivate before Key " + key + " Active " + progressProxyPool.getNumActive(key.toString()) + " Idle " + progressProxyPool.getNumIdle(key.toString()));

	// cancel all pending requests
	try {
	    proxy._cancelAllRequests();
	} catch (Open4GLException ex) {
	    LogUtil.fatal(this.getClass(), "Unable to cancel all pending requests. Exception: " + ex.getMessage());
	}

	log(this.getClass(), "Passivated object '" + key + "'. Object: " + object);
	log(this.getClass(), "Passivate after Total Active " + progressProxyPool.getNumActive() + " Idle " + progressProxyPool.getNumIdle());
	log(this.getClass(), "Passivate after " + key + " Active " + progressProxyPool.getNumActive(key.toString()) + " Idle " + progressProxyPool.getNumIdle(key.toString()));

    }

    @SuppressWarnings("unchecked")
    public void log(Class thisClass, String message) {
	log(thisClass.getSimpleName(), message);
    }

    public void log(String name, String message) {
	LogUtil.log("PROXY OBJECT FACTORY " + name, message);
    }

    public void getPoolInfo(GenericKeyedObjectPool proxyPool, Object key) {

	log(this.getClass(), "Making object '" + key + "'. Total Active " + proxyPool.getNumActive() + " Idle " + proxyPool.getNumIdle());
	log(this.getClass(), "Making object '" + key + "'. KEY Active " + proxyPool.getNumActive(key.toString()) + " Idle " + proxyPool.getNumIdle(key.toString()));

    }
}
