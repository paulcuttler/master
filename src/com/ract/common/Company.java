/*
 * @(#)Company.java
 *
 */
package com.ract.common;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Vector;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import com.ract.util.LogUtil;

/**
 * Represent the different (RACT) companies.
 * 
 * @author Glenn Lewis
 * @version 1.0, 17/5/2002
 * 
 */
public final class Company implements Serializable, UserType {

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((myCompanyCode == null) ? 0 : myCompanyCode.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Company))
	    return false;
	Company other = (Company) obj;
	if (myCompanyCode == null) {
	    if (other.myCompanyCode != null)
		return false;
	} else if (!myCompanyCode.equals(other.myCompanyCode))
	    return false;
	return true;
    }

    /** A description of the company */
    private String myDescription = null;

    /** The company code */
    private String myCompanyCode = null;

    public Company() {

    }

    /**
     * Constructor. (Private constructor ensures that this class can only be
     * constructed by member methods.)
     */
    private Company(String description, String companyCode) {
	myDescription = description;
	myCompanyCode = companyCode;
    }

    /**
     * Get a description of the company
     * 
     * @return a string description of the credit card type
     */
    public String getDescription() {
	return myDescription;
    }

    /** Instance representing the RACT company */
    public static final Company RACT = new Company("RACT", "10");

    /** Instance representing the RACT insurance company */
    public static final Company RACTI = new Company("RACTI", "20");

    private static Vector myCompanies = new Vector();

    static {
	myCompanies.addElement(RACT);
	myCompanies.addElement(RACTI);
    }

    /**
     * Get the company code of a this company
     * 
     * @return the company code as a string
     */
    public String getCompanyCode() {
	return myCompanyCode;
    }

    /**
     * Get the company with a particular code
     * 
     * @param companyCode
     *            a string representing the company code
     * @throws RACTException
     *             if the company code is unknown
     */
    public static Company getCompany(String companyCode) throws GenericException {
	for (int i = 0; i < myCompanies.size(); i++) {
	    Company c = (Company) myCompanies.elementAt(i);
	    if (c.getCompanyCode().equals(companyCode)) {
		return c;
	    }
	}
	throw new GenericException("Unknown company code " + companyCode);
    }

    public String toString() {
	return this.myCompanyCode;
    }

    // hibernate user type interface methods

    int TYPES[] = { Types.VARCHAR };

    public int[] sqlTypes() {
	return TYPES;
    }

    public Class returnedClass() {
	return com.ract.common.Company.class;
    }

    public int hashCode(Object object) throws HibernateException {
	return object.hashCode();
    }

    public boolean equals(Object x, Object y) throws HibernateException {
	if (x == y) {
	    return true;
	}
	if (x == null) {
	    return false;
	}
	return x.equals(y);
    }

    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException {
	LogUtil.debug(this.getClass(), "nullSafeGet" + names[0]);
	Company company = null;
	String companyCode = resultSet.getString(names[0]);
	LogUtil.debug(this.getClass(), "companyCode=" + companyCode);
	if (companyCode != null) {
	    try {
		company = Company.getCompany(companyCode);
	    } catch (GenericException ex) {
		// ignore and return null
		LogUtil.warn(this.getClass(), ex);
	    }
	}
	LogUtil.debug(this.getClass(), "nullSafeGet" + company);
	return company;
    }

    public void nullSafeSet(PreparedStatement statement, Object value, int index) throws HibernateException, SQLException {
	if (value == null) {
	    statement.setNull(index, Types.VARCHAR);
	} else {
	    Company company = (Company) value;
	    statement.setString(index, company.getCompanyCode());
	}
    }

    public Object deepCopy(Object value) throws HibernateException {
	if (value == null) {
	    return null;
	} else {
	    Company company = (Company) value;
	    return new Company(company.getDescription(), company.getCompanyCode());
	}
    }

    public boolean isMutable() {
	return true;
    }

    public Serializable disassemble(Object value) throws HibernateException {
	return (Serializable) value;
    }

    public Object assemble(Serializable cached, Object owner) throws HibernateException {
	return cached;
    }

    public Object replace(Object original, Object target, Object owner) throws HibernateException {
	return deepCopy(original);
    }

}
