package com.ract.common;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import com.ract.util.LogUtil;

/**
 * <p>
 * Handle common cache operations
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class CacheHandler {

    public static Object getCacheValue(Object key, String cacheName) {
	Object value = null;
	net.sf.ehcache.Element element = null;
	CacheManager singletonManager = CacheManager.getInstance();
	Cache cache = singletonManager.getCache(cacheName);
	element = cache.get(key);
	if (element != null) {
	    value = element.getObjectValue();
	}
	LogUtil.debug("CacheHandler", "getCacheValue," + cacheName + ":" + key + "=" + value);
	return value;
    }

    public static void putCacheValue(Object key, Object value, String cacheName) {
	net.sf.ehcache.Element element = null;
	CacheManager singletonManager = CacheManager.getInstance();
	Cache cache = singletonManager.getCache(cacheName);
	net.sf.ehcache.Element newElement = new net.sf.ehcache.Element(key, value);
	LogUtil.debug("CacheHandler", "putCacheValue," + cacheName + ":" + key + "=" + newElement.getObjectValue());
	cache.put(newElement);
    }

    public static void removeCacheValue(Object key, String cacheName) {
	CacheManager singletonManager = CacheManager.getInstance();
	Cache cache = singletonManager.getCache(cacheName);
	LogUtil.debug("CacheHandler", "removeCacheValue," + cacheName + ":" + key);
	cache.remove(key);
    }

    public static void clearAll() {
	CacheManager singletonManager = CacheManager.getInstance();
	LogUtil.debug("CacheHandler", "clearAll");
	singletonManager.clearAll();
    }

    public static void removeAll() {
	CacheManager singletonManager = CacheManager.getInstance();
	LogUtil.debug("CacheHandler", "removeAll");
	singletonManager.removalAll();
    }

}
