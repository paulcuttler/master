package com.ract.common;

import java.rmi.RemoteException;

import com.ract.util.LogUtil;

/**
 * Stores system-wide constants
 * 
 * @todo consider use of properties file. unable to locate the actual file in
 *       the web context during testing.
 * 
 * @author John Holliday
 * @version 1.0
 */

public class CommonConstants {

    // system contexts
    public final static String DIR_MENU = "/menu";

    public final static String DIR_COMMON = "/common";

    public final static String DIR_CLIENT = "/client";

    public final static String DIR_HELP = "/help";

    public final static String DIR_INSURANCE = "/insurance";

    public final static String DIR_MEMBERSHIP = "/membership";

    public final static String DIR_MEMBERSHIP_REPORTS = DIR_MEMBERSHIP + "/reports";

    public final static String DIR_MEMBERSHIP_EXTRACTS = DIR_MEMBERSHIP + "/extracts";

    public final static String DIR_SECURITY = "/security";

    public final static String DIR_PAYMENT = "/payment";

    public final static String DIR_USER = "/user";

    public final static String DIR_DOJO_0_4_1 = "/js/dojo-0.4.3";

    public final static String DIR_DOJO_1_4_2 = "/js/dojo-1.4.2";

    // default page
    public final static String PAGE_Index = "/index.jsp";

    // common characters
    public final static String WILDCARD = "%";

    public final static String WHITESPACE = " ";

    // public final static String DELIMITER = ";";

    // cascading stylesheet
    private final static String DIR_STYLESHEET = "/css/ract.css";

    public final static int MAX_PRECISION = 10;

    // image directory
    public final static String DIR_IMAGES = "/images";

    // error directory
    public final static String DIR_ERROR = "/error";

    // scripts
    public final static String DIR_SCRIPTS = "/scripts";

    public static String getRACTStylesheet() {
	return "<link rel=\"stylesheet\" href=\"" + DIR_STYLESHEET + "\" type=\"text/css\">";
    }

    public static String getSortableStylesheet() {
	return "<link rel=\"stylesheet\" href=\"/css/sortable.css\" type=\"text/css\">";

    }

    public static void setLoginEnabled(boolean loginEnabled) {
	CommonConstants.loginEnabled = loginEnabled;
    }

    public static boolean isLoginEnabled() {
	return loginEnabled;
    }

    /**
     * max records to retrieve in a search
     */
    public final static int MAX_RECORDS = 100;

    /**
     * the jndi reference to the datasources.
     */
    public final static String DATASOURCE_CLIENT = "java:/ClientDS";

    // public final static String DATASOURCE_REPORTING = "java:/ReportingDS";

    public final static String DATASOURCE_PROSPER = "java:/ProsperDS";

    public final static String DATASOURCE_WEB = "java:/WebDS";

    public final static String DATASOURCE_DW = "java:/DWDS";

    public final static String DATASOURCE_EDW = "java:/EDWDS";

    /**
     * the jndi reference to the mail service.
     */
    public final static String SERVICE_MAIL = "java:/Mail";

    // public static final String PROVIDER_URL = "jnp://localhost:1099/";

    // public static final String DEFAULT_DIRECTORY = "/export/home/general";

    // for elixir - should be changed
    // public static final String REPORT_SERVER_NAME = "localhost";
    //
    // public static final String REPORT_SERVER_PORT = "7000";

    public static final String REPORT_WORKSPACE_MEMBERSHIP = "Membership";

    public static final String REPORT_WORKSPACE_INSURANCE = "Insurance";

    public static final String REPORT_WORKSPACE_TRAVEL = "Travel";
    public static final String REPORT_REPOSITORY = "/Ract";

    // Servlet name for reports data server
    public static final String REPORT_DATA_SERVER = "reportdataserver";

    /**
     * enable security
     */
    private static boolean loginEnabled = false;

    public final static String SYSTEM_DEVELOPMENT = "Development";
    public final static String SYSTEM_TEST = "Test";
    public final static String SYSTEM_PRODUCTION = "Production";
    public final static String SYSTEM_TRAINING = "Training";

    /**
     * User constants
     */
    public static final String USER_SYSTEM = "DAEMON";

    public static final String USER_IVR = "IVR";

    public static final String USER_OCR = "OCR";

    public static final String USER_ROADSIDE = "ROA";

    /**
     * Store in database
     */
    // public static String APP_SERVER_USERNAME = "gxl";
    //
    // public static String APP_SERVER_PASSWORD = "gxl1";

    public static final String INITIAL_CONTEXT = "org.jnp.interfaces.NamingContextFactory";

    public static String getProgressAppServerURL() {
	return getProgressAppserverURL(null);
    }

    public static String getProgressAppserverURL(String companyName) {
	String appServerURL = null;
	LogUtil.debug("com.ract.common.CommonConstants", "getProgressAppServerURL 1 " + appServerURL + " " + companyName);
	try {
	    if (companyName == null) {
		// default to RACT if no company name provided
		companyName = Company.RACT.getDescription();
	    }
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    appServerURL = commonMgr.getSystemParameterValue(SystemParameterVO.APPSERVER_URL, companyName);
	} catch (RemoteException ex) {
	    LogUtil.fatal("com.ract.common.CommonConstants", ex.getMessage());
	}
	LogUtil.debug("com.ract.common.CommonConstants", "getProgressAppServerURL 2 " + appServerURL + " " + companyName);
	return appServerURL;
    }

    public static String getProgressAppserverUser() {
	String userName = null;
	try {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    userName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.APPSERVER_USERNAME);
	} catch (RemoteException ex) {
	    LogUtil.fatal("com.ract.common.CommonConstants", ex.getMessage());
	}
	return userName;
    }

    public static String getProgressAppserverPassword() {
	String password = null;
	try {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    password = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.APPSERVER_PASSWORD);
	} catch (RemoteException ex) {
	    LogUtil.fatal("com.ract.common.CommonConstants", ex.getMessage());
	}
	return password;
    }

    public static String getReportServerName() {
	String reportServerName = null;
	try {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    reportServerName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.REPORT_SERVER_NAME);
	} catch (RemoteException ex) {
	    LogUtil.fatal("com.ract.common.CommonConstants", ex.getMessage());
	}
	return reportServerName;
    }

    public static String getReportServerPort() {
	String reportServerPort = null;
	try {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    reportServerPort = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.REPORT_SERVER_PORT);
	} catch (RemoteException ex) {
	    LogUtil.fatal("com.ract.common.CommonConstants", ex.getMessage());
	}
	return reportServerPort;
    }

}
