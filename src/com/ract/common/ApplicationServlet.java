package com.ract.common;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.ract.user.UserSession;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;

/**
 * Servlet that implements security and provides methods for useful servlet actions such as logging and parameter and attribute discovery.
 * 
 * @author John Holliday
 * @created 31 July 2002
 * @version 1.0, 3/3/2002
 */

public abstract class ApplicationServlet extends HttpServlet {
	protected final static String CONTENT_TYPE_HTML = "text/html";

	protected final static String CONTENT_TYPE_XML = "text/xml";

	private final String METHOD_PREFIX = "handleEvent_";

	private boolean fromExceptionHandler = false;

	public void setOverrideSecurity(boolean overrideSecurity) {
		this.overrideSecurity = overrideSecurity;
	}

	public boolean getOverrideSecurity() {
		return overrideSecurity;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	public static final String PARAMETER_EVENT = "event";

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
		response.setContentType(CONTENT_TYPE_HTML);

		String event = null;

		// Check that we have a file upload request
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		LogUtil.log(this.getClass(), "Multipart=" + isMultipart);
		if (!isMultipart) {
			// should at least be one event
			event = request.getParameter(PARAMETER_EVENT);

			if (event == null) {
				event = "";
			}
			try {
				handleEvent(event, request, response);
			} catch (Exception e) {
				// forward to error page
				handleException(request, response, e);
			}
		} else {
			/*
			 * try { // Create a factory for disk-based file items FileItemFactory factory = new DiskFileItemFactory();
			 * 
			 * // Create a new file upload handler ServletFileUpload upload = new ServletFileUpload(factory);
			 * 
			 * // Parse the request List items = upload.parseRequest(request);
			 * 
			 * LogUtil.log(this.getClass(),"items size="+items.size());
			 * 
			 * FileItem fileItem = null; // parse this request by the handler // this gives us a list of items from the request for(int i = 0; i < items.size(); i++) { fileItem = (FileItem)items.get(i); // LogUtil.log(this.getClass(),"fileItem="+fileItem.toString()); if(fileItem.isFormField()) { // LogUtil.log(this.getClass(), // "formfield item: " + fileItem.getFieldName() + ": " + fileItem.getString()); if(fileItem.getFieldName().equals(PARAMETER_EVENT)) { event = fileItem.getString(); } else { request.setAttribute(fileItem.getFieldName(), fileItem.getString()); } } else { // LogUtil.log(this.getClass(), // "non form field item: " + fileItem.getFieldName() + ": " + fileItem.getString()); //set as attributes request.setAttribute(fileItem.getFieldName(), fileItem); } } try { handleEvent(event, request, response); } catch(Exception e) { //forward to error page handleException(request, response, e); } } catch(FileUploadException ex) { throw new ServletException("Error parsing file.", ex); }
			 */
			throw new ServletException("Multipart requests are only supported for Struts 2 actions.");
		}
	}

	public static final String PARAMETER_FILE = "file";

	private boolean overrideSecurity;

	/**
	 * Service method
	 * 
	 * @param request
	 *            Description of the Parameter
	 * @param response
	 *            Description of the Parameter
	 * @exception ServletException
	 *                Description of the Exception
	 * @exception IOException
	 *                Description of the Exception
	 */
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserSession userSession = getUserSession(request);

		// check global
		if (getOverrideSecurity() == false && userSession == null) {
			SecurityException se = new SecurityException("You are not logged in or you are not authorised to view that page.");
			handleException(request, response, se);
			return;
		}

		this.outputSessionAttributes(request);

		// continue if all is well
		super.service(request, response);
	}

	protected UserSession getUserSession(HttpServletRequest request) throws RemoteException {
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		String securityOn = null;
		try {
			securityOn = FileUtil.getProperty("master", SystemParameterVO.SECURITY_ENABLED);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean securityEnabled = securityOn == null ? true : Boolean.valueOf(securityOn).booleanValue();
		LogUtil.debug(this.getClass(), "securityEnabled=" + securityEnabled);

		return ServletUtil.getUserSession(request, securityEnabled);
	}

	/**
	 * log the request parameters
	 * 
	 * @param request
	 *            the HTTP request.
	 */
	protected void outputRequestParameters(HttpServletRequest request) {
		Enumeration en = request.getParameterNames();
		String element = "";
		logEntry("Request Parameters:" + FileUtil.NEW_LINE);
		while (en.hasMoreElements()) {
			element = (String) en.nextElement();
			logEntry(element + ": " + request.getParameter(element));
		}
	}

	/**
	 * Log the request attributes
	 * 
	 * @param request
	 *            the HTTP request.
	 */
	protected void outputRequestAttributes(HttpServletRequest request) {
		Enumeration en = request.getAttributeNames();
		String element = "";
		logEntry("Request Attributes:" + FileUtil.NEW_LINE);
		while (en.hasMoreElements()) {
			element = (String) en.nextElement();
			logEntry(element + ": " + request.getAttribute(element));
		}
	}

	/**
	 * Log the session attributes.
	 * 
	 * @param request
	 *            the HTTP request
	 */
	protected void outputSessionAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Enumeration en = session.getAttributeNames();
		String element = "";
		logEntry("Session Attributes:");
		while (en.hasMoreElements()) {
			element = (String) en.nextElement();
			logEntry(element + ": " + session.getAttribute(element));
		}
	}

	/**
	 * Output text with separator above and below the text.
	 * 
	 * @param text
	 *            Description of the Parameter
	 */
	protected void logEntry(String text) {
		LogUtil.debug(this.getClass(), "[" + text + "]");
	}

	/**
	 * Get publicly available methods for the current class.
	 */
	protected void getMethods() {
		/*
		 * note that this code was simplified to work with methods inside a controller class.
		 */
		Class servletClass = this.getClass();
		log(servletClass.getName());
		try {
			Method methodList[] = servletClass.getMethods();
			for (int i = 0; i < methodList.length; i++) {
				log("method " + methodList[i].getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Use reflection to call the correct method given an event string (passed from a JSP. If the event it not found then handle the exception. The event method must be <b>public</b> .
	 * 
	 * @param event
	 *            the event that has triggered the response.
	 * @param request
	 *            Description of the Parameter
	 * @param response
	 *            Description of the Parameter
	 * @exception ServletException
	 *                Description of the Exception
	 */
	protected void handleEvent(String event, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		Class classTarget = this.getClass();
		String methodName = METHOD_PREFIX + event;
		Method methodTarget;
		try {
			Class[] parameterTypes = new Class[] { HttpServletRequest.class, HttpServletResponse.class };
			methodTarget = classTarget.getMethod(methodName, parameterTypes);
			Object[] arguments = new Object[] { request, response };
			methodTarget.invoke(this, arguments);
		} catch (NoSuchMethodException nsme) {
			RemoteException re = new RemoteException("The event handling method '" + methodName + "' in class '" + classTarget.getName() + "' does not exist!", nsme);
			handleException(request, response, re);
		} catch (Exception e) {
			LogUtil.debug(this.getClass(), "e=" + ExceptionHelper.getExceptionStackTrace(e));
			handleException(request, response, e);
		}
	}

	/**
	 * Handle the exception that was raised. If the exception was a ValidationException or it has a root cause of a ValidationException then pass the validation exception to the validation error page. Otherwise pass the exception to the standard error page. May throw IOExceptions or ServletExceptions if something serious has happened.
	 * 
	 * @todo may be handled via webapp error handling.
	 * 
	 * @param request
	 *            Description of the Parameter
	 * @param response
	 *            Description of the Parameter
	 * @param exception
	 *            Description of the Parameter
	 * @exception ServletException
	 *                Description of the Exception
	 */
	protected void handleException(HttpServletRequest request, HttpServletResponse response, Throwable exception) throws ServletException {
		Throwable rootCauseException = null;

		// An InvocationTargetException wraps the actual exception thrown
		if (exception instanceof InvocationTargetException) {
			InvocationTargetException tmpException = (InvocationTargetException) exception;
			exception = tmpException.getTargetException();
		}

		// If the exception is one of these then it may be wrapping
		// a more specific exception.
		if (exception instanceof ServletException || exception instanceof RemoteException || exception instanceof java.rmi.ServerException || exception instanceof javax.transaction.TransactionRolledbackException) {
			Throwable tmpException = exception;
			Throwable throwableException;
			boolean findingRootCause = true;
			while (findingRootCause) {

				if (tmpException == null) {
					throwableException = null;
				} else if (tmpException instanceof ServletException) {
					ServletException se = (ServletException) tmpException;
					throwableException = se.getRootCause();
				} else if (tmpException instanceof RemoteException) {
					RemoteException re = (RemoteException) tmpException;
					throwableException = re.detail;
				} else if (tmpException instanceof javax.transaction.TransactionRolledbackException) {
					javax.transaction.TransactionRolledbackException trbe = (javax.transaction.TransactionRolledbackException) tmpException;
					throwableException = trbe.detail;
				} else if (tmpException instanceof java.rmi.ServerException) {
					java.rmi.ServerException serverException = (java.rmi.ServerException) tmpException;
					throwableException = serverException.detail;
				} else {
					throwableException = null;
				}

				// These are the exceptions we really want to show to the end
				// user
				if (throwableException == null) {
					// Stop when there are no more root causes
					findingRootCause = false;
				} else if (throwableException instanceof ValidationException) {
					// Change the exception reference to the root cause.
					rootCauseException = throwableException;
					findingRootCause = false;
				} else if (throwableException instanceof WarningException) {
					// Change the exception reference to the root cause.
					rootCauseException = throwableException;
					findingRootCause = false;
				} else if (throwableException instanceof SystemException) {
					// Change the exception reference to the root cause.
					rootCauseException = throwableException;
					findingRootCause = false;
				} else if (throwableException instanceof NullPointerException) {
					// Change the exception reference to the root cause.
					rootCauseException = throwableException;
					findingRootCause = false;
				} else if (throwableException instanceof ServletException || throwableException instanceof RemoteException) {
					// Look for the next root cause.
					tmpException = throwableException;
				} else {
					// Can't determine the next root cause exception because
					// it's not a ServletException so stop
					findingRootCause = false;
				}
			}
		}

		request.setAttribute("exception", exception);
		request.setAttribute("rootCauseException", rootCauseException);

		String pageName = null;
		if (exception instanceof ValidationException) {
			pageName = "/error/ValidationError.jsp";
		} else if (exception instanceof WarningException) {
			pageName = "/error/Warning.jsp";
		} else if (exception instanceof SecurityException) {
			pageName = "/error/SecurityError.jsp";
		} else {
			pageName = "/error/StandardError.jsp";
		}
		fromExceptionHandler = true;
		forwardRequest(request, response, pageName);
	}

	/**
	 * Handle an unknown event. Forward to a page that can display the error.
	 * 
	 * @param request
	 *            Description of the Parameter
	 * @param response
	 *            Description of the Parameter
	 * @exception Exception
	 *                Description of the Exception
	 */
	protected void handleUnknownEvent(HttpServletRequest request, HttpServletResponse response) throws Exception {
		SystemException e = new SystemException("Unknown event name received : " + request.getParameter("event"));
		handleException(request, response, e);
	}

	/**
	 * Forward request to another page in the same context as the called UIC.
	 * 
	 * @param request
	 *            Description of the Parameter
	 * @param response
	 *            Description of the Parameter
	 * @param pageName
	 *            Description of the Parameter
	 * @exception ServletException
	 *                Description of the Exception
	 */
	protected void forwardRequest(HttpServletRequest request, HttpServletResponse response, String pageName) throws ServletException {
		try {
			RequestDispatcher rd = this.getServletContext().getRequestDispatcher(pageName);
			rd.forward(request, response);
		} catch (Exception e) {
			// Prevent infinite loop
			if (!fromExceptionHandler)
				handleException(request, response, e);
			else
				e.printStackTrace();
		}
	}

}
