package com.ract.common;

/**
 * Title:
 * Description:  The interface that class writers must implement.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT Ltd.
 * @author       T. Bakker.
 * @version      1.0
 */

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;

public interface ClassWriter {
    public void startClass(String className) throws RemoteException;

    public void endClass() throws RemoteException;

    public void writeAttribute(String attributeName, int attribute) throws RemoteException;

    public void writeAttribute(String attributeName, float attribute) throws RemoteException;

    public void writeAttribute(String attributeName, double attribute) throws RemoteException;

    public void writeAttribute(String attributeName, boolean attribute) throws RemoteException;

    public void writeAttribute(String attributeName, Integer attribute) throws RemoteException;

    public void writeAttribute(String attributeName, Float attribute) throws RemoteException;

    public void writeAttribute(String attributeName, Double attribute) throws RemoteException;

    public void writeAttribute(String attributeName, Boolean attribute) throws RemoteException;

    public void writeAttribute(String attributeName, String attribute) throws RemoteException;

    public void writeAttribute(String attributeName, Object attribute) throws RemoteException;

    public void writeAttributeList(String listName, Collection list) throws RemoteException;

    public void writeAttributeList(String listName, Hashtable list) throws RemoteException;
}