package com.ract.common;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

/**
 * A class to base implementations of a class writer on. Provides basic
 * functionality for writting primitive attributes. The implementation class
 * must implement the writing of strings, objects and lists.
 * 
 * @author T. Bakker
 * @version 1.0
 */

public abstract class ClassWriterBase implements ClassWriter {
    public abstract void startClass(String className) throws RemoteException;

    public void writeAttribute(String attributeName, int attribute) throws RemoteException {
	writeAttribute(attributeName, Integer.toString(attribute));
    }

    public void writeAttribute(String attributeName, float attribute) throws RemoteException {
	writeAttribute(attributeName, Float.toString(attribute));
    }

    public void writeAttribute(String attributeName, long attribute) throws RemoteException {
	writeAttribute(attributeName, Long.toString(attribute));
    }

    public void writeAttribute(String attributeName, double attribute) throws RemoteException {
	writeAttribute(attributeName, Double.toString(attribute));
    }

    public void writeAttribute(String attributeName, boolean attribute) throws RemoteException {
	writeAttribute(attributeName, new Boolean(attribute).toString());
    }

    public void writeAttribute(String attributeName, Integer attribute) throws RemoteException {
	if (attribute != null) {
	    writeAttribute(attributeName, attribute.toString());
	} else {
	    String str = null;
	    writeAttribute(attributeName, str);
	}
    }

    public void writeAttribute(String attributeName, Float attribute) throws RemoteException {
	if (attribute != null) {
	    writeAttribute(attributeName, attribute.toString());
	} else {
	    String str = null;
	    writeAttribute(attributeName, str);
	}
    }

    public void writeAttribute(String attributeName, Long attribute) throws RemoteException {
	if (attribute != null) {
	    writeAttribute(attributeName, attribute.toString());
	} else {
	    String str = null;
	    writeAttribute(attributeName, str);
	}
    }

    public void writeAttribute(String attributeName, Double attribute) throws RemoteException {
	if (attribute != null) {
	    writeAttribute(attributeName, attribute.toString());
	} else {
	    String str = null;
	    writeAttribute(attributeName, str);
	}
    }

    public void writeAttribute(String attributeName, Boolean attribute) throws RemoteException {
	if (attribute != null) {
	    writeAttribute(attributeName, attribute.toString());
	} else {
	    String str = null;
	    writeAttribute(attributeName, str);
	}
    }

    public void writeAttribute(String attributeName, BigDecimal attribute) throws RemoteException {
	if (attribute != null) {
	    writeAttribute(attributeName, attribute.toString());
	} else {
	    String str = null;
	    writeAttribute(attributeName, str);
	}
    }

    public abstract void writeAttribute(String attributeName, String attribute) throws RemoteException;

    public abstract void writeAttribute(String attributeName, Object attribute) throws RemoteException;

    public abstract void writeAttributeList(String listName, Collection list) throws RemoteException;

    public void writeAttributeList(String listName, Hashtable list) throws RemoteException {
	if (list != null) {
	    writeAttributeList(listName, list.values());
	} else {
	    ArrayList aList = null;
	    writeAttributeList(listName, aList);
	}
    }

    public abstract void endClass() throws RemoteException;

}
