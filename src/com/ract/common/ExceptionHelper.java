package com.ract.common;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Helper to manipulate exceptions and exception messages.
 * 
 * @author jyh
 * @version 1.0
 */

public class ExceptionHelper {

    /**
     * Return a string representation of the stack trace given an exception.
     * 
     * @param exception
     *            Exception
     * @return String
     */
    public static String getExceptionStackTrace(Exception exception) {

	StringWriter sw = new StringWriter();
	PrintWriter pw = new PrintWriter(sw);
	exception.printStackTrace(pw);
	String exceptionString = sw.toString();
	// close any resources
	pw.close();
	return exceptionString;
    }

}
