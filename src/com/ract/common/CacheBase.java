package com.ract.common;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import com.ract.util.LogUtil;

/**
 * Defines and implements the common methods that a cache class needs to manipulate cached items.
 * 
 * @author Terry Bakker
 * @created 2 April 2003
 * @version 1.0
 */

public abstract class CacheBase {

	/**
	 * The dataList holds the list of items that are cached.
	 */
	private ArrayList dataList = null;

	/**
	 * Return the data list. Initialise it if it is empty. The sub class must provide the implementation since it must add items to the list first if the list is empty.
	 * 
	 * @exception RemoteException Description of the Exception
	 */
	public abstract void initialiseList() throws RemoteException;

	/**
	 * Initialise the data list if it has not already been initialised.
	 * 
	 * @exception RemoteException Description of the Exception
	 */
	private synchronized void initList() throws RemoteException {
		LogUtil.info(this.getClass(), "Initialising list.");
		initialiseList();
	}

	/**
	 * Return the specified item object. Search the list of item for one that has the same key.
	 * 
	 * @param key Description of the Parameter
	 * @return The item value
	 * @exception RemoteException Description of the Exception
	 */
	protected CachedItem getItem(String key) throws RemoteException {
		// LogUtil.debug(this.getClass(),"getItem");
		CachedItem item = null;
		ArrayList theList = getItemList();
		if (theList != null) {
			Iterator it = theList.iterator();
			while (item == null && it.hasNext()) {
				CachedItem tmp = (CachedItem) it.next();
				// LogUtil.debug(this.getClass(),"CachedItem="+tmp.getKey()+" "+tmp.toString());
				if (tmp.keyEquals(key)) {
					item = tmp;
				}
			}
		}
		return item;
	}

	/**
	 * Return all of the cached items as an ArrayList. An initialised but empty ArrayList will be returned if there are no items in the cache. The method needs to be synchronised so that multiple threads don't try and initialise the list at the same time
	 * 
	 * @return The itemList value
	 * @exception RemoteException Description of the Exception
	 */
	protected ArrayList getItemList() throws RemoteException {
		if (isEmpty()) {
			initList();
		}
		return this.dataList;
	}

	/**
	 * Return a count of the number of elements in the data list.
	 * 
	 * @return The listSize value
	 * @exception RemoteException Description of the Exception
	 */
	protected int getListSize() throws RemoteException {
		return (this.dataList == null ? 0 : this.dataList.size());
	}

	/**
	 * Returns true if the cache does not contain any items.
	 * 
	 * @return The empty value
	 * @exception RemoteException Description of the Exception
	 */
	protected boolean isEmpty() throws RemoteException {
		return (this.dataList == null || this.dataList.isEmpty());
	}

	/**
	 * Add an item to the list.
	 * 
	 * @param item Description of the Parameter
	 */
	protected void add(CachedItem item) {
		if (this.dataList == null) {
			this.dataList = new ArrayList();
		}
		this.dataList.add(item);
	}

	/**
	 * Add a list of cached items to the list.
	 * 
	 * @param item Description of the Parameter
	 */
	protected void addAll(Collection cachedItemList) {
		if (this.dataList == null) {
			this.dataList = new ArrayList();
		}
		this.dataList.addAll(cachedItemList);
	}

	/**
	 * Replace the item in the cache.
	 * 
	 * @param item Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	protected void replace(CachedItem item) throws Exception {
		CachedItem tmpItem = null;
		String itemKey = item.getKey();

		// Find the item in the cache with the matching key.
		ArrayList theList = getItemList();
		if (theList != null) {
			Iterator it = theList.iterator();
			while (it.hasNext() && tmpItem == null) {
				CachedItem tmp = (CachedItem) it.next();
				if (tmp.keyEquals(itemKey)) {
					tmpItem = tmp;
				}
			}
		}
		if (tmpItem != null) {
			// Remove the matching item and add the new item.
			theList.remove(item);
			theList.add(item);
		} else {
			// Could not find the item so chuck a wobbly.
			throw new Exception("Error replacing item with key '" + itemKey + "'. It does not exist in the cache.");
		}
	}

	/**
	 * Remove the item from the cache
	 * 
	 * @param item Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	protected void remove(CachedItem item) throws Exception {
		ArrayList theList = getItemList();
		if (theList != null) {
			this.dataList.remove(item);
		} else {
			throw new Exception("Failed to remove item from cache. Cache is empty.");
		}
	}

	/**
	 * Reset the cache to empty so that the next reference to it causes a reload.
	 */
	public void reset() {
		this.dataList = null;
	}

	/**
	 * Sort the list in ascending order. Only sort the list if it has more than one element in it and the elements implement the Comparable interface.
	 */
	public void sort() {
		if (this.dataList != null && this.dataList.size() > 1 && this.dataList.get(0) instanceof Comparable) {
			Collections.sort(this.dataList);
		}
	}

}
