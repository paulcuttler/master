package com.ract.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ract.util.DateTime;

/**
 * the tax rate value object
 * 
 * @author ? Terry Bakker?
 * @version 1.0
 */
@Entity
@Table(name = "tax")
public class TaxRateVO extends ValueObject implements CachedItem {

    @Id
    @Column(name = "[tax-date]")
    protected DateTime taxDate;

    protected double gst;

    @Column(name = "[stamp-duty]")
    protected double stampDuty;

    public TaxRateVO() {
    }

    public TaxRateVO(DateTime taxDate, double gst, double stampDuty) {
	this.taxDate = taxDate;
	this.gst = gst;
	this.stampDuty = stampDuty;
    }

    public DateTime getTaxDate() {
	return taxDate;
    }

    public double getGstRate() {
	return gst;
    }

    public void setGstRate(double gst) {
	this.gst = gst;
    }

    public double getStampDuty() {
	return stampDuty;
    }

    public void setStampDuty(double stampDuty) {
	this.stampDuty = stampDuty;
    }

    public TaxRateVO copy() {
	TaxRateVO trVO = new TaxRateVO(this.taxDate, this.gst, this.stampDuty);
	trVO.setReadOnly(true);
	return trVO;
    }

    /******************** CachedItem interface methods **********************/
    public String getKey() {
	return this.taxDate.formatShortDate();
    }

    public boolean keyEquals(String key) {
	if (getKey().equals(key)) {
	    return true;
	} else {
	    return false;
	}
    }

}