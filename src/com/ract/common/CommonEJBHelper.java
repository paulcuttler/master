package com.ract.common;

import java.rmi.RemoteException;

import com.ract.common.cad.CadCommonMgr;
import com.ract.common.cad.CadCommonMgrLocal;
import com.ract.common.cad.CadExportMgr;
import com.ract.common.cad.CadExportMgrLocal;
import com.ract.common.extracts.ExtractMgr;
import com.ract.common.integration.IntegrationMgr;
import com.ract.common.notifier.NotificationMgr;
import com.ract.common.notifier.NotificationMgrLocal;
import com.ract.common.reporting.ReportMgr;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.common.test.TestMgr;
import com.ract.help.HelpMgr;

/**
 * Description of the Class
 * 
 * @author bakkert
 * @created 19 February 2003
 */
public class CommonEJBHelper {

    public static CadCommonMgr getCadCommonMgr() {
	CadCommonMgr cadCommonMgr = null;
	try {
	    cadCommonMgr = (CadCommonMgr) ServiceLocator.getInstance().getObject("CadCommonMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    e.printStackTrace();
	}
	return cadCommonMgr;
    }

    public static CadCommonMgrLocal getCadCommonMgrLocal() {
	CadCommonMgrLocal cadCommonMgrLocal = null;
	try {
	    cadCommonMgrLocal = (CadCommonMgrLocal) ServiceLocator.getInstance().getObject("CadCommonMgrBean/local");
	} catch (ServiceLocatorException e) {
	    e.printStackTrace();
	}
	return cadCommonMgrLocal;
    }

    /**
     * Return a reference to CadExportMgrHome interface
     * 
     * @return The cadExportMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static CadExportMgr getCadExportMgr() {
	CadExportMgr cadExportMgr = null;
	try {
	    cadExportMgr = (CadExportMgr) ServiceLocator.getInstance().getObject("CadExportMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return cadExportMgr;
    }

    public static CadExportMgrLocal getCadExportMgrLocal() {
	CadExportMgrLocal cadExportMgrLocal = null;
	try {
	    cadExportMgrLocal = (CadExportMgrLocal) ServiceLocator.getInstance().getObject("CadExportMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return cadExportMgrLocal;
    }

    public static TestMgr getTestMgr() {
	TestMgr testMgr = null;
	try {
	    testMgr = (TestMgr) ServiceLocator.getInstance().getObject("TestMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return testMgr;
    }

    public static HelpMgr getHelpMgr() {
	HelpMgr helpMgr = null;
	try {
	    helpMgr = (HelpMgr) ServiceLocator.getInstance().getObject("HelpMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return helpMgr;
    }

    /**
     * Get the scheduler mgr remote interface
     */
    public static ScheduleMgr getScheduleMgr() {
	ScheduleMgr scheduleMgr = null;
	try {
	    scheduleMgr = (ScheduleMgr) ServiceLocator.getInstance().getObject("ScheduleMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return scheduleMgr;
    }

    /**
     * get the report mgr remote interface
     */
    public static ReportMgr getReportMgr() {
	ReportMgr reportMgr = null;
	try {
	    reportMgr = (ReportMgr) ServiceLocator.getInstance().getObject("ReportMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return reportMgr;
    }

    /**
     * Gets the commonMgr attribute of the CommonEJBHelper class
     * 
     * @return The commonMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static CommonMgr getCommonMgr() {
	CommonMgr commonMgr = null;
	try {
	    commonMgr = (CommonMgr) ServiceLocator.getInstance().getObject("CommonMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return commonMgr;
    }

    public static CommonMgrLocal getCommonMgrLocal() {
	CommonMgrLocal commonMgrLocal = null;
	try {
	    commonMgrLocal = (CommonMgrLocal) ServiceLocator.getInstance().getObject("CommonMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return commonMgrLocal;
    }

    public static IntegrationMgr getIntegrationMgr() {
	IntegrationMgr integrationMgr = null;
	try {
	    integrationMgr = (IntegrationMgr) ServiceLocator.getInstance().getObject("IntegrationMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return integrationMgr;
    }

    /**
     * Gets the mailMgr attribute of the CommonEJBHelper class
     * 
     * @return The mailMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static MailMgr getMailMgr() {
	MailMgr mailMgr = null;
	try {
	    mailMgr = (MailMgr) ServiceLocator.getInstance().getObject("MailMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return mailMgr;
    }

    /**
     * Return a reference to NotificationMgr interface. NOTE: The
     * NotificationMgr is a STATEFUL session bean. A static reference to it must
     * not be held here.
     * 
     * @return The notificationMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static NotificationMgr getNotificationMgr() {
	NotificationMgr notificationMgr = null;
	try {
	    notificationMgr = (NotificationMgr) ServiceLocator.getInstance().getObject("NotificationMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return notificationMgr;
    }

    public static NotificationMgrLocal getNotificationMgrLocal() {
	NotificationMgrLocal notificationMgrLocal = null;
	try {
	    notificationMgrLocal = (NotificationMgrLocal) ServiceLocator.getInstance().getObject("NotificationMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return notificationMgrLocal;
    }

    /**
     * Gets the streetSuburbMgr attribute of the CommonEJBHelper class
     * 
     * @return The streetSuburbMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static StreetSuburbMgr getStreetSuburbMgr() {
	StreetSuburbMgr streetSuburbMgr = null;
	try {
	    streetSuburbMgr = (StreetSuburbMgr) ServiceLocator.getInstance().getObject("StreetSuburbMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return streetSuburbMgr;
    }

    public static StreetSuburbMgrLocal getStreetSuburbMgrLocal() {
	StreetSuburbMgrLocal streetSuburbMgrLocal = null;
	try {
	    streetSuburbMgrLocal = (StreetSuburbMgrLocal) ServiceLocator.getInstance().getObject("StreetSuburbMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return streetSuburbMgrLocal;
    }

    /**
     * Return a reference to the ExtractMgr entity bean home interface.
     */
    public static ExtractMgr getExtractMgr() {
	ExtractMgr extractMgr = null;
	try {
	    extractMgr = (ExtractMgr) ServiceLocator.getInstance().getObject("ExtractMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return extractMgr;
    }

    public static SequenceMgr getSequenceMgr() {
	SequenceMgr sequenceMgr = null;
	try {
	    sequenceMgr = (SequenceMgr) ServiceLocator.getInstance().getObject("SequenceMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return sequenceMgr;
    }

}
