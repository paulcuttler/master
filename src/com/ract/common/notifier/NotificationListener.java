package com.ract.common.notifier;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemException;
import com.ract.util.XMLHelper;

/**
 * This servlet handles notification events raised by publishers and sent as XML
 * over HTTP. It listens for a POST request, extracts the XML document submitted
 * in the body of the POST request, determines what type of notification event
 * is being published and then converts the event into a call to the
 * notification manager which handles the distribution of the event to the event
 * subscribers.
 * 
 * @author bakkert
 * @created 21 February 2003
 */
public class NotificationListener extends HttpServlet {
    private final static String CONTENT_TYPE_XML = "text/xml";

    private static final String STATUS_OK = "ok";

    private static final String STATUS_FAIL = "fail";

    /**
     * Initialize global variables
     * 
     * @exception ServletException
     *                Description of the Exception
     */
    public void init() throws ServletException {
    }

    /**
     * Process the HTTP Get request. Not used at the moment.
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// All responces are xml formatted
	response.setContentType(CONTENT_TYPE_XML);
	PrintWriter out = response.getWriter();
	out.println(XMLHelper.getXMLHeaderLine());
	out.println("<Response><Status>" + STATUS_FAIL + "</Status><ErrorMessage>GET requests are not supported</ErrorMessage></Response>");
    }

    /**
     * Process the HTTP Post request
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String status = STATUS_OK;
	String errorMessage = "";

	String contentType = request.getContentType();
	// LogUtil.debug(this.getClass(),"contentType="+contentType);

	try {
	    if (CONTENT_TYPE_XML.equalsIgnoreCase(contentType)) {
		processXMLPost(request, response);
	    } else {
		status = STATUS_FAIL;
		errorMessage = "Unknown content type '" + contentType + "'.";
	    }
	} catch (Exception e) {
	    status = STATUS_FAIL;
	    errorMessage = e.getMessage();
	}

	// Send a response
	response.setContentType(CONTENT_TYPE_XML);
	PrintWriter out = response.getWriter();
	out.println(XMLHelper.getXMLHeaderLine());
	out.println("<Response><Status>" + status + "</Status><ErrorMessage>" + errorMessage + "</ErrorMessage></Response>");
    }

    private void processXMLPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
	// Parse the XML document passed in the body of the post request into an
	// XML document object model.
	// Use an input stream to get the XML document parser to read directly
	// from the HTTP post request.
	ServletInputStream stream = request.getInputStream();

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	factory.setValidating(false);
	DocumentBuilder builder = factory.newDocumentBuilder();
	Document document = builder.parse(stream);
	Element documentElement = document.getDocumentElement();
	// What sort of xml document is it?
	if ("Integration".equalsIgnoreCase(documentElement.getNodeName())) {
	    CommonEJBHelper.getIntegrationMgr().processIntegrationDocument(documentElement);
	} else {
	    throw new SystemException("Unknown XML document type " + document.getNodeName());
	}
    }

    /**
     * Process the event detail node as a client change event. The event detail
     * should contian two tags - The action taken on the client. - The client
     * number that was affected. <EventDetail> <Action>Create</Action>
     * <ClientNumber>1234</ClientNumber> </EventDetail>
     * 
     * @param eventDetailNode
     *            The node that contains the event detail
     */
    // // Findout the name of the notification event specified in the document
    // // Should be one event name node.
    // NodeList nodeList = document.getElementsByTagName("EventName");
    // Node eventNameNode = nodeList.item(0);
    //
    // // Should be one event detail node
    // nodeList = document.getElementsByTagName("EventDetail");
    // Node eventDetailNode = nodeList.item(0);
    //
    // if (eventNameNode != null && eventDetailNode != null) {
    // System.out.println("node=" + eventNameNode.getNodeName() + "," +
    // eventNameNode.getFirstChild().getNodeValue());
    // if
    // (NotificationEvent.CLIENT_CHANGE_EVENT.equals(eventNameNode.getFirstChild().getNodeValue()))
    // {
    // processClientChangeNotification(eventDetailNode);
    // }
    // }

    // private void processClientChangeNotification(Node eventDetailNode)
    // {
    // try {
    // NodeList nodeList = eventDetailNode.getChildNodes();
    //
    // String action = nodeList.item(0).getFirstChild().getNodeValue();
    // String clientNumberStr = nodeList.item(1).getFirstChild().getNodeValue();
    // LogUtil.debug(this.getClass(),"processClientChangeNotification(): action="+action+", clientNumber="
    // + clientNumberStr);
    // // Send the event to the event notificiation manager.
    // NotificationEvent event = new ClientChangeNotificationEvent(action,new
    // Integer(clientNumberStr));
    // CommonEJBHelper.getNotificationMgr().notifyEvent(event);
    // }
    // catch (Exception e) {
    // e.printStackTrace();
    // }
    // }

    /**
     * Clean up resources
     */
    public void destroy() {
    }
}