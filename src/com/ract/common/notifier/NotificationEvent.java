package com.ract.common.notifier;

/**
 * Represents a notifiable event.
 * @author
 * @version 1.0
 */

import java.io.Serializable;

import com.ract.util.DateTime;

public abstract class NotificationEvent implements Serializable {

    public final static String MODE_SYNCHRONOUS = "synchronous";
    public final static String MODE_ASYNCHRONOUS = "asynchronous";

    public static final String EVENT_CLIENT_CHANGE = "clientChange";

    public static final String EVENT_MEMBERSHIP_CHANGE = "membershipChange";

    public static final String EVENT_PROGRESS_CLIENT_CHANGE = "progressClientChange";

    public static final String EVENT_PAYMENT_NOTIFICATION = "paymentNotification";

    public static final String EVENT_PENDING_FEE_PAID = "pendingFeePaid";

    public static final String EVENT_PAYMENT_CANCELLATION = "paymentCancellationEvent";

    public static final String EVENT_PROGRESS_STREET_SUBURB_CHANGE = "progressStreetSuburb";

    /**
     * Integrator event initiated by a class name being specified in a
     * integration document.
     */
    public static final String EVENT_PRINT_INTEGRATOR = "printIntegrator";

    /**
     * A time in milliseconds to delay sending the event to subscribers. If a
     * delay from has not been set then the delay will start when the
     * notification manager gets around to processing the event. If a delay from
     * has been specified then the event will not be sent to subscribers until
     * the delay from plus the delay is less than the current time.
     */
    private long delay = 0;

    /**
     * The delay from time in milliseconds.
     */
    private long delayFrom = new DateTime().getTime();

    private String eventName = null;

    /**
     * The time the event was raised.
     */
    private DateTime eventTime = new DateTime();

    protected String eventMode;

    public String getEventName() {
	return this.eventName;
    }

    protected void setEventName(String name) {
	this.eventName = name;
    }

    public DateTime getEventTime() {
	return this.eventTime;
    }

    public boolean eventEquals(String name) {
	if (this.eventName != null) {
	    return this.eventName.equals(name);
	} else {
	    return false;
	}
    }

    public long getDelay() {
	return this.delay;
    }

    public long getDelayFrom() {
	return this.delayFrom;
    }

    public String getEventMode() {
	return eventMode;
    }

    public void setDelay(long delay) {
	this.delay = delay;
    }

    public void setDelayFrom(long delayFrom) {
	this.delayFrom = delayFrom;
    }

    public void setEventMode(String mode) {
	this.eventMode = mode;
    }

    public String getDetail() {
	return "";
    }

    public String toString() {
	return this.eventName;
    }
}
