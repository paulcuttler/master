package com.ract.common.notifier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import com.ract.common.ExceptionHelper;
import com.ract.common.RollBackException;
import com.ract.common.SystemException;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Description of the Class
 * 
 * @author bakkert
 * @created 28 February 2003
 */
@Stateful
@Remote({ NotificationMgr.class })
@Local({ NotificationMgrLocal.class })
public class NotificationMgrBean {
    /**
     * A list to hold events until they are committied. If the list is null then
     * there are no events to commit.
     */
    private ArrayList eventList;

    /**
     * Add an event to be processed. The rollback exception is there to indicate
     * that the NotificationMgr must be committed or rolled back.
     * 
     * @param event
     *            Description of the Parameter
     * @exception RollBackException
     *                Description of the Exception
     */
    public void notifyEvent(NotificationEvent event) throws RollBackException {
	LogUtil.debug(this.getClass(), "adding event " + event.toString());
	// Initialise the list if this is the first call
	if (this.eventList == null) {
	    this.eventList = new ArrayList();
	}
	this.eventList.add(event);
    }

    /**
     * Handle the sending of events raised by publishers to the subscribers to
     * the event.
     * 
     * @todo if an exception is thrown then it may leave the "transaction" in an
     *       inconsistent state.
     * 
     */
    public void commit() throws RemoteException, RollBackException {
	String eventMode = null;
	// Create a worker to do the actual notification of the event to the
	// subscribers so that the publisher is not delayed by what the
	// subscriber
	// has to do.
	if (this.eventList != null && !this.eventList.isEmpty()) {
	    LogUtil.debug(this.getClass(), "start of run thread");

	    NotificationEvent event = null;
	    for (int eventIndex = 0; eventIndex < this.eventList.size(); eventIndex++) {
		// get the event
		event = (NotificationEvent) this.eventList.get(eventIndex);

		eventMode = event.getEventMode();

		LogUtil.debug(this.getClass(), "Event=" + event.getEventName() + ", Detail=" + event.toString() + ", Mode=" + eventMode);

		// default to asynchronous processing.
		if (eventMode == null) {
		    eventMode = NotificationEvent.MODE_ASYNCHRONOUS;
		}

		EventWorker worker = new EventWorker(event);
		// wait for execution of child thread
		if (eventMode.equals(NotificationEvent.MODE_SYNCHRONOUS)) {
		    // LogUtil.debug(this.getClass(), "b4 start");
		    worker.start();
		    try {
			LogUtil.debug(this.getClass(), "Synchronous");
			// join worker thread to this caller
			// to allow synchronous execution
			worker.join();
			LogUtil.debug(this.getClass(), "Joined thread");
			// after joining the thread we can propagate exceptions
			// to the calling client. As the execution is sequential
			// the get event should have the results of running the
			// thread.
			NotificationEventFailure notificationEventFailure = worker.getEventFailure();
			if (notificationEventFailure != null) {
			    StringBuffer error = new StringBuffer();
			    error.append(notificationEventFailure.getNotificationEvent().getEventName());

			    Exception notificationEventException = notificationEventFailure.getException();
			    if (notificationEventException != null) {
				error.append(" root cause: ");
				error.append(notificationEventException.getMessage());
			    }

			    // Build error up
			    ArrayList failedSubscriberList = notificationEventFailure.getFailedSubscriberList();
			    if (failedSubscriberList != null && failedSubscriberList.size() > 0) {
				LogUtil.log(this.getClass(), "failedSubscriberList size=" + failedSubscriberList.size());
				error.append(" failed subscribers: ");
				NotificationEventSubscriberFailure eventSubscriberFailure = null;
				for (int i = 0; i < failedSubscriberList.size(); i++) {
				    if (i > 0) {
					error.append(",");
				    }
				    eventSubscriberFailure = (NotificationEventSubscriberFailure) failedSubscriberList.get(i);
				    error.append(eventSubscriberFailure.getSubscriber().getSubscriberName());
				    error.append(": ");
				    error.append(eventSubscriberFailure.getException().getMessage());
				}
			    }
			    throw new RollBackException(error.toString());
			}
		    } catch (InterruptedException ex) {
			// ignore?
			LogUtil.fatal(this.getClass(), ex);
		    }
		    LogUtil.debug(this.getClass(), "Processed event");
		}
		// don't wait for child thread to finish
		else if (eventMode.equals(NotificationEvent.MODE_ASYNCHRONOUS)) {
		    LogUtil.debug(this.getClass(), "Asynchronous");
		    worker.start();
		}
	    }
	}

	LogUtil.debug(this.getClass(), "End of commit");

	// Set the list back to null to indicate that the events have been
	// processed.
	this.eventList = null;
    }

    /**
     * Throw away the list of events without processing them
     */
    public void rollback() {
	this.eventList = null;
    }

    /**
     * Delete the specified notification failure file.
     */
    public void deleteNotificationFailure(String fileName) throws RemoteException {
	File failureDirectory = NotificationUtil.getFailureDirectory();

	if (failureDirectory != null && failureDirectory.exists()) {
	    File eventFailureFile = new File(failureDirectory, fileName);
	    if (eventFailureFile.exists()) {
		eventFailureFile.delete();
	    }
	}
    }

    /**
     * Try and re-send all notification events that have failed.
     */
    public void resendAllNotificationEventFailures() throws RemoteException {
	NotificationEventFailure eventFailure = null;
	ArrayList eventFailureList = getNotificationFailureList();

	for (int eventIndex = 0; eventIndex < eventFailureList.size(); eventIndex++) {
	    eventFailure = (NotificationEventFailure) eventFailureList.get(eventIndex);
	    resendNotificationEventFailure(eventFailure);
	}
    }

    /**
     * Try and re-send the specified notification event identified by its file
     * name.
     */
    public void resendNotificationFailure(String fileName) throws RemoteException {
	File failureDirectory = NotificationUtil.getFailureDirectory();

	if (failureDirectory != null && failureDirectory.exists()) {
	    File eventFailureFile = new File(failureDirectory, fileName);
	    NotificationEventFailure eventFailure = this.getNotificationFailure(eventFailureFile);
	    if (eventFailure != null) {
		resendNotificationEventFailure(eventFailure);
	    }
	}
    }

    /**
     * Try and re-send the specified notification event.
     */
    private void resendNotificationEventFailure(NotificationEventFailure eventFailure) throws RemoteException {
	NotificationEvent event = eventFailure.getNotificationEvent();
	ArrayList failedSubscriberList = eventFailure.getFailedSubscriberList();
	ArrayList subscriberList = null;
	NotificationEventSubscriber subscriber;

	if (failedSubscriberList != null) {
	    // Get the list of subscribers to send the event to from the
	    // failed subscriber list stored with the event failure
	    subscriberList = new ArrayList();
	    NotificationEventSubscriberFailure subscriberFailure;

	    for (int i = 0; i < failedSubscriberList.size(); i++) {
		subscriberFailure = (NotificationEventSubscriberFailure) failedSubscriberList.get(i);
		subscriberList.add(subscriberFailure.getSubscriber());
	    }
	}
	// If the failure event didn't have the list of subscribers
	// that failed to process the event then we assume all of the
	// subscribers failed to process the event
	else {
	    subscriberList = SubscriberCache.getInstance().getSubscriberList(event.getEventName());
	}

	// Pass on the event to all subscribers.
	if (subscriberList != null) {
	    for (int i = 0; i < subscriberList.size(); i++) {
		subscriber = (NotificationEventSubscriber) subscriberList.get(i);
		subscriber.processNotificationEvent(event);
	    }
	}

	// Sucessfully notified the subscribers this time so delete
	// the persisted NotificationEventFailure object.
	eventFailure.remove();
    }

    /**
     * Return a list of notification event failures.
     */
    public ArrayList getNotificationFailureList() throws RemoteException {
	ArrayList failureList = new ArrayList();
	File failureDirectory = NotificationUtil.getFailureDirectory();

	if (failureDirectory != null && failureDirectory.exists()) {

	    NotificationEventFailure failureEvent = null;
	    File[] fileList = failureDirectory.listFiles();

	    for (int i = 0; i < fileList.length; i++) {
		try {
		    failureEvent = getNotificationFailure(fileList[i]);
		} catch (IOException ioe) {
		    // log an error for those that can't be displayed.
		    LogUtil.fatal(this.getClass(), "Unable to load persisted NotificationEventFailure [" + fileList[i].getAbsoluteFile() + "]. Class structure is out of sync with event." + ioe.toString());
		}

		if (failureEvent != null) {
		    failureList.add(failureEvent);
		}
	    }
	    Collections.sort(failureList);
	}
	return failureList;
    }

    /**
     * Return an instance of a notification event failure from the file.
     */
    public NotificationEventFailure getNotificationFailure(File failureFile) throws RemoteException {
	NotificationEventFailure failureEvent = null;

	if (failureFile != null && failureFile.exists()) {
	    FileInputStream fileStream = null;
	    ObjectInputStream objectStream = null;
	    try {
		fileStream = new FileInputStream(failureFile);
		objectStream = new ObjectInputStream(fileStream);
		failureEvent = (NotificationEventFailure) objectStream.readObject();
		failureEvent.setPersistenceFile(failureFile);
	    } catch (FileNotFoundException fnfe) {
		throw new SystemException("File not found loading persisted NotificationEventFailure objects. ", fnfe);
	    } catch (StreamCorruptedException sce) {
		throw new SystemException("Stream corrupted loading persisted NotificationEventFailure objects. ", sce);
	    } catch (ClassNotFoundException cnfe) {
		throw new SystemException("Class not found loading persisted NotificationEventFailure objects. ", cnfe);
	    } catch (OptionalDataException ode) {
		throw new SystemException("Optional data exception loading persisted NotificationEventFailure objects. ", ode);
	    } catch (IOException ioe) {
		throw new SystemException("IO exception loading persisted NotificationEventFailure objects. ", ioe);
	    }
	}
	return failureEvent;
    }

    // /**
    // * Description of the Method
    // */
    // public void ejbCreate()
    // {
    // }

    // /**
    // * Description of the Method
    // *
    // *@exception RemoteException Description of the Exception
    // */
    // public void ejbRemove()
    // throws RemoteException
    // {
    // if(this.eventList != null)
    // {
    // LogUtil.warn(this.getClass(),
    // "ejbRemove caused rollback of uncommitted event list.");
    // rollback();
    // }
    // }
    //
    // /**
    // * Description of the Method
    // *
    // *@exception RemoteException Description of the Exception
    // */
    // public void ejbActivate()
    // throws RemoteException
    // {
    // this.eventList = null;
    // }
    //
    // /**
    // * Description of the Method
    // *
    // *@exception RemoteException Description of the Exception
    // */
    // public void ejbPassivate()
    // throws RemoteException
    // {
    // if(this.eventList != null)
    // {
    // LogUtil.warn(this.getClass(),
    // "ejbPassivate caused rollback of uncommitted event list.");
    // rollback();
    // }
    // }

}

/**
 * This private class does the actual work of notifying each subscriber of the
 * event. This is done as a separate thread to disconnect the system publishing
 * the event from the time taken for the subscriber systems to respond to the
 * event.
 * 
 * @author bakkert
 * @created 28 February 2003
 */
class EventWorker extends Thread {

    // private ArrayList eventList;

    private NotificationEvent notificationEvent;

    private NotificationEvent getEvent() {
	return notificationEvent;
    }

    private File eventFailureDirectory = null;

    /**
     * Constructor for the EventWorker object
     * 
     * @param ne
     *            Description of the Parameter
     * @param delay
     *            Description of the Parameter
     */
    public EventWorker(NotificationEvent notEvent) {
	// this.eventList = list;
	this.notificationEvent = notEvent;
	this.eventFailure = null;
    }

    private NotificationEventFailure eventFailure = null;

    public NotificationEventFailure getEventFailure() {
	return this.eventFailure;
    }

    /**
     * Main processing method for the EventWorker object
     */
    public void run() {
	ArrayList subscriberList = null;
	ArrayList failedSubscriberList = new ArrayList();
	NotificationEvent event = this.getEvent();
	long delay = 0;

	try {
	    // LogUtil.log(this.getClass(), this.currentThread().getName() +
	    // ": Notifying subscribers of event " + event.getEventName());

	    // Reset the subscriber list in case an error occurs
	    // Don't want the error trapping to associate the wrong subscriber
	    // list with
	    // and event.
	    subscriberList = null;

	    // Workout how much to delay for
	    if (event.getDelayFrom() > 0) {
		delay = new DateTime().getTime() - event.getDelayFrom() + event.getDelay();
		// Make sure we havn't ended up with a riduculas number because
		// the time has
		// crossed over midnight.
		delay = (delay < event.getDelay() ? delay : event.getDelay());
	    } else {
		delay = event.getDelay();
	    }

	    // Only wait if the delay is greater than zero. A zero means
	    // wait forever until interrupted and a negative delay just
	    // wouldn't be sensible.
	    if (delay > 0) {
		// LogUtil.debug(this.getClass(),this.currentThread().getName()+": Sleeping for "
		// + delay + " milliseconds at " + DateUtil.formatDate(new
		// DateTime(),"dd/MM/yyyy HH:mm:ss:SS"));
		// If the wait gets interrupted keep going and don't throw to
		// the error trapping.
		try {
		    sleep(delay);
		} catch (InterruptedException e) {
		    // LogUtil.warn(this.getClass(),this.currentThread().getName()+
		    // ": Processing delay interrupted : " + e.getMessage());
		}
		// LogUtil.log(this.getClass(),this.currentThread().getName()+": Started again at "
		// + DateUtil.formatDate(new
		// DateTime(),"dd/MM/yyyy HH:mm:ss:SS"));
	    }

	    // Get the list of subscribers to the event from the subscriber
	    // cache.
	    subscriberList = SubscriberCache.getInstance().getSubscriberList(event.getEventName());

	    // Pass on the event to all subscribers.
	    failedSubscriberList.clear();
	    if (subscriberList != null) {
		NotificationEventSubscriber subscriber;
		for (int i = 0; i < subscriberList.size(); i++) {
		    subscriber = (NotificationEventSubscriber) subscriberList.get(i);
		    LogUtil.log(this.getClass(), this.currentThread().getName() + ": Publishing event " + event.getEventName() + " to subscriber " + subscriber.getSubscriberName());
		    try {

			subscriber.processNotificationEvent(event);
		    } catch (Exception e) {
			LogUtil.debug(this.getClass(), this.currentThread().getName() + ": Publishing failed for event " + event.getEventName() + " to subscriber " + subscriber.getSubscriberName());
			LogUtil.debug(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
			failedSubscriberList.add(new NotificationEventSubscriberFailure(subscriber, e));
		    }
		}
	    }

	    // If any subscribers failed to process the event then save
	    // a notification event failure.
	    if (!failedSubscriberList.isEmpty()) {
		LogUtil.warn(this.getClass(), "Event '" + (event != null ? event.getEventName() : "") + "' failed to notify subscribers. See Notification Admin for details.");
		eventFailure = new NotificationEventFailure(event, failedSubscriberList, null);
		saveNotificationEventFailure(eventFailure);
	    }

	    // LogUtil.debug(this.getClass(),this.currentThread().getName()+": Finished notifying subscribers of events");

	} catch (Exception e) {
	    LogUtil.warn(this.getClass(), "Event '" + (event != null ? event.getEventName() : "") + "' failed to process. See Notification Admin for details.");
	    eventFailure = new NotificationEventFailure(event, null, e);
	    saveNotificationEventFailure(eventFailure);
	}
    }

    private synchronized void saveNotificationEventFailure(NotificationEventFailure eventFailure) {
	if (eventFailure != null) {
	    File failureDirectory = getFailureDirectory();
	    if (failureDirectory != null) {
		String fileName = String.valueOf(new DateTime().getTime() + eventFailure.hashCode()) + ".evtf";
		ObjectOutputStream outputStream = null;
		try {
		    File failureFile = new File(failureDirectory, fileName);
		    FileOutputStream file = new FileOutputStream(failureFile);
		    outputStream = new ObjectOutputStream(file);
		    outputStream.writeObject(eventFailure);
		    outputStream.flush();
		} catch (IOException ioe) {
		    LogUtil.fatal(this.getClass(), this.currentThread().getName() + ": Failed to write notification event failure to file. " + ioe);
		} finally {
		    try {
			if (outputStream != null) {
			    outputStream.close();
			}
		    } catch (Exception e) {
		    }
		}
	    }
	}
    }

    private File getFailureDirectory() {
	if (this.eventFailureDirectory == null) {
	    this.eventFailureDirectory = NotificationUtil.getFailureDirectory();
	}
	return this.eventFailureDirectory;
    }
}
