package com.ract.common.notifier;

/**
 * Title:        Master Project
 * Description:  Holds the details of a failure to send a notification event
 *               to a subscriber.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author       T. Bakker
 * @version 1.0
 */
import java.io.Serializable;

public class NotificationEventSubscriberFailure implements Serializable {
    private NotificationEventSubscriber subscriber;

    private Exception exception;

    public NotificationEventSubscriberFailure(NotificationEventSubscriber subscriber, Exception exception) {
	this.subscriber = subscriber;
	this.exception = exception;
    }

    public NotificationEventSubscriber getSubscriber() {
	return subscriber;
    }

    public Exception getException() {
	return exception;
    }
}
