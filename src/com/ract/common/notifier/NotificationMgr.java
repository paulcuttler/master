package com.ract.common.notifier;

import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.ejb.Remote;

import com.ract.common.RollBackException;

@Remote
public interface NotificationMgr {
    /**
     * Publish an event to the notification manager. The notification manager
     * will then pass on the event to all event subscribers. The subscribers to
     * the event are notified asynchonously. The publisher does not have to wait
     * until all subscribers have responded to the event.
     */
    public void notifyEvent(NotificationEvent event) throws RemoteException, RollBackException;

    public void commit() throws RemoteException, RollBackException;

    public void rollback() throws RemoteException;

    public void deleteNotificationFailure(String fileName) throws RemoteException;

    public void resendAllNotificationEventFailures() throws RemoteException;

    public void resendNotificationFailure(String fileName) throws RemoteException;

    public ArrayList getNotificationFailureList() throws RemoteException;

    public NotificationEventFailure getNotificationFailure(File failureFile) throws RemoteException;

}
