package com.ract.common.notifier;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.ract.client.notifier.NotificationEventSubscriberClientImpl;
import com.ract.common.CacheBase2;
import com.ract.common.cad.NotificationEventSubscriberCadImpl;
import com.ract.common.integration.NotificationEventSubscriberReportIntegratorImpl;
import com.ract.common.streetsuburb.NotificationEventSubscriberStreetSuburbImpl;
import com.ract.membership.notifier.NotificationEventSubscriberMembershipImpl;
import com.ract.payment.notifier.NotificationEventSubscriberPaymentImpl;

/**
 * Holds a list of subscribers to a particular event. Copyright: Copyright (c)
 * 2002 Company: RACT
 * 
 * @author
 * @version 1.0
 */

public class SubscriberCache extends CacheBase2 {

    private static SubscriberCache instance = new SubscriberCache();

    public SubscriberCache() {
    }

    public static SubscriberCache getInstance() {
	return instance;
    }

    /**
     * Initialise the list of subscribers to each event
     */
    public void initialiseList() throws RemoteException {
	if (isEmpty()) {
	    ArrayList subscriberList = null;
	    SubscriberCacheItem cacheItem = null;

	    // The subscriber list must contain a list of objects that implement
	    // the NotificationEventSubscriber interface.
	    NotificationEventSubscriber clientSubscriber = new NotificationEventSubscriberClientImpl();
	    NotificationEventSubscriber caresSubscriber = new NotificationEventSubscriberCadImpl();
	    NotificationEventSubscriber membershipSubscriber = new NotificationEventSubscriberMembershipImpl();
	    NotificationEventSubscriber paymentSubscriber = new NotificationEventSubscriberPaymentImpl(); // SM
													  // 14/10/03
	    NotificationEventSubscriber stSubSubscriber = new NotificationEventSubscriberStreetSuburbImpl();
	    NotificationEventSubscriber integratorSubscriber = new NotificationEventSubscriberReportIntegratorImpl();

	    // Create a list of subscribers to the client change event.
	    subscriberList = new ArrayList();
	    // tramada
	    subscriberList.add(membershipSubscriber);
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_CLIENT_CHANGE, subscriberList);
	    add(cacheItem);

	    // Create a list of subscribers to the integrator event.
	    subscriberList = new ArrayList();
	    subscriberList.add(integratorSubscriber);
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_PRINT_INTEGRATOR, subscriberList);
	    add(cacheItem);

	    // Create a list of subscribers to the membership change event.
	    subscriberList = new ArrayList();
	    subscriberList.add(caresSubscriber);
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_MEMBERSHIP_CHANGE, subscriberList);
	    add(cacheItem);

	    // Create a list of subscribers to the progress client change event.
	    subscriberList = new ArrayList();
	    subscriberList.add(clientSubscriber);
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_PROGRESS_CLIENT_CHANGE, subscriberList);
	    add(cacheItem);

	    // Create a list of subscribers to the pending fee paid event.
	    subscriberList = new ArrayList();
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_PENDING_FEE_PAID, subscriberList);
	    add(cacheItem);

	    // create a list of subscribers to the payment notification event
	    // for a RECEPTOR payment SM 14/10/03
	    subscriberList = new ArrayList();
	    subscriberList.add(paymentSubscriber);
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_PAYMENT_NOTIFICATION, subscriberList);
	    add(cacheItem);

	    subscriberList = new ArrayList();
	    subscriberList.add(paymentSubscriber);
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_PAYMENT_CANCELLATION, subscriberList);
	    add(cacheItem);

	    // Create a list of subscribers to the street suburb event.
	    subscriberList = new ArrayList();
	    subscriberList.add(stSubSubscriber);
	    cacheItem = new SubscriberCacheItem(NotificationEvent.EVENT_PROGRESS_STREET_SUBURB_CHANGE, subscriberList);
	    add(cacheItem);

	}
    }

    /**
     * Return the list of subscribers to the event. The list contains a list of
     * objects that implement the NotificationEventSubscriber interface.
     */
    protected ArrayList getSubscriberList(String event) throws RemoteException {
	ArrayList subscriberList = null;
	SubscriberCacheItem item = (SubscriberCacheItem) getItem(event);
	if (item != null) {
	    subscriberList = item.getSubscriberList();
	}
	return subscriberList;
    }

    /**
     * Return all of the cached items as an ArrayList. An initialised but empty
     * ArrayList will be returned if there are no items in the cache. The method
     * needs to be synchronised so that multiple threads don't try and
     * initialise the list at the same time
     * 
     * @return The itemList value
     * @exception RemoteException
     *                Description of the Exception
     */
    // protected synchronized Collection getItemList() throws RemoteException

}
