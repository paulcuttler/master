package com.ract.common.notifier;

/**
 * Title:        Master Project
 * Description:  Holds the details of a notification event that failed.
 *               These details are saved and then loaded back and tried again
 *               when the server is restarted.
 *               NOTE: The notification event and all of the objects in the
 *               subscriber list must be serializable.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author
 * @version 1.0
 */
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class NotificationEventFailure implements Serializable, Comparable {

    private ArrayList failedSubscriberList;

    private NotificationEvent notificationEvent;

    private DateTime failureTime = new DateTime();

    private Exception exception;

    /**
     * The handle to the file that this object is persisted in. "transient"
     * means that this attribute is not persisted into the file. This attribute
     * is set once the object is read back in.
     */
    private transient File persistenceFile;

    /**
     * When the object is read back and the persistance file is set the file
     * name is also set.
     */
    private String persistenceFileName;

    public NotificationEventFailure(NotificationEvent event, ArrayList list, Exception e) {
	try {
	    LogUtil.debug(this.getClass(), "event=" + event.getEventName());
	    LogUtil.debug(this.getClass(), "list=" + list);
	    LogUtil.debug(this.getClass(), "e=" + e);
	} catch (Exception ex) {
	    LogUtil.log(this.getClass(), ex);
	}
	this.notificationEvent = event;
	this.failedSubscriberList = list;
	this.exception = e;
    }

    public NotificationEvent getNotificationEvent() {
	return notificationEvent;
    }

    public ArrayList getFailedSubscriberList() {
	return failedSubscriberList;
    }

    public DateTime getFailureTime() {
	return this.failureTime;
    }

    public Exception getException() {
	return this.exception;
    }

    /**
     * It is only valid to get the persistence
     */
    public String getPersistenceFileName() {
	return this.persistenceFileName;
    }

    public void setPersistenceFile(File file) {
	this.persistenceFile = file;
	this.persistenceFileName = file.getName();
    }

    /**
     * Causes the object to delete the file that it is persisted in.
     */
    public void remove() {
	if (this.persistenceFile != null && this.persistenceFile.exists()) {
	    this.persistenceFile.delete();
	}
    }

    public int compareTo(Object obj) {
	NotificationEventFailure that = (NotificationEventFailure) obj;
	if (this.failureTime != null && that.getFailureTime() != null) {
	    return this.failureTime.compareTo(that.getFailureTime());
	} else {
	    return 0;
	}
    }

}
