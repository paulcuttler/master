package com.ract.common.notifier;

import java.util.ArrayList;

import com.ract.common.CachedItem;

/**
 * Description of the Class
 * 
 * @author bakkert
 * @created 19 February 2003
 */
public class SubscriberCacheItem implements CachedItem {
    private String eventKey = null;

    private ArrayList subscriberList = null;

    /**
     * Constructor for the SubscriberCacheItem object
     * 
     * @param key
     *            Description of the Parameter
     * @param subscriberList
     *            Description of the Parameter
     */
    public SubscriberCacheItem(String akey, ArrayList subscriberList) {
	this.eventKey = akey;
	this.subscriberList = subscriberList;
    }

    /**
     * Return the list subscribers for the event identified by the instances
     * key.
     * 
     * @return The subscriberList value
     */
    public ArrayList getSubscriberList() {
	if (this.subscriberList != null) {
	    return new ArrayList(this.subscriberList);
	} else {
	    return new ArrayList();
	}
    }

    /**
     * Gets the key attribute of the SubscriberCacheItem object
     * 
     * @return The key value
     */
    public String getKey() {
	return this.eventKey;
    }

    /**
     * Return true if the given key matches this key.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String akey) {
	return this.eventKey.equals(akey);
    }
}
