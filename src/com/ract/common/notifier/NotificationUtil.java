package com.ract.common.notifier;

import java.io.File;
import java.rmi.RemoteException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemParameterVO;
import com.ract.util.LogUtil;

public class NotificationUtil {

    public static File getFailureDirectory() {
	File eventFailureDirectory = null;
	try {
	    String dirName = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_NOTIFICATION_EVENT_FAILURE);
	    if (dirName != null) {
		File dirFile = new File(dirName);
		if (!dirFile.exists()) {
		    LogUtil.log("NotificationMgrUtil", "Creating notification failure directory with '" + dirName + "'");
		    if (!dirFile.mkdirs()) {
			LogUtil.log("NotificationMgrUtil", "Failed to create membership history directory with '" + dirName + "'");
		    }
		}
		if (dirFile.exists()) {
		    eventFailureDirectory = dirFile;
		}
	    } else {
		LogUtil.log("NotificationMgrUtil", "Failed to get directory name for notification failures.");
	    }
	} catch (RemoteException re) {
	    LogUtil.log("NotificationMgrUtil", "Failed to get directory name for notification failures. " + re);
	}
	return eventFailureDirectory;
    }

}
