package com.ract.common.notifier;

/**
 * Title:        Master Project
 * Description:  The interface that classes must implement to receive NotificationEvents.
 *               NOTE: Implementations of this interface must also implement
 *               the Serializable interface.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author
 * @version 1.0
 */

import java.rmi.RemoteException;

public interface NotificationEventSubscriber {
    public void processNotificationEvent(NotificationEvent event) throws RemoteException;

    public String getSubscriberName();
}