package com.ract.common;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.SortedSet;

import javax.ejb.FinderException;
import javax.ejb.Remote;

@Remote
public interface CommonMgr {
    public String getSystemParameterValue(String category, String param) throws RemoteException;

    public Gift getGift(String giftCode) throws RemoteException;

    public Collection<BusinessType> getBusinessTypes();

    public Publication getPublication(String publicationCode);

    public void startExtract(String extractName, String userId) throws GenericException;

    public void endExtract(String extractName) throws GenericException;

    public Collection getMenuList() throws RemoteException;

    public Object update(Object o);

    public void delete(Object o);

    public void create(Object o);

    public Object get(Class entityClass, Object key);

    public Collection getMenuItemList(String menuName) throws RemoteException;

    public Collection getGiftListByType(String type) throws RemoteException;

    public Collection getTaxRates();

    public ArrayList getStringResultArray(String sql) throws RemoteException;

    public void setSystemParameterValue(String category, String param, String value) throws RemoteException;

    public Collection getCachedSystemParameterList() throws RemoteException;

    public Collection getGiftList() throws RemoteException;

    public void updateStreetSuburb(StreetSuburbVO streetSuburb) throws RemoteException;

    public void deleteStreetSuburb(StreetSuburbVO streetSuburb) throws RemoteException;

    public void createStreetSuburb(StreetSuburbVO streetSuburb) throws RemoteException;

    public void resetSalesBranchCache() throws RemoteException;

    public Collection getResultsPerPage() throws RemoteException;

    public void resetSystemParameterCache() throws RemoteException;

    public int countRecords(String sql) throws RemoteException;

    public ReferenceDataVO getReferenceData(String type, String code);

    public Collection getReferenceDataListByType(String type);

    public TaxRateVO getCurrentTaxRate() throws RemoteException;

    public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID) throws RemoteException;

    public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid) throws RemoteException;

    public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String ausbar) throws RemoteException;

    public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf) throws RemoteException;

    public AddressVO getAddress(String type, String property, String propertyQualifier, Integer streetSuburbID, String dpid, String latitude, String longitude, String gnaf, String ausbar) throws RemoteException;

    public MemoVO getMemo(Integer memoNo);

    public void createMemo(MemoVO memo);

    public MemoVO updateMemo(MemoVO memo);

    public void deleteMemo(MemoVO memo);

    public StreetSuburbVO getStreetSuburb(Integer stSubID) throws RemoteException;

    public Collection getStreetSuburbList(String streetName, String suburbName) throws RemoteException;

    public List<StreetSuburbVO> getStreetSuburbList(String streetName, String suburbName, String state) throws RemoteException;

    public List<StreetSuburbVO> getSuburbList(String suburbName, String state) throws RemoteException;

    public Collection getPublicationList() throws RemoteException;

    public Collection getTravelBranchList() throws RemoteException;

    public BranchVO getBranch(int branchNumber) throws RemoteException;

    public Collection getSalesBranchList();

    public SalesBranchVO getSalesBranch(String salesBranchCode);

    public SalesBranchVO getCachedSalesBranch(String salesBranchCode) throws RemoteException;

    public ArrayList getCachedSalesBranchList() throws RemoteException;

    public PrintForm getPrintForm(String formType, String printerGroup) throws RemoteException;

    public LetterStruct getLetter(String subSystem, String key) throws RemoteException;

    public Collection getPrinterGroups() throws RemoteException;

    public Collection getSalesBranchCodes() throws RemoteException;

    public SortedSet getPrinterCodesAndDescriptions() throws RemoteException;

    public SortedSet getSalesBranchCodesAndDescriptions() throws RemoteException;

    public Collection getBranchList() throws RemoteException, FinderException;

    public SortedSet getBranchRegionsAndDescriptions() throws RemoteException;

    public SortedSet getUpperSalesBranchCodesAndDescriptions() throws RemoteException;

    // public SortedSet getCommonUsers()
    // throws RemoteException;

    public SortedSet getAllRoadSideExtracts() throws RemoteException;

    public SortedSet getAllExtracts(String extractName) throws RemoteException;

    public Hashtable getExtractParameters(String extractName) throws RemoteException;

    public String getPeriodStartDate() throws RemoteException;

    public String getPeriodEndDate() throws RemoteException;

    public String getGnText(String subSys, String type, String name) throws RemoteException;

    /**
     * Routines for managing gn_text table
     */
    public GnTextVO getGnTextVO(String subSystem, String textType, String textName) throws RemoteException;

    public void setGnTextFromVO(GnTextVO gnTextVO) throws RemoteException;

    public Collection getGnTextList(String subSystem, String textType) throws RemoteException;

    public void updateSystemParameter(SystemParameterVO systemParameter) throws RemoteException;

    public void createSystemParameter(SystemParameterVO systemParameter);

    public SystemParameterVO getSystemParameter(String category, String param);

    public Collection<SystemParameterVO> getSystemParameterList();

    /**
     * returns an ArrayList of ReferenceDataVO. Only the referenceType,
     * referenceCode and sort order fields are set
     */
    public Collection getReferenceDataList();

    /**
     * Methods for managing server sockets
     */
    public ArrayList getSocketServerKeyList() throws RemoteException;

    public void updateReferenceData(ReferenceDataVO referenceData);

    public void initialiseSocketServers() throws RemoteException;

    public void startSocketServer(String socketServerKey) throws RemoteException;

    public void stopSocketServer(String socketServerKey) throws RemoteException;

    public void resetSocketServer(String socketServerKey) throws RemoteException;

    public void startAllSocketServers() throws RemoteException;

    public void stopAllSocketServers() throws RemoteException;

    public void resetAllSocketServers() throws RemoteException;

    public String getSocketServerState(String socketServerKey) throws RemoteException;

    public int getSocketServerPort(String socketServerKey) throws RemoteException;

    public Exception getSocketServerException(String socketServerKey) throws RemoteException;

    public int getSocketServerThreadCount(String socketServerKey) throws RemoteException;

    // public void testHibernateSessionFactory() throws RemoteException;

    public Collection getBillerCodes() throws RemoteException;

    public Collection getMotorNewsAddSurnameKeyEqualList() throws RemoteException;

    public Collection getMotorNewsAddSurnameKeyLikeList() throws RemoteException;

    public int getCadExtractMembershipCutOff() throws RemoteException;

    public int getCadDeleteFlagCutOff() throws RemoteException;

    public Collection getSystemParameterList(String categoryCad, String cadMidasExportFiles);
}
