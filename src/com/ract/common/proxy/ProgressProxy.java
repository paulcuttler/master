package com.ract.common.proxy;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;

/**
 * Common proxy interface to allow pooling. All calls to ANY proxy should be at
 * this level.
 * 
 * @author jyh
 * @version 1.0
 */
public interface ProgressProxy {

    /**
     * Release the connection to the app server.
     * 
     * @throws Open4GLException
     * @throws SystemErrorException
     */
    public void _release() throws Open4GLException, SystemErrorException;

    /**
     * Get a unique connection id identifiable in the app server logs
     * 
     * @throws Open4GLException
     * @return Object
     */
    public Object _getConnectionId() throws Open4GLException;

    /**
     * Is the connection still active.
     * 
     * @throws Open4GLException
     * @return boolean
     */
    public boolean _isStreaming() throws Open4GLException;

    /**
     * Cancel all pending requests to the progress proxy.
     * 
     * @throws Open4GLException
     */
    public void _cancelAllRequests() throws Open4GLException;

    /**
     * Get the return string from the progress procedure.
     * 
     * @throws Open4GLException
     * @return String
     */
    public String _getProcReturnString() throws Open4GLException;

}
