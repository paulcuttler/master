package com.ract.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReferenceDataPK implements Serializable {

    public String getReferenceCode() {
	return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
	this.referenceCode = referenceCode;
    }

    public String getReferenceType() {
	return referenceType;
    }

    public void setReferenceType(String referenceType) {
	this.referenceType = referenceType;
    }

    @Column(name = "[ref_code]")
    private String referenceCode;

    @Column(name = "[ref_type]")
    private String referenceType;

    public ReferenceDataPK() {
    }

    public ReferenceDataPK(String referenceCode, String referenceType) {
	this.referenceCode = referenceCode;
	this.referenceType = referenceType;
    }

    public boolean equals(Object obj) {
	if (this.getClass().equals(obj.getClass())) {
	    ReferenceDataPK that = (ReferenceDataPK) obj;
	    return this.referenceCode.equals(that.referenceCode) && this.referenceType.equals(that.referenceType);
	}
	return false;
    }

    public int hashCode() {
	return (referenceCode + referenceType).hashCode();
    }
}