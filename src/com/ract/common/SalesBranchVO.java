package com.ract.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Value object to represent the different sales branches.
 * 
 * @author Glenn Lewis
 * @version 1.0, 17/5/2002
 * 
 */
@Entity
@Table(name = "[gn-slsbch]")
public class SalesBranchVO extends ValueObject implements CachedItem {
    public String getEmailAddress() {
	return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    private String salesBranchCode;

    private String profitCentre;

    private String salesBranchName;

    private Boolean serviceCentre;

    private String emailAddress;

    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.trim().equalsIgnoreCase(myKey);
	}
    }

    @Transient
    public String getKey() {
	return this.getSalesBranchCode();
    }

    /** Constructor */
    public SalesBranchVO(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public SalesBranchVO() {
    }

    /** ================= Getter Methods ======================= */

    @Id
    @Column(name = "[sales-branch]")
    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    @Column(name = "[profit-centre]")
    public String getProfitCentre() {
	return profitCentre;
    }

    @Column(name = "[description]")
    public String getSalesBranchName() {
	return salesBranchName;
    }

    @Column(name = "[service-centre]")
    public Boolean getServiceCentre() {
	return serviceCentre;
    }

    /** ================= Setter Methods ======================= */

    public void setProfitCentre(String profitCentre) {
	this.profitCentre = profitCentre;
    }

    public void setSalesBranchName(String salesBranchName) {
	this.salesBranchName = salesBranchName;
    }

    public void setServiceCentre(Boolean serviceCentre) {
	this.serviceCentre = serviceCentre;
    }

    protected void validateMode(String newMode) throws ValidationException {
    }

    public String toString() {
	return this.salesBranchCode;
    }

}
