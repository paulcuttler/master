package com.ract.common;

import java.rmi.RemoteException;

import javax.ejb.CreateException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.util.AddressUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

/**
 * Implements the common attributes and operations of all address types. Is
 * abstract and must be sub-classed by an address implementation.
 * 
 * @author John Holliday
 * @created 31 July 2002
 * @version 1.0
 */
public abstract class AddressVO extends ValueObject implements Writable {
    public static String ADDRESS_TYPE_STREET = "Street";

    public static String ADDRESS_TYPE_POSTAL = "Postal";

    public static String FIELD_NAME_PROPERTY = "property";

    public static String fIELD_NAME_PROPERTY_QUALIFIER = "propertyQualifier";

    public static String FIELD_NAME_STATE = "state";

    public static String FIELD_NAME_SUBURB = "suburb";

    public static String FIELD_NAME_STREET = "street";

    public static String FIELD_NAME_COUNTRY = "country";

    public static String FIELD_NAME_POSTCODE = "postcode";

    public static String STATE_TAS = "TAS";

    /**
     * Description of the Field
     */
    public final static String WRITABLE_CLASSNAME = "Address";

    /**
     * the actual identifier of the property. eg. flat 1 32b
     */
    protected String propertyQualifier;

    /**
     * property name
     */
    protected String property;

    /**
     * Description of the Field
     */
    protected Integer streetNumber;

    /**
     * Description of the Field
     */
    protected Integer streetSuburbID;

    /**
     * DPID RLG 6/02/07
     */
    protected String dpid;
    /**
     * Description of the Field
     */
    protected StreetSuburbVO streetSuburb;

    protected String ausbar;
    protected String latitude;
    protected String longitude;
    protected String gnaf;

    /**
     * Constructors ******************************
     */

    public AddressVO() {
    }

    /**
     * Construct the address using the data in the XML DOM node. Set the mode to
     * "History" and set the value object to read only.
     * 
     * @param addressNode
     *            Description of the Parameter
     * @exception CreateException
     *                Description of the Exception
     * @todo implement loading street suburb class from xml
     */
    public AddressVO(Node addressNode) throws SystemException {
	if (addressNode == null) {
	    throw new SystemException("Failed to create address history from XML node. The node is not valid.");
	}

	NodeList elementList = addressNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("property".equals(nodeName) && valueText != null) {
		this.property = valueText;
	    } else if ("propertyQualifier".equals(nodeName) && valueText != null) {
		this.propertyQualifier = valueText;
	    } else if ("streetNumber".equals(nodeName) && valueText != null) {
		this.streetNumber = new Integer(valueText);
	    } else if ("streetSuburbID".equals(nodeName) && valueText != null) {
		this.streetSuburbID = new Integer(valueText);
	    } else if ("dpid".equals(nodeName) && valueText != null) {
		this.dpid = valueText;
	    } else if ("streetSuburb".equals(nodeName) && writableNode != null) {
		// ignore
	    }
	}

	setReadOnly(true);
	try {
	    setMode(MODE_HISTORY);
	} catch (ValidationException ve) {
	    // Should be setting it to a valid value so pass the exception
	    // back as a system exception
	    throw new SystemException(ve.getMessage());
	}
    }

    /**
     * abstract class - implementation returns the address type.
     * 
     * @return String
     * @roseuid 3C5DF28E00B1
     */
    public abstract String getAddressType();

    /**
     * Return the of the property.
     * 
     * @return String
     * @roseuid 3C5DD1C4009D
     */
    public String getProperty() {
	return this.property;
    }

    /**
     * get the DPID
     * 
     * @return String
     */
    public String getDpid() {
	return this.dpid;
    }

    /**
     * Was char2no in /gen directory of old system Extract street number from
     * street character
     * 
     * @return the street number for the property.
     * @roseuid 3C5DD1C400B1
     */
    public Integer getStreetNumber() {
	return this.streetNumber;
    }

    /**
     * Return the street for the property. This is referenced from the
     * SreetSuburb object.
     * 
     * @return String
     * @roseuid 3C5DD1C400CF
     */
    public String getStreet() {
	if (this.getStreetSuburb() != null) {
	    return this.getStreetSuburb().getStreet();
	} else {
	    return null;
	}

    }

    /**
     * Return the suburb for the property. This is referenced from the
     * SreetSuburb object.
     * 
     * @return String
     * @roseuid 3C5DD1C400ED
     */
    public String getSuburb() {
	if (this.getStreetSuburb() != null) {
	    // System.out.println("street suburb is not null");
	    return this.getStreetSuburb().getSuburb();
	} else {
	    return null;
	}

    }

    /**
     * Return the postcode for the property. This is referenced from the
     * SreetSuburb object.
     * 
     * @return String
     * @roseuid 3C5DD1C4010B
     */
    public String getPostcode() {
	if (this.getStreetSuburb() != null) {
	    return this.getStreetSuburb().getPostcode();
	} else {
	    return null;
	}

    }

    /**
     * Return the state for the property. This is referenced from the
     * SreetSuburb object.
     * 
     * @return String
     * @roseuid 3C5DD1C40129
     */
    public String getState() {
	if (this.getStreetSuburb() != null) {
	    return this.getStreetSuburb().getState();
	} else {
	    return null;
	}

    }

    /**
     * @return String
     * @roseuid 3C5DD1C4013D
     */
    public String getCountry() {
	if (this.getStreetSuburb() != null) {
	    return this.getStreetSuburb().getCountry();
	} else {
	    return null;
	}
    }

    /**
     * Return the first line of the address when the address is formatted to fit
     * on two lines.
     * 
     * @return String
     * @roseuid 3C5DD1C4015B
     */
    public String getAddressLine1() {
	StringBuffer addressLine1 = new StringBuffer();
	if (this.getProperty() != null) {
	    addressLine1.append(this.property);
	}
	return addressLine1.toString();
    }

    /**
     * Return the second line of the address when the address is formatted to
     * fit on two lines.
     * 
     * @return String
     * @roseuid 3C5DD1C40183
     */
    public String getAddressLine2() {
	StringBuffer addressLine2 = new StringBuffer();
	if (this.propertyQualifier != null) {
	    addressLine2.append(this.getPropertyQualifier());
	}
	StreetSuburbVO localStreetSuburb = this.getStreetSuburb();

	if (this.getStreetSuburb() != null) {
	    if (this.getStreet() != null) {
		addressLine2.append(" ");
		addressLine2.append(localStreetSuburb.getStreet());
	    }
	}
	return addressLine2.toString();
    }

    public String getAddressLine3() {
	StringBuffer addressLine3 = new StringBuffer();
	if (this.getSuburb() != null) {
	    addressLine3.append(this.getSuburb());
	}
	if (this.getState() != null) {
	    addressLine3.append(" ");
	    addressLine3.append(this.getState());
	}
	if (this.getPostcode() != null) {
	    addressLine3.append(" ");
	    addressLine3.append(this.getPostcode());
	}
	return addressLine3.toString();
    }

    public String getAddressLine4() {
	StringBuffer addressLine4 = new StringBuffer();
	if (this.getCountry() != null) {
	    addressLine4.append(this.getCountry());
	}
	return addressLine4.toString();
    }

    /**
     * Return the address formatted to fit on one line.
     * 
     * @return String
     * @roseuid 3C5DD1C40197
     */
    public String getSingleLineAddress() {
	StringBuffer addrLine = new StringBuffer();

	if (this.property != null && this.property.length() > 0) {
	    addrLine.append(StringUtil.toProperCase(this.property));
	    addrLine.append(", ");
	}

	// flat 25c
	if (this.propertyQualifier != null) {
	    addrLine.append(this.propertyQualifier);
	    addrLine.append(" ");
	}

	StreetSuburbVO streetSubVO = getStreetSuburb();
	if (streetSubVO != null) {
	    String street = streetSubVO.getStreet();
	    if (street != null && street.length() > 0) {
		if (addrLine.length() > 0) {
		    addrLine.append(" ");
		}
		addrLine.append(StringUtil.toProperCase(street));
		addrLine.append(", ");
	    }
	    String suburb = streetSubVO.getSuburb();
	    if (suburb != null && suburb.length() > 0) {
		if (addrLine.length() > 0) {
		    addrLine.append(" ");
		}
		addrLine.append(StringUtil.toProperCase(suburb));
	    }
	    String state = streetSubVO.getState();
	    if (state != null && state.length() > 0) {
		if (addrLine.length() > 0) {
		    addrLine.append(" ");
		}
		addrLine.append(state.trim().toUpperCase());
	    }
	    String postcode = streetSubVO.getPostcode();
	    if (postcode != null && postcode.length() > 0) {
		if (addrLine.length() > 0) {
		    addrLine.append(" ");
		}
		addrLine.append(postcode.trim());
	    }
	}

	return addrLine.toString().toUpperCase();
    }

    /**
     * Validate the address attributes. Raise an exception if the validation
     * fails. This method is abstract. It must be implemented by the sub class.
     * 
     * @roseuid 3C5DD1C401C9
     */
    public abstract void validate();

    /**
     * Return a new instance of an object that implements the interface for the
     * address type specified.
     * 
     * @param addressType
     * @return Common_lv.Common.Addr.AddressVO
     * @roseuid 3C5F22940166
     */
    public AddressVO create(String addressType) {
	if (addressType.equals(AddressVO.ADDRESS_TYPE_STREET)) {
	    return new ResidentialAddressVO();
	} else if (addressType.equals(AddressVO.ADDRESS_TYPE_POSTAL)) {
	    return new PostalAddressVO();
	} else {
	    return null;
	}
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public abstract AddressVO copy();

    /**
     * Sets the streetSuburb attribute of the AddressVOBase object
     * 
     * @param streetSuburb
     *            The new streetSuburb value
     */
    public void setStreetSuburb(StreetSuburbVO streetSuburb) {
	this.streetSuburb = streetSuburb;
    }

    /**
     * Gets the streetSuburbID attribute of the AddressVOBase object
     * 
     * @return The streetSuburbID value
     */
    public Integer getStreetSuburbID() {
	if (this.streetSuburbID == null) {
	    StreetSuburbVO s = this.getStreetSuburb();
	    if (s != null) {
		this.streetSuburbID = s.getStreetSuburbID();
	    }
	}
	return this.streetSuburbID;
    }

    /**
     * Gets the streetSuburb attribute of the AddressVOBase object
     * 
     * @return The streetSuburb value
     */
    public StreetSuburbVO getStreetSuburb() {
	// System.out.println("check before returning "+streetSuburb.getSuburb());
	if (this.streetSuburb == null) {
	    if (this.streetSuburbID != null) {
		try {
		    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		    this.streetSuburb = commonMgr.getStreetSuburb(this.streetSuburbID);
		    // StreetSuburbHome sHome =
		    // CommonEJBHelper.getStreetSuburbHome();
		    // StreetSuburb s =
		    // sHome.findByPrimaryKey(this.streetSuburbID);
		    // this.streetSuburb = s.getVO();
		} catch (Exception e) {
		    System.out.println("error getting street suburb vo" + e);
		}
	    }
	}
	return this.streetSuburb;
    }

    /**
     * Set the DPID
     * 
     */
    public void setDpid(String dpid) {
	this.dpid = dpid;
    }

    /**
     * Sets the streetSuburbID attribute of the AddressVOBase object
     * 
     * @param streetSuburbID
     *            The new streetSuburbID value
     */
    public void setStreetSuburbID(Integer streetSuburbID) {
	this.streetSuburbID = streetSuburbID;
    }

    /**
     * Sets the streetNumber attribute of the AddressVOBase object
     */
    private void setStreetNumber() {
	/*
	 * if property qualifier is not null then manipulate it to get the
	 * actual number.
	 */

	this.streetNumber = AddressUtil.getStreetNumberFromStreetChar(propertyQualifier);

    }

    /**
     * Sets the property attribute of the AddressVOBase object
     * 
     * @param property
     *            The new property value
     */
    public void setProperty(String property) {
	this.property = property;
    }

    /**
     * Sets the propertyQualifier attribute of the AddressVOBase object
     * 
     * @param propertyQualifier
     *            The new propertyQualifier value
     */
    public void setPropertyQualifier(String propertyQualifier) {
	this.propertyQualifier = propertyQualifier;
	setStreetNumber();
    }

    /**
     * Gets the propertyQualifier attribute of the AddressVOBase object
     * 
     * @return The propertyQualifier value
     */
    public String getPropertyQualifier() {
	return this.propertyQualifier;
    }

    /**
     * Description of the Method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
    }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("property", this.property);
	cw.writeAttribute("propertyQualifier", this.propertyQualifier);
	cw.writeAttribute("streetNumber", this.streetNumber);
	cw.writeAttribute("streetSuburbID", this.streetSuburbID);
	// cw.writeAttribute("streetSuburb", getStreetSuburb());
	cw.writeAttribute("singleLineAddress", getSingleLineAddress());
	cw.writeAttribute("dpid", this.getDpid());
	cw.endClass();
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public String toString() {
	return this.getSingleLineAddress();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((property == null) ? 0 : property.hashCode());
	result = prime * result + ((propertyQualifier == null) ? 0 : propertyQualifier.hashCode());
	result = prime * result + ((streetSuburbID == null) ? 0 : streetSuburbID.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof AddressVO))
	    return false;

	AddressVO other = (AddressVO) obj;

	if (!fixNull(property).equals(fixNull(other.property)))
	    return false;

	if (!fixNull(propertyQualifier).equals(fixNull(other.propertyQualifier)))
	    return false;

	if (streetSuburbID == null) {
	    if (other.streetSuburbID != null)
		return false;
	} else {
	    if (!streetSuburbID.equals(other.streetSuburbID))
		return false;
	}
	return true;
    }

    private String fixNull(String inStr) {
	String outStr;
	if (inStr == null)
	    outStr = "";
	else
	    outStr = inStr.trim();
	LogUtil.debug(this.getClass(), "outStr=" + outStr);
	return outStr;
    }

    public String getAusbar() {
	return ausbar;
    }

    public void setAusbar(String ausbar) {
	this.ausbar = ausbar;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
	return latitude;
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(String latitude) {
	this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
	return longitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(String longitude) {
	this.longitude = longitude;
    }

    /**
     * @return the gnaf
     */
    public String getGnaf() {
	return gnaf;
    }

    /**
     * @param gnaf
     *            the gnaf to set
     */
    public void setGnaf(String gnaf) {
	this.gnaf = gnaf;
    }
}
