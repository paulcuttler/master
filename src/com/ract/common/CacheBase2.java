package com.ract.common;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;

import com.ract.util.LogUtil;

/**
 * Defines and implements the common methods that a cache class needs to
 * manipulate cached items. This version of the cach base uses a hash table to
 * store the items. This improves the retrieval time for single items compared
 * to using a list to hold the items but is slower when returning a list of the
 * items.
 * 
 * @author Terry Bakker
 * @created 31 July 2002
 * @version 1.0
 */

public abstract class CacheBase2 {

    /**
     * The dataList holds the list of items that are cached.
     */
    private Hashtable table = new Hashtable();

    /**
     * Return the data list. Initialise it if it is empty. The sub class must
     * provide the implementation since it must add items to the list first if
     * the list is empty.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public abstract void initialiseList() throws RemoteException;

    private synchronized void initList() throws RemoteException {
	LogUtil.log(this.getClass(), "Initialising list.");
	initialiseList();
    }

    /**
     * Return the specified item object. Search the list of item for one that
     * has the same key.
     * 
     * @param key
     *            Description of the Parameter
     * @return The item value
     * @exception RemoteException
     *                Description of the Exception
     */
    protected CachedItem getItem(String key) throws RemoteException {
	initialiseList();
	CachedItem item = null;
	if (!isEmpty()) {
	    item = (CachedItem) this.table.get(key);
	}

	return item;
    }

    /**
     * Return all of the cached items as an ArrayList. An initialised but empty
     * ArrayList will be returned if there are no items in the cache. The method
     * needs to be synchronised so that multiple threads don't try and
     * initialise the list at the same time
     * 
     * @return The itemList value
     * @exception RemoteException
     *                Description of the Exception
     */
    protected Collection getItemList() throws RemoteException {
	Collection list = null;
	if (isEmpty()) {
	    initList();
	}
	if (!isEmpty()) {
	    list = this.table.values();
	}
	return list;
    }

    /**
     * Returns true if the cache does not contain any items.
     * 
     * @return The empty value
     */
    protected boolean isEmpty() {
	if (this.table == null) {
	    return true;
	} else {
	    return this.table.isEmpty();
	}
    }

    /**
     * Add an item to the list.
     * 
     * @param item
     *            Description of the Parameter
     */
    protected void add(CachedItem item) {
	if (item != null) {
	    // Initialise the table if it has been reset.
	    if (this.table == null) {
		this.table = new Hashtable();
	    }

	    this.table.put(item.getKey(), item);
	}
    }

    /**
     * Replace the item in the cache.
     * 
     * @param item
     *            Description of the Parameter
     */
    protected void replace(CachedItem item) {
	remove(item);
	add(item);
    }

    /**
     * Remove the item from the cache
     * 
     * @param item
     *            Description of the Parameter
     */
    protected void remove(CachedItem item) {
	if (item != null && this.table != null) {
	    this.table.remove(item.getKey());
	}
    }

    /**
     * Reset the cache to empty so that the next reference to it causes a
     * reload.
     */
    public void reset() {
	this.table = null;
    }

}
