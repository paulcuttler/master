package com.ract.common;

import java.rmi.RemoteException;

/**
 * A class to tag an exception as one that reports a broken business validation
 * rule. The exception can be handled differently than system errors.
 * 
 * @author Terry Bakker
 * @created 31 July 2002
 * @version 1.0
 */

public class ValidationException extends RemoteException {
    /**
     * Constructor for the ValidationException object
     */
    public ValidationException() {
	super("Validation Error: undefined.");
    }

    /**
     * Constructor for the ValidationException object
     * 
     * @param message
     *            Description of the Parameter
     */
    public ValidationException(String message) {
	super(message);
    }

    public ValidationException(Throwable t) {
	super("", t);
    }

    /**
     * Constructor for the ValidationException object
     * 
     * @param message
     *            Description of the Parameter
     * @param e
     *            Description of the Parameter
     */
    public ValidationException(String message, Throwable t) {
	super(message, t);
    }

}
