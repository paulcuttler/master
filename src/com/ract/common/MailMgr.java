package com.ract.common;

import java.rmi.RemoteException;

import javax.ejb.Remote;

import com.ract.common.mail.MailMessage;

/**
 * Mail remote interface.
 * 
 * @author John Holliday
 * @version 1.0
 */
@Remote
public interface MailMgr {
    // public void sendMail(String recipient, String subject, String
    // messageText)
    // throws RemoteException;
    //
    // public void sendMail(String recipient, String subject, String
    // messageText, Hashtable files)
    // throws RemoteException;
    //
    // public void sendMail(String recipient, String subject, String
    // messageText,String replyTo, Hashtable files, boolean specifyEnvironment)
    // throws RemoteException;
    //
    // public void sendMail(String recipient, String bccTo,String subject,
    // String messageText,String fromAddress, Hashtable files, boolean
    // specifyEnvironment, boolean htmlFormat)
    // throws RemoteException;

    public void sendMail(MailMessage message) throws RemoteException;

}
