package com.ract.common;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.w3c.dom.Element;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.streetsuburb.ProgressStreetSuburbChangeNotificationEvent;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * Session bean to perform simple queries related to an address.
 * 
 * @author John Holliday
 * @version 1.0, 21/4/2002
 */
@Stateless
@Remote({ StreetSuburbMgr.class })
@Local({ StreetSuburbMgrLocal.class })
public class StreetSuburbMgrBean {

    @PersistenceContext(unitName = "master")
    Session hsession;

    /**
     * Default datasource
     */
    @Resource(mappedName = "java:/ClientDS")
    DataSource dataSource;

    public void processNotificationEvent(NotificationEvent event) throws RemoteException {
	String action = null;
	try {
	    CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();

	    ProgressStreetSuburbChangeNotificationEvent stEvent = (ProgressStreetSuburbChangeNotificationEvent) event;

	    Element stXML = (Element) stEvent.getStreetSuburbNode();
	    action = stEvent.getAction();
	    Integer stSubId = new Integer(getValueFromXML(stXML, "stsubid"));

	    String suburb = getValueFromXML(stXML, "suburb");
	    String street = getValueFromXML(stXML, "street");
	    String postcode = getValueFromXML(stXML, "postcode");
	    DateTime modified = new DateTime(getValueFromXML(stXML, "last-update"));
	    LogUtil.log(this.getClass(), "modified=" + modified);
	    String country = getValueFromXML(stXML, "country");
	    String state = getValueFromXML(stXML, "state");
	    String modBy = getValueFromXML(stXML, "last-updateid");
	    DateTime deleteDate = null;

	    String deleteDateString = getValueFromXML(stXML, "deleteDate");
	    if (deleteDateString != null && !deleteDateString.trim().equals("")) {
		LogUtil.log(this.getClass(), "deleteDateString=" + deleteDateString);
		deleteDate = new DateTime(deleteDateString);
	    }

	    if (action.equals("create")) {
		StreetSuburbVO streetSuburb = new StreetSuburbVO();
		streetSuburb.setStreetSuburbID(stSubId);
		streetSuburb.setStreet(street);
		streetSuburb.setSuburb(suburb);
		streetSuburb.setPostcode(postcode);
		streetSuburb.setModifyDate(modified);
		streetSuburb.setModifyBy(modBy);
		streetSuburb.setCountry(country);
		streetSuburb.setState(state);
		streetSuburb.setDeleteDate(deleteDate);

		commonMgrLocal.createStreetSuburb(streetSuburb);
	    } else if (action.equals("update")) {
		// not necessary to get the street suburb first with ejb3 and
		// hibernate
		StreetSuburbVO streetSuburb = commonMgrLocal.getStreetSuburb(stSubId);

		streetSuburb.setStreet(street);
		streetSuburb.setSuburb(suburb);
		streetSuburb.setPostcode(postcode);
		streetSuburb.setModifyDate(modified);
		streetSuburb.setModifyBy(modBy);
		streetSuburb.setCountry(country);
		streetSuburb.setState(state);
		streetSuburb.setDeleteDate(deleteDate);

		commonMgrLocal.updateStreetSuburb(streetSuburb);
	    } else if (action.equals("delete")) {
		// not necessary to get the street suburb first with ejb3 and
		// hibernate
		StreetSuburbVO streetSuburb = commonMgrLocal.getStreetSuburb(stSubId);
		commonMgrLocal.deleteStreetSuburb(streetSuburb);
	    }
	} catch (Exception ex) {
	    LogUtil.log(this.getClass(), "Unable to " + action + " street suburb " + ex.getMessage());
	    ex.printStackTrace();
	}

    }

    private String getValueFromXML(Element stXML, String tagName) {
	String tempVal = null;
	try {
	    tempVal = stXML.getElementsByTagName(tagName).item(0).getFirstChild().getNodeValue();
	} catch (Exception ex) {
	    tempVal = null;
	}
	LogUtil.log(this.getClass(), "tagName=" + tagName);
	LogUtil.log(this.getClass(), "tempVal=" + tempVal);
	return ((tempVal == null) ? "" : tempVal);
    }

    public Collection getStreetSuburbList(String street, String suburb) throws RemoteException {

	// Connection connection = null;
	// PreparedStatement statement = null;

	Collection streetSuburbList = new ArrayList();

	if (street == null) {
	    street = "";
	}
	if (suburb == null) {
	    suburb = "";
	}

	if (street.equals("") && !suburb.equals("")) {
	    suburb += CommonConstants.WILDCARD;
	    streetSuburbList = findStreetSuburbBySuburb(suburb);
	} else if (suburb.equals("") && !street.equals("")) {
	    street += CommonConstants.WILDCARD;
	    streetSuburbList = findStreetSuburbByStreet(street);
	} else {
	    suburb += CommonConstants.WILDCARD;
	    street += CommonConstants.WILDCARD;
	    streetSuburbList = findStreetSuburbByStreetAndSuburb(street, suburb);
	}

	return streetSuburbList;
    }

    private Collection findStreetSuburbBySuburb(String suburb) throws RemoteException {
	Collection streetSuburbList = null;
	try {
	    LogUtil.debug(this.getClass(), "findStreetSuburbBySuburb");
	    LogUtil.debug(this.getClass(), "suburb=" + suburb);
	    streetSuburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.like("suburb", suburb)).add(Restrictions.isNull("deleteDate")).addOrder(Order.asc("street")).addOrder(Order.asc("suburb")).list());
	} catch (HibernateException ex) {
	    throw new RemoteException("Unable to find street suburb list.", ex);
	}
	return streetSuburbList;

    }

    private Collection findStreetSuburbByStreet(String street) throws RemoteException {
	Collection streetSuburbList = null;
	try {
	    LogUtil.debug(this.getClass(), "findStreetSuburbByStreet");
	    LogUtil.debug(this.getClass(), "street=" + street);
	    streetSuburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.like("street", street)).add(Restrictions.isNull("deleteDate")).addOrder(Order.asc("street")).addOrder(Order.asc("suburb")).list());
	} catch (HibernateException ex) {
	    throw new RemoteException("Unable to find street suburb list.", ex);
	}
	return streetSuburbList;
    }

    public Collection getStreetSuburbsByPostcode(String postcode) throws RemoteException {
	Collection streetSuburbList = null;
	try {

	    LogUtil.debug(this.getClass(), "findStreetSuburbByStreet");
	    LogUtil.debug(this.getClass(), "postcode=" + postcode);
	    streetSuburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.eq("postcode", postcode)).add(Restrictions.isNull("deleteDate")).addOrder(Order.asc("street")).addOrder(Order.asc("suburb")).list());
	} catch (HibernateException ex) {
	    throw new RemoteException("Unable to find street suburb list.", ex);
	}
	return streetSuburbList;
    }

    public Collection getStreetSuburbsByStreet(String street, String postcode) throws RemoteException {
	Collection streetSuburbList = null;
	if (street == null || street.isEmpty()) {
	    street = "";
	} else {
	    street += CommonConstants.WILDCARD;
	}
	try {

	    LogUtil.debug(this.getClass(), "findStreetSuburbByStreet");
	    LogUtil.debug(this.getClass(), "postcode=" + postcode);
	    LogUtil.debug(this.getClass(), "street=" + street);
	    streetSuburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.eq("postcode", postcode)).add(Restrictions.like("street", street)).add(Restrictions.isNull("deleteDate")).addOrder(Order.asc("street")).addOrder(Order.asc("suburb")).list());
	} catch (HibernateException ex) {
	    throw new RemoteException("Unable to find street suburb list.", ex);
	}
	return streetSuburbList;
    }

    public Collection getStreetSuburbsBySuburb(String suburb, String postcode) throws RemoteException {
	Collection streetSuburbList = null;
	if (suburb == null) {
	    suburb = "";
	}
	try {

	    LogUtil.debug(this.getClass(), "getStreetSuburbsBySuburb");
	    LogUtil.debug(this.getClass(), "postcode=" + postcode);
	    LogUtil.debug(this.getClass(), "suburb=" + suburb);
	    streetSuburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.eq("postcode", postcode)).add(Restrictions.eq("suburb", suburb)).add(Restrictions.isNull("deleteDate")).addOrder(Order.asc("street")).addOrder(Order.asc("suburb")).list());

	} catch (HibernateException ex) {
	    throw new RemoteException("Unable to find street suburb list.", ex);
	}
	return streetSuburbList;
    }

    /**
     * Looks up an individual StreetSuburbVO matching street, suburb and
     * postcode.
     * 
     * @param street
     * @param suburb
     * @param postcode
     * @return the first result or null if none found.
     * @throws RemoteException
     */
    public StreetSuburbVO getUniqueStreetSuburb(String street, String suburb, String postcode) throws RemoteException {
	LogUtil.debug(this.getClass(), "getUniqueStreetSuburb(street=" + street + "suburb=" + suburb + "postcode=" + postcode + ")");

	List<StreetSuburbVO> streetSuburbList = new ArrayList<StreetSuburbVO>(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.eq("postcode", postcode)).add(Restrictions.eq("suburb", suburb)).add(Restrictions.eq("street", street)).add(Restrictions.isNull("deleteDate")).addOrder(Order.asc("street")).addOrder(Order.asc("suburb")).list());

	return (!streetSuburbList.isEmpty()) ? streetSuburbList.get(0) : null;
    }

    private Collection findStreetSuburbByStreetAndSuburb(String street, String suburb) throws RemoteException {
	Collection streetSuburbList = null;
	try {
	    LogUtil.debug(this.getClass(), "findStreetSuburbByStreetAndSuburb");
	    LogUtil.debug(this.getClass(), "street=" + street);
	    LogUtil.debug(this.getClass(), "suburb=" + suburb);
	    streetSuburbList = new ArrayList(hsession.createCriteria(StreetSuburbVO.class).add(Restrictions.like("street", street)).add(Restrictions.like("suburb", suburb)).add(Restrictions.isNull("deleteDate")).addOrder(Order.desc("streetSuburbID")).addOrder(Order.asc("street")).addOrder(Order.asc("suburb")).list());
	} catch (HibernateException ex) {
	    throw new RemoteException("Unable to find street suburb list.", ex);
	}
	return streetSuburbList;
    }

    private final String SQL_STREET_SUBURB_DATA = "select stsubid, street, suburb, postcode, state, country, [last-update] from PUB.[gn-stsub] where state = 'TAS' order by suburb asc, street asc"; // tasmania
																								     // only
    private final String SQL_NOM_STREET_SUBURB = "select 'x' from nom_address where street like ? and locality = ? and postcode = ?";
    private final String SQL_NOM_POSTCODE = "select 'x' from nom_address where street like ? and postcode = ?";

    public static final String CAT_LVL_0 = "TOTAL RECORDS";
    public static final String CAT_LVL_1 = "STREET,SUBURB AND POSTCODE DO NOT MATCH";
    public static final String CAT_LVL_2 = "STREET AND POSTCODE MATCH BUT LOCALITY DOES NOT MATCH";
    public static final String CAT_LVL_3 = "TBA";

    // final String DELIMITER = ",";

    /**
     * Perform a comparison between the RACT street/suburb table and a file
     * provided by the nomenclature board. It is assumed that the latest file is
     * uploaded into the nom_address table.
     * 
     * @return a hashtable of error categories and problem records.
     */
    public Hashtable compareStreetSuburbData() throws RemoteException {
	Connection connection = null;
	PreparedStatement statement1 = null;
	PreparedStatement statement2 = null;
	PreparedStatement statement3 = null;
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	ResultSet resultSet1 = null;
	ResultSet resultSet2 = null;
	ResultSet resultSet3 = null;

	Integer stsubid = null;
	String street = null;
	String suburb = null;
	String state = null;
	String country = null;
	Integer postcode = null;
	DateTime lastUpdate = null;
	StreetSuburbVO streetSuburb = null;
	Hashtable streetSuburbExceptionList = null;

	ArrayList cat0Exceptions = null;
	ArrayList cat1Exceptions = null;
	ArrayList cat2Exceptions = null;

	String stsubDetails = null;
	String streetComponents[] = null;
	int counter = 0;
	// int x = 0;
	int catLvl0ClientCount = 0;
	int catLvl1ClientCount = 0;
	int catLvl2ClientCount = 0;
	int clientCount = 0;
	try {
	    streetSuburbExceptionList = new Hashtable();
	    connection = dataSource.getConnection();
	    statement1 = connection.prepareStatement(SQL_STREET_SUBURB_DATA);
	    resultSet1 = statement1.executeQuery();

	    while (resultSet1.next()) {
		counter++;

		stsubid = new Integer(resultSet1.getString(1));
		//
		clientCount = clientMgr.getClientCountByPostalStreetSuburb(stsubid);
		catLvl0ClientCount = clientCount; // add it to running total

		street = resultSet1.getString(2);

		// match on the first part of the street only
		// streetComponents = new StringTokenizer(street," ");
		streetComponents = street.split(" ");
		if (streetComponents.length > 0) {
		    // first street part only
		    street = streetComponents[0];// 1
		    street += CommonConstants.WILDCARD;
		}

		suburb = resultSet1.getString(3);
		postcode = new Integer(resultSet1.getString(4));
		state = resultSet1.getString(5);
		country = resultSet1.getString(6);

		// create standard details to add to summary hashtable
		// stsubid,street name,suburb,postcode
		stsubDetails = stsubid + FileUtil.SEPARATOR_COMMA + street + FileUtil.SEPARATOR_COMMA + suburb + FileUtil.SEPARATOR_COMMA + postcode;

		// match on street and suburb
		statement2 = connection.prepareStatement(SQL_NOM_STREET_SUBURB);
		statement2.setString(1, street);// like
		statement2.setString(2, suburb);//
		statement2.setInt(3, postcode.intValue());
		resultSet2 = statement2.executeQuery();

		// no address with the street or suburb
		if (!resultSet2.next()) {

		    catLvl1ClientCount = clientCount;
		    addCategoryDetails(streetSuburbExceptionList, CAT_LVL_1, cat1Exceptions, stsubDetails, catLvl1ClientCount);

		    // address problems with the wrong locality
		    statement3 = connection.prepareStatement(SQL_NOM_POSTCODE);
		    statement3.setString(1, street);
		    statement3.setInt(2, postcode.intValue());
		    resultSet3 = statement3.executeQuery();
		    if (resultSet3.next()) {
			catLvl2ClientCount = clientCount;
			addCategoryDetails(streetSuburbExceptionList, CAT_LVL_2, cat2Exceptions, stsubDetails, catLvl2ClientCount);
		    }

		}

		// if (counter % 1000 == 0)
		// {
		// LogUtil.log(this.getClass(),"street suburb id = "+stsubid);
		// LogUtil.log(this.getClass(),"street = '"+street+"'");
		// LogUtil.log(this.getClass(),"suburb = '"+suburb+"'");
		// LogUtil.log(this.getClass(),"postcode = '"+postcode+"'");
		// LogUtil.log(this.getClass(),"exceptions = "+streetSuburbExceptionList.size());
		// }

		addCategoryDetails(streetSuburbExceptionList, CAT_LVL_0, cat0Exceptions, stsubDetails, catLvl0ClientCount);
	    }

	} catch (SQLException e) {
	    throw new RemoteException("Error performing match.", e);
	} finally {
	    ConnectionUtil.closeStatement(statement1);
	    ConnectionUtil.closeStatement(statement2);
	    ConnectionUtil.closeStatement(statement3);
	    ConnectionUtil.closeConnection(connection);
	}
	LogUtil.log(this.getClass(), "total processed = " + counter);
	return streetSuburbExceptionList;
    }

    private void addCategoryDetails(Hashtable streetSuburbExceptionList, String category, ArrayList catExceptions, String stsubDetails, int clientCount) {
	stsubDetails += FileUtil.SEPARATOR_COMMA + clientCount;
	catExceptions = (ArrayList) streetSuburbExceptionList.get(category);
	if (catExceptions == null) {
	    catExceptions = new ArrayList();
	}
	catExceptions.add(stsubDetails);
	streetSuburbExceptionList.put(category, catExceptions);
    }

}
