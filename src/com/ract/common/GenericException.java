/*
 * @(#)GenericException.java
 *
 */

package com.ract.common;

/**
 * Generic exception for any com.ract source. This class ensures that the stack
 * trace is not lost when using RMI. It also provides a layer of abstraction
 * between different tiers.
 * 
 * @author Glenn Lewis
 * @version 1.0, 6/5/2002
 */

public class GenericException extends Exception {
    /** The exception */
    private Throwable chainedException;

    /** Stack trace */
    private String stackTraceString;

    // convert a stack trace to a String so it can be serialized
    static public String generateStackTraceString(Throwable t) {
	return ExceptionHelper.getExceptionStackTrace((Exception) t);
    }

    /** Constructor */
    public GenericException() {
    }

    /** Constructor */
    public GenericException(String msg) {
	super(msg);
    }

    /** Constructor */
    public GenericException(Throwable chainedException) {
	this.chainedException = chainedException;
	stackTraceString = generateStackTraceString(chainedException);
    }

    /** Constructor */
    public GenericException(String msg, Throwable chainedException) {
	this(msg);
	this.chainedException = chainedException;
	stackTraceString = generateStackTraceString(chainedException);
    }

    /** Get the exception that */
    public Throwable getChainedException() {
	return chainedException;
    }

    // print out a chained stack trace (display deepest stack first)
    public String getStackTraceString() {
	if (chainedException == null) {
	    return null;
	}
	StringBuffer traceBuffer = new StringBuffer();
	if (chainedException instanceof GenericException) {
	    traceBuffer.append(((GenericException) chainedException).getStackTraceString());
	    traceBuffer.append("======= called by:\n");
	}
	traceBuffer.append(stackTraceString);
	return traceBuffer.toString();
    }

    // override Exception.toString()
    public String toString() {
	StringBuffer theMsg = new StringBuffer(super.toString());
	if (getChainedException() != null) {
	    theMsg.append("; \n\t====== Nested Exception: ").append(getChainedException());
	}
	return theMsg.toString();
    }

    // override Exception.getMessage()
    public String getMessage() {
	String superMsg = super.getMessage();
	if (getChainedException() == null) {
	    return superMsg;
	}
	StringBuffer theMsg = new StringBuffer();
	// get the chained exception's message
	String chainedMsg = getChainedException().getMessage();
	if (superMsg != null) {
	    theMsg.append(superMsg).append(": ").append(chainedMsg);
	} else {
	    theMsg.append(chainedMsg);
	}
	return theMsg.toString();
    }
}
