package com.ract.common;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.ActionSupport;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public abstract class BaseAction extends ActionSupport implements SessionAware {

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private Map session = null;

    public Collection getList() {
	return list;
    }

    public void setList(Collection list) {
	this.list = list;
    }

    final protected String LIST = "list";
    final protected String SAVE = "save";
    final protected String UPDATE = "update";
    final protected String REMOVE = "remove";

    public boolean isReadOnly() {
	return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
	this.readOnly = readOnly;
	LogUtil.debug(this.getClass(), "setting readonly to " + readOnly);
    }

    public String getMappedRequest() {
	return mappedRequest;
    }

    /**
     * InjectEJB annotation does not allow inheritance so this is the only way
     * to do it.
     */
    protected CommonMgrLocal commonMgr = CommonEJBHelper.getCommonMgrLocal();

    private boolean readOnly = false;

    private String mappedRequest;

    @SkipValidation
    public String view() {
	setReadOnly(true);
	setMappedRequest(LIST);
	return SUCCESS;
    }

    @SkipValidation
    public String create() {
	setMappedRequest(SAVE);
	return SUCCESS;
    }

    public String save() {
	updateAuditColumns();
	commonMgr.create(getModel()); // persist
	return list();
    }

    private void updateAuditColumns() {
	Object mod = getModel();
	if (mod instanceof Auditable) {
	    UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	    ((Auditable) mod).setLastUpdate(new DateTime());
	    ((Auditable) mod).setLastUpdateId(us.getUserId());
	}
    }

    @SkipValidation
    public String edit() {
	setMappedRequest(UPDATE);
	return SUCCESS;
    }

    public String update() {
	updateAuditColumns();
	commonMgr.update(getModel()); // merge
	return list();
    }

    @SkipValidation
    public String delete() {
	setReadOnly(true);
	setMappedRequest(REMOVE);
	return SUCCESS;
    }

    public String remove() {
	commonMgr.delete(getModel()); // remove
	return list();
    }

    @SkipValidation
    public abstract String list();

    protected Collection list;

    public String getActionClass() {
	return getClass().getSimpleName();
    }

    public String getActionMethod() {
	return mappedRequest;
    }

    protected static Pattern PATTERN_EMAIL = Pattern.compile("\\b(^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-])+((\\.com)|(\\.net)|(\\.org)|(\\.info)|(\\.edu)|(\\.mil)|(\\.gov)|(\\.biz)|(\\.ws)|(\\.us)|(\\.tv)|(\\.cc)|(\\.aero)|(\\.arpa)|(\\.coop)|(\\.int)|(\\.jobs)|(\\.museum)|(\\.name)|(\\.pro)|(\\.travel)|(\\.nato)|(\\..{2,3})|(\\..{2,3}\\..{2,3}))$)\\b");

    public void setActionMethod(String method) {
	this.mappedRequest = method;
    }

    public void setMappedRequest(String actionMethod) {
	String req = getActionClass() + "_" + actionMethod;
	this.mappedRequest = req;
	LogUtil.debug(this.getClass(), "setting mappedRequest to " + req);
    }

    public abstract Object getModel();

}
