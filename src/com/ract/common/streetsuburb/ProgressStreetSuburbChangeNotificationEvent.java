package com.ract.common.streetsuburb;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ract.common.SystemException;
import com.ract.common.integration.IntegrationMgrBean;
import com.ract.common.notifier.NotificationEvent;
import com.ract.util.LogUtil;

/**
 * Title: Master Project Description: Holds details about the change made to a
 * Progres stsub. Copyright: Copyright (c) 2002 Company: RACT
 * 
 * @author
 * @version 1.0
 */

public class ProgressStreetSuburbChangeNotificationEvent extends NotificationEvent {
    // The Node object is not Serializable so if the notification event fails
    // and gets saved as a NotificationEventFailure object we don't save the
    // node
    // object and only save the xml. When we get the stsub node we
    // recreate the Node from the xml if we have to.
    private transient Node stSubNode;

    private String stSubXML;

    private String action;

    private Integer stSubNumber;

    public ProgressStreetSuburbChangeNotificationEvent(Element stsubElement, String stsubAction) throws SystemException {
	if (stsubElement == null) {
	    throw new SystemException("The stsub Element parameter is null.");
	}
	if (!IntegrationMgrBean.DATA_PROGRESS_STREET_SUBURB.equalsIgnoreCase(stsubElement.getNodeName())) {
	    throw new SystemException("The stsubElement parameter '" + stsubElement.getNodeName() + "' is not a '" + IntegrationMgrBean.DATA_PROGRESS_STREET_SUBURB + "' element.");
	}

	this.stSubNode = stsubElement;
	// Save the xml string that defines the stsub node so that it can be
	// saved with a NotificationFailureEvent if required.
	this.stSubXML = this.stSubNode.toString();

	if (!("create".equalsIgnoreCase(stsubAction) || "update".equalsIgnoreCase(stsubAction) || "delete".equalsIgnoreCase(stsubAction))) {
	    throw new SystemException("The Data element does not contain a known 'action' attribute (" + stsubAction + ").");
	}
	LogUtil.log(this.getClass(), "stSubXML=" + this.stSubXML);
	this.action = stsubAction;

	// Find the stsub number
	NodeList nodeList = stsubElement.getElementsByTagName("stsubid");
	if (nodeList != null && nodeList.getLength() > 0) {
	    if (nodeList.getLength() > 1) {
		throw new SystemException("The ProgressStreetSuburb element contains too many 'stsubid' tags.");
	    } else {
		String stSubNumberStr = null;
		try {
		    stSubNumberStr = nodeList.item(0).getFirstChild().getNodeValue();
		    this.stSubNumber = new Integer(stSubNumberStr);
		} catch (Exception e) {
		    throw new SystemException("The ProgressStreetSuburb element contains an invalid 'StreetSuburbNo' tag value (" + stSubNumberStr + ").");
		}
	    }
	} else {
	    throw new SystemException("The ProgressStreetSuburb element does not contain a 'StreetSuburbNo' tag.");
	}

	if (this.stSubNumber.intValue() == 0) {
	    throw new SystemException("The stsub number must be greater than zero.");
	}
	setEventName(NotificationEvent.EVENT_PROGRESS_STREET_SUBURB_CHANGE);
    }

    public Node getStreetSuburbNode() {
	if (this.stSubNode == null) {
	    // Reconstitute the stsub node from the xml data
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setValidating(false);
	    try {
		DocumentBuilder builder = factory.newDocumentBuilder();
		StringReader reader = new StringReader(this.stSubXML);
		InputSource source = new InputSource(reader);
		Document document = builder.parse(source);
		if (document != null) {
		    this.stSubNode = document.getDocumentElement();
		}
	    } catch (ParserConfigurationException pce) {
		LogUtil.fatal(this.getClass(), pce);
	    } catch (IOException ioe) {
		LogUtil.fatal(this.getClass(), ioe);
	    } catch (SAXException se) {
		LogUtil.fatal(this.getClass(), se);
	    }
	}
	return this.stSubNode;
    }

    public String getAction() {
	return this.action;
    }

    public Integer getStreetSuburbNumber() {
	return this.stSubNumber;
    }

    public String getStreetSuburbXML() {
	return this.stSubXML;
    }

    public boolean isCreate() {
	return "create".equalsIgnoreCase(this.action);
    }

    public boolean isUpdate() {
	return "update".equalsIgnoreCase(this.action);
    }

    public boolean isDelete() {
	return "delete".equalsIgnoreCase(this.action);
    }

    public String toString() {
	StringBuffer sb = new StringBuffer(this.getEventName());
	sb.append(", action=");
	sb.append(this.action);
	sb.append(", stSubNumber=");
	sb.append(this.stSubNumber);
	return sb.toString();
    }

    public String getDetail() {
	return this.stSubXML;
    }

}
