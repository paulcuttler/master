package com.ract.common.streetsuburb;

import com.ract.common.SystemException;
import com.ract.common.qas.QASAddress;
import com.ract.common.transaction.TransactionAdapter;

public interface StreetSuburbAdapter extends TransactionAdapter {

    public Integer createProgressStreetSuburb(QASAddress q) throws SystemException;

}
