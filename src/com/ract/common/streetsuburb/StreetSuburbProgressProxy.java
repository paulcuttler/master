package com.ract.common.streetsuburb;

import java.io.IOException;

import com.progress.open4gl.ConnectException;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;
import com.ract.common.CommonConstants;
import com.ract.common.proxy.ProgressProxy;
import com.ract.streetsuburb.StreetSuburbProxy;

/**
 * CAD progress proxy implementation.
 * 
 * @author jyh
 * @version 1.0
 */

public class StreetSuburbProgressProxy extends StreetSuburbProxy implements ProgressProxy {
    public StreetSuburbProgressProxy() throws ConnectException, Open4GLException, SystemErrorException, IOException {
	super(CommonConstants.getProgressAppServerURL(), CommonConstants.getProgressAppserverUser(), CommonConstants.getProgressAppserverPassword(), null);

    }
}
