package com.ract.common.streetsuburb;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationEventSubscriber;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class NotificationEventSubscriberStreetSuburbImpl implements NotificationEventSubscriber, Serializable {

    public void processNotificationEvent(NotificationEvent event) throws RemoteException {
	if (event != null && event.eventEquals(NotificationEvent.EVENT_PROGRESS_STREET_SUBURB_CHANGE)) {
	    CommonEJBHelper.getStreetSuburbMgrLocal().processNotificationEvent(event);
	}
    }

    public String getSubscriberName() {
	return "STSUB";
    }
}
