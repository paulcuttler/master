package com.ract.common.streetsuburb;

import com.progress.open4gl.IntHolder;
import com.progress.open4gl.Open4GLException;
import com.ract.common.SystemException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.qas.QASAddress;
import com.ract.util.LogUtil;

public class ProgressStreetSuburbAdapter implements StreetSuburbAdapter {

    public ProgressStreetSuburbAdapter() {
    }

    public Integer createProgressStreetSuburb(QASAddress qasAddress) throws SystemException {

	StreetSuburbProgressProxy streetSuburbProgressProxy = null;

	String suburb = qasAddress.getSuburb();
	String street = qasAddress.getStreet();
	Integer postcode = new Integer(qasAddress.getPostcode());
	String state = qasAddress.getState();
	//
	String user_id = qasAddress.getUserid();
	IntHolder stsubid = new IntHolder();

	try {
	    streetSuburbProgressProxy = this.getStreetSuburbProgressProxy();

	    LogUtil.log(this.getClass(), "Attempting to add street suburb " + qasAddress.toXML());
	    streetSuburbProgressProxy.addStreetSuburb(suburb, street, postcode.intValue(), state, user_id, stsubid);

	} catch (Open4GLException ex) {
	    throw new SystemException("Cannot add StreetSuburb ", ex);
	}

	finally {
	    releaseStreetSuburbProgressProxy(streetSuburbProgressProxy);
	}

	return new Integer(stsubid.getIntValue());
    }

    private final static String SYSTEM_NAME = "ProgressStreetSuburb";

    private void releaseStreetSuburbProgressProxy(StreetSuburbProgressProxy streetSuburbProgressProxy) {
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_STREETSUBURB_PROXY, streetSuburbProgressProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), ex.getMessage());
	} finally {
	    streetSuburbProgressProxy = null;
	}
    }

    public void commit() throws SystemException {
    }

    public Exception getRollbackException() {
	return null;
    }

    public boolean getRollbackOnly() {
	return false;
    }

    public String getSystemName() {
	return SYSTEM_NAME;
    }

    public void release() throws SystemException {
    }

    public void rollback() throws SystemException {
    }

    public void setRollbackOnly(Exception exception) {
    }

    private StreetSuburbProgressProxy getStreetSuburbProgressProxy() throws SystemException {
	StreetSuburbProgressProxy streetSuburbProgressProxy = null;
	try {
	    streetSuburbProgressProxy = (StreetSuburbProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_STREETSUBURB_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return streetSuburbProgressProxy;
    }

}
