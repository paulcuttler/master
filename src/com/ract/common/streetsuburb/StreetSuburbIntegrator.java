package com.ract.common.streetsuburb;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemException;
import com.ract.common.integration.Integrator;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationMgrLocal;

/**
 * Integrates street suburb data.
 * 
 * @author dgk, jyh
 * @version 1.0
 */

public class StreetSuburbIntegrator implements Integrator {

    public String getEventMode() {
	return NotificationEvent.MODE_SYNCHRONOUS;
    }

    public void processXML(String dataType, String action, Node dataNode) throws SystemException {
	handleStreetSuburbUpdate(dataNode, action);
    }

    private void handleStreetSuburbUpdate(Node dataNode, String action) throws SystemException {
	// only one
	Element StreetSuburbElement = (Element) dataNode.getFirstChild();

	NotificationMgrLocal notificationMgrLocal = CommonEJBHelper.getNotificationMgrLocal();

	ProgressStreetSuburbChangeNotificationEvent progStreetSuburbEvent = new ProgressStreetSuburbChangeNotificationEvent(StreetSuburbElement, action);
	try {
	    notificationMgrLocal.notifyEvent(progStreetSuburbEvent);
	    notificationMgrLocal.commit();
	} catch (Exception rbe) {
	    throw new SystemException(rbe);
	}

    }

    public String getResponse() {
	return null;
    }

}
