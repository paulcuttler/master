package com.ract.common;

import java.io.File;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.membership.MembershipMgrLocal;

public class UploadECRReceiptAction extends ActionSupport {

	private File upfile;
	private String upfileFileName;
	private boolean result;

	@InjectEJB(name = "MembershipMgrBean")
	private MembershipMgrLocal membershipMgr;

	public File getUpfile() {
		return upfile;
	}

	public void setUpfile(File upfile) {
		this.upfile = upfile;
	}

	public String getUpfileFileName() {
		return upfileFileName;
	}

	public void setUpfileFileName(String upfileFileName) {
		this.upfileFileName = upfileFileName;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	@Override
	public String execute() throws Exception {
		setResult(membershipMgr.uploadECRRecipt(upfile, upfileFileName));
		return SUCCESS;
	}
}