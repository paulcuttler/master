package com.ract.common;

import java.rmi.RemoteException;

import com.ract.util.LogUtil;

/**
 * Do any cleanup that is required following an abrupt shutdown.
 * 
 * @author jyh
 * @version 1.0
 */

public class ShutdownHook extends Thread {

    public void run() {
	shutdown();
    }

    private void shutdown() {
	// shutdown server sockets
	try {
	    LogUtil.log(this.getClass(), "Shutting down socket servers.");
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    commonMgr.stopAllSocketServers();

	    CacheHandler.removeAll();
	} catch (RemoteException ex) {
	    LogUtil.log(this.getClass(), "error" + ex.getMessage());
	}
    }

}
