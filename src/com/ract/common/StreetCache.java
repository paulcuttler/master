package com.ract.common;

/**
 * Singleton to maintain street data.
 * 
 * @author John Holliday
 * @created 2 April 2003
 * @version 1.0
 */

public class StreetCache
// extends CacheBase
{

    // //initialise the singleton
    // private static String suburb;
    //
    // private static String state;
    //
    // private static StreetCache instance = new StreetCache();
    //
    // /**
    // * Implement the getItemList method of
    // * the super class. Return the list of
    // * cached items. Add items to the list
    // * if it is empty.
    // *
    // *@exception RemoteException Description
    // * of the Exception
    // */
    // public void initialiseList()
    // throws RemoteException
    // {
    // if(this.isEmpty())
    // {
    // StreetSuburbMgr streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
    // Collection dataList = null;
    //
    // try
    // {
    // dataList = streetSuburbMgr.getStreets(this.state, this.suburb);
    // if(dataList != null)
    // {
    // Iterator it = dataList.iterator();
    // while(it.hasNext())
    // {
    // Item state = new Item((String)it.next());
    // this.add(state);
    // }
    // }
    // }
    // catch(Exception e)
    // {
    // System.out.println("Failed to get list of states:\n");
    // e.printStackTrace();
    // }
    // }
    // }
    //
    // /**
    // * Return a reference the instance of
    // * this class.
    // *
    // *@return The instance value
    // */
    // public static StreetCache getInstance()
    // {
    // return instance;
    // }
    //
    // /**
    // * Return the list of streets. Use the
    // * getList() method to make sure the list
    // * has been initialised. A null or an
    // * empty list may be returned.
    // *
    // *@param state Description
    // * of the Parameter
    // *@param suburb Description
    // * of the Parameter
    // *@return The streetList
    // * value
    // *@exception RemoteException Description
    // * of the Exception
    // */
    // public ArrayList getStreetList(String state, String suburb)
    // throws RemoteException
    // {
    // if(this.suburb != suburb || this.state != state)
    // {
    // //reset the list
    // this.reset();
    // //set the new values
    // this.suburb = suburb;
    // this.state = state;
    // //
    // }
    // return this.getItemList();
    // }

}
