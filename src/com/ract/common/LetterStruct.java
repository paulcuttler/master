package com.ract.common;

import java.io.Serializable;

/**
 * A serializable representation of letter text
 * 
 * @todo rename to something reasonable.
 * 
 * @author David Kettlewell
 * @version 1.0
 */

public class LetterStruct implements Serializable {
    private String subSystem = null;

    private String type = null;

    private String typeText = null;

    private String heading = null;

    private String body1 = null;

    private String body2 = null;

    public LetterStruct() {
    }

    public String getSubSystem() {
	return subSystem;
    }

    public String getType() {
	return type;
    }

    public String getHeading() {
	return heading;
    }

    public String getBody1() {
	return body1;
    }

    public String getBody2() {
	return body2;
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    public void setType(String type) {
	this.type = type;
    }

    public void setTypeText(String typeText) {
	this.typeText = typeText;
    }

    public void setHeading(String heading) {
	this.heading = heading;
    }

    public void setBody1(String body1) {
	this.body1 = body1;
    }

    public void setBody2(String body2) {
	this.body2 = body2;
    }

}