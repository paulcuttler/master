package com.ract.common;

/**
 * Security exception
 * 
 * @author John Holliday
 * @version 1.0
 */

public class SecurityException extends GenericException {

    public SecurityException() {
	super("Security error.");
    }

    public SecurityException(String msg) {
	super(msg);
    }

}
