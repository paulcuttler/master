package com.ract.common;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Enumeration;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;

import com.ract.common.mail.MailMessage;
import com.ract.util.LogUtil;

/**
 * Email session bean.
 * 
 * @author John Holliday
 * @created 31 July 2002
 * @version 1.0
 */
@Stateless
@Remote({ MailMgr.class })
@Local({ MailMgrLocal.class })
public class MailMgrBean {

    // /**
    // * Send an email without attachments
    // *
    // *@param recipient The recipient's email address
    // *@param subject The subject of the email
    // *@param messageText The message to add in the body of the email
    // *@exception RemoteException Description of the Exception
    // */
    // public void sendMail(String recipient, String subject, String
    // messageText)
    // throws RemoteException
    // {
    // sendMail(recipient, subject, messageText, null, null, false);
    // }
    //
    // /**
    // * Support existing mail apis.
    // * @param recipient String
    // * @param subject String
    // * @param messageText String
    // * @param files Hashtable
    // * @throws RemoteException
    // */
    // public void sendMail(String recipient, String subject, String
    // messageText, Hashtable files)
    // throws RemoteException
    // {
    // sendMail(recipient, subject, messageText, null, files, false);
    // }
    //
    //
    // /**
    // * Send an email with attachments
    // *
    // *@param recipient The recipient's email address
    // *@param subject The subject of the email
    // *@param messageText The message to add in the body of the email
    // *@param files The hashtable of files with file handles (names) and file
    // paths
    // *@exception RemoteException Description of the Exception
    // */
    // public void sendMail(String recipient, String subject, String
    // messageText,String fromAddress, Hashtable files, boolean
    // specifyEnvironment)
    // throws RemoteException
    // {
    // sendMail(recipient,
    // null,
    // subject,
    // messageText,
    // fromAddress,
    // files,
    // specifyEnvironment, false);
    // }

    public void sendMail(MailMessage message) throws RemoteException {

	try {

	    LogUtil.log(this.getClass(), "message=" + message);

	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String systemName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);

	    String subject = message.getSubject();
	    if (subject == null) {
		subject = "";
	    }

	    if (message.isSpecifyEnvironment()) {
		subject.concat(" ");
		subject.concat("[");
		subject.concat(systemName); // add a system name suffix
		subject.concat("]");
	    }

	    Context ctx = new InitialContext();
	    Session session = (Session) ctx.lookup(CommonConstants.SERVICE_MAIL);

	    // set message details
	    Message msg = new MimeMessage(session);
	    if (message.getRecipient() == null) {
		throw new GenericException("No recipient has been specified.");
	    }
	    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(message.getRecipient(), true));
	    if (message.getBcc() != null) {
		msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(message.getBcc(), true));
	    }

	    msg.setSubject(subject);
	    msg.setSentDate(new Date());
	    // set reply if not null
	    if (message.getFrom() != null && !message.getFrom().equals("")) {
		InternetAddress replyToAddress[] = InternetAddress.parse(message.getFrom(), true);
		msg.setFrom(replyToAddress[0]); // first reply to is the email
						// address.
		msg.setReplyTo(replyToAddress);
	    }

	    // create a new multipart
	    Multipart mp = new MimeMultipart();

	    MimeBodyPart mbpText = new MimeBodyPart();

	    // null message body will result in an exception
	    if (message.getMessage() == null) {
		message.setMessage("");
	    }

	    if (message.isHtml()) {
		mbpText.setContent(message.getMessage(), "text/html");
	    } else {
		// add text component
		mbpText.setText(message.getMessage());
	    }

	    mp.addBodyPart(mbpText);

	    // initialise loop variables
	    MimeBodyPart mimeBodyPart = null;
	    FileDataSource fileDataSource = null;

	    // add the files to the message
	    if (message.getFiles() != null) {
		Enumeration en = message.getFiles().keys();
		String key = null;
		while (en.hasMoreElements()) {
		    key = (String) en.nextElement();
		    if (key != null) {
			mimeBodyPart = new MimeBodyPart();
			fileDataSource = new FileDataSource((String) message.getFiles().get(key));
			mimeBodyPart.setDataHandler(new DataHandler(fileDataSource));
			mimeBodyPart.setFileName(key);

			// and add its parts to it
			mp.addBodyPart(mimeBodyPart);
		    }
		}

	    }

	    msg.setContent(mp);

	    Transport.send(msg);
	} catch (Exception e) {
	    throw new EJBException(e.getMessage());
	}

    }
}
