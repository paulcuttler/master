/*
 * @(#)SourceSystem.java
 *
 */
package com.ract.common;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Vector;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

/**
 * Represent the different source computer systems.
 * 
 * @author Glenn Lewis
 * @created 14 January 2003
 * @version 1.0, 8/5/2002
 */
public final class SourceSystem implements Serializable, Writable, UserType {
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof SourceSystem))
	    return false;
	SourceSystem other = (SourceSystem) obj;
	if (abbreviation == null) {
	    if (other.abbreviation != null)
		return false;
	} else if (!abbreviation.equals(other.abbreviation))
	    return false;
	return true;
    }

    public static final String WRITABLE_CLASSNAME = "SourceSystem";

    private String abbreviation = null;

    private String systemName = null;

    /**
     * Constructor. (Private constructor ensures that this class can only be
     * constructed by member methods.)
     * 
     * @param theAbbreviation
     *            the abbreviation used for the source system in .
     */
    private SourceSystem(String theAbbreviation, String name) {
	this.abbreviation = theAbbreviation;
	this.systemName = name;
    }

    /**
     * Instance representing the CAD
     */
    public final static SourceSystem CAD = new SourceSystem("CAD", "Computer Aided Despatch");

    /**
     * Instance representing the CLAIMS
     */
    public final static SourceSystem CLAIMS = new SourceSystem("CLM", "Claims");

    /**
     * Instance representing the client system
     */
    public final static SourceSystem CLIENT = new SourceSystem("CLT", "Client");

    /**
     * Instance representing the membership system
     */
    public final static SourceSystem MEMBERSHIP = new SourceSystem("MEM", "Roadside");

    /**
     * Instance representing the insurance
     */
    public final static SourceSystem INSURANCE = new SourceSystem("INS", "Insurance");

    /**
     * Instance representing the insurance
     */
    public final static SourceSystem INSURANCE_RACT = new SourceSystem("INS_RAC", "RACT");

    /**
     * Instance representing the insurance
     */
    public final static SourceSystem INSURANCE_ISCU = new SourceSystem("INS_ISCU", "ISCU");

    /**
     * Instance representing the insurance
     */
    public final static SourceSystem INSURANCE_AURORA = new SourceSystem("INS_AUR", "Aurora");

    /**
     * Instance representing the Progress client system
     */
    public final static SourceSystem PROGRESS_CLIENT = new SourceSystem("PCLT", "Progress client");

    /**
     * Instance representing the Progress OCR receipting system
     */
    public final static SourceSystem OCR = new SourceSystem("OCR", "OCR Receipting");

    /**
     * Instance representing the Receptor receipting system
     */
    public final static SourceSystem RECEPTOR = new SourceSystem("RCTR", "Receptor receipting");

    /**
     * Instance representing the ECR receipting system
     */
    public final static SourceSystem ECR = new SourceSystem("ECR", "ECR receipting");

    /**
     * Instance representing the MRM system
     */
    public final static SourceSystem MRM = new SourceSystem("MRM", "MRM");

    /**
     * Source system as recognised by finance system (MFG-PRO)
     */
    public final static SourceSystem RECEPTOR_SHOPFRONT = new SourceSystem("SHP", RECEPTOR.getSystemName());

    /**
     * Instance representing Prosper
     */
    public final static SourceSystem PROSPER = new SourceSystem("PSR", "Prosper");

    public final static SourceSystem TRAVEL = new SourceSystem("TRV", "Travel");

    public final static SourceSystem NVI = new SourceSystem("NVI", "Fleet");

    public final static SourceSystem VI = new SourceSystem("VI", "Vehicle Inspection");

    public final static SourceSystem FIN = new SourceSystem("FIN", "Finance");

    public final static SourceSystem DD = new SourceSystem("DD", "Direct Debit");

    public final static SourceSystem FLEET = new SourceSystem("NVI", "Fleet");

    public final static SourceSystem SECURITY = new SourceSystem("SEC", "Security");

    public final static SourceSystem OCV = new SourceSystem("OCV", "OCV");

    public final static SourceSystem DPS = new SourceSystem("DPS", "Payment Express");

    public final static SourceSystem STREETSUBURB = new SourceSystem("SS", "StreetSuburb");

    public final static SourceSystem OPAL = new SourceSystem("OPAL", "OPAL");

    public final static SourceSystem CMO = new SourceSystem("CMO", "CMO");

    /**
     * Instance representing the membership system
     */
    public final static SourceSystem IVR_MEMBERSHIP = SourceSystem.MEMBERSHIP;

    /**
     * Instance representing the insurance
     */
    public final static SourceSystem IVR_INSURANCE = SourceSystem.INSURANCE;
    public final static SourceSystem IVR_CLAIMS = SourceSystem.CLAIMS;

    private static Vector systemList = new Vector();

    static {
	systemList.addElement(CLIENT);
	systemList.addElement(MEMBERSHIP);
	systemList.addElement(CMO);
	systemList.addElement(INSURANCE);
	systemList.addElement(INSURANCE_RACT);
	systemList.addElement(INSURANCE_AURORA);
	systemList.addElement(INSURANCE_ISCU);
	systemList.addElement(PROSPER);
	systemList.addElement(PROGRESS_CLIENT);
	systemList.addElement(CAD);
	systemList.addElement(FLEET);
	systemList.addElement(FIN);
	systemList.addElement(TRAVEL);
	systemList.addElement(NVI);
	systemList.addElement(VI);
	systemList.addElement(OCR);
	systemList.addElement(RECEPTOR);
	systemList.addElement(RECEPTOR_SHOPFRONT);
	systemList.addElement(ECR);
	systemList.addElement(MRM);
	systemList.addElement(IVR_MEMBERSHIP);
	systemList.addElement(IVR_INSURANCE);
	systemList.addElement(SECURITY);
	systemList.addElement(OCV);
	systemList.addElement(DPS);
	systemList.addElement(STREETSUBURB);
	systemList.addElement(CLAIMS);
	systemList.addElement(IVR_CLAIMS);
    }

    /**
     * Get the abbreviation used for the source system in
     * 
     * @return the abbreviation used for the source system in
     */
    public String getAbbreviation() {
	return (this.abbreviation == null) ? "UNK" : this.abbreviation;
    }

    public void setSystemName(String systemName) {
	this.systemName = systemName;
    }

    public String getSystemName() {
	return this.systemName;
    }

    public void setAbbreviation(String abbreviation) {
	this.abbreviation = abbreviation;
    }

    /**
     * Get the source system with a particular abbreviation
     * 
     * @param sourceSystem
     *            an abbreviation for the source system
     * @return The sourceSystem value
     * @exception GenericException
     *                Description of the Exception
     * @throws RACTException
     *             if the company code is unknown Changed to ignore case - as
     *             Progress does
     */
    public static SourceSystem getSourceSystem(String abbrev) throws GenericException {
	for (int i = 0; i < systemList.size(); i++) {
	    SourceSystem ss = (SourceSystem) systemList.elementAt(i);
	    if (ss.getAbbreviation().equalsIgnoreCase(abbrev)) {
		return ss;
	    }
	}
	throw new GenericException("Unknown source system '" + abbrev + "'.");
    }

    public SourceSystem() {
	// default constructor
    }

    public SourceSystem(Node sourceSystemNode) throws GenericException {
	NodeList elementList = sourceSystemNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("abbreviation".equals(nodeName) && valueText != null) {
		this.abbreviation = valueText;
	    } else if ("systemName".equals(nodeName) && valueText != null) {
		this.systemName = valueText;
	    }

	}

    }

    public static String getElectronicPaymentSourceSystem(String customerReference) {
	if (customerReference == null) {
	    return "BASE";
	} else if (customerReference.startsWith("9")) {
	    return IVR_MEMBERSHIP.getAbbreviation();
	} else {
	    return IVR_INSURANCE.getAbbreviation();
	}
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public String toString() {
	return this.abbreviation;
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("abbreviation", this.abbreviation);
	cw.writeAttribute("systemName", this.systemName);
	cw.endClass();
    }

    // hibernate user type interface methods

    int TYPES[] = { Types.VARCHAR };

    public int[] sqlTypes() {
	return TYPES;
    }

    public Class returnedClass() {
	return com.ract.common.SourceSystem.class;
    }

    public int hashCode(Object object) throws HibernateException {
	return object.hashCode();
    }

    public boolean equals(Object x, Object y) throws HibernateException {
	if (x == y) {
	    return true;
	}
	if (x == null) {
	    return false;
	}
	return x.equals(y);
    }

    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException {
	LogUtil.debug(this.getClass(), "nullSafeGet" + names[0]);
	SourceSystem sourceSystem = null;
	String srcSystem = resultSet.getString(names[0]);
	if (srcSystem != null) {
	    try {
		sourceSystem = SourceSystem.getSourceSystem(srcSystem);
	    } catch (GenericException ex) {
		// ignore and return null
	    }
	}
	LogUtil.debug(this.getClass(), "nullSafeGet" + sourceSystem);
	return sourceSystem;
    }

    public void nullSafeSet(PreparedStatement statement, Object value, int index) throws HibernateException, SQLException {
	if (value == null) {
	    statement.setNull(index, Types.VARCHAR);
	} else {
	    SourceSystem sourceSystem = (SourceSystem) value;
	    statement.setString(index, sourceSystem.getAbbreviation());
	}
    }

    public Object deepCopy(Object value) throws HibernateException {
	if (value == null) {
	    return null;
	} else {
	    SourceSystem sourceSystem = (SourceSystem) value;
	    return new SourceSystem(sourceSystem.getAbbreviation(), sourceSystem.getSystemName());
	}
    }

    public boolean isMutable() {
	return true;
    }

    public Serializable disassemble(Object value) throws HibernateException {
	return (Serializable) value;
    }

    public Object assemble(Serializable cached, Object owner) throws HibernateException {
	return cached;
    }

    public Object replace(Object original, Object target, Object owner) throws HibernateException {
	return deepCopy(original);
    }

}
