package com.ract.common.extracts;

/**
 * Title:        Master Project
 * Description:  Brings all of the projects together into one master project for deployment.
 * Copyright:    Copyright (c) 2003
 * Company:      RACT
 * @author
 * @version 1.0
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.SystemParameterVO;
import com.ract.common.admin.AdminCodeDescription;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.HTMLUtil;

public class ExtractHelper {
    private static GregorianCalendar myCal = null;

    public ExtractHelper() {
    }

    public static ExtractFormatter getExtractFormatter(String format, String publicationCode) throws GenericException {
	if (format.equals(ExtractFormatter.FORMAT_CLIENT_FUTURE_MEDIUM_EMAIL)) {
	    return new FutureMediumEmailExtractFormatter(publicationCode);
	} else {
	    return null;
	}

    }

    public static String getSQLStatement(String sqlFileName, HashMap sqlParams, String suffixDateTime) {
	// StringBuffer sqlCmd = new StringBuffer();

	File sqlFile = new File(sqlFileName);
	File genSqlFile = new File(sqlFile.toString().substring(0, sqlFile.toString().lastIndexOf(sqlFile.separator) + 1) + sqlFile.getName().substring(0, sqlFile.getName().indexOf(".")) + suffixDateTime + ".sql");

	try {
	    PrintWriter genSql = new PrintWriter(new BufferedWriter(new FileWriter(genSqlFile)));
	    String x = buildSQLCmd(sqlFile, genSql, sqlParams);

	    genSql.flush();
	    genSql.close();
	    return x;
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return null;
    }

    private static String buildSQLCmd(File sqlFile, PrintWriter genSql, HashMap sqlParams) {

	StringBuffer sqlCmd = new StringBuffer();

	BufferedReader in = null;

	try {

	    in = new BufferedReader(new FileReader(sqlFile));

	    int beginComment = -1;
	    int endComment = -1;
	    int singleLineComment = -1;
	    boolean processLine = true;
	    int parameterNum = 1;
	    int startOfLine = 0;
	    int endOfLine = 0;
	    String line = "";
	    int chNdx = 0;
	    int psNdx = 0;
	    int peNdx = 0;

	    while ((line = in.readLine()) != null) {

		// Ignore comments
		beginComment = line.indexOf("/*");
		singleLineComment = line.indexOf("//");

		if (beginComment > -1 || singleLineComment > -1) {
		    processLine = false;
		}

		if (processLine) {
		    // Is there a file to include

		    psNdx = line.indexOf("<include");
		    if (psNdx > -1) {
			peNdx = line.indexOf(">", psNdx);

			File fn = new File(sqlFile.toString().substring(0, sqlFile.toString().lastIndexOf(sqlFile.separator) + 1) + line.substring(psNdx + 8, peNdx).trim());

			sqlCmd.append(buildSQLCmd(fn, genSql, sqlParams));

		    } else {
			chNdx = 0;
			psNdx = line.indexOf("?", chNdx);

			while (psNdx > -1) {
			    peNdx = line.indexOf(" ", psNdx);
			    peNdx = (peNdx == -1) ? line.length() : peNdx;
			    String p = (String) sqlParams.get(line.substring(psNdx + 1, peNdx));
			    sqlCmd.append(line.substring(chNdx, psNdx));
			    sqlCmd.append(p);
			    chNdx = peNdx;
			    psNdx = line.indexOf("?", chNdx);
			}

			sqlCmd.append(line.substring(chNdx) + " ");

			endOfLine = sqlCmd.length();
			genSql.println(sqlCmd.substring(startOfLine, endOfLine));
			startOfLine = endOfLine;

		    }

		}
		endComment = line.indexOf("*/");
		if (endComment > -1 || singleLineComment > -1) {
		    processLine = true;
		}

	    }

	    // Close the files

	    in.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return sqlCmd.toString();
    }

    public static String getExtractFilename(String extFileName, String suffixDateTime) {
	return getExtractFilename(extFileName, suffixDateTime, ".csv");
    }

    public static String getExtractFilename(String extFileName, String suffixDateTime, String fileExtension) {
	return extFileName + suffixDateTime + fileExtension;
    }

    public static Hashtable getSqlParameters(HttpServletRequest request) {
	StringBuffer sb = new StringBuffer();

	Hashtable extractParams = (Hashtable) request.getSession().getAttribute("params");

	Enumeration en = request.getParameterNames();
	String element = "";
	HashMap params = new HashMap();

	while (en.hasMoreElements()) {
	    element = (String) en.nextElement();

	    if (extractParams.containsKey(element)) {
		String sqlTypeKey = extractParams.get(element) + "_SQLTYPE";
		String sqlType = (String) extractParams.get(sqlTypeKey);

		String pNum = sqlTypeKey.substring(1, 2);
		String[] vals = request.getParameterValues(element);

		if (sqlType.equalsIgnoreCase("list")) {
		    sb.setLength(0);
		    sb.append(createINList(vals));
		} else if (sqlType.trim().equalsIgnoreCase("date")) {
		    sb.setLength(0);
		    sb.append("convert(datetime,'");
		    sb.append(vals[0]);
		    sb.append("',103)");
		}

		params.put(pNum, sb.toString());
	    }

	}

	extractParams.put("sqlParams", params);

	return extractParams;
    }

    public static String createINList(String[] values) {
	int arrayLen = values.length;
	return createINList(values, arrayLen);
    }

    public static String createINList(String[] values, int arrayLen) {
	StringBuffer sb = new StringBuffer();

	sb.append("IN ( ");

	sb.append(createList(values, arrayLen));

	sb.append(")");

	return sb.toString();

    }

    public static String createList(String[] values, int arrayLen) {
	StringBuffer sb = new StringBuffer();

	for (int i = 0; i < arrayLen; i++) {
	    sb.append("'");
	    sb.append(values[i]);
	    sb.append("',");
	}
	sb.setLength(sb.length() - 1);

	return sb.toString();
    }

    public static HttpServletRequest launchJob(HttpServletRequest request, Hashtable extParams) throws RemoteException {

	CommonMgr cm = CommonEJBHelper.getCommonMgr();
	String rmiPort = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_PORT);
	String rmiHost = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_HOST);

	String dateString = request.getParameter("runDate");
	String hoursString = request.getParameter("runHours");
	String minsString = request.getParameter("runMins");
	DateTime runTime = null;

	try {
	    runTime = new DateTime(dateString);
	} catch (Exception e) {
	    throw new RemoteException(e.getMessage());
	}
	String mailTo = request.getParameter("email");

	String extractFile = (String) extParams.get("BASE_DIR") + (String) extParams.get("EXT_DIR") + (String) extParams.get("EXT_FILE");

	String sqlCmdFile = (String) extParams.get("BASE_DIR") + (String) extParams.get("SQL_DIR") + (String) extParams.get("SQL_FILE");

	GregorianCalendar gc = new GregorianCalendar();
	gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(hoursString), Integer.parseInt(minsString), 0);

	ScheduleMgr sMgr = CommonEJBHelper.getScheduleMgr();
	// Scheduler sched = sMgr.getScheduler();

	String jobName = (String) extParams.get("JOB_NAME");
	String jobTitle = (String) extParams.get("TITLE");
	String userId = UserSession.getUserSession(request.getSession()).getUserId();

	String triggerName = jobName + "Trigger";

	JobDetail jobDetail = new JobDetail(jobName, Scheduler.DEFAULT_GROUP, ExtractJob.class);

	SimpleTrigger trigger = new SimpleTrigger(triggerName, Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);

	JobDataMap dataMap = jobDetail.getJobDataMap();

	jobDetail.setDescription(jobTitle);

	dataMap.put("userId", userId);
	dataMap.put("createDateTime", DateUtil.formatLongDate(new DateTime()));

	dataMap.put("emailAddress", mailTo);
	dataMap.put("extractFile", extractFile);
	dataMap.put("sqlCmdFile", sqlCmdFile);
	dataMap.put("title", jobTitle);
	dataMap.put("sqlParams", (HashMap) extParams.get("sqlParams"));

	String messageText = null;

	String providerURL = rmiHost + ":" + rmiPort;

	dataMap.put("providerURL", providerURL);

	try {

	    sMgr.scheduleJob(jobDetail, trigger);

	    request.setAttribute("heading", jobTitle + " Scheduled");
	    messageText = jobTitle + " has been scheduled to run at" + "<br/>" + (new DateTime(gc.getTime())).formatLongDate();
	    request.setAttribute("message", messageText);
	} catch (Exception e) {
	    request.setAttribute("heading", "WARNING:" + jobTitle + " not scheduled");
	    messageText = "The system was unable to schedule the " + jobTitle + " : <br/>" + e;
	    request.setAttribute("message", messageText);
	}

	return request;
    }

    /**
   *
   */
    public static Vector getHTMLParameters(HttpServletRequest request) {
	CommonMgr cMgr = null;

	try {
	    cMgr = CommonEJBHelper.getCommonMgr();
	} catch (Exception re) {
	    re.printStackTrace();
	}

	Hashtable params = (Hashtable) request.getSession().getAttribute("params");
	HashMap classMap = new HashMap();
	HashMap methodMap = new HashMap();

	StringBuffer pName = new StringBuffer();
	boolean checking = true;
	Vector pVector = new Vector();
	AdminCodeDescription ad = new AdminCodeDescription();

	int pNum = 0;

	for (int i = 0; checking == true; i++) {
	    pNum = i + 1;
	    pName.append("P" + pNum + "_DESCR");

	    if (params.containsKey(pName.toString())) {
		String label = (String) params.get(pName.toString());

		pName.setLength(3);
		pName.append("HTMLTYPE");
		String type = (String) params.get(pName.toString());

		pName.setLength(3);
		pName.append("NAME");
		String inputName = (String) params.get(pName.toString());

		pName.setLength(3);
		pName.append("CLASS");
		String className = (String) params.get(pName.toString());

		pName.setLength(3);
		pName.append("METHOD");
		String methodName = (String) params.get(pName.toString());

		String classMethod = className + ":" + methodName;

		String packageName = null;
		String classToLoad = null;
		Class c = cMgr.getClass();
		Method m = null;

		if (methodMap.containsKey(methodName)) {
		    // c = (Class) classMap.get(className);
		    m = (Method) methodMap.get(methodName);
		    System.out.println("Class already found " + c.getName() + " method is " + m.getName() + " has " + m.getReturnType().getName());
		} else {

		    try {
			m = c.getMethod(methodName, null);
			methodMap.put(methodName, m);
		    } catch (Exception cle) {
			System.out.println(className + " " + cle.getMessage() + " not found");
		    }
		}

		try {
		    if (type.equalsIgnoreCase("selectList")) {

			type = HTMLUtil.selectList(inputName, m.invoke((Object) cMgr, null));
		    } else if (type.equalsIgnoreCase("inputTextBox")) {
			type = HTMLUtil.inputTextBox(inputName, (String) m.invoke((Object) cMgr, null), " ");
		    }

		    pVector.add(i, new AdminCodeDescription(label, type));
		} catch (Exception e) {
		}
	    } else {
		checking = false;
	    }

	    pName.setLength(0);
	}

	return pVector;
    }

    public static String getSuffixDateTime() {

	NumberFormat nf = NumberFormat.getNumberInstance();
	nf.setMinimumIntegerDigits(2);
	nf.setParseIntegerOnly(true);
	myCal = new GregorianCalendar();

	return "_" + new Integer(myCal.get(java.util.Calendar.YEAR)).toString() + nf.format(((long) myCal.get(java.util.Calendar.MONTH)) + 1) + nf.format((long) myCal.get(java.util.Calendar.DAY_OF_MONTH)) + "_" + nf.format((long) myCal.get(java.util.Calendar.HOUR_OF_DAY)) + nf.format((long) myCal.get(java.util.Calendar.MINUTE)) + nf.format((long) myCal.get(java.util.Calendar.SECOND)) + nf.format((long) myCal.get(java.util.Calendar.MILLISECOND));
    }

}
