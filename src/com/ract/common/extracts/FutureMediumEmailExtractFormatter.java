package com.ract.common.extracts;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.GenericException;
import com.ract.common.Publication;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipVO;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

public class FutureMediumEmailExtractFormatter extends ExtractFormatter {

    @Override
    public String getExtractName() {
	return FORMAT_CLIENT_FUTURE_MEDIUM_EMAIL;
    }

    private static final String MEM_SUBSCRIPTION_EXPIRY_PERIOD = "MEM_SUBSCRIPTION_EXPIRY_PERIOD";

    /*
     * Format Specification:
     * 
     * Field Type Salutation First name String (255) Last name String (255)
     * Email String (255) External Link String (255) Address String (255) Suburb
     * String (255) State String (255) Postcode String (4)
     */

    public FutureMediumEmailExtractFormatter(String publicationCode) throws GenericException {
	LogUtil.log(this.getClass(), "FutureMediumEmailExtractFormatter start");
	contents = new StringBuffer();

	contents.append("Salutation");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("First name");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("Last name");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("Email");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("Client number");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("Address");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("Suburb");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("State");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("Postcode");
	contents.append(FileUtil.SEPARATOR_COMMA);
	contents.append("Product");
	contents.append(FileUtil.NEW_LINE);

	// top level node
	try {
	    // prescribed date format
	    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    DateTime now = new DateTime();
	    String today = sd.format(now);
	    ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	    // use EJB-QL
	    Collection clients = clientMgr.getSubscribedClients(publicationCode);

	    Publication publication = CommonEJBHelper.getCommonMgr().getPublication(publicationCode);

	    LogUtil.log(this.getClass(), "Subscribed clients=" + clients.size());
	    ClientVO client = null;
	    String address = "";
	    String emailAddress = null;
	    String membershipSubscriptionExpiryPeriod = null;
	    try {
		membershipSubscriptionExpiryPeriod = FileUtil.getProperty("master", MEM_SUBSCRIPTION_EXPIRY_PERIOD);
	    } catch (Exception e) {
		throw new GenericException("Unable to get membership subscription expiry period for email extract.", e);
	    }
	    int counter = 0;
	    int subscribers = 0;
	    Collection membershipList = null;
	    MembershipVO membership = null;
	    boolean validMembership = false;
	    for (Iterator it = clients.iterator(); it.hasNext();) {
		validMembership = false;
		counter++;
		address = "";
		client = (ClientVO) it.next();
		emailAddress = client.getEmailAddress();
		// validate email address
		try {
		    emailAddress = StringUtil.validateEmail(emailAddress, false);
		} catch (Exception e) {
		    LogUtil.warn(this.getClass(), client.getClientNumber() + ": " + e.getMessage());
		    continue;
		}

		LogUtil.debug(this.getClass(), "publication.isMembershipDependent()=" + publication.isMembershipDependent());
		LogUtil.debug(this.getClass(), "client=" + client.getClientNumber() + ": " + client.getStatus());
		if (publication.isMembershipDependent()) {
		    membershipList = membershipMgr.findMembershipByClientNumber(client.getClientNumber());
		    LogUtil.debug(this.getClass(), "membershipList=" + membershipList);
		    membership = membershipList.iterator().hasNext() ? (MembershipVO) membershipList.iterator().next() : null;
		    LogUtil.debug(this.getClass(), "membership=" + membership);
		    // if there is a membership
		    if (membership != null) {
			LogUtil.debug(this.getClass(), "membership=" + membership);
			DateTime subscriptionExpiryDate = null;
			try {
			    subscriptionExpiryDate = membership.getExpiryDate().add(new Interval(membershipSubscriptionExpiryPeriod));
			    LogUtil.debug(this.getClass(), "subscriptionExpiryDate=" + subscriptionExpiryDate);
			} catch (ParseException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
			LogUtil.debug(this.getClass(), "membership=" + membership.getExpiryDate());
			// and the expiry date is not more that 6 months expired
			if (subscriptionExpiryDate.onOrAfterDay(now.getDateOnly()) && membership.isActive()) {
			    LogUtil.debug(this.getClass(), "validMembership membership=" + membership);
			    validMembership = true;
			}
		    }

		    if (validMembership) {
			subscribers++;
			contents.append(client.getSalutation());
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(client.getGivenNames() != null ? client.getGivenNames().trim() : "");
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(client.getSurname() != null ? client.getSurname().trim() : "");
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(emailAddress);
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(client.getClientNumber().toString());
			contents.append(FileUtil.SEPARATOR_COMMA);
			if (client.getPostProperty() != null) {
			    address += client.getPostProperty();
			}
			if (client.getPostalAddress() != null) {
			    if (client.getPostalAddress().getPropertyQualifier() != null) {
				if (address.length() > 0) {
				    address += " ";
				}
				address += client.getPostalAddress().getPropertyQualifier();
			    }
			    if (client.getPostalAddress().getStreet() != null) {
				if (address.length() > 0) {
				    address += " ";
				}
				address += client.getPostalAddress().getStreet();
			    }
			}
			contents.append(address);
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(client.getPostalAddress().getSuburb());
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(client.getPostalAddress().getState());
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(client.getPostalAddress().getPostcode());
			contents.append(FileUtil.SEPARATOR_COMMA);
			contents.append(membership.getProductCode());
			contents.append(FileUtil.NEW_LINE);
		    }
		}
		if (counter % 1000 == 0) {
		    LogUtil.log(this.getClass(), "Records=" + counter);
		    LogUtil.log(this.getClass(), "Subscribers=" + subscribers);
		}
	    }
	    LogUtil.log(this.getClass(), "Total records=" + counter);
	    LogUtil.log(this.getClass(), "Total subscribers=" + subscribers);

	} catch (RemoteException ex) {
	    throw new GenericException(ex);
	}
	LogUtil.log(this.getClass(), "FutureMediumEmailExtractFormatter end");
    }

    public String getColumnCount() {
	return "12";
    }

}
