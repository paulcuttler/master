package com.ract.common.extracts;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.MailMgr;
import com.ract.common.mail.MailMessage;
import com.ract.util.LogUtil;

/**
 * Scheduled Extract tasks
 * 
 * @author dgk, JYH
 * @created 31 July 2002
 * @version 1.0
 * 
 *          Rewritten to use the Job interface
 */

public class ExtractJob implements Job {

    /**
     * Constructor for the RDSSalesByBranchRunBatch object
     */
    public ExtractJob() {
    }

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	String instName = context.getJobDetail().getName();
	String instGroup = context.getJobDetail().getGroup();

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();

	try {

	    LogUtil.log(this.getClass(), "trigger " + context.getTrigger().getStartTime());

	    String providerURL = dataMap.getString("providerURL");

	    String emailAddress = dataMap.getString("emailAddress");
	    String title = dataMap.getString("title");

	    ExtractMgr extractMgr = CommonEJBHelper.getExtractMgr();

	    Class c = extractMgr.getClass();
	    if (c != null) {
		System.out.println("RLG ++++++++ MGR " + c.getName());
	    }
	    String result = extractMgr.doExtract(dataMap);

	    MailMessage message = new MailMessage();
	    message.setRecipient(emailAddress);
	    message.setSubject(title);
	    message.setMessage(result);

	    MailMgr mailMgr = CommonEJBHelper.getMailMgr();
	    mailMgr.sendMail(message);
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e.getMessage());
	    throw new JobExecutionException(e, false);
	}
    }

}
