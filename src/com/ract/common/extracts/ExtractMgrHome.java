package com.ract.common.extracts;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

/**
 * Title: Membership system Description: Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public interface ExtractMgrHome extends EJBHome {
    public ExtractMgr create() throws CreateException, RemoteException;
}
