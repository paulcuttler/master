package com.ract.common.extracts;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.sql.DataSource;

import org.quartz.JobDataMap;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.util.ConnectionUtil;

/**
 * @todo commenting,move package,etc!!!
 * 
 *       Title: Master Project Description: Brings all of the projects together
 *       into one master project for deployment. Copyright: Copyright (c) 2003
 *       Company: RACT
 * @author
 * @version 1.0
 */

public class ExtractMgrBean implements SessionBean {
    private SessionContext sessionContext;

    private DataSource dataSource;

    public static String extractFileName;

    public static String sqlPath;

    public static String extractPath;

    //
    private static CommonMgr myComMgr;

    /**
     * Description of the Method
     */
    public void ejbCreate() {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbRemove() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbActivate() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbPassivate() throws RemoteException {
    }

    /**
     * Sets the sessionContext attribute of the DeliveryRoundMgrBean object
     * 
     * @param sessionContext
     *            The new sessionContext value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
	dataSource = this.getDataSource();
    }

    /**
     * @return The dataSource value
     * @exception RemoteException
     *                Description of the Exception
     * @todo should be in the setsessioncontext
     */
    public DataSource getDataSource() throws RemoteException {
	if (this.dataSource == null) {
	    try {
		javax.naming.Context context = new javax.naming.InitialContext();
		try {
		    dataSource = (DataSource) context.lookup(CommonConstants.DATASOURCE_CLIENT);
		} catch (Exception e) {
		    throw new EJBException("Error looking up dataSource: " + e.toString());
		}
	    } catch (Exception e) {
		throw new EJBException("Error initializing context:" + e.toString());
	    }
	}
	return this.dataSource;
    }

    public String doExtract(JobDataMap dataMap) throws RemoteException {
	myComMgr = CommonEJBHelper.getCommonMgr();

	String suffixDateTime = ExtractHelper.getSuffixDateTime();

	// generate the sql to use for the extract

	String sqlCmd = ExtractHelper.getSQLStatement(dataMap.getString("sqlCmdFile"), (HashMap) dataMap.get("sqlParams"), suffixDateTime);

	// find out where to put the extract

	String extractTo = ExtractHelper.getExtractFilename(dataMap.getString("extractFile"), suffixDateTime);

	// run the extract

	String recs = CommonEJBHelper.getReportMgr().getCSVResultSet(sqlCmd, extractTo);

	return recs + "\nCSV file is " + extractTo + ". ";
    }

    /**
     * @todo this is potentially dangerous as it may process ANY SQL statements.
     */
    public void executeSQL(String sql) throws RemoteException {
	Connection connection = null;
	PreparedStatement ps = null;
	try {
	    connection = this.getDataSource().getConnection();
	    ps = connection.prepareStatement(sql);

	    boolean i = ps.execute();
	} catch (Exception e) {
	    throw new RemoteException(e.getMessage());
	} finally {
	    ConnectionUtil.closeConnection(connection, ps);
	}

	// closeConnection(connection);
    }

    public void setExtractFileName(String fileName) {
	this.extractFileName = fileName;
    }

}
