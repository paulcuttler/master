package com.ract.common.extracts;

import com.ract.common.GenericException;

public abstract class ExtractFormatter {

    public static final String FORMAT_CLIENT_FUTURE_MEDIUM_EMAIL = "FMSL";

    protected static StringBuffer contents;

    public ExtractFormatter() throws GenericException {
	contents = new StringBuffer();
    }

    public abstract String getColumnCount();

    public String toString() {
	return contents.toString();
    }

    public abstract String getExtractName();

}
