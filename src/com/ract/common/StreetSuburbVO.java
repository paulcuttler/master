package com.ract.common;

import java.rmi.RemoteException;

import com.ract.util.DateTime;

/**
 * Street Suburb value object to store street/suburb combinations against an
 * arbitrary identifier. It is cacheable as it implements a cacheditem.
 * 
 * @hibernate.class table="[gn-stsub]" lazy="false"
 * 
 */

public class StreetSuburbVO extends ValueObject implements /* CachedItem, */Writable {

    public static final String WRITABLE_CLASSNAME = "StreetSuburb";

    protected Integer streetSuburbID;

    protected String suburb;

    protected String street;

    protected DateTime modifyDate;

    protected String country;

    protected String state;

    protected String modifyBy;

    protected String postcode;

    protected DateTime deleteDate;

    final String WHITESPACE = " ";

    final String COMMA = ", ";

    public StreetSuburbVO() {
    }

    public String getStreetSuburbLine() {
	StringBuffer sSubLine = new StringBuffer();
	sSubLine.append(this.getStreet());
	sSubLine.append(WHITESPACE);
	sSubLine.append(this.getSuburb());
	sSubLine.append(COMMA);
	sSubLine.append(this.getState());
	sSubLine.append(WHITESPACE);
	sSubLine.append(this.getPostcode());
	sSubLine.append(COMMA);
	sSubLine.append(this.getCountry());
	return sSubLine.toString();
    }

    // public String getKey()
    // {
    // return this.getStreetSuburbID().toString();
    // }

    public void setSuburb(String suburb) {
	this.suburb = suburb;
	// modified = true;
    }

    public String toString() {
	return this.getStreetSuburbID() + " " + this.getStreet() + " " + this.getSuburb();
    }

    public void setStreet(String street) {
	this.street = street;
	// modified = true;
    }

    public void setState(String state) {
	this.state = state;
	// modified = true;
    }

    public void setModifyDate(DateTime modifyDate) {
	this.modifyDate = modifyDate;
	// modified = true;
    }

    public void setModifyBy(String modifyBy) {
	this.modifyBy = modifyBy;
	// modified = true;
    }

    public void setCountry(String country) {
	this.country = country;
	// modified = true;
    }

    public void setDeleteDate(DateTime deleteDate) {
	this.deleteDate = deleteDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="postcode"
     */
    public String getPostcode() {
	return this.postcode;
    }

    public void setPostcode(String postcode) {
	this.postcode = postcode;
	// modified = true;
    }

    // protected boolean modified;

    // public boolean isModified()
    // {
    // return modified;
    // }

    public void setStreetSuburbID(Integer streetSuburbID) {
	this.streetSuburbID = streetSuburbID;
	// modified = true;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="country"
     */
    public String getCountry() {
	return country;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[last-updateid]"
     */
    public String getModifyBy() {
	return modifyBy;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[last-update]"
     */
    public DateTime getModifyDate() {
	return modifyDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="state"
     */
    public String getState() {
	return state;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="street"
     */
    public String getStreet() {
	return street;
    }

    /**
     * @hibernate.id column="stsubid" generator-class="assigned"
     * @return Integer
     */
    public Integer getStreetSuburbID() {
	return streetSuburbID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="deleteDate"
     */
    public DateTime getDeleteDate() {
	return deleteDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="suburb"
     */
    public String getSuburb() {
	return suburb;
    }

    // public boolean keyEquals(String key)
    // {
    // String myKey = this.getKey();
    // if(key == null)
    // {
    // // See if the code is also null.
    // if(myKey == null)
    // {
    // return true;
    // }
    // else
    // {
    // return false;
    // }
    // }
    // else
    // {
    // // We now know the key is not null so this wont throw a null pointer
    // exception.
    // return key.equalsIgnoreCase(myKey);
    // }
    // }

    protected void validateMode(String newMode) throws ValidationException {
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);

	if (this.country != null && this.country.length() > 0) {
	    cw.writeAttribute("country", this.country);

	}
	cw.writeAttribute("modifyBy", this.modifyBy);
	cw.writeAttribute("modifyDate", this.modifyDate);
	cw.writeAttribute("postcode", this.postcode);
	cw.writeAttribute("state", this.state);
	cw.writeAttribute("street", this.street);
	cw.writeAttribute("streetSuburbID", this.streetSuburbID);
	cw.writeAttribute("suburb", this.suburb);
	cw.writeAttribute("deleteDate", this.deleteDate);

	cw.endClass();
    }
}
