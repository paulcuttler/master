package com.ract.common.admin;

import java.io.Serializable;

/**
 * ???
 * 
 * @author rlg
 * @version 1.0
 */

public class AdminCodeDescription implements Comparable, Serializable {
    String code;

    String description;

    String sortOn;

    int sortOrder; // 0 = ascending, 1 = descending

    public AdminCodeDescription() {
    }

    public AdminCodeDescription(String c, String d) {
	code = c;
	description = d;
	sortOn = c;
	sortOrder = 0;
    }

    public AdminCodeDescription(String c, String d, int so) {
	code = c;
	description = d;
	sortOn = c;
	sortOrder = so;
    }

    public AdminCodeDescription(String c, String d, String s) {
	code = c;
	description = d;
	sortOn = s;
	sortOrder = 0;
    }

    public AdminCodeDescription(String c, String d, String s, int so) {
	code = c;
	description = d;
	sortOn = s;
	sortOrder = so;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String c) {
	code = code;
    }

    public void setDescription(String d) {
	description = d;
    }

    public String getDescription() {
	return description;
    }

    public int compareTo(Object o) {

	AdminCodeDescription c = (AdminCodeDescription) o;

	int w = this.sortOn.compareToIgnoreCase(c.sortOn);
	if (sortOrder == 0) {
	    return w;
	} else {
	    return w * -1;
	}
    }

    public boolean equals(Object o) {
	AdminCodeDescription c = (AdminCodeDescription) o;

	boolean w = (this.sortOn.compareToIgnoreCase(c.sortOn) == 0);

	if (sortOrder == 0) {
	    return w;
	} else {
	    return !w;
	}
    }
}