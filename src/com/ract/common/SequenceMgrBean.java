package com.ract.common;

import java.math.BigDecimal;
import java.rmi.RemoteException;;
import java.sql.*;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.*;

import com.ract.util.*;

@Stateless
@Local({SequenceMgr.class})
@Remote({SequenceMgrLocal.class})
public class SequenceMgrBean
{
	@PersistenceContext(unitName="master")
	EntityManager em;
	
	@Resource(mappedName="java:/ClientDS")
  private DataSource dataSource; 
 
	public void resetTableId(String tableName, Integer Id) throws RemoteException
	{
		Connection connection = null;
		PreparedStatement statement = null;
    try
    {
      connection = dataSource.getConnection();
      statement = connection.prepareStatement("update gn_sequence set last_id = ? where table_name = ?"); 
      statement.setInt(1, Id);
      statement.setString(2, tableName);      
      statement.executeUpdate();
    }
    catch(SQLException e)
    {
      throw new RemoteException("Unable to update id to "+Id+" for " + tableName + ". ", e);
    } 
    finally
    {
      ConnectionUtil.closeConnection(connection, statement);
    }		
	}
	
  /**
   * <p>Get the next id for the specified table name.</p>
   *
   * @param tableName the name of the table to get the next sequence for.
   * @return the id for the given table
   */
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)  	
  public Integer getNextID(String tableName)
      throws RemoteException
  {
    Integer nextID = null;
    CallableStatement statement = null;
    Connection connection = null;
    try 
    {
      connection = dataSource.getConnection();
      statement = connection.prepareCall("{call getNextID(?,?)}");
      statement.setString(1, tableName);
      statement.registerOutParameter(2, Types.INTEGER);
      statement.execute();
      nextID = new Integer(statement.getInt(2));
LogUtil.log(this.getClass(),"["+tableName+"] "+nextID);

    }
    catch(SQLException e)
    {
      throw new RemoteException("Unable to get the next id for " + tableName + ". ", e);
    }
    finally
    {
      ConnectionUtil.closeConnection(connection, statement);
    }
    return nextID;
  }
}
