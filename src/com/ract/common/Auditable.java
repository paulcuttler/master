package com.ract.common;

import com.ract.util.DateTime;

/**
 * Allows when an update has occurred and who updated the record to be saved
 * when reference data is updated. The BaseAction class is aware of this
 * interface and will automatically save this data to the database if the model
 * implements this interface and the database columns exist in the database.
 * 
 * @author hollidayj
 * 
 */
public interface Auditable {

    public void setLastUpdate(DateTime lastUpdate);

    public void setLastUpdateId(String updateId);

}
