package com.ract.common;

public class NotImplementedException extends GenericException {

    public NotImplementedException() {
	super("Feature not implemented.");
    }

    public NotImplementedException(String msg) {
	super(msg + ": Feature not implemented.");
    }
}