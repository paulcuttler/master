package com.ract.common;

/**
 * Cache of street suburb data
 * 
 * @author John Holliday
 * @version 1.0
 */

public class StreetSuburbCache
// extends CacheBase
{
    //
    // private static StreetSuburbCache instance = new StreetSuburbCache();
    //
    // private StreetSuburbHome streetSuburbHome = null;
    //
    // /**
    // * Return a reference to the StreetSuburbHome home interface. Initialise
    // * the reference if this is the first call.
    // */
    // private StreetSuburbHome getHomeInterface()
    // throws RemoteException
    // {
    // if(this.streetSuburbHome == null)
    // {
    // this.streetSuburbHome = CommonEJBHelper.getStreetSuburbHome();
    // }
    // return this.streetSuburbHome;
    // }
    //
    // /**
    // * Implement the getItemList method of the super class.
    // * Return the list of cached items. Add items to the list if it is empty.
    // */
    // public void initialiseList()
    // throws RemoteException
    // {
    // if(this.isEmpty())
    // {
    //
    // StreetSuburbHome home = this.getHomeInterface();
    // Collection dataList = null;
    //
    // try
    // {
    // dataList = home.findAll();
    // if(dataList != null)
    // {
    // Iterator it = dataList.iterator();
    // while(it.hasNext())
    // {
    // StreetSuburb item = (StreetSuburb)it.next();
    // StreetSuburbVO vo = item.getVO();
    // this.add(vo);
    // }
    // }
    // }
    // catch(Exception e)
    // {
    // System.out.println("Failed to get list of street suburbs:\n");
    // e.printStackTrace();
    // }
    // }
    // }
    //
    // /**
    // * Return a reference the instance of this class.
    // */
    // public static StreetSuburbCache getInstance()
    // {
    // return instance;
    // }
    //
    // /**
    // * Return the specified Membership Transaction Type value object.
    // * Search the list of Membership Transaction Types for one that has the
    // same code.
    // * Use the getList() method to make sure the list has been initialised.
    // */
    // public StreetSuburbVO getStreetSuburb(String stsubid)
    // throws RemoteException
    // {
    // return(StreetSuburbVO)this.getItem(stsubid);
    // }
    //
    // /**
    // * Return the list of products.
    // * Use the getList() method to make sure the list has been initialised.
    // * A null or an empty list may be returned.
    // */
    // public ArrayList getStreetSuburbList()
    // throws RemoteException
    // {
    // return this.getItemList();
    // }

}
