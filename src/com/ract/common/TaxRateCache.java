package com.ract.common;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import com.ract.util.DateTime;

public class TaxRateCache extends CacheBase2 {
    private static TaxRateCache instance = new TaxRateCache();

    private TaxRateVO latestTaxRate = null;

    public static TaxRateCache getInstance() {
	return instance;
    }

    public void initialiseList() throws RemoteException {
	if (isEmpty()) {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    Collection taxRateList = commonMgr.getTaxRates();
	    if (taxRateList != null) {
		TaxRateVO taxRateVO;
		Iterator taxRateListIterator = taxRateList.iterator();
		while (taxRateListIterator.hasNext()) {
		    taxRateVO = (TaxRateVO) taxRateListIterator.next();
		    if (this.latestTaxRate == null || taxRateVO.getTaxDate().after(this.latestTaxRate.getTaxDate())) {
			this.latestTaxRate = taxRateVO;
		    }

		    add(taxRateVO);
		}
	    }
	}
    }

    public Collection getTaxRateList() throws RemoteException {
	return getItemList();
    }

    public TaxRateVO getTaxRate(DateTime taxDate) throws RemoteException {
	TaxRateVO taxRateVO = new TaxRateVO(taxDate, 0.0, 0.0);
	return (TaxRateVO) getItem(taxRateVO.getKey());
    }

    public TaxRateVO getCurrentTaxRate() throws RemoteException {
	initialiseList();
	return this.latestTaxRate;
    }
}