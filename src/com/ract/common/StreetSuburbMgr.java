package com.ract.common;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.Remote;

import com.ract.common.notifier.NotificationEvent;

/**
 * Remote interface to session bean
 * 
 * @author John Holliday
 * @version 1.0
 */
@Remote
public interface StreetSuburbMgr {
    public Collection getStreetSuburbsBySuburb(String suburb, String postcode) throws RemoteException;

    public Collection getStreetSuburbsByStreet(String street, String postcode) throws RemoteException;

    public Collection getStreetSuburbsByPostcode(String postcode) throws RemoteException;

    public StreetSuburbVO getUniqueStreetSuburb(String street, String suburb, String postcode) throws RemoteException;

    public Collection getStreetSuburbList(String street, String suburb) throws RemoteException;

    public void processNotificationEvent(NotificationEvent event) throws RemoteException;

    // public Hashtable compareStreetSuburbData() throws RemoteException;
}
