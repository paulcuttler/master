package com.ract.common;

import com.ract.client.ClientVO;

public class CardHelper {
    /**
     * The maximum length of a persons name that can be printed in the embossed
     * area of a card.
     */
    public final static int EMBOSSED_NAME_MAX_LENGTH = 28;

    /**
     * The maximum length of a persons name that can be recorded in the magnetic
     * strip track of a card.
     */
    public final static int TRACK_NAME_MAX_LENGTH = 26;

    /**
     * Return the persons name formatted for the magnetic track on the card
     * 
     * @param clientVO
     *            Description of the Parameter
     * @return The trackName value
     * @exception ValidationException
     *                Description of the Exception
     */
    public static String getTrackName(ClientVO clientVO) throws ValidationException {
	if (clientVO.getClientType().equals(ClientVO.CLIENT_TYPE_PERSON)) {
	    // PersonVO personVO = (PersonVO)clientVO;
	    StringBuffer trackName = new StringBuffer(clientVO.getSurname().trim().toUpperCase());
	    trackName.append("/");
	    trackName.append(clientVO.getGivenNames().trim().toUpperCase());
	    trackName.append(".");
	    trackName.append(clientVO.getTitle().trim().toUpperCase());
	    if (trackName.length() > TRACK_NAME_MAX_LENGTH) {
		return trackName.toString().substring(0, TRACK_NAME_MAX_LENGTH);
	    } else {
		return trackName.toString();
	    }
	} else {
	    throw new ValidationException("Failed to generate a track name. Clients of type " + clientVO.getClientType() + " are not supported.");
	}
    }

    /**
     * Return a card name with the default maximum length of 28 characters
     * 
     * @param clientVO
     *            Description of the Parameter
     * @param suffix
     *            Description of the Parameter
     * @return The cardName value
     * @exception ValidationException
     *                Description of the Exception
     */
    public static String getCardName(ClientVO clientVO, String suffix) throws ValidationException {
	return getCardName(clientVO, suffix, EMBOSSED_NAME_MAX_LENGTH);
    }

    /**
     * Generate a member name suitable for putting on the front of the
     * membership card. The name must not be more than 28 characters long. Five
     * different combinations are tried to achieve the name length restriction:
     * 1. Title, First name, Second initial, Surname, Name suffix 2. Title,
     * First initial, Second initial, Surname, Name suffix 3. Title, First
     * initial, Surname, Name suffix 4. First initial, Surname, Name suffix 5.
     * First initial, Surname
     * 
     * @param clientVO
     *            Description of the Parameter
     * @param nameSuffix
     *            Description of the Parameter
     * @param maxLength
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception ValidationException
     *                Description of the Exception
     */
    public static String getCardName(ClientVO clientVO, String nameSuffix, int maxLength) throws ValidationException {
	StringBuffer cardName = new StringBuffer();

	if (clientVO.getClientType().equals(ClientVO.CLIENT_TYPE_PERSON)) {
	    // PersonVO personVO = (PersonVO)clientVO;
	    if (nameSuffix == null) {
		nameSuffix = "";
	    } else {
		// pad one string in front
		nameSuffix = " " + nameSuffix;
	    }

	    String firstName = "";
	    String firstInitial = "";
	    String secondInitial = "";
	    String givenNames = clientVO.getGivenNames();
	    String title = clientVO.getTitle().trim();
	    String surname = clientVO.getSurname().trim();
	    if (givenNames != null) {
		givenNames = givenNames.trim();

		int pos = givenNames.indexOf(" ");
		if (pos != -1) {
		    firstName = givenNames.substring(0, pos);
		    secondInitial = givenNames.substring(pos + 1, pos + 2);
		    secondInitial = secondInitial.trim();
		    if (secondInitial.length() > 0) {
			secondInitial = " " + secondInitial;
		    }
		} else {
		    firstName = givenNames;
		}
		if (firstName.length() > 0) {
		    firstInitial = firstName.substring(0, 1);
		} else {
		    firstInitial = "";
		}
	    }
	    firstName = firstName.trim();

	    // total length must not exceed the maximum length
	    cardName.append(title);
	    cardName.append(" ");
	    cardName.append(firstName);
	    cardName.append(secondInitial);
	    cardName.append(" ");
	    cardName.append(surname);
	    cardName.append(nameSuffix);

	    if (cardName.length() > maxLength) {
		cardName.setLength(0);
		cardName.append(title);
		cardName.append(" ");
		cardName.append(firstInitial);
		cardName.append(secondInitial);
		cardName.append(" ");
		cardName.append(surname);
		cardName.append(nameSuffix);
	    }
	    if (cardName.length() > maxLength) {
		cardName.setLength(0);
		cardName.append(title);
		cardName.append(" ");
		cardName.append(firstInitial);
		cardName.append(" ");
		cardName.append(surname);
		cardName.append(nameSuffix);
	    }
	    if (cardName.length() > maxLength) {
		cardName.setLength(0);
		cardName.append(firstInitial);
		cardName.append(" ");
		cardName.append(surname);
		cardName.append(nameSuffix);
	    }
	    if (cardName.length() > maxLength) {
		cardName.setLength(0);
		cardName.append(firstInitial);
		cardName.append(" ");
		cardName.append(surname);
	    }
	    if (cardName.length() > maxLength) {
		throw new ValidationException("The card name '" + cardName + "' will be more than " + maxLength + " characters for client " + clientVO.getClientNumber());
	    }
	} else {
	    throw new ValidationException("Failed to generate a card name. Clients of type " + clientVO.getClientType() + " are not supported.");
	}
	return cardName.toString();
    }

    /**
     * format a credit card number
     * 
     * @todo replace with StringUtil method
     */
    public static String formatCardNumber(String cardNumber) {
	String formattedNumber = "";

	if (cardNumber != null) {
	    // card length
	    for (int i = 0; i < cardNumber.length(); i++) {
		// put a space in after every four chars.
		formattedNumber += cardNumber.charAt(i);

		if (i != 0 && ((i + 1) % 4 == 0) && (i + 1) != cardNumber.length()) {
		    formattedNumber += " ";

		}
	    }
	}
	return formattedNumber;
    }

}
