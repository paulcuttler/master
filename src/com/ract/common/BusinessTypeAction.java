package com.ract.common;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.ract.util.LogUtil;

public class BusinessTypeAction extends BaseAction implements ModelDriven, Preparable, ValidationAware {

    @Override
    public void validate() {

	if (model.getTypeCode() == null || model.getTypeCode().length() == 0) {
	    addFieldError("typeCode", "Business type code must be entered.");
	}

	if (model.getDescription() == null || model.getDescription().length() == 0) {
	    addFieldError("description", "Description must be entered.");
	}

    }

    public String getRequestId() {
	return requestId;
    }

    @Override
    public void prepare() throws Exception {
	LogUtil.log(this.getClass(), "requestId=" + (getRequestId() == null ? "null" : getRequestId().toString()));
	if (getRequestId() == null) {
	    model = new BusinessType();
	} else {
	    model = (BusinessType) commonMgr.get(BusinessType.class, getRequestId());
	}
    }

    public void setModel(BusinessType model) {
	this.model = model;
    }

    public void setRequestId(String requestId) {
	this.requestId = requestId;
    }

    private BusinessType model;

    private String requestId;

    public Object getModel() {
	return model;
    }

    @Override
    public String list() {
	list = commonMgr.getBusinessTypes();
	return LIST;
    }

}
