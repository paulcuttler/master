package com.ract.common;

import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Cache JNDI connections.
 * 
 * @author John Holliday
 * @created 3 September 2002
 * @version 1.0
 */

public class DataSourceFactory {

    private static Hashtable datasourceCache = new Hashtable();

    /**
     * Gets the connection attribute of the ConnectionFactory class
     * 
     * @param jndiDataSource
     *            Description of the Parameter
     * @return The connection value
     * @exception NamingException
     *                Description of the Exception
     */
    public static DataSource getDataSource(String jndiDataSource) throws NamingException {
	if (jndiDataSource == null) {
	    throw new NamingException("No JNDI data source name provided.");
	}

	// Try and get the data source from the cache first
	DataSource dataSource = (DataSource) datasourceCache.get(jndiDataSource);

	// Not in the cache so look it up.
	if (dataSource == null) {
	    dataSource = lookupDataSource(jndiDataSource);

	    // Still null so not valid.
	}
	if (dataSource == null) {
	    throw new NamingException("Unable to find JNDI Datasource " + jndiDataSource + ".");
	}

	return dataSource;
    }

    /**
     * Gets the dataSource attribute of the ConnectionFactory class
     * 
     * @param jndiDataSource
     *            Description of the Parameter
     * @return The dataSource value
     */
    private static DataSource lookupDataSource(String jndiDataSource) throws NamingException {
	InitialContext ic = new InitialContext();
	DataSource ds = (DataSource) ic.lookup(jndiDataSource);
	datasourceCache.put(jndiDataSource, ds);
	return ds;
    }
}
