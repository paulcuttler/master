package com.ract.common;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.integration.IntegrationServerSocket;
import com.ract.util.LogUtil;

/**
 * Title: Master Project Description: provides a mechanism by which any number
 * of socket server implemenation classes can be instantiated and managed in a
 * common manner. Copyright: Copyright (c) 2002 Company: RACT
 * 
 * @author T. Bakker
 * @version 1.0
 */

public class SocketServerCache extends CacheBase2 {
    private static SocketServerCache instance = new SocketServerCache();

    public SocketServerCache() {
    }

    public static SocketServerCache getInstance() {
	return instance;
    }

    /**
     * Create the required server sockets and start them up.
     */
    public void initialiseList() throws java.rmi.RemoteException {
	if (isEmpty()) {
	    LogUtil.info(this.getClass(), "Initialising socket servers.");
	    IntegrationServerSocket integrationServer = new IntegrationServerSocket();
	    add(integrationServer);
	    integrationServer.start();
	}
    }

    /**
     * Stop the specifed server socket.
     */
    public void stopSocketServer(String socketServerKey) throws RemoteException {
	ServerSocketManager server = (ServerSocketManager) getItem(socketServerKey);
	if (server != null) {
	    server.sendSignal(ServerSocketManager.SIGNAL_STOP);
	}
    }

    /**
     * Start the specifed server socket.
     */
    public void startSocketServer(String socketServerKey) throws RemoteException {
	ServerSocketManager server = (ServerSocketManager) getItem(socketServerKey);
	if (server != null) {
	    server.sendSignal(ServerSocketManager.SIGNAL_START);
	}
    }

    public void finalize() {
	try {
	    this.stopAll();
	} catch (RemoteException ex) {
	    LogUtil.fatal(this.getClass(), "Unable to stop servers." + ex.getMessage());
	}
    }

    /**
     * Reset the specifed server socket.
     */
    public void resetSocketServer(String socketServerKey) throws RemoteException {
	ServerSocketManager server = (ServerSocketManager) getItem(socketServerKey);
	if (server != null) {
	    server.sendSignal(ServerSocketManager.SIGNAL_RESET);
	}
    }

    /**
     * Start all of the server sockets
     */
    public void startAll() throws RemoteException {
	Collection itemList = getItemList();
	if (itemList != null) {
	    ServerSocketManager server;
	    Iterator itemIterator = itemList.iterator();
	    while (itemIterator.hasNext()) {
		server = (ServerSocketManager) itemIterator.next();
		server.sendSignal(ServerSocketManager.SIGNAL_START);
	    }
	}
    }

    /**
     * Stop all of the server sockets
     */
    public void stopAll() throws RemoteException {
	Collection itemList = getItemList();
	if (itemList != null) {
	    ServerSocketManager server;
	    Iterator itemIterator = itemList.iterator();
	    while (itemIterator.hasNext()) {
		server = (ServerSocketManager) itemIterator.next();
		server.sendSignal(ServerSocketManager.SIGNAL_STOP);
	    }
	}
    }

    /**
     * reset all of the server sockets
     */
    public void resetAll() throws RemoteException {
	Collection itemList = getItemList();
	if (itemList != null) {
	    ServerSocketManager server;
	    Iterator itemIterator = itemList.iterator();
	    while (itemIterator.hasNext()) {
		server = (ServerSocketManager) itemIterator.next();
		server.sendSignal(ServerSocketManager.SIGNAL_RESET);
	    }
	}
    }

    /**
     * Return the last exception raised by the specifed server socket.
     */
    public Exception getSocketServerException(String socketServerKey) throws RemoteException {
	Exception ex = null;
	ServerSocketManager server = (ServerSocketManager) getItem(socketServerKey);
	if (server != null) {
	    ex = server.getException();
	}
	return ex;
    }

    /**
     * Return the port number used by of the specifed server socket.
     */
    public int getSocketServerPort(String socketServerKey) throws RemoteException {
	int port = 0;
	ServerSocketManager server = (ServerSocketManager) getItem(socketServerKey);
	if (server != null) {
	    port = server.getPort();
	}
	return port;
    }

    /**
     * Return the port number used by of the specifed server socket.
     */
    public int getSocketServerThreadCount(String socketServerKey) throws RemoteException {
	int threadCount = 0;
	ServerSocketManager server = (ServerSocketManager) getItem(socketServerKey);
	if (server != null) {
	    threadCount = server.getClientSocketCount();
	}
	return threadCount;
    }

    /**
     * Return the state of the specifed server socket.
     */
    public String getSocketServerState(String socketServerKey) throws RemoteException {
	String state = "???";
	ServerSocketManager server = (ServerSocketManager) getItem(socketServerKey);
	if (server != null) {
	    state = server.getServerSocketState();
	}
	return state;
    }

    /**
     * Return a list (String) of server socket keys. The keys can be used to
     * query details of a particular server socket or to manage the state of a
     * particular server socket.
     */
    public ArrayList getSocketServerKeyList() throws RemoteException {
	ArrayList nameList = new ArrayList();
	Collection itemList = getItemList();
	if (itemList != null) {
	    CachedItem item;
	    Iterator itemIterator = itemList.iterator();
	    while (itemIterator.hasNext()) {
		item = (CachedItem) itemIterator.next();
		nameList.add(item.getKey());
	    }
	}
	return nameList;
    }
}
