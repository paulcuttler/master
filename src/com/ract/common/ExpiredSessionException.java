package com.ract.common;

/**
 * Exception to represent an expired session.
 * 
 * @author
 * @version 1.0
 */

public class ExpiredSessionException extends SystemException {
    private static final String DEFAULT_MESSAGE = "Your session has expired. Log in again and retry the operation.";

    public ExpiredSessionException() {
	super(DEFAULT_MESSAGE);
    }

    public ExpiredSessionException(String msg) {
	super(msg + " " + DEFAULT_MESSAGE);
    }

    public ExpiredSessionException(Throwable t) {
	super(DEFAULT_MESSAGE, t);
    }

    public ExpiredSessionException(String msg, Throwable t) {
	super(msg + " " + DEFAULT_MESSAGE, t);
    }

}
