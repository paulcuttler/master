package com.ract.common.schedule;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

public interface ScheduleMgr extends EJBObject {
    public void scheduleJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException, RemoteException;

    public void deleteJob(String jobName, String groupName) throws SchedulerException, RemoteException;

    public Scheduler getBoundScheduler() throws SchedulerException, RemoteException;

    public static String MODE_RUN_NOW = "runNow";

    public static String MODE_SCHEDULE_LATER = "scheduleLater";

    public boolean isStarted();

    public boolean isInStandbyMode();

    public boolean isPaused();

    public boolean isShutdown();

    public String[] getJobNames(String groupName) throws SchedulerException;

    public JobDetail getJobDetail(String jobName, String groupName) throws SchedulerException;

    public Trigger[] getTriggersOfJob(String jobName, String groupName) throws SchedulerException;

}
