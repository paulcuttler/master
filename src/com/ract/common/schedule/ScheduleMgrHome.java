package com.ract.common.schedule;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface ScheduleMgrHome extends EJBHome {
    public ScheduleMgr create() throws RemoteException, CreateException;
}