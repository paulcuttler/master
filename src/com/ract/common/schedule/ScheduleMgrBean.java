package com.ract.common.schedule;

import java.rmi.RemoteException;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import com.ract.util.LogUtil;

public class ScheduleMgrBean implements SessionBean {
    private SessionContext sessionContext;

    public void ejbCreate() {
    }

    public void ejbRemove() throws RemoteException {
    }

    public void ejbActivate() throws RemoteException {
    }

    public void ejbPassivate() throws RemoteException {
    }

    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
    }

    public boolean isStarted() {
	boolean started = false;
	try {
	    started = getScheduler().isStarted();
	} catch (SchedulerException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return started;
    }

    public boolean isInStandbyMode() {
	boolean standyByMode = false;
	try {
	    standyByMode = getScheduler().isInStandbyMode();
	} catch (SchedulerException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return standyByMode;
    }

    public boolean isPaused() {
	boolean paused = false;
	try {
	    paused = getScheduler().isPaused();
	} catch (SchedulerException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return paused;
    }

    public boolean isShutdown() {
	boolean shutdown = false;
	try {
	    shutdown = getScheduler().isShutdown();
	} catch (SchedulerException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return shutdown;
    }

    private Scheduler getScheduler() throws SchedulerException {
	InitialContext ctx = null;
	Scheduler sched = null;
	try {
	    ctx = new InitialContext();

	    sched = (Scheduler) ctx.lookup("Quartz");

	    // start the scheduler if is not already started
	    if (!sched.isStarted() || sched.isInStandbyMode()) {
		LogUtil.log(this.getClass(), "sched not started");
		sched.start();
	    }
	} catch (SchedulerException se) {
	    throw new SchedulerException("Unable to get the scheduler.", se);
	} catch (NamingException se) {
	    throw new SchedulerException("Unable to find the scheduler instance.", se);
	}
	return sched;
    }

    public void scheduleJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException {
	Scheduler scheduler = getScheduler();
	scheduler.scheduleJob(jobDetail, trigger);
    }

    public void deleteJob(String jobName, String groupName) throws SchedulerException {
	Scheduler scheduler = getScheduler();
	scheduler.deleteJob(jobName, groupName);
    }

    public String[] getJobNames(String groupName) throws SchedulerException {
	if (groupName == null) {
	    groupName = Scheduler.DEFAULT_GROUP;
	}
	Scheduler scheduler = getScheduler();
	return scheduler.getJobNames(groupName);
    }

    public JobDetail getJobDetail(String jobName, String groupName) throws SchedulerException {
	if (groupName == null) {
	    groupName = Scheduler.DEFAULT_GROUP;
	}
	Scheduler scheduler = getScheduler();
	return scheduler.getJobDetail(jobName, groupName);
    }

    public Trigger[] getTriggersOfJob(String jobName, String groupName) throws SchedulerException {
	if (groupName == null) {
	    groupName = Scheduler.DEFAULT_GROUP;
	}
	Scheduler scheduler = getScheduler();
	return scheduler.getTriggersOfJob(jobName, groupName);
    }

}
