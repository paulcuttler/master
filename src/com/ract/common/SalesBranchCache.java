package com.ract.common;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.util.LogUtil;

/**
 * Sales branch cache. Hardly ever changes.
 * 
 * @author jyh
 */

public class SalesBranchCache extends CacheBase {

    private static SalesBranchCache instance = new SalesBranchCache();

    public static SalesBranchCache getInstance() {
	return instance;
    }

    public void initialiseList() throws RemoteException {
	if (this.isEmpty()) {
	    LogUtil.log(this.getClass(), "Initialising cache.");

	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    Collection dataList = commonMgr.getSalesBranchList();
	    this.addAll(dataList);
	}
    }

    /**
     * get sales branch
     */
    public SalesBranchVO getSalesBranch(String salesBranchCode) throws RemoteException {
	return (SalesBranchVO) this.getItem(salesBranchCode);
    }

    /**
     * get list of sales branches
     */
    public ArrayList getSalesBranchList() throws RemoteException {
	return this.getItemList();
    }

}