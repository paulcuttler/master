package com.ract.common;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Value object containing the system parameter data.
 * 
 * @author Glen Wigg, jyh
 * @version 1.0
 */
@Entity
@Table(name = "[gn-table]")
public class SystemParameterVO extends ValueObject implements CachedItem, Comparable {

    public DateTime getDateValue() {
	return dateValue;
    }

    public void setDateValue(DateTime dateValue) {
	this.dateValue = dateValue;
    }

    public Boolean getBooleanValue() {
	return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
	this.booleanValue = booleanValue;
    }

    public BigDecimal getNumericValue1() {
	return numericValue1;
    }

    public void setNumericValue1(BigDecimal numericValue1) {
	this.numericValue1 = numericValue1;
    }

    public BigDecimal getNumericValue2() {
	return numericValue2;
    }

    public void setNumericValue2(BigDecimal numericValue2) {
	this.numericValue2 = numericValue2;
    }

    public static final String CATEGORY_MEMBERSHIP = "MEM";

    public static final String CATEGORY_LOGIN = "LOGIN";

    public static final String CATEGORY_PAYMENT = "PAYMNT";

    public static final String CATEGORY_CLIENT = "CLT";

    public static final String CATEGORY_COMMON = "COMMON";

    public static final String CATEGORY_CAD = "CAD";

    public static final String CATEGORY_TRAMADA = "TRAMADA";
    // Tramada parameters
    public static final String PROXY_USERNAME = "PROXY_USERNAME";
    public static final String PROXY_PASSWORD = "PROXY_PASSWORD";
    public static final String PROXY_AUTH_HOST = "PROXY_AUTH_HOST";
    public static final String PROXY_AUTH_DOMAIN = "PROXY_AUTH_DOMAIN";
    public static final String PROXY_HOST = "PROXY_HOST";
    public static final String PROXY_PORT = "PROXY_PORT";
    public static final String TRAMADA_URL = "URL";
    public static final String TRAMADA_XMLDIR = "XMLDIR";
    public static final String TRAMADA_EVENT = "EVENT";
    public static final String TRAMADA_ENABLE = "ENABLE";

    public static final String CATEGORY_ROADSIDE_EXTRACT = "RDSEXT";

    public static final String CATEGORY_ROADSIDE_ROLE_CREATE = "RDS_ROLE_CREATE";

    public static final String CATEGORY_ELECTRONIC_PAYMENT_DETAILS = "ELECTRONIC_PAYMENT_DETAILS";

    public static final String CATEGORY_MOTOR_NEWS = "MotorNews";

    /*
     * Keys that indicate which household keys should have the surname added on
     */
    public static final String MN_ADD_SURNAME_EQUAL = "addSurnameEqual";
    public static final String MN_ADD_SURNAME_LIKE = "addSurnameLike";

    public static final String CATEGORY_SUBSCRIPTIONS = "Subscriptions";

    public static final String CATEGORY_EMAIL_ADDRESS_MAINTENANCE = "EMAIL_ADDR_MAINT";

    // ******************** CAD EXPORT **************************
    /*
     * The number of days that the membership needs to be expired before is is
     * excluded from the full cad extract
     */
    public static final String CAD_EXTRACT_CUTOFF = "cadExtractCutoff";
    public static final String CAD_DELETE_FLAG = "cadDeleteFlag";

    public static final String CAD_CMO = "CMO";
    public static final String CAD_TOYOTA = "Toyota";
    public static final String CAD_TASGOV = "Tasgov";

    public static final String CAD_DAPFLEET_FILE_PREFIX = "FP_";
    public static final String CAD_FLEET_EXPIRY_CUTOFF = "EXPCUTOFF_";
    public static final String CAD_DAPFLEETLIST = "DAPFLEETS";

    public static final String CAD_FLEET_ABBREV = "MEMTYPE_FLEET";
    public static final String CAD_PERSONAL_ABBREV = "MEMTYPE_PERSONAL";
    public static final String CAD_MEMTYPE_ABBREV = "MEMTYPE_";

    public static final String CAD_DAPFLEET_MAKE = "MAKE_";
    public static final String CAD_DAPFLEET_JOINED = "JOINED_";
    public static final String CAD_DAPFLEET_SCHEME = "SCHEME_";

    public static final String CAD_MIDAS_EXPORT_FILES = "EXPORTFILE_";
    public static final String MIDAS_EXTRACTS = "MIDAS";

    public static final String CAD_MEMLEVEL_ABBREV = "MEMLEVEL_";

    // ************* Extract Directories **************

    public static final String CATEGORY_EXTRACT_DIRS = "EXR";
    // ************* Membership parameter names **************

    public static final String CMO_BASE_DIR = "CMOBASEDIR";

    public static final String DD_FOOTER_MESSAGE = "DDFOOTMESS";

    /**
     * Report server name
     */
    public static final String REPORT_SERVER_NAME = "REPSERV";

    /**
     * Report server port
     */
    public static final String REPORT_SERVER_PORT = "REPPORT";

    public static final String DATA_SERVER_NAME = "DATASERV";

    public static final String DATA_SERVER_PORT = "DATAPORT";

    /**
     * Subscriptions
     */
    public static final String SUBSCRIPTIONS_INPUT_FILE = "inputFile";

    /**
     * Email Address Update
     */
    public static final String EMAIL_ADDRESS_MAINTENANCE_INPUT_FILE = "inputFile";

    public static final String RENEWAL_INPUT_FILE = "RENEWALINPUTFILE";

    /**
     * JBoss RMI host and port.
     */
    public static final String RMI_HOST = "RMIHOST";

    public static final String RMI_PORT = "RMIPORT";

    /**
     * Maximum number of bytes to read on a socket before cutting the read
     * short.
     */
    public static final String MAX_SOCKET_BYTE_SIZE = "MAXSOCKBYTE";

    /**
     * Recipient of Motor New Extract.
     */
    public static final String EMAIL_MOTOR_NEWS = "MOTNEWSEMAIL";

    public static final String EMAIL_FINANCIAL_TRANSFER = "FINTRFEMAIL";

    public static final String LOYALTY_SCHEME_LAUNCH_DATE = "LOYSCHDTE";

    public static final String DD_CONFIRM_MESSAGE = "DDCONFMESS";

    public static final String DD_EARLIEST_DEBIT_PERIOD = "DDEARLDR";

    public static final String SMS_MESSAGE = "SMSMESSAGE";

    public static final String SMS_SUFFIX = "SMSSUFFIX";

    public static final String SMS_REPLY_TO_EMAIL = "SMSEMAIL";

    public static final String SMS_OVERRIDE = "SMSOVERRIDE";

    public static final String AU_SMS_OVERRIDE_EMAIL = "AUSMSOVERRIDE";

    public static final String AU_SMS_MESSAGE = "AUSMSMESSAGE";

    public static final String AU_SMS_REPLY_TO_EMAIL = "AUSMSEMAIL";

    public static final String ADDRESS_UPDATER_ENABLED = "ADDUPDON";

    public static final String ADDRESS_UPDATER_DISABLED_LIST = "No_Updater";

    public static final String UPDATE_HANDLER_ADDRESS = "Update_Emails_to";
    /**
     * Membership Renewals Due Extract.
     */
    public static final String EMAIL_MEMBERSHIP_RENEWALS_DUE = "MEMRENDUEEMAIL";

    public static final String MEMBERSHIP_RENEWALS_DUE_EXTRACT_DIR = "MEMRENDUEEXT";

    public static final String MEMBERSHIP_RENEWALS_DUE_SQL_DIR = "MEMRENDUESQL";

    /**
     * The people who will receive the log output from the renewal run
     */
    public static final String EMAIL_MEMBERSHIP_RENEWAL_RUN = "RENNOTEMAIL";

    public static final String MEMBERSHIP_RENEWAL_RUN_CCTO = "RENNOTCCTO";

    public static final String PROGRESS_HISTORY_LIMIT_YEARS = "PRGHSTLMTYRS";

    /*
     * Proxy Pool Parameters
     */

    public static final String CATEGORY_PROXY_POOL_RDS = "PROXYPOOLRDS";
    public static final String CATEGORY_PROXY_POOL_WEB = "PROXYPOOLWEB";
    public static final String CATEGORY_PROXY_POOL_INS = "PROXYPOOLINS";
    public static final String CATEGORY_PROXY_POOL_CVT = "PROXYPOOLCVT";

    public static final String PROXY_POOL_LIFO = "lifo";
    public static final String PROXY_POOL_MAX_ACTIVE = "maxActive";
    public static final String PROXY_POOL_MAX_IDLE = "maxIdle";
    public static final String PROXY_POOL_MAX_TOTAL = "maxTotal";
    public static final String PROXY_POOL_MAX_WAIT = "maxWait";
    public static final String PROXY_POOL_MIN_EVICTABLE_IDLE_TIME_MILLIS = "minEvictableIdleTimeMillis";
    public static final String PROXY_POOL_MIN_IDLE = "minIdle";
    public static final String PROXY_POOL_NUM_TESTS_PER_EVICTION_RUN = "numTestsPerEvictionRun";
    public static final String PROXY_POOL_TEST_ON_BORROW = "testOnBorrow";
    public static final String PROXY_POOL_TEST_ON_RETURN = "testOnReturn";
    public static final String PROXY_POOL_TEST_WHILE_IDLE = "testWhileIdle";
    public static final String PROXY_POOL_TIME_BETWEEN_EVICTION_RUNS_MILLIS = "timeBetweenEvictionRunsMillis";
    public static final String PROXY_POOL_WHEN_EXHAUSTED_ACTION = "whenExhaustedAction";

    /**
     * RoadSide Sales By Branch Extract.
     */
    public static final String EMAIL_ROADSIDE_SALES_BY_BRANCH = "RDS_SALES_BY_BRANCH_EMAIL";

    public static final String ROADSIDE_SALES_BY_BRANCH_EXTRACT_DIR = "RDS_SALES_BY_BRANCH_EXT";

    public static final String ROADSIDE_SALES_BY_BRANCH_SQL_DIR = "RDS_SALES_BY_BRANCH_SQL";

    /**
     * Indicates if a client must have a vehicle attached to them before a new
     * membership can be created. Values are "Y" and "N".
     */
    public static final String CLIENT_REQUIRES_VEHICLE = "CLNTREQVEH";

    /**
     * The default term of a membership.
     */
    public static final String DEFAULT_MEMBERSHIP_TERM = "DEFTERM";

    /**
     * This is the SYSTEM ADMIN e-mail address.
     */
    public static final String EMAIL_SYS_ADMIN = "SYSADMIN";

    /**
     * The default type of rejoin to do when doing a fast rejoin process.
     */
    public static final String DEFAULT_REJOIN_TYPE = "DEFREJTYPE";

    /**
     * The default rejoin reason to use when doing a fast rejoin process.
     */
    public static final String DEFAULT_REJOIN_REASON = "DEFREJREAS";

    /**
     * The maximum period that a the commence date of a membership can be
     * delayed by.
     */
    public static final String DELAYED_COMMENCE_PERIOD = "DELAYCOMMPER";

    /**
     * The directory where membership history files are stored.
     */
    public static final String DIRECTORY_MEMBERSHIP_HISTORY = "HISTDIR";

    /**
     * The directory where membership log files are stored.
     */
    public static final String MEM_LOG_DIR = "LOGDIR";

    /**
     * The directory where membership log files are stored.
     */
    public static final String MEM_REMOVE_LOGFILE = "REMLOGFILE";

    /**
     * Permissible profiles for CMO.
     */
    public static final String MEM_CMO_PROFILE = "CMOPROFILE";

    /**
     * The minimum period after a membership expires before a rejoin can be
     * done. If the membership expiry date is within this period it must be
     * renewed.
     */
    public static final String POST_EXPIRE_REJOIN_PERIOD = "POSTEXREJPER";

    /**
     * Post expiry renewal period. The maximum period after a membership has
     * expired that it can be renewed in. After this period it must be rejoined.
     */
    public static final String POST_EXPIRE_RENEWAL_PERIOD = "POSTEXRENPER";

    /**
     * Max number of expired renewal docs to cancel at a time.
     */
    public static final String POST_EXPIRE_RENEWAL_MAX_RESULTS = "POSTEXRENMAX";

    /**
     * Transfer expiry period. When transferring an affiliated membership it
     * cannot be transferred if it has been expired for more than this period.
     */
    public static final String TRANSFER_EXPIRED_PERIOD = "TRFEXPER";

    /**
     * Auto-cancel expiry period. The period after which an expired membership
     * will be auto-cancelled.
     */
    public static final String AUTOCANCEL_EXPIRY_PERIOD = "AUTOCEXPPER";

    public static final String AUTOCANCEL_TRANSACTION_THRESHOLD = "AUTOCLIMIT";

    /**
     * Transfer minimum period. When transferring an affiliated membership, if
     * it expires within this period an additional term of membership must be
     * taken.
     */
    public static final String TRANSFER_MINIMUM_PERIOD = "TRFMINPER";

    /**
     * The maximum hold period.
     */
    public static final String MAX_HOLD_PERIOD = "HOLDMAXPER";

    public static final String MAX_TRANSFER_RANGE = "MAXTFRRNG";

    /**
     * The minimum period for which a hold period credit applies.
     */
    // public static final String MIN_HOLD_PERIOD_CREDIT = "HOLDPERCRED";

    /**
     * Maximum period of expiry allowed for on hold
     */
    public static final String MAX_HOLD_EXPIRED = "HOLDEXPMAX";

    /**
     * Pending fee log file.
     */
    public static final String PENDING_FEE_REMOVE_LOG = "PFREMLOG";

    /**
     * The default period prior to expiry during which the membership can be
     * renewed.
     */
    public static final String IN_RENEWAL_PERIOD = "INRENPER";

    /**
     * The directory to which the renewal notice data files will be written.
     */
    public static final String DIRECTORY_RENEWAL_NOTICE = "RENNOTDIR";

    public static final String DIRECTORY_MEMBERSHIP_EXTRACTS = "MEMEXTDIR";
    /**
     * The directory for the motor news extracts.
     */
    public static final String DIRECTORY_MOTOR_NEWS_EXTRACTS = "MOTORNEWSEXTDIR";
    /**
     * The file name for the renewal notice data for memberships requiring new
     * membership cards.
     */
    public static final String FILE_WITH_CARD = "FILECARD";

    /**
     * Access extract file
     */
    public static final String MEM_ACCESS_EXTRACT_FILE = "MEMACCEXT";

    /**
     * Access audit file
     */
    public static final String MEM_ACCESS_AUDIT_FILE = "MEMACCAUD";

    /**
     * The file name for the renewal notice data for memberships not requiring
     * new membership cards.
     */
    public static final String FILE_WITHOUT_CARD = "FILENOCARD";

    public static final String FINANCE_TRANSFER_FILE = "FINTFRFILE";

    /**
     * The filename for card extracts
     */
    public static final String CARD_FILE_NAME = "CARDFILENAME";

    /**
     * The filename for roadside retentions
     */
    public static final String RETENTION_FILE_NAME = "RETFILENAME";

    /**
     * The default card expiry period. Format is an interval specification. i.e.
     * yy:mm:dd
     */
    public static final String DEFAULT_CARD_PERIOD = "DEFCARDPER";

    /**
     * The last time that the automatic renewal process was run. Format is
     * dd/dd/yyyy
     */
    public static final String LAST_AUTO_RENEW_RUN_DATE = "AUTORENDATE";

    /**
     * The email address to send audit and error files to.
     */
    public static final String EMAIL_AUTO_RENEWAL_RUN = "AUTORENEMAIL";

    public static final String EMAIL_DIRECT_DEBIT_AUDIT = "DDAUDEMAIL";

    /**
     * Allow debt clearance
     */
    public static final String MEM_CLEAR_OUTSTANDING_AMOUNT = "CLROSAMT";

    /**
     * The IIN number used to construct membership card numbers
     */
    public final static String MEM_CARD_IIN = "IIN";

    /**
     * The period that a membership quote stays current for.
     */
    public final static String MEM_QUOTE_CURRENT_PERIOD = "MEMQCPER";

    /**
     * The start of the period for reporting on year to date data. Format is
     * dd/MM. You add the current year to make a date.
     */
    public final static String MEM_REPORTING_PERIOD_START = "RPTPERSTART";

    /**
     * The marketing script to read out when entering the marketing field.
     */
    public final static String CLT_MARKETING_SCRIPT = "MKTSCRIPT";

    /**
     * Prefix (digit 1) used for bill pay.
     */
    public final static String MEM_BILLPAY_PREFIX = "BPPFX";

    /**
     * How long to hold the credit amount if the membership is cancelled
     */
    public final static String MEM_EXPIRE_CREDIT = "EXPIRECREDIT";

    /**
     * The effective date option to use when renewing memberships.
     */
    public final static String MEM_RENEWAL_EFFECTIVE_DATE_OPTION = "RENEFFDATE";

    public final static String MEM_TIER_BASED_CARDS = "MEMTBCARD";
    public final static String MEM_TIER_BASED_CARDS_ON_RENEWAL = "MEMTBCARDREN";

    public final static String MEM_FEE_EFFECTIVE_PERIOD = "MEMFEEEFFPER";

    /**
     * Valid values for renewal effective date option.
     */
    public final static String MEM_RENEWAL_EFFECTIVE_DATE_OPTION_TRANSACTION_DATE = "Transaction Date";

    public final static String MEM_RENEWAL_EFFECTIVE_DATE_OPTION_EXPIRY_DATE = "Expiry Date";

    /**
     * Electronic Payment Details
     */
    public final static String BPAY_USER = "BPAY_USER";
    public final static String BPAY_LOCATION = "BPAY_LOCATION";
    public final static String BPAY_TILL = "BPAY_TILL";
    public final static String BPAY_BRANCH = "BPAY_BRANCH";
    public final static String BPAY_PAYTYPE = "BPAY_PAYTYPE";
    public final static String BPAY_BILLER_CODE = "BPAY_BILLER_CODE";
    public final static String BPAY_AUDIT = "BPAY_AUDIT";

    public final static String IVR_USER = "IVR_USER";
    public final static String IVR_LOCATION = "IVR_LOCATION";
    public final static String IVR_TILL = "IVR_TILL";
    public final static String IVR_BRANCH = "IVR_BRANCH";
    public final static String IVR_PAYTYPE = "IVR_PAYTYPE";

    public final static String WEB_USER = "WEB_USER";
    public final static String WEB_LOCATION = "WEB_LOCATION";
    public final static String WEB_TILL = "WEB_TILL";
    public final static String WEB_BRANCH = "WEB_BRANCH";
    public final static String WEB_PAYTYPE = "WEB_PAYTYPE";

    // ************* Login parameter names **************

    /**
     * All user session invalidation
     */
    public static final String ALLOW_USER_SESSION_INVALIDATION = "INVLDATESESS";

    public static final String DEF_REJ_TYPE = "DEFREJTYPE";

    /**
     * The userID to use as a default.
     */
    public static final String DEFAULT_USER_ID = "DEFUSERID";

    // ************* Payment parameter names **************
    // public static final String PAY_APPSERVER_URL = "APPSERVURL";

    public static final String APPSERVER_URL = "APPSERVURL";

    /**
     * The name of the remove payable item log file All removals of payable
     * items will be logged to this file.
     */
    public static final String REMOVE_LOGFILE = "REMLOGFILE";

    /**
     * The directory that contains payment system log files.
     */
    public static final String LOGFILE_DIR = "PAYLOGDIR";

    /** elixir report cache **/
    public static final String DIRECTORY_REPORT_CACHE = "REPCHEDIR";

    /** is security enabled? **/
    public static final String SECURITY_ENABLED = "SECON";

    /** System type - ie. production, etc. **/
    public static final String TYPE_SYSTEM = "SYSTEM";

    /**
     * The receipting system to be used by the payment system. Valid values are
     * a SourceSystem abbreviation. e.g. OCR, RCTR
     */
    public static final String RECEIPTING_SYSTEM = "PAYRECSYS";

    public static final String FINANCE_INVOICE_DIRECTORY = "FININVDIR";

    public static final String FINANCE_RECEIPT_DIRECTORY = "FINRECDIR";
    
    public static final String FINANCE_JOURNAL_TRANSFER_AUDIT_LOC = "FINTRFLOC";

    /*
     * Receipting System to source system mapping
     */

    /**
     * Includes Aurora, Island State Credit Union and RACT insurance systems.
     * PAYRECSYSINS
     */
    public static final String PAY_RECEIPTING_SYSTEM_INS = RECEIPTING_SYSTEM + SourceSystem.INSURANCE.getAbbreviation();

    /**
     * PAYRECSYSMEM
     */
    public static final String PAY_RECEIPTING_SYSTEM_MEM = RECEIPTING_SYSTEM + SourceSystem.MEMBERSHIP.getAbbreviation();

    /**
     * PAYRECSYSFIN
     */
    public static final String PAY_RECEIPTING_SYSTEM_FIN = RECEIPTING_SYSTEM + SourceSystem.FIN.getAbbreviation();

    /**
     * PAYRECSYSNVI
     */
    public static final String PAY_RECEIPTING_SYSTEM_NVI = RECEIPTING_SYSTEM + SourceSystem.NVI.getAbbreviation();

    /**
     * PAYRECSYSFLEET
     */
    public static final String PAY_RECEIPTING_SYSTEM_FLEET = RECEIPTING_SYSTEM + SourceSystem.FLEET.getAbbreviation();

    /**
     * PAYRECSYSTRV
     */
    public static final String PAY_RECEIPTING_SYSTEM_TRV = RECEIPTING_SYSTEM + SourceSystem.TRAVEL.getAbbreviation();

    // control finance system
    public static final String FINANCE_SYSTEM = "FINSYS";

    public static final String FINANCE_SYSTEM_RACTI = FINANCE_SYSTEM + Company.RACTI.getDescription();

    public static final String FINANCE_SYSTEM_RACT = FINANCE_SYSTEM + Company.RACT.getDescription();

    // ************* Common parameter names **************

    public static final String APPSERVER_USERNAME = "APPSVRUSR";

    public static final String APPSERVER_PASSWORD = "APPSVRPWD";

    /** The directory where failed notification events are storted */
    public static final String DIRECTORY_NOTIFICATION_EVENT_FAILURE = "EVTFAILDIR";

    /** The url that the notification listener listens on. */
    public static final String NOTIFICATION_EVENT_LISTENER = "NotLstnr";

    /** The port number that the notification listener socket server listens on. */
    public static final String NOTIFICATION_LISTENER_SOCKET_PORT = "NotLstnrPort";

    /** The url that is used to locate the Elixir report server */
    public static final String ELIXIR_REPORT_SERVER_URL = "http_location";// key
									  // is
									  // not
									  // particularly
									  // descriptive.

    /*
     * Email Bounce Management parameters
     */
    public static final String BOUNCE_REASON_ILLEGAL_WHITESPACE = "Illegal whitespace";
    public static final String BOUNCE_REASON_MAILBOX_FULL = "Mailbox Full";

    /*
     * Email Advice parameters
     */
    public static final String EMAIL_ADVICE_RENEWAL = "ADHOCREN";
    public static final String EMAIL_ADVICE_DD = "DD";
    public static final String EMAIL_ADVICE_QUOTE = "QUOTE";
    public static final String EMAIL_ADVICE_GENERAL = "GENERAL";

    public static final String CACHE_TRANSACTIONS = "CACHETXNS";

    public static final Integer CREDIT_ACCOUNT = 2310;

    // Stored Query Reports
    public static final String PDF_REPORT_MAX_COLUMNS = "PDFREPORTMAXCOLS";

    public SystemParameterPK getSystemParameterPK() {
	return systemParameterPK;
    }

    public void setSystemParameterPK(SystemParameterPK systemParameterPK) {
	this.systemParameterPK = systemParameterPK;
    }

    @EmbeddedId
    private SystemParameterPK systemParameterPK;

    @Column(name = "[tab-desc]")
    private String value;

    @Column(name = "[tab-date]")
    private DateTime dateValue;

    @Column(name = "[tab-log]")
    private Boolean booleanValue;

    @Column(name = "[tab-dec0]")
    private BigDecimal numericValue1;

    @Column(name = "[tab-dec4]")
    private BigDecimal numericValue2;

    public SystemParameterVO() {
    }

    public SystemParameterVO(String cat, String param) {
	SystemParameterPK pk = new SystemParameterPK(cat, param);
	this.setSystemParameterPK(pk);
    }

    public SystemParameterVO(String cat, String param, String val) {
	SystemParameterPK pk = new SystemParameterPK(cat, param);
	this.setSystemParameterPK(pk);
	this.value = val;
    }

    public String getValue() {
	return this.value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    protected void validateMode(String newMode) throws ValidationException {
    }

    /********************** CachedItem interface methods ********************/

    public String getKey() {
	return getSystemParameterPK().getKey();
    }

    public String getCategory() {
	return getSystemParameterPK().getCategory();
    }

    public String getParameter() {
	return getSystemParameterPK().getParameter();
    }

    public boolean keyEquals(String key) {
	if (getKey().equals(key)) {
	    return true;
	} else {
	    return false;
	}
    }

    public int compareTo(Object object) {
	if (object != null && object instanceof SystemParameterVO) {
	    SystemParameterVO systemParameter = (SystemParameterVO) object;
	    int categoryComparison = this.getSystemParameterPK().getCategory().compareTo(systemParameter.getSystemParameterPK().getCategory());
	    int keyComparison = this.getKey().compareTo(systemParameter.getKey());
	    LogUtil.debug(this.getClass(), categoryComparison + " " + keyComparison);
	    LogUtil.debug(this.getClass(), getKey() + " " + systemParameter.getKey());
	    LogUtil.debug(this.getClass(), this.getSystemParameterPK().getCategory() + " " + systemParameter.getSystemParameterPK().getCategory());
	    if (categoryComparison != 0) {
		return categoryComparison;
	    } else // category is the same
	    {
		if (keyComparison != 0) {
		    return keyComparison; // category is the same but keys are
					  // different
		} else {
		    return 0; // keys are the same
		}

	    }

	} else {
	    return 0;
	}
    }

}
