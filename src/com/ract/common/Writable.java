package com.ract.common;

/**
 *
 * The interface that all classes that can be written out using
 * a class writer must implement.
 * @author       T. Bakker.
 * @version      1.0
 */

import java.rmi.RemoteException;

public interface Writable {
    public void write(ClassWriter cw) throws RemoteException;

    public String getWritableClassName();
}