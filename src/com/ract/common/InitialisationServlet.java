package com.ract.common;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import net.sf.ehcache.CacheManager;

import com.ract.membership.MembershipMgrBean;
import com.ract.util.LogUtil;

/**
 * <p>
 * Startup servlet that initialises caches and sequences.
 * </p>
 * <p>
 * There is a load-on-startup flag that has a priority attached to it.
 * </p>
 * 
 * @author jyh
 */
public class InitialisationServlet extends HttpServlet {

    /** Initialize global variables */
    public void init() throws ServletException {
	SimpleDateFormat sd = new SimpleDateFormat("HH:mm:ss");
	LogUtil.log("init", "Time is " + sd.format(new Date()));
	TimeZone tz = TimeZone.getDefault();
	LogUtil.log("init", "Time zone: " + tz.getDisplayName() + " " + tz.getID());

	LogUtil.log(this.getClass(), "Initialising application...");
	try {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    commonMgr.initialiseSocketServers();

	    String whichReceiptingSystem = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.RECEIPTING_SYSTEM);
	    LogUtil.log("init", "Receipting system is '" + whichReceiptingSystem + "'.");

	    CacheManager singletonManager = CacheManager.getInstance();
	    singletonManager.addCache(MembershipMgrBean.CACHE_GROUP_TRANSACTION_LIST);
	    singletonManager.addCache(MembershipMgrBean.CACHE_TRANSACTION_LIST);
	    singletonManager.addCache(MembershipMgrBean.CACHE_MEMBERSHIP_GROUP);

	} catch (RemoteException re) {
	    throw new ServletException("Unable to initialise application.", re);
	}
    }

    /** Clean up resources */
    public void destroy() {

	LogUtil.log(this.getClass(), "Destroying initialisation servlet.");
	// bring servers down.

	CacheManager singletonManager = CacheManager.getInstance();
	singletonManager.removalAll();

	ShutdownHook shutdownHook = new ShutdownHook();
	shutdownHook.start();
	// sync it to parent thread (could use run() method but it doesn't
	// really matter)
	try {
	    shutdownHook.join();
	} catch (InterruptedException ex) {
	    // ignore
	    LogUtil.log(this.getClass(), "joining shutdownhook" + ex);
	}
    }
}
