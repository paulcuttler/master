package com.ract.common.ui;

import com.ract.common.CommonConstants;

/**
 * 
 * Common user interface constants.
 * 
 * @author jyh
 */

public class CommonUIConstants {

    public final static String DIR_COMMON = CommonConstants.DIR_COMMON;

    public final static String DIR_COMMON_REPORTS = DIR_COMMON + "/reporting";
    public final static String DIR_HELP = CommonConstants.DIR_HELP;

    // pages
    public final static String PAGE_CommonAdminUIC = "/CommonAdminUIC";

    public final static String PAGE_SEARCH_HELP = DIR_HELP + "/SearchHelp.jsp";

    public final static String PAGE_EDIT_HELP = DIR_HELP + "/editHelp.jsp";

    public final static String PAGE_CREATE_HELP = DIR_HELP + "/createHelp.jsp";

    public final static String PAGE_VIEW_HELP = DIR_HELP + "/viewHelp.jsp";
    public final static String PAGE_VIEW_HELP_TEXT = DIR_HELP + "/viewHelpText.jsp";

    public final static String PAGE_ConfirmProcess = DIR_COMMON + "/ConfirmProcess.jsp";

    public final static String PAGE_ResetCaches = DIR_COMMON + "/ResetCaches.jsp";

    public static final String MAINTAIN_CODELIST = "AdminCodeList";

    public static final String PAGE_MAINTAIN_ADMIN_LIST = CommonConstants.DIR_COMMON + "/maintainAdminList.jsp";

    public final static String PAGE_ConfirmWindow = DIR_COMMON + "/ConfirmWindow.jsp";

    public final static String PAGE_gnText = DIR_COMMON + "/EditGnText.jsp";

    public final static String PAGE_viewGnText = DIR_COMMON + "/ViewGnText.jsp";

    public final static String PAGE_ViewGnTable = DIR_COMMON + "/ViewGnTable.jsp";

    public final static String PAGE_EditGnTable = DIR_COMMON + "/EditGnTable.jsp";

    public final static String PAGE_TreeList = DIR_COMMON + "/TreeList.jsp";

    public final static String PAGE_ReferenceDataView = DIR_COMMON + "/ReferenceDataView.jsp";

    public final static String PAGE_ReferenceDataEdit = DIR_COMMON + "/ReferenceDataEdit.jsp";

    public final static String PAGE_NOTIFICATIONFAILURELIST = DIR_COMMON + "/NotificationFailureList.jsp";

    public final static String PAGE_VIEWNOTIFICATIONFAILURE = DIR_COMMON + "/ViewNotificationFailure.jsp";

    public final static String PAGE_ManageSocketServers = DIR_COMMON + "/ManageSocketServers.jsp";

    public final static String PAGE_viewScheduledJobs = DIR_COMMON + "/ViewScheduledJobs.jsp";

    public final static String PAGE_QueryList = DIR_COMMON + "/QueryList.jsp";

    public final static String PAGE_EnterQueryParameters = DIR_COMMON + "/EnterQueryParameters.jsp";

    public final static String PAGE_SelectReportDeliveryMethodInclude = DIR_COMMON_REPORTS + "/SelectReportDeliveryMethodInclude.jsp";

    public final static String PAGE_ReportViewerApplet = DIR_COMMON_REPORTS + "/ReportViewerApplet.jsp";

    public final static String PAGE_NotificationListener = "/notificationlistener";

    public final static String PAGE_VIEW_SYSTEM_PARAMETER_LIST = DIR_COMMON + "/ViewSystemParameterList.jsp";

    public final static String PAGE_VIEW_PROGRESS_PROXY_POOL = DIR_COMMON + "/ViewProgressProxyPool.jsp";

}
