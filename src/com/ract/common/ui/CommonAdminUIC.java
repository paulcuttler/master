package com.ract.common.ui;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.quartz.Scheduler;

import com.progress.open4gl.RunTimeProperties;
import com.ract.common.ApplicationServlet;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.GnTextVO;
import com.ract.common.MailMgr;
import com.ract.common.ReferenceDataPK;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.admin.AdminCodeDescription;
import com.ract.common.cad.CadHelper;
import com.ract.common.mail.MailMessage;
import com.ract.common.notifier.NotificationRegister;
import com.ract.common.reporting.Query;
import com.ract.common.reporting.QueryParameter;
import com.ract.common.reporting.QueryParameterPK;
import com.ract.common.reporting.ReportMgr;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.help.HelpMgr;
import com.ract.help.HelpPage;
import com.ract.security.Privilege;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.TreeList;

public class CommonAdminUIC extends ApplicationServlet {
	private final static String CONTENT_TYPE = "text/html";

	public void init() throws ServletException {
	}

	public void destroy() {
	}

	public void handleEvent_ViewGnText(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String subSystem = request.getParameter("subSystem");
		String textType = request.getParameter("textType");
		String textName = request.getParameter("textName");
		request.setAttribute("subSystem", subSystem);
		request.setAttribute("textType", textType);
		request.setAttribute("textName", textName);
		String pageName = CommonUIConstants.PAGE_viewGnText;
		forwardRequest(request, response, pageName);
	}

	public void handleEvent_GnTextEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String subSystem = request.getParameter("subSystem");
		String textType = request.getParameter("textType");
		String textName = request.getParameter("textName");

		request.setAttribute("subSystem", subSystem);
		request.setAttribute("textType", textType);
		request.setAttribute("textName", textName);
		String pageName = CommonUIConstants.PAGE_gnText;
		forwardRequest(request, response, pageName);
	}

	public void handleEvent_notifyHelpAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String searchTerm = request.getParameter("searchTerm");
		final String SUBJECT_HELP = "No Help Found";
		try {
			UserSession userSession = getUserSession(request);
			String messageBody = "No help found for search term '" + searchTerm + "' by user " + userSession.getUser().getUsername() + ".";

			String emailAddress = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.EMAIL_SYS_ADMIN);

			MailMessage message = new MailMessage();
			message.setRecipient(emailAddress);
			message.setSubject(SUBJECT_HELP);
			message.setMessage(messageBody);

			MailMgr mailMgr = CommonEJBHelper.getMailMgr();
			mailMgr.sendMail(message);
		} catch (Exception ex) {
			LogUtil.warn(this.getClass(), ex.getMessage());
			// ignore
		}

		forwardRequest(request, response, CommonUIConstants.PAGE_SEARCH_HELP);
	}

	public void handleEvent_AddGnTextItem(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String pageName = CommonUIConstants.PAGE_gnText;
		forwardRequest(request, response, pageName);
	}

	/**
	 * Trace progress calls.
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public void handleEvent_traceProgressCalls(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException ex) {
			LogUtil.fatal(this.getClass(), ex);
		}

		out.write("Progress Runtime Parameters:");
		out.write("Wait if busy?" + RunTimeProperties.getWaitIfBusy());
		out.write("Tracing on?" + RunTimeProperties.isTracing());

		// hardcoded for now.
		String fileName = request.getParameter("fileName");
		out.write("File name: " + fileName);

		String mode = request.getParameter("mode");
		out.write("Mode: " + mode);

		// if a mode exists
		if (mode != null && !mode.equals("")) {
			if (mode.equals("Yes")) {
				String levelString = request.getParameter("level");
				int level = Integer.parseInt(levelString);
				RunTimeProperties.traceOn(fileName, level);
			} else if (mode.equals("No")) {
				RunTimeProperties.traceOff();
			} else if (mode.equals("setWaitIfBusy")) {
				RunTimeProperties.setWaitIfBusy();
			} else if (mode.equals("setNoWaitIfBusy")) {
				RunTimeProperties.setNoWaitIfBusy();
			}
		}
		// flush and close response
		out.flush();
		out.close();

	}

	public void handleEvent_GnTextSave(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String subSystem = request.getParameter("subSystem");
		String textType = request.getParameter("textType");
		String textName = request.getParameter("textName");
		String textData = request.getParameter("textData");
		String notes = request.getParameter("notes");
		GnTextVO textVO = new GnTextVO();
		textVO.setNotes(notes);
		textVO.setSubSystem(subSystem);
		textVO.setTextData(textData);
		textVO.setTextName(textName);
		textVO.setTextType(textType);
		try {
			CommonMgr gnText = CommonEJBHelper.getCommonMgr();
			gnText.setGnTextFromVO(textVO);
		} catch (RemoteException re) {
			throw new ServletException("Unable to get text data: " + re.getMessage() + " " + re);
		}
		handleEvent_MenuGnTextTree(request, response);
	}

	public void handleEvent_MenuGnTextTree(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		ArrayList textList = null;
		try {
			CommonMgr gnText = CommonEJBHelper.getCommonMgr();
			textList = (ArrayList) gnText.getGnTextList(null, null);
			// returns entire list
		} catch (RemoteException re) {
			throw new ServletException("Unable to get text data: " + re.getMessage() + " " + re);
		}
		TreeList tree = new TreeList("General Text Table", "/CommonAdminUIC", "AddGnTextItem");
		int id = 0;
		int root = 0;
		int parent = 0;
		String subSystem = "";
		String textType = "";
		GnTextVO textVO = null;
		String url = null;
		if (textList.size() > 0) {
			for (int x = 0; x < textList.size(); x++) {
				textVO = (GnTextVO) textList.get(x);
				if (!textVO.getSubSystem().equals(subSystem)) {
					// new root
					root = tree.addRoot(textVO.getSubSystem());
					subSystem = textVO.getSubSystem();
				}
				if (!textVO.getTextType().equals(textType) || !textVO.getSubSystem().equals(subSystem)) {
					// new branch
					parent = tree.addBranch(textVO.getTextType(), root);
					textType = textVO.getTextType();
				}
				url = CommonUIConstants.PAGE_CommonAdminUIC + "?event=ViewGnText" + "&subSystem=" + textVO.getSubSystem() + "&textType=" + textVO.getTextType() + "&textName=" + textVO.getTextName();
				tree.addLeaf(textVO.getTextName(), parent, url);
			}
		}
		String pageName = tree.deploy(request);
		forwardRequest(request, response, pageName);
	}

	public void handleEvent_viewScheduledJobs(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		this.forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_MenuGnTableTree(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		ArrayList tableList = null;

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		tableList = (ArrayList) commonMgr.getSystemParameterList();
		// returns entire list

		TreeList tree = new TreeList("General Table", "/CommonAdminUIC", "AddGnTableItem");
		int id = 0;
		int root = 0;
		int parent = 0;
		String tabName = "";
		SystemParameterVO tableVO = null;
		String url = null;
		if (tableList.size() > 0) {
			for (int x = 0; x < tableList.size(); x++) {
				tableVO = (SystemParameterVO) tableList.get(x);
				if (!tableVO.getSystemParameterPK().getCategory().equals(tabName)) {
					// new root
					root = tree.addRoot(tableVO.getSystemParameterPK().getCategory());
					tabName = tableVO.getSystemParameterPK().getCategory();
				}
				url = CommonUIConstants.PAGE_CommonAdminUIC + "?event=ViewGnTable" + "&tabName=" + tableVO.getSystemParameterPK().getCategory() + "&tabKey=" + tableVO.getSystemParameterPK().getParameter();
				tree.addLeaf(tableVO.getSystemParameterPK().getParameter(), root, url);
			}
		}
		String pageName = tree.deploy(request);
		forwardRequest(request, response, pageName);

	}

	public void handleEvent_ViewGnTable(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String tabName = request.getParameter("tabName");
		String tabKey = request.getParameter("tabKey");
		request.setAttribute("tabName", tabName);
		request.setAttribute("tabKey", tabKey);
		String page = CommonUIConstants.PAGE_ViewGnTable;
		forwardRequest(request, response, page);
	}

	public void handleEvent_GnTableEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String tabName = request.getParameter("tabName");
		String tabKey = request.getParameter("tabKey");
		request.setAttribute("tabName", tabName);
		request.setAttribute("tabKey", tabKey);
		String page = CommonUIConstants.PAGE_EditGnTable;
		forwardRequest(request, response, page);
	}

	public void handleEvent_listNotificationKeys(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		NotificationRegister.listIncompleteNotifications();
	}

	public void handleEvent_AddGnTableItem(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String page = CommonUIConstants.PAGE_EditGnTable;
		forwardRequest(request, response, page);
	}

	public void handleEvent_GnTableSave(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String tabName = request.getParameter("tabName");
		String tabKey = request.getParameter("tabKey");
		String description = request.getParameter("description");
		String dateStr = request.getParameter("dateStr");
		String logicalStr = request.getParameter("logicalStr");
		String intStr = request.getParameter("intStr");
		String decStr = request.getParameter("decStr");

		SystemParameterVO systemParameter = new SystemParameterVO(tabName, tabKey);

		systemParameter.setValue(description);
		try {
			if (!dateStr.equals("")) {
				systemParameter.setDateValue(new DateTime(dateStr));
			}
			if (logicalStr.equals("true")) {
				systemParameter.setBooleanValue(new Boolean(true));
			} else if (logicalStr.equals("false")) {
				systemParameter.setBooleanValue(new Boolean(false));
			}
			if (!intStr.equals("")) {
				systemParameter.setNumericValue1(new BigDecimal(intStr));
			}
			if (!decStr.equals("")) {
				systemParameter.setNumericValue2(new BigDecimal(decStr));
			}
		} catch (Exception e) {
			// error parsing something
			throw new ServletException("Error parsing values: " + e);
		}
		try {
			CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
			commonMgr.updateSystemParameter(systemParameter);
		} catch (RemoteException re) {
			throw new ServletException("Unable to get text data: " + re.getMessage() + " " + re);
		}
		handleEvent_MenuGnTableTree(request, response);
	}

	/**
	 * Display tree list of gn_reference entries Uses the first three letters of the type code as a base level
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	public void handleEvent_MenuReferenceDataTree(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		ArrayList tableList = null;

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		tableList = (ArrayList) commonMgr.getReferenceDataList();
		// returns entire list

		TreeList tree = new TreeList("Reference Data Table", CommonUIConstants.PAGE_CommonAdminUIC, "AddReferenceDataItem");
		int id = 0;
		int root = 0;
		int branch = 0;

		String tabName = "";
		ReferenceDataVO tableVO = null;
		String url = null;
		String rootName = "";
		String thisRootName = "";
		if (tableList.size() > 0) {
			for (int x = 0; x < tableList.size(); x++) {
				tableVO = (ReferenceDataVO) tableList.get(x);

				// MEMJOIN
				thisRootName = tableVO.getReferenceDataPK().getReferenceType();

				// ADD ROOT ONCE
				if (!rootName.equals(thisRootName)) {
					// new root
					root = tree.addRoot(thisRootName);
					rootName = thisRootName;
				}

				url = CommonUIConstants.PAGE_CommonAdminUIC + "?event=ReferenceDataEdit" + "&refType=" + tableVO.getReferenceDataPK().getReferenceType() + "&refCode=" + tableVO.getReferenceDataPK().getReferenceCode();
				tree.addLeaf(tableVO.getReferenceDataPK().getReferenceCode().trim(), root, url);
			}
		}
		String pageName = tree.deploy(request);
		forwardRequest(request, response, pageName);
	}

	private void setHelpSessionAttributes(HttpServletRequest request) throws ServletException {
		Collection helpList = null;
		try {
			HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
			helpList = helpMgr.getAllHelpPages();
		} catch (RemoteException re) {
			throw new ServletException("Unable to get help: " + re.getMessage() + " " + re);
		}

		SortedSet s = new TreeSet();

		if (helpList != null) {
			Iterator it = helpList.iterator();
			while (it.hasNext()) {
				HelpPage help = (HelpPage) it.next();
				s.add(new AdminCodeDescription(help.getHelpId().toString(), help.getHelpHeading()));
			}
		}
		request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);

	}

	public void handleEvent_MenuHelp(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

		setHelpSessionAttributes(request);
		//
		// Add somethings to the request;
		//

		request.getSession().setAttribute("pageTitle", "Help");
		request.getSession().setAttribute("buttonMessage", "Add a new help page.");
		request.getSession().setAttribute("createEvent", "showCreateHelp");
		request.getSession().setAttribute("selectEvent", "editHelp");
		request.getSession().setAttribute("codeName", "helpId");
		request.getSession().setAttribute("UICPage", CommonUIConstants.PAGE_CommonAdminUIC);
		request.getSession().setAttribute("origCode", "");

		request.setAttribute("Refresh", "No");
		forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);

	}

	public void handleEvent_ReferenceDataView(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String refType = request.getParameter("refType");
		String refCode = request.getParameter("refCode");
		request.setAttribute("refType", refType);
		request.setAttribute("refCode", refCode);

		forwardRequest(request, response, CommonUIConstants.PAGE_ReferenceDataView);
	}

	public void handleEvent_ReferenceDataAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		forwardRequest(request, response, CommonUIConstants.PAGE_ReferenceDataEdit);
	}

	public void handleEvent_AddReferenceDataItem(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		forwardRequest(request, response, CommonUIConstants.PAGE_ReferenceDataEdit);
	}

	public void handleEvent_ReferenceDataEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String refType = request.getParameter("refType");
		String refCode = request.getParameter("refCode");
		request.setAttribute("refType", refType);
		request.setAttribute("refCode", refCode);
		forwardRequest(request, response, CommonUIConstants.PAGE_ReferenceDataEdit);
	}

	public void handleEvent_saveHelp(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException {

		Integer helpId = new Integer(request.getParameter("helpId"));
		String helpCategory = request.getParameter("helpCategory");
		String helpHeading = request.getParameter("helpHeading");
		String helpText = request.getParameter("helpText");
		String helpPageReference = request.getParameter("helpPageReference");
		this.outputRequestParameters(request);

		if (helpId == null) {
			throw new ValidationException("Help Id cannot be empty");
		}
		if (helpHeading == null) {
			throw new ValidationException("Help heading cannot be empty");
		}

		HelpPage help = null;
		try {
			HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
			help = helpMgr.getHelpPage(helpId);
			LogUtil.log(this.getClass(), "help=" + help);
			help.setHelpCategory(helpCategory);
			help.setHelpHeading(helpHeading);
			help.setHelpText(helpText);
			help.setHelpPageReference(helpPageReference);
			LogUtil.log(this.getClass(), "help=" + help);
			helpMgr.updateHelp(help);
		} catch (RemoteException ex) {
			LogUtil.warn(this.getClass(), ex);
		}
		setHelpSessionAttributes(request);
		request.setAttribute("refresh", "Yes");
		request.setAttribute("help", help);
		forwardRequest(request, response, CommonUIConstants.PAGE_VIEW_HELP);
	}

	public void handleEvent_viewHelp(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		Integer helpId = new Integer(request.getParameter("helpId"));
		HelpPage help = null;
		try {
			HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
			help = helpMgr.getHelpPage(helpId);
		} catch (RemoteException ex) {
			LogUtil.warn(this.getClass(), ex);
		}
		request.setAttribute("help", help);
		forwardRequest(request, response, CommonUIConstants.PAGE_VIEW_HELP);
	}

	public void handleEvent_viewHelpText(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		Integer helpId = new Integer(request.getParameter("helpId"));
		HelpPage help = null;
		try {
			HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
			help = helpMgr.getHelpPage(helpId);
		} catch (RemoteException ex) {
			LogUtil.warn(this.getClass(), ex);
		}
		request.setAttribute("help", help);
		forwardRequest(request, response, CommonUIConstants.PAGE_VIEW_HELP_TEXT);
	}

	public void handleEvent_showCreateHelp(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		forwardRequest(request, response, CommonUIConstants.PAGE_CREATE_HELP);
	}

	public void handleEvent_createHelp(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException {

		String helpCategory = request.getParameter("helpCategory");
		String helpHeading = request.getParameter("helpHeading");
		if (helpHeading == null) {
			throw new ValidationException("Help heading cannot be empty");
		}
		String helpText = request.getParameter("helpText");
		String helpPageReference = request.getParameter("helpPageReference");

		HelpPage help = new HelpPage();
		try {
			help.setHelpCategory(helpCategory);
			help.setHelpHeading(helpHeading);
			help.setHelpText(helpText);
			help.setHelpPageReference(helpPageReference);
			HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
			help = helpMgr.createHelp(help);
		} catch (RemoteException ex) {
			LogUtil.warn(this.getClass(), ex);
		}
		setHelpSessionAttributes(request);
		request.setAttribute("refresh", "Yes");
		request.setAttribute("help", help);
		forwardRequest(request, response, CommonUIConstants.PAGE_VIEW_HELP);
	}

	public void handleEvent_editHelp(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		Integer helpId = new Integer(request.getParameter("helpId"));

		HelpPage help = null;
		try {
			HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
			help = helpMgr.getHelpPage(helpId);
		} catch (RemoteException ex) {
			LogUtil.warn(this.getClass(), ex);
		}

		request.setAttribute("help", help);
		forwardRequest(request, response, CommonUIConstants.PAGE_EDIT_HELP);
	}

	public void handleEvent_searchHelp(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String searchTerm = request.getParameter("searchTerm");
		try {
			HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
			ArrayList helpList = (ArrayList) helpMgr.searchHelpPages(searchTerm);
			request.setAttribute("searchResults", helpList);
			request.setAttribute("searchTerm", searchTerm);
		} catch (RemoteException ex) {
			throw new ServletException(ex);
		}
		forwardRequest(request, response, CommonUIConstants.PAGE_SEARCH_HELP);
	}

	public void handleEvent_ReferenceDataSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		String refType = request.getParameter("refType");
		String refCode = request.getParameter("refCode");
		String description = request.getParameter("description");
		String sortOrderStr = request.getParameter("sortOrder");
		int sortOrder = 0;
		if (sortOrderStr != null && !sortOrderStr.equals("")) {
			sortOrder = Integer.parseInt(sortOrderStr);
		}
		String refActiveStr = request.getParameter("refActive");
		boolean refActive = refActiveStr.equals("true");

		ReferenceDataVO refVO = new ReferenceDataVO();
		try {
			refVO.setDescription(description);
			refVO.setActive(refActive);
			refVO.setReferenceDataPK(new ReferenceDataPK(refCode, refType));
			refVO.setSortOrder(sortOrder);
			CommonMgr mgr = CommonEJBHelper.getCommonMgr();
			mgr.updateReferenceData(refVO);
		} catch (Exception e) {
			throw new ServletException(e.getMessage() + "\n" + e);
		}
		handleEvent_MenuReferenceDataTree(request, response);

	}

	public void handleEvent_MenuNotificationEvents(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_NOTIFICATION_ADMIN)) {
			forwardRequest(request, response, CommonUIConstants.PAGE_NOTIFICATIONFAILURELIST);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage notification events.");
		}
	}

	public void handleEvent_notificationMgr_btnResendAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_NOTIFICATION_ADMIN)) {
			CommonEJBHelper.getNotificationMgr().resendAllNotificationEventFailures();
			forwardRequest(request, response, CommonUIConstants.PAGE_NOTIFICATIONFAILURELIST);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage notification events.");
		}
	}

	public void handleEvent_notificationMgr_btnDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_NOTIFICATION_ADMIN)) {
			String fileName = request.getParameter("fileName");
			LogUtil.log(this.getClass(), "fileName=" + fileName);
			CommonEJBHelper.getNotificationMgr().deleteNotificationFailure(fileName);
			forwardRequest(request, response, CommonUIConstants.PAGE_NOTIFICATIONFAILURELIST);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage notification events.");
		}
	}

	public void handleEvent_notificationMgr_btnResend(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_NOTIFICATION_ADMIN)) {
			String fileName = request.getParameter("fileName");
			LogUtil.log(this.getClass(), "fileName=" + fileName);
			CommonEJBHelper.getNotificationMgr().resendNotificationFailure(fileName);
			forwardRequest(request, response, CommonUIConstants.PAGE_NOTIFICATIONFAILURELIST);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage notification events.");
		}
	}

	public void handleEvent_MenuSocketServerAdmin(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_SERVER_SOCKET_ADMIN)) {
			forwardRequest(request, response, CommonUIConstants.PAGE_ManageSocketServers);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage socket servers.");
		}
	}

	public void handleEvent_manageSocketServers_btnStop(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_SERVER_SOCKET_ADMIN)) {
			String socketServerKey = request.getParameter("socketServerKey");
			CommonEJBHelper.getCommonMgr().stopSocketServer(socketServerKey);
			forwardRequest(request, response, CommonUIConstants.PAGE_ManageSocketServers);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage socket servers.");
		}
	}

	public void handleEvent_manageSocketServers_btnReset(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_SERVER_SOCKET_ADMIN)) {
			String socketServerKey = request.getParameter("socketServerKey");
			CommonEJBHelper.getCommonMgr().resetSocketServer(socketServerKey);
			forwardRequest(request, response, CommonUIConstants.PAGE_ManageSocketServers);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage socket servers.");
		}
	}

	public void handleEvent_manageSocketServers_btnStart(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_SERVER_SOCKET_ADMIN)) {
			String socketServerKey = request.getParameter("socketServerKey");
			CommonEJBHelper.getCommonMgr().startSocketServer(socketServerKey);
			forwardRequest(request, response, CommonUIConstants.PAGE_ManageSocketServers);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage socket servers.");
		}
	}

	final String QUERY = "query";

	public void handleEvent_editQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		super.outputRequestParameters(request);

		String queryCode = request.getParameter("queryCode");
		Query query = null;
		try {
			ReportMgr rMgr = CommonEJBHelper.getReportMgr();
			query = rMgr.getQuery(queryCode);
		} catch (RemoteException re) {
			throw new ServletException(re);
		}
		request.getSession().setAttribute(QUERY, query);
		displayQueryPage(request, response);

	}

	public void handleEvent_createQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		super.outputRequestParameters(request);

		Query query = new Query();
		query.setCreateDate(new DateTime());

		request.getSession().setAttribute(QUERY, query);
		forwardRequest(request, response, PAGE_CREATE_QUERY);

	}

	public void handleEvent_addQueryParameter(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		super.outputRequestParameters(request);

		Query query = updateSessionQuery(request);

		String parameterName = request.getParameter("parameterName");
		String parameterLabel = request.getParameter("parameterLabel");
		String parameterDescription = request.getParameter("parameterDescription");
		String parameterType = request.getParameter("parameterType");

		int parameterSize = query.getQueryParameters() == null ? 0 : query.getQueryParameters().size();
		QueryParameter queryParameter = new QueryParameter();
		queryParameter.setQueryParameterPK(new QueryParameterPK(query.getQueryCode(), parameterName));
		queryParameter.setParameterType(parameterType);
		queryParameter.setParameterOrder(parameterSize + 1);
		queryParameter.setParameterDescription(parameterDescription);
		queryParameter.setParameterLabel(parameterLabel);
		LogUtil.debug(this.getClass(), "queryParameter=" + queryParameter);

		try {
			query.addQueryParameter(queryParameter);
		} catch (GenericException e) {
			throw new ServletException(e);
		}

		HttpSession session = request.getSession();

		session.setAttribute(QUERY, query);

		displayQueryPage(request, response);

	}

	private void displayQueryPage(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String formAction = request.getParameter("formAction");
		if (formAction == null || formAction.equals("editQuery")) {
			forwardRequest(request, response, PAGE_EDIT_QUERY);
		} else if (formAction.equals("createQuery")) {
			forwardRequest(request, response, PAGE_CREATE_QUERY);
		} else {
			throw new ServletException("No action defined.");
		}
	}

	public void handleEvent_removeQueryParameter(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		super.outputRequestParameters(request);

		String queryCode = request.getParameter("queryCode");
		String parameterName = request.getParameter("removeParameterName");

		Query query = updateSessionQuery(request);
		try {
			query.removeQueryParameter(parameterName);
		} catch (GenericException e) {
			throw new ServletException(e);
		}

		HttpSession session = request.getSession();
		session.setAttribute(QUERY, query);

		displayQueryPage(request, response);

	}

	public void handleEvent_moveParameterDown(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		super.outputRequestParameters(request);

		String queryCode = request.getParameter("queryCode");
		String parameterName = request.getParameter("selectedParameterName");

		Query query = updateSessionQuery(request);
		try {
			query.moveParameterDown(parameterName);
		} catch (GenericException e) {
			throw new ServletException(e);
		}

		HttpSession session = request.getSession();
		session.setAttribute(QUERY, query);

		displayQueryPage(request, response);
	}

	public void handleEvent_moveParameterUp(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		super.outputRequestParameters(request);

		String queryCode = request.getParameter("queryCode");
		String parameterName = request.getParameter("selectedParameterName");

		Query query = updateSessionQuery(request);
		try {
			query.moveParameterUp(parameterName);
		} catch (GenericException e) {
			throw new ServletException(e);
		}

		HttpSession session = request.getSession();
		session.setAttribute(QUERY, query);

		displayQueryPage(request, response);
	}

	final String PAGE_EDIT_QUERY = "/common/editQuery.jsp";
	final String PAGE_CREATE_QUERY = "/common/createQuery.jsp";

	public void handleEvent_saveQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		super.outputRequestParameters(request);
		super.outputSessionAttributes(request);

		String queryCode = request.getParameter("queryCode");
		if (queryCode == null || queryCode.equals("")) {
			throw new ValidationException("Query code may not be empty.");
		}

		Query query = updateSessionQuery(request);

		try {

			int parameterSize = query.getQueryParameters().size();

			LogUtil.log(this.getClass(), "count=" + query.getParameterHolderCount());
			if (query.getParameterHolderCount() != 0 && query.getParameterHolderCount() != parameterSize) {
				throw new ValidationException("Query placeholders do not equal the number of parameters.");
			}

			LogUtil.log(this.getClass(), "query=" + query);

			ReportMgr reportMgr = CommonEJBHelper.getReportMgr();
			reportMgr.updateQuery(query);
		} catch (RemoteException re) {
			throw new ServletException(re);
		}

		// clear session
		HttpSession session = request.getSession();
		session.removeAttribute(QUERY);

		handleEvent_viewQueries(request, response);

	}

	private Query updateSessionQuery(HttpServletRequest request) {

		String queryCode = request.getParameter("queryCode");
		String queryName = request.getParameter("queryName");
		String queryDescription = request.getParameter("queryDescription");
		String queryText = request.getParameter("queryText");
		String queryCategory = request.getParameter("queryCategory");
		String restricted = request.getParameter("timeRestricted");
		boolean timeRestricted = restricted == null ? false : restricted.equalsIgnoreCase("Yes");
		String queryPrivilege = request.getParameter("queryPrivilege");
		String dataSourceName = request.getParameter("dataSourceName");

		Query query = (Query) request.getSession().getAttribute(QUERY);
		LogUtil.debug(this.getClass(), "1 query=" + query);
		query.setQueryCode(queryCode);
		query.setQueryDescription(queryDescription);
		query.setQueryName(queryName);
		query.setQueryText(queryText);
		query.setTimeRestricted(new Boolean(timeRestricted));
		query.setQueryPrivilege(queryPrivilege);
		query.setQueryCategory(queryCategory);
		query.setDataSourceName(dataSourceName);
		LogUtil.debug(this.getClass(), "2 query=" + query);
		return query;
	}

	public void handleEvent_manageSocketServers_btnStartAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_SERVER_SOCKET_ADMIN)) {
			CommonEJBHelper.getCommonMgr().startAllSocketServers();
			forwardRequest(request, response, CommonUIConstants.PAGE_ManageSocketServers);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage socket servers.");
		}
	}

	public void handleEvent_manageSocketServers_btnStopAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_SERVER_SOCKET_ADMIN)) {
			CommonEJBHelper.getCommonMgr().stopAllSocketServers();
			forwardRequest(request, response, CommonUIConstants.PAGE_ManageSocketServers);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage socket servers.");
		}
	}

	public void handleEvent_manageSocketServers_btnResetAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException {
		UserSession userSession = getUserSession(request);
		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_SERVER_SOCKET_ADMIN)) {
			CommonEJBHelper.getCommonMgr().resetAllSocketServers();
			forwardRequest(request, response, CommonUIConstants.PAGE_ManageSocketServers);
		} else {
			throw new com.ract.common.SecurityException("You do not have the privilege to manage socket servers.");
		}
	}

	/**
	 * Enter query parameters
	 */
	public void handleEvent_enterQueryParameters(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String queryCode = request.getParameter("queryCode");
		Query query = null;
		try {
			ReportMgr rMgr = CommonEJBHelper.getReportMgr();
			query = rMgr.getQuery(queryCode);
		} catch (RemoteException re) {
			throw new ServletException(re);
		}
		request.setAttribute("query", query);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_EnterQueryParameters);
	}

	/**
	 * view the queries
	 */
	public void handleEvent_viewQueries(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		Collection queryList = null;
		try {
			ReportMgr rMgr = CommonEJBHelper.getReportMgr();
			queryList = rMgr.getQueryList();
		} catch (RemoteException re) {
			throw new ServletException(re);
		}
		request.setAttribute("queryList", queryList);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_QueryList);
	}

	/**
	 * View the Progress proxy pool.
	 */
	public void handleEvent_viewProgressProxyPool(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		this.forwardRequest(request, response, CommonUIConstants.PAGE_VIEW_PROGRESS_PROXY_POOL);
	}

	/**
	 * View the system parameters.
	 */
	public void handleEvent_viewSystemParameterList(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		this.forwardRequest(request, response, CommonUIConstants.PAGE_VIEW_SYSTEM_PARAMETER_LIST);
	}

	public void handleEvent_CancelScheduledJob(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String jobName = request.getParameter("jobName");
		ScheduleMgr sMgr = null;
		try {
			sMgr = CommonEJBHelper.getScheduleMgr();
			sMgr.deleteJob(jobName, Scheduler.DEFAULT_GROUP);
		} catch (Exception se) {
			LogUtil.log(this.getClass(), "Unable to cancel scheduled job: \n" + se);
			throw new ServletException("Unable to cancel scheduled job: \n" + se);
		}
		this.forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_scheduleCadMembershipRefreshJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String cronExpression = request.getParameter("cronExpression");
		LogUtil.log(this.getClass(), "cronExpression=" + cronExpression);

		CadHelper.scheduleCadMembershipRefreshJob(cronExpression);
		forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

}
