package com.ract.common;

/**
 * Title: Master Project Description: Thrown when a process detects that the
 * client is deceased and the process is not valid for a deceased client.
 * Copyright (c) 2002 Company: RACT
 * 
 * @author T. Bakker
 * @created 28 March 2003
 * @version 1.0
 */

public class DeceasedClientException extends ValidationException {
    private Integer clientNumber;

    public DeceasedClientException(Integer cNum) {
	super("The client is deceased.");
	this.clientNumber = cNum;
    }

    public DeceasedClientException(String message, Integer cNum) {
	super(message);
	this.clientNumber = cNum;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }
}
