package com.ract.common.test;

import java.rmi.RemoteException;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.ract.common.CommonEJBHelper;
import com.ract.util.LogUtil;

public class TestMgrBean implements SessionBean {
    SessionContext sessionContext;

    public void ejbCreate() {
    }

    public void ejbRemove() {
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void setSessionContext(SessionContext sessionContext) {
	this.sessionContext = sessionContext;
    }

    public void testRequiresNew(int x) {
	LogUtil.log(this.getClass(), "testRequiresNew start" + x);

	for (int i = 0; i < 100000; i++) {
	    System.out.println(i);
	}

	LogUtil.log(this.getClass(), "-->testRequiresNew end" + x);
    }

    public void testTransaction() {
	LogUtil.log(this.getClass(), "testTransaction start");
	TestMgr testMgr = CommonEJBHelper.getTestMgr();

	int max = 1000;
	for (int x = 0; x < max; x++) {
	    LogUtil.log(this.getClass(), "start x=" + x);
	    try {
		testMgr.testRequiresNew(x);
	    } catch (RemoteException ex1) {
		LogUtil.warn(this.getClass(), ex1);
	    }
	    LogUtil.log(this.getClass(), "start x=" + x);
	}
	LogUtil.log(this.getClass(), "testTransaction end");
    }

}
