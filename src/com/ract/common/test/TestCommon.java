package com.ract.common.test;

import junit.framework.TestCase;

import com.ract.common.CheckDigit;

/**
 * Test methods in the common packages
 * 
 * @author jyh
 * @version 1.0
 */

public class TestCommon extends TestCase {

    public void testCheckDigitGeneration() {
	String test[] = { "90006508387", "90006508381", "52189700033" };
	boolean expectedReturn = true;
	boolean actualReturn = false;
	for (int i = 0; i < test.length; i++) {
	    String cardNumber = test[i];
	    System.out.println(cardNumber);
	    String prefix = cardNumber.substring(0, cardNumber.length() - 1);
	    System.out.println(prefix);
	    Integer checkDigit = CheckDigit.getExternalCheckDigit(prefix); // incorrect
	    Integer oldCheckDigit = CheckDigit.getCheckDigit(prefix, false); // correct
	    System.out.println("checkDigit-" + checkDigit);
	    System.out.println("oldCheckDigit-" + oldCheckDigit);
	    String newFormat = prefix + checkDigit;
	    String oldFormat = prefix + oldCheckDigit;

	    if (!cardNumber.equals(newFormat) && !cardNumber.equals(oldFormat)) {
		// valid
		actualReturn = false;
	    } else {
		// not valid
		actualReturn = true;
	    }

	    assertEquals("return value for " + cardNumber, expectedReturn, actualReturn);
	}
    }

}
