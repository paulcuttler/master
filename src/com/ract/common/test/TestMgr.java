package com.ract.common.test;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface TestMgr extends EJBObject {
    public void testTransaction() throws RemoteException;

    public void testRequiresNew(int x) throws RemoteException;

}
