package com.ract.common;

import java.rmi.RemoteException;

/**
 * A class to tag an exception as one that reports a warning. The exception can
 * be handled differently than system errors or validation errors.
 * 
 * @author T. bakker
 * @version 1.0, 21 March 2002
 */

public class WarningException extends RemoteException {
    public WarningException() {
    }

    public WarningException(String msg) {
	super(msg);
    }

    public WarningException(Throwable t) {
	super("", t);
    }

    public WarningException(String msg, Throwable t) {
	super(msg, t);
    }
}