package com.ract.common.reporting;

//import com.elixirtech.report.server.command.*;
import com.elixirtech.ers2.client.Shutdown;
import com.ract.common.ExceptionHelper;

/**
 * Shutdown or reload the workspace in the elixir report server.
 * 
 * @author jyh
 * @version 1.0
 */

public class ShutdownElixir {
    public static void main(String[] args) {
	String action = null;
	try {
	    if (args.length != 5) {
		System.err.println("Usage: ShutdownElixir localhost 7123 Administrator admin shutdown");
		return;
	    }
	    // action
	    action = args[4];

	    // ICommand invoke = new Command();
	    Shutdown sd = new Shutdown();
	    // host port

	    if (action.equals("shutdown")) {

		sd.shutDownCommand(args[0], Integer.parseInt(args[1]), args[2], args[3], "now");

	    }
	    // else if (action.equals("reload"))
	    // {
	    // invoke.reloadWorkspaces();
	    // }
	    else {
		throw new Exception("Action '" + action + "' is not known.");
	    }
	} catch (Exception ex) {
	    System.err.println("Error processing action '" + action + "': " + ExceptionHelper.getExceptionStackTrace(ex));
	    return;
	}
    }
}
