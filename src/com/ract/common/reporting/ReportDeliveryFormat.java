package com.ract.common.reporting;

//import com.elixirtech.msg.*;
import java.io.Serializable;

/**
 * Title: ReportDeliveryFormat Description: Defines the interface and common
 * services of a report delivery format. Specific report delivery formats define
 * the parameter to use when setting up Elixir report requests to generate the
 * report in a suitable format. They also define the extension to use on files
 * delivered in the format and the MIME type to use when returning the report to
 * a browser. Copyright: Copyright (c) 2003 Company: RACT
 * 
 * @author T. Bakker
 * @version 1.0
 */

public class ReportDeliveryFormat implements Serializable {
    private String deliveryFormatName;
    private String fileNameExtension;
    private String MIMEType;

    public ReportDeliveryFormat() {
    }

    public String getDeliveryFormatName() {
	return MIMEType;

    }

    public String getFilenameExtension() {
	return fileNameExtension;
    }

    // public abstract MsgType getReportRequestOutputFormat();

    public String getMIMEType() {
	return MIMEType;
    }

    public String toString() {
	return getDeliveryFormatName();
    }

    public static ReportDeliveryFormat getReportDeliveryFormat(String formatName) {
	ReportDeliveryFormat format = new ReportDeliveryFormat();
	format.deliveryFormatName = formatName;

	if (ReportRequestBase.REPORT_DELIVERY_FORMAT_HTML.equals(formatName)) {
	    format.fileNameExtension = ".html";
	    format.MIMEType = ReportRequestBase.MIME_TYPE_HTML;
	} else if (ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF.equals(formatName)) {
	    format.fileNameExtension = ".pdf";
	    format.MIMEType = ReportRequestBase.MIME_TYPE_PDF;
	} else if (ReportRequestBase.REPORT_DELIVERY_FORMAT_XML.equals(formatName)) {
	    format.fileNameExtension = ".xml";
	    format.MIMEType = ReportRequestBase.MIME_TYPE_XML;
	} else if (ReportRequestBase.REPORT_DELIVERY_FORMAT_CSV.equals(formatName)) {
	    format.fileNameExtension = ".csv";
	    format.MIMEType = ReportRequestBase.MIME_TYPE_CSV;
	}
	return format;
    }

}
