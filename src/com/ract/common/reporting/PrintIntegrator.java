package com.ract.common.reporting;

import java.io.Serializable;

import org.w3c.dom.Node;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemException;
import com.ract.common.integration.Integrator;
import com.ract.common.integration.ReportIntegratorNotificationEvent;
import com.ract.common.notifier.NotificationMgr;

/**
 * Handles print requests passed as XML via the integration server the XML
 * includes <ClassName>com.ract.common.reporting.PrintIntegrator</ClassName>
 * 
 * @author dgk
 * @version 1.0
 */

public class PrintIntegrator implements Integrator, Serializable {

    public void processXML(String dataType, String action, Node dataNode) throws SystemException {
	NotificationMgr notificationMgr = CommonEJBHelper.getNotificationMgr();

	ReportIntegratorNotificationEvent reportIntegratorNotificationEvent = new ReportIntegratorNotificationEvent(dataType, action, dataNode);
	try {
	    notificationMgr.notifyEvent(reportIntegratorNotificationEvent);
	    notificationMgr.commit();
	} catch (Exception ex1) {
	    throw new SystemException(ex1);
	}

    }

    // no response to propagate to the client.
    public String getResponse() {
	return null;
    }

}
