package com.ract.common.reporting;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * Maintain a list of reports that have been requested to allow the report
 * server to retrieve them.
 * 
 * @author rlg, jyh
 * @version 1.0
 */
public class ReportRegister {
    private static Hashtable reportTable = new Hashtable();

    /**
     * Can't be constructed
     */
    private ReportRegister() {
    }

    /**
     * Changed to synchronized to prevent possible duplicate reports
     */
    public synchronized static String addReport(ReportRequestBase reportRequest) // throws
										 // Exception
    {
	listReports("addReport 1");
	String reportName = null;
	if (reportRequest != null) {
	    reportName = reportRequest.getReportName() + "_" + new Long(new Date().getTime()).toString();
	    // replace any spaces
	    LogUtil.debug(com.ract.common.reporting.ReportRegister.class, "reportName=" + reportName);
	    reportName = StringUtil.replaceAll(reportName, " ", "");
	    LogUtil.debug(com.ract.common.reporting.ReportRegister.class, "reportName=" + reportName);
	    reportTable.put(reportName, reportRequest);
	}
	LogUtil.debug(com.ract.common.reporting.ReportRegister.class, "reportName=" + reportName);
	listReports("add Report 2");
	return reportName;
    }

    public static void removeReport(String reportName) {
	LogUtil.debug(com.ract.common.reporting.ReportRegister.class, "removeReport " + reportName);
	reportTable.remove(reportName);
    }

    public static void listReports(String context) {
	LogUtil.debug(com.ract.common.reporting.ReportRegister.class, "\n******** listReports " + context + " **********");
	Enumeration en = ReportRegister.reportTable.keys();
	int rowCount = 0;
	while (en.hasMoreElements()) {
	    rowCount++;
	    Object key = en.nextElement();
	    Object value = reportTable.get(key);
	    LogUtil.debug(com.ract.common.reporting.ReportRegister.class, rowCount + ": " + key.toString() + " - " + value.toString());
	}
	LogUtil.debug("ReportRegister", "\n *********** end listReports " + context + " (" + rowCount + " listed) **********");
    }

    public static ReportRequestBase getReport(String reportName) {
	listReports("getReport");
	ReportRequestBase reportRequestBase = null;
	if (reportName != null) {
	    reportRequestBase = (ReportRequestBase) reportTable.get(reportName);
	}
	return reportRequestBase;
    }

    public static int getSize() {
	return reportTable.size();
    }
}
