package com.ract.common.reporting;

import java.io.Serializable;

public class QueryParameterPK implements Serializable {
    private String queryCode;

    private String parameterName;

    /**
     * @hibernate.key-property column="parameter_name"
     * @param parameterName
     *            String
     */
    public String getParameterName() {
	return parameterName;
    }

    public void setQueryCode(String queryCode) {
	this.queryCode = queryCode;
    }

    public void setParameterName(String parameterName) {
	this.parameterName = parameterName;
    }

    /**
     * @hibernate.key-property column="query_code"
     * @return String
     */
    public String getQueryCode() {
	return queryCode;
    }

    public QueryParameterPK(String queryCode, String parameterName) {
	this.setQueryCode(queryCode);
	this.setParameterName(parameterName);
    }

    private QueryParameterPK() {
    }

    /**
     * ABSOLUTELY MANDATORY METHODS FOR COMPOSITE KEYS OR RESULTS IN NPE
     */

    public boolean equals(Object o) {
	boolean equal = false;
	if (o instanceof QueryParameterPK) {
	    QueryParameterPK qp = (QueryParameterPK) o;
	    if (qp.getQueryCode().equals(this.getQueryCode()) && qp.getParameterName().equals(this.getParameterName())) {
		equal = true;
	    }
	}
	return equal;
    }

    public int hashCode() {
	return super.hashCode();
    }

    @Override
    public String toString() {
	return "QueryParameterPK [parameterName=" + parameterName + ", queryCode=" + queryCode + "]";
    }

}
