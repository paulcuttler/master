package com.ract.common.reporting;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.ract.common.CommonEJBHelper;
import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Carries query information.
 * 
 * @hibernate.class table="query" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */

public class Query implements Serializable, Comparable {

    public final static String PARAMETER_HOLDER = "?";

    public Query() {
    }

    public Query(String queryCode, String queryName, String queryText, String queryDescription, Boolean timeRestricted) {
	this.setQueryCode(queryCode);
	this.setQueryName(queryName);
	this.setQueryText(queryText);
	this.setQueryDescription(queryDescription);
	this.setTimeRestricted(timeRestricted);
	this.queryParameters = new ArrayList();
    }

    public void setQueryCode(String queryCode) {
	this.queryCode = queryCode;
    }

    public int getParameterHolderCount() {
	int count = 0;
	if (this.queryText != null) {
	    for (int i = 0; i < this.queryText.length(); i++) {
		if (this.queryText.substring(i, i + 1).equals(PARAMETER_HOLDER)) {
		    count++;
		}
	    }
	}
	return count;
    }

    final String MODE_UP = "up";
    final String MODE_DOWN = "down";

    public void moveParameterDown(String parameterName) throws GenericException {
	moveParameter(parameterName, MODE_DOWN);
    }

    private void moveParameter(String parameterName, String mode) throws GenericException {
	// natural sort order

	Collection queryParameters = getQueryParameters();
	if (queryParameters != null && !queryParameters.isEmpty()) {
	    LogUtil.debug(this.getClass(), "moveParameter " + mode);
	    QueryParameter qParameter;
	    QueryParameter qParameter2;
	    List queryParameterList = (List) queryParameters;
	    int maxSize = queryParameterList.size();
	    for (int i = 0; i < queryParameterList.size(); i++) {
		qParameter = (QueryParameter) queryParameterList.get(i);
		LogUtil.debug(this.getClass(), "qParameter.getQueryParameterPK().getParameterName()=" + qParameter.getQueryParameterPK().getParameterName());
		LogUtil.debug(this.getClass(), "parameterName=" + parameterName);
		LogUtil.debug(this.getClass(), "i=" + i);
		if (qParameter.getQueryParameterPK().getParameterName().equals(parameterName)) {
		    if (mode.equals(MODE_DOWN)) {
			LogUtil.debug(this.getClass(), "down " + i);
			if (i != (maxSize - 1)) {
			    // increase this parameters position
			    qParameter.setParameterOrder(qParameter.getParameterOrder() + 1);
			    // get next parameter
			    qParameter2 = (QueryParameter) queryParameterList.get(i + 1);
			    // decrease its position
			    qParameter2.setParameterOrder(qParameter.getParameterOrder() - 1);
			    LogUtil.debug(this.getClass(), "queryParameterList " + queryParameterList);

			}
		    } else if (mode.equals(MODE_UP)) {
			LogUtil.debug(this.getClass(), "up " + i);
			if (i != 0) {
			    // decrease this parameters position
			    qParameter.setParameterOrder(qParameter.getParameterOrder() - 1);
			    // get previous parameter
			    qParameter2 = (QueryParameter) queryParameterList.get(i - 1);
			    // increase its position
			    qParameter2.setParameterOrder(qParameter.getParameterOrder() + 1);
			    LogUtil.debug(this.getClass(), "queryParameterList " + queryParameterList);

			}
		    }
		}
	    }
	}
    }

    private DateTime createDate;

    /**
     * @hibernate.property
     * @hibernate.column name="create_date"
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public void moveParameterUp(String parameterName) throws GenericException {
	moveParameter(parameterName, MODE_UP);
    }

    /**
     * @hibernate.id column="query_code" generator-class="assigned"
     */
    public String getQueryCode() {
	return queryCode;
    }

    public int compareTo(Object object) {
	if (object != null && object instanceof Query) {
	    Query query = (Query) object;
	    int categoryComparison = this.getQueryCategory() != null ? this.getQueryCategory().compareTo(query.getQueryCategory()) : 0;
	    int nameComparison = this.getQueryName() != null ? this.getQueryName().compareTo(query.getQueryName()) : 0;
	    LogUtil.debug(this.getClass(), categoryComparison + " " + nameComparison);
	    if (categoryComparison != 0) {
		return categoryComparison;
	    } else // category is the same
	    {
		if (nameComparison != 0) {
		    return nameComparison; // category is the same but keys are
					   // different
		} else {
		    return 0; // keys are the same
		}
	    }
	} else {
	    return 0;
	}
    }

    public void setQueryText(String queryText) {
	LogUtil.log(this.getClass(), "queryText size=" + (queryText != null ? queryText.length() : 0));
	this.queryText = queryText;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="query_text"
     */
    public String getQueryText() {
	return queryText;
    }

    public void setQueryName(String queryName) {
	this.queryName = queryName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="query_name"
     */
    public String getQueryName() {
	return queryName;
    }

    public void setQueryParameters(Collection queryParameters) {
	this.queryParameters = queryParameters;
    }

    public void addQueryParameter(QueryParameter queryParameter) throws GenericException {
	QueryParameter qParameter;
	if (this.queryParameters == null) {
	    this.queryParameters = new ArrayList();
	} else {
	    LogUtil.log(this.getClass(), "queryParameter=" + queryParameter);
	    for (Iterator i = queryParameters.iterator(); i.hasNext();) {
		qParameter = (QueryParameter) i.next();
		LogUtil.log(this.getClass(), "qParameter=" + qParameter);
		LogUtil.log(this.getClass(), "qParameter.getQueryParameterPK().getParameterName()=" + qParameter.getQueryParameterPK().getParameterName());
		LogUtil.log(this.getClass(), "queryParameter.getQueryParameterPK().getParameterName()=" + queryParameter.getQueryParameterPK().getParameterName());
		LogUtil.log(this.getClass(), "test=" + qParameter.equals(queryParameter));
		if (qParameter.equals(queryParameter) || qParameter.getQueryParameterPK().getParameterName().equals(queryParameter.getQueryParameterPK().getParameterName())) {
		    throw new GenericException("Parameter already exists.");
		}
	    }
	}
	this.queryParameters.add(queryParameter);
    }

    public void removeQueryParameter(String parameterName) throws GenericException {
	Collection queryParameters = getQueryParameters();
	if (queryParameters != null && !queryParameters.isEmpty()) {
	    QueryParameter qParameter;
	    for (Iterator i = queryParameters.iterator(); i.hasNext();) {
		qParameter = (QueryParameter) i.next();
		if (qParameter.getQueryParameterPK().getParameterName().equals(parameterName)) {
		    i.remove();
		}
	    }
	}
    }

    public Collection getQueryParameters() {
	LogUtil.log(this.getClass(), "queryParameters=" + queryParameters);
	LogUtil.log(this.getClass(), "queryCode=" + queryCode);
	if (this.queryParameters == null && this.queryCode != null) {
	    try {
		ReportMgr rMgr = CommonEJBHelper.getReportMgr();
		this.queryParameters = rMgr.getQueryParameterList(this.queryCode);
	    } catch (RemoteException e) {
		LogUtil.warn(this.getClass(), e);
	    }
	    LogUtil.log(this.getClass(), "queryParameters=" + queryParameters);
	}
	return this.queryParameters;
    }

    public void setQueryDescription(String queryDescription) {
	this.queryDescription = queryDescription;
    }

    public void setQueryCategory(String queryCategory) {
	this.queryCategory = queryCategory;
    }

    @Override
    public String toString() {
	return "Query [MODE_DOWN=" + MODE_DOWN + ", MODE_UP=" + MODE_UP + ", createDate=" + createDate + ", dataSourceName=" + dataSourceName + ", queryCategory=" + queryCategory + ", queryCode=" + queryCode + ", queryDescription=" + queryDescription + ", queryName=" + queryName + ", queryParameters=" + queryParameters + ", queryPrivilege=" + queryPrivilege + ", queryText=" + queryText + ", timeRestricted=" + timeRestricted + "]";
    }

    public void setQueryPrivilege(String queryPrivilege) {
	this.queryPrivilege = queryPrivilege;
    }

    public void setTimeRestricted(Boolean timeRestricted) {
	LogUtil.log(this.getClass(), "timeRestricted=" + timeRestricted);
	this.timeRestricted = timeRestricted;
    }

    // TODO system parameters
    final public static String SOB = "08:30";
    final public static String COB = "18:00";

    public boolean canRun(DateTime now) {
	boolean run = true;
	if (getTimeRestricted().booleanValue()) {

	    if (now == null) {
		now = new DateTime();
	    }
	    LogUtil.debug(this.getClass(), "now=" + now.formatLongDate());
	    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    String date = sd.format(now);
	    LogUtil.debug(this.getClass(), "date=" + date);
	    try {
		DateTime start = new DateTime(sd.parse(date.substring(0, 10) + " " + SOB));
		DateTime end = new DateTime(sd.parse(date.substring(0, 10) + " " + COB));
		LogUtil.debug(this.getClass(), "start=" + start.formatLongDate());
		LogUtil.debug(this.getClass(), "end=" + end.formatLongDate());
		LogUtil.debug(this.getClass(), "now.compareTo(start)=" + now.compareTo(start));
		LogUtil.debug(this.getClass(), "now.compareTo(end)=" + now.compareTo(end));
		// within business hours
		if (now.compareTo(start) > 0 || now.compareTo(end) < 0) {
		    run = false;
		}
		LogUtil.debug(this.getClass(), "run=" + run);
	    } catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	}
	return run;
    }

    public void setDataSourceName(String dataSourceName) {
	this.dataSourceName = dataSourceName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="query_description"
     */
    public String getQueryDescription() {
	return queryDescription;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="query_category"
     */
    public String getQueryCategory() {
	return queryCategory;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="query_privilege"
     */
    public String getQueryPrivilege() {
	return queryPrivilege;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="time_restricted"
     */
    public Boolean getTimeRestricted() {
	return timeRestricted;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="datasource_name"
     */
    public String getDataSourceName() {
	return dataSourceName;
    }

    private String queryCode;

    private String queryText;

    private String queryName;

    private Boolean timeRestricted;

    private Collection queryParameters;

    private String queryDescription;

    private String queryCategory;

    private String queryPrivilege;

    private String dataSourceName;

}
