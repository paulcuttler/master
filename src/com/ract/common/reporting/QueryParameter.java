package com.ract.common.reporting;

import java.io.Serializable;
import java.sql.Types;
import java.text.ParseException;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Query parameter
 * 
 * @hibernate.class table="query_parameter" lazy="false"
 * @author jyh
 * @version 1.0
 */

public class QueryParameter implements Serializable, Comparable {

    public static final String DATATYPE_STRING = "STRING";

    public static final String DATATYPE_DOUBLE = "DOUBLE";

    public static final String DATATYPE_INTEGER = "INTEGER";

    public static final String DATATYPE_DATE = "DATE";

    public static final String DATATYPE_BOOLEAN = "BOOLEAN";

    public static final String DATATYPE_NUMERIC = "NUMERIC";

    public static final String DATATYPE_TEXT = "TEXT";

    public QueryParameter() {
    }

    public QueryParameter(QueryParameterPK queryParameterPK, String parameterType, String parameterLabel, String parameterDescription, int parameterOrder) {
	this.setQueryParameterPK(queryParameterPK);
	this.setParameterType(parameterType);
	this.setParameterOrder(parameterOrder);
	this.setParameterDescription(parameterDescription);
	this.setParameterLabel(parameterLabel);
    }

    private QueryParameterPK queryParameterPK;

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public QueryParameterPK getQueryParameterPK() {
	return this.queryParameterPK;
    }

    public void setQueryParameterPK(QueryParameterPK queryParameterPK) {
	this.queryParameterPK = queryParameterPK;
    }

    public void setParameterType(String parameterType) {
	this.parameterType = parameterType;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="parameter_type"
     */
    public String getParameterType() {
	return this.parameterType;
    }

    public int getJavaParameterType() {
	String parameterType = this.getParameterType();

	int sqlParameterType = 0;
	// determine mappings
	if (parameterType.equalsIgnoreCase(DATATYPE_INTEGER)) {
	    sqlParameterType = Types.INTEGER;
	} else if (parameterType.equalsIgnoreCase(DATATYPE_STRING)) {
	    sqlParameterType = Types.CHAR;
	} else if (parameterType.equalsIgnoreCase(DATATYPE_DOUBLE)) {
	    sqlParameterType = Types.DOUBLE;
	} else if (parameterType.equalsIgnoreCase(DATATYPE_DATE)) {
	    sqlParameterType = Types.DATE;
	} else if (parameterType.equalsIgnoreCase(DATATYPE_BOOLEAN)) {
	    sqlParameterType = Types.BIT;
	} else if (parameterType.equalsIgnoreCase(DATATYPE_NUMERIC)) {
	    sqlParameterType = Types.NUMERIC;
	} else if (parameterType.equalsIgnoreCase(DATATYPE_TEXT)) {
	    sqlParameterType = Types.LONGVARCHAR;
	}
	// test
	return sqlParameterType;
    }

    public void setParameterOrder(int parameterOrder) {
	this.parameterOrder = parameterOrder;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="parameter_order"
     */
    public int getParameterOrder() {
	return parameterOrder;
    }

    public void setParameterValue(String parameterValue) {
	this.parameterValue = parameterValue;
    }

    public String getParameterValue() {
	return this.parameterValue;
    }

    public Object getParameterObjectValue() {
	Object value = null;
	if (this.getJavaParameterType() == Types.DATE) {
	    try {
		value = (new DateTime(this.getParameterValue())).toSQLDate();
	    } catch (ParseException pe) {
		LogUtil.fatal(this.getClass(), pe);
	    }
	} else {
	    value = parameterValue;
	}
	return value;
    }

    public void setParameterLabel(String parameterLabel) {
	this.parameterLabel = parameterLabel;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="parameter_label"
     */
    public String getParameterLabel() {
	return parameterLabel;
    }

    public void setParameterDescription(String parameterDescription) {
	this.parameterDescription = parameterDescription;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="parameter_description"
     */
    public String getParameterDescription() {
	return parameterDescription;
    }

    private int parameterOrder;

    private String parameterValue;

    private String parameterType;

    private String parameterLabel;

    private String parameterDescription;

    public int compareTo(Object anotherParameter) {
	if (!(anotherParameter instanceof QueryParameter)) {
	    throw new ClassCastException("A query parameter object expected.");
	}
	int anotherParameterOrder = ((QueryParameter) anotherParameter).getParameterOrder();
	LogUtil.debug(this.getClass(), "anotherParameterOrder=" + anotherParameterOrder);
	LogUtil.debug(this.getClass(), "this.getParameterOrder()=" + this.getParameterOrder());
	if (this.getParameterOrder() > anotherParameterOrder) {
	    return 1;
	} else if (this.getParameterOrder() < anotherParameterOrder) {
	    return -1;
	} else {
	    return 0;
	}
    }

    public boolean equals(Object o) {
	boolean equal = false;
	LogUtil.debug(this.getClass(), "o=" + o);
	if (o instanceof QueryParameter) {
	    QueryParameter thatParameter = (QueryParameter) o;
	    LogUtil.debug(this.getClass(), "thatParameter=" + thatParameter);
	    if (this.getQueryParameterPK().equals(thatParameter.getQueryParameterPK())) {
		equal = true;
	    }
	}
	LogUtil.debug(this.getClass(), "equal=" + equal);
	return equal;
    }

    @Override
    public String toString() {
	return "QueryParameter [parameterDescription=" + parameterDescription + ", parameterLabel=" + parameterLabel + ", parameterOrder=" + parameterOrder + ", parameterType=" + parameterType + ", parameterValue=" + parameterValue + ", queryParameterPK=" + queryParameterPK + "]";
    }

}
