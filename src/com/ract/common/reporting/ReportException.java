package com.ract.common.reporting;

import com.ract.common.GenericException;

/**
 * <p>
 * Flag for report errors.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class ReportException extends GenericException {
    public ReportException() {
	super();
    }

    public ReportException(String msg) {
	super(msg);
    }

    public ReportException(Exception exception) {
	super(exception);
    }

    public ReportException(String msg, Exception exception) {
	super(msg, exception);
    }

}
