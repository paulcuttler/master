package com.ract.common.reporting;

public class ParameterReportRequest extends ReportRequestBase {
    public String getReportData() throws java.rmi.RemoteException {
	/**
	 * @todo Implement this com.ract.common.reporting.ReportRequestBase
	 *       abstract method
	 */
	throw new java.lang.UnsupportedOperationException("Method getReportData() not yet implemented.");
    }

    public void initialise(String printGroup, String formName, String repName, String workSpace, String format) throws Exception {
	this.printerGroup = printGroup;
	this.reportName = repName;
	this.formName = formName;
	this.reportWorkspace = workSpace;
	this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(format));
	this.setTemplateName(this.reportWorkspace + "/" + this.reportName + ".rml");
    }

}
