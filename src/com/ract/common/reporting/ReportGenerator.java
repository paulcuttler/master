package com.ract.common.reporting;

import com.elixirtech.ers2.client.ERSClient;
import com.elixirtech.net.NetException;
import com.elixirtech.report2.runtime.IJobInfo;
import com.ract.common.CommonConstants;
import com.ract.common.ExceptionHelper;
import com.ract.util.LogUtil;

/**
 * Converted from session bean due to pass by reference issue of Element and
 * serialization issues with Input/Output streams.
 * 
 * @author hollidayj
 * 
 */
public class ReportGenerator {

    public IJobInfo runReport(ReportRequestBase reportBase) throws ReportException {
	// Get a handle to the report server
	// TODO Elixir server connections can now be pooled.
	ERSClient client = new ERSClient(CommonConstants.getReportServerName(), Integer.parseInt(CommonConstants.getReportServerPort()), "Anonymous", "anonymous");
	IJobInfo job = null;
	if (reportBase != null) {
	    try {
		job = client.renderReport(reportBase.getTemplateName(), reportBase.getDeliveryFormat().getMIMEType(), // "application/pdf", mime type
			reportBase.getOutputStream(), // output stream for report
			reportBase.getDynamicParameters(), "me", null); // target - whatever that is ?? what does this mean ??
	    } catch (NetException e) {
		throw new ReportException(e);
	    }
	}
	displayReportGenerateJobInfo(job);
	return job;
    }

    private static double diffSecs(double start, double end) {
	return (end - start) / 1000d;
    }

    private void displayReportGenerateJobInfo(IJobInfo job) {
	try {
	    if (job != null) {
		String name = job.getString(IJobInfo.NAME);
		String owner = job.getString(IJobInfo.JOB_OWNER);
		String statusCode = job.getString(IJobInfo.STATUS_CODE);
		long timeReceived = job.getLong(IJobInfo.JOB_RECEIVED);
		long timeStarted = job.getLong(IJobInfo.JOB_STARTED);
		long timeEnded = job.getLong(IJobInfo.JOB_ENDED);
		double totalQueueTime = diffSecs(timeReceived, timeStarted);
		double totalJobProcessTime = diffSecs(timeStarted, timeEnded);
		double totalJobTime = diffSecs(timeReceived, timeEnded);
		int pageCount = job.getInteger(IJobInfo.PAGE_COUNT);
		int recordCount = job.getInteger(IJobInfo.RECORD_COUNT);
		String mimeType = job.getString(IJobInfo.MIME_TYPE);
		long sizeBytes = job.getLong(IJobInfo.BYTE_SIZE);

		LogUtil.debug(this.getClass(), "name=" + name);
		LogUtil.debug(this.getClass(), "owner=" + owner);
		LogUtil.debug(this.getClass(), "statusCode=" + statusCode);
		LogUtil.debug(this.getClass(), "timeReceived=" + timeReceived);
		LogUtil.debug(this.getClass(), "timeStarted=" + timeStarted);
		LogUtil.debug(this.getClass(), "timeEnded=" + timeEnded);
		LogUtil.debug(this.getClass(), "totalQueueTime=" + totalQueueTime);
		LogUtil.debug(this.getClass(), "totalJobProcessTime=" + totalJobProcessTime);
		LogUtil.debug(this.getClass(), "totalJobTime=" + totalJobTime);
		LogUtil.debug(this.getClass(), "pageCount=" + pageCount);
		LogUtil.debug(this.getClass(), "recordCount=" + recordCount);
		LogUtil.debug(this.getClass(), "mimeType=" + mimeType);
		LogUtil.debug(this.getClass(), "sizeBytes=" + sizeBytes);
	    }
	} catch (Exception e) {
	    LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
	}
    }
}
