package com.ract.common.reporting;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.w3c.dom.Node;

import com.ract.common.CommonConstants;
import com.ract.common.DataSourceFactory;
import com.ract.common.SystemException;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XDocument;
import com.ract.util.XMLHelper;

@Stateless
@Remote({ ReportMgr.class })
public class ReportMgrBean {
    @Resource(mappedName = "java:/ClientDS")
    private DataSource dataSource;

    @PersistenceContext(unitName = "master")
    Session hsession;

    public void ejbCreate() {
    }

    public void ejbRemove() throws RemoteException {
    }

    public void ejbActivate() throws RemoteException {
    }

    public void ejbPassivate() throws RemoteException {
    }

    public String getXMLResultSet(String sql) throws RemoteException {
	Query qVO = new Query();
	qVO.setQueryText(sql);
	return getXMLResultSet(qVO, null);
    }

    public String getXMLResultSet(String sql, String datasource) throws RemoteException {
	Query qVO = new Query();
	qVO.setQueryText(sql);
	return this.getXMLResultSet(qVO, datasource);
    }

    public String getXMLResultSet(Query query) throws RemoteException {
	String dataSourceName = query.getDataSourceName();
	LogUtil.log(this.getClass(), "dataSourceName=" + dataSourceName);
	if (dataSourceName == null) {
	    dataSourceName = CommonConstants.DATASOURCE_CLIENT;
	}
	return getXMLResultSet(query, dataSourceName);
    }

    /**
     * Return an XML result string using result set metadata for column names.
     * To customise the column names you will need to construct the sql with
     * column aliases.
     * 
     * @param sql
     *            Description of the Parameter
     * @return The xMLResultSet value
     * @exception RemoteException
     *                Description of the Exception
     * @todo allow passing in of column tags NOTE: only one result set per sql
     *       statement.
     */
    public String getXMLResultSet(Query query, String datasource) throws RemoteException {
	if (datasource == null) {
	    throw new RemoteException("No datasource specified.");
	}

	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	StringBuffer xmlResultSet = new StringBuffer();
	StringBuffer xmlString = new StringBuffer();

	try {

	    connection = getConnection(datasource);

	    if (query != null) {
		// prepare the statement
		ps = connection.prepareStatement(query.getQueryText());
		// set the parameters
		Collection qpList = query.getQueryParameters();
		if (qpList != null && qpList.size() > 0) {
		    QueryParameter qParameters = null;
		    Iterator qpIt = qpList.iterator();
		    while (qpIt.hasNext()) {
			qParameters = (QueryParameter) qpIt.next();
			// set the query parameters

			LogUtil.debug(this.getClass(), "order = " + qParameters.getParameterOrder());
			LogUtil.debug(this.getClass(), "value = " + qParameters.getParameterValue());
			LogUtil.debug(this.getClass(), "type = " + qParameters.getParameterType());

			ps.setObject(qParameters.getParameterOrder(), qParameters.getParameterObjectValue(), qParameters.getJavaParameterType());
		    }
		}
	    }

	    rs = ps.executeQuery();

	    ResultSetMetaData rsmd = rs.getMetaData();
	    int columnCount = rsmd.getColumnCount();

	    int recordCount = 0;
	    String columnName = null;
	    int columnType = 0;
	    Object value = null;
	    String stringValue = null;
	    while (rs.next()) {
		recordCount++;
		xmlString.append("<" + ReportMgr.TAG_RESULT + " " + ReportMgr.TAG_ROWID + "=\"" + recordCount + "\">" + "\n");

		// column iteration
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {

		    columnName = rsmd.getColumnName(i);
		    // replace dashes and spaces
		    columnName = StringUtil.replace(columnName, "-", "_");
		    columnName = StringUtil.replace(columnName, " ", "_");
		    columnType = rsmd.getColumnType(i);

		    value = rs.getObject(i);

		    // add the row
		    xmlString.append("<" + columnName.toLowerCase() + " " + ReportMgr.TAG_COLUMN_TYPE + "=\"" + rsmd.getColumnTypeName(i) + "\">");

		    if (value != null) {
			stringValue = decodeColumn(value, columnType, false);
			xmlString.append(stringValue);
		    }
		    xmlString.append("</" + columnName.toLowerCase() + ">" + "\n");
		}

		xmlString.append("</" + ReportMgr.TAG_RESULT + ">" + "\n");
	    }

	    if (recordCount == 0) {
		xmlString.append(emptyXMLrow(rsmd));
	    }

	    // construct the document
	    xmlResultSet.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n");
	    xmlResultSet.append("<" + ReportMgr.TAG_RESULTSET + " " + ReportMgr.TAG_RESULTSETSIZE + "=\"" + recordCount + "\" " + ReportMgr.TAG_COLUMN_COUNT + "=\"" + columnCount + "\">" + "\n");
	    xmlResultSet.append(xmlString.toString());

	    xmlString = null;
	    xmlResultSet.append("</" + ReportMgr.TAG_RESULTSET + ">");

	} catch (SQLException e) {
	    throw new RemoteException(query.getQueryText(), e);
	} finally {
	    ConnectionUtil.closeConnection(connection, ps);
	}

	return XMLHelper.encode(xmlResultSet.toString());
    }

    public Node getXMLFromQuery(Query query) throws RemoteException {

	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	StringBuffer xmlString = new StringBuffer();

	XDocument doc = new XDocument();
	Node root = doc.addNode(doc, "report");
	LogUtil.log(this.getClass(), "getXMLFromQuery 1 " + doc);

	try {

	    connection = getConnection(CommonConstants.DATASOURCE_CLIENT);

	    if (query != null) {
		// prepare the statement
		ps = connection.prepareStatement(query.getQueryText());
		// set the parameters
		Collection qpList = query.getQueryParameters();
		if (qpList != null && qpList.size() > 0) {
		    QueryParameter qParameters = null;
		    Iterator qpIt = qpList.iterator();
		    while (qpIt.hasNext()) {
			qParameters = (QueryParameter) qpIt.next();
			// set the query parameters

			LogUtil.debug(this.getClass(), "order = " + qParameters.getParameterOrder());
			LogUtil.debug(this.getClass(), "value = " + qParameters.getParameterValue());
			LogUtil.debug(this.getClass(), "type = " + qParameters.getParameterType());

			ps.setObject(qParameters.getParameterOrder(), qParameters.getParameterObjectValue(), qParameters.getJavaParameterType());
		    }
		}
	    }

	    rs = ps.executeQuery();

	    ResultSetMetaData rsmd = rs.getMetaData();
	    int columnCount = rsmd.getColumnCount();

	    int recordCount = 0;
	    String columnName = null;
	    int columnType = 0;
	    Object value = null;
	    String stringValue = null;

	    while (rs.next()) {
		Node record = doc.addNode(root, "record");
		// column iteration
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {

		    columnName = rsmd.getColumnName(i);
		    // replace dashes and spaces
		    columnName = StringUtil.replace(columnName, "-", "_");
		    columnName = StringUtil.replace(columnName, " ", "_");
		    columnType = rsmd.getColumnType(i);

		    value = rs.getObject(i);

		    // add the row
		    if (value != null) {
			stringValue = decodeColumn(value, columnType, false);
			doc.addLeaf(record, columnName, stringValue);

		    }
		}

	    }

	    if (recordCount == 0) {
		xmlString.append(emptyXMLrow(rsmd));
	    }
	} catch (SQLException e) {
	    throw new RemoteException(query.getQueryText(), e);
	} finally {
	    ConnectionUtil.closeConnection(connection, ps);
	}
	LogUtil.log(this.getClass(), "getXMLFromQuery 2 " + doc);
	return root;
    }

    private Connection getConnection(String datasource) throws RemoteException, SQLException {
	Connection connection;
	try {
	    if (dataSource == null) {
		// default
		dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
		connection = dataSource.getConnection();
	    } else {
		dataSource = DataSourceFactory.getDataSource(datasource);
		connection = dataSource.getConnection();
	    }
	} catch (NamingException ne) {
	    throw new RemoteException("Unable to locate datasource " + dataSource.toString());
	}
	return connection;
    }

    private String emptyXMLrow(ResultSetMetaData rsmd) throws SQLException {
	StringBuffer tmpXml = new StringBuffer();
	tmpXml.append("<" + ReportMgr.TAG_RESULT + " " + ReportMgr.TAG_ROWID + "=\"0\">");

	String columnName = null;
	int columnType = 0;

	// column iteration
	for (int i = 1; i <= rsmd.getColumnCount(); i++) {

	    columnName = rsmd.getColumnName(i);

	    columnName = StringUtil.replace(columnName, "-", "_");
	    columnType = rsmd.getColumnType(i);

	    // add the row
	    tmpXml.append("<" + columnName.toLowerCase() + " " + ReportMgr.TAG_COLUMN_TYPE + "=\"" + rsmd.getColumnTypeName(i) + "\">");

	    tmpXml.append("</" + columnName.toLowerCase() + ">" + "\n");
	}

	tmpXml.append("</" + ReportMgr.TAG_RESULT + ">");

	return tmpXml.toString();
    }

    /**
     * @todo convert to read xml
     * @param sql
     * @param extractToFile
     * @return
     * @throws RemoteException
     */
    public String getCSVResultSet(String sql, String extractToFile) throws RemoteException {

	File f = new File(extractToFile);

	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	StringBuffer setSB = new StringBuffer();
	StringBuffer rowSB = new StringBuffer();
	int prevTransactionLevel = 0;
	int recordCount = 0;

	try {

	    connection = dataSource.getConnection();
	    // prevTransactionLevel = connection.getTransactionIsolation();
	    // connection.setTransactionIsolation(connection.TRANSACTION_READ_UNCOMMITTED);

	    ps = connection.prepareStatement(sql);

	    rs = ps.executeQuery();

	    ResultSetMetaData rsmd = rs.getMetaData();

	    FileWriter csvFile = new FileWriter(f);

	    String columnName = null;
	    int columnType = 0;
	    Object value = null;
	    String stringValue = null;

	    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
		rowSB.append("\"");
		rowSB.append(StringUtil.replace(rsmd.getColumnName(i), "-", "_"));
		rowSB.append("\",");
	    }
	    rowSB.setLength(rowSB.length() - 1);
	    rowSB.append("\n");
	    csvFile.write(rowSB.toString());
	    csvFile.flush();
	    // setSB.append(rowSB.toString());

	    rowSB.setLength(0);

	    while (rs.next()) {
		recordCount++;
		// column iteration
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {

		    columnType = rsmd.getColumnType(i);
		    value = rs.getObject(i);
		    stringValue = decodeColumn(value, columnType);
		    rowSB.append(stringValue);
		    rowSB.append(",");
		}

		rowSB.setLength(rowSB.length() - 1);
		rowSB.append("\n");

		csvFile.write(rowSB.toString());
		csvFile.flush();

		rowSB.setLength(0);
	    }

	} catch (SQLException e) {
	    throw new RemoteException(sql, e);
	} catch (IOException ioe) {
	    ioe.printStackTrace();
	} finally {
	    ConnectionUtil.closeConnection(connection, ps);
	}

	return "Done " + recordCount + " records extracted. "; // setSB.toString();
    }

    private String decodeColumn(Object o, int colType) {
	return decodeColumn(o, colType, true);
    }

    private String decodeColumn(Object o, int colType, boolean csv) {

	String colValue = null;
	if (o != null) {
	    if (colType == java.sql.Types.DATE) {
		colValue = DateUtil.formatShortDate(((DateTime) o));
	    } else if (colType == java.sql.Types.TIME || colType == java.sql.Types.TIMESTAMP) {
		colValue = DateUtil.formatShortDate((java.util.Date) o);
	    } else if ((colType == java.sql.Types.CHAR || colType == java.sql.Types.VARCHAR || colType == java.sql.Types.LONGVARCHAR) && csv) {
		colValue = "\"" + o.toString().trim() + "\"";
	    } else {
		colValue = o.toString().trim();
	    }
	}

	return colValue;
    }

    public ArrayList getQueryParameterList(String queryCode) throws RemoteException {

	ArrayList queryParameterList = null;
	Query query = null;
	try {
	    Criteria crit = hsession.createCriteria(QueryParameter.class);
	    if (queryCode != null) {
		crit.add(Expression.eq("queryParameterPK.queryCode", queryCode));
	    }
	    queryParameterList = (ArrayList) crit.list();

	} catch (HibernateException ex) {
	    throw new SystemException(ex);
	}
	LogUtil.log(this.getClass(), "queryParameterList=" + queryParameterList);
	return queryParameterList;
    }

    public Query getQuery(String queryCode) throws RemoteException {
	Query query = null;
	try {
	    query = (Query) hsession.get(Query.class, new String(queryCode));
	} catch (HibernateException ex) {
	    throw new SystemException(ex);
	}
	return query;

    }

    public void updateQuery(Query query) throws RemoteException {
	hsession.saveOrUpdate(query);

	Collection dbQueryParameters = getQueryParameterList(query.getQueryCode());
	// delete old list
	QueryParameter queryParameter;
	for (Iterator i = dbQueryParameters.iterator(); i.hasNext();) {
	    queryParameter = (QueryParameter) i.next();
	    // delete
	    hsession.delete(queryParameter);
	}

	// insert new list
	Collection queryParameters = query.getQueryParameters();
	if (queryParameters != null) {
	    for (Iterator i = queryParameters.iterator(); i.hasNext();) {
		queryParameter = (QueryParameter) i.next();
		// delete
		hsession.merge(queryParameter);
	    }
	}
    }

    public Collection getQueryList() throws RemoteException {
	return getQueryList(null);
    }

    // query list
    public Collection getQueryList(String queryCode) throws RemoteException {
	ArrayList queryList = null;
	Query query = null;
	try {
	    Criteria crit = hsession.createCriteria(Query.class);
	    if (queryCode != null) {
		crit.add(Expression.eq("queryCode", queryCode));
	    }
	    queryList = (ArrayList) crit.list();

	} catch (HibernateException ex) {
	    throw new SystemException(ex);
	}

	return queryList;
    }

}
