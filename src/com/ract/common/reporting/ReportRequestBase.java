package com.ract.common.reporting;

import java.io.OutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import org.w3c.dom.Element;

import com.ract.common.CommonConstants;
import com.ract.common.SystemParameterVO;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * Provides common services for constructing a report request.
 * 
 * @author T. Bakker
 * @created 9 May 2003
 * @version 1.0
 */

public abstract class ReportRequestBase implements Serializable
/* extends ReportRequest */
{
    public String getTemplateName() {
	return templateName;
    }

    public void setTemplateName(String templateName) {
	this.templateName = templateName;
    }

    private String reportDataURL;
    protected Element reportXML;
    protected String reportName;
    private String templateName;
    // protected String workspace;
    private Properties dynamicParameters;
    private String reportKeyName;

    private boolean removeFile = true;
    private boolean reportRequired = false;

    public boolean isRemoveFile() {
	return removeFile;
    }

    public void setRemoveFile(boolean removeFile) {
	this.removeFile = removeFile;
    }

    private String dataServerContext;
    private ReportDeliveryFormat deliveryFormat;
    protected String reportWorkspace;

    @Override
    public String toString() {
	return "ReportRequestBase [MIMEType=" + MIMEType + ", dataServerContext=" + dataServerContext + ", deliveryFormat=" + deliveryFormat + ", destinationFileName=" + destinationFileName + ", dynamicParameters=" + dynamicParameters + ", formName=" + formName + ", imageLocation=" + imageLocation + ", os=" + os + ", printerGroup=" + printerGroup + ", printerName=" + printerName + ", queryParameters=" + queryParameters + ", removeFile=" + removeFile + ", reportDataURL=" + reportDataURL + ", reportKeyName=" + reportKeyName + ", reportName=" + reportName + ", reportWorkspace=" + reportWorkspace + ", reportXML=" + reportXML + ", templateName=" + templateName + "]";
    }

    protected String formName;
    protected String MIMEType;
    protected OutputStream os;
    protected String printerGroup;
    protected String printerName;
    protected String imageLocation;
    protected String destinationFileName;
    protected Hashtable queryParameters;

    public static final String PARAMETER_DELIVERY_FORMAT = "deliveryFormat";
    public static final String PARAMETER_DELIVERY_METHOD = "deliveryMethod";

    public static final String PARAMETER_USERNAME = "userName";
    public static final String PARAMETER_REPORT_KEY = "reportKey";

    public static final String REPORT_DELIVERY_METHOD_SCREEN = "Screen";
    public static final String REPORT_DELIVERY_METHOD_EMAIL = "Email";
    public static final String REPORT_DELIVERY_METHOD_PRINTER = "Printer";
    public static final String REPORT_DELIVERY_FORMAT_HTML = "HTML";
    public static final String REPORT_DELIVERY_FORMAT_PDF = "PDF";
    public static final String REPORT_DELIVERY_FORMAT_XML = "XML";
    public static final String REPORT_DELIVERY_FORMAT_JGF = "JGF";
    public static final String REPORT_DELIVERY_FORMAT_CSV = "CSV";

    public static String MIME_TYPE_PDF = "application/pdf";
    public static String MIME_TYPE_POSTSCRIPT = "application/postscript";
    public static String MIME_TYPE_EXCEL = "application/vnd.ms-excel";
    public static String MIME_TYPE_RTF = "application/rtf";
    public static String MIME_TYPE_XPRINT = "application/x-print";
    public static String MIME_TYPE_XGLINT = "application/x-glint";
    public static String MIME_TYPE_XML = "text/xml";
    public static String MIME_TYPE_HTML = "text/html";
    public static String MIME_TYPE_XHTML = "application/xhtml+xml";
    public static String MIME_TYPE_CSV = "text/csv";
    public static String MIME_TYPE_LPT = "text/x-lpt";

    public ReportRequestBase() {
	dynamicParameters = new Properties();
    }

    public void setReportWorkspace(String ws) {
	this.reportWorkspace = ws;
    }

    public void setQueryParameters(Hashtable pl) {
	this.queryParameters = pl;
    }

    public Hashtable getQueryParameters() {
	return this.queryParameters;
    }

    public void setDeliveryFormat(ReportDeliveryFormat rdf) {
	this.deliveryFormat = rdf;
    }

    public ReportDeliveryFormat getDeliveryFormat() {
	return this.deliveryFormat;
    }

    public void setDestinationFileName(String dfn) {
	this.destinationFileName = dfn;
    }

    public String getDestinationFileName() {
	return this.destinationFileName;
    }

    public void setReportKeyName(String repName) {
	this.reportKeyName = repName;
    }

    public String getReportKeyName() {
	return this.reportKeyName;
    }

    public void setMIMEType(String mType) {
	this.MIMEType = mType;
    }

    public String getMIMEType() {
	return this.MIMEType;
    }

    public void setFormName(String fn) {
	this.formName = fn;
    }

    public String getFormName() {
	return formName;
    }

    public void setPrinterGroup(String prGroup) {
	this.printerGroup = prGroup;
    }

    public String getPrinterGroup() {
	return this.printerGroup;
    }

    public void setOutputStream(OutputStream oStream) {
	this.os = oStream;
    }

    public OutputStream getOutputStream() {
	return os;
    }

    public void addParameter(String paramName, String value) {
	// System.out.println("Add parameter " + paramName + ":  " + value);
	this.dynamicParameters.put(paramName, value);
    }

    public Properties getDynamicParameters() {
	return this.dynamicParameters;
    }

    public String getDataServerContext() {
	return this.dataServerContext;
    }

    public void setDataServerContext(String dataServerContext) {
	this.dataServerContext = dataServerContext;
    }

    public String getReportDataURL() {
	return this.reportDataURL;
    }

    public void setReportDataURL(String url) {
	this.reportDataURL = url;
    }

    public String getReportName() {
	return this.reportName;
    }

    private String reportAttachmentName;

    public String getReportAttachmentName() {
	return reportAttachmentName;
    }

    public void setReportAttachmentName(String reportAttachmentName) {
	this.reportAttachmentName = reportAttachmentName;
    }

    public void setReportName(String reportName) {
	this.reportName = reportName;
    }

    public void setParameterList(Hashtable pList) {
	Enumeration e = pList.keys();
	String keyName = null;
	String keyValue = null;
	Object kOb = null;
	Object vOb = null;

	while (e.hasMoreElements()) {
	    kOb = null;
	    vOb = null;
	    try {
		kOb = e.nextElement();
		keyName = (String) kOb;
		vOb = pList.get(kOb);
		keyValue = (String) vOb;
		for (int xx = 0; xx < keyValue.length(); xx++) {
		    char ch = keyValue.charAt(xx);
		    if (ch == '"' || ch == '\\' || ch == '\"') {
			System.out.println("FUNNY BITS FOUND: " + keyValue);
		    }
		}
	    } catch (Exception ex) {
		LogUtil.log(this.getClass(), " Error loading parameters for Elixir:" + "\n" + kOb + "   " + vOb + "\n " + ex);
		ex.printStackTrace();
	    }
	    this.dynamicParameters.put(keyName, keyValue);
	}

    }

    public void setReportTemplate(String workSpace, String templateName) throws RemoteException {
	this.templateName = workSpace + "/" + templateName + ".rml";
	this.reportWorkspace = workSpace;
	// Set the output format of the report
    }

    public String constructURL() throws Exception {
	return constructURL(this.reportKeyName);
    }

    protected String constructURL(String reportKeyName) throws Exception {
	String dSC = FileUtil.getProperty("master", SystemParameterVO.ELIXIR_REPORT_SERVER_URL);
	StringBuffer url = new StringBuffer(dSC);
	url.append(FileUtil.getProperty("master", CommonConstants.REPORT_DATA_SERVER));
	url.append("?");
	url.append(PARAMETER_REPORT_KEY);
	url.append("=");
	url.append(reportKeyName);
	this.reportDataURL = url.toString();
	return reportDataURL;
    }

    /**
     * This is the call back method that the report data server calls to
     * generate the XML document that holds the data required by Elixir. This
     * method should be overridden by sub classes that generate specific data
     * for specific reports.
     */
    public abstract String getReportData() throws RemoteException;

	public boolean isReportRequired() {
		return reportRequired;
	}

	public void setReportRequired(boolean reportRequired) {
		this.reportRequired = reportRequired;
	}

    // public abstract String getReportData(Hashtable queryParameters)
    // throws RemoteException;

    // display all properties for diagnostics
    // public String toString()
    // {
    // String ss = "REPORT REQUEST ATTRIBUTES:"
    // + "\n  reportDataURL:     " + reportDataURL
    // + "\n  reportName:        " + reportName
    // + "\n  templateName:      " + templateName
    // + "\n  reportWorkspace:   " + reportWorkspace
    // + "\n  reportKeyName:     " + reportKeyName
    // + "\n  dataServerContext: " + dataServerContext
    // + "\n  printerGroup:      " + printerGroup
    // + "\n  formName:          " + formName
    // + "\n  MIMEType:          " + MIMEType
    // + "\n  destinationFileName: " + this.destinationFileName;
    // if(deliveryFormat==null)
    // {
    // ss = ss + "\n  reportDeliveryFormat: null";
    // }
    // else ss = ss + "\nreportDeliveryFormat: " +
    // this.deliveryFormat.getDeliveryFormatName();
    //
    //
    // if(dynamicParameters.isEmpty() || dynamicParameters==null)
    // {
    // ss = ss + "\nNO PARAMETERS SET";
    // }
    // else
    // {
    // ss = ss + "\n PARAMETERS:";
    // Enumeration e = dynamicParameters.propertyNames();
    // String pName = null;
    // String pValue = null;
    // while(e.hasMoreElements())
    // {
    // pName = (String)e.nextElement();
    // ss = ss + "\n " + pName + " = " + dynamicParameters.getProperty(pName);
    // }
    // }
    // try
    // {
    // ss += "\n  reportXML:         " + getReportData();
    // }
    // catch(RemoteException ex)
    // {
    // //do nothing
    // }
    //
    // return ss;
    // }
}
