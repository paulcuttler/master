package com.ract.common.reporting;

import java.util.Properties;

/**
 * 
 * <p>
 * Properties container for reports. Only accept string values and keys.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class ReportProperties extends Properties {

    public void put(String key, String value) {
	this.put(key, value);
    }

    public synchronized Object put(Object key, Object value) {
	if (!(value instanceof String) || !(key instanceof String)) {
	    throw new IllegalArgumentException("Key and value must be strings for report parameters.");
	}
	return super.put(key, value);
    }
}
