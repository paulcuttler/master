package com.ract.common.reporting;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.Remote;

import org.w3c.dom.Node;

@Remote
public interface ReportMgr {

    public final static String TAG_RESULTSET = "resultset";
    public final static String TAG_RESULT = "row";
    public final static String TAG_ROWID = "id";
    public final static String TAG_RESULTSETSIZE = "size";
    public final static String TAG_COLUMN_TYPE = "type";
    public final static String TAG_COLUMN_COUNT = "columncount";

    /**
     * Save one report in the session at a time
     */
    public static final String XML_REPORT_KEY = "key";

    public String getXMLResultSet(String sql, String dataSourceName) throws RemoteException;

    public String getXMLResultSet(Query query) throws RemoteException;

    public String getCSVResultSet(String sql, String extractToFile) throws RemoteException;

    public Query getQuery(String queryCode) throws RemoteException;

    public Collection getQueryList() throws RemoteException;

    public Collection getQueryList(String qCode) throws RemoteException;

    public ArrayList getQueryParameterList(String queryCode) throws RemoteException;

    public Node getXMLFromQuery(Query query) throws RemoteException;

    public void updateQuery(Query query) throws RemoteException;
}
