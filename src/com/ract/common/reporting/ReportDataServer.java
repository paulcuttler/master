package com.ract.common.reporting;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.XMLHandler;
import com.ract.util.XMLHelper;
import com.ract.util.XMLToCSV;
import com.ract.util.XMLToPDF;

/**
 * This servlet handles HTTP requests for report data. It extracts the
 * parameters from the request, fetches the XML formatted report data and
 * returns it.
 * 
 * T. Bakker, 13 May 2003
 */
public class ReportDataServer extends HttpServlet // applicationservlet?
{
    private static final String CONTENT_TYPE_XML = "text/xml";

    // private static final String DOC_TYPE;

    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	LogUtil.debug(this.getClass(), "doGet");
	doPost(request, response);
    }

    /**
     * Default file name
     */
    public static final String EXTRACT_FILENAME = "extract";
    public static final String EXTRACT_CSV = ".csv";
    public static final String EXTRACT_PDF = ".pdf";

    /**
     * Handle the request for report data.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	LogUtil.debug(this.getClass(), "doPost");
	// log("Request parameters:");

	String query = request.getParameter("query");

	// log(query);

	String key = request.getParameter(ReportMgr.XML_REPORT_KEY);
	if (key != null && !key.equals("")) {
	    String attr[] = new String[5];
	    String xmlDoc = (String) request.getSession().getAttribute(ReportMgr.XML_REPORT_KEY);
	    request.getSession().removeAttribute(key);
	    Hashtable attributes = XMLHelper.getResultSetAttributes(xmlDoc);
	    Integer colCount = (Integer) attributes.get(ReportMgr.TAG_COLUMN_COUNT);
	    attr[0] = xmlDoc;
	    attr[1] = ",";
	    attr[2] = colCount.toString();
	    attr[3] = request.getParameter("excludeHeader");

	    CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();

	    Integer maxPdfColumns = new Integer(8); // default
	    try {
		SystemParameterVO maxPdfColumnsVO = commonMgrLocal.getSystemParameter(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.PDF_REPORT_MAX_COLUMNS);
		maxPdfColumns = maxPdfColumnsVO.getNumericValue1().intValue();
	    } catch (Exception e) {
		LogUtil.warn(getClass(), "Unable to determine PDF Report Max Columns from DB, defaulting to " + maxPdfColumns + " (" + e + ")");
	    }

	    String event = request.getParameter("event");

	    // convert it
	    String fileName = EXTRACT_FILENAME;
	    XMLHandler conv = null;

	    if (event.equals("saveCSV")) {
		conv = new XMLToCSV(attr);
		fileName += EXTRACT_CSV;
	    } else if (event.equals("savePDF")) {

		if (colCount > maxPdfColumns) {
		    throw new ServletException("Too many columns for PDF output, reduce result set columns to " + maxPdfColumns);
		}

		conv = new XMLToPDF(attr);
		fileName += EXTRACT_PDF;
	    }

	    // prompt to download it
	    try {
		FileUtil.downloadContentsAsFile(response, response.getOutputStream(), conv.getContent(), fileName);
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    // stay on same page
	    return;
	}

	if (query == null || query.equals("")) {
	    Enumeration parameterNames = request.getParameterNames();
	    while (parameterNames.hasMoreElements()) {
		String paramName = (String) parameterNames.nextElement();
		LogUtil.debug(this.getClass(), "paramName=" + paramName + ", value=" + request.getParameter(paramName));
	    }

	    response.setContentType(CONTENT_TYPE_XML);

	    try {
		String reportData = getReportData(request);
		LogUtil.debug(this.getClass(), "reportData=" + reportData);
		response.getWriter().write(reportData);
	    } catch (Exception e) {
		// log("Failed to get report data.", e);
		throw new ServletException(e);
	    }
	} else {
	    String results = null;
	    LogUtil.debug(this.getClass(), "query results");
	    try {
		results = getXMLResults(request);
		request.getSession().setAttribute(ReportMgr.XML_REPORT_KEY, results);

	    } catch (RemoteException re) {
		throw new ServletException(re);
	    }
	    request.setAttribute("xmlResults", results);
	    request.setAttribute("query", query);
	    try {
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/common/ViewQueryResults.jsp");
		rd.forward(request, response);
	    } catch (IOException e) {
		throw new ServletException(e);
	    }
	}

    }

    /**
     * Get the data for the report in a string formatted as an XML document.
     */
    private String getReportData(HttpServletRequest request) throws RemoteException {
	ReportRequestBase report = getReport(request);
	return report.getReportData();
    }

    private ReportRequestBase getReport(HttpServletRequest request) {
	return ReportRegister.getReport(request.getParameter(ReportRequestBase.PARAMETER_REPORT_KEY));
    }

    /**
     * Extracts the parameters passed with the request and returns them as a
     * Hashtable where the key is a string and the value is a string.
     */
    private Hashtable extractParameters(HttpServletRequest request) {
	Hashtable parameterList = new Hashtable();
	Enumeration nameList = request.getParameterNames();
	String name;
	while (nameList.hasMoreElements()) {
	    name = (String) nameList.nextElement();
	    parameterList.put(name, request.getParameter(name));
	}
	return parameterList;
    }

    /**
     * Construct the xml document based on an sql query.
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws RemoteException
     */
    private String getXMLResults(HttpServletRequest request) throws ServletException, RemoteException {
	String results = "";

	ReportMgr reportMgr = CommonEJBHelper.getReportMgr();

	Query query = getQuery(request, reportMgr);
	if (!query.canRun(null)) {
	    throw new ValidationException("Query may not be run during normal business hours.");
	}

	try {
	    results = reportMgr.getXMLResultSet(query);
	} catch (RemoteException re) {
	    throw new ServletException("Unable to get the xml result set.", re);
	}

	return results;
    }

    private Query getQuery(HttpServletRequest request, ReportMgr rMgr) throws RemoteException {
	String queryCode = request.getParameter("query");
	Query query = rMgr.getQuery(queryCode);

	Collection queryParameterList = query.getQueryParameters();
	QueryParameter queryParameter = null;
	String paramName = "";
	String value = "";
	Iterator queryParameterListIt = queryParameterList.iterator();
	while (queryParameterListIt.hasNext()) {
	    queryParameter = (QueryParameter) queryParameterListIt.next();
	    paramName = queryParameter.getQueryParameterPK().getParameterName();
	    // set the parameter values
	    value = request.getParameter(paramName);
	    queryParameter.setParameterValue(value);
	}
	return query;
    }

    public void destroy() {
    }
}
