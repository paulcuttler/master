package com.ract.common;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A specialised servlet to process requests from Progress 4GL.
 * 
 * Note: due to problems with socket connections from Progress this will be
 * deprecated in favour of a raw sockets approach.
 * 
 * @author John Holliday.
 */

public abstract class ProgressServlet extends HttpServlet {
    protected final String FAILURE = "failure";

    protected final String SUCCESS = "success";

    protected final String RESPONSE_NAME = "responseName";

    protected final String METHOD_PREFIX = "handleEvent_";

    private final String DATE_FORMAT_MASK = "dd/MM/yyyy HH:mm:ss:SS";

    private final SimpleDateFormat sd = new SimpleDateFormat(DATE_FORMAT_MASK);

    protected void handleEvent(String event, HttpServletRequest request, HttpServletResponse response) {
	System.out.println("In ProgressServlet.handleEvent: " + event);
	Class c = this.getClass();
	System.out.println("class " + c.getName());
	String methodName = METHOD_PREFIX + event;
	System.out.println("method " + methodName);
	Method m;
	try {
	    Class[] parameterTypes = new Class[] { HttpServletRequest.class, HttpServletResponse.class };
	    m = c.getMethod(methodName, parameterTypes);
	    Object[] arguments = new Object[] { request, response };
	    m.invoke(this, arguments);
	} catch (Exception e) {
	    log("Error finding event. " + e);
	    response.setHeader(this.RESPONSE_NAME, this.FAILURE);
	}
    }
}
