//Source file: H:\\Code\\client\\src\\com\\ract\\common\\PostalAddressImpl.java

package com.ract.common;

import org.w3c.dom.Node;

/**
 * An implementation of a postal address.
 */

public class PostalAddressVO extends AddressVO {

    /*********************** Constructors *******************************/
    /**
     * Default constructor
     */
    public PostalAddressVO() {

    }

    /**
     * Construct a skeleton street address
     */
    public PostalAddressVO(String property, String propertyQualifier, StreetSuburbVO streetSuburb) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburb(streetSuburb);
	this.setDpid("");
    }

    /**
     * Construct a skeleton street address
     */
    public PostalAddressVO(String property, String propertyQualifier, Integer stSubID) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburbID(stSubID);
	this.setDpid("");
    }

    /**
     * Construct a skeleton street address added DPID
     */
    public PostalAddressVO(String property, String propertyQualifier, String dpid, StreetSuburbVO streetSuburb) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburb(streetSuburb);
	this.setDpid(dpid);
    }

    /**
     * Construct a skeleton street address added DPID
     */
    public PostalAddressVO(String property, String propertyQualifier, Integer stSubID, String dpid) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburbID(stSubID);
	this.setDpid(dpid);
    }

    /**
     * Construct a skeleton street address added DPID and Ausbar
     */
    public PostalAddressVO(String property, String propertyQualifier, Integer stSubID, String dpid, String ausbar) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburbID(stSubID);
	this.setDpid(dpid);
	this.setAusbar(ausbar);
    }

    /**
     * Construct the address using the data in the XML DOM node. Set the mode to
     * "History" and set the value object to read only.
     */
    public PostalAddressVO(Node addressNode) throws SystemException {
	super(addressNode);
    }

    /*********************** Getter methods *******************************/

    /**
     * Return the address type as a string that can be displayed. Return the
     * constant Address.ADDRESS_TYPE_POSTAL.
     * 
     * @return String
     * @roseuid 3C5DF3CC03CF
     */
    public String getAddressType() {
	return AddressVO.ADDRESS_TYPE_POSTAL;
    }

    /**
     * Validate the address attributes. Raise an exception if the validation
     * fails. Postal addresses may specify post office boxes.
     * 
     * @roseuid 3C02C84E0272
     */
    public void validate() {

    }

    /**
     * Return a copy of the postal address.
     * 
     * @return Common_lv.Common.Addr.PostalAddressVO
     * @roseuid 3C5DD4730163
     */
    public AddressVO copy() {
	return null;
    }

    /**
     * Return a new instance of an object that implements a PostalAddress.
     * 
     * @return Common_lv.Common.Addr.PostalAddressVO
     * @roseuid 3C5DEFF500DD
     */
    public PostalAddressVO create() {
	return new PostalAddressVO();
    }

}
