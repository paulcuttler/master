//Source file: H:\\Code\\client\\src\\com\\ract\\common\\StreetAddressImpl.java

package com.ract.common;

import org.w3c.dom.Node;

/**
 * An implementation of a street address.
 */

public class ResidentialAddressVO extends AddressVO {

    /*********************** Constructors *******************************/
    /**
     * Default constructor
     */
    public ResidentialAddressVO() {

    }

    /**
     * Construct a skeleton street address
     */
    public ResidentialAddressVO(String property, String propertyQualifier, StreetSuburbVO streetSuburb) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburb(streetSuburb);
	this.setDpid("");
    }

    /**
     * Construct a skeleton street address
     */
    public ResidentialAddressVO(String property, String propertyQualifier, Integer stSubID) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburbID(stSubID);
	this.setDpid("");
    }

    /**
     * Construct a skeleton street address - added DPID
     */
    public ResidentialAddressVO(String property, String propertyQualifier, String dpid, StreetSuburbVO streetSuburb) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburb(streetSuburb);
	this.setDpid(dpid);
    }

    /**
     * Construct a skeleton street address - added DPID
     */
    public ResidentialAddressVO(String property, String propertyQualifier, Integer stSubID, String dpid) {
	this.setProperty(property);
	this.setPropertyQualifier(propertyQualifier);
	this.setStreetSuburbID(stSubID);
	this.setDpid(dpid);
    }

    /**
     * Construct the address using the data in the XML DOM node. Set the mode to
     * "History" and set the value object to read only.
     */
    public ResidentialAddressVO(Node addressNode) throws SystemException {
	super(addressNode);
    }

    /*********************** Getter methods *******************************/

    /**
     * Return the address type as a string that can be displayed. Return the
     * constant Address.ADDRESS_TYPE_STREET.
     * 
     * @return String
     * @roseuid 3C5DF3630130
     */
    public String getAddressType() {
	return AddressVO.ADDRESS_TYPE_STREET;
    }

    /**
     * Validate the address attributes. Raise an exception if the validation
     * fails. Street addresses may not specify post office boxes or any location
     * that does not specify an actual street address.
     * 
     * @roseuid 3C02C85601D3
     */
    public void validate() {

    }

    /**
     * Return a copy of the street address.
     * 
     * @return Common_lv.Common.Addr.StreetAddressVO
     * @roseuid 3C5DD43800AA
     */
    public AddressVO copy() {
	return null;
    }

    /**
     * Return a new instance of an object that implements a StreetAddress.
     * 
     * @return Common_lv.Common.Addr.StreetAddressVO
     * @roseuid 3C5DEFC40371
     */
    public AddressVO create() {
	return new ResidentialAddressVO();
    }

}
