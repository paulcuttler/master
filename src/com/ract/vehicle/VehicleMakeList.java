package com.ract.vehicle;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Holds a list of vehicle makes with accessor methods to allow for faster
 * loading of make data.
 * 
 * @author dgk
 * @version 1.0
 */

public class VehicleMakeList implements Serializable {
    private ArrayList list = null;

    public void add(String make, String makeDesc) {
	MakeType mt = new MakeType(make, makeDesc);
	if (list == null) {
	    list = new ArrayList();
	}
	list.add(mt);
    }

    public int size() {
	return list.size();
    }

    public String getMake(int element) {
	return ((MakeType) list.get(element)).make;
    }

    public String getDesc(int element) {
	return ((MakeType) list.get(element)).makeDesc;
    }
}

class MakeType implements Serializable {
    public String make;

    public String makeDesc;

    public MakeType(String make, String desc) {
	this.make = make;
	this.makeDesc = desc;
    }
}