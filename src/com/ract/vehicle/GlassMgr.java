package com.ract.vehicle;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

/**
 * Title: Description: Copyright: Copyright (c) 2002 Company: RACT Ltd
 * 
 * @author
 * @version 1.0
 */

public interface GlassMgr extends EJBObject {
    public VehicleMakeList getGlassMakes() throws RemoteException;

    public VehicleMakeList getGlassMakes(int year) throws RemoteException;

    public String getVehicleDescription(String nvic) throws RemoteException;

    public VehicleType getVehicleType(String nvic) throws RemoteException;

    public VehicleType getVehicleType(GlassMTSPK pk) throws RemoteException;

    public VehicleData getModels(String make, int year) throws RemoteException;

    public VehicleData getSeries(GlassMTSPK pk) throws RemoteException;

}