package com.ract.vehicle;

/**
 * Class containing static methods for obtaining home referencs to EJBs
 *
 * @author David Kettlewell
 * @version 1.0
 *
 * Usage: GlassMakeHome rt = GlassEJBHelper.getGlassMakeHome()
 */

import java.rmi.RemoteException;

import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

public class GlassEJBHelper {

    public static GlassMgr getGlassMgr() throws RemoteException {
	GlassMgrHome glassMgrHome = null;
	GlassMgr glassMgr = null;
	try {
	    // Get naming context
	    InitialContext ctx = new InitialContext();

	    // Look up jndi name
	    Object ref = ctx.lookup("GlassMgr");

	    // Cast to home interface
	    glassMgrHome = (GlassMgrHome) PortableRemoteObject.narrow(ref, GlassMgrHome.class);
	    // now get remote interface
	    glassMgr = glassMgrHome.create();
	} catch (Exception e) {
	    throw new RemoteException("Failed to initialise access to GlassMgr home interface : " + e.getMessage(), e);
	}
	return glassMgr;

    }

}