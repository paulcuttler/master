package com.ract.vehicle.ui;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ract.common.ApplicationServlet;
import com.ract.vehicle.GlassEJBHelper;
import com.ract.vehicle.GlassMTSPK;
import com.ract.vehicle.GlassMgr;
import com.ract.vehicle.VehicleData;
import com.ract.vehicle.VehicleMakeList;
import com.ract.vehicle.VehicleType;

/**
 * @todo Description of the Class
 * 
 * @author David Kettlewell
 * @created 1 August 2002
 */
public class VehicleUIC extends ApplicationServlet {
    private final static String CONTENT_TYPE = "text/html";

    private final static String DIRECT_TO = "/VehicleUIC";

    // HttpSession session = null;

    /**
     * Initialize global variables
     * 
     * @exception ServletException
     *                Description of the Exception
     */
    public void init() throws ServletException {

    }

    /**
     * Process the HTTP Get request
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// response.setContentType(CONTENT_TYPE);
	doPost(request, response);
    }

    /**
     * Process the HTTP Post request
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType(CONTENT_TYPE);
	HttpSession session = request.getSession();
	String riskTypeName = null;
	String event = request.getParameter("event");
	log("VehicleUIC event: " + event);
	// now run the handler with name methodName
	// must take request and response as parameters
	// If the method is not found inside this class, handleTheUnknown is run

	handleEvent(event, request, response);
    }

    /**
     * Retrieve keyList from the session. Usage ArrayList keyList =
     * getKeyList(request);
     */
    private ArrayList getKeyList(HttpServletRequest request) {
	ArrayList keyList = null;
	HttpSession session = request.getSession();
	if (session.getAttribute("keyList") != null) {
	    keyList = (ArrayList) session.getAttribute("keyList");
	}
	return keyList;
    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_Menu_Test(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	handleEvent_AskQuestion(request, response);
    }

    /**
     * ask questions relating to glasses guide as they pertain to a client.
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_AskQuestion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	request.setAttribute("mainHeading", "Motor Vehicle Selection");
	request.setAttribute("directTo", DIRECT_TO);
	request.setAttribute("context", "MotorVehicle_SelectYear");
	request.setAttribute("returnTo", request.getAttribute("returnTo"));
	request.setAttribute("returnEvent", request.getAttribute("returnEvent"));
	forwardRequest(request, response, "/common/VehicleYear.jsp");
    }

    /**
     * move to the next page from the car year page
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_MotorVehicleYear_Next(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	request.setAttribute("mainHeading", "Motor Vehicle Selection");
	request.setAttribute("directTo", DIRECT_TO);
	request.setAttribute("context", "MotorVehicle_SelectMake");
	request.setAttribute("returnTo", request.getParameter("returnTo"));
	request.setAttribute("returnEvent", request.getParameter("returnEvent"));

	String jspName = null;
	String year = (String) request.getParameter("year");
	if (year == null) {
	    throw new ServletException("Failed to get year parameter from page.");
	}

	year = year.trim();
	log("in motor vehicle next");

	request.setAttribute("generalText", "Select the make of the vehicle. (Year: " + year + ")");

	if (!year.equals("")) {

	    session.setAttribute("year", year);
	    ArrayList list = getMakes(Integer.parseInt(year), request);
	    if (list != null) {
		log("list size - " + list.size());
	    }
	    request.setAttribute("itemList", list);
	    jspName = "/common/SelectionList.jsp";
	    forwardRequest(request, response, jspName);
	} else {
	    String returnTo = request.getParameter("returnTo");
	    String returnEvent = request.getParameter("returnEvent");
	    // return to calling thing
	    log("in else");
	    forwardRequest(request, response, returnTo + "?event=" + returnEvent);
	}
    }

    /**
     * move to the next page from the make page
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_MotorVehicle_SelectMake_next(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	String context = request.getParameter("context");
	int itemIndex = Integer.parseInt(request.getParameter("itemIndex"));
	ArrayList keyList = getKeyList(request);
	String make = (String) keyList.get(itemIndex);
	int year = Integer.parseInt((String) session.getAttribute("year"));
	// session.removeAttribute("year");
	String makeDesc = request.getParameter("itemSelected");
	request.setAttribute("returnTo", request.getParameter("returnTo"));
	request.setAttribute("returnEvent", request.getParameter("returnEvent"));

	if (itemIndex == 0) {
	    String returnTo = request.getParameter("returnTo");
	    String returnEvent = request.getParameter("returnEvent");
	    VehicleType veh = new VehicleType();
	    veh.setMake("Unlisted vehicle");
	    request.setAttribute("Vehicle", veh);
	    forwardRequest(request, response, returnTo + "?event=" + returnEvent);
	} else {
	    request.setAttribute("itemList", getModels(make, year, request));
	    request.setAttribute("context", "MotorVehicle_SelectModel");
	    request.setAttribute("mainHeading", "Motor Vehicle Selection");
	    String headingText = "Select the model of the vehicle. (" + year + " " + makeDesc + ")";
	    request.setAttribute("generalText", headingText);
	    request.setAttribute("directTo", DIRECT_TO);
	    session.setAttribute("make", make);
	    forwardRequest(request, response, "/common/SelectionList.jsp");
	}
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_MotorVehicle_SelectModel_next(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String context = request.getParameter("context");
	int itemIndex = Integer.parseInt(request.getParameter("itemIndex"));
	ArrayList keyList = getKeyList(request);
	GlassMTSPK pk = (GlassMTSPK) keyList.get(itemIndex);
	GlassMgr hlp = GlassEJBHelper.getGlassMgr();
	VehicleType veh = hlp.getVehicleType(pk);
	request.setAttribute("Vehicle", veh);
	String returnTo = request.getParameter("returnTo");
	String returnEvent = request.getParameter("returnEvent");

	log("item number: " + itemIndex);
	// return to calling thing
	forwardRequest(request, response, returnTo + "?event=" + returnEvent);
    }

    /**
     * Gets the years attribute of the VehicleUIC object
     * 
     * @return The years value
     */
    private ArrayList getYears() {
	ArrayList years = new ArrayList();

	return years;
    }

    /**
     * get car makes
     * 
     * @return The makes value
     */
    private ArrayList getMakes(int year, HttpServletRequest request) {
	HttpSession session = request.getSession();
	ArrayList makes = new ArrayList();
	ArrayList keyList = new ArrayList();
	try {
	    GlassMgr glassHlp = GlassEJBHelper.getGlassMgr();
	    // GlassHlp glassHlp = gHome.create();
	    log("makelist");

	    VehicleMakeList makeList = glassHlp.getGlassMakes(year);

	    log("makelist loaded");
	    if (makeList != null) {
		log("make list found");
	    } else {
		log("make list is null");
	    }

	    for (int x = 0; x < makeList.size(); x++) {
		if (makeList.getMake(x).equals("")) {
		    makes.add("Unlisted vehicle");
		} else {
		    makes.add(makeList.getDesc(x));
		}
		keyList.add(makeList.getMake(x));
	    }

	} catch (Exception e) {
	    log("Exception getting makes. " + e);
	    e.printStackTrace();
	    return null;
	}
	if (makes != null) {
	    log("" + makes.size());
	}
	session.setAttribute("keyList", keyList);
	return makes;
    }

    /**
     * get car models
     * 
     * @param make
     *            Description of the Parameter
     * @param year
     *            Description of the Parameter
     * @return The models value
     */
    private ArrayList getModels(String make, int year, HttpServletRequest request) {
	HttpSession session = request.getSession();
	ArrayList models = new ArrayList();
	ArrayList keyList = new ArrayList();
	try {
	    // GlassMTSHome gmtsh = GlassEJBHelper.getGlassMTSHome();
	    // GlassMTS gmts = null;
	    // Collection col = gmtsh.findByMakeYear(make, year);
	    // Iterator it = col.iterator();
	    // while (it.hasNext())
	    // {
	    // gmts = (GlassMTS) PortableRemoteObject.narrow(it.next(),
	    // GlassMTS.class);
	    // models.add(gmts.getMts_desc());
	    // keyList.add(gmts.getPrimaryKey());
	    // }
	    GlassMgr glassMgr = GlassEJBHelper.getGlassMgr();
	    VehicleData vehicleData = glassMgr.getModels(make, year);
	    models = vehicleData.dataList;
	    keyList = vehicleData.keyList;
	} catch (Exception e) {
	    log("Exception getting makes. " + e);
	    e.printStackTrace();
	    return null;
	}
	session.setAttribute("keyList", keyList);
	if (models != null) {
	    log("size of models is " + models.size());
	}
	return models;
    }

    /**
     * move back a page from car make page
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_MotorVehicle_SelectMake_Back(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	request.setAttribute("mainHeading", "Motor Vehicle Selection");
	request.setAttribute("directTo", DIRECT_TO);
	request.setAttribute("context", "MotorVehicle_SelectYear");
	request.setAttribute("returnTo", request.getParameter("returnTo"));
	request.setAttribute("returnEvent", request.getParameter("returnEvent"));

	forwardRequest(request, response, "/common/VehicleYear.jsp");
    }

    /**
     * move back a page from car model page
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_MotorVehicle_SelectModel_Back(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	request.setAttribute("mainHeading", "Motor Vehicle Selection");
	request.setAttribute("directTo", DIRECT_TO);
	request.setAttribute("context", "MotorVehicle_SelectMake");
	String jspName = "/common/SelectionList.jsp";
	String year = (String) session.getAttribute("year");
	ArrayList list = getMakes(Integer.parseInt(year), request);
	request.setAttribute("itemList", list);
	request.setAttribute("returnTo", request.getParameter("returnTo"));
	request.setAttribute("returnEvent", request.getParameter("returnEvent"));
	forwardRequest(request, response, jspName);
    }

    /**
     * get vehicle description
     * 
     * @param nvic
     *            Description of the Parameter
     * @return The description value
     */
    public static String getDescription(String nvic) throws RemoteException {
	String description = null;
	// try
	// {
	GlassMgr hlp = GlassEJBHelper.getGlassMgr();
	description = hlp.getVehicleDescription(nvic);
	// }
	// catch (Exception e)
	// {
	// System.out.println(e);
	// }
	return description;
    }
}
