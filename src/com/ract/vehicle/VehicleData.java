package com.ract.vehicle;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Holds vehicle data as two related lists.
 * 
 * @version 1.0
 */

public class VehicleData implements Serializable {
    public ArrayList keyList = new ArrayList();

    public ArrayList dataList = new ArrayList();

    public VehicleData() {
    }
}