package com.ract.vehicle;

/**
 * Object that represents a client vehicle
 *
 * @author David Kettlewell
 * @version 1.0
 */

import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ract.common.ValidationException;
import com.ract.common.ValueObject;

//import com.ract.insurance.motorvehicle.VehicleType;
@Entity
@Table(name = "cl_vehicle")
public class VehicleVO extends ValueObject {

    @Column(name = "client_no")
    protected Integer clientNo = null;

    @Id
    @Column(name = "vehicle_id")
    protected Integer vehicleId = null;

    @Column(name = "veh_year")
    protected Integer vehYear = null;

    protected String make = null;

    protected String model = null;

    protected String colour = null;

    @Column(name = "reg_no")
    protected String regNo = null;

    protected String nvic = null;

    @Transient
    protected String description = null;

    public VehicleVO() {
    }

    public void setVehicleID(Integer vehicleId) {
	this.vehicleId = vehicleId;
    }

    public Integer getVehicleID() {
	return this.vehicleId;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    public Integer getClientNo() {
	return this.clientNo;
    }

    public void setVehYear(Integer vehYear) {
	this.vehYear = vehYear;
    }

    public Integer getVehYear() {
	return this.vehYear;
    }

    public void setMake(String make) {
	this.make = make;
    }

    public String getMake() {
	return this.make;
    }

    public void setModel(String model) {
	this.model = model;
    }

    public String getModel() {
	return this.model;
    }

    public void setColour(String colour) {
	this.colour = colour;
    }

    public void setRegNo(String regNo) {
	this.regNo = regNo;
    }

    public String getRegNo() {
	return this.regNo;
    }

    public void setNVIC(String nvic) {
	this.nvic = nvic;
    }

    public String getNVIC() {
	return this.nvic;
    }

    @Transient
    public boolean isPrimaryVehicle() {
	return true;
    }

    public String getDescription() {
	// if(this.description==null){
	return getRealVehicleYear() + "  " + this.make + "  " + this.model;
    }

    public VehicleVO(VehicleType vehicle) {
	this.make = vehicle.getMake();
	this.model = vehicle.getSeries();
	this.description = vehicle.getDescription();
	this.vehYear = vehicle.getYear();
	this.nvic = vehicle.getNvic();
    }

    protected void validateMode(String newMode) throws ValidationException {
    }

    private String getRealVehicleYear() {

	int theYear = this.vehYear.intValue();
	DecimalFormat nf = new DecimalFormat("00");
	String lastDigits = nf.format(this.vehYear);

	// vehYear null?
	final String NINETEEN_PREFIX = "19";
	final String TWENTY_PREFIX = "20";
	final int PIVOT_DATE = 50;
	final int END_PIVOT_DATE = 99;

	String realVehicleYear = "";

	if (theYear > PIVOT_DATE && theYear <= END_PIVOT_DATE) {
	    realVehicleYear = NINETEEN_PREFIX + lastDigits;
	} else {
	    realVehicleYear = TWENTY_PREFIX + lastDigits;
	}
	return realVehicleYear;
    }

    public String toString() {
	StringBuffer sb = new StringBuffer();
	sb.append(this.getVehicleID());
	sb.append(" ");
	sb.append(this.getClientNo());
	sb.append(" ");
	sb.append(this.getMake());
	sb.append(" ");
	sb.append(this.getModel());
	sb.append(" ");
	sb.append(this.getVehYear());
	return sb.toString();
    }

}
