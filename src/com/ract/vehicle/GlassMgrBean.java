package com.ract.vehicle;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Vector;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.sql.DataSource;

import com.ract.common.CommonConstants;
import com.ract.common.DataSourceFactory;

/**
 * Manager bean for glasses guide.
 * 
 * @author dgk
 * @version 1.0
 */

public class GlassMgrBean implements SessionBean {
    private SessionContext sessionContext;

    private DataSource dataSource = null;

    public void ejbCreate() {
    }

    public void ejbRemove() throws RemoteException {
    }

    public void ejbActivate() throws RemoteException {
    }

    public void ejbPassivate() throws RemoteException {
    }

    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	} catch (Exception e) {
	    throw new EJBException("Error initializing context:" + e.toString());
	}
    }

    public VehicleMakeList getGlassMakes() throws RemoteException {
	String sql = "select \"make\", \"make-desc\" from PUB.\"ins-gl-make\"";
	Connection connection = null;
	PreparedStatement statement = null;
	VehicleMakeList theList = new VehicleMakeList();
	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    statement = connection.prepareStatement(sql);
	    ResultSet resultSet = statement.executeQuery();
	    while (resultSet.next()) {
		String make = resultSet.getString(1);
		theList.add(make, resultSet.getString(2));
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing " + sql + "\n" + e.toString());
	} catch (Exception ex) {
	    throw new EJBException("error" + ex);
	} finally {
	    closeConnection(connection, statement);
	}
	return theList;
    }

    void closeConnection(Connection connection, Statement statement) {
	try {
	    if (statement != null) {
		statement.close();
	    }
	} catch (SQLException e) {
	}
	try {
	    if (connection != null) {
		connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		connection.close();
	    }
	} catch (SQLException e) {
	}
    }

    public String getVehicleDescription(String nvic) throws RemoteException {
	String vehDescription = "";
	boolean source = nvic.substring(0, 1).equals("R");
	String vic = nvic.substring(2, nvic.length());
	String sql = "select a.\"veh-year\" ,\"make-desc\", e.\"mts-desc\"," + "\"trans-desc\",\"body-desc\" " + " from PUB.\"ins-gl-main\" a,PUB.\"ins-gl-make\" b, " + " PUB.\"ins-gl-body\" c,PUB.\"ins-gl-trans\" d," + " PUB.\"ins-gl-mts\" e " + " where nvic = ?" + " and source = ?" + " and b.make = a.make" + " and d.\"trans-type\" = a.\"trans-type\"" + " and c.\"body\" = a.\"body\"" + " and e.make = a.make" + " and e.model = a.model" + " and e.\"type\" = a.\"type\"" + " and e.series = a.series" + " and e.\"veh-year\"=a.\"veh-year\"";
	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, vic);
	    statement.setBoolean(2, source);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		int year = resultSet.getInt(1);
		String make = resultSet.getString(2);
		String series = resultSet.getString(3);
		String transmission = resultSet.getString(4);
		String body = resultSet.getString(5);
		vehDescription = year + " " + make + " " + series + " " + transmission + " " + body;
	    } else {
		throw new EJBException("Error retrieving vehicle details from Glass's guide");
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing " + sql + "\n" + e.toString());
	} finally {
	    closeConnection(connection, statement);
	}
	return vehDescription;
    }

    public VehicleMakeList getGlassMakes(int year) throws RemoteException {
	String sql = "select make, \"make-desc\" " + " from PUB.\"ins-gl-make\"" + " where make in" + " (select distinct make from PUB.\"ins-gl-mts\"" + " where \"veh-year\" = ?)";
	Connection connection = null;
	PreparedStatement statement = null;
	VehicleMakeList theList = new VehicleMakeList();
	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    statement = connection.prepareStatement(sql);
	    statement.setInt(1, year);
	    ResultSet resultSet = statement.executeQuery();
	    while (resultSet.next()) {
		theList.add(resultSet.getString(1), resultSet.getString(2));
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing " + sql + "\n" + e.toString());
	} finally {
	    closeConnection(connection, statement);
	}
	return theList;
    }

    /**
     * Retrieve a vehicle description given the nvic. Results are stored in a
     * ClientVehicle object
     */
    public VehicleType getVehicleType(String nvic) throws RemoteException {
	VehicleType veh = null;
	boolean source = nvic.substring(0, 1).equals("R");
	String vic = nvic.substring(2, nvic.length());
	String sql = "select a.\"veh-year\" ,\"make-desc\", e.\"mts-desc\"," + "\"trans-desc\",\"body-desc\", \"engine-desc\"" + " from PUB.\"ins-gl-main\" a,PUB.\"ins-gl-make\" b, " + " PUB.\"ins-gl-body\" c,PUB.\"ins-gl-trans\" d," + " PUB.\"ins-gl-mts\" e, PUB.\"ins-gl-engine\" f " + " where nvic = ?" + " and source = ?" + " and b.make = a.make" + " and d.\"trans-type\" = a.\"trans-type\"" + " and c.\"body\" = a.\"body\"" + " and e.make = a.make" + " and e.model = a.model" + " and e.\"type\" = a.\"type\"" + " and e.series = a.series" + " and e.\"veh-year\"=a.\"veh-year\"" + " and f.engine = a.engine";
	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, vic);
	    statement.setBoolean(2, source);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		veh = new VehicleType();
		veh.setYear(new Integer(resultSet.getInt(1)));
		veh.setMake(resultSet.getString(2));
		veh.setSeries(resultSet.getString(3));
		veh.setTransmission(resultSet.getString(4));
		veh.setBody(resultSet.getString(5));
		veh.setDescription(veh.getYear() + " " + veh.getMake() + " " + veh.getSeries() + " " + veh.getTransmission() + " " + veh.getBody());
		veh.setEngine(resultSet.getString(6));
	    } else {
		throw new EJBException("Error retrieving vehicle details from Glass's guide");
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing " + sql + "\n" + e.toString());
	} finally {
	    closeConnection(connection, statement);
	}
	return veh;
    }

    public VehicleType getVehicleType(GlassMTSPK pk) throws RemoteException {
	VehicleType veh = null;
	String sql = "select \"make-desc\", a.\"mts-desc\"" + " from PUB.\"ins-gl-mts\" a,PUB.\"ins-gl-make\" b " + " where a.make = ?" + " and a.model = ?" + " and a.\"type\" = ?" + " and a.series = ?" + " and a.\"veh-year\"=?" + " and b.make = a.make";
	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, pk.make);
	    statement.setString(2, pk.model);
	    statement.setString(3, pk.type);
	    statement.setString(4, pk.series);
	    statement.setInt(5, pk.veh_year.intValue());
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		veh = new VehicleType();
		veh.setYear(pk.veh_year);
		veh.setMake(resultSet.getString(1));
		veh.setSeries(resultSet.getString(2));
		veh.setModel(pk.model);
		veh.setDescription(pk.veh_year + " " + veh.getMake() + " " + veh.getSeries());
	    } else {
		throw new EJBException("Error retrieving vehicle details from Glass's guide");
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing " + sql + "\n" + e.toString());
	} finally {
	    closeConnection(connection, statement);
	}
	return veh;
    }

    /**
     * Extract from the database lists of data refering to vehicles of a
     * particular make and year
     * 
     * @param String
     *            make The make (manufacturer) of the vehicle
     * @param int year The year of manufacture of the vehicle
     */
    public VehicleData getModels(String make, int year) throws RemoteException {
	Connection connection = null;
	PreparedStatement statement = null;
	VehicleData vData = new VehicleData();

	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    statement = connection.prepareStatement("SELECT \"model\", \"type\", \"series\",\"mts-desc\" FROM PUB.\"ins-gl-mts\" WHERE \"make\"=? and \"veh-year\"=?");
	    if (make.equals("RACT")) {
		statement.setNull(1, Types.VARCHAR);
	    } else {
		statement.setString(1, make);
	    }
	    statement.setInt(2, year);
	    ResultSet resultSet = statement.executeQuery();
	    Vector keys = new Vector();
	    while (resultSet.next()) {
		String model = resultSet.getString(1);
		String type = resultSet.getString(2);
		String series = resultSet.getString(3);
		String description = resultSet.getString(4);
		vData.keyList.add(new GlassMTSPK(model, type, series, make, new Integer(year)));
		vData.dataList.add(description);
	    }
	    return vData;
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL SELECT \"model\", \"type\", \"series\" FROM PUB.\"ins-gl-mts\" WHERE \"make\"=? and \"veh-year\"=?: " + e.toString());
	} finally {
	    closeConnection(connection, statement);
	}
    }

    public VehicleData getSeries(GlassMTSPK mtsPK) throws RemoteException {
	VehicleData vData = new VehicleData();
	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    statement = connection.prepareStatement("SELECT \"nvic\", \"source\" FROM PUB.\"ins-gl-main\" WHERE \"make\" = ? and \"veh-year\" = ? and \"model\" = ? and \"type\" = ? and \"series\" = ?");
	    statement.setString(1, mtsPK.make);
	    statement.setInt(2, mtsPK.veh_year.intValue());
	    statement.setString(3, mtsPK.model);
	    if (mtsPK.type == null) {
		statement.setNull(4, Types.VARCHAR);
	    } else {
		statement.setString(4, mtsPK.type);
	    }
	    if (mtsPK.series == null) {
		statement.setNull(5, Types.VARCHAR);
	    } else {
		statement.setString(5, mtsPK.series);
	    }
	    ResultSet resultSet = statement.executeQuery();
	    Vector keys = new Vector();
	    while (resultSet.next()) {
		String nvic = resultSet.getString(1);
		Boolean source = new Boolean(resultSet.getBoolean(2));
		vData.keyList.add(new GlassMainPK(nvic, source));
		vData.dataList.add(getVehicleDescription(nvic, source));
	    }
	    ;
	} catch (SQLException e) {
	    throw new EJBException("\n\nError executing SQL SELECT \"nvic\", \"source\" FROM PUB.\"ins-gl-main\": " + "\nMake =   " + mtsPK.make + "\nModel =  " + mtsPK.model + "\nSeries = " + mtsPK.series + "\nYear =   " + mtsPK.veh_year + "\nType =   " + mtsPK.type + "\n" + e.toString());
	} finally {
	    closeConnection(connection, statement);
	}
	return vData;
    }

    public String getVehicleDescription(String nvic, Boolean source) throws RemoteException {
	Connection connection = null;
	PreparedStatement statement = null;
	String description = null;
	try {
	    connection = dataSource.getConnection();
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
	    StringBuffer sql = new StringBuffer();
	    sql.append("\n select cyl,\"engine-size\",points,cost,\"body-desc\",\"trans-desc\",\"engine-desc\"");
	    sql.append("\n from PUB.\"ins-gl-main\" a, PUB.\"ins-gl-engine\" b,PUB.\"ins-gl-body\" c,PUB.\"ins-gl-trans\" d ");
	    sql.append("\n where a.engine = b.engine");
	    sql.append("\n and a.body = c.body");
	    sql.append("\n and a.\"trans-type\" = d.\"trans-type\"");
	    sql.append("\n and a.nvic =?");
	    sql.append("\n and a.source =?");

	    statement = connection.prepareStatement(sql.toString());
	    statement.setString(1, nvic);
	    statement.setBoolean(2, source.booleanValue());
	    ResultSet rs = statement.executeQuery();
	    if (rs.next()) {
		description = rs.getString(1) + " Cyl &nbsp&nbsp" + "  " + rs.getInt(2) + " CC &nbsp&nbsp" + "  " + rs.getString(5) + "&nbsp&nbsp" + "  " + rs.getString(6) + "&nbsp&nbsp" + "  " + rs.getString(7) + "&nbsp&nbsp" + "  (" + rs.getInt(3) + " points)&nbsp&nbsp" + "  $" + rs.getFloat(4);
	    }
	} catch (Exception e) {
	    throw new RemoteException("Error getting vehicle description in GlassMgrBean.getVehicleDescription\n" + e);
	} finally {
	    closeConnection(connection, statement);
	}
	return description;
    }

}
