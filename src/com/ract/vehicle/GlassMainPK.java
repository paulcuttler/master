package com.ract.vehicle;

import java.io.Serializable;

/**
 * Title: Description: Copyright: Copyright (c) 2002 Company: RACT Ltd
 * 
 * @author
 * @version 1.0
 */

public class GlassMainPK implements Serializable {

    public String nvic;

    public Boolean source;

    public GlassMainPK() {
    }

    public GlassMainPK(String nvic, Boolean source) {
	this.nvic = nvic;
	this.source = source;
    }

    public boolean equals(Object obj) {
	if (this.getClass().equals(obj.getClass())) {
	    GlassMainPK that = (GlassMainPK) obj;
	    return this.nvic.equals(that.nvic) && this.source.equals(that.source);
	}
	return false;
    }

    public int hashCode() {
	return (nvic + source).hashCode();
    }
}