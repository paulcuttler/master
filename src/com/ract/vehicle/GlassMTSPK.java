package com.ract.vehicle;

import java.io.Serializable;

/**
 * Title: Description: Copyright: Copyright (c) 2002 Company: RACT Ltd
 * 
 * @author
 * @version 1.0
 */

public class GlassMTSPK implements Serializable {

    public String model;

    public String type;

    public String series;

    public String make;

    public Integer veh_year;

    public GlassMTSPK() {
    }

    public GlassMTSPK(String model, String type, String series, String make, Integer veh_year) {
	this.model = model;
	this.type = type;
	this.series = series;
	this.make = make;
	this.veh_year = veh_year;
    }

    public boolean equals(Object obj) {
	if (this.getClass().equals(obj.getClass())) {
	    GlassMTSPK that = (GlassMTSPK) obj;
	    return this.model.equals(that.model) && this.type.equals(that.type) && this.series.equals(that.series) && this.make.equals(that.make) && this.veh_year.equals(that.veh_year);
	}
	return false;
    }

    public int hashCode() {
	return (model + type + series + make + veh_year).hashCode();
    }
}