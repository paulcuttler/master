package com.ract.vehicle;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

/**
 * Title: Description: Copyright: Copyright (c) 2002 Company: RACT Ltd
 * 
 * @author
 * @version 1.0
 */

public interface GlassMgrHome extends EJBHome {
    public GlassMgr create() throws RemoteException, CreateException;
}