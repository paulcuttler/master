package com.ract.vehicle;

import java.io.Serializable;

/**
 * Vehicle type object
 * 
 * @author dgk
 * @version 1.0
 */

public class VehicleType implements Serializable {
    private String make = null;

    private String model = null;

    private Integer year = null;

    private String series = null;

    private String transmission = null;

    private String body = null;

    private String engine = null;

    private String description = null;

    private String nvic = null;

    public VehicleType() {
    }

    public VehicleType(String nvic) {
	this.nvic = nvic;
    }

    public VehicleType(String make, String Model, Integer year) {
	this.make = make;
	this.model = model;
	this.year = year;
    }

    public void setMake(String make) {
	this.make = make;
    }

    public void setModel(String model) {
	this.model = model;
    }

    public void setYear(Integer year) {
	this.year = year;
    }

    public void setSeries(String series) {
	this.series = series;
    }

    public void setBody(String body) {
	this.body = body;
    }

    public void setTransmission(String transmission) {
	this.transmission = transmission;
    }

    public void setEngine(String engine) {
	this.engine = engine;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getSeries() {
	return this.series;
    }

    public String getTransmission() {
	return this.transmission;
    }

    public String getBody() {
	return this.body;
    }

    public String getEngine() {
	return this.engine;
    }

    public String getDescription() {
	return this.description;
    }

    public String getMake() {
	return make;
    }

    public String getModel() {
	return model;
    }

    public Integer getYear() {
	return year;
    }

    public String getNvic() {
	return nvic;
    }

}