package com.ract.fleet;

import java.io.IOException;

import com.progress.open4gl.ConnectException;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;
import com.ract.common.CommonConstants;
import com.ract.common.proxy.ProgressProxy;

public class FleetProgressProxy extends FleetTxProxy implements ProgressProxy {
    public FleetProgressProxy() throws ConnectException, Open4GLException, SystemErrorException, IOException {
	// connection specific?
	super(CommonConstants.getProgressAppServerURL(), CommonConstants.getProgressAppserverUser(), CommonConstants.getProgressAppserverPassword(), null);
    }

}
