package com.ract.fleet;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;

/**
 * 
 * <p>
 * Connection interface to fleet membership subsystem
 * </p>
 * 
 * @dgk
 * @version 1.0 21/8/2003
 */
public interface FleetAdapter extends TransactionAdapter {
    /**
     * Commit any pending transactions in the finance sub system. The connection
     * to the finance sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void commit() throws SystemException;

    /**
     * Rollback any pending transactions in the finance sub system. The
     * connection to the finance sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws SystemException;

    /**
     * Release any connections held to the finance sub-system. If a transaction
     * is pending and not committed then the transaction will be rolled back.
     */
    public void release();

    public ArrayList getMAPList(Integer clientNumber) throws SystemException;

    public ArrayList getNVIList(Integer clientNumber) throws SystemException;

    /**
     * Notifies the insurance system of the payment of a premium
     * 
     * @param Integer
     *            receiptNo
     * @param Integer
     *            clientNo ClientNo and sequence no provide the key into the
     *            insurance payable amount records (oi-payable)
     * @param Integer
     *            seqNo
     * @param BigDecimal
     *            amount
     * @param String
     *            logonId
     * @param String
     *            salesBranch
     * @throws SystemException
     */
    public void notifyFleetPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch) throws SystemException;

}
