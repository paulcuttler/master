package com.ract.fleet;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.util.LogUtil;

public class ProgressFleetAdapter extends TransactionAdapterType implements FleetAdapter {

    private NviTXProxy fleetTransactionProxy = null;

    public ProgressFleetAdapter() {
    }

    public void commit() throws SystemException {
	if (this.fleetTransactionProxy != null) {
	    try {

		this.fleetTransactionProxy.commit();
	    } catch (Open4GLException gle) {
		throw new SystemException(gle);
	    } finally {
		release();
	    }
	}
    }

    public void rollback() throws SystemException {
	if (this.fleetTransactionProxy != null) {
	    try {
		this.fleetTransactionProxy.rollback();

	    } catch (Open4GLException gle) {
		throw new SystemException(gle);
	    } finally {
		release();
	    }
	}
    }

    public synchronized void release() {
	releaseFleetTransactionProxy();
    }

    private void releaseFleetTransactionProxy() {
	if (this.fleetTransactionProxy != null) {
	    try {
		this.fleetTransactionProxy._release();
	    } catch (Open4GLException gle) {
		LogUtil.fatal(this.getClass(), gle.getMessage());
	    } finally {
		this.fleetTransactionProxy = null;
	    }
	}
	releaseFleetProgressProxy(this.transactionFleetProgressProxy);
	this.transactionFleetProgressProxy = null;
    }

    public void notifyFleetPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch) throws SystemException {
	try {
	    this.fleetTransactionProxy = this.getFleetTransactionProxy();
	    this.fleetTransactionProxy.notifyPayment(receiptNo.intValue(), clientNo.intValue(), seqNo.intValue(), amount, logonId, salesBranch);
	    handleProxyReturnString();
	} catch (Open4GLException ex) {
	    handleProxyReturnString();
	    throw new SystemException("Unable to notify a Fleet payment.", ex);
	}

    }

    public ArrayList getNVIList(Integer clientNumber) throws SystemException {
	ArrayList nviList = new ArrayList();
	ResultSetHolder nviResultSetHolder = new ResultSetHolder();
	String nviId = null;
	FleetProgressProxy fleetProgressProxy = null;
	try {
	    fleetProgressProxy = this.getFleetProgressProxy();
	    fleetProgressProxy.getNVIList(clientNumber.intValue(), nviResultSetHolder);

	    if (nviResultSetHolder != null) {
		try {
		    ResultSet nviResultSet = nviResultSetHolder.getResultSetValue();
		    while (nviResultSet.next()) {
			// get nextt
			nviId = nviResultSet.getString(1);
			nviList.add(nviId);
		    }
		} catch (SQLException ex1) {
		    throw new SystemException(ex1);
		}
	    }
	} catch (Open4GLException ex) {
	    throw new SystemException("Unable to get VI list for client '" + clientNumber + "'.", ex);
	} finally {
	    releaseFleetProgressProxy(fleetProgressProxy);
	}
	return nviList;
    }

    /**
     * 
     * @param clientNumber
     *            Integer
     * @throws SystemException
     * @return ArrayList
     */
    public ArrayList getMAPList(Integer clientNumber) throws SystemException {
	ArrayList mapList = new ArrayList();
	ResultSetHolder mapResultSetHolder = new ResultSetHolder();
	String mapId = null;
	FleetProgressProxy fleetProgressProxy = null;
	try {
	    fleetProgressProxy = this.getFleetProgressProxy();
	    fleetProgressProxy.getMAPList(clientNumber.intValue(), mapResultSetHolder);

	    if (mapResultSetHolder != null) {
		try {
		    ResultSet mapResultSet = mapResultSetHolder.getResultSetValue();
		    while (mapResultSet.next()) {
			// get nextt
			mapId = mapResultSet.getString(1);
			mapList.add(mapId);
		    }
		} catch (SQLException ex1) {
		    throw new SystemException(ex1);
		}
	    }
	} catch (Open4GLException ex) {
	    throw new SystemException("Unable to get VI list for client '" + clientNumber + "'.", ex);
	} finally {
	    releaseFleetProgressProxy(fleetProgressProxy);
	}
	return mapList;
    }

    private void handleProxyReturnString() throws SystemException {
	try {
	    if (fleetTransactionProxy != null) {
		String returnString = fleetTransactionProxy._getProcReturnString();
		LogUtil.debug(this.getClass(), "returnString=" + returnString);
		if (returnString != null && returnString.length() > 0) {
		    throw new SystemException(returnString);
		}
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	}
    }

    private FleetProgressProxy getFleetProgressProxy() throws SystemException {
	FleetProgressProxy fleetProgressProxy = null;
	try {
	    fleetProgressProxy = (FleetProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_FLEET_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return fleetProgressProxy;
    }

    private FleetProgressProxy transactionFleetProgressProxy = null;

    private NviTXProxy getFleetTransactionProxy() throws SystemException {
	if (this.fleetTransactionProxy == null) {
	    transactionFleetProgressProxy = this.getFleetProgressProxy();
	    try {
		this.fleetTransactionProxy = transactionFleetProgressProxy.createPO_NviTXProxy();
	    } catch (Open4GLException ex) {
		throw new SystemException("Unable to get InsuranceTxProxy: " + ex);
	    }
	}
	return this.fleetTransactionProxy;
    }

    private void releaseFleetProgressProxy(FleetProgressProxy fleetProgressProxy) {
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_FLEET_PROXY, fleetProgressProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), ex.getMessage());
	} finally {
	    fleetProgressProxy = null;
	}
    }

    public String getSystemName() {
	return SourceSystem.FLEET.getAbbreviation();
    }

}
