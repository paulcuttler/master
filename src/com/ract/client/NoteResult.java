package com.ract.client;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.common.Auditable;
import com.ract.util.DateTime;

@Entity
public class NoteResult implements Auditable {

    public boolean isRequiresValue() {
	return requiresValue;
    }

    public void setRequiresValue(boolean requiresValue) {
	this.requiresValue = requiresValue;
    }

    public String getLastUpdateId() {
	return lastUpdateId;
    }

    public void setLastUpdateId(String lastUpdateId) {
	this.lastUpdateId = lastUpdateId;
    }

    public String getResultCode() {
	return resultCode;
    }

    public void setResultCode(String resultCode) {
	this.resultCode = resultCode;
    }

    public String getNoteType() {
	return noteType;
    }

    public void setNoteType(String noteType) {
	this.noteType = noteType;
    }

    public String getResultText() {
	return resultText;
    }

    public void setResultText(String resultText) {
	this.resultText = resultText;
    }

    public DateTime getLastUpdate() {
	return lastUpdate;
    }

    public void setLastUpdate(DateTime lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    @Id
    private String resultCode;

    private String noteType;

    private String resultText;

    private boolean requiresValue;

    private DateTime lastUpdate;

    private String lastUpdateId;

}
