package com.ract.client;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.MailMgrLocal;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserMgrLocal;
import com.ract.user.UserSession;
import com.ract.util.DateTime;

public class CompleteNoteAction extends ActionSupport implements SessionAware, ValidationAware {

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    @Override
    public void validate() {

	if (resultCode == null) {
	    addFieldError("resultCode", "A result must be entered.");
	}

	if (completionNotes == null) {
	    addFieldError("completionNotes", "Completion notes must be entered.");
	}

	NoteResult noteResult = clientMgr.getNoteResult(resultCode);
	if (noteResult != null && noteResult.isRequiresValue()) {
	    if (resultValue == null) {
		addFieldError("resultValue", "A result value must be entered for result code " + resultCode);
	    }
	}
    }

    public BigDecimal getResultValue() {
	return resultValue;
    }

    public void setResultValue(BigDecimal resultValue) {
	this.resultValue = resultValue;
    }

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    public String getCompletionReferenceId() {
	return completionReferenceId;
    }

    public void setCompletionReferenceId(String completionReferenceId) {
	this.completionReferenceId = completionReferenceId;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    public String getCompletionNotes() {
	return completionNotes;
    }

    public void setCompletionNotes(String completionNotes) {
	this.completionNotes = completionNotes;
    }

    public String getResultCode() {
	return resultCode;
    }

    public void setResultCode(String resultCode) {
	this.resultCode = resultCode;
    }

    @InjectEJB(name = "ClientMgrBean")
    ClientMgrLocal clientMgr;

    private Integer noteId;

    private String completionNotes;

    @InjectEJB(name = "MailMgrBean")
    private MailMgrLocal mailMgr;

    private String completionReferenceId;

    private String resultCode;

    private Integer clientNumber;

    private BigDecimal resultValue;

    private ClientVO client;

    @InjectEJB(name = "UserMgrBean")
    private UserMgrLocal userMgr;

    private ClientNote clientNote;

    @Override
    public String execute() throws Exception {
	UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);

	clientNote = clientMgr.getClientNote(noteId);
	clientNote.setCompletionNotes(completionNotes);
	clientNote.setCompletionDate(new DateTime());
	clientNote.setCompletionReferenceId(completionReferenceId);
	clientNote.setResultCode(resultCode);
	clientNote.setCompletionId(us.getUserId());
	clientNote.setResultValue(resultValue);
	clientMgr.updateClientNote(clientNote);

	clientNumber = clientNote.getClientNumber();

	client = clientMgr.getClient(clientNote.getClientNumber());

	return SUCCESS;
    }

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private Map session = null;

}
