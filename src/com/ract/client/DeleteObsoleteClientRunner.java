package com.ract.client;

import java.rmi.RemoteException;

import com.ract.common.ExceptionHelper;
import com.ract.common.integration.RACTPropertiesProvider;

/**
 * Run delete obsolete clients.
 * 
 * @author jyh
 * @version 1.0
 */

public class DeleteObsoleteClientRunner {

    /**
     * @param args
     *            String[] host, port and file name with client numbers to
     *            delete.
     */
    public static void main(String[] args) {
	if (args.length != 3) {
	    System.out.println("Usage: DeleteObsoleteClientRunner localhost 1099 /tmp/client/delete.txt");
	    return;
	}

	String hostName = args[0];
	int portNumber = Integer.parseInt(args[1]);
	String fileName = args[2];
	System.out.println("fileName=" + fileName);
	RACTPropertiesProvider.setContextURL(hostName, portNumber);
	try {
	    ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    clientMgr.deleteObsoleteClientsAndBusiness(fileName);
	} catch (RemoteException ex) {
	    System.err.println(ExceptionHelper.getExceptionStackTrace(ex));
	    return;
	}

    }

}
