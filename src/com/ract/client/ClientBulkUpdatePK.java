package com.ract.client;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.ract.util.DateTime;

@Embeddable
public class ClientBulkUpdatePK implements Serializable {

    public static final String TYPE_ADDRESS_UNKNOWN = "Address Unknown";

    private Integer clientNumber; // 1234

    private DateTime createDate; // 1/1/2001 1:34:03.342

    private String updateType;

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((clientNumber == null) ? 0 : clientNumber.hashCode());
	result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
	result = prime * result + ((updateType == null) ? 0 : updateType.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof ClientBulkUpdatePK))
	    return false;
	ClientBulkUpdatePK other = (ClientBulkUpdatePK) obj;
	if (clientNumber == null) {
	    if (other.clientNumber != null)
		return false;
	} else if (!clientNumber.equals(other.clientNumber))
	    return false;
	if (createDate == null) {
	    if (other.createDate != null)
		return false;
	} else if (!createDate.equals(other.createDate))
	    return false;
	if (updateType == null) {
	    if (other.updateType != null)
		return false;
	} else if (!updateType.equals(other.updateType))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ClientBulkUpdatePK [clientNumber=" + clientNumber + ", createDate=" + createDate + ", updateType=" + updateType + "]";
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String getUpdateType() {
	return updateType;
    }

    public void setUpdateType(String updateType) {
	this.updateType = updateType;
    }

}
