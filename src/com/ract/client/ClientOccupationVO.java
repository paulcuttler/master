package com.ract.client;

import com.ract.common.CachedItem;
import com.ract.common.ValueObject;

/**
 * Client occupation value object
 * 
 * @hibernate.class table="[cl_occupation]" lazy="false"
 * 
 * @author John Holliday
 * @version 1.0
 */

public class ClientOccupationVO extends ValueObject implements CachedItem {

    protected Integer occupationID;

    protected String occupation;

    private String ascoCode;

    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this won't throw a null
	    // pointer exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    public String getKey() {
	return String.valueOf(this.getOccupationID());
    }

    public ClientOccupationVO copy() {
	ClientOccupationVO covo = new ClientOccupationVO();
	covo.setOccupationID(occupationID);
	covo.setOccupation(occupation);
	return covo;
    }

    public void setOccupationID(Integer occupationID) {
	this.occupationID = occupationID;
    }

    public ClientOccupationVO() {
    }

    public ClientOccupationVO(Integer occupationID, String occupation) {
	this.occupationID = occupationID;
	this.occupation = occupation;
    }

    /**
     * @hibernate.id column="occupation_id" generator-class="assigned"
     * @return Integer
     */
    public Integer getOccupationID() {
	return occupationID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="occupation"
     */
    public String getOccupation() {
	return occupation;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="asco_code"
     */
    public String getAscoCode() {
	return ascoCode;
    }

    public void setOccupation(String occupation) {
	this.occupation = occupation;
    }

    public void setAscoCode(String ascoCode) {
	this.ascoCode = ascoCode;
    }
}
