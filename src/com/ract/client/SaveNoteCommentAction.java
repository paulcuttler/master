package com.ract.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.DateTime;

public class SaveNoteCommentAction extends ActionSupport implements SessionAware, ValidationAware {

    public boolean isCanUnblockTransactions() {
	return canUnblockTransactions;
    }

    public void setCanUnblockTransactions(boolean canUnblockTransactions) {
	this.canUnblockTransactions = canUnblockTransactions;
    }

    public boolean isCanMarkComplete() {
	return canMarkComplete;
    }

    public void setCanMarkComplete(boolean canMarkComplete) {
	this.canMarkComplete = canMarkComplete;
    }

    public NoteCategory getNoteCategory() {
	return noteCategory;
    }

    public void setNoteCategory(NoteCategory noteCategory) {
	this.noteCategory = noteCategory;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    @Override
    public void validate() {
	if (comment == null || comment.length() == 0) {
	    addFieldError("comment", "A comment must be entered.");
	}
    }

    public NoteType getNoteType() {
	return noteType;
    }

    public void setNoteType(NoteType noteType) {
	this.noteType = noteType;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    private Integer noteId;

    private String comment;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private ClientVO client;

    private Map session = null;

    private Integer clientNumber;

    private boolean canUnblockTransactions;

    private boolean canMarkComplete;

    @Override
    public String execute() throws Exception {
	UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);

	int commentSeq = 0;

	clientNote = clientMgr.getClientNote(noteId);
	client = clientMgr.getClient(clientNote.getClientNumber());
	clientNumber = clientNote.getClientNumber();
	Collection<ClientNoteComment> commentList = clientNote.getClientNoteComments();
	if (commentList == null) {
	    commentSeq = 1;
	    commentList = new ArrayList<ClientNoteComment>();

	} else {
	    commentSeq = commentList.size() + 1;
	}

	ClientNoteCommentPK pk = new ClientNoteCommentPK();
	pk.setNoteId(noteId);
	pk.setCommentSeq(commentSeq);
	ClientNoteComment clientNoteComment = new ClientNoteComment();
	clientNoteComment.setClientCommentNotePK(pk);
	clientNoteComment.setComment(comment);
	clientNoteComment.setCreated(new DateTime());
	clientNoteComment.setCreateId(us.getUserId());

	commentList.add(clientNoteComment);
	clientNote.setClientNoteComments(commentList);

	clientMgr.updateClientNote(clientNote);

	noteType = clientMgr.getNoteType(clientNote.getNoteType());

	canMarkComplete = ClientHelper.canMarkComplete(noteType, clientNote, noteCategory, us);
	canUnblockTransactions = ClientHelper.canUnblockTransactions(noteType, clientNote, noteCategory, us);

	return SUCCESS;
    }

    private NoteCategory noteCategory;

    private ClientNote clientNote;

    private NoteType noteType;

}
