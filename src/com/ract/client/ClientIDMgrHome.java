package com.ract.client;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface ClientIDMgrHome extends EJBHome {
    public ClientIDMgr create() throws RemoteException, CreateException;
}