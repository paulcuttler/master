package com.ract.client;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.util.FileUtil;

public class ViewClientAction extends ActionSupport implements ServletRequestAware {

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    private boolean askBirthDate;

    private boolean askHomePhone;

    private boolean askWorkPhone;

    private boolean askMobilePhone;

    private boolean askEmailAddress;

    private boolean disableClientManagement = false;

    private boolean disableJourneysManagement = false;

    private boolean disableEditMarketing = false;

    public boolean isAskBirthDate() {
	return askBirthDate;
    }

    public void setAskBirthDate(boolean askBirthDate) {
	this.askBirthDate = askBirthDate;
    }

    public boolean isAskHomePhone() {
	return askHomePhone;
    }

    public void setAskHomePhone(boolean askHomePhone) {
	this.askHomePhone = askHomePhone;
    }

    public boolean isAskWorkPhone() {
	return askWorkPhone;
    }

    public void setAskWorkPhone(boolean askWorkPhone) {
	this.askWorkPhone = askWorkPhone;
    }

    public boolean isAskMobilePhone() {
	return askMobilePhone;
    }

    public void setAskMobilePhone(boolean askMobilePhone) {
	this.askMobilePhone = askMobilePhone;
    }

    public boolean isAskEmailAddress() {
	return askEmailAddress;
    }

    public void setAskEmailAddress(boolean askEmailAddress) {
	this.askEmailAddress = askEmailAddress;
    }

    private Integer clientNumber;

    private ClientVO client;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private Collection<ClientHistory> clientHistoryList;

    /**
     * @return
     */
    public String execute() {
	try {
	    client = clientMgr.getClient(clientNumber);

	    clientHistoryList = clientMgr.getClientHistory(clientNumber);

	    servletRequest.setAttribute("client", client);
	} catch (Exception e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	try {
	    askBirthDate = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_BIRTH_DATE, client.getBirthDate() != null ? client.getBirthDate().formatShortDate() : client.getBirthDateString());
	    askEmailAddress = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_EMAIL, client.getEmailAddress());
	    askHomePhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_HOME_PHONE, client.getHomePhone());
	    askMobilePhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_MOBILE_PHONE, client.getMobilePhone());
	    askWorkPhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_WORK_PHONE, client.getWorkPhone());
	} catch (Exception e) {
	    e.printStackTrace();
	    addActionError("Unable to determine if field should be asked. " + e.getMessage());
	    return ERROR;
	}

	try {
	    this.disableClientManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableClientManagement"));
	} catch (Exception e) {
	}

	try {
	    this.disableEditMarketing = Boolean.parseBoolean(FileUtil.getProperty("master", "disableEditMarketing"));
	} catch (Exception e) {
	}

	try {
	    this.disableJourneysManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableJourneysManagement"));
	} catch (Exception e) {
	}

	return SUCCESS;
    }

    public Collection<ClientHistory> getClientHistoryList() {
	return clientHistoryList;
    }

    public void setClientHistoryList(Collection<ClientHistory> clientHistoryList) {
	this.clientHistoryList = clientHistoryList;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public boolean isDisableClientManagement() {
	return disableClientManagement;
    }

    public void setDisableClientManagement(boolean disableClientManagement) {
	this.disableClientManagement = disableClientManagement;
    }

    public boolean isDisableJourneysManagement() {
	return disableJourneysManagement;
    }

    public void setDisableJourneysManagement(boolean disableJourneysManagement) {
	this.disableJourneysManagement = disableJourneysManagement;
    }

    public boolean isDisableEditMarketing() {
	return disableEditMarketing;
    }

    public void setDisableEditMarketing(boolean disableEditMarketing) {
	this.disableEditMarketing = disableEditMarketing;
    }
}