package com.ract.client.ui;

import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.ApplicationServlet;
import com.ract.common.qas.QASNotifier;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;
import com.ract.util.StringUtil;

public class ClientSearchUIC extends ApplicationServlet

{
    private static final String CONTENT_TYPE = "text/html";

    public void handleEvent_openClientSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException {

	/*
	 * setup new windows with details for advanced search
	 */
	request.setAttribute("page", ClientUIConstants.PAGE_ClientSearch);
	this.forwardRequest(request, response, ClientUIConstants.PAGE_ClientSearch);
    }

    public void handleEvent_clientSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String surname = request.getParameter("surname");
	String initial = request.getParameter("initial");
	String streetNo = request.getParameter("streetNo");
	String street = request.getParameter("street");
	String suburb = request.getParameter("suburb");
	String clientNoString = request.getParameter("clientNo");
	String birthDateString = request.getParameter("dateOfBirth");
	String genderStr = request.getParameter("gender");
	String phoneNo = request.getParameter("phoneNo");
	String emailAddress = request.getParameter("emailAddress");
	String tString = "";
	Boolean gender = null;
	char ch;
	DateTime dateOfBirth = null;
	if (birthDateString != null && !birthDateString.equals("")) {
	    try {
		dateOfBirth = new DateTime(birthDateString);
	    } catch (ParseException ex) {
		LogUtil.log(this.getClass(), "Unable to parse birthDate: " + birthDateString);
	    }
	}
	if (genderStr != null) {
	    if (genderStr.equalsIgnoreCase("Male"))
		gender = new Boolean(true);
	    else if (genderStr.equalsIgnoreCase("Female"))
		gender = new Boolean(false);
	    else
		gender = null;
	}
	if (phoneNo == null || phoneNo.equals("") || phoneNo.trim().equals(""))
	    phoneNo = null;
	else {
	    phoneNo = phoneNo.trim();
	    if (!phoneNo.startsWith("0"))
		phoneNo = "03" + phoneNo;
	    for (int xx = 0; xx < phoneNo.length(); xx++) {
		ch = phoneNo.charAt(xx);
		if (ch > 47 && ch < 58) {
		    tString = tString + ch;
		}
	    }
	    phoneNo = tString;
	}
	LogUtil.debug(this.getClass(), "surname =       " + surname + "\ninitial =      " + initial + "\nstreetNo =      " + streetNo + "\nstreet =        " + street + "\nsuburb =        " + suburb + "\ndateOfBirth =   " + dateOfBirth + "\ngender =        " + gender + "\nphoneNo =        " + phoneNo + "emailAddress=" + emailAddress);
	ClientMgr clMgr = null;
	ArrayList resultList = null;
	String responseString = null;
	Integer clientNo = null;
	ClientVO cl = null;
	String id = null;

	if (clientNoString != null && !clientNoString.equals("")) {
	    clientNo = new Integer(clientNoString.trim());
	}
	try {
	    clMgr = ClientEJBHelper.getClientMgr();
	    if (clientNo != null) {
		resultList = new ArrayList();
		cl = clMgr.getClient(clientNo);
		resultList.add(cl);
	    } else {
		resultList = clMgr.clientSearch(surname, initial, streetNo, street, suburb, dateOfBirth, gender, phoneNo, emailAddress);
	    }
	    /********************** convert to JSON format ****************************************/
	    JSONObject ob = null;
	    JSONArray jData = new JSONArray();
	    if (resultList.size() >= 99) {
		ob = new JSONObject();
		ob.put("Id", "Too many");
		jData.put(ob);
	    } else {
		for (int xx = 0; xx < resultList.size(); xx++) {
		    cl = (ClientVO) resultList.get(xx);
		    ob = new JSONObject();
		    id = StringUtil.padNumber(new Integer(xx), 3);
		    ob.put("Id", id);
		    ob.put("clientNo", cl.getClientNumber() + "");
		    ob.put("gname", cl.getGivenNames());
		    ob.put("surname", cl.getSurname());
		    ob.put("initials", cl.getInitials());
		    ob.put("streetNo", cl.getResiStreetChar());
		    ob.put("street", cl.getResidentialAddress().getStreet());
		    ob.put("suburb", cl.getResidentialAddress().getSuburb());
		    ob.put("status", cl.getStatus());
		    ob.put("group", cl.getGroupClient() ? "Y" : "N");
		    jData.put(ob);
		}
	    }
	    responseString = jData.toString();
	    // LogUtil.log(this.getClass(), "\nhandleEvent_clientSearch"
	    // + "\n\n" + responseString);
	    response.getWriter().write(responseString);
	} catch (Exception fex) {
	    throw new ServletException(fex);
	}
    }

    public void handleEvent_getClientDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String clientNoString = request.getParameter("clientNo");
	ClientVO cl = null;
	JSONObject ob = null;
	JSONArray jData = new JSONArray();
	Integer clientNo = new Integer(clientNoString);
	String tHeading = null;
	String tValue = null;
	LogUtil.log(this.getClass(), "--------------------------------------" + "\nhandleEvent_getClientDetail" + "\nfor " + clientNo);
	try {
	    ClientMgr clMgr = ClientEJBHelper.getClientMgr();
	    cl = clMgr.getClient(clientNo);
	    if (cl.getGroupClient()) {
		ClientVO grpMember = null;
		ArrayList groupList = clMgr.getClientGroup(clientNo);
		for (int xx = 0; xx < groupList.size(); xx++) {
		    ob = new JSONObject();
		    grpMember = (ClientVO) groupList.get(xx);
		    if (xx == 0) {
			tHeading = "Clients";
		    } else
			tHeading = "";

		    tValue = grpMember.getClientNumber() + " - ";

		    if (grpMember.getDisplayName() == null) {
			tValue += grpMember.getTitle() + " " + grpMember.getGivenNames() + " " + grpMember.getSurname();
		    } else
			tValue += grpMember.getDisplayName();
		    this.addDetailLine(jData, tHeading, tValue);
		}
	    } else {
		tValue = cl.getClientNumber() + " - ";

		if (cl.getDisplayName() == null) {
		    tValue += cl.getTitle() + " " + cl.getGivenNames() + " " + cl.getSurname();
		} else
		    tValue += cl.getDisplayName();
		this.addDetailLine(jData, "Client", tValue);

	    }
	    if (cl.getResidentialAddress() == null)
		tValue = "Error in Street address";
	    else
		tValue = cl.getResidentialAddress().getSingleLineAddress();
	    this.addDetailLine(jData, "Street Address", tValue);

	    if (cl.getPostalAddress() == null)
		tValue = "Error in postal address";
	    else
		tValue = cl.getPostalAddress().getSingleLineAddress();
	    this.addDetailLine(jData, "Postal Address", tValue);

	    if (cl.getBirthDate() != null)
		this.addDetailLine(jData, "Birth Date", cl.getBirthDate().formatShortDate());

	    if (cl.getHomePhone() != null && !cl.getHomePhone().equals(""))
		this.addDetailLine(jData, "Home Phone", cl.getHomePhone());

	    if (cl.getWorkPhone() != null && !cl.getWorkPhone().equals(""))
		this.addDetailLine(jData, "Work Phone", cl.getWorkPhone());

	    if (cl.getMobilePhone() != null && !cl.getMobilePhone().equals(""))
		this.addDetailLine(jData, "Mobile Phone", cl.getMobilePhone());

	    if (cl.getStatus() != null && !cl.getStatus().equals("") && !cl.getStatus().equalsIgnoreCase("ACTIVE"))
		this.addDetailLine(jData, "Status", cl.getStatus().toUpperCase());

	    if (cl.getEmailAddress() != null && !cl.getEmailAddress().equals(""))
		this.addDetailLine(jData, "Email Address", cl.getEmailAddress());

	    // LogUtil.log(this.getClass(),"\nClient Detail for " + clientNo +
	    // ":\n"+ jData.toString());
	    response.getWriter().write(jData.toString());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException(ex);
	}
    }

    private void addDetailLine(JSONArray jData, String heading, String value) throws JSONException {
	JSONObject ob = new JSONObject();
	ob.put("heading", heading);
	ob.put("value", value);
	jData.put(ob);
    }

    public void handleEvent_notifyQAS(HttpServletRequest request, HttpServletResponse response) {

	LogUtil.debug("RLG + handleEvent_notifyQAS ", " QASUIC entered ");
	String reply = "";
	try {
	    String ipAddress = null;
	    ipAddress = this.getUserSession(request).getIp();
	    String layout = (String) request.getParameter("layout");

	    LogUtil.debug("QASUIC", ipAddress + " - " + layout);
	    QASNotifier.NotifyQAS(ipAddress, layout);
	    reply = "successful";
	} catch (Exception ex) {
	    LogUtil.debug("QASUIC Exception ", ex.getMessage());
	    reply = "unsuccessful";
	}

	ServletUtil.ajaxXMLResponse(response, "<QAS><response>" + reply + "</response></QAS>");

    }

}
