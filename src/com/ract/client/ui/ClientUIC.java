package com.ract.client.ui;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.TransactionRolledbackException;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientSubscription;
import com.ract.client.ClientSubscriptionPK;
import com.ract.client.ClientVO;
import com.ract.client.MarketOption;
import com.ract.client.email.BouncedEmailAddressUpdater;
import com.ract.client.email.EmailAddressJob;
import com.ract.client.subscriptions.SubscriptionsJob;
import com.ract.client.subscriptions.UnsubscribeFileProcessor;
import com.ract.common.AddressVO;
import com.ract.common.ApplicationServlet;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.MailMgr;
import com.ract.common.MemoVO;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.StreetSuburbMgr;
import com.ract.common.StreetSuburbVO;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.extracts.ExtractFormatter;
import com.ract.common.extracts.ExtractHelper;
import com.ract.common.mail.MailMessage;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.common.ui.CommonUIConstants;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.finance.Debtor;
import com.ract.payment.finance.FinanceException;
import com.ract.payment.finance.FinanceMgr;
import com.ract.travel.TramadaClientProfileImporter;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * Controller for the client system. Receives a request and decides what view
 * (JSP) to return.
 * 
 * @author John Holliday
 * @created 1 August 2002
 * @version 1.0, 18/2/02
 */

public class ClientUIC extends ApplicationServlet {

    /**
     * @todo review thread safe use of global variables.
     */

    private final static String CLIENT_NUMBER = "clientNumber";

    // private final static String CLIENT_LIST = "clientList";

    private final static String CLIENT_VO = "client";

    /**
     * @todo change to local variable.
     */
    private String clientNumber;

    /**
     * Initialize global variables
     * 
     * @exception ServletException
     *                Description of the Exception
     */
    public void init() throws ServletException {

    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     */
    public void handleEvent_advancedSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException {

	/*
	 * setup new windows with details for advanced search
	 */
	request.setAttribute("page", ClientUIConstants.PAGE_AdvancedSearchClient);
	this.forwardRequest(request, response, ClientUIConstants.PAGE_AdvancedSearchClient);
    }

    /**
     * Show client search pane
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     */
    public void handleEvent_showClientList(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	forwardRequest(request, response, ClientUIConstants.PAGE_ClientList);
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_viewClientSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	ClientVO client = null;
	String clientNumberText = request.getParameter(CLIENT_NUMBER);

	Integer clientNumber = new Integer(clientNumberText);

	// Try and get the client from the client list in the session first.
	HttpSession session = request.getSession();
	Collection clientList = (Collection) session.getAttribute("clientList");

	// If the client is not in the client list in the session then fetch
	// them
	// from the database
	if (client == null) {
	    client = ClientEJBHelper.getClientMgr().getClient(clientNumber);
	}

	// from the client list
	if (client == null) {
	    if (clientList != null && !clientList.isEmpty()) {
		client = ClientUIHelper.getClient(clientNumber, clientList);
	    }
	}

	// If we still could not get the client then throw a wobbly.
	if (client == null) {
	    throw new ServletException("The client with ID '" + clientNumberText + "' cannot be found.");
	}

	request.setAttribute(CLIENT_VO, client);

	boolean askClientQuestions = false;

	if ((client.getLastUpdate() == null && !client.getMadeDate().getDateOnly().equals(new DateTime().getDateOnly())) || // not
															    // created
															    // today
		(client.getLastUpdate() != null && !client.getLastUpdate().getDateOnly().equals(new DateTime().getDateOnly()))) // if
																// the
																// last
																// change
																// was
																// not
																// made
																// today
	{
	    // check if we need to ask questions
	    ClientMgrLocal clientMgr = ClientEJBHelper.getClientMgrLocal();
	    if (!askClientQuestions)
		askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_BIRTH_DATE, client.getBirthDate() != null ? client.getBirthDate().formatShortDate() : client.getBirthDateString());
	    if (!askClientQuestions)
		askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_EMAIL, client.getEmailAddress());
	    if (!askClientQuestions)
		askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_HOME_PHONE, client.getHomePhone());
	    if (!askClientQuestions)
		askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_MOBILE_PHONE, client.getMobilePhone());
	    if (!askClientQuestions)
		askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_WORK_PHONE, client.getWorkPhone());
	}

	request.setAttribute("askClientQuestions", askClientQuestions);
	/*
	 * send message to OPAL * User us =
	 * this.getUserSession(request).getUser(); String pcIdentifier =
	 * this.getUserSession(request).getIp();
	 * 
	 * try { OPALServerSocket.requestOpal(clientNumber, "CUS", clientNumber,
	 * "clientNumber", "", "", pcIdentifier, us.getSalesBranchCode(),
	 * us.getUserID()); } catch(Exception ex) { // ignore it and keep going
	 * LogUtil.log(this.getClass(), "Unable to notify OPAL");
	 * ex.printStackTrace();
	 * 
	 * }
	 */

	// toggle to disable client management
	boolean disableClientManagement = false;
	try {
	    disableClientManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableClientManagement"));
	} catch (Exception e) {
	}

	request.setAttribute("disableClientManagement", disableClientManagement);

	// toggle to disable marketing prefs
	boolean disableEditMarketing = false;
	try {
	    disableEditMarketing = Boolean.parseBoolean(FileUtil.getProperty("master", "disableEditMarketing"));
	} catch (Exception e) {
	}

	request.setAttribute("disableEditMarketing", disableEditMarketing);

	// toggle to disable journeys prefs
	boolean disableJourneysManagement = false;
	try {
	    disableJourneysManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableJourneysManagement"));
	} catch (Exception e) {
	}

	request.setAttribute("disableJourneysManagement", disableJourneysManagement);

	// toggle to disable eNews prefs
	boolean disablePublicationManagement = false;
	try {
	    disablePublicationManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disablePublicationManagement"));
	} catch (Exception e) {
	}

	request.setAttribute("disablePublicationManagement", disablePublicationManagement);

	forwardRequest(request, response, ClientUIConstants.PAGE_ViewClientSummary);
    }

    public void handleEvent_findClientFromIVRPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	String clientSearchType = request.getParameter("clientSearchType");
	String clientSearchValue = request.getParameter("clientSearchValue");
	String message = request.getParameter("message");
	Integer searchType = null;
	ClientVO clientVO = null;

	if (clientSearchValue == null || clientSearchValue.equalsIgnoreCase("null")) {
	    throw new ServletException("Can't use NULL to search for client");
	}

	try {
	    searchType = Integer.valueOf(clientSearchType);
	} catch (NumberFormatException nfe) {
	    throw new ServletException("The search type '" + clientSearchType + "' is not valid", nfe);
	}

	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	Collection clientList = null;
	// A list of ClientVO objects.

	// Catch validation exceptions and display them as a message on the page
	// Let all other exceptions propogate up.
	try {
	    clientList = clientMgr.searchForClient(searchType.intValue(), clientSearchValue);
	} catch (Exception e) {
	    if (e instanceof ValidationException) {
		message = e.getMessage();
	    } else if (e instanceof TransactionRolledbackException) {
		TransactionRolledbackException trbe = (TransactionRolledbackException) e;
		message = trbe.detail.getMessage();
	    } else {
		throw new SystemException(e);
	    }
	}

	if (clientList == null || clientList.isEmpty()) {
	    throw new ServletException("The client identified by '" + clientSearchValue + "' cannot be found.");
	    // forwardRequest(request, response,
	    // PaymentUIConstants.PAGE_ViewFullIVRLine);
	}

	clientVO = (ClientVO) clientList.iterator().next();

	// If we still could not get the client then throw a wobbly.
	if (clientVO == null) {
	    throw new ServletException("The client with identified by '" + clientSearchValue + "' cannot be found.");
	}

	request.setAttribute(CLIENT_VO, clientVO);

	forwardRequest(request, response, ClientUIConstants.PAGE_ViewClientSummary);

    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */

    /**
     * handle the action of viewing the clients associated business
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_viewClientProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

	String clientNumberText = request.getParameter(CLIENT_NUMBER);
	Integer clientNumber = new Integer(clientNumberText);

	ClientVO clientVO = ClientEJBHelper.getClientMgr().getClient(clientNumber);

	// Check for membership products
	// MembershipHome membershipHome =
	// MembershipEJBHelper.getMembershipHome();
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection memList = null;
	ArrayList memVOList = new ArrayList();
	MembershipVO memVO = null;
	// Membership mem = null;

	memList = membershipMgr.findMembershipByClientNumber(clientVO.getClientNumber());
	Iterator it = memList.iterator();
	while (it.hasNext()) {
	    memVO = (MembershipVO) it.next();
	    memVOList.add(memVO);
	}

	request.setAttribute(CLIENT_VO, clientVO);
	request.setAttribute("memVOList", memVOList);

	forwardRequest(request, response, ClientUIConstants.PAGE_ProductList);

    }

    /**
     * click select client
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */

    public void handleEvent_viewClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	ClientVO client = null;

	String clientNumberText = request.getParameter(CLIENT_NUMBER);
	Integer clientNumber = new Integer(clientNumberText);

	// Try and get the client from the client list in the session first.
	// Collection clientList = (Collection)
	// session.getAttribute(this.CLIENT_LIST);

	// cached view!!!
	// if (clientList != null && !clientList.isEmpty())
	// {
	// clientVO = ClientUIHelper.getVO(clientNumber, clientList);
	// }

	// If the client is not in the client list in the session then fetch
	// them
	// from the database
	if (client == null) {
	    client = ClientEJBHelper.getClientMgr().getClient(clientNumber);
	}

	// If we still could not get the client then throw a wobbly.
	if (client == null) {
	    throw new ServletException("The client with ID '" + clientNumberText + "' cannot be found.");
	}

	request.setAttribute(CLIENT_VO, client);

	forwardRequest(request, response, ClientUIConstants.PAGE_VIEW_CLIENT);
    }

    public void handleEvent_createTramadaClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	UserSession userSession = getUserSession(request);
	Integer clientNumber = new Integer(request.getParameter("clientNumber"));
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	ClientVO client = clientMgr.getClient(clientNumber);
	clientMgr.transferTramadaClientXML(Client.TRAMADA_MODE_EXPLICIT, TramadaClientProfileImporter.TRAMADA_ACTION_ADD, userSession.getUser().getUserID(), userSession.getUser().getSalesBranchCode(), client);

	request.setAttribute(CLIENT_VO, client);

	forwardRequest(request, response, ClientUIConstants.PAGE_VIEW_CLIENT);
    }

    public void handleEvent_createDebtor(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	UserSession userSession = getUserSession(request);
	Integer clientNumber = new Integer(request.getParameter("clientNumber"));

	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	ClientVO client = clientMgr.getClient(clientNumber);

	Debtor debtor = new Debtor(clientNumber, new DateTime());

	try {
	    FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	    financeMgr.createDebtor(debtor);
	} catch (FinanceException ex) {
	    throw new SystemException("Unable to create debtor.", ex);
	}

	// toggle to disable client management
	boolean disableClientManagement = false;
	try {
	    disableClientManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableClientManagement"));
	} catch (Exception e) {
	}

	request.setAttribute("disableClientManagement", disableClientManagement);

	// toggle to disable marketing prefs
	boolean disableEditMarketing = false;
	try {
	    disableEditMarketing = Boolean.parseBoolean(FileUtil.getProperty("master", "disableEditMarketing"));
	} catch (Exception e) {
	}

	request.setAttribute("disableEditMarketing", disableEditMarketing);

	// toggle to disable journeys prefs
	boolean disableJourneysManagement = false;
	try {
	    disableJourneysManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableJourneysManagement"));
	} catch (Exception e) {
	}

	request.setAttribute("disableJourneysManagement", disableJourneysManagement);

	// toggle to disable eNews prefs
	boolean disablePublicationManagement = false;
	try {
	    disablePublicationManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disablePublicationManagement"));
	} catch (Exception e) {
	}

	request.setAttribute("disablePublicationManagement", disablePublicationManagement);

	request.setAttribute(CLIENT_VO, client);

	forwardRequest(request, response, ClientUIConstants.PAGE_ViewClientSummary);
    }

    public void handleEvent_reinstateTramadaClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	UserSession userSession = getUserSession(request);
	Integer clientNumber = new Integer(request.getParameter("clientNumber"));
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	ClientVO client = clientMgr.getClient(clientNumber);
	clientMgr.transferTramadaClientXML(Client.TRAMADA_MODE_EXPLICIT, TramadaClientProfileImporter.TRAMADA_ACTION_REINSTATE, userSession.getUser().getUserID(), userSession.getUser().getSalesBranchCode(), client);

	request.setAttribute(CLIENT_VO, client);

	forwardRequest(request, response, ClientUIConstants.PAGE_VIEW_CLIENT);
    }

    /**
     * Save an organisation record
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     */
    // public void handleEvent_saveOrganisation(HttpServletRequest request,
    // HttpServletResponse response)
    // throws ServletException, RemoteException, ValidationException
    // {
    // HttpSession session = request.getSession();
    // //get the organisation from the session
    // OrganisationVO organisationVO =
    // (OrganisationVO)session.getAttribute(this.CLIENT_VO);
    //
    // //pvo should have a value at this stage
    // if(organisationVO == null)
    // {
    // throw new SystemException("Unable to get organisation from session.");
    // }
    //
    // //update database via EJB using VO
    // UserSession userSession = getUserSession(request);
    // String userID = userSession.getUserId();
    // String transactionReason = Client.HISTORY_UPDATE;
    // String salesBranchCode = userSession.getUser().getSalesBranchCode();
    // String comment = "Alteration to client";
    // String sourceSystem = SourceSystem.CLIENT.getAbbreviation();
    //
    // ClientTransaction clientTx = new
    // ClientTransaction(ClientTransaction.TRANSACTION_UPDATE,organisationVO,userID,transactionReason,salesBranchCode,comment,sourceSystem);
    // clientTx.submit();
    //
    // /**
    // * @todo vehicle management
    // */
    // request.setAttribute(CLIENT_VO, clientTx.getClient());
    //
    //
    // forwardRequest(request, response,
    // ClientUIConstants.PAGE_ViewOrganisation);
    // }

    /**
     * view direct debit authorities
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_viewClientDDDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

	String clientNumber = request.getParameter("clientNumber");
	if (clientNumber == null) {
	    throw new ServletException("Client number is null when view direct debit authorities.");
	}
	ClientVO client = ClientEJBHelper.getClientMgr().getClient(new Integer(clientNumber));

	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	Collection directDebitAuthorities = null;
	try {
	    directDebitAuthorities = pMgr.getDirectDebitAuthorityListByClient(new Integer(clientNumber));
	} catch (PaymentException ex) {
	    throw new ServletException("Unable to get direct debit authority list for client", ex);
	}

	request.setAttribute(CLIENT_VO, client);
	request.setAttribute("directDebitAuthorities", directDebitAuthorities);
	forwardRequest(request, response, ClientUIConstants.PAGE_VIEW_CLIENT_DD_DETAILS);
    }

    public void handleEvent_exportSubscriberFile(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	String publication = request.getParameter("publication");
	String format = request.getParameter("format");
	String emailAddress = request.getParameter("emailAddress");

	try {
	    // xml
	    ExtractFormatter ef = ExtractHelper.getExtractFormatter(format, publication);

	    // write temp file
	    CommonMgrLocal cm = CommonEJBHelper.getCommonMgrLocal();
	    String memDir = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DIRECTORY_MEMBERSHIP_EXTRACTS);
	    String fileName = memDir + ef.getExtractName() + "." + FileUtil.EXTENSION_CSV;
	    FileUtil.writeFile(fileName, ef.toString());

	    // email file

	    Hashtable files = new Hashtable();
	    files.put("extract.csv", fileName);

	    MailMessage message = new MailMessage();
	    message.setSubject("Subsriber extract - " + ef.FORMAT_CLIENT_FUTURE_MEDIUM_EMAIL);
	    message.setFiles(files);
	    message.setRecipient(emailAddress);

	    // send email
	    MailMgr mailMgr = CommonEJBHelper.getMailMgr();
	    mailMgr.sendMail(message);

	    // FileUtil.downloadContentsAsFile(response, out, ef.toString(),
	    // "test.csv");
	} catch (Exception ex1) {
	    throw new RemoteException("Unable to generate extract.", ex1);
	}

    }

    public void handleEvent_addSubscription(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	this.outputRequestParameters(request);
	this.outputRequestAttributes(request);
	this.outputSessionAttributes(request);
	Integer clientNumber = new Integer(request.getParameter("clientNumber"));
	String publicationCode = request.getParameter("publicationCode");
	String salesBranchCode = request.getParameter("travelBranchList");
	String deliveryMethod = request.getParameter("deliveryMethod");
	UserSession userSession = this.getUserSession(request);// user id
	ClientSubscription clientSubscription = new ClientSubscription(new ClientSubscriptionPK(clientNumber, publicationCode, ClientSubscription.STATUS_ACTIVE, new DateTime()), deliveryMethod, salesBranchCode, userSession.getUserId());
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	clientMgr.createClientSubscription(clientSubscription);
	handleEvent_manageSubscriptions(request, response);
    }

    public void handleEvent_removeSubscription(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	this.outputRequestParameters(request);
	this.outputRequestAttributes(request);
	this.outputSessionAttributes(request);
	Integer clientNumber = new Integer(request.getParameter("clientNumber"));
	String subscriptionKey[] = (request.getParameter("publicationCode")).split(FileUtil.SEPARATOR_COMMA);

	String publicationCode = subscriptionKey[0];
	String subscriptionStatus = subscriptionKey[1];
	DateTime subscriptionDate = null;
	try {
	    subscriptionDate = new DateTime(subscriptionKey[2]);
	} catch (ParseException ex) {
	    throw new ValidationException("Subscription date may not be empty.");
	}

	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	// keep a history
	clientMgr.deleteClientSubscription(new ClientSubscriptionPK(clientNumber, publicationCode, subscriptionStatus, subscriptionDate));
	handleEvent_manageSubscriptions(request, response);
    }

    /**
     * Handle the event of clicking the client search button on the client list
     * page.
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     */
    public void handleEvent_clientList_btnSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	handleClientSearch(request, response);
    }

    /**
     * Handle the event of clicking the client search button on the product list
     * page.
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     */
    public void handleEvent_productList_btnSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	handleClientSearch(request, response);
    }

    /**
     * Gather the search criteria from the search page and call the client
     * manager session bean to conduct the search. Show the client list page
     * with the results
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     */
    private void handleClientSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	String clientSearchType = request.getParameter("clientSearchType");
	String clientSearchValue = request.getParameter("clientSearchValue");
	String message = request.getParameter("message");
	Integer searchType = null;

	LogUtil.debug(this.getClass(), "clientSearchType=" + clientSearchType);
	LogUtil.debug(this.getClass(), "clientSearchValue=" + clientSearchValue);
	LogUtil.debug(this.getClass(), "message=" + message);

	try {
	    searchType = Integer.valueOf(clientSearchType);
	} catch (NumberFormatException nfe) {
	    throw new ServletException("The search type '" + clientSearchType + "' is not valid", nfe);
	}

	// Remove any leading or trailing spaces the user may have accidentily
	// typed.
	if (clientSearchValue != null) {
	    clientSearchValue = clientSearchValue.trim();

	}
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	Collection clientList = null;
	// A list of ClientVO objects.

	// Catch validation exceptions and display them as a message on the page
	// Let all other exceptions propogate up.
	try {
	    clientList = clientMgr.searchForClient(searchType.intValue(), clientSearchValue);
	} catch (Exception e) {
	    if (e instanceof ValidationException) {
		message = e.getMessage();
	    } else if (e instanceof TransactionRolledbackException) {
		TransactionRolledbackException trbe = (TransactionRolledbackException) e;
		message = trbe.detail.getMessage();
	    } else {
		throw new SystemException(e);
	    }
	}
	LogUtil.debug(this.getClass(), "clientList = " + clientList);
	if (clientList == null || clientList.isEmpty()) {
	    // Work out a message to display.
	    // Don't overwrite the validation exception message set above
	    // Otherwise make the message meaningful to the search type.
	    if (message == null) {
		switch (searchType.intValue()) {
		case ClientUIConstants.SEARCHBY_CLIENT_NUMBER:
		    message = "Unable to find client number " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_CLIENT_SURNAME:
		    message = "Unable to find client with surname " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_MEMBERSHIP_NUMBER:
		    message = "Unable to find client with membership number " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_POLICY_NUMBER:
		    message = "Unable to find client with policy number " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_HOME_PHONE:
		    message = "Unable to find client with phone number " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_ADDRESS:
		    message = "Unable to find client at that address.";
		    break;
		case ClientUIConstants.SEARCHBY_IVR_NUMBER:
		    message = "Unable to find client with IVR number " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_GROUP_NAME:
		    message = "Unable to find client with group name " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_MOBILE_PHONE:
		    message = "Unable to find client with mobile phone " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_MEMBERSHIP_CARD_NUMBER:
		    message = "Unable to find client with membership card no. " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_MEMBERSHIP_ID:
		    message = "Unable to find client with membership id " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_EMAIL_ADDRESS:
		    message = "Unable to find client with email address " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_VIN:
		    message = "Unable to find client with VIN " + clientSearchValue + ".";
		    break;
		case ClientUIConstants.SEARCHBY_REGO:
		    message = "Unable to find client with rego " + clientSearchValue + ".";
		    break;
		default:
		    message = "Unknown search type '" + searchType + "'";
		}
	    }
	}

	// If the message is still null then we found something.
	if (message == null) {
	    message = "Searched for \"" + clientSearchValue + "\".";
	}

	// Save the search type in the session so we can select it then next
	// time a client search page is displayed.
	request.getSession().setAttribute("searchType", searchType);

	// Add the client list to the session so we can easily view full client
	// details when running down the search result list.
	request.getSession().setAttribute("clientList", clientList);

	// Add the client list and the message to the request so the client
	// list page can easily get them.
	request.setAttribute("clientList", clientList);
	request.setAttribute("message", message);
	request.setAttribute("clientSearchValue", clientSearchValue);

	forwardRequest(request, response, ClientUIConstants.PAGE_ClientList);
    }

    /**
     * add memo
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_addMemo(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	// no memo number as it is new!!!
	this.forwardRequest(request, response, "/common/ChangeMemo.jsp");
    }

    /**
     * add postal address
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_addPostalAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	// no stsubid
	request.setAttribute("addressType", AddressVO.ADDRESS_TYPE_POSTAL);
	request.setAttribute("page", ClientUIConstants.PAGE_ChangeClientAddress);

	this.forwardRequest(request, response, ClientUIConstants.PAGE_ChangeClientAddress);
    }

    private void setAddressAttributes(HttpServletRequest request) throws ValidationException, RemoteException {
	// get the parameters
	String property = request.getParameter("property");
	String propertyQualifier = request.getParameter("propertyQualifier");
	String street = request.getParameter("streetName");
	String suburb = request.getParameter("suburbName");
	String stsubid = request.getParameter("stsubid");
	String addressType = request.getParameter("addressType");

	// set the attributes
	if (property != null) {
	    request.setAttribute("property", property);
	}

	if (propertyQualifier != null) {
	    request.setAttribute("propertyQualifier", propertyQualifier);
	}

	if (stsubid != null) {
	    request.setAttribute("stsubid", stsubid);
	}

	StreetSuburbMgr streetSuburbMgr = null;
	Collection c = null;

	// log("street "+street+", suburb "+suburb);
	streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
	c = streetSuburbMgr.getStreetSuburbList(street, suburb);

	// set the street suburb list
	if (c != null) {
	    request.setAttribute("streetSuburbList", c);
	}

	request.setAttribute("addressType", addressType);

    }

    /**
     * set attributes for the address
     * 
     * @param request
     *            Description of the Parameter
     * @param addressType
     *            Description of the Parameter
     * @return The addressVOFromRequest value
     * @exception ServletException
     *                Description of the Exception
     */
    private AddressVO getAddressVOFromRequest(HttpServletRequest request, String addressType) throws ServletException {
	this.outputRequestParameters(request);
	AddressVO avo = null;
	// get parameters from the request
	String property = request.getParameter("property");
	String propertyQualifier = request.getParameter("propertyQualifier");
	String stSubID = request.getParameter("stsubid");
	Integer streetSuburbID = new Integer(stSubID);
	StreetSuburbVO svo = null;
	try {
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    svo = cMgr.getStreetSuburb(streetSuburbID);
	} catch (Exception ex) {
	    throw new ServletException("Unable to set address for request. " + ex.toString());
	}

	if (addressType.equals(AddressVO.ADDRESS_TYPE_POSTAL)) {
	    // log("post");
	    avo = new PostalAddressVO(property, propertyQualifier, svo);
	} else if (addressType.equals(AddressVO.ADDRESS_TYPE_STREET)) {
	    // log("resi");
	    avo = new ResidentialAddressVO(property, propertyQualifier, svo);
	} else {
	    throw new ServletException("Unknown address type." + addressType);
	}

	return avo;
    }

    /**
     * edit street address
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_editStreetAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	AddressVO avo = this.getAddressVOFromRequest(request, AddressVO.ADDRESS_TYPE_STREET);
	request.setAttribute("addressVO", avo);
	request.setAttribute("page", ClientUIConstants.PAGE_ChangeClientAddress);
	this.forwardRequest(request, response, ClientUIConstants.PAGE_ChangeClientAddress);
    }

    /**
     * edit postal address
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_editPostalAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	AddressVO avo = this.getAddressVOFromRequest(request, AddressVO.ADDRESS_TYPE_POSTAL);
	request.setAttribute("addressVO", avo);
	request.setAttribute("page", ClientUIConstants.PAGE_ChangeClientAddress);
	this.forwardRequest(request, response, ClientUIConstants.PAGE_ChangeClientAddress);
    }

    /**
     * add street address
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_addStreetAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	// no stsubid
	request.setAttribute("addressType", AddressVO.ADDRESS_TYPE_STREET);
	request.setAttribute("page", ClientUIConstants.PAGE_ChangeClientAddress);
	this.forwardRequest(request, response, ClientUIConstants.PAGE_ChangeClientAddress);
    }

    /**
     * edit Memo
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void handleEvent_editMemo(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	String memoNumber = request.getParameter("memoNumber");
	CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	MemoVO mvo = cMgr.getMemo(new Integer(memoNumber));
	// add memo to the request
	request.setAttribute("memo", mvo);
	this.forwardRequest(request, response, "/common/ChangeMemo.jsp");
    }

    public void handleEvent_manageSubscriptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	Integer clientNumber = new Integer(request.getParameter("clientNumber"));
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	Collection clientSubscriptionList = clientMgr.getClientSubscriptionList(clientNumber);
	ClientVO client = clientMgr.getClient(clientNumber);
	request.setAttribute("clientSubscriptionList", clientSubscriptionList);
	request.setAttribute("client", client);

	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

	request.setAttribute("travelBranchList", commonMgr.getTravelBranchList());

	Collection publicationList = commonMgr.getPublicationList();

	Collection unsubscribedPublicationList = ClientHelper.getUnsubscribedPublicationList(clientSubscriptionList, publicationList);
	request.setAttribute("unsubscribedPublicationList", unsubscribedPublicationList);

	this.forwardRequest(request, response, ClientUIConstants.PAGE_MANAGE_SUBSCRIPTIONS);
    }

    /**
     * get a list of street suburb value objects
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     */
    public void handleEvent_selectClientAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	this.outputRequestParameters(request);

	String street = request.getParameter("street");
	String suburb = request.getParameter("suburb");
	String property = request.getParameter("property");
	String propertyQualifier = request.getParameter("propertyQualifier");
	String mode = request.getParameter("mode");

	StreetSuburbMgr streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
	Collection streetSuburbList = streetSuburbMgr.getStreetSuburbList(street, suburb);

	request.setAttribute("streetSuburbList", streetSuburbList);
	request.setAttribute("property", property);
	request.setAttribute("propertyQualifier", propertyQualifier);

	request.setAttribute("mode", mode);

	String addressType = request.getParameter("addressType");
	request.setAttribute("addressType", addressType);

	// forward back to the page
	forwardRequest(request, response, "/client/SelectClientAddress.jsp");
    }

    /**
     * get a list of street suburb value objects
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     */
    public void handleEvent_findStreetSuburbList(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	this.outputRequestParameters(request);

	// String property = request.getParameter("property");
	// String propertyQualifier = request.getParameter("propertyQualifier");
	//
	// String street = request.getParameter("streetName");
	// String suburb = request.getParameter("suburbName");
	// String addressType = request.getParameter("addressType");
	this.setAddressAttributes(request);
	// if (addressType == null)
	// {
	// addressType = "";
	// }
	// if (!addressType.equals(""))
	// {
	// request.setAttribute("addressType", addressType);
	// }

	// get current page
	String page = request.getParameter("page");

	AddressVO avo = null;

	String stsubid = request.getParameter("stsubid");

	request.setAttribute("page", page);

	// forward back to the page
	forwardRequest(request, response, page);
    }

    /**
     * setPhonePassword Update the client record (in all systems) to the new
     * values of phone prompt and password
     */

    public void handleEvent_setPhonePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	// String userId = request.getParameter("userId");
	String clientNoString = request.getParameter("clientNo");
	String phonePassword = request.getParameter("phonePassword");
	String phoneQuestion = request.getParameter("phoneQuestion");
	UserSession userSession = getUserSession(request);
	String userID = userSession.getUserId();
	User xUser = UserEJBHelper.getUserMgr().getUser(userID);
	ClientMgr cltMgr = ClientEJBHelper.getClientMgr();
	if (clientNoString == null || clientNoString.equals("")) {
	    throw new ServletException("Client number not provided");
	}
	Integer clientNo = new Integer(clientNoString);
	ClientVO clt = cltMgr.getClient(clientNo);
	clt.setPhonePassword(phonePassword);
	clt.setPhoneQuestion(phoneQuestion);

	cltMgr.updateClient(clt, userID, "Change Phone Password", xUser.getSalesBranch().getSalesBranchCode(), "", clt.getSourceSystem().toString());
    }

    public void handleEvent_showMarketOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

	String clientNumber = request.getParameter("clientNo");
	request.setAttribute("clientNo", clientNumber);
	forwardRequest(request, response, ClientUIConstants.PAGE_MARKET_OPTIONS);

    }

    public void handleEvent_saveMarketOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	String clientNoString = request.getParameter("clientNo");
	String optString = request.getParameter("optArray");
	String userId = getUserSession(request).getUserId();
	String columnHeadings = request.getParameter("columnHeadings");
	String rowHeadings = request.getParameter("rowHeadings");
	String[] opt = optString.split(",");
	String[] colHd = columnHeadings.split(",");
	String[] rowHd = rowHeadings.split(",");
	String[] optItem = new String[3];
	MarketOption mo = null;
	Integer clientNo = new Integer(clientNoString);
	int row;
	int column;
	boolean option;
	ArrayList optionList = new ArrayList();
	for (int xx = 0; xx < opt.length; xx++) {
	    optItem = opt[xx].split(":");
	    option = (new Boolean(optItem[2])).booleanValue();
	    row = Integer.parseInt(optItem[1]);
	    column = Integer.parseInt(optItem[0]);
	    mo = new MarketOption();
	    mo.setClientNo(clientNo);
	    mo.setSubSystem(colHd[column]);
	    mo.setMedium(rowHd[row]);
	    mo.setUnavailable(new Boolean(!option));
	    optionList.add(mo);
	}
	ClientMgr mgr = ClientEJBHelper.getClientMgr();
	mgr.setMarketOptions(clientNo, optionList, userId);
    }

    public void handleEvent_Subscriptions_DoUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {

	CommonMgr cm = CommonEJBHelper.getCommonMgr();
	String rmiPort = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_PORT);
	String rmiHost = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_HOST);

	String providerURL = rmiHost + ":" + rmiPort;

	outputRequestParameters(request);

	String runType = request.getParameter("runType");

	if (runType != null && runType.equalsIgnoreCase(ScheduleMgr.MODE_RUN_NOW)) {
	    this.runSubscriptionsUpdate(providerURL, request, response);
	} else {
	    this.scheduleUnsubcribeFileUpdate(providerURL, request, response);
	}
    }

    private void runSubscriptionsUpdate(String providerURL, HttpServletRequest request, HttpServletResponse response)

    throws Exception {
	String inputFile = request.getParameter("inputFileName");
	String dateToProcess = request.getParameter("dateToProcess");

	UnsubscribeFileProcessor sr = new UnsubscribeFileProcessor(providerURL, inputFile, dateToProcess);
	request.setAttribute("heading", "Subscriptions Updated");
	String cancelText = "The Subscriptions have been updated. \n\n\n" + "You may now continue with any other process.";
	request.setAttribute("message", cancelText);

	this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

    }

    private void scheduleUnsubcribeFileUpdate(String providerURL, HttpServletRequest request, HttpServletResponse response)

    throws Exception {

	String dateString = request.getParameter("runDate");
	String hoursString = request.getParameter("runHours");
	String minsString = request.getParameter("runMins");
	DateTime runTime = new DateTime(dateString);

	GregorianCalendar gc = new GregorianCalendar();
	gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(hoursString), Integer.parseInt(minsString), 0);

	ScheduleMgr sMgr = CommonEJBHelper.getScheduleMgr();

	JobDetail jobDetail = new JobDetail("Subscriptions", Scheduler.DEFAULT_GROUP, SubscriptionsJob.class);

	jobDetail.setDescription("Subscriptions Update");

	SimpleTrigger trigger = new SimpleTrigger("Subscriptionstrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);

	JobDataMap dataMap = jobDetail.getJobDataMap();

	dataMap.put("inputFile", request.getParameter("inputFileName"));
	dataMap.put("dateToProcess", request.getParameter("dateToProcess"));
	String userId = this.getUserSession(request).getUserId();
	dataMap.put("userId", userId);
	String messageText = null;

	dataMap.put("providerURL", providerURL);

	try {

	    sMgr.scheduleJob(jobDetail, trigger);

	    request.setAttribute("heading", "Subscriptions Update Scheduled");
	    messageText = "The Subscriptions Update will run at" + "<br/>" + (new DateTime(gc.getTime())).formatLongDate();
	    request.setAttribute("message", messageText);
	} catch (Exception e) {
	    request.setAttribute("heading", "WARNING: Subscriptions Update not scheduled");
	    messageText = "The system was unable to schedule the Subscriptions Update: <br/>" + e;
	    request.setAttribute("message", messageText);
	}
	this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

    }

    public void handleEvent_EmailAddress_DoUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {

	CommonMgr cm = CommonEJBHelper.getCommonMgr();

	String rmiPort = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_PORT);
	String rmiHost = cm.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.RMI_HOST);

	String providerURL = rmiHost + ":" + rmiPort;

	String runType = request.getParameter("runType");
	if (runType != null && runType.equalsIgnoreCase(ScheduleMgr.MODE_RUN_NOW)) {
	    this.runBouncedEmailAddressUpdate(providerURL, request, response);
	} else {
	    this.scheduleBouncedEmailAddressUpdate(providerURL, request, response);
	}
    }

    private void runBouncedEmailAddressUpdate(String providerURL, HttpServletRequest request, HttpServletResponse response)

    throws Exception {
	String inputFile = request.getParameter("inputFileName");
	String dateToProcess = request.getParameter("dateToProcess");

	BouncedEmailAddressUpdater sr = new BouncedEmailAddressUpdater(providerURL, inputFile);

	request.setAttribute("heading", "Bounced Email Updated");
	String cancelText = "The Bounced Emails have been updated. \n\n\n" + "You may now continue with any other process.";
	request.setAttribute("message", cancelText);

	this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
    }

    private void scheduleBouncedEmailAddressUpdate(String providerURL, HttpServletRequest request, HttpServletResponse response)

    throws Exception {

	String dateString = request.getParameter("runDate");
	String hoursString = request.getParameter("runHours");
	String minsString = request.getParameter("runMins");
	DateTime runTime = new DateTime(dateString);

	GregorianCalendar gc = new GregorianCalendar();
	gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(hoursString), Integer.parseInt(minsString), 0);

	ScheduleMgr sMgr = CommonEJBHelper.getScheduleMgr();

	JobDetail jobDetail = new JobDetail("EmailAddress", Scheduler.DEFAULT_GROUP, EmailAddressJob.class);

	jobDetail.setDescription("EmailAddress Update");

	SimpleTrigger trigger = new SimpleTrigger("EmailAddresstrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);

	JobDataMap dataMap = jobDetail.getJobDataMap();

	dataMap.put("inputFile", request.getParameter("inputFileName"));
	String userId = this.getUserSession(request).getUserId();
	dataMap.put("userId", userId);
	String messageText = null;

	dataMap.put("providerURL", providerURL);

	try {

	    sMgr.scheduleJob(jobDetail, trigger);

	    request.setAttribute("heading", "EmailAddress Update Scheduled");
	    messageText = "The EmailAddress Update will run at" + "<br/>" + (new DateTime(gc.getTime())).formatLongDate();
	    request.setAttribute("message", messageText);
	} catch (Exception e) {
	    request.setAttribute("heading", "WARNING: EmailAddress Update not scheduled");
	    messageText = "The system was unable to schedule the EmailAddress Update: <br/>" + e;
	    request.setAttribute("message", messageText);
	}
	this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

    }

}
