package com.ract.client.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.table.DefaultTableCellRenderer;

public class DateCellRenderer extends DefaultTableCellRenderer {

    DateFormat formatter;

    public DateCellRenderer() {
	super();
    }

    @Override
    public void setValue(Object value) {
	System.out.println("here");
	formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	setText((value == null) ? "" : formatter.format(value));
    }

}
