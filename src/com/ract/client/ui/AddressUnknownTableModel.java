package com.ract.client.ui;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

public class AddressUnknownTableModel extends AbstractTableModel {
    final boolean DEBUG = true;

    private String[] columnNames = { "Client No", "Name", "Postal Address", "Status", "Delete?" };

    private Vector data;

    public AddressUnknownTableModel(Vector<Vector> myData) {
	System.out.println("myData" + myData);
	System.out.println("myData" + myData.size());
	data = myData;

    }

    public int getColumnCount() {
	return columnNames.length;
    }

    public int getValidRowCount() {
	int counter = 0;
	int rowCount = getRowCount();
	for (int i = 0; i < rowCount; i++) {
	    Object val = getValueAt(i, 0);
	    if (val != null && !val.equals("")) {
		counter++;
	    }
	}
	return counter;
    }

    public int getRowCount() {
	return data.size();
    }

    public String getColumnName(int col) {
	return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
	return ((Vector) data.get(row)).get(col);
    }

    /*
     * JTable uses this method to determine the default renderer/ editor for
     * each cell. If we didn't implement this method, then the last column would
     * contain text ("true"/"false"), rather than a check box.
     */
    public Class getColumnClass(int c) {
	return getValueAt(0, c).getClass();
    }

    /*
     * Don't need to implement this method unless your table's editable.
     */
    public boolean isCellEditable(int row, int col) {
	// Note that the data/cell address is constant,
	// no matter where the cell appears onscreen.
	if (col == 0 || col == 4) { // 0 based
	    return true;
	} else {
	    return false;
	}
    }

    /*
     * Don't need to implement this method unless your table's data can change.
     */
    public void setValueAt(Object value, int row, int col) {
	if (DEBUG) {
	    System.out.println("Setting value at " + row + "," + col + " to " + value + " (an instance of " + value.getClass() + ")");
	}

	Vector myRow = (Vector) data.get(row);
	myRow.set(col, value);

	System.out.println("A");

	fireTableCellUpdated(row, col);

	System.out.println("B");

	if (DEBUG) {
	    System.out.println("New value of data:");
	    printDebugData();
	}
    }

    public Vector getRow(int row) {
	return (Vector) data.get(row);
    }

    public void removeRow(int row) {
	int rowCount = getRowCount();
	data.remove(row);

	fireTableRowsDeleted(getRowCount(), rowCount);
    }

    public void clearTable() {
	int rowCount = getRowCount() - 1;
	for (int i = rowCount; i >= 0; i--) {
	    // leave blank row
	    Object o = getValueAt(i, 1);
	    if (o != null && !o.equals("")) {
		removeRow(i);
	    }
	}
    }

    public void addRow(String clientNumber, String clientName, String address, String status) {
	int rows = data.size();
	System.out.println("AA");
	Vector v = new Vector();
	v.add(clientNumber);
	v.add(clientName);
	v.add(address);
	v.add(status);
	v.add(new Boolean(false));
	data.add(v);
	System.out.println("AB");
	fireTableRowsInserted(rows, data.size() - 1);
    }

    private void printDebugData() {
	int numRows = getRowCount();
	int numCols = getColumnCount();

	for (int i = 0; i < numRows; i++) {
	    System.out.print("    row " + i + ":");
	    for (int j = 0; j < numCols; j++) {
		System.out.print("  " + ((Vector) data.get(i)).get(j));
	    }
	    System.out.println();
	}
	System.out.println("--------------------------");
    }

}
