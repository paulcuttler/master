/*
 * ClientBulkProcessor.java
 *
 * Created on __DATE__, __TIME__
 */

package com.ract.client.ui;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.ract.client.Client;
import com.ract.client.ClientBulkUpdate;
import com.ract.client.ClientBulkUpdatePK;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientHistory;
import com.ract.client.ClientMgr;
import com.ract.client.ClientTransaction;
import com.ract.client.ClientVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.security.ui.LogonDialog;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.StringUtil;

/**
 * 
 * @author __USER__
 */
public class ClientBulkProcessor extends javax.swing.JFrame {

    public void defaultFocus() {
	jTable1.changeSelection(0, 0, false, false);
	jTable1.requestFocus();
    }

    /*
     * 
     * TODO
     * 
     * Logon screen DONE
     * 
     * save audit records
     * 
     * perform actual client update with status of address unknown
     */

    private User user;

    /** Creates new form ClientBulkProcessor */
    public ClientBulkProcessor() {
	try {
	    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	} catch (ClassNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (InstantiationException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IllegalAccessException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (UnsupportedLookAndFeelException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	initComponents();
	jPanel1.setVisible(false);

	clientMgr = ClientEJBHelper.getClientMgr();

	resetLastCreateDate();

	Vector table = new Vector();
	Vector row = new Vector();
	row.add("");
	row.add("");
	row.add("");
	row.add("");
	row.add(new Boolean(false));
	table.add(row);

	AddressUnknownTableModel autm = new AddressUnknownTableModel(table);
	autm.addTableModelListener(new AddressUnknownTableModelListener());
	jTable1.setModel(autm);
	jTable1.setCellSelectionEnabled(true);

	TableColumn column = null;
	for (int i = 0; i < 5; i++) {
	    column = jTable1.getColumnModel().getColumn(i);
	    switch (i) {
	    case 0:
		column.setPreferredWidth(10);
		break;
	    case 1:
		column.setPreferredWidth(100);
		break;
	    case 2:
		column.setPreferredWidth(200);
		break;
	    case 3:
		column.setPreferredWidth(50);
		break;
	    case 4:
		column.setPreferredWidth(10);
		break;
	    default:
	    }
	}

	// TableColumnModel tcm = jTable2.getColumnModel();
	// TableColumn tc = tcm.getColumn(2);
	// System.out.println(""+tc.getIdentifier());
	// tc.setCellRenderer(new DateCellRenderer());
	// System.out.println(""+tc.getCellRenderer());

	Vector columnNames = getSearchColumnNames();
	DefaultTableModel tm = (DefaultTableModel) jTable2.getModel();
	tm.setColumnIdentifiers(columnNames);
	tm.setRowCount(0);
	tm.fireTableStructureChanged();
	jTable2.setAutoCreateRowSorter(true);

	RowSorter sorter = jTable2.getRowSorter();
	List<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
	sortKeys.add(new RowSorter.SortKey(2, SortOrder.DESCENDING));
	sorter.setSortKeys(sortKeys);

	jTable2.setDefaultRenderer(Date.class, new DateCellRenderer());

	defaultFocus();

	userId = "jyh";

	LogonDialog logon = new LogonDialog();
	logon.setTitle("RACT Logon");
	// logon.setHostURL(this.serverHost,this.serverPort);
	logon.setVisible(true);

	userId = logon.getUsername();
	UserMgr uMgr = UserEJBHelper.getUserMgr();
	try {
	    user = uMgr.getUser(userId);
	} catch (RemoteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	if (logon.getUsername() == null) {
	    this.dispose();
	    System.exit(0);
	}
	String role = "addBulk"; // TODO fix
	boolean status = logon.validatePrivilege(role);
	if (!status) {
	    this.dispose();
	    System.exit(0);
	}

    }

    private DateTime resetLastCreateDate() {
	DateTime lastCreateDate = clientMgr.getLastBulkClientCreateDate(ClientBulkUpdatePK.TYPE_ADDRESS_UNKNOWN);
	if (lastCreateDate != null) {
	    jTextField3.setText(lastCreateDate.formatMediumDate());
	}
	return lastCreateDate;
    }

    // GEN-BEGIN:initComponents
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

	jDialog1 = new javax.swing.JDialog();
	jPanel2 = new javax.swing.JPanel();
	jButton4 = new javax.swing.JButton();
	jLabel5 = new javax.swing.JLabel();
	jPanel3 = new javax.swing.JPanel();
	jFormattedClientNumber = new javax.swing.JFormattedTextField();
	jFormattedStartDate = new javax.swing.JFormattedTextField();
	jFormattedEndDate = new javax.swing.JFormattedTextField();
	jRadioButton1 = new javax.swing.JRadioButton();
	jRadioButton2 = new javax.swing.JRadioButton();
	jLabel6 = new javax.swing.JLabel();
	jScrollPane2 = new javax.swing.JScrollPane();
	Vector columnNames = getSearchColumnNames();
	ClientSearchTableModel tm = new ClientSearchTableModel(columnNames, 0);
	jTable2 = new javax.swing.JTable();
	jButton1 = new javax.swing.JButton();
	jScrollPane3 = new javax.swing.JScrollPane();
	jSearchSummary = new javax.swing.JTextArea();
	jLabel8 = new javax.swing.JLabel();
	buttonGroup1 = new javax.swing.ButtonGroup();
	jPanel1 = new javax.swing.JPanel();
	jLabel1 = new javax.swing.JLabel();
	jButton2 = new javax.swing.JButton();
	jLabel2 = new javax.swing.JLabel();
	jTextField2 = new javax.swing.JTextField();
	jLabel3 = new javax.swing.JLabel();
	jTextField3 = new javax.swing.JTextField();
	jLabel4 = new javax.swing.JLabel();
	jButton3 = new javax.swing.JButton();
	jScrollPane1 = new javax.swing.JScrollPane();
	jTable1 = new javax.swing.JTable();
	jLabel7 = new javax.swing.JLabel();
	jLastExtractDate = new javax.swing.JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
	jMenuBar1 = new javax.swing.JMenuBar();
	jMenu2 = new javax.swing.JMenu();
	jMenuItem3 = new javax.swing.JMenuItem();
	jMenu1 = new javax.swing.JMenu();
	jMenuItem1 = new javax.swing.JMenuItem();
	jMenuItem2 = new javax.swing.JMenuItem();

	jDialog1.setTitle("View Processed Clients");

	jButton4.setText("Search");
	jButton4.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jButton4ActionPerformed(evt);
	    }
	});

	jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12));
	jLabel5.setText("Processed Clients");

	jFormattedStartDate.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jFormattedStartDateActionPerformed(evt);
	    }
	});

	jRadioButton1.setText("Client Number");

	jRadioButton2.setSelected(true);
	jRadioButton2.setText("Last processed between");
	jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jRadioButton2ActionPerformed(evt);
	    }
	});

	jLabel6.setText("and");

	javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
	jPanel3.setLayout(jPanel3Layout);
	jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
		jPanel3Layout.createSequentialGroup().addContainerGap().addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jRadioButton2).addComponent(jRadioButton1)).addGap(39, 39, 39).addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel3Layout.createSequentialGroup().addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addComponent(jFormattedEndDate, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE).addComponent(jFormattedStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jLabel6).addGap(150, 150, 150)).addGroup(jPanel3Layout.createSequentialGroup().addComponent(jFormattedClientNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE).addContainerGap()))));
	jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
		jPanel3Layout.createSequentialGroup().addContainerGap().addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jRadioButton2).addComponent(jFormattedStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(jLabel6)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jFormattedEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18).addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jRadioButton1).addComponent(jFormattedClientNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

	jTable2.setModel(tm);
	jScrollPane2.setViewportView(jTable2);

	javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
	jPanel2.setLayout(jPanel2Layout);
	jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()).addGroup(jPanel2Layout.createSequentialGroup().addComponent(jLabel5).addContainerGap(417, Short.MAX_VALUE)).addGroup(jPanel2Layout.createSequentialGroup().addComponent(jButton4).addContainerGap(456, Short.MAX_VALUE)).addGroup(jPanel2Layout.createSequentialGroup().addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 501, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap()))));
	jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jButton4).addGap(18, 18, 18).addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)));

	jButton1.setText("Close");
	jButton1.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jButton1ActionPerformed(evt);
	    }
	});

	jSearchSummary.setColumns(20);
	jSearchSummary.setRows(5);
	jSearchSummary.setEnabled(false);
	jScrollPane3.setViewportView(jSearchSummary);

	jLabel8.setText("Summary");

	javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
	jDialog1.getContentPane().setLayout(jDialog1Layout);
	jDialog1Layout.setHorizontalGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jDialog1Layout.createSequentialGroup().addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addGroup(jDialog1Layout.createSequentialGroup().addContainerGap().addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)).addGroup(jDialog1Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE)).addGroup(jDialog1Layout.createSequentialGroup().addContainerGap().addComponent(jButton1))).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	jDialog1Layout.setVerticalGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jDialog1Layout.createSequentialGroup().addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jLabel8).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18).addComponent(jButton1).addContainerGap(18, Short.MAX_VALUE)));

	setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	setTitle("Bulk Client Processing");

	jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12));
	jLabel1.setText("Address Unknown");

	jButton2.setText("Process");
	jButton2.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jButton2ActionPerformed(evt);
	    }
	});
	jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
	    public void keyPressed(java.awt.event.KeyEvent evt) {
		jButton2KeyPressed(evt);
	    }
	});

	jLabel2.setText("Enter the list of clients below that will require the status to be updated to Address Unknown");

	jTextField2.setText("0");
	jTextField2.setEnabled(false);
	jTextField2.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jTextField2ActionPerformed(evt);
	    }
	});

	jLabel3.setText("Client Count");

	jTextField3.setEnabled(false);
	jTextField3.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jTextField3ActionPerformed(evt);
	    }
	});

	jLabel4.setText("Last processed date");

	jButton3.setText("Clear");
	jButton3.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jButton3ActionPerformed(evt);
	    }
	});

	jTable1.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
	jScrollPane1.setViewportView(jTable1);

	jLabel7.setText("Last extract date");

	jLastExtractDate.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jLastExtractDateActionPerformed(evt);
	    }
	});

	javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
	jPanel1.setLayout(jPanel1Layout);
	jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
		jPanel1Layout
			.createSequentialGroup()
			.addContainerGap()
			.addGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 667, Short.MAX_VALUE).addGroup(jPanel1Layout.createSequentialGroup().addGap(6, 6, 6).addComponent(jButton2).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jButton3)).addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jLabel4).addComponent(jLabel3)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
					.addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE).addGroup(jPanel1Layout.createSequentialGroup().addComponent(jLabel7).addGap(18, 18, 18).addComponent(jLastExtractDate, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)).addComponent(jLabel2)).addContainerGap()));
	jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
		jPanel1Layout.createSequentialGroup().addComponent(jLabel1).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jLabel7).addComponent(jLastExtractDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jLabel2).addGap(18, 18, 18).addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(jLabel3)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
			.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel4).addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jButton3).addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)).addContainerGap(70, Short.MAX_VALUE)));

	jMenu2.setText("File");

	jMenuItem3.setText("Exit");
	jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jMenuItem3ActionPerformed(evt);
	    }
	});
	jMenu2.add(jMenuItem3);

	jMenuBar1.add(jMenu2);

	jMenu1.setText("Address Unknown");

	jMenuItem1.setText("Bulk Processing");
	jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jMenuItem1ActionPerformed(evt);
	    }
	});
	jMenu1.add(jMenuItem1);

	jMenuItem2.setText("View processed");
	jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(java.awt.event.ActionEvent evt) {
		jMenuItem2ActionPerformed(evt);
	    }
	});
	jMenu1.add(jMenuItem2);

	jMenuBar1.add(jMenu1);

	setJMenuBar(jMenuBar1);

	javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
	getContentPane().setLayout(layout);
	layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
	layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

	pack();
    }// </editor-fold>
     // GEN-END:initComponents

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {
	// TODO add your handling code here:
    }

    private void jLastExtractDateActionPerformed(java.awt.event.ActionEvent evt) {
	// TODO add your handling code here:
    }

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {
	if (evt.getKeyCode() == KeyEvent.VK_F10) {
	    System.out.println("F10 pressed");
	}
    }

    private ClientMgr clientMgr;

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {

	Collection<ClientBulkUpdate> bulkUpdateList = null;
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Vector data = new Vector();
	try {
	    // client Number
	    if (jRadioButton1.isSelected()) {
		System.out.println("client");
		String val = jFormattedClientNumber.getText();
		System.out.println("val=" + val);
		if (val != null && !val.equals("")) {
		    // search for client
		    bulkUpdateList = clientMgr.getClientBulkUpdateByClient(new Integer(val));
		}
	    }
	    // date range
	    else if (jRadioButton2.isSelected()) {
		String start = jFormattedStartDate.getText();
		String end = jFormattedEndDate.getText();
		System.out.println("start=" + start);
		System.out.println("end=" + end);
		bulkUpdateList = clientMgr.getClientBulkUpdateByDate(new DateTime(start), new DateTime(end));
	    }

	    ClientVO client = null;
	    MembershipVO mem = null;
	    if (bulkUpdateList != null) {
		ClientBulkUpdate cbu = null;
		StringBuffer results = new StringBuffer();
		for (Iterator<ClientBulkUpdate> i = bulkUpdateList.iterator(); i.hasNext();) {

		    cbu = i.next();
		    Vector row = new Vector<String>();
		    row.add(cbu.getClientBulkUpdatePK().getClientNumber());
		    row.add(cbu.getClientBulkUpdatePK().getUpdateType());
		    row.add(cbu.getClientBulkUpdatePK().getCreateDate());

		    // lookup client status
		    client = clientMgr.getClient(cbu.getClientBulkUpdatePK().getClientNumber());
		    if (client != null) {
			row.add(client.getStatus().toUpperCase());
		    } else {
			System.err.println("No client found for " + cbu.getClientBulkUpdatePK().getClientNumber());
			row.add("");
		    }

		    // lookup product
		    mem = membershipMgr.findMembershipByClientAndType(cbu.getClientBulkUpdatePK().getClientNumber(), MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
		    if (mem != null) {
			row.add(mem.getProductCode());
		    } else {
			row.add("");
		    }

		    data.add(row);

		}
		System.out.println("len=" + results.length());

		Vector columnNames = getSearchColumnNames();
		ClientSearchTableModel tm = (ClientSearchTableModel) jTable2.getModel();
		tm.setDataVector(data, columnNames);

		tm.fireTableStructureChanged();
		tm.fireTableDataChanged();

		jSearchSummary.setText(countTable(data));

		System.out.println("xxxx");

	    } else {
		JOptionPane.showMessageDialog(null, "No clients found.", "Warning", JOptionPane.WARNING_MESSAGE);
	    }
	} catch (Exception e) {
	    JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    e.printStackTrace();
	}

    }

    private Vector getSearchColumnNames() {
	Vector columnNames = new Vector();
	columnNames.add("Client Number");
	columnNames.add("Update Type");
	columnNames.add("Create Date");
	columnNames.add("Client Status");
	columnNames.add("Product Code");
	return columnNames;
    }

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {
	System.exit(0);
    }

    void jFormattedStartDateActionPerformed(java.awt.event.ActionEvent evt) {
	// TODO add your handling code here:
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
	jDialog1.setVisible(false);
    }

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {

	DateTime lastCreateDate = resetLastCreateDate();
	if (lastCreateDate == null) {
	    lastCreateDate = new DateTime();
	}
	DateTime startDate = null;
	try {
	    lastCreateDate = lastCreateDate.add(new Interval(0, 0, 1, 0, 0, 0, 0));
	    startDate = lastCreateDate.subtract(new Interval(0, 0, 7, 0, 0, 0, 0));
	} catch (Exception e) {
	    e.printStackTrace();
	    startDate = null;
	}

	jFormattedStartDate.setText(startDate.formatShortDate());
	jFormattedEndDate.setText(lastCreateDate.formatShortDate());

	buttonGroup1.add(jRadioButton1);
	buttonGroup1.add(jRadioButton2);
	jPanel3.setBorder(BorderFactory.createTitledBorder("Search Options"));

	jDialog1.pack();
	jDialog1.setModal(true);
	jDialog1.setVisible(true);
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {

	DateTime lExtractDate = null;
	DateTime createDate = new DateTime();

	String lastExtractDate = jLastExtractDate.getText();
	if (lastExtractDate == null || lastExtractDate.equals("")) {
	    JOptionPane.showMessageDialog(null, "The last extract date must be entered", "Error", JOptionPane.ERROR_MESSAGE);
	    jLastExtractDate.requestFocus();
	    return;
	} else {
	    // validate last extract date
	    try {
		lExtractDate = new DateTime(lastExtractDate);
		if (lExtractDate.after(createDate.getDateOnly())) {
		    throw new Exception("Last extract date is after today.");
		}
	    } catch (Exception e) {
		JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		return;
	    }
	}

	AddressUnknownTableModel autm = (AddressUnknownTableModel) jTable1.getModel();

	Integer clientNumber = null;
	ClientVO client = null;
	ClientBulkUpdate bulkUpdate = null;
	ClientBulkUpdatePK bulkUpdatePK = null;

	if (autm.getValidRowCount() == 0) {
	    JOptionPane.showMessageDialog(null, "No clients to process.", "Warning", JOptionPane.WARNING_MESSAGE);
	} else {
	    // rows
	    for (int i = 0; i < autm.getRowCount(); i++) {
		System.out.println("date=" + new DateTime());
		Vector cols = autm.getRow(i);

		bulkUpdate = new ClientBulkUpdate();
		bulkUpdatePK = new ClientBulkUpdatePK();

		String name = (String) cols.get(1);

		if (name != null && !name.equals("")) {

		    // columns
		    for (int j = 0; j < cols.size(); j++) {
			Object o = autm.getValueAt(i, j);
			// client number
			if (j == 0) {
			    clientNumber = new Integer(o.toString());
			    System.out.println("clientNumber=" + clientNumber);

			}
			System.out.println("o=" + o);
		    }
		    bulkUpdatePK.setClientNumber(clientNumber);
		    bulkUpdatePK.setUpdateType(ClientBulkUpdatePK.TYPE_ADDRESS_UNKNOWN);
		    bulkUpdatePK.setCreateDate(createDate);
		    System.out.println("bulkUpdatePK=" + bulkUpdatePK);
		    bulkUpdate.setClientBulkUpdatePK(bulkUpdatePK);
		    bulkUpdate.setUserId(userId);
		    bulkUpdate.setUpdateStatus(ClientBulkUpdate.UPDATE_SUCCESS);
		    System.out.println("bulkUpdate=" + bulkUpdate);
		    try {
			client = clientMgr.getClient(clientNumber);
			System.out.println("client=" + client);
		    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }

		    // update client Record
		    client.setStatus(Client.STATUS_ADDRESS_UNKNOWN);
		    try {
			clientMgr.updateClient(client, user.getUserID(), Client.HISTORY_UPDATE, user.getSalesBranchCode(), "Address Unknown bulk update", ClientTransaction.SYSTEM_ROADSIDE);
			bulkUpdate.setUpdateStatus(ClientBulkUpdate.UPDATE_SUCCESS);
		    } catch (RemoteException e) {
			bulkUpdate.setUpdateStatus(ClientBulkUpdate.UPDATE_FAIL);
			// TODO Auto-generated catch block
			bulkUpdate.setMessage(e.getMessage());
			e.printStackTrace();
		    }

		    // save record
		    clientMgr.createClientBulkUpdate(bulkUpdate);

		}
		resetLastCreateDate();
	    }

	    // clear the table. Failure can be reprocessed when the conditions
	    // change.
	    autm.clearTable();

	    // reset counter
	    jTextField2.setText("0");
	}
    }

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
	AddressUnknownTableModel tm = (AddressUnknownTableModel) jTable1.getModel();
	if (tm.getValidRowCount() == 0) {
	    JOptionPane.showMessageDialog(null, "No clients to process.", "Warning", JOptionPane.WARNING_MESSAGE);
	} else {

	    int optionType = JOptionPane.OK_CANCEL_OPTION; // YES+NO+CANCEL
	    int messageType = JOptionPane.PLAIN_MESSAGE; // no standard icon
	    int res = JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the table?", "Warning", optionType, messageType, null);

	    // User hit OK
	    if (res == JOptionPane.OK_OPTION) {
		tm = (AddressUnknownTableModel) jTable1.getModel();
		tm.clearTable();
		jTextField2.setText("" + tm.getValidRowCount());
	    }
	    // User hit CANCEL
	    if (res == JOptionPane.CANCEL_OPTION) {
		System.out.println("CANCEL_OPTION");
	    }// User closed the window without hitting any buttonif (res ==
	     // JOptionPane.CLOSED_OPTION) { System.out.println( "CLOSED_OPTION"
	     // ); }

	}
    }

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {
	// TODO add your handling code here:
    }

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {
	// TODO add your handling code here:
    }

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {
	jPanel1.setVisible(true);
    }

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String args[]) {
	java.awt.EventQueue.invokeLater(new Runnable() {
	    public void run() {
		new ClientBulkProcessor().setVisible(true);
	    }
	});
    }

    // GEN-BEGIN:variables
    // Variables declaration - do not modify
    private javax.swing.ButtonGroup buttonGroup1;

    private javax.swing.JButton jButton1;

    private javax.swing.JButton jButton2;

    private javax.swing.JButton jButton3;

    private javax.swing.JButton jButton4;

    private javax.swing.JDialog jDialog1;

    private javax.swing.JFormattedTextField jFormattedClientNumber;

    private javax.swing.JFormattedTextField jFormattedEndDate;

    private javax.swing.JFormattedTextField jFormattedStartDate;

    private javax.swing.JLabel jLabel1;

    private javax.swing.JLabel jLabel2;

    private javax.swing.JLabel jLabel3;

    private javax.swing.JLabel jLabel4;

    private javax.swing.JLabel jLabel5;

    private javax.swing.JLabel jLabel6;

    private javax.swing.JLabel jLabel7;

    private javax.swing.JLabel jLabel8;

    private javax.swing.JFormattedTextField jLastExtractDate;

    private javax.swing.JMenu jMenu1;

    private javax.swing.JMenu jMenu2;

    private javax.swing.JMenuBar jMenuBar1;

    private javax.swing.JMenuItem jMenuItem1;

    private javax.swing.JMenuItem jMenuItem2;

    private javax.swing.JMenuItem jMenuItem3;

    private javax.swing.JPanel jPanel1;

    private javax.swing.JPanel jPanel2;

    private javax.swing.JPanel jPanel3;

    private javax.swing.JRadioButton jRadioButton1;

    private javax.swing.JRadioButton jRadioButton2;

    private javax.swing.JScrollPane jScrollPane1;

    private javax.swing.JScrollPane jScrollPane2;

    private javax.swing.JScrollPane jScrollPane3;

    private javax.swing.JTextArea jSearchSummary;

    private javax.swing.JTable jTable1;

    private javax.swing.JTable jTable2;

    private javax.swing.JTextField jTextField2;

    private javax.swing.JTextField jTextField3;

    // End of variables declaration//GEN-END:variables

    private String userId;

    public class AddressUnknownTableModelListener implements TableModelListener {

	public AddressUnknownTableModelListener() {

	}

	@Override
	public void tableChanged(TableModelEvent e) {
	    System.out.println("event=" + e);
	    AddressUnknownTableModel autm = (AddressUnknownTableModel) e.getSource();
	    int col = e.getColumn();
	    System.out.println("col=" + col);
	    int row = e.getLastRow();
	    System.out.println("row=" + row);

	    if (col < 0)
		return;

	    Object value = autm.getValueAt(row, col);
	    System.out.println("value=" + value);
	    Vector myRow = autm.getRow(row);
	    System.out.println("1");

	    if (col == 0 && value != null && !value.equals("")) {

		try {

		    // already in the table?
		    for (int i = 0; i < autm.getRowCount(); i++) {
			String val = (String) autm.getValueAt(i, 0);
			if (val.equals(value) && i != row) {
			    throw new Exception("Client " + value + " has already been added.");
			}
		    }

		    // get client name
		    System.out.println("2");
		    ClientVO client = clientMgr.getClient(new Integer(value.toString()));
		    if (client == null) {
			throw new Exception("No client found for " + value.toString() + ".");
		    }
		    if (!client.isActive()) {
			throw new Exception("Client has a status of " + client.getStatus() + " and may not be marked as Address Unknown");
		    }

		    DateTime lExtractDate = null;

		    String lastExtractDate = jLastExtractDate.getText();
		    if (lastExtractDate == null || lastExtractDate.equals("")) {
			JOptionPane.showMessageDialog(null, "The last extract date must be entered", "Error", JOptionPane.ERROR_MESSAGE);
			jLastExtractDate.requestFocus();
			return;
		    } else {
			// validate it
			try {
			    lExtractDate = new DateTime(lastExtractDate);
			    System.out.println("lExtractDate=" + lExtractDate);
			} catch (ParseException ex) {
			    JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			    return;
			}

		    }

		    System.out.println("lExtractDate=" + lExtractDate);
		    System.out.println("client.getLastUpdate()=" + client.getLastUpdate());

		    // if last client update after the nominated extract date
		    if (client.getLastUpdate() != null && client.getLastUpdate().after(lExtractDate)) {
			// state of the client history record
			ClientHistory clHist = clientMgr.getLastClientHistory(client.getClientNumber(), true);

			System.out.println("clHist=" + clHist);

			if (clHist != null) {

			    int optionType = JOptionPane.YES_NO_OPTION; // YES+NO
			    int messageType = JOptionPane.PLAIN_MESSAGE; // no
									 // standard
									 // icon

			    ClientVO prevClient = clHist.getClient();
			    System.out.println("prevClient=" + prevClient.getPostalAddress());
			    System.out.println("client=" + client.getPostalAddress());

			    // client postal address change
			    if (!prevClient.getPostalAddress().equals(client.getPostalAddress())) {
				System.out.println("postal address changed");
				int res = JOptionPane.showConfirmDialog(null, "Client address was updated from " + prevClient.getPostalAddress().getSingleLineAddress() + " to " + client.getPostalAddress().getSingleLineAddress() + " on " + client.getLastUpdate().formatShortDate() + ". Do you want to add this client to the list?", "Warning", optionType, messageType, null);
				if (res == JOptionPane.NO_OPTION) {
				    // clear client number
				    autm.setValueAt("", row, col);
				    jTable1.changeSelection(row, col, false, false);
				    jTable1.requestFocus();
				    return;
				}
			    }

			    // client status change
			    if (!prevClient.getStatus().equals(client.getStatus())) {
				System.out.println("status changed");
				int res = JOptionPane.showConfirmDialog(null, "Client status was updated from " + prevClient.getStatus() + " to " + client.getStatus() + " on " + client.getLastUpdate().formatShortDate() + ". Do you want to add this client to the list?", "Warning", optionType, messageType, null);
				if (res == JOptionPane.NO_OPTION) {
				    // clear client number
				    autm.setValueAt("", row, col);
				    jTable1.changeSelection(row, col, false, false);
				    jTable1.requestFocus();
				    return;
				}
			    }
			}
		    }

		    // add client to grid
		    if (client != null) {
			System.out.println("3");
			myRow.set(col + 1, client.getDisplayName());
			myRow.set(col + 2, client.getPostalAddress().getSingleLineAddress());
			myRow.set(col + 3, client.getStatus());
			System.out.println("4");
			// get the last row - any values already
			String val = (String) autm.getValueAt(autm.getRowCount() - 1, 2);
			if (val != null && !val.equals("")) {
			    System.out.println("5");
			    // add a blank row
			    autm.addRow("", "", "", "");

			    jTable1.changeSelection(row + 1, col, false, false);
			    jTable1.requestFocus();

			    System.out.println("6");

			}
			boolean group = client.getGroupClient();
			System.out.println("grp=" + group);
			if (group) {
			    String message = "Client is a group client. The following clients may need to be updated:";
			    Collection<ClientVO> clients = clientMgr.getClientGroup(client.getClientNumber());
			    for (Iterator<ClientVO> i = clients.iterator(); i.hasNext();) {
				message += FileUtil.NEW_LINE + i.next().getClientNumber();
			    }
			    JOptionPane.showMessageDialog(null, message, "Warning", JOptionPane.WARNING_MESSAGE);
			}

		    } else {
			System.out.println("NO CLIENT");
		    }
		} catch (Exception ex) {
		    JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

		    // clear client number
		    autm.setValueAt("", row, col);
		    jTable1.changeSelection(row, col, false, false);
		    jTable1.requestFocus();

		    // jTextField1.setText(ex.getMessage());
		    // TODO Auto-generated catch block
		    // ex.printStackTrace();
		}
		jTextField2.setText("" + (autm.getRowCount() - 1));
	    }

	    if (col == 4 && (Boolean) value) {
		// can't remove a blank row
		Object name = autm.getValueAt(row, 2);
		System.out.println("name=" + name);
		if (name != null && !name.equals("")) {
		    autm.removeRow(row);
		} else {
		    autm.setValueAt(new Boolean(false), row, 4);
		}
		jTextField2.setText("" + autm.getValidRowCount());
	    }
	}

    }

    private String countTable(Collection table) {
	StringBuffer desc = new StringBuffer();

	Vector row = null;

	Hashtable<String, Integer> clientCount = new Hashtable<String, Integer>();
	Hashtable<String, Integer> statusCount = new Hashtable<String, Integer>();
	Hashtable<String, Integer> productCount = new Hashtable<String, Integer>();

	for (Iterator<Vector> i = table.iterator(); i.hasNext();) {
	    row = i.next();

	    Integer clientNumber = (Integer) row.get(0);
	    String updateType = (String) row.get(1);
	    DateTime createDate = (DateTime) row.get(2);
	    String status = (String) row.get(3);
	    String product = (String) row.get(4);

	    incrementCounter(clientCount, clientNumber.toString());
	    incrementCounter(statusCount, status + FileUtil.SEPARATOR_COMMA + clientNumber.toString());
	    incrementCounter(productCount, product + FileUtil.SEPARATOR_COMMA + clientNumber.toString());
	}

	Set<String> clientKeys = clientCount.keySet();
	int clients = clientKeys.size();
	desc.append(StringUtil.rightPadString("Total Clients", 20) + ": " + clients + FileUtil.NEW_LINE);

	desc.append("Client Status Counts" + FileUtil.NEW_LINE);

	setDistinctClientCount(desc, statusCount);

	desc.append("Product Counts" + FileUtil.NEW_LINE);
	setDistinctClientCount(desc, productCount);
	// Set<String> productKeys = productCount.keySet();
	// for (Iterator<String> i = productKeys.iterator(); i.hasNext();)
	// {
	// String productKey = i.next();
	// Integer prCount = productCount.get(productKey);
	// desc.append(StringUtil.rightPadString(productKey, 20) + ": " +
	// prCount + FileUtil.NEW_LINE);
	// }

	return desc.toString();

    }

    private void setDistinctClientCount(StringBuffer desc, Hashtable<String, Integer> statusCount) {
	Set<String> statusKeys = statusCount.keySet();
	Hashtable<String, Integer> statusCount2 = new Hashtable<String, Integer>();
	for (Iterator<String> i = statusKeys.iterator(); i.hasNext();) {
	    String statusKey = i.next();
	    System.out.println("statusCount2=" + statusCount2);
	    System.out.println("statusKey=" + statusKey);
	    if (statusKey.indexOf(FileUtil.SEPARATOR_COMMA) > 0) {
		// includes comma
		String keyComp[] = statusKey.split(FileUtil.SEPARATOR_COMMA);
		// first component
		String key = keyComp[0];
		System.out.println("key=" + key);
		Integer val = statusCount2.get(key);
		System.out.println("val=" + val);
		if (val == null) {
		    statusCount2.put(key, 1);// initialise to 1
		} else {
		    statusCount2.put(key, val + 1);
		}
	    }

	}
	Set<String> statusKeys2 = statusCount2.keySet();
	for (Iterator<String> i = statusKeys2.iterator(); i.hasNext();) {
	    String key2 = i.next();
	    System.out.println("key2=" + key2);
	    Integer stCount = (Integer) statusCount2.get(key2);
	    System.out.println("stCount" + stCount);
	    desc.append(StringUtil.rightPadString(" " + key2, 20) + ": " + stCount + FileUtil.NEW_LINE);
	}
    }

    private void incrementCounter(Hashtable<String, Integer> counter, String key) {
	key = StringUtil.toProperCase(key);
	if (key == null || key.equals("")) {
	    key = "<Blank>";
	}
	Integer count = counter.get(key);
	if (count == null) {
	    counter.put(key, 1);
	} else {
	    counter.put(key, count + 1);
	}
    }

}