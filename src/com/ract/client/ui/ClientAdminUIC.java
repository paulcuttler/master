package com.ract.client.ui;

import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.ApplicationServlet;
import com.ract.common.ValidationException;

/**
 * UIC for admin work.
 * 
 * @author John Holliday
 * @version 1.0
 */

public class ClientAdminUIC extends ApplicationServlet {

    public void handleEvent_RepairClient_btnRepairDeceasedClientStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	clientMgr.updateDeceasedClientStatus();
	forwardRequest(request, response, ClientUIConstants.PAGE_REPAIR_CLIENT);
    }

    public void handleEvent_viewClientRepair(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	forwardRequest(request, response, ClientUIConstants.PAGE_REPAIR_CLIENT);
    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

}
