package com.ract.client.ui;

import java.util.Date;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class ClientSearchTableModel extends DefaultTableModel {

    public ClientSearchTableModel(Vector columnNames, int rowCount) {
	super(columnNames, rowCount);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
	return false;
    }

    @Override
    public Class getColumnClass(int columnIndex) {
	System.out.println("columnIndex=" + columnIndex);
	if (columnIndex == 0) {
	    return Integer.class;
	} else if (columnIndex == 2) {
	    return Date.class;
	} else {
	    // TODO Auto-generated method stub
	    return super.getColumnClass(columnIndex);
	}
    }

}
