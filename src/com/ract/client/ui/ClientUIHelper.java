package com.ract.client.ui;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.ServletException;

import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientHelper;
import com.ract.client.ClientHistory;
import com.ract.client.ClientMgr;
import com.ract.client.ClientOccupationVO;
import com.ract.client.ClientTitleVO;
import com.ract.client.ClientVO;
import com.ract.common.BranchVO;
import com.ract.common.MemoVO;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.HTMLUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * Helper class for setting up the view.
 * 
 * @author John Holliday
 * @created 1 August 2002
 * @version 1.0, 4/4/02
 */

public class ClientUIHelper {

    /**
     * find a client vo given a list of client vo's and a client number
     * 
     * @todo May cause problems as it caches the vo's
     * 
     * @param clientNumber
     *            client unique identifier
     * @param clientList
     *            the list of client vo's
     * @return The VO value
     */
    public static ClientVO getClient(Integer clientNumber, Collection clientList) {
	ClientVO temp = null;
	if (clientList != null) {
	    Iterator it = clientList.iterator();
	    // iterate through and match client numbers
	    while (it.hasNext()) {
		temp = (ClientVO) it.next();
		if (temp.getClientNumber().equals(clientNumber)) {
		    // should only be one match get out of loop as soon as match
		    // found
		    return temp;
		}
	    }
	}
	return null;
    }

    public static String formatBranchString(ClientVO client) {
	String branchString = "";
	BranchVO branchVO = null;
	try {
	    branchVO = client.getBranch();
	} catch (Exception e) {
	}
	if (branchVO != null) {
	    branchString += branchVO.getBranchNumber();
	    if (branchString.length() > 0) {
		branchString += " - ";
	    }
	    branchString += branchVO.getDescription();
	}
	return branchString;
    }

    public static String formatMemoString(ClientVO client) {
	String memoString = "";
	try {
	    MemoVO mvo = client.getMemo();
	    if (mvo != null && mvo.getMemoLine() != null) {
		memoString = mvo.getMemoLine();
	    }
	} catch (Exception ex) {
	    LogUtil.log("ClientUIHelper", "Unable to get memo for client. " + client.getClientNumber() + ex);
	}
	return memoString;
    }

    public static String formatMotorNewsString(ClientVO client) {
	String motorNewsMessage = "";
	if (Client.MOTOR_NEWS_NO_SEND.equalsIgnoreCase(client.getMotorNewsSendOption())) {
	    motorNewsMessage = "Does not receive Journeys Magazine";
	} else if (Client.MOTOR_NEWS_SEND.equalsIgnoreCase(client.getMotorNewsSendOption())) {
	    motorNewsMessage = "Always receives Journeys Magazine";
	    if (Client.MOTOR_NEWS_DELIVERY_METHOD_POSTAL.equalsIgnoreCase(client.getMotorNewsDeliveryMethod())) {
		motorNewsMessage += " by mail";
	    }
	} else if (Client.MOTOR_NEWS_MEMBERSHIP_SEND.equalsIgnoreCase(client.getMotorNewsSendOption())) {
	    motorNewsMessage = "Will receive Journeys Magazine, if membership is current";
	    if (Client.MOTOR_NEWS_DELIVERY_METHOD_POSTAL.equalsIgnoreCase(client.getMotorNewsDeliveryMethod())) {
		motorNewsMessage += ", by mail";
	    }
	}
	return motorNewsMessage;
    }

    public static String getMotorNewsSendOption(ClientVO client) {
	String motorNewsSendOption = null;
	if (client != null) {
	    motorNewsSendOption = client.getMotorNewsSendOption();
	}
	if (motorNewsSendOption == null || motorNewsSendOption.equals("")) {
	    motorNewsSendOption = Client.MOTOR_NEWS_MEMBERSHIP_SEND;
	}
	return motorNewsSendOption;
    }

    public static String getMotorNewsDeliveryMethod(ClientVO client) {
	String motorNewsDeliveryMethod = null;
	if (client != null) {
	    motorNewsDeliveryMethod = client.getMotorNewsDeliveryMethod();
	}
	if (motorNewsDeliveryMethod == null || motorNewsDeliveryMethod.equals("")) {
	    motorNewsDeliveryMethod = Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL;
	}
	return motorNewsDeliveryMethod;
    }

    public static String getBirthDateString(ClientVO client) {
	StringBuffer birthDateString = new StringBuffer();
	if (client != null) {
	    if (client.getBirthDate() != null) {
		birthDateString.append(client.getBirthDate().formatShortDate());
	    } else if (client.getBirthDateString() != null) {
		birthDateString.append(client.getBirthDateString());
	    }
	}
	return birthDateString.toString();
    }

    public static String formatAge(ClientVO client) {
	String ageDesc = "";
	if (client != null) {
	    Interval age = null;
	    try {
		age = client.getAge();
	    } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    if (age != null) {
		ageDesc = age.getTotalYears() + " yrs";
	    }
	}
	return ageDesc;
    }

    public static String historyMatch(String fieldName, Object current, Object previous) {
	String matchClass = "dataValue";
	if (current != null) {
	    if (previous == null) {
		return matchClass;
	    } else {
		if (current instanceof ClientHistory && previous instanceof ClientHistory) {
		    // Workaround for component issue.
		    ClientVO prevClient = ((ClientHistory) previous).getClient();
		    prevClient.setClientNumber(((ClientHistory) previous).getClientHistoryPK().getClientNumber());
		    ClientVO currentClient = ((ClientHistory) current).getClient();
		    currentClient.setClientNumber(((ClientHistory) current).getClientHistoryPK().getClientNumber());

		    if (!ClientHelper.fieldsMatch(fieldName, currentClient, prevClient)) {
			matchClass = "highlightRow";
		    }
		} else {
		    if (!ClientHelper.fieldsMatch(fieldName, current, previous)) {
			matchClass = "highlightRow";
		    }
		}
	    }
	}

	return matchClass;
    }

    /**
     * Gets the clientTitles attribute of the ClientUIHelper class
     * 
     * @param client
     *            Description of the Parameter
     * @param existingClient
     *            Description of the Parameter
     * @return The clientTitles value
     * @exception Exception
     *                Description of the Exception
     */
    public static String getClientTitles(ClientVO client) throws Exception {
	String clientTitle = null;
	if (client != null && client.getTitle() != null) {
	    clientTitle = client.getTitle().trim();
	}
	ClientMgr c = ClientEJBHelper.getClientMgr();
	Collection ct = c.getCachedClientTitles();
	Iterator it = ct.iterator();
	ClientTitleVO title = null;
	StringBuffer options = new StringBuffer();
	while (it.hasNext()) {
	    title = (ClientTitleVO) it.next();
	    if (title.getGroupId() == null || !title.getGroupId().booleanValue()) {

		options.append("<option value=\"");
		options.append(title.getTitleName());
		options.append("\" ");
		if (title.getTitleName().equalsIgnoreCase(clientTitle)) {
		    options.append("selected");
		}
		options.append(">");
		options.append(title.getTitleName());
		options.append("</option>");
		options.append(FileUtil.NEW_LINE);
	    }
	}
	return options.toString();
    }

    /**
     * Build html client summary table.
     * 
     * @param clientList
     *            Collection
     * @return String
     */
    public static String buildClientSummaryTable(Collection clientList) {
	StringBuffer htmlTable = new StringBuffer();
	htmlTable.append("<table>" + FileUtil.NEW_LINE);

	if (clientList != null && !clientList.isEmpty()) {
	    htmlTable.append("<tr class=\"headerRow\">");
	    htmlTable.append("<td/><td>Number</td><td>Name</td><td>Birth Date</td><td>Address</td><td>Status</td>");
	    htmlTable.append("</tr>");
	    ClientVO client = null;
	    Iterator it = clientList.iterator();
	    int row = 0;
	    // Object col = null;
	    String address = null;
	    DateTime birthDate = null;
	    while (it.hasNext()) {
		row++;
		client = (ClientVO) it.next();
		htmlTable.append("<tr class=\"" + HTMLUtil.getRowType(row) + "\">");
		htmlTable.append("<td>" + row + "</td>");
		htmlTable.append("<td><a href=\"viewClient.action?clientNumber=" + client.getClientNumber() + "\">" + client.getClientNumber() + "</a></td>");
		htmlTable.append("<td>" + client.getDisplayName() + "</td>");

		birthDate = client.getBirthDate();

		htmlTable.append("<td>" + (birthDate == null ? "" : birthDate.formatShortDate()) + "</td>");
		try {
		    if (client.getPostalAddress() != null) {
			address = client.getPostalAddress().getSingleLineAddress();
		    } else {
			address = "No postal address";
		    }
		} catch (RemoteException ex) {
		    address = ex.getMessage();
		}
		htmlTable.append("<td>" + address + "</td>");
		htmlTable.append("<td>" + formatClientStatusHTML(client.getStatus()) + "</td>");
		htmlTable.append(FileUtil.NEW_LINE);
		htmlTable.append("</tr>" + FileUtil.NEW_LINE);
	    }
	}
	htmlTable.append("</table>" + FileUtil.NEW_LINE);
	return htmlTable.toString();
    }

    /**
     * Gets the status attribute of the ClientUIHelper class
     * 
     * @param client
     *            Description of the Parameter
     * @return The status value
     */
    public static String getStatus(ClientVO client) {

	String clientStatus = "";
	if (client != null) {
	    clientStatus = client.getStatus();
	    // trim any whitespace
	    clientStatus = clientStatus.trim();
	}

	// System.out.println("client status: " + clientStatus);
	StringBuffer options = new StringBuffer();
	options.append("<option " + (clientStatus.equalsIgnoreCase(Client.STATUS_ACTIVE) ? "selected" : "") + ">" + Client.STATUS_ACTIVE + "</option>" + FileUtil.NEW_LINE);
	options.append("<option " + (clientStatus.equalsIgnoreCase(Client.STATUS_INACTIVE) ? "selected" : "") + ">" + Client.STATUS_INACTIVE + "</option>" + FileUtil.NEW_LINE);
	options.append("<option " + (clientStatus.equalsIgnoreCase(Client.STATUS_DECEASED) ? "selected" : "") + ">" + Client.STATUS_DECEASED + "</option>" + FileUtil.NEW_LINE);
	options.append("<option " + (clientStatus.equalsIgnoreCase(Client.STATUS_ADDRESS_UNKNOWN) ? "selected" : "") + ">" + Client.STATUS_ADDRESS_UNKNOWN + "</option>" + FileUtil.NEW_LINE);
	options.append("<option " + (clientStatus.equalsIgnoreCase(Client.STATUS_DUPLICATE) ? "selected" : "") + ">" + Client.STATUS_DUPLICATE + "</option>" + FileUtil.NEW_LINE);
	return options.toString();
    }

    public static String getGroupSelectList() throws Exception {
	return getGroupSelectList(new Boolean(false));
    }

    public static String getGroupSelectList(Boolean groupId) throws Exception {

	StringBuffer groupHTML = new StringBuffer();

	// LogUtil.log("","getMarketingSelectList = "+market);
	String values[] = { "", "No", "Yes" };
	groupHTML.append("<select name=\"group\">");
	boolean selected = false;
	String val = "";
	for (int i = 0; i < values.length; i++) {
	    selected = false;
	    // LogUtil.log("","values[i]"+values[i]);
	    if (groupId == null) {
		if (values[i].equals("")) {
		    selected = true;
		}
	    } else {
		if (groupId.booleanValue() == true) {
		    val = "Yes";
		} else {
		    val = "No";
		}

		if (values[i].equals(val)) {
		    selected = true;
		}
	    }
	    groupHTML.append("<option " + (selected ? "selected" : "") + " value=\"" + values[i] + "\">" + values[i] + "</option>");
	}
	groupHTML.append("</select>");
	return groupHTML.toString();
    }

    /**
     * Gets the occupations attribute of the ClientUIHelper class
     * 
     * @param client
     *            Description of the Parameter
     * @param existingClient
     *            Description of the Parameter
     * @return The occupations value
     * @exception Exception
     *                Description of the Exception
     */
    public static String getOccupations(ClientVO client) throws Exception {
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	Collection clientOccupationList = clientMgr.getClientOccupationList();
	Iterator it = clientOccupationList.iterator();
	ClientOccupationVO clientOccupation = null;
	StringBuffer options = new StringBuffer();
	options.append("<option value=\"\">");
	while (it.hasNext()) {
	    clientOccupation = (ClientOccupationVO) it.next();
	    options.append("<option ");
	    if (client != null && client.getClientNumber() != null) {
		if (client.getOccupation() != null) {
		    if (client.getOccupation().getOccupationID().equals(clientOccupation.getOccupationID())) {
			options.append("selected");
		    }
		}
	    }
	    options.append(" value=\"" + clientOccupation.getOccupationID() + "\">" + clientOccupation.getOccupation() + "</option>" + FileUtil.NEW_LINE);
	}
	return options.toString();
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     * @exception ServletException
     *                Description of the Exception
     */
    public static String selectGenderByTitle() throws ServletException {
	String html = "";
	try {
	    ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    Collection clientTitleList = clientMgr.getCachedClientTitles();
	    ClientTitleVO ctvo = null;
	    html += "function selectGenderByTitle() {" + FileUtil.NEW_LINE;
	    html += "var title = document.forms['client'].title.value;" + FileUtil.NEW_LINE;
	    if (clientTitleList != null) {
		Iterator it = clientTitleList.iterator();
		while (it.hasNext()) {
		    ctvo = (ClientTitleVO) it.next();
		    if (ctvo.getTitleName() == null || ctvo.getTitleName().equals("")) {

		    } else {
			if (ctvo.getSexSpecific() != null) {
			    if (ctvo.getSexSpecific().booleanValue()) {
				html += "if (title == '" + ctvo.getTitleName() + "') {" + FileUtil.NEW_LINE;
				if (ctvo.getSex() != null) {
				    if (ctvo.getSex().booleanValue() == true) {
					html += "  document.forms['client'].gender[0].checked = true;" + FileUtil.NEW_LINE;
				    } else {
					html += "  document.forms['client'].gender[1].checked = true;" + FileUtil.NEW_LINE;
				    }
				}
				html += "}" + FileUtil.NEW_LINE;
			    }
			}
		    }
		}
	    }
	    html += "}";
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}
	return html;
    }

    public static String formatClientStatusHTML(String status) {
	String formattedStatus = "";
	if (status != null) {
	    status = status.toUpperCase();

	    if (Client.STATUS_ACTIVE.equals(status)) {
		formattedStatus = "<span class=\"dataValueGreen\">" + status + "</span>";
	    } else if (Client.STATUS_ADDRESS_UNKNOWN.equals(status)) {
		formattedStatus = "<span class=\"dataValueAmber\">" + status + "</span>";
	    } else {
		formattedStatus = "<span class=\"dataValueRed\">" + status + "</span>";
	    }
	}
	return formattedStatus;
    }

    public static String getSMSSelectList(Boolean sms) throws Exception {
	StringBuffer smsHTML = new StringBuffer();
	Hashtable<String, String> smsAllowed = new Hashtable();
	smsAllowed.put("", "");
	smsAllowed.put("false", "No");
	smsAllowed.put("true", "Yes");

	boolean selected = false;
	String val = null;
	String name = null;
	String key = null;
	for (Enumeration<String> e = smsAllowed.keys(); e.hasMoreElements();) {
	    key = e.nextElement();
	    name = smsAllowed.get(key);

	    selected = false;
	    if (sms == null) {
		if (key.equals("")) {
		    selected = true;
		}
	    } else {
		if (sms.booleanValue() == true) {
		    val = "true";
		} else {
		    val = "false";
		}

		if (key.equals(val)) {
		    selected = true;
		}
	    }
	    smsHTML.append("<option " + (selected ? "selected" : "") + " value=\"" + key + "\">" + name + "</option>");
	}
	return smsHTML.toString();
    }
}
