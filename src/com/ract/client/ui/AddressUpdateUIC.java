package com.ract.client.ui;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ract.client.ClientBusiness;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.client.FollowUp;
import com.ract.client.FollowUpPK;
import com.ract.client.FollowupReportRequest;
import com.ract.common.ApplicationServlet;
import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.integration.IntegrationMgr;
import com.ract.common.integration.PopupRequest;
import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.ReportUtil;
import com.ract.util.ServletUtil;
import com.ract.util.StringUtil;

/**
 * UIC for admin work.
 * 
 * @author dgk
 * @version 1.0 16/05/2007
 */

public class AddressUpdateUIC extends ApplicationServlet {
    public void init() throws ServletException {
    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

    public void handleEvent_updateAddresses(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String responseString = "";
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	String clientNoStr = request.getParameter("clientNo");
	String createDateStr = request.getParameter("createDate");
	String tUserid = (String) request.getParameter("userid");
	String slsbch = (String) request.getParameter("slsbch");

	Integer clientNo = new Integer(clientNoStr);
	String data = request.getParameter("val");
	LogUtil.debug(this.getClass(), "\n    handleEvent_updateAddresses" + "\n    clientNo = " + clientNo + "\n    createDate = " + createDateStr + "\n    slsbch = " + slsbch + "\n    Data:- " + data);
	Hashtable business = new Hashtable();
	ClientMgrLocal clMgrLocal = null;
	ClientVO clt = null;
	ClientVO aClt = null;
	// FollowUp fu = null;
	// in here somewhere we need to update all the flagged clients
	// and add others to the 'update later' table.
	// unpack the returned data, etc
	try {
	    DateTime createDate = new DateTime(DateUtil.parseDate(createDateStr));
	    clMgrLocal = ClientEJBHelper.getClientMgrLocal();
	    clMgrLocal.getClientBusiness(clientNo, business); // add business
							      // for the
							      // originally
							      // changed client
							      // first
	    clt = clMgrLocal.getClient(clientNo);

	    JSONArray jArray = new JSONArray(data);
	    for (int xx = 0; xx < jArray.length(); xx++) {
		JSONObject jClt = jArray.getJSONObject(xx);
		Integer aCltNo = new Integer(jClt.get("clientNo") + "");
		String process = jClt.get("process") + "";
		if (process.equalsIgnoreCase("0")) {
		    // change client address
		    aClt = clMgrLocal.getClient(aCltNo);
		    aClt.setPostalAddress(clt.getPostalAddress());
		    aClt.setPostProperty(clt.getPostProperty());
		    aClt.setPostStreetChar(clt.getPostStreetChar());
		    aClt.setPostStsubId(clt.getPostStsubId());
		    aClt.setPostDpid(clt.getPostDpid());
		    /*
		     * do not update residential address if changed client is a
		     * group
		     */
		    if (!clt.getGroupClient()) {
			aClt.setResiProperty(clt.getResiProperty());
			aClt.setResiStreetChar(clt.getResiStreetChar());
			aClt.setResiStsubId(clt.getResiStsubId());
			aClt.setResiDpid(clt.getResiDpid());
			aClt.setResidentialAddress(clt.getResidentialAddress());
		    }
		    aClt.setHomePhone(clt.getHomePhone());
		    // aClt.setStatus(Client.STATUS_ACTIVE);
		    try {
			clMgrLocal.updateClient(aClt, tUserid, "Change Address", slsbch, "Updated with client " + clientNo, clt.getSourceSystem().toString());
		    } catch (RemoteException rex) {
			/*
			 * update has not happened. Set the follow up status to
			 * flag and continue
			 */
			LogUtil.warn(getClass(), "Address Updater failed to update clientNo: " + aCltNo + ", exception follows:");
			rex.printStackTrace();
			process = "1";
		    }
		    // get any business and add to list - any found will be
		    // added to the business hashtable
		    clMgrLocal.getClientBusiness(aCltNo, business);
		}
		// set status in followup table
		FollowUpPK fupk = new FollowUpPK();
		fupk.setChangedClientNo(clientNo);
		fupk.setSystemNumber(aCltNo);
		fupk.setSubSystem("client");
		fupk.setCreateDate(createDate);
		clMgrLocal.setFollowUpStatus(fupk, translateProcess(process));
	    }
	    Enumeration en = business.elements();
	    JSONObject ob = null;
	    JSONArray jData = new JSONArray();
	    while (en.hasMoreElements()) {
		ClientBusiness item = (ClientBusiness) en.nextElement();
		FollowUp fu = new FollowUp();
		fu.setCreateId(tUserid);
		fu.setNewPostalAddress(clt.getPostalAddress().getSingleLineAddress());
		if (!clt.getGroupClient()) {
		    fu.setNewStreetAddress(clt.getResidentialAddress().getSingleLineAddress());
		}
		fu.setNewHomePhone(clt.getHomePhone());
		FollowUpPK pk = new FollowUpPK();
		pk.setChangedClientNo(clientNo);
		pk.setCreateDate(createDate);
		pk.setSubSystem(item.getBusinessType());
		pk.setSystemNumber(item.getBusinessNumber());
		fu.setFollowUpPk(pk);
		fu.setCreateId(tUserid);
		fu.setCreateSlsbch(slsbch);
		fu.setStatus(FollowUp.STATUS_FLAG);
		LogUtil.log(this.getClass(), "fu=" + fu);
		clMgrLocal.createFollowUp(fu);

		ob = new JSONObject();
		ob.put("bOwner", item.getClientNo() + "");
		ob.put("bType", item.getBusinessType());
		ob.put("sysNumber", item.getBusinessNumber() + "");
		ob.put("description", item.getBusinessDesc());
		jData.put(ob);
	    }
	    // jBusiness.put(jData);
	    responseString = jData.toString();
	    // responseString = jBusiness.toString();
	    LogUtil.debug(this.getClass(), "\nhandleEvent_updateAddresses" + "\n\n" + responseString);
	    response.getWriter().write(responseString);
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    private JSONObject businessTableStructure() throws JSONException {
	JSONObject struct = new JSONObject();
	JSONArray headings = new JSONArray();
	JSONObject hding = null;

	hding = new JSONObject();
	hding.put("field", "bOwner");
	hding.put("label", "Owner");
	hding.put("type", "Integer");
	headings.put(hding);

	hding = new JSONObject();
	hding.put("field", "bType");
	hding.put("label", "Type");
	hding.put("type", "String");
	headings.put(hding);

	hding = new JSONObject();
	hding.put("field", "sysNumber");
	hding.put("type", "Integer");
	hding.put("label", "number");
	headings.put(hding);

	hding = new JSONObject();
	hding.put("field", "description");
	hding.put("label", "Description");
	hding.put("type", "String");
	headings.put(hding);

	hding = new JSONObject();
	hding.put("field", "process");
	hding.put("label", "Change?");
	headings.put(hding);

	struct.put("view", "main");
	struct.put("columns", headings);
	return struct;
    }

    public void handleEvent_flagBusinessAddressChange(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String responseString = "";
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	String clientNoStr = request.getParameter("clientNo");
	String createDateStr = request.getParameter("createDate");
	Integer clientNo = new Integer(clientNoStr);
	String data = request.getParameter("val");
	LogUtil.debug(this.getClass(), "\n    handleEvent_flagBusinessAddressChange" + "\n    clientNo = " + clientNo + "\n    createDate = " + createDateStr + "\n    Data:- " + data);
	ClientMgrLocal cltMgrLocal = null;

	try {
	    DateTime createDate = new DateTime(DateUtil.parseDate(createDateStr));
	    cltMgrLocal = ClientEJBHelper.getClientMgrLocal();
	    JSONArray jArray = new JSONArray(data);
	    for (int xx = 0; xx < jArray.length(); xx++) {
		JSONObject jClt = jArray.getJSONObject(xx);
		Integer sysNo = new Integer(jClt.get("sysNumber") + "");
		String subSys = jClt.get("bType") + "";
		String process = jClt.get("process") + "";
		if (process.equalsIgnoreCase("true"))
		    process = FollowUp.STATUS_FLAG;
		else
		    process = "No Change";
		// set status in followup table
		FollowUpPK fupk = new FollowUpPK();
		fupk.setChangedClientNo(clientNo);
		fupk.setSystemNumber(sysNo);
		fupk.setSubSystem(subSys);
		fupk.setCreateDate(createDate);
		cltMgrLocal.setFollowUpStatus(fupk, process);
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    private String translateProcess(String process) {
	if (process.equals("0")) {
	    return "Change";
	} else if (process.equals("1")) {
	    return FollowUp.STATUS_FLAG;
	} else if (process.equals("2")) {
	    return "No change";
	} else if (process.equals("3")) {
	    return "Duplicate";
	} else
	    return process;
    }

    public void handleEvent_loadFollowUp(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	/*
	 * userid:userid, salesBranch:salesBranch, show:show, fDate:fDate,
	 * lDate:lDate},
	 */

	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	String userId = request.getParameter("userid");
	String slsbch = request.getParameter("salesBranch");
	String fDateStr = request.getParameter("fDate");
	String lDateStr = request.getParameter("lDate");
	String status = request.getParameter("status");
	String result = request.getParameter("result");

	DateTime fDate = null;
	DateTime lDate = null;

	// System.out.println("handleEvent_loadFollowUp");
	// System.out.println("userid >>--------------> " + userId);
	// System.out.println("salesBranch >>---------> " + slsbch);
	// System.out.println("fDate >>---------------> " + fDateStr);
	// System.out.println("lDate >>---------------> " + lDateStr);
	// System.out.println("Status  >>-------------> " + status);
	// System.out.println("result >>--------------> " + result);

	ClientMgrLocal cltMgrLocal = null;
	ArrayList fList = null;
	JSONObject ob = null;
	JSONArray jData = new JSONArray();
	FollowUp fu = null;
	String responseString = null;
	String newAddr = null;
	boolean dupReq = false;
	try {
	    if (fDateStr == null || fDateStr.equals(""))
		fDate = null;
	    else
		fDate = new DateTime(fDateStr);
	    if (lDateStr == null || lDateStr.equals(""))
		lDate = null;
	    else {
		/*
		 * lDate will always be at time 0. To collect date with this
		 * date it is necessary to look for anything before midnight
		 * that date. In other words before 0 hours the next day. So add
		 * 1 day
		 */
		lDate = new DateTime(lDateStr);
		lDate = lDate.add(new Interval(0, 0, 1, 0, 0, 0, 0));
	    }
	    cltMgrLocal = ClientEJBHelper.getClientMgrLocal();
	    if (status.trim().equalsIgnoreCase("CHANGED"))
		status = "change";
	    if (status.trim().equalsIgnoreCase("Flag & Duplicate")) {
		dupReq = true;
		status = FollowUp.STATUS_FLAG;
	    }
	    if (result.trim().equalsIgnoreCase("to do"))
		result = "";
	    fList = cltMgrLocal.getFollowUpList(userId, fDate, lDate, status, slsbch, result);
	    if (dupReq) {
		status = "duplicate";
		ArrayList dList = cltMgrLocal.getFollowUpList(userId, fDate, lDate, status, slsbch, result);
		fList.addAll(dList);
	    }
	    for (int xx = 0; xx < fList.size(); xx++) {
		fu = (FollowUp) fList.get(xx);
		ob = new JSONObject();
		String st = fu.getStatus().trim().toUpperCase();
		if (st.equals("CHANGE"))
		    st = "CHANGED";
		ob.put("Id", StringUtil.padNumber(new Integer(xx), 3) + "");
		ob.put("status", st);
		ob.put("sType", fu.getSubSystem().trim().toUpperCase());
		ob.put("sNumber", fu.getSystemNumber() + "");
		ob.put("createId", fu.getCreateId());
		if (fu.getNewPostalAddress().equals(fu.getNewStreetAddress())) {
		    newAddr = fu.getNewPostalAddress();
		} else {
		    newAddr = "(Res) " + fu.getNewStreetAddress() + "<br>(Pst) " + fu.getNewPostalAddress();
		}
		if (fu.getSubSystem().trim().equalsIgnoreCase("Client")) {
		    ClientVO thisClt = cltMgrLocal.getClient(fu.getSystemNumber());
		    // ob.put("name",thisClt.getDisplayName());
		    ob.put("name", thisClt.getSurname() + ", " + thisClt.getGivenNames());
		}
		ob.put("newAddress", newAddr);
		ob.put("createDate", fu.getCreateDate().formatLongDate());
		ob.put("cClientNo", fu.getChangedClientNumber());
		if ((fu.getResult() == null || fu.getResult().trim().equals("")) && (fu.getStatus().trim().equalsIgnoreCase("DUPLICATE") || fu.getStatus().trim().equalsIgnoreCase(FollowUp.STATUS_FLAG))) {
		    String btnName = "result_" + xx;
		    String btnString = "<input type=\"radio\" name=\"" + btnName + "\" value=\"0\">" + "&nbsp;&nbsp;<input type=\"radio\" name=\"" + btnName + "\" value=\"1\">" + "&nbsp;&nbsp;<input type=\"radio\" name=\"" + btnName + "\" value=\"2\" checked=true>";
		    ob.put("process", btnString);
		} else {
		    ob.put("process", fu.getResult().trim().toUpperCase());
		}
		jData.put(ob);
	    }
	    responseString = jData.toString();
	    // LogUtil.debug(this.getClass(),"\nhandleEvent_updateAddresses -----------------------"
	    // + "\n\n" + responseString
	    // + "\n ---------------------------");
	    response.getWriter().write(responseString);

	} catch (Exception ex) {
	    throw new ServletException(ex + "");
	}
    }

    public void handleEvent_saveFollowUp(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String responseString = "";
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	String tUserid = (String) request.getParameter("userId");
	String data = (String) request.getParameter("data");

	LogUtil.debug(this.getClass(), "\n    handleEvent_updateAddresses" + "\n    logonId = " + tUserid + "\n    Data:- " + data);
	Hashtable business = new Hashtable();
	ClientMgrLocal clMgrLocal = null;
	FollowUp fu = null;
	FollowUpPK fupk;
	DateTime followupDate = new DateTime();
	JSONArray jArray = null;
	JSONObject fuOb = null;
	String sType = null;
	Integer sysNumber = null;
	String cdString = null;
	DateTime createDate = null;
	Integer cClient = null;
	String result = null;
	String resultString = null;

	// in here somewhere we need to update all the flagged clients
	// and add others to the 'update later' table.
	// unpack the returned data, etc
	try {

	    clMgrLocal = ClientEJBHelper.getClientMgrLocal();
	    jArray = new JSONArray(data);
	    for (int xx = 0; xx < jArray.length(); xx++) {
		fuOb = jArray.getJSONObject(xx);
		sType = (String) fuOb.get("sType");
		sysNumber = new Integer(fuOb.get("sysNumber") + "");
		cdString = (String) fuOb.get("createDate");

		createDate = new DateTime(cdString);
		cClient = new Integer(fuOb.get("cClient") + "");
		result = fuOb.get("result") + "";
		resultString = null;

		if (result.equalsIgnoreCase("0")) {
		    resultString = "CHANGED";
		} else if (result.equalsIgnoreCase("1")) {
		    resultString = "NO CHANGE";
		}
		// System.out.println("CreateDate =   " + cdString
		// + "\nresult =       " + result
		// + "\nresultString = " + resultString);

		// set status in followup table
		fupk = new FollowUpPK();
		fupk.setChangedClientNo(cClient);
		fupk.setSystemNumber(sysNumber);
		fupk.setSubSystem(sType);
		fupk.setCreateDate(createDate);
		clMgrLocal.setFollowUpResult(fupk, tUserid, followupDate, resultString);
	    }
	    responseString = "Results written to the database";
	} catch (Exception ex) {
	    ex.printStackTrace();
	    responseString = "Error saving results: \n" + ex;
	} finally {
	    try {
		response.getWriter().write(responseString);
	    } catch (IOException ex1) {
	    }
	}
    }

    public void handleEvent_showAddressFollowUp(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	forwardRequest(request, response, ClientUIConstants.PAGE_ADDRESS_FOLLOW_UP);
    }

    public void handleEvent_showAddressUpdater(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("clientNo", request.getParameter("clientNo"));
	forwardRequest(request, response, "/client/AddressUpdater");
    }

    public void handleEvent_getFollowUpDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	String responseString = null;
	try {
	    String sysType = request.getParameter("type");
	    Integer sysNo = new Integer(request.getParameter("sysNumber"));
	    Integer sClientNo = new Integer(request.getParameter("sClientNo"));
	    DateTime cDate = new DateTime(request.getParameter("cDate"));
	    // LogUtil.debug(this.getClass(),"handleEvent_getFollowUpDetail"
	    // + "\n sysNo = " + sysNo
	    // + "\n sysType = " + sysType
	    // + "\n sClientNo = " + sClientNo
	    // + "\n cDate = " + cDate);

	    ClientMgrLocal cltMgrLocal = ClientEJBHelper.getClientMgrLocal();
	    ClientVO sClt = cltMgrLocal.getClient(sClientNo);
	    String tString = null;
	    String sCltString = "<br><br>This change has been flagged because client " + sClientNo + " (" + sClt.getDisplayName() + ")" + " changed address " + "<br>&nbsp;&nbsp;&nbsp;&nbsp; New Residential Address: <b>" + sClt.getResidentialAddress().getSingleLineAddress() + "</b>" + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Postal Address:&nbsp;&nbsp;&nbsp;&nbsp;<b>" + sClt.getPostalAddress().getSingleLineAddress() + "</b>" + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Home Phone: <b>" + sClt.getHomePhone() + "</b>";

	    if (sysType.trim().equalsIgnoreCase("client")) {
		ClientVO tClt = cltMgrLocal.getClient(sysNo);
		tString = "This client may need to have the address(es) changed: " + "<b>" + sysNo + " " + tClt.getDisplayName() + "</b>" + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Residential Address: " + tClt.getResidentialAddress().getSingleLineAddress() + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Postal Address:&nbsp;&nbsp;&nbsp;&nbsp; " + tClt.getPostalAddress().getSingleLineAddress() + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Home Phone: <b>" + tClt.getHomePhone() + "</b>";
	    } else if (sysType.equalsIgnoreCase("INS")) {
		tString = "This insurance policy may need to have the address changed:" + "<br> Policy No: <b>" + sysNo + "</b>";
	    } else
		tString = "The address may need to be changed for " + sysType + "/" + sysNo;
	    responseString = tString + sCltString;
	    // LogUtil.debug(this.getClass(),"Response = \n" + responseString);
	    response.getWriter().write(responseString);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException(ex);
	}
    }

    public void handleEvent_printFollowUp(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	String userId = request.getParameter("userid");
	String slsbch = request.getParameter("salesBranch");
	String fDateStr = request.getParameter("fDate");
	String lDateStr = request.getParameter("lDate");
	String status = request.getParameter("status");
	String result = request.getParameter("result");

	DateTime fDate = null;
	DateTime lDate = null;
	UserSession session = this.getUserSession(request);
	User xUser = session.getUser();
	String userName = xUser.getUsername();
	String printGroup = session.getUser().getPrinterGroup();
	// UserMgr xMgr = UserEJBHelper.getUserMgr();
	// User xUser = xMgr.getUser(userId);
	ReportRequestBase rep = null;
	// System.out.println("handleEvent_printFollowUp");
	// System.out.println("userid >>--------------> " + userId);
	// System.out.println("salesBranch >>---------> " + slsbch);
	// System.out.println("fDate >>---------------> " + fDateStr);
	// System.out.println("lDate >>---------------> " + lDateStr);
	// System.out.println("Status  >>-------------> " + status);
	// System.out.println("result >>--------------> " + result);
	try {
	    if (fDateStr == null || fDateStr.equals(""))
		fDate = null;
	    else
		fDate = new DateTime(fDateStr);
	    if (lDateStr == null || lDateStr.equals(""))
		lDate = null;
	    else {
		/*
		 * lDate will always be at time 0. To collect date with this
		 * date it is necessary to look for anything before midnight
		 * that date. In other words before 0 hours the next day. So add
		 * 1 day
		 */
		lDate = new DateTime(lDateStr);
		lDate = lDate.add(new Interval(0, 0, 1, 0, 0, 0, 0));
	    }
	    rep = new FollowupReportRequest(userName, printGroup, userId, slsbch, fDate, lDate, status, result);
	    rep.setReportName("Address Update Report" + "." + FileUtil.EXTENSION_PDF);

	    // String reportKeyName = ReportRegister.addReport(rep);
	    // rep.setReportKeyName(reportKeyName);
	    // rep.constructURL();
	    // rep.addParameter("URL", rep.getReportDataURL());
	    //
	    // OutputStream fStream =
	    // FileUtil.openStreamForWriting(rep.getDestinationFileName(),
	    // false);
	    // rep.setOutputStream(fStream);
	    // new ReportGenerator().runReport(rep);
	    // ReportRegister.removeReport(reportKeyName);
	    //
	    // this.emailReport("Address Update Report",
	    // "AddressUpdateReport.pdf",
	    // rep.getDestinationFileName(),
	    // xUser.getUserEmailAddress(),
	    // "",
	    // xUser.getUserEmailAddress(),
	    // "Address update report is attached",
	    // false);
	    MailMessage message = new MailMessage();
	    message.setSubject("Address Update Report");
	    // message.

	    ReportUtil.emailReport(rep, false, message);
	} catch (Exception e) {
	    throw new SystemException("Unable to print report. ", e);
	}
    }

    public void handleEvent_uncompletedFollowUp(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	forwardRequest(request, response, ClientUIConstants.PAGE_UNCOMPLETED_FOLLOW_UP);

    }

    public void handleEvent_loadUncompleted(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	String slsbch = request.getParameter("salesBranch");
	String fDateStr = request.getParameter("fDate");
	String lDateStr = request.getParameter("lDate");
	String requestType = "cltAddressUpdate";
	DateTime fDate = null;
	DateTime lDate = null;

	// LogUtil.log(this.getClass()+"handleEvent_loadUncompleted",
	// "\nsalesBranch >>---------> " + slsbch
	// + "\nfDate >>---------------> " + fDateStr
	// + "\nlDate >>---------------> " + lDateStr);
	JSONObject ob = null;
	JSONArray jData = new JSONArray();
	PopupRequest pr = null;
	String pageUrl = null;
	String responseString = null;

	try {
	    UserSession userSession = ServletUtil.getUserSession(request, true);
	    String userID = userSession.getUserId();

	    if (fDateStr == null || fDateStr.equals(""))
		fDate = null;
	    else
		fDate = new DateTime(fDateStr);
	    if (lDateStr == null || lDateStr.equals(""))
		lDate = null;
	    else {
		lDate = new DateTime(lDateStr);
		// lDate = lDate.add(new Interval(0, 0, 1, 0, 0, 0, 0));
	    }
	    IntegrationMgr imgr = CommonEJBHelper.getIntegrationMgr();
	    ArrayList prList = imgr.getPopupRequests(requestType, slsbch, fDate, lDate);
	    ClientMgrLocal cltMgrLocal = ClientEJBHelper.getClientMgrLocal();
	    for (int xx = 0; xx < prList.size(); xx++) {
		pr = (PopupRequest) prList.get(xx);
		Integer clientNo = pr.getInt("clientNo");
		ClientVO clt = cltMgrLocal.getClient(clientNo);
		String link = "<a href=\"javascript:showUpdater(" + xx + "," + clientNo + "," + "'" + pr.getLogonId().trim() + "'," + "'" + userID + "'," + "'" + pr.getRequestNo() + "')\">" + clt.getPostalName() + "</a>";
		ob = new JSONObject();
		ob.put("id", xx + "");
		ob.put("clientNumber", clientNo + "");
		ob.put("createDate", pr.getRequestDate().formatShortDate());
		ob.put("logonId", pr.getLogonId());
		ob.put("name", link);
		ob.put("salesBranch", pr.getSalesBranch().toUpperCase());
		jData.put(ob);
	    }
	    responseString = jData.toString();
	    // System.out.println(">>>>>>" + responseString);
	    response.getWriter().write(responseString);

	} catch (Exception ex) {
	    throw new ServletException(ex + "");
	}
    }

}
