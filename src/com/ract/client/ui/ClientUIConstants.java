package com.ract.client.ui;

import com.ract.common.CommonConstants;

/**
 * Constants relating to the client system.
 * 
 * @todo consider using a properties file.
 * 
 * @author John Holliday
 * @version 1.0
 */

public class ClientUIConstants {

    public final static String ACTION_CREATE_CLIENT = "createClient";

    public final static String ACTION_UPDATE_CLIENT = "updateClient";

    public final static String ADDRESS_UNKNOWN = "Address not specified.";

    /**
     * URL pattern for Client UIC
     */
    public static final String PAGE_ClientUIC = "/ClientUIC";

    /**
     * URL pattern for Client Admin UIC
     */
    public static final String UIC_CLIENT_ADMIN = "/ClientAdminUIC";

    public static final String PAGE_REPAIR_CLIENT = CommonConstants.DIR_CLIENT + "/RepairClient.jsp";
    public static final String PAGE_ADDRESS_FOLLOW_UP = CommonConstants.DIR_CLIENT + "/AddressFollowUp.jsp";
    public static final String PAGE_UNCOMPLETED_FOLLOW_UP = CommonConstants.DIR_CLIENT + "/UncompletedFollowUp.jsp";

    public static final String PAGE_MARKET_OPTIONS = CommonConstants.DIR_CLIENT + "/MarketOptions.jsp";

    /**
     * JSP pages
     */
    public static final String PAGE_VIEW_CLIENT_DD_DETAILS = CommonConstants.DIR_CLIENT + "/viewClientDDDetails.jsp";

    public static final String PAGE_AdvancedSearchClient = CommonConstants.DIR_CLIENT + "/AdvancedSearchClient.jsp";

    public static final String PAGE_ViewOrganisation = CommonConstants.DIR_CLIENT + "/ViewOrganisation.jsp";

    // public static final String PAGE_ViewPerson = CommonConstants.DIR_CLIENT +
    // "/ViewPerson.jsp";

    public static final String PAGE_VIEW_CLIENT = CommonConstants.DIR_CLIENT + "/viewClient.jsp";

    // public static final String PAGE_ChangePerson = CommonConstants.DIR_CLIENT
    // + "/ChangePerson.jsp";

    // public static final String PAGE_ChangeOrganisation =
    // CommonConstants.DIR_CLIENT
    // + "/ChangeOrganisation.jsp";

    public static final String PAGE_ChangeAddressInclude = CommonConstants.DIR_CLIENT + "/ChangeAddressInclude.jsp";

    public static final String PAGE_ChangeClientAddress = CommonConstants.DIR_CLIENT + "/ChangeClientAddress.jsp";

    // public static final String PAGE_CreatePerson = CommonConstants.DIR_CLIENT
    // + "/CreatePerson.jsp";

    // public static final String PAGE_CREATE_CLIENT =
    // CommonConstants.DIR_CLIENT + "/CreateClient.jsp";

    // public static final String PAGE_EDIT_CLIENT = CommonConstants.DIR_CLIENT
    // + "/EditClient.jsp";

    // public static final String PAGE_CreateOrganisation =
    // CommonConstants.DIR_CLIENT
    // + "/CreateOrganisation.jsp";

    public static final String PAGE_ClientMatchList = CommonConstants.DIR_CLIENT + "/ClientMatchList.jsp";

    public static final String PAGE_ClientList = CommonConstants.DIR_CLIENT + "/ClientList.jsp";

    public static final String PAGE_ClientStats = CommonConstants.DIR_CLIENT + "/ClientStats.jsp";

    // public static final String PAGE_EditPersonInclude =
    // CommonConstants.DIR_CLIENT
    // + "/EditPersonInclude.jsp";

    // public static final String PAGE_EditOrganisationInclude =
    // CommonConstants.DIR_CLIENT
    // + "/EditOrganisationInclude.jsp";

    public static final String PAGE_ProductList = CommonConstants.DIR_CLIENT + "/ProductList.jsp";

    public static final String PAGE_SearchClientInclude = CommonConstants.DIR_CLIENT + "/SearchClientHeaderInclude.jsp";

    public static final String PAGE_ViewAddressInclude = CommonConstants.DIR_CLIENT + "/ViewAddressInclude.jsp";

    public static final String PAGE_ViewClientSummary = CommonConstants.DIR_CLIENT + "/ViewClientSummary.jsp";

    public static final String PAGE_EDITMOTORNEWS = CommonConstants.DIR_CLIENT + "/EditMotorNews.jsp";

    public static final String PAGE_MANAGE_SUBSCRIPTIONS = CommonConstants.DIR_CLIENT + "/ManageSubscriptions.jsp";
    public static final String PAGE_ClientSearchUIC = "/ClientSearchUIC";
    public static final String PAGE_ClientSearch = CommonConstants.DIR_CLIENT + "/ClientSearch.jsp";

    /*
     * search options for using switch case comparisons arbitrary index but
     * int's are fast to match on
     */
    public static final int SEARCHBY_DEFAULT = 1;

    public static final int SEARCHBY_CLIENT_NUMBER = 1;

    public static final int SEARCHBY_CLIENT_SURNAME = 2;

    public static final int SEARCHBY_MEMBERSHIP_NUMBER = 3;

    public static final int SEARCHBY_POLICY_NUMBER = 4;

    public static final int SEARCHBY_HOME_PHONE = 5;

    public static final int SEARCHBY_STSUBID_STREET_NUMBER = 6;

    public static final int SEARCHBY_ADDRESS = 7;

    public static final int SEARCHBY_IVR_NUMBER = 8;

    public static final int SEARCHBY_PAYABLE_ITEM_ID = 9;

    public static final int SEARCHBY_SMART_SEARCH = 10;

    public static final int SEARCHBY_GROUP_NAME = 11;

    public static final int SEARCHBY_MOBILE_PHONE = 12;

    public static final int SEARCHBY_DOCUMENT_REF = 13;

    public static final int SEARCHBY_MEMBERSHIP_CARD_NUMBER = 14;

    public static final int SEARCHBY_MEMBERSHIP_ID = 15;

    public static final int SEARCHBY_EMAIL_ADDRESS = 16;

    public static final int SEARCHBY_VIN = 17;

    public static final int SEARCHBY_REGO = 18;

    public static final String SEARCH_MODIFIER_CLIENT_NUMBER = "CN";

    public static final String SEARCH_MODIFIER_CLIENT_SURNAME = "CS";

    public static final String SEARCH_MODIFIER_MEMBERSHIP_NUMBER = "MN";

    public static final String SEARCH_MODIFIER_POLICY_NUMBER = "PN";

    public static final String SEARCH_MODIFIER_HOME_PHONE = "HP";

    public static final String SEARCH_MODIFIER_WORK_PHONE = "WP";

    public static final String SEARCH_MODIFIER_IVR_NUMBER = "IVR";

    public static final String SEARCH_MODIFIER_PAYABLE_ITEM_ID = "PI";

    public static final String ADDRESS_SEARCH_DELIMITER = ",";

}
