package com.ract.client;

import java.rmi.RemoteException;

import org.w3c.dom.Node;

/**
 * An implementation of a Person type client.
 * 
 * @hibernate.subclass discriminator-value="PERSON"
 * 
 * @author hollidayj created 31 July 2002
 */

public class PersonVO extends ClientVO {

    public PersonVO(Node clientNode) throws RemoteException {
	super(clientNode);
    }

    public PersonVO() {
	this.clientType = ClientVO.CLIENT_TYPE_PERSON;
    }

    public String getSortableDisplayName() {
	StringBuffer dName = new StringBuffer();
	if (this.surname != null && this.surname.trim().length() > 0) {

	    dName.append(this.surname.trim());

	    if (this.givenNames != null) {
		dName.append(", ");
		dName.append(this.givenNames.trim());
	    }

	}

	if (dName.length() > 0) {
	    return dName.toString().toUpperCase();
	} else {
	    return "??? UNKNOWN ???";
	}
    }

    public String getDisplayName() {
	return ClientHelper.formatCustomerName(this.title, this.givenNames, this.surname, true, false, true, true).toUpperCase();
    }

}
