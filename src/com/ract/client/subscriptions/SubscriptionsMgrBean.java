package com.ract.client.subscriptions;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientSubscription;
import com.ract.common.CommonConstants;
import com.ract.common.SystemParameterVO;
import com.ract.util.ConnectionUtil;
import com.ract.util.LogUtil;

@Stateless
@Remote({ SubscriptionsMgr.class })
@Local({ SubscriptionsMgrLocal.class })
public class SubscriptionsMgrBean {
    @Resource
    private SessionContext sessionContext;

    @Resource(mappedName = "java:/ClientDS")
    public DataSource dataSource;

    @EJB
    private ClientMgrLocal clientMgrLocal;

    @EJB
    private SubscriptionsMgrLocal subscriptionsMgrLocal;

    public void processUnsubscribeFile(String inputFile, String dateToProcess) throws RemoteException {

	Map<String, String> fieldNames = getSubscriptionsCSVFieldNames();

	CSVReader reader = null;
	try {
	    // Open the Subscriptions file
	    reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));

	    String[] line = null;

	    // Process the Subscriptions file line by line
	    while ((line = reader.readNext()) != null) {
		subscriptionsMgrLocal.processUnsubscribes(line, fieldNames);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    LogUtil.log(this.getClass(), "Fatal error has prevented the completion of the processing the Subscriptions file. " + e);
	    throw new RemoteException("Fatal error has prevented the completion of the processing the Subscriptions file.", e);
	} finally {
	    try {
		reader.close();
	    } catch (Exception e) {
		// ignore
	    }
	}

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void processUnsubscribes(String[] line, Map<String, String> fieldNames) {

	Map<String, String> subscriptionDetails = parseInputLine(line, fieldNames);

	processThisSubscription(subscriptionDetails);
    }

    final String SUBSCRIPTION_GENERAL = "General";

    private void processThisSubscription(Map<String, String> subscriptions) {
	cancelClientSubscription(subscriptions);
    }

    private void cancelClientSubscription(Map<String, String> subscriptions) {
	Integer clientNumber = null;
	try {
	    clientNumber = new Integer((String) subscriptions.get("ClientNumber"));
	    ClientSubscription generalSubscription = clientMgrLocal.getActiveClientSubscription(clientNumber, SUBSCRIPTION_GENERAL);
	    clientMgrLocal.deleteClientSubscription(generalSubscription.getClientSubscriptionPK());
	} catch (Exception ex) {
	    LogUtil.log(this.getClass(), "Can't cancel subscription for " + subscriptions.get("ClientNumber") + ". " + ex.getMessage());
	}
    }

    private Map<String, String> getSubscriptionsCSVFieldNames() {

	String sql = "select [tab-name], [tab-key], [tab-desc], [tab-log], ";
	sql += "[tab-date], [tab-dec0], [tab-dec4] from pub.[gn-table] ";
	sql += "where [tab-name] = ? ";
	sql += "and [tab-key] like ? ";

	Connection connection = null;
	PreparedStatement statement = null;
	Map<String, String> fieldNames = new HashMap<String, String>();
	String val;
	String fldName = "fldName";
	Integer i;
	int y = 0;

	try {
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(sql);

	    statement.setString(1, SystemParameterVO.CATEGORY_SUBSCRIPTIONS);
	    statement.setString(2, fldName + CommonConstants.WILDCARD);

	    ResultSet rs = statement.executeQuery();

	    /*
	     * TAB-KEY have the value fldNameNNN where fieldName is the literal
	     * fieldName and NNN is the fieldnumber e.g. 003
	     */

	    while (rs.next()) {
		val = rs.getString(2);

		i = new Integer(val.substring(fldName.length()));

		fieldNames.put(i.toString(), rs.getString(3));
	    }
	} catch (SQLException ex) {
	    LogUtil.log(this.getClass(), "Getting field names " + ex.getMessage());
	    fieldNames = null;
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return fieldNames;
    }

    /**
     * parses a line from the Subscriptions file from Campaign Monitor. If there
     * were no errors, it returns a Subscription details map, otherwise a
     * SubscriptionsFileFormatException is thrown
     */
    private Map<String, String> parseInputLine(String[] line, Map<String, String> fieldNames) {
	Map<String, String> fields = new HashMap<String, String>();

	for (int i = 0; i < line.length; i++) {
	    fields.put((fieldNames.get(new Integer(i + 1).toString())), line[i]);
	}
	LogUtil.debug(this.getClass(), "fields=" + fields.toString());
	return fields;
    }

}
