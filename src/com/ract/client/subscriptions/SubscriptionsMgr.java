package com.ract.client.subscriptions;

import java.rmi.RemoteException;

import javax.ejb.Remote;

/**
 * Description of the Interface
 * 
 */
@Remote
public interface SubscriptionsMgr {
    /**
    */
    public void processUnsubscribeFile(String inputFile, String dateToProcess) throws RemoteException;

}
