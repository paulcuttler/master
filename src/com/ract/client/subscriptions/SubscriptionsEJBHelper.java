package com.ract.client.subscriptions;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

public class SubscriptionsEJBHelper {

    public static SubscriptionsMgr getSubscriptionsMgr() {
	SubscriptionsMgr subscriptionsMgr = null;
	try {
	    subscriptionsMgr = (SubscriptionsMgr) ServiceLocator.getInstance().getObject("SubscriptionsMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return subscriptionsMgr;
    }

}
