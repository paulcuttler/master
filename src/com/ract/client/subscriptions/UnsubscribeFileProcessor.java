package com.ract.client.subscriptions;

import java.util.Date;

import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.LogUtil;

/**
 * Unsubscribe file processor.
 * 
 * @author Leigh Giles
 * @version 1.0
 */

public class UnsubscribeFileProcessor {

    public UnsubscribeFileProcessor(String URL, String inputFile, String dateToProcess) {
	LogUtil.log(this.getClass(), URL);
	String rmi[] = URL.split(":");

	this.UpdateSubscriptions(rmi[0], rmi[1], inputFile, dateToProcess);
    }

    public UnsubscribeFileProcessor(String host, String port, String inputFile, String dateToProcess) {
	this.UpdateSubscriptions(host, port, inputFile, dateToProcess);
    }

    /**
     * 
     * @param host
     *            String
     * @param port
     *            String
     * @param inputFile
     *            String
     * @param dateToProcess
     *            String
     */

    private void UpdateSubscriptions(String host, String port, String inputFile, String dateToProcess) {

	SubscriptionsMgr SubscriptionsMgr = null;
	try {

	    RACTPropertiesProvider.setContextURL(host, Integer.parseInt(port));
	    System.out.println("Got Context");
	    SubscriptionsMgr = SubscriptionsEJBHelper.getSubscriptionsMgr();

	} catch (Exception ne) {
	    ne.printStackTrace();
	    System.out.println("Failed initializing access to Subscriptions Manager home interface: " + ne.getMessage());

	}

	try {
	    System.out.println(" ======================================================== ");
	    System.out.println(" Host                 " + host);
	    System.out.println(" Port                 " + port);
	    System.out.println(" Subscriptions file   " + inputFile);
	    System.out.println(" Date to process      " + dateToProcess);
	    System.out.println(" Starting             " + new Date().toString());
	    System.out.println(" ======================================================== ");

	    SubscriptionsMgr.processUnsubscribeFile(inputFile, dateToProcess);

	    System.out.println(" Finished             " + new Date().toString());

	} catch (Exception e) {
	    // doesn't throw an exception for other classes to respond to
	    System.out.println("SubscriptionsUpdateReader +++++++ Exception " + e.getMessage());
	    System.out.println("SubscriptionsUpdateReader +++++++ FINISHING");
	}

    }

    /**
     * The main program for the SubscriptionsUpdateReader class
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args) {

	if (args.length < 4) {
	    System.out.println("Not enough command line arguments.");
	    System.out.println("Usage: SubscriptionsUpdateReader [localhost] [1099] [input file] [date to process dd/mm/yy]");
	    return;
	}
	UnsubscribeFileProcessor subs = new UnsubscribeFileProcessor(args[0], args[1], args[2], args[3]);
    }

}
