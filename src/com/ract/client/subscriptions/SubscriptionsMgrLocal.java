package com.ract.client.subscriptions;

import java.util.Map;

import javax.ejb.Local;

@Local
public interface SubscriptionsMgrLocal {
    public void processUnsubscribes(String[] line, Map<String, String> fieldNames);
}
