package com.ract.client;

import com.ract.common.ExceptionHelper;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;

/**
 * Run the program that creates a list of obsolete clients for deleting.
 * 
 * @author jyh
 * @version 1.0
 */

public class ObsoleteClientListRunner {
    public static void main(String[] args) {
	if (args.length != 4) {
	    System.out.println("Usage: ObsoleteClientListRunner localhost 1099 1/7/2001 /tmp/client/delete.txt");
	    return;
	}

	String hostName = args[0];
	int portNumber = Integer.parseInt(args[1]);
	String lastTransactionDateString = args[2];

	DateTime lastTransactionDate = null;
	try {
	    lastTransactionDate = new DateTime(lastTransactionDateString);
	} catch (Exception ex1) {
	    System.err.println(ExceptionHelper.getExceptionStackTrace(ex1));
	    return;
	}
	System.out.println("lastTransactionDate=" + lastTransactionDate.formatShortDate());
	String fileName = args[3];
	System.out.println("fileName=" + fileName);
	RACTPropertiesProvider.setContextURL(hostName, portNumber);
	String obsoleteClientList = null;
	try {
	    ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    obsoleteClientList = clientMgr.getObsoleteClientList(lastTransactionDate);
	    // write the file
	    FileUtil.writeFile(fileName, obsoleteClientList);
	} catch (Exception ex) {
	    System.err.println(ExceptionHelper.getExceptionStackTrace(ex));
	    return;
	}

    }

}
