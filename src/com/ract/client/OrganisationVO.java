package com.ract.client;

import java.rmi.RemoteException;

import org.w3c.dom.Node;

/**
 * An implementation of an Organisation type client.
 * 
 * @hibernate.subclass discriminator-value="ORGANISATION"
 * 
 * 
 * @author hollidayj
 */
public class OrganisationVO extends ClientVO
// implements Writable
{

    public OrganisationVO() {
	this.clientType = ClientVO.CLIENT_TYPE_ORGANISATION;
    }

    public OrganisationVO(Node clientNode) throws RemoteException {
	super(clientNode);
    }

    public String getDisplayName() {
	StringBuffer dName = new StringBuffer();
	if (this.surname != null) {
	    dName.append(this.surname.trim());
	} else {
	    return "??? UNKNOWN ???";
	}
	return dName.toString();
    }

    public String getSortableDisplayName() {
	return getDisplayName();
    }

    // public final static String WRITABLE_CLASSNAME = "Client";
    //
    // /**
    // * attention prefix for contact person
    // */
    // private final String ATTENTION_TEXT = "ATT: ";
    //
    // /**
    // * unknown text
    // */
    // private final String UNKNOWN = "??????????";
    //
    // private String homePhone;
    //
    // private String workPhone;
    //
    // private String mobilePhone;
    //
    // //this is a legacy of the old system
    // private Boolean sex;
    //
    // /**
    // *@roseuid 3C6358D100A9
    // */
    // public OrganisationVO()
    // {}
    //
    // /**
    // * Gets the salutation attribute of the OrganisationVO object
    // *
    // *@return The salutation value
    // */
    // public String getSalutation()
    // {
    // if(this.salutation == null)
    // {
    // this.setSalutation();
    // }
    // return this.salutation;
    // }
    //
    // /**
    // *@todo salutation
    // */
    // private void setSalutation()
    // {
    // this.salutation = this.getAddressTitle();
    // }
    //
    // /**
    // * Construct the client using the data in the XML DOM node. Set the mode
    // to
    // * "History" and set the value object to read only.
    // *
    // *@param clientNode Description of the Parameter
    // *@exception CreateException Description of the Exception
    // *@todo implement filling vehicle list from XML
    // */
    // public OrganisationVO(Node clientNode)
    // throws SystemException
    // {
    // if(clientNode == null)
    // {
    // throw new SystemException(
    // "Failed to create client history from XML node. The node is not valid.");
    // }
    //
    // NodeList elementList = clientNode.getChildNodes();
    // Node elementNode = null;
    // Node writableNode;
    // String nodeName = null;
    // String valueText = null;
    // for(int i = 0; i < elementList.getLength(); i++)
    // {
    // elementNode = elementList.item(i);
    // nodeName = elementNode.getNodeName();
    //
    // writableNode = XMLHelper.getWritableNode(elementNode);
    // valueText = elementNode.hasChildNodes() ?
    // elementNode.getFirstChild().getNodeValue() : null;
    //
    // if("ABN".equals(nodeName) && valueText != null)
    // {
    // this.ABN = new Integer(valueText);
    // }
    // else if("addressTitle".equals(nodeName) && valueText != null)
    // {
    // this.addressTitle = valueText;
    // }
    /*
     * else if ("branchNumber".equals(nodeName) && valueText != null) {
     * this.branchNumber = new Integer(valueText); }
     */
    // else if("clientNumber".equals(nodeName) && valueText != null)
    // {
    // this.clientNumber = new Integer(valueText);
    // }
    // else if("clientType".equals(nodeName) && valueText != null)
    // {
    // this.clientType = valueText;
    // }
    // else if("contactPersonName".equals(nodeName) && valueText != null)
    // {
    // this.contactPersonName = valueText;
    // }
    // else if("contactPersonPhone".equals(nodeName) && valueText != null)
    // {
    // this.contactPersonPhone = valueText;
    // }
    // else if("lastUpdate".equals(nodeName) && valueText != null)
    // {
    // try
    // {
    // this.lastUpdate = new DateTime(valueText);
    // }
    // catch(ParseException pe)
    // {
    // throw new SystemException("The last date is not a valid date : " +
    // pe.getMessage());
    // }
    // }
    // else if("madeDate".equals(nodeName) && valueText != null)
    // {
    // try
    // {
    // this.madeDate = new DateTime(valueText);
    // }
    // catch(ParseException pe)
    // {
    // throw new SystemException("The made date is not a valid date : " +
    // pe.getMessage());
    // }
    // }
    // else if("madeID".equals(nodeName) && valueText != null)
    // {
    // this.madeID = valueText;
    // }
    /*
     * else if ("memo".equals(nodeName) && valueText != null) { this.memo =
     * valueText; }
     */
    //
    // else if("organisationFax".equals(nodeName) && valueText != null)
    // {
    // this.organisationFax = valueText;
    // }
    // else if("organisationName".equals(nodeName) && valueText != null)
    // {
    // this.organisationName = valueText;
    // }
    // else if("organisationPhone".equals(nodeName) && valueText != null)
    // {
    // this.organisationPhone = valueText;
    // }
    // else if("postalName".equals(nodeName) && valueText != null)
    // {
    // this.postalName = valueText;
    // }
    // else if("salutation".equals(nodeName) && valueText != null)
    // {
    // this.salutation = valueText;
    // }
    // else if("status".equals(nodeName) && valueText != null)
    // {
    // this.status = valueText;
    // }
    // else if("postalAddress".equals(nodeName) && writableNode != null)
    // {
    // this.postalAddress = AddressFactory.getPostalAddressVO(writableNode);
    // }
    // else if("residentialAddress".equals(nodeName) && writableNode != null)
    // {
    // this.streetAddress = AddressFactory.getStreetAddressVO(writableNode);
    // }
    // else if("vehicleList".equals(nodeName))
    // {
    //
    // }
    // }
    // setReadOnly(true);
    // try
    // {
    // setMode(MODE_HISTORY);
    // }
    // catch(ValidationException ve)
    // {
    // // Should be setting it to a valid value so pass the exception
    // // back as a system exception
    // throw new SystemException(ve.getMessage());
    // }
    // }
    //
    // /**
    // * The display name is a munged form of the clients name that can be used
    // * in a single line screen display. As a last resort return the string
    // * "??????".
    // *
    // *@return String
    // *@roseuid 3C5DC17702AF
    // */
    // public String getDisplayName()
    // {
    // StringBuffer oName = new StringBuffer();
    // if(this.organisationName != null)
    // {
    // oName.append(this.organisationName);
    // oName.append(" ");
    // }
    // if(oName.length() == 0)
    // {
    // oName.append(UNKNOWN);
    // }
    //
    // return oName.toString();
    // }
    //
    // /**
    // * Gets the clientType attribute of the OrganisationVO object
    // *
    // *@return The clientType value
    // */
    // public String getClientType()
    // {
    // return ClientVO.CLIENT_TYPE_ORGANISATION;
    // }
    //
    // /**
    // * Gets the postalName attribute of the OrganisationVO object
    // *
    // *@return The postalName value
    // */
    // public String getPostalName()
    // {
    // StringBuffer oName = new StringBuffer();
    // if(this.organisationName != null)
    // {
    // oName.append(this.organisationName);
    // oName.append(" ");
    // }
    // if(this.contactPersonName != null)
    // {
    // oName.append("(");
    // oName.append(ATTENTION_TEXT);
    // oName.append(this.contactPersonName);
    // oName.append(")");
    // }
    // if(oName.length() == 0)
    // {
    // oName.append(UNKNOWN);
    // }
    //
    // return oName.toString();
    // }
    //
    // /**
    // * Return a copy of the Client as an OrganisationVO object.
    // *
    // *@return Client_lv.OrganisationVO
    // *@exception CloneNotSupportedException Description of the Exception
    // *@roseuid 3C5DC26402A6
    // */
    // public ClientVO copy()
    // throws CloneNotSupportedException
    // {
    // OrganisationVO vo = null;
    // vo = (OrganisationVO)this.clone();
    // return(ClientVO)vo;
    // }
    //
    // /**
    // * Return a new Organisation value object.
    // *
    // *@return Client_lv.OrganisationVO
    // *@roseuid 3C5F18F5013C
    // */
    // public ClientVO create()
    // {
    // return(ClientVO)new OrganisationVO();
    // }/**
    // * Gets the addressTitle attribute of the OrganisationVO object
    // *
    // *@return The addressTitle value
    // */
    // public String getAddressTitle()
    // {
    // return this.addressTitle;
    // }
    //
    // /**
    // * Sets the addressTitle attribute of the OrganisationVO object
    // */
    // public void setAddressTitle()
    // {
    //
    // StringBuffer aName = new StringBuffer();
    // if(this.contactPersonName != null)
    // {
    // aName.append(StringUtil.toProperCase(this.contactPersonName));
    // }
    //
    // if(aName.length() > 0)
    // {
    // this.addressTitle = aName.toString();
    // }
    // else
    // {
    // //no contact
    // this.addressTitle = this.DEFAULT_SALUTATION;
    // }
    //
    // }
    //
    // /**
    // * set postal name in the form COMPANY NAME, ATT: CONTACT PERSON
    // */
    // public void setPostalName()
    // {
    // StringBuffer pName = new StringBuffer();
    // pName.append(this.organisationName);
    // pName.append(", ");
    // pName.append(ATTENTION_TEXT);
    // pName.append(this.contactPersonName);
    // this.postalName = pName.toString();
    // }
    //
    // /**
    // * Description of the Method
    // *
    // *@return Description of the Return Value
    // */
    // public String toString()
    // {
    // return this.getDisplayName();
    // }
    //
    //
    // /*
    // public String getWritableClassName()
    // {
    // return this.WRITABLE_CLASSNAME;
    // }
    //
    //
    // public void write(ClassWriter cw)
    // throws RemoteException
    // {
    // cw.startClass(WRITABLE_CLASSNAME);
    //
    // if(this.ABN != null && this.ABN.intValue() > 0)
    // {
    // cw.writeAttribute("ABN", this.ABN);
    // }
    //
    // cw.writeAttribute("branchNumber", this.branchNumber);
    // cw.writeAttribute("clientNumber", this.clientNumber);
    // cw.writeAttribute("clientType", this.getClientType());
    // cw.writeAttribute("displayName", this.getDisplayName());
    // cw.writeAttribute("contactPersonName", this.contactPersonName);
    // cw.writeAttribute("contactPersonPhone", this.contactPersonPhone);
    // cw.writeAttribute("faxNumber", this.faxNumber);
    // cw.writeAttribute("lastUpdate", this.lastUpdate);
    // cw.writeAttribute("madeDate", this.madeDate);
    // cw.writeAttribute("madeID", this.madeID);
    //
    // if(this.getMemo() != null && this.getMemo().getMemoLine().length() > 0)
    // {
    // cw.writeAttribute("memo", this.getMemo().getMemoLine());
    // }
    // if(this.motorNewsDeliveryMethod != null &&
    // this.motorNewsDeliveryMethod.length() > 0)
    // {
    // cw.writeAttribute("motorNewsDeliveryMethod",
    // this.motorNewsDeliveryMethod);
    // }
    // if(this.motorNewsSendOption != null && this.motorNewsSendOption.length()
    // > 0)
    // {
    // cw.writeAttribute("motorNewsSendOption", this.motorNewsSendOption);
    // }
    //
    // cw.writeAttribute("postalAddress", getPostalAddress());
    //
    // if(this.postalName != null && this.postalName.length() > 0)
    // {
    // cw.writeAttribute("postalName", this.postalName);
    // }
    // cw.writeAttribute("preferredContactMethod", this.preferredContactMethod);
    // cw.writeAttribute("salutation", this.salutation);
    // cw.writeAttribute("status", this.status);
    // cw.writeAttribute("residentialAddress", this.getStreetAddress());
    //
    // cw.writeAttribute("market", this.getMarket());
    /*
     * cw.writeAttributeList("disallowedMarketingList",
     * getDisallowedMarketingList());
     */
    // cw.writeAttributeList("transactionList", getTransactionList());
    /* cw.writeAttributeList("vehicleList", this.getVehicleList()); */
    //
    // cw.endClass();
    // }
    // */
    //
    // public void setHomePhone(String homePhone)
    // {
    // this.homePhone = homePhone;
    // }
    //
    // public String getHomePhone()
    // {
    // return homePhone;
    // }
    //
    // public void setWorkPhone(String workPhone)
    // {
    // this.workPhone = workPhone;
    // }
    //
    // public String getWorkPhone()
    // {
    // return workPhone;
    // }
    //
    // public void setMobilePhone(String mobilePhone)
    // {
    // this.mobilePhone = mobilePhone;
    // }
    //
    // public String getMobilePhone()
    // {
    // return mobilePhone;
    // }
    //
    // public void setSex(Boolean sex)
    // {
    // this.sex = sex;
    // }
    //
    // public Boolean getSex()
    // {
    // return sex;
    // }
}
