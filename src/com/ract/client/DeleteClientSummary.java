package com.ract.client;

import java.io.Serializable;

/**
 * <p>
 * Represents the results of a client delete.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class DeleteClientSummary implements Serializable {

    public DeleteClientSummary() {
    }

    public final static String TYPE_CLIENT = "Client";

    public final static String TYPE_TRANSACTION = "Transaction";

    public final static String TYPE_MEMBERSHIP = "Membership";

    public final static String TYPE_PAYABLE_ITEM = "Payable Item";

    public void addCount(String type, int amount) {
	if (TYPE_TRANSACTION.equals(type)) {
	    transactionCount = transactionCount + amount;
	} else if (TYPE_MEMBERSHIP.equals(type)) {
	    membershipCount = membershipCount + amount;
	} else if (TYPE_PAYABLE_ITEM.equals(type)) {
	    payableItemCount = payableItemCount + amount;
	} else if (TYPE_CLIENT.equals(type)) {
	    clientCount = clientCount + amount;
	}
    }

    public int getCount(String type) {
	if (TYPE_TRANSACTION.equals(type)) {
	    return transactionCount;
	} else if (TYPE_MEMBERSHIP.equals(type)) {
	    return membershipCount;
	} else if (TYPE_PAYABLE_ITEM.equals(type)) {
	    return payableItemCount;
	} else if (TYPE_CLIENT.equals(type)) {
	    return clientCount;
	} else {
	    return 0; // no match
	}
    }

    private Integer clientNumber;

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public Integer getClientNumber() {
	return this.clientNumber;
    }

    private int transactionCount;

    private int membershipCount;

    private int payableItemCount;

    private int clientCount;

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("clientNumber=");
	desc.append(clientNumber);
	desc.append(",clientCount=");
	desc.append(clientCount);
	desc.append(",membershipCount=");
	desc.append(membershipCount);
	desc.append(",transactionCount=");
	desc.append(transactionCount);
	desc.append(",payableItemCount=");
	desc.append(payableItemCount);
	return desc.toString();
    }
}
