package com.ract.client;

public class ClientBusiness {
    Integer clientNo;
    String businessType;
    Integer businessNumber;
    String businessDesc;

    public String getBusinessDesc() {
	return businessDesc;
    }

    public Integer getBusinessNumber() {
	return businessNumber;
    }

    public String getBusinessType() {
	return businessType;
    }

    public Integer getClientNo() {
	return clientNo;
    }

    public void setBusinessDesc(String businessDesc) {
	this.businessDesc = businessDesc;
    }

    public void setBusinessNumber(Integer businessNumber) {
	this.businessNumber = businessNumber;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }
}
