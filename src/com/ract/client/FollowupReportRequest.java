package com.ract.client;

import java.rmi.RemoteException;
import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;
import com.ract.util.DateTime;
import com.ract.util.XDocument;

public class FollowupReportRequest extends XMLReportRequest {
    XDocument doc = null;

    public FollowupReportRequest(String userName, String printGroup, String userId, String slsbch, DateTime fDate, DateTime lDate, String status, String result) throws Exception {
	initialSettings(userName, printGroup);
	ClientMgr cltMgr = null;
	ArrayList fList = null;
	FollowUp fu = null;
	String newAddr = null;
	Node n = null;
	String tStatus = status.trim();
	String tResult = result.trim();
	boolean dupReq = false;
	cltMgr = ClientEJBHelper.getClientMgr();
	if (status.trim().equalsIgnoreCase("CHANGED"))
	    status = "change";
	if (status.trim().equalsIgnoreCase("Flag & Duplicate")) {
	    dupReq = true;
	    status = "flag";
	}
	if (result.trim().equalsIgnoreCase("to do"))
	    result = "";
	fList = cltMgr.getFollowUpList(userId, fDate, lDate, status, slsbch, result);
	if (dupReq) {
	    status = "duplicate";
	    ArrayList dList = cltMgr.getFollowUpList(userId, fDate, lDate, status, slsbch, result);
	    fList.addAll(dList);
	}
	doc = new XDocument();
	reportXML = (Element) doc.addNode(doc, "client");
	doc.addLeaf(reportXML, "userName", userName);
	doc.addLeaf(reportXML, "printDate", (new DateTime()).formatShortDate());
	n = doc.addNode(reportXML, "reportParameters");
	doc.addLeaf(n, "userId", userId);
	// doc.addLeaf(n,"requestedBy",userName);
	doc.addLeaf(n, "salesBranch", slsbch);
	if (fDate == null)
	    doc.addLeaf(n, "firstDate", "");
	else
	    doc.addLeaf(n, "firstDate", fDate.formatShortDate());
	if (lDate == null)
	    doc.addLeaf(n, "lastDate", "");
	else
	    doc.addLeaf(n, "lastDate", lDate.formatShortDate());
	doc.addLeaf(n, "status", tStatus);
	doc.addLeaf(n, "result", tResult);
	UserMgr xMgr = UserEJBHelper.getUserMgr();
	User xUser = null;

	for (int xx = 0; xx < fList.size(); xx++) {
	    fu = (FollowUp) fList.get(xx);
	    n = doc.addNode(reportXML, "entry");
	    String st = fu.getStatus().trim().toUpperCase();
	    if (st.equals("CHANGE"))
		st = "CHANGED";
	    doc.addLeaf(n, "status", st);
	    if (fu.getSubSystem().equalsIgnoreCase("INS"))
		doc.addLeaf(n, "sType", "Policy");
	    else
		doc.addLeaf(n, "sType", "Client");
	    doc.addLeaf(n, "sNumber", fu.getSystemNumber() + "");
	    doc.addLeaf(n, "createId", fu.getCreateId());
	    if (fu.getCreateId() != null) {
		xUser = xMgr.getUser(fu.getCreateId());
		if (xUser != null)
		    doc.addLeaf(n, "createName", xUser.getUsername());
	    }
	    doc.addLeaf(n, "newResAddress", fu.getNewStreetAddress());
	    doc.addLeaf(n, "newPostAddress", fu.getNewPostalAddress());
	    doc.addLeaf(n, "homePhone", fu.getNewHomePhone());
	    if (fu.getSubSystem().trim().equalsIgnoreCase("Client")) {
		ClientVO thisClt = cltMgr.getClient(fu.getSystemNumber());
		doc.addLeaf(n, "name", thisClt.getSurname() + ", " + thisClt.getGivenNames());
	    }
	    doc.addLeaf(n, "createDate", fu.getCreateDate().formatShortDate());
	    doc.addLeaf(n, "cClientNo", fu.getChangedClientNumber() + "");
	    if (fu.getResult() == null || fu.getResult().trim().equals("")) {
		doc.addLeaf(n, "process", "");
	    } else {
		doc.addLeaf(n, "process", fu.getResult().trim().toUpperCase());
	    }
	}
    }

    private void initialSettings(String username, String printGroup) throws RemoteException {
	this.printerGroup = printGroup;
	this.setFormName("email");
	this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
	this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
	CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	// PrintForm printForm = cMgr.getPrintForm(formName, this.printerGroup);
	// this.destinationFileName =
	// PrintUtil.getFileNameForPrinting(printForm,
	// SourceSystem.MEMBERSHIP);
	this.setReportName("FollowUpReport");
	this.setReportTemplate("/Ract", "client/FollowUpReport");
	try {
	    this.constructURL();
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new RemoteException(e.getMessage());
	}
	this.addParameter("URL", this.getReportDataURL());

	// else if(formName.equals(this.FORM_EMAIL))

	/* write the file to the report cache directory */
	try {
	    CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
	    String reportCache = comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_REPORT_CACHE);
	    String fileName = reportName + (new java.util.Date().getTime() + "").substring(0, 8) + ".pdf";
	    this.setDestinationFileName(reportCache + "/" + fileName);
	    // LogUtil.log(this.getClass(), "cache filename = " +
	    // this.getDestinationFileName());
	} catch (Exception rex) {
	    rex.printStackTrace();
	    throw new RemoteException("" + rex);
	}
    }
}
