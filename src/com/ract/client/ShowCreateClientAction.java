package com.ract.client;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ract.client.ui.ClientUIConstants;
import com.ract.util.FileUtil;

public class ShowCreateClientAction extends ActionSupport implements ServletRequestAware {

    private HttpServletRequest servletRequest;

    private boolean disableClientManagement = false;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    private String eventName;

    public String getEventName() {
	return eventName;
    }

    public void setEventName(String eventName) {
	this.eventName = eventName;
    }

    public String getPageHeading() {
	return pageHeading;
    }

    public void setPageHeading(String pageheading) {
	this.pageHeading = pageheading;
    }

    private String pageHeading;

    /**
     * @return
     */
    public String execute() {
	pageHeading = "Create";
	eventName = ClientUIConstants.ACTION_CREATE_CLIENT;

	try {
	    this.disableClientManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableClientManagement"));
	} catch (Exception e) {
	}

	if (this.disableClientManagement) {
	    addActionError("Client management is not permitted in Roadside.");
	    return ERROR;
	}

	servletRequest.setAttribute("displayResidentialAddress", true);
	servletRequest.setAttribute("displayPostalAddress", true);
	return SUCCESS;
    }

}