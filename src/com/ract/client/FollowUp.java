package com.ract.client;

import java.io.Serializable;

import com.ract.util.DateTime;

/**
 * Data object for 'PUB.addressFollowUp'
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 * 
 * @hibernate.class table="addressFollowUp" lazy="false"
 */
public class FollowUp implements Serializable {

    private String newPostalAddress;
    private String newStreetAddress;
    private String status = "";
    private String newHomePhone;

    private String createId;
    private String createSlsbch;

    private String followUpId;
    private DateTime followUpDate;
    private String result = "";
    private FollowUpPK followUpPk;

    public FollowUp() {
	followUpPk = new FollowUpPK();
    }

    public FollowUp(ClientVO thisClient, ClientVO aClient, DateTime tStamp, String userid) {
	this.followUpPk = new FollowUpPK();
	followUpPk.setSystemNumber(aClient.getClientNumber());
	followUpPk.setSubSystem("client");
	this.newPostalAddress = thisClient.postalAddress.getSingleLineAddress();
	this.newStreetAddress = thisClient.residentialAddress.getSingleLineAddress();
	this.status = "";
	this.followUpPk.setChangedClientNo(thisClient.getClientNumber());
	this.createId = userid;
	followUpPk.setCreateDate(tStamp);
    }

    /**
     * @hibernate.property
     * @hibernate.column name="newHomePhone"
     */
    public String getNewHomePhone() {
	return this.newHomePhone;
    }

    public void setNewHomePhone(String hp) {
	this.newHomePhone = hp;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="createId"
     */

    public String getCreateId() {
	return createId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="followUpDate"
     */

    public DateTime getFollowUpDate() {
	return followUpDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="followUpId"
     */

    public String getFollowUpId() {
	return followUpId;
    }

    // constants should exist for each of these
    public final static String STATUS_FLAG = "Flag";

    private String notes;

    /**
     * @hibernate.property
     * @hibernate.column name="status"
     */
    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public void setFollowUpId(String followUpId) {
	this.followUpId = followUpId;
    }

    public void setFollowUpDate(DateTime followUpDate) {
	this.followUpDate = followUpDate;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="result"
     */

    public String getResult() {
	return result;
    }

    public void setResult(String result) {
	this.result = result;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="newPostalAddress"
     */

    public String getNewPostalAddress() {
	return newPostalAddress;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="newStreetaddress"
     */

    public String getNewStreetAddress() {
	return newStreetAddress;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public FollowUpPK getFollowUpPk() {
	return followUpPk;
    }

    public void setNewPostalAddress(String newPostalAddress) {
	this.newPostalAddress = newPostalAddress;
    }

    public void setNewStreetAddress(String newStreetAddress) {
	this.newStreetAddress = newStreetAddress;
    }

    public void setFollowUpPk(FollowUpPK followUpPk) {
	this.followUpPk = followUpPk;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="createSlsbch"
     */
    public String getCreateSlsbch() {
	return createSlsbch;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="notes"
     */
    public String getNotes() {
	return notes;
    }

    public void setCreateSlsbch(String createSlsbch) {
	this.createSlsbch = createSlsbch;
    }

    public void setNotes(String notes) {
	this.notes = notes;
    }

    public String getSubSystem() {
	return followUpPk.getSubSystem();
    }

    public Integer getSystemNumber() {
	return followUpPk.getSystemNumber();
    }

    public Integer getChangedClientNumber() {
	return followUpPk.getChangedClientNo();
    }

    public DateTime getCreateDate() {
	return followUpPk.getCreateDate();
    }

    @Override
    public String toString() {
	return "FollowUp [createId=" + createId + ", createSlsbch=" + createSlsbch + ", followUpDate=" + followUpDate + ", followUpId=" + followUpId + ", followUpPk=" + followUpPk + ", newHomePhone=" + newHomePhone + ", newPostalAddress=" + newPostalAddress + ", newStreetAddress=" + newStreetAddress + ", notes=" + notes + ", result=" + result + ", status=" + status + "]";
    }
}
