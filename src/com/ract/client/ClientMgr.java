package com.ract.client;

import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.ejb.ObjectNotFoundException;
import javax.ejb.Remote;

import org.w3c.dom.Node;

import com.ract.common.BranchVO;
import com.ract.common.GenericException;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.notifier.NotificationEvent;
import com.ract.travel.TramadaClientTransaction;
import com.ract.user.User;
import com.ract.util.DateTime;

//import org.xml.sax.SAXException;

@Remote
public interface ClientMgr {
    public int getOrganisationCount() throws RemoteException, ObjectNotFoundException;

    // public Collection<ClientHistory> getClientHistory(Integer clientNumber);

    public void createClientBulkUpdate(ClientBulkUpdate clientBulkUpdate);

    public Collection findClientsByHomePhone(String phoneNumber) throws RemoteException;

    public Collection<ClientVO> findClientsByEmailAddress(String emailAddress) throws RemoteException;

    public Collection<ClientBulkUpdate> getClientBulkUpdateByDate(DateTime startDate, DateTime endDate);

    public Collection getClientBulkUpdateByClient(Integer clientNumber);

    public DateTime getLastBulkClientCreateDate(String type);

    public Collection getNoteType(boolean blockTransactions);

    public Collection getClientNoteList(Integer clientNumber, String businessType, String noteType);

    public Collection<NoteResult> getNoteResults(String noteType);

    public Collection getClientNoteListByNoteType(Integer clientNumber, String noteType);

    public Collection<NoteResult> getNoteResults();

    public NoteResult getNoteResult(String resultCode);

    public ClientNote updateClientNote(ClientNote clientNote);

    public ClientHistory getClientHistory(ClientHistoryPK clientHistoryPK);

    public Collection<NoteCategory> getNoteCategories(String businessType);

    public Collection getClientNoteList(String businessType);

    public Collection getClientNoteList(Integer clientNumber, String businessType);

    public ClientNote getLastClientNote(Integer clientNumber, String businessType);

    public Collection getClientNoteList(String businessType, String referenceId);

    public ClientVO getClientByMembershipCardNumber(String cardNumber) throws RemoteException;

    public void validateDuplicateClient(ClientVO client) throws RemoteException, ValidationException;

    public void deleteClientSubscriptions(Integer clientNumber) throws RemoteException;

    public Collection findClientsByNameAndTitle(String initials, String surname, String title) throws RemoteException;

    public Collection getSubscribedClients(String publicationCode) throws RemoteException;

    public void deleteTramadaClientTransactions(Integer clientNumber) throws RemoteException;

    public Collection findClientsByNameAndBirthDate(String initials, String surname, DateTime birthDate) throws RemoteException;

    public Collection findClientsByNameAndAddress(String surname, Integer resiStreetNumber, Integer resiStsubId) throws RemoteException;

    public Collection findClientsByNameAndAddress(String surname, Integer resiStreetNumber, Integer resiStsubId, Boolean sex) throws RemoteException;

    public Collection getClientNoteList(String noteType, Integer clientNumber, boolean blockTransaction);

    public int getPersonCount() throws RemoteException;

    public Collection getNoteTypes();

    public Collection getNoteCategories();

    public Collection<NoteCategory> getFollowUpNoteCategories();

    public ClientNote getLastClientNote(Integer clientNumber);

    public void createClientNote(ClientNote clientNote) throws RemoteException;

    public ClientNote getClientNote(Integer noteId);

    public NoteCategory getNoteCategory(String categoryCode);

    public NoteType getNoteType(String noteType);

    public Collection getClientNoteList(Integer clientNumber);

    public DeleteClientSummary deleteClientAndBusiness(Integer clientNumber, boolean archive) throws RemoteException, GenericException;

    public Collection getClientOccupations() throws RemoteException;

    public Collection getClientTitles() throws RemoteException;

    public Collection getClientTitles(boolean enabledOnly) throws RemoteException;

    public void transferTramadaClientXML(String mode, String action, String userId, String salesBranchCode, ClientVO client) throws RemoteException;

    public TramadaClientTransaction getLastTramadaClientTransaction(Integer clientNumber) throws RemoteException;

    public void createTramadaClientTransaction(TramadaClientTransaction tramadaClientTransaction) throws RemoteException;

    public void loadTramadaClientTransactions(File file, DateTime loadDate) throws RemoteException;

    public void updateClient(ClientVO client) throws RemoteException;

    public void createClientHistory(ClientVO client, String logonId, String transactionReason, String salesBranch, String comments, String subSystem) throws GenericException;

    public void deleteClient(ClientVO client, boolean archive) throws RemoteException;

    public void deleteObsoleteClientsAndBusiness(String clientDeleteFile) throws RemoteException;

    public ClientTitleVO getClientTitle(String titleName);

    public Collection getCachedClientTitles() throws RemoteException;

    public Collection searchForClient(int searchType, String searchValue) throws RemoteException, ValidationException;

    public BranchVO getBranchForPostcode(Integer postcode) throws RemoteException;

    public ClientOccupationVO getClientOccupation(Integer occupationID) throws RemoteException;

    public Collection getClientVehicleList(Integer clientNumber);

    public Collection getClientSubscriptionList(Integer clientNumber) throws RemoteException;

    public void resetClientOccupationCache() throws RemoteException;

    public void deleteClientSubscription(ClientSubscriptionPK clientSubscriptionPK) throws RemoteException;

    public void createClientSubscription(ClientSubscription clientSubscription) throws RemoteException;

    public void resetClientTitleCache() throws RemoteException;

    public void createClientSegments(Integer startClientNumber, Integer endClientNumber, String fileName) throws RemoteException;

    public void updateOrganisationClients() throws RemoteException;

    public List<ClientVO> getClientsWithoutSubscriptions(DateTime startDate) throws RemoteException;

    public Hashtable mergeClient(Integer clientNumber, Node clientNode) throws RemoteException;

    public int getDeceasedClientsToRepairCount() throws RemoteException;

    public void updateDeceasedClientStatus() throws RemoteException;

    public void updateAccessMarketingFlag() throws RemoteException;

    public int getClientCountByPostalStreetSuburb(Integer stsubid) throws RemoteException;

    /**
     * Return a market type with the specified code. Assumes that only one
     * record will match the code given.
     */
    // public MarketingType getMarketingType(String marketingTypeCode)
    // throws RemoteException;

    public void validateMasterClient(ClientVO masterClientVO) throws RemoteException;

    public Collection getClientOccupationList() throws RemoteException;

    public void loadTramadaClients(String fileName) throws RemoteException;

    /**
     * <p>
     * 
     * Create a client using the following process:
     * </p>
     * <ul>
     * <li>Check for matching clients
     * <ol>
     * <li>Check for client by initials, surname and birthdate. (Phone number?)
     * 
     * <li>Check for client by initials, surname, street number and stsubid.
     * 
     * </ol>
     * 
     * <li>No matching clients
     * <li>Insert record into the cl-master table
     * <li>Insert record into the cl-history table with a trans-reason of "NEW"
     * 
     * </ul>
     * 
     * 
     * @param cvo
     *            Description of the Parameter
     * @param chvo
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ClientVO createClient(ClientVO newClientVO, String userID, String transactionReason, String salesBranchCode, String comments, String subSystem) throws RemoteException;

    /**
     * Create a new client from the client data supplied from the Progress
     * client system
     */
    public ClientVO createClientFromProgressData(Integer clientNumber, Node clientNode) throws RemoteException;

    public void createDefaultClientSubscriptions(ClientVO client) throws RemoteException;

    /**
     * Remove an existing client when initaited by a remove of the client in the
     * progress client database.
     */
    public void removeClient_ProgressInitiated(Integer clientNumber, User user, boolean archive) throws RemoteException;

    /**
     * <p>
     * 
     * Update client using the following process:
     * </p>
     * <ul>
     * <li>Check for matching clients
     * <ol>
     * <li>Check for client by initials, surname and birthdate. (Phone number?)
     * 
     * <li>Check for client by initials, surname, street number and stsubid.
     * 
     * </ol>
     * 
     * <li>No matching clients
     * <li>Insert record into cl-history with a trans-reason of "ALT"
     * <li>Update record in cl-master
     * </ul>
     * 
     * 
     * @param cvo
     *            Description of the Parameter
     * @param chvo
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ClientVO updateClient(ClientVO newClientVO, String userID, String transactionReason, String salesBranchCode, String comments, String subSystem) throws RemoteException;

    /**
     * Update an existing client from the client data supplied from the Progress
     * client system.
     */
    public ClientVO updateCustomerFromProgressClient(Integer clientNumber, Node clientNode) throws RemoteException;

    public ClientVO getClient(Integer clientNumber) throws RemoteException, ValidationException;

    // public void createClientHistory(ClientVO client,String userId, String
    // transReason,DateTime modifyDate, Integer modifyTime,String
    // salesBranch,String comments,String subSystem) throws RemoteException,
    // ValidationException;

    public void initialiseClientOccupationList() throws RemoteException;

    public void initialiseClientTitleList() throws RemoteException;

    public void createClient(ClientVO client) throws RemoteException;

    /**
     * Process the notification event
     * 
     * @param event
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void processNotificationEvent(NotificationEvent event) throws RemoteException;

    public ClientSubscription getClientSubscription(Integer clientNumber, String subscriptionCode) throws RemoteException;

    public ClientSubscription getActiveClientSubscription(Integer clientNumber, String subscriptionCode) throws RemoteException;

    public String getObsoleteClientList(DateTime lastTransaction) throws RemoteException;

    public Hashtable getRelatedClients(ChangedAddress cAddress) throws RemoteException;

    // public void setChangedAddress(ChangedAddress oldAdd) throws
    // RemoteException;
    // public ChangedAddress getOldAddress(Integer clientNo, Integer
    // tStamp)throws RemoteException;
    // public void removeUpdatedClient(ChangedAddress uc)throws RemoteException;
    public void getClientBusiness(Integer clientNo, Hashtable bus) throws RemoteException;

    public boolean isPasswordProtected(Integer clientNo) throws RemoteException;

    public void createFollowUp(FollowUp fu) throws RemoteException;

    public void setFollowUpResult(FollowUp fu) throws RemoteException;

    public void setFollowUpStatus(FollowUpPK fupk, String status) throws RemoteException;

    public void savePopupRequest(String requestType, String userId, String slsbch, int tStamp, ClientVO oldClient) throws RemoteException;

    public ClientHistory getLastClientHistory(Integer clientNumber);

    public ClientHistory updateClientHistory(ClientHistory clientHistory);

    public boolean isAskable(Integer clientNumber, String fieldName, String fieldValue) throws SystemException;

    public void addressUnknownNotifications(boolean countOnly);

    public ClientHistory getLastClientHistory(Integer clientNumber, boolean excludeCurrent);

    /**
     * Get follow up list. Returns a Collection of FollowUp objects Any or all
     * of the parameters can be null, in which case they are not used in the
     * select statement. if either tDate is non null, so must the fDate be. If
     * only fDate is provided, records after that date will be returned. if
     * tDate and FDate are both provided the method will return records with
     * dates falling between the two. If more than one criterion is present,
     * they are "AND"ed If the result parameter is entered as an empty string,
     * the lookup will return records for which no result has been entered.
     * 
     * @param userId
     *            String
     * @param fDate
     *            DateTime
     * @param tDate
     *            DateTime
     * @param status
     *            String
     * @param slsbch
     *            String
     * @param result
     *            String
     * @throws RemoteException
     * @return ArrayList of FollowUp objects
     */
    public ArrayList getFollowUpList(String userId, DateTime fDate, DateTime tDate, String status, String slsbch, String result) throws RemoteException;

    public void setFollowUpResult(FollowUpPK fupk, String userId, DateTime fuDate, String result) throws RemoteException;

    public Collection getMarketOptions(Integer clientNo) throws RemoteException;

    public void setMarketOptions(Integer clientNo, Collection options, String userId) throws RemoteException;

    public ArrayList clientSearch(String surname, String initial, String streetNo, String street, String suburb, DateTime dateOfBirth, Boolean gender, String phoneNo, String emailAddress) throws RemoteException;

    public ArrayList getClientGroup(Integer clientNo) throws RemoteException;

    public Collection<ClientHistory> getClientHistory(Integer clientNumber);

    public List<Map<String, String>> getExternalClients(Integer clientNo) throws RemoteException;

    public Long getClientChangesCount(DateTime startDate, DateTime endDate, Collection<String> excludedSystems) throws RemoteException;

    public Boolean isClientChangesExist(DateTime startDate, DateTime endDate, Collection<String> excludedSystems) throws RemoteException;

    public Collection<Integer> getClientChangesIdentifiers(DateTime startDate, DateTime endDate, Collection<String> excludedSystems) throws RemoteException;

    public Collection<ClientVO> getClientChanges(DateTime startDate, DateTime endDate, Collection<String> excludedSystems) throws RemoteException;

    public List<Map<String, String>> getCmoDuplicateSuggestions() throws SystemException;

    public Collection<ClientVO> findClientsWithoutRoadside(ClientVO clientVO) throws SystemException;
}
