package com.ract.client;

import java.io.Serializable;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.Publication;

/**
 * <p>
 * Client publications.
 * </p>
 * 
 * @hibernate.class table="cl_subscription" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class ClientSubscription implements Serializable {

    /**
     * Motor news delivery method - normal (hand delivered?) or postal
     */
    private String deliveryMethod;

    /**
     * Send (always send), no send (never send), normal (if membership),
     * individual (own copy)
     */
    private String sendOption;

    /**
     * @hibernate.property
     */
    public String getSendOption() {
	return sendOption;
    }

    public void setSendOption(String sendOption) {
	this.sendOption = sendOption;
    }

    private ClientSubscriptionPK clientSubscriptionPK;

    private String salesBranchCode;

    /**
     * @hibernate.property
     */
    public String getDeliveryMethod() {
	return deliveryMethod;
    }

    public Publication getPublication() {
	CommonMgrLocal commonMgr = CommonEJBHelper.getCommonMgrLocal();
	return commonMgr.getPublication(getClientSubscriptionPK().getSubscriptionCode());
    }

    public final static String STATUS_ACTIVE = "A";

    public final static String STATUS_DISABLED = "D";

    private String userId;

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public ClientSubscriptionPK getClientSubscriptionPK() {
	return this.clientSubscriptionPK;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="sales_branch_code"
     */
    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="user_id"
     */
    public String getUserId() {
	return userId;
    }

    public boolean isActive() {
	return this.getClientSubscriptionPK().getSubscriptionStatus().equalsIgnoreCase(this.STATUS_ACTIVE);
    }

    public void setDeliveryMethod(String deliveryMethod) {
	this.deliveryMethod = deliveryMethod;
    }

    public void setClientSubscriptionPK(ClientSubscriptionPK clientSubscriptionPK) {
	this.clientSubscriptionPK = clientSubscriptionPK;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    // default required
    public ClientSubscription() {
    }

    public ClientSubscription(ClientSubscriptionPK clientSubscriptionPK, String publicationDeliveryMethod, String salesBranchCode, String userId) {
	this.setClientSubscriptionPK(clientSubscriptionPK);
	this.setDeliveryMethod(publicationDeliveryMethod);
	this.setSalesBranchCode(salesBranchCode);
	this.setUserId(userId);
    }

    public String toString() {
	return getClientSubscriptionPK().toString() + " " + getDeliveryMethod();
    }

}
