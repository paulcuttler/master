package com.ract.client;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({ @NamedQuery(name = "findLastCreateDate", query = "SELECT MAX(c.clientBulkUpdatePK.createDate) " + "FROM ClientBulkUpdate c WHERE c.clientBulkUpdatePK.updateType = :updateType"), @NamedQuery(name = "findClientsByDate", query = "SELECT c " + "FROM ClientBulkUpdate c WHERE c.clientBulkUpdatePK.createDate > :startDate and c.clientBulkUpdatePK.createDate < :endDate"), @NamedQuery(name = "findClient", query = "SELECT c " + "FROM ClientBulkUpdate c WHERE c.clientBulkUpdatePK.clientNumber = :clientNumber") })
public class ClientBulkUpdate implements Serializable {

    public static String UPDATE_SUCCESS = "Success";
    public static String UPDATE_FAIL = "Fail";

    @Id
    private ClientBulkUpdatePK clientBulkUpdatePK;

    public ClientBulkUpdatePK getClientBulkUpdatePK() {
	return clientBulkUpdatePK;
    }

    public void setClientBulkUpdatePK(ClientBulkUpdatePK clientBulkUpdatePK) {
	this.clientBulkUpdatePK = clientBulkUpdatePK;
    }

    private String userId; // jyh

    private String updateStatus; // success, fail

    private String message; // unable to update record

    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public String getUpdateStatus() {
	return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
	this.updateStatus = updateStatus;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    @Override
    public String toString() {
	return "ClientBulkUpdate [clientBulkUpdatePK=" + clientBulkUpdatePK + ", message=" + message + ", updateStatus=" + updateStatus + ", userId=" + userId + "]";
    }

}
