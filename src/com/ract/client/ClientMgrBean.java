package com.ract.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.FinderException;
import javax.ejb.Local;
import javax.ejb.ObjectNotFoundException;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import au.com.bytecode.opencsv.CSVReader;

import com.insight4.ws.CardManagementService;
import com.insight4.ws.GetPersonIdFromCardNumberResult;
import com.insight4.ws.ICardManagementService;
import com.insight4.ws.mrm.Address;
import com.insight4.ws.mrm.GetPerson;
import com.insight4.ws.mrm.GetPersonSoap;
import com.insight4.ws.mrm.Person;
import com.ract.archive.ArchivedClient;
import com.ract.client.notifier.ClientChangeNotificationEvent;
import com.ract.client.notifier.ProgressClientChangeNotificationEvent;
import com.ract.client.ui.ClientUIConstants;
import com.ract.common.AddressVO;
import com.ract.common.BranchVO;
import com.ract.common.BusinessType;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.common.GenericException;
import com.ract.common.MailMgrLocal;
import com.ract.common.MarketingType;
import com.ract.common.Publication;
import com.ract.common.RollBackException;
import com.ract.common.SMSManager;
import com.ract.common.SalesBranchVO;
import com.ract.common.SequenceMgr;
import com.ract.common.ServiceLocator;
import com.ract.common.SourceSystem;
import com.ract.common.StreetSuburbVO;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.hibernate.HibernateUtil;
import com.ract.common.integration.IntegrationMgr;
import com.ract.common.integration.PopupRequest;
import com.ract.common.mail.MailMessage;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationMgr;
import com.ract.fleet.FleetFactory;
import com.ract.insurance.InsuranceFactory;
import com.ract.membership.MembershipCardVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.electronicpayment.EftPaymentMgr;
import com.ract.payment.finance.Debtor;
import com.ract.payment.finance.FinanceException;
import com.ract.payment.finance.FinanceMgr;
import com.ract.payment.receipting.OCRAdapter;
import com.ract.payment.receipting.Payable;
import com.ract.payment.receipting.ReceiptingAdapter;
import com.ract.security.SecurityHelper;
import com.ract.travel.TramadaClient;
import com.ract.travel.TramadaClientHelper;
import com.ract.travel.TramadaClientProfileImporter;
import com.ract.travel.TramadaClientTransaction;
import com.ract.travel.TramadaClientTransactionPK;
import com.ract.travel.TravelFactory;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;
import com.ract.user.UserMgrLocal;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.MethodCounter;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;
import com.ract.util.XDocument;
import com.ract.vehicle.VehicleVO;
import com.ract.vehicleinspection.VehicleInspectionFactory;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.security.UserSecurityMgrRemote;

/**
 * <p>
 * Session bean implementation class
 * </p>
 * 
 * <p>
 * the actual insert, update and delete of clients must be done through the
 * session bean as the process should be as follows:
 * </p>
 * <h3>INSERT</h3>
 * <p>
 * 
 * insert a record into the client table, no change to history table
 * </p>
 * <h3>UPDATE/EDIT</h3>
 * <p>
 * 
 * move existing record into history table with 'UPD' flag.
 * </p>
 * <p>
 * 
 * update client details.
 * </p>
 * <h3>DELETE</h3>
 * <p>
 * 
 * move existing record into history table with the 'DEL' flag. remove record
 * from client table.
 * </p>
 * 
 * @author John Holliday
 * @created 31 July 2002
 * @version 1.0
 */
@Stateless
@Remote({ ClientMgr.class })
@Local({ ClientMgrLocal.class })
public class ClientMgrBean
// implements SessionBean
{

	private static final String AUS_PREFIX = "+61";

	@PersistenceContext(unitName = "master")
	Session hsession;

	@PersistenceContext(unitName = "master")
	EntityManager em;

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource dataSource;

	@EJB
	private MailMgrLocal mailMgr;

	@EJB
	private ClientMgrLocal clientMgr;

	@EJB
	private CommonMgrLocal commonMgr;

	@EJB
	private UserMgrLocal userMgr;

	public DateTime getLastBulkClientCreateDate(String type) {
		Query maxAge = em.createNamedQuery("findLastCreateDate");
		maxAge.setParameter("updateType", type);
		Date lastCreateDate = (Date) maxAge.getSingleResult();
		return (DateTime) lastCreateDate;
	}

	public void createClient(ClientVO client) throws RemoteException {
		Integer clientNumber = client.getClientNumber();
		// Progress Initiated
		if (clientNumber == null) {
			ClientIDMgr idMgr = ClientEJBHelper.getClientIDMgr();
			clientNumber = new Integer(idMgr.getNextClientID());
			client.setClientNumber(clientNumber);
		}

		client.setDerivedFields();

		// Insert the new client
		hsession.save(client);

	}

	public void createClientBulkUpdate(ClientBulkUpdate clientBulkUpdate) {
		em.persist(clientBulkUpdate);
	}

	public void savePopupRequest(String requestType, String userId,
			String slsbch, int tStamp, ClientVO oldClient)
			throws RemoteException {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String emailTo = commonMgr.getSystemParameterValue(
				SystemParameterVO.CATEGORY_CLIENT,
				SystemParameterVO.UPDATE_HANDLER_ADDRESS);
		long time = new Date().getTime();
		Integer rNo = new Integer(tStamp);
		String xmlString = null;
		XDocument doc = new XDocument();
		org.w3c.dom.Node root = doc.addNode(doc, "XMLData");
		doc.addLeaf(root, "clientNo", oldClient.getClientNumber() + "");
		doc.addLeaf(root, "userid", userId);
		doc.addLeaf(root, "slsbch", slsbch);
		doc.addLeaf(root, "oldResiProperty", oldClient.getResiProperty());
		doc.addLeaf(root, "oldResiStreetChar", oldClient.getResiStreetChar());
		doc.addLeaf(root, "oldResiStsubid", oldClient.getResiStsubId() + "");
		doc.addLeaf(root, "oldPostProperty", oldClient.getPostProperty());
		doc.addLeaf(root, "oldPostStreetChar", oldClient.getPostStreetChar());
		doc.addLeaf(root, "oldPostStsubid", oldClient.getPostStsubId() + "");
		doc.addLeaf(root, "oldHomePhone", oldClient.getHomePhone());
		PopupRequest pr = new PopupRequest(rNo, doc.toXMLString());
		pr.setLogonId(userId);
		pr.setRequestDate(new DateTime().getDateOnly());
		pr.setRequestType(requestType);
		pr.setSalesBranch(slsbch);
		IntegrationMgr imgr = CommonEJBHelper.getIntegrationMgr();
		imgr.setPopupRequest(pr);
		/*
		 * String webServerAddress =
		 * commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_CLIENT,
		 * "ws_address"); String pageUrl = "http://" + webServerAddress +
		 * "/PopupUIC?event=showAddressUpdater&userid=" + userId +
		 * "&requestSeq=" + rNo; MailMgr mailMgr = CommonEJBHelper.getMailMgr();
		 * mailMgr.sendMail(emailTo, "Client Record Changed: " +
		 * oldClient.getClientNumber(), "Client " + oldClient.getClientNumber()
		 * + " was changed by " + userId +
		 * ".  Click this hyperlink to open the Address updater " + pageUrl );
		 */
	}

	/**
	 * Load Tramada client extract for the purpose of auditing client quality
	 * and the number that are associated with RACT customers.
	 * 
	 * @param fileName
	 */
	public void loadTramadaClients(String fileName) {

		LogUtil.log(this.getClass(), "loadTramadaClients start");

		int lineCounter = 0;

		Integer clientNumber = null;
		String tramadaProfileCode = null;
		Object tramadaKey = null;
		String createDateString = null;
		String branch = null;
		String consultant = null;
		String clientNumberString = null;
		DateTime createDate = null;
		Hashtable profiles = new Hashtable();
		try {
			CSVReader reader = new CSVReader(new InputStreamReader(
					new FileInputStream(fileName)));
			consultant = null;
			branch = null;

			String lineElements[] = new String[100];

			while ((lineElements = reader.readNext()) != null) {

				LogUtil.log(this.getClass(), "lineElements=" + lineElements[0]
						+ " " + lineElements[1]);

				tramadaProfileCode = null;
				createDate = null;
				clientNumber = null;
				lineCounter++;
				// first line has headings
				if (lineCounter > 1) {
					LogUtil.debug(this.getClass(), lineElements[24] + ","
							+ lineElements[1] + "," + lineElements[34]);

					createDateString = lineElements[24];
					if (createDateString != null
							&& createDateString.trim().length() > 0) {
						createDate = new DateTime(createDateString);
					}
					tramadaProfileCode = lineElements[1];
					LogUtil.debug(this.getClass(), "tramadaProfileCode="
							+ tramadaProfileCode);

					clientNumberString = lineElements[34];
					if (clientNumberString != null
							&& clientNumberString.trim().length() > 0) {
						clientNumber = new Integer(lineElements[34]);
					}

					branch = lineElements[0];
					consultant = lineElements[11];
					LogUtil.debug(this.getClass(), "null=" + tramadaProfileCode);

					// already in the list?
					tramadaKey = profiles.get(tramadaProfileCode);
					if (tramadaKey == null
							|| !tramadaKey.equals(tramadaProfileCode)) {
						// key and value
						profiles.put(tramadaProfileCode, tramadaProfileCode);

						TramadaClient tc = new TramadaClient();
						tc.setTramadaProfileCode(tramadaProfileCode);
						tc.setClientNumber(clientNumber);
						tc.setCreateDate(createDate);
						tc.setBranch(branch);
						tc.setConsultant(consultant);

						// save
						hsession.save(tc);
					}

				}

			}

		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), e);
		}
		profiles.clear();
		LogUtil.log(this.getClass(), "Loaded " + lineCounter + " records.");
	}

	/**
	 * Insert a client after checking for duplicates.
	 * 
	 * @param cvo
	 *            The client value object
	 * @param chvo
	 *            Description of the Parameter
	 * @return the success flag
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public ClientVO createClient(ClientVO newClient, String userID,
			String transactionReason, String salesBranchCode, String comments,
			String subSystem) throws RemoteException {
		LogUtil.debug(this.getClass(), "newClient=" + newClient);
		LogUtil.debug(this.getClass(), "userID=" + userID);
		LogUtil.debug(this.getClass(), "transactionReason=" + transactionReason);
		LogUtil.debug(this.getClass(), "salesBranchCode=" + salesBranchCode);
		LogUtil.debug(this.getClass(), "comments=" + comments);
		LogUtil.debug(this.getClass(), "subSystem=" + subSystem);

		if (newClient == null) {
			throw new SystemException(
					"ClientVO may not be null when creating a new client.");
		}

		ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
		NotificationMgr notificationMgr = CommonEJBHelper.getNotificationMgr();
		try {
			// set branch number from postcode
			setBranchNumber(newClient);

			// store a copy of subSystem here for convenience
			newClient.setSubSystem(subSystem);
			newClient.setLastUpdate(new DateTime());

			// create client record
			createClient(newClient);

			// create a history record
			createClientHistory(newClient, userID, transactionReason,
					salesBranchCode, comments, subSystem);

			// create default subscriptions
			createDefaultClientSubscriptions(newClient);

			if (!subSystem.equals(SourceSystem.PROGRESS_CLIENT
					.getAbbreviation())) {
				// update Progress database
				clientAdapter.updateClient(newClient, userID, salesBranchCode,
						comments, subSystem);

				// update fields
				addFieldUpdates(newClient, clientAdapter);

				clientAdapter.commit();
			}

			NotificationEvent event = new ClientChangeNotificationEvent(
					ClientChangeNotificationEvent.ACTION_CREATE, newClient);
			event.setDelay(100);
			notificationMgr.notifyEvent(event);
			notificationMgr.commit();

		} catch (Exception e) {
			if (notificationMgr != null) {
				notificationMgr.rollback();
			}
			clientAdapter.rollback();
			e.printStackTrace();
			throw new RemoteException("createClient ", e);
		}
		return newClient;
	}

	/**
	 * Get a list of Tramada client transaction by client number and in
	 * descending order of modifed date. A transaction is either a create,
	 * update or delete performed via XML.
	 * 
	 * @param clientNumber
	 *            Integer
	 * @throws RemoteException
	 * @return Collection
	 */
	public Collection getTramadaClientTransactions(Integer clientNumber)
			throws RemoteException {
		ArrayList tramadaClientTransactionList = new ArrayList();
		try {
			tramadaClientTransactionList = new ArrayList(
					hsession.createCriteria(TramadaClientTransaction.class)
							.add(Restrictions.eq(
									"tramadaClientTransactionPK.clientNumber",
									clientNumber))
							.addOrder(
									Order.desc("tramadaClientTransactionPK.modified"))
							.list());
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}
		return tramadaClientTransactionList;
	}

	/**
	 * Delete all Tramada transactions for a given client
	 * 
	 * @param clientNumber
	 *            Integer
	 * @throws RemoteException
	 */
	public void deleteTramadaClientTransactions(Integer clientNumber)
			throws RemoteException {
		LogUtil.log(this.getClass(), "deleteTramadaClientTransactions start");
		LogUtil.log(this.getClass(), "clientNumber=" + clientNumber);
		org.hibernate.Query query = hsession
				.createQuery("delete from TramadaClientTransaction where tramadaClientTransactionPK.clientNumber = :clientNumber");
		query.setInteger("clientNumber", clientNumber);
		int rowsUpdated = query.executeUpdate();
		LogUtil.debug(this.getClass(), "Deleted " + rowsUpdated
				+ " tramada client transactions.");
		LogUtil.log(this.getClass(), "deleteTramadaClientTransactions start");
	}

	public void deleteMarketOptions(Integer clientNumber)
			throws RemoteException {
		LogUtil.log(this.getClass(), "deleteMarketOptions start");
		LogUtil.log(this.getClass(), "clientNumber=" + clientNumber);
		org.hibernate.Query query = hsession
				.createQuery("delete from MarketOption where clientNumber = :clientNumber");
		query.setInteger("clientNumber", clientNumber);
		int rowsUpdated = query.executeUpdate();
		LogUtil.debug(this.getClass(), "Deleted " + rowsUpdated
				+ " market options.");
		LogUtil.log(this.getClass(), "deleteMarketOptions start");
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void createTramadaClientTransaction(
			TramadaClientTransaction tramadaClientTransaction)
			throws RemoteException {
		// Insert the new Tramada client transaction
		hsession.save(tramadaClientTransaction);
	}

	/**
	 * Load the tramada clients into the transaction table that were uploaded in
	 * the initial load so that they will be automatically maintained in the
	 * customer systems.
	 * 
	 * @param file
	 *            File
	 * @throws <any>
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void loadTramadaClientTransactions(File file, DateTime loadDate)
			throws RemoteException {
		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
		BufferedReader fileReader = null;
		int lineCounter = 0;
		int loadErrors = 0;
		String clientReference = null;
		String companyId = null;
		try {
			fileReader = new BufferedReader(new FileReader(file));
			String line = null;
			String lineElements[] = new String[4];

			TramadaClientTransaction tramadaClientTransaction = null;
			while ((line = fileReader.readLine()) != null) {
				lineCounter++;
				// first line has headings
				if (lineCounter > 1) {
					lineElements = line.split(",");
					clientReference = lineElements[6];
					companyId = lineElements[2];
					try {
						tramadaClientTransaction = new TramadaClientTransaction(
								new TramadaClientTransactionPK(
										new Integer(clientReference),
										lineElements[1],
										TramadaClientProfileImporter.TRAMADA_ACTION_ADD,
										loadDate));
						if (!companyId.trim().equals("")) {
							// set the debtor is the id is not empty
							tramadaClientTransaction.setCompanyId(companyId);
						}
						// new transaction
						clientMgr
								.createTramadaClientTransaction(tramadaClientTransaction);
					} catch (NumberFormatException ex) {
						loadErrors++;
						LogUtil.log(
								this.getClass(),
								lineCounter
										+ " "
										+ clientReference
										+ " "
										+ ExceptionHelper
												.getExceptionStackTrace(ex));
					}
					// log every 500th record
					if (lineCounter % 500 == 0) {
						LogUtil.log(this.getClass(), lineCounter + " "
								+ clientReference);
					}
				}

			}
			fileReader.close();
		} catch (FileNotFoundException ex) {
			// loadErrors++;
			LogUtil.log(this.getClass(),
					"Error locating file. " + ex.getStackTrace());
		} catch (IOException ex) {
			// loadErrors++;
			LogUtil.log(this.getClass(),
					"Error processing file. " + ex.getStackTrace());
		}
		LogUtil.log(this.getClass(), "Loaded " + lineCounter
				+ " tramada transactions with " + loadErrors + " errors.");
	}

	public TramadaClientTransaction getLastTramadaClientTransaction(
			Integer clientNumber) throws RemoteException {
		TramadaClientTransaction tramadaClientTransaction = null;
		// check if a transaction exists and use the profile code
		Collection tramadaClientTransactionList = getTramadaClientTransactions(clientNumber);
		if (tramadaClientTransactionList != null
				&& !tramadaClientTransactionList.isEmpty()) {
			Iterator txList = tramadaClientTransactionList.iterator();
			if (txList.hasNext()) // only interested in the last one
			{
				tramadaClientTransaction = (TramadaClientTransaction) txList
						.next();
			}
		}
		return tramadaClientTransaction;
	}

	/**
	 * Generate the tramada client xml file and transfer to the Tramada.
	 * 
	 * @param clientVO
	 *            ClientVO
	 * @throws RemoteException
	 * @return String
	 */
	public void transferTramadaClientXML(String mode, String action,
			String userId, String salesBranchCode, ClientVO client)
			throws RemoteException {
		// System Parameters are cached in memory and not accessed directly from
		// the database.
		String tramadaEnabled = CommonEJBHelper.getCommonMgr()
				.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA,
						SystemParameterVO.TRAMADA_ENABLE);
		LogUtil.debug(this.getClass(), "tramadaEnabled" + tramadaEnabled);
		if (tramadaEnabled != null) {
			if (!new Boolean(tramadaEnabled).booleanValue()) {
				// LogUtil.warn(this.getClass(),"Tramada transfer is disabled. Client changes for '"+client.getClientNumber()+"' will not be transferred to Tramada.");
				return;
			}
		} else {
			throw new SystemException(
					"A system parameter does not exist for TRAMADA/ENABLE");
		}

		String tramadaProfileCode = null;
		String debtorCode = null;
		boolean recordTransaction = true;
		LogUtil.debug(this.getClass(),
				"tramada client number=" + client.getClientNumber());
		LogUtil.debug(this.getClass(), "mode=" + mode);
		if (mode.equals(Client.TRAMADA_MODE_EXPLICIT)) {

			// explicit add
			if (action.equals(TramadaClientProfileImporter.TRAMADA_ACTION_ADD)) {
				if (client.isActive()) {
					// Group client or has an ampersand in the title
					if (client.getGroupClient()
							|| (client.getTitle() != null && client.getTitle()
									.indexOf("&") != -1)) {
						throw new RemoteException(
								"Group clients are not supported in Tramada. Client profiles should be individuals that may be attached to a suitable debtor in the case of corporate groups.");
					}

					tramadaProfileCode = TramadaClientHelper
							.getTramadaProfileCode(client);
					LogUtil.debug(this.getClass(), "tramadaProfileCode="
							+ tramadaProfileCode);
				} else {
					throw new RemoteException(
							"Unable to transfer client to Tramada as client has a status of '"
									+ client.getStatus() + ".");
				}
			}
			// mark a client as terminated or reinstate a terminated client
			else if (action
					.equals(TramadaClientProfileImporter.TRAMADA_ACTION_DELETE)
					|| action
							.equals(TramadaClientProfileImporter.TRAMADA_ACTION_REINSTATE)
					|| action
							.equals(TramadaClientProfileImporter.TRAMADA_ACTION_UPDATE)) {
				if (action
						.equals(TramadaClientProfileImporter.TRAMADA_ACTION_UPDATE)) {
					action += "E"; // explicit update
				}

				// last tramada transaction profile code
				TramadaClientTransaction tramadaClientTransaction = getLastTramadaClientTransaction(client
						.getClientNumber());
				if (tramadaClientTransaction != null) {
					tramadaProfileCode = tramadaClientTransaction
							.getTramadaClientTransactionPK()
							.getTramadaProfileCode();
					LogUtil.debug(this.getClass(), "tramadaProfileCode="
							+ tramadaProfileCode);
				}

			}

		} else if (mode.equals(Client.TRAMADA_MODE_UPDATE)) {
			if (action
					.equals(TramadaClientProfileImporter.TRAMADA_ACTION_UPDATE)) {
				LogUtil.debug(this.getClass(), "mode/action of update");
				// record a transaction using the last tramada transaction
				// profile code
				TramadaClientTransaction tramadaClientTransaction = getLastTramadaClientTransaction(client
						.getClientNumber());
				// if there is an existing transaction then get the code so it
				// can be updated
				if (tramadaClientTransaction != null) {
					if (tramadaClientTransaction
							.getTramadaClientTransactionPK()
							.getActionType()
							.equals(TramadaClientProfileImporter.TRAMADA_ACTION_DELETE)) {
						// if last transaction was a delete then don't record
						// the current transaction or send a tramada transaction
						recordTransaction = false;
					} else {
						tramadaProfileCode = tramadaClientTransaction
								.getTramadaClientTransactionPK()
								.getTramadaProfileCode();
						LogUtil.debug(this.getClass(), "tramadaProfileCode="
								+ tramadaProfileCode);
						debtorCode = tramadaClientTransaction.getCompanyId();
						LogUtil.debug(this.getClass(), "debtorCode="
								+ debtorCode);
					}
				}
			}

		}

		if (tramadaProfileCode != null) {
			// convert the RACT client to Tramada format in XML using Progress
			// implementation
			ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
			LogUtil.debug(this.getClass(), "tramadaProfileCode is not null");
			String xml = clientAdapter.getTramadaClientXML(action,
					tramadaProfileCode, debtorCode, userId, salesBranchCode,
					client);
			LogUtil.debug(this.getClass(), xml);

			try {
				// send the XML "file" to Tramada
				TramadaClientProfileImporter tcpi = new TramadaClientProfileImporter();
				tcpi.sendTramadaClientXML(xml);
			} catch (GenericException ex) {
				throw new RemoteException(
						"Unable to send the Tramada client xml.", ex);
			}

			// do not delete the transaction records as we are not able to
			// retrieve the tramada client code reliably
			// for reinstate.
			if (recordTransaction) {
				// record the action in the Tramada Client Transaction table as
				// "A","U" or "R"
				TramadaClientTransaction tramadaClientTransaction = new TramadaClientTransaction();
				tramadaClientTransaction
						.setTramadaClientTransactionPK(new TramadaClientTransactionPK(
								client.getClientNumber(), tramadaProfileCode,
								action, new DateTime()));
				tramadaClientTransaction.setCompanyId(debtorCode);
				createTramadaClientTransaction(tramadaClientTransaction);
			}
		}
	}

	public void updateClient(ClientVO client) throws RemoteException {
		// set it to be null to make it compatible with the UNIX system.
		String clientStatus = client.getStatus();
		if (Client.STATUS_ACTIVE.equals(clientStatus)) {
			client.setStatus(null);
		}

		client.setDerivedFields();

		LogUtil.debug(this.getClass(), client.toString());
		LogUtil.debug(this.getClass(),
				"clientNumber=" + client.getClientNumber());
		LogUtil.debug(this.getClass(),
				"updateClient client=" + client.getPostalAddress());
		LogUtil.debug(this.getClass(),
				"updateClient client=" + client.getPostStsubId());

		// Insert the new client
		try {
			hsession.update(client);
		} catch (HibernateException e) {
			LogUtil.warn(this.getClass(), e.getMessage());
			hsession.merge(client);
		}

	}

	public Collection<ClientHistory> getClientHistory(Integer clientNumber) {
		Criteria crit = hsession.createCriteria(ClientHistory.class);
		List list = crit
				.add(Restrictions.eq("clientHistoryPK.clientNumber",
						clientNumber))
				.addOrder(Order.desc("clientHistoryPK.modifyDateTime")).list();
		return list;
	}

	public ClientHistory getClientHistory(ClientHistoryPK clientHistoryPK) {
		ClientHistory clientHistory = (ClientHistory) hsession.get(
				ClientHistory.class, clientHistoryPK);
		return clientHistory;
	}

	public ClientHistory getLastClientHistory(Integer clientNumber) {
		return getLastClientHistory(clientNumber, false);
	}

	/**
	 * Get the last client history object.
	 * 
	 * @param clientNumber
	 *            The client number
	 * @param excludeCurrent
	 *            Exclude any history with the status of CUR
	 * @return
	 */
	public ClientHistory getLastClientHistory(Integer clientNumber,
			boolean excludeCurrent) {
		ClientHistory clientHistory = null;
		Collection<ClientHistory> list = getClientHistory(clientNumber);
		ClientHistory last = null;
		for (Iterator<ClientHistory> i = list.iterator(); i.hasNext()
				&& clientHistory == null;) {
			last = i.next();
			if (excludeCurrent) {
				// last transaction reason can't be equal to current
				if (!last.getTransactionReason().equals(Client.HISTORY_CURRENT)) {
					clientHistory = last;
				}
			} else {
				clientHistory = last;
			}
		}
		return clientHistory;
	}

	/**
	 * 2 hour transaction timeout
	 * 
	 * @param countOnly
	 * @throws GenericException
	 * @throws RemoteException
	 */
	public void addressUnknownNotifications(boolean countOnly)
			throws GenericException, RemoteException {

		LogUtil.log(this.getClass(), "addressUnknownNotifications start");

		LogUtil.debug(this.getClass(), "countOnly=" + countOnly);

		DateTime now = new DateTime(); // timestamp
		LogUtil.debug(this.getClass(), "now=" + now);

		FileWriter phoneListWriter = null;

		String auLastUpdate = null;
		String auPhoneListFileName = null;
		String auPhoneListEmail = null;
		String auAuditFileName = null;
		String auAuditEmail = null;
		Interval lastUpdateStart = null;
		DateTime lastUpdate = null;
		try {
			auPhoneListFileName = commonMgr.getSystemParameterValue(
					SystemParameterVO.CATEGORY_MEMBERSHIP,
					"AUPHONELISTFILENAME");
			auPhoneListEmail = commonMgr.getSystemParameterValue(
					SystemParameterVO.CATEGORY_MEMBERSHIP, "AUPHONELISTEMAIL");
			auAuditFileName = commonMgr.getSystemParameterValue(
					SystemParameterVO.CATEGORY_MEMBERSHIP, "AUAUDITFILENAME");
			auAuditEmail = commonMgr.getSystemParameterValue(
					SystemParameterVO.CATEGORY_MEMBERSHIP, "AUAUDITEMAIL");
			auLastUpdate = commonMgr.getSystemParameterValue(
					SystemParameterVO.CATEGORY_MEMBERSHIP, "AULASTUPDATE");

			lastUpdateStart = new Interval(auLastUpdate);
			lastUpdate = now.subtract(lastUpdateStart);

		} catch (Exception e1) {
			throw new GenericException(e1);
		}

		LogUtil.debug(this.getClass(), "auFileName=" + auPhoneListFileName);
		LogUtil.debug(this.getClass(), "auPhoneListEmail=" + auPhoneListEmail);
		LogUtil.debug(this.getClass(), "auAuditFileName=" + auAuditFileName);
		LogUtil.debug(this.getClass(), "auAuditEmail=" + auAuditEmail);
		LogUtil.debug(this.getClass(), "lastUpdate=" + lastUpdate);

		User roadsideUser = null;
		try {
			roadsideUser = SecurityHelper.getRoadsideUser();
		} catch (RemoteException e1) {
			throw new GenericException(e1);
		}
		LogUtil.debug(this.getClass(), "auAuditEmail=" + auAuditEmail);

		StringBuffer phoneList = new StringBuffer();

		final String SMS_TOKEN_SALUTATION = "<salutation/>";

		// if count only option used then don't create client notes
		// count per communication type (eg. SMS-AU 3, EMAIL-AU 11, PHONE-AU 2,
		// NOCONTACT-AU 2)

		int emailCount = 0;
		int smsCount = 0;
		int phoneCount = 0;
		int noContactCount = 0;

		// get a list of all clients with a status of address unknown which
		// don't
		// have a client note with a type of SMS-AU,
		// EMAIL-AU or PHONE-AU - only include active notes (ie. not inactive -
		// read
		// "bounced")
		List clientList = null;
		try {

			Query query = em
					.createQuery("select cl from ClientVO cl where cl.status = :cstatus and not exists (select cn from ClientNote cn where cn.clientNumber = cl.clientNumber and cn.noteType like '%-AU' and (cn.cancelDate is null) ) and cl.lastUpdate > :lastUpdate ");
			query.setParameter("cstatus", Client.STATUS_ADDRESS_UNKNOWN);
			query.setParameter("lastUpdate", lastUpdate);
			clientList = query.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LogUtil.log(this.getClass(), "clientList.size()=" + clientList.size());

		boolean sms = false;
		boolean email = false;
		boolean phone = false;

		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper
				.getMembershipMgrLocal();

		ClientVO client = null;
		// for each client
		for (Iterator<ClientVO> i = clientList.iterator(); i.hasNext();) {
			boolean skipGroupClient = false;

			sms = false;
			email = false;
			phone = false;

			client = i.next();

			MembershipVO memVO = membershipMgrLocal
					.findMembershipByClientAndType(client.getClientNumber(),
							MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
			if (memVO != null
					&& memVO.getStatus().equals(MembershipVO.STATUS_CANCELLED)) {
				// skip cancelled memberships
				continue;
			}

			LogUtil.debug(
					this.getClass(),
					client.getClientNumber() + ": name="
							+ client.getDisplayName() + ", m="
							+ client.getMobilePhone() + ", e="
							+ client.getEmailAddress() + ", p="
							+ client.getHomePhone());

			// if email is not null and is valid (based on loose validation)
			// then
			// email, record client note of EMAIL-AU
			if (client.getEmailAddress() != null
					&& client.getEmailAddress().length() > 0) {
				try {
					StringUtil.validateEmail(client.getEmailAddress(), false);
					email = true;
				} catch (Exception e) {
					email = false;
				}

				// bounces?
			}
			LogUtil.debug(this.getClass(), "email=" + email);
			if (!email) {

				// if sms is yes and mobile is not null and valid then SMS,
				// record
				// client note of SMS-AU
				if (client.getMobilePhone() != null
						&& client.getMobilePhone().length() > 0) {
					try {
						NumberUtil.validatePhoneNumber(
								NumberUtil.PHONE_TYPE_MOBILE,
								client.getMobilePhone());
						if (client.getSmsAllowed()) {
							sms = true;
						}
					} catch (Exception e) {
						sms = false;
					}
				}

				LogUtil.debug(this.getClass(), "sms=" + sms);

				if (!email && !sms) {
					// home phone
					if (client.getHomePhone() != null
							&& client.getHomePhone().length() > 0) {
						try {
							NumberUtil.validatePhoneNumber(
									NumberUtil.PHONE_TYPE_HOME,
									client.getHomePhone());
							phone = true;
						} catch (Exception e) {
							phone = false;
						}
					}

				}

				if (!email && !sms && !phone) {
					// no contact
					LogUtil.debug(this.getClass(), "no contact");
				}

			}

			LogUtil.debug(this.getClass(), "1 email=" + email + ", sms=" + sms
					+ ", phone=" + phone);

			if (email || sms || phone) {
				// if group member is marked as AU and shares the same mobile
				// phone as the group client then send
				// to member only and suppress to group client
				if (client.getGroupClient()) {
					LogUtil.log(this.getClass(), "Group client.");
					try {
						// get member clients
						Collection<ClientVO> clientGroup = getClientGroup(client
								.getClientNumber());
						if (clientGroup != null && !clientGroup.isEmpty()) {
							ClientVO member = null;
							for (Iterator<ClientVO> x = clientGroup.iterator(); x
									.hasNext() && !skipGroupClient;) {
								member = x.next();
								if (sms
										&& client.getMobilePhone() != null
										&& member.getMobilePhone() != null
										&& client.getMobilePhone().equals(
												member.getMobilePhone())) {
									LogUtil.debug(this.getClass(),
											"Matching mobile phone number with "
													+ member.getClientNumber());
									skipGroupClient = true;
								} else if (email
										&& client.getEmailAddress() != null
										&& member.getEmailAddress() != null
										&& client.getEmailAddress().equals(
												member.getEmailAddress())) {
									LogUtil.debug(this.getClass(),
											"Matching email address with "
													+ member.getClientNumber());
									skipGroupClient = true;
								} else if (phone
										&& client.getHomePhone() != null
										&& member.getHomePhone() != null
										&& client.getHomePhone().equals(
												member.getHomePhone())) {
									LogUtil.debug(this.getClass(),
											"Matching home phone with "
													+ member.getClientNumber());
									skipGroupClient = true;
								}
								// else no match and ok
							}

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						LogUtil.warn(
								this.getClass(),
								"Error evaluating group details: "
										+ e.getMessage());
						continue;
					}
				} // is group client?

			}

			LogUtil.debug(this.getClass(), "skipGroupClient=" + skipGroupClient);
			LogUtil.debug(this.getClass(), "1 emailCount=" + emailCount
					+ ", smsCounter=" + smsCount + ", phoneCount=" + phoneCount);
			String noteType = null;
			if (!skipGroupClient) {
				if (email) {
					noteType = NoteType.TYPE_ADDRESS_UNKNOWN_EMAIL;
					emailCount++;
				} else if (sms) {
					noteType = NoteType.TYPE_ADDRESS_UNKNOWN_SMS;
					smsCount++;
				} else if (phone) {
					noteType = NoteType.TYPE_ADDRESS_UNKNOWN_PHONE;
					phoneCount++;
				} else {
					noteType = NoteType.TYPE_ADDRESS_UNKNOWN_NO_CONTACT;
					noContactCount++;
				}
			} else {
				// reset any other flags as it will be counted as no contact
				// even if another flag has already been set.
				email = false;
				sms = false;
				phone = false;

				// prevent it from being sent again
				noteType = NoteType.TYPE_ADDRESS_UNKNOWN_NO_CONTACT;
				noContactCount++;
			}
			LogUtil.debug(this.getClass(), "2 emailCount=" + emailCount
					+ ", smsCount=" + smsCount + ", phoneCount=" + phoneCount);
			LogUtil.debug(this.getClass(), "noteType=" + noteType);
			String smsAddress = null;
			if (!countOnly) {

				// send message

				// record the messsage and send the address unknown reminder
				ClientNote clientNote = new ClientNote();
				clientNote.setNoteType(noteType); // eg. SMS-AU
				clientNote
						.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP); // MEM
				clientNote.setCreated(now);
				clientNote.setCreateId(roadsideUser.getUserID());
				clientNote.setClientNumber(client.getClientNumber());

				LogUtil.debug(this.getClass(), "clientNote=" + clientNote);

				try {
					LogUtil.debug(this.getClass(), "Save client note.");
					clientMgr.createClientNoteNew(clientNote); // new
					// transaction
				} catch (Exception e) {
					LogUtil.warn(
							this.getClass(),
							"Error inserting ClientNote: "
									+ ExceptionHelper.getExceptionStackTrace(e));
					throw new GenericException(e);
				}
				LogUtil.debug(this.getClass(), "2 email=" + email + ", sms="
						+ sms + ", phone=" + phone);
				// send an email
				if (email) {
					String emailAddress = client.getEmailAddress();
					LogUtil.debug(this.getClass(), "emailAddress="
							+ emailAddress);
					try {
						String overrideEmail = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										"AUEMAILOVERRIDE");
						LogUtil.debug(this.getClass(), "overrideEmail="
								+ overrideEmail);
						if (overrideEmail != null
								&& overrideEmail.trim().length() > 0) {
							// check email address
							overrideEmail = StringUtil.validateEmail(
									overrideEmail, false);
							emailAddress = overrideEmail;
						}
						LogUtil.debug(this.getClass(), "emailAddress="
								+ emailAddress);

						final String emailMessage = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										"AUEMAILMESSAGE");
						final String fromName = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										"AUEMAILFROMNAME");
						// eg. Greg Hankin
						final String fromTitle = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										"AUEMAILFROMTITLE");
						// eg. General Manager Members Services
						final String subject = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										"AUEMAILSUBJECT");
						final String returnEmail = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										"AUEMAILRETURNPATH");

						LogUtil.debug(this.getClass(), "emailMessage="
								+ emailAddress);
						LogUtil.debug(this.getClass(), "fromName=" + fromName);
						LogUtil.debug(this.getClass(), "fromTitle=" + fromTitle);
						LogUtil.debug(this.getClass(), "subject=" + subject);
						LogUtil.debug(this.getClass(), "returnEmail="
								+ returnEmail);

						String addressTitle = ClientHelper.formatCustomerName(
								client.getTitle(), client.getGivenNames(),
								client.getSurname(), false, true, false, false);
						LogUtil.log(this.getClass(), "addressTitle="
								+ addressTitle);

						Hashtable var = new Hashtable();
						var.put("addressTitle", addressTitle);
						var.put("fromName", fromName);
						var.put("fromTitle", fromTitle);
						// email parameters
						String body = StringUtil.replaceTag(emailMessage, "<",
								">", var);
						LogUtil.log(this.getClass(), "body=" + body);

						MailMessage message = new MailMessage();
						message.setRecipient(emailAddress);
						message.setMessage(body);
						message.setSubject(subject);
						message.setHtml(true);
						message.setFrom(returnEmail);

						LogUtil.debug(this.getClass(), "message=" + message);

						// send email
						LogUtil.log(this.getClass(), "Send an email.");

						mailMgr.sendMail(message);

					} catch (Exception e) {
						LogUtil.warn(this.getClass(),
								"Error sending sms: " + e.getMessage());
						continue;
					}

				} else if (sms) {
					try {
						final String SMS_SUFFIX = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										SystemParameterVO.SMS_SUFFIX); // "@onlinesms.telstra.com"
						// final String SMS_SUBJECT = docType;
						final String SMS_REPLY_TO_EMAIL = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										SystemParameterVO.AU_SMS_REPLY_TO_EMAIL);
						final String SMS_OVERRIDE = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										SystemParameterVO.SMS_OVERRIDE);
						// final String SMS_SUBJECT = ""; //no subject
						final String SMS_MESSAGE = commonMgr
								.getSystemParameterValue(
										SystemParameterVO.CATEGORY_MEMBERSHIP,
										SystemParameterVO.AU_SMS_MESSAGE);

						LogUtil.debug(this.getClass(), "SMS_SUFFIX="
								+ SMS_SUFFIX);
						LogUtil.debug(this.getClass(), "SMS_REPLY_TO_EMAIL="
								+ SMS_REPLY_TO_EMAIL);
						LogUtil.debug(this.getClass(), "SMS_OVERRIDE_EMAIL="
								+ SMS_OVERRIDE);
						LogUtil.debug(this.getClass(), "SMS_MESSAGE="
								+ SMS_MESSAGE);

						String salutation = ClientHelper
								.getSMSSalutation(client);
						LogUtil.debug(this.getClass(), "salutation="
								+ salutation);

						String smsMessage = SMS_MESSAGE;

						if (salutation.length() > 0) {
							// convert to form "Dear <Salutation>, "
							smsMessage = StringUtil.replaceAll(smsMessage,
									SMS_TOKEN_SALUTATION, salutation);
							// remove pipe delimiter
							smsMessage = StringUtil
									.replace(smsMessage, "|", "");
						} else {
							// split at pipe delimiter
							smsMessage = smsMessage.split("[|]")[1];
							// capitalise the first letter.
							smsMessage = smsMessage.substring(0, 1)
									.toUpperCase()
									+ smsMessage.substring(1,
											smsMessage.length());
						}
						LogUtil.debug(this.getClass(), "smsMessage="
								+ smsMessage);
						// valid override email
						if (SMS_OVERRIDE != null
								&& SMS_OVERRIDE.indexOf("@") != -1) {
							smsAddress = SMS_OVERRIDE;
						} else {
							String mobileNumber = client.getMobilePhone();
							// construct sms number in correct format for MessageMedia
							// +61413777666
							if (mobileNumber.indexOf("0") == 0)
								mobileNumber = mobileNumber.substring(1);
							
							smsAddress = AUS_PREFIX + mobileNumber;// + SMS_SUFFIX;
							smsAddress = client.getMobilePhone() + SMS_SUFFIX;
						}

						LogUtil.debug(this.getClass(), "smsMessage="
								+ smsMessage);

//						MailMessage message = new MailMessage();
//						message.setRecipient(smsAddress);
//						message.setMessage(smsMessage);
//						message.setFrom(SMS_REPLY_TO_EMAIL);
//
//						LogUtil.debug(this.getClass(), "message=" + message);

						LogUtil.debug(this.getClass(), "Send sms message");
						// send the SMS as an email

						// Create the SMS manager instance
						SMSManager smsManager = new SMSManager();
						
						// Create the parameters map and populate
						HashMap<String,String> smsParams = new HashMap<String,String>();
						smsParams.put(smsAddress, smsMessage);
						
						// Send the SMS to the Mule API
						smsManager.sendSMS(smsParams);
//						mailMgr.sendMail(message);
					} catch (Exception e) {
						LogUtil.warn(this.getClass(), "Error sending sms: "
								+ e.getMessage());
						continue;
					}

				} else if (phone) {
					phoneList.append(client.getClientNumber());
					phoneList.append(FileUtil.SEPARATOR_COMMA);
					phoneList.append(Client.STATUS_ADDRESS_UNKNOWN);
					phoneList.append(FileUtil.NEW_LINE);
				} else {
					LogUtil.debug(this.getClass(), "No phone action.");
					// no action
				}

			}

			// else record client not of NOCONTACT-AU (inc. clients with only
			// work
			// phone)

		} // each client

		LogUtil.debug(this.getClass(), "phoneList=" + phoneList);

		// send file to membership admin
		if (phoneList != null && phoneList.length() > 0) {

			try {

				FileUtil.writeFile(auPhoneListFileName, phoneList.toString());

				Hashtable files = new Hashtable();
				files.put("au_phone_list.csv", auPhoneListFileName);

				// email file
				MailMessage message = new MailMessage();
				message.setRecipient(auPhoneListEmail);
				message.setFiles(files);
				message.setSubject("Address Unknown Phone List File");

				LogUtil.debug(this.getClass(), "message=" + message);

				LogUtil.debug(this.getClass(), "send phone list email");

				// send the phone list as an attachment to an email
				mailMgr.sendMail(message);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// only send an audit file if there were any records processed
		if (countOnly
				|| (smsCount > 0 || emailCount > 0 || phoneCount > 0 || noContactCount > 0)) {

			try {
				// construct audit file
				StringBuffer audit = new StringBuffer();
				audit.append("Email");
				audit.append(FileUtil.SEPARATOR_COMMA);
				audit.append(emailCount);
				audit.append(FileUtil.NEW_LINE);
				audit.append("SMS");
				audit.append(FileUtil.SEPARATOR_COMMA);
				audit.append(smsCount);
				audit.append(FileUtil.NEW_LINE);
				audit.append("Phone");
				audit.append(FileUtil.SEPARATOR_COMMA);
				audit.append(phoneCount);
				audit.append(FileUtil.NEW_LINE);
				audit.append("No Contact");
				audit.append(FileUtil.SEPARATOR_COMMA);
				audit.append(noContactCount);
				audit.append(FileUtil.NEW_LINE);
				FileUtil.writeFile(auAuditFileName, audit.toString());

				LogUtil.debug(this.getClass(), "auAuditFileName="
						+ auAuditFileName);
				Hashtable files = new Hashtable();
				files.put("au_audit.csv", auAuditFileName);

				// email audit file
				MailMessage message = new MailMessage();
				message.setRecipient(auPhoneListEmail);
				message.setFiles(files);
				message.setRecipient(auAuditEmail);
				message.setSubject("Address Unknown Audit File");
				LogUtil.debug(this.getClass(), "message=" + message);
				mailMgr.sendMail(message);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				LogUtil.warn(this.getClass(), "Unable to send audit file.");
			}
		}

		LogUtil.log(this.getClass(), "addressUnknownNotifications end");
	}

	public void deleteClient(ClientVO client, User user, boolean archive)
			throws RemoteException {
		// if the client being deleted is the master of another client then that
		// client is to also reference this one's master.
		List clientList = new ArrayList(hsession
				.createCriteria(ClientVO.class)
				.add(Restrictions.eq("masterClientNumber",
						client.getClientNumber()))
				.add(Restrictions.eq("status", Client.STATUS_DUPLICATE)).list());
		if (clientList != null && !clientList.isEmpty()) {
			ClientVO duplicateClient = null;
			Iterator clientListIt = clientList.iterator();
			while (clientListIt.hasNext()) {
				duplicateClient = (ClientVO) clientListIt.next();
				duplicateClient.setMasterClientNumber(client
						.getMasterClientNumber());
				// send update to progress, etc.
				updateClient(client, user.getUserID(), Client.HISTORY_UPDATE,
						user.getSalesBranchCode(), "",
						ClientTransaction.SYSTEM_ROADSIDE);
			}
		}

		// delete client history
		try {
			deleteClientHistory(client.getClientNumber());
		} catch (GenericException ex) {
			LogUtil.warn(this.getClass(), "Unable to delete client history: "
					+ ex);
		}

		// delete client subscriptions
		deleteClientSubscriptions(client.getClientNumber());

		// delete market options
		deleteMarketOptions(client.getClientNumber());

		// delete client notes
		deleteClientNotes(client.getClientNumber());

		// delete client tramada transactions
		deleteTramadaClientTransactions(client.getClientNumber());

		if (archive) {
			ArchivedClient archivedClient = new ArchivedClient();
			archivedClient.setClientNo(client.getClientNumber());
			archivedClient.setArchiveDate(new DateTime());
			archivedClient.setBirthDate(client.getBirthDate());
			// business?
			archivedClient.setGivenNames(client.getGivenNames());
			archivedClient.setInitials(client.getInitials());
			archivedClient.setProperty(client.getResidentialAddress()
					.getProperty());
			archivedClient
					.setStreet(client.getResidentialAddress().getStreet());
			archivedClient.setStreetChar(client.getResiStreetChar());
			archivedClient
					.setSuburb(client.getResidentialAddress().getSuburb());
			archivedClient.setAddressPostcode(client.getResidentialAddress()
					.getPostcode());
			archivedClient.setSurname(client.getSurname());
			archivedClient.setCreateDate(client.getMadeDate());
			archivedClient.setLastUpdate(client.getLastUpdate());
			archivedClient.setStatus(client.getStatus());
			archivedClient.setCreateLogonId(client.getMadeID());
			hsession.save(archivedClient);
		}

		// delete the new client
		hsession.delete(client);

	}

	/**
	 * Update a client after checking validation.
	 * 
	 * @param cvo
	 *            The client value object.
	 * @param chvo
	 *            Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public ClientVO updateClient(ClientVO newClient, String userID,
			String transactionReason, String salesBranchCode, String comments,
			String subSystem) throws RemoteException {
		LogUtil.debug(this.getClass(), "newClient=" + newClient);
		LogUtil.debug(this.getClass(), "userID=" + userID);
		LogUtil.debug(this.getClass(), "transactionReason=" + transactionReason);
		LogUtil.debug(this.getClass(), "salesBranchCode=" + salesBranchCode);
		LogUtil.debug(this.getClass(), "comments=" + comments);
		LogUtil.debug(this.getClass(), "subSystem=" + subSystem);

		if (newClient == null) {
			throw new SystemException("Error: no client to update.");
		}

		ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
		NotificationMgr notificationMgr = CommonEJBHelper.getNotificationMgr();

		try {
			LogUtil.debug(this.getClass(), "get old client");
			// get existing state (in existing transaction as it is not called
			// via remote interface)
			// ClientVO oldClient = this.getClient(newClient.getClientNumber());

			// LogUtil.debug(this.getClass(),"oldClient="+oldClient);
			// LogUtil.debug(this.getClass(),"updateClient oldClient="+oldClient.getPostalAddress());
			// LogUtil.debug(this.getClass(),"updateClient oldClient="+oldClient.getPostStsubId());

			LogUtil.debug(this.getClass(), "set the branch number");
			// update the branch number
			setBranchNumber(newClient);

			LogUtil.debug(this.getClass(), "create client history");

			LogUtil.debug(this.getClass(), "evict old client");
			// evict old instance from the session as otherwise session
			// validation
			// picks up, CORRECTLY, that there are two client objects with the
			// same identifier in scope.
			// HibernateUtil.getCurrentSession().evict(oldClient);

			LogUtil.debug(this.getClass(), "update existing client");

			// store a copy of subSystem here for convenience
			newClient.setSubSystem(subSystem);
			newClient.setLastUpdate(new DateTime());

			// update client record - made id and made date will be ignored,
			// last update will be updated
			updateClient(newClient);

			// Create the history
			createClientHistory(newClient, userID, transactionReason,
					salesBranchCode, comments, subSystem);

			if (!subSystem.equals(SourceSystem.PROGRESS_CLIENT
					.getAbbreviation())) {
				LogUtil.debug(this.getClass(), "update Progress client");

				// update progress client
				clientAdapter.updateClient(newClient, userID, salesBranchCode,
						comments, subSystem);

				// clear fields
				// birth date can't be not applicable
				clientAdapter.clearAskable(newClient.getClientNumber(),
						ClientVO.FIELD_NAME_HOME_PHONE);
				clientAdapter.clearAskable(newClient.getClientNumber(),
						ClientVO.FIELD_NAME_WORK_PHONE);
				clientAdapter.clearAskable(newClient.getClientNumber(),
						ClientVO.FIELD_NAME_MOBILE_PHONE);
				clientAdapter.clearAskable(newClient.getClientNumber(),
						ClientVO.FIELD_NAME_EMAIL);

				addFieldUpdates(newClient, clientAdapter);
			}

			LogUtil.debug(this.getClass(), "update Tramada client");
			// update tramada client
			transferTramadaClientXML(Client.TRAMADA_MODE_UPDATE,
					TramadaClientProfileImporter.TRAMADA_ACTION_UPDATE, userID,
					salesBranchCode, newClient);

			LogUtil.debug(this.getClass(), "update debtor");
			// update debtor
			transferDebtor(newClient);

			clientAdapter.commit();

			LogUtil.debug(this.getClass(), "notify the event");
			NotificationEvent event = new ClientChangeNotificationEvent(
					ClientChangeNotificationEvent.ACTION_UPDATE, newClient);
			event.setDelay(100);
			notificationMgr.notifyEvent(event);
			notificationMgr.commit();

		} catch (Exception e) {
			LogUtil.warn(getClass(),
					"*** Starting Rollback for updateClient...");
			if (notificationMgr != null) {
				notificationMgr.rollback();
			}
			clientAdapter.rollback();
			LogUtil.warn(getClass(), "*** Completed Rollback for updateClient.");
			throw new SystemException("updateClient failed : ", e);
		}

		return newClient;
	}

	private void addFieldUpdates(ClientVO newClient, ClientAdapter clientAdapter)
			throws RollBackException {
		if (newClient.getHomePhone() != null
				&& newClient.getHomePhone().equals(
						ClientVO.FIELD_STATUS_NOT_APPLICABLE)) {
			clientAdapter.addFieldUpdate(newClient.getClientNumber(),
					ClientVO.FIELD_NAME_HOME_PHONE, newClient.getHomePhone());
		}
		if (newClient.getWorkPhone() != null
				&& newClient.getWorkPhone().equals(
						ClientVO.FIELD_STATUS_NOT_APPLICABLE)) {
			clientAdapter.addFieldUpdate(newClient.getClientNumber(),
					ClientVO.FIELD_NAME_WORK_PHONE, newClient.getWorkPhone());
		}
		if (newClient.getMobilePhone() != null
				&& newClient.getMobilePhone().equals(
						ClientVO.FIELD_STATUS_NOT_APPLICABLE)) {
			clientAdapter.addFieldUpdate(newClient.getClientNumber(),
					ClientVO.FIELD_NAME_MOBILE_PHONE,
					newClient.getMobilePhone());
		}
		if (newClient.getEmailAddress() != null
				&& newClient.getEmailAddress().equals(
						ClientVO.FIELD_STATUS_NOT_APPLICABLE)) {
			clientAdapter.addFieldUpdate(newClient.getClientNumber(),
					ClientVO.FIELD_NAME_EMAIL, newClient.getEmailAddress());
		}
	}

	public ClientHistory updateClientHistory(ClientHistory clientHistory) {
		LogUtil.debug(this.getClass(), "updateClientHistory start");
		hsession.update(clientHistory);
		LogUtil.debug(this.getClass(), "updateClientHistory end");
		return clientHistory;
	}

	public ClientNote updateClientNote(ClientNote clientNote)
			throws RemoteException {
		// optional
		notifyClientNote(clientNote);

		return em.merge(clientNote);
	}

	private void notifyClientNote(ClientNote clientNote) throws RemoteException {
		ClientNote prevState = null;
		if (clientNote.getNoteId() != null) {
			prevState = getClientNote(clientNote.getNoteId());
		}

		ClientVO client = getClient(clientNote.getClientNumber());
		NoteType noteType = getNoteType(clientNote.getNoteType());

		NoteCategory noteCategory = null;
		if (clientNote.getCategoryCode() != null) {
			noteCategory = getNoteCategory(clientNote.getCategoryCode());
		}
		NoteResult noteResult = null;
		if (clientNote.getResultCode() != null) {
			noteResult = getNoteResult(clientNote.getResultCode());
		}

		Collection<ClientNoteComment> comments = clientNote
				.getClientNoteComments();

		// emails
		if (prevState != null && // must be a previous state or we can't update
				// it!
				clientNote.getCompletionDate() != null && // the new client note
				// has a completion
				// date
				!clientNote.getCompletionDate().equals(
						prevState.getCompletionDate())) // the
		// new
		// completion
		// date
		// is
		// not
		// null
		// and
		// does
		// not
		// equal
		// the
		// old
		// one
		// -
		// to
		// avoid
		// comment
		// updates.
		{
			StringBuffer body = new StringBuffer();
			StringBuffer subject = new StringBuffer();
			if (noteType.isNotifiable()) {

				if (noteType.isReferral()) {
					LogUtil.log(this.getClass(), "NOTE_TYPE_REFERRAL "
							+ clientNote.getNoteType());
					User referrer = userMgr.getUser(clientNote.getSourceId());
					String referreeId = "";
					if (clientNote.getTargetId() != null
							&& !clientNote.getTargetId().trim().equals("")) {
						User referee = userMgr
								.getUser(clientNote.getTargetId());
						referreeId = referee.getUsername();
					} else if (clientNote.getTargetSalesBranch() != null
							&& !clientNote.getTargetSalesBranch().trim()
									.equals("")) {
						referreeId = commonMgr.getSalesBranch(
								clientNote.getTargetSalesBranch())
								.getSalesBranchName();
					}

					subject.append(StringUtil.toProperCase(clientNote
							.getNoteType())
							+ " from "
							+ referrer.getUsername()
							+ " for "
							+ client.getClientNumber()
							+ " "
							+ client.getDisplayName() + " has been completed");

					body.append("Thankyou for providing the business referral detailed below:"
							+ FileUtil.NEW_LINE);
					body.append(FileUtil.NEW_LINE);
					body.append("The outcome of the referral was: "
							+ noteResult.getResultText() + FileUtil.NEW_LINE);
					body.append(FileUtil.NEW_LINE);
					body.append("Client Number: "
							+ clientNote.getClientNumber() + FileUtil.NEW_LINE);
					body.append("Name: " + client.getDisplayName()
							+ FileUtil.NEW_LINE);
					body.append("Referred to: " + referreeId
							+ FileUtil.NEW_LINE);
					body.append("Background information for referral: "
							+ clientNote.getNoteText() + FileUtil.NEW_LINE);
					body.append("Completion Notes: "
							+ clientNote.getCompletionNotes()
							+ FileUtil.NEW_LINE);
					body.append("Completion Reference: "
							+ clientNote.getCompletionReferenceId()
							+ FileUtil.NEW_LINE);
					addComments(comments, body);

					MailMessage message = new MailMessage();
					message.setRecipient(referrer.getEmailAddress());
					message.setSubject(subject.toString());
					message.setMessage(body.toString());

					mailMgr.sendMail(message);

				} else if (noteType.isFollowup()) {
					LogUtil.log(this.getClass(), "NOTE_TYPE_FOLLOW_UP "
							+ clientNote.getNoteType());
					User creator = userMgr.getUser(clientNote.getCreateId());
					User assignee = userMgr.getUser(noteCategory
							.getNotificationUserId());

					subject.append(StringUtil.toProperCase(clientNote
							.getNoteType())
							+ " for client "
							+ clientNote.getClientNumber()
							+ " "
							+ client.getDisplayName() + " has been completed");

					body.append("Client Number: "
							+ clientNote.getClientNumber() + FileUtil.NEW_LINE);
					body.append("Note: " + clientNote.getNoteText()
							+ FileUtil.NEW_LINE);
					body.append("Category: " + clientNote.getCategoryCode()
							+ FileUtil.NEW_LINE);
					body.append("Due: " + clientNote.getDue()
							+ FileUtil.NEW_LINE);
					String notificationName = "";
					if (assignee != null) {
						notificationName = assignee.getUsername() + " ("
								+ assignee.getEmailAddress() + ")";
					} else {
						notificationName = noteCategory
								.getNotificationEmailAddress();
					}
					body.append("Assignee/Notifications: " + notificationName
							+ FileUtil.NEW_LINE);
					body.append("The outcome of the followup was: "
							+ noteResult.getResultText() + FileUtil.NEW_LINE);
					body.append("Completion Notes: "
							+ clientNote.getCompletionNotes()
							+ FileUtil.NEW_LINE);
					addComments(comments, body);

					MailMessage message = new MailMessage();
					message.setRecipient(creator.getEmailAddress());
					message.setSubject(subject.toString());
					message.setMessage(body.toString());

					mailMgr.sendMail(message);
				}

			}
		} else {
			StringBuffer body = new StringBuffer();
			StringBuffer subject = new StringBuffer();
			if (noteType.isNotifiable()) {

				if (noteType.isReferral()) {
					LogUtil.log(this.getClass(), "NOTE_TYPE_REFERRAL "
							+ clientNote.getNoteType());
					User referrer = userMgr.getUser(clientNote.getSourceId());
					User referee = userMgr.getUser(clientNote.getTargetId());

					String refereeEmailAddress = null;
					if (clientNote.getTargetSalesBranch() != null
							&& !clientNote.getTargetSalesBranch().trim()
									.equals("")) {
						SalesBranchVO salesBranch = commonMgr
								.getSalesBranch(clientNote
										.getTargetSalesBranch());
						refereeEmailAddress = salesBranch.getEmailAddress();
					} else {
						refereeEmailAddress = referee.getEmailAddress();
					}

					subject.append(StringUtil.toProperCase(clientNote
							.getNoteType())
							+ " from "
							+ referrer.getUsername()
							+ " for "
							+ client.getClientNumber()
							+ " "
							+ client.getDisplayName());
					if (prevState == null) {
						subject.append(" has been created");
					}

					body.append("Thankyou for providing the following business referral:"
							+ FileUtil.NEW_LINE);
					body.append(FileUtil.NEW_LINE);
					body.append("Client Number: "
							+ clientNote.getClientNumber() + FileUtil.NEW_LINE);
					body.append("Name: " + client.getDisplayName()
							+ FileUtil.NEW_LINE);
					body.append("Business Type: "
							+ clientNote.getBusinessType() + FileUtil.NEW_LINE);
					body.append("Background information for referral: "
							+ clientNote.getNoteText() + FileUtil.NEW_LINE);
					addComments(comments, body);

					// send two emails - one to the referrer and one to the
					// referee
					MailMessage message = new MailMessage();
					message.setRecipient(referrer.getEmailAddress());
					message.setSubject(subject.toString());
					message.setMessage(body.toString());
					mailMgr.sendMail(message);

					body = new StringBuffer();
					body.append("The following business opportunity has been referred to you for action:"
							+ FileUtil.NEW_LINE);
					body.append(FileUtil.NEW_LINE);
					body.append("Referred by: " + referrer.getUsername()
							+ FileUtil.NEW_LINE);
					body.append("Client Number: "
							+ clientNote.getClientNumber() + FileUtil.NEW_LINE);
					body.append("Name: " + client.getDisplayName()
							+ FileUtil.NEW_LINE);
					body.append("Business Type: "
							+ clientNote.getBusinessType() + FileUtil.NEW_LINE);
					body.append("Background information for referral: "
							+ clientNote.getNoteText() + FileUtil.NEW_LINE);
					if (clientNote.getPreferredContactDate() != null) {
						body.append("Preferred date to call: "
								+ FORMAT_DATE.format(clientNote
										.getPreferredContactDate())
								+ FileUtil.NEW_LINE);
					}
					if (clientNote.getPreferredContactTime() != null) {
						body.append("Preferred time to call: "
								+ clientNote.getPreferredContactTime()
								+ FileUtil.NEW_LINE);
					}
					body.append("Preferred contact number: "
							+ clientNote.getPreferredContactNumber()
							+ FileUtil.NEW_LINE);
					addComments(comments, body);

					message = new MailMessage();
					message.setRecipient(refereeEmailAddress);
					message.setSubject(subject.toString());
					message.setMessage(body.toString());

					mailMgr.sendMail(message);
				} else if (noteType.isFollowup()) {
					LogUtil.log(this.getClass(), "NOTE_TYPE_FOLLOW_UP "
							+ clientNote.getNoteType());
					User creator = userMgr.getUser(clientNote.getCreateId());
					LogUtil.debug(this.getClass(),
							"noteCategory.getNotificationUserId()="
									+ noteCategory.getNotificationUserId());
					User assignee = userMgr.getUser(noteCategory
							.getNotificationUserId());
					LogUtil.debug(this.getClass(), "assignee=" + assignee);

					String assigneeEmailAddress = (noteCategory
							.getNotificationEmailAddress() != null && !noteCategory
							.getNotificationEmailAddress().trim().equals("")) ? noteCategory
							.getNotificationEmailAddress() : assignee
							.getEmailAddress();
					LogUtil.debug(this.getClass(), "assigneeEmailAddress="
							+ assigneeEmailAddress);

					subject.append(StringUtil.toProperCase(clientNote
							.getNoteType())
							+ " for client "
							+ clientNote.getClientNumber()
							+ " "
							+ client.getDisplayName());
					if (prevState == null) {
						subject.append(" has been created");
					}

					body.append("Client Number: "
							+ clientNote.getClientNumber() + FileUtil.NEW_LINE);
					body.append("Note: " + clientNote.getNoteText()
							+ FileUtil.NEW_LINE);
					body.append("Created by: " + creator.getUsername()
							+ FileUtil.NEW_LINE);

					body.append("Category: " + clientNote.getCategoryCode()
							+ FileUtil.NEW_LINE);
					body.append("Due: " + clientNote.getDue()
							+ FileUtil.NEW_LINE);
					String notificationName = "";
					if (assignee != null) {
						notificationName = assignee.getUsername() + " ("
								+ assignee.getEmailAddress() + ")";
					} else {
						notificationName = noteCategory
								.getNotificationEmailAddress();
					}

					body.append("Assignee/Notifications: "
							+ noteCategory.getNotificationEmailAddress()
							+ FileUtil.NEW_LINE);
					addComments(comments, body);

					MailMessage message = new MailMessage();
					message.setRecipient(creator.getEmailAddress());
					message.setSubject(subject.toString());
					message.setMessage(body.toString());

					// send two emails - one to the creator and one to the
					// assignee
					mailMgr.sendMail(message);

					message.setRecipient(assigneeEmailAddress);

					mailMgr.sendMail(message);
				} else if (noteType.isBlockTransactions()) {
					User creator = userMgr.getUser(clientNote.getCreateId());

					subject.append(StringUtil.toProperCase(clientNote
							.getNoteType())
							+ " for client "
							+ clientNote.getClientNumber()
							+ " "
							+ client.getDisplayName() + " has been created");

					body.append("Client Number: "
							+ clientNote.getClientNumber() + FileUtil.NEW_LINE);
					body.append("Note: " + clientNote.getNoteText()
							+ FileUtil.NEW_LINE);
					body.append("Created By: " + creator.getUsername()
							+ FileUtil.NEW_LINE);
					addComments(comments, body);

					MailMessage message = new MailMessage();
					message.setRecipient(noteType.getNotificationEmailAddress());
					message.setSubject(subject.toString());
					message.setMessage(body.toString());

					mailMgr.sendMail(message);
				}

			}
		}
	}

	private void addComments(Collection<ClientNoteComment> comments,
			StringBuffer body) {
		String leadSpace = "     ";
		if (comments != null && comments.size() > 0) {
			ClientNoteComment cnc = null;
			User creator = null;
			body.append("Comments:" + FileUtil.NEW_LINE + FileUtil.NEW_LINE);
			for (Iterator<ClientNoteComment> i = comments.iterator(); i
					.hasNext();) {
				cnc = i.next();
				try {
					creator = userMgr.getUser(cnc.getCreateId());
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				body.append("No: "
						+ cnc.getClientNoteCommentPK().getCommentSeq()
						+ FileUtil.NEW_LINE);
				body.append("Created: " + cnc.getCreated().formatMediumDate()
						+ FileUtil.NEW_LINE);
				body.append("Created By: " + creator.getUsername()
						+ FileUtil.NEW_LINE);
				body.append("Comment: " + cnc.getComment() + FileUtil.NEW_LINE
						+ FileUtil.NEW_LINE);
			}
		}
	}

	/**
	 * Update client business from duplicate client number to master client
	 * number.
	 * 
	 * @param clientNumber
	 *            Integer
	 * @param masterClientNumber
	 *            Integer
	 */
	private void updateClientBusiness(Connection connection,
			Integer duplicateClientNumber, Integer masterClientNumber,
			MembershipVO duplicateClientMembership,
			MembershipVO masterClientMembership,
			PaymentTransactionMgr paymentTxMgr, User user)
			throws RemoteException, RollBackException {

		// check if the client is a debtor
		if (duplicateClientMembership != null
				&& duplicateClientMembership.getClient() != null
				&& duplicateClientMembership.getClient().isDebtor()) {
			throw new RemoteException(
					"Unable to merge clients as the duplicate client being removed is a debtor in the finance system.");
		}

		// transfer membership
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper
				.getMembershipMgrLocal();
		membershipMgrLocal.transferMembership(connection,
				duplicateClientNumber, masterClientNumber,
				duplicateClientMembership, masterClientMembership);

		// transfer tramada
		TramadaClientTransaction tramadaMasterTransaction = getLastTramadaClientTransaction(masterClientNumber);
		TramadaClientTransaction tramadaDuplicationTransaction = getLastTramadaClientTransaction(duplicateClientNumber);
		if (tramadaDuplicationTransaction != null) {
			// create a merge followup request
			int tStamp = new DateTime().getSecondsFromMidnight();
			savePopupRequest(ClientMgrBean.POPUP_CLIENT_MERGE,
					user.getUserID(), user.getSalesBranchCode(), tStamp,
					duplicateClientMembership.getClient());

			// if no master transaction then send to tramada and create tramada
			// client transaction to link to it.
			if (tramadaMasterTransaction == null) {
				transferTramadaClientXML(Client.TRAMADA_MODE_EXPLICIT,
						TramadaClientProfileImporter.TRAMADA_ACTION_ADD,
						user.getUserID(), user.getSalesBranchCode(),
						masterClientMembership.getClient());
				tramadaMasterTransaction = tramadaDuplicationTransaction;
				// update client number
				tramadaMasterTransaction.getTramadaClientTransactionPK()
						.setClientNumber(masterClientNumber);
				createTramadaClientTransaction(tramadaMasterTransaction);
			} // else delete only

		}
		// update client vehicles
		Collection vehicles = getClientVehicleList(duplicateClientNumber);
		if (vehicles != null && vehicles.size() > 0) {
			for (Iterator<VehicleVO> i = vehicles.iterator(); i.hasNext();) {
				VehicleVO vehicle = i.next();
				vehicle.setClientNo(masterClientNumber);
				// create client vehicles
				createClientVehicle(vehicle);
			}
		}

		// update marketing options
		Collection marketingOptions = getMarketOptions(duplicateClientNumber);
		if (marketingOptions != null && marketingOptions.size() > 0) {
			Collection masterMarketOptions = getMarketOptions(masterClientNumber);
			if (masterMarketOptions == null || masterMarketOptions.size() == 0) {
				// create market options
				setMarketOptions(masterClientNumber, marketingOptions,
						user.getUserID());
			}// else we can't reliably merge them
		}

		// update client subscriptions
		Collection<ClientSubscription> dupSubscriptionList = getClientSubscriptionList(duplicateClientNumber);
		if (dupSubscriptionList != null && dupSubscriptionList.size() > 0) {
			for (Iterator<ClientSubscription> i = dupSubscriptionList
					.iterator(); i.hasNext();) {
				ClientSubscription dupSub = i.next();
				ClientSubscription masSub = getActiveClientSubscription(
						masterClientNumber, dupSub.getClientSubscriptionPK()
								.getSubscriptionCode());
				if (masSub == null) {
					// create master client subscription
					dupSub.getClientSubscriptionPK().setClientNumber(
							masterClientNumber);
					createClientSubscription(dupSub);
				}

			}
		}

		// update client notes to master client number
		Collection clientNotes = getClientNoteList(duplicateClientNumber);
		for (Iterator<ClientNote> i = clientNotes.iterator(); i.hasNext();) {
			ClientNote dupNote = i.next();
			dupNote.setClientNumber(masterClientNumber);
			// create client note
			createClientNote(dupNote);
		}

		// record the fact that merge has occurred as note
		ClientNote note = new ClientNote();
		note.setClientNumber(masterClientNumber);
		note.setNoteType(NoteType.TYPE_INFORMATION);
		note.setCreated(new DateTime());
		note.setCreateId("COM");
		note.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
		note.setNoteText("Merged client: " + duplicateClientNumber);
		createClientNote(note);

		// update EFT Transactions
		EftPaymentMgr eftPaymentMgr = PaymentEJBHelper.getEftPaymentMgr();
		eftPaymentMgr.updateClientEFTPayments(duplicateClientNumber,
				masterClientNumber);

		try {
			CustomerMgrRemote customerMgr = (CustomerMgrRemote) ServiceLocator
					.getInstance().getObject("CustomerMgr/remote");
			customerMgr.updateWebClientRACTClientNo(duplicateClientNumber,
					masterClientNumber);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}

		try {
			UserSecurityMgrRemote userSecurityMgr = (UserSecurityMgrRemote) ServiceLocator
					.getInstance().getObject("UserSecurityMgr/remote");
			userSecurityMgr.updateUserClientNumber(duplicateClientNumber,
					masterClientNumber);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}

		// update ExternalClientMatch
		transferExternalClients(duplicateClientNumber, masterClientNumber);

		// TODO consider updating debtor records

	}

	private final String SQL_UPDATE_EXTERNAL_CLIENTS = "update dbo.EC_ExternalClientMatch set client_number = ? where client_number = ?";

	public void transferExternalClients(Integer duplicateClientNumber,
			Integer masterClientNumber) throws RemoteException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			statement = connection
					.prepareStatement(SQL_UPDATE_EXTERNAL_CLIENTS);
			statement.setInt(1, masterClientNumber);
			statement.setInt(2, duplicateClientNumber);
			resultSet = statement.executeQuery();
			resultSet.close();
		} catch (SQLException ex) {
			throw new RemoteException("Error updating external clients "
					+ SQL_UPDATE_EXTERNAL_CLIENTS, ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	private final String SQL_SELECT_EXTERNAL_CLIENTS = "select * from dbo.EC_ExternalClientMatch where client_number = ?";

	public List<Map<String, String>> getExternalClients(Integer clientNo)
			throws RemoteException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		List<Map<String, String>> recordList = new ArrayList<Map<String, String>>();

		try {
			connection = dataSource.getConnection();
			statement = connection
					.prepareStatement(SQL_SELECT_EXTERNAL_CLIENTS);
			statement.setInt(1, clientNo);
			resultSet = statement.executeQuery();

			Map<String, String> record;

			while (resultSet.next()) {
				record = new HashMap<String, String>();
				record.put("sourceSystem", resultSet.getString(1));
				record.put("sourceSystemId", resultSet.getString(2));
				record.put("clientNumber",
						Integer.toString(resultSet.getInt(3)));

				recordList.add(record);
			}

			resultSet.close();
		} catch (SQLException ex) {
			throw new RemoteException("Error selecting external clients "
					+ SQL_SELECT_EXTERNAL_CLIENTS, ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return recordList;
	}

	public int getDeceasedClientsToRepairCount() throws RemoteException {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		return commonMgr.countRecords(SQL_ERROR_DECEASED_CLIENTS);
	}

	private final String SQL_ERROR_DECEASED_CLIENTS = "SELECT [client-no] FROM PUB.[cl-master] "
			+ "WHERE ((upper(surname) LIKE '%ESTATE%' AND upper(surname) LIKE '%LATE%') OR upper(surname) LIKE '%EOTL%') AND (upper([client-status]) <> 'DECEASED' OR [client-status] IS NULL)";

	private final String SQL_ERROR_ORGANISATION_CLIENTS = "select [client-no] from PUB.[cl-master] where initials = ? AND [given-names] = ? AND [client-title] = ? ";

	// private final String SQL_CLIENTS_WITHOUT_SUBSCRIPTIONS =
	// "SELECT [client_number] FROM [PUB].[mem_membership] INNER JOIN [PUB].[cl-master] ON [PUB].[mem_membership].client_number = [PUB].[cl-master].[client-no] WHERE [made-date] >= ? AND client_number NOT IN (SELECT DISTINCT client_number FROM [PUB].cl_subscription)";
	private final String SQL_CLIENTS_WITHOUT_SUBSCRIPTIONS = "SELECT [client-no] FROM [PUB].[cl-master] WHERE [made-date] >= ? AND [client-status] in ('Active','Address Unknown') AND [client-no] NOT IN (SELECT DISTINCT client_number FROM [PUB].cl_subscription)";

	/**
	 * Retrieves a list of clients having no subscriptions.
	 * 
	 * @param startDate
	 * @return
	 * @throws RemoteException
	 */
	public List<ClientVO> getClientsWithoutSubscriptions(DateTime startDate)
			throws RemoteException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		List<ClientVO> clientList = new ArrayList<ClientVO>();

		try {
			connection = dataSource.getConnection();
			statement = connection
					.prepareStatement(SQL_CLIENTS_WITHOUT_SUBSCRIPTIONS);
			statement.setDate(1, startDate.toSQLDate());
			resultSet = statement.executeQuery();
			Integer clientNumber = null;
			ClientVO client = null;

			while (resultSet.next()) {
				clientNumber = new Integer(resultSet.getInt(1));
				// get the client object
				client = this.getClient(clientNumber);

				clientList.add(client);
			}

			resultSet.close();
		} catch (SQLException ex) {
			throw new RemoteException(
					"Error finding clients without subscriptions. "
							+ SQL_CLIENTS_WITHOUT_SUBSCRIPTIONS, ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return clientList;
	}

	/**
	 * Corrects the data as a result of a bug that put "?" in some fields
	 * instead of a blank string.
	 * 
	 * @throws RemoteException
	 */
	public void updateOrganisationClients() throws RemoteException {

		final String PROGRESS_UNKNOWN = "?";
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		// Client client = null;
		int row = 0;
		try {
			ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
			connection = dataSource.getConnection();
			statement = connection
					.prepareStatement(SQL_ERROR_ORGANISATION_CLIENTS);
			statement.setString(1, PROGRESS_UNKNOWN);
			statement.setString(2, PROGRESS_UNKNOWN);
			statement.setString(3, PROGRESS_UNKNOWN);
			resultSet = statement.executeQuery();
			Integer clientNumber = null;
			ClientVO client = null;
			while (resultSet.next()) {
				row++;
				// do the update
				clientNumber = new Integer(resultSet.getInt(1));
				LogUtil.log(this.getClass(), "clientNumber=" + clientNumber);

				// get the client object

				client = this.getClient(clientNumber);

				LogUtil.log(this.getClass(),
						"before client=" + client.getMotorNewsSendOption());

				// default "?" back to "".
				if (client.getTitle().equals(PROGRESS_UNKNOWN)) {
					client.setTitle("");
				}
				if (client.getGivenNames().equals(PROGRESS_UNKNOWN)) {
					client.setGivenNames("");
				}
				if (client.getMobilePhone().equals(PROGRESS_UNKNOWN)) {
					client.setMobilePhone("");
				}
				if (client.getWorkPhone().equals(PROGRESS_UNKNOWN)) {
					client.setWorkPhone("");
				}
				if (client.getHomePhone().equals(PROGRESS_UNKNOWN)) {
					client.setHomePhone("");
				}
				if (client.getPostProperty().equals(PROGRESS_UNKNOWN)) {
					client.setPostProperty("");
				}
				if (client.getResiProperty().equals(PROGRESS_UNKNOWN)) {
					client.setResiProperty("");
				}
				if (client.getResiStreetChar().equals(PROGRESS_UNKNOWN)) {
					client.setResiStreetChar("");
				}
				if (client.getPostStreetChar().equals(PROGRESS_UNKNOWN)) {
					client.setPostStreetChar("");
				}
				if (client.getResiDpid().equals(PROGRESS_UNKNOWN)) {
					client.setResiDpid("");
				}
				if (client.getPostDpid().equals(PROGRESS_UNKNOWN)) {
					client.setPostDpid("");
				}

				// default back to must send
				if (client.getMotorNewsSendOption().equals(
						Client.MOTOR_NEWS_MEMBERSHIP_SEND)) {
					client.setMotorNewsSendOption(Client.MOTOR_NEWS_SEND);
				}

				LogUtil.log(this.getClass(),
						"after client=" + client.getMotorNewsSendOption());
				LogUtil.log(this.getClass(), "clientVO=" + client.toString());

				try {
					// update record in the client database
					clientAdapter.updateClient(client, "sys", "hob",
							"Organisation update", "RSS");
					clientAdapter.commit();
				} catch (RollBackException e) {
					LogUtil.warn(this.getClass(), client + " "
							+ ExceptionHelper.getExceptionStackTrace(e));
					clientAdapter.rollback();
				}

			}
			resultSet.close();

		} catch (SQLException ex) {
			throw new RemoteException(
					"Error finding organisation type clients. "
							+ SQL_ERROR_ORGANISATION_CLIENTS, ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		LogUtil.log(this.getClass(), row + " were processed.");
	}

	private final static String SQL_ACCESS_MEMBERSHIP_MARKETING_FLAG = "select [client-no] from PUB.[cl-master] cl where exists (select 'x' from mem_membership mm where mm.client_number = cl.[client-no] and mm.product_code = '"
			+ ProductVO.PRODUCT_ACCESS + "') and cl.market = ? ";

	/**
	 * Update the client to allow marketing if they have an access membership.
	 * 
	 * @throws RemoteException
	 */
	public void updateAccessMarketingFlag() throws RemoteException {
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		ClientVO clientVO = null;
		int row = 0;
		try {
			User user = SecurityHelper.getSystemUser();
			connection = dataSource.getConnection();
			statement = connection
					.prepareStatement(SQL_ACCESS_MEMBERSHIP_MARKETING_FLAG);
			// non-marketing only
			statement.setBoolean(1, false);
			resultSet = statement.executeQuery();
			Integer clientNumber = null;
			while (resultSet.next()) {
				row++;
				clientNumber = new Integer(resultSet.getInt(1));
				LogUtil.log(this.getClass(), "clientNumber = " + clientNumber);

				// get the client object
				clientVO = this.getClient(clientNumber);

				// update market to true
				clientVO.setMarket(new Boolean(true));
				// update client in database and also the progress database
				this.updateClient(clientVO, user.getUserID(),
						Client.HISTORY_UPDATE, user.getSalesBranchCode(),
						"Update Access members to allow marketing.",
						SourceSystem.CLIENT.getAbbreviation());
			}
			resultSet.close();

		} catch (SQLException ex) {
			throw new RemoteException("Error finding access clients to update."
					+ SQL_ACCESS_MEMBERSHIP_MARKETING_FLAG, ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		LogUtil.log(this.getClass(), "Access marketing flags updated = " + row);
	}

	/**
	 * Update deceased clients with the deceased flag that have variations of
	 * the estate, late, etc. in the surname.
	 * 
	 * @throws RemoteException
	 */
	public void updateDeceasedClientStatus() throws RemoteException {
		// select where name like estate of the late
		// and the status is not set to deceased.
		PreparedStatement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		ClientVO clientVO = null;
		int row = 0;
		try {
			User user = SecurityHelper.getSystemUser();
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_ERROR_DECEASED_CLIENTS);
			resultSet = statement.executeQuery();
			Integer clientNumber = null;
			while (resultSet.next()) {
				row++;
				clientNumber = new Integer(resultSet.getInt(1));
				LogUtil.log(this.getClass(), "clientNumber = " + clientNumber);

				// get the client object
				clientVO = this.getClient(clientNumber);
				// update deceased flag on object
				clientVO.setStatus(Client.STATUS_DECEASED);
				// update market to false
				clientVO.setMarket(new Boolean(false));
				// update client in database and also the progress database
				this.updateClient(clientVO, user.getUserID(),
						Client.TRANSACTION_REASON_ERROR,
						user.getSalesBranchCode(),
						"Error associated with setting of deceased flag.",
						SourceSystem.CLIENT.getAbbreviation());
			}
			resultSet.close();

		} catch (SQLException ex) {
			throw new RemoteException(
					"Error finding erroneous deceased clients. "
							+ SQL_ERROR_DECEASED_CLIENTS, ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		LogUtil.log(this.getClass(), "Deceased clients updated = " + row);
	}

	/**
	 * Create a client in our database based on data from the Progress client
	 * database. The data is supplied in xml format.
	 * 
	 * @param clientNode
	 *            An xml node containing the client data.
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void processNotificationEvent(NotificationEvent event)
			throws RemoteException {
		LogUtil.debug(this.getClass(),
				"processNotificationEvent(" + event.getEventName() + ")");

		LogUtil.debug(this.getClass(), "processNotificationEvent test 1");

		// Find out what sort of event it is
		if (event.eventEquals(NotificationEvent.EVENT_PROGRESS_CLIENT_CHANGE)) {
			LogUtil.debug(this.getClass(), "processNotificationEvent test 2");

			ArrayList notifiableEventList = new ArrayList();

			ProgressClientChangeNotificationEvent progressClientEvent = (ProgressClientChangeNotificationEvent) event;
			// Call the remote interface on the client manager to cause
			// the progress client change to be processed in a new transaction.
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			ClientChangeNotificationEvent clientChangeEvent = null;

			if (progressClientEvent.isCreate()) {
				LogUtil.debug(this.getClass(),
						"ProgressClientChangeNotificationEvent:Create");
				ClientVO clientVO = clientMgr.createClientFromProgressData(
						progressClientEvent.getClientNumber(),
						progressClientEvent.getClientNode());
				clientChangeEvent = new ClientChangeNotificationEvent(
						ClientChangeNotificationEvent.ACTION_CREATE, clientVO);
				notifiableEventList.add(clientChangeEvent);
			} else if (progressClientEvent.isUpdate()) {
				LogUtil.debug(this.getClass(),
						"ProgressClientChangeNotificationEvent:Update");
				ClientVO clientVO = clientMgr.updateCustomerFromProgressClient(
						progressClientEvent.getClientNumber(),
						progressClientEvent.getClientNode());
				if (clientVO != null) {
					clientChangeEvent = new ClientChangeNotificationEvent(
							ClientChangeNotificationEvent.ACTION_UPDATE,
							clientVO);
				} else { // if it doesn't already exist then create it.
					clientVO = clientMgr.createClientFromProgressData(
							progressClientEvent.getClientNumber(),
							progressClientEvent.getClientNode());
					clientChangeEvent = new ClientChangeNotificationEvent(
							ClientChangeNotificationEvent.ACTION_CREATE,
							clientVO);
				}
				notifiableEventList.add(clientChangeEvent);
			} else if (progressClientEvent.isDelete()) {
				LogUtil.debug(this.getClass(),
						"ProgressClientChangeNotificationEvent:Delete");
				clientMgr.removeClient_ProgressInitiated(
						progressClientEvent.getClientNumber(), new User(),
						false);
				clientChangeEvent = new ClientChangeNotificationEvent(
						ClientChangeNotificationEvent.ACTION_REMOVE,
						progressClientEvent.getClientNumber());
				notifiableEventList.add(clientChangeEvent);
			} else if (progressClientEvent.isMerge()) {
				LogUtil.debug(this.getClass(),
						"ProgressClientChangeNotificationEvent:Merge");
				Hashtable clientTable = clientMgr.mergeClient(
						progressClientEvent.getClientNumber(),
						progressClientEvent.getClientNode());

				LogUtil.log(this.getClass(), "isMerge 1");
				// get the current client state
				ClientVO masterClient = (ClientVO) clientTable
						.get(MASTER_CLIENT);
				ClientVO duplicateClient = (ClientVO) clientTable
						.get(DUPLICATE_CLIENT);

				LogUtil.debug(this.getClass(), "isMerge 2" + masterClient);
				LogUtil.debug(this.getClass(), "isMerge 3" + duplicateClient);

				// notify subscribers (eg. Membership and Prosper)

				// setup two events
				clientChangeEvent = new ClientChangeNotificationEvent(
						ClientChangeNotificationEvent.ACTION_UPDATE,
						masterClient);
				clientChangeEvent
						.setEventMode(NotificationEvent.MODE_SYNCHRONOUS);
				LogUtil.debug(this.getClass(), "isMerge 4a" + clientChangeEvent);

				ClientChangeNotificationEvent clientRemoveEvent = new ClientChangeNotificationEvent(
						ClientChangeNotificationEvent.ACTION_REMOVE,
						duplicateClient);
				clientRemoveEvent
						.setEventMode(NotificationEvent.MODE_SYNCHRONOUS);
				clientRemoveEvent.setProgressInitiated(true);
				LogUtil.debug(this.getClass(), "isMerge 4b" + clientRemoveEvent);

				notifiableEventList.add(clientChangeEvent);
				notifiableEventList.add(clientRemoveEvent);

			}

			// Raise a client change notification event
			clientChangeEvent.setProgressInitiated(true);

			NotificationMgr notificationMgr = CommonEJBHelper
					.getNotificationMgr();
			try {
				// notify notifiable events
				for (int i = 0; i < notifiableEventList.size(); i++) {
					LogUtil.debug(this.getClass(), "isMerge 5, event number "
							+ i);
					notificationMgr
							.notifyEvent((NotificationEvent) notifiableEventList
									.get(i));
				}

				notificationMgr.commit();
			} catch (RollBackException rbe) {
				notificationMgr.rollback();
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ClientVO createClientFromProgressData(Integer clientNumber,
			Node clientNode) throws RemoteException {
		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
		ClientVO newClientVO = null;

		newClientVO = new ClientVO();
		newClientVO.setFromProgressClientData(clientNode,
				ClientChangeNotificationEvent.ACTION_CREATE);
		clientMgr.createClient(newClientVO);

		User user = userMgr.getUser(newClientVO.getMadeID());
		try {
			// create history record
			String userId = (user == null) ? newClientVO.getMadeID() : user
					.getUserID();
			String salesBranchCode = (user == null) ? "" : user
					.getSalesBranchCode();
			createClientHistory(newClientVO, userId, Client.HISTORY_CREATE,
					salesBranchCode, "", SourceSystem.CLIENT.getAbbreviation());
		} catch (GenericException e) {
			throw new RemoteException(e.getMessage());
		}

		// create default subscriptions
		createDefaultClientSubscriptions(newClientVO);

		return newClientVO;
	}

	// SQL delete statements

	private final String SQL_DELETE_MEMBERSHIP = "delete from pub.mem_membership where membership_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_TRANSACTION = "delete from pub.mem_transaction where transaction_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_TRANSACTION_FEE = "delete from pub.mem_transaction_fee where transaction_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_TRANSACTION_FEE_DISCOUNT = "delete from pub.mem_transaction_fee_discount where transaction_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_TRANSACTION_ADJUSTMENTS = "delete from pub.mem_transaction_adjustment where transaction_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_CARDS = "delete from pub.mem_membership_card where membership_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_DISCOUNT = "delete from pub.mem_membership_discount where membership_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_DOCUMENT = "delete from pub.mem_document where membership_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_GROUP = "delete from pub.mem_membership_group where membership_id = ?";

	private final String SQL_DELETE_MEMBERSHIP_QUOTE = "delete from pub.mem_membership_quote where membership_id = ?";

	private final String SQL_DELETE_PAYABLE_ITEM = "delete from pub.pt_payableitem where payableitemid = ?";

	private final String SQL_DELETE_PAYABLE_ITEM_COMPONENT = "delete from pub.pt_payableitemcomponent where payableitemcomponentid = ?";

	private final String SQL_DELETE_PAYABLE_ITEM_POSTING = "delete from pub.pt_payableitemposting where payableitemcomponentid = ?";

	private final String SQL_DELETE_PENDING_FEE = "delete from pub.pt_pending_fee where client_number = ?";

	/**
	 * <p>
	 * Delete client records and all associated records based on passed file.
	 * </p>
	 * 
	 * <p>
	 * <b>Note: the transaction timeout of the container will need to be
	 * increased. When run in production on 6/3/05 the transaction timeout value
	 * was 14400.</b>
	 * </p>
	 * 
	 * @param clientDeleteFile
	 *            The file to use for deletions
	 * @throws RemoteException
	 */
	public void deleteObsoleteClientsAndBusiness(String clientDeleteFile)
			throws RemoteException {

		LogUtil.log(this.getClass(),
				"Started deleting at " + new DateTime().formatLongDate()
						+ " using file " + clientDeleteFile + "...");
		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();

		// for each client in the file

		File file = new File(clientDeleteFile);
		if (!file.exists()) {
			throw new SystemException("File '" + clientDeleteFile
					+ "' does not exist.");
		}
		BufferedReader deleteClientList = null;
		try {
			deleteClientList = new BufferedReader(new FileReader(file));

			String clientNumberString = "";
			Integer clientNumber = null;
			DeleteClientSummary deleteClientSummary = null;
			int clientCount = 0;
			int errorCount = 0;

			while ((clientNumberString = deleteClientList.readLine()) != null) {
				clientCount++;

				// trim the string
				clientNumberString = clientNumberString.trim();

				try {
					clientNumber = new Integer(clientNumberString);
				} catch (NumberFormatException ex1) {
					LogUtil.fatal(this.getClass(), "Client number '"
							+ clientNumberString + "' not valid.");
					// go to next record.
					continue;
				}

				// delete client and all associated records.
				try {
					deleteClientSummary = clientMgr.deleteClientAndBusiness(
							clientNumber, true);
					/** @todo notify subscribers **/
				} catch (GenericException ex2) {
					errorCount++;
					LogUtil.warn(this.getClass(), "Error deleting client "
							+ clientNumber + ": " + ex2.getMessage());
				}

				// output to log every 1000 records
				if (clientCount % 1000 == 0) {
					LogUtil.log(this.getClass(), "lineCount=" + clientCount);
					if (deleteClientSummary != null) {
						LogUtil.log(this.getClass(), "delete client summary="
								+ deleteClientSummary.toString());
					}
				}

			}
			// close file
			deleteClientList.close();

			LogUtil.log(this.getClass(), "----------------");
			LogUtil.log(this.getClass(), "total clients processed="
					+ clientCount);
			LogUtil.log(this.getClass(), "total errors=" + errorCount);
			if (deleteClientSummary != null) {
				LogUtil.log(this.getClass(), "last client processed="
						+ deleteClientSummary.toString());
			}

		} catch (FileNotFoundException ex) {
			throw new SystemException("Unable to find file to process.", ex);
		} catch (IOException ex) {
			throw new SystemException("Problem reading file.", ex);
		}
		LogUtil.log(this.getClass(),
				"Finished deleting at " + new DateTime().formatLongDate());
	}

	private final static String SQL_OBSOLETE_CLIENTS = "select [client-no] from pub.[cl-master] "
			+ "where (([client-no] not in (select client_number from mem_membership)) "
			+ "or ([client-no] in (select client_number from pub.mem_membership where expiry_date < ? ))) "
			+ "and ([client-no] not in (select client_number from tr_client_transaction)) "
			+ "and ([client-no] not in (select client_number from fin_debtor)) "
			+ "and (motor_news_send_option <> '"
			+ Client.MOTOR_NEWS_SEND
			+ "' and motor_news_send_option <> '"
			+ Client.MOTOR_NEWS_SEND_INDIVIDUAL
			+ "') "
			+ "and [client-no] <> 0 "
			+ "and (cast([client-no] as varchar(20)) not in ( "
			+ // TODO
			// not
			// ANSI
			// standard
			// SQL
			"SELECT     ORG_CODE "
			+ "FROM         daltonc.JALIFE_PURCHASES pu, daltonc.JALIFE_ORGANISATIONS org "
			+ "where pu.CUSTOMER_ID = org.ORG_ID "
			+ "and pu.create_date > ? "
			+ ")) " + "and [made-date] < ? " + "order by [client-no] asc";

	public static String POPUP_ADDRESS_UPDATE = "cltAddressUpdate";

	public static String POPUP_CLIENT_MERGE = "cltMerge";

	/**
	 * Get a list of clients that either have no business or no business since
	 * the last transaction.
	 * 
	 * @param lastTransaction
	 *            DateTime
	 * @return String
	 */
	public String getObsoleteClientList(DateTime lastTransaction)
			throws RemoteException {

		StringBuffer obsoleteClients = new StringBuffer();
		Connection connection = null;
		PreparedStatement statement = null;
		Integer clientNumber = null;
		DateTime clientStart = new DateTime();
		Interval fourteenDays = null;
		try {
			fourteenDays = new Interval(0, 0, 14, 0, 0, 0, 0);
			clientStart = clientStart.subtract(fourteenDays);
		} catch (Exception ex1) {
			throw new RemoteException(
					"Unable to create client range for deletion.");
		}
		int counter = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_OBSOLETE_CLIENTS);
			LogUtil.log(this.getClass(), "SQL_OBSOLETE_CLIENTS="
					+ SQL_OBSOLETE_CLIENTS);
			statement.setDate(1, lastTransaction.toSQLDate());
			statement.setDate(2, lastTransaction.toSQLDate());
			statement.setDate(3, clientStart.toSQLDate());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				counter++;
				clientNumber = new Integer(resultSet.getInt(1));

				if (counter % 1000 == 0) {
					LogUtil.log(this.getClass(), "Processed " + counter
							+ " records.");
				}

				if (counter > 1) {
					// add a new line if not the first line
					obsoleteClients.append(FileUtil.NEW_LINE);
				}

				// add to list
				obsoleteClients.append(clientNumber.toString());

			}

		} catch (SQLException se) {
			throw new RemoteException(
					"Unable to get list of obsolete clients.", se);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return obsoleteClients.toString();
	}

	private final String MASTER_CLIENT = "Master Client";
	private final String DUPLICATE_CLIENT = "Duplicate Client";

	/**
	 * Merge the duplicate client to the master client.
	 * 
	 * @param clientNumber
	 *            Integer
	 * @param clientNode
	 *            Node
	 * @throws RemoteException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Hashtable mergeClient(Integer clientNumber, Node clientNode)
			throws RemoteException {

		PaymentTransactionMgrLocal paymentTransactionMgr = PaymentEJBHelper
				.getPaymentTransactionMgrLocal();

		LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber);
		Hashtable clientTable = new Hashtable();

		MergeClientVO masterClientVO = null;

		// The client number provided is the master client number.
		Integer masterClientNumber = clientNumber;
		LogUtil.debug(this.getClass(), "masterClientNumber="
				+ masterClientNumber);
		// Get the master client details
		if (masterClientNumber == null || masterClientNumber.intValue() == 0) {
			throw new SystemException("Master client number '"
					+ masterClientNumber + "' is not valid.");
		}
		LogUtil.debug(this.getClass(), "a");
		// create a new merge client vo
		masterClientVO = new MergeClientVO(clientNode);
		validateMasterClient(masterClientVO);

		User user = UserEJBHelper.getUserMgr().getUser(
				masterClientVO.getUserId());

		LogUtil.debug(this.getClass(), "a1");

		MembershipVO masterClientMembership = getActiveMembership(masterClientNumber);

		Integer duplicateClientNumber = masterClientVO
				.getDuplicateClientNumber();

		LogUtil.info(this.getClass(), "duplicateClientNumber="
				+ duplicateClientNumber);

		ClientVO duplicateClientVO = null;
		try {
			// check if duplicate client exists
			duplicateClientVO = getClient(duplicateClientNumber);
		} catch (Exception onfe) {
			throw new SystemException("Failed to find duplicate client '"
					+ duplicateClientNumber + "' from Progress client data.",
					onfe);
		}

		if (masterClientNumber.equals(duplicateClientNumber)) {
			throw new SystemException("Master client number '"
					+ masterClientNumber + "' and duplicate client number '"
					+ duplicateClientNumber + "' may not be the same.");
		}

		MembershipVO duplicateClientMembership = getActiveMembership(duplicateClientNumber);

		LogUtil.info(this.getClass(), "1");

		// get client node

		LogUtil.log(this.getClass(), "1a" + duplicateClientVO);
		if (!duplicateClientVO.getStatus().equalsIgnoreCase(
				Client.STATUS_DUPLICATE)) {
			throw new SystemException("Only clients with a status of '"
					+ Client.STATUS_DUPLICATE + "' may be merged. Client '"
					+ duplicateClientNumber + "' has a status of '"
					+ duplicateClientVO.getStatus() + "'.");
		}
		LogUtil.info(this.getClass(), "2");

		// establish a datasource connection.
		Connection connection = null;

		try {
			connection = dataSource.getConnection();
			LogUtil.info(this.getClass(), "6 " + duplicateClientNumber + " "
					+ masterClientNumber);

			// update the master client with new new client details. eg. best
			// given names data.
			updateCustomerFromProgressClient(masterClientNumber, clientNode);

			LogUtil.debug(this.getClass(), "7");
			if (true)
				throw new Exception("\n**\n* Forcing exit...\n**");
			LogUtil.debug(this.getClass(), "7a");

			// update the master client with the new business.
			updateClientBusiness(connection, duplicateClientNumber,
					masterClientNumber, duplicateClientMembership,
					masterClientMembership, paymentTransactionMgr, user);

			// TODO pass through the user

			// duplicate client's business is transferred to master client but
			// not saved to DB yet.

			LogUtil.info(this.getClass(), "8 " + masterClientVO);

			// TODO verify transaction boundaries.

			// Note: as transaction is read committed it "should" read records
			// within the current transaction.

			// delete the duplicate client and all untransferred business.
			try {
				deleteClientAndBusiness(duplicateClientNumber, user, false);
			} catch (GenericException ex1) {
				throw new SystemException(ex1);
			}

			// check that all records have been deleted

			LogUtil.debug(this.getClass(), "9");

			LogUtil.debug(this.getClass(), "9a");

			paymentTransactionMgr.commit();

			// add client VOs to hastable.
			clientTable.put(MASTER_CLIENT, masterClientVO);
			clientTable.put(DUPLICATE_CLIENT, duplicateClientVO);

			LogUtil.info(this.getClass(), "10");
		} catch (Exception ex) {
			try {
				paymentTransactionMgr.rollback();
			} catch (Exception e) {
				LogUtil.warn(this.getClass(), e);
			}
			throw new SystemException("Unable to merge the clients. ", ex);
		} finally {
			ConnectionUtil.closeConnection(connection);
		}

		return clientTable;
	}

	/**
	 * Validate a master client no. to see if it is suitable for transferring.
	 * 
	 * @param clientNode
	 *            Node
	 * @param clientHome
	 *            ClientHome
	 * @param masterClientVO
	 *            ClientVO
	 * @param masterClientNumber
	 *            Integer
	 * @throws RemoteException
	 * @return ClientVO
	 */
	public void validateMasterClient(MergeClientVO masterClientVO)
			throws RemoteException {
		Integer masterClientNumber = masterClientVO.getMasterClientNumber();

		// check if master client exists
		ClientVO masterClient = getClient(masterClientNumber);

		// master client may not be a duplicate.
		if (masterClientVO.getStatus().equals(Client.STATUS_DUPLICATE)) {
			throw new SystemException("Master client '" + masterClientNumber
					+ "' may not be a duplicate.");
		}
		// may not have another master defined
		if (masterClientVO.getMasterClientNumber() != null
				&& masterClientVO.getMasterClientNumber().intValue() != 0) {
			throw new SystemException("Master client '" + masterClientNumber
					+ "' has a master client of '"
					+ masterClientVO.getMasterClientNumber()
					+ "'. It may not be used as the master client for a merge.");
		}
		LogUtil.debug(this.getClass(), "4");

	}

	public void validateDuplicateClient(ClientVO client)
			throws ValidationException {
		MembershipVO membership;
		try {
			membership = getActiveMembership(client.getClientNumber());
		} catch (RemoteException e) {
			throw new ValidationException(e);
		}
		if (membership != null) {
			throw new ValidationException(
					"A client may not be marked as duplicate if they have an active membership.");
		}
	}

	/**
	 * Check the duplicate client for an active membership.
	 * 
	 * @param clientNumber
	 *            Integer
	 * @throws RemoteException
	 */
	private MembershipVO getActiveMembership(Integer clientNumber)
			throws RemoteException {
		MembershipMgrLocal membershipMgr = MembershipEJBHelper
				.getMembershipMgrLocal();
		Collection membershipList = null;
		MembershipVO membershipVO = null;
		// check if membership exists
		membershipList = membershipMgr
				.findMembershipByClientNumber(clientNumber);
		if (membershipList != null && membershipList.size() > 0) {
			// check membership status.
			Iterator memIt = membershipList.iterator();
			// cater for multiple memberships
			int counter = 0;
			while (memIt.hasNext()) {
				counter++;
				if (counter > 1) {
					throw new SystemException(
							"Unable to handle more that one membership.");
				}
				membershipVO = (MembershipVO) memIt.next();

				// check if membership is active
				if (!membershipVO.isActive() || // not active by expiry date
						!MembershipVO.STATUS_ACTIVE.equals(membershipVO
								.getBaseStatus())) // not
				// active
				// by
				// status
				{
					membershipVO = null;
				}
			}
		}
		return membershipVO;
	}

	/**
	 * Delete all client history associated with a client.
	 * 
	 * @param connection
	 *            Connection
	 * @param clientNumber
	 *            Integer
	 */
	private void deleteClientHistory(Integer clientNumber)
			throws GenericException {
		// TODO
		org.hibernate.Query query = hsession
				.createQuery("delete from ClientHistory where clientHistoryPK.clientNumber = :clientNumber");
		query.setInteger(1, clientNumber);
		int historyDeleted = query.executeUpdate();
		LogUtil.log(this.getClass(), "historyDeleted=" + historyDeleted);
	}

	/**
	 * Delete client and all associated records in the Roadside system. eg. a
	 * duplicate client or an obsolete client.
	 * 
	 * @param connection
	 *            Connection
	 * @param clientNumber
	 *            Integer
	 * @throws SystemException
	 * @throws SQLException
	 * @return DeleteClientSummary
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public DeleteClientSummary deleteClientAndBusiness(Integer clientNumber,
			User user, boolean archive) throws GenericException {
		PaymentTransactionMgr paymentTransactionMgr = PaymentEJBHelper
				.getPaymentTransactionMgr();

		Connection connection = null;
		LogUtil.debug(this.getClass(), "deleteClientAndBusiness start "
				+ clientNumber);
		// DateTime now = new DateTime().getDateOnly();
		DeleteClientSummary deleteClientSummary = new DeleteClientSummary();
		ClientVO client = null;
		int clientCount = 1;
		int membershipCount = 0;
		int membershipTransactionCount = 0;
		int payableItemCount = 0;

		Integer membershipId = null;
		Integer transactionId = null;
		Integer payableItemId = null;
		Integer payableItemComponentId = null;

		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;

		PreparedStatement statement1 = null;
		PreparedStatement statement2 = null;
		PreparedStatement statement3 = null;
		PreparedStatement statement4 = null;
		PreparedStatement statement5 = null;
		PreparedStatement statement6 = null;
		PreparedStatement statement7 = null;
		PreparedStatement statement8 = null;
		PreparedStatement statement9 = null;
		PreparedStatement statement10 = null;
		PreparedStatement statement11 = null;
		PreparedStatement statement12 = null;
		PreparedStatement statement13 = null;
		PreparedStatement statement14 = null;
		PreparedStatement statement15 = null;
		PreparedStatement statement16 = null;
		PreparedStatement statement17 = null;
		PreparedStatement statement18 = null;
		PreparedStatement statement19 = null;
		PreparedStatement statement20 = null;

		Collection membershipList = new ArrayList();
		MembershipVO membership = null;

		try {
			MembershipMgr membershipMgr = MembershipEJBHelper
					.getMembershipMgr();

			connection = dataSource.getConnection();

			// get the client record
			client = getClient(clientNumber);
			if (client == null) {
				throw new GenericException("Client does not exist.");
			}

			statement1 = connection
					.prepareStatement("select membership_id from pub.mem_membership where client_number = ?");
			statement1.setInt(1, clientNumber.intValue());
			rs1 = statement1.executeQuery();
			while (rs1.next()) {
				membershipId = new Integer(rs1.getInt(1));
				membership = membershipMgr.getMembership(membershipId);
				membershipList.add(membership);

				statement2 = connection
						.prepareStatement("select transaction_id from pub.mem_transaction where membership_id = ?");
				statement2.setInt(1, membershipId.intValue());
				rs2 = statement2.executeQuery();

				// iterate through transactions
				while (rs2.next()) {
					transactionId = new Integer(rs2.getInt(1));

					// delete transaction fees
					statement4 = connection
							.prepareStatement(SQL_DELETE_MEMBERSHIP_TRANSACTION_FEE);
					statement4.setInt(1, transactionId.intValue());
					statement4.executeUpdate();

					// delete transaction discounts
					statement5 = connection
							.prepareStatement(SQL_DELETE_MEMBERSHIP_TRANSACTION_FEE_DISCOUNT);
					statement5.setInt(1, transactionId.intValue());
					statement5.executeUpdate();

					// delete adjustments
					statement6 = connection
							.prepareStatement(SQL_DELETE_MEMBERSHIP_TRANSACTION_ADJUSTMENTS);
					statement6.setInt(1, transactionId.intValue());
					statement6.executeUpdate();

					// delete transactions
					statement3 = connection
							.prepareStatement(SQL_DELETE_MEMBERSHIP_TRANSACTION);
					statement3.setInt(1, transactionId.intValue());
					membershipTransactionCount = statement3.executeUpdate();

					deleteClientSummary.addCount(
							DeleteClientSummary.TYPE_TRANSACTION,
							membershipTransactionCount);
				}
				rs2.close();

				// delete membership cards
				statement8 = connection
						.prepareStatement(SQL_DELETE_MEMBERSHIP_CARDS);
				statement8.setInt(1, membershipId.intValue());
				statement8.executeUpdate();

				// delete documents
				statement9 = connection
						.prepareStatement(SQL_DELETE_MEMBERSHIP_DOCUMENT);
				statement9.setInt(1, membershipId.intValue());
				statement9.executeUpdate();

				// delete membership quotes
				statement10 = connection
						.prepareStatement(SQL_DELETE_MEMBERSHIP_QUOTE);
				statement10.setInt(1, membershipId.intValue());
				statement10.executeUpdate();

				// delete membership discount
				statement11 = connection
						.prepareStatement(SQL_DELETE_MEMBERSHIP_DISCOUNT);
				statement11.setInt(1, membershipId.intValue());
				statement11.executeUpdate();

				// delete membership last
				statement7 = connection.prepareStatement(SQL_DELETE_MEMBERSHIP);
				statement7.setInt(1, membershipId.intValue());
				membershipCount = statement7.executeUpdate();

				deleteClientSummary.addCount(
						DeleteClientSummary.TYPE_MEMBERSHIP, membershipCount);

				// delete membership group record
				statement12 = connection
						.prepareStatement(SQL_DELETE_MEMBERSHIP_GROUP);
				statement12.setInt(1, membershipId.intValue());
				statement12.executeUpdate();

			}
			rs1.close();

			// get all payables

			statement13 = connection
					.prepareStatement("select payableitemid from pub.pt_payableitem where clientno = ?");
			statement13.setInt(1, clientNumber.intValue());
			rs3 = statement13.executeQuery();
			while (rs3.next()) {
				payableItemId = new Integer(rs3.getInt(1));

				statement14 = connection
						.prepareStatement("select payableitemcomponentid from pub.pt_payableitemcomponent where payableitemid = ?");
				statement14.setInt(1, payableItemId.intValue());
				rs4 = statement14.executeQuery();
				while (rs4.next()) {
					payableItemComponentId = new Integer(rs4.getInt(1));

					// delete components
					statement15 = connection
							.prepareStatement(SQL_DELETE_PAYABLE_ITEM_COMPONENT);
					statement15.setInt(1, payableItemComponentId.intValue());
					statement15.executeUpdate();

					// delete postings
					statement16 = connection
							.prepareStatement(SQL_DELETE_PAYABLE_ITEM_POSTING);
					statement16.setInt(1, payableItemComponentId.intValue());
					statement16.executeUpdate();
				}
				rs4.close();

				// delete payable item
				statement17 = connection
						.prepareStatement(SQL_DELETE_PAYABLE_ITEM);
				statement17.setInt(1, payableItemComponentId.intValue());
				payableItemCount = statement17.executeUpdate();

				deleteClientSummary
						.addCount(DeleteClientSummary.TYPE_PAYABLE_ITEM,
								payableItemCount);

			}
			rs3.close();

			// delete pending fees
			statement20 = connection.prepareStatement(SQL_DELETE_PENDING_FEE);
			statement20.setInt(1, clientNumber.intValue());
			statement20.executeUpdate();

			// delete client and history
			removeClient_ProgressInitiated(clientNumber, user, archive);

			// Tramada
			TramadaClientTransaction tramadaRemoveTransaction = getLastTramadaClientTransaction(clientNumber);
			if (tramadaRemoveTransaction != null) {
				FollowUp followUp = new FollowUp();
				FollowUpPK followUpPK = new FollowUpPK();
				followUpPK.setSystemNumber(clientNumber);
				followUpPK.setChangedClientNo(clientNumber);
				followUpPK.setCreateDate(new DateTime());
				followUpPK.setSubSystem(SourceSystem.TRAVEL.getAbbreviation());
				followUp.setFollowUpPk(followUpPK);
				followUp.setStatus(FollowUp.STATUS_FLAG);
				String notes = "";
				if (tramadaRemoveTransaction != null) {
					notes = tramadaRemoveTransaction
							.getTramadaClientTransactionPK()
							.getTramadaProfileCode()
							+ " has been removed from the RACT database.";
				}
				followUp.setNotes(notes);
				createFollowUp(followUp);
			}

			// Receptor
			paymentTransactionMgr.deleteReceiptingCustomer(
					clientNumber.toString(), "");
			paymentTransactionMgr.commit();

			// Finance One
			// do not update as per MFG-PRO

		} catch (Exception ex) {
			try {
				paymentTransactionMgr.rollback();
			} catch (RemoteException e) {
				LogUtil.warn(this.getClass(), e);
			}
			throw new GenericException(ex);
		} finally {
			ConnectionUtil.closeStatement(statement1);
			ConnectionUtil.closeStatement(statement2);
			ConnectionUtil.closeStatement(statement3);
			ConnectionUtil.closeStatement(statement4);
			ConnectionUtil.closeStatement(statement5);
			ConnectionUtil.closeStatement(statement6);
			ConnectionUtil.closeStatement(statement7);
			ConnectionUtil.closeStatement(statement8);
			ConnectionUtil.closeStatement(statement9);
			ConnectionUtil.closeStatement(statement10);
			ConnectionUtil.closeStatement(statement11);
			ConnectionUtil.closeStatement(statement12);
			ConnectionUtil.closeStatement(statement13);
			ConnectionUtil.closeStatement(statement14);
			ConnectionUtil.closeStatement(statement15);
			ConnectionUtil.closeStatement(statement16);
			ConnectionUtil.closeStatement(statement17);
			ConnectionUtil.closeStatement(statement18);
			ConnectionUtil.closeStatement(statement19);
			ConnectionUtil.closeStatement(statement20);
			ConnectionUtil.closeConnection(connection);
		}

		deleteClientSummary.addCount(DeleteClientSummary.TYPE_CLIENT,
				clientCount);

		deleteClientSummary.setClientNumber(clientNumber);

		LogUtil.debug(this.getClass(), "deleteClientAndBusiness end "
				+ clientNumber);

		return deleteClientSummary;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ClientVO updateCustomerFromProgressClient(Integer clientNumber,
			Node clientNode) throws RemoteException {
		LogUtil.debug(this.getClass(), "updateCustomerFromProgressClient start");
		ClientVO client = null;
		try {
			client = getClient(clientNumber);
		} catch (Exception onfe) {
			throw new SystemException("Failed to find client " + clientNumber
					+ " for update from Progress client data.", onfe);
		}
		/* if it doesn't exist, create it */
		if (client == null) {
			client = this
					.createClientFromProgressData(clientNumber, clientNode);
		} else {
			LogUtil.debug(
					this.getClass(),
					"clientNode = \n"
							+ com.ract.common.integration.PopupRequest
									.getXMLString(clientNode));
			client.setFromProgressClientData(clientNode,
					ClientChangeNotificationEvent.ACTION_UPDATE);
			updateClient(client);
		}

		// LogUtil.debug(this.getClass(),"updateCustomerFromProgressClient 4 "+client.getVO().toString());
		// Find the "Audit" tag in the progress client data so that the history
		// can be created
		String userID = null;
		String salesBranchCode = null;
		String transactionReason = "";
		String comments = "";
		if (clientNode.getNodeType() == Node.ELEMENT_NODE) {
			Element clientElement = (Element) clientNode;
			NodeList nodeList = clientElement.getElementsByTagName("Audit");
			if (nodeList != null && nodeList.getLength() > 0) {
				// Should only be one
				Node auditNode = nodeList.item(0);
				if (auditNode.hasChildNodes()) {
					NodeList auditTags = auditNode.getChildNodes();
					Node node;
					String nodeName;
					String nodeValue;
					for (int i = 0; i < auditTags.getLength(); i++) {
						node = auditTags.item(i);
						nodeName = node.getNodeName();
						if (node.hasChildNodes()) {
							nodeValue = node.getFirstChild().getNodeValue();
						} else {
							nodeValue = null;
						}
						if (nodeValue != null) {
							nodeValue = nodeValue.trim();
							if ("UserID".equalsIgnoreCase(nodeName)) {
								if (nodeValue.length() > 0) {
									userID = nodeValue;
								}
							} else if ("SalesBranch".equalsIgnoreCase(nodeName)) {
								if (nodeValue.length() > 0) {
									salesBranchCode = nodeValue;
								}
							} else if ("Reason".equalsIgnoreCase(nodeName)) {
								if (nodeValue.length() > 0) {
									transactionReason = nodeValue.toUpperCase();
								}
							} else if ("Comment".equalsIgnoreCase(nodeName)) {
								if (nodeValue.length() > 0) {
									comments = nodeValue;
								}
							}
						}
					}
				}
			}
		}
		if (userID == null || salesBranchCode == null) {
			throw new SystemException(
					"Failed to save progress client data. The audit userID or sales branch was not provided.");
		}

		String subSystem = SourceSystem.PROGRESS_CLIENT.getAbbreviation();

		// Create the history using the old VO.
		try {
			createClientHistory(client, userID, transactionReason,
					salesBranchCode, comments, subSystem);
		} catch (GenericException ex) {
			LogUtil.warn(
					this.getClass(),
					"Unable to create client history for "
							+ client.getClientNumber());
		}

		// update tramada client
		transferTramadaClientXML(Client.TRAMADA_MODE_UPDATE,
				TramadaClientProfileImporter.TRAMADA_ACTION_UPDATE, userID,
				salesBranchCode, client);

		// update debtor
		transferDebtor(client);

		LogUtil.debug(this.getClass(), "updateCustomerFromProgressClient end");

		// Return the new VO.
		return client;
	}

	private void transferDebtor(ClientVO client) throws RemoteException {
		// only transfer if the client is a debtor
		if (client.isDebtor()) {
			Debtor debtor = new Debtor(client.getClientNumber(), new DateTime());
			try {
				FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
				financeMgr.updateDebtor(debtor);
			} catch (FinanceException ex) {
				throw new RemoteException("Unable to update debtor.", ex);
			}
		} else {
			LogUtil.debug(this.getClass(),
					"Client '" + client.getClientNumber()
							+ "' is not a debtor.");
		}
	}

	/**
	 * Delete a client.
	 * 
	 * @param clientNumber
	 *            Integer
	 * @throws RemoteException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void removeClient_ProgressInitiated(Integer clientNumber, User user,
			boolean archive) throws RemoteException {
		ClientVO client = getClient(clientNumber);
		deleteClient(client, user, archive);

		// must be notified manually
	}

	public ClientVO getClientByMembershipCardNumber(String cardNumber)
			throws RemoteException {
		LogUtil.debug(this.getClass(), "cardNumber=" + cardNumber);
		ClientVO client = null;
		Integer clientNumber = null;

		// Get the clientNumber from the card number - pos 9-15 inclusive is the
		// number 7 digits 0 left padded MEM-440
		clientNumber = Integer.parseInt(cardNumber.substring(9, 15));

		// lookup client from MRM WS
		LogUtil.log(getClass(), "Looking up client via MRM WS: " + clientNumber);
		client = this.populateClientFromMRM(clientNumber);

		return client;
	}

	/**
	 * Lookup Person from MRM.
	 * 
	 * @param clientNumber
	 * @return
	 * @throws RemoteException
	 */
	private ClientVO populateClientFromMRM(Integer clientNumber)
			throws RemoteException {

		if (clientNumber == null) {
			LogUtil.fatal(getClass(), "Could not find client by clientNo: "
					+ clientNumber);
			return null;
		}

		GetPerson service = new GetPerson();
		GetPersonSoap portType = service.getGetPersonSoap();
		Person clientData = portType.getPersonInfo(clientNumber);

		if (clientData == null) {
			LogUtil.fatal(getClass(), "Could not find client by clientNo: "
					+ clientNumber);
			return null;
		}

		ClientVO clientVO = new ClientVO();

		try {
			clientVO.setBirthDate(new DateTime(clientData.getBirthDate()));
		} catch (Exception e) {

		}
		clientVO.setEmailAddress(clientData.getEmailAddress());

		if (clientData.getGender() != null) {
			if (clientData.getGender().equalsIgnoreCase("Male")) {
				clientVO.setSex(true);
			} else if (clientData.getGender().equalsIgnoreCase("Female")) {
				clientVO.setSex(false);
			} // otherwise null, e.g. Organisation
		}

		StringBuilder givenNames = new StringBuilder();
		givenNames.append(clientData.getFirstName() != null ? clientData
				.getFirstName().trim() : "");
		if (clientData.getMiddleName() != null
				&& !clientData.getMiddleName().isEmpty()) {
			givenNames.append(clientData.getFirstName() != null
					&& !clientData.getFirstName().isEmpty() ? " " : "");
			givenNames.append(clientData.getMiddleName().trim());
		}
		clientVO.setGivenNames(givenNames.toString());

		clientVO.setHomePhone(clientData.getHomePhone());
		clientVO.setMobilePhone(clientData.getMobilePhone());
		clientVO.setSurname(clientData.getLastName());
		clientVO.setTitle(clientData.getTitle());
		clientVO.setWorkPhone(clientData.getWorkPhone());

		// lookup Residential StreetSuburb
		Address residentialAddress = clientData.getHomeAddress();
		String resiStreet = residentialAddress.getStreetName();
		resiStreet = (resiStreet == null) ? "" : resiStreet; // convert null to
		// empty string

		List<StreetSuburbVO> ssList = CommonEJBHelper.getCommonMgr()
				.getStreetSuburbList(resiStreet, residentialAddress.getCity(),
						residentialAddress.getState());

		Integer resiStsubId = null;
		if (!ssList.isEmpty()) {
			if (ssList.size() > 1) {
				// if multiple, match postcode
				for (StreetSuburbVO ssVO : ssList) {
					if (ssVO.getPostcode().equals(
							residentialAddress.getPostcode())) {
						resiStsubId = ssVO.getStreetSuburbID();
						break;
					}
				}
			}
			if (resiStsubId == null) {
				resiStsubId = ssList.get(0).getStreetSuburbID();
			}
		} else {
			LogUtil.fatal(getClass(),
					"Could not determine residential address for clientNo: "
							+ clientNumber + " from " + resiStreet + ", "
							+ residentialAddress.getCity());
			return null;
		}

		clientVO.setResiDpid(residentialAddress.getDPID());
		clientVO.setResiProperty(residentialAddress.getPropertyName());
		clientVO.setResiStreetChar(residentialAddress.getStreetNumber());
		clientVO.setResiStsubId(resiStsubId);
		clientVO.setResiGnaf(residentialAddress.getGnaf());
		clientVO.setResiLatitude(residentialAddress.getLatitude());
		clientVO.setResiLongitude(residentialAddress.getLongitude());
		clientVO.setResiAusbar(residentialAddress.getAusbar());

		// lookup Postal StreetSuburb
		Address postalAddress = clientData.getPostalAddress();
		String postStreet = postalAddress.getStreetName();
		postStreet = (postStreet == null) ? "" : postStreet; // convert null to
		// empty string

		ssList = CommonEJBHelper.getCommonMgr().getStreetSuburbList(postStreet,
				postalAddress.getCity(), postalAddress.getState());

		Integer postStsubId = null;
		if (!ssList.isEmpty()) {
			if (ssList.size() > 1) {
				// if multiple, match postcode
				for (StreetSuburbVO ssVO : ssList) {
					if (ssVO.getPostcode().equals(postalAddress.getPostcode())) {
						postStsubId = ssVO.getStreetSuburbID();
						break;
					}
				}
			}
			if (postStsubId == null) {
				postStsubId = ssList.get(0).getStreetSuburbID();
			}
		} else {
			LogUtil.fatal(getClass(),
					"Could not determine postal address for clientNo: "
							+ clientNumber + " from " + postStreet + ", "
							+ postalAddress.getCity());
			return null;
		}

		clientVO.setPostDpid(postalAddress.getDPID());
		clientVO.setPostProperty(postalAddress.getPropertyName());
		clientVO.setPostStreetChar(postalAddress.getStreetNumber());
		clientVO.setPostStsubId(postStsubId);
		clientVO.setPostGnaf(postalAddress.getGnaf());
		clientVO.setPostLatitude(postalAddress.getLatitude());
		clientVO.setPostLongitude(postalAddress.getLongitude());
		clientVO.setPostAusbar(postalAddress.getAusbar());
		clientVO.setClientNumber(clientNumber);

		return clientVO;
	}

	private final static String SQL_STSUB_CLIENT_COUNT = "select count([client-no]) from pub.[cl-master] where [post-stsubid] = ?";

	/**
	 * Get the count of clients at the specified street suburb.
	 * 
	 * @param stsubid
	 *            Integer
	 * @throws SQLException
	 * @return int
	 */
	public int getClientCountByPostalStreetSuburb(Integer stsubid)
			throws RemoteException {

		Connection connection = null;
		PreparedStatement statement = null;
		int recordCount = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_STSUB_CLIENT_COUNT);
			statement.setInt(1, stsubid.intValue());
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				recordCount = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			throw new RemoteException("Error: " + SQL_STSUB_CLIENT_COUNT, e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return recordCount;
	}

	public void createClientSegments(Integer startClientNumber,
			Integer endClientNumber, String fileName) throws RemoteException {
		ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
		Integer clientNumber = null;
		ArrayList clientList = (ArrayList) clientAdapter
				.getClientsByClientNumberRange(startClientNumber,
						endClientNumber);

		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName);
		} catch (IOException ex) {
			throw new SystemException("Unable to create " + fileName + ".", ex);
		}

		DateTime startTime = new DateTime();

		Hashtable clientSegments = null;

		try {
			fileWriter.write("Client Number");
			fileWriter.write(FileUtil.SEPARATOR_COMMA);
			fileWriter.write(SEGMENT_POLICIES);
			fileWriter.write(FileUtil.SEPARATOR_COMMA);
			fileWriter.write(SEGMENT_MEMBERSHIPS);
			fileWriter.write(FileUtil.SEPARATOR_COMMA);
			fileWriter.write(SEGMENT_TRAVEL);
			fileWriter.write(FileUtil.SEPARATOR_COMMA);
			fileWriter.write(SEGMENT_VI);
			fileWriter.write(FileUtil.SEPARATOR_COMMA);
			fileWriter.write(SEGMENT_FLEET);
			fileWriter.write(FileUtil.SEPARATOR_COMMA);
			fileWriter.write(SEGMENT_MAPS);
			fileWriter.write(FileUtil.SEPARATOR_COMMA);
			fileWriter.write(SEGMENT_PAYABLES);
			fileWriter.write(FileUtil.NEW_LINE);
			fileWriter.flush();

		} catch (IOException ex3) {
			throw new SystemException("Unable to write file header. "
					+ ex3.getMessage());
		}

		for (int i = 0; i < clientList.size(); i++) {
			clientNumber = (Integer) clientList.get(i);
			LogUtil.debug(this.getClass(), "clientNumber: " + clientNumber);
			LogUtil.debug(this.getClass(), "start");
			clientSegments = this.getClientSegments(clientNumber);
			LogUtil.debug(this.getClass(), "end");

			// write a line
			try {
				fileWriter.write(clientNumber.toString());
				fileWriter.write(FileUtil.SEPARATOR_COMMA);
				fileWriter.write(getSegmentResponse(SEGMENT_POLICIES,
						clientSegments));
				fileWriter.write(FileUtil.SEPARATOR_COMMA);
				fileWriter.write(getSegmentResponse(SEGMENT_MEMBERSHIPS,
						clientSegments));
				fileWriter.write(FileUtil.SEPARATOR_COMMA);
				fileWriter.write(getSegmentResponse(SEGMENT_TRAVEL,
						clientSegments));
				fileWriter.write(FileUtil.SEPARATOR_COMMA);
				fileWriter
						.write(getSegmentResponse(SEGMENT_VI, clientSegments));
				fileWriter.write(FileUtil.SEPARATOR_COMMA);
				fileWriter.write(getSegmentResponse(SEGMENT_FLEET,
						clientSegments));
				fileWriter.write(FileUtil.SEPARATOR_COMMA);
				fileWriter.write(getSegmentResponse(SEGMENT_MAPS,
						clientSegments));
				fileWriter.write(FileUtil.SEPARATOR_COMMA);
				fileWriter.write(getSegmentResponse(SEGMENT_PAYABLES,
						clientSegments));
				fileWriter.write(FileUtil.NEW_LINE);
				fileWriter.flush();
			} catch (IOException ex2) {
				throw new SystemException("Unable to write file line. "
						+ ex2.getMessage());
			}

			LogUtil.log(this.getClass(), clientNumber.toString());
			LogUtil.log(this.getClass(), clientSegments.toString());

		}

		// write list to file?
		DateTime endTime = new DateTime();

		long duration = (endTime.getTime() - startTime.getTime());

		LogUtil.log(this.getClass(), "Completed in " + duration + " ms");

		try {
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException ex1) {
			LogUtil.fatal(this.getClass(), ex1);
		}

	}

	private String getSegmentResponse(String key, Hashtable table) {
		StringBuffer response = new StringBuffer();
		boolean exists = ((Boolean) table.get(key)).booleanValue();
		if (exists) {
			response.append("Y");
		} else {
			response.append("N");
		}
		return response.toString();
	}

	// natural sort order
	private final String SEGMENT_POLICIES = "Policies";
	private final String SEGMENT_MEMBERSHIPS = "Memberships";
	private final String SEGMENT_TRAVEL = "Travel Accounts"; // to be retired
	private final String SEGMENT_VI = "Vehicle Inspections";
	private final String SEGMENT_FLEET = "Fleet";
	private final String SEGMENT_MAPS = "MAPs";
	private final String SEGMENT_PAYABLES = "Payables";

	private Hashtable getClientSegments(Integer clientNumber)
			throws SystemException {
		Hashtable clientSegments = new Hashtable();

		LogUtil.debug(this.getClass(), "1");
		// check insurance
		ArrayList policyList = InsuranceFactory.getInsuranceAdapter()
				.getPolicyList(clientNumber,
						SourceSystem.INSURANCE_RACT.getAbbreviation());
		boolean hasPolicies = !policyList.isEmpty();
		clientSegments.put(SEGMENT_POLICIES, new Boolean(hasPolicies));

		LogUtil.debug(this.getClass(), "2");

		// check membership
		Collection membershipList = new ArrayList();
		try {
			// MembershipHome membershipHome =
			// MembershipEJBHelper.getMembershipHome();
			MembershipMgr membershipMgr = MembershipEJBHelper
					.getMembershipMgr();
			membershipList = membershipMgr
					.findMembershipByClientNumber(clientNumber);
		} catch (RemoteException ex) {
			LogUtil.fatal(this.getClass(), "Unable to get membership list. "
					+ ex);
			// ignore
		}
		boolean hasMemberships = !membershipList.isEmpty();
		clientSegments.put(SEGMENT_MEMBERSHIPS, new Boolean(hasMemberships));

		LogUtil.debug(this.getClass(), "3");

		// check travel
		ArrayList travelAccountList = TravelFactory.getTravelAdapter()
				.getTravelFiles(clientNumber);
		boolean hasTravelFiles = !travelAccountList.isEmpty();
		clientSegments.put(SEGMENT_TRAVEL, new Boolean(hasTravelFiles));

		LogUtil.debug(this.getClass(), "4");

		// check vi
		ArrayList viList = VehicleInspectionFactory
				.getVehicleInspectionAdapter().getVIList(clientNumber);
		boolean hasVIs = !viList.isEmpty();
		clientSegments.put(SEGMENT_VI, new Boolean(hasVIs));

		LogUtil.debug(this.getClass(), "5");

		// check fleet
		ArrayList fleetList = FleetFactory.getFleetAdapter().getNVIList(
				clientNumber);
		boolean hasFleet = !fleetList.isEmpty();
		clientSegments.put(SEGMENT_FLEET, new Boolean(hasFleet));

		LogUtil.debug(this.getClass(), "6");

		// check map
		ArrayList mapList = FleetFactory.getFleetAdapter().getMAPList(
				clientNumber);
		boolean hasMAPs = !mapList.isEmpty();
		clientSegments.put(SEGMENT_MAPS, new Boolean(hasMAPs));

		LogUtil.debug(this.getClass(), "7");

		// check oi-payables for FIN, DS and Claims
		final String PAYABLE_TYPE_LIST[] = { "FIN", "DS", "CLM" };

		ReceiptingAdapter ocrAdapter = new OCRAdapter();
		ArrayList payableList = new ArrayList();
		try {
			payableList = (ArrayList) ocrAdapter
					.getPayableHistory(clientNumber);
		} catch (RemoteException ex1) {
			LogUtil.fatal(this.getClass(), "Unable to get payable list. " + ex1);
			// ignore
		}
		Payable payable = null;
		boolean hasPayables = false;
		for (int i = 0; i < payableList.size(); i++) {
			payable = (Payable) payableList.get(i);
			for (int j = 0; j < PAYABLE_TYPE_LIST.length; j++) {

				if (PAYABLE_TYPE_LIST[j].equalsIgnoreCase(payable
						.getSourceSystem().getAbbreviation())) {
					hasPayables = true;
				}

			}
		}
		clientSegments.put(SEGMENT_PAYABLES, new Boolean(hasPayables));

		LogUtil.debug(this.getClass(), "8");

		LogUtil.debug(this.getClass(), "Results:");
		LogUtil.debug(this.getClass(), "========");
		LogUtil.debug(this.getClass(), "Policies:       " + hasPolicies);
		LogUtil.debug(this.getClass(), "Memberships:    " + hasMemberships);
		LogUtil.debug(this.getClass(), "TravelFiles:    " + hasTravelFiles);
		LogUtil.debug(this.getClass(), "VIs:            " + hasVIs);
		LogUtil.debug(this.getClass(), "Fleet:          " + hasFleet);
		LogUtil.debug(this.getClass(), "MAPs:           " + hasMAPs);
		LogUtil.debug(this.getClass(), "Payables:       " + hasPayables);

		return clientSegments;

	}

	/**
	 * count the number of organisations
	 * 
	 * @return The organisationCount value
	 * @exception ObjectNotFoundException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public int getOrganisationCount() throws RemoteException {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		return commonMgr.countRecords(SQL_ORGANISATION_COUNT);
	}

	private final String SQL_ORGANISATION_COUNT = "select count(\"client-no\") from PUB.\"cl-master\" WHERE \"client-title\" = ''";

	/**
	 * count the number of people
	 * 
	 * @return The personCount value
	 * @exception ObjectNotFoundException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public int getPersonCount() throws RemoteException {
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		return commonMgr.countRecords(SQL_PERSON_COUNT);
	}

	private final String SQL_PERSON_COUNT = "select count(\"client-no\") from PUB.\"cl-master\" WHERE \"client-title\" <> ''";

	public Collection findClientsBySurnameAndAddress(String surname,
			String initials, String street, String suburb, DateTime birthDate) {
		return null;
	}

	public ClientVO findClientByIVRNumber(String ivrNumber)
			throws RemoteException, ValidationException {
		final String SQL_CLIENT_IVR_NUMBER = "SELECT b.client_number FROM PUB.pt_pending_fee a,PUB.mem_membership b WHERE a.client_number = b.client_number AND a.pending_fee_id = ?";

		Connection connection = null;
		PreparedStatement statement = null;

		ClientVO cVO = null;

		try {
			if (ivrNumber != null) {
				connection = dataSource.getConnection();
				statement = connection.prepareStatement(SQL_CLIENT_IVR_NUMBER);

				statement.setString(1, ivrNumber);
				ResultSet resultSet = statement.executeQuery();
				if (resultSet.next()) {
					cVO = getClient(new Integer(resultSet.getInt(1)));
				}
			} else {
				throw new ValidationException("Invalid IVR Number " + ivrNumber);
			}

		} catch (SQLException e) {
			throw new RemoteException(SQL_CLIENT_IVR_NUMBER + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return cVO;

	}

	/**
	 * Get a branch region given a postcode. Only one branch.
	 * 
	 * @param postcode
	 *            Description of the Parameter
	 * @return The branchForPostcode value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public BranchVO getBranchForPostcode(Integer postcode)
			throws RemoteException {
		Connection connection = null;
		PreparedStatement statement = null;
		BranchVO branch = new BranchVO();

		String sql = "";
		sql += "select a1.\"branch-no\",\"description\" from PUB.\"gn-postcode\" a1,PUB.\"gn-branch\" a2";
		sql += " where a1.\"branch-no\" = a2.\"branch-no\"";
		sql += " and postcode = ?";

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setInt(1, postcode.intValue());
			ResultSet resultSet = statement.executeQuery();
			// should only be one match
			if (resultSet.next()) {
				branch.setBranchNumber(resultSet.getInt(1));
				branch.setDescription(resultSet.getString(2));
			}
			return branch;
		} catch (Exception e) {
			throw new RemoteException(sql + " " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	/**
	 * Gets the occupation attribute of the ClientMgrBean object
	 * 
	 * @param clientOccupationID
	 *            Description of the Parameter
	 * @return The occupation value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public ClientOccupationVO getClientOccupation(Integer clientOccupationID)
			throws RemoteException {
		ClientOccupationVO clientOccupation = null;
		try {
			clientOccupation = (ClientOccupationVO) hsession.get(
					ClientOccupationVO.class, clientOccupationID);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find occupation.", ex);
		}
		return clientOccupation;

		// ClientOccupationHome clientOccupationHome = null;
		// ClientOccupation m = null;
		// ClientOccupationVO cvo = null;
		// try
		// {
		// if(clientOccupationID != null && clientOccupationID.intValue() != 0)
		// {
		// clientOccupationHome = ClientEJBHelper.getClientOccupationHome();
		// m = clientOccupationHome.findByPrimaryKey(clientOccupationID);
		// if(m != null)
		// {
		// cvo = m.getVO();
		// }
		// }
		// }
		// catch(Exception e)
		// {
		// throw new RemoteException("Unable to get client occupation (" +
		// clientOccupationID + "). " + e);
		// }
		// return cvo;
	}

	public Collection findClientsByMobilePhone(String mobilePhone)
			throws RemoteException {
		Collection clientList = null;
		try {
			clientList = new ArrayList(hsession.createCriteria(ClientVO.class)
					.add(Restrictions.eq("mobilePhone", mobilePhone)).list());

		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	public Collection findClientsByHomePhone(String homePhone)
			throws RemoteException {
		Collection clientList = null;
		try {
			clientList = new ArrayList(hsession.createCriteria(ClientVO.class)
					.add(Restrictions.eq("homePhone", homePhone)).list());

		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	public Collection findClientsByNameAndTitle(String initials,
			String surname, String title) throws RemoteException {
		Collection clientList = null;
		try {
			LogUtil.debug(this.getClass(), "findClientsByNameAndTitle "
					+ initials + " " + surname + " " + title);

			Criteria criteria = hsession.createCriteria(ClientVO.class);
			criteria.add(Restrictions.eq("initials", initials));
			criteria.add(Restrictions.eq("surname", surname));
			criteria.add(Restrictions.eq("title", title));
			criteria.setMaxResults(100);

			clientList = new ArrayList(criteria.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	public Collection findClientsBySurname(String surname)
			throws RemoteException {
		Collection clientList = null;
		try {
			clientList = new ArrayList(hsession.createCriteria(ClientVO.class)
					.add(Restrictions.eq("surname", surname))
					.setMaxResults(100).list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	/**
	 * Return a list of ClientVO objects matching provided email address query.
	 */
	public Collection<ClientVO> findClientsByEmailAddress(String emailAddress)
			throws RemoteException {
		Collection<ClientVO> clientList = null;
		try {
			clientList = new ArrayList<ClientVO>(hsession
					.createCriteria(ClientVO.class)
					.add(Restrictions.like("emailAddress", emailAddress))
					.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	public Collection findClientsByNameAndBirthDate(String initials,
			String surname, DateTime birthDate) throws RemoteException {
		Collection clientList = null;
		try {
			Criteria criteria = hsession.createCriteria(ClientVO.class);
			criteria.add(Restrictions.eq("surname", surname));
			criteria.add(Restrictions.eq("birthDate", birthDate));
			criteria.add(Restrictions.eq("initials", initials));

			clientList = new ArrayList(criteria.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	public Collection findClientsByNameAndGivenName(String givenName,
			String surname) throws RemoteException {
		Collection clientList = null;
		try {
			// starts with initials
			givenName = givenName + CommonConstants.WILDCARD; // eg. J%
			LogUtil.debug(this.getClass(), "givenName=" + givenName);
			LogUtil.debug(this.getClass(), "surname=" + surname);
			clientList = new ArrayList(hsession.createCriteria(ClientVO.class)
					.add(Restrictions.eq("surname", surname))
					.add(Restrictions.like("givenNames", givenName)).list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	private Collection findClientsByAddress(String searchValue)
			throws RemoteException {
		String values[] = searchValue.split(",");
		if (values.length == 3) {
			return findClientsByAddress(values[0], values[1], new Integer(
					values[2]));
		} else {
			throw new RemoteException("Unexpected number of values in search. "
					+ searchValue
					+ " "
					+ (searchValue == null ? "0" : String.valueOf(searchValue
							.length())));
		}
	}

	public Collection findClientsByAddress(String property,
			String streetNumber, Integer resiStsubId) throws RemoteException {
		Collection clientList = null;
		try {
			Criteria crit = hsession.createCriteria(ClientVO.class).add(
					Restrictions.eq("resiStsubId", resiStsubId));
			if (streetNumber != null && !streetNumber.trim().equals("")) {
				crit.add(Restrictions.eq("resiStreetNumber", new Integer(
						streetNumber)));
			}
			if (property != null && !property.trim().equals("")) {
				property = CommonConstants.WILDCARD + property
						+ CommonConstants.WILDCARD;
				crit.add(Restrictions.like("resiProperty", property));
			}
			clientList = new ArrayList(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	public Collection findClientsByNameAndAddress(String surname,
			Integer resiStreetNumber, Integer resiStsubId)
			throws RemoteException {
		return findClientsByNameAndAddress(surname, resiStreetNumber,
				resiStsubId, null);
	}

	public Collection findClientsByNameAndAddress(String surname,
			Integer resiStreetNumber, Integer resiStsubId, Boolean sex)
			throws RemoteException {
		Collection clientList = null;
		try {
			Criteria criteria = hsession.createCriteria(ClientVO.class);
			criteria.add(Restrictions.eq("surname", surname));
			criteria.add(Restrictions.eq("resiStsubId", resiStsubId));
			criteria.add(Restrictions.eq("resiStreetNumber", resiStreetNumber));
			if (sex != null) {
				criteria.add(Restrictions.eq("sex", sex));
			}

			clientList = new ArrayList(criteria.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find clients.", ex);
		}
		return clientList;
	}

	/**
	 * Gets the client attribute of the ClientMgrBean object
	 * 
	 * @param clientNumber
	 *            Description of the Parameter
	 * @return The client value
	 * @exception RemoteException
	 *                Description of the Exception
	 * @exception ValidationException
	 *                Description of the Exception
	 */
	public ClientVO getClient(Integer clientNumber) {
		MethodCounter.addToCounter("ClientVO", "getClient");
		ClientVO client = null;

		// subclassed as organisation/person
		client = (ClientVO) hsession.get(ClientVO.class, clientNumber);

		return client;
	}

	/**
	 * Gets the clientVehicleList attribute of the ClientMgrBean object
	 * 
	 * @param clientNumber
	 *            Description of the Parameter
	 * @return The clientVehicleList value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection getClientVehicleList(Integer clientNumber) {
		Query vehicleQuery = em
				.createQuery("select e from VehicleVO e where e.clientNo = ?1");
		vehicleQuery.setParameter(1, clientNumber);
		Collection vList = vehicleQuery.getResultList();
		return vList;
	}

	public Collection getNoteTypes() {
		Query noteTypeQuery = em.createQuery("select e from NoteType e");
		Collection noteTypeList = noteTypeQuery.getResultList();
		return noteTypeList;
	}

	public Collection getNoteType(boolean blockTransactions) {
		Query noteTypeQuery = em
				.createQuery("select e from NoteType e where e.blockTransactions = ?1");
		noteTypeQuery.setParameter(1, blockTransactions);
		Collection noteTypeList = noteTypeQuery.getResultList();
		return noteTypeList;
	}

	public Collection getNoteCategories() {
		Query noteTypeQuery = em.createQuery("select e from NoteCategory e");
		Collection noteTypeList = noteTypeQuery.getResultList();
		return noteTypeList;
	}

	public Collection<NoteCategory> getFollowUpNoteCategories() {
		Query noteTypeQuery = em
				.createQuery("select e from NoteCategory e where followUp = :followUp order by e.categoryName asc");
		noteTypeQuery.setParameter("followUp", true);
		Collection<NoteCategory> noteTypeList = noteTypeQuery.getResultList();
		return noteTypeList;
	}

	public ClientNote getLastClientNote(Integer clientNumber) {
		Query query = em
				.createQuery("select mn from ClientNote mn where mn.clientNumber = ?1 and mn.noteId = (select max(mn2.noteId) from ClientNote mn2 where mn2.clientNumber = mn.clientNumber)");
		query.setParameter(1, clientNumber);
		List result = query.getResultList();
		if (result != null && result.size() > 0) {
			return (ClientNote) result.iterator().next();
		} else {
			return null;
		}
	}

	public ClientNote getLastClientNote(Integer clientNumber,
			String businessType) {

		Query query = em
				.createQuery("select mn from ClientNote mn where mn.clientNumber = ?1 and mn.businessType = ?2 and mn.noteId = (select max(mn2.noteId) from ClientNote mn2 where mn2.clientNumber = mn.clientNumber and  mn2.businessType = mn.businessType)");
		query.setParameter(1, clientNumber);
		query.setParameter(2, businessType);
		List result = query.getResultList();
		if (result != null && result.size() > 0) {
			return (ClientNote) result.iterator().next();
		} else {
			return null;
		}
	}

	public Collection getClientNoteList(Integer clientNumber) {
		Query query = em
				.createQuery("select e from ClientNote e where e.clientNumber = ?1");
		query.setParameter(1, clientNumber);
		return query.getResultList();
	}

	public Collection getClientNoteListByNoteType(Integer clientNumber,
			String noteType) {
		Query query = em
				.createQuery("select e from ClientNote e where e.clientNumber = ?1 and e.noteType = ?2");
		query.setParameter(1, clientNumber);
		query.setParameter(2, noteType);
		return query.getResultList();
	}

	public Collection getClientNoteList(String businessType, String referenceId) {
		Query query = em
				.createQuery("select e from ClientNote e where e.referenceId = ?1 and e.businessType = ?2");
		query.setParameter(1, referenceId);
		query.setParameter(2, businessType);
		return query.getResultList();
	}

	public Collection getClientNoteList(Integer clientNumber,
			String businessType, String noteType) {
		Query query = em
				.createQuery("select e from ClientNote e where e.clientNumber = ?1 and e.businessType = ?2 and e.noteType = ?3");
		query.setParameter(1, clientNumber);
		query.setParameter(2, businessType);
		query.setParameter(3, noteType);
		return query.getResultList();
	}

	public Collection getClientNoteList(Integer clientNumber,
			String businessType) {
		Query query = em
				.createQuery("select e from ClientNote e where e.clientNumber = ?1 and e.businessType = ?2");
		query.setParameter(1, clientNumber);
		query.setParameter(2, businessType);
		return query.getResultList();
	}

	public Collection getClientNoteList(String businessType,
			Integer clientNumber, boolean blockTransaction) {
		Query query = em
				.createQuery("select e from ClientNote e, NoteType n where e.noteType = n.noteType and e.clientNumber = ?1 and e.businessType = ?2 and n.blockTransactions = ?3");
		query.setParameter(1, clientNumber);
		query.setParameter(2, businessType);
		query.setParameter(3, blockTransaction);
		return query.getResultList();
	}

	public Collection<ClientBulkUpdate> getClientBulkUpdateByDate(
			DateTime startDate, DateTime endDate) {
		Query query = em.createNamedQuery("findClientsByDate");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		return query.getResultList();
	}

	public Collection getClientBulkUpdateByClient(Integer clientNumber) {
		Query query = em.createNamedQuery("findClient");
		query.setParameter("clientNumber", clientNumber);
		return query.getResultList();
	}

	public ClientNote getClientNote(Integer noteId) {
		return em.find(ClientNote.class, noteId);
	}

	public Collection<NoteCategory> getNoteCategories(String businessType) {
		Query query = em
				.createQuery("select e from NoteCategory e where e.businessType = ?1 order by e.categoryName asc");
		query.setParameter(1, businessType);
		return query.getResultList();
	}

	public Collection<NoteResult> getNoteResults(String noteType) {
		Query query = em
				.createQuery("select e from NoteResult e where e.noteType = ?1");
		query.setParameter(1, noteType);
		return query.getResultList();
	}

	public Collection<NoteResult> getNoteResults() {
		Query query = em.createQuery("select e from NoteResult e");
		return query.getResultList();
	}

	public NoteResult getNoteResult(String resultCode) {
		return em.find(NoteResult.class, resultCode);
	}

	public NoteCategory getNoteCategory(String categoryCode) {
		return em.find(NoteCategory.class, categoryCode);
	}

	public NoteType getNoteType(String noteType) {
		return em.find(NoteType.class, noteType);
	}

	public final static SimpleDateFormat FORMAT_DATE = new SimpleDateFormat(
			"dd/MM/yyyy");

	public final static SimpleDateFormat FORMAT_TIME = new SimpleDateFormat(
			"h:mm a");

	public void createClientNote(ClientNote clientNote) throws RemoteException {
		NoteType noteType = getNoteType(clientNote.getNoteType());
		if (noteType.isBlockTransactions()) {
			Collection blockerNotes = getClientNoteList(
					clientNote.getBusinessType(), clientNote.getClientNumber(),
					noteType.isBlockTransactions());
			ClientNote blocker;
			for (Iterator<ClientNote> i = blockerNotes.iterator(); i.hasNext();) {
				blocker = i.next();
				if (blocker.isBlockTransactions()) {
					throw new SystemException(
							"Only one active transaction blocking note may be created per business type and client.");
				}
			}
		}

		notifyClientNote(clientNote);

		SequenceMgr sequenceMgr = CommonEJBHelper.getSequenceMgr();
		Integer noteId = sequenceMgr
				.getNextID(SequenceMgr.SEQUENCE_CLIENT_NOTE);
		clientNote.setNoteId(noteId);

		if (noteType.isBlockTransactions()) {
			clientNote.setBlockTransactions(true);
		}
		LogUtil.log(this.getClass(), "a clientNote=" + clientNote);
		em.persist(clientNote);
		LogUtil.log(this.getClass(), "b clientNote=" + clientNote);
		ClientVO client = getClient(clientNote.getClientNumber());
		LogUtil.log(this.getClass(), "c clientNote=" + clientNote);
		boolean market = (client.getMarket() != null ? client.getMarket()
				.booleanValue() : false);
		String motorNewsSendOption = client.getMotorNewsSendOption();
		LogUtil.log(this.getClass(), "d clientNote=" + clientNote);
		// set new market preference
		boolean newMarket = false;

		if (clientNote.isBlockTransactions()) {
			// only allow blocker notes on prime addressee
			if (BusinessType.BUSINESS_TYPE_MEMBERSHIP.equals(clientNote
					.getBusinessType())) {
				LogUtil.log(this.getClass(), "e clientNote=" + clientNote);
				MembershipVO membership = null;
				Collection<MembershipVO> membershipList = MembershipEJBHelper
						.getMembershipMgr().findMembershipByClientNumber(
								new Integer(clientNote.getClientNumber()));
				// should be only one.
				LogUtil.log(this.getClass(), "f clientNote=" + clientNote);
				for (Iterator<MembershipVO> i = membershipList.iterator(); i
						.hasNext();) {
					membership = i.next();
					LogUtil.log(this.getClass(), "1 clientNote=" + clientNote);
					if (membership != null) {
						if (!membership.isPrimeAddressee()) {
							throw new SystemException(
									"Blocking notes may only be created on the prime address. Remove the member from the group first.");
						}
					}
				}
				LogUtil.log(this.getClass(), "g clientNote=" + clientNote);
			}
			LogUtil.log(this.getClass(), "h clientNote=" + clientNote);
			newMarket = false;
			// set motor news to do not send
			motorNewsSendOption = Client.MOTOR_NEWS_NO_SEND;
			// remove any subscriptions
			deleteClientSubscriptions(client.getClientNumber());
			LogUtil.log(this.getClass(), "i clientNote=" + clientNote);
		} else {
			if (!clientNote.isBlockTransactions()) {
				// re-enabling membership and therefore allowing marketing
				// again.
				newMarket = true;
			}
		}
		LogUtil.log(this.getClass(), "Setting market " + newMarket);

		client.setMarket(new Boolean(newMarket));
		client.setMotorNewsSendOption(motorNewsSendOption);
		updateClient(client);
		LogUtil.log(this.getClass(), "j clientNote=" + clientNote);

	}

	/**
	 * Get the VO so that we can later validate sex specific titles.
	 * 
	 * @return a list of all client titles. eg. Mr, Sir, etc.
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public Collection getCachedClientTitles() throws RemoteException {
		return ClientTitleCache.getInstance().getClientTitleList();
	}

	public Collection<ClientTitleVO> getClientTitles() throws RemoteException {
		return getClientTitles(false);
	}

	/**
	 * Retrieve a list of ClientTitleVO objects.
	 * 
	 * @param enabledOnly
	 *            limit list to those without a deleteDate set.
	 * @return
	 * @throws RemoteException
	 */
	public Collection<ClientTitleVO> getClientTitles(boolean enabledOnly)
			throws RemoteException {
		Collection<ClientTitleVO> titleList = null;
		try {
			Criteria c = hsession.createCriteria(ClientTitleVO.class);
			if (enabledOnly) {
				c.add(Restrictions.isNull("deleteDate"));
			}
			titleList = new ArrayList<ClientTitleVO>(c.list());

			LogUtil.debug(this.getClass(), "titleList " + titleList);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find client titles.", ex);
		}
		return titleList;
	}

	public ClientTitleVO getClientTitle(String titleName) {
		return (ClientTitleVO) hsession.get(ClientTitleVO.class, titleName);
	}

	/**
	 * @return a list of all client occupations. eg. Doctor, Surgeon, etc.
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection getClientOccupationList() throws RemoteException {
		return ClientOccupationCache.getInstance().getClientOccupationList();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void createClientNoteNew(ClientNote clientNote)
			throws RemoteException {
		createClientNote(clientNote);
	}

	public Collection getClientOccupations() throws RemoteException {
		Collection occupationList = null;
		try {
			occupationList = new ArrayList(hsession.createCriteria(
					ClientOccupationVO.class).list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to find client titles.", ex);
		}
		return occupationList;
	}

	public void deleteClientSubscriptions(Integer clientNumber) {
		LogUtil.log(this.getClass(), "deleteClientSubscriptions start");
		LogUtil.log(this.getClass(), "clientNumber=" + clientNumber);
		org.hibernate.Query query = hsession
				.createQuery("delete from ClientSubscription where clientSubscriptionPK.clientNumber = :clientNumber");
		query.setInteger("clientNumber", clientNumber);
		int rowsUpdated = query.executeUpdate();
		LogUtil.log(this.getClass(), "Deleted " + rowsUpdated
				+ " client subscriptions.");
		LogUtil.log(this.getClass(), "deleteClientSubscriptions end");
	}

	public void deleteClientNotes(Integer clientNumber) {
		LogUtil.log(this.getClass(), "deleteClientNotes start");
		LogUtil.log(this.getClass(), "clientNumber=" + clientNumber);
		org.hibernate.Query query = hsession
				.createQuery("delete from ClientNote where clientNumber = :clientNumber");
		query.setInteger("clientNumber", clientNumber);
		int rowsUpdated = query.executeUpdate();
		LogUtil.log(this.getClass(), "Deleted " + rowsUpdated
				+ " client notes.");
		LogUtil.log(this.getClass(), "deleteClientNotes end");
	}

	public void deleteClientSubscription(
			ClientSubscriptionPK clientSubscriptionPK) {
		ClientSubscription clientSubscription = null;
		LogUtil.log(this.getClass(), "clientSubscriptionPK="
				+ clientSubscriptionPK.toString());
		clientSubscription = (ClientSubscription) hsession.get(
				ClientSubscription.class, clientSubscriptionPK);

		LogUtil.log(this.getClass(), "1clientSubscription="
				+ clientSubscription);

		hsession.delete(clientSubscription);
		HibernateUtil.flushSession(hsession);

		clientSubscriptionPK
				.setSubscriptionStatus(ClientSubscription.STATUS_DISABLED);
		clientSubscription.setClientSubscriptionPK(clientSubscriptionPK);
		LogUtil.log(this.getClass(), "2clientSubscription="
				+ clientSubscription);

		hsession.save(clientSubscription);
		LogUtil.debug(this.getClass(), "3clientSubscription="
				+ clientSubscription);

	}

	/**
	 * 
	 * @param client
	 *            ClientVO
	 * @param userId
	 *            String
	 * @param salesBranchCode
	 *            String
	 * @throws RemoteException
	 */
	public void createDefaultClientSubscriptions(ClientVO client)
			throws RemoteException {
		String userId = client.getMadeID();
		String salesBranchCode = null;
		UserMgr userMgr = UserEJBHelper.getUserMgr();
		User user = userMgr.getUser(userId);
		// if valid user then get the sales branch code
		if (user != null) {
			salesBranchCode = user.getSalesBranchCode();
		}

		try {
			Publication publication = null;
			ClientSubscription clientSubscription = null;
			// only those publications with a customer default flag.
			ArrayList defaultPublicationList = new ArrayList(hsession
					.createCriteria(Publication.class)
					.add(Expression.eq("customerDefault", new Boolean(true)))
					.list());

			// note: market flag is no longer checked for default subscriptions
			if (defaultPublicationList != null) {
				Iterator defaultPubIt = defaultPublicationList.iterator();
				while (defaultPubIt.hasNext()) {
					publication = (Publication) defaultPubIt.next();
					LogUtil.log(this.getClass(), "publication=" + publication);
					clientSubscription = new ClientSubscription(
							new ClientSubscriptionPK(client.getClientNumber(),
									publication.getPublicationCode(),
									ClientSubscription.STATUS_ACTIVE,
									new DateTime()), null, salesBranchCode,
							userId);
					// create a subscription
					createClientSubscription(clientSubscription);
				}
			}
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}

	}

	public void createClientSubscription(ClientSubscription clientSubscription) {
		hsession.save(clientSubscription);
	}

	public void createClientVehicle(VehicleVO vehicle) {
		hsession.save(vehicle);
	}

	public Collection getClientSubscriptionList(Integer clientNumber) {
		ArrayList subscriptionList = new ArrayList();
		subscriptionList = new ArrayList(hsession
				.createCriteria(ClientSubscription.class)
				.add(Restrictions.eq("clientSubscriptionPK.clientNumber",
						clientNumber)).list());
		return subscriptionList;
	}

	/**
	 * Retrieve a Subscription associated with a client, regardless of whether
	 * it is active.
	 * 
	 * @param clientNumber
	 * @param subscriptionCode
	 * @return
	 */
	public ClientSubscription getClientSubscription(Integer clientNumber,
			String subscriptionCode) {
		ClientSubscription clientSubscription = null;
		List<ClientSubscription> subscriptionList = hsession
				.createCriteria(ClientSubscription.class)
				.add(Restrictions.eq("clientSubscriptionPK.clientNumber",
						clientNumber))
				.add(Restrictions.eq("clientSubscriptionPK.subscriptionCode",
						subscriptionCode))
				.addOrder(Order.desc("clientSubscriptionPK.subscriptionDate")).list();
		
		if (subscriptionList != null && subscriptionList.size() > 0) {
			clientSubscription = subscriptionList.iterator().next();
		}
		return clientSubscription;
	}

	/**
	 * Retrieve an Active Subscription associated with a Client.
	 * 
	 * @param clientNumber
	 * @param subscriptionCode
	 * @return
	 */
	public ClientSubscription getActiveClientSubscription(Integer clientNumber,
			String subscriptionCode) {
		ClientSubscription clientSubscription = null;
		List<ClientSubscription> subscriptionList = hsession
				.createCriteria(ClientSubscription.class)
				.add(Restrictions.eq("clientSubscriptionPK.clientNumber",
						clientNumber))
				.add(Restrictions.eq("clientSubscriptionPK.subscriptionCode",
						subscriptionCode))
				.add(Restrictions.eq("clientSubscriptionPK.subscriptionStatus",
						ClientSubscription.STATUS_ACTIVE)).list();
		if (subscriptionList != null && subscriptionList.size() > 0) {
			clientSubscription = subscriptionList.iterator().next();
		}
		return clientSubscription;
	}

	public Collection getSubscribedClients(String publicationCode)
			throws RemoteException {

		Publication pub = commonMgr.getPublication(publicationCode);
		String queryStr = "from ClientVO client, ClientSubscription subscription where client.clientNumber = subscription.clientSubscriptionPK.clientNumber and subscription.clientSubscriptionPK.subscriptionCode = :publicationCode and subscription.clientSubscriptionPK.subscriptionStatus = :status and (client.status is null or client.status = '' or client.status = 'ACTIVE') and (client.groupClient is null or client.groupClient = :groupClient) ";
		if (publicationCode.equals(Publication.RACT_NEWSLETTER)) {
			queryStr += " and client.emailAddress is not null and client.emailAddress != '' and client.emailAddress like '%@%' ";
		}

		if (pub.isMembershipDependent()) {
			// must have a membership
			queryStr += "and exists (select membership from MembershipVO membership where client.clientNumber = membership.clientNumber and membership.baseStatus = :mstatus) ";
		}
		LogUtil.log(this.getClass(), "queryStr=" + queryStr);
		javax.persistence.Query query = em.createQuery(queryStr);
		query.setParameter("publicationCode", publicationCode);
		query.setParameter("status", ClientSubscription.STATUS_ACTIVE);
		query.setParameter("groupClient", false);
		query.setParameter("mstatus", MembershipVO.STATUS_ACTIVE);
		ArrayList clientList = new ArrayList();
		Collection result = query.getResultList();
		ClientVO client = null;
		for (Iterator it = result.iterator(); it.hasNext();) {
			Object[] objects = (Object[]) it.next();
			client = (ClientVO) objects[0];
			clientList.add(client);
		}
		return clientList;
	}

	public void initialiseClientOccupationList() throws RemoteException {
		ClientOccupationCache.getInstance().initialiseList();
	}

	public void initialiseClientTitleList() throws RemoteException {
		ClientTitleCache.getInstance().initialiseList();
	}

	/**
	 * @param searchType
	 *            Description of the Parameter
	 * @param searchValue
	 *            Description of the Parameter
	 * @return a list of clients, as ClientVOs, that match the search criteria.
	 *         Returns an empty list if no matching clients were found.
	 * @exception RemoteException
	 *                Description of the Exception
	 * @exception ValidationException
	 *                Description of the Exception
	 */
	public Collection searchForClient(int searchType, String searchValue)
			throws RemoteException, ValidationException {
		LogUtil.debug(this.getClass(), "searchType: " + searchType);
		LogUtil.debug(this.getClass(), "searchValue: " + searchValue);

		if (searchValue == null || searchValue.length() == 0) {
			throw new ValidationException("A search term must be entered.");
		}

		// Collection clientList = new Vector();

		Collection findList = null;

		ClientVO client = null;
		// ClientHome clientHome = ClientEJBHelper.getClientHome();

		// If doing a smart search then work out what the real search type
		// should be.
		if (searchType == ClientUIConstants.SEARCHBY_SMART_SEARCH) {
			// If the search value contains a colon then interpret the search
			// modifier.
			int colonPos = searchValue.indexOf(":");
			if (colonPos >= 0) {
				String modifier = searchValue.substring(0, colonPos);
				searchValue = searchValue.substring(colonPos + 1).trim();
				if (ClientUIConstants.SEARCH_MODIFIER_CLIENT_NUMBER
						.equalsIgnoreCase(modifier)) {
					searchType = ClientUIConstants.SEARCHBY_CLIENT_NUMBER;
				} else if (ClientUIConstants.SEARCH_MODIFIER_CLIENT_SURNAME
						.equalsIgnoreCase(modifier)) {
					searchType = ClientUIConstants.SEARCHBY_CLIENT_SURNAME;
				} else if (ClientUIConstants.SEARCH_MODIFIER_MEMBERSHIP_NUMBER
						.equalsIgnoreCase(modifier)) {
					searchType = ClientUIConstants.SEARCHBY_MEMBERSHIP_NUMBER;
				} else if (ClientUIConstants.SEARCH_MODIFIER_POLICY_NUMBER
						.equalsIgnoreCase(modifier)) {
					searchType = ClientUIConstants.SEARCHBY_POLICY_NUMBER;
				} else if (ClientUIConstants.SEARCH_MODIFIER_HOME_PHONE
						.equalsIgnoreCase(modifier)) {
					searchType = ClientUIConstants.SEARCHBY_HOME_PHONE;
				} else if (ClientUIConstants.SEARCH_MODIFIER_WORK_PHONE
						.equalsIgnoreCase(modifier)) {
					// Need to change the search by types to search on different
					// phone numbers
					searchType = ClientUIConstants.SEARCHBY_HOME_PHONE;
				} else if (ClientUIConstants.SEARCH_MODIFIER_IVR_NUMBER
						.equalsIgnoreCase(modifier)) {
					searchType = ClientUIConstants.SEARCHBY_IVR_NUMBER;
				} else if (ClientUIConstants.SEARCH_MODIFIER_PAYABLE_ITEM_ID
						.equalsIgnoreCase(modifier)) {
					searchType = ClientUIConstants.SEARCHBY_PAYABLE_ITEM_ID;
				}
			} else {
				// No search modifier so interpret the search value.
				// If the search value starts with a digit then assume it is a
				// client number.
				if (Character.isDigit(searchValue.charAt(0))) {
					searchType = ClientUIConstants.SEARCHBY_CLIENT_NUMBER;
				}
				// If the search value starts with a letter then assume it is a
				// client surname.
				else {
					searchType = ClientUIConstants.SEARCHBY_CLIENT_SURNAME;
				}
			}
		}

		switch (searchType) {
		case ClientUIConstants.SEARCHBY_CLIENT_NUMBER:
			try {
				// The search value should be a valid integer
				Integer clientNumber = Integer.valueOf(searchValue);

				client = getClient(clientNumber); // clientHome.findByPrimaryKey(clientNumber);
				if (client != null) {
					LogUtil.debug(this.getClass(), "adding client to list "
							+ client);
					findList = new ArrayList();
					findList.add(client);
				}
			} catch (NumberFormatException nfe) {
				throw new ValidationException("The client number '"
						+ searchValue + "' is not valid.");
			}
			break;
		case ClientUIConstants.SEARCHBY_CLIENT_SURNAME:

			// no comma
			final String DEL = ",";
			if (searchValue.indexOf(DEL) == -1) {
				findList = findClientsBySurname(searchValue);
			} else {
				String parts[] = searchValue.split(DEL);
				if (parts.length == 2) {
					// LogUtil.debug(this.getClass(),"part 0 = "+parts[0]);
					// LogUtil.debug(this.getClass(),"part 1 = "+parts[1]);
					findList = findClientsByNameAndGivenName(parts[1], parts[0]);
				} else {
					throw new ValidationException(
							"Only one comma may be specified in the search term.");
				}

			}
			break;
		case ClientUIConstants.SEARCHBY_HOME_PHONE:
			findList = findClientsByHomePhone(searchValue);
			break;
		case ClientUIConstants.SEARCHBY_ADDRESS:
			findList = findClientsByAddress(searchValue);
			break;
		case ClientUIConstants.SEARCHBY_IVR_NUMBER:
			client = findClientByIVRNumber(searchValue);
			if (client != null) {
				findList = new ArrayList();
				findList.add(client);
			}
			break;
		case ClientUIConstants.SEARCHBY_PAYABLE_ITEM_ID:
			try {
				client = findClientByPayableItemID(new Integer(searchValue));
				if (client != null) {
					findList = new ArrayList();
					findList.add(client);
				}
			} catch (FinderException fe) {
				// Ignore. An empty list signals this to the calling function.
			}
			break;

		case ClientUIConstants.SEARCHBY_MOBILE_PHONE:
			findList = findClientsByMobilePhone(searchValue);
			break;

		case ClientUIConstants.SEARCHBY_MEMBERSHIP_ID:
			findList = findClientsByMembershipId(searchValue);
			break;

		case ClientUIConstants.SEARCHBY_MEMBERSHIP_CARD_NUMBER:
			client = getClientByMembershipCardNumber(searchValue);
			if (client != null) {
				findList = new ArrayList();
				findList.add(client);
			}
			break;

		case ClientUIConstants.SEARCHBY_GROUP_NAME:

			try {
				findList = findClientsByMembershipGroupName(searchValue);
			} catch (FinderException fe) {
				// Ignore. An empty list signals this to the calling function.
			}
			break;

		case ClientUIConstants.SEARCHBY_DOCUMENT_REF:

			try {
				findList = findClientsByDocumentReference(new Integer(
						searchValue));
			} catch (FinderException fe) {
				// Ignore. An empty list signals this to the calling function.
			}
			break;

		case ClientUIConstants.SEARCHBY_EMAIL_ADDRESS:
			findList = findClientsByEmailAddress(searchValue);
			break;

		case ClientUIConstants.SEARCHBY_VIN:
			findList = findClientsByVin(searchValue);
			break;

		case ClientUIConstants.SEARCHBY_REGO:
			findList = findClientsByRego(searchValue);
			break;

		default:
			throw new RemoteException(
					"Internal error! Not a known search type: '" + searchType
							+ "'");
		}

		LogUtil.debug(this.getClass(), "b4 return " + findList);

		return findList;
	}

	public ClientVO findClientByPayableItemID(Integer payableItemID)
			throws RemoteException, FinderException {
		Connection connection = null;
		PreparedStatement statement = null;
		Integer clientNo = null;
		final String SQL_CLIENT_PAYABLE_ITEM = "SELECT clientno FROM PUB.pt_payableitem WHERE payableitemid = ?";
		try {

			if (payableItemID != null) {
				connection = dataSource.getConnection();

				statement = connection
						.prepareStatement(SQL_CLIENT_PAYABLE_ITEM);

				statement.setInt(1, payableItemID.intValue());
				ResultSet resultSet = statement.executeQuery();
				if (!resultSet.next()) {
					throw new FinderException(
							"Client does not existing for payable item id "
									+ payableItemID);
				} else {
					clientNo = new Integer(resultSet.getInt(1));
				}
			} else {
				throw new FinderException("Invalid payable item id Number "
						+ payableItemID);
			}
			return getClient(clientNo);
		} catch (SQLException e) {
			throw new RemoteException(SQL_CLIENT_PAYABLE_ITEM + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	public Collection findClientsByMembershipGroupName(String groupName)
			throws RemoteException, FinderException {
		Connection connection = null;
		ArrayList clientList = new ArrayList();
		PreparedStatement statement = null;
		Integer clientNo = null;
		final String SQL_CLIENT_GROUP_NAME = "SELECT mm.client_number FROM PUB.mem_membership mm, PUB.mem_membership_group mg, PUB.mem_group g WHERE mm.membership_id = mg.membership_id and mg.group_id = g.group_id and g.group_name = ?";
		try {

			if (groupName != null) {
				connection = dataSource.getConnection();
				statement = connection.prepareStatement(SQL_CLIENT_GROUP_NAME);

				statement.setString(1, groupName);
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()) {
					clientNo = new Integer(resultSet.getInt(1));
					clientList.add(getClient(clientNo));
				}
			} else {
				throw new FinderException("Invalid group name " + groupName);
			}
		} catch (SQLException e) {
			throw new RemoteException(SQL_CLIENT_GROUP_NAME + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return clientList;
	}

	private Collection<ClientVO> findClientsByMembershipId(String membershipId)
			throws RemoteException {
		LogUtil.debug(this.getClass(), "findClientsByMembershipId start");

		LogUtil.debug(this.getClass(), "membershipId=" + membershipId);

		ArrayList<ClientVO> clientList = new ArrayList();
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper
				.getMembershipMgrLocal();
		MembershipVO membership = membershipMgrLocal.getMembership(new Integer(
				membershipId));
		if (membership != null) {
			clientList.add(membership.getClient());
		}
		LogUtil.debug(this.getClass(), "findClientsByMembershipId end");
		return clientList;
	}

	private Collection<ClientVO> findClientsByVin(String vin)
			throws RemoteException {
		LogUtil.debug(this.getClass(), "findClientsByVin start");

		LogUtil.debug(this.getClass(), "vin=" + vin);

		ArrayList<ClientVO> clientList = new ArrayList();
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper
				.getMembershipMgrLocal();
		List<MembershipVO> membershipList = membershipMgrLocal
				.findMembershipByVin(vin, ProductVO.PRODUCT_CMO);
		if (membershipList != null && !membershipList.isEmpty()) {
			for (MembershipVO membership : membershipList) {
				clientList.add(membership.getClient());
			}
		}
		LogUtil.debug(this.getClass(), "findClientsByVin end");
		return clientList;
	}

	private Collection<ClientVO> findClientsByRego(String rego)
			throws RemoteException {
		LogUtil.debug(this.getClass(), "findClientsByRego start");

		LogUtil.debug(this.getClass(), "rego=" + rego);

		ArrayList<ClientVO> clientList = new ArrayList();
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper
				.getMembershipMgrLocal();
		List<MembershipVO> membershipList = membershipMgrLocal
				.findMembershipByRego(rego);
		if (membershipList != null && !membershipList.isEmpty()) {
			for (MembershipVO membership : membershipList) {
				clientList.add(membership.getClient());
			}
		}
		LogUtil.debug(this.getClass(), "findClientsByRego end");
		return clientList;
	}

	private Collection findClientsByDocumentReference(Integer documentReference)
			throws RemoteException, FinderException {
		Connection connection = null;
		ArrayList clientList = new ArrayList();
		PreparedStatement statement = null;
		Integer clientNo = null;
		final String SQL_CLIENT_DOCUMENT_REF = "SELECT mm.client_number FROM PUB.mem_membership mm, PUB.mem_document md WHERE mm.membership_id = md.membership_id and md.document_id = ?";
		try {

			if (documentReference != null) {
				connection = dataSource.getConnection();
				statement = connection
						.prepareStatement(SQL_CLIENT_DOCUMENT_REF);

				statement.setInt(1, documentReference.intValue());
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()) {
					clientNo = new Integer(resultSet.getInt(1));
					clientList.add(getClient(clientNo));
				}
			} else {
				throw new FinderException("Invalid document reference "
						+ documentReference);
			}
		} catch (SQLException e) {
			throw new RemoteException(SQL_CLIENT_DOCUMENT_REF + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return clientList;
	}

	/**
	 * Reset the client occupation cache.
	 * 
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void resetClientOccupationCache() throws RemoteException {
		ClientOccupationCache.getInstance().reset();
	}

	/**
	 * Reset the client title cache.
	 * 
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void resetClientTitleCache() throws RemoteException {
		ClientTitleCache.getInstance().reset();
	}

	/**
	 * Insert the history record for client as the client was AFTER the update.
	 * 
	 * @param client
	 *            Description of the Parameter
	 * @param userId
	 *            Description of the Parameter
	 * @param transReason
	 *            Description of the Parameter
	 * @param modifyDate
	 *            Description of the Parameter
	 * @param modifyTime
	 *            Description of the Parameter
	 * @param salesBranch
	 *            Description of the Parameter
	 * @param comments
	 *            Description of the Parameter
	 * @param subSystem
	 *            Description of the Parameter
	 * @exception RemoteException
	 *                Description of the Exception
	 * @exception ValidationException
	 *                Description of the Exception
	 */
	public void createClientHistory(ClientVO client, String logonId,
			String transactionReason, String salesBranch, String comments,
			String subSystem) throws GenericException {
		LogUtil.debug(this.getClass(), "createClientHistory start");
		if (client == null) {
			throw new GenericException(
					"Client must not be null when creating client history.");
		}

		DateTime modifyDateTime = new DateTime();

		// not updated
		Integer chClientNo = new Integer(0);

		ClientHistoryPK pk = new ClientHistoryPK();
		pk.setClientNumber(client.getClientNumber());
		pk.setModifyDateTime(modifyDateTime);

		ClientHistory clientHistory = new ClientHistory(pk, logonId,
				transactionReason, salesBranch, comments, subSystem,
				chClientNo, client);
		LogUtil.debug(this.getClass(), "clientHistory=" + clientHistory);
		ClientHistory old = getLastClientHistory(client.getClientNumber());
		LogUtil.debug(this.getClass(), "old=" + old);
		if (old != null) {
			LogUtil.debug(this.getClass(), "old exists");
			// existing transaction record
			if (transactionReason.equals(Client.HISTORY_CREATE)) {
				// don't expect to already any history - ignore
			} else if (transactionReason.equals(Client.HISTORY_UPDATE)) {
				if (old.getTransactionReason().equals(Client.HISTORY_CREATE)) {
					// don't update history
					clientHistory.setTransactionReason(Client.HISTORY_CURRENT);
				} else if (old.getTransactionReason().equals(
						Client.HISTORY_CURRENT))
					;
				{
					LogUtil.debug(this.getClass(), "update old transaction");
					old.setTransactionReason(Client.HISTORY_UPDATE);
					updateClientHistory(old);
					LogUtil.debug(this.getClass(), "update old transaction");
					// create the new one as current
					clientHistory.setTransactionReason(Client.HISTORY_CURRENT);
				}
			}
		}
		LogUtil.debug(this.getClass(), "save new transaction");
		hsession.save(clientHistory);
		LogUtil.debug(this.getClass(), "createClientHistory end");
	}

	/**
	 * Return a market type with the specified code. Assumes that only one
	 * record will match the code given.
	 */
	public MarketingType getMarketingType(String marketingTypeCode)
			throws RemoteException {
		MarketingType marketingType = null;
		Connection connection = null;
		PreparedStatement statement = null;
		String sql = "select marketing_type_id, marketing_type_desc from PUB.cl_marketing_type where marketing_type = ?";
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setString(1, marketingTypeCode);
			ResultSet rs = statement.executeQuery();

			if (rs.next()) {
				marketingType = new MarketingType();
				marketingType.setMarketingId(new Integer(rs.getInt(1)));
				marketingType.setMarketingType(marketingTypeCode);
				marketingType.setMarketingTypeDesc(rs.getString(2));
			}
		} catch (SQLException ex) {
			throw new SystemException(
					"Unable to get of marketing type with code="
							+ marketingTypeCode + ".", ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return marketingType;
	}

	/**
	 * Sets the branchNumber attribute of the ClientMgrBean object
	 * 
	 * @param cvo
	 *            The new branchNumber value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	private void setBranchNumber(ClientVO cvo) throws RemoteException {
		String postcode = cvo.getResidentialAddress().getPostcode();
		BranchVO branch = null;
		if (postcode != null) {
			// get branch for the postcode
			branch = this.getBranchForPostcode(new Integer(postcode));
		}

		// set the branch number before creating the record
		if (branch != null) {
			cvo.setBranchNumber(new Integer(branch.getBranchNumber()));
		}
	}

	public Hashtable getRelatedClients(ChangedAddress cAddress)
			throws RemoteException {
		Hashtable list = new Hashtable();
		Connection connection = null;
		// PreparedStatement statement = null;
		// Integer clNo;
		// if this is a group, get members
		// String sqlString = null;

		try {
			connection = dataSource.getConnection();
			// get individuals
			// same surname, same address
			this.addClientsToList(list, cAddress, connection);

			// now look for groups in Progress database
			ClientAdapter clAdapter = ClientFactory.getClientAdapter();
			String[] clientArray = clAdapter.getRelatedClients(cAddress
					.getClientNo());
			if (clientArray != null)
				addOtherClientsToList(list, cAddress.getClientNo(), clientArray);
			clAdapter.release();
			this.removeRedundantFollowUpClients(list, cAddress);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RemoteException("Error finding related clients for "
					+ cAddress.getClientNo(), ex);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
			}
		}
		return list;
	}

	public boolean isAskable(Integer clientNumber, String fieldName,
			String fieldValue) throws SystemException {
		ClientAdapter clAdapter = ClientFactory.getClientAdapter();
		return clAdapter.isAskable(clientNumber, fieldName, fieldValue);
	}

	/**
	 * check that any clients in the list are not already listed for follow up,
	 * or have already been followed up or were the originators of this follow
	 * up
	 * 
	 * @param list
	 *            Hashtable: the list of clients found to have a relationship
	 * @param cAddress
	 *            ChangedAddress: holds the details of the address which
	 *            triggered this process - holds the old address
	 */
	private void removeRedundantFollowUpClients(Hashtable list,
			ChangedAddress cAddress) throws Exception {
		Enumeration en = list.elements();
		// PersonVO person = null;
		ClientVO aClt = null;
		ClientMgr clMgr = ClientEJBHelper.getClientMgr();
		ClientVO thisClt = clMgr.getClient(cAddress.getClientNo());
		ClientVO cClient = null;
		boolean alreadyListed;
		while (en.hasMoreElements()) {
			aClt = (ClientVO) en.nextElement();
			alreadyListed = false;
			if (thisClt.getPostalAddress().equals(
					(AddressVO) aClt.getPostalAddress())
					&& thisClt.getResidentialAddress().equals(
							(AddressVO) aClt.getResidentialAddress())
					&& thisClt.getHomePhone().equals(aClt.getHomePhone())) {
				LogUtil.debug(this.getClass()
						+ ".removeRedundantFollowUpClients",
						aClt.getClientNumber() + " already has new address");
				alreadyListed = true;
			} else {
				ArrayList fList = new ArrayList();
				Criteria c = hsession.createCriteria(FollowUp.class);
				c.add(Restrictions.eq("followUpPk.subSystem", "client"));
				c.add(Restrictions.eq("followUpPk.systemNumber",
						aClt.getClientNumber()));
				fList = new ArrayList(c.list());

				for (int xx = 0; xx < fList.size(); xx++) {
					FollowUp af = (FollowUp) fList.get(xx);
					cClient = clMgr.getClient(af.getChangedClientNumber());
					if (sCompare(af.getNewPostalAddress(), cClient
							.getPostalAddress().getSingleLineAddress())
							&& sCompare(af.getNewStreetAddress(), cClient
									.getResidentialAddress()
									.getSingleLineAddress())
							&& sCompare(af.getNewHomePhone(),
									cClient.getHomePhone())
							&& (sCompare(af.getStatus(), "duplicate") || sCompare(
									af.getStatus(), "flag"))) {
						LogUtil.debug(this.getClass()
								+ ".removeRedundantFollowUpClients",
								aClt.getClientNumber() + " is already flagged");

						alreadyListed = true;
					}
				}
			}
			if (alreadyListed) {
				list.remove(aClt.getClientNumber());
			}
		}
	}

	private boolean sCompare(String left, String right) {
		if (left == null || right == null)
			return false;
		return left.trim().equalsIgnoreCase(right);
	}

	private void addOtherClientsToList(Hashtable list, Integer clientNo,
			String[] clientList) throws Exception {
		ClientVO client = null;
		Integer clNo = null;
		for (int xx = 0; xx < clientList.length; xx++) {
			if (!clientList[xx].equals("")) {
				clNo = new Integer(clientList[xx]);
				if (!list.containsKey(clNo) && !clientNo.equals(clNo)) {
					client = this.getClient(clNo);
					if (client == null) {
						/*
						 * this will only occur if there are clients in the
						 * Progress system which have no counterpart in the
						 * sqlserver database
						 */

						LogUtil.log(
								this.getClass(),
								"\n*****************************************************"
										+ "\n      Client not found for client number "
										+ clNo
										+ "\n      in ClientMgrBean.addOtherClientsToList "
										+ "\n******************************************************");
					} else {
						list.put(clNo, client);
					}
				}
			}

		}
	}

	private void addClientsToList(Hashtable list, ChangedAddress cAddress,
			Connection con) throws Exception {
		PreparedStatement statement = null;
		Integer clNo;

		String sqlString = "select clt2.\"client-no\" from \"cl-master\" clt2"
				+ " where clt2.\"surname\" = ? "
				+ " and clt2.\"resi-stsubid\" =  ?"
				+ " and clt2.\"resi-street-char\" = ?"
				+ " and clt2.\"client-no\" <> ?";
		statement = con.prepareStatement(sqlString);
		statement.setString(1, cAddress.getSurname());
		statement.setInt(2, cAddress.getResiStsubid().intValue());
		statement.setString(3, cAddress.getResiStreetChar());
		statement.setInt(4, cAddress.getClientNo().intValue());
		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			clNo = new Integer(rs.getInt(1));
			if (!list.containsKey(clNo)) {
				ClientVO client = this.getClient(clNo);
				list.put(clNo, client);
			}
		}
		rs.close();
		statement.close();
	}

	public void createFollowUp(FollowUp fu) throws RemoteException {
		hsession.save(fu);
	}

	public void setFollowUpStatus(FollowUpPK fupk, String status)
			throws RemoteException {
		FollowUp fu = (FollowUp) hsession.get(FollowUp.class, fupk);
		fu.setStatus(status);
		hsession.update(fu);
	}

	public void setFollowUpResult(FollowUpPK fupk, String userId,
			DateTime fuDate, String result) throws RemoteException {
		FollowUp fu = (FollowUp) hsession.get(FollowUp.class, fupk);
		fu.setResult(result);
		fu.setFollowUpId(userId);
		fu.setFollowUpDate(fuDate);
		hsession.update(fu);
	}

	public void setFollowUpResult(FollowUp fu) throws RemoteException {
		FollowUp ff = (FollowUp) hsession.get(FollowUp.class,
				fu.getFollowUpPk());
		ff.setResult(fu.getResult());
		ff.setFollowUpDate(fu.getFollowUpDate());
		ff.setFollowUpId(fu.getFollowUpId());
		hsession.update(fu);
	}

	public ArrayList getFollowUpList(String userId, DateTime fDate,
			DateTime tDate, String status, String slsbch, String result)
			throws RemoteException {
		ArrayList fList = new ArrayList();
		try {
			Criteria c = hsession.createCriteria(FollowUp.class);
			if (userId != null && !userId.trim().equalsIgnoreCase("All"))
				c.add(Restrictions.eq("createId", userId.trim()));
			if (!status.equalsIgnoreCase("All"))
				c.add(Restrictions.eq("status", status));
			if (!result.equalsIgnoreCase("All"))
				c.add(Restrictions.eq("result", result));
			if (fDate != null && tDate == null)
				c.add(Restrictions.ge("followUpPk.createDate", fDate));
			else if (fDate != null)
				c.add(Restrictions.between("followUpPk.createDate", fDate,
						tDate));
			if (slsbch != null && !slsbch.trim().equalsIgnoreCase("All"))
				c.add(Restrictions.eq("createSlsbch", slsbch.trim()));

			fList = new ArrayList(c.list());
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}
		return fList;
	}

	public Collection getMarketOptions(Integer clientNo) throws RemoteException {
		ArrayList mList = null;
		MarketOption mo = null;
		try {
			Criteria c = hsession.createCriteria(MarketOption.class);
			c.add(Restrictions.eq("marketOptionPk.clientNo", clientNo));
			c.add(Restrictions.isNull("deleteDateTime"));
			mList = new ArrayList(c.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Error extracting market options", ex);
		}
		// don't actually need this in this context, but it's a good idea to set
		// the 'unavaliable' flag.
		for (int xx = 0; xx < mList.size(); xx++) {
			mo = (MarketOption) mList.get(xx);
			mo.setUnavailable(new Boolean(true));
		}
		return mList;
	}

	/**
	 * setMarketOptions Saves marketing options as set by user to the database.
	 * Cycle through ArrayList of MarketOption. If it is in the database and is
	 * required do nothing. If it is in the database but not required, set
	 * delete date, deleteId If it is required but not in the database then
	 * create. If it is required and in the database, but marked as deleted,
	 * then create a new one If it is not in the database and is not required
	 * then do nothing.
	 * 
	 * @param clientNo
	 *            Integer
	 * @param options
	 *            ArrayList
	 * @param userId
	 *            String
	 * @throws RemoteException
	 */
	public void setMarketOptions(Integer clientNo, Collection options,
			String userId) throws RemoteException {
		MarketOption rOption = null;
		Criteria c = null;
		ArrayList dList = null;
		DateTime updt = new DateTime();
		MarketOption dOpt = null;
		for (Iterator<MarketOption> i = options.iterator(); i.hasNext();) {
			rOption = i.next();
			c = hsession.createCriteria(MarketOption.class);
			c.add(Restrictions.eq("marketOptionPk.clientNo", clientNo));
			c.add(Restrictions.eq("marketOptionPk.subSystem",
					rOption.getSubSystem()));
			c.add(Restrictions.eq("marketOptionPk.medium", rOption.getMedium()));
			c.add(Restrictions.isNull("deleteDateTime"));
			dList = new ArrayList(c.list());
			if (dList.size() > 0) // record exists - already excluded
			{
				if (rOption.isUnavailable()) {
					// do nothing, this is correct
				} else {
					// set deleteId = userId, and delete dateTime = new
					// DateTime();
					dOpt = (MarketOption) dList.get(0);
					dOpt.setDeleteDateTime(updt);
					dOpt.setDeleteId(userId);
					hsession.update(dOpt);
				}
			} else // there is no existing record
			{
				if (rOption.isUnavailable()) {
					// create a new record
					rOption.setCreateId(userId);
					rOption.setCreateDateTime(updt);
					hsession.save(rOption);
				}
			}
		}
	}

	/*
	 * Add client business to the hash table, which must be defined in calling
	 * procedure. This procedure may be called on multiple clients to accumulate
	 * a list of business for a series of related clients.
	 */
	public void getClientBusiness(Integer clientNo, Hashtable bus)
			throws RemoteException {
		ClientAdapter clAdapter = ClientFactory.getClientAdapter();
		ArrayList cltBusiness = clAdapter.getClientBusiness(clientNo);
		ClientBusiness sbus = null;
		for (int xx = 0; xx < cltBusiness.size(); xx++) {
			sbus = (ClientBusiness) cltBusiness.get(xx);
			String key = sbus.getBusinessType() + sbus.getBusinessNumber();
			if (!bus.containsKey(key))
				bus.put(key, sbus);
		}
	}

	public boolean isPasswordProtected(Integer clientNo) throws RemoteException {
		ClientAdapter clAdapter = ClientFactory.getClientAdapter();
		return clAdapter.isProtected(clientNo);
	}

	public ArrayList clientSearch(String surname, String initial,
			String streetNo, String street, String suburb,
			DateTime dateOfBirth, Boolean gender, String phoneNo,
			String emailAddress) throws RemoteException {
		final boolean EXACT = true;
		final boolean WILD = false;
		ArrayList cList = new ArrayList();
		String sql = "select distinct \"client-no\" from pub.\"cl-master\"";
		StringBuffer wClause = new StringBuffer();
		if (street != null && !street.equals("") || suburb != null
				&& !suburb.equals(""))
			sql = sql + ",pub.\"gn-stsub\"";
		if (surname.indexOf('*') > -1) {
			addPredicate(wClause, WILD, "surname", stripAsterisk(surname));
		} else
			addPredicate(wClause, EXACT, "surname", surname);
		addPredicate(wClause, WILD, "initials", initial);

		if (street != null && !street.equals("") || suburb != null
				&& !suburb.equals("")) {
			addPredicate(wClause, WILD, "street", street);
			addPredicate(wClause, WILD, "suburb", suburb);
			wClause.append(" and(\"gn-stsub\".stsubid = \"cl-master\".\"resi-stsubid\" "
					+ "     or \"gn-stsub\".stsubid = \"cl-master\".\"post-stsubid\") ");
		}
		if (streetNo != null && !streetNo.equals("")) {
			streetNo = streetNo.trim();
			addNumberPredicate(wClause, "resi-street-no", streetNo);
		}

		if (dateOfBirth != null) {
			this.addDatePredicate(wClause, "birth-date", dateOfBirth);
		}
		if (gender != null) {

			this.addNumberPredicate(wClause, "sex", gender.booleanValue() ? "1"
					: "0");
		}
		if (phoneNo != null) {
			// home or work or mobile
			String phoneFields[] = { "home-phone", "work-phone", "mobile-phone" };
			this.addPredicate(wClause, true, phoneFields, phoneNo);
		}
		if (emailAddress != null) {
			this.addPredicate(wClause, true, "email", emailAddress);
		}
		if (wClause.toString().equals(""))
			throw new SystemException("Selection criteria not provided");
		sql = sql + wClause.toString();
		LogUtil.debug(this.getClass(), "sql=" + sql);
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			ClientVO clt = null;
			Integer clientNo = null;
			int xx = 0;
			while (rs.next() && xx < 100) {
				xx++;
				clientNo = new Integer(rs.getInt(1));
				clt = this.getClient(clientNo);
				cList.add(clt);
			}
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (Exception ex) {
			}
		}
		return cList;
	}

	private void addNumberPredicate(StringBuffer wClause, String fName,
			String value) {
		if (value != null && !value.equals("")) {
			if (wClause.length() == 0)
				wClause.append(" where ");
			else
				wClause.append(" and ");
			wClause.append("\"" + fName + "\"  = " + value);
		}
	}

	private void addPredicate(StringBuffer wClause, boolean exact,
			String fName, String value) {
		if (value == null)
			return;
		value = value.trim();
		if (value != null && !value.equals("")) {
			if (wClause.length() == 0) {
				wClause.append(" where ");
			} else {
				wClause.append(" and ");
			}
			if (exact) {
				wClause.append("\"" + fName + "\"  = '" + value + "'");
			} else {
				wClause.append("\"" + fName + "\" like '%" + value + "%'");
			}
		}
	}

	private void addPredicate(StringBuffer wClause, boolean exact,
			String[] fName, String value) {
		if (value == null)
			return;
		value = value.trim();
		if (value != null && !value.equals("")) {

			for (int i = 0; i < fName.length; i++) {

				if (wClause.length() == 0) {
					wClause.append(" where (");
				} else {
					if (i == 0) {
						wClause.append(" and (");
					} else {
						wClause.append(" or ");
					}

				}

				if (exact) {
					wClause.append("\"" + fName[i] + "\"  = '" + value + "'");
				} else {
					wClause.append("\"" + fName[i] + "\" like '%" + value
							+ "%'");
				}

				if (i == (fName.length - 1)) {
					wClause.append(") ");
				}

			}
		}
	}

	private void addDatePredicate(StringBuffer wClause, String fName,
			DateTime value) {
		String dString = "" + value.getDateYear() + "-"
				+ (value.getMonthOfYear() + 1) + "-" + value.getDayOfMonth()
				+ " 00:00:00";
		if (value == null)
			return;
		if (wClause.length() == 0)
			wClause.append(" where ");
		else
			wClause.append(" and ");
		wClause.append("\"" + fName + "\" = CONVERT(DATETIME,'" + dString
				+ "',102)"); // TODO
		// not
		// portable
		// SQL
	}

	private String stripAsterisk(String s) {
		String out = "";
		char ch;
		for (int xx = 0; xx < s.length(); xx++) {
			ch = s.charAt(xx);
			if (ch != '*')
				out += ch;
		}
		return out;
	}

	/**
	 * 
	 * @param clientNo
	 * @return
	 * @throws RemoteException
	 */
	public ArrayList<ClientVO> getClientGroup(Integer clientNo)
			throws RemoteException {
		ClientAdapter cltAdapter = null;
		ArrayList grpList = null;
		// ResultSet rs = null;
		// ClientVO thisClt = null;
		try {
			cltAdapter = ClientFactory.getClientAdapter();
			grpList = cltAdapter.getClientGroup(clientNo);
		} catch (Exception ex) {
			throw new SystemException(ex);
		}
		return grpList;
	}

	/**
	 * Returns the number of client changes between the start and end times.
	 * This will be used by an overnight process to ensure all reported changes
	 * have made the transition to the MRM.
	 * 
	 * @param startDate
	 * @param endDate
	 * @param excludedSystems
	 * @return
	 * @throws SystemException
	 */
	public Long getClientChangesCount(DateTime startDate, DateTime endDate,
			Collection<String> excludedSystems) throws RemoteException {

		Query query = em
				.createQuery("select count(clientNumber) from ClientVO where lastUpdate >= :start and lastUpdate <= :end and (subSystem is null or subSystem not in (:excludedSystems))");
		query.setParameter("start", startDate);
		query.setParameter("end", endDate);
		query.setParameter("excludedSystems", excludedSystems);
		Long count = (Long) query.getSingleResult();

		return count;
	}

	/**
	 * Returns boolean; true if changes (creates/updates) exist between the
	 * entered dates, false otherwise
	 * 
	 * @param startDate
	 * @param endDate
	 * @param excludedSystems
	 * @return
	 * @throws SystemException
	 */
	public Boolean isClientChangesExist(DateTime startDate, DateTime endDate,
			Collection<String> excludedSystems) throws RemoteException {
		return (getClientChangesCount(startDate, endDate, excludedSystems) > 0);
	}

	/**
	 * Return the identifiers of all clients changed during the date range. Used
	 * in an overnight process in conjunction with the GetClientChangesCount web
	 * method. If a missing client is found, force a sync on that client.
	 * 
	 * @param startDate
	 * @param endDate
	 * @param excludedSystems
	 * @return
	 * @throws SystemException
	 */
	public Collection<Integer> getClientChangesIdentifiers(DateTime startDate,
			DateTime endDate, Collection<String> excludedSystems)
			throws RemoteException {

		Query query = em
				.createQuery("select clientNumber from ClientVO where lastUpdate >= :start and lastUpdate <= :end and (subSystem is null or subSystem not in (:excludedSystems))");
		query.setParameter("start", startDate);
		query.setParameter("end", endDate);
		query.setParameter("excludedSystems", excludedSystems);
		Collection<Integer> idList = query.getResultList();

		return idList;
	}

	/**
	 * Returns array of Client objects that have been created or modified
	 * between the start and end dates (inclusive). Will not return rows that
	 * were most recently updated by the MRM transactional sync component.
	 * 
	 * @param startDate
	 * @param endDate
	 * @param excludedSystems
	 * @return
	 */
	public Collection<ClientVO> getClientChanges(DateTime startDate,
			DateTime endDate, Collection<String> excludedSystems)
			throws RemoteException {
		Query query = em
				.createQuery("from ClientVO where lastUpdate >= :start and lastUpdate <= :end and (subSystem is null or subSystem not in (:excludedSystems))");
		query.setParameter("start", startDate);
		query.setParameter("end", endDate);
		query.setParameter("excludedSystems", excludedSystems);
		Collection<ClientVO> clientList = query.getResultList();
		
		// Handle the title MX for client changes
		Iterator clientListIterator = clientList.iterator();
		while (clientListIterator.hasNext()) {
			ClientVO client = (ClientVO) clientListIterator.next();
			if (client.title.equals(ClientVO.TITLE_MX))
				client.gender = ClientVO.GENDER_UNSPECIFIED;
		}

		return clientList;
	}

	private static final String SQL_CMO_DUPLICATE_QUERY = "SELECT\n"
			+ "c.[client-no]\n"
			+ ",nonCmo.[client-no] AS masterClientNumber\n"
			+ ",c.[given-names]\n"
			+ ",nonCmo.[given-names] AS masterGivenNames\n"
			+ ",c.surname\n"
			+ ",c.[resi-street-char]\n"
			+ ",nonCmo.[resi-street-char] AS masterResiStreetNo\n"
			+ ",s.street\n"
			+ ",s.suburb\n"
			+ ",c.[birth-date]\n"
			+ ",m.product_code\n"
			+ ",m.profile_code\n"
			+ "FROM pub.[cl-master] c\n"
			+ "INNER JOIN [PUB].[mem_membership] m ON m.client_number = c.[client-no]\n"
			+ "INNER JOIN pub.[gn-stsub] s ON c.[resi-stsubid] = s.stsubid\n"
			+ "INNER JOIN\n"
			+ "(\n"
			+ "	SELECT\n"
			+ "	c2.[client-no]\n"
			+ "	,c2.[given-names]\n"
			+ "	,c2.surname\n"
			+ "	,c2.[resi-stsubid]\n"
			+ "	,c2.[resi-street-char]\n"
			+ "	,s2.street\n"
			+ "	,s2.suburb\n"
			+ "	,c2.[birth-date]\n"
			+ "	,c2.sex\n"
			+ "	,m2.product_code\n"
			+ "	,m2.profile_code\n"
			+ "	FROM pub.[cl-master] c2\n"
			+ "	INNER JOIN [PUB].[mem_membership] m2 ON client_number = c2.[client-no]\n"
			+ "	INNER JOIN pub.[gn-stsub] s2 ON c2.[resi-stsubid] = s2.stsubid\n"
			+ "	WHERE m2.product_code <> '"
			+ ProductVO.PRODUCT_CMO
			+ "'\n"
			+ "	AND c2.[client-status] <> '"
			+ Client.STATUS_DUPLICATE
			+ "'\n"
			+ ") AS nonCmo\n"
			+ "ON c.[resi-stsubid] = nonCmo.[resi-stsubid]\n"
			+ "AND c.surname = nonCmo.surname\n"
			+ "AND DATEDIFF(d, 0, c.[birth-date]) = DATEDIFF(d, 0, nonCmo.[birth-date])\n"
			+ "AND c.sex = nonCmo.sex\n"
			+ "WHERE m.product_code = '"
			+ ProductVO.PRODUCT_CMO
			+ "'\n"
			+ "AND c.[last-update] >= DATEADD(DAY, -1, GETDATE())\n"
			+ "GROUP BY\n"
			+ "c.[client-no]\n"
			+ ",c.[given-names]\n"
			+ ",c.surname\n"
			+ ",c.[birth-date]\n"
			+ ",c.[resi-street-char]\n"
			+ ",s.street\n"
			+ ",s.suburb\n"
			+ ",m.product_code\n"
			+ ",m.profile_code\n"
			+ ",nonCmo.[client-no]\n"
			+ ",nonCmo.[given-names]\n" + ",nonCmo.[resi-street-char]\n";

	public List<Map<String, String>> getCmoDuplicateSuggestions()
			throws SystemException {
		Connection connection = null;
		PreparedStatement statement = null;
		List<Map<String, String>> results = new ArrayList<Map<String, String>>();
		Map<String, String> record;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_CMO_DUPLICATE_QUERY);
			ResultSet rs = statement.executeQuery();

			String clientNo;
			String masterClientNo;
			String givenNames;
			String masterGivenNames;
			String surname;
			String resiStreetChar;
			String masterResiStreetChar;
			String street;
			String suburb;
			String birthDate;
			String productCode;
			String profileCode;

			while (rs.next()) {
				clientNo = rs.getString(1);
				masterClientNo = rs.getString(2);
				givenNames = rs.getString(3);
				masterGivenNames = rs.getString(4);
				surname = rs.getString(5);
				resiStreetChar = rs.getString(6);
				masterResiStreetChar = rs.getString(7);
				street = rs.getString(8);
				suburb = rs.getString(9);
				birthDate = rs.getString(10);
				productCode = rs.getString(11);
				profileCode = rs.getString(12);

				record = new HashMap<String, String>();
				record.put("clientNo", clientNo);
				record.put("masterClientNo", masterClientNo);
				record.put("givenNames", givenNames);
				record.put("masterGivenNames", masterGivenNames);
				record.put("surname", surname);
				record.put("resiStreetChar", resiStreetChar);
				record.put("masterResiStreetChar", masterResiStreetChar);
				record.put("street", street);
				record.put("suburb", suburb);
				record.put("birthDate", birthDate);
				record.put("productCode", productCode);
				record.put("profileCode", profileCode);

				results.add(record);
			}
			rs.close();
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}

	private static final String SQL_FIND_CLIENT_WITHOUT_ROADSIDE = "SELECT [client-no]\n"
			+ "FROM [PUB].[cl-master]\n"
			+ "WHERE\n"
			+ "[resi-stsubid] = ?\n"
			+ "AND surname = ?\n"
			+ "AND [given-names] LIKE ?\n"
			+ "AND [birth-date] = ?\n"
			+ "AND sex = ?\n"
			+ "AND [client-status] = ?\n"
			+ "AND [client-no] NOT IN (SELECT client_number FROM pub.mem_membership)";

	/**
	 * Search for clients matching supplied client that do not currently have
	 * Roadside.
	 * 
	 * @param clientVO
	 * @return
	 * @throws SystemException
	 */
	public Collection<ClientVO> findClientsWithoutRoadside(ClientVO clientVO)
			throws SystemException {
		Connection connection = null;
		PreparedStatement statement = null;
		Collection<ClientVO> results = new ArrayList<ClientVO>();

		LogUtil.debug(getClass(),
				"Matching: \nresiStSubId: " + clientVO.getResiStsubId()
						+ "\nsurname: " + clientVO.getSurname()
						+ "\ngivenNames: " + clientVO.getGivenNames()
						+ "\nbirthDate: " + clientVO.getBirthDate().toSQLDate()
						+ "\nsex: " + clientVO.getSex());

		try {
			connection = dataSource.getConnection();
			statement = connection
					.prepareStatement(SQL_FIND_CLIENT_WITHOUT_ROADSIDE);

			statement.setInt(1, clientVO.getResiStsubId());
			statement.setString(2, clientVO.getSurname());
			statement.setString(3, clientVO.getGivenNames() + "%");
			statement.setDate(4, clientVO.getBirthDate().toSQLDate());
			statement.setBoolean(5, clientVO.getSex());
			statement.setString(6, Client.STATUS_ACTIVE);

			ResultSet rs = statement.executeQuery();

			Integer clientNo;

			while (rs.next()) {
				clientNo = rs.getInt(1);
				results.add(this.getClient(clientNo));
			}
			rs.close();
		} catch (Exception ex) {
			throw new SystemException(ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}
}
