package com.ract.client;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.client.notifier.ClientChangeNotificationEvent;
import com.ract.common.AddressFactory;
import com.ract.common.AddressVO;
import com.ract.common.BranchVO;
import com.ract.common.ClassWriter;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.MemoVO;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.finance.Debtor;
import com.ract.payment.finance.FinanceMgr;
import com.ract.util.AddressUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;
import com.ract.vehicle.VehicleVO;

/**
 * 
 * @hibernate.class table="[cl-master]" lazy="false"
 * 
 * @hibernate.discriminator type="string" formula=
 *                          "case when ([given-names] is null or rtrim([given-names]) = '') AND surname is not null and  ([client-title] is null or rtrim([client-title]) = '' or rtrim([client-title]) in ('.','THE')) then 'ORGANISATION'  else 'PERSON' end"
 * 
 *                          Implements common attributes and operations used by
 *                          all client types.
 */

public class ClientVO implements java.io.Serializable, Writable, Cloneable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -4588494319512890492L;

    @Override
    public Object clone() throws CloneNotSupportedException {
	// TODO Auto-generated method stub
	return super.clone();
    }

    /**
     * Client types
     */
    public static String CLIENT_TYPE_PERSON = "Person";

    public static String CLIENT_TYPE_ORGANISATION = "Organisation";

    /**
     * Client modes
     */
    public final static String WRITABLE_CLASSNAME = "Client";

    public final static String GENDER_MALE = "Male";

    public final static String GENDER_FEMALE = "Female";

    public final static String GENDER_UNSPECIFIED = "unspecified";

    public final static String TITLE_MX = "MX";

    public final static String GROUP_NAME_DELIMITER = "&";

    public final static String MARITAL_STATUS_SINGLE = "Single";

    public final static String FIELD_STATUS_TO_FOLLOW = "To Follow";

    public final static String FIELD_STATUS_REFUSED = "Refused";

    public final static String FIELD_STATUS_NOT_APPLICABLE = "Not Applicable"; // only
									       // track
									       // this
									       // status

    public final static String FIELD_NAME_HOME_PHONE = "homePhone";

    public final static String FIELD_NAME_WORK_PHONE = "workPhone";

    public final static String FIELD_NAME_MOBILE_PHONE = "mobilePhone";

    public final static String FIELD_NAME_EMAIL = "emailAddress";

    public final static String FIELD_NAME_BIRTH_DATE = "birthDate";

    public final static String FIELD_NAME_TITLE = "title";

    public final static String FIELD_NAME_CLIENT_NUMBER = "clientNumber";

    public final static String FIELD_NAME_CREATED_BY = "madeID";

    public final static String FIELD_NAME_GROUP = "groupClient";

    public final static String FIELD_NAME_CREATE_DATE = "madeDate";

    public final static String FIELD_NAME_GIVEN_NAMES = "givenNames";

    public final static String FIELD_NAME_SURNAME = "surname";

    public final static String FIELD_NAME_MEMO = "memoNumber";

    public final static String FIELD_NAME_MASTER_CLIENT = "masterClientNumber";

    public final static String FIELD_NAME_SMS_ALLOWED = "smsAllowed";

    public final static String FIELD_NAME_STATUS = "status";

    public final static String FIELD_NAME_GENDER = "gender";

    public final static String FIELD_NAME_BRANCH = "branchNumber";

    public final static String FIELD_NAME_RESI_PROPERTY = "resiProperty";

    public final static String FIELD_NAME_RESI_PROPERTY_QUAL = "resiPropertyQual";

    public final static String FIELD_NAME_RESI_STREET = "resiStreet";

    public final static String FIELD_NAME_RESI_SUBURB = "resiSuburb";

    public final static String FIELD_NAME_RESI_STATE = "resiState";

    public final static String FIELD_NAME_RESI_POSTCODE = "resiPostcode";

    public final static String FIELD_NAME_POST_PROPERTY = "postProperty";

    public final static String FIELD_NAME_POST_PROPERTY_QUAL = "postPropertyQual";

    public final static String FIELD_NAME_POST_STREET = "postStreet";

    public final static String FIELD_NAME_POST_SUBURB = "postSuburb";

    public final static String FIELD_NAME_POST_STATE = "postState";

    public final static String FIELD_NAME_POST_POSTCODE = "postPostcode";

    public String getSortableDisplayName() {
	return null;
    }

    public final static String MARITAL_STATUS_MARRIED = "Married/Defacto";
    private final Integer DEFAULT_SEQ_NO = new Integer(0);

    private String resiProperty;

    private String postProperty;

    private Integer resiStsubId;

    private Integer postStsubId;

    private Integer resiStreetNumber;

    private String resiStreetChar;

    private String resiDpid;

    private Integer postStreetNumber;

    private String postStreetChar;

    private String postDpid;
    private String postAusbar;
    private String postLatitude;
    private String postLongitude;
    private String postGnaf;

    private String resiLatitude;
    private String resiLongitude;
    private String resiGnaf;
    private String resiAusbar;

    // private String debtorCode;

    private Boolean smsAllowed;
    private String phoneQuestion;
    private String phonePassword;

    protected Collection vehicleList = null;

    /**
     * The unique identifier of the client. This is used for display and as a
     * business reference to the client so it is a number and not an ID (which
     * is internal to the system).
     */
    protected Integer clientNumber;

    /**
     * The type of client. e.g. "Person", "Organisation"
     */
    protected String clientType;

    /**
     * The status of the client. e.g. "Active", "Inactive", "Deceased".
     */
    protected String status;

    /**
     * A pre munged form of the clients name suitable for printing on envelopes.
     */
    protected String postalName;

    /**
     * A pre munged form of the clients name suitable for printing on letters.
     */
    protected String salutation;

    private String birthDateString;

    private String subSystem;

    /**
     * @hibernate.property
     * @return
     */
    public String getBirthDateString() {
	return birthDateString;
    }

    public void setBirthDateString(String birthDateString) {
	this.birthDateString = birthDateString;
    }

    protected Collection transactionList;

    /**
     * The postal address of the client.
     */
    protected PostalAddressVO postalAddress;

    protected ResidentialAddressVO residentialAddress;

    protected String madeID;

    protected DateTime madeDate;

    protected Boolean market;

    /**
     * The source system that was responsible for creating/changing the client
     * data. Used to signal a client data change initiated by the Progress
     * client system so that the new client system does not send the same data
     * back to progress.
     */
    protected SourceSystem sourceSystem = SourceSystem.CLIENT;

    /**
     * Set the mode of the implementation class at instantiation.
     * 
     * //TODO consolidate XML definition between Progress and internal sources.
     * 
     * @param mode
     */
    // public ClientVO(String mode)
    // {
    // }

    public boolean isDebtor() {
	return getDebtor() != null;
    }

    public Debtor getDebtor() {
	Debtor debtor = null;
	try {
	    FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	    debtor = financeMgr.getDebtor(clientNumber);
	} catch (Exception ex) {
	    // unrecoverable
	    LogUtil.fatal(this.getClass(), "Unable to retrive debtor for client " + clientNumber);
	}
	return debtor;
    }

    public void setFromProgressClientData(Node clientNode, String action) throws ValidationException, RemoteException {
	LogUtil.debug(this.getClass(), "----------------------------------" + "\n  setFromProgressClientData " + action + "\n----------------------------------");
	LogUtil.debug(this.getClass(), "motorNewsDeliveryMethod=" + this.motorNewsDeliveryMethod);
	LogUtil.debug(this.getClass(), "motorNewsSendOption=" + this.motorNewsSendOption);

	NodeList nodeList = clientNode.getChildNodes();
	Node childNode = null;
	String nodeName = null;
	String nodeValue = null;
	for (int i = 0; i < nodeList.getLength(); i++) {
	    childNode = nodeList.item(i);
	    nodeName = childNode.getNodeName();
	    if (childNode.hasChildNodes()) {
		nodeValue = childNode.getFirstChild().getNodeValue();
	    } else {
		nodeValue = null;
	    }
	    if (nodeName != null && nodeName.length() > 0) {
		nodeValue = nodeValue == null ? nodeValue : nodeValue.trim();
		if ("ABN".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.ABN = new BigDecimal(nodeValue);
		    } else {
			this.ABN = null;
		    }
		}
		if ("MasterClientNo".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.masterClientNumber = new Integer(nodeValue);
		    } else {
			this.masterClientNumber = null;
		    }
		}

		else if ("AddressTitle".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.addressTitle = nodeValue;
		    } else {
			this.addressTitle = null;
		    }
		} else if ("BirthDate".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			try {
			    this.birthDate = new DateTime(nodeValue);
			} catch (java.text.ParseException pe) {
			    throw new SystemException("Failed to set progress client data birth date. ", pe);
			}
		    } else {
			this.birthDate = null;
		    }
		}

		else if ("BirthDateString".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.birthDateString = nodeValue;
		    } else {
			this.birthDateString = null;
		    }
		} else if ("BranchNo".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.branchNumber = new Integer(nodeValue);
		    } else {
			this.branchNumber = null;
		    }
		} else if ("ChildrenNo".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.numberOfDependants = new Integer(nodeValue);
		    } else {
			this.numberOfDependants = null;
		    }
		} else if ("ClientNo".equalsIgnoreCase(nodeName)) {
		    // Must have a client number
		    this.clientNumber = new Integer(nodeValue);
		} else if ("ClientStatus".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.status = nodeValue;
		    } else {
			this.status = null;
		    }
		} else if ("ClientTitle".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.title = nodeValue;
		    } else {
			this.title = "";
		    }
		} else if ("ContactPersonName".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.contactPersonName = nodeValue;
		    } else {
			this.contactPersonName = null;
		    }
		} else if ("ContactPersonPhone".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.contactPersonPhone = nodeValue;
		    } else {
			this.contactPersonPhone = null;
		    }
		}

		else if ("DriversLicence".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.driversLicenseNumber = nodeValue;
		    } else {
			this.driversLicenseNumber = "";
		    }
		} else if ("Email".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.emailAddress = nodeValue;
		    } else {
			this.emailAddress = "";
		    }
		} else if ("GivenNames".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.givenNames = nodeValue;
		    } else {
			this.givenNames = "";
		    }
		} else if ("GroupType".equalsIgnoreCase(nodeName)) {
		    if ("Group".equalsIgnoreCase(nodeValue)) {
			this.groupClient = true;
		    } else if ("Individual".equalsIgnoreCase(nodeValue)) {
			this.groupClient = false;
		    } else {
			this.groupClient = false;
		    }
		} else if ("HomePhone".equalsIgnoreCase(nodeName)) {
		    LogUtil.log(this.getClass(), "hp = " + nodeName + " " + nodeValue);
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.homePhone = nodeValue;
		    } else {
			this.homePhone = "";
		    }
		} else if ("Initials".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.initials = nodeValue;
		    } else {
			this.initials = "";
		    }
		} else if ("LastSeqUsed".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.lastSeqUsed = new Integer(nodeValue);
		    } else {
			this.lastSeqUsed = null;
		    }
		} else if ("LastUpdate".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			try {
			    // apply current time to supplied date.
			    DateTime lastUpdateDate = new DateTime(nodeValue + " " + DateUtil.formatDate(Calendar.getInstance().getTime(), "HH:mm:ss"));
			    this.lastUpdate = lastUpdateDate;
			} catch (java.text.ParseException pe) {
			    throw new SystemException("Failed to set progress client last update date. ", pe);
			}
		    } else {
			this.lastUpdate = null;
		    }
		} else if ("MadeDate".equalsIgnoreCase(nodeName)) {
		    // Must have a create date and time
		    if (nodeValue != null && nodeValue.length() > 0) {
			try {
			    this.madeDate = new DateTime(nodeValue);
			} catch (java.text.ParseException pe) {
			    throw new SystemException("Failed to set progress client data made date. ", pe);
			}
		    } else {
			this.madeDate = null;
		    }
		} else if ("MadeID".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.madeID = nodeValue;
		    } else {
			this.madeID = "";
		    }
		} else if ("MaritalStatus".equalsIgnoreCase(nodeName)) {
		    if ("Married".equalsIgnoreCase(nodeValue)) {
			this.maritalStatus = new Boolean(true);
		    } else if ("Single".equalsIgnoreCase(nodeValue)) {
			this.maritalStatus = new Boolean(false);
		    } else {
			this.maritalStatus = null;
		    }
		} else if ("Market".equalsIgnoreCase(nodeName)) {
		    if ("Yes".equalsIgnoreCase(nodeValue)) {
			this.market = true;
		    } else if ("No".equalsIgnoreCase(nodeValue)) {
			this.market = false;
		    } else {
			this.market = false;
		    }
		} else if ("Memo".equalsIgnoreCase(nodeName)) {
		    if (childNode.getNodeType() == Node.ELEMENT_NODE) {
			Element memoElement = (Element) childNode;
			Integer memoID = null;
			String memoText = null;
			NodeList memoNoList = memoElement.getElementsByTagName("MemoNo");
			if (memoNoList != null) {
			    // Assume only one "MemoNo" tag.
			    if (memoNoList.item(0).hasChildNodes()) {
				memoID = new Integer(memoNoList.item(0).getFirstChild().getNodeValue());
			    }
			}
			NodeList memoTextList = memoElement.getElementsByTagName("Text");
			if (memoTextList != null) {
			    // Assume only one "Text" tag.
			    if (memoTextList.item(0).hasChildNodes()) {
				memoText = memoTextList.item(0).getFirstChild().getNodeValue();
			    }
			}
			if (memoID != null) {
			    // Update the memo table
			    if (memoText != null && memoText.length() > 0) {
				// Insert or update the memo text record
				setMemoText(memoID, memoText);
			    } else {
				// Delete the memo text record
				deleteMemoText(memoID);
				memoID = null;
			    }
			}
			// Set the memo number of the client
			this.memoNumber = memoID;
		    }
		} else if ("MobilePhone".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.mobilePhone = nodeValue;
		    } else {
			this.mobilePhone = null;
		    }
		}
		// The motor news send option has been renamed.
		// Need both to handle old XML files.
		// not sent in XML file as not maintainted in Progress client
		// database

		else if ("OccupationID".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.occupationID = new Integer(nodeValue);
		    } else {
			this.occupationID = null;
		    }
		} else if ("ResiProperty".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiProperty = nodeValue;
		    } else {
			this.resiProperty = "";
		    }
		} else if ("ResiStreetChar".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiStreetChar = nodeValue;
		    } else {
			this.resiStreetChar = null;
		    }
		} else if ("ResiStreetNo".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiStreetNumber = new Integer(nodeValue);
		    } else {
			this.resiStreetNumber = null;
		    }
		} else if ("ResiSTSubID".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiStsubId = new Integer(nodeValue);
		    } else {
			this.resiStsubId = null;
		    }
		} else if ("ResiDpid".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiDpid = nodeValue;
		    } else {
			this.resiDpid = "";
		    }
		} else if ("ResiLatitude".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiLatitude = nodeValue;
		    } else {
			this.resiLatitude = "";
		    }
		} else if ("ResiLongitude".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiLongitude = nodeValue;
		    } else {
			this.resiLongitude = "";
		    }
		} else if ("ResiGnaf".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiGnaf = nodeValue;
		    } else {
			this.resiGnaf = "";
		    }
		} else if ("ResiAusbar".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.resiAusbar = nodeValue;
		    } else {
			this.resiAusbar = "";
		    }
		} else if ("PostalName".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postalName = nodeValue;
		    } else {
			this.postalName = null;
		    }
		} else if ("PostProperty".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postProperty = nodeValue;
		    } else {
			this.postProperty = "";
		    }
		} else if ("PostStreetChar".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postStreetChar = nodeValue;
		    } else {
			this.postStreetChar = null;
		    }
		} else if ("PostStreetNo".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postStreetNumber = new Integer(nodeValue);
		    } else {
			this.postStreetNumber = null;
		    }
		} else if ("PostStSubID".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postStsubId = new Integer(nodeValue);
		    } else {
			this.postStsubId = null;
		    }
		} else if ("PostDpid".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postDpid = nodeValue;
		    } else {
			this.postDpid = "";
		    }
		} else if ("PostAusbar".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postAusbar = nodeValue;
		    } else {
			this.postAusbar = "";
		    }
		} else if ("PostLatitude".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postLatitude = nodeValue;
		    } else {
			this.postLatitude = "";
		    }
		} else if ("PostLongitude".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postLongitude = nodeValue;
		    } else {
			this.postLongitude = "";
		    }
		} else if ("PostGnaf".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.postGnaf = nodeValue;
		    } else {
			this.postGnaf = "";
		    }
		} else if ("Sex".equalsIgnoreCase(nodeName)) {
		    if ("Male".equalsIgnoreCase(nodeValue)) {
			this.sex = new Boolean(true);
		    } else if ("Female".equalsIgnoreCase(nodeValue)) {
			this.sex = new Boolean(false);
		    } else {
			this.sex = null;
		    }
		} else if ("Surname".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.surname = nodeValue;
		    } else {
			this.surname = "";
		    }
		} else if ("WorkPhone".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.workPhone = nodeValue;
		    } else {
			this.workPhone = null;
		    }
		} else if ("SMSallowed".equalsIgnoreCase(nodeName)) {
		    if ("Yes".equalsIgnoreCase(nodeValue)) {
			this.smsAllowed = true;
		    } else if ("No".equalsIgnoreCase(nodeValue)) {
			this.smsAllowed = false;
		    } else {
			this.smsAllowed = false;
		    }
		} else if ("PhoneQuestion".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.phoneQuestion = nodeValue;
		    } else {
			this.phoneQuestion = null;
		    }
		} else if ("PhonePassword".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.phonePassword = nodeValue;
		    } else {
			this.phonePassword = null;
		    }
		}
	    }
	}

	// default motor news attributes as they are never sent in the XML and
	// are never initialised only
	// if the record is being created
	if (action.equals(ClientChangeNotificationEvent.ACTION_CREATE)) {
	    this.motorNewsDeliveryMethod = Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL;
	    this.motorNewsSendOption = Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL;
	}

	this.subSystem = SourceSystem.PROGRESS_CLIENT.getAbbreviation();

	LogUtil.debug(this.getClass(), "motorNewsDeliveryMethod=" + this.motorNewsDeliveryMethod);
	LogUtil.debug(this.getClass(), "motorNewsSendOption=" + this.motorNewsSendOption);

	LogUtil.log(this.getClass(), "updating from node.");

    }

    /**
     * Set the memo text for the client. Update it if the given ID exists
     * otherwise create it.
     */
    private void setMemoText(Integer memoID, String memoText) throws RemoteException {
	CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
	MemoVO memo = commonMgrLocal.getMemo(memoID);
	if (memo == null) {
	    memo = new MemoVO(memoID, memoText);
	    commonMgrLocal.createMemo(memo);
	} else {
	    memo.setMemoLine(memoText);
	    commonMgrLocal.updateMemo(memo);
	}
    }

    /**
     * Delete the memo text.
     */
    private void deleteMemoText(Integer memoID) throws RemoteException {
	CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
	MemoVO memo = commonMgrLocal.getMemo(memoID);
	if (memo != null) {
	    commonMgrLocal.deleteMemo(memo);
	}
    }

    /**
     * Construct the client using the data in the XML DOM node. Set the mode to
     * "History" and set the value object to read only.
     * 
     * //TODO Complete implementation for constructing client from XML DOM
     * 
     * @param clientNode
     *            Description of the Parameter
     * @exception CreateException
     *                Description of the Exception
     */
    public ClientVO(Node clientNode) throws RemoteException {
	// this.setFromProgressClientData(clientNode,ClientChangeNotificationEvent.ACTION_CREATE);
	if (clientNode == null) {
	    throw new SystemException("Failed to create client history from XML node. The node is not valid.");
	}
	LogUtil.debug(this.getClass(), "clientNode=" + XMLHelper.toXML(clientNode));
	NodeList elementList = clientNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("ABN".equals(nodeName) && valueText != null) {
		this.ABN = new BigDecimal(valueText);
	    } else if ("addressTitle".equals(nodeName) && valueText != null) {
		this.addressTitle = valueText;
	    } else if ("birthDate".equals(nodeName) && valueText != null) {
		try {
		    this.birthDate = new DateTime(valueText);
		} catch (ParseException pe) {
		    throw new SystemException("The birth date is not a valid date : " + pe.getMessage());
		}
	    } else if ("birthDateString".equals(nodeName) && valueText != null) {
		this.birthDateString = valueText;
	    } else if ("branchNumber".equals(nodeName) && valueText != null) {
		this.branchNumber = new Integer(valueText);
	    } else if ("clientNumber".equals(nodeName) && valueText != null) {
		this.clientNumber = new Integer(valueText);
	    } else if ("clientType".equals(nodeName) && valueText != null) {
		this.clientType = valueText;
	    } else if ("contactPersonName".equals(nodeName) && valueText != null) {
		this.contactPersonName = valueText;
	    } else if ("contactPersonPhone".equals(nodeName) && valueText != null) {
		this.contactPersonPhone = valueText;
	    } else if ("driversLicenseNumber".equals(nodeName) && valueText != null) {
		this.driversLicenseNumber = valueText;
	    } else if ("duplicateClientNumber".equals(nodeName) && valueText != null) {
		this.duplicateClientNumber = new Integer(valueText);
	    } else if ("emailAddress".equals(nodeName) && valueText != null) {
		this.emailAddress = valueText;
	    } else if ("gender".equals(nodeName) && valueText != null) {
		this.gender = valueText;
	    } else if ("groupClient".equals(nodeName) && valueText != null) {
		this.groupClient = Boolean.valueOf(valueText);
	    } else if ("givenNames".equals(nodeName) && valueText != null) {
		this.givenNames = valueText;
	    } else if ("homePhone".equals(nodeName) && valueText != null) {
		this.homePhone = valueText;
	    } else if ("initials".equals(nodeName) && valueText != null) {
		this.initials = valueText;
	    } else if ("lastUpdate".equals(nodeName) && valueText != null) {
		try {
		    this.lastUpdate = new DateTime(valueText);
		} catch (ParseException pe) {
		    throw new SystemException("The last date is not a valid date : " + pe.getMessage());
		}
	    } else if ("madeDate".equals(nodeName) && valueText != null) {
		try {
		    this.madeDate = new DateTime(valueText);
		} catch (ParseException pe) {
		    throw new SystemException("The made date is not a valid date : " + pe.getMessage());
		}
	    } else if ("madeID".equals(nodeName) && valueText != null) {
		this.madeID = valueText;
	    } else if ("maritalStatus".equals(nodeName) && valueText != null) {
		this.maritalStatus = Boolean.valueOf(valueText);
	    } else if ("masterClientNumber".equals(nodeName) && valueText != null) {
		this.masterClientNumber = new Integer(valueText);
	    } else if ("mobilePhone".equals(nodeName) && valueText != null) {
		this.mobilePhone = valueText;
	    } else if ("numberOfDependants".equals(nodeName) && valueText != null) {
		this.numberOfDependants = new Integer(valueText);
	    } else if ("occupation".equals(nodeName) && valueText != null) {
		try {
		    this.occupationID = new Integer(valueText);
		} catch (Exception e) {
		    this.occupationID = new Integer(0);
		}
	    } else if ("organisationName".equals(nodeName) && valueText != null) {
		this.organisationName = valueText;
	    } else if ("organisationPhone".equals(nodeName) && valueText != null) {
		this.organisationPhone = valueText;
	    }

	    else if ("postalName".equals(nodeName) && valueText != null) {
		this.postalName = valueText;
	    } else if ("salutation".equals(nodeName) && valueText != null) {
		this.salutation = valueText;
	    } else if ("sex".equals(nodeName) && valueText != null) {
		this.sex = Boolean.valueOf(valueText);
	    } else if ("status".equals(nodeName) && valueText != null) {
		this.status = valueText;
	    } else if ("surname".equals(nodeName) && valueText != null) {
		this.surname = valueText;
	    } else if ("title".equals(nodeName) && valueText != null) {
		this.title = valueText;
	    } else if ("workPhone".equals(nodeName) && valueText != null) {
		this.workPhone = valueText;
	    } else if ("market".equals(nodeName) && valueText != null) {
		this.market = Boolean.valueOf(valueText);
	    } else if ("vehicleList".equals(nodeName)) {
		// not implemented
	    } else if ("postalAddress".equals(nodeName) && writableNode != null) {
		this.postalAddress = AddressFactory.getPostalAddressVO(writableNode);
	    } else if ("residentialAddress".equals(nodeName) && writableNode != null) {
		this.residentialAddress = AddressFactory.getStreetAddressVO(writableNode);
	    }
	}
	//
	// setReadOnly(true);
	// try
	// {
	// setMode(MODE_HISTORY);
	// }
	// catch(ValidationException ve)
	// {
	// // Should be setting it to a valid value so pass the exception
	// // back as a system exception
	// throw new SystemException(ve.getMessage());
	// }
	this.setDerivedFields();
    }

    /**
     * Default constructor.
     */
    public ClientVO() {

    }

    public Collection getSubscriptionList() throws RemoteException {
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	return clientMgr.getClientSubscriptionList(this.clientNumber);
    }

    protected String givenNames;

    /**
     * @hibernate.property
     * @hibernate.column name="[given-names]"
     * @return String
     */
    public String getGivenNames() {
	return givenNames;
    }

    public void setGivenNames(String givenNames) {
	this.givenNames = givenNames;
    }

    public void setGender(String gender) {
	this.gender = gender;
    }

    public String getFirstNameOnly() {
	StringBuffer firstName = new StringBuffer();
	if (givenNames != null) {
	    if (givenNames.indexOf(" ") != -1) {
		firstName.append(givenNames.split(" ", -1)[0]);
	    } else {
		firstName.append(givenNames);
	    }
	}
	return firstName.toString();
    }

    /**
     * Get the market flag.
     * 
     * @hibernate.property
     * @hibernate.column name="[market]"
     * @return
     */
    public Boolean getMarket() {
	return this.market;
    }

    /**
     * Set the market flag.
     * 
     * @param market
     */
    public void setMarket(Boolean market) {
	this.market = market;
    }

    /**
     * Return the client ID. Maybe null if this is a new client and the ID has
     * not been set yet.
     * 
     * @hibernate.id column="[client-no]" generator-class="assigned"
     * @return Integer
     */
    public Integer getClientNumber() {
	return this.clientNumber;
    }

    /**
     * Return the client type. Raise an exception if the client type cannot be
     * determined.
     * 
     * @return String
     */
    // public abstract String getClientType();

    /**
     * Return the current status of the client. //TODO Raise an exception if the
     * status cannot be determined.
     * 
     * @hibernate.property
     * @hibernate.column name="[client-status]"
     * @return String
     */
    public String getStatus() {
	return ClientHelper.formatClientStatus(this.status);
    }

    /**
     * Return the ABN. Return null if no ABN is defined.
     * 
     * @hibernate.property
     * @hibernate.column name="abn"
     * @return String
     */
    public BigDecimal getABN() {
	return this.ABN;
    }

    /**
     * Return a reference to the postal address. Return null if no postal
     * address is defined.
     */
    public PostalAddressVO getPostalAddress() throws RemoteException {
	if (this.postalAddress == null && this.getPostStsubId() != null) {
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    AddressVO pavo = cMgr.getAddress(AddressVO.ADDRESS_TYPE_POSTAL, this.getPostProperty(), this.getPostStreetChar(), this.getPostStsubId(), this.getPostDpid(), this.getPostLatitude(), this.getPostLongitude(), this.getPostGnaf(), this.getPostAusbar());
	    if (pavo != null) {
		this.postalAddress = (PostalAddressVO) pavo;
	    }

	}
	return this.postalAddress;
    }

    /**
     * Return a reference to the street address. Return null if no street
     * address is defined.
     * 
     * @return Common_lv.Common.Addr.StreetAddressVO
     */
    public ResidentialAddressVO getResidentialAddress() throws RemoteException {
	LogUtil.debug(this.getClass(), "residentialAddress=" + residentialAddress);
	LogUtil.debug(this.getClass(), "stsubid=" + getResiStsubId());
	if (this.residentialAddress == null && this.getResiStsubId() != null) {
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    AddressVO savo = cMgr.getAddress(AddressVO.ADDRESS_TYPE_STREET, this.getResiProperty(), this.getResiStreetChar(), this.getResiStsubId(), this.getResiDpid(), this.getResiLatitude(), this.getResiLongitude(), this.getResiGnaf(), this.getResiAusbar());
	    if (savo != null) {
		this.residentialAddress = (ResidentialAddressVO) savo;
	    }
	}
	return this.residentialAddress;
    }

    /**
     * The salutation is a munged form of the clients name. It is used as the
     * salutation on letters. e.g. "Mr & Mrs Jones" As a last resort return the
     * string "Sir/Madam" if a suitable salutation cannot be determined. This
     * attribute derives its value from the cl-master.address-title column.
     * 
     * @return String
     */
    // public abstract String getSalutation();

    /**
     * The display name is a munged form of the clients name that can be used in
     * a single line screen display. As a last resort return the string
     * "??????". This method is abstract. It must be implemented by the sub
     * class.
     * 
     * @return String
     */
    // public abstract String getDisplayName();

    protected String emailAddress;

    /**
     * @hibernate.property
     * @hibernate.column name="email"
     * @return String
     */
    public String getEmailAddress() {
	return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
    }

    /**
     * The postalName is a munged form of the clients name that is used as the
     * first line on postal address labels, cheques and report extracts. As a
     * last resort return the string "???????". The postal name is derived from
     * the cl-master.postal-name column.
     * 
     * @return String
     */
    public final String DEFAULT_POSTALNAME = "????????";

    public final String DEFAULT_SALUTATION = "Sir/Madam";

    /**
     * The the memo vo object.
     */
    private MemoVO memo;

    /**
     * The clients ABN is if they have one and we are interested in it.
     */
    protected BigDecimal ABN;

    /**
     * title for addressing clients
     */
    protected String addressTitle;

    protected DateTime lastUpdate;

    private BranchVO branch;

    /**
     * The option to receive motor news. MOTOR_NEW_MEMEBRSHIP_SEND = send if
     * they have a current memebrship MOTOR_NEWS_SEND = send irrespective of
     * their memebrship existence/status MOTOR_NEWS_NO_SEND = don't send
     * irrespective of their memebrship existence/status
     */
    protected String motorNewsSendOption;

    /**
     * The method by which motor news will be delivered.
     * MOTOR_NEWS_DELIVERY_METHOD_NORML = deliver by hand.
     * MOTOR_NEWS_DELIVERY_METHOD_POSTAL = deliver by mail.
     */
    protected String motorNewsDeliveryMethod;

    protected Boolean groupClient;

    protected Integer branchNumber;

    protected Integer memoNumber;

    protected Integer masterClientNumber;

    protected Integer duplicateClientNumber;

    protected Boolean sex;

    protected String workPhone;

    protected String surname;

    protected Integer occupationID;

    protected String driversLicenseNumber;

    protected Integer numberOfDependants;

    protected DateTime birthDate;

    protected String homePhone;

    protected ClientOccupationVO occupation;

    protected String title;

    protected Boolean maritalStatus;

    protected String mobilePhone;

    protected String gender;

    protected String initials;

    /**
     * Description of the Field
     */
    protected String organisationName;

    /**
     * Description of the Field
     */
    protected String contactPersonPhone;

    /**
     * Description of the Field
     */
    protected String contactPersonName;

    /**
     * Description of the Field
     */
    protected String organisationPhone;

    /**
     * 
     * Description of the Field
     */

    // public abstract String getPostalName();

    // public abstract String getAddressTitle();

    /**
     * Return a list of the transactions that have occured on the client data.
     * via history.
     * 
     * @return java.util.ArrayList //TODO implement the ability to track
     *         transactions on a client
     */
    public Collection getTransactionList() {
	return null;
    }

    public SourceSystem getSourceSystem() {
	return this.sourceSystem;
    }

    // public abstract Boolean getSex();

    /**
     * Set the value of the client ID attribute.
     * 
     * @param clientID
     */
    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    /**
     * Set the clients ABN.
     * 
     * @param abn
     */
    public void setABN(BigDecimal ABN) {
	this.ABN = ABN;
    }

    /**
     * Munge the client name into a suitable postal name. The implementation
     * must be provided by the sub class.
     */
    // public abstract void setPostalName();

    /**
     * Munge the client name into a suitable salutation. The implementation must
     * be provided by the sub class.
     */
    // public abstract void setSalutation();

    /**
     * Set the postal address of the client. May be null.
     * 
     * @param postalAddress
     */
    public void setPostalAddress(PostalAddressVO postalAddress) {
	this.postalAddress = postalAddress;
    }

    /**
     * Set the street address of the client. Maybe null.
     * 
     * @param residentialAddress
     */
    public void setResidentialAddress(ResidentialAddressVO residentialAddress) {
	this.residentialAddress = residentialAddress;
    }

    /**
     * Return an instance of an object that implements the interface for the
     * specified client type.
     * 
     * @param clientType
     * @return Client_lv.ClientVO
     */
    // public abstract ClientVO create();

    // public abstract ClientVO copy()
    // throws CloneNotSupportedException;

    public void setStatus(String status) {
	if (status == null || status.trim().length() < 1) {
	    status = Client.STATUS_ACTIVE;
	}
	this.status = status;
    }

    public void setTransactionList(Collection transactionList) {
	this.transactionList = transactionList;
    }

    // protected void validateMode(String newMode)
    // throws ValidationException
    // {
    // if(ClientVO.CLIENT_MODE_HISTORY.equals(newMode)
    // || ClientVO.CLIENT_MODE_NORMAL.equals(newMode)
    // || ClientVO.CLIENT_MODE_LOADING.equals(newMode))
    // {
    // }
    // else
    // {
    // throw new ValidationException("The mode '" + newMode
    // + "' is not valid for a client value object.");
    // }
    // }

    public void setMadeID(String madeID) {
	this.madeID = madeID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[made-id]"
     * @return String
     */
    public String getMadeID() {
	return madeID;
    }

    public void setMadeDate(DateTime madeDate) {
	this.madeDate = madeDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="resi_dpid"
     * @return String
     */
    public String getResiDpid() {
	return this.resiDpid;
    }

    public void setResiDpid(String resiDpid) {
	this.resiDpid = resiDpid;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="post_dpid"
     * @return String
     */
    public String getPostDpid() {
	return this.postDpid;
    }

    public void setPostDpid(String postDpid) {
	this.postDpid = postDpid;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[made-date]"
     * @return DateTime
     */
    public DateTime getMadeDate() {
	return madeDate;
    }

    public void setMemo(MemoVO memo) {
	this.memo = memo;
    }

    public MemoVO getMemo() throws RemoteException {
	if (this.memo == null) {
	    Integer memoNo = this.getMemoNumber();
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    if (memoNo != null && memoNo.intValue() > 0) {
		this.memo = cMgr.getMemo(memoNo);
	    }
	}
	return this.memo;
    }

    public void setLastUpdate(DateTime lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[last-update]"
     * @return DateTime
     */
    public DateTime getLastUpdate() {
	return lastUpdate;
    }

    // public abstract void setAddressTitle();

    public void setBranch(BranchVO branch) {
	this.branch = branch;
    }

    public void setBranchNumber(Integer branchNumber) {
	this.branchNumber = branchNumber;
    }

    public BranchVO getBranch() throws RemoteException {
	if (this.branch == null && this.branchNumber != null && this.branchNumber.intValue() > 0) {
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    this.branch = cMgr.getBranch(this.branchNumber.intValue());
	}
	return this.branch;
    }

    public boolean isActive() {
	boolean active = false;
	String status = this.getStatus();
	if (status == null || status.trim().equals("") || status.equalsIgnoreCase(Client.STATUS_ACTIVE)) {
	    active = true;
	}
	return active;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[branch-no]"
     * @return Integer
     */
    public Integer getBranchNumber() {
	return this.branchNumber;
    }

    public boolean isVehicleAttached() throws RemoteException {
	boolean vehicleAttached = false;
	Collection vehicles = this.getVehicleList();
	// if the list is not null
	if (vehicles != null) {
	    // and there is actually a list of vehicles (or at least one)
	    if (vehicles.size() != 0) {
		vehicleAttached = true;
	    }
	}
	return vehicleAttached;
    }

    public void setVehicleList(Collection vehicleList) {
	this.vehicleList = vehicleList;
    }

    public void addVehicle(VehicleVO vo) {
	if (vehicleList == null) {
	    this.vehicleList = new ArrayList();
	}

	if (vehicleList.size() == 0) {
	    this.vehicleList.add(vo);
	} else {
	    /** //TODO why do this? JH */
	    VehicleVO oldVehicle = (VehicleVO) ((ArrayList) vehicleList).get(0);
	    Integer vehicleID = oldVehicle.getVehicleID();
	    vo.setVehicleID(vehicleID);
	    vehicleList = new ArrayList();
	    vehicleList.add(vo);
	}
    }

    /**
     * return a list of vehicle vo's
     */
    public Collection getVehicleList() throws RemoteException {
	if (this.vehicleList == null) {
	    ClientMgr cMgr = ClientEJBHelper.getClientMgr();
	    this.vehicleList = cMgr.getClientVehicleList(this.clientNumber);
	}
	return this.vehicleList;
    }

    public void setMotorNewsSendOption(String sendOption) {
	this.motorNewsSendOption = sendOption;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="motor_news_send_option"
     * @return String
     */
    public String getMotorNewsSendOption() {
	return this.motorNewsSendOption;
    }

    public void setMotorNewsDeliveryMethod(String deliveryMethod) {
	this.motorNewsDeliveryMethod = deliveryMethod;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="motor_news_delivery_method"
     * @return String
     */
    public String getMotorNewsDeliveryMethod() {
	return this.motorNewsDeliveryMethod;
    }

    public void setGroupClient(Boolean groupClient) {
	this.groupClient = groupClient;
    }

    public void setSourceSystem(SourceSystem ss) {
	this.sourceSystem = ss;
    }

    /**
     * is the client a group client in the historical sense?
     * 
     * @hibernate.property
     * @hibernate.column name="[group-id]"
     */
    public Boolean getGroupClient() {
	return this.groupClient;
    }

    public void setMemoNumber(Integer memoNumber) {
	this.memoNumber = memoNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[memo-no]"
     * @return Integer
     */
    public Integer getMemoNumber() {
	return memoNumber;
    }

    public void setMasterClientNumber(Integer masterClientNumber) {
	this.masterClientNumber = masterClientNumber;
    }

    public void setDuplicateClientNumber(Integer duplicateClientNumber) {
	this.duplicateClientNumber = duplicateClientNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="master_client_number"
     * @return Integer
     */
    public Integer getMasterClientNumber() {
	return masterClientNumber;
    }

    public Integer getDuplicateClientNumber() {
	return duplicateClientNumber;
    }

    /**
     * Gets the title attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[client-title]"
     * @return The title value
     */
    public String getTitle() {
	return title;
    }

    public void setInitials(String initials) {
	this.initials = initials;
    }

    /**
     * Sets the initials attribute of the PersonVO object
     */
    private void setInitials() {
	if (this.givenNames != null && this.givenNames.trim().length() > 0) {
	    StringBuffer init = new StringBuffer();
	    String name = this.givenNames;

	    String spaceGroupNameDelimiter = " " + GROUP_NAME_DELIMITER + " ";

	    String currentChar = "";

	    // replace double spaces with single spaces
	    name = StringUtil.replaceAll(name, "  ", " ");

	    // replace quotes with apostrophe
	    name = StringUtil.replaceAll(name, "\"", "'");

	    // replace & with _&_
	    // name = StringUtil.replaceAll(name, GROUP_NAME_DELIMITER,
	    // spaceGroupNameDelimiter);

	    // break on space
	    // StringTokenizer tokenizer = new StringTokenizer(name, " ");
	    String[] tokenizer = name.split(" ");
	    for (int i = 0; i < tokenizer.length; i++) {
		currentChar = "";
		if (tokenizer[i].length() > 0) {
		    currentChar = tokenizer[i].substring(0, 1);

		    if (currentChar.equals(GROUP_NAME_DELIMITER)) {
			// add back into String with space
			currentChar = spaceGroupNameDelimiter;
		    }

		    init.append(currentChar);
		}

	    }
	    // if not null
	    if (init != null) {
		this.initials = init.toString().toUpperCase();
	    }
	}
    }

    /**
     * Gets the maritalStatus attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[marital-status]"
     * @return The maritalStatus value
     */
    public Boolean getMaritalStatus() {
	return maritalStatus;
    }

    /**
     * Gets the birthDate attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[birth-date]"
     * @return The birthDate value
     */
    public DateTime getBirthDate() {
	return birthDate;
    }

    /**
     * Gets the numberOfDependants attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[children-no]"
     * @return The numberOfDependants value
     */
    public Integer getNumberOfDependants() {
	return numberOfDependants;
    }

    /**
     * Sets the sex attribute of the PersonVO object
     * 
     * @param sex
     *            The new sex value
     */
    public void setSex(Boolean sex) {
	this.sex = sex;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);

	if (this.ABN != null && this.ABN.compareTo(new BigDecimal(0)) > 0) {
	    cw.writeAttribute("ABN", this.ABN);
	}
	if (this.birthDate != null) {
	    cw.writeAttribute("birthDate", this.birthDate);
	}

	cw.writeAttribute("branchNumber", this.branchNumber);
	cw.writeAttribute("clientNumber", this.clientNumber);
	cw.writeAttribute("clientType", this.getClientType());

	cw.writeAttribute("contactPersonName", this.contactPersonName);
	cw.writeAttribute("contactPersonPhone", this.contactPersonPhone);

	if (this.driversLicenseNumber != null && this.driversLicenseNumber.length() > 0) {
	    cw.writeAttribute("driversLicenseNumber", this.driversLicenseNumber);
	}

	cw.writeAttribute("displayName", this.getDisplayName());

	if (this.emailAddress != null && this.emailAddress.length() > 0) {
	    cw.writeAttribute("emailAddress", this.emailAddress);
	}

	cw.writeAttribute("gender", this.getGender());
	cw.writeAttribute("givenNames", this.givenNames);
	cw.writeAttribute("groupClient", this.getGroupClient());
	cw.writeAttribute("homePhone", this.homePhone);
	cw.writeAttribute("initials", this.initials);
	cw.writeAttribute("lastUpdate", this.lastUpdate);
	cw.writeAttribute("madeDate", this.madeDate);
	cw.writeAttribute("madeID", this.madeID);
	cw.writeAttribute("maritalStatus", getMaritalStatus());

	if (this.getMemo() != null && this.getMemo().getMemoLine().length() > 0) {
	    cw.writeAttribute("memo", this.getMemo().getMemoLine());
	}
	if (this.mobilePhone != null && this.mobilePhone.length() > 0) {
	    cw.writeAttribute("mobilePhone", this.mobilePhone);
	}
	if (this.motorNewsDeliveryMethod != null && this.motorNewsDeliveryMethod.length() > 0) {
	    cw.writeAttribute("motorNewsDeliveryMethod", this.motorNewsDeliveryMethod);
	}
	if (this.motorNewsSendOption != null && this.motorNewsSendOption.length() > 0) {
	    cw.writeAttribute("motorNewsSendOption", this.motorNewsSendOption);
	}
	if (this.numberOfDependants != null && this.numberOfDependants.intValue() > 0) {
	    cw.writeAttribute("numberOfDependants", this.numberOfDependants);
	}
	ClientOccupationVO covo = this.getOccupation();
	if (this.occupationID != null && covo != null) {
	    cw.writeAttribute("occupation", covo.getOccupation());
	}

	cw.writeAttribute("postalAddress", getPostalAddress());

	if (this.postalName != null && this.postalName.length() > 0) {
	    cw.writeAttribute("postalName", this.postalName);
	}

	cw.writeAttribute("salutation", this.salutation);

	cw.writeAttribute("sex", this.sex);
	cw.writeAttribute("status", this.status);
	cw.writeAttribute("masterClientNumber", this.masterClientNumber);
	cw.writeAttribute("residentialAddress", this.getResidentialAddress());
	cw.writeAttribute("surname", this.surname);
	cw.writeAttribute("title", this.title);

	if (this.workPhone != null && this.workPhone.length() > 0) {
	    cw.writeAttribute("workPhone", this.workPhone);
	}

	cw.writeAttribute("market", getMarket());
	cw.writeAttributeList("transactionList", getTransactionList());

	cw.endClass();
    }

    /**
     * Sets the surname attribute of the PersonVO object
     * 
     * @param surname
     *            The new surname value
     */
    public void setSurname(String surname) {
	if (surname != null) {
	    surname = surname.toUpperCase().trim();
	}
	this.surname = surname;
    }

    /**
     * @return The displayName value //TODO implement for group clients in other
     *         systems
     */
    public String getDisplayName() {
	// default implementation for client history
	return ClientHelper.formatCustomerName(this.title, this.givenNames, this.surname, true, false, true, true).toUpperCase();
	// typically implemented in subclass
    }

    /**
     * Gets the surname attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="surname"
     * @return The surname value
     */
    public String getSurname() {
	return surname;
    }

    /**
     * Gets the homePhone attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[home-phone]"
     * @return The homePhone value
     */
    public String getHomePhone() {
	return homePhone;
    }

    /**
     * Relax validation if we are loading data from an entity bean.
     * 
     * @param workPhone
     *            The new workPhone value
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setWorkPhone(String workPhone) throws RemoteException, ValidationException {
	// if(ClientVO.CLIENT_MODE_LOADING.equals(this.getMode()))
	// {
	this.workPhone = workPhone;
	// }
	// else
	// {
	// this.workPhone =
	// NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_WORK,
	// workPhone);
	// }
    }

    /**
     * Sets the driversLicenseNumber attribute of the PersonVO object
     * 
     * @param driversLicenseNumber
     *            The new driversLicenseNumber value
     */
    public void setDriversLicenseNumber(String driversLicenseNumber) {
	this.driversLicenseNumber = driversLicenseNumber;
    }

    /**
     * Gets the occupationID attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="occupation_id"
     * @return The occupationID value
     */
    public Integer getOccupationID() {
	return occupationID;
    }

    /**
     * Gets the driversLicenseNumber attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="drivers_licence_number"
     * @return The driversLicenseNumber value
     */
    public String getDriversLicenseNumber() {
	return driversLicenseNumber;
    }

    /**
     * Sets the numberOfDependants attribute of the PersonVO object
     * 
     * @param numberOfDependants
     *            The new numberOfDependants value
     */
    public void setNumberOfDependants(Integer numberOfDependants) {
	this.numberOfDependants = numberOfDependants;
    }

    /**
     * Gets the workPhone attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[work-phone]"
     * @return The workPhone value
     */
    public String getWorkPhone() {
	return workPhone;
    }

    /**
     * Sets the occupation attribute of the PersonVO object
     * 
     * @param occupation
     *            The new occupation value
     */
    public void setOccupation(ClientOccupationVO occupation) {
	this.occupation = occupation;
    }

    /**
     * Sets the title attribute of the PersonVO object
     * 
     * @param title
     *            The new title value
     */
    public void setTitle(String title) {
	this.title = title;
    }

    /**
     * Sets the maritalStatus attribute of the PersonVO object
     * 
     * @param maritalStatus
     *            The new maritalStatus value
     */
    public void setMaritalStatus(Boolean maritalStatus) {
	this.maritalStatus = maritalStatus;
    }

    /**
     * Sets the birthDate attribute of the PersonVO object
     * 
     * @param birthDate
     *            The new birthDate value
     * @exception ValidationException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setBirthDate(DateTime birthDate) throws ValidationException, RemoteException {
	if (birthDate != null) {
	    if (birthDate.getTime() > (new Date()).getTime()) {
		throw new ValidationException("Birth date must not be before today's date.");
	    }
	}
	this.birthDate = birthDate;
    }

    /**
     * Relax validation if we are loading data from an entity bean.
     * 
     * @param homePhone
     *            The new homePhone value
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setHomePhone(String homePhone) throws RemoteException, ValidationException {
	// if(ClientVO.CLIENT_MODE_LOADING.equals(this.getMode()))
	// {
	this.homePhone = homePhone;
	// }
	// else
	// {
	// this.homePhone =
	// NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_HOME,
	// homePhone);
	// }
    }

    /**
     * Gets the gender attribute of the PersonVO object
     * 
     * @return The gender value
     */
    public String getGender() {
	String personGender = null;
	if (this.gender == null) {
	    if (this.sex != null) {
		this.gender = sex ? GENDER_MALE : GENDER_FEMALE;
	    }
	}
	return this.gender;
    }

    private Integer lastSeqUsed;

    /**
     * @hibernate.property
     * @hibernate.column name="[last-seq-used]"
     * @return Integer
     */
    public Integer getLastSeqUsed() {
	return this.lastSeqUsed;
    }

    public void setLastSeqUsed(Integer lastSeqUsed) {
	this.lastSeqUsed = lastSeqUsed;
    }

    /**
     * Gets the clientType attribute of the PersonVO object
     * 
     * @return The clientType value
     */
    public String getClientType() {
	if (this.clientType == null) {
	    // //surname must have something
	    // if((this.givenNames == null || this.givenNames.trim().equals(""))
	    // &&
	    // (this.surname != null ? this.surname.indexOf("ESTATE") == -1 :
	    // false) &&
	    // (this.status != null ?
	    // !this.status.trim().equals(Client.STATUS_DECEASED) : true) &&
	    // (this.title == null || this.title.trim().equals(""))
	    // )
	    // {
	    // this.clientType = ClientVO.CLIENT_TYPE_ORGANISATION;
	    // }
	    // else
	    // {
	    // this.clientType = ClientVO.CLIENT_TYPE_PERSON;
	    // }
	    // }
	    if (this instanceof OrganisationVO) {
		clientType = ClientVO.CLIENT_TYPE_ORGANISATION;
	    } else {
		clientType = ClientVO.CLIENT_TYPE_PERSON;
	    }
	}
	return this.clientType;
    }

    /**
     * Gets the initials attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="initials"
     * 
     * @return The initials value
     */
    public String getInitials() {
	return this.initials;
    }

    /**
     * Return the clients age as at the time of the specified date.
     * 
     * @param birthDate
     *            Description of the Parameter
     * @return Util.Interval
     * @exception Exception
     *                Description of the Exception
     */
    public Interval getAge(DateTime toDate) throws Exception {
	Interval nAge = null;
	if (birthDate != null) {
	    nAge = new Interval(birthDate, toDate);
	}
	return nAge;
    }

    /**
     * Gets the mobilePhone attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[mobile-phone]"
     * @return The mobilePhone value
     */
    public String getMobilePhone() {
	return mobilePhone;
    }

    /**
     * Gets the occupation attribute of the PersonVO object
     * 
     * @return The occupation value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ClientOccupationVO getOccupation() throws RemoteException {
	if (this.occupation == null && this.occupationID != null) {
	    ClientMgr cMgr = ClientEJBHelper.getClientMgr();
	    ClientOccupationVO covo = cMgr.getClientOccupation(this.occupationID);
	    this.occupation = covo;
	}
	return this.occupation;
    }

    /**
     * Sets the mobilePhone attribute of the PersonVO object
     * 
     * @param mobilePhone
     *            The new mobilePhone value
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setMobilePhone(String mobilePhone) throws RemoteException, ValidationException {
	// if(ClientVO.CLIENT_MODE_LOADING.equals(this.getMode()))
	// {
	this.mobilePhone = mobilePhone;
	// }
	// else
	// {
	// this.mobilePhone =
	// NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_MOBILE,
	// mobilePhone);
	// }
    }

    /**
     * Gets the sex attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[sex]"
     * @return The sex value
     */
    public Boolean getSex() {
	return sex;
    }

    /**
     * Sets the occupationID attribute of the PersonVO object
     * 
     * @param occupationID
     *            The new occupationID value
     */
    public void setOccupationID(Integer occupationID) {
	this.occupationID = occupationID;
    }

    /**
     * Gets the contactPersonPhone attribute of the OrganisationVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[contact_person_phone]"
     * @return The contactPersonPhone value
     */
    public String getContactPersonPhone() {
	return contactPersonPhone;
    }

    /**
     * Sets the organisationName attribute of the OrganisationVO object
     * 
     * @param organisationName
     *            The new organisationName value
     */
    public void setOrganisationName(String organisationName) {
	this.organisationName = organisationName;
    }

    /**
     * Sets the salutation attribute of the PersonVO object
     */
    private void setSalutation() {
	this.salutation = this.DEFAULT_SALUTATION;
    }

    /**
     * Gets the salutation attribute of the PersonVO object
     * 
     * @return The salutation value
     */
    public String getSalutation() {
	initSalutation();
	return this.salutation;
    }

    /**
     * Initialises and returns a salutation.
     * 
     * @return
     */
    private String initSalutation() {
	String sal = this.DEFAULT_SALUTATION;

	String firstName = this.getFirstNameOnly();

	if (firstName != null && firstName.length() > 1 && (!firstName.equals(initials) || givenNames.startsWith(firstName))) {
	    sal = firstName.substring(0, 1) + firstName.substring(1, firstName.length()).toLowerCase();
	} else {
	    sal = addressTitle;
	}

	this.setSalutation(sal);

	return this.salutation;
    }

    /**
     * Sets the contactPersonName attribute of the OrganisationVO object
     * 
     * @param contactPersonName
     *            The new contactPersonName value
     */
    public void setContactPersonName(String contactPersonName) {
	this.contactPersonName = contactPersonName;
    }

    /**
     * Sets the contactPersonPhone attribute of the OrganisationVO object
     * 
     * @param contactPersonPhone
     *            The new contactPersonPhone value
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setContactPersonPhone(String contactPersonPhone) throws RemoteException, ValidationException {
	// if(ClientVO.CLIENT_MODE_LOADING.equals(this.getMode()))
	// {
	this.contactPersonPhone = contactPersonPhone;
	// }
	// else
	// {
	// this.contactPersonPhone =
	// NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_WORK,
	// contactPersonPhone);
	// }
    }

    /**
     * Gets the organisationName attribute of the OrganisationVO object
     * 
     * @return The organisationName value
     */
    public String getOrganisationName() {
	return this.getDisplayName();
    }

    /**
     * Gets the contactPersonName attribute of the OrganisationVO object
     * 
     * @hibernate.property
     * @hibernate.column name="contact_person_name"
     * @return The contactPersonName value
     */
    public String getContactPersonName() {
	return contactPersonName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[address-title]"
     * @return The addressTitle value //TODO implement for group clients in
     *         other systems
     * 
     */
    public String getAddressTitle() {
	return this.addressTitle;
    }

    public void setDerivedFields() {
	// update all derived fields
	this.setInitials();
	this.setAddressTitle();
	this.setSalutation();
	this.setPostalName();
    }

    /**
     * Sets the addressTitle attribute of the PersonVO object
     * APPS-48 Fix Apostrophe issue for letters 19-06-2019
     * Also cater for multiple apostrophes in the address title
     */
    private void setAddressTitle() {
	StringBuffer addressTitle = new StringBuffer();
	String thisTitle = this.getTitle();
	if (thisTitle != null) {
	    addressTitle.append(StringUtil.toProperCase(thisTitle.trim()));
	    addressTitle.append(" ");
	}
	String thisSurname = this.getSurname();
	if (thisSurname != null) {
	    // cater for apostrophes
	    String[] nameParts = thisSurname.split("'");
	    if (nameParts.length > 0) {
		for (int partIt = 0; partIt < nameParts.length; partIt++) {
		    addressTitle.append(StringUtil.toProperCase(nameParts[partIt], true));
		    if (partIt < nameParts.length - 1)
			addressTitle.append("'");
		}
	    } else {
		addressTitle.append(StringUtil.toProperCase(thisSurname.trim(), true));
	    }
	    // THIS CODE DOES NOT CATER FOR MULTIPLE APOSTROPHES
//	    int endIndex = thisSurname.indexOf("'");
//	    if (endIndex > 0) {
//		addressTitle.append(thisSurname.substring(0, endIndex).toUpperCase() + StringUtil.toProperCase(thisSurname.substring(endIndex, thisSurname.length())));
//	    } else {
//		addressTitle.append(StringUtil.toProperCase(thisSurname.trim()));
//	    }
	}
	if (addressTitle.length() > 0) {
	    this.addressTitle = addressTitle.toString();
	} else {
	    this.addressTitle = DEFAULT_SALUTATION;
	}
    }

    /**
     * Gets the postalName attribute of the PersonVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[postal-name]"
     * @return The postalName value
     */
    public String getPostalName() {
	return this.postalName;
    }

    /**
     * Sets the postalName attribute of the PersonVO object
     */
    private void setPostalName() {
	StringBuffer pName = new StringBuffer();
	LogUtil.debug("setPostalName", "1");
	if (this.getTitle() != null) {
	    pName.append(StringUtil.toProperCase(this.getTitle().trim()));
	    pName.append(" ");
	}
	LogUtil.debug("setPostalName", "2");
	if (this.getInitials() != null) {
	    pName.append(this.getInitials().trim());
	    pName.append(" ");
	}
	LogUtil.debug("setPostalName", "3");
	if (this.getSurname() != null) {
	    pName.append(StringUtil.toProperCase(this.getSurname().trim()));
	}
	LogUtil.debug("setPostalName", "4");
	if (pName.length() > 0) {
	    this.postalName = pName.toString();
	}
    }

    /**
     * Return the persons age as of today.
     * 
     * @return The age value
     * @exception Exception
     *                Description of the Exception
     */
    public Interval getAge() throws Exception {
	DateTime today = new DateTime();
	return getAge(today);
    }

    /**
     * Gets the organisationPhone attribute of the OrganisationVO object
     * 
     * @return The organisationPhone value
     */
    public String getOrganisationPhone() {
	return organisationPhone;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-street-char]"
     * @return String
     */
    public String getPostStreetChar() {
	return postStreetChar;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-street-no]"
     * @return Integer
     */
    public Integer getPostStreetNumber() {
	return postStreetNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-street-char]"
     * @return String
     */
    public String getResiStreetChar() {
	return resiStreetChar;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-street-no]"
     */
    public Integer getResiStreetNumber() {
	return resiStreetNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-stsubid]"
     */
    public Integer getPostStsubId() {
	return postStsubId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-stsubid]"
     */
    public Integer getResiStsubId() {
	return resiStsubId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-property]"
     */
    public String getPostProperty() {
	return postProperty;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-property]"
     */
    public String getResiProperty() {
	return resiProperty;
    }

    // public String getDebtorCode()
    // {
    // return debtorCode;
    // }

    /**
     * Sets the organisationPhone attribute of the OrganisationVO object
     * 
     * @param organisationPhone
     *            The new organisationPhone value
     * @exception RemoteException
     *                Description of the Exception
     * @exception ValidationException
     *                Description of the Exception
     */
    public void setOrganisationPhone(String organisationPhone) throws RemoteException, ValidationException {
	// if(ClientVO.CLIENT_MODE_LOADING.equals(this.getMode()))
	// {
	this.organisationPhone = organisationPhone;
	// }
	// else
	// {
	// this.organisationPhone =
	// NumberUtil.validatePhoneNumber("Organisation", organisationPhone);
	// }
    }

    public void setPostStreetChar(String postStreetChar) {
	this.postStreetChar = postStreetChar;
	this.setPostStreetNumber(AddressUtil.getStreetNumberFromStreetChar(this.postStreetChar));
    }

    public void setPostStreetNumber(Integer postStreetNumber) {
	this.postStreetNumber = postStreetNumber;
    }

    public void setResiStreetChar(String resiStreetChar) {
	this.resiStreetChar = resiStreetChar;
	this.setResiStreetNumber(AddressUtil.getStreetNumberFromStreetChar(this.resiStreetChar));
    }

    public void setResiStreetNumber(Integer resiStreetNumber) {
	this.resiStreetNumber = resiStreetNumber;
    }

    public void setPostStsubId(Integer postStsubId) {
	this.postStsubId = postStsubId;
    }

    public void setResiStsubId(Integer resiStsubId) {
	this.resiStsubId = resiStsubId;
    }

    public void setPostProperty(String postProperty) {
	this.postProperty = postProperty;
    }

    public void setResiProperty(String resiProperty) {
	this.resiProperty = resiProperty;
    }

    public void setPostalName(String postalName) {
	this.postalName = postalName;
    }

    public void setAddressTitle(String addressTitle) {
	this.addressTitle = addressTitle;
    }

    public void setSalutation(String salutation) {
	this.salutation = salutation;
    }

    @Override
    public String toString() {
	return "ClientVO [addressTitle=" + addressTitle + ", birthDate=" + birthDate + ", birthDateString=" + birthDateString + ", branch=" + branch + ", branchNumber=" + branchNumber + ", clientNumber=" + clientNumber + ", clientType=" + clientType + ", contactPersonName=" + contactPersonName + ", contactPersonPhone=" + contactPersonPhone + ", driversLicenseNumber=" + driversLicenseNumber + ", duplicateClientNumber=" + duplicateClientNumber + ", emailAddress=" + emailAddress + ", gender=" + gender + ", givenNames=" + givenNames + ", groupClient=" + groupClient + ", homePhone=" + homePhone + ", initials=" + initials + ", lastSeqUsed=" + lastSeqUsed + ", lastUpdate=" + lastUpdate + ", madeDate=" + madeDate + ", madeID=" + madeID + ", maritalStatus=" + maritalStatus + ", market=" + market + ", masterClientNumber=" + masterClientNumber + ", memo=" + memo + ", memoNumber=" + memoNumber + ", mobilePhone=" + mobilePhone + ", motorNewsDeliveryMethod=" + motorNewsDeliveryMethod + ", motorNewsSendOption="
		+ motorNewsSendOption + ", numberOfDependants=" + numberOfDependants + ", occupation=" + occupation + ", occupationID=" + occupationID + ", organisationName=" + organisationName + ", organisationPhone=" + organisationPhone + ", phonePassword=" + phonePassword + ", phoneQuestion=" + phoneQuestion + ", postDpid=" + postDpid + ", postProperty=" + postProperty + ", postStreetChar=" + postStreetChar + ", postStreetNumber=" + postStreetNumber + ", postStsubId=" + postStsubId + ", postalAddress=" + postalAddress + ", postalName=" + postalName + ", resiDpid=" + resiDpid + ", resiProperty=" + resiProperty + ", resiStreetChar=" + resiStreetChar + ", resiStreetNumber=" + resiStreetNumber + ", resiStsubId=" + resiStsubId + ", residentialAddress=" + residentialAddress + ", salutation=" + salutation + ", sex=" + sex + ", smsAllowed=" + smsAllowed + ", sourceSystem=" + sourceSystem + ", status=" + status + ", surname=" + surname + ", title=" + title + ", vehicleList=" + vehicleList + ", workPhone="
		+ workPhone + ", subSystem=" + subSystem + ", resiLatitude=" + resiLatitude + ", resiLongitude=" + resiLongitude + ", resiGnaf=" + resiGnaf + ", postAusbar=" + postAusbar + ", postLatitude=" + postLatitude + ", postLongitude=" + postLongitude + ", postGnaf=" + postGnaf + ", resiAusbar=" + resiAusbar + "]";
    }

    // public String toDebugString()
    // {
    // return this.clientNumber
    // + "\n" + this.getDisplayName()
    // + "\nresi: " + this.resiProperty + " " + this.resiStreetChar + " " +
    // this.resiStsubId
    // + "\npost: " + this.postProperty + " " + this.postStreetChar + " " +
    // this.postStsubId;
    //
    // }
    //
    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="phonePassword"
     * @return String
     */
    public String getPhonePassword() {
	return phonePassword;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="phoneQuestion"
     * @return String
     */
    public String getPhoneQuestion() {
	return phoneQuestion;
    }

    public void setPhonePassword(String phonePassword) {
	if (phonePassword != null)
	    this.phonePassword = phonePassword.trim();
    }

    public void setPhoneQuestion(String phoneQuestion) {
	if (phoneQuestion != null)
	    this.phoneQuestion = phoneQuestion.trim();
    }

    /**
     * @hibernate.property
     * @hibernate.column name="smsAllowed"
     * @return boolean
     */
    public Boolean getSmsAllowed() {
	return smsAllowed;
    }

    public void setSmsAllowed(Boolean smsAllowed) {
	this.smsAllowed = smsAllowed;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[src-system]"
     */
    public String getSubSystem() {
	return subSystem;
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-latitude]"
     * @return the resiLatitude
     */
    public String getResiLatitude() {
	return resiLatitude;
    }

    /**
     * @param resiLatitude
     *            the resiLatitude to set
     */
    public void setResiLatitude(String resiLatitude) {
	this.resiLatitude = resiLatitude;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-longitude]"
     * @return the resiLongitude
     */
    public String getResiLongitude() {
	return resiLongitude;
    }

    /**
     * @param resiLongitude
     *            the resiLongitude to set
     */
    public void setResiLongitude(String resiLongitude) {
	this.resiLongitude = resiLongitude;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-gnafid]"
     * @return the resiGnaf
     */
    public String getResiGnaf() {
	return resiGnaf;
    }

    /**
     * @param resiGnaf
     *            the resiGnaf to set
     */
    public void setResiGnaf(String resiGnaf) {
	this.resiGnaf = resiGnaf;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-ausbar]"
     * @return the postAusbar
     */
    public String getPostAusbar() {
	return postAusbar;
    }

    public void setPostAusbar(String postAusbar) {
	this.postAusbar = postAusbar;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-latitude]"
     */
    public String getPostLatitude() {
	return postLatitude;
    }

    public void setPostLatitude(String postLatitude) {
	this.postLatitude = postLatitude;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-longitude]"
     */
    public String getPostLongitude() {
	return postLongitude;
    }

    public void setPostLongitude(String postLongitude) {
	this.postLongitude = postLongitude;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[post-gnafid]"
     */
    public String getPostGnaf() {
	return postGnaf;
    }

    public void setPostGnaf(String postGnaf) {
	this.postGnaf = postGnaf;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[resi-ausbar]"
     */
    public String getResiAusbar() {
	return resiAusbar;
    }

    public void setResiAusbar(String resiAusbar) {
	this.resiAusbar = resiAusbar;
    }
}
