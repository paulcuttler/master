package com.ract.client;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.util.LogUtil;

public class NoteTypeDetailAction extends ActionSupport {

    public NoteType getNoteTypeDetail() {
	return noteTypeDetail;
    }

    public void setNoteTypeDetail(NoteType noteTypeDetail) {
	this.noteTypeDetail = noteTypeDetail;
    }

    public String getNoteType() {
	return noteType;
    }

    public void setNoteType(String noteType) {
	this.noteType = noteType;
    }

    private NoteType noteTypeDetail;

    private String noteType;

    @Override
    public String execute() throws Exception {
	LogUtil.debug(this.getClass(), "noteType=" + noteType);
	if (noteType != null) {
	    noteTypeDetail = clientMgr.getNoteType(noteType);
	}
	return SUCCESS;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgr clientMgr;

}
