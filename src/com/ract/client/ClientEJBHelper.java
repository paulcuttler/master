package com.ract.client;

import java.rmi.RemoteException;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

/**
 * Obtain easy access to EJBs for client.
 * 
 * @author John Holliday
 * @created 1 August 2002
 * @version 1.0
 */

public class ClientEJBHelper {

    /**
     * Gets the clientMgr attribute of the ClientEJBHelper class
     * 
     * @return The clientMgr value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static ClientMgr getClientMgr() {
	ClientMgr clientMgr = null;
	try {
	    clientMgr = (ClientMgr) ServiceLocator.getInstance().getObject("ClientMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return clientMgr;
    }

    public static ClientMgrLocal getClientMgrLocal() {
	ClientMgrLocal clientMgrLocal = null;
	try {
	    clientMgrLocal = (ClientMgrLocal) ServiceLocator.getInstance().getObject("ClientMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return clientMgrLocal;
    }

    /**
     * Gets the clientMgr attribute of the ClientEJBHelper class
     * 
     * @return The clientMgr value
     */
    public static ClientIDMgr getClientIDMgr() {
	ClientIDMgr clientIdMgr = null;
	try {
	    clientIdMgr = (ClientIDMgr) ServiceLocator.getInstance().getObject("ClientIDMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return clientIdMgr;
    }

}
