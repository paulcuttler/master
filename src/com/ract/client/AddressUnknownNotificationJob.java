package com.ract.client;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AddressUnknownNotificationJob implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	clientMgr.addressUnknownNotifications(false);
    }

}
