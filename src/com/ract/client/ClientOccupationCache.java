package com.ract.client;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.ract.common.CacheBase;

/**
 * Client occupation cache base.
 * 
 * @author John Holliday
 * @created 2 April 2003
 * @version 1.0
 */

public class ClientOccupationCache extends CacheBase {

    /**
     * Holds a reference to an instance of this class. The class instance is
     * created when the Class is first referenced.
     */
    private static ClientOccupationCache instance = new ClientOccupationCache();

    /**
     * Holds a reference to the ClientOccupation home interface. Use the
     * getHomeInterface() method to populate.
     */
    // private ClientOccupationHome clientOccupationHome = null;

    /**
     * Return a reference to the ClientOccupation home interface. Initialise the
     * reference if this is the first call.
     * 
     * @return The homeInterface value
     * @exception RemoteException
     *                Description of the Exception
     */
    // private ClientOccupationHome getHomeInterface()
    // throws RemoteException
    // {
    // if(this.clientOccupationHome == null)
    // {
    // this.clientOccupationHome = ClientEJBHelper.getClientOccupationHome();
    // }
    // return this.clientOccupationHome;
    // }

    /**
     * Implement the initialiseList method of the super class. Initialise the
     * list of cached items.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void initialiseList() throws RemoteException {
	if (this.isEmpty()) {
	    this.addAll(ClientEJBHelper.getClientMgr().getClientOccupations());
	}
    }

    /**
     * Return a reference the instance of this class.
     * 
     * @return The instance value
     */
    public static ClientOccupationCache getInstance() {
	return instance;
    }

    /**
     * Return the specified discount type value object. Search the list of
     * products for one that has the same code. Use the getList() method to make
     * sure the list has been initialised.
     * 
     * @param ClientOccupation
     *            Description of the Parameter
     * @return The clientOccupation value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ClientOccupationVO getClientOccupation(String ClientOccupation) throws RemoteException {
	return (ClientOccupationVO) this.getItem(ClientOccupation);
    }

    /**
     * Return the list of discount types. Use the getList() method to make sure
     * the list has been initialised. A null or an empty list may be returned.
     * 
     * @return The clientOccupationList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getClientOccupationList() throws RemoteException {
	return this.getItemList();
    }

    /**
     * Return the caches reference to the DiscountType home interface
     * 
     * @return The clientOccupationHome value
     * @exception RemoteException
     *                Description of the Exception
     */
    // public ClientOccupationHome getClientOccupationHome()
    // throws RemoteException
    // {
    // return this.getHomeInterface();
    // }

}
