package com.ract.client;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

public class ReassignNoteAction extends ViewClientNoteAction implements SessionAware {

    private String categoryCode;

    @InjectEJB(name = "ClientMgrBean")
    ClientMgrLocal clientMgr;

    @Override
    public String execute() throws Exception {

	if (!isCanReassign()) {
	    this.addActionError("Your account does not have permission to perform this action");
	    return ERROR;
	}

	if (clientNote == null) {
	    this.addActionError("Could not locate Client note with ID " + this.getNoteId());
	    return ERROR;
	}

	clientNote.setCategoryCode(categoryCode);
	clientMgr.updateClientNote(clientNote);

	this.addActionMessage("The Note has been reassigned");

	return SUCCESS;
    }

    public String getCategoryCode() {
	return categoryCode;
    }

    @RequiredStringValidator(message = "Please select a category")
    public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
    }

}
