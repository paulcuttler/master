package com.ract.client;

import java.rmi.RemoteException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;

/**
 * <p>
 * Object to represent the merge client transient data.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class MergeClientVO {
    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public final static String WRITABLE_CLASSNAME = "Client";

    public void write(ClassWriter cw) throws RemoteException {
	throw new RemoteException("Not to be written as a client");
    }

    public MergeClientVO(Node clientNode) {
	NodeList nodeList = clientNode.getChildNodes();
	Node childNode = null;
	String nodeName = null;
	String nodeValue = null;
	for (int i = 0; i < nodeList.getLength(); i++) {
	    childNode = nodeList.item(i);
	    nodeName = childNode.getNodeName();
	    if (childNode.hasChildNodes()) {
		nodeValue = childNode.getFirstChild().getNodeValue();
	    } else {
		nodeValue = null;
	    }
	    if (nodeName != null && nodeName.length() > 0) {
		nodeValue = nodeValue == null ? nodeValue : nodeValue.trim();
		if ("DuplicateClientNo".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.duplicateClientNumber = new Integer(nodeValue);
		    }
		}
		if ("MasterClientNo".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.masterClientNumber = new Integer(nodeValue);
		    }
		}
		// if("ClientNo".equalsIgnoreCase(nodeName))
		// {
		// if(nodeValue != null && nodeValue.length() > 0)
		// {
		// this.masterClientNumber = new Integer(nodeValue);
		// }
		// }
		if ("ClientStatus".equalsIgnoreCase(nodeName)) {
		    if (nodeValue != null && nodeValue.length() > 0) {
			this.status = nodeValue;
		    }
		} else if ("audit".equalsIgnoreCase(nodeName)) {
		    if (childNode.getNodeType() == Node.ELEMENT_NODE) {
			Element userElement = (Element) childNode;
			NodeList userIdList = userElement.getElementsByTagName("UserID");
			if (userIdList != null) {
			    // Assume only one "UserId" tag.
			    if (userIdList.item(0).hasChildNodes()) {
				this.userId = userIdList.item(0).getFirstChild().getNodeValue();
			    }
			}
		    }
		}

	    }
	}
    }

    private String userId;
    private String status;
    private Integer masterClientNumber;
    private Integer duplicateClientNumber;

    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public Integer getMasterClientNumber() {
	return masterClientNumber;
    }

    public void setMasterClientNumber(Integer masterClientNumber) {
	this.masterClientNumber = masterClientNumber;
    }

    public Integer getDuplicateClientNumber() {
	return duplicateClientNumber;
    }

    public void setDuplicateClientNumber(Integer duplicateClientNumber) {
	this.duplicateClientNumber = duplicateClientNumber;
    }

    public MergeClientVO(Integer masterClientNumber) {
	// setClientNumber(clientNumber);
	setMasterClientNumber(masterClientNumber);
    }

    // dummy methods

    public ClientVO create() {
	return null;
    }

    public String getAddressTitle() {
	return null;
    }

    public String getPostalName() {
	return null;
    }

    public String getDisplayName() {
	return null;
    }

    public String getSalutation() {
	return null;
    }

    public String getClientType() {
	return null;
    }

    public ClientVO copy() {
	return null;
    }

    public Boolean getSex() {
	return null;
    }
}
