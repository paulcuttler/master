package com.ract.client;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.common.Auditable;
import com.ract.util.DateTime;

@Entity
public class NoteType implements Auditable {

    public final static String TYPE_INFORMATION = "Information";

    public final static String TYPE_FOLLOW_UP = "Followup";

    public final static String TYPE_ADDRESS_UNKNOWN_SMS = "SMS-AU";

    public final static String TYPE_ADDRESS_UNKNOWN_EMAIL = "Email-AU";

    public final static String TYPE_ADDRESS_UNKNOWN_PHONE = "Phone-AU";

    public final static String TYPE_ADDRESS_UNKNOWN_NO_CONTACT = "No Contact-AU";

    public boolean isReferral() {
	return referral;
    }

    public void setReferral(boolean referral) {
	this.referral = referral;
    }

    public boolean isFollowup() {
	return followup;
    }

    public void setFollowup(boolean followup) {
	this.followup = followup;
    }

    public DateTime getLastUpdate() {
	return lastUpdate;
    }

    public void setLastUpdate(DateTime lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateId() {
	return lastUpdateId;
    }

    public void setLastUpdateId(String lastUpdateId) {
	this.lastUpdateId = lastUpdateId;
    }

    public boolean isCommentable() {
	return commentable;
    }

    public void setCommentable(boolean commentable) {
	this.commentable = commentable;
    }

    public boolean isRequiresCompletion() {
	return requiresCompletion;
    }

    public void setRequiresCompletion(boolean requiresCompletion) {
	this.requiresCompletion = requiresCompletion;
    }

    public String getNoteType() {
	return noteType;
    }

    public void setNoteType(String noteType) {
	this.noteType = noteType;
    }

    public boolean isNotifiable() {
	return notifiable;
    }

    public void setNotifiable(boolean notifiable) {
	this.notifiable = notifiable;
    }

    public boolean isBlockTransactions() {
	return blockTransactions;
    }

    public void setBlockTransactions(boolean blockTransactions) {
	this.blockTransactions = blockTransactions;
    }

    public String getNotificationEmailAddress() {
	return notificationEmailAddress;
    }

    public void setNotificationEmailAddress(String notificationEmailAddress) {
	this.notificationEmailAddress = notificationEmailAddress;
    }

    @Id
    private String noteType;

    private boolean notifiable;

    private boolean referral;

    private boolean followup;

    private boolean blockTransactions;

    private boolean addressUnknown;

    public boolean isAddressUnknown() {
	return addressUnknown;
    }

    public void setAddressUnknown(boolean addressUnknown) {
	this.addressUnknown = addressUnknown;
    }

    private String notificationEmailAddress;

    private DateTime lastUpdate;

    private String lastUpdateId;

    private boolean requiresCompletion;

    private boolean commentable;

}
