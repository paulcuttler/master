package com.ract.client;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.user.User;
import com.ract.user.UserMgr;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

public class CategoryDetailAction extends ActionSupport {
    public NoteCategory getCategory() {
	return category;
    }

    public void setCategory(NoteCategory category) {
	this.category = category;
    }

    public String getNotificationName() {
	return notificationName;
    }

    public void setNotificationName(String notificationName) {
	this.notificationName = notificationName;
    }

    public DateTime getFollowUpDate() {
	return followUpDate;
    }

    public void setFollowUpDate(DateTime followUpDate) {
	this.followUpDate = followUpDate;
    }

    public String getCategoryCode() {
	return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
    }

    @InjectEJB(name = "UserMgrBean")
    private UserMgr userMgr;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgr clientMgr;

    private String categoryCode;

    private String notificationName;

    private DateTime followUpDate;

    @Override
    public String execute() throws Exception {
	LogUtil.debug(this.getClass(), "execute");
	LogUtil.debug(this.getClass(), "categoryCode=" + categoryCode);
	if (categoryCode != null && !categoryCode.equals("")) {
	    category = clientMgr.getNoteCategory(categoryCode);
	    LogUtil.debug(this.getClass(), "category=" + category);
	    DateTime now = new DateTime();
	    followUpDate = now.add(new Interval(0, 0, category.getFollowUpDays(), 0, 0, 0, 0));
	    LogUtil.debug(this.getClass(), "followUpDate=" + followUpDate);
	    category.setFollowupDate(followUpDate.formatShortDate());
	    LogUtil.debug(this.getClass(), "notificationName=" + notificationName);
	    if (category.getNotificationUserId() != null && !category.getNotificationUserId().trim().equals("")) {
		User user = userMgr.getUser(category.getNotificationUserId());
		notificationName = user.getUsername() + " (" + user.getEmailAddress() + ")";
	    } else {
		notificationName = category.getNotificationEmailAddress();
	    }
	}

	return SUCCESS;
    }

    private NoteCategory category;

}
