package com.ract.client;

import java.io.Serializable;

import com.ract.util.DateTime;

public class MarketOptionPK implements Serializable {
    Integer clientNo;
    String subSystem;
    String medium;
    DateTime createDateTime;

    /**
     * @hibernate.key-property
     * @hibernate.column name="clientNo"
     */
    public Integer getClientNo() {
	return clientNo;
    }

    /**
     * @hibernate.key-property
     * @hibernate.column name="createDateTime"
     */
    public DateTime getCreateDateTime() {
	return createDateTime;
    }

    /**
     * @hibernate.key-property
     * @hibernate.column name="medium"
     */
    public String getMedium() {
	return medium;
    }

    /**
     * @hibernate.key-property
     * @hibernate.column name="subSystem"
     */
    public String getSubSystem() {
	return subSystem;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    public void setCreateDateTime(DateTime createDateTime) {
	this.createDateTime = createDateTime;
    }

    public void setMedium(String medium) {
	this.medium = medium;
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    public boolean equals(Object ob) {
	MarketOptionPK mpk = (MarketOptionPK) ob;
	return this.clientNo.equals(mpk.clientNo) && this.createDateTime.equals(mpk.createDateTime) && this.medium.equals(mpk.medium) && this.subSystem.equals(mpk.subSystem);
    }

    public int hashCode() {
	return this.clientNo.hashCode() + this.createDateTime.hashCode() + this.medium.hashCode() + this.subSystem.hashCode();
    }

    public String toString() {
	return this.clientNo + " " + this.subSystem + " " + this.medium + " " + this.createDateTime.formatLongDate();
    }
}
