package com.ract.client.notifier;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ract.common.SystemException;
import com.ract.common.notifier.NotificationEvent;
import com.ract.util.LogUtil;

/**
 * Holds details about the change made to a Progres client.
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class ProgressClientChangeNotificationEvent extends NotificationEvent {
    // The Node object is not Serializable so if the notification event fails
    // and gets saved as a NotificationEventFailure object we don't save the
    // node
    // object and only save the xml. When we get the client node we
    // recreate the Node from the xml if we have to.
    private transient Node clientNode;

    private String clientXML;

    private String action;

    private Integer clientNumber;

    public ProgressClientChangeNotificationEvent(Element clientElement, String clientAction) throws SystemException {

	if (clientElement == null) {
	    throw new SystemException("The clientElement parameter is null.");
	}

	if (!"ProgressClient".equalsIgnoreCase(clientElement.getNodeName())) {
	    throw new SystemException("The clientElement parameter is not a 'ProgressClient' element.");
	}

	this.clientNode = clientElement;
	// Save the xml string that defines the client node so that it can be
	// saved with a NotificationFailureEvent if required.
	this.clientXML = this.clientNode.toString();

	// Find an action for the client in the client nodes attributes
	this.action = clientAction;
	// Find the client number
	NodeList nodeList = clientElement.getElementsByTagName("ClientNo");
	if (nodeList != null && nodeList.getLength() > 0) {
	    if (nodeList.getLength() > 1) {
		throw new SystemException("The ProgressClient element contains too many 'ClientNo' tags.");
	    } else {
		String clientNumberStr = null;
		try {
		    clientNumberStr = nodeList.item(0).getFirstChild().getNodeValue();
		    this.clientNumber = new Integer(clientNumberStr);
		} catch (Exception e) {
		    throw new SystemException("The ProgressClient element contains an invalid 'ClientNo' tag value (" + clientNumberStr + ").");
		}
	    }
	} else {
	    throw new SystemException("The ProgressClient element does not contain a 'ClientNo' tag.");
	}
	if (this.clientNumber.intValue() == 0) {
	    throw new SystemException("The client number must be greater than zero.");
	}

	// change this based on action- eg. transfer should be synchronous
	if (this.isMerge()) {
	    this.setEventMode(NotificationEvent.MODE_SYNCHRONOUS);
	} else {
	    // set publisher to not wait for response
	    this.setEventMode(NotificationEvent.MODE_ASYNCHRONOUS);
	}

	setEventName(NotificationEvent.EVENT_PROGRESS_CLIENT_CHANGE);
    }

    public Node getClientNode() {
	if (this.clientNode == null) {
	    // Reconstitute the client node from the xml data
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setValidating(false);
	    try {
		DocumentBuilder builder = factory.newDocumentBuilder();
		StringReader reader = new StringReader(this.clientXML);
		InputSource source = new InputSource(reader);
		Document document = builder.parse(source);
		if (document != null) {
		    this.clientNode = document.getDocumentElement();
		}
	    } catch (ParserConfigurationException pce) {
		LogUtil.fatal(this.getClass(), pce);
	    } catch (IOException ioe) {
		LogUtil.fatal(this.getClass(), ioe);
	    } catch (SAXException se) {
		LogUtil.fatal(this.getClass(), se);
	    }
	}
	return this.clientNode;
    }

    public String getAction() {
	return this.action;
    }

    public Integer getClientNumber() {
	return this.clientNumber;
    }

    public String getClientXML() {
	return this.clientXML;
    }

    public boolean isCreate() {
	return "create".equalsIgnoreCase(this.action);
    }

    public boolean isUpdate() {
	return "update".equalsIgnoreCase(this.action);
    }

    public boolean isDelete() {
	return "delete".equalsIgnoreCase(this.action);
    }

    public boolean isMerge() {
	return "merge".equalsIgnoreCase(this.action);
    }

    public String toString() {
	StringBuffer sb = new StringBuffer(this.getEventName());
	sb.append(", action=");
	sb.append(this.action);
	sb.append(", clientNumber=");
	sb.append(this.clientNumber);
	return sb.toString();
    }

    public String getDetail() {
	return this.clientXML;
    }

}
