package com.ract.client.notifier;

import com.ract.client.ClientVO;
import com.ract.common.notifier.NotificationEvent;

/**
 * This is a specific notification event for telling other event subscribers
 * about a change to a clients data.
 * 
 * @author Terry Bakker
 * @created 27 February 2003
 * @version 1.0
 */

public class ClientChangeNotificationEvent extends NotificationEvent {
    public final static String ACTION_CREATE = "Create";

    public final static String ACTION_UPDATE = "Update";

    public final static String ACTION_REMOVE = "Remove";

    public final static String ACTION_MERGE = "Merge";

    private String action = null;

    private Integer clientNumber = null;

    private ClientVO clientVO = null;

    private boolean progressInitiated = false;

    /**
     * Constructor for the ClientChangeNotificationEvent object
     * 
     * @param action
     *            Description of the Parameter
     * @param clientNumber
     *            Description of the Parameter
     */
    public ClientChangeNotificationEvent(String action, Integer clientNumber) {
	this.action = action;
	this.clientNumber = clientNumber;
	setEventName(NotificationEvent.EVENT_CLIENT_CHANGE);
    }

    /**
     * Constructor for the ClientChangeNotificationEvent object
     * 
     * @param action
     *            Description of the Parameter
     * @param cvo
     *            Description of the Parameter
     */
    public ClientChangeNotificationEvent(String action, ClientVO cvo) {
	this.eventMode = this.MODE_ASYNCHRONOUS;
	this.action = action;
	this.clientVO = cvo;
	this.clientNumber = cvo.getClientNumber();
	setEventName(NotificationEvent.EVENT_CLIENT_CHANGE);
    }

    /**
     * Gets the action attribute of the ClientChangeNotificationEvent object
     * 
     * @return The action value
     */
    public String getAction() {
	return this.action;
    }

    /**
     * Gets the clientNumber attribute of the ClientChangeNotificationEvent
     * object
     * 
     * @return The clientNumber value
     */
    public Integer getClientNumber() {
	return this.clientNumber;
    }

    /**
     * Gets the clientVO attribute of the ClientChangeNotificationEvent object
     * 
     * @return The clientVO value
     */
    public ClientVO getClientVO() {
	return this.clientVO;
    }

    /**
     * Gets the createAction attribute of the ClientChangeNotificationEvent
     * object
     * 
     * @return The createAction value
     */
    public boolean isCreateAction() {
	return this.ACTION_CREATE.equals(this.action);
    }

    /**
     * Gets the updateAction attribute of the ClientChangeNotificationEvent
     * object
     * 
     * @return The updateAction value
     */
    public boolean isUpdateAction() {
	return this.ACTION_UPDATE.equals(this.action);
    }

    public boolean isRemoveAction() {
	return this.ACTION_REMOVE.equals(this.action);
    }

    public boolean isMergeAction() {
	return this.ACTION_MERGE.equals(this.action);
    }

    public boolean isProgressInitiated() {
	return this.progressInitiated;
    }

    public void setProgressInitiated(boolean initiated) {
	this.progressInitiated = initiated;
    }

    public String toString() {
	StringBuffer str = new StringBuffer();
	str.append(getEventName());
	str.append(", action=");
	str.append(this.action);
	str.append(", client=");
	str.append(this.clientNumber);
	str.append(", progressInitiated=");
	str.append(this.progressInitiated);
	return str.toString();
    }

}
