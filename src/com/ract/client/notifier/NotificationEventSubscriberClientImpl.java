package com.ract.client.notifier;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.client.ClientEJBHelper;
import com.ract.common.SourceSystem;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationEventSubscriber;
import com.ract.util.LogUtil;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public class NotificationEventSubscriberClientImpl implements NotificationEventSubscriber, Serializable {

    public NotificationEventSubscriberClientImpl() {
    }

    public void processNotificationEvent(NotificationEvent event) throws RemoteException {
	LogUtil.log(this.getClass(), "processNotificationEvent in NotificationEventSubscriberClientImpl=" + event);
	if (event != null && event.eventEquals(NotificationEvent.EVENT_PROGRESS_CLIENT_CHANGE)) {
	    ClientEJBHelper.getClientMgrLocal().processNotificationEvent(event);
	}
    }

    public String getSubscriberName() {
	return SourceSystem.CLIENT.getAbbreviation();
    }
}
