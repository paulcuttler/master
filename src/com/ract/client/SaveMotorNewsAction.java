package com.ract.client;

import java.rmi.RemoteException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.FileUtil;

public class SaveMotorNewsAction extends ActionSupport implements SessionAware {

    private Map session;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    public String getMotorNewsSendOption() {
	return motorNewsSendOption;
    }

    public void setMotorNewsSendOption(String motorNewsSendOption) {
	this.motorNewsSendOption = motorNewsSendOption;
    }

    public String getMotorNewsDeliveryMethod() {
	return motorNewsDeliveryMethod;
    }

    public void setMotorNewsDeliveryMethod(String motorNewsDeliveryMethod) {
	this.motorNewsDeliveryMethod = motorNewsDeliveryMethod;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public boolean isDisableClientManagement() {
	return disableClientManagement;
    }

    public void setDisableClientManagement(boolean disableClientManagement) {
	this.disableClientManagement = disableClientManagement;
    }

    public boolean isDisableEditMarketing() {
	return disableEditMarketing;
    }

    public void setDisableEditMarketing(boolean disableEditMarketing) {
	this.disableEditMarketing = disableEditMarketing;
    }

    private String motorNewsSendOption;

    private String motorNewsDeliveryMethod;

    private Integer clientNumber;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private ClientVO client;

    private boolean disableClientManagement = false;

    private boolean disableEditMarketing = false;

    /**
     * @return
     */
    public String execute() {

	try {
	    client = clientMgr.getClient(clientNumber);
	} catch (Exception e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	if (client == null) {
	    addActionError("No client found for client number " + clientNumber);
	    return ERROR;
	}

	client.setMotorNewsDeliveryMethod(motorNewsDeliveryMethod);
	client.setMotorNewsSendOption(motorNewsSendOption);

	UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);

	User user = userSession.getUser();
	String userId = user.getUserID();
	String transactionReason = Client.HISTORY_UPDATE;
	String salesBranchCode = userSession.getUser().getSalesBranchCode();
	String comment = "Journeys Magazine update";
	ClientTransaction clientTx = new ClientTransaction(ClientTransaction.TRANSACTION_UPDATE, client, userId, transactionReason, salesBranchCode, comment);
	try {
	    clientTx.submit();
	} catch (RemoteException e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	try {
	    this.disableClientManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableClientManagement"));
	} catch (Exception e) {
	}

	try {
	    this.disableEditMarketing = Boolean.parseBoolean(FileUtil.getProperty("master", "disableEditMarketing"));
	} catch (Exception e) {
	}

	return SUCCESS;
    }
}