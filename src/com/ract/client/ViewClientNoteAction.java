package com.ract.client;

import java.util.Collection;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.ract.security.Privilege;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.LogUtil;

public class ViewClientNoteAction extends ActionSupport implements SessionAware, Preparable {

    private Integer noteId;
    private boolean canUnblockTransactions;
    private boolean canMarkComplete;
    private boolean canReassign;

    private NoteCategory noteCategory;
    private Map session = null;
    protected ClientNote clientNote;

    private ClientMgrLocal clientMgr = ClientEJBHelper.getClientMgrLocal();

    private ClientVO client;

    private NoteType noteType;
    private Collection<NoteCategory> categoryList;

    @Override
    public void prepare() throws Exception {
	UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);

	categoryList = clientMgr.getFollowUpNoteCategories();
	clientNote = clientMgr.getClientNote(noteId);

	noteType = clientMgr.getNoteType(clientNote.getNoteType());
	if (noteType.isFollowup()) {
	    noteCategory = clientMgr.getNoteCategory(clientNote.getCategoryCode());
	}

	if (us != null) {
	    canMarkComplete = ClientHelper.canMarkComplete(noteType, clientNote, noteCategory, us);
	    canUnblockTransactions = ClientHelper.canUnblockTransactions(noteType, clientNote, noteCategory, us);
	    canReassign = us.getUser().isPrivilegedUser(Privilege.PRIVILEGE_REASSIGN_NOTE);
	} else {
	    LogUtil.log(getClass(), "UserSession is null");

	}

	client = clientMgr.getClient(clientNote.getClientNumber());
	clientNumber = client.getClientNumber();

	LogUtil.debug(this.getClass(), "canMarkComplete " + canMarkComplete);
	LogUtil.debug(this.getClass(), "canUnblockTransactions " + canUnblockTransactions);
	LogUtil.debug(this.getClass(), "noteCategory=" + noteCategory);
    }

    public boolean isCanMarkComplete() {
	return canMarkComplete;
    }

    public void setCanMarkComplete(boolean canMarkComplete) {
	this.canMarkComplete = canMarkComplete;
    }

    public boolean isCanUnblockTransactions() {
	return canUnblockTransactions;
    }

    public void setCanUnblockTransactions(boolean canUnblockTransactions) {
	this.canUnblockTransactions = canUnblockTransactions;
    }

    public NoteCategory getNoteCategory() {
	return noteCategory;
    }

    public void setNoteCategory(NoteCategory noteCategory) {
	this.noteCategory = noteCategory;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    public NoteType getNoteType() {
	return noteType;
    }

    public void setNoteType(NoteType noteType) {
	this.noteType = noteType;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    private Integer clientNumber;

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    public Collection<NoteCategory> getCategoryList() {
	return categoryList;
    }

    public void setCategoryList(Collection<NoteCategory> categoryList) {
	this.categoryList = categoryList;
    }

    public boolean isCanReassign() {
	return canReassign;
    }

    public void setCanReassign(boolean canReassign) {
	this.canReassign = canReassign;
    }
}
