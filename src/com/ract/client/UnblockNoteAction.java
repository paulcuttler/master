package com.ract.client;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.DateTime;

public class UnblockNoteAction extends ActionSupport implements SessionAware {

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private Map session = null;

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    private ClientNote clientNote;

    private Integer noteId;

    private ClientVO client;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private Integer clientNumber;

    @Override
    public String execute() throws Exception {
	UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	clientNote = clientMgr.getClientNote(noteId);
	clientNote.setBlockTransactions(false);
	clientNote.setUnblockDate(new DateTime());
	clientNote.setUnblockId(us.getUserId());
	clientMgr.updateClientNote(clientNote);
	client = clientMgr.getClient(clientNote.getClientNumber());
	clientNumber = clientNote.getClientNumber();
	return SUCCESS;
    }

}
