package com.ract.client;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;

public class ShowEditMotorNewsAction extends ActionSupport implements ServletRequestAware {

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private Integer clientNumber;

    private ClientVO client;

    /**
     * @return
     */
    public String execute() {

	try {
	    client = clientMgr.getClient(clientNumber);
	    // support legacy methodology
	    servletRequest.setAttribute("client", client);
	} catch (Exception e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	return SUCCESS;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }
}