package com.ract.client;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.ract.common.Auditable;
import com.ract.util.DateTime;

@Entity
public class NoteCategory implements Auditable {

    public static final String NOTE_CATEOGRY_IT_ACTION = "IT";

    public String getLastUpdateId() {
	return lastUpdateId;
    }

    public void setLastUpdateId(String lastUpdateId) {
	this.lastUpdateId = lastUpdateId;
    }

    public String getNotificationUserId() {
	return notificationUserId;
    }

    public void setNotificationUserId(String notificationUserId) {
	this.notificationUserId = notificationUserId;
    }

    public int getFollowUpDays() {
	return followUpDays;
    }

    public String getFollowupDate() {
	return followupDate;
    }

    public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
    }

    public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    public void setFollowUp(boolean followUp) {
	this.followUp = followUp;
    }

    public void setFollowUpDays(int followUpDays) {
	this.followUpDays = followUpDays;
    }

    public void setNotifiable(boolean notifiable) {
	this.notifiable = notifiable;
    }

    public void setNotificationEmailAddress(String notificationEmailAddress) {
	this.notificationEmailAddress = notificationEmailAddress;
    }

    public void setLastUpdate(DateTime lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    public void setFollowupDate(String followupDate) {
	this.followupDate = followupDate;
    }

    public String getCategoryCode() {
	return categoryCode;
    }

    public String getCategoryName() {
	return categoryName;
    }

    public String getBusinessType() {
	return businessType;
    }

    public boolean isFollowUp() {
	return followUp;
    }

    public boolean isNotifiable() {
	return notifiable;
    }

    public String getNotificationEmailAddress() {
	return notificationEmailAddress;
    }

    public DateTime getLastUpdate() {
	return lastUpdate;
    }

    private String notificationUserId;

    @Id
    private String categoryCode;

    private String categoryName;

    private String businessType;

    private boolean followUp;

    private int followUpDays;

    private boolean notifiable;

    private String notificationEmailAddress;

    private DateTime lastUpdate;

    private String lastUpdateId;

    @Transient
    private String followupDate;
}
