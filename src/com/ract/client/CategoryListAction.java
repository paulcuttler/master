package com.ract.client;

import java.util.Collection;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;

public class CategoryListAction extends ActionSupport {

    public String getBusinessType() {
	return businessType;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    public Collection<NoteCategory> getCategoryList() {
	return categoryList;
    }

    public void setCategoryList(Collection<NoteCategory> categoryList) {
	this.categoryList = categoryList;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgr clientMgr;

    private String businessType;

    private Collection<NoteCategory> categoryList;

    @Override
    public String execute() throws Exception {
	categoryList = clientMgr.getNoteCategories(businessType);

	return SUCCESS;
    }

}
