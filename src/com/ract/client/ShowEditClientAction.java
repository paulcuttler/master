package com.ract.client;

import java.rmi.RemoteException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.ui.ClientUIConstants;
import com.ract.client.ui.ClientUIHelper;

public class ShowEditClientAction extends ActionSupport implements ServletRequestAware {

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    private String eventName;

    public String getEventName() {
	return eventName;
    }

    public void setEventName(String eventName) {
	this.eventName = eventName;
    }

    public String getPageHeading() {
	return pageHeading;
    }

    public void setPageHeading(String pageheading) {
	this.pageHeading = pageheading;
    }

    private String pageHeading;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private Integer clientNumber;

    private ClientVO client;

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    private boolean askBirthDate;

    private boolean askHomePhone;

    private boolean askWorkPhone;

    private boolean askMobilePhone;

    private boolean askEmailAddress;

    private String branchName;

    /**
     * @return
     */
    public String execute() {

	pageHeading = "Edit";
	eventName = ClientUIConstants.ACTION_UPDATE_CLIENT;
	try {
	    client = clientMgr.getClient(clientNumber);
	} catch (Exception e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	try {
	    askBirthDate = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_BIRTH_DATE, client.getBirthDate() != null ? client.getBirthDate().formatShortDate() : client.getBirthDateString());
	    askEmailAddress = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_EMAIL, client.getEmailAddress());
	    askHomePhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_HOME_PHONE, client.getHomePhone());
	    askMobilePhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_MOBILE_PHONE, client.getMobilePhone());
	    askWorkPhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_WORK_PHONE, client.getWorkPhone());
	} catch (Exception e) {
	    e.printStackTrace();
	    addActionError("Unable to determine if field should be asked. " + e.getMessage());
	    return ERROR;
	}

	branchName = ClientUIHelper.formatBranchString(client);

	if (client.getGroupClient()) {
	    addActionError("You may not edit group clients in the customer system.");
	    return ERROR;
	}

	servletRequest.setAttribute("client", client);
	try {
	    servletRequest.setAttribute("streetAddress", client.getResidentialAddress());
	    servletRequest.setAttribute("postalAddress", client.getPostalAddress());
	} catch (RemoteException e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}
	servletRequest.setAttribute("displayResidentialAddress", true);
	servletRequest.setAttribute("displayPostalAddress", true);

	return SUCCESS;
    }

    public boolean isAskBirthDate() {
	return askBirthDate;
    }

    public void setAskBirthDate(boolean askBirthDate) {
	this.askBirthDate = askBirthDate;
    }

    public boolean isAskHomePhone() {
	return askHomePhone;
    }

    public void setAskHomePhone(boolean askHomePhone) {
	this.askHomePhone = askHomePhone;
    }

    public boolean isAskWorkPhone() {
	return askWorkPhone;
    }

    public void setAskWorkPhone(boolean askWorkPhone) {
	this.askWorkPhone = askWorkPhone;
    }

    public boolean isAskMobilePhone() {
	return askMobilePhone;
    }

    public void setAskMobilePhone(boolean askMobilePhone) {
	this.askMobilePhone = askMobilePhone;
    }

    public boolean isAskEmailAddress() {
	return askEmailAddress;
    }

    public void setAskEmailAddress(boolean askEmailAddress) {
	this.askEmailAddress = askEmailAddress;
    }

    public String getBranchName() {
	return branchName;
    }

    public void setBranchName(String branchName) {
	this.branchName = branchName;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }
}