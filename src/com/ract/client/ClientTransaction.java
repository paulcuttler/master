package com.ract.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.SystemException;

/**
 * <p>
 * Represent a client transaction.
 * </p>
 * <p>
 * Control the transaction so that multiple submissions cannot be performed -
 * this only the first step. See the implementation of TransactionGroup for
 * details on how this class should be implemented.
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class ClientTransaction implements Serializable {

    // public static final String KEY = "clientTransactionKey";
    //
    // private String clientTransactionKey = DateUtil.formatDate(new DateTime(),
    // "MMddHHmmssSS");

    public static final String SYSTEM_ROADSIDE = "RSS";

    public static final String TRANSACTION_UPDATE = "ClientUpdate";

    public static final String TRANSACTION_CREATE = "ClientCreate";

    private String transactionType;

    private String userID;
    private String transactionReason;
    private String salesBranchCode;
    private String comment;
    private String sourceSystem;

    public ClientTransaction() {
    }

    /**
     * Main constructor
     * 
     * @param transactionType
     * @param client
     * @param userID
     * @param transactionReason
     * @param salesBranchCode
     * @param comment
     * @param sourceSystem
     */
    public ClientTransaction(String transactionType, ClientVO client, String userID, String transactionReason, String salesBranchCode, String comment) {
	this.setClient(client);
	this.setTransactionType(transactionType);
	this.setUserID(userID);
	this.setTransactionReason(transactionReason);
	this.setSalesBranchCode(salesBranchCode);
	this.setComment(comment);
	this.setSourceSystem(SYSTEM_ROADSIDE);
    }

    /**
     * The client to perform the transaction on.
     */
    private ClientVO client;

    /**
     * Submit the transaction
     */
    public synchronized void submit() throws RemoteException {
	if (!this.submitted) {
	    this.submitted = true;

	    ClientMgrLocal clientMgrLocal = ClientEJBHelper.getClientMgrLocal();

	    if (this.transactionType.equals(this.TRANSACTION_UPDATE)) {

		this.client = clientMgrLocal.updateClient(client, userID, transactionReason, salesBranchCode, comment, sourceSystem);
	    } else if (this.transactionType.equals(this.TRANSACTION_CREATE)) {
		this.client = clientMgrLocal.createClient(client, userID, transactionReason, salesBranchCode, comment, sourceSystem);

	    }

	} else {
	    throw new SystemException("Transaction has already been submitted.");
	}

    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
	oos.defaultWriteObject();
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
	ois.defaultReadObject();
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public void setSourceSystem(String sourceSystem) {
	this.sourceSystem = sourceSystem;
    }

    public void setTransactionReason(String transactionReason) {
	this.transactionReason = transactionReason;
    }

    public void setTransactionType(String transactionType) {
	this.transactionType = transactionType;
    }

    public void setUserID(String userID) {
	this.userID = userID;
    }

    /**
     * The updated client
     * 
     * @return
     */
    public ClientVO getClient() {
	return client;
    }

    private boolean submitted;
}
