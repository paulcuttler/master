package com.ract.client;

import java.util.regex.Matcher;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.BaseAction;
import com.ract.util.LogUtil;

public class NoteTypeAction extends BaseAction implements ModelDriven, Preparable, ValidationAware {

    @Override
    public void validate() {
	if (model.getNoteType() == null || model.getNoteType().length() == 0) {
	    addFieldError("noteType", "Note type must be entered");
	}
	if (model.getNotificationEmailAddress() == null || model.getNotificationEmailAddress().length() == 0) {
	    addFieldError("notificationEmailAddress", "Notification Email address must be entered");
	}
	if (model.getNotificationEmailAddress() != null) {
	    // unable to get model driven field validator to work so...

	    Matcher m = PATTERN_EMAIL.matcher(model.getNotificationEmailAddress());
	    if (!m.find()) {
		addFieldError("notificationEmailAddress", "Notification email address is not valid.");
	    }
	}
    }

    public void setModel(NoteType model) {
	this.model = model;
    }

    public String getRequestId() {
	return requestId;
    }

    public void setRequestId(String requestId) {
	this.requestId = requestId;
    }

    @InjectEJB(name = "ClientMgrBean")
    protected ClientMgrLocal clientMgr;

    @Override
    public String list() {
	list = clientMgr.getNoteTypes();
	return LIST;
    }

    private NoteType model;

    @Override
    public Object getModel() {
	return model;
    }

    private String requestId;

    @Override
    public void prepare() throws Exception {
	LogUtil.log(this.getClass(), "requestId=" + (getRequestId() == null ? "null" : getRequestId().toString()));
	if (getRequestId() == null) {
	    model = new NoteType();
	} else {
	    model = (NoteType) commonMgr.get(NoteType.class, getRequestId());
	    LogUtil.log(this.getClass(), "model=" + model);
	}
    }

}
