package com.ract.client;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;

public class AddNoteCommentAction extends ActionSupport {

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private ClientNote clientNote;

    private ClientVO client;

    private Integer noteId;

    @Override
    public String execute() throws Exception {
	clientNote = clientMgr.getClientNote(noteId);
	client = clientMgr.getClient(clientNote.getClientNumber());
	return SUCCESS;
    }

}
