package com.ract.client;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.opensymphony.xwork2.validator.annotations.Validation;
import com.ract.common.BusinessType;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.common.MailMgrLocal;
import com.ract.common.SalesBranchVO;
import com.ract.membership.MembershipMgrLocal;
import com.ract.security.Privilege;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

@Validation
public class SaveNoteAction extends ActionSupport implements ModelDriven, SessionAware, Preparable, ValidationAware {

    public boolean isCanMarkComplete() {
	return canMarkComplete;
    }

    public void setCanMarkComplete(boolean canMarkComplete) {
	this.canMarkComplete = canMarkComplete;
    }

    public boolean isCanUnblockTransactions() {
	return canUnblockTransactions;
    }

    public void setCanUnblockTransactions(boolean canUnblockTransactions) {
	this.canUnblockTransactions = canUnblockTransactions;
    }

    public NoteType getNoteType() {
	return noteType;
    }

    public void setNoteType(NoteType noteType) {
	this.noteType = noteType;
    }

    public NoteCategory getNoteCategory() {
	return noteCategory;
    }

    public void setNoteCategory(NoteCategory noteCategory) {
	this.noteCategory = noteCategory;
    }

    public Collection<SalesBranchVO> getSalesBranchList() {
	return salesBranchList;
    }

    public void setSalesBranchList(Collection<SalesBranchVO> salesBranchList) {
	this.salesBranchList = salesBranchList;
    }

    public String getContactDate() {
	return contactDate;
    }

    public void setContactDate(String contactDate) {
	this.contactDate = contactDate;
    }

    public String getContactTime() {
	return contactTime;
    }

    public void setContactTime(String contactTime) {
	this.contactTime = contactTime;
    }

    public Collection<BusinessType> getBusinessTypes() {
	return businessTypes;
    }

    public void setBusinessTypes(Collection<BusinessType> businessTypes) {
	this.businessTypes = businessTypes;
    }

    @Override
    public void validate() {
	LogUtil.debug(this.getClass(), "validate");

	if (clientNote.getBusinessType() == null || clientNote.getBusinessType().length() == 0) {
	    addFieldError("businessType", "Business type must be entered");
	}

	if (clientNote == null || clientNote.getNoteType().length() == 0) {
	    addFieldError("noteType", "Note Type must be entered.");
	}

	if (clientNote.getClientNumber() == null) {
	    addFieldError("clientNumber", "Client number must be entered.");
	}
	if (clientNote.getNoteText() == null || clientNote.getNoteText().length() == 0) {
	    addFieldError("noteText", "Note text must be entered.");
	}

	if (clientNote.getNoteType() != null) {
	    noteType = clientMgr.getNoteType(clientNote.getNoteType());
	    if (noteType != null) {
		if (noteType.isBlockTransactions() && !BusinessType.BUSINESS_TYPE_MEMBERSHIP.equals(clientNote.getBusinessType())) {
		    addActionError("Blocker notes may only be created for the membership business type.");
		}
	    }
	}

	if (noteType != null && noteType.isReferral()) {
	    LogUtil.debug(this.getClass(), "b clientNote.getNoteType()=" + clientNote.getNoteType());

	    if (contactDate != null && !contactDate.trim().equals("")) {
		DateTime preferredContactDate = null;
		try {
		    preferredContactDate = new DateTime(ClientMgrBean.FORMAT_DATE.parse(contactDate));

		    DateTime now = new DateTime().getDateOnly();
		    if (preferredContactDate.before(now)) {
			addFieldError("contactDate", "Contact date must not be before today.");
		    }

		    // set after validation
		    clientNote.setPreferredContactDate(preferredContactDate);
		} catch (ParseException e1) {
		    addFieldError("contactDate", "Contact date is not in the required format dd/MM/yyyy. eg. 21/03/1989.");
		}
	    }

	    if (contactTime != null && !contactTime.trim().equals("")) {
		DateTime preferredContactTime = null;
		try {
		    preferredContactTime = new DateTime(ClientMgrBean.FORMAT_TIME.parse(contactTime));
		    // set after validation
		    clientNote.setPreferredContactTime(ClientMgrBean.FORMAT_TIME.format(preferredContactTime));
		} catch (ParseException e1) {
		    addFieldError("contactTime", "Contact time is not in the required format HH:mm PM. eg. 10:00 PM.");
		}
	    }

	    final int PHONE_LENGTH = 10;
	    if (clientNote.getPreferredContactNumber().length() == PHONE_LENGTH) {
		// digits only
		Pattern digitPattern = Pattern.compile("[0-9]");
		Matcher digitMatcher = digitPattern.matcher(clientNote.getPreferredContactNumber());
		if (!digitMatcher.find()) {
		    addFieldError("preferredContactNumber", "Contact number should only contain digits.");
		}
	    } else if (!clientNote.getPreferredContactNumber().trim().equals("") && clientNote.getPreferredContactNumber().length() != PHONE_LENGTH) {
		addFieldError("preferredContactNumber", "Contact number should be 10 digits.");
	    }

	    if (clientNote.getSourceId() == null) {
		addFieldError("sourceId", "Referree must be selected for a referral.");
	    }

	    if (clientNote.getTargetId().equals(clientNote.getTargetSalesBranch()) || // both
										      // empty
										      // or
										      // both
										      // the
										      // same
		    (!clientNote.getTargetSalesBranch().trim().equals("") && !clientNote.getTargetId().trim().equals(""))) // both
															   // with
															   // a
															   // value
	    {
		addActionError("Either a referree or sales branch must be selected.");
	    }
	}

	if (noteType != null && noteType.isFollowup()) {
	    if (clientNote.getCategoryCode() == null) {
		addActionError("Category must be entered for a followup.");
	    }
	    if (clientNote.getDue() == null) {
		addActionError("Due date must be entered. It is calculated using the category.");
	    }
	}

	if (noteType != null && !noteType.isFollowup()) {
	    if (clientNote != null && clientNote.getCategoryCode() != null && clientNote.getDue() != null) {
		addActionError("A due date has been entered but the note type is not a followup. Either change the note type back to a followup note type or " + "add the note again.");
	    }
	}

	if (clientNumber != null) {
	    try {
		client = clientMgr.getClient(clientNumber);
	    } catch (RemoteException e) {
		addActionError("Client number must not be empty.");
	    }
	}

    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public Collection<NoteCategory> getCategoryList() {
	return categoryList;
    }

    public void setCategoryList(Collection<NoteCategory> categoryList) {
	this.categoryList = categoryList;
    }

    public Collection<User> getSourceUserList() {
	return sourceUserList;
    }

    public void setSourceUserList(Collection<User> sourceUserList) {
	this.sourceUserList = sourceUserList;
    }

    public Collection<User> getTargetUserList() {
	return targetUserList;
    }

    public void setTargetUserList(Collection<User> targetUserList) {
	this.targetUserList = targetUserList;
    }

    // public String getReferenceId()
    // {
    // return referenceId;
    // }
    //
    //
    //
    // public void setReferenceId(String referenceId)
    // {
    // this.referenceId = referenceId;
    // }

    public String getBusinessType() {
	return businessType;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    public Collection<User> getSourceList() {
	return sourceList;
    }

    public void setSourceList(Collection<User> sourceList) {
	this.sourceList = sourceList;
    }

    @Override
    public void prepare() throws Exception {
	LogUtil.log(this.getClass(), "prepare");
	businessTypes = commonMgr.getBusinessTypes();
	noteTypeList = clientMgr.getNoteTypes();
	categoryList = clientMgr.getNoteCategories();
	sourceUserList = userMgr.getActiveUsers();
	targetUserList = userMgr.getActiveUsers();
	salesBranchList = commonMgr.getSalesBranchList();

	if (clientNumber != null) {
	    client = clientMgr.getClient(clientNumber);
	}
    }

    private Collection<SalesBranchVO> salesBranchList;

    private Collection<BusinessType> businessTypes;

    private Integer clientNumber;

    private Collection<NoteCategory> categoryList;

    private Collection<User> sourceUserList;

    private Collection<User> targetUserList;

    // private String referenceId;

    private String businessType;

    public Collection<NoteType> getNoteTypeList() {
	return noteTypeList;
    }

    @InjectEJB(name = "UserMgrBean")
    private UserMgrLocal userMgr;

    public void setNoteTypeList(Collection<NoteType> noteTypeList) {
	this.noteTypeList = noteTypeList;
    }

    public Collection<ClientNote> getClientNoteList() {
	return clientNoteList;
    }

    public void setClientNoteList(Collection<ClientNote> clientNoteList) {
	this.clientNoteList = clientNoteList;
    }

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    @InjectEJB(name = "MailMgrBean")
    private MailMgrLocal mailMgr;

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    private ClientNote clientNote = new ClientNote();

    @Override
    public Object getModel() {
	return clientNote;
    }

    @InjectEJB(name = "ClientMgrBean")
    ClientMgrLocal clientMgr;

    private ClientVO client;

    private Collection<ClientNote> clientNoteList;

    private String contactDate;

    private String contactTime;

    private NoteType noteType;

    private Collection<User> sourceList;

    private Collection<NoteType> noteTypeList = new ArrayList<NoteType>();

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private boolean canMarkComplete;

    private boolean canUnblockTransactions;

    private Map session = null;

    public String execute() {
	LogUtil.log(this.getClass(), "a clientNote.getNoteType()=" + clientNote.getNoteType());
	// eg. 21/02/1976 11:00 PM

	// clientNote.setReferenceId(referenceId);

	LogUtil.log(this.getClass(), "execute");
	try {

	    LogUtil.log(this.getClass(), "session=" + session);
	    UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	    clientNote.setCreated(new DateTime());
	    clientNote.setCreateId(us.getUserId());
	    LogUtil.log(this.getClass(), "us=" + us);

	    LogUtil.log(this.getClass(), "clientNote=" + clientNote);

	    boolean canReassign = us.getUser().isPrivilegedUser(Privilege.PRIVILEGE_REASSIGN_NOTE);
	    if (clientNote.getCategoryCode().equalsIgnoreCase(NoteCategory.NOTE_CATEOGRY_IT_ACTION) && !canReassign) {
		throw new Exception("You do not have permission to select this Category.");
	    }

	    clientMgr.createClientNote(clientNote);

	    LogUtil.log(this.getClass(), "clientNoteList");
	    clientNoteList = clientMgr.getClientNoteList(clientNumber);
	    LogUtil.log(this.getClass(), "clientNoteList=" + clientNoteList);

	    noteType = clientMgr.getNoteType(clientNote.getNoteType());

	    canMarkComplete = ClientHelper.canMarkComplete(noteType, clientNote, noteCategory, us);
	    canUnblockTransactions = ClientHelper.canUnblockTransactions(noteType, clientNote, noteCategory, us);

	    LogUtil.log(this.getClass(), "canMarkComplete " + canMarkComplete);
	    LogUtil.log(this.getClass(), "canUnblockTransactions " + canUnblockTransactions);
	} catch (Exception e) {
	    LogUtil.log(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
	    addActionError(e.getMessage());
	    return ERROR;
	}

	return SUCCESS;
    }

    private NoteCategory noteCategory;

}
