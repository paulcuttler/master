package com.ract.client;

import java.io.Serializable;

import com.ract.util.DateTime;

/**
 * <p>
 * The publication type PK.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class ClientSubscriptionPK implements Serializable {
    private Integer clientNumber;

    private String subscriptionCode;

    private String subscriptionStatus;

    private DateTime subscriptionDate;

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public void setSubscriptionCode(String subscriptionCode) {
	this.subscriptionCode = subscriptionCode;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
	this.subscriptionStatus = subscriptionStatus;
    }

    public void setSubscriptionDate(DateTime subscriptionDate) {
	this.subscriptionDate = subscriptionDate;
    }

    /**
     * @hibernate.key-property position="1" column="client_number"
     */
    public Integer getClientNumber() {
	return clientNumber;
    }

    /**
     * @hibernate.key-property position="2" column="subscription_code"
     */
    public String getSubscriptionCode() {
	return subscriptionCode;
    }

    /**
     * @hibernate.key-property position="3" column="subscription_status"
     */
    public String getSubscriptionStatus() {
	return subscriptionStatus;
    }

    /**
     * @hibernate.key-property position="4" column="subscription_date"
     */
    public DateTime getSubscriptionDate() {
	return subscriptionDate;
    }

    private ClientSubscriptionPK() {

    }

    public ClientSubscriptionPK(Integer clientNumber, String subscriptionCode, String subscriptionStatus, DateTime subscriptionDate) {
	this.setClientNumber(clientNumber);
	this.setSubscriptionCode(subscriptionCode);
	this.setSubscriptionStatus(subscriptionStatus);
	this.setSubscriptionDate(subscriptionDate);
    }

    /**
     * ABSOLUTELY MANDATORY METHODS FOR COMPOSITE KEYS OR RESULTS IN NPE
     */

    public boolean equals(Object o) {
	return super.equals(o);
    }

    public int hashCode() {
	return super.hashCode();
    }

    public String toString() {
	return this.getClientNumber() + " " + this.getSubscriptionCode() + " " + this.subscriptionStatus + " " + this.getSubscriptionDate().formatLongDate();
    }

}
