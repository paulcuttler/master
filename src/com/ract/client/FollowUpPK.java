package com.ract.client;

import java.io.Serializable;

import com.ract.util.DateTime;

public class FollowUpPK implements Serializable {
    private Integer systemNumber;
    private String subSystem;
    private Integer changedClientNo;
    private DateTime createDate;

    /**
     * @hibernate.key-property
     * @hibernate.column name="createDate"
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    /**
     * @hibernate.key-property
     * @hibernate.column name="changedClientNo"
     */
    public Integer getChangedClientNo() {
	return changedClientNo;
    }

    /**
     * @hibernate.key-property
     * @hibernate.column name="subSystem"
     */
    public String getSubSystem() {
	return subSystem;
    }

    public void setSystemNumber(Integer systemNumber) {
	this.systemNumber = systemNumber;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public void setChangedClientNo(Integer changedClientNo) {
	this.changedClientNo = changedClientNo;
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    /**
     * @hibernate.key-property
     * @hibernate.column name="systemNumber"
     */

    public Integer getSystemNumber() {
	return systemNumber;
    }

    public FollowUpPK() {
    }

    public int hashCode() {
	return this.systemNumber.hashCode() + this.subSystem.hashCode() + this.changedClientNo.hashCode() + this.createDate.hashCode();
    }

    public boolean equals(Object obj) {
	FollowUpPK fup = (FollowUpPK) obj;
	return fup.systemNumber.equals(this.systemNumber) && fup.subSystem.equals(this.subSystem) && fup.changedClientNo.equals(changedClientNo) && fup.createDate.equals(createDate);

    }

    @Override
    public String toString() {
	return "FollowUpPK [changedClientNo=" + changedClientNo + ", createDate=" + createDate + ", subSystem=" + subSystem + ", systemNumber=" + systemNumber + "]";
    }
}
