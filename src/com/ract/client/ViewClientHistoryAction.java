package com.ract.client;

import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class ViewClientHistoryAction extends ActionSupport implements ServletRequestAware {

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private String modifyDateTimeString;

    private Integer clientNumber;

    private ClientVO client;

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    private ClientHistory clientHistory;
    private ClientHistory prevClientHistory;

    public ClientHistory getClientHistory() {
	return clientHistory;
    }

    public void setClientHistory(ClientHistory clientHistory) {
	this.clientHistory = clientHistory;
    }

    public ClientHistory getPrevClientHistory() {
	return prevClientHistory;
    }

    public void setPrevClientHistory(ClientHistory prevClientHistory) {
	this.prevClientHistory = prevClientHistory;
    }

    /**
     * @return
     */
    public String execute() {

	// setup pk
	ClientHistoryPK clientHistoryPK = new ClientHistoryPK();
	clientHistoryPK.setClientNumber(clientNumber);
	// consider custom
	DateTime modifyDateTime = null;
	try {
	    modifyDateTime = new DateTime(modifyDateTimeString);
	} catch (ParseException e) {
	    addActionError("Unable to parse modify date. " + e.getMessage());
	    return ERROR;
	}
	clientHistoryPK.setModifyDateTime(modifyDateTime);
	LogUtil.log(this.getClass(), "clientHistoryPK=" + clientHistoryPK);

	ClientHistory tmp = null;
	Collection<ClientHistory> clientHistoryList = clientMgr.getClientHistory(clientNumber); // descending
												// order?
	boolean foundHistory = false;
	for (Iterator<ClientHistory> i = clientHistoryList.iterator(); i.hasNext() && !foundHistory;) {
	    tmp = i.next();
	    LogUtil.log(this.getClass(), "tmp=" + tmp.getClientHistoryPK());
	    if (tmp.getClientHistoryPK().equals(clientHistoryPK)) {
		clientHistory = tmp;
		foundHistory = true;
		if (i.hasNext()) {
		    prevClientHistory = i.next();
		}
	    }
	}

	servletRequest.setAttribute("clientHistory", clientHistory);

	client = clientHistory.getClient();
	client.setClientNumber(clientNumber);

	if (prevClientHistory != null) {
	    servletRequest.setAttribute("prevClient", prevClientHistory.getClient());
	}

	return SUCCESS;
    }

    public String getModifyDateTimeString() {
	return modifyDateTimeString;
    }

    public void setModifyDateTimeString(String modifyDateTimeString) {
	this.modifyDateTimeString = modifyDateTimeString;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }
}