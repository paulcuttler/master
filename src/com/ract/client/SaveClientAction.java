package com.ract.client;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.ui.ClientUIConstants;
import com.ract.common.AddressVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.security.Privilege;
import com.ract.security.ui.LoginUIConstants;
import com.ract.travel.TramadaClientProfileImporter;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

public class SaveClientAction extends ActionSupport implements SessionAware, ModelDriven, ServletRequestAware, Preparable, ValidationAware {

    private Integer clientNumber;

    private boolean askBirthDate;

    private boolean askHomePhone;

    private boolean askWorkPhone;

    private boolean askMobilePhone;

    private boolean askEmailAddress;

    private boolean disableClientManagement = false;

    private boolean disableEditMarketing = false;

    @Override
    public void validate() {

	if (!ignoreMatches) {

	    validatePhoneNumber(client, NumberUtil.PHONE_TYPE_HOME);
	    validatePhoneNumber(client, NumberUtil.PHONE_TYPE_WORK);
	    validatePhoneNumber(client, NumberUtil.PHONE_TYPE_MOBILE);

	    // fix birth date
	    if (client.getBirthDateString() != null && !client.getBirthDateString().trim().equals("")) {
		if (client.getBirthDateString().equals(ClientVO.FIELD_STATUS_REFUSED) || client.getBirthDateString().equals(ClientVO.FIELD_STATUS_TO_FOLLOW)) {
		    try {
			client.setBirthDate(null);
		    } catch (Exception e) {
			addActionError("Unable to reset birth date. " + e.getMessage());
		    }
		} else {
		    try {
			DateTime birthDate = new DateTime(client.getBirthDateString());

			client.setBirthDate(birthDate);
			client.setBirthDateString(null); // birthDateString only
							 // contains Refused or
							 // tTo Follow.
		    } catch (Exception e) {
			addActionError("Unable to process birth date field. " + client.getBirthDateString() + " is not valid. " + e.getMessage());
		    }
		}
	    } else {
		if (client.getBirthDate() == null) {
		    addActionError("Birth date must be entered.");
		}
	    }

	    // validate if duplicate
	    if (Client.STATUS_DUPLICATE.equals(client.getStatus())) {
		try {
		    clientMgr.validateDuplicateClient(client);
		} catch (Exception e) {
		    addActionError("Unable to validate duplicate client. " + e.getMessage());
		}
	    }

	    // validate email address
	    if (client.getEmailAddress() != null) {
		try {
		    StringUtil.validateEmail(client.getEmailAddress(), true);
		} catch (GenericException e) {
		    addActionError("Unable to validate email address. " + e.getMessage());
		}
	    } else {
		addActionError("Email address must be entered.");
	    }

	}
	LogUtil.log(this.getClass(), "actionErrors=" + getActionErrors());
    }

    private void validatePhoneNumber(ClientVO client, String phoneType) {
	String phoneNumber = null;
	if (phoneType.equals(NumberUtil.PHONE_TYPE_HOME)) {
	    phoneNumber = client.getHomePhone();
	} else if (phoneType.equals(NumberUtil.PHONE_TYPE_WORK)) {
	    phoneNumber = client.getWorkPhone();
	} else if (phoneType.equals(NumberUtil.PHONE_TYPE_MOBILE)) {
	    phoneNumber = client.getMobilePhone();
	} else {
	    addActionError("Unknown phone type " + phoneType);
	}

	try {
	    if (phoneNumber != null && !phoneNumber.equals("")) {
		if (phoneNumber.equalsIgnoreCase(ClientVO.FIELD_STATUS_NOT_APPLICABLE) || phoneNumber.equalsIgnoreCase(ClientVO.FIELD_STATUS_REFUSED) || phoneNumber.equalsIgnoreCase(ClientVO.FIELD_STATUS_TO_FOLLOW)) {
		    LogUtil.log(this.getClass(), "valid");
		    // valid
		} else {
		    LogUtil.log(this.getClass(), "phoneNumber=" + phoneNumber);
		    NumberUtil.validatePhoneNumber(phoneType, phoneNumber);
		}
	    } else {
		addActionError(phoneType + " phone number must be entered");
	    }
	} catch (Exception e) {
	    addActionError("Unable to validate home phone number. " + e.getMessage());
	}
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    @Override
    public void prepare() throws Exception {
	if (clientNumber != null) {
	    client = clientMgr.getClient(clientNumber);
	} else {
	    client = new ClientVO();
	}
    }

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    private ClientVO client;

    public ClientVO getModel() {
	return client;
    }

    private String group;

    private String sms;

    public String getGroup() {
	return group;
    }

    public void setGroup(String group) {
	this.group = group;
    }

    public String getSms() {
	return sms;
    }

    public void setSms(String sms) {
	this.sms = sms;
    }

    private boolean createTramadaClient;

    private Map session;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private String eventName;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private Collection<ClientHistory> clientHistoryList;

    public Collection<ClientHistory> getClientHistoryList() {
	return clientHistoryList;
    }

    public void setClientHistoryList(Collection<ClientHistory> clientHistoryList) {
	this.clientHistoryList = clientHistoryList;
    }

    /**
     * @return
     */
    public String execute() {

	try {
	    this.disableClientManagement = Boolean.parseBoolean(FileUtil.getProperty("master", "disableClientManagement"));
	} catch (Exception e) {
	}

	if (this.disableClientManagement) {
	    addActionError("Client management is not permitted in Roadside.");
	    return ERROR;
	}

	UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	User user = userSession.getUser();
	DateTime now = new DateTime();// .getDateOnly();

	// set up collections
	Collection<ClientVO> clientMatchList = new ArrayList();

	if (!ignoreMatches) {

	    LogUtil.log(this.getClass(), "client=" + client);

	    // fix sex
	    if (client.getGender().equals(ClientVO.GENDER_MALE)) {
		client.setSex(true);
	    } else {
		client.setSex(false);
	    }

	    // fix sms
	    if (sms.equalsIgnoreCase("Yes")) {
		client.setSmsAllowed(true);
	    } else {
		client.setSmsAllowed(false);
	    }

	    // fix group
	    if (group.equalsIgnoreCase("Yes")) {
		client.setGroupClient(true);
	    } else {
		client.setGroupClient(false);
	    }

	    client.setDerivedFields();

	    LogUtil.debug(this.getClass(), "createTramadaClient=" + createTramadaClient);

	    String surname = client.getSurname();

	    Integer streetNumber = null;
	    Integer streetSuburbID = null;
	    try {
		streetNumber = client.getResidentialAddress().getStreetNumber();
		streetSuburbID = client.getResidentialAddress().getStreetSuburbID();
	    } catch (RemoteException e) {
		addActionError(e.getMessage());
		return ERROR;
	    }

	    String initials = client.getInitials();
	    String title = client.getTitle();
	    String homePhone = client.getHomePhone();
	    Boolean sex = client.getSex();
	    DateTime birthDate = client.getBirthDate();

	    clientAddressList = new ArrayList();
	    if (streetNumber != null) {
		try {
		    clientAddressList = clientMgr.findClientsByNameAndAddress(surname, streetNumber, streetSuburbID, sex);
		} catch (RemoteException e) {
		    addActionError("Unable to find clients by name and address. " + e.getMessage());
		    return ERROR;
		}
	    }
	    clientNameBirthList = new ArrayList();
	    if (birthDate != null) {
		try {
		    clientNameBirthList = clientMgr.findClientsByNameAndBirthDate(initials, surname, birthDate);
		} catch (RemoteException e) {
		    addActionError("Unable to find clients by name and birth date. " + e.getMessage());
		    return ERROR;
		}
	    }
	    try {
		clientNameTitleList = clientMgr.findClientsByNameAndTitle(initials, surname, title);
	    } catch (RemoteException e) {
		addActionError("Unable to find clients by name and title. " + e.getMessage());
		return ERROR;
	    }

	    clientPhoneList = new ArrayList();
	    if (homePhone != null && !homePhone.equals("") && !homePhone.equalsIgnoreCase(ClientVO.FIELD_STATUS_NOT_APPLICABLE) && !homePhone.equalsIgnoreCase(ClientVO.FIELD_STATUS_REFUSED) && !homePhone.equalsIgnoreCase(ClientVO.FIELD_STATUS_TO_FOLLOW)) {
		try {
		    clientPhoneList = clientMgr.findClientsByHomePhone(homePhone);
		} catch (RemoteException e) {
		    addActionError("Unable to find clients by phone number. " + e.getMessage());
		    return ERROR;
		}
	    }

	    // remove current client from each collection
	    removeClientFromList(client, clientAddressList);
	    removeClientFromList(client, clientNameBirthList);
	    removeClientFromList(client, clientNameTitleList);
	    removeClientFromList(client, clientPhoneList);

	    // remove the current client from the matches
	    clientMatchList.addAll(clientAddressList);
	    clientMatchList.addAll(clientNameBirthList);
	    clientMatchList.addAll(clientNameTitleList);
	    clientMatchList.addAll(clientPhoneList);

	} else {
	    // from matching client screen
	    client = (ClientVO) session.get("client");
	}

	// if sum of lists = 0 or already asked to acknowlege matches then save
	// without showing the match screen
	if (clientMatchList.size() == 0 || ignoreMatches) {
	    LogUtil.debug(this.getClass(), "clientMatchList is empty or ignoreMatches " + ignoreMatches);
	    // //no matches so bypass the match screen
	    if (eventName.equals(ClientUIConstants.ACTION_UPDATE_CLIENT)) {
		LogUtil.debug(this.getClass(), "action is save");
		try {
		    updateClient(user, now);
		} catch (Exception e) {
		    e.printStackTrace();
		    addActionError("Unable to update client " + clientNumber + ". " + e.getMessage());
		    return ERROR;
		}
	    } else if (eventName.equals(ClientUIConstants.ACTION_CREATE_CLIENT)) {
		LogUtil.debug(this.getClass(), "action is create");
		try {
		    createClient(user, now);
		} catch (Exception e) {
		    e.printStackTrace();
		    addActionError("Unable to create client. " + e.getMessage());
		    return ERROR;
		}
	    }

	    LogUtil.debug(this.getClass(), "client=" + client);

	    clientHistoryList = clientMgr.getClientHistory(clientNumber);

	    try {
		askBirthDate = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_BIRTH_DATE, client.getBirthDate() != null ? client.getBirthDate().formatShortDate() : client.getBirthDateString());
		askEmailAddress = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_EMAIL, client.getEmailAddress());
		askHomePhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_HOME_PHONE, client.getHomePhone());
		askMobilePhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_MOBILE_PHONE, client.getMobilePhone());
		askWorkPhone = clientMgr.isAskable(clientNumber, ClientVO.FIELD_NAME_WORK_PHONE, client.getWorkPhone());
	    } catch (Exception e) {
		e.printStackTrace();
		addActionError("Unable to determine if field should be asked. " + e.getMessage());
		return ERROR;
	    }

	    return "view";
	} else {

	    // put client in the session
	    session.put("client", client);

	    try {
		clientAddress = client.getResidentialAddress().getSingleLineAddress();
	    } catch (Exception e) {
		addActionError(e.getMessage());
		return ERROR;
	    }

	    // legacy - avoids rewriting buildClientSummaryHTML...for now.
	    servletRequest.setAttribute("clientAddressList", clientAddressList);
	    servletRequest.setAttribute("clientNameBirthList", clientNameBirthList);
	    servletRequest.setAttribute("clientNameTitleList", clientNameTitleList);
	    servletRequest.setAttribute("clientPhoneList", clientPhoneList);

	    LogUtil.debug(this.getClass(), "forward to client match list = " + clientMatchList.size());
	    return "matchingClients";
	}

    }

    public boolean isAskBirthDate() {
	return askBirthDate;
    }

    public void setAskBirthDate(boolean askBirthDate) {
	this.askBirthDate = askBirthDate;
    }

    public boolean isAskHomePhone() {
	return askHomePhone;
    }

    public void setAskHomePhone(boolean askHomePhone) {
	this.askHomePhone = askHomePhone;
    }

    public boolean isAskWorkPhone() {
	return askWorkPhone;
    }

    public void setAskWorkPhone(boolean askWorkPhone) {
	this.askWorkPhone = askWorkPhone;
    }

    public boolean isAskMobilePhone() {
	return askMobilePhone;
    }

    public void setAskMobilePhone(boolean askMobilePhone) {
	this.askMobilePhone = askMobilePhone;
    }

    public boolean isAskEmailAddress() {
	return askEmailAddress;
    }

    public void setAskEmailAddress(boolean askEmailAddress) {
	this.askEmailAddress = askEmailAddress;
    }

    private String clientAddress;

    private boolean ignoreMatches;

    public String getClientAddress() {
	return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
	this.clientAddress = clientAddress;
    }

    public boolean isIgnoreMatches() {
	return ignoreMatches;
    }

    public void setIgnoreMatches(boolean ignoreMatches) {
	this.ignoreMatches = ignoreMatches;
    }

    private void removeClientFromList(ClientVO clientVO, Collection<ClientVO> clientList) {
	if (clientList != null && !clientList.isEmpty()) {
	    ClientVO tmp = null;
	    // remove client from the list
	    Iterator<ClientVO> clientIt = clientList.iterator();
	    while (clientIt.hasNext()) {
		tmp = clientIt.next();
		if (clientVO.getClientNumber() != null && clientVO.getClientNumber().equals(tmp.getClientNumber())) {
		    // remove the current item from the list
		    clientIt.remove();
		}

	    }
	}
    }

    private Collection clientAddressList;

    private Collection clientNameBirthList;

    public boolean isCreateTramadaClient() {
	return createTramadaClient;
    }

    public void setCreateTramadaClient(boolean createTramadaClient) {
	this.createTramadaClient = createTramadaClient;
    }

    public String getEventName() {
	return eventName;
    }

    public void setEventName(String eventName) {
	this.eventName = eventName;
    }

    public Collection getClientAddressList() {
	return clientAddressList;
    }

    public void setClientAddressList(Collection clientAddressList) {
	this.clientAddressList = clientAddressList;
    }

    public Collection getClientNameBirthList() {
	return clientNameBirthList;
    }

    public void setClientNameBirthList(Collection clientNameBirthList) {
	this.clientNameBirthList = clientNameBirthList;
    }

    public Collection getClientNameTitleList() {
	return clientNameTitleList;
    }

    public void setClientNameTitleList(Collection clientNameTitleList) {
	this.clientNameTitleList = clientNameTitleList;
    }

    public Collection getClientPhoneList() {
	return clientPhoneList;
    }

    public void setClientPhoneList(Collection clientPhoneList) {
	this.clientPhoneList = clientPhoneList;
    }

    private Collection clientNameTitleList;

    private Collection clientPhoneList;

    private void updateClient(User user, DateTime lastUpdate) throws Exception {
	if (client == null) {
	    client = (ClientVO) session.get("client");
	}
	if (client == null) {
	    throw new SystemException("Unable to get client reference.");
	}

	client.setMadeDate(client.getMadeDate()); // fix made date issue
						  // //.getDateOnly()
	client.setLastUpdate(lastUpdate); // .getDateOnly()

	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	ClientVO oldClient = clientMgr.getClient(client.getClientNumber());

	// clear these values as Roadside doesn't know about then
	if (!oldClient.getResidentialAddress().equals((AddressVO) client.getResidentialAddress())) {
	    client.setResiLatitude(null);
	    client.setResiLongitude(null);
	    client.setResiGnaf(null);
	    client.setResiAusbar(null);
	    client.setResiDpid(null);
	}
	if (!oldClient.getPostalAddress().equals((AddressVO) client.getPostalAddress())) {
	    client.setPostLatitude(null);
	    client.setPostLongitude(null);
	    client.setPostGnaf(null);
	    client.setPostAusbar(null);
	    client.setPostDpid(null);
	}

	String transactionReason = Client.HISTORY_UPDATE;
	String comment = "";
	ClientTransaction clientTx = new ClientTransaction(ClientTransaction.TRANSACTION_UPDATE, client, user.getUserID(), transactionReason, user.getUserID(), comment);
	clientTx.submit();

	if (!oldClient.getResidentialAddress().equals((AddressVO) client.getResidentialAddress()) || !oldClient.getPostalAddress().equals((AddressVO) client.getPostalAddress()) || (!oldClient.getHomePhone().equals(client.getHomePhone()))) {
	    // check if address updater is on
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    final String ADDRESS_UPDATER_ENABLED = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_CLIENT, SystemParameterVO.ADDRESS_UPDATER_ENABLED);
	    boolean ignoreChange = false;

	    if (oldClient.getResidentialAddress().equals((AddressVO) client.getResidentialAddress()) && oldClient.getPostalAddress().equals((AddressVO) client.getPostalAddress()) && !oldClient.getHomePhone().equals(client.getHomePhone()) // only
																													      // home
																													      // phone
																													      // changed
		    && (oldClient.getHomePhone() == null || oldClient.getHomePhone().trim().isEmpty() // home
												      // phone
												      // was
												      // blank
		    ) && (client.getHomePhone().equals(ClientVO.FIELD_STATUS_REFUSED) // home
										      // phone
										      // now
										      // "Refused",
										      // "To Follow",
										      // "Not Applicable"
			    || client.getHomePhone().equals(ClientVO.FIELD_STATUS_TO_FOLLOW) || client.getHomePhone().equals(ClientVO.FIELD_STATUS_NOT_APPLICABLE))) {
		ignoreChange = true; // don't trigger AU
	    }

	    if (ADDRESS_UPDATER_ENABLED != null && !ADDRESS_UPDATER_ENABLED.equals("") && ADDRESS_UPDATER_ENABLED.equalsIgnoreCase("Yes") && !ignoreChange) {
		// pop up address change window
		int tStamp = new DateTime().getSecondsFromMidnight();
		ChangedAddress ca = new ChangedAddress();
		ca.setTimeStamp(tStamp);
		ca.setUserid(user.getUserID());
		ca.setClient(oldClient);

		if (user.isPrivilegedUser(Privilege.PRIVILEGE_ADDRESS_UPDATER)) {
		    // System.out.println("POPUP THE ADDRESS UPDATER WINDOW");
		    session.put("changedAddress", ca);

		    // legacy - support servlets
		    servletRequest.setAttribute("showAddressUpdater", "true");
		    servletRequest.setAttribute("slsbch", user.getSalesBranchCode());
		    servletRequest.setAttribute("userId", user.getUserID());
		    servletRequest.setAttribute("tStamp", tStamp + "");
		} else {
		    // System.out.println("SAVE THE RECORD TO THE DATABASE");
		    // create popup request record for this one and
		    // email to the required destination.
		    clientMgr.savePopupRequest(ClientMgrBean.POPUP_ADDRESS_UPDATE, user.getUserID(), user.getSalesBranchCode(), tStamp, oldClient);
		}
	    }
	}

	client = clientTx.getClient();
    }

    private void createClient(User user, DateTime lastUpdate) throws Exception {
	if (client == null) {
	    client = (ClientVO) session.get("client");
	}

	if (client == null) {
	    throw new SystemException("Unable to get client.");
	}

	client.setMadeID(user.getUserID());
	client.setMadeDate(lastUpdate); // .getDateOnly()
	client.setLastUpdate(lastUpdate);

	String transactionReason = Client.HISTORY_CREATE;
	String comment = "Client created";
	ClientTransaction clientTx = new ClientTransaction(ClientTransaction.TRANSACTION_CREATE, client, user.getUserID(), transactionReason, user.getSalesBranchCode(), comment);
	clientTx.submit();

	if (createTramadaClient) {
	    ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    clientMgr.transferTramadaClientXML(Client.TRAMADA_MODE_EXPLICIT, TramadaClientProfileImporter.TRAMADA_ACTION_ADD, user.getUserID(), user.getSalesBranchCode(), client);
	}

	client = clientTx.getClient();
	clientNumber = client.getClientNumber();

    }

    /**
     * @return the disableEditMarketing
     */
    public boolean isDisableEditMarketing() {
	return disableEditMarketing;
    }

    /**
     * @param disableEditMarketing
     *            the disableEditMarketing to set
     */
    public void setDisableEditMarketing(boolean disableEditMarketing) {
	this.disableEditMarketing = disableEditMarketing;
    }

}