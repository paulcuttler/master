package com.ract.client;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

@Entity
public class ClientNoteComment implements Serializable {

    public ClientNoteCommentPK getClientNoteCommentPK() {
	return clientNoteCommentPK;
    }

    public void setClientCommentNotePK(ClientNoteCommentPK clientNoteCommentPK) {
	this.clientNoteCommentPK = clientNoteCommentPK;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getCreateId() {
	return createId;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public DateTime getCreated() {
	return created;
    }

    public void setCreated(DateTime created) {
	this.created = created;
    }

    @Id
    private ClientNoteCommentPK clientNoteCommentPK;

    private String comment;

    private String createId;

    private DateTime created;

}
