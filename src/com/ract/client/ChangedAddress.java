package com.ract.client;

import java.io.Serializable;

/**
 * <p>
 * Manages authentication for client address change popup
 * </p>
 * <p>
 * and related actions
 * </p>
 * 
 * @author dgk
 * @version 1.0
 */

public class ChangedAddress implements Serializable {
    private Integer clientNo;
    private String resiProperty, resiStreetChar, postProperty, postStreetChar;
    private Integer resiStsubid, postStsubid;
    private int timeStamp;
    private String response;
    private String surname;
    private String userid;
    private String homePhone;

    /**
     * getResponse
     * 
     * @return String
     */
    public String getResponse() {
	return this.response;
    }

    public void setClient(ClientVO client) {
	this.clientNo = client.getClientNumber();
	this.postProperty = client.getPostProperty();
	this.postStreetChar = client.getPostStreetChar();
	this.postStsubid = client.getPostStsubId();
	this.resiProperty = client.getResiProperty();
	this.resiStreetChar = client.getResiStreetChar();
	this.resiStsubid = client.getResiStsubId();
	this.surname = client.getSurname();
	this.homePhone = client.getHomePhone();

    }

    public Integer getClientNo() {
	return clientNo;
    }

    public String getPostProperty() {
	return postProperty;
    }

    public Integer getPostStsubid() {
	return postStsubid;
    }

    public String getPostStreetChar() {
	return postStreetChar;
    }

    public String getResiProperty() {
	return resiProperty;
    }

    public String getResiStreetChar() {
	return resiStreetChar;
    }

    public Integer getResiStsubid() {
	return resiStsubid;
    }

    public int getTimeStamp() {
	return this.timeStamp;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    public void setPostStreetChar(String postStreetChar) {
	this.postStreetChar = postStreetChar;
    }

    public void setPostStsubid(Integer postStsubid) {
	this.postStsubid = postStsubid;
    }

    public void setResiProperty(String resiProperty) {
	this.resiProperty = resiProperty;
    }

    public void setResiStreetChar(String resiStreetChar) {
	this.resiStreetChar = resiStreetChar;
    }

    public void setResiStsubid(Integer resiStsubid) {
	this.resiStsubid = resiStsubid;
    }

    public void setResponse(String response) {
	this.response = response;
    }

    public void setTimeStamp(int timeStamp) {
	this.timeStamp = timeStamp;
    }

    public void setPostProperty(String postProperty) {
	this.postProperty = postProperty;
    }

    public String getSurname() {
	return this.surname;
    }

    public void setSurname(String surname) {
	this.surname = surname;
    }

    public String getUserid() {
	return userid;
    }

    public void setUserid(String userid) {
	this.userid = userid;
    }

    public String getHomePhone() {
	return homePhone;
    }

    public void setHomePhone(String homePhone) {
	this.homePhone = homePhone;
    }

}
