package com.ract.client;

import java.rmi.RemoteException;

import org.apache.log4j.Category;

/**
 * Client id cache
 * 
 * @author John Holliday
 * @version 1.0
 */

public class ClientIDCache {

    private static ClientIDCache instance = new ClientIDCache();

    private int lastClientID = 0;

    private int lastOccupationID = 0;

    public ClientIDCache() {
    }

    /**
     * Return a reference to the one and only instance of this class.
     */
    public static ClientIDCache getInstance() {
	return instance;
    }

    /**
     * Return the next ID in the Client ID sequence. Must be synchronised so
     * that two threads don't do the initialisation at the same time.
     */
    // public synchronized int getNextClientID(ClientIDMgrBean idMgr)
    // throws RemoteException
    // {
    // if(this.lastClientID == 0)
    // {
    // this.lastClientID = idMgr.getLastClientID();
    // log("Initialising last client ID to : " + this.lastClientID);
    // }
    // return++this.lastClientID;
    // }

    /**
     * Return the next ID in the Occupation ID sequence. Must be synchronised so
     * that two threads don't do the initialisation at the same time.
     */
    public synchronized int getNextOccupationID(ClientIDMgrBean idMgr) throws RemoteException {
	if (this.lastOccupationID == 0) {
	    this.lastOccupationID = idMgr.getLastOccupationID();
	    log("Initialising last client ID to : " + this.lastOccupationID);
	}
	return ++this.lastOccupationID;
    }

    private void log(String msg) {
	Category cat = Category.getInstance(this.getClass().getName());
	cat.info(msg);
    }

}
