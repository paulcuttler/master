package com.ract.client;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.ExceptionHelper;
import com.ract.common.SourceSystem;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class MarkNoteCancelledAction extends ActionSupport implements SessionAware {

    private Map session = null;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private ClientNote clientNote;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private Integer noteId;

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    /**
     * @return
     */
    public String execute() {
	LogUtil.log(this.getClass(), "MarkNoteCancelledAction.execute start");
	UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	LogUtil.debug(this.getClass(), "us=" + us);
	try {

	    clientNote = clientMgr.getClientNote(noteId);

	    ClientVO client = clientMgr.getClient(clientNote.getClientNumber());
	    LogUtil.debug(this.getClass(), "client=" + client);

	    // cancel means bounced in the context of emails
	    if (clientNote.getNoteType().equals(NoteType.TYPE_ADDRESS_UNKNOWN_EMAIL)) {
		LogUtil.debug(this.getClass(), "Cancel email");
		// update client and clear email address
		client.setEmailAddress(null);
		clientMgr.updateClient(client, us.getUserId(), Client.HISTORY_UPDATE, us.getUser().getSalesBranchCode(), "Email bounced", SourceSystem.CLIENT.getAbbreviation());
	    } else if (clientNote.getNoteType().equals(NoteType.TYPE_ADDRESS_UNKNOWN_SMS)) {
		// update client and clear mobile phone
		LogUtil.debug(this.getClass(), "Cancel sms");
		client.setMobilePhone(null);
		clientMgr.updateClient(client, us.getUserId(), Client.HISTORY_UPDATE, us.getUser().getSalesBranchCode(), "SMS bounced", SourceSystem.CLIENT.getAbbreviation());
	    } else if (clientNote.getNoteType().equals(NoteType.TYPE_ADDRESS_UNKNOWN_PHONE)) {
		// update client and clear home phone
		LogUtil.debug(this.getClass(), "Cancel home phone");
		client.setHomePhone(null);
		clientMgr.updateClient(client, us.getUserId(), Client.HISTORY_UPDATE, us.getUser().getSalesBranchCode(), "Home phone unavailable", SourceSystem.CLIENT.getAbbreviation());
	    }

	    LogUtil.debug(this.getClass(), "clientNote=" + clientNote);
	    // update cancel flag
	    clientNote.setCancelDate(new DateTime());
	    clientNote.setCancelId(us.getUser().getUserID());

	    clientMgr.updateClientNote(clientNote);
	    LogUtil.debug(this.getClass(), "clientNote update");
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), "Unable to update client=" + ExceptionHelper.getExceptionStackTrace(e));
	    return ERROR;
	}
	LogUtil.log(this.getClass(), "MarkNoteCancelledAction.execute end");

	return SUCCESS;
    }
}