package com.ract.client;

import java.rmi.RemoteException;

import javax.ejb.Local;

@Local
public interface ClientMgrLocal extends ClientMgr {
    public void createClientNoteNew(ClientNote clientNote) throws RemoteException;
}
