package com.ract.client;

import java.util.Collection;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.BaseAction;
import com.ract.util.LogUtil;

public class NoteResultAction extends BaseAction implements ModelDriven, Preparable, ValidationAware {

    @Override
    public void validate() {
	if (model.getResultCode() == null || model.getResultCode().length() == 0) {
	    addFieldError("resultCode", "Result code must be entered");
	}

	if (model.getResultText() == null || model.getResultText().length() == 0) {
	    addFieldError("resultText", "Result text must be entered");
	}

	if (model.getNoteType() == null || model.getNoteType().length() == 0) {
	    addFieldError("noteType", "Note type must be entered");
	}
    }

    public Collection<NoteType> getNoteTypes() {
	return noteTypes;
    }

    public void setNoteTypes(Collection<NoteType> noteTypes) {
	this.noteTypes = noteTypes;
    }

    public void setModel(NoteResult model) {
	this.model = model;
    }

    public void setRequestId(String requestId) {
	this.requestId = requestId;
    }

    private NoteResult model;

    @Override
    public Object getModel() {
	return model;
    }

    private String requestId;

    public String getRequestId() {
	return requestId;
    }

    @InjectEJB(name = "ClientMgrBean")
    protected ClientMgrLocal clientMgr;

    @Override
    public String list() {
	list = clientMgr.getNoteResults();
	return LIST;
    }

    @Override
    public void prepare() throws Exception {
	LogUtil.log(this.getClass(), "requestId=" + (getRequestId() == null ? "null" : getRequestId().toString()));
	if (getRequestId() == null) {
	    model = new NoteResult();
	} else {
	    model = (NoteResult) commonMgr.get(NoteResult.class, getRequestId());
	    LogUtil.log(this.getClass(), "model=" + model);
	}
	noteTypes = clientMgr.getNoteTypes();
    }

    private Collection<NoteType> noteTypes;

}
