package com.ract.client;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.Publication;
import com.ract.security.Privilege;
import com.ract.user.UserSession;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * Class with helper methods for client functionality.
 * 
 * @author jyh
 * @version 1.0
 */

public class ClientHelper {

    /**
     * Use getter method of field to determine if the fields of two clients are
     * the same.
     * 
     * @param fieldName
     * @param object1
     * @param object2
     * @return
     */
    public static boolean fieldsMatch(String fieldName, Object object1, Object object2) {
	LogUtil.debug("ClientHelper", "fieldsMatch start");
	LogUtil.debug("ClientHelper", "fieldName=" + fieldName);
	LogUtil.debug("ClientHelper", "object1=" + object1);
	LogUtil.debug("ClientHelper", "object2=" + object2);
	if (object1 == null && object2 == null) {
	    return true;
	}

	if (object1 == null && object2 != null || object1 != null && object2 == null) {
	    return false;
	}

	String methodName = null;

	Object retObj1 = null;
	Object retObj2 = null;
	try {

	    methodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());

	    // reflection
	    Method method1 = object1.getClass().getMethod(methodName, null);
	    retObj1 = method1.invoke(object1, null);

	    Method method2 = object2.getClass().getMethod(methodName, null);
	    retObj2 = method2.invoke(object2, null);
	} catch (Exception e) {
	    LogUtil.warn(ClientHelper.class, e.getMessage());

	    // try is form
	    methodName = "is" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
	    try {
		Method method1 = object1.getClass().getMethod(methodName, null);
		retObj1 = method1.invoke(object1, null);

		Method method2 = object2.getClass().getMethod(methodName, null);
		retObj2 = method2.invoke(object2, null);
	    } catch (Exception e1) {
		LogUtil.warn(ClientHelper.class, e1.getMessage());
		return false;
	    }

	}

	if (retObj1 == null && retObj2 != null || retObj1 != null && retObj2 == null) {
	    return false;
	} else if (retObj1 != null && retObj2 != null) {

	    if (retObj1.equals(retObj2)) {
		return true; // equal
	    } else {
		return false;
	    }
	} else {
	    return true; // both null
	}

    }

    /**
     * Derive the client status.
     * 
     * @param status
     *            String
     * @return String
     */
    public static String formatClientStatus(String status) {
	// LogUtil.debug(this.getClass(), "status is '" + this.status + "'");
	if (status == null || status.trim().length() < 1) {
	    status = Client.STATUS_ACTIVE;
	}
	// LogUtil.debug(this.getClass(), "status is now '" + this.status +
	// "'");
	return status;
    }

    public static Collection getUnsubscribedPublicationList(Collection clientSubscriptionList, Collection publicationList) {
	ArrayList unsubscribedPublicationList = new ArrayList();
	ClientSubscription clientSubscription = null;
	Iterator subscriptionIt = null;
	Publication publication = null;
	if (publicationList != null) {
	    Iterator publicationIt = publicationList.iterator();
	    while (publicationIt.hasNext()) {
		publication = (Publication) publicationIt.next();
		// get subscription type
		LogUtil.debug("getUns", "publication=" + publication.getPublicationCode());
		LogUtil.debug("getUns", "clientSubscriptionList=" + clientSubscriptionList);
		// subscriptionCode =
		// clientSubscription.getClientSubscriptionPK().getSubscriptionCode();

		// remove from publication list
		if (clientSubscriptionList != null && !clientSubscriptionList.isEmpty()) {
		    subscriptionIt = clientSubscriptionList.iterator();
		    boolean addPublication = true;
		    while (subscriptionIt.hasNext() && addPublication) {
			clientSubscription = (ClientSubscription) subscriptionIt.next();
			LogUtil.debug("getUns", "clientSubscription=" + clientSubscription);
			if (clientSubscription.getClientSubscriptionPK().getSubscriptionCode().equals(publication.getPublicationCode()) && clientSubscription.getClientSubscriptionPK().getSubscriptionStatus().equals(ClientSubscription.STATUS_ACTIVE)) {
			    addPublication = false;
			}
		    }
		    if (addPublication) {
			unsubscribedPublicationList.add(publication);
		    }
		} else {
		    unsubscribedPublicationList.add(publication);
		}

	    }
	}
	LogUtil.debug("getUns", "" + unsubscribedPublicationList.size());
	return unsubscribedPublicationList;
    }

    public static boolean canMarkComplete(NoteType noteType, ClientNote clientNote, NoteCategory noteCategory, UserSession us) {

	boolean canMarkComplete = false;

	LogUtil.debug("ClientHelper", "canMarkComplete");

	if (noteType.isFollowup()) {
	    LogUtil.debug("ClientHelper", "checkNoteSecurity follow up" + noteCategory);
	    // if user has the privilege mark note complete or is the assignee
	    // allow the note to be completed.
	    if (us.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MARK_NOTE_COMPLETE) || us.getUser().getUserID().equals(clientNote.getCreateId()) || // creator
		    us.getUser().getUserID().equals(noteCategory.getNotificationUserId())) {
		LogUtil.debug("ClientHelper", "canMarkComplete priv");
		canMarkComplete = true;
	    }
	}
	if (noteType.isReferral()) {
	    LogUtil.debug("ClientHelper", "checkNoteSecurity referral");
	    // if user has the privilege mark note complete or is the referree
	    // allow the note to be completed.
	    if (us.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MARK_NOTE_COMPLETE) || // admin
											 // privileges
		    us.getUser().getUserID().equals(clientNote.getCreateId()) || // creator
		    us.getUser().getUserID().equals(clientNote.getSourceId()) || // referrer
		    us.getUser().getUserID().equals(clientNote.getTargetId()) || // referree
		    us.getUser().getSalesBranchCode().equals(clientNote.getTargetSalesBranch())) // member
												 // of
												 // the
												 // referee's
												 // branch
	    {
		LogUtil.debug("ClientHelper", "canMarkComplete");
		canMarkComplete = true;
	    }
	}

	LogUtil.debug("ClientHelper", "canUnblockTransactions " + canMarkComplete);
	return canMarkComplete;
    }

    public static Boolean canUnblockTransactions(NoteType noteType, ClientNote clientNote, NoteCategory noteCategory, UserSession us) {

	boolean canUnblockTransactions = false;

	LogUtil.debug("ClientHelper", "canUnblockTransactions");

	if (noteType.isBlockTransactions()) {
	    if (us.getUser().isPrivilegedUser(Privilege.PRIVILEGE_UNBLOCK_NOTE)) {
		canUnblockTransactions = true;
	    }
	}

	LogUtil.debug("ClientHelper", "canUnblockTransactions " + canUnblockTransactions);
	return canUnblockTransactions;
    }

    public static String getSMSSalutation(ClientVO client) {
	StringBuffer salutation = new StringBuffer();
	String givenNames = null;
	String givenName = null;
	givenNames = client.getGivenNames();

	// space
	if (givenNames.indexOf(" ") != -1) {
	    // first element
	    givenName = givenNames.split(" ")[0];
	} else {
	    givenName = givenNames;
	}

	if (givenName.length() > 2) {
	    salutation.append(StringUtil.toProperCase(givenName));
	} else {
	    // look for title and surname
	    if (client.getTitle() != null && client.getTitle().trim().length() > 0 && client.getSurname() != null && client.getSurname().trim().length() > 0) {
		salutation.append(StringUtil.toProperCase(client.getTitle().trim() + " " + client.getSurname()));
	    }
	}
	return salutation.toString();
    }

    public static String formatCustomerName(String title, String givenNames, String surname, boolean includeTitle, boolean firstNameOnly, boolean includeSurname, boolean includeInitials) {
	LogUtil.debug(com.ract.client.ClientHelper.class, "formatCustomerName start");
	LogUtil.debug(com.ract.client.ClientHelper.class, "title=" + title);
	LogUtil.debug(com.ract.client.ClientHelper.class, "givenNames=" + givenNames);
	LogUtil.debug(com.ract.client.ClientHelper.class, "surname=" + surname);
	LogUtil.debug(com.ract.client.ClientHelper.class, "includeTitle=" + includeTitle);
	LogUtil.debug(com.ract.client.ClientHelper.class, "firstNameOnly=" + firstNameOnly);
	LogUtil.debug(com.ract.client.ClientHelper.class, "includeSurname=" + includeSurname);
	LogUtil.debug(com.ract.client.ClientHelper.class, "includeInitials=" + includeInitials);
	// override include title
	boolean initialsOnly = (givenNames != null && givenNames.trim().length() <= 2);
	StringBuffer dName = new StringBuffer();
	if (title != null && (includeTitle || initialsOnly) && includeInitials) {
	    dName.append(StringUtil.toProperCase(title.trim()));
	    dName.append(" ");
	    includeSurname = true;
	}
	LogUtil.debug(com.ract.client.ClientHelper.class, "1 dName=" + dName);
	if (givenNames != null) {
	    String firstName = "";
	    int endIndex = givenNames.trim().indexOf(" ");
	    if (endIndex > 0 && firstNameOnly) {
		firstName = StringUtil.toProperCase(givenNames.substring(0, endIndex));
		initialsOnly = firstName.trim().length() <= 2;
		if (!initialsOnly) {
		    // first name only
		    dName.append(firstName);
		    dName.append(" ");
		} else {
		    // initials only
		    dName.append(StringUtil.toProperCase(title.trim()));
		    dName.append(" ");
		}
		LogUtil.debug(com.ract.client.ClientHelper.class, "2 dName=" + dName);
	    } else {
		dName.append(StringUtil.toProperCase(givenNames.trim()));
		dName.append(" ");
	    }
	    LogUtil.debug(com.ract.client.ClientHelper.class, "3 dName=" + dName);

	}
	if (includeSurname && surname != null) {
	    // APPS-48 Fix Apostrophe issue for letters 19-06-2019
	    // Also cater for multiple apostrophes in the address title
	    String[] nameParts = surname.split("'");
	    if (nameParts.length > 0) {
		for (int partIt = 0; partIt < nameParts.length; partIt++) {
		    dName.append(StringUtil.toProperCase(nameParts[partIt], true));
		    if (partIt < nameParts.length - 1)
			dName.append("'");
		}
	    } else {
		dName.append(StringUtil.toProperCase(surname.trim(), true));
	    }
	    // THIS CODE DOES NOT CATER FOR MULTIPLE APOSTROPHES
//	    int endIndex = surname.indexOf("'");
//	    // cater for apostrophes
//	    if (endIndex > 0) {
//		dName.append(surname.substring(0, endIndex).toUpperCase() + StringUtil.toProperCase(surname.substring(endIndex, surname.length())));
//	    } else {
//		dName.append(StringUtil.toProperCase(surname.trim()));
//	    }
	    LogUtil.debug(com.ract.client.ClientHelper.class, "4 dName=" + dName);
	}
	LogUtil.debug(com.ract.client.ClientHelper.class, "dName=" + dName);
	LogUtil.debug(com.ract.client.ClientHelper.class, "formatCustomerName end");
	if (dName.length() == 0) {
	    return "?";
	} else {
	    return dName.toString().trim();
	}
    }

}
