package com.ract.client;

/**
 * Represent client details as at a point in time.
 * 
 * @hibernate.class table="[cl-history]" lazy="false"
 * 
 * @author John Holliday
 * @version 1.0
 */

public class ClientHistory implements java.io.Serializable {

    public ClientHistory() {

    }

    public ClientHistory(ClientHistoryPK clientHistoryPK, String logonId, String transactionReason, String salesBranchCode, String comments, String subSystem, Integer changedClientNumber, ClientVO client) {
	this.setClientHistoryPK(clientHistoryPK);

	this.setLogonId(logonId);
	this.setTransactionReason(transactionReason);
	this.setSalesBranchCode(salesBranchCode);
	this.setComments(comments);
	this.setSubSystem(subSystem);

	this.setClient(client);
    }

    private ClientHistoryPK clientHistoryPK;

    public void setClientHistoryPK(ClientHistoryPK clientHistoryPK) {
	this.clientHistoryPK = clientHistoryPK;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public ClientHistoryPK getClientHistoryPK() {
	return this.clientHistoryPK;
    }

    private Integer changedClientNumber;

    /**
     * @hibernate.property
     * @hibernate.column name="[ch-client-no]"
     */
    public Integer getChangedClientNumber() {
	return changedClientNumber;
    }

    public void setChangedClientNumber(Integer changedClientNumber) {
	this.changedClientNumber = changedClientNumber;
    }

    private String logonId;

    private String transactionReason;

    private String salesBranchCode;

    private String comments;

    private String subSystem;

    private String authorityNumber;

    /**
     * @hibernate.property
     * @hibernate.column name="[authority-no]"
     */
    public String getAuthorityNumber() {
	return authorityNumber;
    }

    public void setAuthorityNumber(String authorityNumber) {
	this.authorityNumber = authorityNumber;
    }

    private ClientVO client;

    /**
     * @hibernate.component class="com.ract.client.ClientVO"
     * @return
     */
    public ClientVO getClient() {
	return client;
    }

    /**
     * @hibernate.property
     */
    public String getComments() {
	return comments;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[sales-branch]"
     */
    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[trans-reason]"
     */
    public String getTransactionReason() {
	return transactionReason;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[sub-system]"
     */
    public String getSubSystem() {
	return subSystem;
    }

    @Override
    public String toString() {
	return "ClientHistory [authorityNumber=" + authorityNumber + ", changedClientNumber=" + changedClientNumber + ", client=" + client + ", clientHistoryPK=" + clientHistoryPK + ", comments=" + comments + ", logonId=" + logonId + ", salesBranchCode=" + salesBranchCode + ", subSystem=" + subSystem + ", transactionReason=" + transactionReason + "]";
    }

    /**
     * @hibernate.property
     * @hibernate.column name="[logon-id]"
     */
    public String getLogonId() {
	return logonId;
    }

    public void setLogonId(String userID) {
	this.logonId = userID;
    }

    public void setTransactionReason(String transactionReason) {
	this.transactionReason = transactionReason;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

    public void setClient(ClientVO clientVO) {
	this.client = clientVO;
    }

}
