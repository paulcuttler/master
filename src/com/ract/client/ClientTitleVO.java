package com.ract.client;

import com.ract.common.CachedItem;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.util.DateTime;

/**
 * Holds the client title details such as Mr, Mrs, etc. and whether it is gender
 * specific.
 * 
 * @hibernate.class table="[gn-title]" lazy="false"
 * 
 * @author John Holliday
 * @version 1.0, 10/4/2002
 */

public class ClientTitleVO extends ValueObject implements CachedItem {

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    protected ClientTitleVO copy() {
	ClientTitleVO ctvo = new ClientTitleVO();
	ctvo.setTitleName(this.getTitleName());
	ctvo.setSex(this.getSex());
	ctvo.setSexSpecific(this.getSexSpecific());
	ctvo.setGroupId(this.getGroupId());
	ctvo.setDeleteDate(this.getDeleteDate());

	return ctvo;
    }

    /**
     * Description of the Field
     */
    protected String titleName;

    /**
     * Description of the Field
     */
    protected Boolean sex;

    /**
     * Description of the Field
     */
    protected Boolean groupId;

    /**
     * Description of the Field
     */
    protected Boolean sexSpecific;

    private DateTime deleteDate;

    /**
     * Gets the key attribute of the ClientTitleVO object
     * 
     * @return The key value
     */
    public String getKey() {
	return this.getTitleName();
    }

    /**
     * Description of the Method
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this won't throw a null
	    // pointer exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /**
     * Gets the titleName attribute of the ClientTitleVO object
     * 
     * @hibernate.id column="[title-name]" generator-class="assigned"
     * 
     * @return The titleName value
     */
    public String getTitleName() {
	return titleName;
    }

    /**
     * Sets the titleName attribute of the ClientTitleVO object
     * 
     * @param titleName
     *            The new titleName value
     */
    public void setTitleName(String titleName) {
	this.titleName = titleName;
    }

    /**
     * Sets the sex attribute of the ClientTitleVO object
     * 
     * @param sex
     *            The new sex value
     */
    public void setSex(Boolean sex) {
	this.sex = sex;
    }

    /**
     * Gets the sex attribute of the ClientTitleVO object
     * 
     * @hibernate.property
     * @hibernate.column name="sex"
     * 
     * @return The sex value
     */
    public Boolean getSex() {
	return sex;
    }

    /**
     * Sets the sexSpecific attribute of the ClientTitleVO object
     * 
     * @param sexSpecific
     *            The new sexSpecific value
     */
    public void setSexSpecific(Boolean sexSpecific) {
	this.sexSpecific = sexSpecific;
    }

    /**
     * Gets the sexSpecific attribute of the ClientTitleVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[sex-specific]"
     * 
     * @return The sexSpecific value
     */
    public Boolean getSexSpecific() {
	return sexSpecific;
    }

    /**
     * Sets the groupId attribute of the ClientTitleVO object
     * 
     * @param groupId
     *            The new groupId value
     */
    public void setGroupId(Boolean groupId) {
	this.groupId = groupId;
    }

    /**
     * Gets the groupId attribute of the ClientTitleVO object
     * 
     * @hibernate.property
     * @hibernate.column name="[group-id]"
     * 
     * @return The groupId value
     */
    public Boolean getGroupId() {
	return groupId;
    }

    /**
     * Sets the deleteDate attribute of the ClientTitleVO object.
     * 
     * @param deleteDate
     */
    protected void setDeleteDate(DateTime deleteDate) {
	this.deleteDate = deleteDate;
    }

    /**
     * Gets the deleteDate attribute of the ClientTitleVO object.
     * 
     * @hibernate.property
     * @hibernate.column name="deleteDate"
     * 
     * @return The deleteDate value
     */
    public DateTime getDeleteDate() {
	return deleteDate;
    }

    /**
     * Description of the Method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
    }

}
