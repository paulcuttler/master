package com.ract.client;

import java.util.Collection;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.BusinessType;
import com.ract.common.CommonMgrLocal;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipVO;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.user.UserSession;
import com.ract.util.LogUtil;

public class AddNoteAction extends ActionSupport implements Preparable, SessionAware {

    public Collection getSalesBranchList() {
	return salesBranchList;
    }

    public void setSalesBranchList(Collection salesBranchList) {
	this.salesBranchList = salesBranchList;
    }

    public String getSourceId() {
	return sourceId;
    }

    public void setSourceId(String sourceId) {
	this.sourceId = sourceId;
    }

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private Map session = null;

    private Collection salesBranchList;

    @Override
    public void prepare() throws Exception {
	noteTypeList = clientMgr.getNoteTypes();
	categoryList = clientMgr.getNoteCategories();
	sourceUserList = userMgr.getActiveUsers();
	targetUserList = userMgr.getActiveUsers();
	// TODO business type list from database
	businessTypes = commonMgr.getBusinessTypes();
	salesBranchList = commonMgr.getSalesBranchList();
	LogUtil.log(this.getClass(), "businessType" + businessType);

	if (BusinessType.BUSINESS_TYPE_MEMBERSHIP.equals(businessType)) {
	    MembershipVO membership = membershipMgr.getMembership(new Integer(referenceId));
	    LogUtil.log(this.getClass(), "membership" + membership);

	    client = membership.getClient();
	    LogUtil.log(this.getClass(), "client" + client);
	    setClientNumber(client.getClientNumber());
	    LogUtil.log(this.getClass(), "clientNumber" + clientNumber);

	} else {
	    if (clientNumber != null) {
		client = clientMgr.getClient(clientNumber);
	    }
	}

    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    private String sourceId;

    public Collection<User> getSourceUserList() {
	return sourceUserList;
    }

    public void setSourceUserList(Collection<User> sourceUserList) {
	this.sourceUserList = sourceUserList;
    }

    public Collection<User> getTargetUserList() {
	return targetUserList;
    }

    public void setTargetUserList(Collection<User> targetUserList) {
	this.targetUserList = targetUserList;
    }

    public String getNoteType() {
	return noteType;
    }

    public void setNoteType(String noteType) {
	this.noteType = noteType;
    }

    public void setCategoryList(Collection<NoteCategory> categoryList) {
	this.categoryList = categoryList;
    }

    public Collection<BusinessType> getBusinessTypes() {
	return businessTypes;
    }

    public void setBusinessTypes(Collection<BusinessType> businessTypes) {
	this.businessTypes = businessTypes;
    }

    public String getBusinessType() {
	return businessType;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    public String getReferenceId() {
	return referenceId;
    }

    public void setReferenceId(String referenceId) {
	this.referenceId = referenceId;
    }

    public Collection<NoteCategory> getCategoryList() {
	return categoryList;
    }

    private Collection<BusinessType> businessTypes;

    public void setNoteTypeList(Collection<NoteType> noteTypeList) {
	this.noteTypeList = noteTypeList;
    }

    public Collection<NoteType> getNoteTypeList() {
	return noteTypeList;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    private Collection<NoteType> noteTypeList;

    private ClientVO client;

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    private Collection<User> sourceUserList;

    private Collection<User> targetUserList;

    private String businessType;

    private String noteType;

    private String referenceId;

    private Integer clientNumber;

    private Collection<NoteCategory> categoryList;

    @SuppressWarnings("unchecked")
    @Override
    public String execute() throws Exception {
	// set default referrer
	UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	sourceId = us.getUserId();
	// set to empty
	referenceId = null;
	return SUCCESS;
    }

    @InjectEJB(name = "UserMgrBean")
    private UserMgrLocal userMgr;

}
