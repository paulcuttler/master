package com.ract.client;

import java.util.Collection;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.BusinessType;
import com.ract.common.CommonMgrLocal;

public class FilterNotesAction extends ActionSupport {

    public String getBusinessType() {
	return businessType;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    public Collection<BusinessType> getBusinessTypeList() {
	return businessTypeList;
    }

    public void setBusinessTypeList(Collection<BusinessType> businessTypeList) {
	this.businessTypeList = businessTypeList;
    }

    public Collection<NoteType> getNoteTypeList() {
	return noteTypeList;
    }

    public void setNoteTypeList(Collection<NoteType> noteTypeList) {
	this.noteTypeList = noteTypeList;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public String getNoteType() {
	return noteType;
    }

    public void setNoteType(String noteType) {
	this.noteType = noteType;
    }

    private Collection<NoteType> noteTypeList;

    public Collection<ClientNote> getClientNoteList() {
	return clientNoteList;
    }

    public void setClientNoteList(Collection<ClientNote> clientNoteList) {
	this.clientNoteList = clientNoteList;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private ClientVO client;

    private String noteType;

    private String businessType;

    private Collection<ClientNote> clientNoteList;

    @Override
    public String execute() throws Exception {

	boolean noteTypeFilter = noteType != null && !noteType.trim().equals("");
	boolean businessTypeFilter = businessType != null && !businessType.trim().equals("");
	if (noteTypeFilter && businessTypeFilter) {
	    clientNoteList = clientMgr.getClientNoteList(clientNumber, businessType, noteType);
	} else if (noteTypeFilter && !businessTypeFilter) {
	    clientNoteList = clientMgr.getClientNoteListByNoteType(clientNumber, noteType);
	} else if (!noteTypeFilter && businessTypeFilter) {
	    clientNoteList = clientMgr.getClientNoteList(clientNumber, businessType);
	} else {
	    // clear filter and show by client number
	    clientNoteList = clientMgr.getClientNoteList(clientNumber);
	}
	noteTypeList = clientMgr.getNoteTypes();
	businessTypeList = commonMgr.getBusinessTypes();
	client = clientMgr.getClient(clientNumber);
	return SUCCESS;
    }

    private Integer clientNumber;

    private Collection<BusinessType> businessTypeList;

}
