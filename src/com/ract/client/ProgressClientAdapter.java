package com.ract.client;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import com.progress.open4gl.BooleanHolder;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.RollBackException;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * <p>
 * Progress implementation of a client adapter.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class ProgressClientAdapter extends TransactionAdapterType implements ClientAdapter {

    private ClientTransactionProxy clientTransactionProxy;

    private ClientTransactionProxy getClientTransactionProxy() throws SystemException {
	if (this.clientTransactionProxy == null) {
	    ClientProgressProxy clientProgressProxy = getClientProgressProxy();
	    try {
		this.clientTransactionProxy = clientProgressProxy.createPO_ClientTransactionProxy();
	    } catch (Open4GLException o4gle) {
		throw new SystemException(o4gle);
	    } finally {
		releaseClientProgressProxy(clientProgressProxy);
	    }
	}
	return this.clientTransactionProxy;
    }

    public void commit() throws SystemException {
	LogUtil.debug(this.getClass(), "commit()");
	if (this.clientTransactionProxy != null) {
	    try {
		this.clientTransactionProxy.commit();
	    } catch (Open4GLException gle) {
		throw new SystemException(gle);
	    } finally {
		release();
	    }
	}
    }

    private void releaseTransactionProxy() {
	if (this.clientTransactionProxy != null) {
	    try {
		this.clientTransactionProxy._release();
	    } catch (Open4GLException gle) {
		LogUtil.fatal(this.getClass(), gle.getMessage());
	    } finally {
		this.clientTransactionProxy = null;
	    }
	}
    }

    public void updateClient(ClientVO client, String userId, String salesBranch, String comments, String subSystem) throws RollBackException {
	LogUtil.log(this.getClass(), "client: " + client.toString() + " " + client.getClientNumber());

	if (client == null) {
	    throw new RollBackException("Client may not be null when notifying Progress.");
	}

	ClientTransactionProxy clientTransactionProxy = null;
	try {

	    clientTransactionProxy = getClientTransactionProxy();

	    GregorianCalendar birthDate = null;

	    DateTime birthDateDateTime = client.getBirthDate();
	    if (birthDateDateTime != null) {
		birthDate = new GregorianCalendar();
		birthDate.setTime(birthDateDateTime);
	    }

	    GregorianCalendar madeDate = null;
	    DateTime madeDateDateTime = client.getMadeDate();
	    if (madeDateDateTime != null) {
		madeDate = new GregorianCalendar();
		madeDate.setTime(madeDateDateTime);
	    }

	    GregorianCalendar lastUpdate = null;
	    DateTime lastUpdateDateTime = client.getLastUpdate();
	    if (lastUpdateDateTime != null) {
		lastUpdate = new GregorianCalendar();
		lastUpdate.setTime(lastUpdateDateTime);
	    }

	    PostalAddressVO pavo = client.getPostalAddress();
	    ResidentialAddressVO savo = client.getResidentialAddress();

	    int postalStreetNumber = 0;
	    if (pavo.getStreetNumber() != null) {
		postalStreetNumber = pavo.getStreetNumber().intValue();
	    }

	    int residentialStreetNumber = 0;
	    if (savo.getStreetNumber() != null) {
		residentialStreetNumber = savo.getStreetNumber().intValue();
	    }

	    int postalStsubID = 0;
	    if (pavo.getStreetSuburbID() != null) {
		postalStsubID = pavo.getStreetSuburbID().intValue();
	    }

	    int residentialStsubID = 0;
	    if (savo.getStreetSuburbID() != null) {
		residentialStsubID = savo.getStreetSuburbID().intValue();
	    }

	    // will always be updated to be true or false
	    boolean market = false;
	    if (client.getMarket() != null) {
		market = client.getMarket().booleanValue();
	    }
	    boolean sms = client.getSmsAllowed();

	    int memoNo = 0;
	    if (client.getMemoNumber() != null) {
		memoNo = client.getMemoNumber().intValue();
	    }

	    int branchNumber = 0;
	    if (client.getBranchNumber() != null) {
		branchNumber = client.getBranchNumber().intValue();
	    }

	    // person specific stuff
	    int numberDependents = 0;

	    if (client.numberOfDependants != null) {
		numberDependents = client.getNumberOfDependants().intValue();
	    }

	    String homePhone = StringUtil.convertNull(client.getHomePhone());
	    String workPhone = StringUtil.convertNull(client.getWorkPhone());
	    String mobilePhone = StringUtil.convertNull(client.getMobilePhone());
	    String clientTitle = StringUtil.convertNull(client.getTitle());
	    String initials = StringUtil.convertNull(client.getInitials());
	    String givenNames = StringUtil.convertNull(client.getGivenNames());
	    boolean maritalStatus = client.getMaritalStatus() == null ? false : client.getMaritalStatus().booleanValue();
	    String surname = StringUtil.convertNull(client.getSurname());
	    int masterClientNumber = client.getMasterClientNumber() == null ? 0 : client.getMasterClientNumber().intValue();
	    boolean gender = (client.getSex() == null ? false : client.getSex().booleanValue());

	    clientTransactionProxy.updateClient(client.getAddressTitle(), birthDate, branchNumber, numberDependents, client.getClientNumber().intValue(), StringUtil.convertNull(client.getStatus()), clientTitle, StringUtil.convertNull(client.getEmailAddress()), givenNames, client.getGroupClient(), homePhone, initials, lastUpdate, madeDate, StringUtil.convertNull(client.getMadeID()), maritalStatus, market, memoNo, mobilePhone, (pavo == null ? null : StringUtil.convertNull(pavo.getProperty())), (pavo == null ? null : StringUtil.convertNull(pavo.getPropertyQualifier())), postalStreetNumber, postalStsubID, StringUtil.convertNull(client.getPostalName()), (savo == null ? null : StringUtil.convertNull(savo.getProperty())), (savo == null ? null : StringUtil.convertNull(savo.getPropertyQualifier())), residentialStreetNumber, residentialStsubID, gender, surname, workPhone, masterClientNumber, StringUtil.convertNull(client.getResiDpid()), StringUtil.convertNull(client.getPostDpid()), userId, salesBranch, comments, sms,
		    client.getPhoneQuestion(), client.getPhonePassword(), subSystem, client.getBirthDateString(), (savo == null ? null : StringUtil.convertNull(savo.getLatitude())), (savo == null ? null : StringUtil.convertNull(savo.getLongitude())), (savo == null ? null : StringUtil.convertNull(savo.getGnaf())), (pavo == null ? null : StringUtil.convertNull(savo.getAusbar())), (savo == null ? null : StringUtil.convertNull(pavo.getLatitude())), (savo == null ? null : StringUtil.convertNull(pavo.getLongitude())), (savo == null ? null : StringUtil.convertNull(pavo.getGnaf())), (pavo == null ? null : StringUtil.convertNull(pavo.getAusbar())));

	    handleTransactionProxyReturnString();

	} catch (Exception e) {
	    throw new RollBackException(e);
	}

    }

    // public void createClient()
    // {
    // }

    public ClientVO getClient(Integer clientNumber) {
	return null;
    }

    public Collection getClientTitleList() {
	return null;
    }

    public Collection getClientsByPhoneNumber(String phoneNumber) {
	return null;
    }

    public Collection getClientsBySurname(String surname) {
	return null;
    }

    public Collection getClientsBySurnameAndAddress(String surname, Integer streetNumber, Integer stsubid) {
	return null;
    }

    public Collection getClientsBySurnameAndBirthDate(String surname, String initials, DateTime birthDate) {
	return null;
    }

    public Collection getClientsBySurnameAndTitle(String surname, String title, String initials) {
	return null;
    }

    public String getSystemName() {
	return "";
    }

    public void release() {
	LogUtil.debug(this.getClass(), "release()");
	releaseTransactionProxy();
    }

    public void rollback() throws SystemException {
	LogUtil.debug(this.getClass(), "rollback()");
	try {
	    if (this.clientTransactionProxy != null) {
		this.clientTransactionProxy.rollback();
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	} finally {
	    release();
	}
    }

    public void addFieldUpdate(Integer clientNumber, String fieldName, String fieldValue) throws RollBackException {
	try {
	    ClientTransactionProxy clientTransactionProxy = getClientTransactionProxy();
	    clientTransactionProxy.setFieldUpdate(clientNumber, fieldName, fieldValue);

	    handleTransactionProxyReturnString();
	} catch (Exception e) {
	    throw new RollBackException(e);
	}
    }

    public void clearAskable(Integer clientNumber, String fieldName) throws RollBackException {
	try {
	    ClientTransactionProxy clientTransactionProxy = getClientTransactionProxy();
	    clientTransactionProxy.clearAskable(clientNumber, fieldName);

	    handleTransactionProxyReturnString();
	} catch (Exception e) {
	    throw new RollBackException(e);
	}
    }

    private void handleTransactionProxyReturnString() throws RollBackException {
	if (this.clientTransactionProxy != null) {
	    try {
		String returnString = this.clientTransactionProxy._getProcReturnString();
		if (returnString != null && returnString.length() > 0) {
		    throw new RollBackException(new ValidationException(returnString));
		}
	    } catch (Open4GLException gle) {
		throw new RollBackException(gle);
	    }
	}
    }

    public boolean isAskable(Integer clientNumber, String fieldName, String fieldValue) throws SystemException {
	LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber);
	LogUtil.debug(this.getClass(), "fieldName=" + fieldName);
	LogUtil.debug(this.getClass(), "fieldValue=" + fieldValue);
	ClientProgressProxy clientProgressProxy = null;
	boolean askableResponse = false;
	try {
	    BooleanHolder askable = new BooleanHolder();
	    clientProgressProxy = getClientProgressProxy();
	    if (fieldValue != null && fieldValue.trim().length() > 0) {
		clientProgressProxy.getAskable(clientNumber, fieldName, fieldValue, askable);
		if (askable != null) {
		    askableResponse = askable.getBooleanValue();
		}
	    } else // null or empty string is askable
	    {
		askableResponse = true;
	    }
	} catch (Exception e) {
	    throw new SystemException(e);
	} finally {
	    releaseClientProgressProxy(clientProgressProxy);
	}
	LogUtil.debug(this.getClass(), "askableResponse=" + askableResponse);
	return askableResponse;

    }

    public String getTramadaClientXML(String action, String tramadaProfileCode, String debtorCode, String userId, String salesBranchCode, ClientVO client) throws SystemException {
	String tramadaClientXMLString = null;

	// client number is mandatory as it is the primary key in Tramada
	if (client.getClientNumber() == null) {
	    throw new SystemException("Client number may not be null when generating XML for Tramada.");
	}

	ClientProgressProxy clientProgressProxy = null;
	StringHolder tramadaClientXML = new StringHolder();
	GregorianCalendar birthDate = null;
	if (client.getBirthDate() != null) {
	    birthDate = new GregorianCalendar();
	    birthDate.setTime(client.getBirthDate());
	}
	GregorianCalendar madeDate = null;
	if (client.getMadeDate() != null) {
	    madeDate = new GregorianCalendar();
	    madeDate.setTime(client.getMadeDate());
	}
	GregorianCalendar lastUpdate = null;
	if (client.getLastUpdate() != null) {
	    lastUpdate = new GregorianCalendar();
	    lastUpdate.setTime(client.getLastUpdate());
	}
	try {
	    clientProgressProxy = getClientProgressProxy();

	    clientProgressProxy.getTramadaClientXML(client.getAddressTitle() == null ? "" : client.getAddressTitle(), birthDate, client.getBranchNumber() == null ? 0 : client.getBranchNumber().intValue(), client.getNumberOfDependants() == null ? 0 : client.getNumberOfDependants().intValue(), client.getClientNumber().intValue(),// should
																																									 // never
																																									 // be
																																									 // null
		    client.getStatus() == null ? "" : client.getStatus(), client.getTitle() == null ? "" : client.getTitle(), client.getEmailAddress() == null ? "" : client.getEmailAddress(), client.getGivenNames() == null ? "" : client.getGivenNames(), client.getGroupClient(), client.getHomePhone() == null ? "" : client.getHomePhone(), client.getInitials() == null ? "" : client.getInitials(), lastUpdate, madeDate, client.getMadeID() == null ? "" : client.getMadeID(), client.getMaritalStatus() == null ? false : client.getMaritalStatus().booleanValue(), client.getMarket() == null ? false : client.getMarket().booleanValue(), client.getMemoNumber() == null ? 0 : client.getMemoNumber().intValue(), client.getMobilePhone() == null ? "" : client.getMobilePhone(), client.getPostProperty() == null ? "" : client.getPostProperty(), client.getPostStreetChar() == null ? "" : client.getPostStreetChar(), client.getPostStreetNumber() == null ? 0 : client.getPostStreetNumber().intValue(), client.getPostStsubId() == null ? 0
			    : client.getPostStsubId().intValue(), client.getPostalName() == null ? "" : client.getPostalName(), client.getResiProperty() == null ? "" : client.getResiProperty(), client.getResiStreetChar() == null ? "" : client.getResiStreetChar(), client.getResiStreetNumber() == null ? 0 : client.getResiStreetNumber().intValue(), client.getResiStsubId() == null ? 0 : client.getResiStsubId().intValue(), client.getSex() == null ? false : client.getSex().booleanValue(), client.getSurname() == null ? "" : client.getSurname(), client.getWorkPhone() == null ? "" : client.getWorkPhone(), client.getMasterClientNumber() == null ? 0 : client.getMasterClientNumber().intValue(), action, tramadaProfileCode == null ? "" : tramadaProfileCode, debtorCode == null ? "" : debtorCode, userId == null ? "" : userId, salesBranchCode == null ? "" : salesBranchCode, tramadaClientXML);

	    tramadaClientXMLString = tramadaClientXML.getStringValue();

	    // circumvent error due to Tramada not processing xml documents
	    // according to spec
	    tramadaClientXMLString = StringUtil.replaceAll(tramadaClientXMLString, "<?xml version=\"1.0\" ?>\n", "");

	    LogUtil.debug(this.getClass(), tramadaClientXMLString);

	} catch (Exception ex) {
	    throw new SystemException("Error getting client list.", ex);
	} finally {
	    releaseClientProgressProxy(clientProgressProxy);
	}
	return tramadaClientXMLString;
    }

    public Collection getClientsByClientNumberRange(Integer startClientNumber, Integer endClientNumber) throws SystemException {
	ArrayList clientList = new ArrayList();
	Integer clientNumber = null;
	ClientProgressProxy clientProgressProxy = null;
	ResultSetHolder clientResultSetHolder = new ResultSetHolder();
	try {
	    clientProgressProxy = getClientProgressProxy();
	    clientProgressProxy.getClientNumberByRange(startClientNumber.intValue(), endClientNumber.intValue(), clientResultSetHolder);

	    ResultSet rs = clientResultSetHolder.getResultSetValue();
	    while (rs.next()) {
		clientNumber = new Integer(rs.getInt(1));
		clientList.add(clientNumber);
	    }
	} catch (Exception ex) {
	    throw new SystemException("Error getting client list.", ex);
	} finally {
	    releaseClientProgressProxy(clientProgressProxy);
	}
	return clientList;
    }

    public Hashtable hasEligibleBusiness(Integer clientNumber) throws SystemException {
	Hashtable eligibleBusiness = new Hashtable();
	ClientProgressProxy clientProgressProxy = null;
	LogUtil.debug(this.getClass(), "hasEligibleBusiness");
	try {

	    DateTime endDate = new DateTime().getDateOnly();
	    DateTime startDate = endDate.subtract(new Interval(1, 0, 0, 0, 0, 0, 0));

	    LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber + ",startDate=" + startDate + ",endDate=" + endDate);

	    GregorianCalendar startCal = new GregorianCalendar();
	    startCal.setTime(startDate);
	    GregorianCalendar endCal = new GregorianCalendar();
	    endCal.setTime(endDate);

	    BooleanHolder eligibleTravelHolder = new BooleanHolder();
	    BooleanHolder eligibleInsuranceHolder = new BooleanHolder();

	    boolean eligibleTravel = false;
	    boolean eligibleInsurance = false;

	    clientProgressProxy = getClientProgressProxy();
	    LogUtil.debug(this.getClass(), "clientProgressProxy=" + clientProgressProxy);
	    clientProgressProxy.hasBusiness(clientNumber.intValue(), startCal, endCal, eligibleTravelHolder, eligibleInsuranceHolder);

	    eligibleTravel = eligibleTravelHolder.getBooleanValue();
	    eligibleInsurance = eligibleInsuranceHolder.getBooleanValue();

	    eligibleBusiness.put(BUSINESS_TRAVEL, new Boolean(eligibleTravel));
	    eligibleBusiness.put(BUSINESS_INSURANCE, new Boolean(eligibleInsurance));
	    LogUtil.debug(this.getClass(), "eligibleBusiness=" + eligibleBusiness);
	} catch (Exception ex) {
	    StringBuffer message = new StringBuffer();
	    try {
		message.append(clientProgressProxy._getProcReturnString());
	    } catch (Open4GLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    message.append(" ");
	    message.append(ex.getMessage());
	    throw new SystemException(message.toString());
	} finally {
	    releaseClientProgressProxy(clientProgressProxy);
	}
	return eligibleBusiness;
    }

    public static String BUSINESS_TRAVEL = "Travel";

    public static String BUSINESS_INSURANCE = "Insurance";

    public ClientVO updateClient() {
	return null;
    }

    // get progress proxy
    private ClientProgressProxy getClientProgressProxy() throws SystemException {
	ClientProgressProxy clientProgressProxy = null;
	try {
	    clientProgressProxy = (ClientProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_CLIENT_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return clientProgressProxy;
    }

    // release progress proxy
    private void releaseClientProgressProxy(ClientProgressProxy clientProgressProxy) {
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_CLIENT_PROXY, clientProgressProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), ex.getMessage());
	} finally {
	    clientProgressProxy = null;
	}
    }

    public String[] getRelatedClients(Integer clientNo) throws SystemException {
	String[] clientList = null;
	ClientProgressProxy clientPx = null;
	StringHolder clientListHolder = new StringHolder();

	try {
	    clientPx = getClientProgressProxy();
	    clientPx.getRelatedClients(clientNo.intValue(), clientListHolder);
	    String cList = clientListHolder.getStringValue();
	    if (cList == null || cList.equals(""))
		clientList = null;
	    else
		clientList = cList.split(",");
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex);
	} finally {
	    releaseClientProgressProxy(clientPx);
	}
	return clientList;
    }

    public Collection<Integer> getOwnedGroups(Integer clientNo) throws SystemException {
	Collection<Integer> clientList = new ArrayList<Integer>();
	ClientProgressProxy clientPx = null;
	ResultSetHolder clientListHolder = new ResultSetHolder();

	try {
	    clientPx = getClientProgressProxy();
	    clientPx.getOwnedGroups(clientNo.intValue(), clientListHolder);
	    ResultSet rs = clientListHolder.getResultSetValue();
	    while (rs.next()) {
		clientList.add(rs.getInt(1));
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex);
	} finally {
	    releaseClientProgressProxy(clientPx);
	}
	return clientList;
    }

    public ArrayList getClientBusiness(Integer clientNo) throws SystemException {
	ClientProgressProxy clientPx = null;
	ResultSetHolder businessHolder = new ResultSetHolder();
	ArrayList businessList = new ArrayList();
	ClientBusiness clBus = null;
	try {
	    clientPx = getClientProgressProxy();
	    clientPx.getClientBusiness(clientNo.intValue(), businessHolder);
	    ResultSet rs = businessHolder.getResultSetValue();
	    while (rs.next()) {
		clBus = new ClientBusiness();
		clBus.setClientNo(new Integer(rs.getInt(1)));
		clBus.setBusinessType(rs.getString(2));
		clBus.setBusinessNumber(new Integer(rs.getInt(3)));
		clBus.setBusinessDesc(rs.getString(4));
		businessList.add(clBus);
	    }
	} catch (Exception ex) {
	    throw new SystemException(ex);
	} finally {
	    releaseClientProgressProxy(clientPx);
	}
	return businessList;
    }

    public boolean isProtected(Integer clientNo) throws SystemException {
	ClientProgressProxy clientPx = null;
	BooleanHolder bHolder = new BooleanHolder();
	try {
	    clientPx = getClientProgressProxy();
	    clientPx.isProtected(clientNo.intValue(), bHolder);
	} catch (Exception ex) {
	    throw new SystemException("Error getting client protection status " + ex);
	} finally {
	    releaseClientProgressProxy(clientPx);
	}
	return bHolder.getBooleanValue();
    }

    /**
     * getClientGroup
     * 
     * @param clientNo
     *            Integer
     * @return ArrayList
     */
    public ArrayList getClientGroup(Integer clientNo) throws SystemException {
	ClientProgressProxy clientProgressProxy = null;
	StringHolder sHolder = new StringHolder();
	ArrayList grpList = new ArrayList();
	ClientVO clt = null;
	Integer thisClientNo = null;
	ClientMgr cltMgr = null;
	String arrayString = null;
	try {
	    cltMgr = ClientEJBHelper.getClientMgr();
	    clientProgressProxy = getClientProgressProxy();
	    clientProgressProxy.getClientGroup(clientNo.intValue(), sHolder);
	    arrayString = sHolder.getStringValue();
	    if (arrayString != null && !arrayString.trim().equals("")) {
		String[] clArray = sHolder.getStringValue().split(",");
		for (int xx = 0; xx < clArray.length; xx++) {
		    thisClientNo = new Integer(clArray[xx]);
		    clt = cltMgr.getClient(thisClientNo);
		    grpList.add(clt);
		}
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex);
	} finally {
	    releaseClientProgressProxy(clientProgressProxy);
	}
	return grpList;
    }

}
