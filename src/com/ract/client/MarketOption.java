package com.ract.client;

import java.io.Serializable;

import com.ract.util.DateTime;

/**
 * @hibernate.class table="marketOption" lazy="false"
 */
public class MarketOption implements Serializable {

    String createId;
    String deleteId;
    DateTime deleteDateTime;
    MarketOptionPK marketOptionPk;
    Boolean unavailable;

    public MarketOption() {
	this.marketOptionPk = new MarketOptionPK();
    }

    public MarketOption(Integer clientNo, String subSystem, String medium, String createId, DateTime createDate, String deleteId, DateTime deleteDate) {
	this.marketOptionPk = new MarketOptionPK();
	this.marketOptionPk.setClientNo(clientNo);
	this.marketOptionPk.setSubSystem(subSystem);
	this.marketOptionPk.setMedium(medium);
	this.marketOptionPk.setCreateDateTime(createDate);
	this.createId = createId;
	this.deleteId = deleteId;
	this.deleteDateTime = deleteDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="createId"
     */
    public String getCreateId() {
	return createId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="deleteDateTime"
     */
    public DateTime getDeleteDateTime() {
	return deleteDateTime;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="deleteId"
     */
    public String getDeleteId() {
	return deleteId;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public void setDeleteDateTime(DateTime deleteDateTime) {
	this.deleteDateTime = deleteDateTime;
    }

    public void setDeleteId(String deleteId) {
	this.deleteId = deleteId;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public MarketOptionPK getMarketOptionPk() {
	return this.marketOptionPk;
    }

    public void setMarketOptionPk(MarketOptionPK mPk) {
	this.marketOptionPk = mPk;
    }

    public Integer getClientNo() {
	return this.marketOptionPk.clientNo;
    }

    public String getSubSystem() {
	return this.marketOptionPk.subSystem;
    }

    public String getMedium() {
	return this.marketOptionPk.medium;
    }

    public DateTime getCreateDateTime() {
	return this.marketOptionPk.createDateTime;
    }

    public boolean isUnavailable() {
	if (this.unavailable == null)
	    return true;
	else
	    return unavailable.booleanValue();
    }

    public void setUnavailable(Boolean unavailable) {
	this.unavailable = unavailable;
    }

    public void setClientNo(Integer clientNo) {
	this.marketOptionPk.clientNo = clientNo;
    }

    public void setSubSystem(String subSystem) {
	this.marketOptionPk.subSystem = subSystem;
    }

    public void setMedium(String medium) {
	this.marketOptionPk.medium = medium;
    }

    public void setCreateDateTime(DateTime cdt) {
	this.marketOptionPk.createDateTime = cdt;
    }

    public String toString() {
	return "clientNo =   " + this.marketOptionPk.clientNo + "\nsubSystem =  " + this.marketOptionPk.subSystem + "\nMedium =     " + this.marketOptionPk.medium + "\nCreateDate = " + this.marketOptionPk.createDateTime.formatLongDate() + "\ncreateId =   " + this.createId + "\ndeleteDate = " + this.deleteDateTime.formatLongDate() + "\ndeleteId =   " + this.deleteId + "\nunAvailable = " + this.isUnavailable();
    }

    public String list() {
	return this.marketOptionPk.subSystem + "; " + this.marketOptionPk.medium + "; " + (this.isUnavailable() ? "Unavailable" : "Available");

    }

}
