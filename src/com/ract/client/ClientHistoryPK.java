package com.ract.client;

import java.io.Serializable;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class ClientHistoryPK implements Serializable {

    public ClientHistoryPK() {

    }

    private Integer clientNumber;

    private DateTime modifyDateTime;

    /**
     * 
     * @hibernate.key-property position="2"
     * @hibernate.column name="[client-no]"
     * 
     */
    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((clientNumber == null) ? 0 : clientNumber.hashCode());
	result = prime * result + ((modifyDateTime == null) ? 0 : modifyDateTime.hashCode());
	return result;
    }

    public String getLongModifyDateTime() {
	return this.modifyDateTime.formatLongDate();
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof ClientHistoryPK))
	    return false;
	ClientHistoryPK other = (ClientHistoryPK) obj;

	LogUtil.log(this.getClass(), "other=" + other.modifyDateTime.formatLongDate());
	LogUtil.log(this.getClass(), "this=" + modifyDateTime.formatLongDate());

	if (clientNumber == null) {
	    if (other.clientNumber != null)
		return false;
	} else if (!clientNumber.equals(other.clientNumber))
	    return false;
	if (modifyDateTime == null) {
	    if (other.modifyDateTime != null)
		return false;
	} else if (!modifyDateTime.equals(other.modifyDateTime))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ClientHistoryPK [clientNumber=" + clientNumber + ", modifyDateTime=" + (modifyDateTime == null ? "" : modifyDateTime.formatLongDate()) + "]";
    }

    /**
     * 
     * @hibernate.key-property type="com.ract.util.DateTimeCombined"
     *                         position="2"
     * @hibernate.column name="[modify-date]"
     * @hibernate.column name="[modify-time]"
     * 
     */
    public DateTime getModifyDateTime() {
	return modifyDateTime;
    }

    public void setModifyDateTime(DateTime modifyDateTime) {
	this.modifyDateTime = modifyDateTime;
    }

}
