package com.ract.client;

import java.util.Collection;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.LogUtil;

public class MarkNoteCompleteAction extends ActionSupport implements SessionAware {

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public boolean isCanMarkComplete() {
	return canMarkComplete;
    }

    public void setCanMarkComplete(boolean canMarkComplete) {
	this.canMarkComplete = canMarkComplete;
    }

    public boolean isCanUnblockTransactions() {
	return canUnblockTransactions;
    }

    public void setCanUnblockTransactions(boolean canUnblockTransactions) {
	this.canUnblockTransactions = canUnblockTransactions;
    }

    public NoteCategory getNoteCategory() {
	return noteCategory;
    }

    public void setNoteCategory(NoteCategory noteCategory) {
	this.noteCategory = noteCategory;
    }

    public NoteType getNoteType() {
	return noteType;
    }

    public void setNoteType(NoteType noteType) {
	this.noteType = noteType;
    }

    public ClientNote getClientNote() {
	return clientNote;
    }

    public void setClientNote(ClientNote clientNote) {
	this.clientNote = clientNote;
    }

    public ClientVO getClient() {
	return client;
    }

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private NoteCategory noteCategory;

    private Map session = null;

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public Collection<NoteResult> getCompletionResults() {
	return completionResults;
    }

    public void setCompletionResults(Collection<NoteResult> completionResults) {
	this.completionResults = completionResults;
    }

    public Integer getNoteId() {
	return noteId;
    }

    private ClientNote clientNote;

    private ClientVO client;

    private Integer clientNumber;

    @InjectEJB(name = "ClientMgrBean")
    ClientMgrLocal clientMgr;

    private Collection<NoteResult> completionResults;

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    private NoteType noteType;

    private Integer noteId;

    @Override
    public String execute() throws Exception {
	UserSession us = (UserSession) session.get(LoginUIConstants.USER_SESSION);

	clientNote = clientMgr.getClientNote(noteId);
	client = clientMgr.getClient(clientNote.getClientNumber());
	clientNumber = clientNote.getClientNumber();
	completionResults = clientMgr.getNoteResults(clientNote.getNoteType());

	noteType = clientMgr.getNoteType(clientNote.getNoteType());

	canMarkComplete = ClientHelper.canMarkComplete(noteType, clientNote, noteCategory, us);
	canUnblockTransactions = ClientHelper.canUnblockTransactions(noteType, clientNote, noteCategory, us);

	LogUtil.log(this.getClass(), "completionResults=" + completionResults);
	return SUCCESS;
    }

    private boolean canMarkComplete;

    private boolean canUnblockTransactions;

}
