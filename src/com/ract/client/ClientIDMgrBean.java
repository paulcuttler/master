package com.ract.client;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.sql.DataSource;

import com.progress.open4gl.IntHolder;
import com.ract.common.CommonConstants;
import com.ract.common.DataSourceFactory;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.util.ConnectionUtil;
import com.ract.util.LogUtil;

public class ClientIDMgrBean implements SessionBean {
    private DataSource dataSource;

    private SessionContext sessionContext;

    public void ejbCreate() {
    }

    public void ejbRemove() throws RemoteException {
    }

    public void ejbActivate() throws RemoteException {
    }

    public void ejbPassivate() throws RemoteException {
    }

    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	} catch (Exception e) {
	    throw new EJBException("Error looking up dataSource: " + e.toString());
	}
    }

    /**
     * Return the next ID in the occupation ID sequence.
     */
    public int getNextOccupationID() throws RemoteException {
	return ClientIDCache.getInstance().getNextOccupationID(this);
    }

    private final String SEQUENCE_CLIENT_KEY = "CLIENT#";

    /**
     * Return the next ID in the Client ID sequence.
     */
    public int getNextClientID() throws RemoteException {
	int clientNo = 0;
	IntHolder clientNumber = new IntHolder();
	ClientProgressProxy clientProgressProxy = null;
	try {
	    clientProgressProxy = (ClientProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_CLIENT_PROXY);
	    clientProgressProxy.getClientNumber(SEQUENCE_CLIENT_KEY, clientNumber);
	    if (clientNumber != null) {
		LogUtil.log(this.getClass(), "client number = " + clientNumber.getIntValue());
	    } else {
		LogUtil.log(this.getClass(), "client number is null");
	    }
	} catch (Exception oe) {
	    throw new RemoteException("Unable to get the client number.", oe);
	} finally {
	    try {
		ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_CLIENT_PROXY, clientProgressProxy);
	    } catch (Exception oe) {
		throw new RemoteException("Error returning client proxy object.", oe);
	    }
	}
	if (clientNumber != null) {
	    clientNo = clientNumber.getIntValue();
	} else {
	    throw new RemoteException("Progress proxy did not return a valid client number.");
	}

	if (clientNo == 0) {
	    throw new RemoteException("Client number may not be equal to '0'.");
	}

	return clientNo;
    }

    /**
     * Return the next ID in the Client ID sequence.
     */
    // public int getNextClientID() throws RemoteException
    // {
    // return ClientIDCache.getInstance().getNextClientID(this);
    // }

    /************************* sql methods *******************************/

    /**
     * get the last client id
     */
    // public int getLastClientID()
    // throws RemoteException
    // {
    // int lastID = 0;
    // Connection connection = null;
    // String statementText =
    // "select max(\"client-no\") from PUB.\"cl-master\"";
    // PreparedStatement statement = null;
    // try
    // {
    // connection = dataSource.getConnection();
    // statement = connection.prepareStatement(statementText);
    // ResultSet resultSet = statement.executeQuery();
    // if(resultSet.next())
    // {
    // lastID = resultSet.getInt(1);
    // }
    // }
    // catch(SQLException e)
    // {
    // throw new EJBException("Error executing SQL " + statementText + " : " +
    // e.toString());
    // }
    // finally
    // {
    // closeConnection(connection, statement);
    // }
    // return lastID;
    // }

    /**
     * get the last occupation id
     */
    public int getLastOccupationID() throws RemoteException {
	int lastID = 0;
	Connection connection = null;
	String statementText = "select max(\"occupation_id\") from PUB.\"cl_occupation\"";
	PreparedStatement statement = null;
	try {
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		lastID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return lastID;
    }

}
