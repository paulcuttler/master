package com.ract.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import com.ract.util.DateTime;

/**
 * 
 * <p>
 * Represent a note attached to a client.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
@Entity
public class ClientNote implements Serializable, Comparable {

    public String getPreferredContactTime() {
	return preferredContactTime;
    }

    public void setPreferredContactTime(String preferredContactTime) {
	this.preferredContactTime = preferredContactTime;
    }

    public String getUnblockId() {
	return unblockId;
    }

    public void setUnblockId(String unblockId) {
	this.unblockId = unblockId;
    }

    public DateTime getUnblockDate() {
	return unblockDate;
    }

    public void setUnblockDate(DateTime unblockDate) {
	this.unblockDate = unblockDate;
    }

    public String getTargetSalesBranch() {
	return targetSalesBranch;
    }

    public void setTargetSalesBranch(String targetSalesBranch) {
	this.targetSalesBranch = targetSalesBranch;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((noteId == null) ? 0 : noteId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof ClientNote))
	    return false;
	ClientNote other = (ClientNote) obj;
	if (noteId == null) {
	    if (other.noteId != null)
		return false;
	} else if (!noteId.equals(other.noteId))
	    return false;
	return true;
    }

    @Override
    public int compareTo(Object o) {
	final int BEFORE = -1;
	final int EQUAL = 0;
	final int AFTER = 1;

	if (this == o)
	    return EQUAL;

	if (o instanceof ClientNote) {
	    ClientNote that = (ClientNote) o;
	    if (this.equals(that)) {
		return EQUAL;
	    }

	    if (this.created != null && that.created != null) {
		return this.created.compareTo(that.created);
	    } else if (this.created != null && that.created == null) {
		return BEFORE;
	    } else if (this.created == null && that.created != null) {
		return AFTER;
	    }
	}

	return EQUAL;
    }

    public DateTime getPreferredContactDate() {
	return preferredContactDate;
    }

    public void setPreferredContactDate(DateTime preferredContactDate) {
	this.preferredContactDate = preferredContactDate;
    }

    public BigDecimal getResultValue() {
	return resultValue;
    }

    public void setResultValue(BigDecimal resultValue) {
	this.resultValue = resultValue;
    }

    public String getCompletionId() {
	return completionId;
    }

    public void setCompletionId(String completionId) {
	this.completionId = completionId;
    }

    public Collection<ClientNoteComment> getClientNoteComments() {
	return clientNoteComments;
    }

    public void setClientNoteComments(Collection<ClientNoteComment> clientNoteComments) {
	this.clientNoteComments = clientNoteComments;
    }

    public boolean isBlockTransactions() {
	return blockTransactions;
    }

    public void setBlockTransactions(boolean blockTransactions) {
	this.blockTransactions = blockTransactions;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public String getBusinessType() {
	return businessType;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    // public String getReferenceId()
    // {
    // return referenceId;
    // }
    //
    // public void setReferenceId(String referenceId)
    // {
    // this.referenceId = referenceId;
    // }

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    public String getCategoryCode() {
	return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
    }

    public String getSourceId() {
	return sourceId;
    }

    public void setSourceId(String sourceId) {
	this.sourceId = sourceId;
    }

    private boolean blockTransactions;

    private DateTime cancelDate;

    private String cancelId;

    public DateTime getCancelDate() {
	return cancelDate;
    }

    public void setCancelDate(DateTime cancelDate) {
	this.cancelDate = cancelDate;
    }

    public String getCancelId() {
	return cancelId;
    }

    public void setCancelId(String cancelId) {
	this.cancelId = cancelId;
    }

    public String getTargetId() {
	return targetId;
    }

    public void setTargetId(String targetId) {
	this.targetId = targetId;
    }

    public DateTime getDue() {
	return due;
    }

    public void setDue(DateTime due) {
	this.due = due;
    }

    public String getPreferredContactNumber() {
	return preferredContactNumber;
    }

    public void setPreferredContactNumber(String preferredContactNumber) {
	this.preferredContactNumber = preferredContactNumber;
    }

    public DateTime getCompletionDate() {
	return completionDate;
    }

    public void setCompletionDate(DateTime completionDate) {
	this.completionDate = completionDate;
    }

    public String getCompletionNotes() {
	return completionNotes;
    }

    public void setCompletionNotes(String completionNotes) {
	this.completionNotes = completionNotes;
    }

    public String getCompletionReferenceId() {
	return completionReferenceId;
    }

    public void setCompletionReferenceId(String completionReferenceId) {
	this.completionReferenceId = completionReferenceId;
    }

    public String getResultCode() {
	return resultCode;
    }

    public void setResultCode(String resultCode) {
	this.resultCode = resultCode;
    }

    // public static String NOTE_TYPE_INFORMATION = "Information";
    //
    // public static String NOTE_TYPE_BLOCKER = "Blocker";
    //
    // public static String NOTE_TYPE_REFERRAL = "Referral";
    //
    // public static String NOTE_TYPE_FOLLOW_UP = "Followup";

    private Integer clientNumber;

    private String businessType;

    // private String referenceId;

    @Id
    private Integer noteId;

    private String noteType;

    private String noteText;

    private String categoryCode;

    private String createId;

    private DateTime created;

    private String sourceId;

    private String targetId;

    private DateTime due;

    private String preferredContactTime;

    private DateTime preferredContactDate;

    private String preferredContactNumber;

    private DateTime completionDate;

    private String completionNotes;

    private String completionReferenceId;

    private String resultCode;

    private String completionId;

    private String unblockId;

    private DateTime unblockDate;

    private BigDecimal resultValue;

    public DateTime getCreated() {
	return created;
    }

    public String getCreateId() {
	return createId;
    }

    public String getNoteText() {
	return noteText;
    }

    public String getNoteType() {
	return noteType;
    }

    public void setCreated(DateTime created) {
	this.created = created;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public void setNoteText(String noteText) {
	this.noteText = noteText;
    }

    @RequiredFieldValidator(type = ValidatorType.FIELD, message = "Note type is required.")
    public void setNoteType(String noteType) {
	this.noteType = noteType;
    }

    public ClientNote() {
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(getNoteId() + ":");
	desc.append(getNoteType() + ",");
	desc.append(getBusinessType() + ",");
	desc.append(getClientNumber() + ",");
	desc.append(getNoteText());
	return desc.toString();
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "noteId")
    private Collection<ClientNoteComment> clientNoteComments = new ArrayList<ClientNoteComment>();

    private String targetSalesBranch;

}
