package com.ract.client.email;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.util.LogUtil;

public class EmailAddressJob implements Job {

    public EmailAddressJob() {
    }

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	String instName = context.getJobDetail().getName();
	String instGroup = context.getJobDetail().getGroup();

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();

	try {
	    String inputFileName = dataMap.getString("inputFile");
	    String providerURL = dataMap.getString("providerURL");

	    BouncedEmailAddressUpdater email = new BouncedEmailAddressUpdater(providerURL, inputFileName);
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }

}
