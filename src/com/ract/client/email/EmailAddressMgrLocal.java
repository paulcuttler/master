package com.ract.client.email;

import java.rmi.RemoteException;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface EmailAddressMgrLocal extends EmailAddressMgr {

    public void processBouncedEmailAddress(String[] line, Map<String, String> attributeNames) throws RemoteException;
}
