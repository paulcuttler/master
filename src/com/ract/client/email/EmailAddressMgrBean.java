package com.ract.client.email;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.Client;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientTransaction;
import com.ract.client.ClientVO;
import com.ract.common.CommonConstants;
import com.ract.common.SystemParameterVO;
import com.ract.util.ConnectionUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

@Stateless
@Remote({ EmailAddressMgr.class })
@Local({ EmailAddressMgrLocal.class })
public class EmailAddressMgrBean {

    @Resource
    private SessionContext sessionContext;

    @Resource(mappedName = "java:/ClientDS")
    public DataSource dataSource;

    @EJB
    private ClientMgrLocal clientMgrLocal;

    @EJB
    private EmailAddressMgrLocal emailMgrLocal;

    public void processBouncedEmailAddressFile(String inputFile) throws RemoteException {

	Map<String, String> attributeNames = getBouncedEmailAddressCSVattributeNames();

	CSVReader reader = null;
	try {
	    // Open the EmailAddress file, ignore quotes
	    reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));

	    String[] line = null;

	    // Process the EmailAddress file line by line
	    while ((line = reader.readNext()) != null) {
		emailMgrLocal.processBouncedEmailAddress(line, attributeNames);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    LogUtil.fatal(this.getClass(), "Fatal error has prevented the completion of the processing the EmailAddress file. " + e);
	    throw new RemoteException("Fatal error has prevented the completion of the processing the EmailAddress file.", e);
	} finally {
	    try {
		reader.close();
	    } catch (IOException ex) {
		// ignore
	    }
	}

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void processBouncedEmailAddress(String[] line, Map<String, String> attributeNames) throws RemoteException {

	Map<String, String> EmailAddress = parseInputLine(line, attributeNames);

	try {
	    if (parsedOK(EmailAddress)) {
		processThisEmailAddress(EmailAddress, attributeNames);
	    }
	} catch (Exception ex) {
	    // Do Nothing
	    LogUtil.log(this.getClass(), "Unable to process " + line);
	    LogUtil.log(this.getClass(), ex.getMessage());
	}

    }

    /**
     * Find clients matching email address and either remove whitespace if that
     * is the reason for bounce or clear email field. Do nothing if bounce
     * reason is "Mailbox Full"
     * 
     * @param EmailAddress
     * @param attributeNames
     */
    private void processThisEmailAddress(Map<String, String> EmailAddress, Map<String, String> attributeNames) throws RemoteException {

	String emailAddress = EmailAddress.get("Email");
	String bounceReason = EmailAddress.get("BounceReason");

	LogUtil.log(this.getClass(), "Bounce processing " + emailAddress + ". Reason is - " + bounceReason);

	boolean removeWhitespace = (bounceReason.indexOf(SystemParameterVO.BOUNCE_REASON_ILLEGAL_WHITESPACE) != -1);
	boolean mailboxFull = (bounceReason.indexOf(SystemParameterVO.BOUNCE_REASON_MAILBOX_FULL) != -1);

	if (mailboxFull) {
	    LogUtil.log(this.getClass(), "Mailbox is full, ignoring.");
	    return;
	}

	try {

	    Collection<ClientVO> clientList = clientMgrLocal.findClientsByEmailAddress(emailAddress);

	    if (clientList != null) {
		LogUtil.log(this.getClass(), "Found " + clientList.size() + " client(s) matching email address");
	    } else {
		LogUtil.log(this.getClass(), "Found 0 client(s) matching email address");
		return;
	    }

	    for (ClientVO client : clientList) {
		// sanity check to ensure email matches exactly
		if (client.getEmailAddress().equalsIgnoreCase(emailAddress)) {
		    // LogUtil.log(this.getClass(), "Present email address " +
		    // client.getEmailAddress());
		    if (removeWhitespace) {
			String newAddress = StringUtil.removeWhitespace(client.getEmailAddress());
			LogUtil.log(this.getClass(), "rlg +++++++++++++ " + newAddress + " / " + client.getEmailAddress());
			client.setEmailAddress(newAddress);
		    } else {
			client.setEmailAddress("");
		    }
		    LogUtil.log(this.getClass(), "Changed email address " + client.getEmailAddress());

		    // Save it away
		    String userID = "COM";
		    String transactionReason = Client.HISTORY_UPDATE;
		    String salesBranchCode = "HOB";
		    String comment = "Email Address Bounced ";

		    ClientTransaction clientTx = new ClientTransaction(ClientTransaction.TRANSACTION_UPDATE, client, userID, transactionReason, salesBranchCode, comment);
		    clientTx.submit();
		}
	    }
	} catch (Exception ex) {
	    LogUtil.log(this.getClass(), "Unable to process client because " + ex.getMessage());
	}

    }

    /**
     * Retrieve a list of Bounce Email CSV file attribute names.
     * 
     * @todo convert to EJB
     * @return
     */
    private Map<String, String> getBouncedEmailAddressCSVattributeNames() {

	String sql = "select [tab-name], [tab-key], [tab-desc], [tab-log], ";
	sql += "[tab-date], [tab-dec0], [tab-dec4] from pub.[gn-table] ";
	sql += "where [tab-name] = ? ";
	sql += "and [tab-key] like ? ";

	Connection connection = null;
	PreparedStatement statement = null;
	Map<String, String> attributeNames = new HashMap<String, String>();
	String val;
	String fldName = "fldName";
	Integer i;
	int y = 0;

	try {
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(sql);

	    statement.setString(1, SystemParameterVO.CATEGORY_EMAIL_ADDRESS_MAINTENANCE);
	    statement.setString(2, fldName + CommonConstants.WILDCARD);

	    ResultSet rs = statement.executeQuery();

	    /*
	     * TAB-KEY have the value fldNameNNN where attributeName is the
	     * literal attributeName and NNN is the attributenumber e.g. 003
	     */

	    while (rs.next()) {
		val = rs.getString(2);

		i = new Integer(val.substring(fldName.length()));

		attributeNames.put(i.toString(), rs.getString(3));
	    }
	} catch (SQLException ex) {
	    LogUtil.log(this.getClass(), "Getting attribute names " + ex.getMessage());
	    attributeNames = null;
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return attributeNames;

    }

    /**
     * parses a line from the EmailAddress file from telstra. If there were no
     * errors, it returns a EmailAddressUpdate object, otherwise a
     * EmailAddressFileFormatException is thrown
     */
    private Map<String, String> parseInputLine(String[] line, Map<String, String> attributeNames) {

	Map<String, String> attributes = new HashMap<String, String>();

	//
	// Parse this Line
	//
	// line = StringUtil.replace(line, ",,", ", ,");
	//
	// String lineTokens[] = line.split(",");

	attributes.put("AttributesExpected", new Integer(attributeNames.size()).toString());
	attributes.put("AttributesParsed", new Integer(line.length).toString());

	for (int i = 0; i < line.length; i++) {
	    attributes.put((attributeNames.get(new Integer(i + 1).toString())), line[i]);
	}

	return attributes;
    }

    private boolean parsedOK(Map<String, String> parsedLine) {
	String expected = parsedLine.get("AttributesExpected");
	String parsed = parsedLine.get("AttributesParsed");

	return expected.equalsIgnoreCase(parsed);
    }

}
