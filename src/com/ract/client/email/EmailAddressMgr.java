package com.ract.client.email;

import java.rmi.RemoteException;

import javax.ejb.Remote;

/**
 * Description of the Interface
 * 
 */
@Remote
public interface EmailAddressMgr {
    /**
    */
    public void processBouncedEmailAddressFile(String inputFile) throws RemoteException;

}
