package com.ract.client.email;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

public class EmailAddressEJBHelper {
    public static EmailAddressMgr getEmailAddressMgr() {
	EmailAddressMgr emailAddressMgr = null;
	try {
	    emailAddressMgr = (EmailAddressMgr) ServiceLocator.getInstance().getObject("EmailAddressMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return emailAddressMgr;
    }

}
