package com.ract.client.email;

import java.util.Date;

import com.ract.util.LogUtil;

/**
 * Update reader.
 * 
 * @author Leigh Giles
 * @version 1.0
 */

public class BouncedEmailAddressUpdater {

    public BouncedEmailAddressUpdater(String URL, String inputFile) {
	LogUtil.log(this.getClass(), URL);
	String rmi[] = URL.split(":");

	this.UpdateBouncedEmailAddress(rmi[0], rmi[1], inputFile);
    }

    public BouncedEmailAddressUpdater(String host, String port, String inputFile) {
	this.UpdateBouncedEmailAddress(host, port, inputFile);
    }

    /**
     * 
     * @param host
     *            String
     * @param port
     *            String
     * @param inputFile
     *            String
     * @param dateToProcess
     *            String
     */

    private void UpdateBouncedEmailAddress(String host, String port, String inputFile) {

	EmailAddressMgr emailAddressMgr = null;
	try {
	    System.out.println("Got Context");
	    emailAddressMgr = EmailAddressEJBHelper.getEmailAddressMgr();
	} catch (Exception ne) {
	    ne.printStackTrace();
	    System.out.println("Failed initializing access to EmailAddress Manager home interface: " + ne.getMessage());

	}

	try {
	    System.out.println(" ======================================================== ");
	    System.out.println(" Host                 " + host);
	    System.out.println(" Port                 " + port);
	    System.out.println(" EmailAddress file    " + inputFile);
	    System.out.println(" Starting             " + new Date().toString());
	    System.out.println(" ======================================================== ");

	    emailAddressMgr.processBouncedEmailAddressFile(inputFile);

	    System.out.println(" Finished             " + new Date().toString());

	} catch (Exception e) {
	    System.out.println("EmailAddressUpdateReader +++++++ Exception " + e.getMessage());
	    System.out.println("EmailAddressUpdateReader +++++++ FINISHING");
	}

    }

    /**
     * The main program for the EmailAddressUpdateReader class
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args) {

	if (args.length < 3) {
	    System.out.println("Not enough command line arguments.");
	    System.out.println("Usage: EmailAddressUpdateReader [localhost] [1099] [input file]");
	    return;
	}
	BouncedEmailAddressUpdater subs = new BouncedEmailAddressUpdater(args[0], args[1], args[2]);
    }

}
