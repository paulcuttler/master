package com.ract.client;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.util.LogUtil;

public class ResultValueAction extends ActionSupport {

    public String getResultCode() {
	return resultCode;
    }

    public void setResultCode(String resultCode) {
	this.resultCode = resultCode;
    }

    public NoteResult getNoteResult() {
	return noteResult;
    }

    public void setNoteResult(NoteResult noteResult) {
	this.noteResult = noteResult;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgr clientMgr;

    @Override
    public String execute() throws Exception {
	LogUtil.debug(this.getClass(), "execute");

	noteResult = clientMgr.getNoteResult(resultCode);

	return SUCCESS;
    }

    private NoteResult noteResult;

    private String resultCode;

}
