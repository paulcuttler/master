package com.ract.client;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface ClientIDMgr extends EJBObject {
    public int getNextClientID() throws RemoteException;

    public int getNextOccupationID() throws RemoteException;
}
