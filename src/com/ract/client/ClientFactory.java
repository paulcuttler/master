package com.ract.client;

import java.rmi.RemoteException;
import java.text.ParseException;

import javax.ejb.CreateException;
import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.AddressVO;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;

/**
 * Factory to return the correct VO class for a client type.
 * 
 * @author John Holliday
 * @created 31 July 2002
 * @version 1.0
 */

public class ClientFactory {

    /**
     * Gets the personVO attribute of the ClientFactory class
     * 
     * @return The personVO value
     */
    // public static PersonVO getPersonVO()
    // {
    // return new PersonVO();
    // }

    /**
     * Gets the organisationVO attribute of the ClientFactory class
     * 
     * @return The organisationVO value
     */
    // public static OrganisationVO getOrganisationVO()
    // {
    // return new OrganisationVO();
    // }

    /**
     * Gets the clientVO attribute of the ClientFactory class
     * 
     * @param clientType
     *            Description of the Parameter
     * @return The clientVO value
     */
    // public static ClientVO getClientVO(String clientType)
    // {
    // if(ClientVO.CLIENT_TYPE_ORGANISATION.equals(clientType))
    // {
    // return getOrganisationVO();
    // }
    // else if(ClientVO.CLIENT_TYPE_PERSON.equals(clientType))
    // {
    // return getPersonVO();
    // }
    // else
    // {
    // return null;
    // }
    // }

    /**
     * Create a new client using the data from the XML DOM node. The returned
     * client will have a mode of "History" and it will be set to read only.
     * 
     * @param clientNode
     *            Description of the Parameter
     * @return The clientVO value
     * @exception CreateException
     *                Description of the Exception
     */
    public static ClientVO getClientVO(Node clientNode) throws RemoteException {
	ClientVO clientVO = null;
	if (clientNode != null) {
	    // Find the clientType node to figure out which type of client to
	    // construct.
	    NodeList nodeList = clientNode.getChildNodes();
	    Node elementNode = null;
	    String clientType = null;
	    int i = 0;

	    // try
	    // {
	    // clientVO = new ClientVO(clientNode);
	    // }
	    // catch(RemoteException ex)
	    // {
	    // throw new SystemException(ex);
	    // }

	    while (i < nodeList.getLength() && clientType == null) {
		if (nodeList.item(i).getNodeName().equals("clientType")) {
		    clientType = nodeList.item(i).getFirstChild().getNodeValue();
		}
		i++;
	    }

	    if (ClientVO.CLIENT_TYPE_ORGANISATION.equals(clientType)) {
		clientVO = new OrganisationVO(clientNode);
	    } else if (ClientVO.CLIENT_TYPE_PERSON.equals(clientType)) {
		clientVO = new PersonVO(clientNode);
	    } else {
		throw new SystemException("The client type " + clientType + " could not be determined.");
	    }
	}
	return clientVO;
    }

    public static ClientAdapter getClientAdapter() {
	return new ProgressClientAdapter();
    }

    /**
     * @todo review set methods to determine if all attributes are set
     * @param request
     *            HttpServletRequest
     * @throws RemoteException
     * @return ClientVO
     */
    private static ClientVO getClientVOFromRequest(HttpServletRequest request) throws RemoteException {
	String clientNumberString = request.getParameter("clientNumber");
	Integer clientNumber = null;
	if (clientNumberString != null && !clientNumberString.equals("")) {
	    clientNumber = new Integer(clientNumberString);
	}

	String clientTitle = request.getParameter("title");
	String givenNames = request.getParameter("givennames");
	String surname = request.getParameter("surname");

	// initials are set elsewhere

	String birthDate = request.getParameter("birthdate");
	DateTime DOB = null;
	String gender = request.getParameter("gender");

	boolean sex = false;
	if (gender != null && !gender.equals("")) {
	    if (gender.equalsIgnoreCase(ClientVO.GENDER_MALE)) {
		sex = true;
	    }
	}
	String homePhone = request.getParameter("homephone");
	String workPhone = request.getParameter("workphone");
	String mobilePhone = request.getParameter("mobilephone");
	String emailAddress = request.getParameter("email");
	String masterClientNumberString = request.getParameter("masterClientNumber");

	Integer masterClientNumber = null;
	if (masterClientNumberString != null && !masterClientNumberString.equals("")) {
	    masterClientNumber = new Integer(masterClientNumberString);
	}
	LogUtil.debug("ClientFactory", "masterClientNumber = " + masterClientNumber);

	String status = request.getParameter("status");
	LogUtil.debug("ClientFactory", "status = " + status);
	String maritalStatus = request.getParameter("maritalstatus");
	boolean marital = false;
	if (maritalStatus != null && !maritalStatus.equals("")) {
	    if (maritalStatus.equalsIgnoreCase(ClientVO.MARITAL_STATUS_MARRIED)) {
		marital = true;
	    }
	}
	String numberOfDependants = request.getParameter("numberofdependants");
	Integer children = null;
	if (numberOfDependants != null && !numberOfDependants.equals("")) {
	    children = new Integer(numberOfDependants);
	}

	if (birthDate != null && !birthDate.equals("")) {
	    try {
		DOB = new DateTime(birthDate);
	    } catch (ParseException pe) {
		throw new ValidationException("The date of birth is not a valid date " + pe.getMessage());
	    }
	}

	// load the VO
	ClientVO client = null;
	if (clientNumber != null) {
	    ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	    client = clientMgr.getClient(clientNumber);
	    client.setLastUpdate(new DateTime());
	} else {
	    client = new ClientVO();
	    client.setMadeDate(new DateTime());
	    client.setMadeID(ServletUtil.getUserSession(request, true).getUserId());
	}

	String marketString = request.getParameter("market");
	LogUtil.debug("ClientFactory", "market=" + marketString);
	String deliveryMethod = request.getParameter("motorNewsDeliveryMethod");
	LogUtil.debug("ClientFactory", "deliveryMethod=" + deliveryMethod);
	String sendOption = request.getParameter("motorNewsSendOption");
	LogUtil.debug("ClientFactory", "sendOption=" + sendOption);
	// String preferredContactMethod =
	// request.getParameter("preferredContactMethod");
	// LogUtil.log("ClientFactory","preferredContactMethod="+preferredContactMethod);

	client.setMotorNewsDeliveryMethod(deliveryMethod);
	client.setMotorNewsSendOption(sendOption);

	String group = request.getParameter("group");
	Boolean groupClient = group == null ? new Boolean(false) : new Boolean(group.equals("Yes"));
	client.setGroupClient(groupClient);

	boolean market = false;
	if (marketString != null && !marketString.equals("")) {
	    market = marketString.equals("Yes") ? true : false;
	}
	client.setMarket(new Boolean(market));
	String sms = request.getParameter("sms");
	LogUtil.debug("ClientFactory", "sms=" + sms);
	if (!sms.equals("")) {
	    client.setSmsAllowed(new Boolean(sms.equalsIgnoreCase("Yes") ? true : false));
	}
	// client.setPreferredContactMethod(preferredContactMethod);

	client.setTitle(clientTitle);
	client.setGivenNames(givenNames);
	client.setSurname(surname);
	client.setBirthDate(DOB);
	client.setSex(new Boolean(sex));
	client.setHomePhone(homePhone);
	client.setWorkPhone(workPhone);
	client.setMobilePhone(mobilePhone);
	client.setEmailAddress(emailAddress);
	client.setMaritalStatus(new Boolean(marital));
	/*
	 * client.setOccupationID(occupationID);
	 */
	client.setNumberOfDependants(children);

	// set the address
	setAddress(request, client);

	client.setMasterClientNumber(masterClientNumber);
	client.setStatus(status);

	// setup the derived fields
	client.setDerivedFields();

	return client;

    }

    /**
     * Sets the address attribute of the ClientUIC object
     * 
     */
    private static void setAddress(HttpServletRequest request, ClientVO cvo) {

	/*
	 * Get the address parameters
	 */
	String saProperty = request.getParameter("saProperty");
	String saPropertyQualifier = request.getParameter("saPropertyQualifier");
	String saStsubid = request.getParameter("saStsubid");
	String saDpid = request.getParameter("saDpid");

	String paProperty = request.getParameter("paProperty");
	String paPropertyQualifier = request.getParameter("paPropertyQualifier");
	String paStsubid = request.getParameter("paStsubid");
	String paDpid = request.getParameter("paDpid");

	LogUtil.log("ClientFactory", "sa " + saProperty + "/" + saPropertyQualifier + "/" + saStsubid + "/" + saDpid);
	LogUtil.log("ClientFactory", "pa " + paProperty + "/" + paPropertyQualifier + "/" + paStsubid + "/" + paDpid);

	// set the addresses
	if (paPropertyQualifier == null) {
	    paPropertyQualifier = "";
	}
	if (paStsubid == null) {
	    paStsubid = "";
	}

	// Set the DPID's
	if (paDpid == null) {
	    paDpid = "";
	}

	if (!paStsubid.equals("")) {
	    AddressVO avo = new PostalAddressVO(paProperty, paPropertyQualifier, new Integer(paStsubid), paDpid);

	    cvo.setPostalAddress((PostalAddressVO) avo);
	    cvo.setPostProperty(avo.getProperty());
	    cvo.setPostStreetChar(avo.getPropertyQualifier());
	    cvo.setPostStreetNumber(avo.getStreetNumber());
	    cvo.setPostStsubId(avo.getStreetSuburbID());
	    cvo.setPostDpid(avo.getDpid());
	}

	if (saPropertyQualifier == null) {
	    saPropertyQualifier = "";
	}
	if (saStsubid == null) {
	    saStsubid = "";
	}

	if (saDpid == null) {
	    saDpid = "";
	}

	if (!saStsubid.equals("")) {
	    AddressVO avo = new ResidentialAddressVO(saProperty, saPropertyQualifier, new Integer(saStsubid), saDpid);
	    cvo.setResidentialAddress((ResidentialAddressVO) avo);
	    cvo.setResiProperty(avo.getProperty());
	    cvo.setResiStreetChar(avo.getPropertyQualifier());
	    cvo.setResiStreetNumber(avo.getStreetNumber());
	    cvo.setResiStsubId(avo.getStreetSuburbID());
	    cvo.setResiDpid(avo.getDpid());

	}
    }

}
