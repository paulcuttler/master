package com.ract.client;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.ract.common.CacheBase;

/**
 * Cache of client titles.
 * 
 * @author John Holliday
 * @created 2 April 2003
 * @version 1.0
 */

public class ClientTitleCache extends CacheBase {
    /**
     * Holds a reference to an instance of this class. The class instance is
     * created when the Class is first referenced.
     */
    private static ClientTitleCache instance = new ClientTitleCache();

    /**
     * Holds a reference to the ClientTitle home interface. Use the
     * getHomeInterface() method to populate.
     */
    // private ClientTitleHome clientTitleHome = null;

    /**
     * Return a reference to the ClientTitle home interface. Initialise the
     * reference if this is the first call.
     * 
     * @return The homeInterface value
     * @exception RemoteException
     *                Description of the Exception
     */
    // private ClientTitleHome getHomeInterface()
    // throws RemoteException
    // {
    // if(this.clientTitleHome == null)
    // {
    // this.clientTitleHome = ClientEJBHelper.getClientTitleHome();
    // }
    // return this.clientTitleHome;
    // }

    /**
     * Implement the initialiseList method of the super class. Initialise the
     * list of cached items.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void initialiseList() throws RemoteException {
	if (this.isEmpty()) {
	    this.addAll(ClientEJBHelper.getClientMgr().getClientTitles());
	}
    }

    /**
     * Return a reference the instance of this class.
     * 
     * @return The instance value
     */
    public static ClientTitleCache getInstance() {
	return instance;
    }

    /**
     * Return the specified discount type value object. Search the list of
     * products for one that has the same code. Use the getList() method to make
     * sure the list has been initialised.
     * 
     * @param clientTitle
     *            Description of the Parameter
     * @return The clientTitle value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ClientTitleVO getClientTitle(String clientTitle) throws RemoteException {
	return (ClientTitleVO) this.getItem(clientTitle);
    }

    /**
     * Return the list of discount types. Use the getList() method to make sure
     * the list has been initialised. A null or an empty list may be returned.
     * 
     * @return The clientTitleList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getClientTitleList() throws RemoteException {
	return this.getItemList();
    }

    /**
     * Return the caches reference to the DiscountType home interface
     * 
     * @return The clientTitleHome value
     * @exception RemoteException
     *                Description of the Exception
     */
    // public ClientTitleHome getClientTitleHome()
    // throws RemoteException
    // {
    // return this.getHomeInterface();
    // }
}
