package com.ract.client;

import java.util.Collection;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.BusinessType;
import com.ract.common.CommonMgrLocal;
import com.ract.common.SourceSystem;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipVO;

public class ViewClientNoteListAction extends ActionSupport {

    public Collection<BusinessType> getBusinessTypeList() {
	return businessTypeList;
    }

    public void setBusinessTypeList(Collection<BusinessType> businessTypeList) {
	this.businessTypeList = businessTypeList;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public String getReferenceId() {
	return referenceId;
    }

    public void setReferenceId(String referenceId) {
	this.referenceId = referenceId;
    }

    public String getBusinessType() {
	return businessType;
    }

    public void setBusinessType(String businessType) {
	this.businessType = businessType;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public Collection<NoteType> getNoteTypeList() {
	return noteTypeList;
    }

    public void setNoteTypeList(Collection<NoteType> noteTypeList) {
	this.noteTypeList = noteTypeList;
    }

    public Collection<ClientNote> getClientNoteList() {
	return clientNoteList;
    }

    public void setClientNoteList(Collection<ClientNote> clientNoteList) {
	this.clientNoteList = clientNoteList;
    }

    private Collection<BusinessType> businessTypeList;

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    @Override
    public String execute() throws Exception {
	noteTypeList = clientMgr.getNoteTypes();
	businessTypeList = commonMgr.getBusinessTypes();

	if (businessType != null && referenceId != null) {

	    if (SourceSystem.MEMBERSHIP.getAbbreviation().equals(businessType)) {
		MembershipVO membership = membershipMgr.getMembership(new Integer(referenceId));
		try {
		    client = membership.getClient();
		    clientNumber = client.getClientNumber();
		} catch (Exception e) {
		    addActionError(e.getMessage());
		    return ERROR;
		}
	    }

	}

	if (clientNumber != null) {
	    client = clientMgr.getClient(clientNumber);
	    clientNoteList = clientMgr.getClientNoteList(clientNumber);
	}

	if (client != null && clientNumber == null) {
	    clientNumber = client.getClientNumber();
	}

	return SUCCESS;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgr clientMgr;

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgr membershipMgr;

    private String referenceId;

    private String businessType;

    private ClientVO client;

    private Integer clientNumber;

    private Collection<NoteType> noteTypeList;

    private Collection<ClientNote> clientNoteList;

}
