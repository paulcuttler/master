package com.ract.client;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ClientNoteCommentPK implements Serializable {
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((commentSeq == null) ? 0 : commentSeq.hashCode());
	result = prime * result + ((noteId == null) ? 0 : noteId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ClientNoteCommentPK other = (ClientNoteCommentPK) obj;
	if (commentSeq == null) {
	    if (other.commentSeq != null)
		return false;
	} else if (!commentSeq.equals(other.commentSeq))
	    return false;
	if (noteId == null) {
	    if (other.noteId != null)
		return false;
	} else if (!noteId.equals(other.noteId))
	    return false;
	return true;
    }

    public Integer getNoteId() {
	return noteId;
    }

    public void setNoteId(Integer noteId) {
	this.noteId = noteId;
    }

    public Integer getCommentSeq() {
	return commentSeq;
    }

    public void setCommentSeq(Integer commentSeq) {
	this.commentSeq = commentSeq;
    }

    private Integer noteId;

    private Integer commentSeq;
}
