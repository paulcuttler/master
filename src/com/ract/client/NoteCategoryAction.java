package com.ract.client;

import java.util.Collection;
import java.util.regex.Matcher;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.BaseAction;
import com.ract.common.BusinessType;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.util.LogUtil;

public class NoteCategoryAction extends BaseAction implements ModelDriven, Preparable, ValidationAware {

    public String getRequestId() {
	return requestId;
    }

    public void setRequestId(String requestId) {
	this.requestId = requestId;
    }

    @Override
    public void validate() {

	if (model.getCategoryCode() == null || model.getCategoryCode().length() == 0) {
	    addFieldError("categoryCode", "Category code must be entered.");
	}

	if (model.getBusinessType() == null || model.getBusinessType().length() == 0) {
	    addFieldError("businessType", "Business type must be entered.");
	}

	if (model.isFollowUp()) {
	    if (model.getFollowUpDays() <= 0 || model.getFollowUpDays() > 7) {
		addFieldError("followUpDays", "Followup days should be greater than 0 but less than or equal to 7.");
	    }
	}

	if (model.getCategoryName() == null || model.getCategoryName().length() == 0) {
	    addFieldError("categoryName", "Category name  must be entered.");
	}

	boolean userIdExists = false;
	if (model.getNotificationUserId() == null || model.getNotificationUserId().length() == 0) {
	    // ignore
	} else {
	    userIdExists = true;
	}

	if (model.getNotificationEmailAddress() != null && model.getNotificationEmailAddress().length() > 0) {
	    if (userIdExists) {
		addActionError("You must specify either a notification user or a notification email address not both.");
	    }

	    // unable to get model driven field validator to work so...
	    Matcher m = PATTERN_EMAIL.matcher(model.getNotificationEmailAddress());
	    if (!m.find()) {
		addFieldError("notificationEmailAddress", "Notification email address is not valid.");
	    }
	} else {
	    if (!userIdExists) {
		addActionError("Either a notification user or an email address must be specified.");
	    }
	}

	LogUtil.log(this.getClass(), "validation end");
    }

    public Collection<User> getNotificationUsers() {
	return notificationUsers;
    }

    public void setNotificationUsers(Collection<User> notificationUsers) {
	this.notificationUsers = notificationUsers;
    }

    public Collection<BusinessType> getBusinessTypes() {
	return businessTypes;
    }

    public void setBusinessTypes(Collection<BusinessType> businessTypes) {
	this.businessTypes = businessTypes;
    }

    public void setModel(NoteCategory model) {
	this.model = model;
    }

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    @InjectEJB(name = "UserMgrBean")
    private UserMgrLocal userMgr;

    private NoteCategory model;

    @Override
    public Object getModel() {
	return model;
    }

    private Collection<BusinessType> businessTypes;

    @Override
    public String list() {
	list = clientMgr.getNoteCategories();
	return LIST;
    }

    private String requestId;

    @Override
    public void prepare() throws Exception {
	LogUtil.log(this.getClass(), "requestId=" + (getRequestId() == null ? "null" : getRequestId().toString()));
	if (getRequestId() == null) {
	    model = new NoteCategory();
	} else {
	    model = (NoteCategory) commonMgr.get(NoteCategory.class, getRequestId());
	}
	businessTypes = commonMgr.getBusinessTypes();
	notificationUsers = userMgr.getActiveUsers();
    }

    private Collection<User> notificationUsers;

}
