package com.ract.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import com.ract.common.RollBackException;
import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.util.DateTime;

/**
 * <p>
 * Transaction adapter to manage customer transactions.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public interface ClientAdapter extends TransactionAdapter {
    // public void createClient();

    public void updateClient(ClientVO cvo, String userId, String salesBranch, String comments, String subSystem) throws RollBackException;

    public Collection getClientsBySurname(String surname);

    public Collection getClientsByPhoneNumber(String phoneNumber);

    public Hashtable hasEligibleBusiness(Integer clientNumber) throws SystemException;

    public Collection getClientsBySurnameAndTitle(String surname, String title, String initials);

    public Collection getClientsBySurnameAndBirthDate(String surname, String initials, DateTime birthDate);

    public Collection getClientsBySurnameAndAddress(String surname, Integer streetNumber, Integer stsubid);

    public Collection getClientsByClientNumberRange(Integer startClientNumber, Integer endClientNumber) throws SystemException;

    public Collection getClientTitleList();

    public ClientVO getClient(Integer clientNumber);

    public void commit() throws SystemException;

    public String getSystemName();

    public void release();

    public String getTramadaClientXML(String action, String tramadaProfileCode, String debtorCode, String userId, String salesBranchCode, ClientVO client) throws SystemException;

    public void rollback() throws SystemException;

    public String[] getRelatedClients(Integer clientNo) throws SystemException;

    public ArrayList getClientBusiness(Integer clientNo) throws SystemException;

    public Collection<Integer> getOwnedGroups(Integer clientNo) throws SystemException;

    public boolean isProtected(Integer clientNo) throws SystemException;

    public ArrayList getClientGroup(Integer clientNo) throws SystemException;

    public void addFieldUpdate(Integer clientNumber, String fieldName, String fieldValue) throws RollBackException;

    public void clearAskable(Integer clientNumber, String fieldName) throws RollBackException;

    public boolean isAskable(Integer clientNumber, String fieldName, String fieldValue) throws SystemException;

}
