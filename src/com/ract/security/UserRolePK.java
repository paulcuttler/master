package com.ract.security;

import java.io.Serializable;

public class UserRolePK implements Serializable {

    private String userId;

    private String roleId;

    /**
     * @hibernate.key-property position="1" column="user_id"
     */
    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
	result = prime * result + ((userId == null) ? 0 : userId.hashCode());
	return result;
    }

    @Override
    public String toString() {
	return "UserRolePK [roleId=" + roleId + ", userId=" + userId + "]";
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof UserRolePK))
	    return false;
	UserRolePK other = (UserRolePK) obj;
	if (roleId == null) {
	    if (other.roleId != null)
		return false;
	} else if (!roleId.equals(other.roleId))
	    return false;
	if (userId == null) {
	    if (other.userId != null)
		return false;
	} else if (!userId.equals(other.userId))
	    return false;
	return true;
    }

    /**
     * @hibernate.key-property position="1" column="role_id"
     */
    public String getRoleId() {
	return roleId;
    }

    public void setRoleId(String roleId) {
	this.roleId = roleId;
    }
}
