package com.ract.security;

import java.util.ArrayList;

import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;

/**
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public interface SecurityAdapter extends TransactionAdapter {
    public boolean authenticateUser(String userName, String password) throws SystemException;

    public ArrayList getUserMenuMetaData(String userId, String menuName) throws SystemException;
}
