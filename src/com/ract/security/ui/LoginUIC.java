package com.ract.security.ui;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;
import com.ract.security.SecurityHelper;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgrLocal;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * Login servlet that controls a user login.
 * 
 * @author hollidayj
 * @created 31 July 2002
 */

public class LoginUIC extends HttpServlet {

    private final static String CONTENT_TYPE = "text/html";

    /**
     * Initialize global variables
     */

    private UserSession us;

    /**
     * Description of the Method
     * 
     * @exception ServletException
     *                Description of the Exception
     */
    public void init() throws ServletException {
    }

    /**
     * Process the HTTP Get request
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doPost(request, response);
    }

    public static final String EVENT_LOGIN = "login";

    public static final String EVENT_RESET_SESSION = "resetSession";

    /**
     * Process the HTTP Post request
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	boolean passwordsMatch = false;

	// printRequestParameters(request);

	String event = request.getParameter("event");
	LogUtil.debug(this.getClass(), "_________________event=" + event);
	if (EVENT_RESET_SESSION.equals(event)) {
	    invalidateUserSession(request, true);
	    forwardRequest(request, response, LoginUIConstants.PAGE_TitleFrame);
	    return;
	} else {
	    if (EVENT_LOGIN.equals(event)) {
		// See if we can do a default login first.
		// The default user ID should not be set in the system
		// parameters table.
		// But it may be set in the "master.properties" file which can
		// override
		// system parameter values for a local instance of JBoss i.e. a
		// developers installation.
		String defaultUserID = null;
		try {
		    LogUtil.log(this.getClass(), "defaultUserID");
		    // in container?
		    defaultUserID = FileUtil.getProperty("master", SystemParameterVO.DEFAULT_USER_ID);

		    LogUtil.log(this.getClass(), "defaultUserID=" + defaultUserID);
		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}

		if (defaultUserID != null) {
		    User user = null;
		    try {
			user = getUser(defaultUserID);
		    } catch (Exception ex) {
			LogUtil.info(this.getClass(), "Error getting user '" + defaultUserID + "'. " + ex);
			forwardRequest(request, response, LoginUIConstants.PAGE_Login);
		    }

		    LogUtil.debug(this.getClass(), "login");
		    HttpSession session = request.getSession();
		    String sessionId = session.getId();
		    DateTime createTime = new DateTime(session.getCreationTime());
		    String ipAddress = request.getRemoteAddr();
		    String systemName = null;
		    us = new UserSession(user, sessionId, ipAddress, systemName, createTime);
		    // set the session attribute
		    session.setAttribute(LoginUIConstants.USER_SESSION, us);
		    forwardRequest(request, response, LoginUIConstants.PAGE_Index);
		} else {
		    LogUtil.debug(this.getClass(), "in here");
		    forwardRequest(request, response, LoginUIConstants.PAGE_Login);
		    return;
		}
	    } else {
		LogUtil.debug(this.getClass(), "other");
		HttpSession session = request.getSession();

		us = UserSession.getUserSession(session);

		// get the parameters
		String logout = request.getParameter("logout");
		String userId = request.getParameter("user");
		String pass = request.getParameter("pass");
		String systemName = request.getParameter("systemName");

		// LogUtil.log(this.getClass(),"logout: ["+logout+"]");
		// LogUtil.log(this.getClass(),"user: "+user);
		// LogUtil.log(this.getClass(),"pass: "+pass);
		// LogUtil.log(this.getClass(),"systemName: "+systemName);

		// if any logout value is sent
		if (logout != null && logout.length() > 0) {
		    LogUtil.debug(this.getClass(), "logout");
		    // LogUtil.log(this.getClass(),"in logout");

		    /*
		     * delete the cookie. the cookie is deleted when the browser
		     * is shut down
		     */
		    // SecurityHelper.deleteCookie(request, "session");

		    // log the user out
		    session.invalidate();

		    request.setAttribute("user", userId);
		    request.setAttribute(LoginUIConstants.MESSAGE, userId + " logged out " + (new DateTime()).formatLongDate() + ".");
		    forwardRequest(request, response, LoginUIConstants.PAGE_Logout);
		    return;
		}

		// validate user and password
		if (!validate(userId)) {
		    request.setAttribute(LoginUIConstants.MESSAGE, "Error: user id may not be empty");
		    forwardRequest(request, response, LoginUIConstants.PAGE_Login);
		    return;
		}

		try {
		    // encrypt password that is sent from the form if is not
		    // null
		    if (validate(pass)) {
			pass = SecurityHelper.encryptPassword(pass);
		    }
		} catch (Exception e) {
		    throw new ServletException("Error: unable to encrypt password. " + e.getMessage());
		}
		LogUtil.debug(this.getClass(), "b4 validation [" + userId + "]");
		// get the vo if we have a valid user
		User user = null;
		try {
		    user = this.getUser(userId);
		} catch (Exception ex) {
		    LogUtil.warn(this.getClass(), "Error getting user '" + userId + "'. " + ex);
		    // do nothing as we will validate futher down
		}

		/*
		 * match up passwords
		 */
		if (user != null) {
		    String thisPassword = user.getPassword().trim();

		    LogUtil.debug(this.getClass(), "userVO is not null");
		    LogUtil.debug(this.getClass(), "db pass [" + thisPassword + "]");
		    LogUtil.debug(this.getClass(), "other pass [" + pass + "]");

		    if (thisPassword == null) {
			thisPassword = "";
		    }

		    if (thisPassword.equalsIgnoreCase(pass)) {
			passwordsMatch = true;
		    }
		}

		// LogUtil.log(this.getClass(),"before us check");

		// if user session does not exist
		if (us == null) {
		    // LogUtil.log(this.getClass(),"no user session");
		    // look for user
		    if (user == null) {
			// user does not exist
			request.setAttribute(LoginUIConstants.MESSAGE, "Error: user \"" + userId + "\" does not exist.");
			forwardRequest(request, response, LoginUIConstants.PAGE_Login);
			return;
		    } else {

			if (User.STATUS_INACTIVE.equalsIgnoreCase(user.getStatus())) {
			    // missing one or both login credentials
			    request.setAttribute(LoginUIConstants.MESSAGE, "Error: user \"" + userId + "\" is not active.");
			    forwardRequest(request, response, LoginUIConstants.PAGE_Login);
			    return;
			}

			// new session
			if (passwordsMatch) {
			    // identified a valid user
			    String sessionId = session.getId();
			    DateTime createTime = new DateTime(session.getCreationTime());
			    String ipAddress = request.getRemoteAddr();
			    us = new UserSession(user, sessionId, ipAddress, systemName, createTime);
			    // set the session attribute
			    session.setAttribute(LoginUIConstants.USER_SESSION, us);

			    // set the cookie on the browser
			    // SecurityHelper.setCookie(response, "session",
			    // sessionId);

			    forwardRequest(request, response, LoginUIConstants.PAGE_Index);
			    return;
			} else if (!passwordsMatch) {
			    // missing one or both login credentials
			    request.setAttribute(LoginUIConstants.MESSAGE, "Error: password is incorrect for user \"" + userId + "\".");
			    forwardRequest(request, response, LoginUIConstants.PAGE_Login);
			    return;
			} else {
			    // password is incorrect
			    request.setAttribute(LoginUIConstants.MESSAGE, "Error: username or password is incorrect.");
			    forwardRequest(request, response, LoginUIConstants.PAGE_Login);
			    return;
			}
		    }
		} else {
		    // already logged in
		    // LogUtil.log(this.getClass(),"already logged in");

		    // calculate how long this session has existed
		    // int totalMins =
		    // DateUtil.getTimeDiff(session.getCreationTime());

		    // notice the use of Integer and not int
		    // session.setAttribute("totalMins", new
		    // Integer(totalMins));

		    // request.setAttribute(LoginUIConstants.MESSAGE,
		    // "Error: already logged in. Log out first before logging in.");

		    forwardRequest(request, response, LoginUIConstants.PAGE_Index);
		    return;
		}
	    }
	}
    }

    /**
     * Unable to extend Application Servlet for this functionality as this would
     * cause circular referencing.
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @param pageName
     *            Description of the Parameter
     * @exception IOException
     *                Description of the Exception
     * @exception ServletException
     *                Description of the Exception
     */
    private void forwardRequest(HttpServletRequest request, HttpServletResponse response, String pageName) throws IOException, ServletException {
	RequestDispatcher rd = request.getRequestDispatcher(pageName);
	rd.forward(request, response);
    }

    /**
     * check if string is not empty
     * 
     * @param str
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    private boolean validate(String str) {
	return (str != null && str.length() > 0);
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param restrictedInvalidation
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    private void invalidateUserSession(HttpServletRequest request, boolean restrictedInvalidation) throws RemoteException {
	if (restrictedInvalidation) {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String allowUserSessionInvalidation = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_LOGIN, SystemParameterVO.ALLOW_USER_SESSION_INVALIDATION);
	    if ("Y".equals(allowUserSessionInvalidation)) {
		request.getSession().invalidate();
		// LogUtil.log(this.getClass(),"User session invalidated on request.");
	    }
	} else {
	    request.getSession().invalidate();
	}
    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

    /**
     * Gets the user attribute of the LoginUIC object
     * 
     * @param userId
     *            Description of the Parameter
     * @return The user value object
     * @exception ServletException
     *                Description of the Exception
     */
    private User getUser(String userId) throws ServletException {
	UserMgrLocal uMgrLocal = null;
	User user = null;
	try {
	    uMgrLocal = UserEJBHelper.getUserMgrLocal();
	    user = uMgrLocal.getUser(userId);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException("Unable to find EJB Home.", ex);
	}
	return user;
    }

}
