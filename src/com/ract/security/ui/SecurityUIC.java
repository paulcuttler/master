package com.ract.security.ui;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.common.ApplicationServlet;
import com.ract.common.ExceptionHelper;
import com.ract.common.ValueObject;
import com.ract.common.admin.AdminCodeDescription;
import com.ract.common.ui.CommonUIConstants;
import com.ract.security.Role;
import com.ract.security.SecurityEJBHelper;
import com.ract.security.SecurityMgrLocal;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgrLocal;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;
import com.ract.util.StringUtil;

/**
 * The security controller servlet.
 * 
 * @author hollidayj
 * @created 7 August 2002
 */

public class SecurityUIC extends ApplicationServlet {
    private final static String CONTENT_TYPE = "text/html";

    /**
     * Initialize global variables
     * 
     * @exception ServletException
     *                Description of the Exception
     */
    public void init() throws ServletException {
    }

    // /**
    // * Process the HTTP Get request
    // *
    // *@param request Description of the Parameter
    // *@param response Description of the Parameter
    // *@exception ServletException Description of the Exception
    // *@exception IOException Description of the Exception
    // */
    // public void doGet(HttpServletRequest request, HttpServletResponse
    // response)
    // throws ServletException, IOException
    // {
    // doPost(request, response);
    // }
    //
    // /**
    // * Process the HTTP Post request
    // *
    // *@param request Description of the Parameter
    // *@param response Description of the Parameter
    // *@exception ServletException Description of the Exception
    // *@exception IOException Description of the Exception
    // */
    // public void doPost(HttpServletRequest request, HttpServletResponse
    // response)
    // throws ServletException, IOException
    // {
    // String event = request.getParameter("event");
    // if(event == null)
    // {
    // event = "";
    // }
    //
    // this.handleEvent(event, request, response);
    // }

    /**
     * view logged in users
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_viewUserSessions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	Collection c = null;
	try {
	    SecurityMgrLocal sMgrLocal = SecurityEJBHelper.getSecurityMgrLocal();
	    c = sMgrLocal.getLoggedInUsers();
	} catch (Exception e) {
	    throw new ServletException("Unable to find any users logged into the system." + e.toString());
	}
	request.setAttribute("sessions", c);
	this.forwardRequest(request, response, LoginUIConstants.PAGE_UserSessions);
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_viewChangePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.forwardRequest(request, response, LoginUIConstants.PAGE_ChangePassword);
    }

    public void handleEvent_viewChangeMyPassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	this.forwardRequest(request, response, LoginUIConstants.PAGE_ChangeMyPassword);
    }

    public void handleEvent_viewRoleList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.forwardRequest(request, response, LoginUIConstants.PAGE_ViewRoleList);
    }

    private User getUser(String userID) {
	User user = null;
	try {
	    UserMgrLocal userMgrLocal = UserEJBHelper.getUserMgrLocal();
	    user = userMgrLocal.getUser(userID);
	} catch (RemoteException re) {
	    log("Unable to retrive user." + re.toString());
	}
	return user;
    }

    public void handleEvent_changeMyPassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// get form input
	String userID = request.getParameter("userID");
	String newPassword = request.getParameter("newPassword");
	String verifyPassword = request.getParameter("verifyPassword");

	try {
	    SecurityMgrLocal securityMgrLocal = SecurityEJBHelper.getSecurityMgrLocal();
	    securityMgrLocal.changePassword(userID, newPassword, verifyPassword);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	User user = this.getUser(userID);

	request.setAttribute("user", user);

	request.setAttribute(LoginUIConstants.MESSAGE, "Success: password has been changed for " + user.getUsername() + ".");

	this.forwardRequest(request, response, LoginUIConstants.PAGE_ChangeMyPassword);
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_changePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// get form input
	String userID = request.getParameter("userID");
	String newPassword = request.getParameter("newPassword");
	String verifyPassword = request.getParameter("verifyPassword");

	try {
	    SecurityMgrLocal securityMgrLocal = SecurityEJBHelper.getSecurityMgrLocal();
	    securityMgrLocal.changePassword(userID, newPassword, verifyPassword);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	User user = this.getUser(userID);

	request.setAttribute("user", user);

	request.setAttribute(LoginUIConstants.MESSAGE, "Success: password has been changed for " + user.getUsername() + ".");

	this.forwardRequest(request, response, LoginUIConstants.PAGE_ChangePassword);

    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_viewUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	this.outputSessionAttributes(request);
	String userId = request.getParameter("userId");

	System.out.println("userId: " + userId);

	User user = null;

	try {
	    UserMgrLocal userMgrLocal = UserEJBHelper.getUserMgrLocal();
	    user = userMgrLocal.getUser(userId);
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}

	request.setAttribute("user", user);

	this.forwardRequest(request, response, LoginUIConstants.PAGE_ViewUser);
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_viewUserList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	Collection users = null;

	try {
	    UserMgrLocal userMgrLocal = UserEJBHelper.getUserMgrLocal();
	    users = userMgrLocal.getUsers();
	} catch (Exception e) {
	    throw new ServletException(e.toString());
	}
	request.setAttribute("users", users);
	this.forwardRequest(request, response, LoginUIConstants.PAGE_ViewUserList);
    }

    public void handleEvent_createUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	User user = new User();
	request.setAttribute("Refresh", "No");
	gotoUserPage(request, response, "", user, "");
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_changeUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
	String userCode = request.getParameter("userID");

	/** @todo replace */

	UserMgrLocal userMgrLocal = UserEJBHelper.getUserMgrLocal();
	User user = userMgrLocal.getUser(userCode);

	Collection s = user.getRoles();

	request.setAttribute("Refresh", "No");
	gotoUserPage(request, response, userCode, user, "");

	// this.forwardRequest(request, response,
	// LoginUIConstants.PAGE_ChangeUser);
    }

    /**
     * Description of the Method
     * 
     * @param request
     *            Description of the Parameter
     * @param response
     *            Description of the Parameter
     * @exception ServletException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    public void handleEvent_maintainUser_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {

	User user = this.getUserFromRequest(request);
	String userCode = user.getUserID();
	LogUtil.debug(this.getClass(), "user=" + user.getRoles());
	StringBuffer displayText = new StringBuffer();
	String origCode = (String) request.getSession().getAttribute("origCode");

	LogUtil.debug(this.getClass(), "origCode=" + origCode);

	UserMgrLocal userMgrLocal = UserEJBHelper.getUserMgrLocal();

	if (StringUtil.isNull(userCode)) {
	    displayText.append("Code is SPACES. No Action taken.");
	} else {

	    if (StringUtil.isBlank(origCode)) {
		try {
		    userMgrLocal.saveUser(user);
		    displayText.append("Created Successfully");
		    request.setAttribute("Refresh", "Yes");
		    this.addToAdminList(request, user.getUserID(), user.getUsername());
		} catch (Exception e) {
		    LogUtil.fatal(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    userMgrLocal.updateUser(user);
		    displayText.append("Saved Successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    LogUtil.fatal(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
		    displayText.append(e.getMessage());
		}
	    }
	}

	gotoUserPage(request, response, userCode, user, displayText.toString());
    }

    public void handleEvent_maintainUser_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	User userVO = this.getUserFromRequest(request);
	StringBuffer displayText = new StringBuffer();
	String origCode = (String) request.getSession().getAttribute("origCode");
	displayText.append("Cannot delete. Set to Inactive");
	request.setAttribute("Refresh", "No");
	gotoUserPage(request, response, origCode, userVO, displayText.toString());
    }

    public void handleEvent_maintainUserRoles_btnAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
	String roleCode = request.getParameter("listCode");
	User user = this.getUserFromRequest(request);
	String origCode = (String) request.getSession().getAttribute("origCode");

	Collection userRoles = user.getRoles();
	if (userRoles == null) {
	    userRoles = new HashSet();
	}

	userRoles.add(SecurityEJBHelper.getSecurityMgrLocal().getRole(roleCode));
	user.setRoles(userRoles);
	request.setAttribute("Refresh", "No");
	gotoUserPage(request, response, origCode, user, "");
    }

    public void handleEvent_maintainUserRoles_btnRemove(HttpServletRequest request, HttpServletResponse response) throws Exception {
	String roleCode = request.getParameter("listCode");
	User user = this.getUserFromRequest(request);
	String origCode = (String) request.getSession().getAttribute("origCode");

	Collection userRoles = user.getRoles();

	Collection newUR = new HashSet();

	if (userRoles != null) {
	    Iterator ix = userRoles.iterator();
	    while (ix.hasNext()) {
		Role x1 = (Role) ix.next();
		if (!(x1.getRoleID().equalsIgnoreCase(roleCode))) {
		    newUR.add(x1);
		}

	    }
	}
	user.setRoles(newUR);
	request.setAttribute("Refresh", "No");
	gotoUserPage(request, response, origCode, user, "");
    }

    public void handleEvent_maintainUser_checkUserId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	SortedSet thisList = (SortedSet) request.getSession().getAttribute(CommonUIConstants.MAINTAIN_CODELIST);
	User user = this.getUserFromRequest(request);
	String origCode = (String) request.getSession().getAttribute("origCode");

	StringBuffer displayText = new StringBuffer();
	StringBuffer alphabet = new StringBuffer();
	String userId = (String) request.getParameter("listCode");
	String fcUser = null;
	String lcUser = null;
	try {
	    fcUser = userId.substring(0, 1);
	    lcUser = userId.substring(userId.length() - 1);

	    boolean idUsed = false;

	    if (!(thisList == null)) {
		Iterator it = thisList.iterator();
		while (it.hasNext()) {
		    AdminCodeDescription cd = (AdminCodeDescription) it.next();
		    String usedId = cd.getCode();
		    String fcUsed = null;
		    String lcUsed = null;
		    try {
			fcUsed = usedId.substring(0, 1);
			lcUsed = usedId.substring(usedId.length() - 1);
		    } catch (Exception e) {
		    }
		    if (usedId.equalsIgnoreCase(userId)) {
			idUsed = true;
			displayText.append("ID in use. ");
		    }
		    if (fcUser.equalsIgnoreCase(fcUsed) & lcUser.equalsIgnoreCase(lcUsed)) {
			String mcUsed = usedId.substring(1, 2);
			alphabet.append(usedId + " ");
		    }
		}
		if (idUsed) {
		    displayText.append(" Other ID's USED are " + alphabet.toString());
		    userId = "";
		}

	    }
	} catch (Exception e) {
	    // displayText.append(e.getMessage());
	}

	request.setAttribute("Refresh", "No");
	user.setUserID(userId);
	gotoUserPage(request, response, origCode, user, displayText.toString());
    }

    public void handleEvent_selectMaintainUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	/** @todo replace */

	// UserHome userHome = UserEJBHelper.getUserHome();
	UserMgrLocal userMgrLocal = UserEJBHelper.getUserMgrLocal();

	Collection userList = null;
	SortedSet s = new TreeSet();
	int maxLen = 0;

	try {
	    userList = userMgrLocal.getUsers(); // userHome.findAll();
	} catch (Exception f) {
	    throw new RemoteException("Cant find Users. " + f.getMessage());
	}

	if (userList != null) {
	    Iterator it = userList.iterator();
	    while (it.hasNext()) {
		User user = (User) it.next();

		String u = user.getUserID().trim();
		String n = (StringUtil.isNull(user.getUsername())) ? u : user.getUsername().trim();
		if (maxLen < n.length()) {
		    maxLen = n.length();
		}

		if (!(n == null)) {
		    s.add(new AdminCodeDescription(u, n, n));
		}
	    }
	}

	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
	request.getSession().setAttribute("pageTitle", "User");
	request.getSession().setAttribute("buttonMessage", "Add a new User.");
	request.getSession().setAttribute("createEvent", "createUser");
	request.getSession().setAttribute("selectEvent", "changeUser");
	request.getSession().setAttribute("codeName", "userID");
	request.getSession().setAttribute("UICPage", LoginUIConstants.PAGE_SecurityUIC);
	request.getSession().setAttribute("origCode", "");
	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
    }

    /**
     * usage /SecurityUIC?event=resetPassword&userID=xxx
     */
    public void handleEvent_resetPassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String userID = request.getParameter("userID");

	SecurityMgrLocal securityMgrLocal = SecurityEJBHelper.getSecurityMgrLocal();
	securityMgrLocal.resetPassword(userID);
    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

    private User getUserFromRequest(HttpServletRequest request) {
	User user = (User) request.getSession().getAttribute(LoginUIConstants.KEY_USER);
	user.setUserID(request.getParameter("userID"));
	user.setPassword(request.getParameter("password"));
	user.setUsername(request.getParameter("userName"));
	user.setPrinterGroup(request.getParameter("printGroup"));
	user.setSalesBranch(request.getParameter("salesBranch"));
	user.setBranchNumber(ServletUtil.getIntegerParam("branch", request).intValue());
	user.setEmailAddress(request.getParameter("email"));
	user.setStatus(request.getParameter("selectStatus"));
	return user;
    }

    private void gotoPage(HttpServletRequest request, HttpServletResponse response, String oCode, Object object, String name, String statMsg, String page) throws ServletException {
	request.getSession().setAttribute("origCode", oCode);
	request.getSession().removeAttribute(name);
	request.getSession().setAttribute(name, object);
	request.setAttribute("statusMessage", statMsg);
	forwardRequest(request, response, page);
    }

    private void gotoUserPage(HttpServletRequest request, HttpServletResponse response, String oCode, ValueObject VO, String statMsg) throws ServletException {
	gotoPage(request, response, oCode, VO, LoginUIConstants.KEY_USER, statMsg, LoginUIConstants.PAGE_ChangeUser);
    }

    private void addToAdminList(HttpServletRequest request, String code, String desc) {
	// Add this to the list of Codes
	SortedSet s = (SortedSet) request.getSession().getAttribute(CommonUIConstants.MAINTAIN_CODELIST);
	s.add(new AdminCodeDescription(code, desc, desc));
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
    }

}
