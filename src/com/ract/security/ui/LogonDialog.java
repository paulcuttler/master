package com.ract.security.ui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.SystemColor;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import com.ract.security.SecurityEJBHelper;
import com.ract.security.SecurityMgr;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;

/**
 * <p>
 * Title: Logon dialog
 * </p>
 * <p>
 * Description: Provides an encapsulated user logon facility Users provide a
 * username and password which are then authenticated against UNIX password
 * files.
 * </p>
 * 
 * <p>
 * Typical usage: LogonDialog logon = new LogonDialog();
 * logon.setTitle("RACT EFT Logon"); logon.show(); String role = "receipt";
 * String status = logon.validateUser(role);
 * 
 * If validateUser returns a non empty string, the user has not been
 * authenticated. The validateUser() method (no parameters) checks username and
 * password only. The validateUser(role) checks username, password and privilege
 * (as recorded in Receptor tables).
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @dgk
 * @version 1.0 09/01/2006
 */

public class LogonDialog extends JDialog {
    JPanel panel1 = new JPanel();
    BorderLayout borderLayout1 = new BorderLayout();
    XYLayout xYLayout1 = new XYLayout();
    JPanel jPanel1 = new JPanel();
    XYLayout xYLayout2 = new XYLayout();
    JLabel jLabel1 = new JLabel();
    JLabel jLabel2 = new JLabel();
    JLabel jLabel3 = new JLabel();
    JTextField userName = new JTextField();
    JPasswordField password = new JPasswordField();
    String vUserName = null;
    String vPassword = null;
    SecurityMgr secMgr = null;
    UserMgr userMgr = null;
    int attempts = 0;
    JLabel jMessage = new JLabel();
    String jBossHost = null;
    int jBossPort = 0;

    public LogonDialog(Frame frame, String title, boolean modal) {
	super(frame, title, modal);
	try {
	    jbInit();
	    pack();
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    public LogonDialog() {
	this(null, "", false);
    }

    public void setLogonTitle(String title) {
	this.setTitle(title);
    }

    private void jbInit() throws Exception {
	this.getContentPane().setBackground(SystemColor.controlLtHighlight);
	this.setModal(true);
	this.setLocation(350, 250);
	panel1.setLayout(borderLayout1);
	this.getContentPane().setLayout(xYLayout1);
	jPanel1.setLayout(xYLayout2);
	xYLayout1.setWidth(320);
	xYLayout1.setHeight(295);
	jLabel1.setFont(new java.awt.Font("Dialog", 1, 15));
	jLabel1.setRequestFocusEnabled(true);
	try {
	    Icon icon = new ImageIcon(ClassLoader.getSystemResource("images/ract_logo.gif"));
	    jLabel1 = new JLabel(icon);
	} catch (Exception e) {
	    // ignore if image does not load
	}

	jLabel2.setText("User Name");
	jLabel3.setText("Password");
	userName.setText("");
	userName.addKeyListener(new LogonDialog_userName_keyAdapter(this));
	password.setText("");
	password.addKeyListener(new LogonDialog_password_keyAdapter(this));
	getContentPane().add(panel1, new XYConstraints(0, 0, -1, -1));
	this.getContentPane().add(jPanel1, new XYConstraints(156, 130, -1, -1));
	this.getContentPane().add(userName, new XYConstraints(114, 210, 125, -1));
	this.getContentPane().add(password, new XYConstraints(114, 234, 125, -1));
	this.getContentPane().add(jLabel2, new XYConstraints(58, 212, -1, -1));
	this.getContentPane().add(jLabel3, new XYConstraints(58, 238, -1, -1));
	this.getContentPane().add(jLabel1, new XYConstraints(50, 24, 212, 167));
	this.getContentPane().add(jMessage, new XYConstraints(54, 264, 210, 22));
    }

    public String getUsername() {
	return this.vUserName;
    }

    public String getPassword() {
	return this.vPassword;
    }

    void userName_keyPressed(KeyEvent e) {
	if (e.getKeyText(e.getKeyCode()).equals("Enter")) {
	    password.requestFocus();
	} else if (e.getKeyText(e.getKeyCode()).equals("Escape")) {
	    this.vUserName = null;
	    this.vPassword = null;
	    this.hide();
	} else
	    jMessage.setText("");

    }

    void password_keyPressed(KeyEvent e) {
	if (e.getKeyText(e.getKeyCode()).equals("Enter")) {
	    this.vUserName = this.userName.getText();
	    this.vPassword = this.password.getText();
	    this.attempts++;
	    if (this.validateUser()) {
		this.hide();
	    } else if (attempts >= 3) {
		this.vUserName = null;
		this.vPassword = null;
		this.hide();
	    } else {
		jMessage.setText("User name/password incorrect");
		vUserName = null;
		vPassword = null;
		this.userName.requestFocus();
	    }
	} else if (e.getKeyText(e.getKeyCode()).equals("Escape")) {
	    this.vUserName = null;
	    this.vPassword = null;
	    this.hide();
	} else
	    jMessage.setText("");
    }

    private SecurityMgr getSecMgr() {
	if (secMgr == null) {
	    try {
		// RACTPropertiesProvider.setContextURL(this.jBossHost,
		// this.jBossPort);
		secMgr = SecurityEJBHelper.getSecurityMgr();
	    } catch (Exception ex) {
	    }
	}
	return secMgr;
    }

    public boolean validateUser() {
	boolean isValid = false;
	if (this.vUserName == null || this.vUserName.trim().equals("")) {
	    // no name entered.
	    System.out.println("no username entered");
	} else {
	    try {
		isValid = getSecMgr().validateUnixUser(this.vUserName, this.vPassword);
	    } catch (RemoteException ex) {
		System.out.println("unable to validate unix user");
		ex.printStackTrace();
		return false;
	    }
	}
	return isValid;
    }

    public boolean validatePrivilege(String privilege) {
	boolean isValid = false;
	try {
	    User user = this.getUserMgr().getUser(this.vUserName);
	    isValid = user.isPrivilegedUser(privilege);
	    // isValid = secMgr.validateRole(this.vUserName, role);
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
	return isValid;
    }

    public String validateUser(String privilege) {
	String returnString = "";
	boolean isValid = false;
	if (this.vUserName == null || this.vUserName.trim().equals("")) {
	    // do nothing. No name has been entered.
	    returnString = "User name not provided";
	} else {
	    this.getSecMgr();
	    if (secMgr == null) {
		return "Unable to connect to server";
	    }
	    try {
		isValid = secMgr.validateUnixUser(this.vUserName, this.vPassword);
		if (!isValid) {
		    returnString = "User name or password incorrect";
		} else {
		    isValid = false;

		    User user = this.getUserMgr().getUser(this.vUserName);

		    isValid = user.isPrivilegedUser(privilege);
		    if (!isValid) {
			returnString = "User does not have permission for this function";
		    }
		}
	    } catch (RemoteException ex) {
		return "" + ex;
	    }
	}
	return returnString;
    }

    public void setHostURL(String host, int port) {
	this.jBossHost = host;
	this.jBossPort = port;
    }

    private UserMgr getUserMgr() {
	if (userMgr == null) {
	    try {
		userMgr = UserEJBHelper.getUserMgr();
	    } catch (Exception ex) {
	    }
	}
	return userMgr;
    }

}

class LogonDialog_userName_keyAdapter extends java.awt.event.KeyAdapter {
    LogonDialog adaptee;

    LogonDialog_userName_keyAdapter(LogonDialog adaptee) {
	this.adaptee = adaptee;
    }

    public void keyPressed(KeyEvent e) {
	adaptee.userName_keyPressed(e);
    }
}

class LogonDialog_password_keyAdapter extends java.awt.event.KeyAdapter {
    LogonDialog adaptee;

    LogonDialog_password_keyAdapter(LogonDialog adaptee) {
	this.adaptee = adaptee;
    }

    public void keyPressed(KeyEvent e) {
	adaptee.password_keyPressed(e);
    }
}
