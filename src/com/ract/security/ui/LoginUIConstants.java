package com.ract.security.ui;

import com.ract.common.CommonConstants;

/**
 * Constants for the Login user interface
 * 
 * @author hollidayj
 * @created 2 August 2002
 */
public class LoginUIConstants {
    public final static String PAGE_LoginUIC = "/LoginUIC";

    public final static String PAGE_SecurityUIC = "/SecurityUIC";

    public final static String DIR_SECURITY = CommonConstants.DIR_SECURITY;

    public final static String PAGE_TitleFrame = CommonConstants.DIR_MENU + "/TitleFrame.jsp";

    public static final String PAGE_UserSessions = DIR_SECURITY + "/ViewUserSessions.jsp";

    public final static String PAGE_Index = CommonConstants.PAGE_Index;

    public final static String PAGE_Login = DIR_SECURITY + "/Login.jsp";

    public static final String PAGE_Logout = DIR_SECURITY + "/Logout.jsp";

    public static final String PAGE_ChangePassword = DIR_SECURITY + "/ChangePassword.jsp";

    public static final String PAGE_ChangeMyPassword = DIR_SECURITY + "/ChangeMyPassword.jsp";

    // public static final String PAGE_NoAccess = DIR_SECURITY +
    // "/NoAccess.jsp";

    public static final String PAGE_ViewRoleList = DIR_SECURITY + "/ViewRoles.jsp";

    public static final String PAGE_ViewUserList = DIR_SECURITY + "/ViewUserList.jsp";

    public static final String PAGE_ViewUser = DIR_SECURITY + "/ViewUser.jsp";

    public static final String PAGE_ChangeUser = DIR_SECURITY + "/ChangeUser.jsp";

    public static final String PAGE_CreateUser = DIR_SECURITY + "/ChangeUser.jsp";

    // user session object handle
    public final static String USER_SESSION = "usersession";

    // cookie handle
    // public final static String

    // message handle
    public final static String MESSAGE = "MESSAGE";

    // SYSTEMs
    public final static String ISCU_SYSTEM = "ISCU";

    public final static String RACT_SYSTEM = "RACT";

    public final static String AURORA_SYSTEM = "AURORA";

    // broker?
    public static final String KEY_USER = "user";
}
