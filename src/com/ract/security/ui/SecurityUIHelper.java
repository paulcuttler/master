package com.ract.security.ui;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.security.Role;
import com.ract.security.SecurityEJBHelper;
import com.ract.security.SecurityMgr;

/**
 * Security helper classes
 * 
 * @author John Holliday
 * @version 1.0
 */

public class SecurityUIHelper {

    public static String getPrinterGroupOptions(String code) throws ServletException {
	String options = "";
	Collection printerGroupList = null;
	try {
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    printerGroupList = cMgr.getPrinterGroups();
	} catch (RemoteException ex) {
	    throw new ServletException(ex);
	}
	options = getOptions(code, printerGroupList);
	return options;
    }

    private static String getOptions(String code, Collection codeList) throws ServletException {
	StringBuffer options = new StringBuffer();
	String currentOption = "";
	if (codeList != null) {
	    Iterator it = codeList.iterator();
	    if (it != null) {
		while (it.hasNext()) {
		    currentOption = (String) it.next();
		    options.append("<option value=\"" + currentOption + "\" ");
		    if (currentOption.equalsIgnoreCase(code)) {
			options.append("selected");
		    }
		    options.append(">" + currentOption + "</option>");
		}
	    }
	}
	return options.toString();
    }

    public static String getSalesBranchOptions(String code) throws ServletException {
	String options = "";
	Collection salesBranchCodeList = null;
	try {
	    CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	    salesBranchCodeList = cMgr.getSalesBranchCodes();
	} catch (RemoteException re) {
	    throw new ServletException(re);
	}
	options = getOptions(code, salesBranchCodeList);
	return options;
    }

    public static String getRoleOptions() throws ServletException {
	StringBuffer options = new StringBuffer();
	Role currentRole = null;
	Collection roleList = null;
	try {
	    SecurityMgr sMgr = SecurityEJBHelper.getSecurityMgr();
	    roleList = sMgr.getAllRoles();
	    if (roleList != null) {
		Iterator it = roleList.iterator();
		while (it.hasNext()) {
		    currentRole = (Role) it.next();
		    options.append("<option value=\"" + currentRole.getRoleID() + "\" ");
		    options.append(">" + currentRole.getName() + "</option>");
		}
	    }
	} catch (RemoteException re) {
	    throw new ServletException(re);
	}
	return options.toString();
    }

    public static String getPrivilegeOptions(String code) throws ServletException {
	String options = "";
	Collection privilegeList = null;
	try {
	    SecurityMgr sMgr = SecurityEJBHelper.getSecurityMgr();
	    privilegeList = sMgr.getAllPrivileges();
	} catch (RemoteException re) {
	    throw new ServletException(re);
	}
	options = getOptions(code, privilegeList);
	return options;
    }

}