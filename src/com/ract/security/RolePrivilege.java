package com.ract.security;

/**
 * @hibernate.class table="x_role_priv" lazy="false"
 * @author hollidayj
 */
public class RolePrivilege {

    private RolePrivilegePK rolePrivilegePK;

    @Override
    public String toString() {
	return "RolePrivilege [rolePrivilegePK=" + rolePrivilegePK + "]";
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public RolePrivilegePK getRolePrivilegePK() {
	return rolePrivilegePK;
    }

    public void setRolePrivilegePK(RolePrivilegePK rolePrivilegePK) {
	this.rolePrivilegePK = rolePrivilegePK;
    }

}
