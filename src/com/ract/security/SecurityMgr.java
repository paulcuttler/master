package com.ract.security;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.Remote;

import com.ract.util.DateTime;

/**
 * Remote interface to security manager bean.
 * 
 * @author John Holliday
 * @version 1.0
 */
@Remote
public interface SecurityMgr {
    public Collection getUsersByPrivilege(String privId);

    public Collection getUsersByPrivilegeAndBranch(String privId, String branchId);

    public void logIn(String sessionID, String userID, DateTime sessionStart, String ipAddress, String systemName) throws RemoteException;

    public void logOut(String sessionID) throws RemoteException;

    public Collection getRolePrivileges(String roleID) throws RemoteException;

    public void resetPassword(String userID) throws RemoteException;

    public void resetAllPasswords() throws RemoteException;

    public void changePassword(String userId, String newPassword, String verifyPassword) throws RemoteException;

    public Collection getLoggedInUsers() throws RemoteException;

    public Collection getUserRoles(String userId) throws RemoteException;

    public boolean isLoggedIn(String sessionId) throws RemoteException;

    public Collection getAllPrivileges() throws RemoteException;

    public Collection getUserPrivileges(String userId) throws RemoteException;

    public boolean isPrivilegedUser(String userId, String priv) throws RemoteException;

    public Collection getAllRoles() throws RemoteException;

    public Collection getRolesUserAllowedToAdd(Collection roles) throws RemoteException;

    public Role getRole(String roleId) throws RemoteException;

    // public boolean validateUser(String userId, String password) throws
    // RemoteException;
    // public boolean validateRole(String userId, String role) throws
    // RemoteException;
    public boolean validateUnixUser(String userId, String password) throws RemoteException;

    public ArrayList getUserMenuMetaData(String userId, String menuName) throws RemoteException;
}
