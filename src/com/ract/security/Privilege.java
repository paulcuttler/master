package com.ract.security;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Privilege class
 * 
 * @hibernate.class table="x_priv" lazy="false"
 * 
 * @author John Holliday created 20 February 2003
 * @version 1.0
 */

public class Privilege implements Serializable {

    /**
     * Description of the Field
     */
    public final static String PRIVILEGE_MEM_CHANGE_PROFILE = "MEMCHGPROFILE";

    public final static String PRIVILEGE_MEM_REVERSE_TRANSACTION = "REVERSEMT";

    public final static String PRIVILEGE_APPROVE_CREDIT_DISPOSAL = "approvecr";

    public final static String PRIVILEGE_CREATE_DEBTOR = "MEMCREATEDR";

    public final static String PRIVILEGE_UNBLOCK_NOTE = "UNBLOCKNOTE";

    public final static String PRIVILEGE_REASSIGN_NOTE = "REASSIGNNOTE";

    /**
     * Allows the user to administer the exporting of membership data to the CAD
     * system
     */
    public final static String PRIVILEGE_MEM_CAD_ADMIN = "MEMCADADMIN";

    /**
     * Allows the user to export individual membership data to the CAD system
     */
    public final static String PRIVILEGE_MEM_CAD_EXPORT = "MEMCADEXPORT";

    /**
     * Description of the Field
     */
    public final static String PRIVILEGE_MEM_FEES = "MMF";

    /**
     * Description of the Field
     */
    public final static String PRIVILEGE_MEM_REFERENCE = "MRI";

    /**
     * Description of the Field
     */
    public final static String PRIVILEGE_MEM_USER_ADMIN = "AU";

    public final static String PRIVILEGE_EDIT_HELP = "EDITHELP";

    public final static String PRIVILEGE_CREATE_HELP = "CREATEHELP";

    /**
     * Override a transaction block
     */
    public final static String PRIVILEGE_MEM_OVERRIDE_BLOCK = "MEMOVRBLK";

    /**
     * Allow user to manage credit against a membership
     */
    public final static String PRIVILEGE_MEM_MANAGE_CREDIT = "MEMMANAGECR";

    public final static String PRIVILEGE_MEM_XFER_TO_RECEIPTING = "MEMXFERRCPT";

    public final static String PRIVILEGE_MEM_MOTOR_NEWS = "MNA";

    public final static String PRIVILEGE_MEM_MOTOR_NEWS_EDIT = "MNE";

    public final static String PRIVILEGE_MEM_MOTOR_NEWS_ADMIN = "AMN";

    /**
     * Allow a user to edit a transaction
     */
    public final static String PRIVILEGE_MEM_EDIT_TRANSACTION = "MEMEDITTRAN";

    public final static String PRIVILEGE_EDIT_QUERY = "EDITQUERY";

    public final static String PRIVILEGE_MEM_REMOVE_TRANSACTION = "REMOVEMT";

    public final static String PRIVILEGE_MEM_CANCEL_DD = "CANCELDD";

    public final static String PRIVILEGE_MEM_EDIT = "MEMEDIT";

    public final static String PRIVILEGE_MEM_VIEW = "VM";

    public final static String PRIVILEGE_FIN_VIEW = "VF";

    public final static String PRIVILEGE_TRAVEL_MEMBERSHIP_VIEW = "TVM";

    public final static String PRIVILEGE_PT_REMOVE_PAYABLEITEM = "REMOVEPI";

    public final static String PRIVILEGE_MEM_REMOVE_CARD_REQ = "REMOVECRQ";

    /**
     * The privilege required to perform repair functions on memberships.
     * Controls access to the RepairMembership.jsp page.
     */
    public final static String PRIVILEGE_MEM_REPAIR = "MEMREPAIR";

    public final static String PRIVILEGE_MEM_UNDO = "MEMUNDO";

    /**
     * The privilege required to manage notification failures
     */
    public final static String PRIVILEGE_NOTIFICATION_ADMIN = "NOTIFYADMIN";

    /**
     * The privilege required to manage server sockets
     */
    public final static String PRIVILEGE_SERVER_SOCKET_ADMIN = "SVRSOCKETADMIN";

    public final static String PRIVILEGE_DD_APPROVAL = "DDAPPROVAL";

    public final static String PRIVILEGE_ACC_BILLPAY = "ACCBILLPAY";

    public final static String PRIVILEGE_RS_OPERATIONS = "RSOP";

    public final static String PRIVILEGE_RS_OPERATIONS_UPLOAD_AGENTS = "RSOPUA";

    public final static String PRIVILEGE_ACC_FINANCE = "ACCFIN";

    public final static String PRIVILEGE_ACC_FINANCE_REPORTS = "ACCFINREP";

    public final static String PRIVILEGE_MEM_REPORTS = "MEMRPT";

    public final static String PRIVILEGE_MEM_EXTRACTS = "MEMEXT";

    public final static String PRIVILEGE_MEM_CARD_EXTRACT = "MEMCARDEXT";

    public final static String PRIVILEGE_MEM_RENEWAL_RUN = "MEMRENRUN";

    public final static String PRIVILEGE_RESET_CACHES = "RESCHE";

    public final static String PRIVILEGE_DD_TRANSFER_SCHEDULE = "TFRSCH";

    public final static String PRIVILEGE_MENU_CLIENT = "MNUCLT";

    public final static String PRIVILEGE_MENU_MEMBERSHIP = "MNUMEM";

    public final static String PRIVILEGE_MENU_ACCOUNTS = "MNUACC";

    public final static String PRIVILEGE_MENU_SECURITY = "MNUSEC";

    public final static String PRIVILEGE_MENU_TOOLS = "MNUTLS";

    public final static String PRIVILEGE_MENU_ADMINISTRATION = "MNUADM";

    public final static String PRIVILEGE_CLT_REPAIR = "CLTRPR";

    public final static String PRIVILEGE_CMN_VIEW_PROXY_POOL = "CMNVWPRXY";

    public final static String PRIVILEGE_CMN_VIEW_SYSTEM_PARAMETERS = "CMNVWSP";

    public final static String PRIVILEGE_ADDRESS_UPDATER = "addUpd";

    public final static String PRIVILEGE_MARK_NOTE_COMPLETE = "MARKNOTECOMPLETE";

    /**
     * Description of the Method
     * 
     * @param ois
     *            Description of the Parameter
     * @exception ClassNotFoundException
     *                Description of the Exception
     * @exception IOException
     *                Description of the Exception
     */
    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
	ois.defaultReadObject();
    }

    /**
     * Description of the Method
     * 
     * @param oos
     *            Description of the Parameter
     * @exception IOException
     *                Description of the Exception
     */
    private void writeObject(ObjectOutputStream oos) throws IOException {
	oos.defaultWriteObject();
    }

    /**
     * Sets the privilegeID attribute of the Privilege object
     * 
     * @param privilegeID
     *            The new privilegeID value
     */
    public void setPrivilegeID(String privilegeID) {
	this.privilegeID = privilegeID;
    }

    /**
     * @hibernate.id column="priv_id" generator-class="assigned"
     */
    public String getPrivilegeID() {
	return privilegeID;
    }

    @Override
    public String toString() {
	return "Privilege [description=" + description + ", name=" + name + ", privilegeID=" + privilegeID + "]";
    }

    /**
     * Sets the name attribute of the Privilege object
     * 
     * @param name
     *            The new name value
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="priv_name"
     */
    public String getName() {
	return name;
    }

    /**
     * Sets the description attribute of the Privilege object
     * 
     * @param description
     *            The new description value
     */
    public void setDescription(String description) {
	this.description = description;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="description"
     */
    public String getDescription() {
	return description;
    }

    private String privilegeID;

    private String name;

    private String description;

}
