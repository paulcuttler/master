package com.ract.security;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.user.UserSession;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.StringUtil;

/**
 * Manage security in RACT application
 * 
 * @author John Holliday
 * @version 1.0
 */
@Stateless
@Remote({ SecurityMgr.class })
@Local({ SecurityMgrLocal.class })
public class SecurityMgrBean
// implements SessionBean
{
    @Resource(mappedName = "java:/ClientDS")
    private DataSource clientDataSource;

    @PersistenceContext(unitName = "master")
    Session hsession;

    @EJB
    private UserMgrLocal userMgr;

    /**
     * log out a user's session and delete corresponding record from the session
     * table
     */

    public void logOut(String sessionId) throws RemoteException {

	System.out.println("session id for deletion is [" + sessionId + "]");

	String sql = "DELETE FROM PUB.\"x_session\" WHERE \"session_id\" = ? ";

	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, sessionId);

	    int i = statement.executeUpdate();

	    if (i != 1) {
		throw new RemoteException("Unable to delete session for " + sessionId + ".");
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + sql + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
    }

    /**
     * Is the user logged in?
     */
    public boolean isLoggedIn(String sessionId) throws RemoteException {
	String sql = "SELECT \"user_id\" FROM PUB.\"x_session\" WHERE \"session_id\" = ?";
	Connection connection = null;
	PreparedStatement statement = null;
	boolean loggedIn = false;
	try {
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, sessionId);
	    ResultSet rs = statement.executeQuery();
	    if (rs.next()) {
		loggedIn = true;
	    }
	} catch (SQLException e) {

	    throw new RemoteException("Error executing SQL " + sql + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return loggedIn;

    }

    /**
     * get all privileges in the system
     */
    public Collection getAllPrivileges() throws RemoteException {
	Criteria crit = hsession.createCriteria(Privilege.class);
	return crit.list();
    }

    /**
     * get all the roles the logged in user is allowed to allocate
     */
    public Collection getRolesUserAllowedToAdd(Collection roles) throws RemoteException {

	String sql = getAllowedRoles(roles);

	return getTheRoles(sql);
    }

    public Collection getRolePrivileges(String roleId) throws RemoteException {
	String queryString = "select p from RolePrivilege rp, Privilege p " + "where rp.rolePrivilegePK.privilegeId = p.privilegeID " + "and rp.rolePrivilegePK.roleId = :roleId";
	Query query = hsession.createQuery(queryString);
	query.setString("roleId", roleId);
	return query.list();
    }

    public Collection getUserRoles(String userId) throws RemoteException {
	String queryString = "select r from UserRole ur, Role r " + "where ur.userRolePK.roleId = r.roleID " + "and ur.userRolePK.userId = :userId";
	Query query = hsession.createQuery(queryString);
	query.setString("userId", userId);
	return query.list();
    }

    public Collection getUsersByPrivilege(String privId) {
	String queryString = "select u from User u, UserRole ur, Role r, RolePrivilege rp, Privilege p " + "where u.userID = ur.userRolePK.userId " + "and ur.userRolePK.roleId = r.roleID " + "and r.roleID = rp.rolePrivilegePK.roleId " + "and rp.rolePrivilegePK.privilegeId = p.privilegeID " + "and p.privilegeID = :privId " + "and u.status = :status";
	Query query = hsession.createQuery(queryString);
	query.setString("privId", privId);
	query.setString("status", User.STATUS_ACTIVE);
	return query.list();
    }

    public Collection getUsersByPrivilegeAndBranch(String privId, String branchId) {
	String queryString = "select u from User u, UserRole ur, Role r, RolePrivilege rp, Privilege p " + "where u.userID = ur.userRolePK.userId " + "and u.branchNumber = :branchId " + "and ur.userRolePK.roleId = r.roleID " + "and r.roleID = rp.rolePrivilegePK.roleId " + "and rp.rolePrivilegePK.privilegeId = p.privilegeID " + "and p.privilegeID = :privId " + "and u.status = :status";
	Query query = hsession.createQuery(queryString);
	query.setString("branchId", branchId);
	query.setString("privId", privId);
	query.setString("status", User.STATUS_ACTIVE);

	System.out.println("getUsersByPrivilegeAndBranch: SQL: " + query.toString());

	return query.list();
    }

    public boolean isPrivilegedUser(String userId, String priv) throws RemoteException {

	Privilege thePriv = null;
	Collection privList = getUserPrivileges(userId);
	String privString = null;
	if (privList != null) {
	    Iterator privIterator = privList.iterator();
	    while (privIterator.hasNext()) {
		thePriv = (Privilege) privIterator.next();
		privString = thePriv.getPrivilegeID().trim();
		if (privString.equalsIgnoreCase(priv)) {
		    return true;
		}
	    }
	}

	return false;

    }

    public Collection getUserPrivileges(String userId) throws RemoteException {
	String queryString = "select p from User u, UserRole ur, Role r, RolePrivilege rp, Privilege p " + "where u.userID = ur.userRolePK.userId " + "and ur.userRolePK.roleId = r.roleID " + "and r.roleID = rp.rolePrivilegePK.roleId " + "and rp.rolePrivilegePK.privilegeId = p.privilegeID " + "and u.userID = :userId";
	Query query = hsession.createQuery(queryString);
	query.setString("userId", userId);
	return query.list();
    }

    private String getAllowedRoles(Collection roles) throws RemoteException {
	String sql = "select role_id,role_name,description from PUB.x_role ";
	StringBuffer whereClause = new StringBuffer();

	Iterator roleIT = roles.iterator();
	boolean allFound = false;
	whereClause.setLength(0);

	while (roleIT.hasNext() && allFound == false) {

	    String role = ((Role) roleIT.next()).getRoleID().trim();

	    String allowedRoles = CommonEJBHelper.getCommonMgrLocal().getSystemParameterValue(SystemParameterVO.CATEGORY_ROADSIDE_ROLE_CREATE, role);

	    try {
		if (allowedRoles.trim().equalsIgnoreCase("all")) {
		    whereClause.setLength(0);
		    allFound = true;
		} else {
		    whereClause.append(allowedRoles);
		    whereClause.append(",");
		}
	    } catch (NullPointerException ne) {
		// do nothing
	    }

	}

	if (whereClause.length() > 0) {
	    whereClause.setLength(whereClause.length() - 1);
	    sql += " where role_id in ( '" + StringUtil.replaceAll(whereClause.toString(), ",", "','") + "' ) ";
	}

	return sql;
    }

    /**
     * get all roles in the system
     */
    public Collection getAllRoles() throws RemoteException {
	Criteria crit = hsession.createCriteria(Role.class);
	return crit.list();
    }

    /**
     * Get the roles according to the sql statement.
     * 
     * @throws RemoteException
     * @return Collection
     */
    private Collection getTheRoles(String sql) throws RemoteException {
	Connection connection = null;
	PreparedStatement statement = null;
	ArrayList roleList = new ArrayList();
	Role role = null;
	try {
	    connection = clientDataSource.getConnection();

	    statement = connection.prepareStatement(sql);

	    ResultSet resultSet = statement.executeQuery();
	    while (resultSet.next()) {
		// new role
		role = new Role();
		role.setRoleID(resultSet.getString(1));
		role.setName(resultSet.getString(2));
		role.setDescription(resultSet.getString(3));
		// add role to the list
		roleList.add(role);
	    }
	    return roleList;
	} catch (SQLException e) {
	    throw new RemoteException("Unable to get roles: " + e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
    }

    /**
     * reset all user passwords in the system
     */
    public void resetAllPasswords() throws RemoteException {
	Collection c = userMgr.getUsers();
	if (c != null) {
	    Iterator it = c.iterator();
	    while (it.hasNext()) {
		User user = (User) it.next();
		this.resetPassword(user.getUserID());
	    }
	}
    }

    /**
     * reset a user's password to their user id
     */
    public void resetPassword(String userID) throws RemoteException {

	User user = userMgr.getUser(userID);
	String newPassword = "";
	try {
	    newPassword = SecurityHelper.encryptPassword(userID);
	} catch (java.security.GeneralSecurityException gse) {
	    throw new RemoteException(gse.toString());
	}
	user.setPassword(newPassword);
	userMgr.updateUser(user);
    }

    /**
     * change user password
     */
    public void changePassword(String userID, String newPassword, String verifyPassword) throws RemoteException {
	// check user id is valid
	if (userID == null || (userID.length() == 0)) {
	    throw new RemoteException("User Id is null or empty. Choose a valid user first.");
	}
	// check if password is null
	if (newPassword == null || verifyPassword == null) {
	    throw new RemoteException("Old password or new password may not be null");
	}
	// check if passwords match
	if (!newPassword.equals(verifyPassword)) {
	    throw new RemoteException("New password and old password do not match.");
	}

	String encryptedPassword = "";
	String encryptedVerifyPassword = "";
	try {
	    encryptedPassword = SecurityHelper.encryptPassword(newPassword);
	    encryptedVerifyPassword = SecurityHelper.encryptPassword(verifyPassword);
	} catch (java.security.GeneralSecurityException gse) {
	    throw new RemoteException("Unable to encrypt password. " + gse);
	}

	User user = userMgr.getUser(userID);
	user.setPassword(encryptedPassword);
	userMgr.updateUser(user);
    }

    /**
     * Get role by Id.
     */
    public Role getRole(String roleID) throws RemoteException {
	Role role = null;
	try {
	    role = (Role) hsession.get(Role.class, roleID);
	} catch (HibernateException ex) {
	    throw new SystemException(ex);
	}
	return role;
    }

    /**
     * Obtain a list of logged in users
     */
    public Collection getLoggedInUsers() throws RemoteException {

	Collection loggedInUsers = null;

	String sql = "SELECT \"session_id\", \"create_date\", \"create_time\", \"user_id\",\"ip_address\",\"system_name\" FROM PUB.\"x_session\"";

	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    ResultSet rs = statement.executeQuery();

	    User user = null;
	    UserSession us = null;

	    loggedInUsers = new ArrayList();

	    while (rs.next()) {

		// get the records
		String sessionId = rs.getString(1);
		java.sql.Date createDate = rs.getDate(2);
		int createTime = rs.getInt(3);
		String userId = rs.getString(4);
		String ipAddress = rs.getString(5);
		String systemName = rs.getString(6);

		try {
		    user = userMgr.getUser(userId);
		} catch (Exception e) {
		    e.printStackTrace();
		    throw new EJBException(e.toString());
		}

		// create the usersession objects
		us = new UserSession(user, sessionId, ipAddress, systemName, new DateTime(createDate.getTime() + createTime));

		// add usersession
		loggedInUsers.add(us);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new EJBException("Error executing SQL " + sql + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return loggedInUsers;

    }

    /**
     * log user in and insert a record into the session table
     */
    public void logIn(String sessionID, String userID, DateTime sessionStart, String ipAddress, String systemName) throws RemoteException {

	// get time component
	Integer sessionStartTime = null;
	// get date component
	DateTime sessionStartDate = null;
	if (sessionStart != null) {
	    sessionStartTime = new Integer(sessionStart.getMillisecondsFromMidnight());
	    sessionStartDate = sessionStart.getDateOnly();
	}

	// System.out.println("sessionStartTime "+sessionStartTime);

	String sql = "INSERT INTO PUB.\"x_session\" (\"session_id\", \"create_date\", \"create_time\", \"user_id\",\"ip_address\",\"system_name\") VALUES (?, ?, ?, ?, ?, ?)";

	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, sessionID);
	    if (sessionStartDate == null) {
		statement.setNull(2, Types.DATE);
	    } else {
		statement.setDate(2, sessionStartDate.toSQLDate());

	    }
	    if (sessionStartTime == null) {
		statement.setNull(3, Types.INTEGER);
	    } else {
		statement.setInt(3, sessionStartTime.intValue());

	    }
	    statement.setString(4, userID);
	    statement.setString(5, ipAddress);
	    statement.setString(6, systemName);

	    if (statement.executeUpdate() != 1) {
		throw new RemoteException("Error adding session information to database.");
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + sql + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
    }

    public boolean validateUnixUser(String userId, String password) throws RemoteException {
	boolean isValid = false;
	SecurityAdapter secAdapter = SecurityFactory.getSecurityAdapter();
	isValid = secAdapter.authenticateUser(userId, password);
	return isValid;
    }

    public ArrayList getUserMenuMetaData(String userId, String menuName) throws RemoteException {
	ArrayList menuList = new ArrayList();
	SecurityAdapter secAdapter = SecurityFactory.getSecurityAdapter();
	menuList = secAdapter.getUserMenuMetaData(userId, menuName);
	return menuList;
    }

}
