package com.ract.security;

import java.rmi.RemoteException;
import java.security.GeneralSecurityException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.common.CommonConstants;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;
import com.ract.util.LogUtil;

/**
 * Security helper.
 * 
 * @author John Holliday
 * @created 2 August 2002
 * @version 1.0
 */

public class SecurityHelper {

    /**
     * Gets the cookie attribute of the SecurityHelper class
     * 
     * @param request
     *            Servlet request
     * @param CookieName
     *            the name of the cookie
     * @return The cookie value
     */
    public static String getCookie(HttpServletRequest request, String cookieName) {
	String cookieResult = "";
	Cookie[] reqCookies = request.getCookies();
	if (reqCookies != null) {
	    for (int i = 0; i < reqCookies.length; i++) {
		if (reqCookies[i].getName().equals(cookieName)) {
		    cookieResult = reqCookies[i].getValue();
		}
	    }
	}
	// else
	// {
	// log("no cookies available");
	// }

	return cookieResult;
    }

    /**
     * Sets the cookies on the response
     * 
     * @param response
     *            Servlet response
     * @param CookieName
     *            The name of the cookie
     * @param CookieValue
     *            The new cookie value
     */
    public static void setCookie(HttpServletResponse response, String CookieName, String CookieValue) {

	// Create a new cookie and set the value.
	Cookie cookie = new Cookie(CookieName, CookieValue);

	// set the cookie version.
	cookie.setVersion(0);
	// expire when browser exits.
	cookie.setMaxAge(-1);

	// add the cookie to the response
	response.addCookie(cookie);
    }

    /**
     * factory method to return the default system user
     */
    public static User getSystemUser() throws RemoteException {
	return getUser(CommonConstants.USER_SYSTEM);
    }

    /**
     * factory method to return the ivr user
     */
    public static User getIVRUser() throws RemoteException {
	return getUser(CommonConstants.USER_IVR);
    }

    /**
     * Return a user session given a user id with default session attributes.
     */
    public static User getUser(String userID) throws RemoteException {
	User user = null;
	try {
	    UserMgr uMgr = UserEJBHelper.getUserMgr();
	    user = uMgr.getUser(userID);
	    LogUtil.log("UserSession", "user=" + user);
	} catch (Exception e) {
	    throw new RemoteException("Error: unable to get the  user '" + userID + "'.", e);
	}
	return user;
    }

    /**
     * Factory method to return the Roadside user
     */
    public static User getRoadsideUser() throws RemoteException {
	return getUser(CommonConstants.USER_ROADSIDE);
    }

    /**
     * Factory method to return the OCR user
     */
    public static User getOCRUser() throws RemoteException {
	return getUser(CommonConstants.USER_OCR);
    }

    /**
     * delete cookies from the request
     * 
     * @param request
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static void deleteCookie(HttpServletRequest request, String cookieName) {
	Cookie[] reqCookies = request.getCookies();
	if (reqCookies != null) {
	    for (int i = 0; i < reqCookies.length; i++) {
		if (reqCookies[i].getName().equals(cookieName)) {
		    reqCookies[i].setMaxAge(0);
		}
	    }
	}
	// else
	// {
	// log("no cookies available");
	// }
    }

    /**
     * delete cookies from the request
     * 
     * @param request
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public static String deleteCookies(HttpServletRequest request) {
	String cookieResult = "";
	Cookie[] reqCookies = request.getCookies();
	if (reqCookies != null) {
	    for (int i = 0; i < reqCookies.length; i++) {
		reqCookies[i].setMaxAge(0);
	    }
	}
	// else
	// {
	// log("no cookies available");
	// }

	return cookieResult;
    }

    /**
     * One way encryption mechanism
     * 
     * @todo Resolve issue with character set difference between java and sql
     *       server.
     * @todo The sql server 2000 column should be varbinary and stored natively
     *       as a binary array.
     * 
     * @param password
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception GeneralSecurityException
     *                Description of the Exception
     */
    public static String encryptPassword(String password) throws GeneralSecurityException {
	String encryptedPassword = "";
	// MessageDigest sha = null;

	// try
	// {
	// sha = MessageDigest.getInstance("SHA");
	// }
	// catch (NoSuchAlgorithmException e)
	// {
	// throw new
	// GeneralSecurityException("Error: encryption algorithm does not exist. "
	// + e.getMessage());
	// }
	// sha.reset();
	// sha.update(password.getBytes());
	//
	// byte[] hash = sha.digest();
	//
	// try
	// {
	// //use specific character encoding
	// encryptedPassword = new String(hash,"ISO8859-1");
	// }
	// catch (Exception e)
	// {
	// throw new GeneralSecurityException(e.toString());
	// }

	encryptedPassword = password.trim();
	return encryptedPassword;
    }

}
