package com.ract.security;

import java.io.Serializable;

public class RolePrivilegePK implements Serializable {
    private String roleId;

    private String privilegeId;

    /**
     * @hibernate.key-property position="1" column="role_id"
     */
    public String getRoleId() {
	return roleId;
    }

    public void setRoleId(String roleId) {
	this.roleId = roleId;
    }

    @Override
    public String toString() {
	return "RolePrivilegePK [privilegeId=" + privilegeId + ", roleId=" + roleId + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((privilegeId == null) ? 0 : privilegeId.hashCode());
	result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof RolePrivilegePK))
	    return false;
	RolePrivilegePK other = (RolePrivilegePK) obj;
	if (privilegeId == null) {
	    if (other.privilegeId != null)
		return false;
	} else if (!privilegeId.equals(other.privilegeId))
	    return false;
	if (roleId == null) {
	    if (other.roleId != null)
		return false;
	} else if (!roleId.equals(other.roleId))
	    return false;
	return true;
    }

    /**
     * @hibernate.key-property position="1" column="priv_id"
     */
    public String getPrivilegeId() {
	return privilegeId;
    }

    public void setPrivilegeId(String privilegeId) {
	this.privilegeId = privilegeId;
    }
}
