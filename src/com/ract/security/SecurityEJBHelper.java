package com.ract.security;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

/**
 * EJBHelper to get Home interface of security EJBs.
 * 
 * @author John Holliday
 * @version 1.0
 */

public class SecurityEJBHelper {

    public static SecurityMgr getSecurityMgr() {
	SecurityMgr securityMgr = null;
	try {
	    securityMgr = (SecurityMgr) ServiceLocator.getInstance().getObject("SecurityMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return securityMgr;
    }

    public static SecurityMgrLocal getSecurityMgrLocal() {
	SecurityMgrLocal securityMgrLocal = null;
	try {
	    securityMgrLocal = (SecurityMgrLocal) ServiceLocator.getInstance().getObject("SecurityMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return securityMgrLocal;
    }

}