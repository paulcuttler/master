package com.ract.security;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class WindowsAccountManager {

    // Need LDAP over SSL for password changes

    public static void main(String[] args) {

	Hashtable env = new Hashtable();
	String adminName = "CN=John Holliday,OU=Information Systems,OU=Hobart,DC=ract,DC=com,DC=au";
	String adminPassword = "XXXX";
	String userName = "CN=temp,CN=Users,DC=ract,DC=com,DC=au";
	String newPassword = "Password123";

	// Access the keystore, this is where the Root CA public key cert was
	// installed
	// Could also do this via command line java
	// -Djavax.net.ssl.trustStore....
	String keystore = "/Program Files/Java/jdk1.5.0_11/jre/lib/security";
	System.setProperty("javax.net.ssl.trustStore", keystore);

	env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

	// set security credentials, note using simple cleartext authentication
	env.put(Context.SECURITY_AUTHENTICATION, "simple");
	env.put(Context.SECURITY_PRINCIPAL, adminName);
	env.put(Context.SECURITY_CREDENTIALS, adminPassword);

	// specify use of ssl
	env.put(Context.SECURITY_PROTOCOL, "ssl");

	// connect to my domain controller
	String ldapURL = "ldaps://saab:636"; // 636 - SSL ldaps
	env.put(Context.PROVIDER_URL, ldapURL);
	LdapContext ctx = null;
	try {

	    // Create the initial directory context
	    ctx = new InitialLdapContext(env, null);

	    // set password is a ldap modfy operation
	    ModificationItem[] mods = new ModificationItem[1];

	    // Replace the "unicdodePwd" attribute with a new value
	    // Password must be both Unicode and a quoted string
	    String newQuotedPassword = "\"" + newPassword + "\"";
	    byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");

	    mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("unicodePwd", newUnicodePassword));

	    // Perform the update
	    ctx.modifyAttributes(userName, mods);

	    System.out.println("Reset Password for: " + userName);
	    ctx.close();

	}

	catch (Exception e) {
	    e.printStackTrace();
	    try {
		// System.out.println("Problem resetting password: " + e);
		ctx.close();
	    } catch (Exception ez) {
		// TODO: handle exception
	    }
	}

    }

}
