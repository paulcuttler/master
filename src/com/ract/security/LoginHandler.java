package com.ract.security;

import java.rmi.RemoteException;

import javax.servlet.http.HttpSession;

import com.ract.user.UserSession;

/**
 * Login handler to check if a user is logged in and other user functions.
 * 
 * @author John Holliday
 * @created 2 August 2002
 * @version 1.0, 21/5/2002
 */

public class LoginHandler {

    /**
     * check if a user is currently logged in
     * 
     * @param sessionId
     *            Description of the Parameter
     * @return The loggedIn value
     * @exception Exception
     *                Description of the Exception
     */
    public static boolean isLoggedIn(String sessionId, HttpSession session) throws Exception {
	boolean loggedIn = false;
	/**
	 * @todo check the database for any current sessions.
	 */
	try {
	    SecurityMgr sMgr = SecurityEJBHelper.getSecurityMgr();
	    // record in the database
	    loggedIn = sMgr.isLoggedIn(sessionId);

	    // record in the session
	    if (UserSession.getUserSession(session) == null) {
		loggedIn = false;
	    }
	} catch (RemoteException ex) {
	    throw new Exception("Unable to verify if user is logged in. " + ex.toString());
	}
	return loggedIn;
    }

}
