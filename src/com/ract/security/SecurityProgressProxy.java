package com.ract.security;

import java.io.IOException;

import com.progress.open4gl.ConnectException;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;
import com.ract.common.CommonConstants;
import com.ract.common.proxy.ProgressProxy;

public class SecurityProgressProxy extends SecurityProxy implements ProgressProxy {
    public SecurityProgressProxy() throws ConnectException, Open4GLException, SystemErrorException, IOException {
	// connection specific?
	super(CommonConstants.getProgressAppServerURL(), CommonConstants.getProgressAppserverUser(), CommonConstants.getProgressAppserverPassword(), null);
    }

}
