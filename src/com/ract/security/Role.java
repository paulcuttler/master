package com.ract.security;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Collection;

import com.ract.util.LogUtil;

/**
 * Role object
 * 
 * @hibernate.class table="x_role" lazy="false"
 * 
 * @author John Holliday
 * @version 1.0
 */

public class Role implements Serializable {
    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
	ois.defaultReadObject();
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
	oos.defaultWriteObject();
    }

    public void setRoleID(String roleID) {
	this.roleID = roleID;
    }

    /**
     * @hibernate.id column="role_id" generator-class="assigned"
     */
    public String getRoleID() {
	return roleID;
    }

    public void setName(String Name) {
	this.Name = Name;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="role_name"
     */
    public String getName() {
	return Name;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="description"
     */
    public String getDescription() {
	return description;
    }

    @Override
    public String toString() {
	return "Role [Name=" + Name + ", description=" + description + ", privileges=" + privileges + ", roleID=" + roleID + "]";
    }

    private String roleID;

    private String Name;

    private String description;

    public Collection getPrivileges() {
	if (this.privileges == null) {
	    try {
		SecurityMgrLocal securityMgrLocal = SecurityEJBHelper.getSecurityMgrLocal();
		securityMgrLocal.getRolePrivileges(this.roleID);
	    } catch (RemoteException e) {
		LogUtil.warn(this.getClass(), e);
	    }
	}
	return this.privileges;
    }

    public Collection privileges;

}
