package com.ract.security;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.progress.open4gl.BooleanHolder;
import com.progress.open4gl.ResultSetHolder;
import com.ract.common.SystemException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.util.LogUtil;

/**
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class ProgressSecurityAdapter extends TransactionAdapterType implements SecurityAdapter {

    public ProgressSecurityAdapter() {
    }

    public void release() throws com.ract.common.SystemException {
	/**
	 * @todo Implement this com.ract.common.transaction.TransactionAdapter
	 *       abstract method
	 */
    }

    public void rollback() throws com.ract.common.SystemException {
	/**
	 * @todo Implement this com.ract.common.transaction.TransactionAdapter
	 *       abstract method
	 */
    }

    public String getSystemName() {
	/**
	 * @todo Implement this com.ract.common.transaction.TransactionAdapter
	 *       abstract method
	 */
	return "Security";
    }

    public void commit() throws com.ract.common.SystemException {
	/**
	 * @todo Implement this com.ract.common.transaction.TransactionAdapter
	 *       abstract method
	 */
    }

    public boolean authenticateUser(String userId, String password) throws SystemException {
	BooleanHolder validHolder = new BooleanHolder();
	boolean isValid = false;
	SecurityProgressProxy secProxy = null;
	try {
	    secProxy = this.getSecurityProgressProxy();
	    secProxy.authenticateUser(userId, password, validHolder);
	    isValid = validHolder.getBooleanValue();
	} catch (Exception ex) {
	    throw new SystemException("SecurityProxy error", ex);
	} finally {
	    this.releaseProxy(secProxy);
	}
	return isValid;
    }

    private SecurityProgressProxy getSecurityProgressProxy() throws SystemException {
	SecurityProgressProxy securityProgressProxy = null;
	try {
	    securityProgressProxy = (SecurityProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_SECURITY_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return securityProgressProxy;
    }

    private void releaseProxy(SecurityProgressProxy proxy) {
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_SECURITY_PROXY, proxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), ex.getMessage());
	} finally {
	    proxy = null;
	}
    }

    public ArrayList getUserMenuMetaData(String userId, String menuName) throws SystemException {
	ArrayList menuList = new ArrayList();
	SecurityProgressProxy secProxy = null;
	ResultSetHolder rsh = new ResultSetHolder();
	ResultSet rs = null;
	UserMenuMetaData um = null;
	try {
	    secProxy = this.getSecurityProgressProxy();
	    secProxy.getusrmenu(menuName, userId, rsh);
	    rs = rsh.getResultSetValue();
	    while (rs.next()) {
		um = new UserMenuMetaData();
		// um.setSeqNo(rs.getInt(1));
		// um.setDispSeq(rs.getInt(2));
		um.setMParent(rs.getString(3));
		um.setMName(rs.getString(4));
		um.setMType(rs.getString(5));
		um.setLabel(rs.getString(6));
		um.setMHelp(rs.getString(7));
		um.setHotKey(rs.getString(8));

		um.setMAvailable(rs.getBoolean(9));
		menuList.add(um);
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException("SecurityProxy error", ex);
	} finally {
	    this.releaseProxy(secProxy);
	}
	return menuList;
    }
}
