package com.ract.security;

/**
 * @hibernate.class table="x_user_role" lazy="false"
 * @author hollidayj
 */
public class UserRole {
    private UserRolePK userRolePK;

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public UserRolePK getUserRolePK() {
	return userRolePK;
    }

    public void setUserRolePK(UserRolePK userRolePK) {
	this.userRolePK = userRolePK;
    }

    @Override
    public String toString() {
	return "UserRole [userRolePK=" + userRolePK + "]";
    }
}
