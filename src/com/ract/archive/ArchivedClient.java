package com.ract.archive;

import java.io.Serializable;

import com.ract.util.DateTime;
import com.ract.util.FileUtil;

/**
 * <p>
 * Represent an archived client
 * </p>
 * 
 * @author dgk
 * @version 1.0 28/05/07
 * @hibernate.class table="ArchivedClient" lazy="false"
 */

public class ArchivedClient implements Serializable {
    private Integer clientNo = null;
    private String givenNames = null;
    private String initials = null;
    private String property = null;
    private String streetChar = null;
    private String street = null;
    private String suburb = null;
    private String addressPostcode = null;
    private String business = null;
    private com.ract.util.DateTime archiveDate = null;
    private com.ract.util.DateTime birthDate = null;
    private String surname = null;

    private String status;

    private DateTime createDate;

    private DateTime lastUpdate;

    private String createLogonId;

    /**
     * @hibernate.id column="clientNo" generator-class="assigned"
     */
    public Integer getClientNo() {
	return clientNo;
    }

    public void setClientNo(Integer clientNo) {
	this.clientNo = clientNo;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="givenNames"
     */
    public String getGivenNames() {
	return givenNames;
    }

    public void setGivenNames(String givenNames) {
	this.givenNames = givenNames;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="initials"
     */
    public String getInitials() {
	return initials;
    }

    public void setInitials(String initials) {
	this.initials = initials;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="property"
     */
    public String getProperty() {
	return property;
    }

    public void setProperty(String addressProperty) {
	this.property = addressProperty;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="streetChar"
     */
    public String getStreetChar() {
	return streetChar;
    }

    public void setStreetChar(String addressChar) {
	this.streetChar = addressChar;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="street"
     */
    public String getStreet() {
	return street;
    }

    public void setStreet(String addressStreet) {
	this.street = addressStreet;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="suburb"
     */
    public String getSuburb() {
	return suburb;
    }

    public void setSuburb(String addressSuburb) {
	this.suburb = addressSuburb;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="postcode"
     */
    public String getAddressPostcode() {
	return addressPostcode;
    }

    public void setAddressPostcode(String addressPostcode) {
	this.addressPostcode = addressPostcode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="business"
     */
    public String getBusiness() {
	return business;
    }

    public void setBusiness(String business) {
	this.business = business;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="archiveDate"
     */
    public com.ract.util.DateTime getArchiveDate() {
	return archiveDate;
    }

    public void setArchiveDate(com.ract.util.DateTime archiveDate) {
	this.archiveDate = archiveDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="birthDate"
     */
    public com.ract.util.DateTime getBirthDate() {
	return birthDate;
    }

    public void setBirthDate(com.ract.util.DateTime birthDate) {
	this.birthDate = birthDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="surname"
     */
    public String getSurname() {
	return surname;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="status"
     */
    public String getStatus() {
	return status;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="createDate"
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="createLogonId"
     */
    public String getCreateLogonId() {
	return createLogonId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="lastUpdate"
     */
    public DateTime getLastUpdate() {
	return lastUpdate;
    }

    public void setSurname(String surname) {
	this.surname = surname;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(clientNo);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(surname);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(status);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(createDate);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(createLogonId);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(lastUpdate);
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(business);
	return desc.toString();

    }

    public void setCreateLogonId(String createLogonId) {
	this.createLogonId = createLogonId;
    }

    public void setLastUpdate(DateTime lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    public String display() {
	String disp = "\nClient no =   " + this.clientNo + "\nGiven Names = " + this.givenNames + "\nInitials =    " + this.initials + "\nSurname =     " + this.surname + "\nBirth Date =  " + this.birthDate + "\nStreet Char = " + this.streetChar + "\nStreet =      " + this.street + "\nSuburb =      " + this.suburb;
	return disp;
    }
}
