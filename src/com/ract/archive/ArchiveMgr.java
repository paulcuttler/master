package com.ract.archive;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.ejb.Remote;

@Remote
public interface ArchiveMgr {
    public ArrayList search(ArchivedClient sCrit) throws RemoteException;

    public ArchivedClient getArchivedClient(Integer clientNo) throws RemoteException;

    public void updateArchivedClient(ArchivedClient archivedClient) throws RemoteException;

    public void createArchivedClient(ArchivedClient archivedClient) throws RemoteException;
}
