package com.ract.archive;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

@Stateless
@Remote({ ArchiveMgr.class })
public class ArchiveMgrBean {

    @PersistenceContext(unitName = "master")
    Session hsession;

    public void updateArchivedClient(ArchivedClient archivedClient) {
	hsession.update(archivedClient);
    }

    public void createArchivedClient(ArchivedClient archivedClient) {
	hsession.save(archivedClient);
    }

    public ArchivedClient getArchivedClient(Integer clientNo) throws RemoteException {
	ArchivedClient ac = null;
	try {
	    ac = (ArchivedClient) hsession.get(ArchivedClient.class, clientNo);
	} catch (HibernateException ex) {
	    throw new RemoteException("ERROR retrieving ArchivedClient\n", ex);
	}
	return ac;
    }

    public ArrayList search(ArchivedClient sCrit) throws RemoteException {
	ArrayList fList = null;
	Criteria c = null;
	boolean otherCrit = false;
	// LogUtil.log(this.getClass(), "\nSearching......." + sCrit.display());
	try {
	    c = hsession.createCriteria(ArchivedClient.class);
	    if (sCrit.getClientNo() != null) {
		c.add(Restrictions.eq("clientNo", sCrit.getClientNo()));
	    } else {
		if (sCrit.getSurname() != null) {
		    otherCrit = true;
		    c.add(Restrictions.like("surname", sCrit.getSurname() + "%").ignoreCase());
		}
		if (sCrit.getInitials() != null) {
		    otherCrit = true;
		    c.add(Restrictions.like("initials", "%" + sCrit.getInitials() + "%"));
		}
		if (sCrit.getStreet() != null) {
		    otherCrit = true;
		    c.add(Restrictions.like("street", sCrit.getStreet() + "%"));
		}
		if (sCrit.getStreetChar() != null) {
		    otherCrit = true;
		    c.add(Restrictions.like("streetChar", sCrit.getStreetChar() + "%"));
		}
		if (sCrit.getSuburb() != null) {
		    otherCrit = true;
		    c.add(Restrictions.eq("suburb", sCrit.getSuburb()));
		}
		if (sCrit.getBirthDate() != null) {
		    if (otherCrit) {
			c.add(Restrictions.or(Restrictions.eq("birthDate", sCrit.getBirthDate()), Restrictions.isNull("birthDate")));
		    } else
			c.add(Restrictions.eq("birthDate", sCrit.getBirthDate()));
		}
	    }
	    fList = new ArrayList(c.list());
	} catch (HibernateException ex) {
	    throw new RemoteException("ERROR searching for archived clients\n", ex);

	}
	// LogUtil.log(this.getClass(),c.list().size() + " records found");
	return fList;
    }

}
