package com.ract.archive;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

public class ArchiveEJBHelper {

    public static ArchiveMgr getArchiveMgr() {
	ArchiveMgr archiveMgr = null;
	try {
	    archiveMgr = (ArchiveMgr) ServiceLocator.getInstance().getObject("ArchiveMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return archiveMgr;
    }

}
