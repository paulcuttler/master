package com.ract.travel;

import java.rmi.RemoteException;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

public class TramadaClientHelper {
    // private TramadaClientHelper()
    // {
    // }

    public static String getTramadaProfileCode(ClientVO client) {
	// RACT construct
	final int max = 30;
	final String DELIMITER = "&";
	String initials = client.getInitials() == null ? "" : client.getInitials();
	initials = StringUtil.replaceAll(initials, " ", "");
	if (initials.indexOf(DELIMITER) != -1) {
	    initials = initials.split(DELIMITER)[0]; // element 0
	}
	String surname = client.getSurname() == null ? "" : client.getSurname();
	surname = StringUtil.replaceAll(surname, " ", "");
	String clientNumber = client.getClientNumber().toString();
	String tramadaClientProfileCode = surname + initials + clientNumber;
	if (tramadaClientProfileCode.length() > max) {
	    int overlength = tramadaClientProfileCode.length() - max; // eg. 6
								      // for len
								      // 36
	    tramadaClientProfileCode = surname + initials;
	    tramadaClientProfileCode = tramadaClientProfileCode.substring(0, tramadaClientProfileCode.length() - overlength);
	    tramadaClientProfileCode = tramadaClientProfileCode + clientNumber;
	}
	LogUtil.debug("TramadaClientHelper", "tramadaClientProfileCode=" + tramadaClientProfileCode);
	LogUtil.debug("TramadaClientHelper", "length=" + tramadaClientProfileCode.length());
	return tramadaClientProfileCode;
    }

    public static TramadaClientTransaction getTramadaClientTransaction(Integer clientNumber) throws RemoteException {
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	TramadaClientTransaction tramadaClientTransaction = clientMgr.getLastTramadaClientTransaction(clientNumber);
	return tramadaClientTransaction;
    }

}
