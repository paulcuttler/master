package com.ract.travel.notifier;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Properties;

import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentTransactionMgr;

public class TravelPaymentAdapter extends TransactionAdapterType implements TransactionAdapter {

    private PaymentTransactionMgr paymentTransactionMgr;

    String userId;
    String branchCode;
    String tillCode;
    String receiptNumber;
    String clientNumber;
    String[] products;
    double[] costs;
    String[] paymentType;
    double[] tendered;
    Properties[] paymentBreakdown;
    String comments;
    Date createDate;
    String sourceSystem;

    public TravelPaymentAdapter(String userName, String branchCode, String tillCode, String receiptNumber, String clientNumber, String[] products, double[] costs, String[] paymentType, double[] tendered, Properties[] paymentBreakdown, String comments, Date createDate, String sourceSystem) throws RemoteException {
	this.userId = userName;
	this.branchCode = branchCode;
	this.tillCode = tillCode;
	this.receiptNumber = receiptNumber;
	this.clientNumber = clientNumber;
	this.products = products;
	this.costs = costs;
	this.paymentType = paymentType;
	this.tendered = tendered;
	this.paymentBreakdown = paymentBreakdown;
	this.comments = comments;
	this.createDate = createDate;
	this.sourceSystem = sourceSystem;

    }

    public void createPaidTransaction() throws RemoteException {
	paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();
	try {
	    paymentTransactionMgr.createPaidTransaction(userId, branchCode, tillCode, receiptNumber, clientNumber, products, costs, paymentType, tendered, paymentBreakdown, comments, createDate, sourceSystem);

	} catch (RollBackException rbe) {
	    throw new SystemException(rbe);
	}

    }

    public String getSystemName() {
	return SourceSystem.TRAVEL.getAbbreviation();
    }

    public void commit() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.commit();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    }
	}
    }

    public void rollback() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.rollback();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    }
	}
    }

    public void release() throws SystemException {
	if (paymentTransactionMgr != null) {
	    try {
		paymentTransactionMgr.release();
	    } catch (RemoteException ex) {
		throw new SystemException(ex);
	    }
	}
    }
}
