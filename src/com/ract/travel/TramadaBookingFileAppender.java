package com.ract.travel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * <p>
 * Append one booking file to another file skipping the header
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class TramadaBookingFileAppender {

    public static void main(String[] args) {
	String inputFileName = args[0]; // new file
	String outputFileName = args[1]; // existing file
	System.out.println("inputFileName=" + inputFileName);
	System.out.println("outputFileName=" + outputFileName);
	try {
	    FileWriter outputFileWriter = new FileWriter(outputFileName, true);
	    File inputFile = new File(inputFileName);
	    // look for file1
	    BufferedReader inputFileReader = new BufferedReader(new FileReader(inputFile));
	    String line = null;
	    int counter = 0;
	    int linesWritten = 0;
	    while ((line = inputFileReader.readLine()) != null) {
		counter++;
		// skip the header
		if (counter > 1) {
		    linesWritten++;
		    outputFileWriter.write(line + "\n");
		} else {
		    // ignore header line
		    // outputFileWriter.write("\n");
		}
	    }
	    // flush and close output stream
	    outputFileWriter.flush();
	    outputFileWriter.close();
	    inputFileReader.close();

	    System.out.println("Appended " + linesWritten + " lines to file '" + inputFileName + "' from file '" + outputFileName + "'.");
	    // delete the input file
	    inputFile.delete();

	} catch (FileNotFoundException ex) {
	    System.err.println(ex);
	} catch (IOException ex) {
	    System.err.println(ex);
	}

    }

}
