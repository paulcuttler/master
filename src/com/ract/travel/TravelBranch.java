package com.ract.travel;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SalesBranchVO;

/**
 * <p>
 * Travel branch
 * </p>
 * 
 * @hibernate.class table="tr_branch" lazy="false"
 * 
 * @author not attributable
 * @version 1.0
 */
public class TravelBranch implements Serializable {
    private String salesBranchCode;

    private String emailAddress;

    /**
     * @hibernate.property
     * @hibernate.column name="email_address"
     */
    public String getEmailAddress() {
	return emailAddress;
    }

    public SalesBranchVO getSalesBranch() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.getSalesBranch(this.salesBranchCode);
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
    }

    /**
     * @hibernate.id column="sales_branch_code" generator-class="assigned"
     */
    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    public TravelBranch() {
    }
}
