package com.ract.travel;

import java.util.ArrayList;

import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;

public interface TravelAdapter extends TransactionAdapter {

    public ArrayList getTravelFiles(Integer clientNumber) throws SystemException;

}
