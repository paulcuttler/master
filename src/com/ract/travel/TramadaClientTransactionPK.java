package com.ract.travel;

import java.io.Serializable;

import com.ract.util.DateTime;

public class TramadaClientTransactionPK implements Serializable {
    private Integer clientNumber;

    private String tramadaProfileCode;

    private DateTime modified;

    private String actionType;

    /**
     * @hibernate.key-property position="1" column="modified"
     */
    public DateTime getModified() {
	return modified;
    }

    /**
     * @hibernate.key-property position="2" column="tramada_profile_code"
     */
    public String getTramadaProfileCode() {
	return tramadaProfileCode;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public void setModified(DateTime modified) {
	this.modified = modified;
    }

    public void setTramadaProfileCode(String tramadaProfileCode) {
	this.tramadaProfileCode = tramadaProfileCode;
    }

    public void setActionType(String actionType) {
	this.actionType = actionType;
    }

    /**
     * @hibernate.key-property position="3" column="client_number"
     * @return Integer
     */
    public Integer getClientNumber() {
	return clientNumber;
    }

    /**
     * @hibernate.key-property position="4" column="action_type"
     */
    public String getActionType() {
	return actionType;
    }

    public TramadaClientTransactionPK() {
    }

    public TramadaClientTransactionPK(Integer clientNumber, String tramadaProfileCode, String actionType, DateTime modified) {
	setClientNumber(clientNumber);
	setTramadaProfileCode(tramadaProfileCode);
	setActionType(actionType);
	setModified(modified);
    }

    /**
     * ABSOLUTELY MANDATORY METHODS FOR COMPOSITE KEYS OR RESULTS IN NPE
     */

    public boolean equals(Object o) {
	return super.equals(o);
    }

    public int hashCode() {
	return super.hashCode();
    }

    public String toString() {
	return this.getClientNumber() + " " + this.getTramadaProfileCode() + " " + this.getModified() + " " + this.getActionType();
    }

}
