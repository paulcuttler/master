package com.ract.travel;

import com.ract.util.DateTime;

/**
 * @hibernate.class lazy="false"
 */
public class TramadaClient {

    private String tramadaProfileCode;

    private Integer clientNumber;

    private DateTime createDate;

    private String consultant;

    private String branch;

    /**
     * @hibernate.property
     */
    public String getConsultant() {
	return consultant;
    }

    public void setConsultant(String consultant) {
	this.consultant = consultant;
    }

    /**
     * @hibernate.property
     */
    public String getBranch() {
	return branch;
    }

    public void setBranch(String branch) {
	this.branch = branch;
    }

    /**
     * @hibernate.id generator-class="assigned" unsaved-value="any"
     */
    public String getTramadaProfileCode() {
	return tramadaProfileCode;
    }

    public void setTramadaProfileCode(String tramadaProfileCode) {
	this.tramadaProfileCode = tramadaProfileCode;
    }

    /**
     * @hibernate.property
     */
    public Integer getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    /**
     * @hibernate.property
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }
}
