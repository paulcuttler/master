package com.ract.travel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.progress.open4gl.BigDecimalHolder;
import com.progress.open4gl.BooleanHolder;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.SystemException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class ProgressTravelAdapter implements TravelAdapter {
    public ProgressTravelAdapter() {
    }

    public ArrayList getTravelFiles(Integer clientNumber) throws SystemException {

	ArrayList travelFileList = new ArrayList();
	TravelProgressProxy travelProgressProxy = null;
	BooleanHolder commercial = new BooleanHolder();
	StringHolder corporateList = new StringHolder();
	ResultSetHolder travelAccountResultSetHolder = new ResultSetHolder();
	ResultSetHolder travelFileResultSetHolder = new ResultSetHolder();
	String travelAccountNumber = null;
	BigDecimalHolder totalGrossHolder = new BigDecimalHolder();
	BigDecimalHolder totalCommissionHolder = new BigDecimalHolder();
	BigDecimalHolder totalDiscountHolder = new BigDecimalHolder();
	BigDecimalHolder totalJournalHolder = new BigDecimalHolder();
	BigDecimalHolder totalRefundHolder = new BigDecimalHolder();
	BigDecimalHolder totalPaidHolder = new BigDecimalHolder();
	BigDecimalHolder totalSystemPaidHolder = new BigDecimalHolder();
	try {
	    travelProgressProxy = this.getTravelProgressProxy();
	    // LogUtil.log(this.getClass(),
	    // "Validating claim, gathered the InsuranceProxy");
	    travelProgressProxy.getTravelAccountList(clientNumber.intValue(), commercial, corporateList, travelFileResultSetHolder);

	    if (travelFileResultSetHolder != null) {

		try {
		    ResultSet travelAccountResultSet = travelAccountResultSetHolder.getResultSetValue();
		    int size = 0;
		    DateTime bookingDate = null;
		    while (travelAccountResultSet.next()) {
			// System.out.println("travel file"+size);
			travelAccountNumber = travelAccountResultSet.getString(1);

			// set travel file details
			travelProgressProxy.getTravelItemDetails(travelAccountNumber, totalGrossHolder, totalCommissionHolder, totalDiscountHolder, totalJournalHolder, totalRefundHolder, totalPaidHolder, totalSystemPaidHolder, travelFileResultSetHolder);

			if (travelFileResultSetHolder != null) {
			    ResultSet travelFileResultSet = travelFileResultSetHolder.getResultSetValue();
			    while (travelFileResultSet.next()) {
				bookingDate = new DateTime(travelFileResultSet.getDate(8));
				travelFileList.add(new TravelFile(travelAccountNumber, bookingDate));
				size++;
			    }
			}
		    }
		} catch (SQLException ex1) {
		    throw new SystemException(ex1);
		}

	    }

	} catch (Open4GLException ex) {
	    throw new SystemException("Unable to get travel file list for client '" + clientNumber + "'.", ex);
	} finally {
	    releaseTravelProgressProxy(travelProgressProxy);
	}

	return travelFileList;
    }

    private final static String SYSTEM_PROGRESS_TRAVEL = "ProgressTravel";

    private void releaseTravelProgressProxy(TravelProgressProxy travelProgressProxy) {
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_TRAVEL_PROXY, travelProgressProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), ex.getMessage());
	} finally {
	    travelProgressProxy = null;
	}
    }

    public void commit() throws SystemException {
    }

    public Exception getRollbackException() {
	return null;
    }

    public boolean getRollbackOnly() {
	return false;
    }

    public String getSystemName() {
	return SYSTEM_PROGRESS_TRAVEL;
    }

    public void release() throws SystemException {
    }

    public void rollback() throws SystemException {
    }

    public void setRollbackOnly(Exception exception) {
    }

    private TravelProgressProxy getTravelProgressProxy() throws SystemException {
	TravelProgressProxy travelProgressProxy = null;
	try {
	    travelProgressProxy = (TravelProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_TRAVEL_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return travelProgressProxy;
    }

}
