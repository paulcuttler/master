package com.ract.travel;

import com.ract.util.DateTime;
import com.ract.util.Interval;

public class TravelFile {
    private String travelAccountNumber;

    private DateTime bookingDate;

    public void setTravelAccountNumber(String travelAccountNumber) {
	this.travelAccountNumber = travelAccountNumber;
    }

    public void setBookingDate(DateTime bookingDate) {
	this.bookingDate = bookingDate;
    }

    public String getTravelAccountNumber() {
	return travelAccountNumber;
    }

    public DateTime getBookingDate() {
	return bookingDate;
    }

    public TravelFile(String travelAccountNumber, DateTime bookingDate) {
	setTravelAccountNumber(travelAccountNumber);
	setBookingDate(bookingDate);
    }

    public String toString() {
	return getTravelAccountNumber().toString();
    }

    /**
     * Determine if the booking data is within the the current period.
     * 
     * @param period
     *            Interval
     * @return boolean
     */
    public boolean isCurrent(Interval period) {
	// transaction
	DateTime now = new DateTime().getDateOnly();
	DateTime effectiveDate = now.subtract(period);
	if (this.getBookingDate().onOrAfterDay(effectiveDate)) {
	    return true;
	} else {
	    return false;
	}
    }
}
