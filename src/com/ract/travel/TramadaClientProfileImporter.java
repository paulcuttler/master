package com.ract.travel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.SystemParameterVO;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

/**
 * <p>
 * Allow an RACT client to be converted and transferred to the Tramada travel
 * system. Any errors returned will result in an exception.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class TramadaClientProfileImporter {
    public TramadaClientProfileImporter() {
	// no setup in constructor
    }

    public final static String TRAMADA_ACTION_ADD = "A"; // add a client
    public final static String TRAMADA_ACTION_UPDATE = "U"; // update a client
    public final static String TRAMADA_ACTION_DELETE = "D"; // add a termination
							    // date
    public final static String TRAMADA_ACTION_REINSTATE = "R"; // clear
							       // termination
							       // date and use
							       // "U" tramada
							       // action

    // Known elements
    final String ELEMENT_RESPONSE = "response";
    final String ELEMENT_ERROR = "error";
    final String ELEMENT_SUCCESS = "success";
    final String ELEMENT_ERROR_NUMBER = "errorno";
    final String ELEMENT_ERROR_DESCRIPTION = "errordesc";

    public void sendTramadaClientXML(String xml) throws GenericException {

	LogUtil.debug(this.getClass(), xml);

	try {

	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String username = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.PROXY_USERNAME);
	    String password = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.PROXY_PASSWORD);
	    String proxyHost = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.PROXY_HOST);
	    String proxyPort = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.PROXY_PORT);
	    String authHost = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.PROXY_AUTH_HOST);
	    String domain = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.PROXY_AUTH_DOMAIN);
	    String dir = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.TRAMADA_XMLDIR);
	    String url = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.TRAMADA_URL);
	    String event = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_TRAMADA, SystemParameterVO.TRAMADA_EVENT);

	    LogUtil.debug(this.getClass(), "username=" + username);
	    LogUtil.debug(this.getClass(), "password=" + password);
	    LogUtil.debug(this.getClass(), "proxyHost=" + proxyHost);
	    LogUtil.debug(this.getClass(), "proxyPort=" + proxyPort);
	    LogUtil.debug(this.getClass(), "authHost=" + authHost);
	    LogUtil.debug(this.getClass(), "domain=" + domain);
	    LogUtil.debug(this.getClass(), "dir=" + dir);
	    LogUtil.debug(this.getClass(), "url=" + url);
	    LogUtil.debug(this.getClass(), "event=" + event);

	    // set the host the proxy should create a connection to
	    //
	    // Note: By default port 80 will be used. Some proxies only allow
	    // conections
	    // to ports 443 and 8443. This is because the HTTP CONNECT method
	    // was intended
	    // to be used for tunneling HTTPS.
	    HttpClient httpclient = new HttpClient();

	    // JH 19/01/2009 Disabled proxy with Internet cutover
	    // if(url.indexOf("localhost") == -1)
	    // {
	    // httpclient.getHostConfiguration().setHost(url);
	    // httpclient.getHostConfiguration().setProxy(proxyHost,
	    // Integer.parseInt(proxyPort));
	    // // set the proxy credentials, only necessary for authenticating
	    // proxies
	    // httpclient.getState().setProxyCredentials(
	    // new AuthScope(proxyHost, Integer.parseInt(proxyPort), null),
	    // (UsernamePasswordCredentials)new NTCredentials(username,
	    // password, authHost,
	    // domain));
	    // }
	    PostMethod httppost = new PostMethod(url);

	    // construct file from xml string
	    File f = new File(getFileName(dir));
	    FileWriter fw = new FileWriter(f);
	    fw.write(xml);
	    fw.close();

	    // validate xml against schema
	    DOMParser parser = new DOMParser();
	    parser.setFeature("http://xml.org/sax/features/validation", true);
	    parser.setProperty("http://java.sun.com/xml/jaxp/properties/schemaLanguage", "http://www.w3.org/2001/XMLSchema");

	    // ???
	    // parser.setProperty(
	    // "http://apache.org/xml/properties/schema/external-noNamespaceSchemaLocation",
	    // "I:\\Projects\\Tramada\\Tramada Client Integration.xsd");

	    LogUtil.debug(this.getClass(), "Path = " + f.getAbsolutePath());

	    parser.parse(f.getAbsolutePath()); // test file

	    final String PARAMETER_EVENT = "E";
	    final String PARAMETER_FILE = "linkfile";

	    StringPart sp = new StringPart(PARAMETER_EVENT, event); // tramadapost

	    // The following setting of null and default parameters is necessary
	    // to
	    // interact with Tramada's Apache server.
	    sp.setTransferEncoding(null);
	    sp.setContentType(null);

	    FilePart fp = new FilePart(PARAMETER_FILE, f);
	    fp.setContentType("text/xml");
	    fp.setTransferEncoding(null);
	    fp.setCharSet(null);

	    Part[] parts = { sp, fp };
	    httppost.setRequestEntity(new MultipartRequestEntity(parts, httppost.getParams()));

	    try {
		httpclient.executeMethod(httppost);
		System.out.println("1");

		String xmlResponse = httppost.getResponseBodyAsString();

		if (xmlResponse == null || xmlResponse.length() == 0) {
		    throw new GenericException("No XML response was received.");
		} else {
		    // process and send xml response
		    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		    factory.setValidating(false);
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    LogUtil.debug(this.getClass(), xmlResponse);
		    // expect well formed xml with any illegal characters
		    // enclosed in CDATA sections
		    Document document = builder.parse(new InputSource(new StringReader(xmlResponse)));
		    Element documentElement = document.getDocumentElement(); // ie.

		    LogUtil.debug(this.getClass(), "document=" + documentElement.getNodeName());

		    if (documentElement.getNodeName().equals(ELEMENT_RESPONSE)) {
			// response type element
			Element responseElement = (Element) documentElement.getFirstChild();
			LogUtil.debug(this.getClass(), "responseElement=" + responseElement.getNodeName());
			if (responseElement.getNodeName().equals(ELEMENT_SUCCESS)) {
			    // Updated successfully
			    LogUtil.log(this.getClass(), "Transfer to Tramada successful.");
			} else if (responseElement.getNodeName().equals(ELEMENT_ERROR)) {

			    StringBuffer errors = extractErrorString(documentElement);
			    throw new GenericException(errors.toString());
			} else {
			    throw new GenericException("Unknown element type.");
			}
		    } else {
			// unexpected type of response
			throw new GenericException("Unknown response type. " + documentElement.getNodeName());
		    }
		}

	    } finally {
		httppost.releaseConnection();

		// cleanup temporary file
		f.delete();
	    }
	} catch (ParserConfigurationException ex) {
	    // XML parsing problems
	    throw new GenericException(ex);
	} catch (IOException ex) {
	    // File problems
	    throw new GenericException(ex);
	} catch (SAXNotSupportedException ex) {
	    // XML problems
	    throw new GenericException(ex);
	} catch (SAXNotRecognizedException ex) {
	    // XML problems
	    throw new GenericException(ex);
	} catch (SAXException ex) {
	    // XML problems
	    throw new GenericException(ex);
	}
    }

    private StringBuffer extractErrorString(Element documentElement) throws DOMException {
	StringBuffer errors = new StringBuffer();
	// assume multiple errors so go to root element and look for all error
	// elements
	NodeList errorList = documentElement.getChildNodes();
	Node itemNode = null;
	// for each error
	for (int x = 0; x < errorList.getLength(); x++) {
	    itemNode = errorList.item(x); // error element
	    LogUtil.debug(this.getClass(), "node=" + itemNode.getNodeName());
	    if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
		NodeList errorFields = itemNode.getChildNodes();
		for (int y = 0; y < errorFields.getLength(); y++) {
		    Node errorField = errorFields.item(y);
		    String nodeName = errorField.getNodeName();
		    String valueText = errorField.hasChildNodes() ? errorField.getFirstChild().getNodeValue() : null;
		    LogUtil.debug(this.getClass(), nodeName + ": " + valueText);
		    errors.append(nodeName + ": " + valueText);
		    if (y != errorFields.getLength()) {
			errors.append(FileUtil.NEW_LINE);
		    }
		}
	    }
	}
	return errors;
    }

    private String getFileName(String dir) {
	final String PREFIX = "cl";
	final String SUFFIX = ".xml";
	return dir + PREFIX + (new DateTime().getTime()) + SUFFIX;
    }

}
