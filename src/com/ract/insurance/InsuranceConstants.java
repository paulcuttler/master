package com.ract.insurance;

import com.ract.common.CommonConstants;

public class InsuranceConstants {

    public static String PAGE_UPLOAD_SUPPLIERS = CommonConstants.DIR_INSURANCE + "/UploadSupplierFile.jsp";

    public static String PAGE_SEARCH_SUPPLIERS = CommonConstants.DIR_INSURANCE + "/SearchSuppliers.jsp";

    public static String UIC_INSURANCE = "/InsuranceUIC";

}
