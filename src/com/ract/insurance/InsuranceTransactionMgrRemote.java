package com.ract.insurance;

import java.math.BigDecimal;

import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;

public interface InsuranceTransactionMgrRemote extends TransactionAdapter {
    /**
     * Creates a payment record in the insurance system for a claim recovery
     * payment.
     * 
     * @param Integer
     *            claimNo
     * @param BigDecimal
     *            amount
     * @param Integer
     *            receiptNo
     * @param String
     *            description (Payer name, or other detail)
     * @param String
     *            logonId
     * @param String
     *            salesBranch
     * @param String
     *            company (RACT, ISCU or AURORA)
     * @throws SystemException
     */

    public void notifyRecoveryPayment(Integer claimNo, BigDecimal amount, Integer receiptNo, String description, String logonId, String salesBranch, String company) throws SystemException;

    /**
     * Creates a payment records in the insurance system for a bulk claim
     * recovery payment.
     * 
     * @param BigDecimal
     *            amount
     * @param Integer
     *            receiptNo
     * @param String
     *            description (Payer name, or other detail)
     * @param String
     *            refNo (hopefully this will hold the filename of the associated
     *            data file)
     * @param String
     *            logonId
     * @param String
     *            salesBranch
     * @param String
     *            company (RACT, ISCU or AURORA)
     * @throws SystemException
     */

    public void notifyBulkRecoveryPayment(BigDecimal amount, Integer receiptNo, String agency, String chequeNo, String email, String fileName, String logonId, String salesBranch, String company) throws SystemException;

    public void notifyPremiumPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch, String companyName) throws SystemException;

}
