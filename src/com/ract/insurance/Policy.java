package com.ract.insurance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.util.DateTime;
import com.ract.util.FileUtil;

/**
 * Simple representation of an insurance policy.
 * 
 * @author jyh
 * @version 1.0
 */
public class Policy implements Serializable, Comparable {

    @Override
    public int compareTo(Object o) {
	if (o instanceof Policy) {
	    Policy p = (Policy) o;
	    return this.policyNumber.compareTo(p.getPolicyNumber());
	}
	return 0;
    }

    public final static String STATUS_CANCELLED = "Cancelled";
    public final static String STATUS_LAPSED = "Lapsed";
    public final static String STATUS_CURRENT = "Current";

    /**
	 * 
	 */
    private static final long serialVersionUID = -279137313610341603L;

    private Integer policyNumber;

    private DateTime expiryDate;

    private String policyStatus;

    private String productType;

    private String nameOnPolicy;

    private String riskSummary; // comma separated

    private Collection riskList = new ArrayList();

    private boolean directDebit;

    private void setPolicyNumber(Integer policyNumber) {
	this.policyNumber = policyNumber;
    }

    private void setExpiryDate(DateTime expiryDate) {
	this.expiryDate = expiryDate;
    }

    private void setPolicyStatus(String policyStatus) {
	this.policyStatus = policyStatus;
    }

    public Integer getPolicyNumber() {
	return policyNumber;
    }

    public DateTime getExpiryDate() {
	return expiryDate;
    }

    public String getPolicyStatus() {
	return policyStatus;
    }

    public Policy(Integer policyNumber, DateTime expiryDate, String policyStatus) {
	setPolicyNumber(policyNumber);
	setExpiryDate(expiryDate);
	setPolicyStatus(policyStatus);
    }

    public Policy(Integer policyNumber, String productType, DateTime expiryDate, String policyStatus, boolean directDebit, String nameOnPolicy, String riskSummary) {
	setPolicyNumber(policyNumber);
	setProductType(productType);
	setExpiryDate(expiryDate);
	setPolicyStatus(policyStatus);
	setDirectDebit(directDebit);
	setNameOnPolicy(nameOnPolicy);
	setRiskSummary(riskSummary);
	// need to add risk list through exposed methods
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((policyNumber == null) ? 0 : policyNumber.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Policy))
	    return false;
	Policy other = (Policy) obj;
	if (policyNumber == null) {
	    if (other.policyNumber != null)
		return false;
	} else if (!policyNumber.equals(other.policyNumber))
	    return false;
	return true;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.getPolicyNumber());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(this.getPolicyStatus());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(this.getNameOnPolicy());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(this.getProductType());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(this.getExpiryDate());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(this.getPolicyStatus());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(this.isDirectDebit());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(this.getRiskList());
	return desc.toString();
    }

    public boolean isCurrent() {
	return (this.getExpiryDate() == null ? false : this.getExpiryDate().compareTo(new DateTime()) > 0);
    }

    public String getProductType() {
	return productType;
    }

    public void setProductType(String productType) {
	this.productType = productType;
    }

    public String getNameOnPolicy() {
	return nameOnPolicy;
    }

    public void setNameOnPolicy(String nameOnPolicy) {
	this.nameOnPolicy = nameOnPolicy;
    }

    public String getRiskSummary() {
	return riskSummary;
    }

    public void setRiskSummary(String riskSummary) {
	this.riskSummary = riskSummary;
    }

    public Collection getRiskList() {
	return riskList;
    }

    public void setRiskList(Collection riskList) {
	this.riskList = riskList;
    }

    public void addRisks(Collection risks) {
	this.riskList.addAll(risks);
    }

    public void addRisk(Risk risk) {
	this.riskList.add(risk);
    }

    public boolean isDirectDebit() {
	return directDebit;
    }

    public void setDirectDebit(boolean directDebit) {
	this.directDebit = directDebit;
    }

}
