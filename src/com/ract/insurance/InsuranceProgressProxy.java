package com.ract.insurance;

import java.io.IOException;

import com.progress.open4gl.ConnectException;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;
import com.ract.common.CommonConstants;
import com.ract.common.GenericException;
import com.ract.common.SourceSystem;
import com.ract.common.proxy.ProgressProxy;
import com.ract.util.LogUtil;

public class InsuranceProgressProxy extends InsuranceProxy implements ProgressProxy {
    public InsuranceProgressProxy(String companyAbbreviation) throws ConnectException, Open4GLException, SystemErrorException, IOException {
	// connect to a different app server based on sourceSystem name. eg.
	// INS_RAC, INS_ISCU
	super(CommonConstants.getProgressAppserverURL(getCompanyName(companyAbbreviation)), CommonConstants.getProgressAppserverUser(), CommonConstants.getProgressAppserverPassword(), null);
    }

    /**
     * Static converter
     * 
     * @param companyNameAbbreviation
     *            String
     * @return String
     */
    private static String getCompanyName(String companyAbbreviation) {
	String companyName = null;
	try {
	    companyName = SourceSystem.getSourceSystem(companyAbbreviation).getSystemName();
	} catch (GenericException ex) {
	    LogUtil.warn("getCompanyName", "Unable to get company from company abbreviation '" + companyAbbreviation + "'.");
	    companyName = null;
	}
	return companyName;
    }

}
