package com.ract.insurance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.ract.util.DateTime;

/**
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 * 
 * 
 *          field rfDateSeq as int field tab-name as char field eff-date as date
 *          field key1 as char field key2 as char field key3 as int field fclass
 *          as char field fdesc as char field fValue as dec
 */

public class RFDetail implements Serializable {
    private int rfDateSeq;
    private String tabName;
    private DateTime effDate;
    private String key1;
    private String key2;
    private int key3;
    private String fClass;
    private String fDesc;
    private BigDecimal fValue;

    public RFDetail() {
    }

    public DateTime getEffDate() {
	return effDate;
    }

    public String getFClass() {
	return fClass;
    }

    public String getFDesc() {
	return fDesc;
    }

    public BigDecimal getFValue() {
	return fValue;
    }

    public String getKey1() {
	return key1;
    }

    public String getKey2() {
	return key2;
    }

    public int getKey3() {
	return key3;
    }

    public int getRfDateSeq() {
	return rfDateSeq;
    }

    public String getTabName() {
	return tabName;
    }

    public void setEffDate(DateTime effDate) {
	this.effDate = effDate;
    }

    public void setFClass(String fClass) {
	this.fClass = fClass;
    }

    public void setFDesc(String fDesc) {
	this.fDesc = fDesc;
    }

    public void setFValue(BigDecimal fValue) {
	DecimalFormat fm = new DecimalFormat("##,##0.0000");
	String formattedAmount = fm.format(fValue);
	this.fValue = new BigDecimal(formattedAmount);
    }

    public void setKey1(String key1) {
	this.key1 = key1;
    }

    public void setKey2(String key2) {
	this.key2 = key2;
    }

    public void setKey3(int key3) {
	this.key3 = key3;
    }

    public void setRfDateSeq(int rfDateSeq) {
	this.rfDateSeq = rfDateSeq;
    }

    public void setTabName(String tabName) {
	this.tabName = tabName;
    }

    /**
     * Create a new RFDetail instance which is an exact copy of this one.
     * 
     * @return RFDetail
     */
    public RFDetail cloneRFD() {
	RFDetail rfd = new RFDetail();
	rfd.rfDateSeq = this.rfDateSeq;
	rfd.tabName = this.tabName;
	rfd.effDate = new DateTime(this.effDate);
	rfd.key1 = this.key1;
	rfd.key2 = this.key2;
	rfd.key3 = this.key3;
	rfd.fClass = this.fClass;
	rfd.fDesc = this.fDesc;
	rfd.fValue = this.fValue;
	return rfd;
    }

    /**
     * Compare using rfDateSequence
     * 
     * @param rfd
     *            RFDetail
     * @return int
     */
    public int compareR(RFDetail rfd) {

	if (this.rfDateSeq < rfd.rfDateSeq)
	    return -1;
	else if (this.rfDateSeq > rfd.rfDateSeq)
	    return 1;
	if (this.key3 < rfd.key3)
	    return -1;
	else if (this.key3 > rfd.key3)
	    return 1;
	else
	    return 0;

    }

    /**
     * Compare using all fields
     * 
     * @param rfd
     *            RFDetail
     * @return int
     */
    public int compareT(RFDetail rfd) {
	int comp;
	comp = this.tabName.compareTo(rfd.tabName);
	if (comp != 0)
	    return comp;
	comp = this.key1.compareTo(rfd.key1);
	if (comp != 0)
	    return comp;
	comp = this.key2.compareTo(rfd.key2);
	if (comp != 0)
	    return comp;
	comp = this.effDate.compareTo(rfd.effDate);
	if (comp != 0)
	    return comp;
	if (this.key3 < rfd.key3)
	    comp = -1;
	else if (this.key3 > rfd.key3)
	    comp = 1;
	else
	    comp = 0;
	return comp;

    }

}
