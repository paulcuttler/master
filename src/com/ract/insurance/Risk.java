package com.ract.insurance;

import java.io.Serializable;

import com.ract.util.FileUtil;

public class Risk implements Serializable, Comparable {
    /**
	 * 
	 */
    private static final long serialVersionUID = 2535039371042319923L;

    @Override
    public int compareTo(Object o) {
	if (o instanceof Risk) {
	    Risk r = (Risk) o;
	    return this.riskNumber.compareTo(r.getRiskNumber());
	}
	return 0;
    }

    public Risk(Integer riskNumber, String riskType, String riskDescription) {
	setRiskNumber(riskNumber);
	setRiskType(riskType);
	setRiskDescription(riskDescription);
    }

    private Integer riskNumber;

    private String riskType;

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((riskNumber == null) ? 0 : riskNumber.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Risk))
	    return false;
	Risk other = (Risk) obj;
	if (riskNumber == null) {
	    if (other.riskNumber != null)
		return false;
	} else if (!riskNumber.equals(other.riskNumber))
	    return false;
	return true;
    }

    private String riskDescription;

    public Integer getRiskNumber() {
	return riskNumber;
    }

    public void setRiskNumber(Integer riskNumber) {
	this.riskNumber = riskNumber;
    }

    public String getRiskType() {
	return riskType;
    }

    public void setRiskType(String riskType) {
	this.riskType = riskType;
    }

    public String getRiskDescription() {
	return riskDescription;
    }

    public void setRiskDescription(String riskDescription) {
	this.riskDescription = riskDescription;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(getRiskNumber());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(getRiskType());
	desc.append(FileUtil.SEPARATOR_COMMA);
	desc.append(getRiskDescription());
	return desc.toString();
    }

}
