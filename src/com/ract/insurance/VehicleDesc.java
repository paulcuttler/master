package com.ract.insurance;

/**
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * @author not attributable
 * @version 1.0
 */
import java.io.Serializable;

import com.ract.util.DateTime;

public class VehicleDesc implements Serializable {
    private String make;
    private String model;
    private String type;
    private String series;
    private String year;
    private String release;
    private String family;
    private String variant;
    private String seriesName;
    private String capacity;
    private String cylinders;
    private String value;
    private String nvic;
    private String body;
    private String engine;
    private String transmission;
    private String fullYear;
    private String nominalCapacity;
    private boolean source = false;
    private int points;
    private int category;
    private DateTime effDate;

    public boolean getSource() {
	return this.source;
    }

    public void setSource(boolean s) {
	this.source = s;
    }

    public String getBody() {
	return body;
    }

    public String getCapacity() {
	return capacity;
    }

    public String getEngine() {
	return engine;
    }

    public String getCylinders() {
	return cylinders;
    }

    public String getMake() {
	return make;
    }

    public String getFamily() {
	return family;
    }

    public String getModel() {
	return model;
    }

    public String getNvic() {
	return nvic;
    }

    public String getRelease() {
	return release;
    }

    public String getSeries() {
	return series;
    }

    public String getSeriesName() {
	return seriesName;
    }

    public String getTransmission() {
	return transmission;
    }

    public String getType() {
	return type;
    }

    public String getValue() {
	return value;
    }

    public String getVariant() {
	return variant;
    }

    public String getYear() {
	return year;
    }

    public void setBody(String body) {
	this.body = body;
    }

    public void setCapacity(String capacity) {
	this.capacity = capacity;
    }

    public void setCylinders(String cylinders) {
	this.cylinders = cylinders;
    }

    public void setEngine(String engine) {
	this.engine = engine;
    }

    public void setFamily(String family) {
	this.family = family;
    }

    public void setMake(String make) {
	this.make = make;
    }

    public void setModel(String model) {
	this.model = model;
    }

    public void setNvic(String nvic) {
	this.nvic = nvic;
    }

    public void setRelease(String release) {
	this.release = release;
    }

    public void setSeries(String series) {
	this.series = series;
    }

    public void setSeriesName(String seriesName) {
	this.seriesName = seriesName;
    }

    public void setTransmission(String transmission) {
	this.transmission = transmission;
    }

    public void setType(String type) {
	this.type = type;
    }

    public void setValue(String value) {
	this.value = value;
    }

    public void setVariant(String variant) {
	this.variant = variant;
    }

    public void setYear(String year) {
	this.year = year;
    }

    public int getCategory() {
	return category;
    }

    public void setCategory(int category) {
	this.category = category;
    }

    public DateTime getEffDate() {
	return effDate;
    }

    public void setEffDate(DateTime effDate) {
	this.effDate = effDate;
    }

    public int getPoints() {
	return points;
    }

    public void setPoints(int points) {
	this.points = points;
    }

    public String toCsv() {
	String oStr;
	oStr = "*" + this.nvic.trim() + "*," + (this.source ? "RACT" : "GLASSES") + "," + this.make + "," + this.model + "," + this.body + "," + this.engine + "," + this.capacity + "," + this.cylinders + "," + this.transmission + "," + this.year + "," + this.release + "," + this.points + "," + this.value + "," + (this.effDate != null ? this.effDate.formatShortDate() : "") + "," + this.category + "," + this.fullYear + "," + this.nominalCapacity + "\n";

	return oStr;
    }

    public static String getCsvHeadings() {
	return "NVIC,Source,Make,Model,Body,Engine,Engine-size,Cyl,Trans-type," + "Year,Release,Points,Value,Effective-date,Category,Full-year,Nominal-Capacity";

    }

    public String getFullYear() {
	return fullYear;
    }

    public void setFullYear(String fullYear) {
	this.fullYear = fullYear;
    }

    public String getNominalCapacity() {
	return nominalCapacity;
    }

    public void setNominalCapacity(String nominalCapacity) {
	this.nominalCapacity = nominalCapacity;
    }
}
