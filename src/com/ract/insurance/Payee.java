package com.ract.insurance;

import java.io.Serializable;

/**
 * @hibernate.class table="ins_payee" lazy="false"
 */
public class Payee implements Serializable {
    private String payeeClass;

    private String payeeName;

    private String description;

    private String region;

    private String category;

    private String type;

    private String address;

    private String contactName;

    private String phone;

    private String fax;

    private String mobile;

    private String email;

    private String property;

    private String ABN;

    private String payeeCode;

    private String comment;

    private String partyCode;

    /**
     * @hibernate.property
     * @hibernate.column name="comment"
     */
    public String getComment() {
	return comment;
    }

    /**
     * @hibernate.id column="payee_code" generator-class="assigned"
     */
    public String getPayeeCode() {
	return payeeCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="abn"
     */
    public String getABN() {
	return ABN;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="property"
     */
    public String getProperty() {
	return property;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="email"
     */
    public String getEmail() {
	return email;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="mobile"
     */
    public String getMobile() {
	return mobile;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="fax"
     */
    public String getFax() {
	return fax;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="phone"
     */
    public String getPhone() {
	return phone;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="contact_name"
     */
    public String getContactName() {
	return contactName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="address"
     */
    public String getAddress() {
	return address;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="type"
     */
    public String getType() {
	return type;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="category"
     */
    public String getCategory() {
	return category;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="region"
     */
    public String getRegion() {
	return region;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="description"
     */
    public String getDescription() {
	return description;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="payee_name"
     */
    public String getPayeeName() {
	return payeeName;
    }

    public void setPayeeClass(String payeeClass) {
	this.payeeClass = payeeClass;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public void setPayeeCode(String payeeCode) {
	this.payeeCode = payeeCode;
    }

    public void setABN(String ABN) {
	this.ABN = ABN;
    }

    public void setProperty(String property) {
	this.property = property;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setMobile(String mobile) {
	this.mobile = mobile;
    }

    public void setFax(String fax) {
	this.fax = fax;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public void setContactName(String contactName) {
	this.contactName = contactName;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    public void setType(String type) {
	this.type = type;
    }

    public void setCategory(String category) {
	this.category = category;
    }

    public void setRegion(String region) {
	this.region = region;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public void setPayeeName(String payeeName) {
	this.payeeName = payeeName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="class"
     */
    public String getPayeeClass() {
	return payeeClass;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="party_code"
     */
    public String getPartyCode() {
	return partyCode;
    }

    public void setPartyCode(String partyCode) {
	this.partyCode = partyCode;
    }

}
