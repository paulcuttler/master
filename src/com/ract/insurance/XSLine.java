package com.ract.insurance;

public class XSLine {
    private Integer xsSeqNo;
    private String xsCode;
    private Integer displaySeq;
    private String lineText;
    private Integer lineAmount;

    public XSLine() {
    }

    public Integer getXsSeqNo() {
	return xsSeqNo;
    }

    public void setXsSeqNo(Integer xsSeqNo) {
	this.xsSeqNo = xsSeqNo;
    }

    public String getXsCode() {
	return xsCode;
    }

    public void setXsCode(String xsCode) {
	this.xsCode = xsCode;
    }

    public Integer getDisplaySeq() {
	return displaySeq;
    }

    public void setDisplaySeq(Integer displaySeq) {
	this.displaySeq = displaySeq;
    }

    public String getLineText() {
	return lineText;
    }

    public void setLineText(String lineText) {
	this.lineText = lineText;
    }

    public Integer getLineAmount() {
	return lineAmount;
    }

    public void setLineAmount(Integer lineAmount) {
	this.lineAmount = lineAmount;
    }

    public String toString() {
	return "xsSeqNo    = " + this.xsSeqNo + "\nxsCode     = " + this.xsCode + "\ndisplaySeq = " + this.displaySeq + "\nlineText   = " + this.lineText + "\nlineAmount = " + this.lineAmount;
    }

    public String toJSONArray() {
	String os = "[{\"title\":\"Sequence No\",\"value\":\"" + this.xsSeqNo + "\"}," + "{\"title\":\"Excess Code\",\"value\":\"" + this.xsCode + "\"}," + "{\"title\":\"Display Sequence\",\"value\":\"" + this.displaySeq + "\"}," + "{\"title\":\"Line text\",\"value\":\"" + this.lineText + "\"}," + " {\"title\":\"Line amount\",\"value\":\"" + this.lineAmount + "\"}]";
	return os;
    }

}
