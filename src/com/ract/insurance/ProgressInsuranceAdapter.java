package com.ract.insurance;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import com.progress.open4gl.BigDecimalHolder;
import com.progress.open4gl.BooleanHolder;
import com.progress.open4gl.DateHolder;
import com.progress.open4gl.InputResultSet;
import com.progress.open4gl.IntHolder;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class ProgressInsuranceAdapter extends TransactionAdapterType implements InsuranceAdapter {

    private String transType = null;
    private BigDecimal tAmount = null;
    private InsTxProxy insuranceTransactionProxy;

    private final static String SYSTEM_PROGRESS_INSURANCE = "ProgressInsurance";

    public String getSystemName() {
	LogUtil.debug(this.getClass(), "systemName=" + SYSTEM_PROGRESS_INSURANCE);
	return SYSTEM_PROGRESS_INSURANCE;
    }

    public ProgressInsuranceAdapter() {
    }

    /**
     * Read only.
     * 
     * @param clientNumber
     *            Integer
     * @return Policy[]
     */
    public ArrayList getPolicyList(Integer clientNumber, String companyName) throws SystemException {
	LogUtil.debug(this.getClass(), "getPolicyList");
	LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber);
	LogUtil.debug(this.getClass(), "companyName=" + companyName);

	// check for active policy
	ArrayList policyList = new ArrayList();

	// get policy list by client number
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder policyListHolder = new ResultSetHolder();
	int policyNumber;
	DateTime expiryDate = null;
	String policyStatus = null;
	Policy policy = null;
	int size = 0;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getPolicyList(clientNumber.intValue(), policyListHolder);
	    if (policyListHolder != null) {
		ResultSet policyListResultSet = policyListHolder.getResultSetValue();

		try {

		    while (policyListResultSet.next()) {
			policyNumber = policyListResultSet.getInt(1);
			expiryDate = new DateTime(policyListResultSet.getDate(2));
			policyStatus = policyListResultSet.getString(3);

			policy = new Policy(new Integer(policyNumber), expiryDate, policyStatus);

			policyList.add(policy);

			size++; // 0 based index
		    }

		    policyListResultSet.close();
		} catch (SQLException ex1) {
		    throw new SystemException(ex1);
		}
	    }

	} catch (Open4GLException ex) {
	    throw new SystemException("Unable to get policy list for client '" + clientNumber + "'.", ex);
	} finally {
	    releaseInsuranceProgressProxy(insuranceProgressProxy);
	}

	return policyList;
    }

    public void commit() throws SystemException {
	LogUtil.debug(this.getClass(), "commit()");
	try {
	    if (this.insuranceTransactionProxy != null) {
		this.insuranceTransactionProxy.commit();
	    }
	} catch (IllegalStateException ise) {
	    // no transaction pending. Do nothing
	    ise.printStackTrace();
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	} finally {
	    LogUtil.debug(this.getClass(), "commit release()");
	    release();
	}
    }

    public void rollback() throws SystemException {
	LogUtil.debug(this.getClass(), "rollback");
	try {
	    if (this.insuranceTransactionProxy != null) {
		this.insuranceTransactionProxy.rollback();
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	} finally {
	    LogUtil.debug(this.getClass(), "rollback release()");
	    release();
	}
    }

    public synchronized void release() {
	LogUtil.debug(this.getClass(), "RELEASE " + this.transType + " " + this.tAmount);
	releaseInsuranceTransactionProxy();
	// release proxy connection controlling transaction
	releaseInsuranceProgressProxy(this.transactionInsuranceProgressProxy);
    }

    private void releaseInsuranceTransactionProxy() {
	LogUtil.debug(this.getClass(), "releaseInsuranceTransactionProxy 1");
	try {
	    if (this.insuranceTransactionProxy != null) {
		this.insuranceTransactionProxy._release();
	    }
	} catch (Open4GLException gle) {
	    LogUtil.fatal(this.getClass(), gle.getMessage());
	} finally {
	    this.insuranceTransactionProxy = null;
	}
	LogUtil.debug(this.getClass(), "releaseInsuranceTransactionProxy 2");
    }

    public void notifyRecoveryPayment(Integer claimNo, BigDecimal amount, Integer receiptNo, String description, String logonId, String salesBranch, String companyName) throws SystemException {
	LogUtil.debug(this.getClass(), "notifyRecoveryPayment");
	LogUtil.debug(this.getClass(), "claimNo=" + claimNo);
	LogUtil.debug(this.getClass(), "amount=" + amount);
	LogUtil.debug(this.getClass(), "receiptNo=" + receiptNo);
	LogUtil.debug(this.getClass(), "description=" + description);
	LogUtil.debug(this.getClass(), "logonId=" + logonId);
	LogUtil.debug(this.getClass(), "salesBranch=" + salesBranch);
	LogUtil.debug(this.getClass(), "companyName=" + companyName);
	try {
	    getInsuranceTransactionProxy(companyName);
	    this.insuranceTransactionProxy.createRecovery(claimNo.intValue(), amount, description, receiptNo.intValue(), logonId, salesBranch);
	    handleProxyReturnString();
	} catch (Open4GLException ex) {
	    handleProxyReturnString();
	    throw new SystemException("Unable to create recovery payment in Insurance system.", ex);
	}
    }

    private void handleProxyReturnString() throws SystemException {
	LogUtil.debug(this.getClass(), "handleProxyReturnString");
	try {
	    if (transactionInsuranceProgressProxy != null) {
		String returnString = transactionInsuranceProgressProxy._getProcReturnString();
		LogUtil.debug(this.getClass(), "returnString=" + returnString);
		if (returnString != null && returnString.length() > 0) {
		    throw new SystemException(returnString);
		}
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	}
    }

    public void notifyPremiumPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch, String companyName) throws SystemException {
	this.transType = "NOTIFY PAYMENT";
	this.tAmount = amount;
	try {
	    LogUtil.debug(this.getClass(), "notifyPremiumPayment getInsuranceTransactionProxy");
	    LogUtil.debug(this.getClass(), "receiptNo=" + receiptNo);
	    LogUtil.debug(this.getClass(), "clientNo " + clientNo);
	    LogUtil.debug(this.getClass(), "seqNo=" + seqNo);
	    LogUtil.debug(this.getClass(), "amount=" + amount);
	    LogUtil.debug(this.getClass(), "logonId=" + logonId);
	    LogUtil.debug(this.getClass(), "salesBranch=" + salesBranch);
	    LogUtil.debug(this.getClass(), "companyName=" + companyName);

	    this.insuranceTransactionProxy = getInsuranceTransactionProxy(companyName);
	    this.insuranceTransactionProxy.notifyPayment(receiptNo.intValue(), clientNo.intValue(), seqNo.intValue(), amount, logonId, salesBranch);
	    handleProxyReturnString();
	} catch (Open4GLException ex) {
	    handleProxyReturnString();
	    throw new SystemException("Unable to notify Insurance premium payment.", ex);
	}

    }

    private InsuranceProgressProxy getInsuranceProgressProxy(String companyName) // String
										 // companyName
	    throws SystemException {
	LogUtil.debug(this.getClass(), "getInsuranceProgressProxy");
	LogUtil.debug(this.getClass(), "companyName=" + companyName);
	// company name should only be INS_RAC
	if (companyName == null || !companyName.equals(SourceSystem.INSURANCE_RACT.getAbbreviation())) {
	    throw new SystemException("Source system '" + companyName + "' is not valid.");
	}
	this.setCompanyName(companyName);
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    insuranceProgressProxy = (InsuranceProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(companyName);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return insuranceProgressProxy;
    }

    private void releaseInsuranceProgressProxy(InsuranceProgressProxy insuranceProgressProxy) {
	try {
	    LogUtil.debug(this.getClass(), "releaseInsuranceProgressProxy");
	    String companyName = this.getCompanyName();
	    LogUtil.debug(this.getClass(), "companyName=" + companyName);

	    if (companyName != null && (companyName.equals("") || !companyName.equals(SourceSystem.INSURANCE_RACT.getAbbreviation()))) {
		throw new IllegalArgumentException("Company name must be RACT Insurance only. Company name was '" + companyName + "'.");
	    }
	    LogUtil.debug(this.getClass(), "companyName=" + companyName + ",insuranceProgressProxy=" + (insuranceProgressProxy == null));
	    ProgressProxyObjectPool.getInstance().returnObject(companyName, insuranceProgressProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), "Error releasing insurance progress proxy. Exception:" + ex.getMessage());
	} finally {
	    insuranceProgressProxy = null;
	    // setting the company name to null causes some issues as release
	    // must be called multiple times
	    // this.companyName = null;
	}
    }

    public boolean validateClaimNumber(Integer claimNumber, String companyName) throws SystemException {
	LogUtil.debug(this.getClass(), "validateClaimNumber");
	LogUtil.debug(this.getClass(), "claimNumber=" + claimNumber);
	LogUtil.debug(this.getClass(), "companyName=" + companyName);

	BooleanHolder isValid = new BooleanHolder();
	isValid.setBooleanValue(false);
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.verifyClaim(claimNumber.intValue(), isValid);

	} catch (Open4GLException ex) {
	    throw new SystemException("Unable to verify claim number.", ex);
	} finally {
	    releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return isValid.getBooleanValue();
    }

    private InsuranceProgressProxy transactionInsuranceProgressProxy;

    private InsTxProxy getInsuranceTransactionProxy(String companyName) throws SystemException {
	LogUtil.debug(this.getClass(), "\ngetInsuranceTransactionProxy: " + "\nthis:" + this + "\ntransactionInsuranceProgressProxy: " + this.transactionInsuranceProgressProxy + "\ninsuranceTransactionProxy        : " + this.insuranceTransactionProxy);
	LogUtil.debug(this.getClass(), "companyName=" + companyName);

	// need to maintain a table of insurance progress transaction proxies
	// otherwise
	// we will run into Progress restriction of running one AUTOMATIC.p at a
	// time.
	if (this.insuranceTransactionProxy == null) {
	    try {
		this.transactionInsuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
		LogUtil.debug(this.getClass(), "getInsuranceTransactionProxy 222222: " + "\ntransactionInsuranceProgressProxy: " + this.transactionInsuranceProgressProxy + "\ninsuranceTransactionProxy        : " + this.insuranceTransactionProxy);
		this.insuranceTransactionProxy = transactionInsuranceProgressProxy.createPO_InsTxProxy();
		LogUtil.debug(this.getClass(), "getInsuranceTransactionProxy 222222: " + "\ntransactionInsuranceProgressProxy: " + this.transactionInsuranceProgressProxy + "\ninsuranceTransactionProxy        : " + this.insuranceTransactionProxy);
	    } catch (Open4GLException ex) {
		ex.printStackTrace();
		throw new SystemException("Unable to get InsuranceTxProxy: " + ex);
	    }
	}
	return insuranceTransactionProxy;
    }

    private String companyName;

    public String getCompanyName() {
	return this.companyName;
    }

    private synchronized void setCompanyName(String companyName) throws SystemException {
	LogUtil.debug(this.getClass(), "setCompanyName");
	LogUtil.debug(this.getClass(), "companyName=" + companyName);
	// ensure the company name cannot be changed per instance.
	if (this.getCompanyName() != null && !this.companyName.equals(companyName)) {
	    throw new SystemException("A transaction proxy may only be associated with one company at a time. " + companyName + "/" + this.getCompanyName());
	} else {
	    this.companyName = companyName;
	}
    }

    public BigDecimal getAmountOutstandingOnPolicy(Integer clientNo, Integer seqNo, String companyName) throws SystemException {
	LogUtil.debug(this.getClass(), "getAmountOutstandingOnPolicy");
	LogUtil.debug(this.getClass(), "clientNo=" + clientNo);
	LogUtil.debug(this.getClass(), "seqNo=" + seqNo);
	LogUtil.debug(this.getClass(), "companyName=" + companyName);
	BigDecimalHolder amountHolder = new BigDecimalHolder();
	BigDecimal amountOutstanding = null;
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getAmountOnPolicy(clientNo.intValue(), seqNo.intValue(), amountHolder);
	    amountOutstanding = amountHolder.getBigDecimalValue();
	    LogUtil.debug(this.getClass(), "amountOutstanding=" + amountOutstanding);
	} catch (Exception e) {
	    throw new SystemException(e);
	} finally {
	    releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return amountOutstanding;
    }

    public ArrayList getRFDates(String companyName) throws SystemException {
	ArrayList dateList = null;
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rshDateList = new ResultSetHolder();
	ResultSet rs = null;
	RFDate oneDate = null;
	java.sql.Date tDate = null;
	// this.companyName = companyName;
	try {
	    dateList = new ArrayList();
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getRFDates(rshDateList);
	    rs = rshDateList.getResultSetValue();
	    while (rs.next()) {
		oneDate = new RFDate();
		oneDate.setTabName(rs.getString(1));
		tDate = rs.getDate(2);
		if (tDate != null) {
		    oneDate.setEffDate(new DateTime(tDate));
		}
		oneDate.setKey1(rs.getString(3));
		oneDate.setKey2(rs.getString(4));
		tDate = rs.getDate(5);
		if (tDate != null) {
		    oneDate.setCreateDate(new DateTime(tDate));
		}
		oneDate.setCreateId(rs.getString(6));
		oneDate.setRfDateSeq(new Integer(rs.getInt(7)));
		dateList.add((Object) oneDate);
	    }
	} catch (Exception e) {
	    System.out.println("error in getRFDates....");
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return dateList;

    }

    /**
     * Gets the entire table of in-rf-det. It shouldn't ever be very large
     * 
     * @throws SystemException
     * @return ArrayList
     */

    /*
     * rfDateSeq; tabName; effDate; key1; key2; key3; fclass; fdesc; fValue;
     */
    public ArrayList getRFDetails(String companyName) throws SystemException {
	ArrayList detailList = null;
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rshdetailList = new ResultSetHolder();
	ResultSet rs = null;
	RFDetail oneDetail = null;
	java.sql.Date tDate = null;
	// this.companyName = companyName;
	try {
	    detailList = new ArrayList();
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getRFDetails(rshdetailList);
	    rs = rshdetailList.getResultSetValue();
	    while (rs.next()) {
		oneDetail = new RFDetail();
		oneDetail.setRfDateSeq(rs.getInt(1));
		oneDetail.setTabName(rs.getString(2));
		tDate = rs.getDate(3);
		if (tDate != null) {
		    oneDetail.setEffDate(new DateTime(tDate));
		}
		oneDetail.setKey1(rs.getString(4));
		oneDetail.setKey2(rs.getString(5));
		oneDetail.setKey3(rs.getInt(6));
		oneDetail.setFClass(rs.getString(7));
		oneDetail.setFDesc(rs.getString(8));
		oneDetail.setFValue(new BigDecimal(rs.getDouble(9)));
		detailList.add((Object) oneDetail);
	    }
	} catch (Exception e) {
	    System.out.println("error in getRFDates....");
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return detailList;
    }

    public void setRFData(String companyName, ArrayList datesList, ArrayList detailsList) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    RFDateHolder rsDates = new RFDateHolder(datesList);
	    RFDetailsHolder rsDetails = new RFDetailsHolder(detailsList);
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.setRFData(rsDates, rsDetails);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}

    }

    /**
     * Private classes needed to pass data back to progress methods for each
     * table valued parameter
     */
    private class RFDateHolder extends InputResultSet {
	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public void updateNClob(int columnIndex, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	ArrayList datesList = null;
	RFDate currentRec = null;
	int seqNo;

	public RFDateHolder(ArrayList datesList) {
	    this.datesList = datesList;
	    seqNo = -1;
	}

	public boolean next() {
	    seqNo++;
	    if (seqNo >= datesList.size())
		return false;
	    else {
		currentRec = (RFDate) datesList.get(seqNo);
		return true;
	    }
	}

	public Object getObject(int columnNum) {
	    Object obj = null;
	    switch (columnNum) {
	    case 1:
		obj = (Object) currentRec.getTabName();
		break;
	    case 2:
		obj = currentRec.getEffDate().toSQLDate();
		break;
	    case 3:
		obj = (Object) currentRec.getKey1();
		break;
	    case 4:
		obj = (Object) currentRec.getKey2();
		break;
	    case 5:
		obj = null; // (Object)(new DateTime()).formatShortDate();
		break;
	    case 6:
		obj = (Object) currentRec.getCreateId();
		break;
	    case 7:
		obj = (Object) currentRec.getRfDateSeq();
		break;
	    }
	    return obj;
	}

	public URL getURL(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public URL getURL(String columnName) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public void updateArray(int columnIndex, Array x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateArray(String columnName, Array x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, Blob x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnName, Blob x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Clob x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnName, Clob x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public int getHoldability() throws SQLException {
	    // TODO Auto-generated method stub
	    return 0;
	}

	public Reader getNCharacterStream(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public Reader getNCharacterStream(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public boolean isClosed() throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(int columnIndex, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(String columnLabel, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateRef(int columnIndex, Ref x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateRef(String columnName, Ref x) throws SQLException {
	    // TODO Auto-generated method stub
	}

    }

    private class RFDetailsHolder extends InputResultSet {
	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public void updateNClob(int columnIndex, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	ArrayList detailsList = null;
	RFDetail currentRec = null;

	public int getHoldability() throws SQLException {
	    // TODO Auto-generated method stub
	    return 0;
	}

	public Reader getNCharacterStream(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public Reader getNCharacterStream(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public boolean isClosed() throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(int columnIndex, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(String columnLabel, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	int seqNo;

	public RFDetailsHolder(ArrayList detailsList) {
	    this.detailsList = detailsList;
	    seqNo = -1;
	}

	public boolean next() {
	    seqNo++;
	    if (seqNo >= detailsList.size())
		return false;
	    else {
		currentRec = (RFDetail) detailsList.get(seqNo);
		return true;
	    }
	}

	public Object getObject(int columnNum) {
	    Object obj = null;
	    switch (columnNum) {
	    case 1:
		obj = new Integer(currentRec.getRfDateSeq());
		break;
	    case 2:
		obj = (Object) currentRec.getTabName();
		break;
	    case 3:
		obj = currentRec.getEffDate().toSQLDate();
		break;
	    case 4:
		obj = (Object) currentRec.getKey1();
		break;
	    case 5:
		obj = (Object) currentRec.getKey2();
		break;
	    case 6:
		obj = (Object) (new Integer(currentRec.getKey3()));
		break;
	    case 7:
		obj = (Object) currentRec.getFClass();
		break;
	    case 8:
		obj = (Object) currentRec.getFDesc();
		break;
	    case 9:
		obj = (Object) currentRec.getFValue();
		break;
	    }
	    return obj;
	}

	public java.net.URL getURL(int n) {
	    return null;
	}

	public void updateArray(int n, Array array) {

	}

	public void updateBlob(int columnIndex, Blob x) {
	}

	public void updateClob(int columnIndex, Clob x) {
	}

	public void updateRef(int columnIndex, Ref x) {
	}

	public URL getURL(String columnName) {
	    return null;
	}

	public void updateArray(String columnName, Array x) {
	}

	public void updateBlob(String columnName, Blob x) {
	}

	public void updateClob(String columnName, Clob x) {
	}

	public void updateRef(String columnName, Ref x) {
	}

	public void updateAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateAsciiStream(arg0, arg1, arg2);
	}

	public void updateAsciiStream(String arg0, InputStream arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateAsciiStream(arg0, arg1, arg2);
	}

	public void updateBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBigDecimal(arg0, arg1);
	}

	public void updateBigDecimal(String arg0, BigDecimal arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBigDecimal(arg0, arg1);
	}

	public void updateBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBinaryStream(arg0, arg1, arg2);
	}

	public void updateBinaryStream(String arg0, InputStream arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBinaryStream(arg0, arg1, arg2);
	}

	public void updateBoolean(int arg0, boolean arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBoolean(arg0, arg1);
	}

	public void updateBoolean(String arg0, boolean arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBoolean(arg0, arg1);
	}

	public void updateByte(int arg0, byte arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateByte(arg0, arg1);
	}

	public void updateByte(String arg0, byte arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateByte(arg0, arg1);
	}

	public void updateBytes(int arg0, byte[] arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBytes(arg0, arg1);
	}

	public void updateBytes(String arg0, byte[] arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateBytes(arg0, arg1);
	}

	public void updateCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateCharacterStream(arg0, arg1, arg2);
	}

	public void updateCharacterStream(String arg0, Reader arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateCharacterStream(arg0, arg1, arg2);
	}

	public void updateDate(int arg0, Date arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateDate(arg0, arg1);
	}

	public void updateDate(String arg0, Date arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateDate(arg0, arg1);
	}

	public void updateDouble(int arg0, double arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateDouble(arg0, arg1);
	}

	public void updateDouble(String arg0, double arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateDouble(arg0, arg1);
	}

	public void updateFloat(int arg0, float arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateFloat(arg0, arg1);
	}

	public void updateFloat(String arg0, float arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateFloat(arg0, arg1);
	}

	public void updateInt(int arg0, int arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateInt(arg0, arg1);
	}

	public void updateInt(String arg0, int arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateInt(arg0, arg1);
	}

	public void updateLong(int arg0, long arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateLong(arg0, arg1);
	}

	public void updateLong(String arg0, long arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateLong(arg0, arg1);
	}

	public void updateNull(int arg0) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateNull(arg0);
	}

	public void updateNull(String arg0) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateNull(arg0);
	}

	public void updateObject(int arg0, Object arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateObject(arg0, arg1, arg2);
	}

	public void updateObject(int arg0, Object arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateObject(arg0, arg1);
	}

	public void updateObject(String arg0, Object arg1, int arg2) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateObject(arg0, arg1, arg2);
	}

	public void updateObject(String arg0, Object arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateObject(arg0, arg1);
	}

	public void updateShort(int arg0, short arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateShort(arg0, arg1);
	}

	public void updateShort(String arg0, short arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateShort(arg0, arg1);
	}

	public void updateString(int arg0, String arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateString(arg0, arg1);
	}

	public void updateString(String arg0, String arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateString(arg0, arg1);
	}

	public void updateTime(int arg0, Time arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateTime(arg0, arg1);
	}

	public void updateTime(String arg0, Time arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateTime(arg0, arg1);
	}

	public void updateTimestamp(int arg0, Timestamp arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateTimestamp(arg0, arg1);
	}

	public void updateTimestamp(String arg0, Timestamp arg1) throws SQLException {
	    // TODO Auto-generated method stub
	    super.updateTimestamp(arg0, arg1);
	}

    }

    public ArrayList getMessageText(String companyName) throws SystemException {
	ArrayList textList = null;
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rshDateList = new ResultSetHolder();
	ResultSet rs = null;
	MessageText mt = null;
	java.sql.Date tDate = null;
	// this.companyName = companyName;
	try {
	    textList = new ArrayList();
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getGnTextTable(rshDateList);
	    rs = rshDateList.getResultSetValue();
	    while (rs.next()) {
		mt = new MessageText();

		tDate = rs.getDate(1);
		if (tDate != null) {
		    mt.setCreateDate(new DateTime(tDate));
		}
		mt.setCreateId(rs.getString(2));
		tDate = rs.getDate(3);
		if (tDate != null) {
		    mt.setDeleteDate(new DateTime(tDate));
		}
		mt.setDeleteId(rs.getString(4));
		mt.setHeading(rs.getString(5));
		mt.setLocation(rs.getString(6));
		mt.setMessageText(rs.getString(7));
		mt.setNotes(rs.getString(8));
		mt.setProdType(rs.getString(9));
		tDate = rs.getDate(10);
		if (tDate != null) {
		    mt.setStartDate(new DateTime(tDate));
		}
		mt.setSubSystem(rs.getString(11));
		mt.setSubType(rs.getString(12));
		mt.setTransType(rs.getString(13));
		mt.setSameAs(rs.getString(14));
		mt.setRecordStatus(rs.getString(15));
		textList.add(mt);
		// for (int x=1; x<16;x++)
		// {
		// Object obj = rs.getObject(x);
		// System.out.println(x + " " + obj.getClass() + "  >>--->  " +
		// obj.toString());
		// }
	    }
	} catch (Exception e) {
	    System.out.println("error in getMessageText....");
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return textList;

    }

    public void setMessageText(String companyName, ArrayList mtList) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    MessageTextHolder mtHolder = new MessageTextHolder(mtList);

	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.writeGnText(mtHolder);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}

    }

    /* holder class for setMessageText */
    private class MessageTextHolder extends InputResultSet {

	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public void updateNClob(int columnIndex, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public int getHoldability() throws SQLException {
	    // TODO Auto-generated method stub
	    return 0;
	}

	public Reader getNCharacterStream(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public Reader getNCharacterStream(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public boolean isClosed() throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(int columnIndex, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(String columnLabel, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	ArrayList mtList = null;
	int seqNum;
	MessageText mt = null;

	public MessageTextHolder(ArrayList mtList) {
	    this.mtList = mtList;
	    seqNum = -1;
	}

	public Object getObject(int columnNo) {
	    Object col = null;
	    java.sql.Date dummyDate = null;
	    try {
		DateTime tDate = new DateTime("01/01/9999");
		dummyDate = tDate.toSQLDate();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    switch (columnNo) {
	    case 1:
		if (mt.getCreateDate() != null)
		    col = mt.getCreateDate().toSQLDate();
		else
		    col = dummyDate;
		break;
	    case 2:
		col = mt.getCreateId();
		break;
	    case 3:
		if (mt.getDeleteDate() != null)
		    col = mt.getDeleteDate().toSQLDate();
		else
		    col = dummyDate;
		break;
	    case 4:
		if (mt.getDeleteId() != null)
		    col = mt.getDeleteId();
		else
		    col = "";
		break;
	    case 5:
		if (mt.getHeading() != null)
		    col = mt.getHeading();
		else
		    col = "";
		break;
	    case 6:
		col = mt.getLocation();
		break;
	    case 7:
		col = mt.getMessageText();
		break;
	    case 8:
		if (mt.getNotes() != null)
		    col = mt.getNotes();
		else
		    col = "";
		break;
	    case 9:
		col = mt.getProdType();
		break;
	    case 10:
		if (mt.getStartDate() != null)
		    col = mt.getStartDate().toSQLDate();
		else
		    col = dummyDate;
		break;
	    case 11:
		col = mt.getSubSystem();
		break;
	    case 12:
		if (mt.getSubType() != null)
		    col = mt.getSubType();
		else
		    col = "";
		break;
	    case 13:
		col = mt.getTransType();
		break;
	    case 14:
		if (mt.getSameAs() != null)
		    col = mt.getSameAs();
		else
		    col = "";
		break;
	    case 15:
		if (mt.getRecordStatus() != null)
		    col = mt.getRecordStatus();
		else
		    col = "";
		break;
	    }
	    return col;
	}

	public boolean next() {
	    seqNum++;
	    if (seqNum >= mtList.size())
		return false;
	    mt = (MessageText) mtList.get(seqNum);
	    return true;
	}

	public URL getURL(int columnIndex) {
	    return null;
	}

	public void updateArray(int columnIndex, Array x) {
	}

	public void updateBlob(int columnIndex, Blob x) {
	}

	public void updateClob(int columnIndex, Clob x) {
	}

	public void updateRef(int columnIndex, Ref x) {
	}

	public URL getURL(String columnName) {
	    return null;
	}

	public void updateArray(String columnName, Array x) {
	}

	public void updateBlob(String columnName, Blob x) {
	}

	public void updateClob(String columnName, Clob x) {
	}

	public void updateRef(String columnName, Ref x) {
	}

    }

    public ArrayList getSuburbList(String companyName, String extType) throws SystemException {
	ArrayList suburbList = null;
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rshSuburbList = new ResultSetHolder();
	ResultSet rs = null;
	InsSuburb is = null;
	java.sql.Date tDate = null;
	java.sql.Date dt = null;
	// this.companyName = companyName;
	try {
	    suburbList = new ArrayList();
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getsuburb(extType, rshSuburbList);
	    rs = rshSuburbList.getResultSetValue();

	    while (rs.next()) {
		is = new InsSuburb();
		is.setSuburbName(rs.getString(1));
		dt = rs.getDate(2);
		if (dt != null)
		    is.setEffDate(new DateTime(dt));
		else
		    is.setEffDate(null);
		is.setBldgAmt(new BigDecimal(rs.getDouble(3)));
		is.setBldgPercent(new BigDecimal(rs.getDouble(4)));
		is.setCntsAmt(new BigDecimal(rs.getDouble(5)));
		is.setCntsPercent(new BigDecimal(rs.getDouble(6)));
		is.setCarAmt(new BigDecimal(rs.getDouble(7)));
		is.setCarPercent(new BigDecimal(rs.getDouble(8)));
		suburbList.add(is);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return suburbList;
    }

    public void setSuburbList(String companyName, ArrayList sl) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    InsSuburbHolder ish = new InsSuburbHolder(sl);
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    StringHolder returnMessageHolder = new StringHolder();
	    insuranceProgressProxy.putsuburb(ish, returnMessageHolder);
	    String returnMessage = returnMessageHolder.getStringValue();
	    System.out.println(returnMessage);
	    if (returnMessage != null && returnMessage.indexOf("Success") == -1) {
		throw new SystemException("Error setting suburbs: " + returnMessage);
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
    }

    private class InsSuburbHolder extends InputResultSet {

	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public void updateNClob(int columnIndex, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public int getHoldability() throws SQLException {
	    // TODO Auto-generated method stub
	    return 0;
	}

	public Reader getNCharacterStream(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public Reader getNCharacterStream(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public boolean isClosed() throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(int columnIndex, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(String columnLabel, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	ArrayList suburbList;
	InsSuburb is;
	int index;

	public InsSuburbHolder(ArrayList sL) {
	    this.suburbList = sL;
	    index = -1;
	}

	public Object getObject(int int0) {
	    Object ob = null;
	    switch (int0) {
	    case 1:
		ob = is.getSuburbName();
		break;
	    case 2:
		ob = is.getEffDate().toSQLDate();
		break;
	    case 3:
		ob = is.getBldgAmt();
		break;
	    case 4:
		ob = is.getBldgPercent();
		break;
	    case 5:
		ob = is.getCntsAmt();
		break;
	    case 6:
		ob = is.getCntsPercent();
		break;
	    case 7:
		ob = is.getCarAmt();
		break;
	    case 8:
		ob = is.getCarPercent();
		break;
	    }
	    return ob;
	}

	/**
	 * next
	 * 
	 * @return boolean
	 */
	public boolean next() {
	    index++;
	    if (index >= suburbList.size())
		return false;
	    else {
		is = (InsSuburb) suburbList.get(index);
		return true;
	    }
	}

	public URL getURL(int columnIndex) {
	    return null;
	}

	public void updateArray(int columnIndex, Array x) {
	}

	public void updateBlob(int columnIndex, Blob x) {
	}

	public void updateClob(int columnIndex, Clob x) {
	}

	public void updateRef(int columnIndex, Ref x) {
	}

	public URL getURL(String columnName) {
	    return null;
	}

	public void updateArray(String columnName, Array x) {
	}

	public void updateBlob(String columnName, Blob x) {
	}

	public void updateClob(String columnName, Clob x) {
	}

	public void updateRef(String columnName, Ref x) {
	}

    }

    public String setGLVehicle(String companyName, VehicleDesc veh) throws SystemException {
	String returnMessage = null;
	// InsuranceProgressProxy insuranceProgressProxy = null;
	// try
	// {
	// insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	// StringHolder returnMessageHolder = new StringHolder();
	// insuranceProgressProxy.setGlVehicle(veh.getMake(),
	// veh.getModel(),
	// veh.getType(),
	// veh.getSeries(),
	// veh.getYear(),
	// veh.getRelease(),
	// veh.getFamily(),
	// veh.getVariant(),
	// veh.getSeriesName(),
	// veh.getCapacity(),
	// veh.getCylinders(),
	// veh.getValue(),
	// veh.getNvic(),
	// veh.getBody(),
	// veh.getEngine(),
	// veh.getTransmission(),
	// veh.getFullYear(),
	// veh.getNominalCapacity(),
	//
	// require other parameters
	//
	// returnMessageHolder);
	// returnMessage = returnMessageHolder.getStringValue();
	// }
	// catch(com.progress.open4gl.ConnectFailedException cfe)
	// {
	// cfe.printStackTrace();
	// throw new SystemException(cfe + "");
	// }
	// catch(Exception e)
	// {
	// e.printStackTrace();
	// throw new SystemException(e);
	// }
	// finally
	// {
	// this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	// }
	if (returnMessage == null) {
	    throw new SystemException("Not implemented!");
	}
	return returnMessage;
    }

    public String setGLCode(String companyName, String tableName, String code, String value) throws SystemException {
	String returnMessage = null;
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    StringHolder returnMessageHolder = new StringHolder();
	    insuranceProgressProxy.setGlCode(tableName, code, value, returnMessageHolder);
	    returnMessage = returnMessageHolder.getStringValue();
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}

	return returnMessage;
    }

    public String setInvalidVehicle(String companyName, String action, String nvic, String userId) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	String returnMessage = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    StringHolder returnMessageHolder = new StringHolder();
	    insuranceProgressProxy.setInvalidGl(action, nvic, userId, returnMessageHolder);
	    returnMessage = returnMessageHolder.getStringValue();
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}

	return returnMessage;
    }

    public String getVehicleDescription(String companyName, String nvic, boolean source) throws SystemException {
	String desc;
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    StringHolder returnMessageHolder = new StringHolder();
	    insuranceProgressProxy.getVehicleFullDesc(nvic, source, returnMessageHolder);
	    desc = returnMessageHolder.getStringValue();
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}

	return desc;
    }

    public ArrayList getVehicleList(String companyName, String type) throws SystemException {
	ArrayList vehicleList = null;
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rshVehicleList = new ResultSetHolder();
	ResultSet rs = null;
	VehicleDesc vehicle = null;
	java.sql.Date tDate = null;
	java.sql.Date dt = null;
	// this.companyName = companyName;
	try {
	    vehicleList = new ArrayList();
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getglasses(type, rshVehicleList);
	    rs = rshVehicleList.getResultSetValue();

	    while (rs.next()) {
		vehicle = new VehicleDesc();
		vehicle.setNvic(rs.getString(1));
		vehicle.setSource(rs.getString(2).equalsIgnoreCase("Y"));
		vehicle.setMake(rs.getString(3));
		vehicle.setModel(rs.getString(4));
		vehicle.setBody(rs.getString(5));
		vehicle.setEngine(rs.getString(6));
		vehicle.setCapacity(rs.getInt(7) + "");
		vehicle.setCylinders(rs.getString(8));
		vehicle.setTransmission(rs.getString(9));
		vehicle.setYear(rs.getInt(10) + "");
		vehicle.setRelease(rs.getString(11));
		vehicle.setPoints(rs.getInt(12));
		Object obj = rs.getObject(13);
		// System.out.println(obj.getClass() + "  " + obj.toString());
		if (obj != null)
		    vehicle.setValue(obj.toString());
		// vehicle.setValue(rs.getBigDecimal(13)+ "");
		dt = rs.getDate(14);
		if (dt != null)
		    vehicle.setEffDate(new DateTime(dt));
		else
		    vehicle.setEffDate(null);
		vehicle.setCategory(rs.getInt(15));

		vehicleList.add(vehicle);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new SystemException(e);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return vehicleList;
    }

    public String setVehicleList(String companyName, ArrayList vl) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	String returnMessage = null;
	try {
	    MotorVehicleDescHolder vh = new MotorVehicleDescHolder(vl);
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    StringHolder returnMessageHolder = new StringHolder();
	    insuranceProgressProxy.putglpoints(vh, returnMessageHolder);
	    returnMessage = returnMessageHolder.getStringValue();
	    System.out.println(returnMessage);
	    if (returnMessage != null && returnMessage.indexOf("Success") == -1) {
		throw new SystemException("Error setting suburbs: " + returnMessage);
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return returnMessage;
    }

    private class MotorVehicleDescHolder extends InputResultSet {
	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public void updateNClob(int columnIndex, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateNClob(String columnLabel, NClob clob) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
	    // TODO Auto-generated method stub

	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	ArrayList dataList = null;
	int position;
	VehicleDesc vd = null;

	public int getHoldability() throws SQLException {
	    // TODO Auto-generated method stub
	    return 0;
	}

	public Reader getNCharacterStream(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public Reader getNCharacterStream(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(int columnIndex) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public String getNString(String columnLabel) throws SQLException {
	    // TODO Auto-generated method stub
	    return null;
	}

	public boolean isClosed() throws SQLException {
	    // TODO Auto-generated method stub
	    return false;
	}

	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNClob(String columnLabel, Reader reader) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(int columnIndex, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public void updateNString(String columnLabel, String string) throws SQLException {
	    // TODO Auto-generated method stub

	}

	public MotorVehicleDescHolder(ArrayList vl) {
	    this.dataList = vl;
	    this.position = -1;
	}

	public Object getObject(int f) {
	    Object ob = null;
	    switch (f) {
	    case 1:
		ob = vd.getNvic();
		break;
	    case 2:
		if (vd.getSource())
		    ob = "RACT";
		else
		    ob = "Glasses";
		break;
	    case 3:
		ob = vd.getPoints() + "";
		break;
	    case 4:
		ob = vd.getValue();
		break;
	    case 5:
		ob = vd.getEffDate().formatShortDate();
		break;
	    case 6:
		ob = vd.getCategory() + "";
		break;
	    }
	    return "";
	}

	public boolean next() {
	    position++;
	    if (position >= dataList.size())
		return false;
	    else {
		vd = (VehicleDesc) this.dataList.get(position);
		return true;
	    }
	}

	public URL getURL(int columnIndex) {
	    return null;
	}

	public void updateArray(int columnIndex, Array x) {
	}

	public void updateBlob(int columnIndex, Blob x) {
	}

	public void updateClob(int columnIndex, Clob x) {
	}

	public void updateRef(int columnIndex, Ref x) {
	}

	public URL getURL(String columnName) {
	    return null;
	}

	public void updateArray(String columnName, Array x) {
	}

	public void updateBlob(String columnName, Blob x) {
	}

	public void updateClob(String columnName, Clob x) {
	}

	public void updateRef(String columnName, Ref x) {
	}

    }

    /*
     * public void openVehicleQuery(String companyName, String type)throws
     * SystemException public void releaseVehicleQuery()throws SystemException
     * public VehicleDesc getNextVehicle()throws SystemException
     * 
     * }
     */
    GlassesVehicles getVehicleProc;

    public void openVehicleQuery(String companyName, String type) throws SystemException {
	try {
	    InsuranceProgressProxy iP = this.getInsuranceProgressProxy(companyName);
	    this.getVehicleProc = iP.createPO_GlassesVehicles(type);
	} catch (Exception e) {
	    throw new SystemException(e);
	}
    }

    public void releaseVehicleQuery() throws SystemException {
	try {
	    getVehicleProc.closeQuery();
	    getVehicleProc._release();
	} catch (Open4GLException ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex);
	}
    }

    public VehicleDesc getNextVehicle() throws SystemException {

	VehicleDesc vehicle = null;
	java.sql.Date tDate = null;
	java.sql.Date dt = null;
	StringHolder nvic = new StringHolder();
	StringHolder source = new StringHolder();
	StringHolder make = new StringHolder();
	StringHolder model = new StringHolder();
	StringHolder body = new StringHolder();
	StringHolder engine = new StringHolder();
	IntHolder engineSize = new IntHolder();
	StringHolder cyl = new StringHolder();
	StringHolder transType = new StringHolder();
	IntHolder vehYear = new IntHolder();
	StringHolder vehRelease = new StringHolder();
	IntHolder points = new IntHolder();
	BigDecimalHolder vehValue = new BigDecimalHolder();
	DateHolder effDate = new DateHolder();
	IntHolder category = new IntHolder();

	try {
	    getVehicleProc.getNextVehicle(nvic, source, make, model, body, engine, engineSize, cyl, transType, vehYear, vehRelease, points, vehValue, effDate, category);

	    vehicle = new VehicleDesc();
	    vehicle.setNvic(nvic.getStringValue());
	    vehicle.setSource(source.getStringValue().equalsIgnoreCase("RACT"));
	    vehicle.setMake(make.getStringValue());
	    vehicle.setModel(model.getStringValue());
	    vehicle.setBody(body.getStringValue());
	    vehicle.setEngine(engine.getStringValue());
	    vehicle.setCapacity(engineSize.getIntValue() + "");
	    vehicle.setCylinders(cyl.getStringValue());
	    vehicle.setTransmission(transType.getStringValue());
	    vehicle.setYear(vehYear.getIntValue() + "");
	    vehicle.setRelease(vehRelease.getStringValue());
	    vehicle.setPoints(points.getIntValue());
	    vehicle.setValue(vehValue.getBigDecimalValue().toString());
	    vehicle.setEffDate(new DateTime(effDate.getDateValue().getTime()));
	    vehicle.setCategory(category.getIntValue());
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new SystemException(e);
	}
	return vehicle;
    }

    public ArrayList getXSTypes(String prodType, String riskType, DateTime effDate, String companyName) throws SystemException {
	ArrayList typeList = new ArrayList();
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rsh = new ResultSetHolder();
	XSType xsType = null;
	try {

	    GregorianCalendar gEffDate = null;
	    if (effDate != null) {
		gEffDate = new GregorianCalendar();
		gEffDate.setTime(effDate);
	    }
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXSTypes(prodType, riskType, gEffDate, rsh);
	    ResultSet rs = rsh.getResultSetValue();
	    while (rs.next()) {
		xsType = new XSType();
		xsType.setXsSeqNo(new Integer(rs.getInt(1)));
		xsType.setRiskType(rs.getString(2));
		xsType.setXsType(rs.getString(3));
		xsType.setEffDate(new DateTime(rs.getDate(4)));
		xsType.setXsHeading(rs.getString(5));
		xsType.setPrintIfNil(new Boolean(rs.getBoolean(6)));
		xsType.setRestricted(new Boolean(rs.getBoolean(7)));
		xsType.setCreateDate(new DateTime(rs.getDate(8)));
		xsType.setCreateId(rs.getString(9));
		xsType.setRequired(new Boolean(rs.getBoolean(10)));
		xsType.setAppendText(rs.getString(11));
		xsType.setDisplayOrder(new Integer(rs.getInt(12)));
		xsType.setReducible(new Boolean(rs.getBoolean(13)));
		xsType.setExclude(rs.getString(14));
		xsType.setProdType(rs.getString(15));
		typeList.add(xsType);
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return typeList;
    }

    public ArrayList getXSOptions(Integer xsSeqNo, String companyName) throws SystemException {
	ArrayList typeList = new ArrayList();
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rsh = new ResultSetHolder();
	XSOption xsOpt = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXSOptions(xsSeqNo.intValue(), rsh);
	    ResultSet rs = rsh.getResultSetValue();
	    while (rs.next()) {
		xsOpt = new XSOption();
		xsOpt.setXsSeqNo(new Integer(rs.getInt(1)));
		Object amtObject = rs.getObject(2);
		if (amtObject != null)
		    xsOpt.setXsAmount((Integer) (amtObject));
		xsOpt.setRatingType(rs.getString(3));
		Object ob = rs.getObject(4);
		if (ob != null) {
		    xsOpt.setRateValue(new BigDecimal(ob.toString()));
		}
		xsOpt.setUnit(rs.getString(5));
		xsOpt.setOptNo(new Integer(rs.getInt(6)));
		xsOpt.setReplacesValues(rs.getString(7));
		typeList.add(xsOpt);
		// LogUtil.log(this.getClass()+".getXSOptions",
		// "\n" + xsOpt.toString());
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return typeList;
    }

    public ArrayList getXSDetails(Integer xsSeqNo, String xsCode, String companyName) throws SystemException {
	ArrayList typeList = new ArrayList();
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rsh = new ResultSetHolder();
	XSDetail xsDet = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXSDetails(xsSeqNo.intValue(), xsCode, rsh);
	    ResultSet rs = rsh.getResultSetValue();
	    while (rs.next()) {
		xsDet = new XSDetail();
		xsDet.setSeqNo(new Integer(rs.getInt(1)));
		xsDet.setXsCode(rs.getString(2));
		xsDet.setXsName(rs.getString(3));
		xsDet.setFixed(new Boolean(rs.getBoolean(4)));
		xsDet.setHasText(new Boolean(rs.getBoolean(5)));
		xsDet.setDocText(rs.getString(6));
		xsDet.setRateable(new Boolean(rs.getBoolean(7)));
		xsDet.setIncidentTypes(rs.getString(8));
		xsDet.setAmount(new Integer(rs.getInt(9)));
		xsDet.setAllowMultiples(new Boolean(rs.getBoolean(10)));
		xsDet.setXsUnit(rs.getString(11));
		xsDet.setHasAmount(new Boolean(rs.getBoolean(12)));
		xsDet.setMultiLine(new Boolean(rs.getBoolean(13)));
		xsDet.setExclude(rs.getString(14));
		typeList.add(xsDet);
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return typeList;
    }

    public XSType getXSType(Integer xsSeqNo, String companyName) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	XSType xst = null;
	StringHolder rTypeHolder = new StringHolder();
	StringHolder xsTypeHolder = new StringHolder();
	DateHolder effDateHolder = new DateHolder();
	StringHolder headingHolder = new StringHolder();
	BooleanHolder printIfNilHolder = new BooleanHolder();
	BooleanHolder restrictedHolder = new BooleanHolder();
	StringHolder createIdHolder = new StringHolder();
	DateHolder createDateHolder = new DateHolder();
	BooleanHolder required = new BooleanHolder();
	StringHolder atHolder = new StringHolder();
	IntHolder doHolder = new IntHolder();
	BooleanHolder redHolder = new BooleanHolder();
	StringHolder excludeHolder = new StringHolder();
	StringHolder prodHolder = new StringHolder();
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXsType(xsSeqNo.intValue(), rTypeHolder, xsTypeHolder, effDateHolder, headingHolder, printIfNilHolder, restrictedHolder, createIdHolder, createDateHolder, required, atHolder, doHolder, redHolder, excludeHolder, prodHolder);
	    xst = new XSType();
	    xst.setXsSeqNo(xsSeqNo);
	    xst.setCreateDate(new DateTime(createDateHolder.getDateValue().getTime()));
	    xst.setCreateId(createIdHolder.getStringValue());
	    xst.setEffDate(new DateTime(effDateHolder.getDateValue().getTime()));
	    xst.setPrintIfNil(new Boolean(printIfNilHolder.getBooleanValue()));
	    xst.setRestricted(new Boolean(restrictedHolder.getBooleanValue()));
	    xst.setRiskType(rTypeHolder.getStringValue());
	    xst.setXsHeading(headingHolder.getStringValue());
	    xst.setXsType(xsTypeHolder.getStringValue());
	    xst.setRequired(new Boolean(required.getBooleanValue()));
	    xst.setAppendText(atHolder.getStringValue());
	    xst.setDisplayOrder(new Integer(doHolder.getIntValue()));
	    xst.setReducible(new Boolean(redHolder.getBooleanValue()));
	    xst.setExclude(excludeHolder.getStringValue());
	    xst.setProdType(prodHolder.getStringValue());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return xst;
    }

    public XSDetail getXSDetail(Integer xsSeqNo, String xsCode, String companyName) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	XSDetail xsd = null;
	StringHolder nameHolder = new StringHolder();
	BooleanHolder fixedHolder = new BooleanHolder();
	IntHolder amountHolder = new IntHolder();
	BooleanHolder hasTextHolder = new BooleanHolder();
	StringHolder docTextHolder = new StringHolder();
	BooleanHolder rateableHolder = new BooleanHolder();
	StringHolder incTypesHolder = new StringHolder();
	BooleanHolder allowMultiplesHolder = new BooleanHolder();
	StringHolder unitHolder = new StringHolder();
	BooleanHolder hasAmountHolder = new BooleanHolder();
	BooleanHolder multiLineHolder = new BooleanHolder();
	StringHolder excludeHolder = new StringHolder();
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXsDet(xsSeqNo.intValue(), xsCode, nameHolder, fixedHolder, amountHolder, hasTextHolder, docTextHolder, rateableHolder, incTypesHolder, allowMultiplesHolder, unitHolder, hasAmountHolder, multiLineHolder, excludeHolder);
	    xsd = new XSDetail();
	    if (!amountHolder.isNull())
		xsd.setAmount(new Integer(amountHolder.getIntValue()));
	    xsd.setDocText(docTextHolder.getStringValue());
	    xsd.setFixed(new Boolean(fixedHolder.getBooleanValue()));
	    xsd.setHasText(new Boolean(hasTextHolder.getBooleanValue()));
	    xsd.setIncidentTypes(incTypesHolder.getStringValue());
	    xsd.setRateable(new Boolean(rateableHolder.getBooleanValue()));
	    xsd.setSeqNo(xsSeqNo);
	    xsd.setXsCode(xsCode);
	    xsd.setXsName(nameHolder.getStringValue());
	    xsd.setAllowMultiples(new Boolean(allowMultiplesHolder.getBooleanValue()));
	    xsd.setXsUnit(unitHolder.getStringValue());
	    xsd.setHasAmount(new Boolean(hasAmountHolder.getBooleanValue()));
	    xsd.setMultiLine(new Boolean(multiLineHolder.getBooleanValue()));
	    xsd.setExclude(excludeHolder.getStringValue());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return xsd;
    }

    public XSOption getXSOption(Integer optNo, String companyName) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	XSOption xso = null;
	StringHolder ratingTypeHolder = new StringHolder();
	BigDecimalHolder rateHolder = new BigDecimalHolder();
	StringHolder unitHolder = new StringHolder();
	IntHolder seqHolder = new IntHolder();
	IntHolder amountHolder = new IntHolder();
	StringHolder replacesHolder = new StringHolder();
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    // System.out.println("\nProgressInsuranceAdapter.getXSOption"
	    // + "\nseqNo = " + seqNo + " (" + seqNo.getClass() + ")"
	    // + "\namount " + amount + " (" + amount.getClass() + ")");

	    insuranceProgressProxy.getXsOpt(optNo.intValue(), seqHolder, ratingTypeHolder, rateHolder, unitHolder, amountHolder, replacesHolder);
	    xso = new XSOption();
	    if (!rateHolder.isNull())
		xso.setRateValue(rateHolder.getBigDecimalValue());
	    xso.setRatingType(ratingTypeHolder.getStringValue());
	    if (!amountHolder.isNull())
		xso.setXsAmount(new Integer(amountHolder.getIntValue()));
	    xso.setXsSeqNo(new Integer(seqHolder.getIntValue()));
	    xso.setUnit(unitHolder.getStringValue());
	    xso.setOptNo(optNo);
	    xso.setReplacesValues(replacesHolder.getStringValue());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return xso;
    }

    public XSType setXSType(XSType xst, String companyName) throws SystemException {
	// LogUtil.log(this.getClass()+ ".setXSType",
	// "\nUpdatine xsType " + xst.toString());
	InsuranceProgressProxy insuranceProgressProxy = null;
	GregorianCalendar effDateCal = new GregorianCalendar();
	effDateCal.setTime(xst.getEffDate());
	GregorianCalendar cDate = new GregorianCalendar();
	cDate.setTime(xst.getCreateDate());
	IntHolder oSeqHolder = new IntHolder();
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.putXSType(xst.getXsSeqNo().intValue(), xst.getRiskType(), xst.getXsType(), effDateCal, xst.getXsHeading(), xst.getPrintIfNil().booleanValue(), xst.getRestricted().booleanValue(), xst.getCreateId(), xst.getRequired().booleanValue(), xst.getAppendText(), xst.getDisplayOrder().intValue(), xst.getReducible().booleanValue(), xst.getExclude(), xst.getProdType(), oSeqHolder);
	    xst.setXsSeqNo(new Integer(oSeqHolder.getIntValue()));
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return xst;
    }

    public void setXSDetail(XSDetail xsd, String companyName) throws SystemException {
	InsuranceProgressProxy ipp = null;

	try {
	    // LogUtil.log(this.getClass()+ ".setXSDetail",
	    // "\n" + xsd.toString());
	    ipp = this.getInsuranceProgressProxy(companyName);
	    ipp.putXSDetail(xsd.getSeqNo().intValue(), xsd.getXsCode(), xsd.getXsName(), xsd.getFixed().booleanValue(), xsd.getAmount(), xsd.getHasText().booleanValue(), xsd.getDocText(), xsd.getRateable().booleanValue(), xsd.getIncidentTypes(), xsd.getAllowMultiples().booleanValue(), xsd.getXsUnit(), xsd.getHasAmount().booleanValue(), xsd.getMultiLine().booleanValue(), xsd.getExclude());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(ipp);
	}

    }

    public void setXSOption(XSOption xso, String companyName) throws SystemException {
	InsuranceProgressProxy ipp = null;
	IntHolder optNoHolder = new IntHolder();
	// LogUtil.log(this.getClass()+ ".setXSOption",
	// "Setting xsOption for " + xso.toString());
	try {
	    ipp = this.getInsuranceProgressProxy(companyName);
	    ipp.putXSOption(xso.getXsSeqNo().intValue(), xso.getXsAmount(), xso.getRatingType(), xso.getRateValue(), xso.getUnit(), xso.getReplacesValues(), optNoHolder);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(ipp);
	}

    }

    /**
     * deleteExcessDet
     * 
     * @param xsSeqNo
     *            Integer
     * @param excessCode
     *            String
     * @param companyName
     *            String
     */
    public void deleteExcessDet(Integer xsSeqNo, String excessCode, String companyName) throws SystemException {
	InsuranceProgressProxy ipp = null;

	try {
	    ipp = this.getInsuranceProgressProxy(companyName);
	    ipp.deleteXSDet(xsSeqNo.intValue(), excessCode);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(ipp);
	}

    }

    /**
     * deleteExcessType
     * 
     * @param xsSeqNo
     *            Integer
     * @param companyName
     *            String
     */
    public void deleteExcessType(Integer xsSeqNo, String companyName) throws SystemException {
	InsuranceProgressProxy ipp = null;

	try {
	    ipp = this.getInsuranceProgressProxy(companyName);
	    ipp.deleteXSType(xsSeqNo.intValue());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(ipp);
	}

    }

    /**
     * deleteXSOption
     * 
     * @param xsSeqNo
     *            Integer
     * @param xsAmt
     *            Integer
     * @param companyName
     *            String
     */
    public void deleteXSOption(Integer optNo, String companyName) throws SystemException {
	InsuranceProgressProxy ipp = null;

	try {
	    ipp = this.getInsuranceProgressProxy(companyName);
	    ipp.deleteXSOpt(optNo.intValue());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(ipp);
	}
    }

    /**
     * newXSVersion
     * 
     * @param xst
     *            XSType
     * @param oldSeqNo
     *            Integer
     * @param companyName
     *            String
     */
    public void newXSVersion(XSType xst, Integer oldSeqNo, String companyName) throws SystemException {
	InsuranceProgressProxy ipp = null;
	GregorianCalendar effDate = new GregorianCalendar();
	effDate.setTime(xst.getEffDate());
	IntHolder sHolder = new IntHolder();
	try {
	    ipp = this.getInsuranceProgressProxy(companyName);
	    ipp.newXSVersion(oldSeqNo.intValue(), xst.getRiskType(), xst.getXsType(), effDate, xst.getXsHeading(), xst.getPrintIfNil().booleanValue(), xst.getRestricted().booleanValue(), xst.getCreateId(), xst.getRequired().booleanValue(), xst.getAppendText(), xst.getDisplayOrder().intValue(), xst.getReducible().booleanValue(), xst.getExclude(), xst.getProdType(), sHolder);
	    xst.setXsSeqNo(new Integer(sHolder.getIntValue()));
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(ipp);
	}
    }

    public void createNewXSVersionsForRiskType(String prodType, String riskType, DateTime oldEffDate, DateTime newEffDate, String userId, String companyName) throws SystemException {
	InsuranceProgressProxy ipp = null;
	GregorianCalendar oEffDate = new GregorianCalendar();
	oEffDate.setTime(oldEffDate);
	GregorianCalendar nEffDate = new GregorianCalendar();
	nEffDate.setTime(newEffDate);

	IntHolder sHolder = new IntHolder();
	try {
	    ipp = this.getInsuranceProgressProxy(companyName);
	    ipp.newXSVersionForRisk(prodType, riskType, oEffDate, nEffDate, userId);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(ipp);
	}
    }

    /**
     * getXSTypesByType
     * 
     * @param riskType
     *            String
     * @param xsType
     *            String
     * @param companyName
     *            String
     * @return ArrayList
     */
    public ArrayList getXSTypesByType(String prodType, String riskType, String xsTypeName, String companyName) throws SystemException {
	ArrayList typeList = new ArrayList();
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rsh = new ResultSetHolder();
	XSType xsType = null;
	try {

	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXSTypesByType(prodType, riskType, xsTypeName, rsh);
	    ResultSet rs = rsh.getResultSetValue();
	    while (rs.next()) {
		xsType = new XSType();
		xsType.setXsSeqNo(new Integer(rs.getInt(1)));
		xsType.setRiskType(rs.getString(2));
		xsType.setXsType(rs.getString(3));
		xsType.setEffDate(new DateTime(rs.getDate(4)));
		xsType.setXsHeading(rs.getString(5));
		xsType.setPrintIfNil(new Boolean(rs.getBoolean(6)));
		xsType.setRestricted(new Boolean(rs.getBoolean(7)));
		xsType.setCreateDate(new DateTime(rs.getDate(8)));
		xsType.setCreateId(rs.getString(9));
		xsType.setRequired(new Boolean(rs.getBoolean(10)));
		xsType.setAppendText(rs.getString(11));
		xsType.setDisplayOrder(new Integer(rs.getInt(12)));
		xsType.setReducible(new Boolean(rs.getBoolean(13)));
		xsType.setExclude(rs.getString(14));
		xsType.setProdType(rs.getString(15));
		typeList.add(xsType);
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new SystemException(ex.getCause());
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return typeList;

    }

    public ArrayList getXsLines(Integer xsSeqNo, String xsCode, String companyName) throws SystemException {
	ArrayList lineList = new ArrayList();
	InsuranceProgressProxy insuranceProgressProxy = null;
	ResultSetHolder rsh = new ResultSetHolder();
	XSLine xsl = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXsLines(xsSeqNo.intValue(), xsCode, rsh);
	    ResultSet rs = rsh.getResultSetValue();
	    while (rs.next()) {
		xsl = new XSLine();
		xsl.setXsSeqNo(new Integer(rs.getInt(1)));
		xsl.setXsCode(rs.getString(2));
		xsl.setDisplaySeq(new Integer(rs.getInt(3)));
		xsl.setLineText(rs.getString(4));
		xsl.setLineAmount(new Integer(rs.getInt(5)));
		lineList.add(xsl);
	    }
	} catch (Exception ex) {
	    throw new SystemException(ex);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return lineList;

    }

    public XSLine getXsLine(Integer xsSeqNo, String xsCode, Integer dispSeq, String companyName) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	XSLine xsl = null;
	StringHolder textHolder = new StringHolder();
	IntHolder amountHolder = new IntHolder();
	try {
	    xsl = new XSLine();
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.getXsLine(xsSeqNo.intValue(), xsCode, dispSeq.intValue(), textHolder, amountHolder);
	    xsl.setXsSeqNo(xsSeqNo);
	    xsl.setXsCode(xsCode);
	    xsl.setDisplaySeq(dispSeq);
	    xsl.setLineText(textHolder.getStringValue());
	    xsl.setLineAmount(new Integer(amountHolder.getIntValue()));
	} catch (Exception ex) {
	    throw new SystemException(ex);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
	return xsl;
    }

    public void setXsLine(XSLine xsl, String companyName) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    // LogUtil.log(this.getClass()+".setXsLine",
	    // "\nsending XsLine to progress database"
	    // + "\n" + xsl.toString());
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.putXsLine(xsl.getXsSeqNo().intValue(), xsl.getXsCode(), xsl.getDisplaySeq().intValue(), xsl.getLineText(), xsl.getLineAmount().intValue());
	} catch (Exception ex) {
	    throw new SystemException(ex);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
    }

    public void deleteXsLine(XSLine xsl, String companyName) throws SystemException {
	InsuranceProgressProxy insuranceProgressProxy = null;
	try {
	    insuranceProgressProxy = this.getInsuranceProgressProxy(companyName);
	    insuranceProgressProxy.deleteXsLine(xsl.getXsSeqNo().intValue(), xsl.getXsCode(), xsl.getDisplaySeq().intValue());
	} catch (Exception ex) {
	    throw new SystemException(ex);
	} finally {
	    this.releaseInsuranceProgressProxy(insuranceProgressProxy);
	}
    }

    public void notifyBulkRecoveryPayment(BigDecimal amount, Integer receiptNo, String agency, String chequeNo, String email, String fileName, String logonId, String salesBranch, String company) throws SystemException {
	this.companyName = company;
	/*
	 * LogUtil.log(this.getClass(),"notifyBulkRecoveryPayment");
	 * LogUtil.log(this.getClass(),"amount =      " + amount);
	 * LogUtil.log(this.getClass(),"receiptNo =   " + receiptNo);
	 * LogUtil.log(this.getClass(),"agency =      " + agency);
	 * LogUtil.log(this.getClass(),"chequeNo =    " + chequeNo);
	 * LogUtil.log(this.getClass(),"email =       " + email );
	 * LogUtil.log(this.getClass(),"fileName =    " + fileName);
	 * LogUtil.log(this.getClass(),"logonId =     " + logonId);
	 * LogUtil.log(this.getClass(),"salesBranch = " + salesBranch);
	 * LogUtil.log(this.getClass(),"company =     " + company);
	 */
	try {
	    getInsuranceTransactionProxy(company);
	    this.insuranceTransactionProxy.bulkRecoveryPayment(amount, receiptNo.intValue(), agency, chequeNo, email, fileName, logonId, salesBranch);
	    handleProxyReturnString();
	} catch (Open4GLException ex) {
	    ex.printStackTrace();
	    handleProxyReturnString();
	    throw new SystemException("Unable to create recovery payment in Insurance system.", ex);
	}

    }

}
