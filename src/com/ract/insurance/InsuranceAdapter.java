package com.ract.insurance;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.util.DateTime;

/**
 * <p>
 * Standard interface for connection to an external insurance system
 * </p>
 * 
 * @author: dgk
 * @version 1.0 14/8/2003
 */
public interface InsuranceAdapter extends TransactionAdapter {

    public void release();

    /**
     * Commit any pending transactions in the finance sub system. The connection
     * to the finance sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    // public void commit()
    // throws SystemException;

    /**
     * Get the policy list for the client
     * 
     * @param policyNumber
     *            String
     * @throws SystemException
     * @return boolean
     */
    public ArrayList getPolicyList(Integer clientNumber, String companyName) throws SystemException;

    /**
     * Rollback any pending transactions in the finance sub system. The
     * connection to the finance sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    // public void rollback()
    // throws SystemException;

    /**
     * Release any connections held to the finance sub-system. If a transaction
     * is pending and not committed then the transaction will be rolled back.
     */
    // public void release();

    /**
     * Creates a payment record in the insurance system for a claim recovery
     * payment.
     * 
     * @param Integer
     *            claimNo
     * @param BigDecimal
     *            amount
     * @param Integer
     *            receiptNo
     * @param String
     *            description (Payer name, or other detail)
     * @param String
     *            logonId
     * @param String
     *            salesBranch
     * @param String
     *            company (RACT, ISCU or AURORA)
     * @throws SystemException
     */

    public void notifyRecoveryPayment(Integer claimNo, BigDecimal amount, Integer receiptNo, String description, String logonId, String salesBranch, String company) throws SystemException;

    /**
     * Creates a payment records in the insurance system for a bulk claim
     * recovery payment.
     * 
     * @param BigDecimal
     *            amount
     * @param Integer
     *            receiptNo
     * @param String
     *            description (Payer name, or other detail)
     * @param String
     *            refNo (hopefully this will hold the filename of the associated
     *            data file)
     * @param String
     *            logonId
     * @param String
     *            salesBranch
     * @param String
     *            company (RACT, ISCU or AURORA)
     * @throws SystemException
     */

    public void notifyBulkRecoveryPayment(BigDecimal amount, Integer receiptNo, String agency, String chequeNo, String email, String fileName, String logonId, String salesBranch, String company) throws SystemException;

    /**
     * Notifies the insurance system of the payment of a premium
     * 
     * @param Integer
     *            receiptNo
     * @param Integer
     *            clientNo ClientNo and sequence no provide the key into the
     *            insurance payable amount records (oi-payable)
     * @param Integer
     *            seqNo
     * @param BigDecimal
     *            amount
     * @param String
     *            logonId
     * @param String
     *            salesBranch
     * @param String
     *            company (RACT,ISCU or AURORA
     * @throws SystemException
     */
    public void notifyPremiumPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch, String company) throws SystemException;

    /**
     * Returns true if the claim number specified is the number of an open claim
     * in the specified insurance company
     * 
     * @param claimNumber
     * @param company
     * @return True if valid
     * @throws SystemException
     */
    public boolean validateClaimNumber(Integer claimNumber, String companyName) throws SystemException;

    /**
     * Looks up the insurance system and returns the unpaid amount for the
     * policy to which the clientNo/seqNo refer. Does not include amounts to be
     * collected by direct debit.
     * 
     * @param clientNo
     *            The client number
     * @param seqNo
     *            The oi-payable seq-no
     * @return The total outstanding on all oi-payables for this policy
     * @throws SystemException
     */
    public BigDecimal getAmountOutstandingOnPolicy(Integer clientNo, Integer seqNo, String companyName) throws SystemException;

    public ArrayList getRFDates(String companyName) throws SystemException;

    public ArrayList getRFDetails(String companyName) throws SystemException;

    public void setRFData(String companyName, ArrayList datesList, ArrayList detailsList) throws SystemException;

    public ArrayList getMessageText(String companyName) throws SystemException;

    public void setMessageText(String companyName, ArrayList mtList) throws SystemException;

    public void setSuburbList(String companyName, ArrayList sl) throws SystemException;

    public ArrayList getSuburbList(String companyName, String extType) throws SystemException;

    public String setInvalidVehicle(String companyName, String action, String nvic, String userId) throws SystemException;

    public String setGLCode(String companyName, String tableName, String code, String value) throws SystemException;

    public String setGLVehicle(String companyName, VehicleDesc veh) throws SystemException;

    public String getVehicleDescription(String companyName, String nvic, boolean source) throws SystemException;

    public ArrayList getVehicleList(String companyName, String type) throws SystemException;

    public String setVehicleList(String companyName, ArrayList vl) throws SystemException;

    public void openVehicleQuery(String companyName, String type) throws SystemException;

    public void releaseVehicleQuery() throws SystemException;

    public VehicleDesc getNextVehicle() throws SystemException;

    public ArrayList getXSTypes(String prodType, String riskType, DateTime effDate, String companyName) throws SystemException;

    public ArrayList getXSTypesByType(String prodType, String riskType, String xsTypeName, String companyName) throws SystemException;

    public ArrayList getXSOptions(Integer xsSeqNo, String companyName) throws SystemException;

    public ArrayList getXSDetails(Integer xsSeqNo, String xsCode, String companyName) throws SystemException;

    public XSOption getXSOption(Integer optNo, String companyName) throws SystemException;

    public XSDetail getXSDetail(Integer xsSeqNo, String xsCode, String companyName) throws SystemException;

    public XSType getXSType(Integer xsSeqNo, String companyName) throws SystemException;

    public XSType setXSType(XSType xst, String companyName) throws SystemException;

    public void setXSDetail(XSDetail xsd, String companyName) throws SystemException;

    public void setXSOption(XSOption xso, String companyName) throws SystemException;

    public void newXSVersion(XSType xst, Integer oldSeqNo, String companyName) throws SystemException;

    public void deleteExcessType(Integer xsSeqNo, String companyName) throws SystemException;

    public void deleteExcessDet(Integer xsSeqNo, String excessCode, String companyName) throws SystemException;

    public void deleteXSOption(Integer optNo, String companyName) throws SystemException;

    public void createNewXSVersionsForRiskType(String prodType, String riskType, DateTime oldEffDate, DateTime newEffDate, String userId, String companyName) throws SystemException;

    public ArrayList getXsLines(Integer xsSeqNo, String xsCode, String companyName) throws SystemException;

    public XSLine getXsLine(Integer xsSeqNo, String xsCode, Integer dispSeq, String companyName) throws SystemException;

    public void setXsLine(XSLine xsl, String companyName) throws SystemException;

    public void deleteXsLine(XSLine xsl, String companyName) throws SystemException;

}
