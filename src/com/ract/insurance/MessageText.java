package com.ract.insurance;

/**
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * @author not attributable
 * @version 1.0
 */
import java.io.Serializable;

import com.ract.util.DateTime;

public class MessageText implements Serializable {
    String subSystem;
    String transType;
    String location;
    String prodType;
    String subType;
    DateTime startDate;
    String messageText;
    String notes;
    String heading;
    String createId;
    DateTime createDate;
    DateTime deleteDate;
    String deleteId;
    String sameAs;
    String recordStatus;

    public DateTime getCreateDate() {
	return createDate;
    }

    public DateTime getDeleteDate() {
	return deleteDate;
    }

    public String getCreateId() {
	return createId;
    }

    public String getDeleteId() {
	return deleteId;
    }

    public String getHeading() {
	return heading;
    }

    public String getLocation() {
	return location;
    }

    public String getMessageText() {
	return messageText;
    }

    public String getNotes() {
	return notes;
    }

    public String getProdType() {
	return prodType;
    }

    public String getSameAs() {
	return sameAs;
    }

    public DateTime getStartDate() {
	return startDate;
    }

    public String getSubSystem() {
	return subSystem;
    }

    public String getSubType() {
	return subType;
    }

    public String getTransType() {
	return transType;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public void setDeleteDate(DateTime deleteDate) {
	this.deleteDate = deleteDate;
    }

    public void setDeleteId(String deleteId) {
	this.deleteId = deleteId;
    }

    public void setHeading(String heading) {
	this.heading = heading;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public void setMessageText(String messageText) {
	this.messageText = messageText;
    }

    public void setNotes(String notes) {
	this.notes = notes;
    }

    public void setProdType(String prodType) {
	this.prodType = prodType;
    }

    public void setSameAs(String sameAs) {
	this.sameAs = sameAs;
    }

    public void setStartDate(DateTime startDate) {
	this.startDate = startDate;
    }

    public void setSubSystem(String subSystem) {
	this.subSystem = subSystem;
    }

    public void setSubType(String subType) {
	this.subType = subType;
    }

    public void setTransType(String transType) {
	this.transType = transType;
    }

    public void setRecordStatus(String s) {
	this.recordStatus = s;
    }

    public String getRecordStatus() {
	return this.recordStatus;
    }

    public int compareTo(MessageText mt) {
	int comp;
	comp = this.subSystem.toUpperCase().compareTo(mt.subSystem.toUpperCase());
	if (comp == 0) {
	    comp = this.transType.toUpperCase().compareTo(mt.transType.toUpperCase());
	    if (comp == 0) {
		comp = this.location.toUpperCase().compareTo(mt.location.toUpperCase());
		if (comp == 0) {
		    comp = this.prodType.toUpperCase().compareTo(mt.prodType.toUpperCase());
		    if (comp == 0) {
			comp = this.subType.toUpperCase().compareTo(mt.subType.toUpperCase());
			if (comp == 0) {
			    comp = this.startDate.compareTo(mt.startDate);
			}
		    }
		}
	    }
	}
	return comp;
    }

    public MessageText cloneMT() {
	MessageText mt = new MessageText();
	mt.subSystem = this.subSystem;
	mt.transType = this.transType;
	mt.location = this.location;
	mt.prodType = this.prodType;
	mt.subType = this.subType;
	if (this.startDate != null)
	    mt.startDate = new DateTime(this.startDate);
	mt.messageText = this.messageText;
	mt.notes = this.notes;
	mt.heading = this.heading;
	mt.createId = this.createId;
	if (this.createDate != null)
	    mt.createDate = new DateTime(this.createDate);
	if (this.deleteDate != null)
	    mt.deleteDate = new DateTime(this.deleteDate);
	mt.deleteId = this.deleteId;
	mt.sameAs = this.sameAs;
	mt.recordStatus = this.recordStatus;
	return mt;
    }

    public String toString() {
	return this.subSystem + "/" + this.transType + "/" + this.location + "/" + this.prodType + "/" + this.subType + "/" + this.startDate;

    }

}
