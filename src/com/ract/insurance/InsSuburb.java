package com.ract.insurance;

/**
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * <p> </p>
 * @author not attributable
 * @version 1.0
 */
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.ract.util.DateTime;

public class InsSuburb implements Serializable {
    String suburbName;
    DateTime effDate;
    BigDecimal bldgAmt;
    BigDecimal bldgPercent;
    BigDecimal cntsAmt;
    BigDecimal cntsPercent;
    BigDecimal carAmt;
    BigDecimal carPercent;
    DecimalFormat fm = new DecimalFormat("##,##0.0000");
    DecimalFormat currencyFormat = new DecimalFormat("###,###,##0.00");

    public String toCSV() {
	String eDateStr = "null";
	if (effDate != null)
	    eDateStr = effDate.formatShortDate();
	String csv = suburbName + "," + eDateStr + "," + bldgAmt.toString() + "," + bldgPercent.toString() + "," + cntsAmt.toString() + "," + cntsPercent.toString() + "," + carAmt.toString() + "," + carPercent.toString() + "\n";
	return csv;
    }

    public BigDecimal getBldgAmt() {
	return bldgAmt;
    }

    public BigDecimal getBldgPercent() {
	return bldgPercent;
    }

    public BigDecimal getCarAmt() {
	return carAmt;
    }

    public BigDecimal getCarPercent() {
	return carPercent;
    }

    public BigDecimal getCntsAmt() {
	return cntsAmt;
    }

    public BigDecimal getCntsPercent() {
	return cntsPercent;
    }

    public DateTime getEffDate() {
	return effDate;
    }

    public String getSuburbName() {
	return suburbName;
    }

    public void setBldgAmt(BigDecimal bldgAmt) {
	String formattedAmount = currencyFormat.format(bldgAmt);
	this.bldgAmt = new BigDecimal(formattedAmount);
    }

    public void setBldgPercent(BigDecimal bldgPercent) {
	String formattedAmount = fm.format(bldgPercent);
	this.bldgPercent = new BigDecimal(formattedAmount);
    }

    public void setCarAmt(BigDecimal carAmt) {
	String formattedAmount = currencyFormat.format(carAmt);
	this.carAmt = new BigDecimal(formattedAmount);
    }

    public void setCarPercent(BigDecimal carPercent) {
	String formattedAmount = fm.format(carPercent);
	this.carPercent = new BigDecimal(formattedAmount);
    }

    public void setCntsAmt(BigDecimal cntsAmt) {
	String formattedAmount = currencyFormat.format(cntsAmt);
	this.cntsAmt = new BigDecimal(formattedAmount);
    }

    public void setCntsPercent(BigDecimal cntsPercent) {
	String formattedAmount = fm.format(cntsPercent);
	this.cntsPercent = new BigDecimal(formattedAmount);
    }

    public void setEffDate(DateTime effDate) {
	this.effDate = effDate;
    }

    public void setSuburbName(String suburbName) {
	this.suburbName = suburbName;
    }
}
