package com.ract.insurance;

import java.math.BigDecimal;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import com.ract.common.SystemException;
import com.ract.util.LogUtil;

@Stateful
@Remote({ InsuranceTransactionMgrRemote.class })
@Local({ InsuranceTransactionMgrLocal.class })
public class InsuranceTransactionMgrBean implements InsuranceTransactionMgrRemote {

    ProgressInsuranceAdapter ad;
    String transType;
    BigDecimal tAmount;

    public InsuranceTransactionMgrBean() {
	this.ad = new ProgressInsuranceAdapter();
    }

    public void commit() throws SystemException {
	ad.commit();
    }

    public Exception getRollbackException() {
	// TODO Auto-generated method stub
	return ad.getRollbackException();
    }

    public boolean getRollbackOnly() {
	return ad.getRollbackOnly();
    }

    public String getSystemName() {
	return ad.getSystemName();
    }

    public void release() throws SystemException {
	LogUtil.log(this.getClass(), "RELEASE proxy");
	LogUtil.log(this.getClass(), this.transType + " " + this.tAmount);
	ad.release();
	ad = null;
    }

    public void rollback() throws SystemException {
	ad.rollback();
    }

    public void setRollbackOnly(Exception ex) {
	ad.setRollbackOnly(ex);
    }

    public void notifyRecoveryPayment(Integer claimNo, BigDecimal amount, Integer receiptNo, String description, String logonId, String salesBranch, String company) throws SystemException {
	this.transType = "RECOVERY PAYMENT";
	this.tAmount = amount;
	ad.notifyRecoveryPayment(claimNo, amount, receiptNo, description, logonId, salesBranch, company);
    }

    public void notifyBulkRecoveryPayment(BigDecimal amount, Integer receiptNo, String agency, String chequeNo, String email, String fileName, String logonId, String salesBranch, String company) throws SystemException {
	ad.notifyBulkRecoveryPayment(amount, receiptNo, agency, chequeNo, email, fileName, logonId, salesBranch, company);
    }

    public void notifyPremiumPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch, String companyName) throws SystemException {
	ad.notifyPremiumPayment(receiptNo, clientNo, seqNo, amount, logonId, salesBranch, companyName);
    }

}
