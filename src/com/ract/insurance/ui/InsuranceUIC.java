package com.ract.insurance.ui;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ract.common.ApplicationServlet;
import com.ract.common.ServiceLocator;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.hibernate.HibernatePage;
import com.ract.insurance.InsuranceAdapter;
import com.ract.insurance.InsuranceConstants;
import com.ract.insurance.InsuranceEJBHelper;
import com.ract.insurance.InsuranceFactory;
import com.ract.insurance.InsuranceMgrBean;
import com.ract.insurance.InsuranceMgrLocal;
import com.ract.insurance.XSDetail;
import com.ract.insurance.XSLine;
import com.ract.insurance.XSOption;
import com.ract.insurance.XSType;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * 
 * <p>
 * Insurance user interface controller
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class InsuranceUIC extends ApplicationServlet {
    Boolean TRUE = new Boolean(true);
    Boolean FALSE = new Boolean(false);
    public static String DELIM = "~";

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	setOverrideSecurity(true);
	super.service(request, response);
    }

    // public void handleEvent_uploadFile(HttpServletRequest request,
    // HttpServletResponse response)
    // throws ServletException, SystemException
    // {
    //
    // try
    // {
    // InsuranceMgrLocal insMgrLocal =
    // InsuranceEJBHelper.getInsuranceMgrLocal();
    // FileItem fileItem = (FileItem)request.getAttribute("upfile");
    //
    // int recordCount =
    // insMgrLocal.uploadPayeesFile(fileItem.getInputStream());
    // Double fileSize = new Double(fileItem.getSize()/1024);
    //
    // request.setAttribute("fileSize",fileSize);
    // request.setAttribute("fileName",fileItem.getName());
    // request.setAttribute("recordCount",new Integer(recordCount));
    //
    // }
    // catch(Exception ex)
    // {
    // ex.printStackTrace();
    // throw new ServletException(ex);
    // }
    //
    // this.forwardRequest(request,response,InsuranceConstants.PAGE_UPLOAD_SUPPLIERS);
    // }

    public void handleEvent_searchSuppliers(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {

	this.outputRequestParameters(request);

	String region = request.getParameter("region");
	String payeeClass = request.getParameter("payeeClass");
	String category = request.getParameter("category");
	String type = request.getParameter("type");
	String payeeName = request.getParameter("payeeName");
	String ABN = request.getParameter("ABN");
	String sortBy = request.getParameter("sortBy");

	String startPageString = request.getParameter("startPage");
	String pageSizeString = request.getParameter("pageSize");

	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceMgrLocal insMgrLocal = InsuranceEJBHelper.getInsuranceMgrLocal();

	    // todo
	    int startPage = Integer.parseInt(startPageString);
	    int pageSize = Integer.parseInt(pageSizeString);
	    // LogUtil.debug(this.getClass(),"startPage="+startPage);
	    // LogUtil.debug(this.getClass(),"pageSize="+pageSize);
	    Hashtable crit = new Hashtable();

	    // set criteria
	    if (region != null && !region.trim().equals("")) {
		crit.put(new Integer(InsuranceMgrBean.CRITERIA_REGION), region);
		request.setAttribute("region", region);
	    }
	    if (payeeClass != null && !payeeClass.trim().equals("")) {
		crit.put(new Integer(InsuranceMgrBean.CRITERIA_CLASS), payeeClass);
		request.setAttribute("payeeClass", payeeClass);
	    }
	    if (category != null && !category.trim().equals("")) {
		crit.put(new Integer(InsuranceMgrBean.CRITERIA_CATEGORY), category);
		request.setAttribute("category", category);
	    }
	    if (type != null && !type.trim().equals("")) {
		crit.put(new Integer(InsuranceMgrBean.CRITERIA_TYPE), type);
		request.setAttribute("type", type);
	    }
	    if (payeeName != null && !payeeName.trim().equals("")) {
		crit.put(new Integer(InsuranceMgrBean.CRITERIA_NAME), payeeName);
		request.setAttribute("payeeName", payeeName);
	    }
	    if (ABN != null && !ABN.trim().equals("")) {
		crit.put(new Integer(InsuranceMgrBean.CRITERIA_ABN), ABN);
		request.setAttribute("ABN", ABN);
	    }
	    if (sortBy != null && !sortBy.trim().equals("")) {
		request.setAttribute("sortBy", sortBy);
	    }

	    HibernatePage payeeListPage = insMgrLocal.getPayees(crit, startPage, pageSize, sortBy);

	    request.setAttribute("payeeListPage", payeeListPage);
	    request.setAttribute("startPage", new Integer(startPage));
	    request.setAttribute("pageSize", new Integer(pageSize));
	    request.setAttribute("totalElementSize", new Integer(payeeListPage.getTotalElementSize()));

	    this.outputRequestAttributes(request);

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException(ex);
	}

	this.forwardRequest(request, response, InsuranceConstants.PAGE_SEARCH_SUPPLIERS);
    }

    // AJAX sample - return string of xml
    public void handleEvent_getCategories(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {

	String payeeClass = request.getParameter("payeeClass").trim();
	LogUtil.debug(this.getClass(), "payeeClass=" + payeeClass);

	// get xml for categories corresponding to payeeClass
	try {
	    InsuranceMgrLocal insMgrLocal = InsuranceEJBHelper.getInsuranceMgrLocal();
	    response.setContentType("text/xml");
	    String categoryXML = insMgrLocal.getSupplierCategoriesXMLByClass(payeeClass);
	    LogUtil.log(this.getClass(), "------->categoryXML=" + categoryXML);
	    PrintWriter out = response.getWriter();
	    out.println(categoryXML);
	    out.flush();
	    out.close();

	} catch (Exception ex) {
	    throw new SystemException(ex);
	}

    }

    public void handleEvent_getClasses(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {

	String region = request.getParameter("region").trim();
	LogUtil.debug(this.getClass(), "region=" + region);
	// get xml for categories corresponding to payeeClass
	try {
	    InsuranceMgrLocal insMgrLocal = InsuranceEJBHelper.getInsuranceMgrLocal();
	    response.setContentType("text/xml");
	    String payeeClassXML = insMgrLocal.getSupplierClassesXMLByRegion(region);
	    LogUtil.log(this.getClass(), "------->categoryXML=" + payeeClassXML);
	    PrintWriter out = response.getWriter();
	    out.println(payeeClassXML);
	    out.flush();
	    out.close();

	} catch (Exception ex) {
	    throw new SystemException(ex);
	}

    }

    /*
     * public void handleEvent_loadInsExcesses(HttpServletRequest request,
     * HttpServletResponse response) throws ServletException, SystemException {
     * response.setContentType("text/xml"); response.setHeader("Cache-Control",
     * "no-cache"); try { JSONObject jso = new
     * JSONObject(request.getParameter("data")); ServiceLocator serviceLocator =
     * ServiceLocator.getInstance(); InsuranceAdapter insAdapter =
     * InsuranceFactory.getInsuranceAdapter(); JSONObject node =
     * jso.getJSONObject("node"); String objectId = node.getString("objectId");
     * String companyName = SourceSystem.INSURANCE_RACT.getAbbreviation();
     * String fieldName = ""; String fieldValue = null; String fieldValue2 =
     * null; String tString = ""; String res = ""; JSONArray jsOut = new
     * JSONArray(); JSONObject js = null; ArrayList xsTypeList = null; String
     * bank = null; Hashtable tList = new Hashtable(); int xx;
     * //LogUtil.log(this.getClass()+".loadInsExcesses ------>", //
     * "\nobjectId = " + objectId); if(objectId.equals("")) //is root {
     * xsTypeList = insAdapter.getXSTypes(null, null, companyName); for(xx = 0;
     * xx < xsTypeList.size(); xx++) { XSType xsType =
     * (XSType)xsTypeList.get(xx); if(!tList.containsKey(xsType.getRiskType()))
     * { tList.put(xsType.getRiskType(), ""); js = new JSONObject();
     * js.put("title", xsType.getRiskType()); js.put("isFolder", true);
     * js.put("objectId", "riskType" + DELIM + xsType.getRiskType());
     * jsOut.put(js); } } } else { String[] entries = objectId.split(DELIM);
     * fieldName = entries[0];
     * 
     * if(fieldName.equals("riskType")) { xsTypeList =
     * insAdapter.getXSTypes(entries[1], null, companyName); for(xx = 0; xx <
     * xsTypeList.size(); xx++) { XSType xsType = (XSType)xsTypeList.get(xx);
     * if(!tList.containsKey(xsType.getEffDate())) {
     * tList.put(xsType.getEffDate(), ""); js = new JSONObject(); tString =
     * xsType.getEffDate().formatShortDate(); if(tString == null ||
     * tString.trim().equals(""))tString = "*** Error ***"; js.put("title",
     * tString); js.put("isFolder", true); js.put("objectId", "effDate" + DELIM
     * + xsType.getRiskType() + DELIM + xsType.getEffDate()); jsOut.put(js); } }
     * } else if(fieldName.equals("effDate")) { xsTypeList =
     * insAdapter.getXSTypes(entries[1], new DateTime(entries[2]), companyName);
     * for(xx = 0; xx < xsTypeList.size(); xx++) { XSType xsType =
     * (XSType)xsTypeList.get(xx); js = new JSONObject(); js.put("title",
     * xsType.getXsHeading()); js.put("isFolder", true); js.put("objectId",
     * "xsSeqNo" + DELIM + xsType.getXsSeqNo() + DELIM + xsType.getXsType());
     * jsOut.put(js); } } else if(fieldName.equals("xsSeqNo")) { Integer xsSeqNo
     * = new Integer(entries[1]); xsTypeList = insAdapter.getXSDetails(xsSeqNo,
     * null, companyName); for(xx = 0; xx < xsTypeList.size(); xx++) { XSDetail
     * xsd = (XSDetail)xsTypeList.get(xx); js = new JSONObject();
     * js.put("title", xsd.getXsName()); js.put("isFolder", false);
     * js.put("objectId", "excess" + DELIM + xsSeqNo + DELIM + xsd.getXsCode());
     * jsOut.put(js); } xsTypeList = insAdapter.getXSOptions(xsSeqNo,
     * companyName); if(xsTypeList.size()>0) { js = new JSONObject();
     * js.put("title","Options"); js.put("isFolder", true); js.put("objectId",
     * "options" + DELIM + new Integer(entries[1]) + DELIM + xsSeqNo);
     * jsOut.put(js); } } else if(fieldName.equals("options")) { xsTypeList =
     * insAdapter.getXSOptions(new Integer(entries[1]), companyName); for(xx =
     * 0; xx < xsTypeList.size(); xx++) { XSOption xso =
     * (XSOption)xsTypeList.get(xx); js = new JSONObject(); String xsAmtStr =
     * null; if(xso.getXsAmount()==null) xsAmtStr = ""; else xsAmtStr =
     * xso.getXsAmount()+""; if(xso.getUnit()!=null) {
     * if(xso.getUnit().equals("$"))xsAmtStr = "$" + xsAmtStr; else xsAmtStr +=
     * " " + xso.getUnit(); } else xsAmtStr = "$" + xsAmtStr; xsAmtStr =
     * xsAmtStr.trim(); js.put("title", xsAmtStr); js.put("isFolder", false);
     * js.put("objectId", "excessAmt" + DELIM + xso.getOptNo() + DELIM +
     * xso.getXsSeqNo()); jsOut.put(js); } } } //
     * System.out.println(jsOut.toString());
     * response.getWriter().write(jsOut.toString()); } catch(Exception ex) {
     * ex.printStackTrace(); throw new ServletException(ex); }
     * 
     * }
     */

    public void handleEvent_loadInsExcesses(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	try {
	    JSONObject jso = new JSONObject(request.getParameter("data"));
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	    JSONObject node = jso.getJSONObject("node");
	    String objectId = node.getString("objectId");
	    String companyName = SourceSystem.INSURANCE_RACT.getAbbreviation();
	    String fieldName = "";
	    String fieldValue = null;
	    String fieldValue2 = null;
	    String tString = "";
	    String res = "";
	    JSONArray jsOut = new JSONArray();
	    JSONObject js = null;
	    ArrayList xsTypeList = null;
	    String bank = null;
	    Hashtable tList = new Hashtable();
	    int xx;
	    // LogUtil.log(this.getClass()+".loadInsExcesses ------>",
	    // "\nobjectId = " + objectId);
	    if (objectId.equals("")) // is root
	    {
		xsTypeList = insAdapter.getXSTypes(null, null, null, companyName);
		for (xx = 0; xx < xsTypeList.size(); xx++) {
		    XSType xsType = (XSType) xsTypeList.get(xx);
		    if (!tList.containsKey(xsType.getProdType())) {
			tList.put(xsType.getProdType(), "");
			js = new JSONObject();
			js.put("title", xsType.getProdType());
			js.put("isFolder", true);
			js.put("objectId", "prodType" + DELIM + xsType.getProdType());
			jsOut.put(js);
		    }
		}
	    } else {
		String[] entries = objectId.split(DELIM);
		fieldName = entries[0];
		if (fieldName.equals("prodType")) {
		    xsTypeList = insAdapter.getXSTypes(entries[1], null, null, companyName);
		    for (xx = 0; xx < xsTypeList.size(); xx++) {
			XSType xsType = (XSType) xsTypeList.get(xx);
			if (!tList.containsKey(xsType.getRiskType())) {
			    tList.put(xsType.getRiskType(), "");
			    js = new JSONObject();
			    js.put("title", xsType.getRiskType());
			    js.put("isFolder", true);
			    js.put("objectId", "riskType" + DELIM + xsType.getRiskType() + DELIM + xsType.getProdType());
			    jsOut.put(js);
			}
		    }

		} else if (fieldName.equals("riskType")) {
		    xsTypeList = insAdapter.getXSTypes(entries[2], entries[1], null, companyName);
		    for (xx = 0; xx < xsTypeList.size(); xx++) {
			XSType xsType = (XSType) xsTypeList.get(xx);
			if (!tList.containsKey(xsType.getXsType())) {
			    tList.put(xsType.getXsType(), "");
			    js = new JSONObject();
			    js.put("title", xsType.getXsHeading());
			    js.put("isFolder", true);
			    js.put("objectId", "xsType" + DELIM + xsType.getRiskType() + DELIM + xsType.getXsType() + DELIM + xsType.getProdType());
			    jsOut.put(js);
			}
		    }
		} else if (fieldName.equals("xsType")) {
		    xsTypeList = insAdapter.getXSTypesByType(entries[3], entries[1], entries[2], companyName);
		    for (xx = 0; xx < xsTypeList.size(); xx++) {
			XSType xsType = (XSType) xsTypeList.get(xx);
			js = new JSONObject();
			js.put("title", xsType.getEffDate());
			js.put("isFolder", true);
			js.put("objectId", "xsSeqNo" + DELIM + xsType.getXsSeqNo() + DELIM + xsType.getEffDate());
			jsOut.put(js);
		    }
		    /* add memos if there are any */
		} else if (fieldName.equals("xsSeqNo")) {
		    Integer xsSeqNo = new Integer(entries[1]);
		    xsTypeList = insAdapter.getXSDetails(xsSeqNo, null, companyName);
		    for (xx = 0; xx < xsTypeList.size(); xx++) {
			XSDetail xsd = (XSDetail) xsTypeList.get(xx);
			js = new JSONObject();
			js.put("title", xsd.getXsName());
			js.put("isFolder", xsd.getMultiLine().booleanValue());
			js.put("objectId", "excess" + DELIM + xsSeqNo + DELIM + xsd.getXsCode());
			jsOut.put(js);
		    }
		    xsTypeList = insAdapter.getXSOptions(xsSeqNo, companyName);
		    if (xsTypeList.size() > 0) {
			js = new JSONObject();
			js.put("title", "Options");
			js.put("isFolder", true);
			js.put("objectId", "options" + DELIM + new Integer(entries[1]) + DELIM + xsSeqNo); /* xsSeqNo */
			jsOut.put(js);
		    }
		}
		/* multi line excesses */
		else if (fieldName.equals("excess")) {
		    Integer xsSeqNo = new Integer(entries[1]);
		    String xsCode = entries[2];
		    ArrayList lineList = insAdapter.getXsLines(xsSeqNo, xsCode, companyName);
		    for (xx = 0; xx < lineList.size(); xx++) {
			XSLine xsLine = (XSLine) lineList.get(xx);
			js = new JSONObject();
			js.put("title", "Line " + xsLine.getDisplaySeq());
			js.put("isFolder", false);
			js.put("objectId", "xsLine" + DELIM + xsLine.getXsSeqNo() + DELIM + xsLine.getXsCode() + DELIM + xsLine.getDisplaySeq());
			jsOut.put(js);
		    }
		} else if (fieldName.equals("options")) {
		    xsTypeList = insAdapter.getXSOptions(new Integer(entries[1]), companyName);
		    for (xx = 0; xx < xsTypeList.size(); xx++) {
			XSOption xso = (XSOption) xsTypeList.get(xx);
			js = new JSONObject();
			String xsAmtStr = null;
			if (xso.getXsAmount() == null)
			    xsAmtStr = "";
			else
			    xsAmtStr = xso.getXsAmount() + "";
			if (xso.getUnit() != null) {
			    if (xso.getUnit().equals("$"))
				xsAmtStr = "$" + xsAmtStr;
			    else
				xsAmtStr += " " + xso.getUnit();
			} else
			    xsAmtStr = "$" + xsAmtStr;
			xsAmtStr = xsAmtStr.trim();
			js.put("title", xsAmtStr);
			js.put("isFolder", false);
			js.put("objectId", "excessAmt" + DELIM + xso.getOptNo() + DELIM + xso.getXsSeqNo());
			jsOut.put(js);
		    }
		}
	    }
	    // System.out.println(jsOut.toString());
	    response.getWriter().write(jsOut.toString());
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException(ex);
	}

    }

    public void handleEvent_getExcessDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	String retString = null;
	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	    String objectId = request.getParameter("objectId");
	    String companyName = SourceSystem.INSURANCE_RACT.getAbbreviation();
	    JSONArray jsOut = new JSONArray();
	    JSONObject js = null;
	    String[] keys = objectId.split(DELIM);
	    String obType = keys[0];
	    if (obType.equals("xsSeqNo")) {
		XSType xst = insAdapter.getXSType(new Integer(keys[1]), companyName);
		retString = xst.toJSONArray();
	    } else if (obType.equals("excess")) {
		XSDetail xsd = insAdapter.getXSDetail(new Integer(keys[1]), keys[2], companyName);
		retString = xsd.toJSONArray();
	    } else if (obType.equals("excessAmt")) {
		// LogUtil.log(this.getClass()+".handleEvent_getExcessDetail",
		// "objectId =  " + objectId);

		Integer xsAmt = null;
		XSOption xso = insAdapter.getXSOption(new Integer(keys[1]), companyName);
		retString = xso.toJSONArray();
	    } else if (obType.equals("xsLine")) {
		/*
		 * + xsLine.getXsSeqNo()+DELIM + xsLine.getXsCode() + DELIM +
		 * xsLine.getDisplaySeq());
		 */
		XSLine xsl = insAdapter.getXsLine(new Integer(keys[1]), keys[2], new Integer(keys[3]), companyName);
		retString = xsl.toJSONArray();
	    } else if (obType.equals("newType")) {

	    }
	    if (retString != null)
		response.getWriter().write(retString);

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw new ServletException(ex);
	}
    }

    public void handleEvent_saveXsType(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ParseException {
	String xssNoString = request.getParameter("xsSeqNo");
	String riskType = request.getParameter("riskType");
	String edString = request.getParameter("effDate");
	String xsType = request.getParameter("xsType");
	String xsHeading = request.getParameter("xsHeading");
	String pinString = request.getParameter("printIfNil");
	String resString = request.getParameter("restrict");
	String requestType = request.getParameter("requestType");
	String requiredString = request.getParameter("required");
	String appendText = request.getParameter("appendText");
	String displayOrderString = request.getParameter("displayOrder");
	String reducibleString = request.getParameter("reducible");
	String exclude = request.getParameter("exclude");
	String userId = request.getParameter("userId");
	String prodType = request.getParameter("prodType");

	LogUtil.log(this.getClass() + ".handleEvent_saveXsType", "\n xssNoString        = " + xssNoString + "\n riskType           = " + riskType + "\n edString           = " + edString + "\n xsType             = " + xsType + "\n xsHeading          = " + xsHeading + "\n pinString          = " + pinString + "\n resString          = " + resString + "\n requestType        = " + requestType + "\n requiredString     = " + requiredString + "\n appendText         = " + appendText + "\n displayOrderString =" + displayOrderString + "\n reducibleString    = " + reducibleString + "\n exclude            = " + exclude + "\n userId             = " + userId + "\n prodType           = " + prodType);

	Integer oldSeqNo = null;
	String companyName = "INS_RAC";
	XSType xst = new XSType();
	xst.setAppendText(appendText);
	if (displayOrderString == null)
	    displayOrderString = "0";
	try {
	    xst.setDisplayOrder(new Integer(displayOrderString));
	} catch (Exception e1) {
	    xst.setDisplayOrder(new Integer(0));
	}
	if (reducibleString == null || reducibleString.equals("off"))
	    xst.setReducible(FALSE);
	else
	    xst.setReducible(TRUE);
	if (xssNoString != null)
	    if (!xssNoString.equalsIgnoreCase("null")) {
		oldSeqNo = new Integer(xssNoString);
		xst.setXsSeqNo(new Integer(xssNoString));
	    }
	if (xst.getXsSeqNo() == null)
	    xst.setXsSeqNo(new Integer(-1));
	if (prodType == null || prodType.equals(""))
	    throw new SystemException("Product type can not be empty (should be PIP,50+ or TSP");
	else
	    xst.setProdType(prodType);
	if (riskType == null || riskType.equals(""))
	    throw new SystemException("Risk Type can not be empty");
	else
	    xst.setRiskType(riskType);
	if (edString == null || edString.equals(""))
	    throw new SystemException("Effective date must be supplied");
	else
	    xst.setEffDate(new DateTime(edString));
	if (xsType == null || xsType.equals(""))
	    throw new SystemException("Excess type code not supplied");
	else
	    xst.setXsType(xsType);
	if (xsHeading == null || xsHeading.equals(""))
	    throw new SystemException("Excess heading not supplied");
	else
	    xst.setXsHeading(xsHeading);
	if (pinString == null || pinString.equals("off"))
	    xst.setPrintIfNil(FALSE);
	else
	    xst.setPrintIfNil(TRUE);
	if (resString == null || resString.equals("off"))
	    xst.setRestricted(FALSE);
	else
	    xst.setRestricted(TRUE);
	if (requiredString == null || requiredString.equals("off"))
	    xst.setRequired(FALSE);
	else
	    xst.setRequired(TRUE);
	xst.setCreateDate(new DateTime());
	xst.setCreateId(userId);
	xst.setExclude(exclude);

	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	    if (requestType.equalsIgnoreCase("newVersion")) {
		insAdapter.newXSVersion(xst, oldSeqNo, companyName);
	    } else {
		insAdapter.setXSType(xst, companyName);
	    }
	} catch (Exception rex) {
	    throw new SystemException("Unable to write XSType: \n", rex);
	}
    }

    public void handleEvent_setExcessOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ParseException {
	String xssNoString = request.getParameter("xsSeqNo");
	String startCountString = request.getParameter("startCount");
	int startCount = Integer.parseInt(startCountString);
	int row = startCount;
	row++;
	String xsAmtStr = request.getParameter("excess" + DELIM + row);
	String companyName = "INS_RAC";
	String rateValueStr;
	boolean more = xsAmtStr != null;
	XSOption xso = null;
	Integer xsSeqNo = new Integer(xssNoString);
	// LogUtil.log(this.getClass()+".handleEvent_setExcessOption",
	// "\nxsSeqNo      = " + xsSeqNo
	// + "\nstartCount  = " + startCount
	// + "\nxsAmtStr    = " + xsAmtStr);
	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();

	    while (more) {
		xso = new XSOption();
		xso.setXsSeqNo(xsSeqNo);
		xsAmtStr = request.getParameter("excess" + DELIM + row);
		if (xsAmtStr.startsWith("=")) {
		    xso.setUnit(xsAmtStr);
		} else {
		    if (!xsAmtStr.equals(""))
			xso.setXsAmount(new Integer(xsAmtStr));
		    xso.setUnit(request.getParameter("unit" + DELIM + row));
		}
		xso.setRatingType(request.getParameter("rType" + DELIM + row));
		rateValueStr = request.getParameter("rVal" + DELIM + row);
		try {
		    xso.setRateValue(new BigDecimal(rateValueStr));
		} catch (Exception ex) {
		    xso.setRateValue(new BigDecimal(0));
		}
		String replacesString = request.getParameter("rReplace" + DELIM + row);
		if (replacesString != null)
		    xso.setReplacesValues(replacesString);
		insAdapter.setXSOption(xso, companyName);
		/* now send it to the data base */
		row++;
		xsAmtStr = request.getParameter("excess" + DELIM + row);
		more = (xsAmtStr != null);
	    }
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
    }

    public void handleEvent_setExcessDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ParseException {
	String xssNoString = request.getParameter("xsSeqNo");
	String xsCode = request.getParameter("xsCode");
	String xsName = request.getParameter("xsName");
	String xsFixed = request.getParameter("fixed");
	String xsDocText = request.getParameter("docText");
	String xsHasText = request.getParameter("hasText");
	String xsIncTypes = request.getParameter("incidentTypes");
	String xsRateable = request.getParameter("rateable");
	String xsAmount = request.getParameter("xsAmount");
	String xsAMString = request.getParameter("allowMultiple");
	String unitStr = request.getParameter("xsUnit");
	String multiLineStr = request.getParameter("multiLine");
	String hasAmountStr = request.getParameter("hasAmount");
	String exclude = request.getParameter("exclude");
	String companyName = "INS_RAC";
	/*
	 * LogUtil.log(this.getClass(), "\nsetExcessDetail:--------------" +
	 * "\n xssNoString  = " + xssNoString + "\n xsCode       = " + xsCode +
	 * "\n xsName       = " + xsName + "\n xsFixed      = " + xsFixed +
	 * "\n xsAmount     = " + xsAmount + "\n xsDocText    = " + xsDocText +
	 * "\n xsHasText    = " + xsHasText + "\n xsIncTypes   = " + xsIncTypes
	 * + "\n xsRateable   = " + xsRateable + "\n xsAmString   = " +
	 * xsAMString + "\n unitStr      = " + unitStr + "\n exclude      = " +
	 * exclude );
	 */
	if (xsFixed == null)
	    xsFixed = "off";
	if (xsHasText == null)
	    xsHasText = "off";
	if (xsRateable == null)
	    xsRateable = "off";
	if (xsAMString == null)
	    xsAMString = "off";
	if (xsDocText != null)
	    xsDocText = xsDocText.trim();
	else
	    xsDocText = "";
	if (hasAmountStr == null)
	    hasAmountStr = "off";
	if (multiLineStr == null)
	    multiLineStr = "off";
	Integer xsSeqNo = new Integer(xssNoString);

	XSDetail xsd = new XSDetail();
	if (xsAmount != null && !xsAmount.equals(""))
	    xsd.setAmount(new Integer(xsAmount));
	xsd.setDocText(xsDocText);
	xsd.setFixed(new Boolean(xsFixed.equals("on")));
	xsd.setHasText(new Boolean(xsHasText.equals("on")));
	xsd.setIncidentTypes(xsIncTypes);
	xsd.setRateable(new Boolean(xsRateable.trim().equals("on")));
	xsd.setSeqNo(xsSeqNo);
	xsd.setXsCode(xsCode);
	xsd.setXsName(xsName);
	xsd.setAllowMultiples(new Boolean(xsAMString.trim().equals("on")));
	xsd.setXsUnit(unitStr);
	xsd.setHasAmount(new Boolean(hasAmountStr.trim().equals("on")));
	xsd.setMultiLine(new Boolean(multiLineStr.trim().equals("on")));
	xsd.setExclude(exclude);
	if (xsd.getFixed().booleanValue() && xsd.getRateable().booleanValue()) {
	    // options are incompatible currently. throw an error
	    throw new SystemException("Excess Details cannot be both fixed and rateable");
	}
	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	    insAdapter.setXSDetail(xsd, companyName);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
    }

    public void handleEvent_deleteExcessNode(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ParseException {
	response.setContentType("text/plain");
	response.setHeader("Cache-Control", "no-cache");

	String objectId = request.getParameter("objectId");
	String companyName = "INS_RAC";
	String[] items = objectId.split(DELIM);
	Integer xsSeqNo = null;
	DateTime today = new DateTime();
	String respString = "";
	XSType xst = null;
	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	    if (items[0].equalsIgnoreCase("xsSeqNo")) {
		xsSeqNo = new Integer(items[1]);
		xst = insAdapter.getXSType(xsSeqNo, companyName);
		// if(xst.getCreateDate().beforeDay(today)
		// || xst.getCreateDate().afterDay(today))
		// {
		// respString = "Error";
		// }
		// else
		// {
		insAdapter.deleteExcessType(xsSeqNo, companyName);
		// }
	    } else if (items[0].equalsIgnoreCase("excess")) {
		xsSeqNo = new Integer(items[1]);
		String excessCode = items[2];
		insAdapter.deleteExcessDet(xsSeqNo, excessCode, companyName);
	    } else if (items[0].equalsIgnoreCase("excessAmt")) {
		Integer optNo = new Integer(items[1]);
		xsSeqNo = new Integer(items[2]);
		xst = insAdapter.getXSType(xsSeqNo, companyName);
		// if(xst.getCreateDate().beforeDay(today)
		// || xst.getCreateDate().afterDay(today))
		// {
		// respString = "Error";
		// }
		// else
		// {
		insAdapter.deleteXSOption(optNo, companyName);
		// }
	    } else if (items[0].equalsIgnoreCase("xsLine")) {
		XSLine xsl = new XSLine();
		xsl.setXsSeqNo(new Integer(items[1]));
		xsl.setXsCode(items[2]);
		xsl.setDisplaySeq(new Integer(items[3]));
		insAdapter.deleteXsLine(xsl, companyName);
	    }
	    // doesn't actually seem to work
	    response.getWriter().write(respString);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
    }

    public void handleEvent_newXsTypeVersions(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ParseException {
	String userId = (String) request.getAttribute("userId");
	String objectId = request.getParameter("objectId");
	String newDateStr = request.getParameter("newEffDate");
	String companyName = "INS_RAC";
	LogUtil.log(this.getClass() + ".handleEvent_newXsTypeVersions", objectId);
	String[] t = objectId.split(DELIM);
	String riskType = t[1];
	String effDateString = t[2];
	String prodType = t[3];
	DateTime oldEffDate = new DateTime(effDateString);
	DateTime newEffDate = new DateTime(newDateStr);
	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	    insAdapter.createNewXSVersionsForRiskType(prodType, riskType, oldEffDate, newEffDate, userId, companyName);
	} catch (Exception ex) {
	    throw new SystemException("Error creating new versions");
	}
    }

    public void handleEvent_setExcessLines(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ParseException {
	String xssNoString = request.getParameter("xsSeqNo");
	String xsCode = request.getParameter("xsCode");
	String startCountString = request.getParameter("startCount");
	int startCount = Integer.parseInt(startCountString);
	int row = startCount;
	row++;
	String displaySeqStr = request.getParameter("displaySeq" + DELIM + row);
	String lineText = request.getParameter("lineText" + DELIM + row);
	String lineAmtStr = request.getParameter("lineAmount" + DELIM + row);
	boolean more = displaySeqStr != null;
	String companyName = "INS_RAC";
	XSLine xsl = null;
	Integer xsSeqNo = new Integer(xssNoString);
	// LogUtil.log(this.getClass().getName()+".handleEvent_setExcessLines",
	// "\nxsSeqNo    = " + xsSeqNo
	// + "\nxsCode     = " + xsCode
	// + "\nstartCount = " + startCount
	// + "\ndisplaySeq = " + displaySeqStr
	// + "\nlineText   = " + lineText
	// + "\nlineAmount = " + lineAmtStr);
	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();

	    while (more) {
		xsl = new XSLine();
		xsl.setXsSeqNo(xsSeqNo);
		xsl.setXsCode(xsCode);
		xsl.setDisplaySeq(new Integer(displaySeqStr));
		xsl.setLineText(lineText);
		xsl.setLineAmount(new Integer(lineAmtStr));
		insAdapter.setXsLine(xsl, companyName);
		/* now send it to the data base */
		row++;
		displaySeqStr = request.getParameter("displaySeq" + DELIM + row);
		lineText = request.getParameter("lineText" + DELIM + row);
		lineAmtStr = request.getParameter("lineAmount" + DELIM + row);
		more = displaySeqStr != null;
	    }
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}

    }

}
