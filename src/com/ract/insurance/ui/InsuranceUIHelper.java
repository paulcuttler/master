package com.ract.insurance.ui;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrBean;
import com.ract.common.ExceptionHelper;
import com.ract.common.ServiceLocator;
import com.ract.insurance.InsuranceEJBHelper;
import com.ract.insurance.InsuranceMgrBean;
import com.ract.insurance.InsuranceMgrLocal;
import com.ract.util.HTMLUtil;
import com.ract.util.LogUtil;

public class InsuranceUIHelper {

    public static String getSupplierSelectFromArrayList(int queryType, String searchTerm, String key) {
	LogUtil.log("InsuranceUIHelper", "1: " + queryType);
	ServiceLocator serviceLocator = null;
	InsuranceMgrLocal insuranceMgrLocal = null;
	CommonMgr commonMgr = null;
	Collection list = null;
	String name = "";
	try {
	    insuranceMgrLocal = InsuranceEJBHelper.getInsuranceMgrLocal();
	    commonMgr = CommonEJBHelper.getCommonMgr();
	    LogUtil.log("InsuranceUIHelper", "2");
	    switch (queryType) {
	    case InsuranceMgrBean.CRITERIA_CLASS:
		name = "payeeClass";
		list = insuranceMgrLocal.getSupplierClassesByRegion(key);
		break;
	    case InsuranceMgrBean.CRITERIA_CATEGORY:
		name = "category";

		break;
	    case InsuranceMgrBean.CRITERIA_REGION:
		name = "region";
		list = insuranceMgrLocal.getSupplierRegions();
		break;
	    case InsuranceMgrBean.CRITERIA_TYPE:
		name = "type";
		list = insuranceMgrLocal.getSupplierTypes();
		break;
	    case InsuranceMgrBean.CRITERIA_SORT:
		name = "sortBy";
		list = insuranceMgrLocal.getSupplierSorts();
		break;
	    case CommonMgrBean.CRITERIA_RESULTS:
		name = "pageSize";
		list = commonMgr.getResultsPerPage();
		break;
	    }
	    LogUtil.log("InsuranceUIHelper", "3");
	} catch (RemoteException re) {
	    LogUtil.log("InsuranceUIHelper", ExceptionHelper.getExceptionStackTrace(re));
	}
	LogUtil.log("InsuranceUIHelper", "4");

	// if class put in onchange javascript
	String javascript = "";
	if (name.equals("payeeClass")) {
	    javascript = "showCategories(this)";
	}
	if (name.equals("region")) {
	    javascript = "showClasses(this)";
	}
	if (name.equals("category")) {
	    javascript = "setCategory(this)";
	}
	return HTMLUtil.getSelectList(queryType, searchTerm, list, name, javascript);
    }

    public static boolean isPayeeValueEmpty(String value) {
	boolean empty = false;
	String retVal;
	final char NBSP = 160; // embedded in payee file
	if (value != null) {
	    retVal = value.replace(NBSP, ' ');
	    retVal = retVal.trim();
	} else {
	    retVal = "";
	}
	// if not empty
	if (retVal.equals("")) {
	    empty = true;
	}
	return empty;
    }

    // build a navigator
    public static String getPayeeNavigationBar(int startPage, int pageSize, int totalElementSize, int currentPage, int pages, String payeeName, String ABN, String region, String payeeClass, String category, String type, String sortBy) {
	String html = "";

	/** @todo use constants */
	final String navigationURLPrefix = "/InsuranceUIC?event=searchSuppliers";

	String navigationURL = null;

	Hashtable params = null;

	int startIndex = 0;
	int endIndex = 0;

	// start at the start

	for (int pageNumber = 1; pageNumber <= pages; pageNumber++) {

	    startIndex = ((pageNumber * pageSize) - (pageSize)) + 1;

	    params = new Hashtable();
	    params.put("startPage", new Integer(startIndex));
	    params.put("pageSize", new Integer(pageSize));
	    params.put("totalElementSize", new Integer(totalElementSize));
	    if (payeeName != null) {
		params.put("payeeName", payeeName);
	    }
	    if (ABN != null) {
		params.put("ABN", ABN);
	    }
	    if (region != null) {
		params.put("region", region);
	    }
	    if (payeeClass != null) {
		params.put("payeeClass", payeeClass);
	    }
	    if (category != null) {
		params.put("category", category);
	    }
	    if (type != null) {
		params.put("type", type);
	    }
	    if (sortBy != null) {
		params.put("sortBy", sortBy);
	    }
	    navigationURL = navigationURLPrefix + HTMLUtil.appendURLParameters(params);
	    if (pageNumber == currentPage) {
		html += pageNumber;
	    } else {
		html += "<a href=\"" + navigationURL + "\">" + pageNumber + "</a>";
	    }
	    if (pages != 1 && pageNumber != pages) {
		html += " | ";
	    }
	}
	return html;
    }
}
