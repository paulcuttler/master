package com.ract.insurance;

import java.io.Serializable;

public class XSDetail implements Serializable {
    Integer seqNo;
    private String xsCode = "";
    private String xsName = "";
    private Boolean fixed = new Boolean(false);
    private Integer amount = new Integer(0);
    private Boolean hasText = new Boolean(false);
    private String docText = "";
    private Boolean rateable = new Boolean(false);
    private String incidentTypes = "";
    private Boolean allowMultiples;
    private String xsUnit = "$";
    private Boolean hasAmount;
    private Boolean multiLine;
    private String exclude;

    public Integer getSeqNo() {
	return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
	this.seqNo = seqNo;
    }

    public String getXsCode() {
	return xsCode;
    }

    public void setXsCode(String xsCode) {
	this.xsCode = xsCode;
    }

    public String getXsName() {
	return xsName;
    }

    public void setXsName(String xsName) {
	this.xsName = xsName;
    }

    public Boolean getFixed() {
	return fixed;
    }

    public void setFixed(Boolean fixed) {
	this.fixed = fixed;
    }

    public Integer getAmount() {
	return amount;
    }

    public void setAmount(Integer amount) {
	this.amount = amount;
    }

    public Boolean getHasText() {
	return hasText;
    }

    public void setHasText(Boolean hasText) {
	this.hasText = hasText;
    }

    public String getDocText() {
	return docText;
    }

    public void setDocText(String docText) {
	this.docText = docText;
    }

    public Boolean getRateable() {
	return rateable;
    }

    public void setRateable(Boolean rateable) {
	this.rateable = rateable;
    }

    public String getIncidentTypes() {
	return incidentTypes;
    }

    public void setIncidentTypes(String incidentTypes) {
	this.incidentTypes = incidentTypes;
    }

    public String toJSONArray() {
	String dText = this.docText;
	// need to replace tags in doc text with CDATA sections or &lt;, &gt;
	dText = dText.replaceAll("<", "&lt;");
	dText = dText.replaceAll(">", "&gt;");

	String os = "[{\"title\":\"Sequence No\",\"value\":\"" + this.seqNo + "\"}," + "{\"title\":\"Excess Code\",\"value\":\"" + this.xsCode + "\"}," + " {\"title\":\"Excess Name\",\"value\":\"" + this.xsName + "\"}," + " {\"title\":\"Fixed\",\"value\":\"" + this.fixed + "\"}," + " {\"title\":\"Amount\",\"value\":\"" + this.amount + "\"}," + " {\"title\":\"Unit\",\"value\":\"" + this.xsUnit + "\"}," + " {\"title\":\"Has text field\",\"value\":\"" + this.hasText + "\"}," + " {\"title\":\"Has Amount field\",\"value\":\"" + this.hasAmount + "\"}," + " {\"title\":\"Document Text\",\"value\":\"" + dText + "\"}," + " {\"title\":\"Rateable\",\"value\":\"" + this.rateable + "\"}," + " {\"title\":\"Incident Types\",\"value\":\"" + this.incidentTypes + "\"}," + " {\"title\":\"Allow Multiples\",\"value\":\"" + this.allowMultiples + "\"}," + " {\"title\":\"Multi line\",\"value\":\"" + this.multiLine + "\"}," + " {\"title\":\"Exclude\",\"value\":\"" + this.exclude + "\"}" + "]";
	return os;
    }

    /*
     * Integer seqNo; private String xsCode; private String xsName; private
     * Boolean fixed; private Integer amount; private Boolean hasText; private
     * String docText; private Boolean rateable; private String incidentTypes;
     */

    public String toString() {
	String os = "\nSequence No        " + this.seqNo + "\nExcess Code      " + this.xsCode + "\nExcess Name      " + this.xsName + "\nFixed            " + this.fixed + "\namount           " + this.amount + "\nunit             " + this.xsUnit + "\nHas text field   " + this.hasText + "\nHas amount field " + this.hasAmount + "\nDocument Text    " + this.docText + "\nrateable         " + this.rateable + "\nincidentTypes    " + this.incidentTypes + "\nallowMultiples   " + this.allowMultiples + "\nmulti line       " + this.multiLine + "\nexclude          " + this.exclude;
	return os;
    }

    public Boolean getAllowMultiples() {
	return allowMultiples;
    }

    public void setAllowMultiples(Boolean allowMultiples) {
	this.allowMultiples = allowMultiples;
    }

    public String getXsUnit() {
	return xsUnit;
    }

    public void setXsUnit(String xsUnit) {
	this.xsUnit = xsUnit;
    }

    public Boolean getHasAmount() {
	return hasAmount;
    }

    public void setHasAmount(Boolean hasAmount) {
	this.hasAmount = hasAmount;
    }

    public Boolean getMultiLine() {
	return multiLine;
    }

    public void setMultiLine(Boolean multiLine) {
	this.multiLine = multiLine;
    }

    public String getExclude() {
	return exclude;
    }

    public void setExclude(String exclude) {
	this.exclude = exclude;
    }

}
