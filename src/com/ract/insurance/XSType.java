package com.ract.insurance;

import java.io.Serializable;

import com.ract.util.DateTime;

public class XSType implements Serializable {
    private Integer xsSeqNo;
    private String prodType;
    private String riskType;
    private String xsType;
    private com.ract.util.DateTime effDate;
    private String xsHeading;
    private Boolean printIfNil;
    private Boolean restricted;
    private com.ract.util.DateTime createDate;
    private String createId;
    private Boolean required;
    private String appendText;
    private Integer displayOrder;
    private Boolean reducible;
    private String exclude;

    public XSType cloneXsType() {
	XSType xst = new XSType();
	xst.xsSeqNo = this.xsSeqNo;
	xst.riskType = this.riskType;
	xst.xsType = this.xsType;
	xst.effDate = new DateTime(this.effDate);
	xst.xsHeading = this.xsHeading;
	xst.printIfNil = new Boolean(this.printIfNil.booleanValue());
	xst.restricted = new Boolean(this.restricted.booleanValue());
	xst.createDate = new DateTime(this.createDate);
	xst.createId = this.createId;
	xst.required = new Boolean(this.required.booleanValue());
	xst.appendText = this.appendText;
	xst.displayOrder = this.displayOrder;
	xst.reducible = new Boolean(this.reducible.booleanValue());
	xst.exclude = this.exclude;
	xst.prodType = this.prodType;
	return xst;
    }

    public Integer getXsSeqNo() {
	return xsSeqNo;
    }

    public void setXsSeqNo(Integer xsSeqNo) {
	this.xsSeqNo = xsSeqNo;
    }

    public String getRiskType() {
	return riskType;
    }

    public void setRiskType(String riskType) {
	this.riskType = riskType;
    }

    public String getXsType() {
	return xsType;
    }

    public void setXsType(String xsType) {
	this.xsType = xsType;
    }

    public com.ract.util.DateTime getEffDate() {
	return effDate;
    }

    public void setEffDate(com.ract.util.DateTime effDate) {
	this.effDate = effDate;
    }

    public String getXsHeading() {
	return xsHeading;
    }

    public void setXsHeading(String xsHeading) {
	this.xsHeading = xsHeading;
    }

    public Boolean getPrintIfNil() {
	return printIfNil;
    }

    public void setPrintIfNil(Boolean printIfNil) {
	this.printIfNil = printIfNil;
    }

    public Boolean getRestricted() {
	return restricted;
    }

    public void setRestricted(Boolean restricted) {
	this.restricted = restricted;
    }

    public com.ract.util.DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(com.ract.util.DateTime createDate) {
	this.createDate = createDate;
    }

    public String getCreateId() {
	return createId;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public String toJSONArray() {
	String os = "[{\"title\":\"Sequence No\",\"value\":\"" + xsSeqNo + "\"}," + "{\"title\":\"Prod Type\",\"value\":\"" + this.prodType + "\"}," + "{\"title\":\"Risk Type\",\"value\":\"" + this.riskType + "\"}," + " {\"title\":\"Excess Type\",\"value\":\"" + this.xsType + "\"}," + " {\"title\":\"Effective Date\",\"value\":\"" + this.effDate + "\"}," + " {\"title\":\"Append Text\",\"value\":\"" + this.appendText + "\"}," + " {\"title\":\"Doc Heading\",\"value\":\"" + this.xsHeading + "\"}," + " {\"title\":\"Print if Nil\",\"value\":\"" + this.printIfNil + "\"}," + " {\"title\":\"Restricted Access\",\"value\":\"" + this.restricted + "\"}," + " {\"title\":\"Create Date\",\"value\":\"" + this.createDate + "\"}," + " {\"title\":\"Create Id\",\"value\":\"" + this.createId + "\"}," + " {\"title\":\"Display Order\",\"value\":\"" + this.displayOrder + "\"}," + " {\"title\":\"Required\",\"value\":\"" + this.required + "\"}," + " {\"title\":\"Reducible\",\"value\":\"" + this.reducible + "\"},"
		+ " {\"title\":\"Exclude\",\"value\":\"" + this.exclude + "\"}" + "]";
	return os;
    }

    public String toString() {
	return "\nSequence No       " + xsSeqNo + "\nProd Type         " + this.prodType + "\nRisk Type         " + this.riskType + "\nExcess Type       " + this.xsType + "\nEffective Date    " + this.effDate + "\nAppend Text       " + this.appendText + "\nHeading           " + this.xsHeading + "\nPrint if Nil      " + this.printIfNil + "\nRestricted Access " + this.restricted + "\nCreate Date       " + this.createDate + "\nCreate Id         " + this.createId + "\nRequired          " + this.required + "\nDisplay Order     " + this.displayOrder + "\nReducible         " + this.reducible + "\nExclude           " + this.exclude;
    }

    public Boolean getRequired() {
	return required;
    }

    public void setRequired(Boolean required) {
	this.required = required;
    }

    public String getAppendText() {
	return appendText;
    }

    public void setAppendText(String appendText) {
	this.appendText = appendText;
    }

    public Integer getDisplayOrder() {
	return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
	this.displayOrder = displayOrder;
    }

    public Boolean getReducible() {
	return reducible;
    }

    public void setReducible(Boolean reducible) {
	this.reducible = reducible;
    }

    public String getExclude() {
	return exclude;
    }

    public void setExclude(String exclude) {
	this.exclude = exclude;
    }

    public void setProdType(String pType) {
	this.prodType = pType;
    }

    public String getProdType() {
	return this.prodType;
    }

}
