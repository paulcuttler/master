package com.ract.insurance;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

public class InsuranceEJBHelper {

    // public static InsuranceMgr getInsuranceMgr()
    // {
    // InsuranceMgr insuranceMgr = null;
    // try
    // {
    // insuranceMgr =
    // (InsuranceMgr)ServiceLocator.getInstance().getObject("InsuranceMgrBean/remote");
    // }
    // catch (ServiceLocatorException e)
    // {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // return insuranceMgr;
    // }

    public static InsuranceMgrLocal getInsuranceMgrLocal() {
	InsuranceMgrLocal insuranceMgrLocal = null;
	try {
	    insuranceMgrLocal = (InsuranceMgrLocal) ServiceLocator.getInstance().getObject("InsuranceMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return insuranceMgrLocal;
    }

    public static InsuranceTransactionMgrRemote getInsuranceTransactionMgr() {
	InsuranceTransactionMgrRemote it = null;
	try {
	    it = (InsuranceTransactionMgrRemote) ServiceLocator.getInstance().getObject("InsuranceTransactionMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    e.printStackTrace();
	}
	return it;
    }

    public static InsuranceTransactionMgrLocal getInsuranceTransactionMgrLocal() {
	InsuranceTransactionMgrLocal it = null;
	try {
	    it = (InsuranceTransactionMgrLocal) ServiceLocator.getInstance().getObject("InsuranceTransactionMgrBean/Local");
	} catch (ServiceLocatorException e) {
	    e.printStackTrace();
	}
	return it;
    }

}
