package com.ract.insurance;

import java.io.File;
import java.io.FileInputStream;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;

public class UploadSupplierFileAction extends ActionSupport {

    public File getUpfile() {
	return upfile;
    }

    public void setUpfile(File upfile) {
	this.upfile = upfile;
    }

    public String getUpfileFileName() {
	return upfileFileName;
    }

    public void setUpfileFileName(String upfileFileName) {
	this.upfileFileName = upfileFileName;
    }

    public Double getFileSize() {
	return fileSize;
    }

    public void setFileSize(Double fileSize) {
	this.fileSize = fileSize;
    }

    public Integer getRecordCount() {
	return recordCount;
    }

    public void setRecordCount(Integer recordCount) {
	this.recordCount = recordCount;
    }

    @InjectEJB(name = "InsuranceMgrBean")
    private InsuranceMgrLocal insMgrLocal;

    private File upfile;

    private String upfileFileName;

    private Double fileSize;

    private Integer recordCount;

    @Override
    public String execute() throws Exception {
	recordCount = insMgrLocal.uploadPayeesFile(new FileInputStream(upfile));
	fileSize = new Double(upfile.length() / 1024);
	return SUCCESS;
    }

}
