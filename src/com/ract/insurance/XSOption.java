package com.ract.insurance;

import java.io.Serializable;
import java.math.BigDecimal;

public class XSOption implements Serializable {
    private Integer xsSeqNo;
    private Integer xsAmount;
    private String ratingType;
    private BigDecimal rateValue;
    private String unit;
    private Integer optNo;
    private String replacesValues;

    public Integer getXsSeqNo() {
	return xsSeqNo;
    }

    public void setXsSeqNo(Integer xsSeqNo) {
	this.xsSeqNo = xsSeqNo;
    }

    public Integer getXsAmount() {
	return xsAmount;
    }

    public void setXsAmount(Integer xsAmount) {
	this.xsAmount = xsAmount;
    }

    public String getRatingType() {
	return ratingType;
    }

    public void setRatingType(String ratingType) {
	this.ratingType = ratingType;
    }

    public BigDecimal getRateValue() {
	return rateValue;
    }

    public void setRateValue(BigDecimal rateValue) {
	this.rateValue = rateValue;
    }

    public String toJSONArray() {
	String os = "[{\"title\":\"Sequence No\",\"value\":\"" + this.xsSeqNo + "\"}," + "{\"title\":\"Option No\",\"value\":\"" + this.optNo + "\"}," + "{\"title\":\"Excess Amount\",\"value\":\"" + this.xsAmount + "\"}," + "{\"title\":\"Unit\",\"value\":\"" + this.unit + "\"}," + " {\"title\":\"Rating Type\",\"value\":\"" + this.ratingType + "\"}," + " {\"title\":\"Rating Value\",\"value\":\"" + this.rateValue + "\"}," + " {\"title\":\"Replaces Values\",\"value\":\"" + this.replacesValues + "\"}]";

	return os;
    }

    public String toString() {
	return "Sequence No   = " + this.xsSeqNo + "\nOption No     = " + this.optNo + "\nExcess Amount = " + this.xsAmount + "\nUnit          = " + this.unit + "\nRating Type   = " + this.ratingType + "\nRating Value  = " + this.rateValue + "\nReplaces Values = " + this.replacesValues;
    }

    public String getUnit() {
	return unit;
    }

    public void setUnit(String unit) {
	this.unit = unit;
    }

    public Integer getOptNo() {
	return optNo;
    }

    public void setOptNo(Integer optNo) {
	this.optNo = optNo;
    }

    public String getReplacesValues() {
	return replacesValues;
    }

    public void setReplacesValues(String replacesValues) {
	this.replacesValues = replacesValues;
    }
}
