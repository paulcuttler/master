package com.ract.insurance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.hibernate.HibernatePage;
import com.ract.util.ConnectionUtil;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;
import com.ract.util.XMLHelper;

@Stateless
@Remote({ InsuranceMgr.class })
@Local({ InsuranceMgrLocal.class })
public class InsuranceMgrBean {

    InsuranceAdapter pAdapter;

    @PersistenceContext(unitName = "master")
    Session hsession;

    public int uploadPayeesFile(InputStream inputStream) throws RemoteException {
	// delete all records first
	Payee payee = null;
	List payeeList = (hsession.createCriteria(Payee.class)).list();
	if (payeeList != null && payeeList.size() > 0) {
	    Iterator payeeIt = payeeList.iterator();
	    while (payeeIt.hasNext()) {
		payee = (Payee) payeeIt.next();
		hsession.delete(payee);
	    }
	}

	// reinitialise
	payee = null;

	BufferedReader br = null;
	int recordCount = 0;
	int successCount = 0;
	final int COLUMN_COUNT = 19;
	final String HEADER_COLUMN = "index";
	try {
	    String line = null;
	    String payeeLine[] = null;
	    br = new BufferedReader(new InputStreamReader(inputStream));
	    while ((line = br.readLine()) != null) {
		recordCount++;
		if (!line.startsWith(HEADER_COLUMN)) {
		    // replace quotes
		    line = StringUtil.replaceAll(line, "\"", "");
		    LogUtil.debug(this.getClass(), recordCount + ": " + line);
		    payeeLine = line.split(FileUtil.TAB);
		    if (payeeLine.length == 0) {
			// skip blank lines
			continue;
		    } else if (payeeLine.length != COLUMN_COUNT) {
			LogUtil.warn(this.getClass(), "File does not contain the correct number of columns - " + COLUMN_COUNT + ". Column count for record " + recordCount + " was " + payeeLine.length + "\n " + line);
			continue;
		    }
		    payee = new Payee();
		    // set the data
		    payee.setPayeeClass(payeeLine[0]);
		    payee.setPayeeName(payeeLine[3]);
		    payee.setDescription(payeeLine[4]);
		    payee.setRegion(payeeLine[5]);
		    payee.setCategory(payeeLine[6]);
		    payee.setType(payeeLine[7]);
		    payee.setAddress(payeeLine[8]);
		    payee.setContactName(payeeLine[9]);
		    payee.setPhone(payeeLine[10]);
		    payee.setMobile(payeeLine[11]);
		    payee.setFax(payeeLine[12]);
		    payee.setEmail(payeeLine[13]);
		    payee.setProperty(payeeLine[14]);
		    payee.setABN(payeeLine[15]);
		    payee.setComment(payeeLine[16]);
		    payee.setPayeeCode(payeeLine[17]);
		    payee.setPartyCode(payeeLine[18]);
		    LogUtil.log(this.getClass(), "rec=" + recordCount + " " + line);
		    hsession.save(payee);
		    successCount++;
		} else {
		    if (recordCount != 1) {
			LogUtil.warn(this.getClass(), "File is not in the expected format.");
			continue;
		    }
		}
	    }
	} catch (IOException ex) {
	    throw new RemoteException("Unable to process payee file.", ex);
	}
	return successCount;
    }

    public static final int CRITERIA_NAME = 1;
    public static final int CRITERIA_ABN = 2;
    public static final int CRITERIA_REGION = 3;
    public static final int CRITERIA_CLASS = 4;
    public static final int CRITERIA_TYPE = 5;
    public static final int CRITERIA_CATEGORY = 6;
    public static final int CRITERIA_SORT = 7;

    public Collection getSupplierSorts() throws RemoteException {
	ArrayList sorts = new ArrayList();
	sorts.add("comment");
	sorts.add("payeeName");
	sorts.add("region");
	sorts.add("category");
	sorts.add("type");
	return sorts;
    }

    public HibernatePage getPayees(Hashtable criteria, int startPage, int pageSize, String sortBy) throws RemoteException {
	// LogUtil.debug(this.getClass(),"criteria="+criteria);
	// LogUtil.debug(this.getClass(),"startPage="+startPage);
	// LogUtil.debug(this.getClass(),"pageSize="+pageSize);

	HibernatePage payeeList = null;

	// default crit
	Criteria crit = hsession.createCriteria(Payee.class);

	String criteriaName = (String) criteria.get(new Integer(CRITERIA_NAME));
	if (criteriaName != null) {
	    crit.add(Expression.like("payeeName", CommonConstants.WILDCARD + criteriaName + CommonConstants.WILDCARD));
	}
	String criteriaABN = (String) criteria.get(new Integer(CRITERIA_ABN));
	if (criteriaABN != null) {
	    crit.add(Expression.like("ABN", CommonConstants.WILDCARD + criteriaABN + CommonConstants.WILDCARD));
	}
	String criteriaClass = (String) criteria.get(new Integer(CRITERIA_CLASS));
	if (criteriaClass != null) {
	    crit.add(Expression.eq("payeeClass", criteriaClass));
	}
	String criteriaRegion = (String) criteria.get(new Integer(CRITERIA_REGION));
	if (criteriaRegion != null) {
	    crit.add(Expression.eq("region", criteriaRegion));
	}
	String criteriaCategory = (String) criteria.get(new Integer(CRITERIA_CATEGORY));
	if (criteriaCategory != null) {
	    crit.add(Expression.eq("category", criteriaCategory));
	}
	String criteriaType = (String) criteria.get(new Integer(CRITERIA_TYPE));
	if (criteriaType != null) {
	    crit.add(Expression.eq("type", criteriaType));
	}

	// sort by name
	if (sortBy != null) {
	    LogUtil.log(this.getClass(), "sortBy='" + sortBy + "'");
	    crit.addOrder(Order.asc(sortBy));
	}

	payeeList = new HibernatePage(crit, startPage, pageSize);

	return payeeList;
    }

    public Hashtable getClassCategories(String supplierClass) {
	Hashtable classSuppliers = new Hashtable();
	final String SQL_SUPPLIER_CLASS_CATEGORY = SQL_SUPPLIER_CATEGORY + " where supplier_class = ?";

	return null;
    }

    private final String SQL_SUPPLIER_REGION = "select distinct region from PUB.ins_payee";
    private final String SQL_SUPPLIER_CLASS = "select distinct class from PUB.ins_payee";
    private final String SQL_SUPPLIER_TYPE = "select distinct type from PUB.ins_payee";
    private final String SQL_SUPPLIER_CATEGORY = "select distinct category from PUB.ins_payee";

    @Resource(mappedName = "java:/ClientDS")
    private DataSource dataSource;

    public Collection getSupplierRegions() throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getStringResultArray(SQL_SUPPLIER_REGION);
    }

    public Collection getSupplierClasses() throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getStringResultArray(SQL_SUPPLIER_CLASS);
    }

    public Collection getSupplierClassesByRegion(String region) throws RemoteException {
	String query = SQL_SUPPLIER_CLASS;
	if (region != null && !region.trim().equals("")) {
	    query += " where region = '" + region + "'";
	}
	return CommonEJBHelper.getCommonMgr().getStringResultArray(query);
    }

    public Collection getSupplierTypes() throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getStringResultArray(SQL_SUPPLIER_TYPE);
    }

    public Collection getSupplierCategories() throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getStringResultArray(SQL_SUPPLIER_CATEGORY);
    }

    public Collection getSupplierCategoriesByClass(String payeeClass) throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getStringResultArray(SQL_SUPPLIER_CATEGORY + " where payeeClass = '" + payeeClass + "'");
    }

    public String getSupplierClassesXMLByRegion(String region) throws RemoteException {
	// get distinct categories where class = payeeClass return as xml string
	Connection connection = null;
	String sql = "select distinct ins.class from PUB.ins_payee ins";

	if (region != null && !region.trim().equals("")) {
	    sql += " where ins.region = ?";
	}

	LogUtil.debug(this.getClass(), "getSupplierClassesByRegion '" + region + "'");
	Document document = null;
	PreparedStatement statement = null;
	try {
	    Element categoryElement = null;
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	    DocumentBuilder builder = factory.newDocumentBuilder();
	    document = builder.newDocument();

	    Element root = (Element) document.createElement("payeeClassList");
	    document.appendChild(root);

	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    if (region != null && !region.trim().equals("")) {
		statement.setString(1, region);
	    }

	    ResultSet rs = statement.executeQuery();
	    // LogUtil.log(this.getClass(),"execute query ");
	    String payeeClass = null;
	    while (rs.next()) {
		payeeClass = rs.getString(1);
		categoryElement = document.createElement("payeeClass");
		root.appendChild(categoryElement);
		categoryElement.appendChild(document.createCDATASection(payeeClass));
		LogUtil.log(this.getClass(), "payeeClass=" + payeeClass);
	    }
	} catch (Exception se) {
	    se.printStackTrace();
	    throw new RemoteException("Unable to get payee class list for region '" + region + "'. " + sql, se);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return XMLHelper.toXML(document.getFirstChild());
    }

    public String getSupplierCategoriesXMLByClass(String payeeClass) throws RemoteException {
	// get distinct categories where class = payeeClass return as xml string
	Connection connection = null;
	String sql = "select distinct ins.category from PUB.ins_payee ins where ins.class = ?";
	LogUtil.debug(this.getClass(), "getSupplierCategoriesByClass '" + payeeClass + "'");
	Document document = null;
	PreparedStatement statement = null;
	try {
	    Element categoryElement = null;
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	    DocumentBuilder builder = factory.newDocumentBuilder();
	    document = builder.newDocument();

	    Element root = (Element) document.createElement("categoryList");
	    document.appendChild(root);

	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(sql);
	    statement.setString(1, payeeClass);
	    ResultSet rs = statement.executeQuery();
	    // LogUtil.log(this.getClass(),"execute query ");
	    String category = null;
	    while (rs.next()) {
		category = rs.getString(1);
		categoryElement = document.createElement("category");
		root.appendChild(categoryElement);
		categoryElement.appendChild(document.createCDATASection(category));
		LogUtil.log(this.getClass(), "category=" + category);
	    }
	} catch (Exception se) {
	    se.printStackTrace();
	    throw new RemoteException("Unable to get category list for payee class '" + payeeClass + "'. " + sql, se);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return XMLHelper.toXML(document.getFirstChild());
    }

    public ArrayList getRFDates(String companyName) throws RemoteException {
	ArrayList outList = null;
	outList = InsuranceFactory.getInsuranceAdapter().getRFDates(companyName);
	return outList;
    }

    public ArrayList getRFDetails(String companyName) throws RemoteException {
	ArrayList outList = null;
	outList = InsuranceFactory.getInsuranceAdapter().getRFDetails(companyName);
	return outList;
    }

    public void setRFData(String companyName, ArrayList datesList, ArrayList detailsList) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	insAdapter.setRFData(companyName, datesList, detailsList);
    }

    public ArrayList getMessageText(String companyName) throws RemoteException {
	ArrayList outList = null;
	outList = InsuranceFactory.getInsuranceAdapter().getMessageText(companyName);
	return outList;
    }

    public void setMessageText(String companyName, ArrayList mtList) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	insAdapter.setMessageText(companyName, mtList);
    }

    public void setSuburbList(String companyName, ArrayList sl) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	insAdapter.setSuburbList(companyName, sl);

    }

    public ArrayList getSuburbList(String companyName, String extType) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return insAdapter.getSuburbList(companyName, extType);
    }

    public String setInvalidVehicle(String companyName, String action, String nvic, String userId) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return insAdapter.setInvalidVehicle(companyName, action, nvic, userId);
    }

    public String setGLCode(String companyName, String tableName, String code, String value) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return insAdapter.setGLCode(companyName, tableName, code, value);
    }

    public String setGLVehicle(String companyName, VehicleDesc veh) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return insAdapter.setGLVehicle(companyName, veh);
    }

    public String getVehicleDescription(String companyName, String nvic, boolean source) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return insAdapter.getVehicleDescription(companyName, nvic, source);
    }

    public ArrayList getVehicleList(String companyName, String type) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return insAdapter.getVehicleList(companyName, type);
    }

    public String setVehicleList(String companyName, ArrayList vl) throws RemoteException {
	InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return insAdapter.setVehicleList(companyName, vl);
    }

    public void openVehicleQuery(String companyName, String type) throws RemoteException {
	this.pAdapter = InsuranceFactory.getInsuranceAdapter();
	pAdapter.openVehicleQuery(companyName, type);
    }

    public void releaseVehicleQuery() throws RemoteException {
	// InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	pAdapter.releaseVehicleQuery();
    }

    public VehicleDesc getNextVehicle() throws RemoteException {
	// InsuranceAdapter insAdapter = InsuranceFactory.getInsuranceAdapter();
	return pAdapter.getNextVehicle();
    }

}
