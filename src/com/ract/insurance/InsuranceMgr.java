package com.ract.insurance;

import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.ract.common.hibernate.HibernatePage;

@Remote
public interface InsuranceMgr {
    public int uploadPayeesFile(InputStream inputStream) throws RemoteException;

    public Collection getSupplierRegions() throws RemoteException;

    public Collection getSupplierClasses() throws RemoteException;

    public Collection getSupplierTypes() throws RemoteException;

    // public Collection getSupplierCategories() throws RemoteException;

    public String getSupplierCategoriesXMLByClass(String payeeClass) throws RemoteException;

    public Collection getSupplierClassesByRegion(String region) throws RemoteException;

    public Collection getSupplierCategoriesByClass(String payeeClass) throws RemoteException;

    public String getSupplierClassesXMLByRegion(String region) throws RemoteException;

    public HibernatePage getPayees(Hashtable criteria, int startIndex, int maxResults, String sortBy) throws RemoteException;

    public Collection getSupplierSorts() throws RemoteException;

    public ArrayList getRFDates(String companyName) throws RemoteException;

    public ArrayList getRFDetails(String companyName) throws RemoteException;

    public void setRFData(String companyName, ArrayList datesList, ArrayList detailsList) throws RemoteException;

    public ArrayList getMessageText(String companyName) throws RemoteException;

    public void setMessageText(String companyName, ArrayList mtList) throws RemoteException;

    public void setSuburbList(String companyName, ArrayList sl) throws RemoteException;

    public ArrayList getSuburbList(String companyName, String extType) throws RemoteException;

    public String setInvalidVehicle(String companyName, String action, String nvic, String userId) throws RemoteException;

    public String setGLCode(String companyName, String tableName, String code, String value) throws RemoteException;

    public String setGLVehicle(String companyName, VehicleDesc veh) throws RemoteException;

    public String getVehicleDescription(String companyName, String nvic, boolean source) throws RemoteException;

    public ArrayList getVehicleList(String companyName, String type) throws RemoteException;

    public String setVehicleList(String companyName, ArrayList vl) throws RemoteException;

    public void openVehicleQuery(String companyName, String type) throws RemoteException;

    public void releaseVehicleQuery() throws RemoteException;

    public VehicleDesc getNextVehicle() throws RemoteException;
}
