package com.ract.insurance;

import java.io.Serializable;

import com.ract.util.DateTime;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * </p>
 * <p>
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class RFDate implements Serializable {
    private String tabName;
    private DateTime effDate;
    private String key1 = "", key2 = "";
    private DateTime createDate;
    private String createId;
    private Integer rfDateSeq;

    public RFDate() {
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public String getCreateId() {
	return createId;
    }

    public DateTime getEffDate() {
	return effDate;
    }

    public String getKey1() {
	return key1;
    }

    public String getKey2() {
	return key2;
    }

    public Integer getRfDateSeq() {
	return rfDateSeq;
    }

    public String getTabName() {
	return tabName;
    }

    public void setTabName(String tabName) {
	this.tabName = tabName;
    }

    public void setRfDateSeq(Integer rfDateSeq) {
	this.rfDateSeq = rfDateSeq;
    }

    public void setKey2(String key2) {
	this.key2 = key2;
    }

    public void setKey1(String key1) {
	this.key1 = key1;
    }

    public void setEffDate(DateTime effDate) {
	this.effDate = effDate;
    }

    public void setCreateId(String createId) {
	this.createId = createId;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    public String toString() {
	return "\n  tabName     " + tabName + "\n  effDate     " + effDate + "\n  key1        " + key1 + "\n  key2        " + key2 + "\n  createDate  " + createDate + "\n  createId    " + createId + "\n  rfDateSeq   " + rfDateSeq;
    }

    public RFDate cloneRFD() {
	RFDate rfd = new RFDate();
	rfd.tabName = this.tabName;
	rfd.key1 = this.key1;
	rfd.key2 = this.key2;
	if (this.effDate != null)
	    rfd.effDate = new DateTime(this.effDate);
	rfd.createDate = new DateTime().getDateOnly();
	return rfd;
    }

    public int compareTo(RFDate rfd) {
	int comp;
	comp = this.tabName.compareTo(rfd.tabName);
	if (comp != 0)
	    return comp;
	comp = this.key1.compareTo(rfd.key1);
	if (comp != 0)
	    return comp;
	comp = this.key2.compareTo(rfd.key2);
	if (comp != 0)
	    return comp;
	comp = this.effDate.compareTo(rfd.effDate);
	return comp;
    }

}
