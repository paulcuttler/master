package com.ract.membership;

/**
 * Holds methods used for repairing membership data.
 */

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.ejb.Remote;

import com.ract.util.DateTime;

@Remote
public interface MembershipRepairMgr {

    public int getGroupsWithOneMemberCount() throws RemoteException;

    public void repairOneMemberGroups() throws RemoteException;

    public int getMembershipCardJoinDateToRepairCount() throws RemoteException;

    public void repairRemovedCards(DateTime startDate) throws RemoteException;

    public void updateMarketingClient(Integer clientNumber) throws RemoteException;

    public int getMembershipCardNameToRepairCount() throws RemoteException;

    public int getAccessMemberMarketingCount() throws RemoteException;

    public void repairAccessMemberMarketing() throws RemoteException;

    public void repairMembershipCardNameWorker(Integer membershipID, Integer cardID) throws RemoteException;

    public void repairMembershipCardJoinDate() throws RemoteException;

    public ArrayList repairMembershipCardName() throws RemoteException;

    public int getDuplicationTransactionsToRepairCount() throws RemoteException;

    public void repairMembershipsWithTransferredStatus(String userId) throws RemoteException;

    public double reverseTransaction(Integer transId, String description, String userId, boolean dereferencePayableItem, boolean removeTransaction, boolean restrictToRenew) throws RemoteException;

    public int getMembershipsWithTransferredStatusCount() throws RemoteException;

    /**
     * Converts the membership history documents from being stored as a file in
     * the file system to being stored in a column on the mem_transaction table.
     * Can be run repeatedly. It will convert any files that havn't been
     * converted yet. Returns the number of history files converted.
     */
    public int convertMembershipHistoryFiles() throws RemoteException;

    public void repairDuplicateTransactions() throws RemoteException;

    /**
     * Does the actual conversion work. Don't call directly, call
     * convertMembershipHistoryFiles() instead.
     */
    public ArrayList convertMembershipHistoryFilesWorker() throws RemoteException;

    public int repairTransactionDetails() throws RemoteException;

    public int getTransactionDetailsToRepairCount() throws RemoteException;

}
