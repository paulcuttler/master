package com.ract.membership;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.RemoveException;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;

import com.insight4.ws.AddCardRequestResult;
import com.insight4.ws.CardManagementService;
import com.insight4.ws.ICardManagementService;
import com.insight4.ws.RemoveCardRequestResult;
import com.ract.client.Client;
import com.ract.client.ClientAdapter;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientFactory;
import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientNote;
import com.ract.client.ClientVO;
import com.ract.client.NoteType;
import com.ract.client.ProgressClientAdapter;
import com.ract.client.notifier.ClientChangeNotificationEvent;
import com.ract.common.Account;
import com.ract.common.BusinessType;
import com.ract.common.CacheHandler;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.DataSourceFactory;
import com.ract.common.ExceptionHelper;
import com.ract.common.ExtractControl;
import com.ract.common.GenericException;
import com.ract.common.Gift;
import com.ract.common.MailMgr;
import com.ract.common.PostalAddressVO;
import com.ract.common.ReferenceDataVO;
import com.ract.common.RollBackException;
import com.ract.common.SequenceMgr;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.cad.Agent;
import com.ract.common.cad.AgentPK;
import com.ract.common.mail.MailMessage;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationMgr;
import com.ract.membership.club.AffiliatedClubVO;
import com.ract.membership.notifier.MembershipChangeNotificationEvent;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.security.SecurityHelper;
import com.ract.user.User;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.MethodCounter;
import com.ract.util.StringUtil;

/**
 * This stateless session bean provides utility methods for membership entities and value objects. It facilitates the lazy lookup of membership related lists of data.
 */
@Stateless
@Remote({ MembershipMgr.class })
@Local({ MembershipMgrLocal.class })
public class MembershipMgrBean {

	@PersistenceContext(unitName = "master")
	private EntityManager em;

	@PersistenceContext(unitName = "master")
	Session hsession;

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource clientDataSource;

	/**
	 * Get the list of membership cards, as MembershipCardVO, that have been issued to the membership. The membership card value objects will be set to read only.
	 */
	@Deprecated
	public ArrayList getMembershipCardList(Integer membershipId) throws RemoteException {
		return (ArrayList) findMembershipCardsByMembership(membershipId);
	}

	public Collection getProductBenefitTypeList() {
		Query productBenefitQuery = em.createQuery("select e from ProductBenefitTypeVO e");
		return productBenefitQuery.getResultList();
	}

	@Deprecated
	public void removeMembershipCard(Integer cardId) throws RemoteException {
		MembershipCardVO memCard = getMembershipCard(cardId);
		removeMembershipCard(memCard);
	}

	public ProductBenefitTypeVO getProductBenefitType(String productBenefitTypeCode) {
		return em.find(ProductBenefitTypeVO.class, productBenefitTypeCode);
	}

	public void createProductBenefitType(ProductBenefitTypeVO productBenefitType) {
		em.persist(productBenefitType);
	}

	public void deleteProductBenefitType(ProductBenefitTypeVO productBenefitType) {
		productBenefitType = em.merge(productBenefitType);
		em.remove(productBenefitType);
	}

	/**
	 * Requires a table called ConvertAccessMembership
	 * 
	 * @param limit
	 * @throws RemoteException
	 */
	public void convertAccessMembers(int limit) throws RemoteException {

		LogUtil.log(this.getClass(), "convertAccessMembers start");
		LogUtil.log(this.getClass(), "limit=" + limit);

		// import file of access members
		Hashtable<Integer, String> accessMembers = new Hashtable();
		String profileCode = null;
		try {
			List convertAccessMembers = em.createQuery("from ConvertAccessMembership mt").getResultList();
			LogUtil.log(this.getClass(), "convertAccessMembers size=" + convertAccessMembers.size());
			ConvertAccessMembership convertMembership = null;
			final String TRUE = "Y";
			for (Iterator<ConvertAccessMembership> i = convertAccessMembers.iterator(); i.hasNext();) {
				profileCode = null;

				convertMembership = i.next();

				// elements[1] //Marketing
				boolean hasInsurance = convertMembership.getInsurance().equalsIgnoreCase(TRUE); // INS
				boolean hasTravel = (convertMembership.getTramada().equalsIgnoreCase(TRUE) || convertMembership.getTravel().equalsIgnoreCase(TRUE));

				if (hasTravel) {
					profileCode = MembershipProfileVO.PROFILE_GIFTED_TRAVEL;
				} else if (hasInsurance) {
					profileCode = MembershipProfileVO.PROFILE_GIFTED_INSURANCE;
				}

				LogUtil.debug(this.getClass(), "clientNumber=" + convertMembership.getClientNumber() + ", profileCode=" + profileCode);
				// add to hashtable indicating the profile code to use for
				// conversion.
				if (profileCode != null) {
					accessMembers.put(new Integer(convertMembership.getClientNumber()), profileCode);
				}

			}
			LogUtil.debug(this.getClass(), "convertAccessMembers accessMembers=" + accessMembers.size());
		} catch (Exception e) {
			throw new RemoteException("Unable to process access member file to determine the appropriate membership profile.", e);
		}

		// get all members with product code of access or non motoring
		// TODO extremely large list may require some tweaks to transaction
		// timeout
		LogUtil.log(this.getClass(), "convertAccessMembers nonRoadsideMembers");
		List<MembershipVO> nonRoadsideMembers = hsession.createCriteria(MembershipVO.class).add(Expression.or(Expression.eq("productCode", ProductVO.PRODUCT_ACCESS), Expression.eq("productCode", ProductVO.PRODUCT_NON_MOTORING))).list();
		LogUtil.log(this.getClass(), "convertAccessMembers nonRoadsideMembers size=" + nonRoadsideMembers.size());
		User roadsideUser = SecurityHelper.getRoadsideUser();

		MembershipVO membership = null;
		ProductVO accessProduct = getProduct(ProductVO.PRODUCT_ACCESS);

		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();

		int conversionCounter = 0;
		for (Iterator<MembershipVO> i = nonRoadsideMembers.iterator(); i.hasNext() && (limit == -1 || conversionCounter < limit);) {
			conversionCounter++;
			membership = i.next();

			// NEW TRANSACTION START
			membershipMgrLocal.convertAccessMembership(membership, accessProduct, roadsideUser, accessMembers);
			// NEW TRANSACTION END

			if (conversionCounter % 1000 == 0) {
				LogUtil.log(this.getClass(), conversionCounter + ": convertAccessMembers membership " + membership);
			}
		}

		LogUtil.log(this.getClass(), "convertAccessMembers end");

	}

	/**
	 * 
	 * @param membership
	 * @param accessProduct
	 * @param roadsideUser
	 * @param accessMembers
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void convertAccessMembership(MembershipVO membership, ProductVO accessProduct, User roadsideUser, Hashtable<Integer, String> accessMembers) throws RemoteException {

		ClientMgrLocal clientMgr = ClientEJBHelper.getClientMgrLocal();
		ClientVO client = membership.getClient();

		// move generic code
		LogUtil.debug(this.getClass(), "membership=" + membership + ", accessProduct=" + accessProduct + ", roadsideUser=" + roadsideUser);

		DateTime DATE_DEFAULT_EXPIRY = null;
		try {
			DATE_DEFAULT_EXPIRY = new DateTime("01/07/2010");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		final DateTime DATE_NOW = new DateTime().getDateOnly();

		MembershipVO oldMembership = null;
		MembershipTransactionVO transaction = null;
		oldMembership = membership.copy();

		// NON MOTORING
		if (membership.getProductCode().equals(ProductVO.PRODUCT_NON_MOTORING)) {
			membership.setStatus(MembershipVO.STATUS_ACTIVE);
			membership.setAllowToLapse(Boolean.valueOf(false));

			if (membership.isGroupMember()) {
				LogUtil.warn(this.getClass(), "Unable to convert as member is in a group. " + membership);
				return;
			}

			// update product
			membership.setProduct(accessProduct);
			membership.setNextProductCode(null);
			membership.setMembershipProfileCode(MembershipProfileVO.PROFILE_NON_MOTORING);

			// create a change product transaction
			transaction = new MembershipTransactionVO(ValueObject.MODE_CREATE, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, DATE_NOW);

			transaction.setTransactionDate(DATE_NOW);
			transaction.setUsername(roadsideUser.getUserID());
			transaction.setSalesBranchCode(roadsideUser.getSalesBranchCode());
			transaction.setMembership(oldMembership);
			transaction.setNewMembership(membership);
			transaction.setProductCode(accessProduct.getProductCode());
			transaction.setProductEffectiveDate();

			// override the product effective date
			membership.setProductEffectiveDate(DATE_NOW);

			transaction.setEffectiveDate(DATE_NOW);
			transaction.setEndDate(membership.getExpiryDate());

			// delete all existing cards as there have never been non motoring
			// cards.
			removeMembershipCards(membership);

			String cardReasonCode = ReferenceDataVO.CARD_REQUEST_CONVERT;

			// create a card request
			// create a membership card request only
			// MembershipCardVO cardVO = new
			// MembershipCardVO(membership.getMembershipID(), 0, DATE_NOW,
			// ReferenceDataVO.REF_TYPE_MEMBERSHIP_CARD_REQUEST, cardReasonCode,
			// String.valueOf(MembershipTier.TIER_CODE_DEFAULT), DATE_NOW,
			// membership.getRego());
			//
			// // Create the card record and generate a card ID.
			// createMembershipCard(cardVO);

		} else
		// ACCESS
		{

			// allow multiple conversions - doesn't cater for non-motoring
			// conversions.
			if (membership.getMembershipProfile() != null) {
				if (membership.getMembershipProfileCode().equals(MembershipProfileVO.PROFILE_GIFTED_INSURANCE) || membership.getMembershipProfileCode().equals(MembershipProfileVO.PROFILE_GIFTED_TRAVEL) || membership.getMembershipProfileCode().equals(MembershipProfileVO.PROFILE_NON_MOTORING)) {
					LogUtil.warn(this.getClass(), "Skipping member as member " + membership.getMembershipNumber() + " has already been converted - " + membership.getMembershipProfile().getProfileCode() + ".");
					return;
				}
			}

			// check in temporary table
			String profileCode = accessMembers.get(membership.getClientNumber());
			// remove from hashtable to speed up processing
			accessMembers.remove(membership.getClientNumber());

			LogUtil.debug(this.getClass(), "membership=" + membership + ", profileCode=" + profileCode);
			LogUtil.debug(this.getClass(), "accessMembers=" + accessMembers.size());

			// default to Insurance if we don't know - not in any audit file or
			// created as an adhoc Access member
			if (profileCode == null) {
				LogUtil.warn(this.getClass(), "Defaulting profile for " + membership.getClientNumber());
				profileCode = MembershipProfileVO.PROFILE_GIFTED_INSURANCE;
			}
			membership.setProduct(accessProduct);
			membership.setMembershipProfileCode(profileCode);
			membership.setExpiryDate(DATE_DEFAULT_EXPIRY);

			// add a transaction of EDIT
			transaction = new MembershipTransactionVO(ValueObject.MODE_CREATE, MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP, MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP, DATE_NOW);

			transaction.setTransactionDate(DATE_NOW);
			transaction.setUsername(roadsideUser.getUserID());
			transaction.setSalesBranchCode(roadsideUser.getSalesBranchCode());
			transaction.setMembership(oldMembership);
			transaction.setNewMembership(membership);
			transaction.setProductCode(accessProduct.getProductCode());
			transaction.setProductEffectiveDate();
			transaction.setGroupCount(1);

			transaction.setEffectiveDate(DATE_NOW);
			transaction.setEndDate(DATE_DEFAULT_EXPIRY);

		}
		MembershipTransactionVO lastTransVO = membership.getLastTransaction();
		try {
			if (lastTransVO.getMembershipHistory() == null) {
				LogUtil.log(this.getClass(), "createAccessMembership 5a" + lastTransVO);
				lastTransVO.setMembershipXML(oldMembership.toXML());
				updateMembershipTransaction(lastTransVO);
			}
		} catch (Exception e) {
			// an exception getting
			LogUtil.warn(this.getClass(), "Unable to get membership history. " + membership);
			LogUtil.warn(this.getClass(), e);
		}

		// update in DB
		updateMembership(membership);

		transaction.setMembershipXML(membership.toXML());
		LogUtil.log(this.getClass(), "createAccessMembership 6");

		createMembershipTransaction(transaction);

		// ensure correct motor news options are set
		client.setMotorNewsSendOption(Client.MOTOR_NEWS_MEMBERSHIP_SEND);
		client.setMotorNewsDeliveryMethod(Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL);
		clientMgr.updateClient(client);

		// leave prosper to full reload

		LogUtil.log(this.getClass(), "Converted " + membership.getMembershipNumber());
	}

	public void updateProductBenefitType(ProductBenefitTypeVO productBenefitType) {
		em.merge(productBenefitType);
	}

	@Deprecated
	public MembershipCardVO getMembershipCardByCardNumber(String cardNumber) throws RemoteException {
		MembershipCardVO membershipCard = null;
		ArrayList cardList = null;

		Criteria crit = hsession.createCriteria(MembershipCardVO.class).add(Expression.eq("cardNumber", cardNumber));
		cardList = new ArrayList(crit.list());
		if (cardList != null) {
			Iterator cardIt = cardList.iterator();
			if (cardIt.hasNext()) {
				membershipCard = (MembershipCardVO) cardIt.next();
			}
		}
		return membershipCard;
	}

	public Collection getDiscountTypeByAccountId(Integer accountID) throws RemoteException {
		ArrayList discountTypeList = null;

		Criteria crit = hsession.createCriteria(DiscountTypeVO.class).add(Expression.eq("accountID", accountID));
		discountTypeList = new ArrayList(crit.list());
		return discountTypeList;
	}

	public Collection getDiscountTypes() throws RemoteException {
		ArrayList discountTypeList = null;

		Criteria crit = hsession.createCriteria(DiscountTypeVO.class);
		discountTypeList = new ArrayList(crit.list());
		return discountTypeList;
	}

	public Collection getDiscountAttributes(String discountTypeCode) throws RemoteException {
		LogUtil.debug(this.getClass(), "getDiscountAttribute=" + discountTypeCode);
		ArrayList discountAttributeList = null;

		Criteria crit = hsession.createCriteria(MembershipDiscountAttribute.class).add(Expression.eq("membershipDiscountAttributePK.discountTypeCode", discountTypeCode));
		discountAttributeList = new ArrayList(crit.list());
		return discountAttributeList;
	}

	public Collection getTransactionFeeDiscountAttributes(Integer transID, Integer feeSpecID, String discountTypeCode) throws RemoteException {
		ArrayList discountAttributeList = null;

		LogUtil.debug(this.getClass(), "getTransactionFeeDiscountAttributes=" + transID + "/" + feeSpecID + "/" + discountTypeCode);
		Criteria crit = hsession.createCriteria(TransactionFeeDiscountAttribute.class).add(Expression.eq("transactionFeeDiscountAttributePK.discountTypeCode", discountTypeCode)).add(Expression.eq("transactionFeeDiscountAttributePK.feeSpecificationID", feeSpecID)).add(Expression.eq("transactionFeeDiscountAttributePK.transactionID", transID));
		discountAttributeList = new ArrayList(crit.list());
		LogUtil.debug(this.getClass(), "discountAttributeList=" + discountAttributeList.size());
		return discountAttributeList;
	}

	public DiscountTypeVO getDiscountType(String discountTypeCode) throws RemoteException {

		DiscountTypeVO discountType = (DiscountTypeVO) hsession.get(DiscountTypeVO.class, discountTypeCode);
		return discountType;
	}

	public Collection getDiscountTypeByFeeSpecificationId(Integer feeSpecificationID) throws RemoteException {
		DateTime now = new DateTime().getDateOnly();
		Connection connection = null;
		PreparedStatement statement = null;
		final String SQL_DISCOUNT_BY_FEE_SPEC = "SELECT distinct d.discount_type_code " + "FROM PUB.mem_discount d, PUB.mem_allowed_fee_discount a " + "WHERE d.discount_type_code = a.discount_type_code " + "AND d.discount_type_status = ? " + "AND a.fee_specification_id = ? " + "AND (d.effective_from_date <= ? OR d.effective_from_date is null) " + "AND (d.effective_to_date >= ? OR d.effective_to_date is null)";
		ArrayList<DiscountTypeVO> discountTypeList = new ArrayList<DiscountTypeVO>();
		DiscountTypeVO discountType = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(SQL_DISCOUNT_BY_FEE_SPEC);
			statement.setString(1, DiscountTypeVO.STATUS_Active);
			statement.setInt(2, feeSpecificationID.intValue());
			statement.setDate(3, now.toSQLDate());
			statement.setDate(4, now.toSQLDate());
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				String discountTypeCode = resultSet.getString(1);
				discountType = getDiscountType(discountTypeCode);
				discountTypeList.add(discountType);
			}
		} catch (SQLException e) {
			throw new RemoteException("Error executing SQL " + SQL_DISCOUNT_BY_FEE_SPEC + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return discountTypeList;
	}

	public Collection getProducts() {

		Collection productList = hsession.createCriteria(ProductVO.class).list();
		return productList;
	}

	public Collection getProducts(String affiliateProductLevel) {
		LogUtil.debug(this.getClass(), "affiliateProductLevel=" + affiliateProductLevel);

		Criteria productLevelMatch = hsession.createCriteria(ProductVO.class).add(Expression.eq("affiliateProductLevel", affiliateProductLevel));
		Collection productLevelMatches = productLevelMatch.list();
		List productList = new ArrayList();
		ProductVO product = null;
		// direct match - should only be one
		for (Iterator i = productLevelMatches.iterator(); i.hasNext();) {
			product = (ProductVO) i.next();
			LogUtil.debug(this.getClass(), "1 product=" + product.getProductCode());
			productList.add(product); // Ult
		}

		Criteria lesserLevelProduct = hsession.createCriteria(ProductVO.class).add(Expression.gt("productRanking", product.getProductRanking())) // product ranking is
		// higher (eg. Ult is 1,
		// Adv is 2, etc.
		.add(Expression.isNotNull("affiliateProductLevel")); // must
		// have a
		// product
		// level
		Collection lesserLevelProducts = lesserLevelProduct.list();
		for (Iterator x = lesserLevelProducts.iterator(); x.hasNext();) {
			// reuse the product variable
			product = (ProductVO) x.next();
			LogUtil.debug(this.getClass(), "2 product=" + product.getProductCode());
			if (!productList.contains(product)) {
				productList.add(product); // Adv
			}
		}

		return productList;
	}

	public ProductVO getProduct(String productCode) {

		ProductVO product = (ProductVO) hsession.get(ProductVO.class, productCode);
		return product;
	}

	public void createProduct(ProductVO product) throws RemoteException {

		hsession.save(product);

		insertProductBenefitList(product);
	}

	public void updateProduct(ProductVO product) throws RemoteException {

		hsession.update(product);

		// update product benefit list
		Collection productBenefitList = product.getProductBenefitList();
		if (productBenefitList != null) {
			removeProductBenefitList(product);
			insertProductBenefitList(product);
		}
	}

	/**
	 * Transaction Required
	 * 
	 * @param effectiveDate
	 * @param createMembership
	 * @param limit
	 * @param userId
	 * @throws RemoteException
	 */
	public void createEligibleAccessMembershipClients(DateTime effectiveDate, boolean createMembership, int limit, String userId) throws RemoteException {

		final SimpleDateFormat fileMask = new SimpleDateFormat("yyyyMMddHHmmSS");
		final String dateStamp = fileMask.format(new DateTime());
		final String logFile = "access_eligible_client" + dateStamp + "." + FileUtil.EXTENSION_CSV;
		final String auditFile = "access_eligible_client_audit" + dateStamp + "." + FileUtil.EXTENSION_TXT;
		final String errorFile = "access_eligible_client_error" + dateStamp + "." + FileUtil.EXTENSION_CSV;
		final String createdErrorFile = "access_created_error" + dateStamp + "." + FileUtil.EXTENSION_CSV;
		final String createdFile = "access_created" + dateStamp + "." + FileUtil.EXTENSION_CSV;

		LogUtil.log(this.getClass(), "limit=" + limit);
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String directoryName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DIRECTORY_MEMBERSHIP_EXTRACTS); // includes
		// /

		// write to file
		String logFileName = directoryName + logFile;
		String auditFileName = directoryName + auditFile;
		String errorFileName = directoryName + errorFile;
		String createdFileName = directoryName + createdFile;
		String createdErrorFileName = directoryName + createdErrorFile;

		LogUtil.log(this.getClass(), "directoryName=" + directoryName);

		LogUtil.log(this.getClass(), "logFileName=" + logFileName);
		LogUtil.log(this.getClass(), "auditFileName=" + auditFileName);
		LogUtil.log(this.getClass(), "errorFileName=" + errorFileName);
		LogUtil.log(this.getClass(), "createdFileName=" + createdFileName);
		LogUtil.log(this.getClass(), "createdErrorFileName=" + createdErrorFileName);

		try {
			commonMgr.startExtract(ExtractControl.EXTRACT_ACCESS, userId);
		} catch (GenericException e) {
			throw new RemoteException("Unable to start extract.", e);
		}

		LogUtil.log(this.getClass(), "createMembership=" + createMembership);
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		AccessMembership accessMembership = null;
		ArrayList eligibleClientList = membershipMgr.getEligibleAccessMembershipClients(effectiveDate, limit, logFileName, auditFileName, errorFileName);
		LogUtil.log(this.getClass(), "eligibleClientList=" + eligibleClientList.size());
		int membershipsCreated = 0;
		if (createMembership) {

			BufferedWriter createdFileWriter = null;
			BufferedWriter errorFileWriter = null;

			try {
				createdFileWriter = new BufferedWriter(new FileWriter(new File(createdFileName)));
				errorFileWriter = new BufferedWriter(new FileWriter(new File(createdErrorFileName)));

				createdFileWriter.write("Client number" + FileUtil.SEPARATOR_COMMA + "Source" + FileUtil.NEW_LINE);
				errorFileWriter.write("Client number" + FileUtil.SEPARATOR_COMMA + "Error description" + FileUtil.NEW_LINE);

				Iterator accessMembershipIt = eligibleClientList.iterator();
				while (accessMembershipIt.hasNext()) {
					accessMembership = (AccessMembership) accessMembershipIt.next();
					LogUtil.log(this.getClass(), "Creating eligible membership for " + accessMembership.getClientNumber() + " - " + membershipsCreated);
					// create the membership, transaction and card request using
					// a new remote interface that is marked as RequiresNew
					try {
						membershipMgr.createAccessMembership(accessMembership.getClientNumber(), accessMembership.getMembershipId(), effectiveDate, accessMembership.getEligibilitySource());
						membershipsCreated++;
						createdFileWriter.write(accessMembership.getClientNumber() + FileUtil.SEPARATOR_COMMA + accessMembership.getEligibilitySource() + FileUtil.NEW_LINE);
					} catch (RemoteException e) {
						LogUtil.warn(this.getClass(), "Error creating access membership for client " + accessMembership.getClientNumber() + ": " + ExceptionHelper.getExceptionStackTrace(e));
						errorFileWriter.write(accessMembership.getClientNumber() + FileUtil.SEPARATOR_COMMA + e.getMessage() + FileUtil.NEW_LINE);
					}
					LogUtil.log(this.getClass(), "Created eligible membership for " + accessMembership.getClientNumber());
				}

				errorFileWriter.flush();
				errorFileWriter.close();

				createdFileWriter.flush();
				createdFileWriter.close();
			} catch (IOException e1) {
				throw new RemoteException("Unable to write to created audit file or created error file.", e1);
			}

		}

		try {
			commonMgr.endExtract(ExtractControl.EXTRACT_ACCESS);
		} catch (GenericException e) {
			throw new RemoteException("Unable to end extract. The extract control record for '" + ExtractControl.EXTRACT_ACCESS + "' will need to removed before " + "running the extract again.", e);
		}

		LogUtil.log(this.getClass(), "membershipsCreated=" + membershipsCreated);
	}

	public ArrayList getEligibleAccessMembershipClients(DateTime effectiveDate, int limit, String logFileName, String auditFileName, String errorFileName) throws RemoteException {
		LogUtil.log(this.getClass(), "createEligibleAccessMembershipClients 1");
		LogUtil.log(this.getClass(), "effectiveDate=" + effectiveDate);
		LogUtil.log(this.getClass(), "limit=" + limit);
		LogUtil.log(this.getClass(), "logFileName=" + logFileName);
		LogUtil.log(this.getClass(), "auditFileName=" + auditFileName);
		LogUtil.log(this.getClass(), "errorFileName=" + errorFileName);

		String insuranceSrc = SourceSystem.INSURANCE.getAbbreviation();
		try {
			insuranceSrc = FileUtil.getProperty("master", "accessInsuranceStore");
		} catch (Exception e) {
			LogUtil.warn(this.getClass(), "Could not determine insuranceSrc from properties file, defaulting to: " + insuranceSrc);
		}
		LogUtil.log(this.getClass(), "insuranceSrc=" + insuranceSrc);

		ArrayList accessMembershipClientList = new ArrayList();

		BufferedWriter fileWriter = null;
		BufferedWriter auditFileWriter = null;
		BufferedWriter errorFileWriter = null;
		try {
			fileWriter = new BufferedWriter(new FileWriter(new File(logFileName)));
			fileWriter.write("Client number" + FileUtil.SEPARATOR_COMMA + "Insurance (" + insuranceSrc + ")" + FileUtil.SEPARATOR_COMMA + "Travel (Progress)" + FileUtil.SEPARATOR_COMMA + "Travel (Tramada)" + FileUtil.SEPARATOR_COMMA + "Membership" + FileUtil.SEPARATOR_COMMA + "Invalid data (Error file)" + FileUtil.NEW_LINE);

			auditFileWriter = new BufferedWriter(new FileWriter(new File(auditFileName)));
			errorFileWriter = new BufferedWriter(new FileWriter(new File(errorFileName)));
		} catch (IOException ex3) {
			throw new RemoteException("Unable to write to file.", ex3);
		}

		ClientAdapter clientAdapter = ClientFactory.getClientAdapter();

		Hashtable households = new Hashtable();

		StringBuffer eligibleSQL = new StringBuffer();
		eligibleSQL.append("select c1.[client-no], c1.[market], c1.[sex], c1.[birth-date], c1.[resi-stsubid], c1.[resi-street-no], c1.[resi-street-char], c1.[resi-property], mm.membership_id, c1.[post-property], c1.[post-street-char], st.street ");
		eligibleSQL.append("from PUB.[cl-master] c1 ");
		eligibleSQL.append("inner join [gn-stsub] st on c1.[post-stsubid] = st.stsubid ");
		eligibleSQL.append("left outer join mem_membership mm on c1.[client-no] = mm.client_number ");
		eligibleSQL.append("where [group-id] = ? "); // not a group
		eligibleSQL.append("and ([client-status] is null or [client-status] = '' or [client-status] = '" + Client.STATUS_ACTIVE + "') "); // active
		// client
		// exclude organisation clients
		eligibleSQL.append("and ([client-title] is not null and [client-title] <> '' and [given-names] is not null and [given-names] <> '') ");
		// 01/10/2008 JH Allow interstate members to be created.

		// Tasmania residences
		eligibleSQL.append("and exists ");
		eligibleSQL.append("(select 'x' from PUB.[gn-stsub] g1 where c1.[resi-stsubid] = g1.stsubid and g1.state = 'TAS') ");
		// Tasmania postal addresses
		eligibleSQL.append("and exists ");
		eligibleSQL.append("(select 'x' from PUB.[gn-stsub] g1 where c1.[post-stsubid] = g1.stsubid and g1.state = 'TAS') ");

		// not a membership record with an expiry date greater than a year ago
		eligibleSQL.append("and not exists ");
		eligibleSQL.append("( ");
		eligibleSQL.append("select 'x' ");
		eligibleSQL.append("from mem_membership m1 ");
		eligibleSQL.append("where m1.client_number = c1.[client-no] ");
		eligibleSQL.append("and m1.expiry_date >= ? ");
		eligibleSQL.append(") ");
		// exclude any on hold or cancelled
		eligibleSQL.append("and not exists ");
		eligibleSQL.append("( ");
		eligibleSQL.append("select 'x' ");
		eligibleSQL.append("from mem_membership m1 ");
		eligibleSQL.append("where m1.client_number = c1.[client-no] ");
		eligibleSQL.append("and m1.membership_status in ('" + MembershipVO.STATUS_CANCELLED + "','" + MembershipVO.STATUS_ONHOLD + "') ");
		eligibleSQL.append(") ");
		// and not an active member in the same household
		/*
		 * Disabled by CR MEM-189 eligibleSQL.append("and not exists "); eligibleSQL.append("( "); eligibleSQL.append("select 'x' "); eligibleSQL.append("from PUB.[cl-master] c2, mem_membership m2 "); eligibleSQL.append("where c2.[client-no] = m2.client_number "); //convert any nulls eligibleSQL.append("and c2.[resi-stsubid] = c1.[resi-stsubid] "); eligibleSQL.append("and c2.[resi-street-no] = c1.[resi-street-no] "); //property or street char is both null or both '' or combinations of null and '' eligibleSQL.append( "and isnull(c2.[resi-street-char],'') = isnull(c1.[resi-street-char],'') " ); eligibleSQL.append( "and isnull(c2.[resi-property],'') = isnull(c1.[resi-property],'') " ); eligibleSQL.append("and m2.membership_status = '"+MembershipVO. STATUS_ACTIVE+"' "); //active eligibleSQL.append("and m2.expiry_date > ? "); eligibleSQL.append(") ");
		 */
		// additional criteria 14/7/2006
		// deceased with missed flag
		eligibleSQL.append("and ([surname] not like '%ESTATE OF%') ");
		eligibleSQL.append("and ([surname] not like '%EOTL%') ");
		// strata in surname
		eligibleSQL.append("and ([surname] not like '%STRATA%') ");
		// duplicates - lowest client-no is the master client. ie. throw out the
		// others
		eligibleSQL.append("and not exists  ");
		eligibleSQL.append("( ");
		eligibleSQL.append("select 'x' ");
		eligibleSQL.append("from [cl-master] cl2 ");
		eligibleSQL.append("where cl2.[client-title] = c1.[client-title] ");
		eligibleSQL.append("and cl2.[given-names] = c1.[given-names] ");
		eligibleSQL.append("and cl2.surname = c1.surname ");
		eligibleSQL.append("and cl2.[resi-street-char] = c1.[resi-street-char] ");
		eligibleSQL.append("and cl2.[resi-stsubid] = c1.[resi-stsubid] ");
		eligibleSQL.append("and cl2.[client-no] != c1.[client-no] ");
		eligibleSQL.append(") ");
		// other "dodgy" clients
		eligibleSQL.append("and [surname] not like '%&%' ");
		eligibleSQL.append("and [surname] not like '%/%' ");
		eligibleSQL.append("and [surname] not like '%\\%' ");
		eligibleSQL.append("and [client-title] not like '%&%' ");
		eligibleSQL.append("and [client-title] not like '%MANAGER%' ");
		// restrict by date
		// created or modified in last year (this could include a Member Calc
		// update)
		eligibleSQL.append("and ( ");
		eligibleSQL.append("  c1.[made-date] >= ?");
		eligibleSQL.append("  or c1.[last-update] >= ?");
		eligibleSQL.append(") ");
		eligibleSQL.append("order by c1.[client-no] desc");

		PreparedStatement statement = null;
		Connection connection = null;
		Integer clientNumber = null;

		java.sql.Date birthDate = null;
		boolean sex = false;

		boolean eligibleTravel = false;
		Boolean eligibleProgressTravel = null;
		Boolean eligibleTramadaTravel = null;

		boolean eligibleInsurance = false;
		Boolean eligibleProgressInsurance = null;
		Boolean eligibleMRMInsurance = null;

		// 12 months ago
		DateTime startDate = null;
		DateTime sixMonths = null;
		try {
			if (effectiveDate == null) {
				effectiveDate = new DateTime().getDateOnly();
			}
			startDate = effectiveDate.subtract(new Interval(0, 12, 0, 0, 0, 0, 0));
			sixMonths = effectiveDate.subtract(new Interval(0, 6, 0, 0, 0, 0, 0));
		} catch (Exception ex2) {
			throw new RemoteException("Unable to create date parameters.", ex2);
		}
		int recordCount = 0;
		int travelCount = 0;
		int insuranceCount = 0;
		int eligibleCount = 0;
		int ageSegment1Count = 0;
		int ageSegment2Count = 0;
		int ageSegment3Count = 0;
		int ageSegment4Count = 0;
		int ageSegment5Count = 0;
		int maleCount = 0;
		int femaleCount = 0;
		int ageYears = 0;
		int eligibleClients = 0;

		final int AGE_SEGMENT_1 = 17;
		final int AGE_SEGMENT_2 = 25;
		final int AGE_SEGMENT_3 = 40;
		final int AGE_SEGMENT_4 = 55;

		String resiStsubid = null;
		String resiStreetNumber = null;
		String resiPropertyQualifier = null;
		String resiProperty = null;

		String postProperty = null;
		String postPropertyQualifier = null;
		String postStreet = null;

		String householdKey = null;
		Integer householdCount = new Integer(0);
		int householdCounter = 0;
		int membershipId = 0;

		final String KEY_RACT = "RACT";
		boolean eligible = false;
		boolean eligibleMembership = false;
		Boolean gender = null;
		Interval age = null;
		Hashtable eligibleBusiness = null;
		try {
			connection = clientDataSource.getConnection();
			boolean allowGroupClient = false;
			statement = connection.prepareStatement(eligibleSQL.toString());
			statement.setBoolean(1, allowGroupClient); // group flag
			statement.setDate(2, sixMonths.toSQLDate()); // old memberships six
			// months and older
			statement.setDate(3, startDate.toSQLDate()); // made date
			statement.setDate(4, startDate.toSQLDate()); // made date
			LogUtil.log(this.getClass(), "eligibleSQL=" + eligibleSQL);
			LogUtil.log(this.getClass(), "allowGroupClient=" + allowGroupClient);
			LogUtil.log(this.getClass(), "startDate=" + startDate);
			LogUtil.log(this.getClass(), "sixMonths=" + sixMonths);
			LogUtil.log(this.getClass(), "effectiveDate=" + effectiveDate);
			ResultSet eligibleRS = statement.executeQuery();
			LogUtil.log(this.getClass(), "query executed");
			String eligibilitySource = null;
			boolean maxReached = false;
			boolean exclude = false;
			// for each customer check if:
			while (eligibleRS.next() && !maxReached) {
				exclude = false;

				// init to false
				recordCount++;

				LogUtil.debug(this.getClass(), "rec=" + recordCount);

				clientNumber = new Integer(eligibleRS.getInt(1));
				sex = eligibleRS.getBoolean(3);
				if (!eligibleRS.wasNull()) {
					// if sex not null
					gender = new Boolean(sex);
				} else {
					gender = null;
				}
				birthDate = eligibleRS.getDate(4);

				resiStsubid = String.valueOf(eligibleRS.getInt(5));
				resiStreetNumber = String.valueOf(eligibleRS.getInt(6));
				resiPropertyQualifier = eligibleRS.getString(7);
				resiProperty = eligibleRS.getString(8);

				postProperty = eligibleRS.getString(10);
				postPropertyQualifier = eligibleRS.getString(11);
				postStreet = eligibleRS.getString(12);

				membershipId = eligibleRS.getInt(9);
				if (eligibleRS.wasNull()) {
					eligibleMembership = false;
				} else {
					eligibleMembership = true;
				}

				// insurance or travel financial transactions within the last
				// year
				try {
					eligibleProgressInsurance = false;
					if (insuranceSrc.contains(SourceSystem.INSURANCE.getAbbreviation())) {
						eligibleBusiness = clientAdapter.hasEligibleBusiness(clientNumber);
						eligibleProgressInsurance = (Boolean) eligibleBusiness.get(ProgressClientAdapter.BUSINESS_INSURANCE);
					}

					eligibleMRMInsurance = false;
					if (insuranceSrc.contains(SourceSystem.MRM.getAbbreviation())) {
						eligibleMRMInsurance = hasMRMInsurance(clientNumber);
					}

					eligibleInsurance = eligibleProgressInsurance.booleanValue() || eligibleMRMInsurance.booleanValue();

					// assume travel and cruise will have a tr-item record
					// created on conversion
					eligibleProgressTravel = false;
					// eligibleProgressTravel =
					// (Boolean)eligibleBusiness.get(ProgressClientAdapter.BUSINESS_TRAVEL);
					eligibleTramadaTravel = false;
					// eligibleTramadaTravel =
					// hasTramadaTravel(clientNumber,startDate);

					eligibleTravel = eligibleProgressTravel.booleanValue() || eligibleTramadaTravel.booleanValue();

					eligible = (eligibleInsurance || eligibleTravel);

					// Insurance then Travel or null
					eligibilitySource = eligibleInsurance ? SourceSystem.INSURANCE.getAbbreviation() : (eligibleTravel ? SourceSystem.TRAVEL.getAbbreviation() : null);

					if (eligibleTravel) {
						travelCount++;
					} else if (eligibleInsurance) {
						insuranceCount++;
					}
				} catch (Exception ex1) {
					try {
						errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + ex1.getMessage() + FileUtil.NEW_LINE);
					} catch (IOException e) {
						e.printStackTrace();
					}
					eligible = false;
				}
				LogUtil.debug(this.getClass(), "eligible=" + eligible);
				if (eligible) {
					eligibleCount++; // qualified via travel or insurance

					if (limit != -1 && eligibleCount > limit) {
						maxReached = true;
					}

					if (resiProperty != null && resiProperty.toUpperCase().indexOf(KEY_RACT) > 0) {
						try {
							errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "residential property starts with a number." + FileUtil.NEW_LINE);
						} catch (IOException e) {
							e.printStackTrace();
						}
						exclude = true;
					}

					if (postProperty != null && postProperty.toUpperCase().indexOf(KEY_RACT) > 0) {
						try {
							errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "postal property is contains the key word \"RACT\"." + FileUtil.NEW_LINE);
						} catch (IOException e) {
							e.printStackTrace();
						}
						exclude = true;
					}

					// validate residential attributes
					if (resiProperty != null && resiProperty.length() > 0) {
						try {
							Integer stNum = new Integer(resiProperty.substring(0, 1));
							// if we can parse then it is number so exclude
							try {
								errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "residential property starts with a number." + FileUtil.NEW_LINE);
							} catch (IOException e) {
								e.printStackTrace();
							}
							exclude = true;
							LogUtil.debug(this.getClass(), "stNum=" + stNum);
						} catch (NumberFormatException ex6) {
							// if parsing fails it is valid
							exclude = false;
						}
					} else {
						if (resiPropertyQualifier == null || resiPropertyQualifier.length() == 0) {
							try {
								errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "no residential property or property qualifier." + FileUtil.NEW_LINE);
							} catch (IOException e) {
								e.printStackTrace();
							}
							exclude = true;
						}
					}

					// validate postal attributes
					if (postProperty != null && postProperty.length() > 0) {
						try {
							Integer stNum = new Integer(postProperty.substring(0, 1));
							// if we can parse then it is number so exclude
							try {
								errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "postal property starts with a number." + FileUtil.NEW_LINE);
							} catch (IOException e) {
								e.printStackTrace();
							}
							exclude = true;
							LogUtil.debug(this.getClass(), "stNum=" + stNum);
						} catch (NumberFormatException ex6) {
							// if parsing fails it is valid
							exclude = false;
						}
					} else {
						// no property or property qualifier
						if (postPropertyQualifier == null || postPropertyQualifier.length() == 0) {
							try {
								errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "no postal property or property qualifier." + FileUtil.NEW_LINE);
							} catch (IOException e) {
								e.printStackTrace();
							}
							exclude = true;
						}
					}

					// validate postal address
					if (postStreet != null && postStreet.length() > 0) {

						// no street and no street number
						if (postPropertyQualifier == null || postPropertyQualifier.length() == 0) {
							try {
								errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "postal street but no property qualifier." + FileUtil.NEW_LINE);
							} catch (IOException e) {
								e.printStackTrace();
							}
							exclude = true;
						}
						// ok if property
					}

					if (birthDate != null) {
						try {
							age = new Interval(new DateTime(birthDate), effectiveDate);
						} catch (Exception ex5) {
							// ignore
						}
						ageYears = age.getTotalYears();
						// arbitrary segments
						if (ageYears > 0 && ageYears <= AGE_SEGMENT_1) {
							ageSegment1Count++;
						} else if (ageYears > AGE_SEGMENT_1 && ageYears <= AGE_SEGMENT_2) {
							ageSegment2Count++;
						} else if (ageYears > AGE_SEGMENT_2 && ageYears <= AGE_SEGMENT_3) {
							ageSegment3Count++;
						} else if (ageYears > AGE_SEGMENT_3 && ageYears <= AGE_SEGMENT_4) {
							ageSegment4Count++;
						} else
						// over segment 3
						{
							ageSegment5Count++;
						}
					}

					// exclude if aged equal to or less than 17
					if (birthDate != null && ageYears <= AGE_SEGMENT_1) {
						try {
							errorFileWriter.write(clientNumber + FileUtil.SEPARATOR_COMMA + "client too young - " + ageYears + "." + FileUtil.NEW_LINE);
						} catch (IOException e) {
							e.printStackTrace();
						}
						exclude = true;
					}

					if (gender != null) {
						// sex is now considered reliable
						if (sex) {
							maleCount++;
						} else {
							femaleCount++;
						}
					}

					householdKey = (resiProperty == null ? "" : resiProperty) + (resiPropertyQualifier == null ? "" : resiPropertyQualifier) + resiStsubid;
					// remove any spaces
					householdKey = StringUtil.replaceAll(householdKey, " ", "");
					householdKey = StringUtil.replaceAll(householdKey, "-", "");
					householdKey = StringUtil.replaceAll(householdKey, ".", "");
					householdKey = StringUtil.replaceAll(householdKey, "/", "");
					householdKey = StringUtil.replaceAll(householdKey, "\\", "");

					householdCount = (Integer) households.get(householdKey);
					if (householdCount == null) {
						householdCount = new Integer(1);
						households.put(householdKey, householdCount);
					} else {
						householdCounter = householdCount.intValue() + 1;
						households.put(householdKey, new Integer(householdCounter));
					}
					LogUtil.debug(this.getClass(), "createAccessMembership");

					if (!exclude) {
						accessMembershipClientList.add(new AccessMembership(clientNumber, new Integer(membershipId), eligibilitySource));
						eligibleClients++;
					}

					// write out record to file
					try {
						fileWriter.write(clientNumber.toString() + FileUtil.SEPARATOR_COMMA + StringUtil.convertBooleanValue(eligibleInsurance) + FileUtil.SEPARATOR_COMMA + StringUtil.convertBooleanValue(eligibleProgressTravel.booleanValue()) + FileUtil.SEPARATOR_COMMA + StringUtil.convertBooleanValue(eligibleTramadaTravel.booleanValue()) + FileUtil.SEPARATOR_COMMA + StringUtil.convertBooleanValue(eligibleMembership) + FileUtil.SEPARATOR_COMMA + StringUtil.convertBooleanValue(exclude) + FileUtil.NEW_LINE);
					} catch (IOException ex5) {
						throw new RemoteException("Unable to write to file '" + auditFileName + "'.");
					}
				}
				// check if still running
				if (recordCount % 10000 == 0) {
					LogUtil.log(this.getClass(), recordCount + ": " + clientNumber + " " + householdKey);
				}
			}

		} catch (SQLException ex) {
			throw new RemoteException("Unable to get eligible customer list.", ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		try {
			auditFileWriter.write(StringUtil.rightPadString("Total non-roadside records", 75) + "=" + recordCount + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible (travel or insurance)", 75) + "=" + eligibleCount + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible insurance", 75) + "=" + insuranceCount + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible travel", 75) + "=" + travelCount + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible age segment 1 (0-" + AGE_SEGMENT_1 + ")", 75) + "=" + ageSegment1Count + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible age segment 2 (" + (AGE_SEGMENT_1 + 1) + "-" + AGE_SEGMENT_2 + ")", 75) + "=" + ageSegment2Count + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible age segment 3 (" + (AGE_SEGMENT_2 + 1) + "-" + AGE_SEGMENT_3 + ")", 75) + "=" + ageSegment3Count + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible age segment 4 (" + (AGE_SEGMENT_3 + 1) + "-" + AGE_SEGMENT_4 + ")", 75) + "=" + ageSegment4Count + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible age segment 5 (" + (AGE_SEGMENT_4) + "+)", 75) + "=" + ageSegment5Count + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible male", 75) + "=" + maleCount + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible female", 75) + "=" + femaleCount + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible households", 75) + "=" + households.size() + FileUtil.NEW_LINE);
			auditFileWriter.write(StringUtil.rightPadString("Eligible clients", 75) + "=" + eligibleClients + FileUtil.NEW_LINE);
			auditFileWriter.flush();
			auditFileWriter.close();

			errorFileWriter.close();

			fileWriter.close();
		} catch (IOException ex4) {
			throw new RemoteException("Unable to close file.", ex4);
		}

		return accessMembershipClientList;
	}

	/**
	 * @deprecated
	 * @param clientNumber
	 * @param effectiveDate
	 * @return
	 */
	private Boolean hasTramadaTravel(Integer clientNumber, DateTime effectiveDate) {
		Boolean hasTramadaTravel = Boolean.valueOf(false);
		;

		String SQL_HAS_TRAMADA_BOOKING = "SELECT booking_number FROM ss_tram_header " + "WHERE create_date >= ? AND status = 'A' " + "and ract_client_number = ?";

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			DataSource dwDataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_DW);
			connection = dwDataSource.getConnection();
			statement = connection.prepareStatement(SQL_HAS_TRAMADA_BOOKING);
			statement.setDate(1, effectiveDate.toSQLDate());
			statement.setInt(2, clientNumber.intValue());

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				hasTramadaTravel = Boolean.valueOf(true);
			}

		} catch (Exception ex) {
			LogUtil.warn(this.getClass(), "Unable to determine if client '" + clientNumber + "' has Tramada business. " + ExceptionHelper.getExceptionStackTrace(ex));
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return hasTramadaTravel;
	}

	/**
	 * Interrogate EDW to see if client has MRM Insurance.
	 * 
	 * @param clientNumber
	 * @return
	 */
	private Boolean hasMRMInsurance(Integer clientNumber) {
		Boolean hasMRMInsurance = Boolean.valueOf(false);

		long start = (new Date()).getTime();

		String SQL_HAS_MRM_INSURANCE = "SELECT COUNT(1) FROM [ract_InsuranceRiskExtensionBase] i\n" + "INNER JOIN [ract_insuranceriskBase] b ON i.ract_insuranceriskId = b.ract_insuranceriskId\n" + "INNER JOIN [ContactExtensionBase] c ON c.ContactId = i.ract_person\n" + "-- common\n" + "WHERE i.ract_expirydate > GETDATE()\n" + "AND\n" + "(\n" + "	-- putty\n" + "	i.ract_insuranceriskstatus = 'Current'\n" + "	-- pure\n" + "	OR (\n" + "		i.ract_PureVersionStatus IN ('Current','Under Renewal')\n" + "		AND i.ract_PureRiskStatus = 'c'\n" + "		AND i.ract_SubriskDeleted = 0\n" + "		AND i.ract_SecondaryPolicyDeleted = 0\n" + "	)\n" + ")\n" + "AND b.statecode = 0\n" + "AND c.ract_personid = ?\n";

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			DataSource dwDataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_EDW);
			connection = dwDataSource.getConnection();
			statement = connection.prepareStatement(SQL_HAS_MRM_INSURANCE);
			statement.setString(1, clientNumber.toString());

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				int count = rs.getInt(1);
				hasMRMInsurance = (count > 0);
			}

		} catch (Exception ex) {
			LogUtil.warn(this.getClass(), "Unable to determine if client '" + clientNumber + "' has MRM Insurance business. " + ExceptionHelper.getExceptionStackTrace(ex));
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		long end = (new Date()).getTime();

		LogUtil.info(getClass(), "Completed EDW MRM Insurance test for: " + clientNumber + " in " + (end - start) + " millis, has insurance? " + hasMRMInsurance);

		return hasMRMInsurance;
	}

	/**
	 * Gets the membershipTransactionDiscountList attribute of the MembershipTransactionMgrBean object
	 * 
	 * @param transID Description of the Parameter
	 * @param feeSpecID Description of the Parameter
	 * @return The membershipTransactionDiscountList value
	 * @exception RemoteException Description of the Exception
	 */
	public Vector getMembershipTransactionDiscountList(Integer transID, Integer feeSpecID) throws RemoteException {
		Vector memTransDiscList = new Vector();
		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = null;
		if (transID != null) {
			try {
				connection = clientDataSource.getConnection();
				statementText = "SELECT discount_type_code, discount_amount, discount_reason FROM PUB.mem_transaction_fee_discount WHERE transaction_id = ? AND fee_specification_id = ?";
				statement = connection.prepareStatement(statementText);
				statement.setInt(1, transID.intValue());
				statement.setInt(2, feeSpecID.intValue());
				ResultSet rs = statement.executeQuery();
				while (rs.next()) {
					String discTypeCode = rs.getString(1);
					double discAmount = rs.getDouble(2);
					String discReason = rs.getString(3);
					MembershipTransactionDiscount mtd = new MembershipTransactionDiscount(discTypeCode, discAmount, discReason);
					memTransDiscList.add(mtd);
				}
			} catch (SQLException e) {
				throw new RemoteException("Unable to get discount list " + statementText + " : " + e.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
		return memTransDiscList;
	}

	/**
	 * Get the list of recurring discounts, as MembershipDiscount, attached to the membership.
	 */
	public Collection getDiscountList(Integer membershipID) throws RemoteException {
		Connection connection = null;
		PreparedStatement statement = null;
		final String statementText = "SELECT discount_type_code FROM PUB.mem_membership_discount WHERE membership_id = ?";
		ArrayList discountList = new ArrayList();
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			statement.setInt(1, membershipID.intValue());
			ResultSet discountListRS = statement.executeQuery();
			while (discountListRS.next()) {
				String discountCode = discountListRS.getString(1);
				MembershipDiscount disc = new MembershipDiscount(discountCode);
				discountList.add(disc);
			}
		} catch (SQLException e) {
			throw new RemoteException("Error executing SQL " + statementText + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return discountList;
	}

	/**
	 * Look for the existence of a particular discount on a given membership for a particular end (expiry) date. Return true if the discount is present. Return false if not present. If the Start date is not provided, (is null), the search will be over all time. If membershipId is not supplied, return false; Ignore undone or cancelled transactions
	 * 
	 * @param Integer membership Id
	 * @param String Discount type code
	 * @param DateTime Start date of the period
	 * @return
	 * @throws RemoteException
	 */
	public boolean hasDiscount(Integer memId, String discountTypeCode, DateTime effectiveDate) throws RemoteException {
		boolean found = false;
		Connection connection = null;
		PreparedStatement statement = null;
		if (memId == null || memId.intValue() == 0)
			return false;

		String sql = "SELECT PUB.mem_transaction_fee_discount.discount_type_code" + " FROM  PUB.mem_membership INNER JOIN" + " PUB.mem_transaction ON PUB.mem_membership.membership_id = PUB.mem_transaction.membership_id INNER JOIN" + " PUB.mem_transaction_fee_discount ON PUB.mem_transaction.transaction_id = PUB.mem_transaction_fee_discount.transaction_id" + " WHERE (PUB.mem_transaction.membership_id = ?)" + " AND (PUB.mem_transaction_fee_discount.discount_type_code = ?)" + " and (pub.mem_transaction.undone_date is null)";
		if (effectiveDate != null) {
			sql += " AND (PUB.mem_transaction.effective_date >= ?)";
		}
		ArrayList discountList = new ArrayList();
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setInt(1, memId.intValue());
			statement.setString(2, discountTypeCode);
			if (effectiveDate != null) {
				statement.setDate(3, effectiveDate.toSQLDate());
			}
			ResultSet discountListRS = statement.executeQuery();
			if (discountListRS.next()) {
				found = true;
			}
		} catch (SQLException e) {
			throw new RemoteException("Error executing SQL " + sql + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return found;
	}

	public Collection getCurrentTransactionList(Integer membershipId) throws RemoteException {
		MethodCounter.addToCounter("MembershipMgr", "getCurrentTransactionList");
		DateTime currentDate = new DateTime();

		// if end date null they were previously excluded
		String queryString = "from MembershipTransactionVO mt where (mt.endDate >= :effectiveDate or mt.endDate is null) and mt.membershipID = :membershipId ";
		List result = hsession.createQuery(queryString).setDate("effectiveDate", currentDate).setInteger("membershipId", membershipId.intValue()).list();
		return result;
	}

	public MembershipVO getMembership(Integer membershipId) throws RemoteException {
		MethodCounter.addToCounter("MembershipMgr", "getMembership");
		LogUtil.debug(this.getClass(), "getMembership " + membershipId);

		MembershipVO membership = (MembershipVO) hsession.get(MembershipVO.class, membershipId);
		return membership;
	}

	public PayableItemVO getPayableItem(Integer payableItemID) throws RemoteException {

		if (payableItemID == null) return null;
		
		String queryString = "from PayableItemVO pi where pi.payableItemID = :payableItemID";
		List result = hsession.createQuery(queryString).setInteger("payableItemID", payableItemID.intValue()).list();
		if (result != null && result.size() > 0) {
			return (PayableItemVO) result.iterator().next();
		} else {
			return null;
		}
	}

	/**
	 * Get the list of documents, as membershipDocument , that have been created for the membership. The membership document value objects will be set to read only.
	 */
	public Collection getDocumentList(Integer membershipId) throws RemoteException {

		Vector docList = null;
		try {

			Criteria crit = hsession.createCriteria(MembershipDocument.class).add(Expression.eq("membershipId", membershipId));
			docList = new Vector(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return docList;
	}

	/**
	 * Get the list of eRenewal documents, as membershipDocument , that have been created for the membership and have been emailed. The membership document value objects will be set to read only.
	 */
	public List<MembershipDocument> getERenewalDocumentList(Integer membershipId) throws RemoteException {

		List<MembershipDocument> docList = new ArrayList<MembershipDocument>();
		try {
			String types[] = { MembershipDocument.RENEWAL_NOTICE, MembershipDocument.RENEWAL_ADVICE };

			Criteria crit = hsession.createCriteria(MembershipDocument.class).add(Expression.eq("membershipId", membershipId)).add(Expression.eq("emailed", true)).add(Expression.in("documentTypeCode", types));

			docList = (List<MembershipDocument>) crit.list();
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return docList;
	}

	/**
	 * Get the list of MembershipProductOptions associated with the Product Benefit Type.
	 * 
	 * @param productBenefitCode Description of the Parameter
	 * @return The productOptionList value
	 * @exception RemoteException Description of the Exception
	 */
	public Collection getProductOptionList(Integer membershipID) throws RemoteException {
		if (membershipID == null) {
			throw new RemoteException("Failed to fetch product option list. The membership ID cannot be null!");
		}
		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = "SELECT product_benefit_code, effective_date FROM PUB.mem_membership_product_option WHERE membership_id = ?";
		ArrayList optionList = new ArrayList();
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			statement.setInt(1, membershipID.intValue());
			ResultSet optionListRS = statement.executeQuery();
			while (optionListRS.next()) {
				String productBenefitCode = optionListRS.getString(1);
				java.sql.Date d = optionListRS.getDate(2);
				DateTime effectiveDate = (d == null ? null : new DateTime(d));
				MembershipProductOption po = new MembershipProductOption(productBenefitCode, effectiveDate);
				optionList.add(po);
			}
		} catch (SQLException e) {
			throw new RemoteException("Error executing SQL " + statementText + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return optionList;
	}

	/**
	 * checks whether a member is the prime addressee of a group
	 */
	public boolean isPrimeAddressee(Integer membershipID) throws RemoteException {
		if (membershipID == null) {
			throw new RemoteException("Failed to determine if prime addressee. The membership ID cannot be null!");
		}

		boolean primeAddressee = false;

		Connection connection = null;
		PreparedStatement statement = null;

		String sql = "SELECT is_Prime_Addressee ";
		sql += "FROM   PUB.mem_membership_group ";
		sql += "WHERE  membership_id = ? ";

		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setInt(1, membershipID.intValue());
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				primeAddressee = rs.getBoolean(1);
			} else {
				// single
				primeAddressee = true;
			}
		} catch (SQLException e) {
			throw new RemoteException("Error executing SQL " + sql + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return primeAddressee;
	}

	/**
	 * Return a reverse sorted list of transactions, as MembershipTransactionVO, for the membership. Sorting is based on transaction date.
	 * 
	 * @param membershipID Description of the Parameter
	 * @return The transactionList value
	 * @exception RemoteException Description of the Exception
	 */
	/*
	 * public Collection getTransactionList(Integer membershipID) throws RemoteException { LogUtil.debug(this.getClass(),"MemMgr getTransactionList1"+membershipID); // ArrayList memTransVOList = new ArrayList(); // MembershipTransactionHome membershipTransHome = MembershipEJBHelper. // getMembershipTransactionHome();
	 * 
	 * MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
	 * 
	 * ArrayList transList = (ArrayList)memMgr.findMembershipTransactionByMembershipID(membershipID); MembershipTransactionComparator membershipTxComparator = new MembershipTransactionComparator( MembershipTransactionComparator. SORTBY_TRANSACTION_DATE, true);
	 * 
	 * Collections.sort(transList, membershipTxComparator);
	 * 
	 * return transList; }
	 */

	/**
	 * Get the list of vehicles, as MembershipVehicle, that are attached to the membership.
	 * 
	 * @todo implement.
	 */
	public Collection getVehicleList(Integer membershipID) throws RemoteException {

		// // Load the membership vehicles
		// try
		// {
		// log("Loading membership vehicles for membershipID="+this.membershipID);
		// statementText =
		// "SELECT vehicle_id FROM PUB.mem_membership_vehicle WHERE membership_id = ?";
		// statement = connection.prepareStatement(statementText);
		// statement.setInt(1, this.membershipID.intValue());
		// ResultSet vehicleListRS = statement.executeQuery();
		// ArrayList vehicleList = new ArrayList();
		// while (vehicleListRS.next())
		// {
		// int vehicleID = vehicleListRS.getInt(1);
		// vehicleList.add(new Integer(vehicleID));
		// }
		// log("records found = " + vehicleList.size());
		// if (vehicleList.size() > 0)
		// this.vehicleIDList = vehicleList;
		// else
		// this.vehicleIDList = null;
		// }
		// catch(SQLException e)
		// {
		// throw new EJBException("Error executing SQL " + statementText + " : "
		// + e.toString());
		// }

		return new ArrayList();
	}

	/**
	 * Remove all membership cards issued on or after the specified date. If the specified date is null then all membership cards will be removed.
	 */
	@Deprecated
	public void removeMembershipCardsByIssueDate(Integer membershipID, DateTime issuedFromDate) throws RemoteException {
		MembershipCardVO memCard = null;
		LogUtil.debug(this.getClass(), "membershipID = " + membershipID);
		LogUtil.debug(this.getClass(), "findFromIssueDate 1");
		Collection cardList = findMembershipCardsByIssueDate(membershipID, issuedFromDate);
		LogUtil.debug(this.getClass(), "findFromIssueDate 2");
		Iterator cardIterator = cardList.iterator();
		while (cardIterator.hasNext()) {
			memCard = (MembershipCardVO) cardIterator.next();
			removeMembershipCard(memCard);
		}
	}

	@Deprecated
	private void removeMembershipCards(MembershipVO membership) {
		int rowsUpdated = hsession.createQuery("delete from MembershipCardVO c where membershipID = :membershipId").setInteger("membershipId", membership.getMembershipID()).executeUpdate();
		LogUtil.warn(this.getClass(), "removeMembershipCards " + rowsUpdated + " rows updated.");
	}

	@Deprecated
	public void createMembershipCard(MembershipCardVO memCard) throws RemoteException {

		LogUtil.log(this.getClass(), "cardid " + memCard.getCardID());

		String memberCardDataStore = MembershipCardVO.DATA_STORE_DATABASE;
		try {
			memberCardDataStore = FileUtil.getProperty("master", "memberCardDataStore");
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine member card datastore, defaulting to: " + memberCardDataStore);
		}

		String memberCardForceDB = "false";
		try {
			memberCardForceDB = FileUtil.getProperty("master", "memberCardForceDB");
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine member card force DB, defaulting to: " + memberCardForceDB);
		}
		Boolean forceDB = Boolean.parseBoolean(memberCardForceDB);

		if (memberCardDataStore.equals(MembershipCardVO.DATA_STORE_WEB_SERVICE)) {

			CardManagementService service = new CardManagementService();
			ICardManagementService portType = service.getBasicHttpBindingICardManagementService();

			int tierCode = (memCard.getTierCode() == null ? 1 : Integer.parseInt(memCard.getTierCode()));
			MembershipVO memVO = this.getMembership(memCard.getMembershipID());

			// tier changes and CMO only
			if (memCard.getReasonCode().equalsIgnoreCase(ReferenceDataVO.CARD_REQUEST_TIER_CHANGE) || memVO.getProductCode().equalsIgnoreCase(ProductVO.PRODUCT_CMO)) {
				LogUtil.log(getClass(), "Creating card via WS for (" + (memVO.getProductCode().equalsIgnoreCase(ProductVO.PRODUCT_CMO) ? "CMO" : "Tier Change") + ") c/n: " + memVO.getClientNumber());

				// Request parameter logging
				LogUtil.log(getClass(), "Logging the variables used in addCardRequest call:");
				LogUtil.log(getClass(), "=================================================");
				LogUtil.log(getClass(), "Client number - " + memVO.getClientNumber().toString());
				LogUtil.log(getClass(), "isPrimeAddressee - " + memVO.isPrimeAddressee());
				LogUtil.log(getClass(), "Tier code - " + tierCode);
				LogUtil.log(getClass(), "Staff member - " + memVO.getMembershipProfileCode().equals(MembershipProfileVO.PROFILE_RACT_STAFF));
				LogUtil.log(getClass(), "Rego number - " + memCard.getRego());
				LogUtil.log(getClass(), "Product Identifier - " + Integer.toString(memVO.getProduct().getProductIdentifier()));
				LogUtil.log(getClass(), "New card request - " + true);
				LogUtil.log(getClass(), "Reason code - " + memCard.getReasonCode());
				LogUtil.log(getClass(), "=================================================");

				// Call the service
				AddCardRequestResult result = portType.addCardRequest(memVO.getClientNumber().toString(), memVO.isPrimeAddressee(), tierCode, memVO.getMembershipProfileCode().equals(MembershipProfileVO.PROFILE_RACT_STAFF), memCard.getRego(), Integer.toString(memVO.getProduct().getProductIdentifier()), true, memCard.getReasonCode()); // always New

				Integer errorCode = result.getErrorCode();
				String errorMsg = result.getErrorMessage().getValue();
				if (errorCode != -1) {
					LogUtil.warn(getClass(), "Unable to create card for non-existent client # " + memVO.getClientNumber().toString() + " in MRM" + ": " + errorMsg);

					// ### CODE REMOVED - table no longer exists ####
					// Is there already a record?
					/*
					 * Query query = em.createNativeQuery( "select client_number from PUB.cmo_import_card_request_retry where client_number = :clientNumber" ).setParameter("clientNumber", memVO.getClientNumber().toString()); List client_list = query.getResultList(); if (client_list.isEmpty()) {
					 * 
					 * // if not, create record for MRM card request retry in PUB.cmo_import_card_request_retry - defaults to not processed - new quartz job will retry until successful or fails 5 times em.createNativeQuery( "INSERT INTO PUB.cmo_import_card_request_retry (client_number, is_prime_addressee, tier_code, staff, rego, product_identifier, is_new_card_request, reason_code, retry_count)  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)" ).setParameter(1, memVO.getClientNumber().toString()).setParameter(2, memVO.isPrimeAddressee()).setParameter(3, tierCode).setParameter(4, memVO.getMembershipProfileCode() .equals(MembershipProfileVO .PROFILE_RACT_STAFF)).setParameter(5, memCard.getRego()).setParameter(6, Integer.toString(memVO. getProduct().getProductIdentifier())).setParameter(7, true).setParameter(8, memCard.getReasonCode()).setParameter(9, 1).executeUpdate(); }
					 */

					// Don't do this - it fails everything
					// throw new
					// RemoteException("Unable to create membership card request for clientNo: "
					// + memVO.getClientNumber() + ": " + errorMsg);
				}
			}
		}

		// Now turned off
		if (forceDB || memberCardDataStore.equals(MembershipCardVO.DATA_STORE_DATABASE)) {

			LogUtil.log(getClass(), "Creating card via DB.");

			if (memCard.getCardID() == null) {
				SequenceMgr sequenceMgr = CommonEJBHelper.getSequenceMgr();
				Integer cardId = sequenceMgr.getNextID(SequenceMgr.SEQUENCE_MEM_CARD);
				// LogUtil.log(this.getClass(),"cardid 3 "+ cardId);
				memCard.setCardID(cardId);
			}
			hsession.save(memCard);
			hsession.flush();
		}

	}

	@Deprecated
	public MembershipCardVO getMembershipCard(Integer cardId) throws RemoteException {

		MembershipCardVO memCard = (MembershipCardVO) hsession.get(MembershipCardVO.class, cardId);
		return memCard;
	}

	@Deprecated
	public void updateMembershipCard(MembershipCardVO memCard) throws RemoteException {

		hsession.update(memCard);
	}

	public void updateMembershipDocument(MembershipDocument document) throws RemoteException {

		hsession.update(document);
	}

	/**
	 * create the card list for the given membership with the given cards. No longer used
	 */
	@Deprecated
	public void createMembershipCards(Collection cardList) throws RemoteException {
		// Insert the new cards
		// MembershipCardVO memCard = null;
		// Iterator cardIterator = cardList.iterator();
		// while (cardIterator.hasNext()) {
		// memCard = (MembershipCardVO) cardIterator.next();
		// createMembershipCard(memCard);
		// }
	}

	/**
	 * Undo a batch of access memberships that were created on a particular day
	 * 
	 * @param effectiveDate DateTime
	 * @param testMode boolean
	 * @throws RemoteException
	 */
	public void undoAccessMemberships(DateTime effectiveDate, boolean testMode, int limit) throws RemoteException {
		// ignore restoring group records as they have expired anyway

		LogUtil.log(this.getClass(), "undoAccessMemberships start");
		LogUtil.log(this.getClass(), "effectiveDate=" + effectiveDate);
		LogUtil.log(this.getClass(), "testMode=" + testMode);
		LogUtil.log(this.getClass(), "limit=" + limit);

		final String SQL_ACCESS_MEMBERSHIPS = "select distinct mm.membership_id from PUB.mem_membership mm, PUB.mem_transaction mt where mm.membership_id = mt.membership_id and mm.product_code = '" + ProductVO.PRODUCT_ACCESS + "' and mm.commence_date = ? and mt.username = ? and mt.transaction_type_code in ('" + MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE + "','" + MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN + "')";
		LogUtil.log(this.getClass(), "SQL_ACCESS_MEMBERSHIPS=" + SQL_ACCESS_MEMBERSHIPS);
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();

		User roadsideUser = SecurityHelper.getRoadsideUser();

		// issued or otherwise
		Connection connection = null;
		PreparedStatement statement = null;
		Integer membershipId = null;

		int undoCounter = 0;
		int recordCounter = 0;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(SQL_ACCESS_MEMBERSHIPS);
			statement.setDate(1, effectiveDate.toSQLDate());
			statement.setString(2, roadsideUser.getUserID());
			ResultSet accessRS = statement.executeQuery();
			while (accessRS.next()) {
				recordCounter++;
				membershipId = new Integer(accessRS.getInt(1));
				LogUtil.debug(this.getClass(), "membershipId=" + membershipId);
				if (limit == -1 || undoCounter < limit) {
					try {
						// new transactions
						membershipMgrLocal.undoAccessMembership(effectiveDate, testMode, membershipId);
						undoCounter++;
					} catch (UndoAccessMemberException ex) {
						LogUtil.warn(this.getClass(), ex.getMessage());
					}
				}
			}

		} catch (SQLException se) {
			throw new RemoteException("Unable to get access memberships", se);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		LogUtil.log(this.getClass(), "Access memberships undone=" + undoCounter + ", records=" + recordCounter);

		LogUtil.log(this.getClass(), "undoAccessMemberships end");

		// rollback for now
		// sessionContext.setRollbackOnly();
	}

	public Collection<FeeSpecificationVO> getFeeSpecifications(String productCode, String productBenefitCode, String membershipTypeCode, String transactionTypeCode, Integer groupNumber) throws GenericException {
		if (productCode == null && productBenefitCode == null) {
			throw new GenericException("Either a product code or a product benefit code must be specified.");
		}
		String JPQL_FEE_SPEC = "select e from FeeSpecificationVO e where";
		int paramCount = 0;
		boolean prod = false;
		int prodOrder = 0;
		if (productCode != null) {
			paramCount++;
			prodOrder = paramCount;
			prod = true;
			JPQL_FEE_SPEC += " e.productCode = ?" + prodOrder + " ";
		}
		boolean prodBen = false;
		int prodBenOrder = 0;
		if (productBenefitCode != null) {
			paramCount++;
			prodBenOrder = paramCount;
			prodBen = true;
			if (prod) {
				JPQL_FEE_SPEC += "and";
			}
			JPQL_FEE_SPEC += " e.productBenefitCode = ?" + prodBenOrder + " ";
		}
		int memTypeOrder = 0;
		boolean memType = false;
		if (membershipTypeCode != null) {
			paramCount++;
			memType = true;
			memTypeOrder = paramCount;
			JPQL_FEE_SPEC += "and e.membershipTypeCode = ?" + memTypeOrder + " ";
		} else {
			JPQL_FEE_SPEC += "and e.membershipTypeCode = '' ";
		}

		int transOrder = 0;
		boolean trans = false;
		if (transactionTypeCode != null) {
			paramCount++;
			trans = true;
			transOrder = paramCount;
			JPQL_FEE_SPEC += "and e.transactionTypeCode = ?" + transOrder + " ";
		} else {
			JPQL_FEE_SPEC += "and e.transactionTypeCode = '' ";
		}

		paramCount++;
		int groupOrder = paramCount;
		JPQL_FEE_SPEC += "and e.groupNumber = ?" + groupOrder + " ";
		LogUtil.debug(this.getClass(), "JPQL_FEE_SPEC=" + JPQL_FEE_SPEC);
		LogUtil.debug(this.getClass(), "prod " + prod + " " + prodOrder + " " + productCode);
		LogUtil.debug(this.getClass(), "prodBen " + prodBen + " " + prodBenOrder + " " + productBenefitCode);
		LogUtil.debug(this.getClass(), "memType " + memType + " " + memTypeOrder + " " + membershipTypeCode);
		LogUtil.debug(this.getClass(), "transactionTypeCode " + transOrder + " " + transactionTypeCode);
		LogUtil.debug(this.getClass(), "groupNumber " + groupOrder + " " + groupNumber);
		Query feeSpecQuery = em.createQuery(JPQL_FEE_SPEC);
		if (prod) {
			feeSpecQuery.setParameter(prodOrder, productCode);
		}
		if (prodBen) {
			feeSpecQuery.setParameter(prodBenOrder, productBenefitCode);
		}
		if (memType) {
			feeSpecQuery.setParameter(memTypeOrder, membershipTypeCode);
		}
		if (trans) {
			feeSpecQuery.setParameter(transOrder, transactionTypeCode);
		}
		feeSpecQuery.setParameter(groupOrder, groupNumber);
		return feeSpecQuery.getResultList();
	}

	public Collection<FeeSpecificationVO> getFeeSpecifications(String productCode, String productBenefitCode, String membershipTypeCode, String transactionTypeCode, String feeTypeCode, Integer groupNumber) throws GenericException {

		String JPQL_FEE_SPEC = "select e from FeeSpecificationVO e where";
		int paramCount = 0;
		boolean prod = false;
		int prodOrder = 0;
		if (productCode != null) {
			paramCount++;
			prodOrder = paramCount;
			prod = true;
			JPQL_FEE_SPEC += " e.productCode = ?" + prodOrder + " ";
		} else {
			JPQL_FEE_SPEC += " e.productCode is null ";
		}
		boolean prodBen = false;
		int prodBenOrder = 0;
		if (productBenefitCode != null) {
			paramCount++;
			prodBenOrder = paramCount;
			prodBen = true;
			JPQL_FEE_SPEC += "and e.productBenefitCode = ?" + prodBenOrder + " ";
		} else {
			JPQL_FEE_SPEC += "and (e.productBenefitCode is null or e.productBenefitCode = '') ";
		}
		int memTypeOrder = 0;
		boolean memType = false;
		if (membershipTypeCode != null) {
			paramCount++;
			memType = true;
			memTypeOrder = paramCount;
			JPQL_FEE_SPEC += "and e.membershipTypeCode = ?" + memTypeOrder + " ";
		}

		int transOrder = 0;
		boolean trans = false;
		if (transactionTypeCode != null) {
			paramCount++;
			trans = true;
			transOrder = paramCount;
			JPQL_FEE_SPEC += "and e.transactionTypeCode = ?" + transOrder + " ";
		}

		int feeOrder = 0;
		boolean feeType = false;
		if (feeTypeCode != null) {
			paramCount++;
			feeType = true;
			feeOrder = paramCount;
			JPQL_FEE_SPEC += "and e.feeTypeCode = ?" + feeOrder + " ";
		}

		paramCount++;
		int groupOrder = paramCount;
		JPQL_FEE_SPEC += "and e.groupNumber = ?" + groupOrder + " ";
		JPQL_FEE_SPEC += "order by e.effectiveFromDate desc";
		LogUtil.debug(this.getClass(), "JPQL_FEE_SPEC=" + JPQL_FEE_SPEC);
		LogUtil.debug(this.getClass(), "prod " + prod + " " + prodOrder + " " + productCode);
		LogUtil.debug(this.getClass(), "prodBen " + prodBen + " " + prodBenOrder + " " + productBenefitCode);
		LogUtil.debug(this.getClass(), "memType " + memType + " " + memTypeOrder + " " + membershipTypeCode);
		LogUtil.debug(this.getClass(), "transactionTypeCode " + transOrder + " " + transactionTypeCode);
		LogUtil.debug(this.getClass(), "groupNumber " + groupOrder + " " + groupNumber);
		Query feeSpecQuery = em.createQuery(JPQL_FEE_SPEC);
		if (prod) {
			feeSpecQuery.setParameter(prodOrder, productCode);
		}
		if (prodBen) {
			feeSpecQuery.setParameter(prodBenOrder, productBenefitCode);
		}
		if (memType) {
			feeSpecQuery.setParameter(memTypeOrder, membershipTypeCode);
		}
		if (trans) {
			feeSpecQuery.setParameter(transOrder, transactionTypeCode);
		}
		if (feeType) {
			feeSpecQuery.setParameter(feeOrder, feeTypeCode);
		}
		feeSpecQuery.setParameter(groupOrder, groupNumber);
		return feeSpecQuery.getResultList();
	}

	public Collection<FeeSpecificationVO> getFeeSpecification(String feeTypeCode) {
		final String JPQL_FEE_SPEC = "select e from FeeSpecificationVO e where e.feeTypeCode = ?1";
		Query feeSpecQuery = em.createQuery(JPQL_FEE_SPEC);
		feeSpecQuery.setParameter(1, feeTypeCode);
		return feeSpecQuery.getResultList();
	}

	public Collection<FeeSpecificationVO> getOrderedFeeSpecifications() {
		final String JPQL_FEE_SPEC = "select e from FeeSpecificationVO e order by e.membershipTypeCode, e.productCode, e.productBenefitCode, e.transactionTypeCode, e.feeTypeCode, e.groupNumber";
		Query feeSpecQuery = em.createQuery(JPQL_FEE_SPEC);
		return feeSpecQuery.getResultList();
	}

	public Collection<FeeSpecificationVO> getFeeSpecifications() {
		final String JPQL_FEE_SPEC = "select e from FeeSpecificationVO e";
		Query feeSpecQuery = em.createQuery(JPQL_FEE_SPEC);
		return feeSpecQuery.getResultList();
	}

	public Collection<MembershipAccountVO> getMembershipAccounts() {
		final String JPQL_ACCOUNT = "select e from MembershipAccountVO e";
		Query accountQuery = em.createQuery(JPQL_ACCOUNT);
		return accountQuery.getResultList();
	}

	public Collection<FeeTypeVO> getFeeTypes() {
		final String JPQL_FEE_TYPE = "select e from FeeTypeVO e";
		Query feeTypeQuery = em.createQuery(JPQL_FEE_TYPE);
		return feeTypeQuery.getResultList();
	}

	public FeeSpecificationVO getFeeSpecification(Integer feeSpecificationId) {
		return em.find(FeeSpecificationVO.class, feeSpecificationId);
	}

	public FeeTypeVO getFeeType(String feeTypeCode) {
		return em.find(FeeTypeVO.class, feeTypeCode);
	}

	public MembershipAccountVO getMembershipAccount(Integer accountId) {
		return em.find(MembershipAccountVO.class, accountId);
	}

	public void deleteFeeSpecification(FeeSpecificationVO feeSpec) {
		feeSpec = em.merge(feeSpec);
		em.remove(feeSpec);
	}

	public FeeSpecificationVO updateFeeSpecification(FeeSpecificationVO feeSpec) {
		return em.merge(feeSpec);
	}

	public void createFeeSpecification(FeeSpecificationVO feeSpec) {
		try {
			SequenceMgr seqMgr = CommonEJBHelper.getSequenceMgr();
			feeSpec.setFeeSpecificationID(seqMgr.getNextID(SequenceMgr.SEQUENCE_MEM_FEE_SPECIFICATION));
		} catch (RemoteException e) {
			// Ignore
		}
		em.persist(feeSpec);
	}

	public void deleteMembershipAccount(MembershipAccountVO memAccount) {
		memAccount = em.merge(memAccount);
		em.remove(memAccount);
	}

	public MembershipAccountVO updateMembershipAccount(MembershipAccountVO memAccount) {
		return em.merge(memAccount);
	}

	public MembershipAccountVO createMembershipAccount(MembershipAccountVO memAccount) {
		try {
			SequenceMgr seqMgr = CommonEJBHelper.getSequenceMgr();
			memAccount.setAccountID(seqMgr.getNextID(SequenceMgr.SEQUENCE_MEM_ACCOUNT));
		} catch (RemoteException e) {
			// Ignore
		}
		em.persist(memAccount);
		return memAccount;
	}

	public void deleteFeeType(FeeTypeVO feeType) {
		feeType = em.merge(feeType);
		em.remove(feeType);
	}

	public FeeTypeVO updateFeeType(FeeTypeVO feeType) {
		return em.merge(feeType);
	}

	public void createFeeType(FeeTypeVO feeType) {
		em.persist(feeType);
	}

	/*
	 * findOnly(productCode, productBenefitCode, memTypeCode, transTypeCode, numberInGroup);
	 */

	/**
	 * Undo an access membership.
	 * 
	 * @param effectiveDate DateTime
	 * @param testMode boolean
	 * @param membershipId Integer
	 * @throws RemoteException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void undoAccessMembership(DateTime effectiveDate, boolean testMode, Integer membershipId) throws UndoAccessMemberException {

		LogUtil.log(this.getClass(), "undoAccessMembership start");
		LogUtil.log(this.getClass(), "effectiveDate=" + effectiveDate);
		LogUtil.log(this.getClass(), "testMode=" + testMode);
		LogUtil.log(this.getClass(), "membershipId=" + membershipId);

		final String SQL_DELETE_MEMBERSHIP_CARD = "delete from PUB.mem_membership_card where membership_id = ? and request_date = ?";
		// final String SQL_DELETE_MEMBERSHIP_TRANSACTION =
		// "delete from PUB.mem_transaction where membership_id = ? and transaction_date = ?";

		MembershipVO undoMemVO = null;
		MembershipVO revertMemVO = null;
		MembershipTransactionVO undoTransaction = null;
		MembershipTransactionVO revertTransaction = null;
		MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE, true);
		ArrayList txList = null;
		int index = 0;
		int cardDeleteCount = 0;
		int transactionDeleteCount = 0;
		int membershipDeleteCount = 0;
		int membershipUndoCount = 0;

		PreparedStatement deleteCardStatement = null;
		PreparedStatement deleteTransactionStatement = null;
		Connection connection = null;
		try {
			MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
			connection = clientDataSource.getConnection();
			membershipDeleteCount = 0;
			membershipUndoCount = 0;

			LogUtil.debug(this.getClass(), "membershipId=" + membershipId);

			undoMemVO = membershipMgrLocal.getMembership(membershipId);

			// error if not access product
			if (!undoMemVO.getProductCode().equals(ProductVO.PRODUCT_ACCESS)) {
				throw new UndoAccessMemberException("Not able to undo access membership '" + membershipId + "' as product has been changed.");
			}

			txList = (ArrayList) undoMemVO.getTransactionList();
			Collections.sort(txList, mtc);
			// in reverse order the transaction to undo is the first
			undoTransaction = (MembershipTransactionVO) txList.get(index);
			if (undoTransaction.isUndone()) {
				throw new UndoAccessMemberException("Unable to undo from an undone transaction.");
			}

			if (undoTransaction.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE)) {
				// delete membership
				membershipMgrLocal.removeMembership(undoMemVO);
				membershipDeleteCount++;
				transactionDeleteCount++;
			} else if (undoTransaction.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN)) {
				// find revert to transaction

				// revert membership is the next transaction
				boolean revertToFound = false;
				for (int i = 1; i < txList.size() && !revertToFound; i++) {

					revertTransaction = (MembershipTransactionVO) txList.get(i);
					revertToFound = !revertTransaction.isUndone();

				}

				if (revertTransaction == null) {
					throw new UndoAccessMemberException("No revert to transaction.");
				} else {
					revertMemVO = (MembershipVO) revertTransaction.getMembershipHistory();
				}

				if (revertMemVO == null && testMode) // ie. no membership
				// history against
				// transaction
				{
					// set arbitrary values on membership
					DateTime commenceDate = effectiveDate.subtract(new Interval(3, 0, 0, 0, 0, 0, 0));
					DateTime expiryDate = effectiveDate.subtract(new Interval(2, 0, 0, 0, 0, 0, 0));
					LogUtil.warn(this.getClass(), "No history found for transaction " + revertTransaction.getTransactionID() + ".");
					revertMemVO = undoMemVO.copy();
					revertMemVO.setProductCode(revertTransaction.getProductCode());
					revertMemVO.setProductEffectiveDate(commenceDate);
					revertMemVO.setJoinDate(commenceDate);
					revertMemVO.setCommenceDate(commenceDate);
					// make record eligible again.
					revertMemVO.setExpiryDate(expiryDate);
				} else {
					throw new UndoAccessMemberException("Unable to find revert transaction.");
				}

				// update membership
				membershipMgrLocal.updateMembership(revertMemVO);
				membershipUndoCount++;

				// delete transaction
				removeMembershipTransaction(undoTransaction);
				transactionDeleteCount++;
			} else {
				throw new UndoAccessMemberException("Transaction type '" + undoTransaction.getTransactionTypeCode() + "' may not be undone for membership '" + membershipId + "'.");
			}

			// delete card (issued or otherwise
			deleteCardStatement = connection.prepareStatement(SQL_DELETE_MEMBERSHIP_CARD);
			deleteCardStatement.setInt(1, membershipId.intValue());
			deleteCardStatement.setDate(2, effectiveDate.toSQLDate());
			cardDeleteCount = deleteCardStatement.executeUpdate();

			LogUtil.log(this.getClass(), "undo " + membershipId + ": mu=" + membershipUndoCount + " md=" + membershipDeleteCount + " td=" + transactionDeleteCount + " cd=" + cardDeleteCount);

		} catch (Exception ex) {
			sessionContext.setRollbackOnly(); // as requiresnew this will
			// rollback this transaction only
			throw new UndoAccessMemberException(membershipId + ": " + ExceptionHelper.getExceptionStackTrace(ex));
		} finally {
			ConnectionUtil.closeStatement(deleteTransactionStatement);
			ConnectionUtil.closeStatement(deleteCardStatement);
			ConnectionUtil.closeConnection(connection);
		}

		LogUtil.log(this.getClass(), "undoAccessMembership end");
	}

	// Added expiryDate to support multi-year CMO memberships

	public void createCMOMembership(Integer clientNumber, Integer membershipId, DateTime effectiveDate, String rego, String vin, String make, String model, String year, String profile, DateTime expiryDate) throws RemoteException {
		createProductTypeMembership(clientNumber, membershipId, effectiveDate, SourceSystem.CMO.getAbbreviation(), rego, vin, make, model, year, profile, expiryDate);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void createAccessMembership(Integer clientNumber, Integer membershipId, DateTime effectiveDate, String eligibilitySource) throws RemoteException {
		createProductTypeMembership(clientNumber, membershipId, effectiveDate, eligibilitySource, null, null, null, null, null, null, null);
	}

	/**
	 * Create a membership record, transaction record and an issued card request.
	 * 
	 * @param clientNumber Integer
	 * @param membershipId Integer
	 * @throws RemoteException
	 */
	private void createProductTypeMembership(Integer clientNumber, Integer membershipId, DateTime effectiveDate, String eligibilitySource, String rego, String vin, String make, String model, String year, String profile, DateTime expiryDate) throws RemoteException {
		LogUtil.log(this.getClass(), "createAccessMembership start " + clientNumber + " " + membershipId + " " + effectiveDate + " " + eligibilitySource);

		Collection memberships = findMembershipByClientNumber(clientNumber);
		if (memberships != null && memberships.size() > 0 && // at least one
		// membership
		membershipId == null) // no membership provided
		{
			throw new RemoteException("Attempting to create a new membership but a membership already exists for client " + clientNumber + ".");
		}

		ClientMgrLocal clientMgrLocal = ClientEJBHelper.getClientMgrLocal();
		ClientVO client = clientMgrLocal.getClient(clientNumber);

		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		MembershipTransactionVO memTransVO = null;

		if (membershipId != null) {
			oldMemVO = getMembership(membershipId);
		}

		String joinReasonCode = null;
		String profileCode = null;
		if (eligibilitySource.equals(SourceSystem.TRAVEL.getAbbreviation())) {
			joinReasonCode = ReferenceDataVO.REASON_ACCESS_TRAVEL;
			profileCode = MembershipProfileVO.PROFILE_GIFTED_TRAVEL;
		} else if (eligibilitySource.equals(SourceSystem.INSURANCE.getAbbreviation())) {
			joinReasonCode = ReferenceDataVO.REASON_ACCESS_INSURANCE;
			profileCode = MembershipProfileVO.PROFILE_GIFTED_INSURANCE;
		} else if (eligibilitySource.equals(SourceSystem.CMO.getAbbreviation())) {
			joinReasonCode = ReferenceDataVO.REASON_CMO;
			profileCode = profile;
		}

		if (joinReasonCode == null) {
			throw new SystemException("Failed to get join/rejoin reason code");
		}

		LogUtil.log(this.getClass(), "joinReasonCode=" + joinReasonCode);
		LogUtil.log(this.getClass(), "profileCode=" + profileCode);

		String cardReasonCode = null;
		cardReasonCode = joinReasonCode;

		ReferenceDataVO joinReason = null;

		User roadsideUser = SecurityHelper.getRoadsideUser();

		LogUtil.log(this.getClass(), "roadsideUser=" + roadsideUser);

		TransactionGroupCreateImpl transGroupCreate = null;

		String transactionReason = null;
		if (oldMemVO != null) {
			if (oldMemVO.isGroupMember()) {
				// remove from group - old version incorrectly disbanded the
				// group. Does not record a change group transaction for other
				// group members.
				MembershipHelper.removeFromAllGroups(oldMemVO.getMembershipID());

				// reset so that subsequence fetch gets the right members
				oldMemVO.resetMembershipGroup();

			}

			transGroupCreate = new TransactionGroupAccessCreateImpl(roadsideUser, MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN, joinReasonCode, effectiveDate, profileCode);
			transactionReason = transGroupCreate.addSelectedClient(oldMemVO);
		} else {
			if (joinReasonCode.equals(ReferenceDataVO.REASON_CMO)) {
				transGroupCreate = new TransactionGroupCMOCreateImpl(roadsideUser, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, joinReasonCode, effectiveDate, profileCode);
			} else {
				transGroupCreate = new TransactionGroupAccessCreateImpl(roadsideUser, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, joinReasonCode, effectiveDate, profileCode);
			}
			transactionReason = transGroupCreate.addSelectedClient(client);
		}

		if (transactionReason != null && transactionReason.trim().length() > 0) {
			throw new RemoteException("Unable to add selected client. " + transactionReason);
		}
		if (oldMemVO != null) {
			transGroupCreate.setContextClient(oldMemVO);
		} else {
			transGroupCreate.setContextClient(client);
		}
		transGroupCreate.createTransactionsForSelectedClients();
		transGroupCreate.createTransactionFees();
		transGroupCreate.calculateTransactionFee();

		if (transGroupCreate.getAmountPayable() != 0) {
			throw new ValidationException("There should be no amount payable for access memberships.");
		}

		memTransVO = transGroupCreate.getMembershipTransactionForPA();

		newMemVO = memTransVO.getNewMembership();
		if (joinReasonCode.equals(ReferenceDataVO.REASON_CMO)) {
			newMemVO.setRego(rego);
			newMemVO.setVin(vin);
			newMemVO.setMake(make);
			newMemVO.setModel(model);
			newMemVO.setYear(year);

			// Supports multi-year CMO nominations
			// RLG 09/05/2019 if (!expiryDate.equals(null))
			if (expiryDate != null) {
				newMemVO.setExpiryDate(expiryDate);
			}
		}

		// check last membership transaction to see if history exists. if not
		// create it so we can undo the transaction.
		if (oldMemVO == null) {
			createMembership(newMemVO);
		} else {
			MembershipTransactionVO lastTransVO = oldMemVO.getLastTransaction();
			try {
				if (lastTransVO.getMembershipHistory() == null) {
					lastTransVO.setMembershipXML(oldMemVO.toXML());
					updateMembershipTransaction(lastTransVO);
				}
			} catch (Exception e) {
				// an exception getting
				LogUtil.warn(this.getClass(), "Unable to get membership history. " + oldMemVO);
				LogUtil.warn(this.getClass(), e);
			}

			updateMembership(newMemVO);
		}

		memTransVO.setMembershipXML(newMemVO.toXML());

		// set membership id on transaction
		memTransVO.setMembershipID(newMemVO.getMembershipID());
		createMembershipTransaction(memTransVO);

		// ************** Not required *************
		// create a membership card request only
		// MembershipCardVO cardVO = new
		// MembershipCardVO(newMemVO.getMembershipID(), 0, effectiveDate,
		// ReferenceDataVO.REF_TYPE_MEMBERSHIP_CARD_REQUEST, cardReasonCode,
		// String.valueOf(MembershipTier.TIER_CODE_DEFAULT), effectiveDate,
		// newMemVO.getRego());
		//
		// // Create the card record.
		// createMembershipCard(cardVO);

		// ensure correct motor news options are set
		client.setMotorNewsSendOption(Client.MOTOR_NEWS_MEMBERSHIP_SEND);
		client.setMotorNewsDeliveryMethod(Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL);
		clientMgrLocal.updateClient(client);

		LogUtil.log(this.getClass(), "createAccessMembership end " + clientNumber + " " + membershipId);
	}

	public MembershipProfileVO getMembershipProfile(String profileCode) {
		return em.find(MembershipProfileVO.class, profileCode);
	}

	public Collection<MembershipProfileVO> getMembershipProfiles() {
		javax.persistence.Query profileQuery = em.createQuery("select e from MembershipProfileVO e");
		return profileQuery.getResultList();
	}

	public void createMembershipProfile(MembershipProfileVO profile) throws RemoteException {
		em.persist(profile);
		insertProfileDiscountList(profile.getProfileCode(), profile.getDiscountList());
	}

	public void deleteMembershipProfile(MembershipProfileVO profile) throws RemoteException {
		profile = em.merge(profile);
		em.remove(profile);
		removeProfileDiscountList(profile.getProfileCode());
	}

	public MembershipProfileVO updateMembershipProfile(MembershipProfileVO profile) throws RemoteException {
		MembershipProfileVO updatedProfile = em.merge(profile);
		removeProfileDiscountList(profile.getProfileCode());
		insertProfileDiscountList(profile.getProfileCode(), profile.getDiscountList());
		return updatedProfile;
	}

	public static final String ACCESS_CARD_REASON = "Access card initiative";

	/**
	 * Get a list of membership document remote interface to remove
	 */
	public void removeDocumentList(Integer membershipId) throws RemoteException {

		try {
			Collection documentList = getDocumentList(membershipId);
			MembershipDocument memDocVO = null;
			Iterator docIt = documentList.iterator();

			while (docIt.hasNext()) {
				memDocVO = (MembershipDocument) docIt.next();
				hsession.delete(memDocVO);
			}
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}

	}

	public void removeDiscountType(DiscountTypeVO discountType) throws RemoteException {

		// Check mem_allowed_fee_discounts to see if this discount is being used
		Connection connection = null;
		String statementText = null;
		PreparedStatement statement = null;
		try {
			connection = clientDataSource.getConnection();
			statementText = "select 'x' from pub.mem_allowed_fee_discount where discount_type_code = ?";
			statement = connection.prepareStatement(statementText);
			statement.setString(1, discountType.getDiscountTypeCode());
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				throw new RemoteException("Discount '" + discountType.getDiscountTypeCode() + "' is in use. Unable to delete.");
			}
		} catch (SQLException e) {
			throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		removeAllowedFeeDiscountList(discountType);

		hsession.delete(discountType);

	}

	/**
	 * Replace the document list for the given membership with the given documents.
	 */
	public void createMembershipDocuments(Collection docVOList) throws RemoteException {

		MembershipDocument memDocVO;
		Iterator docVOIterator = docVOList.iterator();
		while (docVOIterator.hasNext()) {
			memDocVO = (MembershipDocument) docVOIterator.next();
			// Insert the new documents
			hsession.save(memDocVO);
		}
		hsession.flush();
	}

	public void createDiscountType(DiscountTypeVO discountType) throws RemoteException {

		hsession.save(discountType);

		insertAllowedFeesDiscountList(discountType);
	}

	public void updateDiscountType(DiscountTypeVO discountType) throws RemoteException {

		hsession.update(discountType);

		removeAllowedFeeDiscountList(discountType);
		insertAllowedFeesDiscountList(discountType);

	}

	public Collection getMembershipTransactionTypes() {
		Query membershipTypeQuery = em.createQuery("select e from MembershipTransactionTypeVO e");
		return membershipTypeQuery.getResultList();
	}

	public Collection getMembershipTypes() {
		Query membershipTypeQuery = em.createQuery("select e from MembershipTypeVO e");
		return membershipTypeQuery.getResultList();
	}

	public MembershipTypeVO getMembershipType(String membershipTypeCode) {
		return em.find(MembershipTypeVO.class, membershipTypeCode);
	}

	public Collection<MembershipTypeVO> getMembershipTypeByClientType(String clientTypeCode) {
		final String SQL_MEMBERSHIP_TYPES = "select membership_type_code from mem_allowed_client_type where client_type_code = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		String membershipTypeCode = null;
		MembershipTypeVO membershipType = null;
		Collection<MembershipTypeVO> membershipTypeList = new ArrayList<MembershipTypeVO>();
		try {
			try {
				connection = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT).getConnection();
			} catch (NamingException e) {
				e.printStackTrace();
			}
			statement = connection.prepareStatement(SQL_MEMBERSHIP_TYPES);
			statement.setString(1, clientTypeCode);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				membershipTypeCode = resultSet.getString(1);
				membershipType = getMembershipType(membershipTypeCode);
				membershipTypeList.add(membershipType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return membershipTypeList;
	}

	public void createMembership(MembershipVO membership) throws RemoteException {
		LogUtil.debug(this.getClass(), "createMembership start");

		LogUtil.debug(this.getClass(), "createMembership, 1" + hsession);
		// set the id - expose membership id
		MembershipIDMgr memIDMgr = MembershipEJBHelper.getMembershipIDMgr();
		LogUtil.debug(this.getClass(), "createMembership, 1a");
		int membershipID = memIDMgr.getNextMembershipID();
		LogUtil.debug(this.getClass(), "createMembership,1b membershipID=" + membershipID);
		LogUtil.debug(this.getClass(), "createMembership,1c membership=" + membership);
		membership.setMembershipID(new Integer(membershipID));
		membership.setLastUpdate(new DateTime());
		LogUtil.debug(this.getClass(), "createMembership,2 membership=" + membership);
		// Insert the new membership
		hsession.save(membership);
		LogUtil.debug(this.getClass(), "createMembership,3");
		// Insert the product options attached to the membership.
		insertProductOptionList(membership);
		LogUtil.debug(this.getClass(), "createMembership,3a");
		// Insert the discounts attached to the membership.
		insertDiscountList(membership);
		LogUtil.debug(this.getClass(), "createMembership,3b");
		// Insert the vehicles attached to the membership.
		insertVehicleList(membership);
		LogUtil.debug(this.getClass(), "createMembership,3c");
		hsession.flush();
		LogUtil.debug(this.getClass(), "createMembership,3d membership=" + membership);
		LogUtil.debug(this.getClass(), "createMembership end");
	}

	/**
	 * Insert the product option records for the membership. Product options are stored as a list of MembershipProductOption objects.
	 * 
	 * @param connection Description of the Parameter
	 * @exception CreateException Description of the Exception
	 */
	private void insertProductOptionList(MembershipVO membership) throws RemoteException {
		Connection connection = null;
		LogUtil.log(this.getClass(), "insertProductOptionList 1");
		Collection productOptionList = membership.getProductOptionList();
		if (productOptionList != null && !productOptionList.isEmpty()) {
			LogUtil.log(this.getClass(), "insertProductOptionList 2");
			String statementText = "INSERT INTO PUB.mem_membership_product_option (product_benefit_code, membership_id, effective_date) VALUES (?, ?, ?)";
			PreparedStatement statement = null;
			DateTime effectiveDate = null;
			MembershipProductOption productOption = null;
			String prodBenefitTypeCode = null;

			try {
				connection = clientDataSource.getConnection();
				Iterator iterator = productOptionList.iterator();
				while (iterator.hasNext()) {
					productOption = (MembershipProductOption) iterator.next();
					prodBenefitTypeCode = productOption.getProductBenefitTypeCode();
					statement = connection.prepareStatement(statementText);
					statement.setString(1, prodBenefitTypeCode);
					statement.setInt(2, membership.getMembershipID().intValue());

					// If the option effective date is not set then default it
					// to
					// the base product effective date.
					effectiveDate = productOption.getEffectiveDate();
					if (effectiveDate == null) {
						effectiveDate = membership.getProductEffectiveDate();
					}

					if (effectiveDate != null) {
						statement.setDate(3, effectiveDate.toSQLDate());
					} else {
						statement.setNull(3, Types.DATE);
					}

					if (statement.executeUpdate() != 1) {
						throw new RemoteException("Error adding membership product option with code = " + prodBenefitTypeCode + "," + membership.getMembershipID());
					}
				}
			} catch (SQLException e) {
				throw new RemoteException("Error executing SQL " + statementText + " with values '" + prodBenefitTypeCode + "', '" + membership.getMembershipID() + "', '" + effectiveDate.toSQLDate() + "' : " + e.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	/**
	 * Insert the discounts that have been attached to the membership.
	 * 
	 * @param connection Description of the Parameter
	 * @exception CreateException Description of the Exception
	 */
	private void insertDiscountList(MembershipVO membership) throws RemoteException {
		Connection connection = null;
		LogUtil.debug(this.getClass(), "insertDiscountList 1 " + membership.getMembershipID());
		Collection discountList = membership.getDiscountList();
		if (discountList != null && !discountList.isEmpty()) {
			String statementText = "INSERT INTO PUB.mem_membership_discount (membership_id, discount_type_code) VALUES (?, ?)";
			PreparedStatement statement = null;
			String discountCode = null;
			try {
				MembershipDiscount memDisc = null;
				Iterator iterator = discountList.iterator();
				while (iterator.hasNext()) {
					memDisc = (MembershipDiscount) iterator.next();
					discountCode = memDisc.getDiscountTypeCode();
					connection = clientDataSource.getConnection();
					statement = connection.prepareStatement(statementText);
					statement.setInt(1, membership.getMembershipID().intValue());
					statement.setString(2, discountCode);
					if (statement.executeUpdate() != 1) {
						throw new RemoteException("Error adding membership discount with key = " + membership.getMembershipID() + "," + discountCode);
					}
				}
			} catch (SQLException e) {
				throw new RemoteException("Error executing SQL " + statementText + ". With values '" + membership.getMembershipID() + "', '" + discountCode + "' : " + e.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	/**
	 * @param connection Description of the Parameter
	 * @exception CreateException Description of the Exception
	 * @todo revise insertVehicleList()
	 */
	private void insertVehicleList(MembershipVO membership) throws RemoteException {
		LogUtil.debug(this.getClass(), "insertVehicleList 1=" + membership.getMembershipID());
	}

	/**
	 * Delete all of the product options for the membership
	 * 
	 * @param connection Description of the Parameter
	 * @exception SQLException Description of the Exception
	 */
	private void removeProductOptionList(MembershipVO membership) throws RemoteException {
		Connection connection = null;
		LogUtil.debug(this.getClass(), "removeProductOptionList 1");
		Collection productOptionList = membership.getProductOptionList();
		if (productOptionList != null) {
			LogUtil.debug(this.getClass(), "removeProductOptionList 2");
			PreparedStatement statement = null;
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement("DELETE FROM PUB.mem_membership_product_option WHERE membership_id = ?");
				statement.setInt(1, membership.getMembershipID().intValue());
				statement.executeUpdate();
				// Don't care if no rows were deleted.
			} catch (SQLException se) {
				throw new RemoteException("Unable to remove production option list.", se);
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	/**
	 * Delete all of the discounts attached to the membership.
	 * 
	 * @param connection Description of the Parameter
	 * @exception SQLException Description of the Exception
	 */
	private void removeDiscountList(MembershipVO membership) throws RemoteException {
		Connection connection = null;
		LogUtil.debug(this.getClass(), "removeDiscountList 1");
		Collection discountList = membership.getDiscountList();
		if (discountList != null) {
			LogUtil.debug(this.getClass(), "removeDiscountList 2");
			PreparedStatement statement = null;
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement("DELETE FROM PUB.mem_membership_discount WHERE membership_id = ?");
				statement.setInt(1, membership.getMembershipID().intValue());
				statement.executeUpdate();
				// Don't care if no rows were deleted.
			} catch (SQLException se) {
				throw new RemoteException("Unable to remove discount list.", se);
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	/**
	 * Delete all of the vehicles attached to the membership.
	 * 
	 * @param connection Description of the Parameter
	 * @exception SQLException Description of the Exception
	 */
	private void removeVehicleList(MembershipVO membership) throws RemoteException {
		LogUtil.log(this.getClass(), "removeVehicleList 1");
		Connection connection = null;
		Collection vehicleList = membership.getVehicleList();
		if (vehicleList != null) {
			LogUtil.log(this.getClass(), "removeVehicleList 2");
			PreparedStatement statement = null;
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement("DELETE FROM PUB.mem_membership_vehicle WHERE membership_id = ?");
				statement.setInt(1, membership.getMembershipID().intValue());
				statement.executeUpdate();
				// Don't care if no rows were deleted.
			} catch (SQLException se) {
				throw new RemoteException("Error deleting vehicle list.", se);
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	public MembershipVO updateMembership(MembershipVO membership) throws RemoteException {
		// HibernateUtil.outputStatistics();
		LogUtil.debug(this.getClass(), "updateMembership " + membership);
		membership.setLastUpdate(new DateTime());

		try {
			LogUtil.debug(this.getClass(), "updating membership " + membership);
			hsession.update(membership);
		} catch (HibernateException ex) {
			LogUtil.warn(this.getClass(), "Error updating membership: " + ex.getMessage() + ": (merging instead) " + membership + ". ");

			// In some circumstances, such as when a renewal document is
			// removed, memberships are
			// loaded as a result of populating a transaction group for
			// validation. The membership
			// group and associated memberships are in a transient state during
			// the transaction as they have
			// been loaded, deleted,
			// and reinserted within the same transaction. The transient nature
			// of the membership
			// may result in an update failing due to a membership with the same
			// identifier being
			// present in the session (NonUniqueObjectException). A merge will
			// bind the passed object
			// to the database without considering any other persistent versions
			// of the object and will
			// circumvent the update problem.
			hsession.merge(membership);
		}

		LogUtil.debug(this.getClass(), "updateMembership 1a " + membership.getMembershipID());

		LogUtil.debug(this.getClass(), "updateMembership 1b " + membership.getMembershipID());

		LogUtil.debug(this.getClass(), "updateMembership 2 " + membership.toString());

		// Replace the product options attached to the membership.
		removeProductOptionList(membership);
		insertProductOptionList(membership);

		LogUtil.debug(this.getClass(), "updateMembership 3 " + membership.getMembershipID());

		// Replace the discounts attached to the membership.
		removeDiscountList(membership);
		insertDiscountList(membership);

		LogUtil.debug(this.getClass(), "updateMembership 4 " + membership.getMembershipID());
		// Replace the vehicles attached to the membership.
		removeVehicleList(membership);
		insertVehicleList(membership);

		LogUtil.debug(this.getClass(), "updateMembership 5 " + membership.getMembershipID());
		hsession.flush();
		LogUtil.debug(this.getClass(), "updateMembership 6 " + membership.getMembershipID());
		return membership;
	}

	public void removeMembership(MembershipVO membership) throws RemoteException {

		/** @todo membership transaction list **/
		Collection transactionList = membership.getTransactionList();
		if (transactionList != null && !transactionList.isEmpty()) {
			for (Iterator i = transactionList.iterator(); i.hasNext();) {
				removeMembershipTransaction((MembershipTransactionVO) i.next());
			}
		}

		// Delete membership product options
		removeProductOptionList(membership);

		// Delete membership discounts
		removeDiscountList(membership);

		// Delete membership vehicles
		removeVehicleList(membership);

		// Delete the membership
		hsession.delete(membership);

	}

	public MembershipVO findMembershipByClientAndType(Integer clientNumber, String memTypeCode) throws RemoteException {
		MembershipVO membership = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipVO.class).add(Expression.eq("clientNumber", clientNumber)).add(Expression.eq("membershipTypeCode", memTypeCode));
			List<MembershipVO> membershipList = new ArrayList(crit.list());
			if (membershipList != null) {
				if (membershipList.size() > 1) {
					throw new RemoteException("More than one membership exists with client number " + clientNumber + " and membership type " + memTypeCode);
				} else if (membershipList.size() == 1) {
					membership = membershipList.iterator().next();
				}
			}
			LogUtil.log(this.getClass(), "findMembershipByClientAndType " + membershipList);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membership;
	}

	public Collection findMembershipByClientNumber(Integer clientNumber) throws RemoteException {
		Collection membershipList = null;
		try {

			Criteria crit = hsession.createCriteria(MembershipVO.class).add(Expression.eq("clientNumber", clientNumber));
			membershipList = new ArrayList(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipList;
	}

	public List<MembershipVO> findMembershipByVin(String vin, String productCode) throws RemoteException {
		List<MembershipVO> membershipList = null;
		try {
			Criteria crit = hsession.createCriteria(MembershipVO.class).add(Expression.eq("vin", vin)).add(Expression.eq("productCode", productCode)).add(Expression.eq("baseStatus", MembershipVO.STATUS_ACTIVE));
			membershipList = new ArrayList<MembershipVO>(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}

		return membershipList;
	}

	public List<MembershipVO> findMembershipByVin(String vin, String productCode, String status) throws RemoteException {
		List<MembershipVO> membershipList = null;
		try {
			Criteria crit = hsession.createCriteria(MembershipVO.class).add(Expression.eq("vin", vin)).add(Expression.eq("productCode", productCode)).add(Expression.eq("baseStatus", status));
			membershipList = new ArrayList<MembershipVO>(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipList;
	}

	public List<MembershipVO> findMembershipByRego(String rego) throws RemoteException {
		List<MembershipVO> membershipList = null;
		try {

			Criteria crit = hsession.createCriteria(MembershipVO.class).add(Expression.eq("rego", rego)).add(Expression.eq("baseStatus", MembershipVO.STATUS_ACTIVE));
			membershipList = new ArrayList<MembershipVO>(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipList;
	}

	public Collection findMembershipByGroupId(Integer groupId) {
		return null;
	}

	public Collection findMembershipByMembershipNumber(Integer membershipNumber) throws RemoteException {
		Collection membershipList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipVO.class).add(Expression.eq("membershipNumber", membershipNumber));
			membershipList = new ArrayList(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipList;
	}

	/**
	 * Looks for the most recent document of the specified type in the specified date range and returns the document Id. If no document is found, return null.
	 * 
	 * @param Integer membershipId
	 * @param String documentTypeCode
	 * @param DateTime firstDate
	 * @param DateTime lastDate
	 * @return Integer
	 * @throws RemoteException
	 */
	public Integer getDocumentId(Integer membershipId, String documentTypeCode, DateTime firstDate, DateTime lastDate) throws RemoteException {
		return getDocumentId(membershipId, documentTypeCode, firstDate, lastDate, true);
	}

	public MembershipDocument getMembershipDocument(Integer documentId) throws RemoteException {

		MembershipDocument membershipDocument = null;
		try {

			membershipDocument = (MembershipDocument) hsession.get(MembershipDocument.class, documentId);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to load document.", ex);
		}

		return membershipDocument;
	}

	/**
	 * Look for a document belonging to a given membership and of a given type. If found return the document Id. If not found return null optionally include cancelled documents in the search If multiple documents are found, return the id of the most recent
	 * 
	 * @param membershipId
	 * @param documentTypeCode
	 * @param firstDate
	 * @param lastDate
	 * @param includeCancelled
	 * @return
	 * @throws RemoteException
	 */
	public Integer getDocumentId(Integer membershipId, String documentTypeCode, DateTime firstDate, DateTime lastDate, boolean includeCancelled) throws RemoteException {
		StringBuffer sql = new StringBuffer();
		sql.append("select document_id from pub.mem_document ");
		sql.append(" where membership_id = ? and document_type_code = ?");
		sql.append(" and document_date >= ? and document_date < ?");
		if (!includeCancelled) {
			sql.append(" and (document_status IS NULL or document_status <> ?)");
		}
		sql.append(" order by document_id desc");
		LogUtil.debug(this.getClass(), "getDocumentId sql=" + sql);
		Integer documentId = null;

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(sql.toString());
			statement.setInt(1, membershipId.intValue());
			statement.setString(2, documentTypeCode);
			statement.setDate(3, firstDate.toSQLDate());
			statement.setDate(4, lastDate.toSQLDate());
			if (!includeCancelled) {
				statement.setString(5, MembershipDocument.DOCUMENT_STATUS_CANCELLED);
			}
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				documentId = new Integer(rs.getInt(1));
			}
		} catch (Exception e) {
			throw new RemoteException("Error retrieving documentId " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return documentId;
	}

	/**
	 * Checks to see if there is a membership card request pending. Returns true if there is
	 * 
	 * @param Integer membershipId. The membership Id for which the card info is required
	 * @return boolean.
	 * @throws RemoteException
	 */
	public boolean isCardRequested(Integer membershipId) throws RemoteException {
		boolean cardIsRequested = false;
		StringBuffer sql = new StringBuffer();
		sql.append("select card_id from pub.mem_membership_card ");
		sql.append(" where membership_id = ?");
		sql.append(" and issue_date is null");

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(sql.toString());
			statement.setInt(1, membershipId.intValue());
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				cardIsRequested = true;
			}
		} catch (Exception e) {
			throw new RemoteException("Error retrieving mem_membership_card records " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return cardIsRequested;
	}

	/**
	 * Process an event that the membership system has been subscribed to in the NotificationMgr.
	 */
	public void processNotificationEvent(NotificationEvent event) throws RemoteException {
		// Find out what sort of event it is
		if (event.eventEquals(NotificationEvent.EVENT_CLIENT_CHANGE)) {
			Integer clientNumber = ((ClientChangeNotificationEvent) event).getClientNumber();
			// LogUtil.debug(this.getClass(),"Processing CLIENT_CHANGE_EVENT for client "
			// + clientNumber);

			// For client change notifications Find all of the active
			// memberships
			// owned by the client and
			// - request a new membership card if the clients name has changed,
			// - send the membership details to CARES
			Collection membershipList = this.findMembershipByClientNumber(clientNumber);

			NotificationMgr notificationMgr = CommonEJBHelper.getNotificationMgr();
			try {
				NotificationEvent notificationEvent;
				MembershipTransactionMgr transMgr = MembershipEJBHelper.getMembershipTransactionMgr();
				DateTime today = new DateTime();
				MembershipVO memVO = null;
				Iterator membershipIterator = membershipList.iterator();
				while (membershipIterator.hasNext()) {
					memVO = (MembershipVO) membershipIterator.next();

					// Since the membership is dependant on the client data
					// raise a membership change notification event.
					// CARES is a subscriber to this event and will
					// cause the new membership details to be sent to CARES.
					notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE_CLIENT, memVO);
					try {
						notificationMgr.notifyEvent(notificationEvent);
					} catch (RollBackException rbe) {
						throw new SystemException("Failed to publish event : " + notificationEvent);
					}

					// Limit all other membership changes to active memberships
					// if(MembershipVO.STATUS_ACTIVE.equals(memVO.getStatus())
					// || MembershipVO.STATUS_FUTURE.equals(memVO.getStatus())
					// ||
					// MembershipVO.STATUS_INRENEWAL.equals(memVO.getStatus()))
					// {
					//
					// // Create a card request if required. It will workout the
					// reason.
					// try
					// {
					// transMgr.createMembershipCardRequest(memVO, null, false);
					// }
					// catch(RemoteException re)
					// {
					// if(re instanceof com.ract.client.DeceasedClientException)
					// {
					// // Ignore as we don't want to create cards for deceased
					// clients anyway.
					// }
					// else
					// {
					// throw re;
					// }
					// }
					// }
				}
				// The commit method tells the notification manager to start
				// sending
				// published events if there are any.
				notificationMgr.commit();
			} catch (Exception e) {
				try {
					notificationMgr.rollback();
				} catch (Exception e1) {
				}
				if (e instanceof RemoteException) {
					throw (RemoteException) e;
				} else {
					throw new SystemException(e);
				}
			}
		}
	}

	public void createMembershipGroupDetail(MembershipGroupDetailVO membershipGroupDetail) throws RemoteException {
		LogUtil.debug(this.getClass(), "createMembershipGroupDetail start " + membershipGroupDetail);

		LogUtil.debug(this.getClass(), "createMembershipGroupDetail 1 evict=" + membershipGroupDetail.getMembershipGroupDetailPK());
		// This instance needs to be evicted as there is transaction state
		// attached to it. The effect of not
		// evicting any attached instances is that insert statements are
		// generated!
		// Is this due to deletes being performed via HQL and insert via the
		// entity framework?
		if (hsession.contains(membershipGroupDetail)) {
			LogUtil.debug(this.getClass(), "createMembershipGroupDetail 1a evicting membershipGroupDetail");
			hsession.evict(membershipGroupDetail);
		}

		LogUtil.debug(this.getClass(), "createMembershipGroupDetail 2");
		// Insert the new group detail
		hsession.save(membershipGroupDetail);
		LogUtil.debug(this.getClass(), "createMembershipGroupDetail 3");
		hsession.flush();
		LogUtil.debug(this.getClass(), "createMembershipGroupDetail 4");
		// update the group
		CacheHandler.removeCacheValue(membershipGroupDetail.getMembershipGroupDetailPK().getGroupID(), CACHE_MEMBERSHIP_GROUP);

		LogUtil.debug(this.getClass(), "removeCacheValue=" + membershipGroupDetail.getMembershipGroupDetailPK());

		LogUtil.debug(this.getClass(), "createMembershipGroupDetail end " + membershipGroupDetail);
	}

	public void removeMembershipGroupDetail(MembershipGroupDetailVO membershipGroupDetail) throws RemoteException {
		LogUtil.debug(this.getClass(), "removeMembershipGroupDetail start " + membershipGroupDetail);

		LogUtil.debug(this.getClass(), "removeMembershipGroupDetail 1");
		// Remove the group detail
		hsession.delete(membershipGroupDetail);
		LogUtil.debug(this.getClass(), "removeMembershipGroupDetail 2");
		hsession.flush();
		LogUtil.debug(this.getClass(), "removeMembershipGroupDetail 3");
		// update the group
		CacheHandler.removeCacheValue(membershipGroupDetail.getMembershipGroupDetailPK().getGroupID(), CACHE_MEMBERSHIP_GROUP);

		LogUtil.debug(this.getClass(), "removeMembershipGroupDetail end " + membershipGroupDetail);
	}

	public void removeMembershipGift(MembershipGift membershipGift) throws RemoteException {
		LogUtil.debug(this.getClass(), "removeMembershipGift=" + membershipGift.getMembershipGiftPK());
		hsession.delete(membershipGift);
	}

	public void removeMembershipGift(MembershipGiftPK membershipGiftPK) throws RemoteException {
		MembershipGift membershipGift = getMembershipGift(membershipGiftPK);
		LogUtil.debug(this.getClass(), "removeMembershipGift=" + membershipGift.getMembershipGiftPK());
		hsession.delete(membershipGift);
	}

	public void removeMembershipCard(MembershipCardVO membershipCard) throws RemoteException {
		LogUtil.debug(this.getClass(), "removeMembershipCard=" + membershipCard.getCardID());

		String memberCardDataStore = MembershipCardVO.DATA_STORE_DATABASE;
		try {
			memberCardDataStore = FileUtil.getProperty("master", "memberCardDataStore");
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine member card datastore, defaulting to: " + memberCardDataStore);
		}

		String memberCardForceDB = "false";
		try {
			memberCardForceDB = FileUtil.getProperty("master", "memberCardForceDB");
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine member card force DB, defaulting to: " + memberCardForceDB);
		}
		Boolean forceDB = Boolean.parseBoolean(memberCardForceDB);

		if (memberCardDataStore.equals(MembershipCardVO.DATA_STORE_WEB_SERVICE)) {

			int tierCode = (membershipCard.getTierCode() == null ? 0 : Integer.parseInt(membershipCard.getTierCode()));
			MembershipVO memVO = this.getMembership(membershipCard.getMembershipID());

			// tier changes and CMO only
			if (tierCode > 1 || memVO.getProductCode().equals(ProductVO.PRODUCT_CMO)) {

				LogUtil.log(getClass(), "Removing card via WS.");

				CardManagementService service = new CardManagementService();
				ICardManagementService portType = service.getBasicHttpBindingICardManagementService();

				RemoveCardRequestResult result = portType.removeCardRequest(membershipCard.getMembershipID().toString());

				Integer errorCode = result.getErrorCode();
				String errorMsg = result.getErrorMessage().getValue();
				if (errorCode != -1) {
					LogUtil.warn(this.getClass(), "Unable to remove membership card request for membershipId: " + membershipCard.getMembershipID() + ": " + errorMsg);
				}
			}
		}

		if (forceDB || memberCardDataStore.equals(MembershipCardVO.DATA_STORE_DATABASE)) {
			LogUtil.log(getClass(), "Removing card via DB.");
			hsession.delete(membershipCard);
		}
	}

	public final static String CACHE_MEMBERSHIP_GROUP = "membershipGroup";

	/**
	 * Performance bottleneck before caching
	 * 
	 * @param membershipID Integer
	 * @throws RemoteException
	 * @return MembershipGroupVO
	 */
	public MembershipGroupVO getMembershipGroupVO(Integer membershipID) throws RemoteException {
		MembershipGroupVO membershipGroupVO = null;
		MethodCounter.addToCounter("MembershipMgrBean", "getMembershipGroupVO");
		Collection groupDetailList = new ArrayList();
		MembershipGroupDetailVO groupDetail = null;
		Collection groupsByMembership = findMembershipGroupsByMembershipID(membershipID);
		if (groupsByMembership != null) {
			if (groupsByMembership.size() > 1) {
				throw new RemoteException("More than one group for membership '" + membershipID + "'. " + groupsByMembership.toString());
			}
			LogUtil.debug(this.getClass(), "groupsByMembership " + groupsByMembership.size());
			Iterator groupIt = groupsByMembership.iterator();
			// only one
			if (groupIt.hasNext() && membershipGroupVO == null) {
				groupDetail = (MembershipGroupDetailVO) groupIt.next();
				// get key
				Integer groupID = groupDetail.getMembershipGroupDetailPK().getGroupID();

				LogUtil.debug(this.getClass(), "groupID=" + groupID);
				if (groupID != null) {
					// can we get it from the cache
					membershipGroupVO = (MembershipGroupVO) CacheHandler.getCacheValue(groupID, CACHE_MEMBERSHIP_GROUP);
					if (membershipGroupVO == null) {
						LogUtil.debug(this.getClass(), "Retrieving group " + groupDetail);
						// add all group details for the group
						groupDetailList.addAll(findMembershipGroupsByGroupID(groupDetail.getMembershipGroupDetailPK().getGroupID()));
						// construct a new one
						membershipGroupVO = new MembershipGroupVO(groupDetailList);
						// add to cache
						/** @todo when does the cache expire */
						LogUtil.debug(this.getClass(), "Adding cache entry membershipGroupVO.getMembershipGroupID()=" + membershipGroupVO.getMembershipGroupID());
						CacheHandler.putCacheValue(groupID, membershipGroupVO, CACHE_MEMBERSHIP_GROUP);
					} else {
						LogUtil.debug(this.getClass(), "Returning cached group " + membershipGroupVO);
					}
				}
			}
		}
		return membershipGroupVO;
	}

	public Collection findMembershipGroupsByMembershipID(Integer membershipID) throws RemoteException {
		Collection membershipGroupList = null;

		try {
			LogUtil.debug(this.getClass(), ">>>>>>>>>>>>>>>>>>>>>> searching " + membershipID);

			Criteria crit = hsession.createCriteria(MembershipGroupDetailVO.class).add(Expression.eq("membershipGroupDetailPK.membershipID", membershipID)).addOrder(Order.desc("groupJoinDate"));
			membershipGroupList = new ArrayList(crit.list());
			LogUtil.debug(this.getClass(), "membershipGroupList " + membershipGroupList.size());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipGroupList;
	}

	public Collection findMembershipGroupsByGroupID(Integer groupID) throws RemoteException {
		Collection membershipGroupList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipGroupDetailVO.class).add(Expression.eq("membershipGroupDetailPK.groupID", groupID));
			membershipGroupList = new ArrayList(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipGroupList;
	}

	public MembershipTier getMembershipTier(String tierCode) throws RemoteException {

		MembershipTier membershipTier = (MembershipTier) hsession.get(MembershipTier.class, tierCode);
		return membershipTier;
	}

	public Collection getMembershipTierList() throws RemoteException {

		List membershipTierList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipTier.class);
			membershipTierList = crit.list();
		} catch (HibernateException ex) {
			throw new SystemException(ex);
		}
		return membershipTierList;

	}

	public void createMembershipQuote(MembershipQuote memQuote) throws RemoteException {

		// quote number!!!
		Integer quoteNumber = new Integer(MembershipEJBHelper.getMembershipIDMgr().getNextQuoteNumber());
		memQuote.setQuoteNumber(quoteNumber);

		// Insert the new quote
		hsession.save(memQuote);

	}

	public void createMembershipGift(MembershipGift memGift) throws RemoteException {

		hsession.save(memGift);
	}

	public void updateMembershipGift(MembershipGift memGift) throws RemoteException {

		hsession.update(memGift);
	}

	public void updateMembershipQuote(MembershipQuote memQuote) throws RemoteException {

		// Update the quote
		hsession.update(memQuote);
	}

	/**
	 * getMembershipQuoteVO Retrieves a membership quote record using the membershipQuote enterprise bean given the quote number
	 * 
	 * @param Integer quoteNumber The quote number to retrieve
	 * @return MembershipQuoteVO The required membership quote data
	 * @throws RemoteException
	 */
	public MembershipQuote getMembershipQuote(Integer quoteNumber) throws RemoteException {

		MembershipQuote membershipQuote = null;
		try {

			membershipQuote = (MembershipQuote) hsession.get(MembershipQuote.class, quoteNumber);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to load document.", ex);
		}
		return membershipQuote;

	}

	/**
	 * Return a list of MembershipQuoteVO for the given membership ID.
	 * 
	 * @param Integer membershipID The membershipID for which quotes are required
	 * @return Vector The list of membershipQuotes found. If none found returns an empty list.
	 * @throws RemoteException
	 */
	public Vector getMembershipQuoteListByMembershipID(Integer membershipId) throws RemoteException {
		Vector quoteList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipQuote.class).add(Expression.eq("membershipId", membershipId));
			quoteList = new Vector(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return quoteList;

	}

	/**
	 * 
	 * @param membershipId Integer
	 * @throws RemoteException
	 * @return Vector
	 */
	public Collection getMembershipGiftByMembershipID(Integer membershipId) throws RemoteException {
		Vector membershipGiftList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipGift.class).add(Expression.eq("membershipGiftPK.membershipId", membershipId));
			membershipGiftList = new Vector(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipGiftList;

	}

	public MembershipGift getMembershipGift(MembershipGiftPK membershipGiftPK) throws RemoteException {
		MembershipGift membershipGift = null;

		try {

			LogUtil.debug(this.getClass(), "membershipGiftPK=" + membershipGiftPK);
			membershipGift = (MembershipGift) hsession.get(MembershipGift.class, membershipGiftPK);
			LogUtil.debug(this.getClass(), "membershipGift=" + membershipGift);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipGift;

	}

	public void produceGMVoucherLetters(DateTime effectiveDate) throws RemoteException {
		LogUtil.log(this.getClass(), "produceGMVoucherLetters start");
		DateTime now = new DateTime().getDateOnly();

		String mailMessage = "";
		SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
		
		final String SUBJECT_VOUCHER_LETTER = "Voucher letter generation completed on " + now.formatShortDate();
		String directoryName = null;
		try {
			directoryName = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DIRECTORY_MEMBERSHIP_EXTRACTS); // includes
			// /
		} catch (RemoteException ex5) {
			LogUtil.warn(this.getClass(), "Unable to find the directory name." + ex5);
		}
		final String DATE_STAMP = sd.format(now);
		final String FILE_PREFIX = directoryName + "RS_GM-TierChange-";
		final String FILE_SUFFIX = "y";
		final String FILE_EXTENSION = "." + FileUtil.EXTENSION_TXT;
		final String FILE_SUMMARY = directoryName + "GM-TierChange-Summary" + DATE_STAMP + ".txt";

		// effective date in form mm/yyyy
		DateTime endDate = null;
		try {
			endDate = effectiveDate.add(new Interval(0, 1, 0, 0, 0, 0, 0));
		} catch (Exception ex) {
			LogUtil.warn(this.getClass(), "Unable to get end date " + ex);
		}

		LogUtil.log(this.getClass(), "effectiveDate=" + effectiveDate);
		LogUtil.log(this.getClass(), "endDate=" + endDate);
		LogUtil.log(this.getClass(), "membershipStatus=" + MembershipVO.STATUS_ACTIVE);
		LogUtil.log(this.getClass(), "allowToLapse=" + Boolean.FALSE.booleanValue());
		LogUtil.log(this.getClass(), "renewalTierCode=" + MembershipTier.TIER_CODE_LOYALTY);

		final String SQL_VOUCHER_LOYALTY = "select membership_id from vw_membership_loyalty where expiry_date >= ? and expiry_date < ? " 
										+ "and membership_status = ? and allow_to_lapse = ? " 
										+ "and product_code in ('" + ProductVO.PRODUCT_ADVANTAGE + "','" + ProductVO.PRODUCT_ULTIMATE 	
										+ "','" + ProductVO.PRODUCT_LIFESTYLE + "')" 
										/*	+ "and current_tier_code != renewal_tier_code " 
										+ "and current_tier_code <> ?";   */  /** rlg 20/4/21 **/
										+  "and renewal_tier_code > ?"; 

		LogUtil.log(this.getClass(), "SQL_VOUCHER_LOYALTY=" + SQL_VOUCHER_LOYALTY);

		Connection connection = null;
		PreparedStatement statement = null;
		Hashtable files = new Hashtable();
		Hashtable summary = new Hashtable();
		StringBuffer summaryDesc = new StringBuffer();

		FileWriter fw = null;

		try {
			connection = clientDataSource.getConnection();

			int membershipId = 0;
			MembershipVO membership = null;

			int counter = 0;
			int memyears = 0;

			summaryDesc.append("GM voucher letters generated " + now.formatShortDate());
			summaryDesc.append(FileUtil.NEW_LINE);
			summaryDesc.append("Effective date " + effectiveDate.formatShortDate());
			summaryDesc.append(FileUtil.NEW_LINE);
			summaryDesc.append(FileUtil.NEW_LINE);

			MembershipTier renewalTier = null;
			statement = connection.prepareStatement(SQL_VOUCHER_LOYALTY);
			statement.setDate(1, effectiveDate.toSQLDate());
			statement.setDate(2, endDate.toSQLDate());
			statement.setString(3, MembershipVO.STATUS_ACTIVE);
			statement.setBoolean(4, Boolean.FALSE.booleanValue());
			/*	statement.setString(5, String.valueOf(MembershipTier.TIER_CODE_DEFAULT));  */ /* 2 - rlg 20/04/21 */
			statement.setString(5, String.valueOf(MembershipTier.TIER_CODE_LOYALTY)); 
			
			ResultSet rs = statement.executeQuery();

			// for each voucher letter
			while (rs.next()) {

				membershipId = rs.getInt(1);

				try {

					membership = getMembership(new Integer(membershipId));

					// get the renewal tier
					renewalTier = membership.getRenewalMembershipTier();
					memyears = new Interval(membership.getJoinDate(),membership.getExpiryDate()).getTotalYears();
										
					String docType = MembershipDocument.VOUCHER_LETTER_TIER_CHANGE + renewalTier.getTierName();

					LogUtil.debug(this.getClass(), "docType=" + docType);
					// check if document already generated
					List docList = hsession.createCriteria(MembershipDocument.class).add(Expression.eq("membershipId", membership.getMembershipID())).add(Expression.eq("documentTypeCode", docType)).list();

					if (docList != null && docList.size() >= 1) {
						String message = "A document of type " + docType + " already exists for membership " + membership.getMembershipNumber();
						mailMessage += message + FileUtil.NEW_LINE;
						throw new Exception(message);
					}

					// get the file writer based on tier name
					String renewalTierFileKey = FILE_PREFIX + renewalTier.getTierName() + FILE_SUFFIX +"_" + new Integer(memyears).toString() + "_" + DATE_STAMP + FILE_EXTENSION;
					fw = (FileWriter) files.get(renewalTierFileKey);
					if (fw == null) {
						fw = new FileWriter(renewalTierFileKey, true);
					}

					Integer keyCount = (Integer) summary.get(renewalTierFileKey);
					if (keyCount == null) {
						summary.put(renewalTierFileKey, new Integer(1));
					} else {
						// add 1 to count
						keyCount = new Integer(keyCount.intValue() + 1);
						summary.put(renewalTierFileKey, keyCount);
					}

					LogUtil.debug(this.getClass(), "renewalTierFileKey=" + renewalTierFileKey);

					LogUtil.debug(this.getClass(), "no unissued cards");

					ClientVO client = membership.getClient();
					PostalAddressVO address = client.getPostalAddress();

					// format:
					// membership_number, client_title, given_names, surname,
					// property, street_number, street, suburb, state, postcode
					// ,client_branch, expiry_date, renewal_tier_code,
					// pending_tier_change, renewal_tier_description,
					// renewal_tier_name
					StringBuffer line = new StringBuffer();
					line.append(membership.getMembershipNumber());
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(client.getTitle().trim());
					line.append(FileUtil.SEPARATOR_COMMA);
					// first name only (MEM-222)
					line.append(client.getFirstNameOnly());
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(client.getSurname().trim());
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(address.getProperty() != null ? address.getProperty() : "");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(address.getPropertyQualifier() != null ? address.getPropertyQualifier() : "");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(address.getStreet() != null ? address.getStreet() : "");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(address.getSuburb() != null ? address.getSuburb() : "");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(address.getState() != null ? address.getState() : "");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(address.getPostcode() != null ? address.getPostcode() : "");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(client.getBranchNumber() != null ? client.getBranchNumber().toString() : "");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(membership.getExpiryDate());
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(renewalTier.getTierCode());
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append("Y");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(renewalTier.getTierDescription() + " years");
					line.append(FileUtil.SEPARATOR_COMMA);
					line.append(renewalTier.getTierName() + " years");
					line.append(FileUtil.NEW_LINE);

					LogUtil.log(this.getClass(), "line=" + line);

					fw.write(line.toString());
					fw.flush();

					// create a document based on tier
					MembershipDocument memDoc = new MembershipDocument();
					// generate a document id
					MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
					Integer documentId = new Integer(idMgr.getNextDocumentID());
					// basic details required
					memDoc.setDocumentId(documentId);
					memDoc.setDocumentTypeCode(docType);
					memDoc.setDocumentDate(now);
					memDoc.setMembershipDate(endDate);
					memDoc.setFeeEffectiveDate(effectiveDate);
					memDoc.setMembershipId(membership.getMembershipID());
					hsession.save(memDoc);
					LogUtil.debug(this.getClass(), "after doc");

				} 
				catch (Exception ex2) 
				{
					if ( ex2.getMessage().startsWith("A document of type" ))
					{
						LogUtil.log(this.getClass(), ex2.getMessage());				/** rlg 20/04/21 **/
					}
					else
					{
						LogUtil.warn(this.getClass(), ex2);
					}
				}
			}
		} catch (SQLException se) {
			throw new RemoteException("Unable to retrieve eligible loyalty members.", se);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		summaryDesc.append("File counts:");
		summaryDesc.append(FileUtil.NEW_LINE);
		summaryDesc.append(FileUtil.NEW_LINE);

		// append to summary in ascending order
		SortedSet keys = new TreeSet(summary.keySet());
		for (Iterator keysIt = keys.iterator(); keysIt.hasNext();) {
			Object key = keysIt.next();
			Object value = summary.get(key);
			summaryDesc.append(key.toString() + ": " + value.toString() + FileUtil.NEW_LINE);
		}

		LogUtil.debug(this.getClass(), "Closing files");

		try {
			// close extract files
			Set fileKeys = files.keySet();
			Iterator fileIt = fileKeys.iterator();
			while (fileIt.hasNext()) {
				fw = (FileWriter) fileIt.next();
				fw.close();
			}
			// write summary file
			FileUtil.writeFile(FILE_SUMMARY, summaryDesc.toString());

			// write summary to email
			mailMessage += FileUtil.NEW_LINE;
			mailMessage += summaryDesc.toString();
		} catch (IOException ex3) {
			LogUtil.warn(this.getClass(), ex3);
		}

		LogUtil.debug(this.getClass(), "Sending email");

		String emailAddress = null;
		try {
			emailAddress = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.EMAIL_SYS_ADMIN);
			MailMgr mailMgr = CommonEJBHelper.getMailMgr();

			MailMessage message = new MailMessage();
			message.setRecipient(emailAddress);
			message.setSubject(SUBJECT_VOUCHER_LETTER);
			message.setMessage(mailMessage);

			mailMgr.sendMail(message);
		} catch (RemoteException ex4) {
			LogUtil.warn(this.getClass(), "Unable to send email to " + emailAddress + " " + ex4);
		}

		LogUtil.log(this.getClass(), "produceGMVoucherLetters end");

	}

	public static final String SQL_AGENT_LOCATIONS = "select distinct location from PUB.rs_agent order by location asc";

	public Collection getAgentLocationList() throws RemoteException {
		return CommonEJBHelper.getCommonMgr().getStringResultArray(SQL_AGENT_LOCATIONS);
	}

	public Collection getAgentList(String agentId, String agentName, String location, Boolean roadside, Boolean towing, Boolean battery) {
		LogUtil.debug(this.getClass(), "getAgentList start");
		LogUtil.debug(this.getClass(), "agentId=[" + agentId + "]");
		LogUtil.debug(this.getClass(), "agentName=[" + agentName + "]");
		LogUtil.debug(this.getClass(), "location=[" + location + "]");
		LogUtil.debug(this.getClass(), "roadside=[" + roadside + "]");
		LogUtil.debug(this.getClass(), "towing=[" + towing + "]");
		LogUtil.debug(this.getClass(), "battery=[" + battery + "]");

		Criteria crit = hsession.createCriteria(Agent.class);

		if (agentId != null && !agentId.trim().equals("")) {
			crit.add(Expression.like("agentPK.agentId", agentId + CommonConstants.WILDCARD));
		}
		if (agentName != null && !agentName.trim().equals("")) {
			crit.add(Expression.like("agentName", agentName + CommonConstants.WILDCARD));
		}
		if (location != null && !location.trim().equals("")) {
			crit.add(Expression.like("agentPK.location", location + CommonConstants.WILDCARD));
		}
		if (roadside != null && roadside.booleanValue()) {
			crit.add(Expression.eq("roadside", roadside));
		}
		if (towing != null && towing.booleanValue()) {
			crit.add(Expression.eq("towing", towing));
		}
		if (battery != null && battery.booleanValue()) {
			crit.add(Expression.eq("battery", battery));
		}

		crit.addOrder(Order.asc("agentPK.location"));
		crit.addOrder(Order.asc("priority"));

		// LogUtil.log(this.getClass(),"crit="+crit.toString());
		return crit.list();
	}

	public int uploadAgentsFile(File file) throws RemoteException {

		// delete all records first

		Agent agent = null;

		List agentList = (hsession.createCriteria(Agent.class)).list();
		if (agentList != null && agentList.size() > 0) {
			Iterator agentIt = agentList.iterator();
			while (agentIt.hasNext()) {
				agent = (Agent) agentIt.next();
				hsession.delete(agent);
			}
		}

		// reinitialise
		agent = null;

		int recordCount = 0;
		final int COLUMN_COUNT = 17;
		try {

			String agentLine[] = null;

			CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(file)));
			int counter = 0;
			while ((agentLine = reader.readNext()) != null) {
				counter++;
				if (counter > 1) // skip the header
				{
					recordCount++;
					// LogUtil.debug(this.getClass(),recordCount+": "+line);
					if (agentLine.length != COLUMN_COUNT) {
						throw new RemoteException("File does not contain the correct number of columns - " + COLUMN_COUNT + ". Column count was " + recordCount + " - " + agentLine.length + "\n " + agentLine);
					}
					agent = new Agent();
					AgentPK agentPK = new AgentPK();
					// set the data
					agentPK.setAgentId(agentLine[0]);
					agent.setPriority(Integer.parseInt(agentLine[1]));
					agent.setAgentName(agentLine[2]);
					agent.setContact1Name(agentLine[3]);
					agent.setContact1Phone(agentLine[4]);
					agent.setContact2Name(agentLine[5]);
					agent.setContact2Phone(agentLine[6]);
					agentPK.setLocation(agentLine[7]);
					agent.setBusinessPhone(agentLine[8]);
					agent.setMobilePhone(agentLine[9]);
					agent.setHomePhone(agentLine[10]);
					agent.setPreferredContactMethod(agentLine[11]);
					agent.setRoadside(agentLine[12].equalsIgnoreCase("Y"));
					agent.setTowing(agentLine[13].equalsIgnoreCase("Y"));
					agent.setTowingType(agentLine[14]);
					agent.setNotes(agentLine[15]);
					agent.setBattery(agentLine[16].equals("Y"));
					agent.setAgentPK(agentPK);
					LogUtil.log(this.getClass(), "rec=" + recordCount + " " + agent);
					hsession.save(agent);
					hsession.flush();
				} else {
					if (recordCount != 0) {
						throw new RemoteException("File is not in the expected format.");
					}
				}

			}
		} catch (IOException ex) {
			throw new RemoteException("Unable to process agent file.", ex);
		}

		return recordCount;
	}

	/**
	 * Upload an ECR receipt for attachment to the general advice (optional) - APPS-298 GS 4/9/20
	 * @param file
	 * @return int
	 * @throws IOException 
	 */
	public boolean uploadECRRecipt(File file, String actualFileName) throws IOException {

		String newAbsolutePath = file.getAbsolutePath();
		String path = file.getPath();
		String cpath = file.getCanonicalPath();
		String name = file.getName();
		InputStream inputStream = new FileInputStream(file);
        OutputStream outputStream = new FileOutputStream("/ract/app/ecr_receipts" + actualFileName);

        try {

            
            int byteRead;
            
            while ((byteRead = inputStream.read()) != -1) {
                outputStream.write(byteRead);
            }
		} finally {
			inputStream.close();
			outputStream.flush();
			outputStream.close();
		}
			
		return true;
	}

	/**
   *
   */
	public Vector getMembershipQuoteList() throws RemoteException {
		Vector quoteList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipQuote.class);
			quoteList = new Vector(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return quoteList;

	}

	public void updateMembershipQuotesWithUserDetails() throws RemoteException {
		MembershipQuote memQuote = null;
		TransactionGroup transGroup = null;
		Collection quoteList = this.getMembershipQuoteList();
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		if (quoteList != null) {
			Iterator quoteListIt = quoteList.iterator();
			while (quoteListIt.hasNext()) {
				memQuote = (MembershipQuote) quoteListIt.next();
				if (memQuote.getUserId() == null) {
					transGroup = memQuote.getTransactionGroup();
					if (transGroup != null) {
						// update with user id and sales branch code and update
						// quote
						memQuote.setUserId(transGroup.getUserID().toLowerCase());
						memQuote.setSalesBranchCode(transGroup.getSalesBranchCode().toLowerCase());
						membershipMgr.updateMembershipQuote(memQuote);
					}
				}
			}
		}
	}

	/**
	 * Return a list of MembershipQuoteVO containing all roadside quotes for the given client whether the quotes are attached to a transaction or not. If there are not quotes for the specified client an empty list is returned.
	 * 
	 * @param Integer clientNumber The client number for which quotes are to be found
	 * @return Vector The list of MembershipQuoteVO objects retrieved from the database
	 * @throws RemoteException
	 */
	public Vector getMembershipQuoteListByClientNumber(Integer clientNumber) throws RemoteException {
		return getMembershipQuoteListByClientNumber(clientNumber, true);
	}

	/**
	 * Return a list of MembershipQuoteVO for the given client. If the includeAttached flag is true then all quotes are returned for the client whether they are attached to a transaction or not. If the includeAttached is false then only those quotes that are NOT attached to a transaction are returned. If there are no quotes for the given client, an empty list is returned.
	 * 
	 * @param Integer clientNumber The client number for which the quotes are to be found
	 * @param boolean includeAttached If set to true includes all quotes for this client
	 * @return Vector The list of MembershipQuoteVO objects retrieved from the database.
	 * @throws RemoteException
	 */
	public Vector getMembershipQuoteListByClientNumber(Integer clientNumber, boolean includeAttached) throws RemoteException {
		MembershipQuote quote = null;

		Vector quoteList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipQuote.class).add(Expression.eq("clientNumber", clientNumber));
			quoteList = new Vector(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}

		if (!includeAttached) {
			for (Iterator i = quoteList.iterator(); i.hasNext();) {
				quote = (MembershipQuote) i.next();
				// exclude any attached quotes from the list
				if (quote.isAttached()) {
					i.remove();
				}
			}
		}

		return quoteList;

	}

	public Collection getCurrentQuoteList(Integer clientNumber) throws RemoteException {
		DateTime quoteEffectiveDate = MembershipHelper.getQuoteEffectiveDate(null);

		String queryString = "from MembershipQuote where clientNumber = :clientNumber and quoteDate >= :quoteEffectiveDate";
		List result = hsession.createQuery(queryString).setInteger("clientNumber", clientNumber.intValue()).setDate("quoteEffectiveDate", quoteEffectiveDate).list();
		return result;
	}

	public MembershipTier getMembershipTierByJoinDate(DateTime joinDate) throws RemoteException {
		return this.getMembershipTierByMembershipYears(this.getMembershipYears(new DateTime().getDateOnly(), joinDate));
	}

	public MembershipTier getMembershipTierByMembershipYears(int membershipYears) throws RemoteException {
		LogUtil.debug(this.getClass(), "membershipYears=" + membershipYears);
		MembershipTier membershipTier = null;
		ArrayList membershipTierList = (ArrayList) this.getMembershipTierList();
		Collections.sort(membershipTierList); // in natural ascending sort order
		boolean tierFound = false;
		int recordNumber = 0;
		for (int i = 0; i < membershipTierList.size() && !tierFound; i++) {
			membershipTier = (MembershipTier) membershipTierList.get(i);
			LogUtil.debug(this.getClass(), "membershipTier = " + membershipTier + " " + i);
			recordNumber = i + 1;
			// if mem years greater than current tier years
			if (membershipYears < membershipTier.getMembershipYears().intValue() || recordNumber == membershipTierList.size() // last
			// record
			) {
				LogUtil.debug(this.getClass(), "Found membership years = " + membershipTier.getMembershipYears().intValue());

				tierFound = true;

			}
			LogUtil.debug(this.getClass(), membershipTier.toString() + ", tier found = " + tierFound);
		}

		return membershipTier;
	}

	/**
	 * Get a list of discounts for a given tier code.
	 * 
	 * @param tierCode String
	 * @return Collection
	 */
	public Collection getDiscountListByTier(String tierCode) throws RemoteException {
		final String SQL_DISCOUNTS_BY_TIER = "select discount_type_code from PUB.mem_tier_discount where tier_code = ?";
		ArrayList discountList = new ArrayList();
		Connection connection = null;
		PreparedStatement statement = null;
		String discountTypeCode = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(SQL_DISCOUNTS_BY_TIER);

			statement.setString(1, tierCode);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				discountTypeCode = rs.getString(1);
				MembershipDiscount disc = new MembershipDiscount(discountTypeCode);
				discountList.add(disc);
			}
		} catch (SQLException e) {
			throw new RemoteException("Unable to get discount list. " + SQL_DISCOUNTS_BY_TIER, e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return discountList;
	}

	/**
	 * 
	 * @param groupID Integer
	 * @return GroupVO
	 */
	public GroupVO getGroupByGroupId(Integer groupId) throws RemoteException {
		LogUtil.log(this.getClass(), "getGroupByGroupId " + groupId);

		GroupVO group = (GroupVO) hsession.get(GroupVO.class, groupId);
		return group;
	}

	public void createGroup(GroupVO group) throws RemoteException {
		LogUtil.log(this.getClass(), "createGroup " + group);

		hsession.save(group);
	}

	public void updateGroup(GroupVO group) throws RemoteException {
		LogUtil.log(this.getClass(), "createGroup " + group);

		hsession.update(group);
	}

	public void removeGroup(GroupVO group) throws RemoteException {
		LogUtil.log(this.getClass(), "removeGroup " + group);

		hsession.delete(group);
	}

	public void deleteMembershipFromGroups(Integer membershipId) {
		LogUtil.debug(this.getClass(), "deleteMembershipFromGroups start " + membershipId);
		MembershipGroupDetailVO membershipGroupDetail = null;
		Collection<MembershipGroupDetailVO> groupDetails = null;
		LogUtil.debug(this.getClass(), "deleteMembershipFromGroups 1");
		try {
			groupDetails = findMembershipGroupsByMembershipID(membershipId);
		} catch (RemoteException e) {
			LogUtil.warn(this.getClass(), e);
		}
		LogUtil.debug(this.getClass(), "deleteMembershipFromGroups 2");
		org.hibernate.Query query = hsession.createQuery("delete from MembershipGroupDetailVO m where m.membershipGroupDetailPK.membershipID = :membershipId");
		query.setInteger("membershipId", membershipId);
		// typically 1
		LogUtil.debug(this.getClass(), "deleteMembershipFromGroups 3");
		int rowCount = query.executeUpdate();
		LogUtil.log(this.getClass(), "Deleted " + rowCount + " MembershipGroupVO records for membership " + membershipId);
		hsession.flush();
		LogUtil.debug(this.getClass(), "deleteMembershipFromGroups 4");
		for (Iterator<MembershipGroupDetailVO> i = groupDetails.iterator(); i.hasNext();) {
			membershipGroupDetail = i.next();
			CacheHandler.removeCacheValue(membershipGroupDetail.getMembershipGroupDetailPK().getGroupID(), CACHE_MEMBERSHIP_GROUP);
		}
		LogUtil.debug(this.getClass(), "deleteMembershipFromGroups end " + membershipId);
	}

	public void deleteMembershipGroup(Integer groupID) {
		LogUtil.debug(this.getClass(), "deleteMembershipGroup start " + groupID);
		LogUtil.debug(this.getClass(), "deleteMembershipGroup 1");
		org.hibernate.Query query = hsession.createQuery("delete from MembershipGroupDetailVO m where m.membershipGroupDetailPK.groupID = :groupId");
		query.setInteger("groupId", groupID);
		LogUtil.debug(this.getClass(), "deleteMembershipGroup 2");
		// typically > 1
		int rowCount = query.executeUpdate();
		LogUtil.log(this.getClass(), "Deleted " + rowCount + " MembershipGroupVO records for groupID " + groupID);
		hsession.flush();
		LogUtil.debug(this.getClass(), "deleteMembershipGroup 3");
		CacheHandler.removeCacheValue(groupID, CACHE_MEMBERSHIP_GROUP);

		LogUtil.debug(this.getClass(), "deleteMembershipGroup end " + groupID);
	}

	/**
	 * <p>
	 * "Back out" a renewal.
	 * </p>
	 * <p>
	 * This includes:
	 * </p>
	 * <ul>
	 * <li>Remove the document</li>
	 * <li>Remove the pending fee and payable fee; or</li>
	 * <li>Remove the renewal version of the direct debit schedule</li>
	 * </ul>
	 * 
	 * @param documentId Integer
	 * @throws RemoteException
	 * @return boolean True if a renewal document was found and removed
	 */
	public boolean removeRenewalDocument(Integer documentId, String mode, PaymentTransactionMgrLocal paymentTransactionMgrLocal, boolean ignoreErrors) throws RemoteException, RenewalNoticeException {
		LogUtil.log(this.getClass(), "removeRenewalDocument start");
		LogUtil.log(this.getClass(), "removeRenewalDocument mode=" + mode);
		LogUtil.log(this.getClass(), "removeRenewalDocument documentId=" + documentId);
		boolean documentRemoved = false;
		// get document
		MembershipDocument membershipDocument = getMembershipDocument(documentId);

		DateTime documentDate = membershipDocument.getDocumentDate();

		if (MembershipMgr.MODE_CANCEL.equals(mode) && MembershipDocument.DOCUMENT_STATUS_CANCELLED.equals(membershipDocument.getDocumentStatus())) {
			throw new RenewalNoticeException("Renewal document is already cancelled.");
		}

		String docType = membershipDocument.getDocumentTypeCode();

		LogUtil.log(this.getClass(), "removeRenewalDocument docType=" + docType);

		PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();

		MembershipGroupVO membershipGroup = null;

		try {
			if (MembershipDocument.RENEWAL_ADVICE.equals(docType)) {
				String transactionXML = membershipDocument.getTransactionXML();

				// xml exists so new version
				if (transactionXML != null) {
					try {
						Integer directDebitScheduleId = null;
						Document document = null;
						DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
						factory.setValidating(false);
						DocumentBuilder builder = null;
						try {
							builder = factory.newDocumentBuilder();
							document = builder.parse(new InputSource(new StringReader(transactionXML)));
						} catch (ParserConfigurationException ex3) {
							throw new RemoteException("Unable to get document builder.", ex3);
						} catch (IOException ex4) {
							throw new RemoteException("Unable to get input source.", ex4);
						} catch (SAXException ex4) {
							throw new RemoteException("Unable to parse transaction XML.", ex4);
						}

						// should only be one TransactionGroup node in the
						// document.
						Node node = document.getFirstChild();
						// reconstitute the transaction group
						TransactionGroup transGroup = new TransactionGroup(node);

						// get group
						membershipGroup = transGroup.getSelectedMembershipGroup();

						directDebitScheduleId = transGroup.getDirectDebitScheduleId();
						// may be a free membership that does not have a
						// schedule attached.
						if (directDebitScheduleId != null) {
							DirectDebitSchedule ddSchedule = null;
							LogUtil.debug(this.getClass(), "removeRenewalDocument directDebitScheduleId=" + directDebitScheduleId);
							try {
								ddSchedule = paymentMgrLocal.getDirectDebitSchedule(directDebitScheduleId);
							} catch (PaymentException ex5) {
								final String MESSAGE_DD_EX = "The direct debit schedule could not be found." + ExceptionHelper.getExceptionStackTrace(ex5);
								if (!ignoreErrors) {
									LogUtil.fatal(getClass(), MESSAGE_DD_EX);
									throw new RenewalNoticeException(MESSAGE_DD_EX);
								} else {
									LogUtil.warn(this.getClass(), MESSAGE_DD_EX);
								}
							}

							if (ddSchedule == null) {
								final String MESSAGE_DD_NULL = "Direct debit schedule may not be null.";
								if (!ignoreErrors) {
									LogUtil.fatal(getClass(), MESSAGE_DD_NULL);
									throw new RenewalNoticeException(MESSAGE_DD_NULL);
								} else {
									LogUtil.warn(this.getClass(), MESSAGE_DD_NULL);
								}
							} else {
								if (ddSchedule.isRenewalVersion()) {
									paymentTransactionMgrLocal.removeDirectDebitSchedule(directDebitScheduleId);
								} else if (ddSchedule.isCancelled()) {
									// ignore cancelled as most likely an undone
									// schedule
								} else {
									final String MESSAGE_DD_ACTIVATED = "Unable to remove the schedule as it has been activated.";
									if (!ignoreErrors) {
										LogUtil.fatal(getClass(), MESSAGE_DD_ACTIVATED);
										throw new RenewalNoticeException(MESSAGE_DD_ACTIVATED);
									} else {
										LogUtil.warn(this.getClass(), MESSAGE_DD_ACTIVATED);
									}
								}
							}
						}
					} catch (RollBackException ex2) {
						// paymentTransactionMgr.rollback();
						throw new RemoteException("Error removing direct debit schedule.", ex2);
					}
				}
			} else if (MembershipDocument.RENEWAL_NOTICE.equals(docType)) {
				PendingFee pendingFee = null;
				try {
					// get pending fee
					LogUtil.debug(this.getClass(), "documentId=" + documentId);
					pendingFee = paymentMgrLocal.getPendingFee(documentId, SourceSystem.MEMBERSHIP);
					LogUtil.debug(this.getClass(), "pendingFee=" + pendingFee);
				} catch (PaymentException ex) {
					LogUtil.warn(this.getClass(), "Unable to get pending fee for document " + documentId);
					LogUtil.warn(this.getClass(), ex);
					// throw new RemoteException("Unable to find pending fee.");
				}
				// if there is a pending fee
				if (pendingFee != null) {

					if (pendingFee.isPaid()) {
						final String MESSAGE_PENDING_PAID = "Unable to remove pending fee attached to document '" + documentId + "' as it is paid.";
						if (!ignoreErrors) {
							LogUtil.fatal(getClass(), MESSAGE_PENDING_PAID);
							throw new RenewalNoticeException(MESSAGE_PENDING_PAID);
						} else {
							LogUtil.warn(this.getClass(), MESSAGE_PENDING_PAID);
						}
					}

					LogUtil.debug(this.getClass(), "pendingFee found");
					LogUtil.debug(this.getClass(), "pendingFee=" + pendingFee);
					// remove pending fee (and payable fee)
					try {
						paymentTransactionMgrLocal.removePendingFee(pendingFee);
					} catch (RollBackException ex1) {
						final String MESSAGE_PENDING_EX = "Unable to remove pending fee." + ExceptionHelper.getExceptionStackTrace(ex1);

						if (!ignoreErrors) {
							LogUtil.fatal(getClass(), MESSAGE_PENDING_EX);
							throw new RemoteException(MESSAGE_PENDING_EX);
						} else {
							LogUtil.warn(this.getClass(), MESSAGE_PENDING_EX);
						}
					}
				}
			} else {
				// else only remove the document as it is a reminder, final
				// notice, unfinancial advice,
				// SMS reminder (document only) etc.
			}

			/*
			 * //remove any card requests associated with the document MembershipVO memVO = null; //no membership group from transaction group associated with Renewal advice if (membershipGroup == null) { //get membership memVO = getMembership(membershipDocument.getMembershipId()); //group member if (memVO.isGroupMember()) { LogUtil.debug(this.getClass(), "removeRenewalDocument membershipGroup "+memVO); //get group as it not a renewal advice so we haven't already got the group membershipGroup = memVO.getMembershipGroup(); } else { //non group LogUtil.debug(this.getClass(),"Non group "+memVO); } }
			 * 
			 * //non groups if(membershipGroup == null && memVO != null) { LogUtil .debug(this.getClass(),"Remove for individual only "+memVO); //remove for individual only removeCardByIssueDate(memVO.getMembershipID(), documentDate); } else { LogUtil.debug(this.getClass(),"Remove for group members"); MembershipGroupDetailVO memGroupDetail = null; Iterator memGroupIt = membershipGroup.getMembershipGroupDetailList().iterator(); while(memGroupIt.hasNext()) { memGroupDetail = (MembershipGroupDetailVO)memGroupIt.next(); if(memGroupDetail != null) { //remove for each member of the identified group removeCardByIssueDate (memGroupDetail.getMembership().getMembershipID(), documentDate); } } }
			 */
			// update document to cancelled or delete depending on the mode

			try {

				if (MembershipMgr.MODE_CANCEL.equals(mode)) {
					membershipDocument.setDocumentStatus(MembershipDocument.DOCUMENT_STATUS_CANCELLED);
					membershipDocument.setCancelDate(new DateTime());
					hsession.update(membershipDocument);
				} else if (MembershipMgr.MODE_DELETE.equals(mode)) {
					hsession.delete(membershipDocument);
				} else {
					throw new SystemException("Mode '" + mode + "' is not supported.");
				}
				documentRemoved = true;
			} catch (HibernateException ex7) {
				throw new SystemException(ex7);
			}

		} catch (Exception ex6) {
			// rollback pending payment transactions
			// paymentTransactionMgr.rollback();
			if (ex6 instanceof RenewalNoticeException) {
				// rethrow
				throw new RenewalNoticeException("", ex6);
			} else {
				// rethrow remote exceptions, etc.
				ex6.printStackTrace();
				throw new RemoteException("Error with '" + mode + "' document.", ex6);
			}
		}
		LogUtil.log(this.getClass(), "removeRenewalDocument end");
		return documentRemoved;
	}

	public void updateRenewalDocument(MembershipDocument document) throws RemoteException {
		hsession.merge(document);
	}

	/**
	 * Remove cards issues on the document date for a given membership.
	 * 
	 * @param membershipId Integer
	 * @param documentDate DateTime
	 * @throws RemoteException
	 */
	private void removeCardByIssueDate(Integer membershipId, DateTime documentDate) throws RemoteException {
		try {
			Collection membershipCardList = findMembershipCardsByIssueDate(membershipId, documentDate);
			if (membershipCardList != null && !membershipCardList.isEmpty()) {
				MembershipCardVO memCard = null;
				Iterator memCardIt = membershipCardList.iterator();
				while (memCardIt.hasNext()) {
					memCard = (MembershipCardVO) memCardIt.next();
					// remove the card record
					removeMembershipCard(memCard);
				}
			}
			// end of card list
		}

		catch (Exception ex8) {
			// throw finderexceptions, etc. as remoteexceptions
			throw new RemoteException("Unable to remove cards.", ex8);
		}
	}

	/**
	 * Update the membership with the new direct debit authority.
	 * 
	 * @param newAuthority DirectDebitAuthority
	 * @param membershipVO Integer
	 * @throws RemoteException
	 * @return MembershipVO the updated membership value object
	 */
	public MembershipVO updateMembershipDirectDebitAuthority(DirectDebitAuthority newAuthority, MembershipVO membershipVO, String userID) throws RemoteException {
		PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
		Integer newAuthorityID = null;
		LogUtil.debug(this.getClass(), "newAuthority=" + newAuthority);

		try {
			// update the authority
			if (newAuthority != null) {
				newAuthorityID = paymentTxMgr.updateDirectDebitAuthority(newAuthority, userID);
			}

			membershipVO.setDirectDebitAuthorityID(newAuthorityID);

			// update membership in the database
			updateMembership(membershipVO);

			paymentTxMgr.commit();

		} catch (Exception ex) {
			if (paymentTxMgr != null) {
				paymentTxMgr.rollback();
			}
			throw new SystemException("Unable to update the membership '" + membershipVO.getMembershipNumber() + "' with the direct debit authority id '" + newAuthorityID + "'.", ex);
		}
		return membershipVO;
	}

	public MembershipDocument getRenewalDocument(Integer memID, DateTime expiryDate) throws RemoteException {
		MembershipDocument docVO = null;
		final String sql = " select * from pub.mem_document" + " where (document_type_code = 'Renewal Advice'" + "      or document_type_code = 'Renewal Notice')" + " and mem_date = ? " + " and membership_id = ?" + " and (document_status is null or not document_status = 'Cancelled')";
		Connection connection = null;
		PreparedStatement statement = null;
		java.sql.Date tempDate = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setDate(1, expiryDate.toSQLDate());
			statement.setInt(2, memID.intValue());
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				docVO = new MembershipDocument();
				tempDate = rs.getDate("document_date");
				if (tempDate != null) {
					docVO.setDocumentDate(new DateTime(tempDate));
				}
				docVO.setDocumentStatus(rs.getString("document_status"));
				docVO.documentId = new Integer(rs.getInt("document_id"));
				docVO.documentTypeCode = rs.getString("document_type_code");
				tempDate = rs.getDate("mem_date");
				if (tempDate != null) {
					docVO.membershipDate = new DateTime(tempDate);
				}
				docVO.membershipId = memID;
				docVO.runNumber = new Integer(rs.getInt("run_number"));
				tempDate = rs.getDate("fee_effective_date");
				if (tempDate != null) {
					docVO.feeEffectiveDate = new DateTime(tempDate);
				}
			} else {
				throw new Exception("No current renewal document found for membershipId = " + memID + ", expiry_date = " + expiryDate);
			}

		} catch (Exception e) {
			throw new RemoteException("Unable to retrieve renewal document " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return docVO;
	}

	/**
	 * Remove the credit amount from membership records that have been cancelled for more than a specified period of time (ie. 3 months)
	 */
	public Hashtable removeExpiredMembershipCredits() throws RemoteException {
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		Account suspenseAccount = refMgr.getMembershipSuspenseAccount();
		// String suspenseAccountNumber = suspenseAccount.getAccountNumber();
		Hashtable removedCredit = new Hashtable();
		/**
		 * Uses view to prevent use of non-standard sql inline.
		 */
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT memid,client,product,credit_amount,status,transId,tranDate,tranTime,payableItemId,accountNumber,amount ");
		sqlBuffer.append("FROM   vw_expired_credit ");
		sqlBuffer.append("WHERE  status = ?  ");
		sqlBuffer.append("AND    tranDate < ? ");

		LogUtil.debug(this.getClass(), sqlBuffer.toString());

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			String intervalSpec = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_EXPIRE_CREDIT);

			Interval expireInterval = new Interval(intervalSpec);
			LogUtil.debug(this.getClass(), "Interval " + intervalSpec + " " + SystemParameterVO.CATEGORY_MEMBERSHIP + "/" + SystemParameterVO.MEM_EXPIRE_CREDIT + " - " + new DateTime().subtract(expireInterval).toSQLDate());
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(sqlBuffer.toString());

			statement.setString(1, MembershipVO.STATUS_CANCELLED);
			statement.setDate(2, new DateTime().subtract(expireInterval).toSQLDate());

			ResultSet rs = statement.executeQuery();
			Integer memId = null;
			Integer transId = null;
			MembershipVO memVO = null;
			DateTime transactionDate = null;
			double creditRemoved = 0;
			String resp = null;
			User roadsideUser = SecurityHelper.getRoadsideUser();
			while (rs.next()) {
				memId = new Integer(rs.getString(1));
				transId = new Integer(rs.getString(6));
				LogUtil.debug(this.getClass(), "--->>>transactionId=" + transId);
				try {
					memVO = membershipMgr.getMembership(memId);
					LogUtil.debug(this.getClass(), "--->>>clientNumber=" + memVO.getClientNumber());
					transactionDate = new DateTime(rs.getDate(7).getTime() + rs.getInt(8));
					// do this in a new transaction
					creditRemoved = membershipMgr.removeExpiredCredit(transId, memVO, transactionDate, roadsideUser);
					removedCredit.put(memVO.getClientNumber(), new Double(creditRemoved));
				} catch (RemoteException ex) {
					LogUtil.fatal(this.getClass(), "Error removing credit. " + memId + " " + ex.getMessage());
				}
			}
			// redundant as closed by close statement below
			rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			throw new SystemException("Remove Expired Membership Credits " + sqlBuffer.toString(), e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return removedCredit;
	}

	/**
	 * TRANSACTION BOUNDARY
	 * 
	 * @param transId Integer
	 * @param memVO MembershipVO
	 * @param transactionDate DateTime
	 * @param systemUser UserVO
	 * @throws RemoteException
	 * @return TransactionGroupExpireCreditImpl
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public double removeExpiredCredit(Integer transId, MembershipVO memVO, DateTime transactionDate, User systemUser) throws RemoteException {
		// create a specialised transaction group
		TransactionGroupExpireCreditImpl transGroup = new TransactionGroupExpireCreditImpl(memVO, transId, transactionDate, systemUser);

		transGroup.addSelectedClient(memVO);

		ClientVO client = memVO.getClient();
		if (Client.STATUS_DECEASED.equals(client.getStatus())) {
			throw new RemoteException("Unable to remove expired credit as member is deceased and may require a refund.");
		}

		transGroup.setContextClient(memVO);

		// create the transactions for the selected clients - this includes
		// initialising the
		// credit removed variable.
		transGroup.createTransactionsForSelectedClients();

		// only process the credit if the credit to be removed is greater than 0
		if (transGroup.getCreditRemoved() > 0) {
			// save the transactions
			transGroup.submit();
		}

		return transGroup.getCreditRemoved();
	}

//	private final String SQL_EXPIRED_ALLOW_TO_LAPSE = "select top 1000 membership_id, allow_to_lapse_reason_code from pub.mem_membership where allow_to_lapse = ? and expiry_date < ? and membership_status <> '" + MembershipVO.STATUS_CANCELLED + " order by expiry_date desc " + "'";
	private final String SQL_EXPIRED_ALLOW_TO_LAPSE = "select membership_id, allow_to_lapse_reason_code from pub.mem_membership where allow_to_lapse = ? and expiry_date < ? and membership_status <> '" + MembershipVO.STATUS_CANCELLED + "' order by expiry_date desc ";

	/**
	 * Get all memberships that are allowed to lapse and have expired.
	 * 
	 * @throws RemoteException
	 */
	public List<Integer> autoCancelAllowToLapse() throws RemoteException {
		List<Integer> autoCancelledAllowToLapseMemberships = new ArrayList<Integer>();
		DateTime today = new DateTime().getDateOnly();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Integer membershipId = null;
		String allowToLapseReasonCode = null;
		User roadsideUser = SecurityHelper.getRoadsideUser();
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();

		int cancelledCount = 0;
		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		List<MembershipVO> membersToCancel = new ArrayList<MembershipVO>();

		String thresholdStr = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.AUTOCANCEL_TRANSACTION_THRESHOLD);
		int threshold = Integer.parseInt(thresholdStr);

		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(SQL_EXPIRED_ALLOW_TO_LAPSE);
			// only those marked with allow to lapse
			statement.setBoolean(1, true);
			statement.setDate(2, today.toSQLDate());
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				membershipId = new Integer(resultSet.getInt(1));
				try {
					MembershipVO memVO = membershipMgrLocal.getMembership(membershipId);
					membersToCancel.add(memVO);
				} catch (RemoteException r) {
					LogUtil.log(getClass(), "Unable to lookup membership by id: " + membershipId);
				}
			}
		} catch (Exception ex) {
			LogUtil.fatal(getClass(), ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		for (MembershipVO memVO : membersToCancel) {
			allowToLapseReasonCode = memVO.getAllowToLapseReasonCode();
			// default if there is not one already
			if (allowToLapseReasonCode == null || allowToLapseReasonCode.equals("")) {
				allowToLapseReasonCode = ReferenceDataVO.MEMCANCELREASON_ALLOW_TO_LAPSE;
			}
			LogUtil.debug(this.getClass(), "membershipId=" + membershipId + ",allowToLapseReasonCode=" + allowToLapseReasonCode);

			try {
				membershipMgrLocal.cancelMembership(allowToLapseReasonCode, memVO, roadsideUser);
				autoCancelledAllowToLapseMemberships.add(memVO.getClientNumber());
				cancelledCount++;
			} catch (RemoteException e) {
				LogUtil.log(getClass(), "Unable to auto-cancel (allow to lapse) membership id: " + membershipId);
			}

			if (cancelledCount > threshold) {
				LogUtil.log(getClass(), "Auto Cancel System (allow to lapse) hit limit (" + threshold + ") and forced abort.");
				break;
			}
		}

		LogUtil.log(this.getClass(), "Records affected (allow to lapse)=" + cancelledCount);

		return autoCancelledAllowToLapseMemberships;
	}

//	private static final String SQL_DECEASED_WITH_ACTIVE_MEMBERSHIPS = "select top 1000 membership_id, membership_status from pub.mem_membership where membership_status <> '" + MembershipVO.STATUS_CANCELLED + "' and client_number in (select [client-no] from [pub].[cl-master] where [client-status] = '" + Client.STATUS_DECEASED + " order by expiry_date desc " + "')";
	private static final String SQL_DECEASED_WITH_ACTIVE_MEMBERSHIPS = "select membership_id, membership_status from pub.mem_membership mem join PUB.[cl-master] cl on [client-no] = mem.client_number where mem.membership_status <> '" + MembershipVO.STATUS_CANCELLED + "' and cl.[client-status] = '" + Client.STATUS_DECEASED + "' order by mem.expiry_date desc";
	/**
	 * Get all active memberships that are attached to a deceased client and cancel.
	 * 
	 * @throws RemoteException
	 */
	public List<Integer> autoCancelDeceased() throws RemoteException {
		List<Integer> autoCancelledDeceasedMemberships = new ArrayList<Integer>();
		DateTime today = new DateTime().getDateOnly();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Integer membershipId = null;

		int cancelledCount = 0;
		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		User roadsideUser = SecurityHelper.getRoadsideUser();
		List<MembershipVO> membersToCancel = new ArrayList<MembershipVO>();
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();

		String thresholdStr = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.AUTOCANCEL_TRANSACTION_THRESHOLD);
		int threshold = Integer.parseInt(thresholdStr);

		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(SQL_DECEASED_WITH_ACTIVE_MEMBERSHIPS);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				membershipId = new Integer(resultSet.getInt(1));
				LogUtil.debug(this.getClass(), "membershipId=" + membershipId);

				try {
					MembershipVO memVO = membershipMgrLocal.getMembership(membershipId);
					membersToCancel.add(memVO);
				} catch (RemoteException r) {
					LogUtil.log(getClass(), "Unable to lookup membership by id: " + membershipId);
				}
			}
		} catch (Exception ex) {
			LogUtil.fatal(getClass(), ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		// cancel memberships
		for (MembershipVO memVO : membersToCancel) {
			LogUtil.log(getClass(), cancelledCount + ": attempting to auto-cancel (deceased) membership id: " + membershipId);
			try {
				membershipMgrLocal.cancelMembership(ReferenceDataVO.MEMCANCELREASON_DECEASED, memVO, roadsideUser);
				autoCancelledDeceasedMemberships.add(memVO.getClientNumber());
				cancelledCount++;
			} catch (RemoteException e) {
				LogUtil.log(getClass(), "Unable to auto-cancel (deceased) membership id: " + membershipId);
			}

			if (cancelledCount > threshold) {
				LogUtil.log(getClass(), "Auto Cancel System (deceased) hit limit (" + threshold + ") and forced abort.");
				break;
			}
		}

		LogUtil.log(this.getClass(), "Records affected (deceased)=" + cancelledCount);

		return autoCancelledDeceasedMemberships;
	}

	private static final String SQL_EXPIRED_WITH_ACTIVE_MEMBERSHIPS = "select membership_id from pub.mem_membership where expiry_date < ? and membership_status <> '" + MembershipVO.STATUS_CANCELLED + "' and membership_status <> '" + MembershipVO.STATUS_UNDONE + "'" + " order by expiry_date desc ";

	/**
	 * Get all active memberships that are expired since a given Interval.
	 * 
	 * @throws RemoteException
	 */
	public List<Integer> autoCancelExpired() throws RemoteException {
		List<Integer> autoCancelledExpiredMemberships = new ArrayList<Integer>();
		DateTime today = new DateTime().getDateOnly();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Integer membershipId = null;
		Interval expiryDate = null;
		int cancelledCount = 0;
		int processedCount = 0;

		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
		List<MembershipVO> membersToCancel = new ArrayList<MembershipVO>();
		User roadsideUser = SecurityHelper.getRoadsideUser();

		String thresholdStr = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.AUTOCANCEL_TRANSACTION_THRESHOLD);
		int threshold = Integer.parseInt(thresholdStr);

		String intervalSpec = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.AUTOCANCEL_EXPIRY_PERIOD);
		try {
			expiryDate = new Interval(intervalSpec);
		} catch (Exception e) {
			throw new SystemException(e);
		}

		try {
			connection = clientDataSource.getConnection();

			statement = connection.prepareStatement(SQL_EXPIRED_WITH_ACTIVE_MEMBERSHIPS);
			statement.setDate(1, new DateTime().subtract(expiryDate).toSQLDate());
			resultSet = statement.executeQuery();

			// get memberships
			while (resultSet.next()) {
				membershipId = new Integer(resultSet.getInt(1));
				LogUtil.debug(this.getClass(), "membershipId=" + membershipId);

				try {
					MembershipVO memVO = membershipMgrLocal.getMembership(membershipId);
					membersToCancel.add(memVO);
				} catch (RemoteException r) {
					LogUtil.log(getClass(), "Unable to lookup membership by id: " + membershipId);
				}
			}
		} catch (Exception ex) {
			LogUtil.fatal(getClass(), ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		// cancel memberships
		for (MembershipVO memVO : membersToCancel) {
			membershipId = memVO.getMembershipID();
			processedCount++;

			String reasonCode = ReferenceDataVO.MEMCANCELREASON_UNFINANCIAL;
			if (memVO.getClient().getStatus().equalsIgnoreCase(Client.STATUS_ADDRESS_UNKNOWN)) {
				reasonCode = ReferenceDataVO.MEMCANCELREASON_ADDRESS_UNKNOWN;
			}

			LogUtil.log(getClass(), "processed " + processedCount + " cancelled " + cancelledCount + ": attempting to auto-cancel (expired) membership id: " + membershipId);

			try {
				membershipMgrLocal.cancelMembership(reasonCode, memVO, roadsideUser);
				autoCancelledExpiredMemberships.add(memVO.getClientNumber());
				cancelledCount++;
			} catch (RemoteException r) {
				LogUtil.log(getClass(), "Unable to auto-cancel (expired) membership id: " + membershipId);
			}

			if (cancelledCount > threshold) {
				LogUtil.log(getClass(), "Auto Cancel System (expired) hit limit (" + threshold + ") and forced abort.");
				break;
			}
		}

		LogUtil.log(this.getClass(), "Records affected (expired)=" + cancelledCount);

		return autoCancelledExpiredMemberships;
	}

	public void cancelExpiredRenewalDocuments() throws IOException, ParseException {
		LogUtil.log(this.getClass(), "cancelExpiredRenewalDocuments start");
		MembershipDocument md = null;

		// get renewal notices that are not cancelled
		String hql = "from MembershipDocument md where (md.documentStatus = '' or md.documentStatus is null) and md.documentDate < :pastdate " + "and (md.documentTypeCode = :advice) order by md.documentDate desc"; // md.documentTypeCode

		CommonMgrLocal commonMgr = CommonEJBHelper.getCommonMgrLocal();

		// today - 5 months.
		String postExpireRenewalPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.POST_EXPIRE_RENEWAL_PERIOD);
		Interval postExpireRenewalPeriod = new Interval(postExpireRenewalPeriodSpec);

		// batch size to process
		String maxResultsKey = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.POST_EXPIRE_RENEWAL_MAX_RESULTS);
		int maxResults = Integer.parseInt(maxResultsKey);

		// log dir
		String outputDirectoryName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_LOG_DIR);
		if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') {
			outputDirectoryName += "/";
		}

		String fileName = outputDirectoryName + DateUtil.formatYYYYMMDD(new DateTime()) + "cancelmemdocs.csv";

		DateTime pastDate = new DateTime().subtract(postExpireRenewalPeriod);

		FileWriter fileWriter = new FileWriter(fileName, true); // append
		fileWriter.write("Membership ID,Document ID,\n");

		LogUtil.log(this.getClass(), "Cancelling all documents expiring before " + pastDate.formatShortDate());

		// LogUtil.log(this.getClass(),"hql="+hql);
		org.hibernate.Query query = hsession.createQuery(hql);
		query.setDate("pastdate", pastDate);
		// query.setString("notice", MembershipDocument.RENEWAL_NOTICE);
		query.setString("advice", MembershipDocument.RENEWAL_ADVICE);
		query.setMaxResults(maxResults);

		List<MembershipDocument> results = query.list();
		// ScrollableResults sr = query.scroll(ScrollMode.FORWARD_ONLY);

		Integer documentId = null;
		MembershipVO mem = null;

		int counter = 0;
		// MembershipDocument doc = null;
		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
		// for each renewal notice
		for (MembershipDocument doc : results)
		// while (sr.next())
		{
			counter++;
			try {
				// doc = (MembershipDocument)sr.get(0);
				documentId = doc.getDocumentId();
				LogUtil.log(this.getClass(), counter + ": Removing document " + documentId);
				membershipMgrLocal.cancelRenewalDocument(documentId, true); // ignore
				// errors

				fileWriter.write(doc.getMembershipId() + "," + documentId + ",\n");

				if (counter % 100 == 0) {
					hsession.flush();
					hsession.clear();
					fileWriter.flush();
				}
			} catch (Exception e) {
				LogUtil.log(this.getClass(), "Error removing renewal document for " + documentId + " " + ExceptionHelper.getExceptionStackTrace(e));

				fileWriter.write(doc.getMembershipId() + "," + documentId + "," + ExceptionHelper.getExceptionStackTrace(e) + "\n");
			}
		}

		results = null; // schedule for gc

		fileWriter.flush();
		fileWriter.close();

		// sr.close();
		LogUtil.log(this.getClass(), "cancelExpiredRenewalDocuments end");

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelRenewalDocument(Integer documentId, boolean ignoreErrors) throws RemoteException, RenewalNoticeException {
		PaymentTransactionMgrLocal paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
		try {
			removeRenewalDocument(documentId, MembershipMgr.MODE_CANCEL, paymentTransactionMgrLocal, ignoreErrors);
			paymentTransactionMgrLocal.commit();
		} catch (Exception e) {
			paymentTransactionMgrLocal.rollback();
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelRenewalDocument(Integer documentId) throws RemoteException, RenewalNoticeException {
		cancelRenewalDocument(documentId, false);
	}

	/**
	 * @param reasonCode
	 * @param memVO
	 * @param systemUser
	 * @throws RemoteException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelMembership(String reasonCode, MembershipVO memVO, User systemUser) throws RemoteException {

		System.out.println(">>> auto-cancelling membership, clientNo: " + memVO.getClientNumber() + ", reasonCode: " + reasonCode);

		if (reasonCode.equalsIgnoreCase(ReferenceDataVO.MEMCANCELREASON_DECEASED)) {
			ClientVO client = memVO.getClient();
			if (!Client.STATUS_DECEASED.equalsIgnoreCase(client.getStatus())) {
				throw new RemoteException("Unable to auto cancel membership as reason is 'deceased', but client is not 'deceased'.");
			}
		}
		if (reasonCode.equalsIgnoreCase(ReferenceDataVO.MEMCANCELREASON_ALLOW_TO_LAPSE) && memVO.isAllowToLapse() != null && !memVO.isAllowToLapse().booleanValue()) {
			throw new RemoteException("Unable to auto cancel membership as reason is 'allow to lapse', but the membership is not marked as 'allow to lapse'.");
		}

		// remove from group
		MembershipHelper.removeFromAllGroups(memVO.getMembershipID());

		// Create a cancel TransactionGroup object to hold the common data in
		// the transaction
		TransactionGroup transGroup = TransactionGroupFactory.getTransactionGroupForCancel(systemUser);
		try {
			transGroup.setMembershipType(memVO.getMembershipType());

			// Add the clients membership being cancelled to the transaction
			// group. This will also add the other members of the membership
			// group
			// that they might belong to. It will also set a default prime
			// addressee.
			String msg = transGroup.addSelectedClient(memVO);
			if (msg != null && !msg.isEmpty()) {
				throw new SystemException(msg);
			}
			transGroup.setContextClient(memVO);

			// Create transactions for all selected clients
			transGroup.createTransactionsForSelectedClients();

			// Set the reason for the cancellation.
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			ReferenceDataVO cancellationReason = refMgr.getCancelReason(reasonCode);
			if (cancellationReason == null) {
				throw new SystemException("Failed to get cancellation reason '" + reasonCode + "'");
			}

			transGroup.setTransactionReason(cancellationReason);
			transGroup.calculateTransactionFee();

			LogUtil.debug(this.getClass(), "txList=" + transGroup.getTransactionList());

			transGroup.setBatch(true);

			// submit transaction
			transGroup.submit();

		} catch (RemoteException ex1) {
			LogUtil.fatal(this.getClass(), "Unable to auto cancel membership. " + ex1.getMessage());

			CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();

			String outputDirectoryName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_LOG_DIR);
			if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') {
				outputDirectoryName += "/";
			}

			String fileName = outputDirectoryName + DateUtil.formatYYYYMMDD(new DateTime()) + "autocancel-error.csv";

			try {
				FileWriter fileWriter = new FileWriter(fileName, true); // append
				fileWriter.write(memVO.getClientNumber() + "," + ex1.getMessage() + "\n");
				fileWriter.flush();
				fileWriter.close();
			} catch (Exception e) {
				LogUtil.fatal(this.getClass(), "Unable to log error to file:" + e.getMessage());
			}

			throw new SystemException(ex1);
		}
	}

	public Collection getClubList() {

		List clubList = hsession.createCriteria(AffiliatedClubVO.class).list();
		return clubList;
	}

	public AffiliatedClubVO getClub(String clubCode) {

		AffiliatedClubVO club = (AffiliatedClubVO) hsession.get(AffiliatedClubVO.class, clubCode);
		return club;
	}

	public void deleteClub(AffiliatedClubVO club) {

		hsession.delete(club);
	}

	public void createClub(AffiliatedClubVO club) {

		hsession.save(club);
	}

	public void updateClub(AffiliatedClubVO club) {

		hsession.update(club);
	}

	public Collection findMembershipCardsByMembership(Integer membershipId) throws RemoteException {
		Criteria crit = hsession.createCriteria(MembershipCardVO.class).add(Expression.eq("membershipID", membershipId));
		Collection cardList = new ArrayList(crit.list());
		return cardList;
	}

	public Collection findMembershipCardsByIssueDate(Integer membershipId, DateTime issueDate) throws RemoteException {

		Criteria crit = hsession.createCriteria(MembershipCardVO.class).add(Expression.eq("membershipID", membershipId));
		if (issueDate != null) {
			// ie. issue_date >= ? or issue_date is null
			crit.add(Expression.or(Expression.ge("issueDate", issueDate), Expression.isNull("issueDate")));
		}
		Collection cardList = new ArrayList(crit.list());
		return cardList;
	}

	private final String SQL_UPDATE_MEMBERSHIP_QUOTE = "UPDATE pub.mem_membership_quote SET client_number = ? WHERE client_number = ?";

	private final String SQL_UPDATE_MEMBERSHIP = "UPDATE pub.mem_membership SET client_number = ?, membership_number = ? WHERE client_number = ?";

	private final String SQL_UPDATE_PENDING_FEE = "UPDATE pub.pt_pending_fee SET client_number = ? WHERE client_number = ?";

	private final String SQL_UPDATE_PAYABLE_ITEM = "UPDATE pub.pt_payableitem SET clientno = ?, description = description + ? WHERE clientno = ?";

	private final String SQL_UPDATE_PAYABLE_ITEM_COMPONENT = "UPDATE pub.pt_payableitemcomponent SET sourcesystemreference = ? WHERE sourcesystemreference = ?";

	public void transferMembership(Connection connection, Integer duplicateClientNumber, Integer masterClientNumber, MembershipVO duplicateClientMembership, MembershipVO masterClientMembership) throws RemoteException {
		if (duplicateClientMembership != null && duplicateClientMembership.isGroupMember()) {
			throw new SystemException("The duplicate client member '" + duplicateClientNumber + "' is a member of a group. The member must be removed from the group first.");
		}

		final String PAYABLE_NARRATIVE = " (Tfr from client " + duplicateClientNumber + ".)";

		PreparedStatement statement1 = null;
		PreparedStatement statement2 = null;
		PreparedStatement statement3 = null;
		PreparedStatement statement4 = null;
		PreparedStatement statement5 = null;

		try {
			LogUtil.debug(this.getClass(), "u 1");
			// payable item components
			statement1 = connection.prepareStatement(SQL_UPDATE_PAYABLE_ITEM_COMPONENT);
			statement1.setString(1, masterClientNumber.toString());
			statement1.setString(2, duplicateClientNumber.toString());
			statement1.executeUpdate();

			LogUtil.debug(this.getClass(), "u 2");

			// Note: payable item may refer to a transaction that does not
			// exist.

			// payable items
			statement2 = connection.prepareStatement(SQL_UPDATE_PAYABLE_ITEM);
			statement2.setInt(1, masterClientNumber.intValue());
			statement2.setString(2, PAYABLE_NARRATIVE);
			statement2.setInt(3, duplicateClientNumber.intValue());
			statement2.executeUpdate();

			LogUtil.debug(this.getClass(), "u 3");
			// pending fees
			statement3 = connection.prepareStatement(SQL_UPDATE_PENDING_FEE);
			statement3.setInt(1, masterClientNumber.intValue());
			statement3.setInt(2, duplicateClientNumber.intValue());
			statement3.executeUpdate();

			LogUtil.debug(this.getClass(), "u 4");
			// quotes
			statement4 = connection.prepareStatement(SQL_UPDATE_MEMBERSHIP_QUOTE);
			statement4.setInt(1, masterClientNumber.intValue());
			statement4.setInt(2, duplicateClientNumber.intValue());
			statement4.executeUpdate();

			// rule applied previously prevents this from occurring.

			/*
			 * Update membership if the master client does not have a membership and the duplicate client has a membership (Expired, Cancelled, etc.).
			 * 
			 * An expired membership cannot be brought across if the master client already has a membership. This is because a client may currently only hold one membership of the same type. The transactions may not be transferred either as they would confuse undo transactions and credit calculations.
			 */
			if (masterClientMembership == null && duplicateClientMembership != null) {
				LogUtil.debug(this.getClass(), "u 5");
				// membership
				statement5 = connection.prepareStatement(SQL_UPDATE_MEMBERSHIP);
				statement5.setInt(1, masterClientNumber.intValue());
				statement5.setString(2, masterClientNumber.toString());
				statement5.setInt(3, duplicateClientNumber.intValue());
				statement5.executeUpdate();

				// may not update any records as the duplicate client may not
				// have
				// had a membership.
			}
			// both clients hold active memberships
			else if (masterClientMembership != null && masterClientMembership.isActive() && duplicateClientMembership != null && duplicateClientMembership.isActive()) {
				throw new SystemException("Both the master client and the duplicate client have active memberships.");
			} else {
				// don't transfer any memberships. Any untransferred memberships
				// will be deleted. The
				// master client's memberships will not be updated.
			}
			LogUtil.debug(this.getClass(), "u 6");
		} catch (SQLException ex) {
			throw new SystemException(ex);
		} finally {
			ConnectionUtil.closeStatement(statement1);
			ConnectionUtil.closeStatement(statement2);
			ConnectionUtil.closeStatement(statement3);
			ConnectionUtil.closeStatement(statement4);
			ConnectionUtil.closeStatement(statement5);
			// leave connection open?
		}

	}

	/**
	 * Notify a membership change.
	 * 
	 * @param notificationMgr NotificationMgr
	 * @param membershipList Collection The list of memberships to notify the change.
	 * @param delete boolean Mark membership as cancelled which will appear as deleted in CAD
	 * @throws RemoteException
	 */
	// public void notifyMembership(NotificationMgr notificationMgr, Collection
	// membershipList, boolean delete)
	// throws RemoteException
	// {
	// boolean commit = false;
	// if (membershipList != null &&
	// !membershipList.isEmpty())
	// {
	// //if notificationMgr is null then transaction scope is limited to this
	// method
	// if (notificationMgr == null)
	// {
	// notificationMgr = CommonEJBHelper.getNotificationMgr();
	// commit = true;
	// }
	// MembershipVO deletedMembership = null;
	// try
	// {
	// //typically only one.
	// for(Iterator i = membershipList.iterator(); i.hasNext(); )
	// {
	// deletedMembership = (MembershipVO)i.next();
	// if (delete)
	// {
	// deletedMembership.setStatus(MembershipVO.STATUS_CANCELLED);
	// }
	// NotificationEvent event = new MembershipChangeNotificationEvent(
	// MembershipChangeNotificationEvent.ACTION_UPDATE, deletedMembership);
	// event.setDelay(100);
	// notificationMgr.notifyEvent(event);
	// }
	// if(commit)
	// {
	// if (notificationMgr != null)
	// {
	// notificationMgr.commit();
	// }
	// }
	// }
	// catch(RollBackException ex1)
	// {
	// if (notificationMgr != null)
	// {
	// notificationMgr.rollback();
	// }
	// throw new RemoteException("Unable to notify membership.",ex1);
	// }
	// }
	// }

	public Collection findMembershipCardRequests(Integer membershipId) throws RemoteException {
		LogUtil.log(this.getClass(), "findMembershipCardRequests membershipId=" + membershipId);

		Criteria crit = hsession.createCriteria(MembershipCardVO.class).add(Expression.eq("membershipID", membershipId));
		// issued date is null
		crit.add(Expression.isNull("issueDate"));
		Collection cardList = new ArrayList(crit.list());
		LogUtil.log(this.getClass(), "cardList=" + cardList);
		return cardList;
	}

	// membership transaction operations to avoid BMT in
	// MembershipTransactionMgr

	public MembershipTransactionVO findMembershipTransactionByQuoteNumber(Integer quoteNum) throws RemoteException {
		MembershipTransactionVO memTransVO = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipTransactionVO.class).add(Expression.eq("quoteNumber", quoteNum));
			Iterator memTxIt = crit.list().iterator();
			if (memTxIt.hasNext()) {
				memTransVO = (MembershipTransactionVO) memTxIt.next();
			}
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return memTransVO;
	}

	/**
	 * Retrieve a list of renewal notices that have been sent in the pre-expiry renewal period.
	 * 
	 * @param membershipId
	 * @param expiryDate
	 * @return
	 * @throws GenericException
	 */
	public List<MembershipDocument> getCurrentDocuments(Integer membershipId, DateTime expiryDate) throws GenericException {

		Interval preRenewalPeriod = null;

		try {
			CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
			String preRenewalPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.IN_RENEWAL_PERIOD);
			preRenewalPeriod = new Interval(preRenewalPeriodSpec);
		} catch (Exception e) {
			throw new GenericException(e.getMessage());
		}
		DateTime renewalPeriodStart = expiryDate.subtract(preRenewalPeriod);

		String queryString = "from MembershipDocument md where md.membershipId = :membershipId ";
		queryString += "and (md.documentTypeCode = '" + MembershipDocument.RENEWAL_NOTICE + "' or md.documentTypeCode = '" + MembershipDocument.RENEWAL_ADVICE + "') ";
		queryString += "and (md.documentStatus is null or md.documentStatus != '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "') ";
		queryString += "and (md.documentDate >= :renewalPeriodStart or md.feeEffectiveDate = :feeEffectiveDate) ";
		queryString += " order by md.documentId desc";

		org.hibernate.Query query = hsession.createQuery(queryString);
		query.setInteger("membershipId", membershipId.intValue());
		query.setDate("renewalPeriodStart", renewalPeriodStart);
		query.setDate("feeEffectiveDate", expiryDate);

		List<MembershipDocument> result = query.list();

		LogUtil.log(this.getClass(), "result=" + result.toString());

		return result;
	}

	/**
	 * Look for a renewal notice that has been sent in the pre-expiry renewal period.
	 * 
	 * @param membershipId
	 * @param expiryDate
	 * @return
	 * @throws RemoteException
	 * @throws GenericException
	 */
	public MembershipDocument getCurrentDocument(Integer membershipId, DateTime expiryDate) throws RemoteException, GenericException {
		LogUtil.debug(this.getClass(), "membershipId=" + membershipId);
		MembershipDocument currentDocument = null;
		List<MembershipDocument> result = getCurrentDocuments(membershipId, expiryDate);

		if (result.size() > 1) {
			// cancel all except latest
			currentDocument = result.remove(0);

			PaymentTransactionMgrLocal paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
			for (MembershipDocument membershipDocument : result) {
				this.removeRenewalDocument(membershipDocument.getDocumentId(), MembershipMgr.MODE_CANCEL, paymentTransactionMgrLocal, true);
			}
		} else if (result.size() == 1) {
			currentDocument = result.get(0);
		} else if (result.size() == 0) {
			return null;
		}

		return currentDocument;
	}

	private final String SQL_SELECT_MEMBERSHIP_RENEWAL_ADVICE_PREFIX = "SELECT distinct [PUB].[mem_membership].[membership_Id] " + "FROM [PUB].[mem_document] inner join [PUB].mem_membership " + "on [PUB].[mem_document].membership_id = [PUB].mem_membership.membership_id " + "where document_type_code = 'Renewal Advice' and cancelDate is null " + "and fee_effective_date = expiry_date";

	/**
	 * Retrieve a list of memberships having active renewal advices.
	 * 
	 * @param expiryDate
	 * @return
	 * @throws RemoteException
	 */
	public List<MembershipVO> getMembershipsWithRenewalAdvices(DateTime expiryDate) throws RemoteException {

		List<MembershipVO> members = new ArrayList<MembershipVO>();

		PreparedStatement statement = null;
		String statementText = SQL_SELECT_MEMBERSHIP_RENEWAL_ADVICE_PREFIX;

		if (expiryDate != null) {
			statementText += " and expiry_date >= ?";
		}

		Connection connection = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			if (expiryDate != null) {
				statement.setDate(1, expiryDate.toSQLDate());
			}

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				members.add(this.getMembership(resultSet.getInt(1)));
			}
		} catch (SQLException e) {
			throw new RemoteException("Error retrieving memberships with renewal advices.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return members;
	}

	private final String SQL_MEMBERSHIPS_WITHOUT_CARDS = "SELECT membership_id \n" + "FROM [PUB].[mem_membership] \n" + "WHERE membership_id NOT IN (SELECT DISTINCT membership_id FROM pub.mem_membership_card) \n" + "AND membership_status NOT IN ('cancelled','undone') \n" + "AND product_code IN ('Advantage','Ultimate','Access')\n" + "AND expiry_date > GETDATE()";

	/**
	 * Retrieve a list of active Advantage, Ultimate, Access memberships with no cards.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public List<MembershipVO> getMembershipsWithoutCards() throws RemoteException {
		List<MembershipVO> results = new ArrayList<MembershipVO>();

		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = SQL_MEMBERSHIPS_WITHOUT_CARDS;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer membershipId = resultSet.getInt(1);
				results.add(this.getMembership(membershipId));
			}
		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving memberships without cards.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}

	private final String SQL_MEMBERSHIPS_WITH_VOUCHERS = "SELECT clientNumber, membership_id, voucher, txn_date, membership_status FROM\n" + "(\n" + "SELECT clientNumber, maxx.membership_id, SUM(dbo.FinanceVoucher.disposalAmount) AS voucher, maxx.txn_date, membership_status\n" + "FROM dbo.FinanceVoucher\n" + "INNER JOIN pub.mem_membership ON pub.mem_membership.client_number = dbo.FinanceVoucher.clientNumber\n" + "INNER JOIN\n" + "(\n" + "  SELECT membership_id, MAX(transaction_date) AS txn_date FROM pub.mem_transaction\n" + "  GROUP BY membership_id\n" + ") AS maxx\n" + "ON PUB.mem_membership.membership_id = maxx.membership_id\n" + "WHERE processDate IS NOT null AND status = 'A'\n" + "GROUP BY clientNumber, maxx.membership_id, maxx.txn_date, membership_status\n" + ") AS vouchers\n" + "WHERE vouchers.voucher <> 0";

	/**
	 * Retrieve a list of mapped fields relating to memberships with vouchers.
	 * 
	 * @return Map containing clientNumber, membershipId, voucher, last transaction date - txnDate, status.
	 * @throws RemoteException
	 */
	public List<Map<String, Object>> getMembershipsWithVouchers() throws RemoteException {
		List<Map<String, Object>> members = new ArrayList<Map<String, Object>>();
		Map<String, Object> result;

		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = SQL_MEMBERSHIPS_WITH_VOUCHERS;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer clientNo = resultSet.getInt(1);
				Integer membershipId = resultSet.getInt(2);
				BigDecimal credit = resultSet.getBigDecimal(3);
				Date txnDate = resultSet.getDate(4);
				String status = resultSet.getString(5);

				result = new HashMap<String, Object>();
				result.put("clientNumber", clientNo);
				result.put("membershipId", membershipId);
				result.put("voucher", credit);
				result.put("txnDate", txnDate);
				result.put("status", status);

				members.add(result);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving memberships with credit.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return members;
	}

	private final String SQL_MEMBERSHIPS_WITH_CREDIT_POSTINGS = "SELECT clientNo, membership_id, amt, txn_date, membership_status FROM\n" + "(\n" + "SELECT clientNo, maxx.membership_id, SUM(PUB.pt_payableItemPosting.amount) AS amt, maxx.txn_date, membership_status\n" + "FROM [PUB].[pt_payableItemPosting]\n" + "INNER JOIN pub.pt_payableItemComponent ON PUB.pt_payableItemPosting.payableItemComponentID = PUB.pt_payableItemComponent.payableItemComponentID\n" + "INNER JOIN pub.pt_payableItem ON PUB.pt_payableItemComponent.payableItemID = PUB.pt_payableItem.payableItemID\n" + "INNER JOIN pub.mem_membership ON pub.mem_membership.client_number = pub.pt_payableItem.clientNo\n" + "INNER JOIN\n" + "(\n" + "  SELECT membership_id, MAX(transaction_date) AS txn_date FROM pub.mem_transaction\n" + "  GROUP BY membership_id\n" + ") AS maxx\n" + "ON PUB.mem_membership.membership_id = maxx.membership_id\n" + "WHERE accountNumber = 2310 and posted is not null\n" + "GROUP BY clientNo, maxx.membership_id, maxx.txn_date, membership_status\n" + ") AS payables\n" + "WHERE payables.amt <> 0\n";

	/**
	 * Retrieve a list of mapped fields relating to memberships with credit postings.
	 * 
	 * @return Map containing clientNumber, membershipId, posting, last transaction date - txnDate, status.
	 * @throws RemoteException
	 */
	public List<Map<String, Object>> getMembershipsWithCreditPostings() throws RemoteException {
		List<Map<String, Object>> members = new ArrayList<Map<String, Object>>();
		Map<String, Object> result;

		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = SQL_MEMBERSHIPS_WITH_CREDIT_POSTINGS;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer clientNo = resultSet.getInt(1);
				Integer membershipId = resultSet.getInt(2);
				BigDecimal credit = resultSet.getBigDecimal(3);
				Date txnDate = resultSet.getDate(4);
				String status = resultSet.getString(5);

				result = new HashMap<String, Object>();
				result.put("clientNumber", clientNo);
				result.put("membershipId", membershipId);
				result.put("posting", credit);
				result.put("txnDate", txnDate);
				result.put("status", status);

				members.add(result);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving memberships with credit.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return members;
	}

	private final String SQL_MEMBERSHIPS_WITH_CURRENT_CREDIT = "SELECT client_number, maxx.membership_id, credit_amount, maxx.txn_date, membership_status\n" + "FROM \n" + "(\n" + "	SELECT membership_id, MAX(transaction_date) AS txn_date FROM pub.mem_transaction\n" + "	GROUP BY membership_id\n" + ") AS maxx\n" + "INNER JOIN pub.mem_transaction ON maxx.membership_id = PUB.mem_transaction.membership_id\n" + "INNER JOIN pub.mem_membership ON PUB.mem_transaction.membership_id = PUB.mem_membership.membership_id\n" + "WHERE credit_amount <> 0\n" + "GROUP BY client_number, maxx.membership_id, credit_amount, maxx.txn_date, membership_status\n" + "ORDER BY maxx.txn_date asc\n";

	/**
	 * Retrieve a list of mapped fields relating to memberships with credit.
	 * 
	 * @return Map containing clientNumber, membershipId, amount, last transaction date - txnDate, status.
	 * @throws RemoteException
	 */
	public List<Map<String, Object>> getMembershipsWithCredit() throws RemoteException {
		List<Map<String, Object>> members = new ArrayList<Map<String, Object>>();
		Map<String, Object> result;

		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = SQL_MEMBERSHIPS_WITH_CURRENT_CREDIT;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer clientNo = resultSet.getInt(1);
				Integer membershipId = resultSet.getInt(2);
				BigDecimal credit = resultSet.getBigDecimal(3);
				Date txnDate = resultSet.getDate(4);
				String status = resultSet.getString(5);

				result = new HashMap<String, Object>();
				result.put("clientNumber", clientNo);
				result.put("membershipId", membershipId);
				result.put("credit", credit);
				result.put("txnDate", txnDate);
				result.put("status", status);

				members.add(result);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving memberships with credit.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return members;
	}

	private final String SQL_SUM_MEMBERSHIP_CREDIT_PREFIX = "SELECT SUM(credit_amount) FROM pub.mem_transaction_credit WHERE transaction_id IN (\n" + "SELECT MAX(transaction_id) FROM pub.mem_transaction_credit WHERE transaction_id is not null\n";

	public BigDecimal getTotalInstantaneousMembershipCredit(DateTime startDate, DateTime endDate) throws RemoteException {
		if (startDate == null && endDate == null) {
			throw new SystemException("Both startDate and endDate cannot be null");
		}

		BigDecimal total = BigDecimal.ZERO;

		PreparedStatement statement = null;

		String statementText = SQL_SUM_MEMBERSHIP_CREDIT_PREFIX;

		if (startDate != null) {
			statementText += "		AND transaction_date >= ? \n";
		}
		if (endDate != null) {
			statementText += "		AND transaction_date < ? \n";
		}
		statementText += "		group by membership_id \n" + ")";

		Connection connection = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			int nextIndex = 1;
			if (startDate != null) {
				statement.setDate(nextIndex, startDate.toSQLDate());
				nextIndex++;
			}
			if (endDate != null) {
				statement.setDate(nextIndex, endDate.toSQLDate());
			}

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				total = resultSet.getBigDecimal(1);
			}

			// sanity check
			if (total == null) {
				total = BigDecimal.ZERO;
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving instantaneous membership credit.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return total;
	}

	private final String SQL_MEMBERSHIP_CREDIT_PREFIX = "SELECT membership_id, credit_amount FROM pub.mem_transaction_credit WHERE transaction_id IN (\n" + "SELECT MAX(transaction_id) FROM pub.mem_transaction_credit \n" + "WHERE transaction_id is not null\n";

	/**
	 * Retrieve membership IDs matched to instantaneous credit amounts for date range.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return Map of client numbers to credit amount.
	 * @throws RemoteException
	 */
	public Map<Integer, BigDecimal> getInstantaneousMembershipCredit(DateTime startDate, DateTime endDate) throws RemoteException {
		Map<Integer, BigDecimal> members = new HashMap<Integer, BigDecimal>();

		PreparedStatement statement = null;

		String statementText = SQL_MEMBERSHIP_CREDIT_PREFIX;
		if (startDate != null) {
			statementText += "		AND transaction_date >= ? \n";
		}
		if (endDate != null) {
			statementText += "		AND transaction_date < ? \n";
		}
		statementText += "		group by membership_id \n" + ")";

		Connection connection = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			int nextIndex = 1;
			if (startDate != null) {
				statement.setDate(nextIndex, startDate.toSQLDate());
				nextIndex++;
			}
			if (endDate != null) {
				statement.setDate(nextIndex, endDate.toSQLDate());
			}

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer clientNo = resultSet.getInt(1);
				BigDecimal amount = resultSet.getBigDecimal(2);

				members.put(clientNo, amount);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving instantaneous membership credit.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return members;
	}

	/**
	 * Retrieve the total membership credit amount.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public BigDecimal getTotalCredit() throws RemoteException {
		PreparedStatement statement = null;
		BigDecimal total = BigDecimal.ZERO;

		String statementText = "SELECT sum([credit_amount]) FROM [PUB].[mem_membership]";

		Connection connection = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				total = resultSet.getBigDecimal(1);
			}
		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving total of credit.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return total;
	}

	private final String SQL_SELECT_AUTOREN_AUDIT = "select * from [dbo].[20120430autoren_audit]";

	public List<Map<String, Object>> getAutoRenAuditEntries() throws RemoteException {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
		PreparedStatement statement = null;

		String statementText = SQL_SELECT_AUTOREN_AUDIT;

		Connection connection = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Map<String, Object> lookup = new HashMap<String, Object>();

				Integer clientNo = resultSet.getInt(1);
				String paymentType = resultSet.getString(2);
				Date renewalDate = resultSet.getDate(3);
				BigDecimal amount = resultSet.getBigDecimal(4);
				Integer groupCount = resultSet.getInt(5);
				String product = resultSet.getString(6);

				lookup.put("clientNo", clientNo);
				lookup.put("paymentType", paymentType);
				lookup.put("renewalDate", renewalDate);
				lookup.put("amount", amount);
				lookup.put("groupCount", groupCount);
				lookup.put("product", product);

				results.add(lookup);
			}

		} catch (SQLException e) {
			System.err.println("SQL: " + statementText);
			throw new RemoteException("Error retrieving autorenew audit list.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return results;
	}

	/**
	 * Returns a mapping of client numbers to DD schedule IDs for renewal advices that contain links to non-existant DD schedules.
	 * 
	 * @param expiryDate
	 * @return
	 * @throws RemoteException
	 */
	public Map<String, String> getRenewalAdvicesWithoutDDSchedules(DateTime expiryDate) throws RemoteException {

		Map<String, String> result = new HashMap<String, String>();
		List<MembershipVO> members = this.getMembershipsWithRenewalAdvices(expiryDate);

		LogUtil.log(getClass(), "DD Audit - found " + members.size() + " members with renewal advices");

		for (MembershipVO memVO : members) {
			LogUtil.log(getClass(), "DD Audit - getting current renewal doc for member: " + memVO.getClientNumber());
			MembershipDocument doc = memVO.getCurrentRenewalDocument();

			if (doc == null) {
				// System.out.println("no renewal doc found");
				continue;
			}
			TransactionGroup transGroup = MembershipHelper.getTransactionGroup(doc.getTransactionXML());

			try {
				transGroup.getDirectDebitSchedule();
			} catch (RemoteException e) {
				result.put(transGroup.getDirectDebitScheduleId().toString(), memVO.getClientNumber().toString());
			}
		}

		return result;
	}

	public Collection getTransactionList(Integer memID) throws RemoteException {
		// default order desc
		MethodCounter.addToCounter("MembershipMgr", "getTransactionList");
		Collection membershipTransactionList = null;

		membershipTransactionList = (Collection) CacheHandler.getCacheValue(memID, CACHE_TRANSACTION_LIST);

		if (membershipTransactionList == null) {

			try {

				Criteria crit = hsession.createCriteria(MembershipTransactionVO.class).add(Expression.eq("membershipID", memID)).addOrder(Order.desc("transactionDate"));
				membershipTransactionList = new ArrayList(crit.list());

				CacheHandler.putCacheValue(memID, membershipTransactionList, CACHE_TRANSACTION_LIST);

			} catch (HibernateException ex) {
				throw new RemoteException("Unable to locate session factory.", ex);
			}
		}
		return membershipTransactionList;
	}

	/*
	 * membershipTransactionList = (Collection)CacheHandler.getCacheValue(transactionGroupId, CACHE_GROUP_TRANSACTION_LIST);
	 * 
	 * if(membershipTransactionList == null) { MethodCounter.addToCounter("MembershipMgr", "findMembershipTransactionByTransactionGroupID" + transGroupID);
	 * 
	 * try {
	 * 
	 * Criteria crit = hsession.createCriteria(MembershipTransactionVO.class).add(Expression.eq( "transactionGroupID", transactionGroupId)); membershipTransactionList = new ArrayList(crit.list()); CacheHandler.putCacheValue(transactionGroupId, membershipTransactionList,CACHE_GROUP_TRANSACTION_LIST);
	 */

	public final static String CACHE_GROUP_TRANSACTION_LIST = "groupTransactionList";

	public final static String CACHE_TRANSACTION_LIST = "transactionList";

	public Collection findMembershipTransactionByTransactionGroupID(int transGroupID) throws RemoteException {
		Collection membershipTransactionList = null;
		Integer transactionGroupId = new Integer(transGroupID);

		membershipTransactionList = (Collection) CacheHandler.getCacheValue(transactionGroupId, CACHE_GROUP_TRANSACTION_LIST);

		if (membershipTransactionList == null) {
			MethodCounter.addToCounter("MembershipMgr", "findMembershipTransactionByTransactionGroupID" + transGroupID);

			try {

				Criteria crit = hsession.createCriteria(MembershipTransactionVO.class).add(Expression.eq("transactionGroupID", transactionGroupId));
				membershipTransactionList = new ArrayList(crit.list());
				CacheHandler.putCacheValue(transactionGroupId, membershipTransactionList, CACHE_TRANSACTION_LIST);

			} catch (HibernateException ex) {
				throw new RemoteException("Unable to locate session factory.", ex);
			}
		}
		return membershipTransactionList;

	}

	public MembershipTransactionVO findMembershipTransactionForMemberAndGroup(int membershipID, int transactionGroupID) throws RemoteException {
		Collection membershipTransactionList = null;
		MembershipTransactionVO memTransVO = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipTransactionVO.class).add(Expression.eq("transactionGroupID", new Integer(transactionGroupID))).add(Expression.eq("membershipID", new Integer(membershipID)));
			membershipTransactionList = new ArrayList(crit.list());
			if (membershipTransactionList != null) {
				if (membershipTransactionList.size() > 1) {
					throw new RemoteException("Should only be one transaction for member and group.");
				}
				memTransVO = (MembershipTransactionVO) membershipTransactionList.iterator().next();
			}

		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return memTransVO;

	}

	public Collection findMembershipTransactionsByTransactionID(Integer transID) throws RemoteException {
		MembershipTransactionVO memTransVO = getMembershipTransaction(transID);
		Collection transList = null;
		if (memTransVO.getTransactionGroupID() == 0) {
			transList = new ArrayList();
			transList.add(memTransVO);
		} else {
			transList = findMembershipTransactionByTransactionGroupID(memTransVO.getTransactionGroupID());
		}
		return transList;
	}

	/**
	 * Return a list of membership transactions belonging to the same transaction group that the specified transaction is in.
	 * 
	 * @param transID Description of the Parameter
	 * @return Description of the Return Value
	 */
	public MembershipTransactionVO getMembershipTransaction(Integer transID) throws RemoteException {
		MethodCounter.addToCounter("MembershipMgr", "getMembershipTransaction");

		MembershipTransactionVO memTransVO = null;
		try {

			memTransVO = (MembershipTransactionVO) hsession.get(MembershipTransactionVO.class, transID);
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to load membership transaction.", ex);
		}
		return memTransVO;

	}

	public Collection findMembershipTransactionByPayableItemID(Integer payItemID) throws RemoteException {
		Collection membershipTransactionList = null;

		try {

			Criteria crit = hsession.createCriteria(MembershipTransactionVO.class).add(Expression.eq("payableItemID", payItemID));
			membershipTransactionList = new ArrayList(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipTransactionList;
	}

	public MembershipTransactionVO getLastMembershipTransaction(Integer membershipId) throws RemoteException {

		String queryString = "from MembershipTransactionVO mt where mt.membershipID = :membershipId and mt.transactionID = (select max(mt2.transactionID) from MembershipTransactionVO mt2 where mt2.membershipID = mt.membershipID and undone_date is null)";
		List result = hsession.createQuery(queryString).setInteger("membershipId", membershipId.intValue()).list();
		if (result != null && result.size() > 0) {
			return (MembershipTransactionVO) result.iterator().next();
		} else {
			return null;
		}
	}

	private void insertAdjustmentList(Integer transactionID, Collection adjList) throws RemoteException {
		Connection connection = null;
		LogUtil.log(this.getClass(), "insertAdjustmentList 1");
		if (adjList != null && !adjList.isEmpty()) {
			LogUtil.log(this.getClass(), "insertAdjustmentList 2");
			MembershipTransactionAdjustment memTransAdjustment = null;
			String statementText = "INSERT INTO PUB.mem_transaction_adjustment (transaction_id, adjustment_type_code, adjustment_amount, adjustment_reason) VALUES (?, ?, ?, ?)";
			PreparedStatement statement = null;
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement(statementText);
				Iterator adjIterator = adjList.iterator();
				while (adjIterator.hasNext()) {
					memTransAdjustment = (MembershipTransactionAdjustment) adjIterator.next();
					statement.setInt(1, transactionID.intValue());
					statement.setString(2, memTransAdjustment.getAdjustmentTypeCode());
					statement.setDouble(3, memTransAdjustment.getAdjustmentAmount().doubleValue());
					statement.setString(4, memTransAdjustment.getAdjustmentReason());
					if (statement.executeUpdate() != 1) {
						throw new RemoteException("Failed to insert MembershipTransactionAdjustment");
					}

				}
			} catch (SQLException e) {
				throw new RemoteException("Unable to insert adjustment list.", e);
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	private void insertTransactionFeeList(MembershipTransactionVO memTransVO/*
																			 * Integer transactionID , Collection transFeeList , String userId , String salesBranchCode
																			 */) throws RemoteException {
		Integer transactionID = memTransVO.getTransactionID();
		Collection transFeeList = memTransVO.getTransactionFeeList();
		String userId = memTransVO.getUsername();
		String salesBranchCode = memTransVO.getSalesBranchCode();
		Connection connection = null;
		LogUtil.log(this.getClass(), "insertTransactionFeeList 1");
		if (transFeeList != null && !transFeeList.isEmpty()) {
			LogUtil.log(this.getClass(), "insertTransactionFeeList 1");
			MembershipTransactionFee transFee = null;
			String statementText = "INSERT INTO PUB.mem_transaction_fee (transaction_id, fee_specification_id, fee_per_member, fee_per_vehicle, vehicle_count, fee_description) VALUES (?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = null;
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement(statementText);
				Iterator listIT = transFeeList.iterator();
				while (listIT.hasNext()) {
					transFee = (MembershipTransactionFee) listIT.next();
					statement.setInt(1, transactionID.intValue());
					statement.setInt(2, transFee.getFeeSpecificationID().intValue());
					statement.setDouble(3, transFee.getFeePerMember());
					statement.setDouble(4, transFee.getFeePerVehicle());
					statement.setInt(5, transFee.getVehicleCount());
					statement.setString(6, transFee.getFeeDescription());

					if (statement.executeUpdate() != 1) {
						throw new RemoteException("Failed to insert MembershipTransactionFee");
					}

					// Now create the membership transaction fee discounts
					insertTransactionFeeDiscountList(memTransVO, transFee.getFeeSpecificationID(), transFee.getDiscountList(), userId, salesBranchCode);
				}
			} catch (SQLException e) {
				StringBuffer errMsg = new StringBuffer();
				errMsg.append("Error executing SQL ");
				errMsg.append(statementText);
				errMsg.append(" : \n");
				errMsg.append(e.getMessage());
				errMsg.append(" : \n");
				Throwable nextE = e.getNextException();
				if (nextE != null) {
					errMsg.append("nextSQL exception - " + e.getMessage());
					errMsg.append(" : \n");
				} else {
					errMsg.append("next SQL exception is null. : \n");
				}
				errMsg.append((transFee == null ? "No fee data available." : transFee.toString()));
				errMsg.append("\n: ");
				errMsg.append(e.getMessage());

				throw new RemoteException(errMsg.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	private void insertTransactionFeeDiscountList(MembershipTransactionVO memTransVO, Integer feeSpecID, Collection feeDiscountList, String userId, String salesBranchCode) throws RemoteException {
		Integer transactionID = memTransVO.getTransactionID();
		Connection connection = null;
		LogUtil.debug(this.getClass(), "insertTransactionFeeDiscountList 1");
		if (feeDiscountList != null && !feeDiscountList.isEmpty()) {
			LogUtil.debug(this.getClass(), "insertTransactionFeeDiscountList 2");
			MembershipTransactionDiscount transDiscount = null;
			String statementText = "INSERT INTO PUB.mem_transaction_fee_discount (transaction_id, fee_specification_id, discount_type_code, discount_amount, discount_reason, discount_description, discount_period) VALUES (?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = null;
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement(statementText);
				Iterator listIT = feeDiscountList.iterator();
				while (listIT.hasNext()) {
					transDiscount = (MembershipTransactionDiscount) listIT.next();
					statement.setInt(1, transactionID.intValue());
					statement.setInt(2, feeSpecID.intValue());
					statement.setString(3, transDiscount.getDiscountCode());
					statement.setDouble(4, transDiscount.getDiscountAmount());

					if (transDiscount.getDiscountReason() != null) {
						statement.setString(5, transDiscount.getDiscountReason());
					} else {
						statement.setString(5, "");
					}

					if (transDiscount.getDiscountDescription() != null) {
						statement.setString(6, transDiscount.getDiscountDescription());
					} else {
						statement.setString(6, "");
					}

					if (transDiscount.getDiscountPeriod() != null) {
						statement.setString(7, transDiscount.getDiscountPeriod().toString());
					} else {
						statement.setString(7, "");
					}

					if (statement.executeUpdate() != 1) {
						throw new RemoteException("Failed to insert MembershipTransactionDiscount");
					}

					LogUtil.debug(this.getClass(), "transDiscount=" + transDiscount);

					// discount attributes
					Collection discountAttributeList = transDiscount.getDiscountAttributeList(null, null);

					if (discountAttributeList != null && !discountAttributeList.isEmpty()) {
						Iterator discAttrIt = discountAttributeList.iterator();
						while (discAttrIt.hasNext()) {
							TransactionFeeDiscountAttribute txFeeDiscAttr = (TransactionFeeDiscountAttribute) discAttrIt.next();

							/** @todo set the transactionId */
							txFeeDiscAttr.getTransactionFeeDiscountAttributePK().setTransactionID(transactionID);

							LogUtil.debug(this.getClass(), "txFeeDiscAttr=" + txFeeDiscAttr.getTransactionFeeDiscountAttributePK().getFeeSpecificationID());
							LogUtil.debug(this.getClass(), "txFeeDiscAttr=" + txFeeDiscAttr.getTransactionFeeDiscountAttributePK().getDiscountTypeCode());
							LogUtil.debug(this.getClass(), "txFeeDiscAttr=" + txFeeDiscAttr.getTransactionFeeDiscountAttributePK().getTransactionID());
							LogUtil.debug(this.getClass(), "txFeeDiscAttr=" + txFeeDiscAttr.getTransactionFeeDiscountAttributePK().getAttributeName());

							// save discount attribute to the database

							hsession.save(txFeeDiscAttr);

							if (MembershipDiscountAttribute.ATTRIBUTE_GM_MEMBERSHIP_NUMBER.equals(txFeeDiscAttr.getTransactionFeeDiscountAttributePK().getAttributeName())) {

								// get the membership id given the membership
								// number
								MembershipVO GMMembership = null;
								String membershipNumber = txFeeDiscAttr.getAttributeValue();
								try {
									GMMembership = (MembershipVO) findMembershipByClientNumber(new Integer(membershipNumber)).iterator().next();
								} catch (Exception ex) {
									throw new RemoteException("Unable to get membership " + membershipNumber, ex);
								}

								// create a membership gift
								MembershipGiftPK memGiftPK = new MembershipGiftPK();
								memGiftPK.setMembershipId(GMMembership.getMembershipID());
								memGiftPK.setStatus(Gift.STATUS_GIFT_ACTIVE);

								final Gift GIFT_FREE_MEMBERSHIP = (Gift) CommonEJBHelper.getCommonMgr().getGiftListByType(Gift.TYPE_GIFT_FREE_MEMBERSHIP).iterator().next();
								memGiftPK.setGiftCode(GIFT_FREE_MEMBERSHIP.getGiftCode());
								memGiftPK.setTierCode(GMMembership.getRenewalMembershipTier().getTierCode());
								MembershipGift memGift = new MembershipGift();
								memGift.setMembershipGiftPK(memGiftPK);
								memGift.setCreateDate(new DateTime());
								MembershipVO newMembership = memTransVO.getNewMembership();
								memGift.setMembershipNumber(newMembership.getMembershipNumber());
								memGift.setUserId(userId);
								memGift.setSalesBranchCode(salesBranchCode);
								memGift.setNotes("");

								MembershipGift tmpGift = getMembershipGift(memGiftPK);
								// check if a gift already exists?
								if (tmpGift != null) {
									throw new ValidationException("Membership gift for " + tmpGift.getMembershipNumber() + " already exists against GM member " + GMMembership.getMembershipNumber() + ". Limit of 1 active gift type per member per tier. Delete or return gift attached to GM member.");
								}

								// create a gift record
								createMembershipGift(memGift);

							}

						}
					}

				}
			} catch (SQLException e) {
				throw new RemoteException("Error executing sql " + statementText, e);
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	private void removeTransactionFeeList(MembershipTransactionVO memTransVO) throws RemoteException {
		Connection connection = null;
		LogUtil.log(this.getClass(), "removeTransactionFeeList 1");
		Collection feeList = memTransVO.getTransactionFeeList();
		if (feeList != null && !feeList.isEmpty()) {
			LogUtil.log(this.getClass(), "removeTransactionFeeList 2");
			PreparedStatement statement = null;
			String statementText = "DELETE FROM PUB.mem_transaction_fee WHERE transaction_id = ?";
			try {
				connection = clientDataSource.getConnection();

				// Remove any transaction fee discounts first
				MembershipTransactionFee memTransFee = null;
				Iterator listIT = feeList.iterator();
				while (listIT.hasNext()) {
					memTransFee = (MembershipTransactionFee) listIT.next();
					removeTransactionFeeDiscountList(memTransVO, memTransFee.getFeeSpecificationID());
					memTransFee.clearDiscountList();
				}

				// Now remove the transaction fees
				statement = connection.prepareStatement(statementText);
				statement.setInt(1, memTransVO.getTransactionID().intValue());

				// Execute the delete. We don't care if no rows were deleted.
				statement.executeUpdate();

				// Clear the list.
				memTransVO.getTransactionFeeList().clear();
			} catch (SQLException e) {
				throw new RemoteException("Error executing SQL " + statementText + " : " + e.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	private void removeAdjustmentList(MembershipTransactionVO memTransVO) throws RemoteException {
		Connection connection = null;
		LogUtil.log(this.getClass(), "removeAdjustmentList 1");
		Collection adjustmentList = memTransVO.getAdjustmentList();
		if (adjustmentList != null && !adjustmentList.isEmpty()) {

			LogUtil.log(this.getClass(), "removeAdjustmentList 2");
			PreparedStatement statement = null;
			String statementText = "DELETE FROM PUB.mem_transaction_adjustment WHERE transaction_id = ?";
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement(statementText);
				statement.setInt(1, memTransVO.getTransactionID().intValue());
				// Execute the delete. We don't care if no rows were deleted.
				statement.executeUpdate();

				memTransVO.getAdjustmentList().clear();

			} catch (SQLException e) {
				throw new RemoteException("Error executing SQL " + statementText + " : " + e.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	/**
	 * Remove the transaction fee discounts for the specified fee on the transaction.
	 * 
	 * @param feeSpecID Description of the Parameter
	 * @param connection Description of the Parameter
	 * @exception RemoveException Description of the Exception
	 * @exception RemoteException Description of the Exception
	 */
	private void removeTransactionFeeDiscountList(MembershipTransactionVO memTransVO, Integer feeSpecID) throws RemoteException {
		Connection connection = null;
		LogUtil.log(this.getClass(), "removeTransactionFeeDiscountList 1");
		PreparedStatement statement = null;
		String statementText = "DELETE FROM PUB.mem_transaction_fee_discount WHERE transaction_id = ? and fee_specification_id = ?";
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			statement.setInt(1, memTransVO.getTransactionID().intValue());
			statement.setInt(2, feeSpecID.intValue());
			// Execute the delete. We don't care if no rows were deleted.
			statement.executeUpdate();

			/** @todo remove the discount attribute list */

		} catch (SQLException e) {
			throw new RemoteException("Error executing SQL " + statementText + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	public void updateMembershipTransaction(MembershipTransactionVO memTransVO) throws RemoteException {

		try {
			LogUtil.debug(this.getClass(), "updating membership transaction" + memTransVO.toString());
			hsession.update(memTransVO);
		} catch (HibernateException ex) {
			LogUtil.warn(this.getClass(), "Error updating membership transaction " + memTransVO + ". " + ex.getMessage());
			hsession.merge(memTransVO);
		}

		hsession.flush();

		// update cache
		CacheHandler.removeCacheValue(new Integer(memTransVO.getTransactionGroupID()), CACHE_GROUP_TRANSACTION_LIST);
		CacheHandler.removeCacheValue(memTransVO.getMembershipID(), CACHE_TRANSACTION_LIST);
	}

	public MembershipTransactionVO findTransactionByTransactionGroupAndMembership(Integer membershipId, Integer transactionGroupId) throws RemoteException {
		MembershipTransactionVO memTransVO = null;
		MembershipTransactionVO matchTransVO = null;
		Collection transactionList = findMembershipTransactionByTransactionGroupID(transactionGroupId.intValue());
		for (Iterator i = transactionList.iterator(); i.hasNext() && matchTransVO == null;) {
			memTransVO = (MembershipTransactionVO) i.next();
			if (memTransVO.getMembershipID().equals(membershipId)) {
				matchTransVO = memTransVO;
			}
		}
		return matchTransVO;
	}

	public void createMembershipTransaction(MembershipTransactionVO memTransVO) throws RemoteException {

		LogUtil.debug(this.getClass(), "createMembershipTransaction " + memTransVO.getTransactionDate().formatLongDate());

		// set the id - expose membership id
		MembershipIDMgr memIDMgr = MembershipEJBHelper.getMembershipIDMgr();
		int transactionID = memIDMgr.getNextTransactionID();
		memTransVO.setTransactionID(new Integer(transactionID));

		// Insert the new membership
		hsession.save(memTransVO);

		// Now insert the transaction lists.
		insertAdjustmentList(memTransVO.getTransactionID(), memTransVO.getAdjustmentList());
		insertTransactionFeeList(memTransVO);

		CacheHandler.removeCacheValue(new Integer(memTransVO.getTransactionGroupID()), CACHE_GROUP_TRANSACTION_LIST);
		CacheHandler.removeCacheValue(memTransVO.getMembershipID(), CACHE_TRANSACTION_LIST);
	}

	public void removeMembershipTransaction(MembershipTransactionVO memTransVO) throws RemoteException {

		hsession.delete(memTransVO);

		removeAdjustmentList(memTransVO);
		removeTransactionFeeList(memTransVO);

		CacheHandler.removeCacheValue(new Integer(memTransVO.getTransactionGroupID()), CACHE_GROUP_TRANSACTION_LIST);
		CacheHandler.removeCacheValue(memTransVO.getMembershipID(), CACHE_TRANSACTION_LIST);
	}

	/**
	 * Transferred amount owing from failed direct debit to receipting system.
	 * 
	 * @param clientNumber
	 * @throws RemoteException
	 * @throws PaymentException
	 * @throws RollBackException
	 */
	public void transferFailedDirectDebit(Integer clientNumber) throws RemoteException, PaymentException, RollBackException {
		transferFailedDirectDebit(clientNumber, null);
	}

	public void transferFailedDirectDebit(Integer clientNumber, User user) throws RemoteException, PaymentException, RollBackException {

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgrLocal();
		PaymentTransactionMgr paymentTxnMgr = PaymentEJBHelper.getPaymentTransactionMgrLocal();
		ClientMgr clientMgr = ClientEJBHelper.getClientMgrLocal();
		String salesBranchCode = null;
		if (user != null)
			salesBranchCode = user.getSalesBranchCode();

		List<Map<String, Object>> results = paymentMgr.getFailedDDSchedules(clientNumber, SourceSystem.MEMBERSHIP.getAbbreviation());

		if (results == null || results.isEmpty()) {
			LogUtil.log(getClass(), "No failed DD schedules found");
			return;
		}

		for (Map<String, Object> r : results) {
			Integer clientNo = (Integer) r.get("clientNo");
			Integer oiPayableSeq = (Integer) r.get("oiPayableSeq");
			Integer payableSeq = (Integer) r.get("payableSeq");
			Integer ddPayableSeq = (Integer) r.get("ddPayableSeq");
			Double amount = (Double) r.get("amt");

			LogUtil.log(getClass(), "clientNo: " + clientNo);
			LogUtil.log(getClass(), "oiPayableSeq: " + oiPayableSeq);
			LogUtil.log(getClass(), "payableSeq: " + payableSeq);
			LogUtil.log(getClass(), "ddPayableSeq: " + ddPayableSeq);
			LogUtil.log(getClass(), "amt: " + amount);

			MembershipVO memVO = this.findMembershipByClientAndType(clientNo, MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);

			PayableItemVO payableItemVO = paymentMgr.getPayableItem(payableSeq);
			if (payableItemVO == null) {
				LogUtil.warn(getClass(), "Unable to find payableItem by payableSeq: " + payableSeq);

				List<PayableItemVO> payables = (List<PayableItemVO>) paymentMgr.getPayableItems(PaymentMethod.DIRECT_DEBIT, ddPayableSeq.toString());

				LogUtil.log(getClass(), "Found " + payables.size() + " candidates for ddPayableSeq: " + ddPayableSeq);

				if (payables != null && !payables.isEmpty()) {
					payableItemVO = payables.get(0);
				}
			}

			LogUtil.log(getClass(), "productCode: " + payableItemVO.getProductCode());
			LogUtil.log(getClass(), "sequenceNo: " + payableItemVO.getPayableItemID());

			// 1. Ensure oi-payable os-amt is zero
			paymentTxnMgr.cancelPaymentMethod(payableItemVO.getPaymentMethod(), SourceSystem.MEMBERSHIP);
			LogUtil.log(getClass(), "1. cancelled amount outstanding");

			// 2. Create failed item as a pending item (using previous os-amt)
			// in Receptor
			/*
			 * String payMethodID = paymentTxnMgr.createPayableFee( clientNo, payableItemVO.getPayableItemID().toString(), payableItemVO.getProductCode().toUpperCase(), amount, "Transferred from failed direct debit", SourceSystem.MEMBERSHIP.getAbbreviation(), "HOB", "COM", null); LogUtil.log(getClass(), "2. created pending item in Receptor");
			 */
			PaymentMethod paymentMethod = new ReceiptingPaymentMethod(clientNo, SourceSystem.MEMBERSHIP);
			if (salesBranchCode == null)
				salesBranchCode = payableItemVO.getSalesBranchCode();
			PayableItemVO newPayableItemVO = new PayableItemVO(null, clientNo, paymentMethod, new ArrayList(), payableItemVO.getSourceSystem(), 1, salesBranchCode, 1, payableItemVO.getProductCode(), "COM", payableItemVO.getStartDate(), payableItemVO.getEndDate(), "Transferred from failed direct debit");
			newPayableItemVO.setAmountPayable(amount);
			newPayableItemVO.setAmountOutstanding(amount);
			newPayableItemVO.setUserID(user.getUserID());

			paymentTxnMgr.addPayableItem(newPayableItemVO);
			LogUtil.log(getClass(), "2. created new payable item");

			// 3. Update payable item to point to pending item and change
			// membership payment method to receipting
			/*
			 * payableItemVO.setPaymentMethodID(payMethodID); payableItemVO.setPaymentMethodDescription (PaymentMethod.RECEIPTING); payableItemVO.setPaymentSystem(null); payableItemVO.setSourceSystemReferenceID(1); paymentTxnMgr.updatePayableItem(payableItemVO); LogUtil.log(getClass(), "3. updated payable item");
			 */

			// link txn
			MembershipTransactionVO memTransVO = memVO.getLastTransaction();
			memTransVO.setPayableItemID(newPayableItemVO.getPayableItemID(), true);
			this.updateMembershipTransaction(memTransVO);
			LogUtil.log(getClass(), "3. update txn with new payable item");

			// 4. Clear payment_method_id from membership record
			memVO.setDirectDebitAuthorityID(null);
			this.updateMembership(memVO);
			LogUtil.log(getClass(), "4. cleared DD authority");

			// 5. Create client note to prevent use of DD
			ClientNote note = new ClientNote();
			// note.setBlockTransactions(true);
			note.setClientNumber(clientNo);
			note.setNoteType(NoteType.TYPE_INFORMATION);
			note.setCreated(new DateTime());
			note.setCreateId("COM");
			note.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
			note.setNoteText("Use of Direct Debit prohibited due to failed payments.");

			clientMgr.createClientNote(note);
			LogUtil.log(getClass(), "5. created blocker client note");

			try {
				paymentTxnMgr.commit();
			} catch (Exception e) {
				try {
					paymentTxnMgr.rollback();
				} catch (Exception ex) {
					LogUtil.warn(this.getClass(), e);
				}
				throw new SystemException("Unable to transfer failed direct debit", e);
			}
		}
	}

	private void insertAllowedFeesDiscountList(DiscountTypeVO discountType) throws RemoteException {
		Connection connection = null;
		Collection feeTypeList = discountType.getFeeTypeList();
		if (feeTypeList != null && !feeTypeList.isEmpty()) {
			PreparedStatement statement = null;
			String statementText = "INSERT INTO PUB.mem_allowed_fee_discount (fee_type_code, discount_type_code) VALUES (?, ?)";
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement(statementText);
				statement.setString(2, discountType.getDiscountTypeCode());

				FeeTypeVO feeTypeVO;
				Iterator it = feeTypeList.iterator();

				while (it.hasNext()) {
					feeTypeVO = (FeeTypeVO) it.next();
					statement.setString(1, feeTypeVO.getFeeTypeCode());

					if (statement.executeUpdate() != 1) {
						throw new EJBException("Failed to insert allowed discount '" + discountType.getDiscountTypeCode() + "', '" + feeTypeVO.getFeeTypeCode() + "'");
					}
				}
			} catch (SQLException sqle) {
				throw new EJBException("Failed to insert allowed discount list for discount type '" + discountType.getDiscountTypeCode() + "'.");
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	private void removeAllowedFeeDiscountList(DiscountTypeVO discountType) throws RemoteException {
		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = "DELETE FROM PUB.mem_allowed_fee_discount WHERE discount_type_code = ?";
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			statement.setString(1, discountType.getDiscountTypeCode());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new RemoteException("Error executing SQL " + statementText + " : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	/**
	 * Remove all product benefits associated with the product.
	 */
	private void removeProductBenefitList(ProductVO product) {
		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = null;
		try {
			connection = clientDataSource.getConnection();
			statementText = "DELETE FROM PUB.mem_product_benefit WHERE product_code = ?";
			statement = connection.prepareStatement(statementText);
			statement.setString(1, product.getProductCode());
			statement.executeUpdate(); // Don't care if no rows were deleted.
		} catch (SQLException e) {
			throw new EJBException("Error executing SQL " + statementText + ". productCode = '" + product.getProductCode() + "' : " + e.toString());
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	/**
	 * Insert the association between products and product benefits. The sort order will be set to whatever the order of benefits is in the list.
	 */
	private void insertProductBenefitList(ProductVO product) throws RemoteException {
		Connection connection = null;
		Collection productBenefitList = product.getProductBenefitList();
		if (productBenefitList != null && productBenefitList.size() > 0) {

			PreparedStatement statement = null;
			String statementText = null;
			String benefitCode = null;
			try {
				connection = clientDataSource.getConnection();
				statementText = "INSERT INTO PUB.mem_product_benefit (product_code, product_benefit_code, sort_order) VALUES (?, ?, ?)";
				int count = 1;
				Iterator it = productBenefitList.iterator();
				while (it.hasNext()) {
					ProductBenefitTypeVO pbtVO = (ProductBenefitTypeVO) it.next();
					benefitCode = pbtVO.getProductBenefitCode();

					statement = connection.prepareStatement(statementText);
					statement.setString(1, product.getProductCode());
					statement.setString(2, benefitCode);
					statement.setInt(3, count++);
					if (statement.executeUpdate() != 1) {
						throw new EJBException("Error adding row to mem_product_benefit for key = " + product.getProductCode() + "," + benefitCode);
					}
					ConnectionUtil.closeStatement(statement);
				}
			} catch (SQLException e) {
				throw new RemoteException("Error executing SQL INSERT INTO PUB.mem_product_benefit (product_code, product_benefit_code) VALUES ('" + product.getProductCode() + "','" + benefitCode + "' : " + e.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
	}

	private void removeProfileDiscountList(String profileCode) throws RemoteException {
		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = "DELETE FROM PUB.mem_profile_discount WHERE profile_code = ?";
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			statement.setString(1, profileCode);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new RemoteException("Error removing profile discount list.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	private void insertProfileDiscountList(String profileCode, Collection discountList) throws RemoteException {

		Connection connection = null;
		if (discountList != null && !discountList.isEmpty()) {
			PreparedStatement statement = null;
			String statementText = "INSERT INTO PUB.mem_profile_discount (profile_code, discount_type_code) VALUES (?, ?)";
			try {
				connection = clientDataSource.getConnection();
				statement = connection.prepareStatement(statementText);
				statement.setString(1, profileCode);
				DiscountTypeVO discTypeVO;
				Iterator profileDiscountIterator = discountList.iterator();
				while (profileDiscountIterator.hasNext()) {
					discTypeVO = (DiscountTypeVO) profileDiscountIterator.next();
					statement.setString(2, discTypeVO.getDiscountTypeCode());

					if (statement.executeUpdate() != 1) {
						throw new EJBException("Failed to insert membership profile discount '" + profileCode + "', '" + discTypeVO.getDiscountTypeCode() + "'");
					}
				}
			} catch (SQLException sqle) {
				throw new RemoteException("Failed to insert discount list for membership profile '" + profileCode + "'.", sqle);
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}

		}
	}

	public Collection getProfileDiscountList(String profileCode) throws RemoteException {
		Collection discountList = new ArrayList();
		Connection connection = null;
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		DiscountTypeVO discTypeVO;
		PreparedStatement statement = null;
		String statementText = "SELECT discount_type_code FROM PUB.mem_profile_discount WHERE profile_code = ?";
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(statementText);
			statement.setString(1, profileCode);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				discTypeVO = refMgr.getDiscountType(resultSet.getString(1));
				if (discTypeVO != null) {
					discountList.add(discTypeVO);
				}
			}
		} catch (SQLException e) {
			throw new RemoteException("Error getting profile discounts.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return discountList;
	}

	private static final String SQL_DELETE_GROUPS_OF_ONE = "delete from pub.mem_membership_group where group_id in (" + "\nselect group_id from pub.mem_membership mm, pub.mem_membership_group mg" + "\nwhere mm.membership_id = mg.membership_id" + "\nand exists (SELECT     group_Id, COUNT(membership_id) AS Expr1" + "\nFROM         PUB.mem_membership_group mg2" + "\nwhere mg2.group_id = mg.group_id" + "\nGROUP BY group_Id" + "\nHAVING      (COUNT(membership_id) = 1))" + "\n)";

	/**
	 * Delete orphaned groups.
	 * 
	 * @throws RemoteException
	 */
	public void deleteGroupsOfOne() throws RemoteException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(SQL_DELETE_GROUPS_OF_ONE);
			statement.execute();
		} catch (SQLException e) {
			throw new RemoteException("Error deleting groups of one.", e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	public List<MembershipVO> getMembershipsByProduct(String productCode) throws RemoteException {
		List<MembershipVO> membershipList = null;
		try {

			Criteria crit = hsession.createCriteria(MembershipVO.class).add(Expression.eq("productCode", productCode));
			membershipList = new ArrayList<MembershipVO>(crit.list());
		} catch (HibernateException ex) {
			throw new RemoteException("Unable to locate session factory.", ex);
		}
		return membershipList;
	}

	private class AccessMembership implements Serializable {
		private Integer clientNumber;
		private Integer membershipId;
		private String eligibilitySource;

		public AccessMembership(Integer clientNumber, Integer membershipId, String eligibilitySource) {
			this.clientNumber = clientNumber;
			this.membershipId = membershipId;
			this.eligibilitySource = eligibilitySource;
		}

		public Integer getClientNumber() {
			return this.clientNumber;
		}

		public String getEligibilitySource() {
			return this.eligibilitySource;
		}

		public String getJoinReasonCode() {
			return this.eligibilitySource;
		}

		public Integer getMembershipId() {
			return this.membershipId;
		}

	}

	/**
	 * Returns array of Roadside objects that have been created or modified between the start and end dates (inclusive). Excludes Access based on system configuration.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws SystemException
	 */
	public Long getMembershipChangesCount(DateTime startDate, DateTime endDate) throws RemoteException {

		String excludeAccessFromTsync = "false";
		try {
			excludeAccessFromTsync = FileUtil.getProperty("master", "excludeAccessFromTsync");
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine exclude Access state for TSYNC, defaulting to: " + excludeAccessFromTsync);
		}
		Boolean excludeAccess = Boolean.parseBoolean(excludeAccessFromTsync);

		String querySql = "select count(membershipID) from MembershipVO where lastUpdate >= :start and lastUpdate <= :end";
		if (excludeAccess) {
			querySql += " and productCode <> :access";
		}

		Query query = em.createQuery(querySql);
		query.setParameter("start", startDate);
		query.setParameter("end", endDate);

		if (excludeAccess) {
			query.setParameter("access", ProductVO.PRODUCT_ACCESS);
		}

		LogUtil.debug(getClass(), querySql);

		Long count = (Long) query.getSingleResult();

		return count;
	}

	/**
	 * Returns the number of roadside product changes between the start and end times. This will be used by an overnight process to ensure all reported changes have made the transition to the MRM. Excludes Access based on system configuration.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws SystemException
	 */
	public Boolean isMembershipChangesExist(DateTime startDate, DateTime endDate) throws RemoteException {
		return (getMembershipChangesCount(startDate, endDate) > 0);
	}

	/**
	 * Return the identifiers of all roadside products changed during the date range. Used in an overnight process in conjunction with the @getMembershipChangesCount web method. If a missing roadside product is found, force a sync on that roadside product. Excludes Access based on system configuration.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws SystemException
	 */
	public Collection<Integer> getMembershipChangesIdentifiers(DateTime startDate, DateTime endDate) throws RemoteException {

		String excludeAccessFromTsync = "false";
		try {
			excludeAccessFromTsync = FileUtil.getProperty("master", "excludeAccessFromTsync");
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine exclude Access state for TSYNC, defaulting to: " + excludeAccessFromTsync);
		}
		Boolean excludeAccess = Boolean.parseBoolean(excludeAccessFromTsync);

		String querySql = "select membershipID from MembershipVO where lastUpdate >= :start and lastUpdate <= :end";
		if (excludeAccess) {
			querySql += " and productCode <> :access";
		}
		Query query = em.createQuery(querySql);

		query.setParameter("start", startDate);
		query.setParameter("end", endDate);

		if (excludeAccess) {
			query.setParameter("access", ProductVO.PRODUCT_ACCESS);
		}

		LogUtil.debug(getClass(), querySql);

		Collection<Integer> idList = query.getResultList();

		return idList;
	}

	/**
	 * Returns array of Roadside objects that have been created or modified between the start and end dates (inclusive). Excludes Access based on system configuration.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws SystemException
	 */
	public Collection<MembershipVO> getMembershipChanges(DateTime startDate, DateTime endDate) throws RemoteException {

		String excludeAccessFromTsync = "false";
		try {
			excludeAccessFromTsync = FileUtil.getProperty("master", "excludeAccessFromTsync");
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine exclude Access state for TSYNC, defaulting to: " + excludeAccessFromTsync);
		}
		Boolean excludeAccess = Boolean.parseBoolean(excludeAccessFromTsync);

		String querySql = "from MembershipVO where lastUpdate >= :start and lastUpdate <= :end";
		if (excludeAccess) {
			querySql += " and productCode <> :access";
		}

		Query query = em.createQuery(querySql);
		query.setParameter("start", startDate);
		query.setParameter("end", endDate);

		if (excludeAccess) {
			query.setParameter("access", ProductVO.PRODUCT_ACCESS);
		}

		LogUtil.debug(getClass(), querySql);

		Collection<MembershipVO> membershipList = query.getResultList();

		return membershipList;
	}

	/**
	 * Get a collection of membershipVO objects for a membership group. Only provides basic membership data Group is returned given any group member Also sets prime addressee in MembershipVO object This code is a bit primitive (does not use EJB3 data ql) as a 3 way join is required with output from two tables
	 * 
	 * @param Integer membership Id
	 * @return ArrayList<MembershipVO>
	 * @throws RemoteException
	 */

	public ArrayList<MembershipVO> getMembershipGroup(Integer memId) throws RemoteException {
		ArrayList<MembershipVO> thisGroup = new ArrayList<MembershipVO>();
		String qString = "SELECT mem.membership_id," + " mem.membership_type_code," + " mem.membership_number," + " mem.client_number," + " mem.product_code," + " mem.product_date," + " mem.join_date," + " mem.commence_date," + " mem.expiry_date," + " mem.vehicle_count," + " mem.vehicle_expiry_type," + " mem.membership_status," + " mem.membership_term," + " mem.profile_code," + " mem.next_product_code," + " mem.affiliated_club_code," + " mem.affiliated_club_id," + " mem.affiliated_club_expiry_date," + " mem.credit_period," + " mem.credit_amount," + " mem.payment_method_id," + " mem.allow_to_lapse," + " mem.membership_value," + " mem.suspense_amount," + " mem.affiliated_club_credit_amount," + " mem.allow_to_lapse_reason_code," + " mem.affliated_club_product_level," + " mem.inceptionSalesBranch," + " mem.transactionReference," + " mem.roadside_create_date," + " mem.rego," + " mem.vin," + " mem.make," + " mem.model," + " mem.year," + " mem.lastUpdate," + " group2.is_Prime_Addressee" + " FROM  PUB.mem_membership_group AS group1 INNER JOIN" + " PUB.mem_membership_group AS group2 ON group1.group_Id = group2.group_Id INNER JOIN" + " PUB.mem_membership AS mem ON group2.membership_id = mem.membership_id" + " WHERE (group1.membership_id = ?)";
		Connection connection = null;
		PreparedStatement statement = null;
		MembershipVO mem = null;
		try {
			connection = clientDataSource.getConnection();
			statement = connection.prepareStatement(qString);
			statement.setInt(1, memId.intValue());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				mem = new MembershipVO();
				mem.setMembershipID(resultSet.getInt(1));
				mem.setMembershipTypeCode(resultSet.getString(2));
				mem.setMembershipNumber(resultSet.getString(3));
				mem.setClientNumber(resultSet.getInt(4));
				mem.setProductCode(resultSet.getString(5));
				mem.setProductEffectiveDate(setDateToField(resultSet.getDate(6)));
				mem.setJoinDate(setDateToField(resultSet.getDate(7)));
				mem.setCommenceDate(setDateToField(resultSet.getDate(8)));
				mem.setExpiryDate(setDateToField(resultSet.getDate(9)));
				mem.setVehicleCount(resultSet.getInt(10));
				mem.setVehicleExpiryType(resultSet.getString(11));
				mem.setStatus(resultSet.getString(12));
				String temp = resultSet.getString(13);
				if (temp != null && !temp.equals("")) {
					mem.setMembershipTerm(new Interval(temp));
				}
				mem.setMembershipProfileCode(resultSet.getString(14));
				mem.setNextProductCode(resultSet.getString(15));
				mem.setAffiliatedClubCode(resultSet.getString(16));
				mem.setAffiliatedClubId(resultSet.getString(17));

				mem.setAffiliatedClubExpiryDate(setDateToField(resultSet.getDate(18)));
				temp = resultSet.getString(19);
				if (temp != null && !temp.equals("")) {
					mem.setCreditPeriod(new Interval(temp));
				}
				mem.setCreditAmount(resultSet.getDouble(20));
				mem.setDirectDebitAuthorityID(resultSet.getInt(21));
				mem.setAllowToLapse(resultSet.getBoolean(22));
				mem.setMembershipValue(resultSet.getBigDecimal(23));
				// mem.setSuspenseAmount(resultSet.getBigDecimal(24));
				// mem.setAffiliatedClubCreditAmount(resultSet.getBigDecimal(25));
				mem.setAllowToLapseReasonCode(resultSet.getString(26));
				// mem.setAffliatedClubProductLevel(resultSet.getString(27));
				mem.setInceptionSalesBranch(resultSet.getString(28));
				mem.setTransactionReference(resultSet.getString(29));
				mem.setRoadsideCreateDate(setDateToField(resultSet.getDate(30)));
				mem.setRego(resultSet.getString(31));
				mem.setVin(resultSet.getString(32));
				mem.setMake(resultSet.getString(33));
				mem.setModel(resultSet.getString(34));
				mem.setYear(resultSet.getString(35));
				mem.setLastUpdate(setDateToField(resultSet.getDate(36)));
				mem.setPrimeAddressee(resultSet.getBoolean(37));
				thisGroup.add(mem);
			}
			if (thisGroup.size() == 0) {
				// is actually an individual membership
				mem = getMembership(memId);
				thisGroup.add(mem);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RemoteException(ex + "");
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}

		return thisGroup;
	}

	private DateTime setDateToField(Date dt) {
		if (dt != null)
			return new DateTime(dt);
		else
			return null;
	}

	public int getMembershipYears(DateTime effectiveDate, DateTime joinDate) {
		int years = 0;
		if (joinDate != null && effectiveDate != null) {
			if (joinDate.onOrBeforeDay(effectiveDate)) {
				years = DateUtil.getYears(joinDate, effectiveDate) + 1;
			}
		}
		return years;
	}
}