package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.CacheBase;

public class MembershipProfileCache extends CacheBase {
    private static MembershipProfileCache instance = new MembershipProfileCache();

    private MembershipProfileVO defaultProfile = null;

    /**
     * Implement the getItemList method of the super class. Return the list of
     * cached items. Add items to the list if it is empty.
     */
    public void initialiseList() throws RemoteException {
	// log("Initialising MembershipProfile cache.");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	Collection dataList = null;

	try {
	    dataList = membershipMgr.getMembershipProfiles();
	    if (dataList != null) {
		Iterator<MembershipProfileVO> it = dataList.iterator();
		while (it.hasNext()) {
		    MembershipProfileVO item = it.next();

		    item.setReadOnly(true);

		    if (MembershipProfileVO.PROFILE_CLIENT.equals(item.getProfileCode())) {
			this.defaultProfile = item;
		    }

		    add(item);
		}
	    }
	} catch (Exception e) {
	    System.out.println("Failed to get list of membership profiles:\n");
	    e.printStackTrace();
	}
    }

    /**
     * Return a reference the instance of this class.
     */
    public static MembershipProfileCache getInstance() {
	return instance;
    }

    /**
     * Return the specified membership profile value object.
     */
    public MembershipProfileVO getMembershipProfile(String profileCode) throws RemoteException {
	return (MembershipProfileVO) this.getItem(profileCode);
    }

    /**
     * Return the specified membership profile value object.
     */
    public MembershipProfileVO getDefaultMembershipProfile() throws RemoteException {
	// Make sure that the list has been initialised.
	Collection list = this.getItemList();
	return this.defaultProfile;
    }

    /**
     * Return the list of membership profiles. A null or an empty list may be
     * returned.
     */
    public ArrayList getMembershipProfileList() throws RemoteException {
	return this.getItemList();
    }

}