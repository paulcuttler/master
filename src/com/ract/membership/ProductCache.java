package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.CacheBase;
import com.ract.util.LogUtil;

/**
 * This class caches a list of products in memory. It uses the Singleton design
 * pattern. Use the following code to call the methods on this class.
 * 
 * ProductCache.getInstance().getProduct(<membershipTypeCode>);
 * ProductCache.getInstance().getProductList();
 */

public class ProductCache extends CacheBase {
    private static ProductCache instance = new ProductCache();

    /**
     * Implement the getItemList method of the super class. Return the list of
     * cached items. Add items to the list if it is empty.
     */
    public void initialiseList() throws RemoteException {
	LogUtil.log(this.getClass(), "Initialising Product cache.");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection dataList = null;

	try {
	    dataList = membershipMgr.getProducts();
	    if (dataList != null) {
		Iterator it = dataList.iterator();
		while (it.hasNext()) {
		    ProductVO item = (ProductVO) it.next();
		    add(item);
		}
	    }
	    sort();
	    LogUtil.log(this.getClass(), "Loaded " + getListSize() + " product types.");
	} catch (Exception e) {
	    System.out.println("Failed to get list of products:\n");
	    e.printStackTrace();
	}
    }

    /**
     * Return a reference the instance of this class.
     */
    public static ProductCache getInstance() {
	return instance;
    }

    /**
     * Return the specified product value object. Search the list of products
     * for one that has the same code. Use the getList() method to make sure the
     * list has been initialised.
     */
    public ProductVO getProduct(String productCode) throws RemoteException {
	return (ProductVO) getItem(productCode);
    }

    /**
     * Return the list of products. Use the getList() method to make sure the
     * list has been initialised. A null or an empty list may be returned.
     */
    public ArrayList getProductList() throws RemoteException {
	return getItemList();
    }

}
