package com.ract.membership;

/**
 * Title:        TransactionContainer
 * Description:  Describes the interface to a container for a transaction.
 *               The transaction container holds all of the transaction data
 *               between pages displayed to the user.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author       T. Bakker
 * @version      1.0
 * @see TransactionGroup, UndoTransactionGroup
 */

import com.ract.util.DateTime;
import com.ract.util.DateUtil;

public abstract class TransactionContainer {
    /**
     * The name of the transaction group key to use in hidden fields on JSPs
     */
    public static final String KEY = "transactionGroupKey";

    /**
     * The value of the transaction group key given to this instance of the
     * transaction container.
     */
    private String transactionGroupKey = DateUtil.formatDate(new DateTime(), "MMddHHmmssSS");

    /**
     * Indicates if the transaction container has been submitted to the
     * transaction manager. The UIC checks this flag to make sure that the
     * transaction container is not submitted multiple times.
     */
    protected boolean submitted = false;

    public String getTransactionGroupKey() {
	return this.transactionGroupKey;
    }

    public boolean isSubmitted() {
	return this.submitted;
    }
}