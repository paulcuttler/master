package com.ract.membership;

/**
 * Title:        MembershipSessionUtil
 * Description:  Holds methods used putting objects int the session and
 *               retrieveing them back out
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author
 * @version 1.0
 */

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.ract.common.SystemException;
import com.ract.membership.ui.MembershipUIConstants;

public class MembershipSessionUtil {

    /**
     * Add the transaction container to the transaction container list in the
     * session. Throw an exception if the transaction container is already in
     * the session. Also throw an exception if the specified transaction
     * container is null.
     */
    public static void addTransactionContainer(HttpServletRequest request, TransactionContainer transContainer) throws SystemException {
	// Check for null object to add.
	if (transContainer == null) {
	    throw new SystemException("Cannot add a null transaction container to the transaction container list!");
	}

	// Make sure the object is not already in the list.
	String containerKey = transContainer.getTransactionGroupKey();
	Hashtable containerList = getTransactionContainerList(request);
	if (containerList.containsKey(containerKey)) {
	    Collection transList = containerList.values();
	    StringBuffer sb = new StringBuffer("The transaction container identified by transaction container key '" + containerKey + "' cannot be added to the transaction container list. It already exists. Existing keys are : ");
	    Iterator listIterator = transList.iterator();
	    while (listIterator.hasNext()) {
		TransactionContainer tmpContainer = (TransactionContainer) listIterator.next();
		sb.append(" ,");
		sb.append(tmpContainer.getTransactionGroupKey());
	    }
	    throw new SystemException(sb.toString());
	}

	// Add the object to the list.
	containerList.put(containerKey, transContainer);
    }

    /**
     * Get the list of transaction groups (TransactionGroup) from the session.
     * An operator may be processing more than one transaction in the session.
     * Initialise the list if this is the first call.
     */
    public static Hashtable getTransactionContainerList(HttpServletRequest request) {
	Hashtable containerList = (Hashtable) request.getSession().getAttribute(MembershipUIConstants.SESSION_TRANSACTION_CONTAINER_LIST);
	if (containerList == null) {
	    containerList = new Hashtable();
	    request.getSession().setAttribute(MembershipUIConstants.SESSION_TRANSACTION_CONTAINER_LIST, containerList);
	}
	return containerList;
    }

    /**
     * Return the transaction group from the session. Assumes a parameter is
     * passed on the request called "transactionGroupID".
     */
    public static TransactionGroup getTransactionGroup(HttpServletRequest request) throws SystemException {
	String transGroupKeyText = request.getParameter(TransactionGroup.KEY);
	return getTransactionGroup(request, transGroupKeyText);
    }

    /**
     * Return the specified transaction group from the transaction group list
     * held in the session. Throw an exception if the transaction cannot be
     * found in the list.
     */
    public static TransactionGroup getTransactionGroup(HttpServletRequest request, String transGroupKey) throws SystemException {
	TransactionGroup transGroup = null;
	TransactionContainer transContainer = getTransactionContainer(request, transGroupKey);
	if (transContainer instanceof TransactionGroup) {
	    transGroup = (TransactionGroup) transContainer;
	} else {
	    throw new SystemException("The transaction container with key " + transGroupKey + " is not a TransactionGroup type of container.");
	}
	return transGroup;
    }

    /**
     * Return the specified undo transaction group from the transaction group
     * list held in the session. Throw an exception if the transaction cannot be
     * found in the list.
     */
    public static UndoTransactionGroup getUndoTransactionGroup(HttpServletRequest request) throws SystemException {
	UndoTransactionGroup transGroup = null;
	TransactionContainer transContainer = getTransactionContainer(request);
	if (transContainer instanceof UndoTransactionGroup) {
	    transGroup = (UndoTransactionGroup) transContainer;
	} else {
	    throw new SystemException("The transaction container is not an UndoTransactionGroup type of container.");
	}

	return transGroup;
    }

    public static TransactionContainer getTransactionContainer(HttpServletRequest request) throws SystemException {
	String transGroupKeyText = request.getParameter(TransactionContainer.KEY);
	return getTransactionContainer(request, transGroupKeyText);
    }

    public static TransactionContainer getTransactionContainer(HttpServletRequest request, String containerKey) throws SystemException {
	TransactionContainer transContainer = null;
	if (containerKey != null) {
	    // Get the transaction list from the session
	    Hashtable containerList = getTransactionContainerList(request);

	    // Find the specified transaction in the list
	    transContainer = (TransactionContainer) containerList.get(containerKey);
	}

	// Chuck a wobbly if the transaction could not be found. It should be
	// there.
	if (transContainer == null) {
	    SystemException se = new SystemException("The transaction container with key " + containerKey + " no longer exists in the session.");
	    throw new SystemException("The session has timed out.  Please start the transaction again.", se);
	}

	return transContainer;
    }

    /**
     * Remove the transaction group with the specified ID from the transaction
     * group list held in the session. Do nothing if the transaction group
     * cannot be found.
     */
    public static void removeTransactionContainer(HttpServletRequest request, String transGroupKey) throws SystemException {
	if (transGroupKey != null) {
	    // Get the transaction list from the session
	    Hashtable containerList = getTransactionContainerList(request);

	    // Remove the transaction container from the list
	    containerList.remove(transGroupKey);
	}
    }

}
