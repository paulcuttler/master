package com.ract.membership;

import java.io.FileWriter;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.common.SystemParameterVO;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;

/**
 * Auto cancel the following memberships: - client deceased - expired since
 * interval - expired and allow to lapse
 * 
 * Typically run daily.
 * 
 * @author jyh,gzn
 * @version 1.0
 */

public class MembershipAutoCancelJob implements Job {

    public MembershipAutoCancelJob() {
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {

	try {
	    CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();

	    String outputDirectoryName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_LOG_DIR);
	    if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') {
		outputDirectoryName += "/";
	    }

	    String fileName = outputDirectoryName + DateUtil.formatYYYYMMDD(new DateTime()) + "autocancel.csv";

	    MembershipMgr membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
	    LogUtil.log(getClass(), "Membership Auto Cancel started.");

	    FileWriter fileWriter = new FileWriter(fileName, true); // append
	    fileWriter.write("Client number,Reason\n");

	    try {
		List<Integer> cancelledDeceasedMemberships = membershipMgrLocal.autoCancelDeceased();
		for (Integer clientNumber : cancelledDeceasedMemberships) {
		    fileWriter.write(clientNumber.toString() + ",Deceased\n");
		}
	    } catch (Exception e) {
		LogUtil.log(getClass(), "An exception occurred processing auto-cancels for Deceased memberships: " + e);
	    }

	    try {
		List<Integer> cancelledExpiredMemberships = membershipMgrLocal.autoCancelExpired();
		for (Integer clientNumber : cancelledExpiredMemberships) {
		    fileWriter.write(clientNumber.toString() + ",Expired\n");
		}
	    } catch (Exception e) {
		LogUtil.log(getClass(), "An exception occurred processing auto-cancels for Expired memberships: " + e);
	    }

	    try {
		List<Integer> cancelledAllowToLapseMemberships = membershipMgrLocal.autoCancelAllowToLapse();
		for (Integer clientNumber : cancelledAllowToLapseMemberships) {
		    fileWriter.write(clientNumber.toString() + ",Allow to lapse\n");
		}
	    } catch (Exception e) {
		LogUtil.log(getClass(), "An exception occurred processing auto-cancels for Allow to lapse memberships: " + e);
	    }

	    fileWriter.flush();
	    fileWriter.close();

	    try {
		membershipMgrLocal.deleteGroupsOfOne();
	    } catch (Exception e) {
		LogUtil.fatal(getClass(), "An exception occurred deleting groups of one: " + e);
	    }

	    LogUtil.log(getClass(), "Membership Auto Cancel finished.");
	} catch (Exception ex) {
	    LogUtil.fatal(getClass(), "Membership Auto Cancel failed, see following message");
	    System.err.println(ExceptionHelper.getExceptionStackTrace(ex));
	    throw new JobExecutionException(ex);
	}
    }
}
