package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Iterator;

import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * 
 * <p>
 * A transaction group implementation for an affiliated club.
 * </p>
 * 
 * @author Leigh Giles, extensively modified by JYH
 * @version 1.0
 */

public class TransactionGroupAffiliatedClubTransferImpl extends TransactionGroup {

    // private MembershipVO oldMemVO;

    private String affiliatedClubCode;

    private String affiliatedClubMemberId;

    private DateTime affiliatedClubJoinDate;

    private DateTime affiliatedClubExpiryDate;

    private String affiliatedProductLevel;

    public TransactionGroupAffiliatedClubTransferImpl(User user) throws RemoteException {
	super(MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER, user);

	// required???
	this.setTransactionDate(new DateTime().getDateOnly());

	// set the default reason
	ReferenceDataVO reasonVO = ReferenceDataVO.TRANSFER_FINANCIAL;
	if (reasonVO == null) {
	    throw new SystemException("Failed to get join reason '" + reasonVO.getDescription() + "'");
	}
	this.setTransactionReason(reasonVO);

    }

    public void createTransactionFees() throws RemoteException {
	// note: processing at this level creates a redundant looping through
	// the transactions.
	// there will only be one transaction though!!!
	MembershipTransactionVO membershipTransactionVO = null;
	Iterator transListIt = this.getTransactionList().iterator();
	while (transListIt.hasNext()) {
	    membershipTransactionVO = (MembershipTransactionVO) transListIt.next();
	    // no fees for transferring membership. add here if it is decided.
	    if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(this.getTransactionGroupTypeCode())) {
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(membershipTransactionVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(membershipTransactionVO.getTransactionTypeCode())) {
		    // call existing create fee
		    super.createTransactionFees();
		} else {
		    throw new SystemException("Only rejoin or create transactions are supported in the context of a transfer group transaction type.");
		}
	    }
	}
    }

    public void createTransactionsForSelectedClients() throws RemoteException, ValidationException {

	LogUtil.log(this.getClass(), "club=" + this.getAffiliatedClubCode());
	LogUtil.log(this.getClass(), "id=" + this.getAffiliatedClubMemberId());
	LogUtil.log(this.getClass(), "join=" + this.getAffiliatedClubJoinDate());
	LogUtil.log(this.getClass(), "expiry=" + this.getAffiliatedClubExpiryDate());

	this.doTransferTransaction();

    }

    /**
     * Transfer on existing membership
     * 
     * @param selectedClient
     *            SelectedClient
     * @throws RemoteException
     * @throws ValidationException
     * @return MembershipTransactionVO
     */
    private MembershipTransactionVO getTransferTransaction(SelectedClient selectedClient) throws RemoteException, ValidationException {
	LogUtil.log(this.getClass(), "getTransferTransaction");
	ClientVO clientVO = selectedClient.getClient();

	LogUtil.log(this.getClass(), "a selectedClient = " + selectedClient);
	MembershipVO oldMemVO = selectedClient.getMembership();
	LogUtil.log(this.getClass(), "b selectedClient = " + (oldMemVO == null));
	MembershipVO newMemVO = null;
	String memTypeCode = MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL;
	String transactionType = null;
	DateTime preferredCommenceDate = getPreferredCommenceDate(); // Defaults
								     // to the
								     // transaction
								     // date.

	// get trans date
	DateTime transDate = this.getTransactionDate();

	if (oldMemVO != null) {
	    // rejoin
	    newMemVO = oldMemVO.copy();
	    newMemVO.setReadOnly(false);
	    transactionType = MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN;
	} else {
	    newMemVO = new MembershipVO();
	    newMemVO.setReadOnly(false);
	    transactionType = MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE;
	}

	DateTime expiryDate = getAffiliatedClubExpiryDate();
	if (expiryDate.before(transDate)) {
	    expiryDate = transDate;
	}

	// set default attributes
	newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
	newMemVO.setAllowToLapse(Boolean.valueOf(false));

	newMemVO.setMembershipTypeCode(memTypeCode);
	newMemVO.setMembershipNumber(clientVO.getClientNumber().toString()); // Membership
									     // numbers
									     // are
									     // the
									     // same
									     // as
									     // client
									     // numbers
									     // for
									     // the
									     // moment
	newMemVO.setClient(clientVO);
	newMemVO.setMembershipTerm(this.getMembershipTerm());

	// update affiliated club details - setPreferredCommenceDate resets the
	// join date so needs to
	// happen here.
	setAffiliatedClubDetails(oldMemVO, newMemVO);

	newMemVO.setCommenceDate(preferredCommenceDate);
	newMemVO.setProductEffectiveDate(preferredCommenceDate);
	newMemVO.setExpiryDate(expiryDate);
	newMemVO.setNextProductCode(null);

	if (this.membershipProfileVO != null) {
	    newMemVO.setMembershipProfile(this.membershipProfileVO);
	} else {
	    newMemVO.setMembershipProfile(MembershipEJBHelper.getMembershipRefMgr().getDefaultMembershipProfile());

	}
	if (this.selectedProduct != null) {
	    newMemVO.setProduct(this.selectedProduct);
	}

	newMemVO.setReadOnly(true);

	LogUtil.log(this.getClass(), "2 get");
	// Create a new transaction object for the membership transaction
	MembershipTransactionVO memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.getTransactionGroupTypeCode(), transactionType, selectedClient.getMembershipGroupJoinDate());

	memTransVO.setTransactionDate(this.getTransactionDate());
	memTransVO.setMembership(oldMemVO);
	memTransVO.setNewMembership(newMemVO);
	memTransVO.setPreferredCommenceDate(preferredCommenceDate, false); // dates
									   // already
									   // set

	memTransVO.setUsername(this.getUserID());
	memTransVO.setSalesBranchCode(this.getSalesBranchCode());
	memTransVO.setProductEffectiveDate();

	memTransVO.setEffectiveDate(preferredCommenceDate);
	memTransVO.setEndDate(newMemVO.getExpiryDate());

	return memTransVO;

    }

    private void setAffiliatedClubDetails(MembershipVO oldMemVO, MembershipVO newMemVO) throws ValidationException, RemoteException {
	LogUtil.log(this.getClass(), "1 setAffiliatedClubDetails");
	// Add the affiliated club details to the membership VO
	newMemVO.setAffiliatedClubCode(this.getAffiliatedClubCode());
	newMemVO.setAffiliatedClubId(this.getAffiliatedClubMemberId());
	newMemVO.setAffiliatedClubExpiryDate(this.getAffiliatedClubExpiryDate());
	newMemVO.setAffiliatedClubProductLevel(this.getAffiliatedProductLevel());
	LogUtil.log(this.getClass(), "2 setAffiliatedClubDetails");
	// update join date of membership with club join date if before existing
	if (oldMemVO != null) {
	    LogUtil.log(this.getClass(), "3a setAffiliatedClubDetails");
	    DateTime oldJoinDate = oldMemVO.getJoinDate();
	    LogUtil.log(this.getClass(), "oldJoinDate=" + oldJoinDate);
	    // old join date is after the affiliated club join date so set it.
	    if (oldJoinDate.after(this.getAffiliatedClubJoinDate())) {
		newMemVO.setJoinDate(this.getAffiliatedClubJoinDate());
	    }
	} else {
	    LogUtil.log(this.getClass(), "3b setAffiliatedClubDetails");
	    // no membership so set it.
	    newMemVO.setJoinDate(this.getAffiliatedClubJoinDate());
	    newMemVO.setCommenceDate(this.getTransactionDate());
	}

	LogUtil.log(this.getClass(), "mem details = " + newMemVO.toString());
    }

    private void doTransferTransaction() throws RemoteException, ValidationException {
	MembershipTransactionVO memTransVO = null;
	SelectedClient selectedClient;
	if (this.getSelectedClientList().size() > 1) {
	    throw new SystemException("Only one client can be selected for transfer transaction.");
	}

	// Get the one client VO from the list.
	Enumeration keyList = this.getSelectedClientKeys();
	selectedClient = (SelectedClient) this.getSelectedClientMap().get(keyList.nextElement());
	LogUtil.log(this.getClass(), "selectedClient = " + selectedClient);
	memTransVO = getTransferTransaction(selectedClient);
	memTransVO.setPrimeAddressee(true);

	this.addMembershipTransaction(memTransVO);
    }

    public void setAffiliatedClubCode(String affiliatedClubCode) throws ValidationException {
	LogUtil.log(this.getClass(), "code=" + affiliatedClubCode);

	if (affiliatedClubCode == null || affiliatedClubCode.equals("")) {
	    throw new ValidationException("A club code must be entered");
	}
	this.affiliatedClubCode = affiliatedClubCode;
    }

    public DateTime getAffiliatedClubExpiryDate() {
	return this.affiliatedClubExpiryDate;
    }

    public void setAffiliatedClubExpiryDate(DateTime affiliatedClubExpiryDate) throws ValidationException {
	if (affiliatedClubExpiryDate == null || affiliatedClubExpiryDate.equals("")) {
	    throw new ValidationException("An affiliated club expiry date must be entered.");
	}

	DateTime now = new DateTime().getDateOnly();
	DateTime maxExpiryDate = null;
	try {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String maximumTransferRange = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MAX_TRANSFER_RANGE);
	    maxExpiryDate = now.add(new Interval(maximumTransferRange));
	} catch (Exception e) {
	    throw new ValidationException(e.getMessage());
	}
	if (affiliatedClubExpiryDate.after(maxExpiryDate)) {
	    throw new ValidationException("Affiliated expiry date may not be after " + maxExpiryDate.formatShortDate());
	}

	DateTime transferExpiryDate = null;
	Interval transferExpiredPeriod = null;
	try {

	    // get expired period
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String transferExpiredPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.TRANSFER_EXPIRED_PERIOD);
	    transferExpiredPeriod = new Interval(transferExpiredPeriodSpec);

	    // get expiry date
	    transferExpiryDate = new DateTime();
	    transferExpiryDate = transferExpiryDate.subtract(transferExpiredPeriod);
	    LogUtil.debug(this.getClass(), "transferExpiryDate-" + transferExpiryDate);

	} catch (Exception e) {
	    throw new ValidationException(e.getMessage());
	}

	if (affiliatedClubExpiryDate.before(transferExpiryDate)) {
	    throw new ValidationException("The transfer period has expired. " + transferExpiredPeriod.formatForDisplay(Interval.FORMAT_YEAR, Interval.FORMAT_MONTH));
	}

	LogUtil.log(this.getClass(), "affiliatedClubExpiryDate=" + affiliatedClubExpiryDate);
	this.affiliatedClubExpiryDate = affiliatedClubExpiryDate;

	this.setExpiryDate(affiliatedClubExpiryDate);
    }

    public DateTime getAffiliatedClubJoinDate() {
	return affiliatedClubJoinDate;
    }

    public String getAffiliatedProductLevel() {
	return affiliatedProductLevel;
    }

    public String getAffiliatedClubCode() {
	return affiliatedClubCode;
    }

    public String getAffiliatedClubMemberId() {
	return affiliatedClubMemberId;
    }

    public void setAffiliatedClubJoinDate(DateTime affiliatedClubJoinDate) throws RemoteException {
	if (affiliatedClubJoinDate == null || affiliatedClubJoinDate.equals("")) {
	    throw new ValidationException("An affiliated club join date must be entered.");
	}
	LogUtil.log(this.getClass(), "affiliatedClubJoinDate=" + affiliatedClubJoinDate);
	if (affiliatedClubJoinDate.after(this.getTransactionDate())) {
	    throw new SystemException("Affiliated join date '" + affiliatedClubJoinDate + "' may not be after today.");
	}
	this.affiliatedClubJoinDate = affiliatedClubJoinDate;
    }

    public void setAffiliatedClubMemberId(String affiliatedClubMemberId) throws ValidationException {
	if (affiliatedClubMemberId == null || affiliatedClubMemberId.equals("")) {
	    throw new ValidationException("A membership identifier must be entered.");
	}
	LogUtil.log(this.getClass(), "affiliatedClubMemberId=" + affiliatedClubMemberId);
	this.affiliatedClubMemberId = affiliatedClubMemberId;
    }

    public void setAffiliatedProductLevel(String affiliatedProductLevel) {
	this.affiliatedProductLevel = affiliatedProductLevel;
    }
}
