package com.ract.membership.club;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.membership.TransactionCredit;

/**
 * Affiliated club credit
 * 
 * @author jyh
 * @version 1.0
 */

public class AffiliatedClubCredit implements Serializable, TransactionCredit {

    private BigDecimal affiliatedClubCreditAmount;

    private String description;

    private Integer daysRemaining;

    public void setDescription(String description) {
	this.description = description;
    }

    public void setDaysRemaining(Integer daysRemaining) {
	this.daysRemaining = daysRemaining;
    }

    public String getDescription() {
	return description;
    }

    public Integer getDaysRemaining() {
	return daysRemaining;
    }

    public AffiliatedClubCredit(Integer daysRemaining, String description, BigDecimal amount) {
	this.setDescription(description);
	this.setTransactionCreditAmount(amount);
	this.setDaysRemaining(daysRemaining);
    }

    public AffiliatedClubCredit() {
	this.setTransactionCreditAmount(new BigDecimal(0));
	this.setDaysRemaining(new Integer(0));
    }

    public BigDecimal getTransactionCreditAmount() {
	return this.affiliatedClubCreditAmount;
    }

    public void setTransactionCreditAmount(BigDecimal amount) {
	this.affiliatedClubCreditAmount = amount;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.getDaysRemaining() + " days remaining,");
	desc.append(this.getTransactionCreditAmount() + " credit,");
	desc.append(this.getDescription() + ".");
	return desc.toString();
    }
}
