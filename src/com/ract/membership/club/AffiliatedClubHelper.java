package com.ract.membership.club;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import com.ract.common.admin.AdminCodeDescription;
import com.ract.membership.MembershipEJBHelper;

/**
 * Helper class for working with affiliated club.
 * 
 * @author Leigh Giles
 * @since 21 August 2003
 * @version 1.0
 */

public class AffiliatedClubHelper {

    /**
     * Finds all the affiliated clubs as code and descriptions. Sorts them into
     * code order
     * 
     * @return SortedSet
     */
    public static SortedSet getSortedClubList() {

	SortedSet s = new TreeSet();
	// Collection cl = null;
	try {
	    Collection cl = MembershipEJBHelper.getMembershipMgr().getClubList();
	    Iterator it = cl.iterator();

	    while (it.hasNext()) {
		AffiliatedClubVO afc = (AffiliatedClubVO) it.next();
		if (afc.isActive()) {
		    s.add(new AdminCodeDescription(afc.getClubCode(), afc.getDescription(), afc.getDescription()));
		}
	    }

	} catch (Exception e) {
	    // Do nothing - an empty set will be returned - a null set actually
	}

	return s;
    }

    /**
     * Get an affiliated club vo using the club code.
     * 
     * @param clubCode
     *            String
     * @throws RemoteException
     * @return AffiliatedClubVO
     */
    public static AffiliatedClubVO getAffiliatedClub(String clubCode) throws RemoteException {
	AffiliatedClubVO club = MembershipEJBHelper.getMembershipMgr().getClub(clubCode);
	return club;
    }

    public static final int DEFAULT_MEMBERSHIP_TERM_DAYS = 365;

    /**
     * Calculate the amount of credit entitled for an affiliated club
     * membership.
     * 
     * @param affiliatedClub
     *            AffiliatedClubVO
     * @param transactionDate
     *            DateTime
     * @param expiryDate
     *            DateTime
     * @throws SystemException
     * @return BigDecimal
     */
    // public static AffiliatedClubCredit
    // calculateAffiliatedClubCredit(AffiliatedClubVO affiliatedClub,
    // DateTime transactionDate, DateTime expiryDate)
    // throws SystemException
    // {
    // Integer daysRemaining = new Integer(DateUtil.getDateDiff(transactionDate,
    // expiryDate,
    // Calendar.DATE));
    // BigDecimal creditAmount = null;
    // boolean active = false;
    // try
    // {
    // active = MembershipHelper.isMembershipActive(expiryDate);
    // }
    // catch(RemoteException ex)
    // {
    // throw new SystemException(
    // "Unable to determine if expiry date allows membership to be active.",
    // ex);
    // }
    // if(active)
    // {
    // //in the 1 to 3 months expired period.
    // if(transactionDate.after(expiryDate))
    // {
    // creditAmount = new BigDecimal(0);
    // daysRemaining = new Integer(0);
    // }
    // else
    // {
    // LogUtil.log("calculateAffiliatedClubCreditAmount", "daysRemaining" +
    // daysRemaining);
    // //days in the membership term
    // daysRemaining = calculateMaximumTerm(daysRemaining);
    // LogUtil.log("calculateAffiliatedClubCreditAmount", "daysRemaining" +
    // daysRemaining);
    //
    // BigDecimal feePerDay = calculateAffiliatedClubFeePerDay(affiliatedClub);
    // LogUtil.log("calculateAffiliatedClubCreditAmount", "feePerDay" +
    // feePerDay);
    // //credit amount is equals to the club fee divided by the default term
    // multiplied by the days remaining
    // creditAmount = feePerDay.multiply(new
    // BigDecimal(daysRemaining.intValue()));
    // LogUtil.log("calculateAffiliatedClubCreditAmount", "creditAmount" +
    // creditAmount);
    // }
    // }
    // else
    // {
    // throw new SystemException(
    // "The membership being transferred is expired under the current business rules.");
    // }
    // AffiliatedClubCredit affiliatedClubCredit = new
    // AffiliatedClubCredit(daysRemaining, "",
    // creditAmount);
    // LogUtil.log("calculateAffiliatedClubCredit", "affiliatedClubCredit" +
    // affiliatedClubCredit);
    // return affiliatedClubCredit;
    // }

    /**
     * Calculate the fee per day.
     * 
     * @param affiliatedClub
     *            AffiliatedClubVO
     * @return BigDecimal
     */
    // public static BigDecimal
    // calculateAffiliatedClubFeePerDay(AffiliatedClubVO affiliatedClub)
    // {
    // //the standard fee which at the time of writing was based on the lowest
    // product type
    // return affiliatedClub.getFee().divide(new
    // BigDecimal(DEFAULT_MEMBERSHIP_TERM_DAYS),CommonConstants.MAX_PRECISION,BigDecimal.ROUND_HALF_DOWN);
    // }

    // public static Integer calculateMaximumTerm(Integer daysRemaining)
    // {
    // if(daysRemaining.intValue() > DEFAULT_MEMBERSHIP_TERM_DAYS)
    // {
    // //exception?
    // daysRemaining = new Integer(DEFAULT_MEMBERSHIP_TERM_DAYS);
    // }
    // return daysRemaining;
    // }

}
