package com.ract.membership;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

/**
 * Title: Membership system Description: This class provides methods to get
 * references to entity bean remote interfaces. Copyright: Copyright (c) 2002
 * Company: RACT
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class MembershipEJBHelper {

    /**
     * Return a reference to the membership manager.
     */
    public static MembershipMgr getMembershipMgr() {
	MembershipMgr membershipMgr = null;
	try {
	    membershipMgr = (MembershipMgr) ServiceLocator.getInstance().getObject("MembershipMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipMgr;
    }

    public static MembershipMgrLocal getMembershipMgrLocal() {
	MembershipMgrLocal membershipMgrLocal = null;
	try {
	    membershipMgrLocal = (MembershipMgrLocal) ServiceLocator.getInstance().getObject("MembershipMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipMgrLocal;
    }

    /**
     * Return a reference to the membership ID manager.
     */
    public static MembershipIDMgr getMembershipIDMgr() {
	MembershipIDMgr membershipIDMgr = null;
	try {
	    membershipIDMgr = (MembershipIDMgr) ServiceLocator.getInstance().getObject("MembershipIDMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipIDMgr;
    }

    /**
     * Return a reference to the membership reference manager.
     */
    public static MembershipRefMgr getMembershipRefMgr() {
	MembershipRefMgr membershipRefMgr = null;
	try {
	    membershipRefMgr = (MembershipRefMgr) ServiceLocator.getInstance().getObject("MembershipRefMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipRefMgr;
    }

    public static MembershipRenewalMgr getMembershipRenewalMgr() {
	MembershipRenewalMgr membershipRenewalMgr = null;
	try {
	    membershipRenewalMgr = (MembershipRenewalMgr) ServiceLocator.getInstance().getObject("MembershipRenewalMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipRenewalMgr;
    }

    public static MembershipRenewalMgrLocal getMembershipRenewalMgrLocal() {
	MembershipRenewalMgrLocal membershipRenewalMgrLocal = null;
	try {
	    membershipRenewalMgrLocal = (MembershipRenewalMgrLocal) ServiceLocator.getInstance().getObject("MembershipRenewalMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipRenewalMgrLocal;
    }

    /**
     * Return a reference to the membership reference manager.
     */
    public static MembershipRepairMgr getMembershipRepairMgr() {
	MembershipRepairMgr membershipRepairMgr = null;
	try {
	    membershipRepairMgr = (MembershipRepairMgr) ServiceLocator.getInstance().getObject("MembershipRepairMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipRepairMgr;
    }

    /**
     * Return a reference to the membership reference manager.
     */
    public static MembershipTransactionMgr getMembershipTransactionMgr() {
	MembershipTransactionMgr membershipTransactionMgr = null;
	try {
	    membershipTransactionMgr = (MembershipTransactionMgr) ServiceLocator.getInstance().getObject("MembershipTransactionMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipTransactionMgr;
    }

    public static MembershipTransactionMgrLocal getMembershipTransactionMgrLocal() {
	MembershipTransactionMgrLocal membershipTransactionMgrLocal = null;
	try {
	    membershipTransactionMgrLocal = (MembershipTransactionMgrLocal) ServiceLocator.getInstance().getObject("MembershipTransactionMgrBean/local");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return membershipTransactionMgrLocal;
    }

    public static TransactionMgr getTransactionMgr() {
	TransactionMgr transactionMgr = null;
	try {
	    transactionMgr = (TransactionMgr) ServiceLocator.getInstance().getObject("TransactionMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return transactionMgr;
    }

    /**
     * Return a reference to RenewalMgrHome interface
     */
    public static RenewalMgr getRenewalMgr() {
	RenewalMgr renewalMgr = null;
	try {
	    renewalMgr = (RenewalMgr) ServiceLocator.getInstance().getObject("RenewalMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return renewalMgr;
    }

    public static CardMgr getCardMgr() {
	CardMgr cardMgr = null;
	try {
	    cardMgr = (CardMgr) ServiceLocator.getInstance().getObject("CardMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return cardMgr;
    }

    public static DeliveryRoundMgr getDeliveryRoundMgr() {
	DeliveryRoundMgr deliveryRoundMgr = null;
	try {
	    deliveryRoundMgr = (DeliveryRoundMgr) ServiceLocator.getInstance().getObject("DeliveryRoundMgr/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return deliveryRoundMgr;
    }

}
