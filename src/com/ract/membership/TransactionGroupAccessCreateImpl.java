package com.ract.membership;

/**
 * Provides an implementation of the TransactionGroup class specifically
 * for managing the "Change product option" primary membership transaction.
 *
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author jyh
 * @version 1.0
 */

import java.rmi.RemoteException;

import com.ract.user.User;
import com.ract.util.DateTime;

public class TransactionGroupAccessCreateImpl extends TransactionGroupCreateImpl {

    public TransactionGroupAccessCreateImpl(User user, String transactionTypeCode, String joinReasonCode, DateTime effectiveDate, String profileCode) throws RemoteException {
	super(user, transactionTypeCode, joinReasonCode, effectiveDate, profileCode);
    }

}
