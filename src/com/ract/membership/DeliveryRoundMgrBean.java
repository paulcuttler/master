package com.ract.membership;

import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

import com.ract.client.Client;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.DataSourceFactory;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * Motor news delivery round processing.
 * 
 * @todo this class has had considerable sql server reliant functionality added
 *       that will need to be removed (eg. fn, dateadd, replace?) as they negate
 *       the value of an open architecture. PLEASE DO NOT REMOVE THIS COMMENT!
 * 
 * @todo add access membership filtering
 * 
 * @author dgk
 * @created 31 July 2002
 * @version 1.0
 */

public class DeliveryRoundMgrBean implements SessionBean {
    private SessionContext sessionContext;

    private DataSource dataSource;

    private MotorNewsSQLContainer sqlStatements;

    private DateTime threeMonthsAgo;

    private String summaryString;

    public DeliveryRoundMgrBean() throws RemoteException {
	DateTime now = new DateTime().getDateOnly();
	Interval threeMonths = null;
	try {
	    threeMonths = new Interval(0, 3, 0, 0, 0, 0, 0);
	} catch (Exception ex) {
	    throw new RemoteException("Unable to construct interval of three months.", ex);
	}

	now = new DateTime().getDateOnly();
	threeMonthsAgo = now.subtract(threeMonths);

	Collection clientStatusList = new ArrayList();
	clientStatusList.add(Client.STATUS_ADDRESS_UNKNOWN);
	clientStatusList.add(Client.STATUS_DECEASED);
	clientStatusList.add(Client.STATUS_DUPLICATE);
	clientStatusList.add(Client.STATUS_INACTIVE);

	Collection membershipStatusList = new ArrayList();
	membershipStatusList.add(MembershipVO.STATUS_ONHOLD);
	membershipStatusList.add(MembershipVO.STATUS_CANCELLED);

	Collection excludedProductList = new ArrayList();
	excludedProductList.add(ProductVO.PRODUCT_ACCESS);

	Collection motorNewsReasonList = new ArrayList();
	motorNewsReasonList.add("mn");
	motorNewsReasonList.add("ac");

	Collection annualReportReasonList = new ArrayList();
	annualReportReasonList.add("ar");

	Collection addSurnamesKeyEqualList = new ArrayList();
	addSurnamesKeyEqualList = getAddSurnamesKeyEqualList();

	Collection addSurnamesKeyLikeList = new ArrayList();
	addSurnamesKeyLikeList = getAddSurnamesKeyLikeList();

	summaryString = null;

	sqlStatements = new MotorNewsSQLContainer(clientStatusList, membershipStatusList, excludedProductList, motorNewsReasonList, annualReportReasonList, addSurnamesKeyEqualList, addSurnamesKeyLikeList);
    }

    /**
     * Description of the Method
     */
    public void ejbCreate() {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbRemove() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbActivate() throws RemoteException {
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void ejbPassivate() throws RemoteException {
    }

    /**
     * Sets the sessionContext attribute of the DeliveryRoundMgrBean object
     * 
     * @param sessionContext
     *            The new sessionContext value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setSessionContext(SessionContext sessionContext) throws RemoteException {
	this.sessionContext = sessionContext;
	try {
	    dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	} catch (NamingException e) {
	    throw new EJBException("Error looking up dataSource: " + e.toString());
	}

    }

    private Collection getAddSurnamesKeyEqualList() throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getMotorNewsAddSurnameKeyEqualList();
    }

    private Collection getAddSurnamesKeyLikeList() throws RemoteException {
	return CommonEJBHelper.getCommonMgr().getMotorNewsAddSurnameKeyLikeList();
    }

    /**
     * <p>
     * 
     * Extract list of clients for delivery of motor news.<br>
     * Results are written to text files. <br>
     * Uses Motor_News as a temporary table.
     * </p>
     * <b> Process as follows: </b> <br/>
     * 
     * <ol>
     * <li>Update Motor News Delivery List
     * <li>Extract Motor News text file
     * <li>Extract Annual Report text file
     * <li>Email results
     * </ol>
     * 
     * @param annualReportFileName
     *            List of members that receive the Annual Report
     * @param motorNewsFileName
     *            List of members that receive Motor News
     * @param errorFileName
     *            List of members in error
     * @param emailAddress
     *            Email results to these people
     * 
     * @exception RemoteException
     */
    public void extractDeliveryList(String annualReportFileName, String motorNewsFileName, String accessMembersFileName, String errorFileName, String emailAddress) throws RemoteException {

	LogUtil.log(this.getClass(), "First annualReportFileName = " + annualReportFileName + ",motorNewsFileName = " + motorNewsFileName + ",accessMembersFileName " + accessMembersFileName + ",errorFileName =  " + errorFileName);

	MotorNewsSummary drs = null;

	updateDeliveryTable();

	LogUtil.log(this.getClass(), "Motor News : Writing Output ");
	drs = writeFiles(annualReportFileName, motorNewsFileName, accessMembersFileName, errorFileName);

	LogUtil.log(this.getClass(), "Motor news distribution list extract complete");
	MailMessage message = new MailMessage();
	message.setRecipient(emailAddress);
	message.setSubject("Journeys Extract Report");
	message.setMessage(drs.toString());

	CommonEJBHelper.getMailMgr().sendMail(message);

    }

    private String trimmedOrEmpty(String s) {
	return (s == null) ? "" : s.trim();
    }

    /**
     * Creates text files representing the data held in the Motor_News table.<br>
     * 
     * <ol>
     * <li>Annual Report File - populated with records that have REASON = ar and
     * HOUSEHOLD = no
     * <li>Motor News File - populated with records that have REASON = mn and
     * HOUSEHOLD = no or REASON = ec
     * <li>Error File
     * </ol>
     * 
     * The MotorNewsSummary object contains counts of the records written to the
     * files
     * 
     * @param annualReportFileName
     *            List of members that receive the Annual Report
     * @param motorNewsFileName
     *            List of members that receive Motor News
     * @param errorFileName
     *            List of members in error
     * 
     * @throws RemoteException
     * @returns MotorNewsSummary
     */
    private MotorNewsSummary writeFiles(String annualReportFileName, String motorNewsFileName, String accessMembersFileName, String errorFileName) throws RemoteException {
	Connection connection = null;
	PreparedStatement statement = null;
	MotorNewsSummary motorNewsSummary = new MotorNewsSummary();

	int buffLen = 500;
	int sendLen = 472;
	StringBuffer outData = new StringBuffer(buffLen);
	outData.ensureCapacity(buffLen);
	for (int i = 0; i < (buffLen - 1); i++) {
	    outData.append(" ");
	}

	int clientNo;
	String clientTitle = null;
	String initials = null;
	String surname = null;
	String property = null;
	int streetNo;
	String streetChar = null;
	String street = null;
	String suburb = null;
	int postcode;
	String postCodeStr = null;
	String state = null;

	String toFile = null;

	String clientStatus = null;
	String memNo = null;
	String addrLine = null;

	// file details
	FileWriter annualReportStream = null;
	FileWriter errorStream = null;
	FileWriter whichFile = null;
	FileWriter motorNewsStream = null;
	FileWriter accessMembersStream = null;

	String errorMessage = "";

	String outputDirectoryName = null;
	int motorNewsTotal = 0;
	int accessMembersTotal = 0;
	int annualReportTotal = 0;
	int eligibleClientCount = 0;
	int errorTotal = 0;

	try {

	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    outputDirectoryName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MOTOR_NEWS, SystemParameterVO.DIRECTORY_MOTOR_NEWS_EXTRACTS);

	    LogUtil.log(this.getClass(), "Motor News : Opening " + outputDirectoryName + annualReportFileName);
	    annualReportStream = new FileWriter(outputDirectoryName + annualReportFileName);

	    LogUtil.log(this.getClass(), "Motor News : Opening " + outputDirectoryName + errorFileName);
	    errorStream = new FileWriter(outputDirectoryName + errorFileName);

	    LogUtil.log(this.getClass(), "Motor News : Opening " + outputDirectoryName + motorNewsFileName);
	    motorNewsStream = new FileWriter(outputDirectoryName + motorNewsFileName);

	    LogUtil.log(this.getClass(), "Motor News : Opening " + outputDirectoryName + accessMembersFileName);
	    accessMembersStream = new FileWriter(outputDirectoryName + accessMembersFileName);

	    errorStream.write("MOTOR NEWS EXTRACT " + new java.util.Date() + "\n");

	    whichFile = null;

	    connection = dataSource.getConnection();

	    StringBuffer sql = new StringBuffer();

	    sql.append("select    mn.client, ");
	    sql.append("          cl.[client-title], ");
	    sql.append("          cl.initials, ");
	    sql.append("          mn.surname, ");
	    sql.append("          mn.property, ");
	    sql.append("          mn.streetNo, ");
	    sql.append("          mn.streetChar , ");
	    sql.append("          st.street, ");
	    sql.append("          st.suburb, ");
	    sql.append("          st.postcode, ");
	    sql.append("          st.state, ");
	    sql.append("          mn.reason, ");
	    sql.append("          cl.[client-status], ");
	    sql.append("          mm.membership_number ");
	    sql.append(" from  ");
	    sql.append("          pub.motor_news_extract mn ");
	    sql.append("          inner join ");
	    sql.append("          PUB.[cl-master] cl on cl.[client-no] = mn.client ");
	    sql.append("          left outer join ");
	    sql.append("          pub.mem_membership mm on cl.[client-no] = mm.client_number ");
	    sql.append("          left outer join ");
	    sql.append("          PUB.[gn-stsub] st on st.stsubid = mn.stsubid ");
	    sql.append(" ");
	    sql.append(" where    mn.household = 'no' ");

	    statement = connection.prepareStatement(sql.toString());
	    ResultSet rs = statement.executeQuery();
	    int y = 0;

	    while (rs.next()) {
		y++;
		clientNo = rs.getInt(1);

		clientTitle = trimmedOrEmpty(rs.getString(2));
		initials = StringUtil.insertSpaces(trimmedOrEmpty(rs.getString(3)));
		surname = trimmedOrEmpty(rs.getString(4));
		property = trimmedOrEmpty(rs.getString(5));

		streetNo = rs.getInt(6);
		streetChar = trimmedOrEmpty(rs.getString(7));

		street = trimmedOrEmpty(rs.getString(8));
		suburb = trimmedOrEmpty(rs.getString(9));
		postcode = rs.getInt(10);
		state = trimmedOrEmpty(rs.getString(11));
		toFile = trimmedOrEmpty(rs.getString(12));
		clientStatus = rs.getString(13);
		memNo = trimmedOrEmpty(rs.getString(14));
		if (memNo == null) {
		    memNo = " ";
		}

		try {

		    errorMessage = null;

		    if (clientStatus != null && clientStatus.equalsIgnoreCase(Client.STATUS_ADDRESS_UNKNOWN)) {
			errorMessage = "\"Address unknown:  \"";
		    }

		    if (suburb == null) {
			errorMessage += "\"Suburb null:  \"";
		    } else if (suburb.equals("")) {
			errorMessage += "\"Suburb blank:  \"";
		    }

		    try {
			postCodeStr = new Integer(postcode).toString();
		    } catch (Exception e) {
			errorMessage += "\" No Postcode : \"";
		    }

		    if (errorMessage != null) {
			errorStream.write(errorMessage);
			errorStream.write("Client No = " + clientNo + "\n");
			errorTotal++;
		    } else {

			addrLine = (((streetChar == null) ? " " : streetChar.trim()) + " " + ((street == null) ? " " : street.trim())).trim();

			outData.replace(0, 19, clientTitle.trim());
			outData.replace(20, 25, initials);
			outData.replace(26, 75, surname);
			outData.replace(76, 81, ((streetChar == null) ? " " : streetChar.trim()));
			outData.replace(82, 131, property.trim());
			outData.replace(132, 181, addrLine);
			outData.replace(182, 231, suburb);
			outData.replace(232, 234, state);
			outData.replace(235, 239, postCodeStr);
			outData.replace(240, 249, memNo);
			outData.replace(250, 259, (new Integer(clientNo).toString()).trim());

			// now write the data
			if (toFile.equalsIgnoreCase("mn")) {
			    whichFile = motorNewsStream;
			    motorNewsTotal++;
			} else if (toFile.equalsIgnoreCase("ac")) // Access
								  // members
			{
			    whichFile = accessMembersStream;
			    accessMembersTotal++;
			} else if (toFile.equalsIgnoreCase("ec")) {
			    whichFile = motorNewsStream;
			    eligibleClientCount++;
			} else {
			    whichFile = annualReportStream;
			    annualReportTotal++;
			}

			if (outData.length() < buffLen) {
			    for (int i = outData.length(); i < (buffLen - 1); i++) {
				outData.append(" ");
			    }

			}

			whichFile.write(outData.substring(0, sendLen) + "\n");

			outData.setLength(0);
			for (int i = 0; i < (buffLen - 1); i++) {
			    outData.append(" ");
			}

		    }
		} catch (Exception e) {
		    errorStream.write("Unable to write record for " + clientNo + " " + e + "\n");
		    errorTotal++;
		}
	    }
	    rs.close();

	    LogUtil.log(this.getClass(), "Total Records Scanned " + y);

	    motorNewsSummary.setMotorNews(motorNewsTotal);
	    motorNewsSummary.setAccessMembers(accessMembersTotal);
	    motorNewsSummary.setAnnualReport(annualReportTotal);
	    motorNewsSummary.setEligibleClients(eligibleClientCount);
	    motorNewsSummary.setSummaryString(this.summaryString);
	    motorNewsSummary.setErrors(errorTotal);

	    errorStream.write("\n");

	    errorStream.write(motorNewsSummary.toString());
	    errorStream.write("\nFINISHED: " + new java.util.Date());

	} catch (Exception e) {
	    throw new RemoteException("Error extracting mailing data", e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);

	    try {
		annualReportStream.close();
		accessMembersStream.close();
		motorNewsStream.close();
		errorStream.close();

	    } catch (IOException ioe) {
		LogUtil.fatal(this.getClass(), ioe);
	    }
	}
	LogUtil.log(this.getClass(), "MotorNewsExtract:- Write complete " + new java.util.Date());
	return motorNewsSummary;
    }

    /**
     * <p>
     * 
     * Extract list of clients for delivery of motor news.<br>
     * Uses Motor_News as a temporary table.
     * </p>
     * <b> Process as follows: </b> <br/>
     * 
     * <ol>
     * <li>Empty Motor_News table
     * <li>Collect Eligible Memberships ( send option normal )
     * <li>Determine Households and remove all but 1 member
     * <li>Collect "Individual" copies
     * <li>Collect "Free Subscribers"
     * <li>Collect Annual Report members
     * </ol>
     * 
     * @throws RemoteException
     */
    private void updateDeliveryTable() throws RemoteException {

	int eligibleMembers;
	int individuals;
	int freeSubscribers;
	int houseHoldKeys;
	int surnamesAdded;
	int membersRemovedHouseHolds;
	int annualReportMembers;
	int annualReportHouseHoldMembers;
	int annualReporRemovedHouseHolds;
	int annualReportHouseHoldKeys;
	int houseHoldMembers;
	int accessMembers;

	String membershipCounts;

	UserTransaction ut = this.sessionContext.getUserTransaction();

	try {
	    ut.setTransactionTimeout(3600);
	    ut.begin();

	    LogUtil.log(this.getClass(), "Motor News : Removing Extracted");
	    this.removeAllExtractRecords();

	    LogUtil.log(this.getClass(), "Motor News : Count Memberships");
	    membershipCounts = this.countMemberships();

	    LogUtil.log(this.getClass(), "Motor News : Insert Eligible Memberships ");
	    eligibleMembers = this.insertEligibleMemberships();

	    LogUtil.log(this.getClass(), "Motor News : Create HouseHoldKeys ");
	    houseHoldKeys = createHouseHoldKeys(sqlStatements.motorNewsReasonInList);

	    LogUtil.log(this.getClass(), "Motor News : Add surnames to HouseHoldKeys ");
	    surnamesAdded = addSurnamesToHouseHoldKeys(sqlStatements.motorNewsReasonInList);

	    LogUtil.log(this.getClass(), "Motor News : Set PA for Access Members");
	    accessMembers = updatePAforAccessMembers();

	    LogUtil.log(this.getClass(), "Motor News : Find Households ");
	    membersRemovedHouseHolds = this.findHouseholds(sqlStatements.motorNewsReasonInList);

	    LogUtil.log(this.getClass(), "Motor News : Update Households ");
	    houseHoldMembers = this.updateHouseholds(sqlStatements.motorNewsReasonInList);

	    LogUtil.log(this.getClass(), "Motor News : Insert Other Eligible Memberships ");
	    individuals = this.insertOtherEligibleMemberships();

	    LogUtil.log(this.getClass(), "Motor News : Insert Other Eligible Clients ");
	    freeSubscribers = this.insertOtherEligibleClients();

	    LogUtil.log(this.getClass(), "Motor News : Insert Annual Report ");
	    annualReportMembers = this.insertAnnualReportMembers();

	    LogUtil.log(this.getClass(), "Motor News : Create HouseHoldKeys (Annual Report)");
	    annualReportHouseHoldKeys = createHouseHoldKeys(sqlStatements.annualReportReasonInList);

	    LogUtil.log(this.getClass(), "Motor News : Find Households (Annual Report)");
	    annualReportHouseHoldMembers = this.findHouseholds(sqlStatements.annualReportReasonInList);

	    LogUtil.log(this.getClass(), "Motor News : Update Households (Annual Report)");
	    annualReporRemovedHouseHolds = this.updateHouseholds(sqlStatements.annualReportReasonInList);

	    this.summaryString = membershipCounts + "\n*************************************************" + "\n First pass eligible memberships       : " + eligibleMembers + "\n HouseHold Keys created                : " + houseHoldKeys + " ( " + surnamesAdded + " ) " + "\n    HouseHolds Found                   : " + houseHoldMembers + "\n    Members Removed                    : " + membersRemovedHouseHolds + "\n " + "\n Total Memberships for Journeys        : " + (eligibleMembers - membersRemovedHouseHolds + houseHoldMembers) + "\n " + "\n Individual copies                     : " + individuals + "\n Always send                           : " + freeSubscribers + "\n " + "\n Total Journeys                        : " + (eligibleMembers - membersRemovedHouseHolds + houseHoldMembers + individuals + freeSubscribers) + "\n             ===============" + "\n Members for Annual Report             : " + annualReportMembers + "\n HouseHold Keys created                : " + annualReportHouseHoldKeys
		    + "\n    HouseHolds Found                   : " + annualReportHouseHoldMembers + "\n    Removed from Annual Report         : " + annualReporRemovedHouseHolds + "\n " + "\n Total Annual Report                   : " + (annualReportMembers - annualReportHouseHoldMembers + annualReporRemovedHouseHolds) + "\n*************************************************";

	    LogUtil.log(this.getClass(), this.summaryString);
	    ut.commit();
	    LogUtil.log(this.getClass(), "Motor News : Committed");

	} catch (Exception se) {
	    LogUtil.log(this.getClass(), "Motor News : Error " + se.getMessage());
	    if (ut != null) {
		try {
		    ut.rollback();
		} catch (Exception ex) {
		    throw new RemoteException("Unable to rollback the transaction.", ex);
		}
	    }
	    throw new RemoteException("Unable to update delivery table list.", se);
	}

	LogUtil.log(this.getClass(), "MotorNewsExtract:- Extract complete " + new java.util.Date());

    }

    /**
     * Insert a record in the Motor_News table for each current membership. <br>
     * <br>
     * Extract criteria:<br>
     * <br>
     * ...expiry date after today - 3 months (ie can be up to 3 months expired)<br>
     * ...not RACT staff<br>
     * ...motor_news_send_option is either null or set to "normal"<br>
     * ...client status is not 'address unknown', 'Deceased','duplicate' or
     * 'INACTIVE'<br>
     * ...membership status is not 'On hold' or 'Cancelled'<br>
     * ...client name is not like '*ESTATE*'
     * 
     * @throws RemoteException
     * @return The number of records added to the Motor_News table
     */
    private int insertEligibleMemberships() throws RemoteException {

	int eligibleMemberships;

	Connection connection = null;
	PreparedStatement statement = null;

	String sql = sqlStatements.getEligibleMembershipsSQL();

	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sql);

	    statement.setDate(1, threeMonthsAgo.toSQLDate());
	    statement.setString(2, DiscountTypeVO.TYPE_RACT_STAFF);
	    statement.setString(3, Client.MOTOR_NEWS_MEMBERSHIP_SEND);

	    eligibleMemberships = statement.executeUpdate();
	    LogUtil.log(this.getClass(), "Eligible memberships = " + eligibleMemberships);

	} catch (SQLException e) {
	    throw new RemoteException(sql, e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return eligibleMemberships;
    }

    private int updatePAforAccessMembers() throws RemoteException {
	int accessMembers;

	Connection connection = null;
	PreparedStatement statement = null;

	String sql = sqlStatements.getUpdatePAforAccessMembersSQL();

	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sql);

	    accessMembers = statement.executeUpdate();
	    LogUtil.log(this.getClass(), "Access memberships = " + accessMembers);

	} catch (SQLException e) {
	    throw new RemoteException(sql, e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return accessMembers;
    }

    /**
     * 
     * 
     * @param reason
     *            String
     * @throws RemoteException
     * @return int
     */
    private int createHouseHoldKeys(String reasons) throws RemoteException {
	Connection connection = null;
	PreparedStatement statement = null;

	int rowsAffected = 0;

	try {
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(sqlStatements.getHouseHoldKeySQL() + "( " + reasons.trim() + " ) ");

	    rowsAffected = statement.executeUpdate();
	} catch (SQLException e) {
	    throw new RemoteException(statement.toString(), e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return rowsAffected;
    }

    private int addSurnamesToHouseHoldKeys(String reasons) throws RemoteException {
	Connection connection = null;
	PreparedStatement statement = null;

	int rowsAffected = 0;

	if (sqlStatements.addSurnamesToHouseholdKeySQL != null) {
	    LogUtil.log("RLG +++++++++ ", sqlStatements.addSurnamesToHouseholdKeySQL);
	    try {
		connection = dataSource.getConnection();
		statement = connection.prepareStatement(sqlStatements.addSurnamesToHouseholdKeySQL + "( " + reasons.trim() + " ) ");

		rowsAffected = statement.executeUpdate();
	    } catch (SQLException e) {
		throw new RemoteException(statement.toString(), e);
	    } finally {
		ConnectionUtil.closeConnection(connection, statement);
	    }
	}

	return rowsAffected;

    }

    /**
     * Add records to Motor_News table for clients having the motor news send
     * option set to SEND.<br>
     * (This will include members who have indicated that they definitely want a
     * personal copy of Motor News, as well as non members who receive a copy<br>
     * Excludes client status 'deceased' and surnames including ' ESTATE '
     * 
     * @throws RemoteException
     * @return The number of (non-member) clients added to the Motor_News table.
     */
    private int insertOtherEligibleClients() throws RemoteException {
	int otherClients;
	Connection connection = null;
	PreparedStatement statement = null;

	String sql = sqlStatements.getEligibleClientsSQL();
	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sql);

	    statement.setString(1, Client.MOTOR_NEWS_SEND);
	    statement.setString(2, Client.STATUS_DECEASED);

	    otherClients = statement.executeUpdate();
	    LogUtil.log(this.getClass(), "Other clients = " + otherClients);

	} catch (SQLException e) {
	    throw new RemoteException(sql, e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return otherClients;
    }

    private int insertAnnualReportMembers() throws RemoteException {
	int annualReport;
	Connection connection = null;
	PreparedStatement statement = null;

	String sql = sqlStatements.getAnnualReportSQL();
	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sql);

	    statement.setDate(1, threeMonthsAgo.toSQLDate());

	    annualReport = statement.executeUpdate();
	    LogUtil.log(this.getClass(), "Annual Report = " + annualReport);

	} catch (SQLException e) {
	    throw new RemoteException(sql, e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return annualReport;
    }

    private int insertOtherEligibleMemberships() throws RemoteException {
	int eligibleMemberships;

	Connection connection = null;
	PreparedStatement statement = null;

	String sql = sqlStatements.getOtherEligibleMembershipsSQL();

	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sql);

	    statement.setDate(1, threeMonthsAgo.toSQLDate());
	    statement.setString(2, DiscountTypeVO.TYPE_RACT_STAFF);
	    statement.setString(3, Client.MOTOR_NEWS_SEND_INDIVIDUAL);

	    eligibleMemberships = statement.executeUpdate();
	    LogUtil.log(this.getClass(), "Send Individual memberships = " + eligibleMemberships);
	} catch (SQLException e) {
	    throw new RemoteException(sql, e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return eligibleMemberships;

    }

    private int findHouseholds(String reasons) throws RemoteException {
	StringBuffer sql = new StringBuffer();
	sql.append("update  pub.motor_news_extract ");
	sql.append("set houseHold = 'yes' ");
	sql.append(" ");
	sql.append("from ");
	sql.append("    (   SELECT     houseKey ");
	sql.append("        FROM       pub.motor_news_extract ");
	sql.append("        GROUP BY   houseKey ");
	sql.append("        HAVING     ({ fn LENGTH(houseKey) } > 0) AND (COUNT(*) > 1) ");
	/** @todo sql server specific function fn length */
	sql.append(") mh ");
	sql.append(" ");
	sql.append("where mh.houseKey = pub.motor_news_extract.housekey  ");
	sql.append("AND     pub.motor_news_extract.reason in  ");
	sql.append("(" + reasons.trim() + ")");

	int houseHolds;

	Connection connection = null;
	PreparedStatement statement = null;

	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sql.toString());

	    houseHolds = statement.executeUpdate();

	} catch (SQLException e) {
	    throw new RemoteException(sql.toString(), e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return houseHolds;

    }

    private int updateHouseholds(String reasons) throws RemoteException {

	StringBuffer sql = new StringBuffer();
	sql.append("update  pub.motor_news_extract ");
	sql.append("set     houseHold = 'no' ");
	sql.append("from  ( select client  ");
	sql.append("         from pub.motor_news_extract mn ");
	sql.append("         where mn.houseHold = 'yes'  ");
	sql.append("         and mn.reason in ( " + reasons.trim() + " ) ");
	sql.append("         and client = ( select min(client) ");
	sql.append("                         from pub.motor_news_extract mc");
	sql.append("                         where joined = ( select min(joined) ");
	sql.append("                                           from pub.motor_news_extract mj");
	sql.append("                                           where reason = ( select max(reason)  ");
	sql.append("                                                              from  pub.motor_news_extract mr");
	sql.append("                                                              where pa = ( select max (pa) ");
	sql.append("                                                                             from  pub.motor_news_extract mp  ");
	sql.append("                                                                             where  mr.housekey = mp.housekey");
	sql.append("                                                                         )");
	sql.append("                                                                and mr.housekey = mj.housekey ");
	sql.append("                                                          )");
	sql.append("                                          and mj.housekey = mc.housekey");
	sql.append("                                        )");
	sql.append("                        and mc.housekey = mn.housekey ");
	sql.append("                      ) ");
	sql.append("      ) householders ");
	sql.append("WHERE pub.motor_news_extract.client = householders.client");

	int houseHolds;

	Connection connection = null;
	PreparedStatement statement = null;

	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sql.toString());

	    houseHolds = statement.executeUpdate();

	} catch (SQLException e) {
	    throw new RemoteException(sql.toString(), e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return houseHolds;

    }

    private String countMemberships() throws RemoteException {

	String membershipCounts = "";
	StringBuffer t = new StringBuffer(80);

	Connection connection = null;
	PreparedStatement statement = null;

	StringBuffer sqlBuffer = new StringBuffer();

	sqlBuffer.append("select ");
	sqlBuffer.append("      count(*) as num, ");
	sqlBuffer.append("      expired = case when expiry_date >= dateadd(month,-3,getdate()) ");
	/** @todo sql server specific */
	sqlBuffer.append("                then 'Current' ");
	sqlBuffer.append("                else 'Expired' ");
	sqlBuffer.append("             end, ");
	sqlBuffer.append("      membership_status, ");
	sqlBuffer.append("      product_code");
	sqlBuffer.append(" ");
	sqlBuffer.append("from    pub.mem_membership ");
	sqlBuffer.append(" ");
	sqlBuffer.append("group by ");
	sqlBuffer.append("      case when expiry_date >= dateadd(month,-3,getdate()) ");
	sqlBuffer.append("              then 'Current' ");
	sqlBuffer.append("              else 'Expired' ");
	sqlBuffer.append("      end, ");
	sqlBuffer.append(" ");
	sqlBuffer.append("      membership_status, ");
	sqlBuffer.append("      product_code");
	sqlBuffer.append(" ");
	sqlBuffer.append("order by case when expiry_date >= dateadd(month,-3,getdate()) ");
	sqlBuffer.append("              then 'Current' ");
	sqlBuffer.append("              else 'Expired' ");
	sqlBuffer.append("          end, ");
	sqlBuffer.append(" ");
	sqlBuffer.append("          membership_status, ");
	sqlBuffer.append("          product_code");

	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(sqlBuffer.toString());

	    ResultSet rs = statement.executeQuery();

	    membershipCounts = "\n*************************************************";
	    membershipCounts += "\n                   Membership Counts            ";
	    membershipCounts += "\n ";

	    while (rs.next()) {
		membershipCounts += "\n " + StringUtil.convertNull(rs.getString(2)).trim() + " \t " + StringUtil.convertNull(rs.getString(3)).trim() + " \t " + StringUtil.convertNull(rs.getString(4)).trim() + " \t " + " : " + StringUtil.convertNull(rs.getString(1)).trim();
	    }

	    membershipCounts += "\n ";

	} catch (Exception e) {
	    throw new RemoteException(membershipCounts + " /// SQL IS " + sqlBuffer.toString(), e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	return membershipCounts;
    }

    // this was originally not a good name. ie SqlStatement. it is actually a
    // collection of specific SQL statements
    // not a general object.
    /**
     * Container for building sql statements for motor news processing.
     * 
     * @author jyh
     * @version 1.0
     */
    private class MotorNewsSQLContainer {
	String membershipStatusInList;
	String clientStatusInList;
	String motorNewsReasonInList;
	String annualReportReasonInList;

	String eligibleMembershipsSQL;
	String otherEligibleMembershipsSQL;
	String genericInsertSQL;
	String eligibleClientsSQL;
	String houseHoldKeySQL;
	String motorNewsSelectSQL;
	String genericSelectSQL;
	String annualReportSQL;
	String updatePAforAccessMembersSQL;
	String addSurnamesToHouseholdKeySQL;

	private MotorNewsSQLContainer() {
	    // not used
	}

	public MotorNewsSQLContainer(Collection clientStatus, Collection membershipStatus, Collection excludedProductList, Collection motorNewsReasons, Collection annualReportReasons, Collection addSurnamesKeyEqualList, Collection addSurnamesKeyLikeList) {

	    clientStatusInList = this.buildSQLInList(clientStatus);
	    membershipStatusInList = this.buildSQLInList(membershipStatus);
	    motorNewsReasonInList = this.buildSQLInList(motorNewsReasons);
	    annualReportReasonInList = this.buildSQLInList(annualReportReasons);

	    genericInsertSQL = buildGenericInsertSQL();
	    genericSelectSQL = buildGenericSelectSQL();

	    motorNewsSelectSQL = buildMotorNewsSelectSQL();
	    eligibleMembershipsSQL = buildEligibleMembershipsSQL();
	    otherEligibleMembershipsSQL = buildOtherEligibleMembershipsSQL();
	    eligibleClientsSQL = buildEligibleClientsSQL();
	    houseHoldKeySQL = buildHouseHoldKeySQL();
	    annualReportSQL = buildAnnualReportSQL(excludedProductList);

	    updatePAforAccessMembersSQL = buildPAforAccessMembersSQL();
	    addSurnamesToHouseholdKeySQL = buildAddSurnamesToHouseholdKeySQL(addSurnamesKeyEqualList, addSurnamesKeyLikeList);
	}

	public String getEligibleMembershipsSQL() {
	    return eligibleMembershipsSQL;
	}

	public String getUpdatePAforAccessMembersSQL() {
	    return updatePAforAccessMembersSQL;
	}

	public String getAnnualReportSQL() {
	    return annualReportSQL;
	}

	public String getOtherEligibleMembershipsSQL() {
	    return otherEligibleMembershipsSQL;
	}

	public String getEligibleClientsSQL() {
	    return eligibleClientsSQL;
	}

	public String getHouseHoldKeySQL() {
	    return houseHoldKeySQL;
	}

	private String buildPAforAccessMembersSQL() {
	    StringBuffer sqlBuffer = new StringBuffer();

	    sqlBuffer.append("update pub.motor_news_extract ");
	    sqlBuffer.append("       set pa = -2 ");
	    sqlBuffer.append("where  reason = 'ac' ");

	    return sqlBuffer.toString();
	}

	private String buildSQLInList(Collection listItems) {
	    StringBuffer inList = new StringBuffer();
	    Iterator searchList = listItems.iterator();
	    int count = 0;
	    while (searchList.hasNext()) {
		count++;
		inList.append("'" + searchList.next() + "'");
		if (count < listItems.size()) {
		    inList.append(FileUtil.SEPARATOR_COMMA);
		}
	    }
	    return inList.toString(); // cl.substring(0,cl.length()-2);
	}

	private String buildAnnualReportSQL(Collection excludedProductList) {
	    StringBuffer sqlBuffer = new StringBuffer();

	    sqlBuffer.append(genericInsertSQL);
	    sqlBuffer.append(genericSelectSQL);

	    sqlBuffer.append("     'ar'	                    as     reason ");
	    sqlBuffer.append(" ");
	    sqlBuffer.append("from    pub.[cl-master] cl  ");
	    sqlBuffer.append("        join  ");
	    sqlBuffer.append("        pub.mem_membership mm on cl.[client-no] = mm.client_number  ");
	    sqlBuffer.append("        left outer join ");
	    sqlBuffer.append("        pub.mem_membership_group mg on mg.membership_id = mm.membership_id      ");
	    sqlBuffer.append("        LEFT OUTER JOIN ");
	    sqlBuffer.append("        pub.motor_news_extract mn ON mm.client_number = mn.client ");
	    sqlBuffer.append(" ");
	    sqlBuffer.append("where   (mn.client IS NULL)  ");
	    sqlBuffer.append(" ");
	    sqlBuffer.append(" AND     mm.expiry_date > ?  ");
	    sqlBuffer.append(" ");
	    sqlBuffer.append("AND (   mm.membership_status not in ( " + membershipStatusInList + " ) ");
	    sqlBuffer.append("    OR  mm.membership_status IS NULL  ");
	    sqlBuffer.append("    ) ");
	    sqlBuffer.append(" ");
	    sqlBuffer.append("AND (NOT (cl.surname LIKE '% ESTATE %'))  ");
	    sqlBuffer.append(" ");
	    sqlBuffer.append("AND (   cl.[client-status] not in ( " + clientStatusInList + " )  ");
	    sqlBuffer.append("    or  cl.[client-status] IS NULL ");
	    sqlBuffer.append("    ) ");
	    if (excludedProductList.size() > 0) {
		sqlBuffer.append("and mm.product_code not in (" + buildSQLInList(excludedProductList) + ") ");
	    }

	    return sqlBuffer.toString();
	}

	private String buildGenericInsertSQL() {
	    StringBuffer sqlBuffer = new StringBuffer();
	    sqlBuffer.append("insert into pub.motor_news_extract ( ");
	    sqlBuffer.append("      client,");
	    sqlBuffer.append("      stsubid,");
	    sqlBuffer.append("      streetNo,");
	    sqlBuffer.append("      property,");
	    sqlBuffer.append("      streetChar,");
	    sqlBuffer.append("      household,");
	    sqlBuffer.append("      houseKey,");
	    sqlBuffer.append("      pa,");
	    sqlBuffer.append("      joined,");
	    sqlBuffer.append("      surname,");
	    sqlBuffer.append("      sendOption, ");
	    sqlBuffer.append("      reason ) ");
	    sqlBuffer.append(" ");

	    return sqlBuffer.toString();
	}

	private String buildAddSurnamesToHouseholdKeySQL(Collection keyEqualsList, Collection keyLikeList) {
	    String addSurnameToEqualKey = buildKeyEqualString(keyEqualsList);
	    String addSurnameToLikeKey = buildKeyLikeString(keyLikeList);

	    boolean addSurnames = ((addSurnameToEqualKey != null) || (addSurnameToLikeKey != null));

	    LogUtil.log("RLG =========== add surnames ", new Boolean(addSurnames).toString());
	    LogUtil.log("RLG =========== addSurnameToEqualKey ", new Boolean((addSurnameToEqualKey == null)).toString());
	    LogUtil.log("RLG =========== addSurnameToLikeKey ", new Boolean((addSurnameToLikeKey == null)).toString());
	    StringBuffer sqlBuffer = new StringBuffer();

	    if (addSurnames) {
		sqlBuffer.append(buildAddSurnamesSQL(addSurnameToEqualKey, addSurnameToLikeKey));
		return sqlBuffer.toString();
	    }

	    return null;

	}

	private StringBuffer buildAddSurnamesSQL(String addSurnameToEqualKey, String addSurnameToLikeKey) {
	    StringBuffer sqlBuffer = new StringBuffer();
	    /**
	     * @todo sql server specific
	     */
	    sqlBuffer.append("update pub.motor_news_extract ");
	    sqlBuffer.append("    set houseKey =  ");
	    sqlBuffer.append("        upper   ( ");
	    sqlBuffer.append("            replace ( ");
	    sqlBuffer.append("                replace ( ");
	    sqlBuffer.append("                    replace ( ");
	    sqlBuffer.append("                        replace(rtrim(housekey)  + ");
	    sqlBuffer.append("                                rtrim(surname)     ");
	    sqlBuffer.append("                                ,' ' ");
	    sqlBuffer.append("                                ,'' ");
	    sqlBuffer.append("                                ) ");
	    sqlBuffer.append("                            ,'.' ");
	    sqlBuffer.append("                            ,'' ");
	    sqlBuffer.append("                            ) ");
	    sqlBuffer.append("                        ,'-' ");
	    sqlBuffer.append("                        ,'' ");
	    sqlBuffer.append("                        ) ");
	    sqlBuffer.append("                    ,'/' ");
	    sqlBuffer.append("                    ,'' ");
	    sqlBuffer.append("                    )      ");
	    sqlBuffer.append("                ) ");
	    sqlBuffer.append(" where ( ");

	    if (addSurnameToEqualKey != null) {
		sqlBuffer.append(addSurnameToEqualKey);
		if (addSurnameToLikeKey != null) {
		    sqlBuffer.append(" or ");
		}
	    }

	    if (addSurnameToLikeKey != null) {
		sqlBuffer.append(addSurnameToLikeKey);
	    }
	    sqlBuffer.append(" ) and reason in ");
	    LogUtil.log("RLG ++++++ buildAddSurnamesSQL ", sqlBuffer.toString());
	    return sqlBuffer;

	}

	private String buildKeyEqualString(Collection keyList) {
	    String keyString = null;
	    Iterator it = keyList.iterator();

	    while (it.hasNext()) {
		LogUtil.log("RLG +++++ bke ", keyString);
		if (keyString != null) {
		    keyString += " or ";
		} else {
		    keyString = "";
		}

		keyString += "housekey = '";
		LogUtil.log("RLG +++++ bke 1 ", keyString);
		keyString += ((String) it.next()).trim();
		keyString += "'";
	    }
	    LogUtil.log("RLG buildKeyEqualString ", keyString);
	    return keyString;
	}

	private String buildKeyLikeString(Collection keyList) {
	    String keyString = null;
	    Iterator it = keyList.iterator();

	    while (it.hasNext()) {
		if (keyString != null) {
		    keyString += " or ";
		} else {
		    keyString = "";
		}

		keyString += "patindex('%";
		keyString += ((String) it.next()).trim();
		keyString += "%',houseKey) > 0";
	    }
	    LogUtil.log("RLG buildKeyLikeString ", keyString);
	    return keyString;
	}

	private String buildHouseHoldKeySQL() {
	    StringBuffer sqlBuffer = new StringBuffer();
	    /**
	     * @todo sql server specific
	     */
	    sqlBuffer.append("update pub.motor_news_extract ");
	    sqlBuffer.append("    set houseKey =  ");
	    sqlBuffer.append("        upper   ( ");
	    sqlBuffer.append("            replace ( ");
	    sqlBuffer.append("                replace ( ");
	    sqlBuffer.append("                    replace ( ");
	    sqlBuffer.append("                        replace(rtrim(cast(stsubid as varchar))  + ");
	    /*
	     * sqlBuffer.append (
	     * "                                rtrim(cast(streetNo as varchar)) + "
	     * );
	     */
	    sqlBuffer.append("                                rtrim(streetChar)                + ");
	    sqlBuffer.append("                                rtrim(property)                  + ");
	    sqlBuffer.append(" ' '");
	    /*
	     * sqlBuffer.append (
	     * "                                rtrim(surname)                     "
	     * );
	     */
	    sqlBuffer.append("                                ,' ' ");
	    sqlBuffer.append("                                ,'' ");
	    sqlBuffer.append("                                ) ");
	    sqlBuffer.append("                            ,'.' ");
	    sqlBuffer.append("                            ,'' ");
	    sqlBuffer.append("                            ) ");
	    sqlBuffer.append("                        ,'-' ");
	    sqlBuffer.append("                        ,'' ");
	    sqlBuffer.append("                        ) ");
	    sqlBuffer.append("                    ,'/' ");
	    sqlBuffer.append("                    ,'' ");
	    sqlBuffer.append("                    )      ");
	    sqlBuffer.append("                ) ");
	    sqlBuffer.append(" where reason in ");

	    return sqlBuffer.toString();

	}

	private String buildEligibleClientsSQL() {
	    StringBuffer sqlBuffer = new StringBuffer();

	    sqlBuffer.append(genericInsertSQL);
	    sqlBuffer.append("select ");
	    sqlBuffer.append("     [client-no]                    as     client,");
	    sqlBuffer.append("     [post-stsubid]                 as     stsubid,");
	    sqlBuffer.append("     streetNo =");
	    sqlBuffer.append("       case");
	    sqlBuffer.append("          when [post-street-no] is null ");
	    sqlBuffer.append("               then 0");
	    sqlBuffer.append("               else [post-street-no]     ");
	    sqlBuffer.append("       end,");
	    sqlBuffer.append("     property =");
	    sqlBuffer.append("       case");
	    sqlBuffer.append("          when [post-property] is null");
	    sqlBuffer.append("               then ' '");
	    sqlBuffer.append("               else [post-property]");
	    sqlBuffer.append("       end,");
	    sqlBuffer.append("     streetChar = ");
	    sqlBuffer.append("       case");
	    sqlBuffer.append("          when [post-street-char] is null ");
	    sqlBuffer.append("               then ' '");
	    sqlBuffer.append("               else [post-street-char]");
	    sqlBuffer.append("       end,");
	    sqlBuffer.append("     'no '                          as     household,");
	    sqlBuffer.append("     ' '                            as     houseKey,");
	    sqlBuffer.append("     1                              as     pa, ");
	    sqlBuffer.append("     getDate()                      as     joined, ");
	    sqlBuffer.append("     surname, ");
	    sqlBuffer.append("     motor_news_send_option         as     sendOption, ");
	    sqlBuffer.append("     'ec'                           as     reason ");
	    sqlBuffer.append(" ");

	    sqlBuffer.append("from    pub.[cl-master] cl ");

	    sqlBuffer.append("where   surname not like '% ESTATE %' ");
	    sqlBuffer.append("and     cl.motor_news_send_option =  ?  ");
	    sqlBuffer.append("and     (       cl.[client-status] is null ");
	    sqlBuffer.append("            or  cl.[client-status] <> ? ");
	    sqlBuffer.append("        ) ");

	    return sqlBuffer.toString();

	}

	private String buildEligibleMembershipsSQL() {
	    StringBuffer sqlBuffer = new StringBuffer();

	    sqlBuffer.append(genericInsertSQL);
	    sqlBuffer.append(motorNewsSelectSQL);

	    sqlBuffer.append("and     (       cl.motor_news_send_option is null ");
	    sqlBuffer.append("            or  cl.motor_news_send_option =  ? ");
	    sqlBuffer.append("        ) ");
	    sqlBuffer.append("and     (       cl.[client-status] is null ");
	    sqlBuffer.append("            or  cl.[client-status] not in (  " + clientStatusInList + " ) ");
	    sqlBuffer.append("        ) ");
	    sqlBuffer.append("and     (       mm.membership_status is null ");
	    sqlBuffer.append("            or  mm.membership_status not in (  " + membershipStatusInList + " ) ");
	    sqlBuffer.append("        )");

	    return sqlBuffer.toString();

	}

	private String buildOtherEligibleMembershipsSQL() {
	    StringBuffer sqlBuffer = new StringBuffer();

	    sqlBuffer.append(genericInsertSQL);
	    sqlBuffer.append(motorNewsSelectSQL);

	    sqlBuffer.append("and  cl.motor_news_send_option =  ?  ");
	    sqlBuffer.append("and     (       cl.[client-status] is null ");
	    sqlBuffer.append("            or  cl.[client-status] not in (  " + clientStatusInList + " ) ");
	    sqlBuffer.append("        ) ");
	    sqlBuffer.append("and     (       mm.membership_status is null ");
	    sqlBuffer.append("            or  mm.membership_status not in (  " + membershipStatusInList + " ) ");
	    sqlBuffer.append("        )");

	    return sqlBuffer.toString();

	}

	private String buildGenericSelectSQL() {
	    StringBuffer sqlBuffer = new StringBuffer();

	    sqlBuffer.append("select ");
	    sqlBuffer.append("     [client-no]                    as     client,");
	    sqlBuffer.append("     [post-stsubid]                 as     stsubid,");
	    sqlBuffer.append("     streetNo =");
	    sqlBuffer.append("       case");
	    sqlBuffer.append("          when [post-street-no] is null ");
	    sqlBuffer.append("               then 0");
	    sqlBuffer.append("               else [post-street-no]     ");
	    sqlBuffer.append("       end,");
	    sqlBuffer.append("     property =");
	    sqlBuffer.append("       case");
	    sqlBuffer.append("          when [post-property] is null");
	    sqlBuffer.append("               then ' '");
	    sqlBuffer.append("               else [post-property]");
	    sqlBuffer.append("       end,");
	    sqlBuffer.append("     streetChar = ");
	    sqlBuffer.append("       case");
	    sqlBuffer.append("          when [post-street-char] is null ");
	    sqlBuffer.append("               then ' '");
	    sqlBuffer.append("               else [post-street-char]");
	    sqlBuffer.append("       end,");
	    sqlBuffer.append("     'no '                          as     household,");
	    sqlBuffer.append("     ' '                            as     houseKey,");
	    sqlBuffer.append("     pa =");
	    sqlBuffer.append("       case ");
	    sqlBuffer.append("          when mg.is_Prime_Addressee is null ");
	    sqlBuffer.append("               then 1");
	    sqlBuffer.append("               else mg.is_Prime_Addressee ");
	    sqlBuffer.append("       end, ");
	    sqlBuffer.append("     mm.join_date                   as     joined, ");
	    sqlBuffer.append("     cl.surname                     as     surname, ");
	    sqlBuffer.append("     motor_news_send_option         as     sendOption, ");

	    return sqlBuffer.toString();
	}

	private String buildMotorNewsSelectSQL() {
	    StringBuffer sqlBuffer = new StringBuffer();

	    sqlBuffer.append(genericSelectSQL);

	    // sqlBuffer.append("     'mn'                           as     reason ");
	    sqlBuffer.append("      reason = ");
	    sqlBuffer.append("        case ");
	    sqlBuffer.append("           when mm.product_code = 'access' ");
	    sqlBuffer.append("                then 'ac' ");
	    sqlBuffer.append("                else 'mn' ");
	    sqlBuffer.append("         end ");
	    sqlBuffer.append("");
	    sqlBuffer.append("from    pub.[cl-master] cl ");
	    sqlBuffer.append("        join ");
	    sqlBuffer.append("        pub.mem_membership mm on cl.[client-no] = mm.client_number ");
	    sqlBuffer.append("        left outer join");
	    sqlBuffer.append("        pub.mem_membership_group mg on mg.membership_id = mm.membership_id     ");
	    sqlBuffer.append("          ");
	    sqlBuffer.append("where   surname not like '% ESTATE %' ");
	    sqlBuffer.append("and     mm.expiry_date >= ? ");
	    sqlBuffer.append("and     mm.profile_code <> ? ");

	    return sqlBuffer.toString();

	}
    }

    /**
     * Delete all existing records from Motor_News table
     * 
     * @throws RemoteException
     */
    private void removeAllExtractRecords() throws RemoteException {
	Connection connection = null;
	PreparedStatement statement = null;

	StringBuffer statementText = new StringBuffer();

	statementText.append("delete from pub.motor_news_extract");
	try {
	    connection = this.dataSource.getConnection();
	    statement = connection.prepareStatement(statementText.toString());
	    int recs = statement.executeUpdate();
	    LogUtil.log(this.getClass(), recs + " removed.");
	} catch (SQLException e) {
	    throw new RemoteException(statementText.toString(), e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
    }

    /**
     * Structured data type to carry delivery round summary results
     */
    private class MotorNewsSummary {
	private int annualReport;
	private int accessMembers;
	private int motorNews;
	private int errors;
	private int eligibleClients;
	private String summaryString;

	public MotorNewsSummary() {
	    this.setAnnualReport(0);
	    this.setMotorNews(0);
	    this.setAccessMembers(0);
	    this.setEligibleClients(0);
	    this.setSummaryString(" ");
	    this.setErrors(0);
	}

	public void setSummaryString(String s) {
	    this.summaryString = s;
	}

	public String getSummaryString() {
	    return summaryString;
	}

	public int getAnnualReport() {
	    return annualReport;
	}

	public int getMotorNews() {
	    return motorNews;
	}

	public int getErrors() {
	    return errors;
	}

	public void setAnnualReport(int annualReport) {
	    this.annualReport = annualReport;
	}

	public void setMotorNews(int motorNews) {
	    this.motorNews = motorNews;
	}

	public void setErrors(int errors) {
	    this.errors = errors;
	}

	public void setEligibleClients(int eligibleClients) {
	    this.eligibleClients = eligibleClients;
	}

	public void setAccessMembers(int accessMembers) {
	    this.accessMembers = accessMembers;
	}

	public int getEligibleClients() {
	    return this.eligibleClients;
	}

	public int getAccessMembers() {
	    return accessMembers;
	}

	public String toString() {
	    return "Journeys extract complete." + "\nJourneys            = " + this.motorNews + "\nAccess Members      = " + this.accessMembers + "\nEligible Clients    = " + this.eligibleClients + "\nAnnual Report       = " + this.annualReport + "\nErrors detected     = " + this.errors + this.summaryString;
	}

    }

}
