package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Properties;

import javax.ejb.Remote;

import com.ract.common.SystemException;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.user.User;

@Remote
public interface MembershipRenewalMgr {

    public Properties produceIndividualRenewalNotice(MembershipVO memVO, TransactionGroup transactionGroup, User user, PaymentTransactionMgr paymentTransactionMgr) throws SystemException, RemoteException;

    /**
     * Create and print a new renewal document for a given membership The
     * document will only be printed if the membership is within the normal
     * renewal period. Otherwise does nothing Does not check to see if there is
     * an existing renewal document
     * 
     * @param memVO
     *            The membership for which the document is to be printed.
     * @param user
     *            The user creating the document
     * @throws Exception
     */
    public void makeRenewalNotice(MembershipVO memVO, User user) throws Exception;

    /**
     * IF there was a previous renewal notice which has been cancelled in the
     * same period, create a new renewal notice Only create a replacement if the
     * original notice is present and cancelled
     * 
     * @param memVO
     *            The membership to which this applies
     * @param user
     *            User doing the transaction
     * @throws Exception
     */

    public void replaceRenewalNotice(MembershipVO memVO, User user) throws Exception;

    /**
     * IF there was a previous renewal notice which has been cancelled in the
     * same period, create a new renewal notice Only create a replacement if the
     * original notice is present and cancelled This version allows for change
     * of PA
     * 
     * @param oldPaMemVO
     *            The original membership - against which the renewal document
     *            would be held
     * @param newPaMemVO
     *            The new membership - against which to make the new document
     * @param user
     *            User doing the transaction
     * @throws Exception
     */

    public void replaceRenewalNotice(MembershipVO oldPaMemVO, MembershipVO newPaMemVO, User user) throws Exception;

    public void replaceRenewalNotice(MembershipVO oldPaMemVO, TransactionGroup transGroup, User user) throws Exception;

}
