package com.ract.membership;

import java.rmi.RemoteException;

import com.ract.common.ValueObject;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * <p>
 * Membership transaction implementation for a change product transaction.
 * </p>
 * 
 * @hibernate.subclass discriminator-value="CHANGE PRODUCT"
 * 
 * @author jyh
 * @version 1.0
 */
public class MemTransChangeProduct extends MembershipTransactionVO {

    public MemTransChangeProduct() {
	// default constructor
    }

    public MemTransChangeProduct(String transactionGroupTypeCode, String transactionTypeCode, User user, boolean rejoinTypeNew, MembershipVO oldMemVO, MembershipVO newMemVO, ProductVO selectedProduct, Interval membershipTerm, DateTime membershipGroupJoinDate, DateTime transDate, boolean isPrimeAddressee, DateTime preferredCommenceDate, DateTime groupExpiryDate) throws RemoteException {

	// Create a new transaction object for the membership transaction
	super(ValueObject.MODE_CREATE, transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, membershipGroupJoinDate);

	// set only what has changed

	this.setTransactionDate(transDate);
	this.setUsername(user.getUserID());
	this.setSalesBranchCode(user.getSalesBranchCode());
	this.setMembership(oldMemVO);
	this.setNewMembership(newMemVO);
	this.setPrimeAddressee(isPrimeAddressee);

	// Get old product
	ProductVO oldProductVO = oldMemVO.getProduct();

	// These should already be set this way according to the business rules
	// but do it any way in case the rule implementation has been changed
	// elsewhere.
	newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
	newMemVO.setAllowToLapse(Boolean.valueOf(false));
	newMemVO.setCreditPeriod(null);

	// join date can stay the same.

	// The membership expiry date is aligned with the group expiry date
	newMemVO.setExpiryDate(groupExpiryDate);

	newMemVO.setNextProductCode(null);
	// effective at next renewal
	newMemVO.setProduct(selectedProduct); // Sets the product effective date
					      // to today if the product has
					      // been changed.

	this.setProductEffectiveDate();

	if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(transactionTypeCode)) {

	    if (MembershipVO.STATUS_ONHOLD.equals(oldMemVO.getStatus())) {
		// transaction date
		this.setEffectiveDate(preferredCommenceDate);
	    } else {
		this.setEffectiveDate(oldMemVO.getExpiryDate());
	    }

	    this.setEndDate(newMemVO.getExpiryDate());

	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transactionTypeCode)) {

	    this.setEffectiveDate(preferredCommenceDate);
	    this.setEndDate(newMemVO.getExpiryDate());

	}

	// dd details?

	LogUtil.debug(this.getClass(), this.toString());

    }

}
