package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.CacheBase;
import com.ract.common.ExceptionHelper;
import com.ract.util.LogUtil;

/**
 * This class caches a list of discount types in memory. It uses the Singleton
 * design pattern. Use the following code to call the methods on this class.
 * DiscountTypeCache.getInstance().getDiscountType(discountTypeCode> );
 * DiscountTypeCache.getInstance().getDiscountTypeList();
 * 
 * @author T.Bakker
 * @created 2 April 2003
 * @version 1.0, 6 March 2002
 */

public class DiscountTypeCache extends CacheBase {
    /**
     * Holds a reference to an instance of this class. The class instance is
     * created when the Class is first referenced.
     */
    private static DiscountTypeCache instance = new DiscountTypeCache();

    /**
     * Implement the initialiseList method of the super class. Initialise the
     * list of cached items.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void initialiseList() throws RemoteException {
	// DiscountTypeHome home = MembershipEJBHelper.getDiscountTypeHome();
	Collection dataList = null;

	try {
	    dataList = MembershipEJBHelper.getMembershipMgr().getDiscountTypes();
	    if (dataList != null) {
		Iterator it = dataList.iterator();
		while (it.hasNext()) {
		    DiscountTypeVO item = (DiscountTypeVO) it.next();
		    LogUtil.debug(this.getClass(), "discountType=" + item.getDiscountTypeCode() + " " + item.getDescription());
		    this.add(item);
		}
	    }
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
	}
    }

    /**
     * Return a reference the instance of this class.
     * 
     * @return The instance value
     */
    public static DiscountTypeCache getInstance() {
	return instance;
    }

    /**
     * Return the specified discount type value object. Search the list of
     * products for one that has the same code. Use the getList() method to make
     * sure the list has been initialised.
     * 
     * @param discountTypeCode
     *            Description of the Parameter
     * @return The discountType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public DiscountTypeVO getDiscountType(String discountTypeCode) throws RemoteException {
	return (DiscountTypeVO) getItem(discountTypeCode);
    }

    /**
     * Return the list of discount types. Use the getList() method to make sure
     * the list has been initialised. A null or an empty list may be returned.
     * 
     * @return The discountTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getDiscountTypeList() throws RemoteException {
	return getItemList();
    }

}
