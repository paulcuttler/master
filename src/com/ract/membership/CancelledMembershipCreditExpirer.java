package com.ract.membership;

import java.io.File;
import java.io.FileWriter;
import java.util.Enumeration;
import java.util.Hashtable;

import com.ract.common.ExceptionHelper;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;

/**
 * Run the remove expired membership credits method from the command line.
 * 
 * @author rlg
 * @version 1.0
 */

public class CancelledMembershipCreditExpirer {

    private CancelledMembershipCreditExpirer(String url, String port, String outputDirectory) {
	try {
	    RACTPropertiesProvider.setContextURL(url, Integer.parseInt(port));
	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	    System.out.println("Membership Credit Expirer started " + new DateTime().formatLongDate());
	    Hashtable expiredCreditMemberships = membershipMgr.removeExpiredMembershipCredits();
	    String fileName = FileUtil.constructFileName(outputDirectory, null, FileUtil.EXTENSION_CSV);
	    FileWriter fileWriter = new FileWriter(new File(fileName));
	    Integer clientNumber = null;
	    Double creditRemoved = null;
	    StringBuffer creditLog = new StringBuffer();
	    Enumeration keys = expiredCreditMemberships.keys();
	    while (keys.hasMoreElements()) {
		clientNumber = (Integer) keys.nextElement();
		creditRemoved = (Double) expiredCreditMemberships.get(clientNumber);
		// write to string
		creditLog.append(clientNumber.toString());
		creditLog.append(FileUtil.SEPARATOR_COMMA);
		creditLog.append(CurrencyUtil.formatDollarValue(creditRemoved.doubleValue()));
		creditLog.append(FileUtil.NEW_LINE);
	    }
	    // writer lines to file
	    fileWriter.write(creditLog.toString());
	    fileWriter.flush();
	    fileWriter.close();

	    System.out.println("Memberships with credit removed: " + expiredCreditMemberships.size());
	} catch (Exception e) {
	    System.err.println(ExceptionHelper.getExceptionStackTrace(e));
	}
    }

    /**
     * The main program for the cancelled membership credit expirer class.
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args) {
	if (args.length != 3) {
	    System.out.println("Not enough command line arguments.");
	    System.out.println("Usage: CancelledMembershipCreditExpirer localhost 1099 /export/home/general/mem/credit/");
	    return;
	}
	CancelledMembershipCreditExpirer cmce = new CancelledMembershipCreditExpirer(args[0], args[1], args[2]);
    }

}
