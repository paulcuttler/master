package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.CacheBase;

/**
 * This class caches a list of membership types in memory. It uses the Singleton
 * design pattern. Use the following code to call the methods on this class.
 * 
 * MembershipTypeCache.getInstance().getMembershipType(<membershipTypeCode>);
 * MembershipTypeCache.getInstance().getMembershipTypeList();
 */

public class MembershipTypeCache extends CacheBase {

    private static MembershipTypeCache instance = new MembershipTypeCache();

    /**
     * Implement the getItemList method of the super class. Return the list of
     * cached items. Add items to the list if it is empty.
     */
    public void initialiseList() throws RemoteException {
	// log("Initialising MembershipType cache.");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection dataList = membershipMgr.getMembershipTypes();
	this.addAll(dataList);
	sort();
    }

    /**
     * Return a reference the instance of this class.
     */
    public static MembershipTypeCache getInstance() {
	return instance;
    }

    /**
     * Return the specified membership type value object. Search the list of
     * membership types for one that has the same code. Use the getList() method
     * to make sure the list has been initialised.
     */
    public MembershipTypeVO getMembershipType(String membershipTypeCode) throws RemoteException {
	return (MembershipTypeVO) this.getItem(membershipTypeCode);
    }

    /**
     * Return the list of membership types. Use the getList() method to make
     * sure the list has been initialised. A null or an empty list may be
     * returned.
     */
    public ArrayList getMembershipTypeList() throws RemoteException {
	return this.getItemList();
    }

}
