package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.util.CurrencyUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * Identifies the specific discount that was applied to the transaction.
 * 
 * @author hollidayj
 * @created 1 August 2002
 */
public class MembershipTransactionDiscount implements Serializable, Writable {

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "MembershipTransactionDiscount";

    /**
     * The code for the discount type that the transaction discount is
     * associated with.
     */
    private String discountTypeCode;

    public void setDiscountTypeCode(String discountTypeCode) throws RemoteException {
	if (discountTypeCode == null) {
	    throw new SystemException("Discount type code can not be null.");
	} else {
	    // validate the code
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    DiscountTypeVO discTypeVO = refMgr.getDiscountType(discountTypeCode);
	    if (discTypeVO == null) {
		throw new SystemException("Failed to construct MembershipTransactionDiscount. The discount type code '" + this.discountTypeCode + "' is not valid.");
	    }
	    this.discountTypeCode = discountTypeCode;
	    this.discountTypeVO = discTypeVO;
	}

    }

    /**
     * The discount type value object that the discount type code relates to.
     */
    private DiscountTypeVO discountTypeVO;

    /**
     * The amount of discount caclulated.
     */
    private double discountAmount;

    /**
     * Holds the reason why the operator gave an arbitrary discount amount.
     */
    private String discountReason;

    /**
     * Holds a description of how the discount amount was calculated. This is
     * set programatically when discounts are applied.
     */
    private String discountDescription;

    private Interval discountPeriod;

    private Collection discountAttributeList;

    public void addDiscountAttribute(TransactionFeeDiscountAttribute transactionFeeDiscountAttribute, MembershipTransactionVO memTransVO) throws ValidationException {
	if (discountAttributeList == null) {
	    this.discountAttributeList = new ArrayList();
	}
	LogUtil.debug(this.getClass(), "this=" + this);
	LogUtil.debug(this.getClass(), "memTransVO=" + memTransVO);
	LogUtil.debug(this.getClass(), "transactionFeeDiscountAttribute=" + transactionFeeDiscountAttribute);
	if (transactionFeeDiscountAttribute != null) {

	    if (MembershipDiscountAttribute.ATTRIBUTE_GM_MEMBERSHIP_NUMBER.equals(transactionFeeDiscountAttribute.getTransactionFeeDiscountAttributePK().getAttributeName())) {

		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

		// validate
		MembershipVO GMMembership = null;
		String membershipNumber = transactionFeeDiscountAttribute.getAttributeValue();

		try {
		    GMMembership = (MembershipVO) membershipMgr.findMembershipByClientNumber(new Integer(membershipNumber)).iterator().next();
		} catch (NoSuchElementException ex) {
		    throw new ValidationException("No GM membership exists with membership number " + membershipNumber, ex);
		} catch (Exception ex) {
		    throw new ValidationException("Unable to get GM membership with membership number " + membershipNumber, ex);
		}

		try {
		    if (!GMMembership.getRenewalMembershipTier().isEligibleForLoyaltyScheme()) {
			throw new ValidationException("GM member specified is not eligible for the loyalty scheme.");
		    }

		    if (!GMMembership.isActive()) {
			throw new ValidationException("GM members is not active.");
		    }

		} catch (RemoteException ex1) {
		    throw new ValidationException(ex1);
		}

		try {
		    MembershipVO oldMembership = memTransVO.getMembership();
		    // no membership that is not undone, cancelled or on hold,
		    // or not expired.
		    if (oldMembership != null && !MembershipVO.STATUS_UNDONE.equals(oldMembership.getStatus()) && // not
														  // undone
			    !MembershipVO.STATUS_CANCELLED.equals(oldMembership.getStatus()) && !MembershipVO.STATUS_ONHOLD.equals(oldMembership.getStatus()) && oldMembership.getProduct().includesRoadsideAssistance() && oldMembership.isActive()) {
			throw new ValidationException("The member receiving the free membership must not already have a current membership providing Roadside benefits.");
		    }
		} catch (RemoteException ex3) {
		    throw new ValidationException(ex3);
		}

	    }
	    this.discountAttributeList.add(transactionFeeDiscountAttribute);
	}

    }

    /**
     * Constructors ****************************
     * 
     * @param discTypeCode
     *            Description of the Parameter
     * @param discAmount
     *            Description of the Parameter
     * @param discReason
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    /**
     * Constuct the membership transaction discount given a discount type code,
     * discount amount and discount reason. Fetch the discount type value object
     * for the discount type code. If the specified discount amount is null then
     * use the discount amount from the discount type value object. Copy the
     * 'user defined amount' flag from the discount type value object.
     * 
     * @param discTypeCode
     *            Description of the Parameter
     * @param discAmount
     *            Description of the Parameter
     * @param discReason
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTransactionDiscount(String discTypeCode, double discAmount, String discReason) throws RemoteException {
	this.setDiscountTypeCode(discTypeCode);
	// this.discountTypeCode = discTypeCode;
	this.discountAmount = NumberUtil.roundDouble(discAmount, 2);
	this.discountReason = discReason;
	this.discountPeriod = this.discountTypeVO.getDiscountPeriod();
    }

    /**
     * Construct the membership transaction discount given a discount type value
     * object, discount amount and discount reason. If the specified discount
     * amount is null then use the discount amount from the discount type value
     * object. Copy the 'user defined amount' flag from the discount type value
     * object.
     * 
     * @param discTypeVO
     *            Description of the Parameter
     * @param discAmount
     *            Description of the Parameter
     * @param discReason
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTransactionDiscount(DiscountTypeVO discTypeVO, double discAmount, String discReason) throws RemoteException {
	if (discTypeVO == null) {
	    throw new SystemException("Failed to create MembershipTransactionDiscount. The discount type cannot ne null.");
	}
	this.discountTypeVO = discTypeVO;
	this.discountTypeCode = this.discountTypeVO.getDiscountTypeCode();
	this.discountAmount = NumberUtil.roundDouble(discAmount, 2);
	this.discountReason = discReason;
	this.discountPeriod = this.discountTypeVO.getDiscountPeriod();
    }

    public MembershipTransactionDiscount(Node discNode) throws RemoteException {
	if (discNode == null || !discNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
	    throw new SystemException("Failed to create MembershipTransactionDiscount from XML node. The node is not valid.");
	}

	NodeList elementList = discNode.getChildNodes();
	Node elementNode = null;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();
	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;
	    }

	    if ("discountAmount".equals(nodeName) && valueText != null) {
		this.discountAmount = Double.parseDouble(valueText);
	    } else if ("discountDescription".equals(nodeName) && valueText != null) {
		this.discountDescription = valueText;
	    } else if ("discountPeriod".equals(nodeName) && valueText != null) {
		try {
		    this.discountPeriod = new Interval(valueText);
		} catch (java.text.ParseException pe) {
		    throw new SystemException("The discount period '" + valueText + "' is not a valid interval specification.", pe);
		}
	    } else if ("discountReason".equals(nodeName) && valueText != null) {
		this.discountReason = valueText;
	    } else if ("discountTypeCode".equals(nodeName) && valueText != null) {
		this.setDiscountTypeCode(valueText);
		// this.discountTypeCode = valueText;
	    }
	}
    }

    /*
     * Getter methods ****************************
     */

    /**
     * @return String
     */
    public String getDiscountCode() {
	return this.discountTypeCode;
    }

    /**
     * Return the discount value object associated with the transaction
     * discount.
     * 
     * @return The discountTypeVO value
     */
    public DiscountTypeVO getDiscountTypeVO() throws RemoteException {
	return this.discountTypeVO;
    }

    /**
     * Return the reason why the discount was given.
     * 
     * @return String
     */
    public String getDiscountReason() {
	String reason = "";
	if (this.discountReason == null || this.discountReason.length() < 1) {
	    reason = this.getDiscountCode() + " - " + CurrencyUtil.formatDollarValue(this.discountAmount);
	} else {
	    reason = this.discountReason;
	}
	return reason;
    }

    /**
     * Return the amount of discount given. This may be the fixed discount
     * amount from the discount. Or it may be the calculated discount amount
     * derived from the discount percentage. Or it may be the discretionary
     * discount amount entered by the operator.
     * 
     * @return double
     */
    public double getDiscountAmount() {
	return this.discountAmount;
    }

    /**
     * Return the discount description. If no explicit description has been set
     * then return a default description
     * 
     * @return The discountDescription value
     */
    public String getDiscountDescription() {
	String discDesc = this.discountDescription;
	if (discDesc == null) {
	    StringBuffer sb = new StringBuffer();
	    sb.append(getDiscountReason());
	}
	return discDesc;
    }

    /**
     * Return a copy of the discount period. May return null if the discount
     * type is an automatic type of discount or the discount type did not
     * specify a discount period.
     * 
     * @return The discountPeriod value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Interval getDiscountPeriod() throws RemoteException {
	if (this.discountPeriod != null) {
	    try {
		return this.discountPeriod.copy();
	    } catch (Exception e) {
		throw new SystemException("Failed to copy discount period : " + e.getMessage(), e);
	    }
	} else {
	    return null;
	}
    }

    public Collection getDiscountAttributeList(Integer transID, Integer feeSpecID) {
	LogUtil.debug(this.getClass(), "this=" + this);
	LogUtil.debug(this.getClass(), "transID=" + transID);
	LogUtil.debug(this.getClass(), "feeSpecID=" + feeSpecID);
	LogUtil.debug(this.getClass(), "discountAttributeList=" + discountAttributeList);

	if (discountAttributeList == null && transID != null && feeSpecID != null) {
	    try {
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		discountAttributeList = membershipMgr.getTransactionFeeDiscountAttributes(transID, feeSpecID, this.discountTypeCode);
	    } catch (RemoteException ex) {
		LogUtil.warn(this.getClass(), "Unable to get discount attributes " + ex);
	    }
	}
	return discountAttributeList;
    }

    /**
     * Setter methods ****************************
     * 
     * @param amt
     *            The new discountAmount value
     */

    public void setDiscountAmount(double amt) {
	this.discountAmount = amt;
    }

    /**
     * Sets the discountReason attribute of the MembershipTransactionDiscount
     * object
     * 
     * @param discountReason
     *            The new discountReason value
     */
    public void setDiscountReason(String discountReason) {
	this.discountReason = discountReason;
    }

    /**
     * Sets the discountDescription attribute of the
     * MembershipTransactionDiscount object
     * 
     * @param discDesc
     *            The new discountDescription value
     */
    public void setDiscountDescription(String discDesc) {
	this.discountDescription = discDesc;
    }

    /**
     * Sets the discountPeriod attribute of the MembershipTransactionDiscount
     * object
     * 
     * @param discPeriod
     *            The new discountPeriod value
     */
    public void setDiscountPeriod(Interval discPeriod) {
	this.discountPeriod = discPeriod;
    }

    public void setDiscountAttributeList(Collection discountAttributeList) {
	this.discountAttributeList = discountAttributeList;
    }

    /**
     * Returns the list of postings that are the source of this discount. NOTE
     * This only applies to TYPE_UNEARNED_CREDIT. All others will throw a
     * ValidationException.
     */
    // public Collection getUnearnedCreditPostingList()
    // throws ValidationException
    // {
    // if(DiscountType.TYPE_UNEARNED_CREDIT.equals(this.discountTypeCode))
    // {
    // return this.postingList;
    // }
    // else
    // {
    // throw new
    // ValidationException("Invalid Discount Type for getPostingList.");
    // }
    // }

    /**
     * Sets the list of postings that are the source of this discount. NOTE This
     * only applies to TYPE_UNEARNED_CREDIT. All others will throw a
     * ValidationException.
     */
    // public void setUnearnedCreditPostingList(Collection postingList)
    // throws ValidationException
    // {
    // if(DiscountType.TYPE_UNEARNED_CREDIT.equals(this.discountTypeCode))
    // {
    // this.postingList = postingList;
    // }
    // else
    // {
    // throw new
    // ValidationException("Invalid Discount Type for setPostingList.");
    // }
    // }

    /**
     * Adds a posting to the list of postings that are the source of this
     * discount. NOTE This only applies to TYPE_UNEARNED_CREDIT. All others will
     * throw a ValidationException.
     */
    // public void addPosting(PayableItemPostingVO posting)
    // throws ValidationException
    // {
    // if(DiscountType.TYPE_UNEARNED_CREDIT.equals(this.discountTypeCode))
    // {
    // this.postingList.add(posting);
    // }
    // else
    // {
    // throw new ValidationException("Invalid Discount Type for addPosting.");
    // }
    // }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("discountTypeCode=" + this.discountTypeCode + "\n");
	desc.append("discountDescription=" + this.discountDescription + "\n");
	desc.append("discountReason=" + this.discountReason + "\n");
	return desc.toString();
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    /**
     * Description of the Method
     * 
     * @param cw
     *            Description of the Parameter
     * @param deepWrite
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("discountAmount", NumberUtil.formatValue(this.discountAmount, "########0.00"));
	cw.writeAttribute("discountDescription", this.discountDescription);
	cw.writeAttribute("discountPeriod", this.discountPeriod);
	cw.writeAttribute("discountReason", this.discountReason);
	cw.writeAttribute("discountTypeCode", this.discountTypeCode);
	cw.endClass();
    }
}
