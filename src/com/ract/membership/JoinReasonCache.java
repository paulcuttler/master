package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.ract.common.CacheBase;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ReferenceDataVO;

/**
 * This class caches a list of products in memory. It uses the Singleton design
 * pattern. Use the following code to call the methods on this class.
 * 
 * ProductCache.getInstance().getProduct(<membershipTypeCode>);
 * ProductCache.getInstance().getProductList();
 */

public class JoinReasonCache extends CacheBase {
    private static JoinReasonCache instance = new JoinReasonCache();

    /**
     * Implement the getItemList method of the super class. Return the list of
     * cached items. Add items to the list if it is empty.
     */
    public void initialiseList() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	this.addAll(commonMgr.getReferenceDataListByType(ReferenceDataVO.REF_TYPE_JOIN_REASON));
    }

    /**
     * Return a reference the instance of this class.
     */
    public static JoinReasonCache getInstance() {
	return instance;
    }

    /**
     * Return the specified join reason as a ReferenceData value object.
     * Construct the search key in the same way as the ReferenceDataVO.getKey()
     * method does.
     */
    public ReferenceDataVO getJoinReason(String rCode) throws RemoteException {
	String tmpKey = ReferenceDataVO.REF_TYPE_JOIN_REASON + rCode;
	return (ReferenceDataVO) this.getItem(tmpKey);
    }

    /**
     * Return the list of Reference Data. Use the getList() method to make sure
     * the list has been initialised. A null or an empty list may be returned.
     */
    public ArrayList getJoinReasonList() throws RemoteException {
	return this.getItemList();
    }

}
