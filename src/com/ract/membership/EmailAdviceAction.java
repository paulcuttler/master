package com.ract.membership;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Properties;

import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.ract.client.Client;
import com.ract.client.ClientNote;
import com.ract.client.ClientTransaction;
import com.ract.client.ClientVO;
import com.ract.client.NoteType;
import com.ract.common.BusinessType;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.membership.reporting.MembershipAdviceRequest;
import com.ract.membership.reporting.MembershipDDRequest;
import com.ract.membership.reporting.MembershipQuoteRequest;
import com.ract.membership.reporting.MembershipRenewalAdviceRequest;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.security.SecurityHelper;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.ReportUtil;

/**
 * Send member email advice and record note.
 * 
 * @author newtong
 * 
 */
public class EmailAdviceAction extends EmailAdviceBaseAction {

	private String emailTo;
	private String message;
	private boolean copyToMe;
	private boolean updateEmail;
	private String formName;

	private String noticeAction;

	private File upfile;
	private String upfileFileName;
	private boolean result;

	@InjectEJB(name = "MembershipMgrBean")
	private MembershipMgrLocal membershipMgr;

	public File getUpfile() {
		return upfile;
	}

	public void setUpfile(File upfile) {
		this.upfile = upfile;
	}

	public String getUpfileFileName() {
		return upfileFileName;
	}

	public void setUpfileFileName(String upfileFileName) {
		this.upfileFileName = upfileFileName;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	@InjectEJB(name = "PaymentTransactionMgrBean")
	PaymentTransactionMgrLocal paymentTransactionMgrLocal;

	@InjectEJB(name = "MembershipRenewalMgrBean")
	MembershipRenewalMgrLocal renewalMgrLocal;

	@InjectEJB(name = "PaymentMgrBean")
	PaymentMgrLocal paymentMgrLocal;

	public String execute() throws Exception {

		try {
			
			if (upfile != null)
				membershipMgr.uploadECRRecipt(upfile, upfileFileName);
			
			// prepare report based on adviceType
			ReportRequestBase rep = prepareReport();

			// prepare email content based on adviceType
			MailMessage message = prepareEmail();

			// send email
			ReportUtil.emailReport(rep, false, message);

			// update renewal advice document, if applicable
			updateRenewalDocumentDetails();

			// save note
			writeClientAdviceNote();

			// update client email, if requested
			updateClientEmail();

			this.addActionMessage("Email advice has been successfully sent.");

			// suppress form
			this.setSuccess(true);

		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), e);
			this.addActionError("Unable to generate email advice: " + e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}

	/**
	 * Update renewal document email particulars, if applicable
	 * 
	 * @throws RemoteException
	 */
	private void updateRenewalDocumentDetails() throws RemoteException {

		if (!this.getAdviceType().equals(SystemParameterVO.EMAIL_ADVICE_RENEWAL)) {
			return;
		}

		MembershipDocument document = this.getMembershipVO().getCurrentRenewalDocument();
		document.setEmailAddress(this.getEmailTo());
		document.setEmailed(true);
		document.setEmailDate(new DateTime());

		membershipMgrLocal.updateRenewalDocument(document);

	}

	/**
	 * Prepare email advice based on advice type.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	private MailMessage prepareEmail() throws RemoteException {
		String subject = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, this.getAdviceType() + "EMAILSUBJECT");

		Hashtable<String, String> files = new Hashtable<String, String>();
		
		if (upfile != null)
			files.put(this.upfileFileName, "/ract/app/ecr_receipts" + this.upfileFileName);
		MailMessage message = new MailMessage();
		message.setRecipient(this.getEmailTo());
		if (this.copyToMe) {
			message.setBcc(this.getUser().getEmailAddress());
		}

		message.setMessage(this.getMessage());
		message.setSubject(subject);
		message.setHtml(false);
//		message.setFrom(this.getUser().getEmailAddress());
		message.setFrom(commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "NOREPLYEMAIL"));
		message.setFiles(files);

		return message;
	}

	/**
	 * Prepare report PDF based on requested advice type.
	 * 
	 * @return
	 * @throws Exception
	 */
	private ReportRequestBase prepareReport() throws Exception {

		ReportRequestBase rep = null;

		User user = SecurityHelper.getRoadsideUser();
		String printGroup = user.getPrinterGroup();

		if (this.getAdviceType().equals(SystemParameterVO.EMAIL_ADVICE_DD)) {
			DirectDebitSchedule ddSchedule = null;
			if (this.getDirectDebitScheduleId() != null) {
				ddSchedule = paymentMgrLocal.getDirectDebitSchedule(this.getDirectDebitScheduleId());
			} else { // get most recent schedule
				ddSchedule = this.getMembershipVO().getCurrentDirectDebitSchedule();
			}
			rep = new MembershipDDRequest(user, printGroup, this.getMembershipVO(), (ddSchedule != null), ddSchedule, true);

		} else if (this.getAdviceType().equals(SystemParameterVO.EMAIL_ADVICE_RENEWAL)) {

			if (this.getNoticeAction().equals(PrintRenewalAction.ACTION_CREATE_RENEWAL_DOCUMENT)) {
				// cancel existing renewal notice
				MembershipDocument document = this.getMembershipVO().getCurrentRenewalDocument();
				try {
					membershipMgrLocal.removeRenewalDocument(document.getDocumentId(), MembershipMgr.MODE_CANCEL, paymentTransactionMgrLocal, false);
					paymentTransactionMgrLocal.commit();
				} catch (RenewalNoticeException e) {
					paymentTransactionMgrLocal.rollback();
					addActionError(e.getMessage());
					throw new RenewalNoticeException("Unable to remove renewal document");
				}
			}

			TransactionGroup renewalTransactionGroup = TransactionGroup.getRenewalTransactionGroup(this.getMembershipVO(), user, this.getMembershipVO().getExpiryDate());
			Properties renewalProperties = renewalMgrLocal.produceIndividualRenewalNotice(this.getMembershipVO(), renewalTransactionGroup, user, null);
			rep = new MembershipRenewalAdviceRequest(user, printGroup, renewalProperties, renewalTransactionGroup, true);

		} else if (this.getAdviceType().equals(SystemParameterVO.EMAIL_ADVICE_QUOTE)) {
			rep = new MembershipQuoteRequest(this.getUser().getUserID(), printGroup, this.getQuoteNumber(), true);

		} else if (this.getAdviceType().equals(SystemParameterVO.EMAIL_ADVICE_GENERAL)) {
			rep = new MembershipAdviceRequest(user, printGroup, this.getMembershipVO(), true);
//			if (!rep.isReportRequired())
//				rep = null;
		}

		if (rep != null) {
			// report cache and time stamp
			String reportCache = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_REPORT_CACHE);
			rep.setDestinationFileName(reportCache + "/" + this.getClientNumber() + "_" + Long.toString(new DateTime().getTime()) + "." + FileUtil.EXTENSION_PDF);
			rep.setRemoveFile(false);

			String attachmentName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, this.getAdviceType() + "EMAILATTACHMENTNAME");
			attachmentName += "." + FileUtil.EXTENSION_PDF;
			rep.setReportName(attachmentName);
		}

		return rep;
	}

	/**
	 * Write client advice note.
	 * 
	 * @throws RemoteException
	 */
	private void writeClientAdviceNote() throws RemoteException {
		ClientNote note = new ClientNote();
		note.setClientNumber(this.getClientNumber());
		note.setNoteType(NoteType.TYPE_INFORMATION);
		note.setCreated(new DateTime());
		note.setCreateId(this.getUser().getUserID());
		note.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
		note.setNoteText("Sent client email advice of type '" + this.getAdviceType() + "'");

		clientMgrLocal.createClientNote(note);
	}

	/**
	 * If requested, update client email and record history.
	 * 
	 * @throws RemoteException
	 */
	private void updateClientEmail() throws RemoteException {

		if (!this.isUpdateEmail()) {
			return;
		}

		ClientVO client = this.getClientVO();
		client.setEmailAddress(this.getEmailTo());

		// Save it away
		String userID = this.getUser().getUserID();
		String transactionReason = Client.HISTORY_UPDATE;
		String salesBranchCode = this.getUser().getSalesBranchCode();
		String comment = "Email Address updated";

		ClientTransaction clientTx = new ClientTransaction(ClientTransaction.TRANSACTION_UPDATE, client, userID, transactionReason, salesBranchCode, comment);
		clientTx.submit();
	}

	@RequiredStringValidator(message = "Please provide an email address", shortCircuit = true)
	@EmailValidator(message = "Please provide a valid email address")
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailTo() {
		return emailTo;
	}

	@RequiredStringValidator(message = "Please provide a message")
	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setCopyToMe(boolean copyToMe) {
		this.copyToMe = copyToMe;
	}

	public boolean isCopyToMe() {
		return copyToMe;
	}

	public void setNoticeAction(String noticeAction) {
		this.noticeAction = noticeAction;
	}

	public String getNoticeAction() {
		return noticeAction;
	}

	public boolean isUpdateEmail() {
		return updateEmail;
	}

	public void setUpdateEmail(boolean updateEmail) {
		this.updateEmail = updateEmail;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

}
