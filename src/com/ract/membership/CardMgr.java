package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.EJBObject;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public interface CardMgr extends EJBObject {
    public final String COUNT_CARD_SUCCESS = "Successful Cards";

    public final String COUNT_CARD_FAILURE = "Failed Cards";

    public Hashtable doCardExtract(ArrayList productList, String userId, boolean eligibleLoyaltyScheme, boolean newAccessCardsOnly, boolean vehicleBasedCardsOnly) throws RemoteException;
}
