package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.ServletUtil;

@Entity
@Table(name = "[mem_profile]")
public class MembershipProfileVO extends ValueObject implements Writable, CachedItem, Comparable {

    public static final String PROFILE_CLIENT = "Client";

    public static final String PROFILE_RACT_STAFF = "RACT Staff";

    public static final String PROFILE_NON_MOTORING = "Non-Motoring";

    public static final String STATUS_Active = "Active";

    public static final String STATUS_Inactive = "Inactive";

    public static final String STATUS_LIFE = "Life";

    public static final String PROFILE_GIFTED_INSURANCE = "Gifted Insurance";

    public static final String PROFILE_GIFTED_TRAVEL = "Gifted Travel";

    public static final String PROFILE_FCM = "FCM";
    public static final String PROFILE_FDC = "FDC";

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "MembershipProfile";

    /**
     * A unique code to identify each profile.
     */
    @Id
    @Column(name = "profile_code")
    protected String profileCode;

    /**
     * A description of the profile stating what it is for and what impact it
     * has on the way the system processes transactions.
     */
    @Column(name = "description")
    protected String description;

    /**
     * A suffix to append to the customers name. Can be used for appending to
     * RACT staff names on membership cards.
     */
    @Column(name = "name_suffix")
    protected String nameSuffix;

    /**
     * Does the operator have to have administrator privileges to apply this
     * profile?
     */
    @Column(name = "admin_only")
    protected boolean adminOnly;

    /**
     * Is this profile active or inactive?
     */
    @Column(name = "profile_status")
    protected String profileStatus;

    private boolean groupable;

    public boolean isGroupable() {
	return groupable;
    }

    public void setGroupable(boolean groupable) {
	this.groupable = groupable;
    }

    /**
     * A list of discounts that are available to members with this profile.
     * These discounts are in addition to member recurring discounts and
     * automatic discounts.
     */
    @Transient
    protected Collection discountList;

    /*************************** Constructor ****************************/
    /**
     * Default constructor.
     */
    public MembershipProfileVO() {

    }

    public MembershipProfileVO(String profileCode, String desc, String nameSuffix, boolean adminOnly, String profileStatus, Collection discList) {
	this.profileCode = profileCode;
	this.description = desc;
	this.nameSuffix = nameSuffix;
	this.adminOnly = adminOnly;
	this.profileStatus = profileStatus;
	this.discountList = discList;
    }

    /*************************** Getter methods ****************************/

    public String getProfileCode() {
	return profileCode;
    }

    public String getDescription() {
	return description;
    }

    public String getNameSuffix() {
	return this.nameSuffix;
    }

    public boolean isAdminOnly() {
	return adminOnly;
    }

    public String getProfileStatus() {
	return profileStatus;
    }

    public Collection getDiscountList() throws RemoteException {
	if (this.discountList == null & this.profileCode != null) {
	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	    this.discountList = membershipMgr.getProfileDiscountList(this.profileCode);
	}
	return this.discountList;
    }

    /*************************** Setter methods ****************************/

    public void setProfileCode(String profileCode) throws RemoteException {
	checkReadOnly();
	this.profileCode = profileCode;
    }

    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    public void setNameSuffix(String suffix) throws RemoteException {
	checkReadOnly();
	this.nameSuffix = suffix;
    }

    public void setDiscountList(Collection list) throws RemoteException {
	checkReadOnly();
	this.discountList = list;
    }

    public void setProfileStatus(String profileStatus) throws RemoteException {
	checkReadOnly();
	this.profileStatus = profileStatus;
    }

    public void setAdminOnly(boolean adminOnly) throws RemoteException {
	checkReadOnly();
	this.adminOnly = adminOnly;
    }

    protected void validateMode(String newMode) throws ValidationException {
    }

    public MembershipProfileVO copy() {
	MembershipProfileVO profileVO = new MembershipProfileVO(this.profileCode, this.description, this.nameSuffix, this.adminOnly, this.profileStatus, this.discountList);

	return profileVO;
    }

    public void updateFromRequest(HttpServletRequest request) throws RemoteException {
	this.setProfileCode(request.getParameter("profileCode"));
	this.setDescription(request.getParameter("description"));
	this.setProfileStatus(request.getParameter("profileStatus"));
	this.setNameSuffix(request.getParameter("nameSuffix"));
	this.setAdminOnly(ServletUtil.getBooleanParam("adminOnly", request));
    }

    /********************** CachedItem interface methods ****************/

    /**
     * Return the membership transaction type code as the cached item key.
     */
    public String getKey() {
	return getProfileCode();
    }

    /**
     * Determine if the specified key matches the this key. Return true if both
     * the specified key and this key are null.
     */
    public boolean keyEquals(String key) {
	String myKey = getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the specified key is not null so this wont throw a
	    // null pointer exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("adminOnly", this.adminOnly);
	cw.writeAttribute("description", this.description);
	cw.writeAttributeList("discountList", this.discountList);
	cw.writeAttribute("nameSuffix", this.nameSuffix);
	cw.writeAttribute("profileCode", this.profileCode);
	cw.writeAttribute("profileStatus", this.profileStatus);
	cw.endClass();
    }

    /************************ Comparable interface methods **********************/
    /**
     * Compare this product sort order to another products sort order
     */
    public int compareTo(Object obj) {
	if (obj != null && obj instanceof MembershipProfileVO) {
	    MembershipProfileVO profileVO = (MembershipProfileVO) obj;
	    return this.profileCode.compareToIgnoreCase(profileVO.getProfileCode());
	} else {
	    return 0;
	}
    }

    public boolean equals(Object obj) {
	if (obj instanceof MembershipProfileVO) {
	    MembershipProfileVO that = (MembershipProfileVO) obj;
	    return ((this.profileCode.compareToIgnoreCase(that.getProfileCode()) == 0));
	}
	return false;
    }

}