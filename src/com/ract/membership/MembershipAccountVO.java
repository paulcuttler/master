package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import com.ract.common.Account;
import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.Writable;
import com.ract.util.ServletUtil;

@Entity
@Table(name = "mem_membership_account")
public class MembershipAccountVO
// extends ValueObject
	implements CachedItem, Writable, Comparable, Account, Serializable {

    // by description
    public static final String MEMBERSHIP_MISC_ACCOUNT = "Miscellaneous discount expense account";

    // move to parameters
    public static final String MEMBERSHIP_MISC_ACCOUNT_COST_CENTRE = "03";

    // public static final String MEMBERSHIP_DUMMY_ACCOUNT = "Dummy account";

    // public static final String MEMBERSHIP_WRITE_OFF_ACCOUNT =
    // "Write-off account";

    // public static final String MEMBERSHIP_REFUND_ACCOUNT = "Refund account";

    public static final String MEMBERSHIP_REFUND_FEE_ACCOUNT = "Refund fee account";

    public static final String MEMBERSHIP_TRANSFER_ACCOUNT = "Transfer account";

    public static final String WRITABLE_CLASSNAME = "MembershipAccount";

    /**
     * A unique ID for the account in the membership system. Not using the
     * account number as this is the attribute that may change.
     */
    @Id
    @Column(name = "account_id")
    protected Integer accountID;

    /**
     * A title for the account to make it easier to understand what it is for.
     */
    @Column(name = "account_title")
    protected String accountTitle;

    /**
     * The actual account number in the finance system.
     */
    @Column(name = "account_number")
    protected String accountNumber;

    /**
     * The cost center to use when posting to the finance system. May be one of
     * the defined cost centers in the database, null if no cost center is
     * specified or either one of the special identifiers. The special
     * identifiers are "Member branch cost center" which means the cost center
     * of the branch that owns the membership or "Sales Branch cost center"
     * which means the cost center of the branch that did the transaction.
     */
    @Column(name = "cost_center")
    protected String costCenter;

    @Column(name = "sub_account")
    protected String subAccount;

    /**
     * Indicates if this account is the GST account. There should only be one
     * account with this flag set.
     */
    @Column(name = "gst_account")
    protected boolean gstAccount;

    /**
     * Indicates if this account is the membership clearing account. There
     * should only be one account with this flag set.
     */
    @Column(name = "clearing_account")
    protected boolean clearingAccount;

    /**
     * Indicates if this account is the direct debit clearing account. There
     * should only be one account with this flag set.
     */
    @Column(name = "dd_clearing_account")
    protected boolean ddClearingAccount;

    @Column(name = "suspense_account")
    protected boolean suspenseAccount;

    protected boolean taxable;

    private boolean writeOff;

    public boolean isWriteOff() {
	return writeOff;
    }

    public void setWriteOff(boolean writeOff) {
	this.writeOff = writeOff;
    }

    /**
     * Default constructor
     */
    public MembershipAccountVO() {

    }

    public MembershipAccountVO(String accNumber, String costCentre, String subAccount) {
	this.accountNumber = accNumber;
	this.costCenter = costCentre;
	this.subAccount = subAccount;
    }

    public MembershipAccountVO(Integer accID, String accTitle, String accNumber, String costCentre, String subAccount, boolean clearingAccount, boolean suspenseAccount, boolean gstAccount, boolean taxable, boolean writeoff) {
	this.accountID = accID;
	this.accountTitle = accTitle;
	this.accountNumber = accNumber;
	this.costCenter = costCentre;
	this.subAccount = subAccount;
	this.clearingAccount = clearingAccount;
	this.suspenseAccount = suspenseAccount;
	this.gstAccount = gstAccount;
	this.taxable = taxable;
	this.writeOff = writeoff;
    }

    public Integer getAccountID() {
	return accountID;
    }

    public String getAccountNumber() {
	return accountNumber;
    }

    public String getSubAccountNumber() {
	return this.subAccount;
    }

    public String getAccountTitle() {
	return accountTitle;
    }

    public boolean isClearingAccount() {
	return clearingAccount;
    }

    public boolean isDirectDebitClearingAccount() {
	return this.ddClearingAccount;
    }

    public boolean isTaxable() {
	return this.taxable;
    }

    public boolean isSuspenseAccount() {
	return this.suspenseAccount;
    }

    public String getCostCenter() {
	return costCenter;
    }

    public boolean isGstAccount() {
	return gstAccount;
    }

    public void setAccountID(Integer accountID) throws RemoteException {
	this.accountID = accountID;
    }

    public void setAccountNumber(String accountNumber) throws RemoteException {
	this.accountNumber = accountNumber;
    }

    public void setAccountTitle(String accountTitle) throws RemoteException {
	this.accountTitle = accountTitle;
    }

    public void setClearingAccount(boolean clearingAccount) throws RemoteException {
	this.clearingAccount = clearingAccount;
    }

    public void setTaxable(boolean taxable) throws RemoteException {
	this.taxable = taxable;
    }

    public void setDirectDebitClearingAccount(boolean ddClearingAccount) throws RemoteException {
	this.ddClearingAccount = ddClearingAccount;
    }

    public void setSuspenseAccount(boolean suspenseAccount) throws RemoteException {
	this.suspenseAccount = suspenseAccount;
    }

    public void setCostCenter(String costCenter) throws RemoteException {
	this.costCenter = costCenter;
    }

    public void setGstAccount(boolean gstAccount) throws RemoteException {
	this.gstAccount = gstAccount;
    }

    public void setSubAccount(String subAccount) throws RemoteException {
	this.subAccount = subAccount;
    }

    public void updateFromRequest(HttpServletRequest request) throws RemoteException {
	this.setAccountID(ServletUtil.getIntegerParam("accountId", request));
	this.setAccountNumber(ServletUtil.getStringParam("accountNumber", request));
	this.setAccountTitle(ServletUtil.getStringParam("accountTitle", request));
	this.setGstAccount(ServletUtil.getBooleanParam("isGstAccount", request));
	this.setClearingAccount(ServletUtil.getBooleanParam("isClearingAccount", request));
	this.setCostCenter(ServletUtil.getStringParam("costCentre", request));
	this.setSubAccount(ServletUtil.getStringParam("subAccount", request));
	// this.setDirectDebitClearingAccount(ServletUtil.getBooleanParam("isDirectDebit",request));
	this.setSuspenseAccount(ServletUtil.getBooleanParam("isSuspenseAccount", request));
	// this.setTaxable(ServletUtil.getBooleanParam("isTaxable",request));
    }

    public String toString() {
	StringBuffer sb = new StringBuffer();
	if (this.accountID != null) {
	    sb.append("accountID=" + accountID + "\n");
	    sb.append("accountNumber=" + accountNumber + "\n");
	    sb.append("accountTitle=" + accountTitle + "\n");
	    sb.append("clearingAccount=" + clearingAccount + "\n");
	    sb.append("costCenter=" + costCenter + "\n");
	    sb.append("ddClearingAccount=" + ddClearingAccount + "\n");
	    sb.append("gstAccount=" + gstAccount + "\n");
	    sb.append("subAccount=" + subAccount + "\n");
	    sb.append("suspenseAccount=" + suspenseAccount + "\n");
	    return sb.toString();
	} else {
	    sb.append("accountNumber=" + accountNumber + "\n");
	    sb.append("costCenter=" + costCenter + "\n");
	    sb.append("subAccount=" + subAccount + "\n");
	}
	return sb.toString();
    }

    @Transient
    public String getKey() {
	if (this.accountID != null) {
	    return this.accountID.toString();
	} else {
	    return null;
	}
    }

    public boolean keyEquals(String key) {
	String myKey = getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {

    }

    /************************ Comparable interface methods **********************/
    /**
     * Compare this product sort order to another products sort order
     */
    public int compareTo(Object obj) {

	if (obj != null && obj instanceof MembershipAccountVO) {

	    MembershipAccountVO that = (MembershipAccountVO) obj;

	    return that.getAccountID().compareTo(this.accountID);
	} else {
	    return 0;
	}
    }

    public boolean equals(Object obj) {
	boolean equals = false;
	if (obj != null && obj instanceof MembershipAccountVO) {
	    MembershipAccountVO accountVO = (MembershipAccountVO) obj;
	    equals = ((accountVO.getAccountID().compareTo(this.accountID)) == 0);
	}
	return equals;
    }

    public MembershipAccountVO copy() {
	MembershipAccountVO memAccountVO = new MembershipAccountVO(this.accountID, this.accountTitle, this.accountNumber, this.costCenter, this.subAccount, this.clearingAccount, this.suspenseAccount, this.gstAccount, this.taxable, this.writeOff);
	try {
	    memAccountVO.setDirectDebitClearingAccount(this.ddClearingAccount);
	} catch (RemoteException e) {
	    /** @todo this method should pass up the exception */
	}
	return memAccountVO;
    }

}
