package com.ract.membership;

/**
 * Title:        Master Project
 * Description:  Provides methods to generate TransactionGroup type objects
 *               for the primary membership transactions.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author  T. Bakker
 * @version 1.0
 */

import java.rmi.RemoteException;

import com.ract.user.User;

public class TransactionGroupFactory {
    public static TransactionGroup getTransactionGroupForChangeProductOption(User user) throws RemoteException {
	return new TransactionGroupChangeProductOptionImpl(user);
    }

    public static TransactionGroup getTransactionGroupForHold(User user) throws RemoteException {
	return new TransactionGroupHoldImpl(user);
    }

    public static TransactionGroup getTransactionGroupForCancel(User user) throws RemoteException {
	return new TransactionGroupCancelImpl(user);
    }

}
