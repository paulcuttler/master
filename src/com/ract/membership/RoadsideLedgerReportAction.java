package com.ract.membership;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.SystemParameterVO;
import com.ract.payment.PaymentMgrLocal;
import com.ract.util.LogUtil;

/**
 * Report on Roadside ledger amounts for 2310 vs approved vouchers vs credit
 * amounts. Includes monthly breakdown if a variance exists.
 * 
 * @author newtong
 * 
 */
public class RoadsideLedgerReportAction extends ActionSupport {

    @InjectEJB(name = "MembershipMgrBean")
    MembershipMgrLocal membershipMgrLocal;

    @InjectEJB(name = "PaymentMgrBean")
    PaymentMgrLocal paymentMgrLocal;

    private BigDecimal postingTotal;
    private BigDecimal voucherTotal;
    private BigDecimal creditTotal;
    private BigDecimal varianceTotal;

    private BigDecimal postingSubTotal;
    private BigDecimal voucherSubTotal;
    private BigDecimal creditSubTotal;
    private BigDecimal varianceSubTotal;

    private List<Map<String, Map<String, BigDecimal>>> monthlyBreakdown = new ArrayList<Map<String, Map<String, BigDecimal>>>();

    private final String LABEL_BALANCE_BROUGHT_FORWARD = "Balance brought forward";

    public String execute() throws Exception {

	postingSubTotal = new BigDecimal(0);
	voucherSubTotal = new BigDecimal(0);
	creditSubTotal = new BigDecimal(0);
	varianceSubTotal = new BigDecimal(0);

	LogUtil.log(getClass(), "Retrieving Total Postings");
	postingTotal = paymentMgrLocal.getTotalPostings(SystemParameterVO.CREDIT_ACCOUNT);

	LogUtil.log(getClass(), "Retrieving Total Vouchers");
	voucherTotal = paymentMgrLocal.getTotalVouchers();

	LogUtil.log(getClass(), "Retrieving Total Credit");
	creditTotal = membershipMgrLocal.getTotalCredit();

	varianceTotal = postingTotal.add(voucherTotal).add(creditTotal);
	/*
	 * if (varianceTotal.compareTo(BigDecimal.ZERO) != 0) { // some variance
	 * 
	 * //config-based start date, else last FY
	 * 
	 * DateTime overrideStart = null; try { String startDateStr =
	 * FileUtil.getProperty("master", "roadsideLedgerReportStartDate");
	 * overrideStart = new DateTime(startDateStr);
	 * 
	 * } catch (Exception e1) { LogUtil.warn(getClass(),
	 * "Could not determine roadsideLedgerReportStartDate, defaulting to: last FY"
	 * ); }
	 * 
	 * Calendar startDate = Calendar.getInstance();
	 * 
	 * if (overrideStart != null) { startDate.setTime(overrideStart); } else
	 * { // since last FY startDate.add(Calendar.YEAR, -1);
	 * startDate.set(Calendar.DATE, 1); startDate.set(Calendar.MONTH, 7); }
	 * 
	 * BigDecimal postings; BigDecimal vouchers; //BigDecimal credits;
	 * //BigDecimal variance; String label; Map<String,BigDecimal>
	 * resultSet; Map<String, Map<String, BigDecimal>> holder;
	 * 
	 * // balance brought forward DateTime endDt = new
	 * DateTime(startDate.getTime()); label = LABEL_BALANCE_BROUGHT_FORWARD;
	 * 
	 * LogUtil.log(getClass(), "Retrieving Total Postings until " + endDt);
	 * postings =
	 * paymentMgrLocal.getTotalPostings(SystemParameterVO.CREDIT_ACCOUNT,
	 * null, endDt); postingSubTotal = postingSubTotal.add(postings);
	 * 
	 * LogUtil.log(getClass(), "Retrieving Total Vouchers until " + endDt);
	 * vouchers = paymentMgrLocal.getTotalVouchers(null, endDt);
	 * voucherSubTotal = voucherSubTotal.add(vouchers);
	 * 
	 * //LogUtil.log(getClass(), "Retrieving Total Credit until " + endDt);
	 * //credits =
	 * membershipMgrLocal.getTotalInstantaneousMembershipCredit(null,
	 * endDt); //creditSubTotal = creditSubTotal.add(credits);
	 * 
	 * //variance = postings.add(vouchers).add(credits); //varianceSubTotal
	 * = varianceSubTotal.add(variance);
	 * 
	 * resultSet = new HashMap<String,BigDecimal>();
	 * resultSet.put("postings", postings); resultSet.put("vouchers",
	 * vouchers); //resultSet.put("credits", credits);
	 * //resultSet.put("variance", variance);
	 * 
	 * holder = new HashMap<String, Map<String, BigDecimal>>();
	 * holder.put(label, resultSet);
	 * 
	 * monthlyBreakdown.add(holder);
	 * 
	 * // loop through 12 months Calendar endDate = Calendar.getInstance();
	 * Calendar endRange = Calendar.getInstance();
	 * 
	 * while (startDate.before(endDate)) {
	 * endRange.setTime(startDate.getTime()); endRange.add(Calendar.MONTH,
	 * 1);
	 * 
	 * DateTime startDt = new DateTime(startDate.getTime()); endDt = new
	 * DateTime(endRange.getTime());
	 * 
	 * label = (new DateTime(startDate.getTime())).formatShortDate();
	 * 
	 * LogUtil.log(getClass(), "Retrieving Total Postings between " +
	 * startDt + " and " + endDt); postings =
	 * paymentMgrLocal.getTotalPostings(SystemParameterVO.CREDIT_ACCOUNT,
	 * startDt, endDt); postingSubTotal = postingSubTotal.add(postings);
	 * LogUtil.log(getClass(), "Total postings: " + postings);
	 * 
	 * LogUtil.log(getClass(), "Retrieving Total Vouchers between " +
	 * startDt + " and " + endDt); vouchers =
	 * paymentMgrLocal.getTotalVouchers(startDt, endDt); voucherSubTotal =
	 * voucherSubTotal.add(vouchers); LogUtil.log(getClass(),
	 * "Total vouchers: " + vouchers);
	 * 
	 * //LogUtil.log(getClass(), "Retrieving Total Credit between " +
	 * startDt + " and " + endDt); //credits =
	 * membershipMgrLocal.getTotalInstantaneousMembershipCredit(startDt,
	 * endDt); //creditSubTotal = creditSubTotal.add(credits);
	 * //LogUtil.log(getClass(), "Total credits: " + credits);
	 * 
	 * //variance = postings.add(vouchers).add(credits); //varianceSubTotal
	 * = varianceSubTotal.add(variance);
	 * 
	 * resultSet = new HashMap<String,BigDecimal>();
	 * resultSet.put("postings", postings); resultSet.put("vouchers",
	 * vouchers); //resultSet.put("credits", credits);
	 * //resultSet.put("variance", variance);
	 * 
	 * holder = new HashMap<String, Map<String, BigDecimal>>();
	 * holder.put(label, resultSet);
	 * 
	 * monthlyBreakdown.add(holder);
	 * 
	 * startDate.add(Calendar.MONTH, 1); } }
	 */

	return SUCCESS;
    }

    public BigDecimal getPostingTotal() {
	return postingTotal;
    }

    public void setPostingTotal(BigDecimal postingTotal) {
	this.postingTotal = postingTotal;
    }

    public BigDecimal getVoucherTotal() {
	return voucherTotal;
    }

    public void setVoucherTotal(BigDecimal voucherTotal) {
	this.voucherTotal = voucherTotal;
    }

    public BigDecimal getCreditTotal() {
	return creditTotal;
    }

    public void setCreditTotal(BigDecimal creditTotal) {
	this.creditTotal = creditTotal;
    }

    public BigDecimal getVarianceTotal() {
	return varianceTotal;
    }

    public void setVarianceTotal(BigDecimal variance) {
	this.varianceTotal = variance;
    }

    public List<Map<String, Map<String, BigDecimal>>> getMonthlyBreakdown() {
	return monthlyBreakdown;
    }

    public void setMonthlyBreakdown(List<Map<String, Map<String, BigDecimal>>> monthlyBreakdown) {
	this.monthlyBreakdown = monthlyBreakdown;
    }

    public String getLABEL_BALANCE_BROUGHT_FORWARD() {
	return LABEL_BALANCE_BROUGHT_FORWARD;
    }

    public BigDecimal getPostingSubTotal() {
	return postingSubTotal;
    }

    public void setPostingSubTotal(BigDecimal postingSubTotal) {
	this.postingSubTotal = postingSubTotal;
    }

    public BigDecimal getVoucherSubTotal() {
	return voucherSubTotal;
    }

    public void setVoucherSubTotal(BigDecimal voucherSubTotal) {
	this.voucherSubTotal = voucherSubTotal;
    }

    public BigDecimal getCreditSubTotal() {
	return creditSubTotal;
    }

    public void setCreditSubTotal(BigDecimal creditSubTotal) {
	this.creditSubTotal = creditSubTotal;
    }

    public BigDecimal getVarianceSubTotal() {
	return varianceSubTotal;
    }

    public void setVarianceSubTotal(BigDecimal varianceSubTotal) {
	this.varianceSubTotal = varianceSubTotal;
    }

}
