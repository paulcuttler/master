package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;

import com.ract.common.CacheBase;

public class MembershipAccountCache extends CacheBase {

	private static MembershipAccountCache instance = new MembershipAccountCache();

	public static MembershipAccountCache getInstance() {
		return instance;
	}

	public void initialiseList() throws RemoteException {
		if (isEmpty()) {
			MembershipMgr memberhshipMgr = MembershipEJBHelper.getMembershipMgr();
			Collection dataList = memberhshipMgr.getMembershipAccounts();
			this.addAll(dataList);
		}
	}

	public MembershipAccountVO getMembershipAccount(Integer accountID) throws RemoteException {
		// LogUtil.log(this.getClass(),"account to find="+accountID);
		if (accountID == null) {
			return null;
		} else {
			return (MembershipAccountVO) getItem(accountID.toString());
		}
	}

	public Collection getMembershipAccountList() throws RemoteException {
		return getItemList();
	}

}
