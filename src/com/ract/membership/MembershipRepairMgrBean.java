package com.ract.membership;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CardHelper;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ReferenceDataVO;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationMgrLocal;
import com.ract.membership.notifier.MembershipChangeNotificationEvent;
import com.ract.payment.PayableItemComponentVO;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.security.SecurityHelper;
import com.ract.user.User;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * Holds methods used for repairing membership data.
 */
@Stateless
@Remote({ MembershipRepairMgr.class })
public class MembershipRepairMgrBean {

    @Resource(mappedName = "java:/ClientDS")
    private DataSource clientDataSource;

    @EJB
    private MembershipMgrLocal membershipMgrLocal;

    /**
     * Repair the join date data on the membership cards. If the join date on
     * the card is null then copy the join date from the membership to the card.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void repairMembershipCardJoinDate() throws RemoteException {
	StringBuffer sql = new StringBuffer();
	sql.append("update pub.mem_membership_card ");
	sql.append("set join_date = mem.join_date ");
	sql.append("from pub.mem_membership_card as card, pub.mem_membership as mem ");
	sql.append("where card.membership_id = mem.membership_id ");
	sql.append("and card.join_date is null");

	int rowsUpdated = 0;
	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(sql.toString());
	    rowsUpdated = statement.executeUpdate();
	} catch (SQLException sqle) {
	    throw new SystemException(sqle);
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
    }

    private final String SQL_CARD_NAME = "SELECT card_id, membership_id FROM PUB.mem_membership_card WHERE card_name is null AND issue_date is not null";

    /**
     * Repair the member name data on the membership cards. If the member name
     * on the card is null then generate a member name and set it on the card.
     * Returns a list of exceptions that occured.
     * 
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList repairMembershipCardName() throws RemoteException {
	ArrayList exceptionList = new ArrayList();
	Connection connection = null;
	PreparedStatement statement = null;
	try {
	    MembershipRepairMgr memRepairMgr = MembershipEJBHelper.getMembershipRepairMgr();
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(SQL_CARD_NAME);
	    ResultSet cardIDRS = statement.executeQuery();
	    LogUtil.debug(this.getClass(), "repairMembershipCardName() : Starting repair of membership cards with null card names.");
	    int rowCount = 0;
	    while (cardIDRS.next()) {
		try {
		    memRepairMgr.repairMembershipCardNameWorker(new Integer(cardIDRS.getInt(2)), new Integer(cardIDRS.getInt(1)));
		    rowCount++;
		} catch (Exception e) {
		    exceptionList.add(e);
		}
		if (rowCount % 100 == 0) {
		    LogUtil.debug(this.getClass(), "repairMembershipCardName() : " + rowCount + " records repaired so far.");
		}
	    }
	    LogUtil.debug(this.getClass(), "repairMembershipCardName() : " + rowCount + " total records repaired.");
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + SQL_CARD_NAME + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return exceptionList;
    }

    public void repairAccessMemberMarketing() throws RemoteException {

	LogUtil.debug(this.getClass(), "repairAccessMemberMarketing 1");

	Integer clientNumber = null;
	Connection connection = null;
	PreparedStatement statement = null;
	MembershipRepairMgr membershipRepairMgr = MembershipEJBHelper.getMembershipRepairMgr();
	try {
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(SQL_ACCESS_MARKETING); // non-marketing
	    ClientVO client = null;
	    ResultSet accessMemberRS = statement.executeQuery();
	    User user = SecurityHelper.getSystemUser();
	    while (accessMemberRS.next()) {
		clientNumber = new Integer(accessMemberRS.getInt(1));
		try {
		    membershipRepairMgr.updateMarketingClient(clientNumber);
		} catch (RemoteException ex) {
		    // ignore
		    LogUtil.warn(this.getClass(), "Unable to update marketing client " + clientNumber + ". " + ex.toString());
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + SQL_ACCESS_MARKETING + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}

    }

    /**
     * New Transaction
     * 
     * @param clientNumber
     *            Integer
     * @throws RemoteException
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void updateMarketingClient(Integer clientNumber) throws RemoteException {
	ClientVO client = null;
	ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
	client = clientMgr.getClient(clientNumber);
	client.setMarket(new Boolean(true));
	User user = SecurityHelper.getSystemUser();
	// updates all systems
	clientMgr.updateClient(client, user.getUserID(), "Access member update", user.getSalesBranchCode(), "", "RSS");
	LogUtil.debug(this.getClass(), "repairAccessMemberMarketing clientNumber=" + clientNumber);
    }

    private final String SQL_TRANSACTION_WITH_NO_CARDS = "select transaction_id " + "from PUB.mem_transaction mt,PUB.mem_membership mm " + "where mt.membership_id = mm.membership_id " + "and transaction_type_code in ('Create','Card request','Change product') " + // ignore
																																       // Rejoin
	    "and transaction_Date >= ? " + // bind date
	    "and undone_date is null " + "and not exists " + "( " +
	    // card issued or requested on or after transaction date
	    "  select 'x' " + "  from PUB.mem_membership_card mc" + "  where mc.membership_id = mm.membership_id " + "  and mc.request_date >= mt.transaction_date " + ") " + "order by transaction_date desc ";

    public int getMembershipsWithTransferredStatusCount() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.countRecords(SQL_TRANSFERRED_MEMBERSHIPS);
    }

    private final String SQL_TRANSFERRED_MEMBERSHIPS = "select membership_id from PUB.mem_membership where membership_status = '" + MembershipVO.STATUS_TRANSFERRED + "'";

    private final String SQL_CARD = "select card_id from PUB.mem_membership_card where card_id = ? or (membership_id = ? and issue_date is null)";

    private final String SQL_INSERT_CARD = "insert into PUB.mem_membership_card (card_id,issuance_number,membership_id,ref_code,ref_type,request_date) values (?,?,?,?,?,?) ";

    public void repairOneMemberGroups() throws RemoteException {
	final String SQL_DELETE_GROUP = "delete from PUB.mem_membership_group where group_id in (" + SQL_ONE_MEMBER_GROUPS + ")";
	LogUtil.log(this.getClass(), "SQL_DELETE_GROUP=" + SQL_DELETE_GROUP);
	Connection connection = null;
	PreparedStatement groupStatement = null;
	PreparedStatement deleteGroupStatement = null;
	int counter = 0;
	try {
	    connection = clientDataSource.getConnection();
	    deleteGroupStatement = connection.prepareStatement(SQL_DELETE_GROUP);
	    deleteGroupStatement.executeUpdate();
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    throw new RemoteException("Unable to delete one member groups.", e);
	} finally {
	    ConnectionUtil.closeConnection(connection);
	    ConnectionUtil.closeStatement(deleteGroupStatement);
	}
    }

    public void repairRemovedCards(DateTime startDate) throws RemoteException {
	LogUtil.log(this.getClass(), startDate.formatShortDate());
	LogUtil.log(this.getClass(), SQL_TRANSACTION_WITH_NO_CARDS);
	Integer transactionId = null;
	Integer membershipId = null;
	MembershipCardVO cardVO = null;

	MembershipTransactionVO transactionVO = null;
	MembershipHistory history = null;

	ArrayList transactionList = new ArrayList();

	ArrayList cardList = null;
	String reasonCode = null;

	Connection connection = null;
	PreparedStatement transIdStatement = null;
	PreparedStatement cardStatement = null;
	PreparedStatement cardInsertStatement = null;
	Integer cardId = null;
	Iterator cardIt = null;
	int counter = 0;
	int recordInserted = 0;
	int cardsInserted = 0;
	try {
	    connection = clientDataSource.getConnection();
	    transIdStatement = connection.prepareStatement(SQL_TRANSACTION_WITH_NO_CARDS);
	    transIdStatement.setDate(1, startDate.toSQLDate());

	    ResultSet transRs = transIdStatement.executeQuery();
	    ResultSet cardRs = null;

	    LogUtil.debug(this.getClass(), "1");

	    // return the transaction_id as a serializable list as the list
	    // will be modified by the inserts changing the number of rows
	    // returned.
	    while (transRs.next()) {
		transactionId = new Integer(transRs.getInt(1));
		transactionList.add(transactionId);
	    }

	    for (int i = 0; i < transactionList.size(); i++) {
		LogUtil.log(this.getClass(), "row: " + i);
		counter++;

		transactionId = (Integer) transactionList.get(i);

		LogUtil.debug(this.getClass(), "2: " + transactionId);

		// get the transaction using the transaction id
		transactionVO = membershipMgrLocal.getMembershipTransaction(transactionId);

		if (transactionVO == null) {
		    // LogUtil.warn(this.getClass(),"No Transaction for id "+transactionId);
		    throw new SystemException("No Transaction for id " + transactionId);
		}

		LogUtil.debug(this.getClass(), "3");

		// get the membership history record
		history = transactionVO.getMembershipHistory();

		if (history != null) {

		    // LogUtil.log(this.getClass(),"History client number"+history.getClientNumber()+" "+history.isGroupMember());
		    cardList = (ArrayList) history.getMembershipCardList(false); // load
										 // from
										 // history
		    LogUtil.debug(this.getClass(), "4");
		    // is there a card list in history?
		    if (cardList != null && cardList.size() > 0) {

			if (cardList.size() > 1) {
			    LogUtil.debug(this.getClass(), "Check " + history.getClientNumber());
			}

			// get most recent card
			Collections.reverse(cardList);

			cardIt = cardList.iterator();
			cardVO = (MembershipCardVO) cardIt.next();

			// ignore tier changes
			if (ReferenceDataVO.CARD_REQUEST_TIER_CHANGE.equals(cardVO.getReasonCode())) {
			    LogUtil.warn(this.getClass(), "Changing tier change card for client " + history.getClientNumber() + " to change product");
			    reasonCode = ReferenceDataVO.CARD_REQUEST_CHANGE_PRODUCT;
			} else {
			    reasonCode = cardVO.getReasonCode();
			}

			// check if card request exists in the database
			cardId = cardVO.getCardID();
			membershipId = cardVO.getMembershipID();

			LogUtil.debug(this.getClass(), "5: " + cardId);

			cardStatement = connection.prepareStatement(SQL_CARD);
			cardStatement.setInt(1, cardId.intValue());
			cardStatement.setInt(2, membershipId.intValue());
			cardRs = cardStatement.executeQuery();

			if (cardRs.next()) {
			    LogUtil.warn(this.getClass(), "Card request " + cardRs.getInt(1) + " already exists for card " + cardId + " and client " + history.getClientNumber() + ".");
			    continue;
			}
			// close card rs
			cardRs.close();

			LogUtil.debug(this.getClass(), "6");

			LogUtil.log(this.getClass(), "Card request: " + cardVO.getCardID() + " " + cardVO.getIssuanceNumber() + " " + cardVO.getMembershipID() + " " + reasonCode + " " + cardVO.getReasonType() + " " + cardVO.getRequestDate());

			// create it if it does not.
			cardInsertStatement = connection.prepareStatement(SQL_INSERT_CARD);
			cardInsertStatement.setInt(1, cardId.intValue());
			cardInsertStatement.setInt(2, cardVO.getIssuanceNumber());
			cardInsertStatement.setInt(3, cardVO.getMembershipID().intValue());
			cardInsertStatement.setString(4, reasonCode);
			cardInsertStatement.setString(5, cardVO.getReasonType());
			cardInsertStatement.setDate(6, cardVO.getRequestDate().toSQLDate());
			recordInserted = cardInsertStatement.executeUpdate();
			if (recordInserted == 0) {
			    LogUtil.warn(this.getClass(), "No card was inserted for " + cardId);
			} else {
			    cardsInserted += recordInserted;
			}

		    }// end of card list
		    else {
			LogUtil.warn(this.getClass(), "No history for transaction " + transactionId);
		    }

		}// end of history

	    }// end of transaction loop
	     // close trans rs
	    transRs.close();

	} catch (SQLException sqle) {
	    throw new SystemException(sqle);
	} finally {
	    ConnectionUtil.closeStatement(transIdStatement);
	    ConnectionUtil.closeStatement(cardStatement);
	    ConnectionUtil.closeStatement(cardInsertStatement);
	    ConnectionUtil.closeConnection(connection);
	}

	LogUtil.log(this.getClass(), "Record processed: " + counter);
	LogUtil.log(this.getClass(), "Cards Inserted: " + cardsInserted);

	// this.sessionContext.setRollbackOnly();

    }

    /**
     * This method is called by the repairMembershipCardName() method. It has
     * been separated out so that we can set a new container managed transaction
     * for each call.
     * 
     * @param membershipID
     *            Description of the Parameter
     * @param cardID
     *            Description of the Parameter
     * @exception Exception
     *                Description of the Exception
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void repairMembershipCardNameWorker(Integer membershipID, Integer cardId) throws RemoteException {
	MembershipCardVO cardVO;
	MembershipVO memVO = MembershipEJBHelper.getMembershipMgr().getMembership(membershipID);
	if (memVO != null) {
	    cardVO = membershipMgrLocal.getMembershipCard(cardId);
	    cardVO.setCardName(CardHelper.getCardName(memVO.getClient(), memVO.getMembershipProfile().getNameSuffix()));
	    membershipMgrLocal.updateMembershipCard(cardVO);
	}
    }

    private final String SQL_CARD_JOIN_DATE = "SELECT card_id FROM PUB.mem_membership_card WHERE join_date is null and issue_date IS NOT NULL";

    public int getMembershipCardJoinDateToRepairCount() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.countRecords(SQL_CARD_JOIN_DATE);
    }

    public int getAccessMemberMarketingCount() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.countRecords(SQL_ACCESS_MARKETING);
    }

    private final String SQL_TRANSACTION_DETAILS = "SELECT transaction_id FROM PUB.mem_transaction WHERE effective_date IS NULL AND end_date IS NULL AND membership_xml IS NOT NULL";

    public int getTransactionDetailsToRepairCount() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.countRecords(SQL_TRANSACTION_DETAILS);
    }

    /**
     * Update the erroneous payable item.
     * 
     * @param payableItemVO
     *            PayableItemVO
     * @param description
     *            String
     * @throws RemoteException
     */
    private double updatePayableItem(PayableItemVO payableItemVO, String description, boolean dereferencePayableItem) throws RemoteException {
	PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();

	// note: the payable item will not be deleted
	LogUtil.log(this.getClass(), "setting payable item by payable item id");
	Integer payableItemID = payableItemVO.getPayableItemID();
	double reverseAmount = 0;
	if (dereferencePayableItem) {
	    PaymentMgrLocal paymentMgr = PaymentEJBHelper.getPaymentMgrLocal();
	    try {
		PayableItemVO payableItem = paymentMgr.getPayableItem(payableItemID);
		payableItem.setPaymentMethodID(null);
		paymentTxMgr.updatePayableItem(payableItem);
	    } catch (Exception e) {
		throw new RemoteException("Unable to update payable item.", e);
	    }
	}

	LogUtil.log(this.getClass(), "getting components");

	// reverse the components
	PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	Collection componentList = paymentMgr.getComponents(SourceSystem.MEMBERSHIP, payableItemID);
	if (componentList != null && componentList.size() > 0) {
	    Collection reversePostings = null;
	    PayableItemComponentVO pic = null;
	    Iterator compIt = componentList.iterator();
	    PayableItemPostingVO pip = null;

	    while (compIt.hasNext()) {
		pic = (PayableItemComponentVO) compIt.next();

		// get the reverse postings
		try {
		    reversePostings = paymentMgr.reverseAllPostings(pic, description);
		} catch (PaymentException ex2) {
		    throw new RemoteException("Unable to create reversal postings.", ex2);
		}

		LogUtil.log(this.getClass(), reversePostings.toString());

		// create the new postings in the database
		if (reversePostings != null && reversePostings.size() > 0) {
		    Iterator it = reversePostings.iterator();
		    while (it.hasNext()) {
			pip = (PayableItemPostingVO) it.next();

			// sum postings
			if (pip.getAccount().isClearingAccount()) {
			    reverseAmount += pip.getAmount();
			}
			try {
			    pip.setContainerID(pic.getPayableItemComponentID());
			    paymentTxMgr.createPayableItemPosting(pip);
			} catch (Exception ex) {
			    throw new RemoteException("Failed to create the posting.", ex);
			}
		    }
		}
	    }
	}
	return reverseAmount;
    }

    /**
     * SQL statement to get memberships that have payable items that have the
     * same payment method id.
     */
    private final String SQL_DUPLICATES = "SELECT a.membership_id " + "FROM pub.mem_transaction a, pub.mem_membership b, pub.pt_payableitem p " + "WHERE transaction_type_code = 'Renew' " + "AND a.membership_id = b.membership_id " + "AND a.transaction_id = p.sourcesystemreferenceid " + "AND transaction_date >= ? " + "AND undone_date is null " + "AND paymentmethodid is not null " + "GROUP BY a.membership_id,paymentmethod,p.paymentmethodid " + "HAVING COUNT(transaction_id) >= 2 ";

    private final String SQL_ACCESS_MARKETING = "SELECT [client-no] FROM " + "pub.[cl-master] c WHERE market = 0 AND EXISTS (SELECT 'x' FROM pub.mem_membership m " + "WHERE product_code = 'Access' AND m.client_number = c.[client-no] ) ";

    /**
     * Repair duplicate transactions resulting from bug with fast track
     * renewals.
     * 
     * @throws RemoteException
     */
    public void repairDuplicateTransactions() throws RemoteException {

	// identify duplication transactions
	DateTime threshold = null;
	try {
	    threshold = new DateTime(DATE_FASTTRACK);
	} catch (ParseException ex1) {
	    // ignore
	}

	Connection connection = null;
	PreparedStatement statement = null;

	StringBuffer repairAudit = new StringBuffer();

	double totalReverseAmount = 0;

	try {
	    connection = this.clientDataSource.getConnection();
	    statement = connection.prepareStatement(SQL_DUPLICATES);
	    statement.setDate(1, threshold.toSQLDate());
	    int counter = 0;
	    ResultSet rs = statement.executeQuery();

	    // for each erroroneous membership
	    while (rs.next()) {
		counter++;

		LogUtil.log(this.getClass(), "count=" + counter);
		// get the membership ID

		double txReverseAmount = 0;

		Integer memID = null;
		Integer transID = null;
		MembershipVO memVO = null;
		ArrayList transList = null;
		MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE, true);

		memID = new Integer(rs.getInt(1));

		try {
		    // get the membership VO
		    memVO = membershipMgrLocal.getMembership(memID);

		    repairAudit.append("C" + memVO.getClientNumber() + ",");

		    // get the transaction list for the membership
		    transList = (ArrayList) memVO.getTransactionList();

		    if (transList == null || transList.isEmpty()) {
			throw new SystemException("Transaction list for '" + memID + " is empty");
		    }
		    // sort in reverse order
		    Collections.sort(transList, mtc);

		    // get the first transaction which will be the last
		    // transaction created
		    transID = ((MembershipTransactionVO) transList.get(0)).getTransactionID();

		    txReverseAmount = reverseTransaction(transID, "Reverse posting for duplicate transaction error. Ref:", null, true, true, true);

		    totalReverseAmount += txReverseAmount;
		    repairAudit.append(",\"" + NumberUtil.formatValue(txReverseAmount) + "\"\n");
		} catch (Exception e) {
		    LogUtil.log(this.getClass(), memVO == null ? "M" + memID.toString() : "C" + memVO.getClientNumber() + ":" + e.getMessage());
		    repairAudit.append(",," + e.getMessage() + "\n");
		}
	    }
	} catch (SQLException ex2) {
	    throw new RemoteException("Unable to get list of duplicate transactions.", ex2);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}

	repairAudit.append(",,\"" + NumberUtil.formatValue(totalReverseAmount) + "\"");

	LogUtil.log(this.getClass(), repairAudit.toString());

    }

    /**
     * Reverse a transaction and return the value of the payable items attached
     * to the transaction.
     * 
     * @param transId
     *            Integer
     * @return double
     */
    public double reverseTransaction(Integer transId, String description, String userId, boolean dereferencePayableItem, boolean removeTransaction, boolean restrictToRenew) throws RemoteException {

	LogUtil.log(this.getClass(), "reverseTransaction start");

	LogUtil.debug(this.getClass(), "transId=" + transId);
	LogUtil.debug(this.getClass(), "description=" + description);
	LogUtil.debug(this.getClass(), "userId=" + userId);
	LogUtil.debug(this.getClass(), "dereferencePayableItem=" + dereferencePayableItem);
	LogUtil.debug(this.getClass(), "removeTransaction=" + removeTransaction);
	LogUtil.debug(this.getClass(), "restrictToRenew=" + restrictToRenew);

	DateTime effectiveDate = new DateTime().getDateOnly();

	MembershipTransactionVO removeTransVO = null;
	MembershipTransactionVO revertToTransVO = null;
	MembershipVO removeMemVO = null;
	MembershipVO revertToMemVO = null;
	MembershipGroupVO memGroupVO = null;
	MembershipGroupDetailVO memGroupDetailVO = null;

	MembershipTransactionVO memTransVO = null;

	NotificationMgrLocal notificationMgrLocal = CommonEJBHelper.getNotificationMgrLocal();
	NotificationEvent notificationEvent = null;

	RemoveTransactionGroup transGroup = null;
	PayableItemVO payableItemVO = null;

	Integer payableItemId = null;

	double txReverseAmount = 0;

	// create a remove transaction group
	transGroup = new RemoveTransactionGroup(transId, restrictToRenew);

	// for each transaction in the transaction group
	for (int i = 0; i < transGroup.getTransactionListSize(); i++) {
	    // get the transaction to remove
	    removeTransVO = transGroup.getRemoveTransactionVO(i);
	    // get the transaction to revert the membership to
	    revertToTransVO = transGroup.getRevertToTransactionVO(i);

	    removeMemVO = transGroup.getRemoveMembershipVO(i);
	    revertToMemVO = transGroup.getRevertToMembershipVO(i);

	    memGroupDetailVO = MembershipHelper.removeFromAllGroups(removeMemVO.getMembershipID());

	    if (revertToMemVO == null) {
		if (restrictToRenew) {
		    throw new SystemException("revertToMemVO is null and there should be a previous transaction for renew.");
		} else {
		    // The membership status is changed to "Undone".
		    removeMemVO.setStatus(MembershipVO.STATUS_UNDONE);
		    // expiry is set to commence date
		    removeMemVO.setExpiryDate(removeMemVO.getCommenceDate());

		    // reset all affiliated club details
		    removeMemVO.setAffiliatedClubCode(null);
		    removeMemVO.setAffiliatedClubId(null);
		    removeMemVO.setAffiliatedClubExpiryDate(null);
		    removeMemVO.setAffiliatedClubProductLevel(null);

		    LogUtil.debug(this.getClass(), "removeMemVO=" + removeMemVO);
		    membershipMgrLocal.updateMembership(removeMemVO);

		    // prosper/cad
		    notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE, removeMemVO);
		}
	    } else {
		LogUtil.debug(this.getClass(), "revertToMemVO=" + revertToMemVO);
		membershipMgrLocal.updateMembership(revertToMemVO);

		// Insert the previous membership group detail
		memGroupVO = revertToMemVO.getMembershipGroup();

		// currently a group
		if (memGroupDetailVO != null) {
		    LogUtil.log(this.getClass(), "reverseTransaction 3 " + memGroupDetailVO);
		    // previously a group
		    if (memGroupVO != null) {
			LogUtil.log(this.getClass(), "reverseTransaction 4 " + memGroupVO);
			// update the details
			MembershipGroupDetailVO groupDetail = memGroupVO.getMembershipGroupDetail(revertToTransVO.getMembershipID());
			LogUtil.log(this.getClass(), "reverseTransaction 4a " + groupDetail);
			if (groupDetail != null) {

			    LogUtil.log(this.getClass(), "reverseTransaction 5");
			    memGroupDetailVO.setGroupJoinDate(groupDetail.getGroupJoinDate());
			    memGroupDetailVO.setPrimeAddressee(groupDetail.isPrimeAddressee());
			    LogUtil.log(this.getClass(), "reverseTransaction 6");
			    membershipMgrLocal.createMembershipGroupDetail(memGroupDetailVO);
			}
		    }
		}
		// not currently a group
		else {
		    LogUtil.log(this.getClass(), "reverseTransaction 7");
		    // previously a group
		    if (memGroupVO != null) {
			LogUtil.log(this.getClass(), "reverseTransaction 8");
			memGroupDetailVO = memGroupVO.getMembershipGroupDetail(revertToTransVO.getMembershipID());
			LogUtil.log(this.getClass(), "reverseTransaction 8a");
			if (memGroupDetailVO != null) {
			    LogUtil.log(this.getClass(), "reverseTransaction 9");
			    membershipMgrLocal.createMembershipGroupDetail(memGroupDetailVO);
			}
		    }
		    // if not previously a group then no group records will be
		    // created
		}

		// update prosper/cad
		notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE, revertToMemVO);

	    }

	    payableItemVO = removeTransVO.getPayableItem();
	    if (payableItemVO != null) {
		LogUtil.log(this.getClass(), "paymentMethod=" + payableItemVO.getPaymentMethod());
		if (!PaymentMethod.RECEIPTING.equals(payableItemVO.getPaymentMethodDescription())) {
		    throw new SystemException("Only receipting transactions may be reversed.");
		}
		txReverseAmount = this.updatePayableItem(payableItemVO, description, dereferencePayableItem);
	    }

	    // don't remove any documents or cards

	    // remove cards issued on or after the transaction date being
	    // reversed
	    // JH 23/07/2008 disabled card removal
	    // Collection cardList = removeMemVO.getMembershipCardList(true);
	    // if(cardList != null && !cardList.isEmpty())
	    // {
	    // boolean found = false;
	    // MembershipCardVO cardVO;
	    // Iterator cardIterator = cardList.iterator();
	    // while(cardIterator.hasNext() && !found)
	    // {
	    // cardVO = (MembershipCardVO)cardIterator.next();
	    // //if issued same day then potential problem
	    // if(cardVO.getIssueDate() == null ||
	    // cardVO.getIssueDate().onOrAfterDay(removeTransVO.getTransactionDate()))
	    // {
	    // membershipMgrLocal.removeMembershipCard(cardVO);
	    // }
	    // }
	    // }

	    if (removeTransaction) {
		// remove the transaction
		membershipMgrLocal.removeMembershipTransaction(removeTransVO);
	    } else {
		// set undone attributes and remove payable item reference
		removeTransVO.setUndoneDate(effectiveDate);
		removeTransVO.setUndoneUsername(userId);
		removeTransVO.setPayableItemID(null); // disassociate payable
						      // item
		membershipMgrLocal.updateMembershipTransaction(removeTransVO);
	    }

	    // notify event
	    try {
		notificationMgrLocal.notifyEvent(notificationEvent);
		notificationMgrLocal.commit();
	    } catch (RollBackException ex) {
		notificationMgrLocal.rollback();
		throw new SystemException("Unable to notify event.", ex);
	    }

	}// transactions
	LogUtil.log(this.getClass(), "reverseTransaction end");
	return txReverseAmount;
    }

    public int getDuplicationTransactionsToRepairCount() throws RemoteException {
	DateTime threshold = null;
	try {
	    threshold = new DateTime(DATE_FASTTRACK);
	} catch (ParseException ex1) {
	    // ignore
	}

	int repairCount = 0;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = SQL_DUPLICATES;
	try {
	    connection = clientDataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    statement.setDate(1, threshold.toSQLDate());
	    ResultSet duplicatesCountRS = statement.executeQuery();
	    while (duplicatesCountRS.next()) {
		repairCount++;
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return repairCount;
    }

    private final String DATE_FASTTRACK = "19/2/2003";

    private final String SQL_ONE_MEMBER_GROUPS = "select group_id " + "from PUB.mem_membership mm, PUB.mem_membership_group mg " + "where mm.membership_id = mg.membership_id " + "and exists (SELECT     group_Id " + "FROM         PUB.mem_membership_group mg2 " + "where mg2.group_id = mg.group_id " + "GROUP BY group_Id " + "HAVING      (COUNT(membership_id) = 1))";

    public int getGroupsWithOneMemberCount() throws RemoteException {
	CommonMgrLocal commonMgr = CommonEJBHelper.getCommonMgrLocal();
	return commonMgr.countRecords(SQL_ONE_MEMBER_GROUPS);
    }

    public int getMembershipCardNameToRepairCount() throws RemoteException {
	CommonMgrLocal commonMgr = CommonEJBHelper.getCommonMgrLocal();
	return commonMgr.countRecords(SQL_CARD_NAME);
    }

    /**
     * Update transaction effective date and end date where possible.
     * 
     * Use the payable item
     */
    public int repairTransactionDetails() throws RemoteException {
	int updateCounter = 0;
	int counter = 0;
	int transID = 0;

	DateTime transDate = null;
	DateTime startDate = null;
	DateTime endDate = null;
	Interval membershipTerm = null;
	MembershipTransactionVO memTransVO = null;

	MembershipHistory mHistory = null;
	MembershipTransactionVO mtVO = null;
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	Connection connection = null;
	PreparedStatement selectStatement = null;
	PreparedStatement updateStatement = null;

	final String SQL_TRANSACTION_UPDATE = "UPDATE PUB.mem_transaction SET effective_date = ?, end_date = ? WHERE transaction_id = ?";
	try {
	    connection = this.clientDataSource.getConnection();
	    selectStatement = connection.prepareStatement(SQL_TRANSACTION_DETAILS);

	    ResultSet selectRS = selectStatement.executeQuery();

	    // for each transaction
	    while (selectRS.next()) {
		transID = selectRS.getInt(1);
		startDate = null;
		endDate = null;
		mtVO = membershipMgr.getMembershipTransaction(new Integer(transID));
		LogUtil.debug(this.getClass(), "mtVO=" + mtVO);
		transDate = mtVO.getTransactionDate();

		if (mtVO.isHistoryAvailable()) {

		    // get history
		    try {
			mHistory = mtVO.getMembershipHistory();
		    } catch (Exception e) {
			LogUtil.warn(this.getClass(), e);
		    }

		    if (mHistory == null) {
			// ignore
			continue;
		    }
		    // history is not null
		    else {
			endDate = mHistory.getExpiryDate();

			// if not a group transaction
			if (mtVO.getTransactionGroupID() == 0) {
			    // renew
			    if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(mtVO.getTransactionTypeCode())) {
				membershipTerm = mHistory.getMembershipTerm();
				startDate = endDate.subtract(membershipTerm);
			    } else {
				// product option,change
				// product,create,rejoin,card request,cancel,
				// hold,transfer
				startDate = transDate;
			    }
			}
			// group transaction
			else {
			    // change group
			    // remove from group,renew,change group pa,change
			    // product
			    if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(mtVO.getTransactionTypeCode())) {
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(mtVO.getTransactionGroupTypeCode())) {
				    // start is one term before the expiry date
				    membershipTerm = mHistory.getMembershipTerm();
				    startDate = endDate.subtract(membershipTerm);
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(mtVO.getTransactionGroupTypeCode())) {
				    // renew from transaction to finish
				    startDate = transDate;
				} else {
				    // create group or null group type code
				    // if null there may be a possible
				    startDate = transDate;
				}
			    } else {
				// change group PA,prod option
				startDate = transDate; // upgrade
			    }
			}

			counter++;

			// update transaction with payable item details.
			if (startDate != null && endDate != null) {
			    updateStatement = connection.prepareStatement(SQL_TRANSACTION_UPDATE);
			    updateStatement.setDate(1, startDate.toSQLDate());
			    updateStatement.setDate(2, endDate.toSQLDate());
			    updateStatement.setInt(3, transID);
			    int updateCount = updateStatement.executeUpdate(); // no.
									       // rows
									       // updates
			    if (updateCount < 1) {
				LogUtil.fatal(this.getClass(), "Failed to repair transaction details for " + transID);
			    } else {
				// update counter
				updateCounter++;
			    }
			}
		    }
		}
		if (counter % 1000 == 0) {
		    LogUtil.log(this.getClass(), "Total Record Counter=" + counter);
		}
	    }
	} catch (SQLException se) {
	    throw new RemoteException("Error selecting records.", se);
	} catch (IOException ioe) {
	    throw new RemoteException("Error writing to file.", ioe);
	} finally {
	    ConnectionUtil.closeStatement(selectStatement);
	    ConnectionUtil.closeStatement(updateStatement);
	    ConnectionUtil.closeConnection(connection);
	}
	return updateCounter;
    }

    /**
   *
   */
    public int convertMembershipHistoryFiles() throws RemoteException {
	// Call the worker method to read the contents of the history files into
	// the database.
	ArrayList convertedFileList = MembershipEJBHelper.getMembershipRepairMgr().convertMembershipHistoryFilesWorker();
	File historyDir = getMembershipHistoryDir();
	// Now that the database changes have been committed, delete the
	// converted files.
	for (int i = 0; i < convertedFileList.size(); i++) {
	    String filename = (String) convertedFileList.get(i);
	    LogUtil.log(this.getClass(), "filename=" + filename);
	    File historyFile = new File(historyDir, filename);
	    if (historyFile.exists()) {
		historyFile.delete();
	    }
	}
	return convertedFileList.size();
    }

    /**
     * This method does the actual work of converting membership history files.
     * It is marked as requiring a new transaction in the EJB group and should
     * only be call from the convertMembershipHistoryFiles() method which
     * finishes the job of converting. Returns a list of the file names that
     * were converted and when control is passed back to the calling method
     * these changes should be committed in the database.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public ArrayList convertMembershipHistoryFilesWorker() throws RemoteException {
	File historyDir = getMembershipHistoryDir();
	ArrayList convertedFileList = new ArrayList();
	Connection connection = null;
	PreparedStatement selectStatement = null;
	PreparedStatement updateStatement = null;
	String selectSQL = "select transaction_id, snapshot_filename from PUB.mem_transaction where ({ fn LENGTH(snapshot_filename) } > 0)";
	String updateSQL = "update PUB.mem_transaction set snapshot_filename = null, membership_xml = ? where transaction_id = ?";
	try {
	    connection = clientDataSource.getConnection();
	    selectStatement = connection.prepareStatement(selectSQL);
	    updateStatement = connection.prepareStatement(updateSQL);
	    ResultSet selectRS = selectStatement.executeQuery();
	    int transID = 0;
	    String filename = null;
	    String snapshot = null;
	    while (selectRS.next()) {
		transID = selectRS.getInt(1);
		filename = selectRS.getString(2);
		snapshot = getSnapshot(historyDir, filename);
		if (snapshot != null) {
		    updateStatement.setString(1, snapshot);
		    updateStatement.setInt(2, transID);
		    if (updateStatement.executeUpdate() < 1) {
			LogUtil.fatal(this.getClass(), "convertMembershipHistoryFilesWorker() : failed to update transaction ID " + transID);
		    } else {
			convertedFileList.add(filename);
		    }
		} else {
		    LogUtil.warn(this.getClass(), "convertMembershipHistoryFilesWorker() : Failed to get history file \"" + historyDir.getAbsolutePath() + historyDir.pathSeparator + filename + "\". Transaction " + transID + " not converted.");
		}
	    }
	} catch (SQLException sqle) {
	    throw new SystemException(sqle);
	}
	return convertedFileList;
    }

    /**
     * Read the snapshot file and return the contents as a string. Retrun a null
     * if the file does not exist or could not be read.
     */
    private String getSnapshot(File historyDir, String filename) {
	StringBuffer snapshot = new StringBuffer();
	if (historyDir != null && filename != null && filename.trim().length() > 0) {
	    try {
		File historyFile = new File(historyDir, filename);
		if (historyFile.exists()) {
		    BufferedReader reader = new BufferedReader(new FileReader(historyFile));
		    String line;
		    while ((line = reader.readLine()) != null) {
			snapshot.append(line);
		    }
		}
	    } catch (Exception e) {
		LogUtil.fatal(this.getClass(), "getSnapshot(" + historyDir.getAbsolutePath() + "," + filename + ") : " + e.getMessage());
	    }
	}
	if (snapshot.length() > 0) {
	    return snapshot.toString();
	} else {
	    return null;
	}
    }

    private File getMembershipHistoryDir() throws RemoteException {
	// Get the membership history directory and make sure it exists.
	CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
	String historyDirectoryName = comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "HISTDIR");
	if (historyDirectoryName == null) {
	    throw new SystemException("Failed to get membership history directory name from system parameters");
	}

	File historyDirectory = new File(historyDirectoryName);
	if (!historyDirectory.exists()) {
	    throw new SystemException("The membership history directory '" + historyDirectoryName + "' does not exist on this machine.");
	}

	return historyDirectory;
    }

    /**
     * Tests the ability of the database to remove a record from a set and then
     * read the set back again, minus the removed record, before committing.
     * Continues removing records from the set until none are left. And yes, it
     * was successful. Accessed from RepairMembership.jsp
     */

}

class RemoveTransaction {
    public RemoveTransaction(MembershipTransactionVO removeMemTrans, boolean isContext, boolean restrictToRenew) throws RemoteException {
	this.removeTransVO = removeMemTrans;
	this.removeMemVO = this.removeTransVO.getMembership();
	this.payee = this.removeTransVO.getPayableItemID() != null;
	this.contextTransaction = isContext;
	if (this.payee) {
	    Integer payableItemID = this.removeTransVO.getPayableItemID();
	    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    try {
		this.payableItemVO = paymentMgr.getPayableItem(payableItemID);
	    } catch (PaymentException ex) {
		throw new RemoteException("Unable to get payable item.", ex);
	    }
	}
	findRevertToTransaction(restrictToRenew);

	// check the integrity of the groups being reversed
	checkMembershipGroup();
    }

    private void checkMembershipGroup() throws RemoteException {
	// Get the definition of the membership group at the end of the
	// transaction
	// being undone from the membership history file. Can't use the current
	// definition of the membership group because changes to other group
	// members,
	// such as cancelling them, may have removed them from the group.

	// MembershipHistory memHist =
	// this.removeTransVO.getMembershipHistory();
	// if(memHist == null)
	// {
	// throw new
	// SystemException("Failed to get membership history for transaction ID "
	// + this.removeTransVO.getTransactionID());
	// }

	MembershipGroupVO memGroupVO = this.removeMemVO.getMembershipGroup();
	if (memGroupVO != null) {
	    Collection groupDetailList = memGroupVO.getMembershipGroupDetailList();
	    if (groupDetailList != null) {
		// Search the transactions for the other members of the group to
		// see if any of them have a transaction that was done later
		// than
		// the transaction we are undoing
		Iterator groupDetailIterator = groupDetailList.iterator();
		MembershipGroupDetailVO memGroupDetailVO;
		MembershipVO memVO;
		boolean found = false;
		while (groupDetailIterator.hasNext() && !found) {
		    // Get the detail of the previous membership group
		    memGroupDetailVO = (MembershipGroupDetailVO) groupDetailIterator.next();

		    if (this.removeMemVO.getMembershipID().equals(memGroupDetailVO.getMembershipGroupDetailPK().getMembershipID())) {
			// Make sure that the context membership is the PA
			if (this.contextTransaction && !memGroupDetailVO.isPrimeAddressee()) {
			    throw new SystemException("The undo of the transaction must be initiated from the prime addressee of the membership group.");
			}
		    } else {
			// Get the transactions of the other group membership
			memVO = memGroupDetailVO.getMembership();

			// Now search their transactions for a transaction that
			// is
			// not undone and was done later than the transaction we
			// are undoing.
			Collection transList = memVO.getTransactionList();
			if (transList != null) {
			    Iterator transIterator = transList.iterator();
			    while (transIterator.hasNext() && !found) {
				MembershipTransactionVO memTransVO = (MembershipTransactionVO) transIterator.next();
				found = !memTransVO.isUndone() && memTransVO.getTransactionDate().after(this.removeTransVO.getTransactionDate());
				if (found) {
				    throw new SystemException("Membership '" + memVO.getMembershipNumber() + "' of the membership group has since been changed.");
				}
			    }
			}
		    }
		}
	    }
	} else {
	    checkPreviousMembershipGroup();
	}
    }

    private void checkPreviousMembershipGroup() throws RemoteException {
	// Get the membership group ID of the previous membership group.
	if (this.revertToMemVO != null) {
	    MembershipGroupVO prevMemGroupVO = this.revertToMemVO.getMembershipGroup();
	    if (prevMemGroupVO != null) {
		Integer prevMemGroupID = prevMemGroupVO.getMembershipGroupID();

		// If they are not currently in a group or the previous
		// membership
		// group was different then get all of the current members of
		// the
		// previous membership group and see if any of them have had a
		// transaction done on them after this transaction that is being
		// undone.

		MembershipGroupVO curMemGroupVO = this.removeMemVO.getMembershipGroup();
		if (curMemGroupVO == null || !prevMemGroupID.equals(curMemGroupVO.getMembershipGroupID())) {
		    Collection groupDetailList = prevMemGroupVO.getMembershipGroupDetailList();
		    if (groupDetailList != null) {
			// Use the membership IDs from the previous membership
			// group detail
			// to get the current state of the membership that was
			// in the previous group.
			// Remember that the previous membership group details
			// come from the
			// history file which does not contain the details of
			// each member in the group
			// at that time.
			MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
			Iterator groupDetailIterator = groupDetailList.iterator();
			MembershipVO groupMemVO;
			MembershipGroupDetailVO memGroupDetailVO;
			boolean found = false;
			while (groupDetailIterator.hasNext() && !found) {
			    // Get the detail of the previous membership group
			    memGroupDetailVO = (MembershipGroupDetailVO) groupDetailIterator.next();

			    // Get the current state of a membership from the
			    // previous membership group
			    groupMemVO = membershipMgrLocal.getMembership(memGroupDetailVO.getMembershipGroupDetailPK().getMembershipID());

			    // Now search their transactions for a transaction
			    // that is
			    // not undone and was done later than the
			    // transaction we are undoing.
			    Collection transList = groupMemVO.getTransactionList();
			    if (transList != null) {
				Iterator transIterator = transList.iterator();
				while (transIterator.hasNext() && !found) {
				    MembershipTransactionVO memTransVO = (MembershipTransactionVO) transIterator.next();
				    found = !memTransVO.isUndone() && memTransVO.getTransactionDate().after(this.removeTransVO.getTransactionDate());
				    if (found) {
					throw new SystemException("Undoing the transaction will cause the membership to be rejoined to a group and membership " + groupMemVO.getMembershipNumber() + " of the group has since been changed.");
				    }
				}
			    }
			}
			// while (groupDetailIterator.hasNext() && !found)
		    }
		    // if (groupDetailList != null)
		}
		// if (curMemGroupVO == null ||...
	    }
	    // if (prevMemGroupVO != null)
	}
	// if (this.revertToMemVO != null)
    }

    private boolean contextTransaction;

    /**
     * The payableitem for the payee
     */
    private PayableItemVO payableItemVO;

    /**
     * Are they the payee
     */
    private boolean payee;

    public boolean isPayee() {
	return this.payee;
    }

    /**
     * The membership transaction being removed.
     */
    public MembershipTransactionVO removeTransVO = null;

    /**
     * The membership transaction to revert to.
     */
    public MembershipTransactionVO revertToTransVO = null;

    public MembershipVO removeMemVO = null;

    public MembershipVO revertToMemVO = null;

    /**
     * Find the transaction to revert to.
     */
    private void findRevertToTransaction(boolean restrictToRenew) throws RemoteException {
	if (!restrictToRenew || // not restricted by transaction or is but is a
				// renew transaction
		(restrictToRenew && MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.removeTransVO.getTransactionTypeCode()))) {
	    // For all other transactions find the previous transaction
	    // that the membership can be reverted back to.

	    // Get the list of transactions for the membership that the undo
	    // transaction relates to.
	    MembershipVO memVO = this.removeTransVO.getMembership();
	    Collection transList = memVO.getTransactionList();

	    // Search back through the transactions for the membership for
	    // one that is prior to the transaction being undone.
	    if (transList != null && !transList.isEmpty()) {
		ArrayList memTransList = new ArrayList(transList);
		// Sort the transaction list in reverse date order.
		MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE, true);
		Collections.sort(memTransList, mtc);

		boolean foundRemoveTrans = false;
		MembershipTransactionVO memTransVO;
		Iterator transListIterator = memTransList.iterator();
		while (transListIterator.hasNext() && this.revertToTransVO == null) {
		    memTransVO = (MembershipTransactionVO) transListIterator.next();

		    if (foundRemoveTrans) {
			// In a previous pass we found the transaction we are
			// undoing
			// so now we can find the transaction to revert back to.
			if (!memTransVO.isUndone()) {
			    // We can revert back to this transaction
			    this.revertToTransVO = memTransVO;

			    // Fetch the membership state to revert back to.
			    MembershipHistory memHistory = memTransVO.getMembershipHistory();
			    if (memHistory != null) {
				// This is a little bit dodgy and is only
				// possible because
				// we know that a membership history object is
				// implemented by
				// a membership value object. Realy should have
				// a constructor
				// on MembershipVO that takes a
				// MembershipHistory object.
				this.revertToMemVO = (MembershipVO) memHistory;
			    } else {
				throw new SystemException("No history exists for transaction '" + memTransVO.getTransactionID() + "'.");
			    }
			}
		    }

		    // See if we have found the transaction we are undoing
		    if (memTransVO.getTransactionID().equals(this.removeTransVO.getTransactionID())) {
			foundRemoveTrans = true;
		    }
		}
	    }
	} else {
	    if (restrictToRenew) {
		throw new SystemException("Only a renew transaction is supported for reverse.");
	    }
	}

	// both should be renews
	if (restrictToRenew && revertToTransVO != null && !revertToTransVO.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW)) {
	    throw new SystemException("The transaction being reverted to is not a renew transaction.");
	}

    }
}

class RemoveTransactionGroup {

    public RemoveTransactionGroup(Integer transID, boolean restrictToRenew) throws RemoteException {
	findTransactions(transID, restrictToRenew);
    }

    public int getTransactionListSize() {
	return this.transactionList.size();
    }

    public MembershipVO getRevertToMembershipVO(int index) {
	RemoveTransaction removeTransaction = (RemoveTransaction) this.transactionList.get(index);
	return removeTransaction.revertToMemVO;
    }

    public MembershipTransactionVO getRevertToTransactionVO(int index) {
	RemoveTransaction removeTransaction = (RemoveTransaction) this.transactionList.get(index);
	return removeTransaction.revertToTransVO;
    }

    public MembershipVO getRemoveMembershipVO(int index) {
	RemoveTransaction removeTransaction = (RemoveTransaction) this.transactionList.get(index);
	return removeTransaction.removeMemVO;
    }

    public MembershipTransactionVO getRemoveTransactionVO(int index) {
	RemoveTransaction removeTransaction = (RemoveTransaction) this.transactionList.get(index);
	return removeTransaction.removeTransVO;
    }

    /**
     * Holds a list of RemoveTransaction objects. The RemoveTransaction class is
     * a private inner class to this class.
     */
    private Vector transactionList = new Vector();

    private void findTransactions(Integer transID, boolean restrictToRenew) throws RemoteException {
	RemoveTransaction removeTransaction = null;

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	MembershipTransactionVO contextMemTransVO = membershipMgr.getMembershipTransaction(transID);
	LogUtil.log(this.getClass(), "finding transactions");
	// Get the list of other membership transactions that were created at
	// the same time.
	int transGroupID = contextMemTransVO.getTransactionGroupID();
	if (transGroupID > 0) {
	    Collection transList = membershipMgr.findMembershipTransactionByTransactionGroupID(transGroupID);
	    // Hold the membership transactions in our private RemoveTransaction
	    // class
	    MembershipTransactionVO memTransVO;
	    if (transList != null) {
		Iterator memTxIt = transList.iterator();
		while (memTxIt.hasNext()) {
		    memTransVO = (MembershipTransactionVO) memTxIt.next();
		    removeTransaction = new RemoveTransaction(memTransVO, (memTransVO.getTransactionID().equals(contextMemTransVO.getTransactionID())), restrictToRenew);
		    this.transactionList.add(removeTransaction);
		}
	    }
	}

	// If no other transactions were involved just process the context
	// transaction.
	if (this.transactionList == null || this.transactionList.isEmpty()) {
	    removeTransaction = new RemoveTransaction(contextMemTransVO, true, restrictToRenew);
	    this.transactionList.add(removeTransaction);
	}

	if (removeTransaction == null) {
	    throw new SystemException("No remove transaction exists.");
	}

	// Transaction to remove must be a renew
	if (removeTransaction != null && restrictToRenew && !removeTransaction.removeTransVO.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW)) {
	    throw new SystemException("The transaction being removed is not a renew transaction for transaction '" + transID + "'.");
	}

    }

}
