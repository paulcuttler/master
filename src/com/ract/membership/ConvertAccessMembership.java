package com.ract.membership;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

@Entity
public class ConvertAccessMembership {

    public String getMarket() {
	return market;
    }

    public void setMarket(String market) {
	this.market = market;
    }

    public String getClientNumber() {
	return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
	this.clientNumber = clientNumber;
    }

    public String getInsurance() {
	return insurance;
    }

    public void setInsurance(String insurance) {
	this.insurance = insurance;
    }

    public String getMembership() {
	return membership;
    }

    public void setMembership(String membership) {
	this.membership = membership;
    }

    public String getTravel() {
	return travel;
    }

    public void setTravel(String travel) {
	this.travel = travel;
    }

    public String getTramada() {
	return tramada;
    }

    public void setTramada(String tramada) {
	this.tramada = tramada;
    }

    public DateTime getCreateDate() {
	return createDate;
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    @Id
    private String clientNumber;
    private String market;
    private String insurance;
    private String membership;
    private String travel;
    private String tramada;
    private DateTime createDate;

}
