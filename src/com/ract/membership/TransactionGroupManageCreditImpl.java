package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import com.ract.client.ClientVO;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.payment.FinanceVoucher;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * A refund transaction group implementation.
 * 
 * @author jyh
 * @version 1.0
 */

public class TransactionGroupManageCreditImpl extends TransactionGroup {

    private FinanceVoucher creditDisposal;

    public void setCreditDisposal(FinanceVoucher creditDisposal) {
	this.creditDisposal = creditDisposal;
    }

    public FinanceVoucher getCreditDisposal() {
	return creditDisposal;
    }

    public TransactionGroupManageCreditImpl(String transactionTypeCode, User user) throws RemoteException {
	super(transactionTypeCode, user);
    }

    public void createTransactionsForSelectedClients() throws RemoteException, ValidationException {
	doCreditDisposalTransaction();
    }

    // overrride default implementation
    public synchronized Collection submit() throws RemoteException, ValidationException {
	Collection transactionList = null;

	LogUtil.debug(this.getClass(), ">>>SUBMITTING TRANSACTION " + this.getTransactionGroupID() + "<<<");

	if (!this.submitted) {
	    MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
	    try {
		this.submitted = true;

		// a refund
		LogUtil.debug(this.getClass(), "Credit disposal");
		transactionList = memTransMgr.createCreditDisposal(this, this.getCreditDisposal());
	    }

	    catch (Exception e) {
		this.submitted = false;
		if (e instanceof ValidationException) {
		    throw (ValidationException) e;
		} else if (e instanceof SystemException) {
		    throw (SystemException) e;
		} else if (e instanceof RemoteException) {
		    throw (RemoteException) e;
		} else {
		    throw new SystemException(e);
		}
	    }
	}
	return transactionList;
    }

    private void doCreditDisposalTransaction() throws RemoteException, ValidationException {
	MembershipTransactionVO memTransVO = null;
	MembershipVO oldMemVO = null;
	MembershipVO newMemVO = null;
	SelectedClient selectedClient;
	ClientVO clientVO = null;
	DateTime transDate = getTransactionDate();
	Collection clientList = this.getSelectedClientList();

	Iterator clientListIT = clientList.iterator();
	// only one
	if (clientListIT.hasNext()) {
	    selectedClient = (SelectedClient) clientListIT.next();
	    clientVO = selectedClient.getClient();
	    oldMemVO = selectedClient.getMembership();
	    // Copy the membership being cancelled and set its status
	    newMemVO = oldMemVO.copy();
	    // Create a new transaction object for the membership transaction
	    memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.getTransactionGroupTypeCode(), this.getTransactionGroupTypeCode(), selectedClient.getMembershipGroupJoinDate());

	    memTransVO.setTransactionDate(transDate);
	    memTransVO.setUsername(this.getUserID());
	    memTransVO.setSalesBranchCode(this.getSalesBranchCode());
	    memTransVO.setMembership(oldMemVO);
	    memTransVO.setNewMembership(newMemVO);
	    memTransVO.setPrimeAddressee(true);

	    memTransVO.setEffectiveDate(transDate);
	    memTransVO.setEndDate(oldMemVO.getExpiryDate());

	    addMembershipTransaction(memTransVO);
	}
    }

}
