package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.ServletUtil;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @created 13 March 2003
 * @version 1.0
 */
@Entity
@Table(name = "mem_fee_type")
public class FeeTypeVO extends ValueObject implements Writable, CachedItem, Comparable {

    public boolean isJoinType() {
	return joinType;
    }

    public void setJoinType(boolean joinType) {
	this.joinType = joinType;
    }

    public boolean isOnRoad() {
	return onRoad;
    }

    public void setOnRoad(boolean onRoad) {
	this.onRoad = onRoad;
    }

    public boolean isTermMultiplier() {
	return termMultiplier;
    }

    public void setTermMultiplier(boolean termMultiplier) {
	this.termMultiplier = termMultiplier;
    }

    // These are the statuses applicable to a Fee type.
    public static final String STATUS_ACTIVE = "Active";

    public static final String STATUS_INACTIVE = "Inactive";

    @Id
    @Column(name = "fee_type_code")
    protected String feeTypeCode;

    @Column(name = "fee_status")
    protected String feeStatus;

    protected String description;

    @Column(name = "earned_account_id")
    protected Integer earnedAccountID;

    @Column(name = "unearned_account_id")
    protected Integer unearnedAccountID;

    private boolean termMultiplier;

    private boolean onRoad;

    private boolean joinType;

    /**
     * 
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "FeeType";

    /**
     * The list of allowable discounts for this fee spec The list of allowable
     * discounts for this fee spec
     */
    @Transient
    protected Collection allowableDiscountList;

    @Transient
    private MembershipAccountVO earnedAccount;

    @Transient
    private MembershipAccountVO unearnedAccount;

    /**
     * Constructors ***************************
     */

    public FeeTypeVO() {
    }

    public FeeTypeVO(String feeTypeCode, String feeStatus, String description, Integer earnedAccountID, Integer unearnedAccountID) {
	this.feeTypeCode = feeTypeCode;
	this.feeStatus = feeStatus;
	this.description = description;
	this.earnedAccountID = earnedAccountID;
	this.unearnedAccountID = unearnedAccountID;
    }

    /**
     * Getter methods ****************************
     * 
     * @return The feeTypeCode value
     */

    public String getFeeTypeCode() {
	return this.feeTypeCode;
    }

    public String getStatus() {
	return this.feeStatus;
    }

    public String getDescription() {
	return this.description;
    }

    public Integer getEarnedAccountID() {
	return this.earnedAccountID;
    }

    public MembershipAccountVO getEarnedAccount() throws RemoteException {
	// LogUtil.log(this.getClass(),"earnedAccountID="+earnedAccountID);

	if (this.earnedAccount == null && this.earnedAccountID != null && this.earnedAccountID.intValue() > 0) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.earnedAccount = refMgr.getMembershipAccount(this.earnedAccountID);
	    if (this.earnedAccount == null) {
		throw new RemoteException("Failed to get earned account '" + this.earnedAccountID + "'. It does not exist!");
	    }
	}
	return this.earnedAccount;
    }

    public Integer getUnearnedAccountID() {
	return this.unearnedAccountID;
    }

    public MembershipAccountVO getUnearnedAccount() throws RemoteException {
	// LogUtil.log(this.getClass(),"unearnedAccountID="+unearnedAccountID);
	if (this.unearnedAccount == null && this.unearnedAccountID != null && this.unearnedAccountID.intValue() > 0) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.unearnedAccount = refMgr.getMembershipAccount(this.unearnedAccountID);
	    if (this.unearnedAccount == null) {
		throw new RemoteException("Failed to get unearned account '" + this.unearnedAccountID + "'. It does not exist!");
	    }
	}
	return this.unearnedAccount;
    }

    /**
     * Return true if this fee specification has the specified accounts defined.
     * 
     * @param earnedAcc
     *            Description of the Parameter
     * @param unearnedAcc
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean hasAccounts(MembershipAccountVO earnedAcc, MembershipAccountVO unearnedAcc) {
	// log("hasAccounts: " + (earnedAcc == null ? "null" :
	// earnedAcc.toString()) + "," + (unearnedAcc == null ? "null" :
	// unearnedAcc.toString()) + " ? " + (earnedAccountID == null ? "null" :
	// earnedAccountID.toString()) + "," + (unearnedAccountID == null ?
	// "null" : unearnedAccountID.toString()));

	// Check the earened account
	if (this.earnedAccountID == null && earnedAcc != null) {
	    return false;
	}

	// A value of zero is equivalent to no account.
	if (this.earnedAccountID != null && this.earnedAccountID.intValue() > 0 && earnedAcc == null) {
	    return false;
	}

	if (this.earnedAccountID != null && earnedAcc != null && !this.earnedAccountID.equals(earnedAcc.getAccountID())) {
	    return false;
	}

	// Check the unearned account
	if (this.unearnedAccountID == null && unearnedAcc != null) {
	    return false;
	}

	// A value of zero is equivalent to no account.
	if (this.unearnedAccountID != null && this.unearnedAccountID.intValue() > 0 && unearnedAcc == null) {
	    return false;
	}

	if (this.unearnedAccountID != null && unearnedAcc != null && !this.unearnedAccountID.equals(unearnedAcc.getAccountID())) {
	    return false;
	}

	return true;
    }

    /**
     * Tests each discount to see if it is non-discretionary. If one is found
     * returns true, else false.
     * 
     * @param includeAutomaticDiscounts
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean hasAllowedDiscounts(boolean includeAutomaticDiscounts) throws RemoteException {
	boolean found = false;
	Collection discList = getAllowableDiscountList();
	if (discList != null && !discList.isEmpty()) {
	    DiscountTypeVO discount;
	    Iterator discListIterator = discList.iterator();
	    while (!found && discListIterator.hasNext()) {
		discount = (DiscountTypeVO) discListIterator.next();
		if (discount.isAutomaticDiscount()) {
		    found = includeAutomaticDiscounts;
		} else {
		    found = true;
		}
	    }
	}
	return found;
    }

    /**
     * Return true if the fee is considered to be earned immediately. The fee is
     * earned immediately if there is no unearned account defined.
     * 
     * @return The earned value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean isEarned() throws RemoteException {
	MembershipAccountVO acc = getUnearnedAccount();
	return acc == null;
    }

    /**
     * Return true if the fee is used to set the membership value. The fee is a
     * value fee if there is no unearned account defined.
     * 
     * @return The valueFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean isValueFee() throws RemoteException {
	MembershipAccountVO acc = getUnearnedAccount();
	LogUtil.debug(this.getClass(), "acc=" + acc);
	return acc != null;
    }

    /**
     * Return a copy of the list of discounts, as DiscountTypeVO, that are
     * allowed for this fee. These are the discounts that can be applied to the
     * fee Initialise the list if this is the first call. Can't have multiple
     * threads initialising the list at the same time so must be synchronized
     * 
     * @return The allowableDiscountList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public synchronized Collection getAllowableDiscountList() throws RemoteException {
	ArrayList discList = null;
	if (this.allowableDiscountList == null && this.feeTypeCode != null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.allowableDiscountList = refMgr.getFeeTypeDiscountList(this.feeTypeCode);
	}
	if (this.allowableDiscountList != null) {
	    discList = new ArrayList(this.allowableDiscountList);
	}
	return discList;
    }

    /**
     * Returs the specified discount type if it is an allowed discount type for
     * the fee type. Returns null if the discount type is not an allowable
     * discount type for the fee type.
     * 
     * @param discountTypeCode
     *            Description of the Parameter
     * @return The allowableDiscountType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public DiscountTypeVO getAllowableDiscountType(String discountTypeCode) throws RemoteException {
	DiscountTypeVO discTypeVO = null;
	Collection discTypeList = getAllowableDiscountList();
	if (discTypeList != null) {
	    DiscountTypeVO tmpDiscTypeVO = null;
	    Iterator discTypeListIterator = discTypeList.iterator();
	    while (discTypeVO == null && discTypeListIterator.hasNext()) {
		tmpDiscTypeVO = (DiscountTypeVO) discTypeListIterator.next();
		if (tmpDiscTypeVO.getDiscountTypeCode().equals(discountTypeCode)) {
		    discTypeVO = tmpDiscTypeVO;
		}
	    }
	}
	return discTypeVO;
    }

    /**
     * Setter methods ************************
     * 
     * @param feeStatus
     *            The new feeStatus value
     */

    public void setFeeTypeCode(String feeTypeCode) throws RemoteException {
	checkReadOnly();
	this.feeTypeCode = feeTypeCode;
    }

    public void setFeeStatus(String feeStatus) throws RemoteException {
	checkReadOnly();
	this.feeStatus = feeStatus;
    }

    public void setEarnedAccountID(Integer earnedAccountID) throws RemoteException {
	checkReadOnly();
	this.earnedAccountID = earnedAccountID;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.getFeeTypeCode());
	desc.append("/" + this.getDescription());
	desc.append("/" + this.getStatus());
	desc.append("/" + this.getKey());
	desc.append("/" + this.isTermMultiplier());
	return desc.toString();
    }

    public void setUnearnedAccountID(Integer unearnedAccountID) throws RemoteException {
	checkReadOnly();
	this.unearnedAccountID = unearnedAccountID;
    }

    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    public synchronized void setAllowableDiscountList(Collection adl) throws RemoteException {
	checkReadOnly();
	this.allowableDiscountList = adl;
    }

    /**
     * Utility methods ***************************
     * 
     * @return Description of the Return Value
     */

    public FeeTypeVO copy() {
	FeeTypeVO feeTypeVO = new FeeTypeVO(this.feeTypeCode, this.feeStatus, this.description, this.earnedAccountID, this.unearnedAccountID);
	return feeTypeVO;
    }

    public void updateFromRequest(HttpServletRequest request) throws RemoteException {

	this.setFeeTypeCode(request.getParameter("feeTypeCode"));
	this.setDescription(request.getParameter("description"));
	this.setFeeStatus(request.getParameter("selectStatus"));
	this.setEarnedAccountID(NumberUtil.reqInteger(request.getParameter("selectEarned")));
	this.setUnearnedAccountID(NumberUtil.reqInteger(request.getParameter("selectUnearned")));
	this.setTermMultiplier(new Boolean(ServletUtil.getBooleanParam("termMultiplier", request)));
	this.setOnRoad(new Boolean(ServletUtil.getBooleanParam("onRoad", request)));
    }

    /**
     * CachedItem interface methods ***************
     * 
     * @return The key value
     */

    /**
     * Return the fee type type code as the cached item key.
     * 
     * @return The key value
     */
    public String getKey() {
	return this.feeTypeCode;
    }

    /**
     * Determine if the specified key matches the fee type code. Return true if
     * both the key and the fee type code are null.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("feeTypeCode", this.feeTypeCode);
	cw.writeAttribute("feeTypeStatus", this.feeStatus);
	cw.writeAttribute("earnedAccount", this.earnedAccountID);
	cw.writeAttribute("unEarnedAccount", this.unearnedAccountID);
	cw.endClass();
    }

    /************************ Comparable interface methods **********************/
    /**
     * Compare this product sort order to another products sort order
     */
    public int compareTo(Object obj) {
	if (obj != null && obj instanceof FeeTypeVO) {
	    FeeTypeVO feeTypeVO = (FeeTypeVO) obj;
	    return this.feeTypeCode.compareTo(feeTypeVO.getFeeTypeCode());
	} else {
	    return 0;
	}
    }

    public boolean equals(Object obj) {
	if (obj instanceof FeeTypeVO) {
	    FeeTypeVO that = (FeeTypeVO) obj;
	    return ((this.feeTypeCode.compareTo(that.getFeeTypeCode()) == 0));
	}
	return false;
    }

}
