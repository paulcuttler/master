package com.ract.membership;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.payment.PaymentMgrLocal;

public class RoadsideLedgerReportVoucherAction extends ActionSupport {

    @InjectEJB(name = "PaymentMgrBean")
    PaymentMgrLocal paymentMgrLocal;

    private List<Map<String, Object>> vouchers;

    public String execute() throws Exception {
	vouchers = paymentMgrLocal.getApprovedVouchers();

	return SUCCESS;
    }

    public List<Map<String, Object>> getVouchers() {
	return vouchers;
    }

    public void setVouchers(List<Map<String, Object>> vouchers) {
	this.vouchers = vouchers;
    }
}
