package com.ract.membership;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Title: RenewalNoticesRunnerOneOff Description: Executes a membership renewal
 * run for a one off run Copyright: Copyright (c) 2003 Company: RACT
 * 
 * @author dgk 2/7/03
 * @version 1.0
 */

public class RenewalNoticesJob implements Job {
    public RenewalNoticesJob() {
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();
	try {
	    String lastDateString = dataMap.getString("lastDate");
	    DateTime lastDate = new DateTime(lastDateString);
	    String userId = dataMap.getString("userId");
	    RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	    renewalMgr.createMembershipRenewalNotices(lastDate, userId);
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }
}
