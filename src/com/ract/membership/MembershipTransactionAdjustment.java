package com.ract.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.util.Interval;
import com.ract.util.NumberUtil;
import com.ract.util.XMLHelper;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2003 Company:
 * RACT
 * 
 * @author
 * @created 8 April 2003
 * @version 1.0
 */

public class MembershipTransactionAdjustment implements Writable, Serializable, TransactionCredit {
    public static final String WRITABLE_CLASSNAME = "MembershipTransactionAdjustment";

    private String adjustmentTypeCode;

    /**
     * The amount of adjustment to apply. This is always a positive amount. If
     * isDebitAdjustment() returns true then the amount is to be substracted
     * from the total transaction fee. If isDebitAdjustment() return false then
     * the amount is to be added to the total transaction fee.
     */
    private BigDecimal adjustmentAmount;

    private String adjustmentReason;

    private AdjustmentTypeVO adjustmentTypeVO;

    /**
     * The period of free membership
     */
    private com.ract.util.Interval adjustmentPeriod;

    public MembershipTransactionAdjustment(String adjTypeCode, BigDecimal adjAmount, String adjReason) throws RemoteException {

	this.adjustmentTypeCode = adjTypeCode;
	AdjustmentTypeVO adjTypeVO = getAdjustmentTypeVO();

	if (adjTypeVO == null) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with an invalid adjustment type code '" + this.adjustmentTypeCode + "'.");
	}
	if (adjAmount == null) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with a null adjustment amount.");
	}
	if (adjAmount.doubleValue() == 0.0) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with a zero adjustment amount.");
	}
	if (adjAmount.doubleValue() < 0.0) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with a negative adjustment amount.");
	}

	this.adjustmentAmount = adjAmount;
	this.adjustmentReason = adjReason;
    }

    public MembershipTransactionAdjustment(String adjTypeCode, BigDecimal adjAmount, Interval adjPeriod, String adjReason) throws RemoteException {
	this.adjustmentTypeCode = adjTypeCode;
	AdjustmentTypeVO adjTypeVO = getAdjustmentTypeVO();

	if (adjTypeVO == null) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with an invalid adjustment type code '" + this.adjustmentTypeCode + "'.");
	}
	if (adjAmount == null) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with a null adjustment amount.");
	}
	if (adjAmount.doubleValue() == 0.0) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with a zero adjustment amount.");
	}
	if (adjAmount.doubleValue() < 0.0) {
	    throw new SystemException("Cannot create a MembershipTransactionAdjustment with a negative adjustment amount.");
	}

	this.adjustmentAmount = adjAmount;
	this.adjustmentReason = adjReason;

	this.adjustmentPeriod = adjPeriod;
    }

    public MembershipTransactionAdjustment(Node adjustmentNode) throws RemoteException {
	if (adjustmentNode == null || !adjustmentNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
	    throw new SystemException("Failed to create MembershipTransactionAdjustment from XML node. The node is not valid.");
	}

	NodeList elementList = adjustmentNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;
	    // LogUtil.debug(this.getClass(),"nodeName="+nodeName+", valueText="+valueText);

	    if ("adjustmentAmount".equals(nodeName) && valueText != null) {
		this.adjustmentAmount = new BigDecimal(valueText).abs().setScale(2, BigDecimal.ROUND_HALF_UP);
	    } else if ("adjustmentReason".equals(nodeName) && valueText != null) {
		this.adjustmentReason = valueText;
	    } else if ("adjustmentType".equals(nodeName) && writableNode != null) {
		// LogUtil.debug(this.getClass(),"is adjustmentType");
		this.adjustmentTypeVO = new AdjustmentTypeVO(writableNode);
		if (this.adjustmentTypeVO != null) {
		    // LogUtil.debug(this.getClass(),"adjustmentType="+this.adjustmentTypeVO.toString());
		    this.adjustmentTypeCode = this.adjustmentTypeVO.getAdjustmentTypeCode();
		}
	    }
	    // LogUtil.debug(this.getClass(),"adjustmentTypeCode="+this.adjustmentTypeCode+", adjustmentAmount="+this.adjustmentAmount);

	}

	// Must have an adjustment type code
	if (this.adjustmentTypeCode == null) {
	    throw new SystemException("Failed to create MembershipTransactionAdjustment from XML data.  The adjustmentTypeCode is null.");
	}

	// Must have an adjustment amount
	if (this.adjustmentAmount == null) {
	    throw new SystemException("Failed to create MembershipTransactionAdjustment from XML data.  The adjustmentAmount is null.");
	}
    }

    /**
     * Return the amount of adjustment. This should always be a positive amount.
     * Use the isDebitAdjutsment() method to see how to apply the adjustment to
     * the total transaction fee.
     */
    public BigDecimal getAdjustmentAmount() {
	return adjustmentAmount;
    }

    /**
     * If the adjustment is a debit then return the adjustment amount as a
     * negative value.
     */
    public BigDecimal getSignedAdjustmentAmount() throws RemoteException {
	BigDecimal amt = null;
	if (isDebitAdjustment()) {
	    amt = this.adjustmentAmount.negate();
	} else {
	    amt = this.adjustmentAmount;
	}
	return amt;
    }

    public String getAdjustmentReason() {
	return adjustmentReason;
    }

    public String getAdjustmentTypeCode() {
	return adjustmentTypeCode;
    }

    public AdjustmentTypeVO getAdjustmentTypeVO() throws RemoteException {
	if (this.adjustmentTypeVO == null && this.adjustmentTypeCode != null) {
	    this.adjustmentTypeVO = MembershipEJBHelper.getMembershipRefMgr().getAdjustmentTypeVO(this.adjustmentTypeCode);
	}
	return this.adjustmentTypeVO;
    }

    /**
     * If isDebitAdjustment() returns true then the adjustment amount is to be
     * substracted from the total transaction fee. If isDebitAdjustment()
     * returns false then the adjustment amount is to be added to the total
     * transaction fee.
     */
    public boolean isDebitAdjustment() throws RemoteException {
	AdjustmentTypeVO adjTypeVO = getAdjustmentTypeVO();
	return adjTypeVO.isDebitAdjustment();
    }

    /**
     * If isCreditAdjustment() returns true then the adjustment amount is to be
     * added to the total transaction fee. If isCreditAdjustment() returns false
     * then the adjustment amount is to be subtracted from the total transaction
     * fee.
     */
    public boolean isCreditAdjustment() throws RemoteException {
	AdjustmentTypeVO adjTypeVO = getAdjustmentTypeVO();
	return adjTypeVO.isCreditAdjustment();
    }

    public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
	this.adjustmentAmount = adjustmentAmount;
    }

    public void setAdjustmentReason(String adjustmentReason) {
	this.adjustmentReason = adjustmentReason;
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("adjustmentAmount", NumberUtil.formatValue(this.adjustmentAmount.doubleValue(), "########0.00"));
	cw.writeAttribute("adjustmentReason", this.adjustmentReason);
	cw.writeAttribute("adjustmentType", this.adjustmentTypeVO);
	cw.endClass();
    }

    public BigDecimal getTransactionCreditAmount() {
	return this.adjustmentAmount;
    }

    public void setTransactionCreditAmount(BigDecimal creditAmount) {
	setAdjustmentAmount(creditAmount);
    }

    public com.ract.util.Interval getAdjustmentPeriod() {
	return adjustmentPeriod;
    }

    public void setAdjustmentPeriod(com.ract.util.Interval adjustmentPeriod) {
	this.adjustmentPeriod = adjustmentPeriod;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("adjustmentTypeCode=" + this.getAdjustmentTypeCode());
	desc.append("\n");
	desc.append("adjustmentReason=" + this.getAdjustmentReason());
	desc.append("\n");
	desc.append("adjustmentPeriod=" + this.getAdjustmentPeriod());
	desc.append("\n");
	desc.append("adjustmentAmount=" + this.getAdjustmentAmount());
	return desc.toString();
    }

}
