package com.ract.membership;

import java.io.Serializable;

/**
 * Primary key class for MembershipGroup entity bean.
 */
public class MembershipGroupDetailPK implements Serializable {
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((groupID == null) ? 0 : groupID.hashCode());
	result = prime * result + ((membershipID == null) ? 0 : membershipID.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof MembershipGroupDetailPK))
	    return false;
	MembershipGroupDetailPK other = (MembershipGroupDetailPK) obj;
	if (groupID == null) {
	    if (other.groupID != null)
		return false;
	} else if (!groupID.equals(other.groupID))
	    return false;
	if (membershipID == null) {
	    if (other.membershipID != null)
		return false;
	} else if (!membershipID.equals(other.membershipID))
	    return false;
	return true;
    }

    private Integer membershipID;

    private Integer groupID;

    public void setGroupID(Integer groupID) {
	this.groupID = groupID;
    }

    public void setMembershipID(Integer membershipID) {
	this.membershipID = membershipID;
    }

    /**
     * @hibernate.key-property position="1" column="membership_id"
     * @return Integer
     */
    public Integer getMembershipID() {
	return membershipID;
    }

    /**
     * @hibernate.key-property position="2" column="group_id"
     * @return Integer
     */
    public Integer getGroupID() {
	return groupID;
    }

    /**
     * Default constructor.
     */
    public MembershipGroupDetailPK() {
    }

    /**
     * Construct with primary key attributes set.
     */
    public MembershipGroupDetailPK(Integer membershipID, Integer groupID) {
	this.membershipID = membershipID;
	this.groupID = groupID;
    }

    public String toString() {
	return this.membershipID.intValue() + "/" + this.groupID.intValue();
    }

}
