package com.ract.membership;

import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * A membership that participates in the same group as the associated
 * membership.
 * 
 * @hibernate.class table="mem_membership_group" lazy="false"
 * @hibernate.cache usage="read-write"
 */
public class MembershipGroupDetailVO extends ValueObject implements Writable, Comparable {
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((membershipGroupDetailPK == null) ? 0 : membershipGroupDetailPK.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof MembershipGroupDetailVO))
	    return false;
	MembershipGroupDetailVO other = (MembershipGroupDetailVO) obj;
	if (membershipGroupDetailPK == null) {
	    if (other.membershipGroupDetailPK != null)
		return false;
	} else if (!membershipGroupDetailPK.equals(other.membershipGroupDetailPK))
	    return false;
	return true;
    }

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "MembershipGroup";

    /**
     * The ID if the membership belonging to the group.
     */
    // protected Integer membershipID;

    /**
     * A reference to the membership value object corresponding to the
     * membershipID.
     */
    private MembershipVO membershipVO;

    /**
     * The ID of the membership group that the membership belongs to.
     */
    // protected Integer groupID;

    /**
     * The date the the membership was joined to the group.
     */
    protected DateTime groupJoinDate;

    /**
     * Indicates if the membership is the prime addressee of the group.
     */
    protected boolean primeAddressee = false;

    /**
     * Composite primary key
     */
    private MembershipGroupDetailPK membershipGroupDetailPK;

    /**
     * Default constructor
     */
    public MembershipGroupDetailVO() {

    }

    public MembershipGroupDetailVO(MembershipGroupDetailPK membershipGroupDetailPK, DateTime groupJoinDate, boolean primeAddressee) {
	this.setMembershipGroupDetailPK(membershipGroupDetailPK);
	this.setGroupJoinDate(groupJoinDate);
	this.setPrimeAddressee(primeAddressee);
    }

    public MembershipGroupDetailVO(Node node) throws SystemException {
	// LogUtil.log(this.getClass(),"MembershipGroupDetailVO Node "+XMLHelper.toXML(node));
	NodeList elementList = node.getChildNodes();
	Node elementNode;
	String valueText;
	Integer groupID = null;
	Integer membershipID = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);

	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;
	    }

	    if (valueText != null) {
		if ("groupID".equals(elementNode.getNodeName())) {
		    groupID = new Integer(valueText);
		} else if ("membershipID".equals(elementNode.getNodeName())) {
		    membershipID = new Integer(valueText);
		} else if ("groupJoinDate".equals(elementNode.getNodeName())) {
		    try {
			this.groupJoinDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			addWarning("The group join date '" + valueText + "' is not a valid date : " + pe.getMessage());
		    }
		} else if ("isDefault".equals(elementNode.getNodeName())) {
		    this.primeAddressee = Boolean.valueOf(valueText).booleanValue();
		}
	    }
	}
	this.setMembershipGroupDetailPK(new MembershipGroupDetailPK(membershipID, groupID));
	// LogUtil.debug(this.getClass(),"Node "+ this.toString());
	setReadOnly(true);
	try {
	    setMode(ValueObject.MODE_HISTORY);
	} catch (ValidationException ve) {
	    // Should be setting it to a valid value so pass the exception
	    // back as a system exception
	    throw new SystemException(ve.getMessage());
	}
    }

    /**
     * @hibernate.property type="com.ract.util.DateTimeCombined"
     * @hibernate.column name="group_join_date"
     * @hibernate.column name="group_join_time"
     * @return DateTime
     */
    public DateTime getGroupJoinDate() {
	return this.groupJoinDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="is_prime_addressee"
     */
    public boolean isPrimeAddressee() {
	return this.primeAddressee;
    }

    public synchronized MembershipVO getMembership() throws RemoteException {
	LogUtil.debug(this.getClass(), "getMembership history=" + isHistory());
	Integer membershipID = this.getMembershipGroupDetailPK().getMembershipID();
	LogUtil.debug(this.getClass(), "membershipID=" + membershipID);
	if (membershipID != null && this.membershipVO == null &&
	// unable to get membership at the same Point In Time using just the id
	// TODO enable PIT group retrieval
		!isHistory()) {
	    LogUtil.debug(this.getClass(), "getMembership1");
	    MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();// local?!!
	    LogUtil.debug(this.getClass(), "getMembership2");
	    // evicting the membership here rolls back pending updates to
	    // membership object.
	    this.membershipVO = memMgr.getMembership(membershipID);
	    LogUtil.debug(this.getClass(), "getMembership3");
	}
	LogUtil.debug(this.getClass(), "membership=" + membershipVO);
	return this.membershipVO;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public MembershipGroupDetailPK getMembershipGroupDetailPK() {
	return membershipGroupDetailPK;
    }

    /************************* Setter methods ********************************/

    public void setGroupJoinDate(DateTime groupJoinDate)
    // throws RemoteException
    {
	// checkReadOnly();
	this.groupJoinDate = groupJoinDate;
    }

    // public void setMembershipID(Integer membershipID)
    // throws RemoteException
    // {
    // checkReadOnly();
    // this.membershipID = membershipID;
    // this.setModified(true);
    // }

    public void setPrimeAddressee(boolean primeAddressee)
    // throws RemoteException
    {
	// checkReadOnly();
	this.primeAddressee = primeAddressee;
    }

    public void setMembershipGroupDetailPK(MembershipGroupDetailPK membershipGroupDetailPK) {
	this.membershipGroupDetailPK = membershipGroupDetailPK;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.getMembershipGroupDetailPK());
	desc.append(" ");
	desc.append(this.groupJoinDate);
	desc.append(" ");
	desc.append(this.primeAddressee);
	return desc.toString();
    }

    /**
     * The natural sort order for the membership group detail list is by group
     * join date.
     */
    public int compareTo(Object obj) {
	if (obj != null && obj instanceof MembershipGroupDetailVO) {
	    MembershipGroupDetailVO memGroupDetVO = (MembershipGroupDetailVO) obj;
	    return this.groupJoinDate.compareTo(memGroupDetVO.getGroupJoinDate());
	} else {
	    return 0;
	}
    }

    /********************** Writable interface methods ************************/

    /**
     * Write the classes data using the specified class writter.
     * 
     ** NOTE ** NOTE ** NOTE ** NOTE ** NOTE ** NOTE ** NOTE ** Do not write the
     * membership value object as this will cause a circular reference and the
     * writing will never stop. NOTE ** NOTE ** NOTE ** NOTE ** NOTE ** NOTE **
     * NOTE **
     */
    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("groupID", this.getMembershipGroupDetailPK().getGroupID());
	cw.writeAttribute("groupJoinDate", this.groupJoinDate);
	cw.writeAttribute("membershipID", this.getMembershipGroupDetailPK().getMembershipID());
	cw.writeAttribute("isDefault", this.primeAddressee);
	cw.endClass();
    }

}
