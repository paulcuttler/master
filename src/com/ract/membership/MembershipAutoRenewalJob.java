package com.ract.membership;

/**
 * Title: MembershipAutoRenewalJob Description: Automatically renews memberships
 * for which no amount is payable or for which payment is via direct debit.
 * firstDate and lastDate parameters may be supplied. If they are supplied
 * memberships expiry between those dates (inclusive) will be processed. If
 * first and last dates are not supplied, defaults of eight weeks ago and today
 * are used. Ie it will renew memberships expiring up to and including today.
 * Designed to be run from the Quartz timer. (See also
 * MembershipAutoRenewalRunner for an alternative which can be run from the UNIX
 * prompt or a UNIX cron job) Copyright: Copyright (c) 2003 Company: RACT
 * 
 * @author
 * @version 1.0
 */

public class MembershipAutoRenewalJob
// implements Job
{

    // public MembershipAutoRenewalJob()
    // {
    // }
    //
    // /**
    // * The job method
    // */
    // public void execute(JobExecutionContext context)
    // throws JobExecutionException
    // {
    // String instName = context.getJobDetail().getName();
    // String instGroup = context.getJobDetail().getGroup();
    // DateTime fromDate = null;
    // DateTime toDate = null;
    // //get the job data map
    // JobDataMap dataMap = context.getJobDetail().getJobDataMap();
    //
    // try
    // {
    // //get the parameters
    // // String providerURL = dataMap.getString("providerURL");
    // fromDate = (DateTime)dataMap.get("fromDate");
    // toDate = (DateTime)dataMap.get("toDate");
    // if(fromDate == null)
    // {
    // fromDate = new DateTime().getDateOnly().subtract(new Interval(0, 0, 56,
    // 0, 0, 0, 0));
    // }
    // if(toDate == null)
    // {
    // toDate = new DateTime().getDateOnly();
    // }
    //
    // RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
    // renewalMgr.processAutomaticRenewals(fromDate, toDate);
    //
    // }
    // catch(Exception e)
    // {
    // LogUtil.fatal(this.getClass(), e);
    // throw new JobExecutionException(e, false);
    // }
    //
    // }
}
