package com.ract.membership;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * Title: RenewalNoticesCronJob Description: Executes a membership renewal run
 * an auto timed job Requires the following parameters to be set into the data
 * map: int renewalInterval (days) Strin providerURL Copyright: Copyright (c)
 * 2003 Company: RACT
 * 
 * @author dgk 2/7/03
 * @version 1.0
 */

public class RenewalNoticesCronJob implements Job {
    public RenewalNoticesCronJob() {
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {
	String instName = context.getJobDetail().getName();
	String instGroup = context.getJobDetail().getGroup();

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();
	RenewalBatchVO batch = null;

	try {
	    int renewalInterval = dataMap.getInt("renewalInterval");
	    String userId = "COM";
	    RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	    batch = renewalMgr.getLastRun();
	    DateTime lastDate = batch.getLastDate().add(new Interval(0, 0, renewalInterval, 0, 0, 0, 0));
	    renewalMgr.createMembershipRenewalNotices(lastDate, userId);
	} catch (Exception e) {
	    e.printStackTrace();
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }
}