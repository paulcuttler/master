package com.ract.membership;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.SystemParameterVO;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.LogUtil;

/**
 * Common components for Email Advice processing.
 * 
 * @author newtong
 * 
 */
public class EmailAdviceBaseAction extends ActionSupport implements SessionAware {

	private Integer membershipId;
	private Integer clientNumber;
	private String adviceType;
	private Integer quoteNumber;
	private Integer directDebitScheduleId;
	private String emailFrom;
	private boolean success;

	protected Map<String, Object> session;
	private MembershipVO membershipVO;
	private ClientVO clientVO;
	private User user;

	/**
	 * InjectEJB annotation does not allow inheritance so this is the only way to do it.
	 */
	protected MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
	protected ClientMgrLocal clientMgrLocal = ClientEJBHelper.getClientMgrLocal();
	protected CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setMembershipId(Integer membershipId) {
		this.membershipId = membershipId;
	}

	public Integer getMembershipId() {
		if (this.membershipId == null) {
			// attempt to extract membershipId from quote
			if (this.getAdviceType().equals(SystemParameterVO.EMAIL_ADVICE_QUOTE) && this.getQuoteNumber() != null) {
				try {
					this.membershipId = membershipMgrLocal.getMembershipQuote(this.getQuoteNumber()).getMembershipID();
				} catch (Exception e) {
					LogUtil.fatal(this.getClass(), "Unable to retrieve membership quote by id: " + this.getQuoteNumber() + ", " + e);
				}
			}
		}
		return membershipId;
	}

	@RequiredStringValidator(message = "Please provide an advice type")
	public void setAdviceType(String adviceType) {
		this.adviceType = adviceType;
	}

	public String getAdviceType() {
		return adviceType;
	}

	public void setMembershipVO(MembershipVO membershipVO) {
		this.membershipVO = membershipVO;
	}

	protected MembershipVO getMembershipVO() {
		if (this.membershipVO == null) {
			try {
				this.membershipVO = membershipMgrLocal.getMembership(this.getMembershipId());
			} catch (Exception e) {
				LogUtil.warn(this.getClass(), "Unable to retrieve membership VO by id: " + this.getMembershipId() + ", " + e);
			}
		}
		return membershipVO;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		if (this.user == null) {
			UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);
			this.user = userSession.getUser();
		}
		return this.user;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailFrom() {
		if (this.emailFrom == null) {
			this.emailFrom = this.getUser().getEmailAddress();
		}
		return emailFrom;
	}

	public void setQuoteNumber(Integer quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public Integer getQuoteNumber() {
		return quoteNumber;
	}

	public void setDirectDebitScheduleId(Integer directDebitScheduleId) {
		this.directDebitScheduleId = directDebitScheduleId;
	}

	public Integer getDirectDebitScheduleId() {
		return directDebitScheduleId;
	}

	public Integer getClientNumber() {
		if (this.clientNumber == null) {
			// attempt to extract clientNumber from quote
			if (this.getAdviceType().equals(SystemParameterVO.EMAIL_ADVICE_QUOTE) && this.getQuoteNumber() != null) {
				try {
					this.clientNumber = membershipMgrLocal.getMembershipQuote(this.getQuoteNumber()).getClientNumber();
				} catch (Exception e) {
					LogUtil.fatal(this.getClass(), "Unable to retrieve membership quote by id: " + this.getQuoteNumber() + ", " + e);
				}
			} else if (this.getMembershipVO() != null) {
				this.clientNumber = this.getMembershipVO().getClientNumber();
			}
		}
		return clientNumber;
	}

	public void setClientNumber(Integer clientNumber) {
		this.clientNumber = clientNumber;
	}

	public ClientVO getClientVO() {
		if (this.clientVO == null) {
			try {
				this.clientVO = clientMgrLocal.getClient(this.getClientNumber());
			} catch (Exception e) {
				LogUtil.warn(this.getClass(), "Unable to retrieve client VO by id: " + this.getClientNumber() + ", " + e);
			}
		}
		return clientVO;
	}

	public void setClientVO(ClientVO clientVO) {
		this.clientVO = clientVO;
	}
}
