package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.ReferenceDataVO;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

abstract public class TransactionGroupCreateImpl extends TransactionGroup {

    public DateTime getEffectiveDate() {
	return effectiveDate;
    }

    public void setEffectiveDate(DateTime effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    public String getProfileCode() {
	return profileCode;
    }

    public void setProfileCode(String profileCode) {
	this.profileCode = profileCode;
    }

    public String getJoinReasonCode() {
	return joinReasonCode;
    }

    public void setJoinReasonCode(String joinReasonCode) {
	this.joinReasonCode = joinReasonCode;
    }

    public String joinReasonCode;
    public DateTime effectiveDate;
    public String profileCode;

    public TransactionGroupCreateImpl(User user, String transactionTypeCode, String joinReasonCode, DateTime effectiveDate, String profileCode) throws RemoteException {
	super(transactionTypeCode, user);
	this.joinReasonCode = joinReasonCode;
	this.effectiveDate = effectiveDate;
	this.profileCode = profileCode;
    }

    /**
     * Create a membership transaction for each of the clients that have been
     * selected to be transacted. If an existing membership group has been
     * identified then additional transactions may also need to be generated for
     * the group members even though they do not directly partake in this
     * transaction.
     */
    public void createTransactionsForSelectedClients() throws RemoteException, ValidationException {
	if (getSelectedClientCount() < 1) {
	    throw new SystemException("Cannot create membership transactions. The list of selected clients is empty.");
	}

	DateTime transactionDate = new DateTime();

	MembershipRefMgr memRefMgr = MembershipEJBHelper.getMembershipRefMgr();
	ProductVO productVO = null;

	if (joinReasonCode.equals(ReferenceDataVO.REASON_CMO)) {
	    productVO = memRefMgr.getProduct(ProductVO.PRODUCT_CMO);
	} else {
	    productVO = memRefMgr.getProduct(ProductVO.PRODUCT_ACCESS);
	}

	MembershipTypeVO membershipType = memRefMgr.getMembershipType(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);

	this.setSelectedProduct(productVO);
	this.setMembershipType(membershipType);

	ReferenceDataVO joinReason = null;

	SelectedClient selectedClient;
	MembershipTransactionVO memTransVO = null;
	MembershipVO oldMemVO = null;
	MembershipVO newMemVO = null;
	clearTransactionList();
	DateTime transDate = getTransactionDate();
	Collection clientList = getSelectedClientList();
	Iterator clientListIterator = clientList.iterator();

	DateTime expiryDate = null;
	try {
	    // add default term
	    expiryDate = effectiveDate.add(this.getMembershipTerm());
	} catch (Exception ex) {
	    // ignore
	}

	while (clientListIterator.hasNext()) // only one
	{
	    selectedClient = (SelectedClient) clientListIterator.next();

	    oldMemVO = selectedClient.getMembership();

	    if (oldMemVO == null) {
		LogUtil.log(this.getClass(), "createAccessMembership 2");
		joinReason = memRefMgr.getJoinReason(joinReasonCode);

		// create an access membership record
		newMemVO = new MembershipVO();
		newMemVO.setMembershipTypeCode(membershipType.getMembershipTypeCode());
		newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
		newMemVO.setMembershipNumber(selectedClient.getClient().getClientNumber().toString());
		newMemVO.setClient(selectedClient.getClient());

		// set the profile code
		newMemVO.setMembershipProfileCode(profileCode);
		newMemVO.setAllowToLapse(Boolean.valueOf(false));
		newMemVO.setJoinDate(effectiveDate);
		newMemVO.setCommenceDate(effectiveDate);
		newMemVO.setProductEffectiveDate(effectiveDate);
		newMemVO.setExpiryDate(expiryDate);
		newMemVO.setMembershipTerm(this.getMembershipTerm());

		// Access product
		newMemVO.setProduct(productVO);
		newMemVO.setMembershipValue(new BigDecimal(0)); // required?

		// create transaction
		memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, effectiveDate);

		memTransVO.setTransactionDate(transactionDate);
		memTransVO.setProductCode(productVO.getProductCode());
		memTransVO.setMembership(null); // no old membership
		memTransVO.setNewMembership(newMemVO);
		memTransVO.setGroupCount(new Integer(1));
		memTransVO.setTransactionReason(joinReason.getReferenceDataPK().getReferenceType(), joinReason.getReferenceDataPK().getReferenceCode());
		memTransVO.setPreferredCommenceDate(effectiveDate); // sets join date,etc on new mem
		memTransVO.setUsername(user.getUserID());
		memTransVO.setSalesBranchCode(user.getSalesBranchCode());
		memTransVO.setProductEffectiveDate();
		memTransVO.setPrimeAddressee(true);
		memTransVO.setEffectiveDate(effectiveDate);
		memTransVO.setEndDate(expiryDate); // keep in line with expiry date

	    } else {
		LogUtil.log(this.getClass(), "createAccessMembership 3");
		joinReason = memRefMgr.getRejoinReason(joinReasonCode);
		newMemVO = oldMemVO.copy();

		newMemVO.setMembershipProfileCode(profileCode);
		newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
		newMemVO.setAllowToLapse(Boolean.valueOf(false));
		newMemVO.setJoinDate(effectiveDate);
		newMemVO.setCommenceDate(effectiveDate);
		newMemVO.setProductEffectiveDate(effectiveDate);
		newMemVO.setExpiryDate(expiryDate);
		newMemVO.setMembershipTerm(this.getMembershipTerm());

		newMemVO.setNextProductCode(null);

		// Since the newMemVO is a copy of the oldMemVO you only need to
		// set the things that have changed. TB

		newMemVO.setProduct(productVO);

		// TODO change?
		newMemVO.setMembershipValue(new BigDecimal(0));

		LogUtil.log(this.getClass(), "createAccessMembership 4");
		// create transaction
		memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN, MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN, effectiveDate);

		memTransVO.setTransactionDate(transactionDate);
		memTransVO.setProductCode(productVO.getProductCode());
		memTransVO.setUsername(user.getUserID());
		memTransVO.setSalesBranchCode(user.getSalesBranchCode());
		memTransVO.setMembership(oldMemVO);
		memTransVO.setNewMembership(newMemVO);
		memTransVO.setGroupCount(new Integer(1));
		memTransVO.setTransactionReason(joinReason.getReferenceDataPK().getReferenceType(), joinReason.getReferenceDataPK().getReferenceCode());
		memTransVO.setPreferredCommenceDate(effectiveDate);
		memTransVO.setProductEffectiveDate();
		memTransVO.setPrimeAddressee(true);

		memTransVO.setEffectiveDate(effectiveDate);
		memTransVO.setEndDate(newMemVO.getExpiryDate());

		LogUtil.log(this.getClass(), "createAccessMembership 5a");

	    }
	    addMembershipTransaction(memTransVO);
	}
    }

}
