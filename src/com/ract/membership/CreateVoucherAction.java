package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.Client;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.common.CommonMgrLocal;
import com.ract.common.PostalAddressVO;
import com.ract.common.SourceSystem;
import com.ract.payment.FinanceVoucher;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.finance.FinanceMgrLocal;
import com.ract.security.Privilege;
import com.ract.security.SecurityMgrLocal;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class CreateVoucherAction extends ActionSupport implements SessionAware, ValidationAware, Preparable {

    static public final String ESTATE_MATCHER = " \\(?ESTATE OF.*";

    private FinanceVoucher voucher;

    private boolean memberDetails;

    public boolean isMemberDetails() {
	return memberDetails;
    }

    public void setMemberDetails(boolean memberDetails) {
	this.memberDetails = memberDetails;
    }

    @InjectEJB(name = "MembershipRefMgrBean")
    private MembershipRefMgr membershipRefMgr;

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    @InjectEJB(name = "SecurityMgrBean")
    private SecurityMgrLocal securityMgrLocal;

    @InjectEJB(name = "FinanceMgrBean")
    private FinanceMgrLocal financeMgr;

    @InjectEJB(name = "PaymentMgrBean")
    private PaymentMgrLocal paymentMgr;

    private String creditAmount;

    @Override
    public void prepare() throws Exception {

	final String MASK_CURRENCY = "######0.00"; // TODO constant
	final String ESTATE_REPLACEMENT = "EST OF THE LATE ";

	refDataList = commonMgr.getReferenceDataListByType("MEMREFREASON"); // TODO
									    // constant

	// determine branch number from userSession
	UserSession userSession = (UserSession) this.getSession().get(LoginUIConstants.USER_SESSION);
	String branchId = Integer.toString(userSession.getUser().getBranchNumber());
	approvingUserList = securityMgrLocal.getUsersByPrivilegeAndBranch(Privilege.PRIVILEGE_APPROVE_CREDIT_DISPOSAL, branchId);

	memVO = membershipMgr.getMembership(membershipId);
	client = memVO.getClient();

	voucher = new FinanceVoucher();
	voucher.setApprovingUserId(approvingUserId);
	voucher.setParticulars(particulars);

	voucher.setDisposalMode(disposalMode);
	voucher.setDisposalType(disposalType);

	// eft
	voucher.setBsb(bsb);
	if (bankAccountNumber != null && bankAccountNumber.trim().length() > 0) {
	    voucher.setBankAccountNumber(bankAccountNumber);
	}

	// write off
	voucher.setCostCentre(costCentre);
	if (writeoffAccountNumber != null && writeoffAccountNumber.trim().length() > 0) {
	    voucher.setWriteOffAccountNumber(writeoffAccountNumber);
	}

	if (disposalType.equals(FinanceVoucher.TYPE_TRANSFER_TO_MEMBER)) {
	    voucher.setTfrMembershipNumber(tfrMembershipNumber);
	} else {
	    voucher.setTfrMembershipNumber(null);
	}
	voucher.setDisposalReason(disposalReason);
	voucher.setSourceSystem(SourceSystem.MEMBERSHIP.getAbbreviation());
	voucher.setSourceSystemReference(memVO.getMembershipID().toString());
	voucher.setDisposalAmount(disposalAmount);
	voucher.setFeeAmount(feeAmount);
	voucher.setClientNumber(memVO.getClientNumber().toString());

	LogUtil.debug(this.getClass(), "voucher=" + voucher);

	if (disposalType.equals(FinanceVoucher.TYPE_EFT_ACCOUNT)) {
	    voucher.setPayType(FinanceVoucher.PAY_TYPE_EFT);
	    voucher.setEft(true);
	} else {
	    voucher.setPayType(FinanceVoucher.PAY_TYPE_CHEQUE);
	    voucher.setEft(false);
	}

	if (disposalType.equals(FinanceVoucher.TYPE_CHEQUE_THIRD_PARTY)) {

	    voucher.setPayeeType(FinanceVoucher.PAYEE_TYPE_TP);
	    voucher.setThirdParty(true);

	    voucher.setPayeeName(chequeName);

	    if (address1 != null && address1.trim().length() > 0) {
		voucher.setAddress1(address1);
		voucher.setAddress2(address2);
		voucher.setState(state);
		voucher.setPostcode(postcode);
	    }

	} else {
	    voucher.setPayeeType(FinanceVoucher.PAYEE_TYPE_CLIENT);
	    voucher.setThirdParty(false);

	    ClientVO client = clientMgr.getClient(new Integer(voucher.getClientNumber()));

	    if (disposalType.equalsIgnoreCase(FinanceVoucher.TYPE_CHEQUE_MEMBER) && client.getStatus().equalsIgnoreCase(Client.STATUS_DECEASED)) {
		// Format: Est of the Late J Holliday
		StringBuilder clientName = new StringBuilder(ESTATE_REPLACEMENT);
		clientName.append(client.getFirstNameOnly().substring(0, 1));
		clientName.append(" ");

		// need to strip variations of 'ESTATE OF THE LATE', '(ESTATE OF
		// THE LATE)', 'ESTATE OF'
		String rawSurname = client.getSurname();
		rawSurname = Pattern.compile(ESTATE_MATCHER).matcher(rawSurname).replaceAll("");
		clientName.append(rawSurname);

		voucher.setPayeeName(clientName.toString());
	    } else {
		voucher.setPayeeName(client.getDisplayName());
	    }

	    PostalAddressVO address = client.getPostalAddress();
	    if (voucher.isEft() || memberDetails) // eft or use member details
	    {
		voucher.setAddress1(address.getAddressLine1().length() == 0 ? address.getAddressLine2() : address.getAddressLine1());
		voucher.setAddress2(address.getSuburb());
		voucher.setState(address.getState());
		voucher.setPostcode(address.getPostcode());
	    } else
	    // member cheque with supplied details
	    {
		if (memberAddress1 != null && memberAddress1.trim().length() > 0) {
		    voucher.setAddress1(memberAddress1);
		    voucher.setAddress2(memberAddress2);
		    voucher.setState(memberState);
		    voucher.setPostcode(memberPostcode);
		}
	    }

	    voucher.setPayeeEmailAddress(client.getEmailAddress());

	    // ignore fax number and ABN
	}

    }

    public String getCreditAmount() {
	return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
	this.creditAmount = creditAmount;
    }

    private Collection approvingUserList;

    public Collection getApprovingUserList() {
	return approvingUserList;
    }

    public void setApprovingUserList(Collection approvingUserList) {
	this.approvingUserList = approvingUserList;
    }

    public Collection getRefDataList() {
	return refDataList;
    }

    public void setRefDataList(Collection refDataList) {
	this.refDataList = refDataList;
    }

    private Collection refDataList;

    @Override
    public void validate() {

	// can only have one unprocessed voucher
	try {
	    FinanceVoucher existing = null;
	    boolean found = false;
	    Collection creditDisposals = financeMgr.getVouchersForClient(voucher.getClientNumber());
	    for (Iterator<FinanceVoucher> i = creditDisposals.iterator(); i.hasNext() && !found;) {
		existing = i.next();
		if (existing != null && existing.getStatus() == null) {
		    LogUtil.log(this.getClass(), "existing=" + existing);
		    found = true;
		    addActionError("There is already an unprocessed voucher " + existing.getDisposalId() + " for this client awaiting processing by " + existing.getApprovingUserId());
		}
	    }
	} catch (RemoteException e1) {
	    addActionError(e1.getMessage());
	}

	// check the reason
	if (voucher.getDisposalReason() != null && voucher.getDisposalReason().equalsIgnoreCase(FinanceVoucher.DISPOSAL_REASON_DECEASED) && !client.getStatus().equalsIgnoreCase(Client.STATUS_DECEASED)) {
	    addActionError("The reason for disposal is " + FinanceVoucher.DISPOSAL_REASON_DECEASED + " but the client is not marked as " + Client.STATUS_DECEASED + ". Client has a status of '" + client.getStatus() + "'.");
	}

	if (disposalType.equals(FinanceVoucher.TYPE_EFT_ACCOUNT)) {
	    if (client.getStatus().equals(Client.STATUS_DECEASED)) {
		addActionError("Unable to use EFT if the client is deceased.");
	    }

	    LogUtil.log(this.getClass(), "validate1 " + disposalType);
	    if (voucher.getBsb() == null || voucher.getBsb().length() != 6) {
		addActionError("BSB number must have 6 digits");
	    } else {
		LogUtil.log(this.getClass(), "validate2 " + disposalType);
		try {
		    String bankCode = paymentMgr.validateBsbNumber(this.bsb);
		    LogUtil.log(this.getClass(), "bankCode=" + bankCode);
		    if (bankCode == null || bankCode.trim().length() == 0) {
			LogUtil.log(this.getClass(), "validate3 " + disposalType);
			addActionError("Not a valid BSB. BSB=" + this.bsb + ", bank code=" + bankCode);
		    } else {
			voucher.setBankCode(bankCode);
		    }
		} catch (Exception e) {
		    addActionError(e.getMessage());
		}
	    }

	    if (voucher.getBankAccountNumber() != null) {
		if (voucher.getBankAccountNumber().length() > 9) {
		    addActionError("Account number must not exceed 9 digits");
		}
		if (voucher.getBankAccountNumber().length() < 4) {
		    addActionError("Account numbers with less than 4 digits are not acceptable");
		}
	    } else {
		addActionError("Account number not specified.");
	    }
	} else if (disposalType.equals(FinanceVoucher.TYPE_WRITE_OFF)) {
	    if (voucher.getWriteOffAccountNumber() != null) {
		try {
		    MembershipAccountVO account = membershipRefMgr.getMembershipAccount(voucher.getWriteOffAccountNumber());
		    if (account == null) {
			addActionError("Account " + voucher.getWriteOffAccountNumber() + " is not a registered account number in the roadside system.");
		    } else {
			if (!account.isWriteOff()) {
			    addActionError("Account " + voucher.getWriteOffAccountNumber() + " does not allow writeoffs.");
			}
		    }
		} catch (RemoteException e) {
		    addActionError(e.getMessage());
		}
	    } else {
		addActionError("Account number not specified.");
	    }
	    if (voucher.getCostCentre() == null) {
		addActionError("Cost centre not specified.");
	    } else {
		if (voucher.getCostCentre().length() != 2) {
		    addActionError("Cost centre must have 2 characters.");
		}
	    }
	}

	if (voucher.getApprovingUserId() == null || voucher.getApprovingUserId().trim().length() == 0) {
	    addActionError("No approving user selected.");
	}

	if (voucher.isFinanceSystem()) {
	    if (voucher.getAddress1() == null || voucher.getAddress1().length() < 2) {
		addActionError("Address not supplied");
	    }
	    if (voucher.getPayeeName() == null || voucher.getPayeeName().length() < 2) {
		addActionError("Payee name not supplied");
	    }
	}

	if (voucher.getParticulars().length() == 0) {
	    addActionError("Particulars not entered");
	}

	if (disposalType.equals(FinanceVoucher.TYPE_TRANSFER_TO_MEMBER)) {
	    if (tfrMembershipNumber == null || tfrMembershipNumber.trim().length() == 0) {
		addActionError("Transfer membership number is required during a transfer.");
	    } else {
		try {
		    Collection<MembershipVO> transferMemberships = membershipMgr.findMembershipByClientNumber(new Integer(tfrMembershipNumber));
		    if (transferMemberships == null || transferMemberships.size() == 0) {
			addActionError("No transfer membership exists with that client number.");
		    } else {
			MembershipVO transferMembership = transferMemberships.iterator().next();
			// TODO any restrictions on membership being transferred
			// to?
			ClientVO transferClient = transferMembership.getClient();
			if (!transferClient.isActive()) {
			    addActionError("Client being transferred to is not active. Client has a status of '" + transferClient.getStatus() + "'.");
			}
		    }
		} catch (Exception e) {
		    addActionError(e.getMessage());
		}

	    }
	}

	BigDecimal total = voucher.getTotalAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
	BigDecimal creditAmount = new BigDecimal(memVO.getCreditAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);

	LogUtil.debug(this.getClass(), "total=" + total);
	LogUtil.debug(this.getClass(), "creditAmount=" + creditAmount);

	if (total.compareTo(new BigDecimal(0)) < 0 || total.compareTo(creditAmount) > 0) {
	    addActionError("The total of the disposal amount and the fee amount " + total + " cannot exceed the credit amount " + creditAmount);
	}

    }

    private Map<String, Object> session = null;

    public Map<String, Object> getSession() {
	return session;
    }

    public void setSession(Map<String, Object> session) {
	this.session = session;
    }

    public String getParticulars() {
	return particulars;
    }

    public void setParticulars(String particulars) {
	this.particulars = particulars;
    }

    public String getChequeName() {
	return chequeName;
    }

    public void setChequeName(String chequeName) {
	this.chequeName = chequeName;
    }

    public String getAddress1() {
	return address1;
    }

    public void setAddress1(String address1) {
	this.address1 = address1;
    }

    public String getAddress2() {
	return address2;
    }

    public void setAddress2(String address2) {
	this.address2 = address2;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getPostcode() {
	return postcode;
    }

    public void setPostcode(String postcode) {
	this.postcode = postcode;
    }

    public String getBsb() {
	return bsb;
    }

    public void setBsb(String bsb) {
	this.bsb = bsb;
    }

    public String getTfrMembershipNumber() {
	return tfrMembershipNumber;
    }

    public void setTfrMembershipNumber(String tfrMembershipNumber) {
	this.tfrMembershipNumber = tfrMembershipNumber;
    }

    public String getDisposalReason() {
	return disposalReason;
    }

    public void setDisposalReason(String disposalReason) {
	this.disposalReason = disposalReason;
    }

    public Integer getMembershipId() {
	return membershipId;
    }

    public void setMembershipId(Integer membershipId) {
	this.membershipId = membershipId;
    }

    public String getDisposalType() {
	return disposalType;
    }

    public void setDisposalType(String disposalType) {
	this.disposalType = disposalType;
    }

    public BigDecimal getDisposalAmount() {
	return disposalAmount;
    }

    public void setDisposalAmount(BigDecimal disposalAmount) {
	this.disposalAmount = disposalAmount;
    }

    public BigDecimal getFeeAmount() {
	return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
	this.feeAmount = feeAmount;
    }

    private BigDecimal disposalAmount;

    private BigDecimal feeAmount;

    private String particulars;

    private String chequeName;

    // third party address details
    private String address1;

    private String address2;

    private String state;

    private String postcode;

    // member address details
    private String memberAddress1;

    public String getMemberAddress1() {
	return memberAddress1;
    }

    public void setMemberAddress1(String memberAddress1) {
	this.memberAddress1 = memberAddress1;
    }

    public String getMemberAddress2() {
	return memberAddress2;
    }

    public void setMemberAddress2(String memberAddress2) {
	this.memberAddress2 = memberAddress2;
    }

    public String getMemberState() {
	return memberState;
    }

    public void setMemberState(String memberState) {
	this.memberState = memberState;
    }

    public String getMemberPostcode() {
	return memberPostcode;
    }

    public void setMemberPostcode(String memberPostcode) {
	this.memberPostcode = memberPostcode;
    }

    public String getBankAccountNumber() {
	return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
	this.bankAccountNumber = bankAccountNumber;
    }

    public String getWriteoffAccountNumber() {
	return writeoffAccountNumber;
    }

    public void setWriteoffAccountNumber(String writeoffAccountNumber) {
	this.writeoffAccountNumber = writeoffAccountNumber;
    }

    private String memberAddress2;

    private String memberState;

    private String memberPostcode;

    private MembershipVO memVO;

    public FinanceVoucher getVoucher() {
	return voucher;
    }

    public void setVoucher(FinanceVoucher voucher) {
	this.voucher = voucher;
    }

    public MembershipVO getMemVO() {
	return memVO;
    }

    public void setMemVO(MembershipVO memVO) {
	this.memVO = memVO;
    }

    private String bsb;

    private String bankAccountNumber;

    private String accountName;

    private String tfrMembershipNumber;

    private String writeoffAccountNumber;

    private String costCentre;

    private String approvingUserId;

    public String getApprovingUserId() {
	return approvingUserId;
    }

    public void setApprovingUserId(String approvingUserId) {
	this.approvingUserId = approvingUserId;
    }

    public String getCostCentre() {
	return costCentre;
    }

    public void setCostCentre(String costCentre) {
	this.costCentre = costCentre;
    }

    private String disposalReason;

    private Integer membershipId;

    private String disposalMode;

    public String getDisposalMode() {
	return disposalMode;
    }

    public void setDisposalMode(String disposalMode) {
	this.disposalMode = disposalMode;
    }

    private String disposalType;

    public String getAccountName() {
	return accountName;
    }

    public void setAccountName(String accountName) {
	this.accountName = accountName;
    }

    public Collection getTransactionList() {
	return transactionList;
    }

    public void setTransactionList(Collection transactionList) {
	this.transactionList = transactionList;
    }

    private ClientVO client;

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    /**
     * @return
     */
    public String execute() throws Exception {

	TransactionGroupManageCreditImpl transGroup;

	// get logged in user
	UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);

	voucher.setUserId(userSession.getUserId());
	voucher.setCreateDate(new DateTime());

	String transTypeCode = null;

	if (disposalType.equals(FinanceVoucher.TYPE_WRITE_OFF)) {
	    voucher.setWriteOff(true);
	    transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_WRITE_OFF; // GL
	} else if (disposalType.equals(FinanceVoucher.TYPE_TRANSFER_TO_MEMBER)) {
	    transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER_CREDIT; // TFR
	    // CREDIT
	} else
	// must be a refund to member or third party
	{
	    transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_REFUND; // REFUND
	}

	voucher.setTransactionTypeCode(transTypeCode);

	transGroup = new TransactionGroupManageCreditImpl(transTypeCode, userSession.getUser());

	LogUtil.log(this.getClass(), "voucher=" + voucher.toString());

	transGroup.setMembershipType(memVO.getMembershipType());
	transGroup.setSelectedProduct(memVO.getProduct());
	LogUtil.log(this.getClass(), "execute 1");
	String message = transGroup.addSelectedClient(memVO);
	if (message != null && message.trim().length() > 0) {
	    addActionError(message);
	    return ERROR;
	}
	LogUtil.log(this.getClass(), "execute 2");
	transGroup.setContextClient(memVO);
	LogUtil.log(this.getClass(), "execute 3");
	transGroup.createTransactionsForSelectedClients();
	LogUtil.log(this.getClass(), "execute 4");
	transGroup.setCreditDisposal(voucher);
	LogUtil.log(this.getClass(), "execute 5");
	client = transGroup.getContextClient();
	LogUtil.log(this.getClass(), "execute 6");
	transactionList = transGroup.submit();
	LogUtil.log(this.getClass(), "transactionList" + transactionList.size());

	return SUCCESS;
    }

    private Collection transactionList;
}