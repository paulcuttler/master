package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.util.DateTime;

/**
 * Holds details about an optional product benefit that has been added to the
 * base membership product. This is a normal java class that is populated and
 * stored along with the MembershipBean.
 * 
 * @author T. Bakker.
 */
public class MembershipProductOption implements Writable, Serializable {

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "MembershipProductOption";

    /**
     * The code for the social membership product option
     */
    public static final String SOCIAL_OPTION = "Social";

    /**
     * The date on which a product option becomes effective and the benefits
     * available through the product option can be used. If the product option
     * is added at join time or renewal time then the product option effective
     * date will be the same as the membership Commence date. If the product
     * option is added during the period of the membership then the product
     * option effective date will be set to the date it was added and paid for.
     */
    private DateTime effectiveDate;

    /**
     * The ID of the optional product benefit that has been added to the
     * membership. This may be used for display purposes.
     */
    private String productBenefitTypeCode;

    /**
     * A reference to the optional product benefit that has been added to the
     * base product option. Use the getBenefit() method to populate.
     */
    private ProductBenefitTypeVO productBenefitTypeVO;

    /******************************* Constructors ****************************/
    /**
     * Default constructor.
     */
    public MembershipProductOption(ProductBenefitTypeVO prodBenefitTypeVO, DateTime effectDate) throws RemoteException {
	if (prodBenefitTypeVO == null || effectDate == null) {
	    throw new SystemException("Failed to create MembershipProductOption. Product benefit type or effective date is null.");
	}
	this.productBenefitTypeCode = prodBenefitTypeVO.getProductBenefitCode();
	this.productBenefitTypeVO = prodBenefitTypeVO;
	this.effectiveDate = effectDate;
    }

    // This constructor is used when constructing an object to pass to the
    // equals method.
    public MembershipProductOption(String prodBenefitTypeCode) {
	this.productBenefitTypeCode = prodBenefitTypeCode;
    }

    public MembershipProductOption(String prodBenefitTypeCode, DateTime effectDate) {
	this.productBenefitTypeCode = prodBenefitTypeCode;
	this.effectiveDate = effectDate;
    }

    public MembershipProductOption(Node node) {
	NodeList elementList = node.getChildNodes();
	Node elementNode;
	String valueText;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);

	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;

	    }
	    if (valueText != null) {
		if ("productBenefitTypeCode".equals(elementNode.getNodeName())) {
		    this.productBenefitTypeCode = valueText;
		} else if ("effectiveDate".equals(elementNode.getNodeName())) {
		    try {
			this.effectiveDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			// ignore
		    }
		}
	    }
	}
    }

    /******************************* getter methods ****************************/
    /**
     * @return DateTime
     */
    public DateTime getEffectiveDate() {
	return this.effectiveDate;
    }

    /**
     * @return String
     */
    public String getProductBenefitTypeCode() {
	return this.productBenefitTypeCode;
    }

    /**
     * @return ProductBenefitTypeVO
     */
    public ProductBenefitTypeVO getProductBenefitType() throws RemoteException {
	if (this.productBenefitTypeCode != null && this.productBenefitTypeVO == null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.productBenefitTypeVO = refMgr.getProductBenefitType(this.productBenefitTypeCode);
	    if (this.productBenefitTypeVO == null) {
		throw new RemoteException("Failed to get product benefit type '" + this.productBenefitTypeCode + "'. It does not exist!");
	    }
	}
	return this.productBenefitTypeVO;
    }

    /**
     * Return the XML tag name to use when generating an XML document element
     * from this classes data.
     * 
     * @return String
     */
    public String getXMLTagName() {
	return "MembershipProductOption";
    }

    /**
   */
    public void setEffectiveDate(DateTime effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    /**
   */
    // public void setProductBenefitTypeCode(String prodBenefitTypeCode)
    // {
    // this.productBenefitTypeCode = prodBenefitTypeCode;
    // this.productBenefitTypeVO = null;
    // }
    //
    //
    // public void setProductBenefitType(ProductBenefitTypeVO prodBenefitTypeVO)
    // {
    // this.productBenefitTypeVO = prodBenefitTypeVO;
    // if (this.productBenefitTypeVO != null)
    // this.productBenefitTypeCode =
    // this.productBenefitTypeVO.getProductBenefitCode();
    // else
    // this.productBenefitTypeCode = null;
    // }

    /************************** Utility methods *******************************/

    public MembershipProductOption copy() {
	return new MembershipProductOption(this.productBenefitTypeCode, this.effectiveDate);
    }

    /**
     * Return true if the specified object is a MembershipProductOption and its
     * product benefit type code is the same as this product benefit type code.
     * Return false otherwise.
     */
    public boolean equals(Object obj) {
	boolean equal = false;
	if (obj != null && obj instanceof MembershipProductOption) {
	    MembershipProductOption that = (MembershipProductOption) obj;
	    equal = this.productBenefitTypeCode.equals(that.getProductBenefitTypeCode());
	}
	return equal;
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("productBenefitTypeCode", this.productBenefitTypeCode);
	cw.writeAttribute("effectiveDate", this.effectiveDate);
	cw.writeAttribute("productBenefitType", getProductBenefitType());
	cw.endClass();
    }
}