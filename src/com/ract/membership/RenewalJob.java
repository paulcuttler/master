package com.ract.membership;

import java.util.Hashtable;

import javax.naming.InitialContext;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Renewal job implementation
 * 
 * @author jyh
 */

public class RenewalJob implements Job {

    /**
     * Constructor for the RenewalJob object
     */
    public RenewalJob() {
    }

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	String instName = context.getJobDetail().getName();
	String instGroup = context.getJobDetail().getGroup();

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();

	try {

	    String providerURL = dataMap.getString("providerURL");

	    String initialContext = "org.jnp.interfaces.NamingContextFactory";
	    Hashtable environment = new Hashtable(2);
	    environment.put(javax.naming.Context.PROVIDER_URL, providerURL);
	    environment.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, initialContext);
	    InitialContext jndiContext = new javax.naming.InitialContext(environment);

	    Object ref = jndiContext.lookup("RenewalMgr");
	    RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	    int numRenewals = renewalMgr.createMembershipRenewalNotices(new DateTime(), "AUTO");
	    System.out.println(numRenewals + " renewals processed");

	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }

}