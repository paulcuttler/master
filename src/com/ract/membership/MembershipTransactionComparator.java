package com.ract.membership;

import java.util.Comparator;

/**
 * Title: MembershipTransactionComparator Description: Implements the comparison
 * of two MembershipTransactionVO objects. The two MembershipTransactionVO
 * objects can be compared by transaction ID transaction date membership group
 * join date Use the sort by constants to set which mode you want. The sort
 * order can also be set. Copyright: Copyright (c) 2002 Company: RACT
 * 
 * @author T. Bakker.
 * @version 1.0 6 Jan 2003
 */

public class MembershipTransactionComparator implements Comparator {
    public static final int SORTBY_TRANSACTION_ID = 0;

    public static final int SORTBY_TRANSACTION_DATE = 1;

    public static final int SORTBY_MEMBERSHIP_GROUP_JOIN_DATE = 2;

    private int mode = 0;

    private boolean reverse = false;

    /**
     * Default constructor uses sort by transaction ID in ascending order.
     */
    public MembershipTransactionComparator() {
    }

    /**
     * Constructor to specify the sort by mode and the sort direction.
     */
    public MembershipTransactionComparator(int sortByMode, boolean useReverseSortOrder) {
	setMode(sortByMode);
	setReverse(useReverseSortOrder);
    }

    public int compare(Object o1, Object o2) {
	if (o1 instanceof MembershipTransactionVO && o2 instanceof MembershipTransactionVO) {
	    MembershipTransactionVO memTransVO1 = (MembershipTransactionVO) o1;
	    MembershipTransactionVO memTransVO2 = (MembershipTransactionVO) o2;

	    switch (this.mode) {
	    case SORTBY_TRANSACTION_ID:
		return applySortOrder(memTransVO1.getTransactionID().compareTo(memTransVO2.getTransactionID()));
	    case SORTBY_TRANSACTION_DATE:
		return applySortOrder(memTransVO1.getTransactionDate().compareTo(memTransVO2.getTransactionDate()));
	    case SORTBY_MEMBERSHIP_GROUP_JOIN_DATE:
		return applySortOrder(memTransVO1.getMembershipGroupJoinDate().compareTo(memTransVO2.getMembershipGroupJoinDate()));
	    default:
		return 0;
	    }
	}
	return 0;
    }

    public boolean equals(Object obj) {
	if (obj instanceof MembershipTransactionComparator) {
	    MembershipTransactionComparator mtc = (MembershipTransactionComparator) obj;
	    if (this.mode == mtc.getMode() && this.reverse == mtc.isReverse()) {
		return true;
	    }
	}

	return false;
    }

    /**
     * The compareTo methods return a result for sorting in ascending order by
     * default. A negative integer means that the second object was less than
     * the first. A zero means that they were the same. A positive integer means
     * that the second object was greater than the first. If we want a reverse
     * sorted list then invert the result.
     */
    private int applySortOrder(int compareResult) {
	return (this.reverse ? compareResult * -1 : compareResult);
    }

    public int getMode() {
	return this.mode;
    }

    public boolean isReverse() {
	return this.reverse;
    }

    public void setMode(int sortByMode) {
	this.mode = sortByMode;
    }

    public void setReverse(boolean useReverseSortOrder) {
	this.reverse = useReverseSortOrder;
    }

}