package com.ract.membership.reporting;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.w3c.dom.Element;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.GenericException;
import com.ract.common.PrintForm;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SourceSystem;
import com.ract.common.reporting.Query;
import com.ract.common.reporting.QueryParameter;
import com.ract.common.reporting.QueryParameterPK;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.util.LogUtil;
import com.ract.util.PrintUtil;
import com.ract.util.XDocument;

/**
 * Holds all of the specific logic required to construct the report in the context of the reporting infrastructure.
 * 
 * @author T. Bakker, jyh
 * @created 12 May 2003
 * @version 1.0
 */

public class BranchSalesReportRequest extends XMLReportRequest {

	public static final String PARAMETER_PERIOD_START_DATE = "periodStartDate";
	public static final String PARAMETER_PERIOD_END_DATE = "periodEndDate";
	public static final String PARAMETER_SALES_BRANCH_LIST = "salesBranchList";
	public static final String PARAMETER_SALES_EXCLUDE_WEB = "excludeWeb";
	public static final String PARAMETER_SALES_GROUP_BY_TRANSACTION_SALES_BRANCH = "transactionSalesBranch";

	/**
	 * The name of this report.
	 */
	protected static final String REPORT_NAME = "mem/BranchSales_5";
	private XDocument doc = new XDocument();

	public BranchSalesReportRequest() {
		super.setReportName(REPORT_NAME);
	}

	public BranchSalesReportRequest(String dataServerContext, Hashtable parameterList, Hashtable qParameterList, ReportDeliveryFormat deliveryFormat) throws ReportException {
		super.setReportName(REPORT_NAME);
		this.setDataServerContext(dataServerContext);
		this.setDeliveryFormat(deliveryFormat);
		/* this.setParameterList(dynamicParameterList); */
		this.setQueryParameters(qParameterList);
		this.setParameterList(parameterList); // all parameters now handled the
		// same way
		try {
			Query query = this.assembleQuery(qParameterList);
			LogUtil.log(this.getClass(), "BranchSalesReportRequest 1 " + doc);
			Element element = (Element) CommonEJBHelper.getReportMgr().getXMLFromQuery(query);
			reportXML = (Element) doc.importNode(element, true);
			LogUtil.log(this.getClass(), "doc=" + doc.toXMLString());
			LogUtil.log(this.getClass(), "reportXML=" + reportXML);
			LogUtil.log(this.getClass(), "BranchSalesReportRequest 2 " + doc);
			doc.addLeaf(reportXML, "fromDate", (String) parameterList.get(PARAMETER_PERIOD_START_DATE));
			doc.addLeaf(reportXML, "lastDate", (String) parameterList.get(PARAMETER_PERIOD_END_DATE));
		} catch (RemoteException ex) {
			ex.printStackTrace();
			throw new ReportException(ex);
		}

	}

	public void initialSetting(String username, String printGroup) throws RemoteException {
		this.printerGroup = printGroup;
		this.setFormName("blank");
		this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
		this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		PrintForm printForm = cMgr.getPrintForm(formName, this.printerGroup);
		this.destinationFileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.MEMBERSHIP);
		this.setReportName("MembershipBranchSales");
		this.setReportTemplate("/Ract", "mem/BranchSales_6");
	}

	public static String WEB_SALES_BRANCH = "WB";

	public Query assembleQuery(Hashtable qParameterList) throws RemoteException {
		LogUtil.debug(this.getClass(), "getReportData");
		String periodStartDateStr = (String) qParameterList.get(PARAMETER_PERIOD_START_DATE);
		String periodEndDateStr = (String) qParameterList.get(PARAMETER_PERIOD_END_DATE);
		String salesBranchList = (String) qParameterList.get(PARAMETER_SALES_BRANCH_LIST);
		boolean excWeb = (Boolean) qParameterList.get(PARAMETER_SALES_EXCLUDE_WEB);
		boolean grpTxSalesBranch = (Boolean) qParameterList.get(PARAMETER_SALES_GROUP_BY_TRANSACTION_SALES_BRANCH);
		LogUtil.debug(this.getClass(), "periodStartDateStr=" + periodStartDateStr);
		LogUtil.debug(this.getClass(), "periodEndDateStr=" + periodEndDateStr);
		LogUtil.debug(this.getClass(), "salesBranchList=" + salesBranchList);
		LogUtil.debug(this.getClass(), "grpTxSalesBranch=" + grpTxSalesBranch);
		LogUtil.debug(this.getClass(), "excWeb=" + excWeb);

		// query string
		String sqlCmd = null;

		Query query = new Query();
		query.setQueryCode("branch sales");

		sqlCmd = "SELECT upper(sb.description) AS sales_branch, upper(us.us_username) AS username, upper(mm.product_code) AS prod_code," + " count(mt.transaction_id) as sales" + " FROM PUB.mem_membership mm INNER JOIN" + " PUB.mem_transaction mt ON mm.membership_id = mt.membership_id INNER JOIN" + " PUB.x_user us ON mt.username = us.us_user INNER JOIN ";

		if (grpTxSalesBranch) {
			sqlCmd += " PUB.[gn-slsbch] sb ON mt.sales_branch = sb.[sales-branch]";
		} else {
			sqlCmd += " PUB.[gn-slsbch] sb ON us.[sales-branch] = sb.[sales-branch]";
		}

		sqlCmd += " WHERE mt.undone_date IS NULL " + " AND mt.transaction_date >= ? " + " AND mt.transaction_date <= ? " + " AND mt.product_code IN ('Advantage', 'Ultimate', 'Non-Motoring', 'Lifestyle') " + " AND ((mt.transaction_type_code IN ('Rejoin', 'Create')" + " AND NOT EXISTS (SELECT 'x' FROM PUB.mem_transaction mt2" + "		WHERE mt2.transaction_date = mt.transaction_date " + "     AND mt2.membership_id = mt.membership_id " + "     AND mt2.transaction_type_code = 'Cancel' " + "     AND mt.reason_code <> '" + ReferenceDataVO.REJOIN_REASON_ON_ROAD + "')) " + " 	OR (mt.transaction_type_code = 'Change product'" + "     AND EXISTS(SELECT 'x' FROM PUB.mem_transaction mt2" + "         WHERE mm.membership_id = mt2.membership_id " + "         AND mt2.transaction_id < mt.transaction_id " + "         AND mt2.product_code = 'Access'))" + " OR (mt.transaction_type_code = '" + MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT + "'" + " 	AND mt.transaction_group_type_code in ('Renew','Rejoin')))";

		if (salesBranchList != null && !salesBranchList.equals("*") && !salesBranchList.equals("")) {
			if (grpTxSalesBranch) {
				sqlCmd += " AND mt.sales_branch in (" + salesBranchList + ") ";
			} else {
				sqlCmd += " AND us.[sales-branch] in (" + salesBranchList + ") ";
			}
		}

		if (excWeb) {
			sqlCmd += " AND mt.sales_branch <> '" + WEB_SALES_BRANCH + "' AND (mm.inceptionSalesBranch IS NULL OR mm.inceptionSalesBranch <> '" + WEB_SALES_BRANCH + "')";
		}

		sqlCmd += " GROUP BY sb.description, us.us_username, mm.product_code";
		sqlCmd += " ORDER BY sb.description, us.us_username, mm.product_code";

		LogUtil.log(this.getClass(), "sqlCmd=" + sqlCmd);

		query.setQueryText(sqlCmd);

		QueryParameter queryParameter1 = new QueryParameter(new QueryParameterPK(query.getQueryCode(), PARAMETER_PERIOD_START_DATE), QueryParameter.DATATYPE_DATE, "", "", 1);
		queryParameter1.setParameterValue(periodStartDateStr);
		QueryParameter queryParameter2 = new QueryParameter(new QueryParameterPK(query.getQueryCode(), PARAMETER_PERIOD_END_DATE), QueryParameter.DATATYPE_DATE, "", "", 2);
		queryParameter2.setParameterValue(periodEndDateStr);

		try {
			query.addQueryParameter(queryParameter1);
			query.addQueryParameter(queryParameter2);
		} catch (GenericException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return query;
	}

}
