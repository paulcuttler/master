package com.ract.membership.reporting;

public class MembershipDDScheduleRequest // extends XMLReportRequest
{
    // private static String NL = "\n";
    // private String adviceName = null;
    // private String productName = null;
    // XDocument doc = null;
    // private double totalDiscount = 0,
    // totalFee = 0,
    // totalCredit = 0,
    // totalAmount = 0,
    // gstAmount = 0,
    // creditAmountTotal = 0,
    // discountAmountTotal = 0;
    // private int groupLineCount = 0,
    // memberLineCount=0;
    //
    // public MembershipDDScheduleRequest(String userName,
    // String printGroup,
    // MembershipVO memVO) throws ReportException
    // {
    // try
    // {
    // initialSetting(userName, printGroup);
    // doc = new XDocument();
    // reportXML = (Element)doc.addNode(doc, "membership");
    // doc.addLeaf(reportXML, "userName", userName);
    // addMemberDetails(memVO,
    // reportXML);
    // addDDDetails(memVO,
    // doc.addNode(reportXML,"directDebit"));
    // }
    // catch(RemoteException ex)
    // {
    // throw new ReportException(ex);
    // }
    // }
    //
    // private void initialSetting(String username,
    // String printGroup) throws RemoteException
    // {
    // this.printerGroup = printGroup;
    // this.setFormName("memPlus");
    // this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
    // this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
    // CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
    // PrintForm printForm = cMgr.getPrintForm(formName, this.printerGroup);
    // this.destinationFileName = PrintUtil.getFileNameForPrinting(printForm,
    // SourceSystem.MEMBERSHIP);
    // this.setReportName("MembershipDDSchedule");
    // this.setReportTemplate("/Ract","mem/MembershipDDSchedule_6");
    // }
    //
    // private void addMemberDetails(MembershipVO memVO,
    // Node parent)throws ReportException
    // {
    // try
    // {
    // this.productName = memVO.getProduct().getDescription().toUpperCase();
    // ClientVO client = memVO.getClient();
    // if(client == null)
    // {
    // throw new ReportException("ClientVO null in printDirectDebitSchedule");
    // }
    // //postalVO has address details already processed.
    // PostalAddressVO address = client.getPostalAddress();
    // if(address == null)
    // {
    // throw new
    // ReportException("PostalAddressVO is null in printDirectDebitSchedule");
    // }
    // String clientTitle = null;
    // if(client instanceof PersonVO)
    // {
    // clientTitle = ((PersonVO)client).getTitle();
    // }
    // if(clientTitle == null)
    // {
    // clientTitle = "";
    // }
    // doc.addLeaf(parent, "clientTitle", clientTitle);
    //
    // doc.addLeaf(parent, "clientNo",
    // StringUtil.padNumber(memVO.getClientNumber(), 7));
    // String[] addressArray = new String[3];
    // addressArray[0] = address.getAddressLine1();
    // if(addressArray[0] == null)
    // {
    // addressArray[0] = "";
    // }
    // addressArray[1] = address.getAddressLine2();
    // if(addressArray[1] == null)
    // {
    // addressArray[1] = "";
    // }
    // addressArray[2] = address.getSuburb() + " " +
    // address.getState().toUpperCase() + " " +
    // address.getPostcode().toString();
    // StringUtil.removeBlankLines(addressArray, 0);
    //
    // doc.addLeaf(parent, "address1", addressArray[0].toUpperCase());
    // doc.addLeaf(parent, "address2", addressArray[1].toUpperCase());
    // doc.addLeaf(parent, "address3", addressArray[2].toUpperCase());
    // doc.addLeaf(parent, "postalName", client.getPostalName().toUpperCase());
    // doc.addLeaf(parent, "addressTitle", client.getAddressTitle());
    // this.addRefData(parent, "Member No:",
    // StringUtil.padNumber(client.getClientNumber(), 7));
    // this.addRefData(parent, "Roadside Cover:",
    // this.productName.toUpperCase());
    // this.addRefData(parent, "Pricing Option:", pricingOptionText(memVO));
    // this.addRefData(parent, "Commenced:",
    // memVO.getCommenceDate().formatShortDate());
    // this.addRefData(parent, "Next Renewal Date:",
    // memVO.getExpiryDate().formatShortDate());
    // }
    // catch(Exception ex)
    // {
    // throw new ReportException(ex);
    // }
    // }
    //
    //
    //
    //
    //
    //
    // private void addDDDetails(MembershipVO memVO,
    // Node parent) throws RemoteException
    // {
    //
    // PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
    // double amount = 0;
    // double schedAmount = 0;
    // DirectDebitSchedule ddSchedule = null;
    //
    // String ddMessage = CommonEJBHelper.getCommonMgr().getGnText("DD",
    // "SCHED",
    // "SCHEDROADSIDE");
    // Hashtable varList = new Hashtable();
    // varList.put("productName",
    // memVO.getProduct().getDescription().toUpperCase());
    // ddMessage = StringUtil.replaceTag(ddMessage, "<", ">", varList);
    // doc.addLeaf(parent,"ddMessage",ddMessage);
    // // get direct debit information if there is any.
    // StringBuffer directDebit = new StringBuffer();
    // directDebit.append("");
    //
    // DirectDebitAuthority ddAuthority = null;
    // try
    // {
    // ddAuthority =
    // paymentMgr.getDirectDebitAuthority(memVO.getDirectDebitAuthorityID());
    // }
    // catch(PaymentException ex2)
    // {
    // throw new RemoteException("Unable to get direct debit authority.",ex2);
    // }
    // //get account details
    // int dayToDebit = ddAuthority.getDayToDebit();
    // DirectDebitFrequency frequency = ddAuthority.getDeductionFrequency();
    // String paymentFrequency = frequency.getDescription();
    // StringBuffer bankDetails1 = new StringBuffer();
    // StringBuffer bankDetails2 = new StringBuffer();
    // BankAccount bankAccount = ddAuthority.getBankAccount();
    // if (bankAccount instanceof CreditCardAccount)
    // {
    // CreditCardAccount ccAccount = (CreditCardAccount)bankAccount;
    // bankDetails1.append(ccAccount.getCardTypeDescription());
    // bankDetails1.append(" ");
    // bankDetails1.append(StringUtil.formatCreditCardNumber(ccAccount.getCardNumber()));
    // bankDetails2.append("Expires "+ccAccount.getCardExpiry());
    // bankDetails2.append(" ");
    // bankDetails2.append(ccAccount.getAccountName());
    // //exp
    // ccAccount.getCardExpiry();
    // }
    // else if (bankAccount instanceof DebitAccount)
    // {
    // DebitAccount dAccount = (DebitAccount)bankAccount;
    // String branchName =
    // paymentMgr.validateBsbNumber(dAccount.getBsbNumber());
    // bankDetails1.append(branchName);
    // bankDetails2.append("BSB "+dAccount.getBsbNumber());
    // bankDetails2.append(" ");
    // bankDetails2.append("Account "+dAccount.getAccountNumber());
    // bankDetails2.append(" ");
    // bankDetails2.append(dAccount.getAccountName());
    // }
    // doc.addLeaf(parent,"bankAccountDetails1",bankDetails1.toString());
    // doc.addLeaf(parent,"bankAccountDetails2",bankDetails2.toString());
    // doc.addLeaf(parent,"paymentFrequency", paymentFrequency);
    //
    // //find the schedule(s)
    // Vector ddScheduleList = null;
    // try
    // {
    // ddScheduleList =
    // paymentMgr.getDirectDebitScheduleListByDirectDebitAuthority(ddAuthority);
    // }
    // catch(PaymentException ex3)
    // {
    // throw new RemoteException("Unable to get schedule list.", ex3);
    // }
    // //if there is a schedule
    // Node ddNode = null;
    // int paymentCount = 0;
    // if(ddScheduleList != null && ddScheduleList.size() > 0)
    // {
    // for(int x = 0; x < ddScheduleList.size(); x++)
    // {
    // ddSchedule = (DirectDebitSchedule)ddScheduleList.elementAt(x);
    // Collection payments = ddSchedule.getScheduleItemList();
    // Iterator ddIt = payments.iterator();
    // while(ddIt.hasNext())
    // {
    // DirectDebitScheduleItem item = (DirectDebitScheduleItem)ddIt.next();
    // if(item.isDisplayable())
    // {
    // ddNode = doc.addNode(parent, "payment");
    // paymentCount++;
    // doc.addLeaf(ddNode, "paymentDate",
    // item.getScheduledDate().formatShortDate());
    // doc.addLeaf(ddNode, "amount",
    // CurrencyUtil.formatDollarValue(item.getAmount()));
    // doc.addLeaf(ddNode, "column", paymentCount > 6 ? "2" : "1");
    // doc.addLeaf(ddNode,"status",item.getPrintableStatus());
    // }
    // }
    // }
    // }
    // }
    //
    //
    //
    //
    // private String pricingOptionText(MembershipVO memVO)
    // throws RemoteException
    // {
    // int groupSize = 1;
    // if(memVO.isGroupMember())
    // {
    // groupSize = memVO.getMembershipGroup().getMemberCount();
    // if(groupSize > MembershipHelper.SEGMENT_COUNT)
    // {
    // groupSize = MembershipHelper.SEGMENT_COUNT;
    // }
    // }
    // if(groupSize < 0)
    // {
    // groupSize = 1;
    // }
    // return MembershipHelper.SEGMENT_TEXT[groupSize - 1];
    // }
    //
    //
    // private void addRefData(Node parent, String title, String value)
    // {
    // Node thisNode = doc.addNode(parent,"refData");
    // doc.addLeaf(thisNode,"title",title);
    // doc.addLeaf(thisNode,"value",value);
    // }
    //

}
