package com.ract.membership.reporting;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ract.client.ClientVO;
import com.ract.client.PersonVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.PostalAddressVO;
import com.ract.common.PrintForm;
import com.ract.common.SourceSystem;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitFrequency;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.directdebit.DirectDebitScheduleItem;
import com.ract.user.User;
import com.ract.util.CurrencyUtil;
import com.ract.util.LogUtil;
import com.ract.util.PrintUtil;
import com.ract.util.StringUtil;
import com.ract.util.XDocument;

public class MembershipDDRequest extends XMLReportRequest {

    private String adviceName = null;
    private String productName = null;
    XDocument doc = null;
    private double totalDiscount = 0, totalFee = 0, totalCredit = 0, totalAmount = 0, gstAmount = 0, creditAmountTotal = 0, discountAmountTotal = 0;
    private int groupLineCount = 0, memberLineCount = 0;

    public MembershipDDRequest(User user, String printGroup, MembershipVO memVO, boolean includeSchedule, DirectDebitSchedule ddSchedule, boolean email) throws ReportException {
	try {
	    initialSetting(user.getUsername(), printGroup);
	    doc = new XDocument();
	    reportXML = (Element) doc.addNode(doc, "membership");
	    doc.addLeaf(reportXML, "userName", user.getUsername());
	    doc.addLeaf(reportXML, "userId", user.getUserID());
	    doc.addLeaf(reportXML, "email", email ? "true" : "false");
	    addMemberDetails(memVO, reportXML);
	    addDDDetails(memVO, includeSchedule, ddSchedule, doc.addNode(reportXML, "directDebit"));
	    LogUtil.log(this.getClass(), "doc=" + doc.toXMLString());
	} catch (RemoteException ex) {
	    throw new ReportException(ex);
	}
    }

    private void initialSetting(String username, String printGroup) throws RemoteException {
	this.printerGroup = printGroup;
	this.setFormName("memPlus");
	this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
	this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
	CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	PrintForm printForm = cMgr.getPrintForm(formName, this.printerGroup);
	this.destinationFileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.MEMBERSHIP);
	this.setReportName("MembershipDDSchedule");
	this.setReportTemplate("/Ract", "mem/MembershipDDSchedule_6");
    }

    private void addMemberDetails(MembershipVO memVO, Node parent) throws ReportException {
	try {
	    this.productName = memVO.getProduct().getDescription().toUpperCase();
	    ClientVO client = memVO.getClient();
	    if (client == null) {
		throw new ReportException("ClientVO null in printDirectDebitSchedule");
	    }
	    // postalVO has address details already processed.
	    PostalAddressVO address = client.getPostalAddress();
	    if (address == null) {
		throw new ReportException("PostalAddressVO is null in printDirectDebitSchedule");
	    }
	    String clientTitle = null;
	    if (client instanceof PersonVO) {
		clientTitle = ((PersonVO) client).getTitle();
	    }
	    if (clientTitle == null) {
		clientTitle = "";
	    }
	    doc.addLeaf(parent, "clientTitle", clientTitle);

	    doc.addLeaf(parent, "clientNo", StringUtil.padNumber(memVO.getClientNumber(), 7));
	    String[] addressArray = new String[3];
	    addressArray[0] = address.getAddressLine1();
	    if (addressArray[0] == null) {
		addressArray[0] = "";
	    }
	    addressArray[1] = address.getAddressLine2();
	    if (addressArray[1] == null) {
		addressArray[1] = "";
	    }
	    addressArray[2] = address.getSuburb() + " " + address.getState().toUpperCase() + " " + address.getPostcode().toString();
	    StringUtil.removeBlankLines(addressArray, 0);

	    doc.addLeaf(parent, "address1", addressArray[0].toUpperCase());
	    doc.addLeaf(parent, "address2", addressArray[1].toUpperCase());
	    doc.addLeaf(parent, "address3", addressArray[2].toUpperCase());
	    doc.addLeaf(parent, "postalName", client.getPostalName().toUpperCase());
	    doc.addLeaf(parent, "addressTitle", client.getAddressTitle());
	    this.addRefData(parent, "Member No:", StringUtil.padNumber(client.getClientNumber(), 7));
	    if (!ProductVO.PRODUCT_LIFESTYLE.equals(memVO.getProductCode()) && !ProductVO.PRODUCT_ACCESS.equals(memVO.getProductCode())) {
	    	this.addRefData(parent, "Roadside Cover:", this.productName.toUpperCase());
	    }
	    this.addRefData(parent, "Pricing Option:", pricingOptionText(memVO));
	    this.addRefData(parent, "Join Date:", memVO.getJoinDate().formatShortDate());
	    this.addRefData(parent, "Next Renewal Date:", memVO.getExpiryDate().formatShortDate());
	} catch (Exception ex) {
	    throw new ReportException(ex);
	}
    }

    private void addDDDetails(MembershipVO memVO, boolean includeSchedule, DirectDebitSchedule schedule, Node parent) throws RemoteException {
	LogUtil.log(this.getClass(), "Adding dd details");
	PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	double amount = 0;
	double schedAmount = 0;
	// DirectDebitSchedule ddSchedule = null;
	DirectDebitAuthority ddAuthority = null;

	String ddMessage = "";
	if (includeSchedule) {
	    ddMessage = CommonEJBHelper.getCommonMgr().getGnText("DD", "SCHED", "SCHEDROADSIDE");
	} else {
	    ddMessage = CommonEJBHelper.getCommonMgr().getGnText("DD", "ACCOUNT", "ACCROADSIDE");
	}

	Hashtable varList = new Hashtable();
	varList.put("productName", memVO.getProduct().getDescription().toUpperCase());
	ddMessage = StringUtil.replaceTag(ddMessage, "<", ">", varList);
	doc.addLeaf(parent, "ddMessage", ddMessage);
	doc.addLeaf(parent, "includeSchedule", includeSchedule ? "Yes" : "No");
	// get direct debit information if there is any.
	StringBuffer directDebit = new StringBuffer();
	// directDebit.append("");

	if (schedule != null) {
	    ddAuthority = schedule.getDirectDebitAuthority();
	} else {
	    try {
		ddAuthority = paymentMgr.getDirectDebitAuthority(memVO.getDirectDebitAuthorityID());
	    } catch (PaymentException ex2) {
		throw new RemoteException("Unable to get direct debit authority.", ex2);
	    }
	}

	// TODO DUPLICATE CODE!!!

	// get account details
	int dayToDebit = ddAuthority.getDayToDebit();
	DirectDebitFrequency frequency = ddAuthority.getDeductionFrequency();
	String paymentFrequency = frequency.getDescription();
	StringBuffer bankDetails1 = new StringBuffer();
	StringBuffer bankDetails2 = new StringBuffer();
	BankAccount bankAccount = ddAuthority.getBankAccount();
	if (bankAccount instanceof CreditCardAccount) {
	    CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
	    bankDetails1.append(ccAccount.getCardTypeDescription());
	    bankDetails1.append(" ");
	    bankDetails1.append(StringUtil.obscureFormattedCreditCardNumber(ccAccount.getCardNumber()));
	    bankDetails2.append("Expires " + ccAccount.getCardExpiry());
	    bankDetails2.append(" ");
	    bankDetails2.append(ccAccount.getAccountName());
	    // exp
	    ccAccount.getCardExpiry();
	} else if (bankAccount instanceof DebitAccount) {
	    DebitAccount dAccount = (DebitAccount) bankAccount;
	    String branchName = paymentMgr.validateBsbNumber(dAccount.getBsbNumber());
	    bankDetails1.append(branchName);
	    bankDetails2.append("BSB " + dAccount.getBsbNumber());
	    bankDetails2.append(" ");
	    bankDetails2.append("Account " + StringUtil.obscureBankAccountNumber(dAccount.getAccountNumber()));
	    bankDetails2.append(" ");
	    bankDetails2.append(dAccount.getAccountName());
	}
	doc.addLeaf(parent, "bankAccountDetails1", bankDetails1.toString());
	doc.addLeaf(parent, "bankAccountDetails2", bankDetails2.toString());
	doc.addLeaf(parent, "paymentFrequency", paymentFrequency);

	LogUtil.log(this.getClass(), "includeSchedule 1 " + includeSchedule);
	LogUtil.log(this.getClass(), "ddSchedule 1 " + schedule);
	if (includeSchedule) {

	    LogUtil.log(this.getClass(), "includeSchedule 2 " + includeSchedule);

	    int paymentCount = 0;

	    Node pNode = null;

	    if (schedule != null) {
		constructDDSched(parent, schedule, pNode, paymentCount);
	    } else {
		// ignore
		// find the schedule(s)
		// Vector ddScheduleList = null;
		// try
		// {
		// ddScheduleList =
		// paymentMgr.getDirectDebitScheduleListByDirectDebitAuthority(ddAuthority);
		// }
		// catch(PaymentException ex3)
		// {
		// throw new RemoteException("Unable to get schedule list.",
		// ex3);
		// }
		// //if there is a schedule
		//
		// if(ddScheduleList != null && ddScheduleList.size() > 0)
		// {
		// for(int x = 0; x < ddScheduleList.size(); x++)
		// {
		// ddSchedule =
		// (DirectDebitSchedule)ddScheduleList.elementAt(x);
		// LogUtil.log(this.getClass(), "ddSchedule = " + ddSchedule);
		//
		// constructDDSched(parent, ddSchedule, pNode, paymentCount);
		// }
		// }
	    }
	}
    }

    private void constructDDSched(Node parent, DirectDebitSchedule ddSchedule, Node pNode, int paymentCount) {
	Collection payments = ddSchedule.getScheduleItemList();
	Iterator ddIt = payments.iterator();
	while (ddIt.hasNext()) {
	    LogUtil.log(this.getClass(), "payments = " + payments);
	    DirectDebitScheduleItem item = (DirectDebitScheduleItem) ddIt.next();
	    if (item.isDisplayable()) {
		pNode = doc.addNode(parent, "payment");
		paymentCount++;
		doc.addLeaf(pNode, "paymentDate", item.getScheduledDate().formatShortDate());
		doc.addLeaf(pNode, "amount", CurrencyUtil.formatDollarValue(item.getAmount()));
		doc.addLeaf(pNode, "feeAmount", CurrencyUtil.formatDollarValue(item.getFeeAmount()));
		doc.addLeaf(pNode, "column", paymentCount > 6 && paymentCount > (payments.size() / 2) ? "2" : "1");
		doc.addLeaf(pNode, "status", item.getPrintableStatus());
	    }
	}
    }

    private String pricingOptionText(MembershipVO memVO) throws RemoteException {
	int groupSize = 1;
	if (memVO.isGroupMember()) {
	    groupSize = memVO.getMembershipGroup().getMemberCount();
	    if (groupSize > MembershipHelper.SEGMENT_COUNT) {
		groupSize = MembershipHelper.SEGMENT_COUNT;
	    }
	}
	if (groupSize < 0) {
	    groupSize = 1;
	}
	return MembershipHelper.SEGMENT_TEXT[groupSize - 1];
    }

    private void addRefData(Node parent, String title, String value) {
	Node thisNode = doc.addNode(parent, "refData");
	doc.addLeaf(thisNode, "title", title);
	doc.addLeaf(thisNode, "value", value);
    }

}
