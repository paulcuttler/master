package com.ract.membership.reporting;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Hashtable;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.extracts.ExtractHelper;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.util.FileUtil;

/**
 * Title: Branch discount report request. Description: Holds all of the specific
 * logic required to construct the report in the context of the reporting
 * infrastructure. Copyright (c) 2003 Company: RACT
 * 
 * @author T. Bakker
 * @created 12 May 2003
 * @version 1.0
 */

public class BranchSalesExtractRequest extends ReportRequestBase {
    /**
     * The name of this report.
     */
    public static final String REPORT_NAME = "RoadsideSalesBySalesBranchReport";

    /**
     * The name of the Elixir data source used by this report.
     */
    public static final String DATASOURCE = "RoadsideSalesBySalesBranch";

    public BranchSalesExtractRequest() {
	setReportNameAndDataSource();
    }

    public BranchSalesExtractRequest(String dataServerContext, Hashtable parameterList, ReportDeliveryFormat deliveryFormat) {
	setReportNameAndDataSource();

	this.setDataServerContext(dataServerContext);
	this.setDeliveryFormat(deliveryFormat);
	this.setParameterList(parameterList);
    }

    public void setReportNameAndDataSource() {
	super.setReportName(REPORT_NAME);
    }

    public void setDataServerContext(String dataServerContext) {
	super.setDataServerContext(dataServerContext);
    }

    public void setDeliveryFormat(ReportDeliveryFormat deliveryFormat) {
	super.setDeliveryFormat(deliveryFormat);
    }

    public String getReportData(Hashtable paramList) throws RemoteException {
	return getReportData();
    }

    public String getReportData() throws RemoteException {
	String suffixDateTime = ExtractHelper.getSuffixDateTime();

	Hashtable parameterList = this.getQueryParameters();

	String extractFile = (String) parameterList.get("BASE_DIR") + (String) parameterList.get("EXT_DIR") + (String) parameterList.get("EXT_FILE");

	String sqlCmdFile = (String) parameterList.get("BASE_DIR") + (String) parameterList.get("SQL_DIR") + (String) parameterList.get("SQL_RPT_FILE");

	HashMap sqlParams = (HashMap) parameterList.get("sqlParams");

	String sqlCmd = ExtractHelper.getSQLStatement(sqlCmdFile, sqlParams, suffixDateTime);

	// find out where to put the extract

	String extractTo = ExtractHelper.getExtractFilename(extractFile, suffixDateTime, ".xml");

	String xmlResult = CommonEJBHelper.getReportMgr().getXMLResultSet(sqlCmd, CommonConstants.DATASOURCE_CLIENT);

	try {
	    FileUtil.writeFile(extractTo, xmlResult);
	} catch (IOException ex) {
	    throw new RemoteException("Unable to write the extract to " + extractTo + ".", ex);
	}
	return xmlResult;
    }

}
