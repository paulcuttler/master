package com.ract.membership.reporting;

import java.util.Properties;

import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.membership.TransactionGroup;
import com.ract.user.User;
import com.ract.util.ReportUtil;

public class MembershipReportHelper {

    public static void printMembershipRenewal(User user, String printGroup, Properties renewalProperties, TransactionGroup transactionGroup) throws ReportException {
	ReportRequestBase rep = new MembershipRenewalAdviceRequest(user, printGroup, renewalProperties, transactionGroup, false);
	try {
	    ReportUtil.printReport(rep);
	} catch (Exception ex) {
	    throw new ReportException("Unable to print membership renewal.", ex);
	}
    }

    public static void emailMembershipRenewal(User user, String printGroup, Properties renewalProperties, TransactionGroup transactionGroup, MailMessage message, boolean smbMode) throws ReportException {
	ReportRequestBase rep = new MembershipRenewalAdviceRequest(user, printGroup, renewalProperties, transactionGroup, true);

	try {
	    ReportUtil.emailReport(rep, smbMode, message);
	} catch (Exception ex) {
	    throw new ReportException("Unable to print membership renewal.", ex);
	}
    }

}
