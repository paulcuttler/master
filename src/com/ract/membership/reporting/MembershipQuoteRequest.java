package com.ract.membership.reporting;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import javax.ejb.FinderException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ract.client.ClientVO;
import com.ract.client.PersonVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.PostalAddressVO;
import com.ract.common.PrintForm;
import com.ract.common.SourceSystem;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.membership.AdjustmentTypeVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipProfileVO;
import com.ract.membership.MembershipQuote;
import com.ract.membership.MembershipTransactionAdjustment;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.TransactionGroup;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.PrintUtil;
import com.ract.util.StringUtil;
import com.ract.util.XDocument;

public class MembershipQuoteRequest extends XMLReportRequest {
    private static String NL = "\n";
    private String adviceName = null;
    private String productName = null;
    private MembershipQuote memQuote;
    TransactionGroup transGroup = null;
    XDocument doc = null;
    private double totalDiscount = 0, totalFee = 0, totalCredit = 0, totalAmount = 0, gstAmount = 0, creditAmountTotal = 0, discountAmountTotal = 0;
    private int groupLineCount = 0, memberLineCount = 0;
    private MembershipTransactionVO transVo;

    public MembershipQuoteRequest(String userName, String printGroup, Integer quoteNumber, boolean email) throws ReportException {

	try {
	    initialSetting(userName, printGroup);
	    doc = new XDocument();
	    reportXML = (Element) doc.addNode(doc, "membership");
	    doc.addLeaf(reportXML, "userName", userName);
	    doc.addLeaf(reportXML, "email", email ? "true" : "false");
	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	    memQuote = membershipMgr.getMembershipQuote(quoteNumber);
	    transGroup = MembershipHelper.getTransactionGroup(memQuote.getTransactionXML());

	    // Calculate the transaction fee
	    transGroup.calculateTransactionFee();
	    addQuoteData(transGroup, reportXML);
	} catch (RemoteException ex) {
	    throw new ReportException(ex);
	} catch (FinderException fex) {
	    throw new ReportException(fex);
	}
    }

    private void initialSetting(String username, String printGroup) throws RemoteException {
	this.printerGroup = printGroup;
	this.setFormName("memPlus");
	this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
	this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
	CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	PrintForm printForm = cMgr.getPrintForm(formName, this.printerGroup);
	this.destinationFileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.MEMBERSHIP);
	this.setReportName("MembershipQuote");
	this.setReportTemplate("/Ract", "mem/MembershipAdvice_6");
    }

    private void addQuoteData(TransactionGroup transGroup, Node parent) throws ReportException, RemoteException, FinderException, RemoteException {
	MembershipTransactionVO transVO = transGroup.getMembershipTransactionForPA();
	MembershipVO memVO = transVO.getNewMembership();

	// what kind of quote is this? Single new, Upgrade to ultimate, group,
	// addmember etc

	StringBuffer groupList = new StringBuffer();
	String typeText = "";
	StringBuffer typeStr = new StringBuffer();

	int pricingSegment = MembershipHelper.getTransAndGroupData(typeStr, groupList, transGroup);
	typeText = CommonEJBHelper.getCommonMgr().getGnText("MEM", "QUOTE", "QTE" + typeStr.toString());
	// this.productName = memVO.getProduct().getDescription().toUpperCase();
	doc.addLeaf(parent, "bodyText1", typeText);

	if (pricingSegment > 1) {
	    this.addGroupData(transGroup, parent);
	}
	ClientVO client = memVO.getClient();
	if (client == null) {
	    throw new ReportException("ClientVO null in printMembershipAdvice");
	}
	String clientTitle = null;
	if (client instanceof PersonVO) {
	    clientTitle = ((PersonVO) client).getTitle();
	}
	doc.addLeaf(parent, "clientTitle", clientTitle);
	// postalVO has address details already processed.
	PostalAddressVO address = client.getPostalAddress();
	if (address == null) {
	    throw new ReportException("PostalAddressVO is null in printMembershipAdvice");
	}
	String[] addressArray = new String[3];
	addressArray[0] = address.getAddressLine1();
	if (addressArray[0] == null) {
	    addressArray[0] = "";
	}
	addressArray[1] = address.getAddressLine2();
	if (addressArray[1] == null) {
	    addressArray[1] = "";
	}
	addressArray[2] = address.getSuburb() + " " + address.getState().toUpperCase() + " " + address.getPostcode().toString();
	StringUtil.removeBlankLines(addressArray, 0);
	doc.addLeaf(parent, "address1", addressArray[0].toUpperCase());
	doc.addLeaf(parent, "address2", addressArray[1].toUpperCase());
	doc.addLeaf(parent, "address3", addressArray[2].toUpperCase());
	doc.addLeaf(parent, "postalName", client.getPostalName().toUpperCase());
	doc.addLeaf(parent, "addressTitle", client.getAddressTitle());
	this.addFeeDetail(parent, "Fees", CurrencyUtil.formatDollarValue(transGroup.getGrossTransactionFee()));
	doc.addLeaf(parent, "gst", CurrencyUtil.formatDollarValue(transGroup.getGstAmount()));
	double amount = transGroup.getAmountPayable();

	// include amount payable even if $0
	doc.addLeaf(parent, "amount", CurrencyUtil.formatDollarValue(amount));
	doc.addLeaf(parent, "amountPayable", CurrencyUtil.formatDollarValue(amount));
	doc.addLeaf(parent, "amountHeading", "Amount Payable");

	if (memVO.getCreditAmount() != 0) {
	    this.addFeeDetail(parent, "Credits", CurrencyUtil.formatDollarValue(memVO.getCreditAmount()));
	}
	double totalDiscounts = transGroup.getTotalDiscount();
	// + this.getDiscretionaryDiscount(transVO);
	MembershipTransactionAdjustment ma = transGroup.getAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT);
	if (ma != null && ma.getAdjustmentAmount() != null) {
	    totalDiscounts += ma.getAdjustmentAmount().doubleValue();
	}
	// transGroup.getAdjustmentList()

	if (totalDiscounts != 0) {
	    this.addFeeDetail(parent, "Discounts", CurrencyUtil.formatDollarValue(totalDiscounts));
	}
	String referenceHeading = null;
	if (transVO.isCreatingMembership()) {
	    referenceHeading = "Reference No.:";
	} else {
	    referenceHeading = "Membership No.:";
	}
	this.addRefData(parent, referenceHeading, StringUtil.padNumber(memVO.getClientNumber(), 7));
	// Pricing segment
	if (pricingSegment > MembershipHelper.SEGMENT_COUNT - 1) {
	    pricingSegment = MembershipHelper.SEGMENT_COUNT - 1;
	}

	String groupType = MembershipHelper.SEGMENT_TEXT[pricingSegment - 1];
	String bodyText = CommonEJBHelper.getCommonMgr().getGnText("MEM", "QUOTE", "BODY");
	Hashtable tagList = new Hashtable();
	tagList.put("productName", memVO.getProduct().getDescription().toUpperCase());
    if (!ProductVO.PRODUCT_LIFESTYLE.equals(memVO.getProductCode()) && !ProductVO.PRODUCT_ACCESS.equals(memVO.getProductCode())) {
    	this.addRefData(parent, "Roadside Cover:", memVO.getProduct().getDescription().toUpperCase());
    }
	this.addRefData(parent, "Pricing Option:", groupType);
	this.addRefData(parent, "Quote Date:", (new DateTime()).formatShortDate());
	this.addRefData(parent, "Quote No:", memQuote.getQuoteNumber().toString());
	doc.addLeaf(parent, "prodMessage", StringUtil.replaceTag(bodyText, "<", ">", tagList));
    }

    private void addGroupData(TransactionGroup transGroup, Node parent) throws ReportException {
	Vector transList = null;
	MembershipTransactionVO trans = null;
	ClientVO client = null;
	MembershipVO groupMemVo = null;
	Hashtable clientList = new Hashtable();
	int groupCount = 0;
	Node gpNode = doc.addNode(parent, "groupDetails");
	Node gMember = null;

	if (transGroup != null) {
	    transList = transGroup.getTransactionList();
	    for (int x = 0; x < transList.size(); x++) {
		trans = (MembershipTransactionVO) transList.get(x);
		try {
		    groupMemVo = trans.getNewMembership();
		    client = groupMemVo.getClient();

		} catch (RemoteException rex) {
		    throw new ReportException(rex);
		}
		if (!clientList.containsKey(client.getClientNumber())) {
		    clientList.put(client.getClientNumber(), "");
		    groupCount++;
		    groupLineCount++;
		    gMember = doc.addNode(gpNode, "groupMember");
		    doc.addLeaf(gMember, "clientNo", client.getClientNumber() + "");
		    doc.addLeaf(gMember, "postalName", client.getPostalName());
		    doc.addLeaf(gMember, "joinDate", groupMemVo.getJoinDate().formatShortDate());
		}
	    }
	}
	doc.addLeaf(gpNode, "groupCount", groupCount + "");
	doc.addLeaf(gpNode, "groupLineCount", groupLineCount + "");
    }

    private String pricingOptionText(MembershipVO memVO) throws RemoteException {
	int groupSize = 1;
	if (memVO.isGroupMember()) {
	    groupSize = memVO.getMembershipGroup().getMemberCount();
	    if (groupSize > MembershipHelper.SEGMENT_COUNT) {
		groupSize = MembershipHelper.SEGMENT_COUNT;
	    }
	}
	if (groupSize < 0) {
	    groupSize = 1;
	}
	return MembershipHelper.SEGMENT_TEXT[groupSize - 1];
    }

    private void addRefData(Node parent, String title, String value) {
	Node thisNode = doc.addNode(parent, "refData");
	doc.addLeaf(thisNode, "title", title);
	doc.addLeaf(thisNode, "value", value);
    }

    private void addFeeDetail(Node parent, String title, String value) {
	Node thisNode = doc.addNode(parent, "feeDetail");
	doc.addLeaf(thisNode, "title", title);
	doc.addLeaf(thisNode, "value", value);
    }

    private String memberProfileText(MembershipVO memVO) throws RemoteException {
	String pText = "";
	String pCode = memVO.getMembershipProfileCode();
	if (!pCode.equals(MembershipProfileVO.PROFILE_CLIENT)) {
	    pText = memVO.getMembershipProfile().getDescription();
	}
	return pText;
    }
    /*
     * private double getDiscretionaryDiscount(MembershipTransactionVO transVO)
     * throws ReportException { double discretionaryDiscount = 0; try{
     * MembershipTransactionHome transHome =
     * MembershipEJBHelper.getMembershipTransactionHome();
     * MembershipTransactionTypeVO transType = transVO.getTransactionType();
     * String transactionTypeCode = transType.getTransactionTypeCode();
     * 
     * if(transVO.getTransactionGroupID() > 0) { Collection transGroupList =
     * transHome.findByTransactionGroupID(transVO. getTransactionGroupID());
     * Iterator it = transGroupList.iterator(); MembershipTransaction gpTrans =
     * null; MembershipTransactionVO gpTransVO = null;
     * 
     * while(it.hasNext()) { gpTrans =
     * (MembershipTransaction)PortableRemoteObject.narrow(it.next(),
     * MembershipTransaction.class); gpTransVO = gpTrans.getVO();
     * discretionaryDiscount += gpTransVO.getDiscretionaryDiscountAmount().
     * doubleValue(); } } else { // processing for single memberships - not
     * transaction group discretionaryDiscount =
     * transVO.getDiscretionaryDiscountAmount(). doubleValue(); } }
     * catch(RemoteException rex) { throw new ReportException(rex); }
     * catch(FinderException fex) { throw new ReportException(fex); } return
     * discretionaryDiscount; }
     */
}
