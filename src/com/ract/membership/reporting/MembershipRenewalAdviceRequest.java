package com.ract.membership.reporting;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.PrintForm;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.membership.DiscountTypeVO;
import com.ract.membership.MembershipGroupDetailVO;
import com.ract.membership.MembershipProductOption;
import com.ract.membership.MembershipProfileVO;
import com.ract.membership.MembershipTransactionDiscount;
import com.ract.membership.MembershipTransactionFee;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.RenewalMgrBean;
import com.ract.membership.TransactionGroup;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitFrequency;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.directdebit.DirectDebitScheduleItem;
import com.ract.user.User;
import com.ract.util.CurrencyUtil;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.PrintUtil;
import com.ract.util.StringUtil;
import com.ract.util.XDocument;

public class MembershipRenewalAdviceRequest extends XMLReportRequest {

    private String adviceName = null;
    private String productName = null;
    private String privacyAttachmentName = null;
    private String privacyAttachmentFile = null;

    XDocument doc = null;
    private double totalDiscount = 0, totalFee = 0, totalCredit = 0, totalAmount = 0, gstAmount = 0, creditAmountTotal = 0, discountAmountTotal = 0;
    private int groupLineCount = 0, memberLineCount = 0;
    private MembershipTransactionVO transVo;

    public MembershipRenewalAdviceRequest(User user, String printGroup, Properties prop, TransactionGroup transGroup, boolean email) throws ReportException {
	try {
	    initialSetting(user.getUsername(), printGroup);
	} catch (RemoteException ex) {
	    throw new ReportException(ex);
	}
	doc = new XDocument();
	reportXML = (Element) doc.addNode(doc, "membership");
	doc.addLeaf(reportXML, "userName", user.getUsername());
	doc.addLeaf(reportXML, "userId", user.getUserID());
	doc.addLeaf(reportXML, "email", email ? "true" : "false");
	addRenewalData(prop, reportXML, transGroup);

    }

    @Override
    public String toString() {
	return "MembershipRenewalAdviceRequest [adviceName=" + adviceName + ", creditAmountTotal=" + creditAmountTotal + ", discountAmountTotal=" + discountAmountTotal + ", doc=" + doc + ", groupLineCount=" + groupLineCount + ", gstAmount=" + gstAmount + ", memberLineCount=" + memberLineCount + ", productName=" + productName + ", totalAmount=" + totalAmount + ", totalCredit=" + totalCredit + ", totalDiscount=" + totalDiscount + ", totalFee=" + totalFee + ", transVo=" + transVo + ", MIMEType=" + MIMEType + ", destinationFileName=" + destinationFileName + ", formName=" + formName + ", imageLocation=" + imageLocation + ", os=" + os + ", printerGroup=" + printerGroup + ", printerName=" + printerName + ", queryParameters=" + queryParameters + ", reportName=" + reportName + ", reportWorkspace=" + reportWorkspace + ", reportXML=" + reportXML + "]";
    }

    private void initialSetting(String username, String printGroup) throws RemoteException {
	LogUtil.log(this.getClass(), "username=" + username);
	LogUtil.log(this.getClass(), "printGroup=" + printGroup);

	this.printerGroup = printGroup;
	this.setFormName("memPlus");
	this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
	this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
	CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
	PrintForm printForm = cMgr.getPrintForm(formName, this.printerGroup);
	LogUtil.log(this.getClass(), "formName=" + formName);
	this.destinationFileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.MEMBERSHIP);
	this.setReportName("MembershipRenewal");
	this.setReportTemplate("/Ract", "mem/MembershipAdvice_6");

	this.setPrivacyAttachmentName(cMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "ADHOCRENEMAILPRIVACYNAME") + "." + FileUtil.EXTENSION_PDF);
	this.setPrivacyAttachmentFile(cMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "ADHOCRENEMAILPRIVACYFILE"));
    }

    private void addRenewalData(Properties prop, Node parent, TransactionGroup transGroup) throws ReportException {
	String paySlipMessage = null;
	DirectDebitSchedule ddSchedule = (DirectDebitSchedule) prop.get(RenewalMgrBean.KEY_DD_SCHEDULE);
	doc.addLeaf(parent, "clientTitle", prop.getProperty("clientTitle"));
	doc.addLeaf(parent, "clientNo", prop.getProperty("reference"));
	doc.addLeaf(parent, "address1", prop.getProperty("address1"));
	doc.addLeaf(parent, "address2", prop.getProperty("address2"));
	doc.addLeaf(parent, "address3", prop.getProperty("address3"));
	doc.addLeaf(parent, "postalName", prop.getProperty("postalName"));
	doc.addLeaf(parent, "addressTitle", prop.getProperty("addressTitle"));
	doc.addLeaf(parent, "heading", prop.getProperty("adviceHeading"));
	doc.addLeaf(parent, "footerMessage", "");
	doc.addLeaf(parent, "prodMessage", prop.getProperty("letterText"));
	doc.addLeaf(parent, "amount", prop.getProperty("amount"));
	doc.addLeaf(parent, "gst", prop.getProperty("gst"));
	doc.addLeaf(parent, "membershipNo", prop.getProperty("reference"));
	//
	// Membership Tier
	//
	doc.addLeaf(parent, "tierDescription", prop.getProperty("renewalTierDescription"));
	//
	this.addRefData(parent, "Member No:", prop.getProperty("reference"));
    if (!ProductVO.PRODUCT_LIFESTYLE.equals(prop.getProperty("prodName")) && !ProductVO.PRODUCT_ACCESS.equals(prop.getProperty("prodName"))) {
    	this.addRefData(parent, "Roadside Cover:", prop.getProperty("prodName"));
    }
	this.addRefData(parent, "Pricing Option:", prop.getProperty("pricingSegment"));
	this.addRefData(parent, "Renewal To:", prop.getProperty("renewTo"));

	doc.addLeaf(parent, "payBy", prop.getProperty("payBy"));
	doc.addLeaf(parent, "offerText", prop.getProperty("offerText"));
	doc.addLeaf(parent, "offerMessage", prop.getProperty("offerMessage"));
	doc.addLeaf(parent, "endMessage", prop.getProperty("endMessage"));

	addGroupData(transGroup, doc.addNode(parent, "groupDetails"));
	try {
	    if (ddSchedule != null) {
		paySlipMessage = CommonEJBHelper.getCommonMgr().getGnText("MEM", "MAN_REN", "PAYSLIPDD");

		addDDData(prop, ddSchedule, parent);
	    } else {
		paySlipMessage = CommonEJBHelper.getCommonMgr().getGnText("MEM", "MAN_REN", "PAYSLIPNODD");

		doc.addLeaf(parent, "payRefNo", prop.getProperty("ivrNumber"));
	    }
	    addFees(prop, parent);
	    doc.addLeaf(parent, "amountPayable", prop.getProperty("amount"));
	    doc.addLeaf(parent, "payslipMessage", paySlipMessage);
	} catch (RemoteException rex) {
	    throw new ReportException(rex);
	}
    }

    private void addFees(Properties prop, Node parent) throws ReportException {
	addFeeDet(parent, "Fees", prop);
	addFeeDet(parent, "Discounts", prop);
	addFeeDet(parent, "Credits", prop);
    }

    private void addFeeDet(Node parent, String feeName, Properties prop) {
	String feeAmount = "";
	String tName = "";
	if (prop.containsKey(feeName))
	    tName = feeName;
	else if (prop.containsKey(feeName.toLowerCase()))
	    tName = feeName.toLowerCase();
	else
	    return;
	feeAmount = prop.getProperty(tName);
	if (!feeAmount.equals("") && feeAmount != null) {
	    Node feeNode = doc.addNode(parent, "feeDetail");
	    doc.addLeaf(feeNode, "title", feeName);
	    doc.addLeaf(feeNode, "value", prop.getProperty(tName));
	}
    }

    private void addRefData(Node parent, String title, String value) {
	Node thisNode = doc.addNode(parent, "refData");
	doc.addLeaf(thisNode, "title", title);
	doc.addLeaf(thisNode, "value", value);
    }

    private void addGroupData(TransactionGroup transGroup, Node parent) throws ReportException {
	try {
	    MembershipVO memVO = transGroup.getMembershipGroupPA();
	    if (memVO != null && memVO.isGroupMember() && memVO.isPrimeAddressee()) {
		Collection groupList = memVO.getMembershipGroup().getMembershipGroupDetailList();
		Iterator it = groupList.iterator();
		MembershipGroupDetailVO groupMemDetailVo = null;
		// String memString = null;
		doc.addLeaf(parent, "groupCount", groupList.size() + "");
		Node gmNode = null;
		while (it.hasNext()) {
		    groupMemDetailVo = (MembershipGroupDetailVO) it.next();
		    gmNode = doc.addNode(parent, "groupMember");
		    assembleGroupMemberLine(groupMemDetailVo, transGroup, gmNode);
		}
		doc.addLeaf(parent, "groupLineCount", this.groupLineCount + "");
	    }
	} catch (Exception ex) {
	    throw new ReportException(ex);
	}

    }

    private void assembleGroupMemberLine(MembershipGroupDetailVO groupMemVO, TransactionGroup transGroup, Node parent) throws Exception {
	MembershipVO memVO = groupMemVO.getMembership();
	ClientVO client = memVO.getClient();
	boolean firstLineAdded = false;
	String thisLine = "";
	doc.addLeaf(parent, "clientNo", StringUtil.padNumber(client.getClientNumber(), 7));
	doc.addLeaf(parent, "postalName", client.getDisplayName());
	doc.addLeaf(parent, "joinDate", groupMemVO.getGroupJoinDate().formatShortDate());
	//
	// Add the extras node
	//
	Node extrasNode = null;
	extrasNode = doc.addNode(parent, "extras");
	//
	// Membership Tier
	//
	doc.addLeaf(extrasNode, "extra", (String) RenewalMgrBean.getRenewalTierCodeAndDescription(memVO).get(RenewalMgrBean.KEY_TIER_DESCRIPTION));
	this.groupLineCount++;
	//
	// Profile Code ?
	//
	if (!memVO.getMembershipProfileCode().equals(MembershipProfileVO.PROFILE_CLIENT) && memVO.getMembershipProfileCode().trim().length() > 0) {
	    doc.addLeaf(extrasNode, "extra", memVO.getMembershipProfileCode().trim());
	    this.groupLineCount++;
	}
	//
	// Social Member ?
	//
	if (memVO.hasProductOption(MembershipProductOption.SOCIAL_OPTION)) {
	    this.groupLineCount++;
	    doc.addLeaf(extrasNode, "extra", "Social Member");
	}

	//
	// Discounts ?
	//

	try {
	    MembershipTransactionVO transVO = transGroup.getMembershipTransactionForClient(client.getClientNumber());
	    MembershipTransactionDiscount discount = null;
	    Collection feeList = transVO.getTransactionFeeList();
	    Iterator feeListIterator = feeList.iterator();
	    Iterator discountIterator = null;
	    MembershipTransactionFee fee = null;
	    ArrayList discountList = null;

	    while (feeListIterator.hasNext()) {
		fee = (MembershipTransactionFee) feeListIterator.next();
		discountList = fee.getDiscountList();
		discountIterator = discountList.iterator();

		while (discountIterator.hasNext()) {
		    discount = (MembershipTransactionDiscount) discountIterator.next();

		    if (discount.getDiscountAmount() != 0) {
			if (discount.getDiscountCode().equals(DiscountTypeVO.TYPE_UNEARNED_CREDIT)) {
			    this.creditAmountTotal += discount.getDiscountAmount();
			} else {
			    this.discountAmountTotal += discount.getDiscountAmount();
			}

			doc.addLeaf(extrasNode, "extra", discount.getDiscountTypeVO().getDescription() + ": (Discount " + CurrencyUtil.formatDollarValue(discount.getDiscountAmount()) + ")");
			this.groupLineCount++;
		    }

		}
	    }
	} catch (Exception fe) {
	    // this is not necessarily an error
	    LogUtil.log(this.getClass(), "Unable to get transaction for group member " + fe.getMessage());
	}

	// doc.addLeaf(extrasNode,"extra","  ------");
	// this.groupLineCount++;
    }

    private void addDDData(Properties prop, DirectDebitSchedule ddSchedule, Node parent) throws ReportException {
	try {
	    Node ddNode = doc.addNode(parent, "directDebit");
	    String ddMessage = null;
	    DirectDebitAuthority ddAuthority = null;
	    ddMessage = CommonEJBHelper.getCommonMgr().getGnText("DD", "SCHED", "SCHEDROADSIDE");
	    Hashtable varList = new Hashtable();
	    varList.put("productName", prop.getProperty("prodName"));
	    ddMessage = StringUtil.replaceTag(ddMessage, "<", ">", varList);
	    doc.addLeaf(ddNode, "ddMessage", ddMessage);
	    doc.addLeaf(ddNode, "includeSchedule", "Yes");
	    ddAuthority = ddSchedule.getDirectDebitAuthority();
	    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

	    // get account details
	    int dayToDebit = ddAuthority.getDayToDebit();
	    DirectDebitFrequency frequency = ddAuthority.getDeductionFrequency();
	    String paymentFrequency = frequency.getDescription();
	    StringBuffer bankDetails1 = new StringBuffer();
	    StringBuffer bankDetails2 = new StringBuffer();
	    BankAccount bankAccount = ddAuthority.getBankAccount();
	    if (bankAccount instanceof CreditCardAccount) {
		CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
		bankDetails1.append(ccAccount.getCardTypeDescription());
		bankDetails1.append(" ");
		// bankDetails1.append(StringUtil.formatCreditCardNumber(ccAccount.getCardNumber()));
		bankDetails1.append(StringUtil.obscureFormattedCreditCardNumber(ccAccount.getCardNumber()));
		bankDetails2.append("Expires " + ccAccount.getCardExpiry());
		bankDetails2.append(" ");
		bankDetails2.append(ccAccount.getAccountName());
		// exp
		ccAccount.getCardExpiry();
	    } else if (bankAccount instanceof DebitAccount) {
		DebitAccount dAccount = (DebitAccount) bankAccount;
		String branchName = paymentMgr.validateBsbNumber(dAccount.getBsbNumber());
		bankDetails1.append(branchName);
		bankDetails2.append("BSB " + dAccount.getBsbNumber());
		bankDetails2.append(" ");
		bankDetails2.append("Account " + StringUtil.obscureBankAccountNumber(dAccount.getAccountNumber()));
		bankDetails2.append(" ");
		bankDetails2.append(dAccount.getAccountName());
	    }
	    doc.addLeaf(ddNode, "bankAccountDetails1", bankDetails1.toString());
	    doc.addLeaf(ddNode, "bankAccountDetails2", bankDetails2.toString());
	    doc.addLeaf(ddNode, "paymentFrequency", paymentFrequency);
	    Vector ddScheduleList = null;
	    try {
		ddScheduleList = paymentMgr.getDirectDebitScheduleListByDirectDebitAuthority(ddAuthority);
	    } catch (Exception ex) {
		throw new ReportException(ex);
	    }
	    // if there is a schedule
	    Node pNode = null;
	    int paymentCount = 0;
	    if (ddScheduleList != null && ddScheduleList.size() > 0) {
		// for(int x = ddScheduleList.size() - 1; x >= 0; x--)
		// {
		ddSchedule = (DirectDebitSchedule) ddScheduleList.lastElement(); // current
										 // schedule
		Collection payments = ddSchedule.getScheduleItemList();
		Iterator ddIt = payments.iterator();
		while (ddIt.hasNext()) {
		    DirectDebitScheduleItem item = (DirectDebitScheduleItem) ddIt.next();
		    if (item.isRenewal()) {
			paymentCount++;
			pNode = doc.addNode(ddNode, "payment");
			doc.addLeaf(pNode, "paymentDate", item.getScheduledDate().formatShortDate());
			doc.addLeaf(pNode, "feeAmount", CurrencyUtil.formatDollarValue(item.getFeeAmount()));
			doc.addLeaf(pNode, "amount", CurrencyUtil.formatDollarValue(item.getAmount()));
			doc.addLeaf(pNode, "column", paymentCount > 6 ? "2" : "1");
			doc.addLeaf(pNode, "status", item.getPrintableStatus());
		    }
		}
		// }
	    }

	} catch (RemoteException rex) {
	    throw new ReportException(rex);
	}
    } // end of addDDData

    public String getPrivacyAttachmentName() {
	return privacyAttachmentName;
    }

    public void setPrivacyAttachmentName(String privacyAttachmentName) {
	this.privacyAttachmentName = privacyAttachmentName;
    }

    public String getPrivacyAttachmentFile() {
	return privacyAttachmentFile;
    }

    public void setPrivacyAttachmentFile(String privacyAttachmentFile) {
	this.privacyAttachmentFile = privacyAttachmentFile;
    }

}
