package com.ract.membership.reporting;

import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemException;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.util.DateTime;

/**
 * Title: MembershipMovementReportRequest Description: Holds all of the specific
 * logic required to construct the report in the context of the reporting
 * infrastructure. Copyright: Copyright (c) 2003 Company: RACT
 * 
 * @author T. Bakker
 * @version 1.0
 */

public class MembershipMovementReportRequest extends ReportRequestBase {

    public static final String PARAMETER_FROM_DATE = "fromDate";

    public static final String PARAMETER_TO_DATE = "toDate";

    public static final String PARAMETER_SALES_BRANCH_LIST = "salesBranchList";

    public static final String PARAMETER_USERID = "userID";

    public static final String REPORT_NAME = "mem/MembershipMovementReport_5";

    public MembershipMovementReportRequest() {
	setReportNameAndDataSource();
    }

    public MembershipMovementReportRequest(String dataServerContext, Hashtable parameterList, ReportDeliveryFormat deliveryFormat) {
	setReportNameAndDataSource();

	this.setDataServerContext(dataServerContext);
	this.setDeliveryFormat(deliveryFormat);
	this.setParameterList(parameterList);

    }

    public void setReportNameAndDataSource() {
	super.setReportName(REPORT_NAME);
    }

    public void setDataServerContext(String dataServerContext) {
	super.setDataServerContext(dataServerContext);
    }

    public void setDeliveryFormat(ReportDeliveryFormat deliveryFormat) {
	super.setDeliveryFormat(deliveryFormat);
    }

    public void setParameterList(Hashtable parameterList) {
	super.setQueryParameters(parameterList);
    }

    public String getReportData() throws RemoteException {
	Hashtable parameterList = this.getQueryParameters();
	return getReportData(parameterList);
    }

    public String getReportData(Hashtable parameterList) throws RemoteException {
	// All parameters are passed as strings because they get pased through a
	// URL.
	String userID = (String) parameterList.get(PARAMETER_USERID);
	String repId = userID + (new DateTime()).getSecondsFromMidnight();
	DateTime fromDate = null;
	DateTime toDate = null;
	try {
	    fromDate = new DateTime((String) parameterList.get(PARAMETER_FROM_DATE));
	} catch (java.text.ParseException pe) {
	    throw new SystemException("Invalid 'fromDate' [" + parameterList.get(PARAMETER_FROM_DATE) + "].");
	}
	try {
	    toDate = new DateTime((String) parameterList.get(PARAMETER_TO_DATE));
	} catch (java.text.ParseException pe) {
	    throw new SystemException("Invalid 'toDate' [" + parameterList.get(PARAMETER_TO_DATE) + "].");
	}
	// get the comma delimited list of sales branches and turn it into an
	// array list
	// of sales branch codes
	String salesBranchList = (String) parameterList.get(PARAMETER_SALES_BRANCH_LIST);

	StringBuffer sql = new StringBuffer();

	sql.append("select mm.year_month as yearMonth, ");
	sql.append("mm.product_code as productCode, ");
	sql.append("mm.segment_code as segmentCode, ");
	// sql.append("dbo.fn_getBranchRegionDescription(mm.branch_number) as branchRegion, ");
	sql.append(" mm.branchRegion, ");
	sql.append("sum(mm.create_count) as createCount, ");
	sql.append("sum(mm.rejoin_count) as rejoinCount, ");
	sql.append("sum(mm.renew_count) as renewCount, ");
	sql.append("sum(mm.cancel_count) as cancelCount, ");
	sql.append("sum(mm.transfer_in_count) as transferInCount, ");
	sql.append("sum(mm.transfer_out_count) as transferOutCount, ");
	sql.append("sum(mm.lapsed_count) as lapsedCount, ");
	sql.append("sum(mm.unfinacial_count) as unfinancialCount, ");
	sql.append("sum(mm.active_count) as activeCount, ");
	sql.append("sum(mm.onhold_count) as onholdCount, ");
	// sql.append(" dbo.fn_getPreviousMonthActiveMembership(mm.year_month,branch_number,product_code,segment_code) as broughtForward, ");
	sql.append(" sum(previousMonth) as broughtForward, ");
	// sql.append(" (activeCount - broughtForward) as netIncrease, ");
	sql.append(" mm.month_char as monthChar ");
	sql.append(" from vw_dw_fact_membership_movement mm");
	sql.append(" where mm.fact_date >= '" + fromDate.toSQLDate().toString() + "'");
	sql.append(" and mm.fact_date <= '" + toDate.toSQLDate().toString() + "'");
	sql.append(" and mm.branch_number in (" + salesBranchList.toString() + ")");
	sql.append(" group by year_month, branchRegion, product_code, segment_code, month_char");
	// sql.append(" group by year_month, branchRegion, product_code, segment_code, month_char");
	String reportData = CommonEJBHelper.getReportMgr().getXMLResultSet(sql.toString(), CommonConstants.DATASOURCE_DW);

	/*
	 * System.out.println("\n\n" + sql.toString() + "\n\n" + reportData);
	 */
	// Return the report data.
	return reportData;
    }

    private void putParametersInProperyList(Properties p, Hashtable parameterList) {
	Enumeration keys = parameterList.keys();
	String parameterName = null;
	String parameterValue = null;
	while (keys.hasMoreElements()) {
	    parameterName = (String) keys.nextElement();
	    parameterValue = (String) parameterList.get(parameterName);
	    p.put(parameterName, parameterValue);
	}
    }

}
