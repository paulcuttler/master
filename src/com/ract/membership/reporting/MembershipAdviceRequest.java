package com.ract.membership.reporting;

import java.io.StringReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import com.ract.client.ClientVO;
import com.ract.client.PersonVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.PostalAddressVO;
import com.ract.common.PrintForm;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.membership.DiscountTypeVO;
import com.ract.membership.GroupVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipGroupDetailVO;
import com.ract.membership.MembershipGroupVO;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipProductOption;
import com.ract.membership.MembershipProfileVO;
import com.ract.membership.MembershipTransactionDiscount;
import com.ract.membership.MembershipTransactionFee;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitFrequency;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.directdebit.DirectDebitScheduleItem;
import com.ract.user.User;
import com.ract.util.CurrencyUtil;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.PrintUtil;
import com.ract.util.StringUtil;
import com.ract.util.XDocument;

/**
 * MembershipAdviceRequest handles the selection of the appropriate report template for the required advice and manages population of the XML data source document
 */
public class MembershipAdviceRequest extends XMLReportRequest {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant DOCUMENT_TYPE_SCHEDULE. */
	private static final String DOCUMENT_TYPE_SCHEDULE = "SCHED";

	/** The Constant BODY_PART_ACCESS_CREATE. */
	private static final String BODY_PART_ACCESS_CREATE = "CREATE_ACCESS_BODY";

	/** The Constant DOCUMENT_TYPE_ADVICE. */
	private static final String DOCUMENT_TYPE_ADVICE = "ADVICE";

	/** The advice name. */
	private String adviceName = null;

	/** The product name. */
	private String productName = null;

	/** The Constant ADVICE_ACCESS_MEMBERSHIP_ADVICE. */
	private final static String ADVICE_ACCESS_MEMBERSHIP_ADVICE = "mem/AccessMembershipAdvice";

	/** The Constant ADVICE_LIFESTYLE_MEMBERSHIP_ADVICE. */
	private final static String ADVICE_NEW_LIFESTYLE_MEMBERSHIP_ADVICE = "mem/NewLifestyleMembershipAdvice";

	/** The Constant ADVICE_LIFESTYLE_MEMBERSHIP_EMAILADVICE. */
	private final static String ADVICE_NEW_LIFESTYLE_MEMBERSHIP_EMAIL_ADVICE = "mem/NewLifestyleMembershipEmailAdvice";

	/** The Constant ADVICE_NEW_ROADSIDE_MEMBERSHIP_ADVICE. */
	private final static String ADVICE_NEW_ROADSIDE_MEMBERSHIP_ADVICE = "mem/NewRoadsideMembershipAdvice";

	/** The Constant ADVICE_NEW_ROADSIDE_MEMBERSHIP_EMAIL_ADVICE. */
//	private final static String ADVICE_NEW_ROADSIDE_MEMBERSHIP_EMAIL_ADVICE = "mem/NewRoadsideMembershipEmailAdvice";
	// Changes for print reduction initiative and new tone - APPS-296 GS
	private final static String ADVICE_MEMBERSHIP_TAX_INVOICE = "mem/MembershipTaxInvoice";

	/** The Constant ADVICE_OLD_MEMBERSHIP_ADVICE. */
	private final static String ADVICE_OLD_MEMBERSHIP_ADVICE = "mem/MembershipAdvice_6";

	/** The Constant ADVICE_NAME_ACCESS. */
	private final static String ADVICE_NAME_ACCESS = "CREATE_ACCESS";

	/** The Constant ADVICE_NAME_LIFESTYLE. */
	private final static String ADVICE_NAME_LIFESTYLE = "CREATE_LIFESTYLE";

	/** The Constant ROOT_FOLDER. */
	private final static String ROOT_FOLDER = "/Ract";

	/** The Constant CREATE_ROADSIDE. */
	private final static String CREATE_ROADSIDE = "CREATE_ROADSIDE";

	/** The Constant CREATE_LIFESTYLE. */
	private final static String CREATE_LIFESTYLE = "CREATE_LIFESTYLE";

	/** The Constant CHANGE_ROADSIDE. */
	private final static String CHANGE_ROADSIDE = "CHANGE_ROADSIDE";

	/** The Constant CREATE_NON_MOTORING. */
	private final static String CREATE_NON_MOTORING = "CREATE_NON_MOTORING";

	/** The Constant CHANGE_NON_MOTORING. */
	private final static String CHANGE_NON_MOTORING = "CHANGE_NON_MOTORING";

	/** The Constant CHANGEPROD. */
	private final static String CHANGEPROD = "CHANGEPROD";

	/** The Constant UPGRADE_ULTIMATE. */
	private final static String UPGRADE_ULTIMATE = "UPGRADE_ULTIMATE";

	/** The Constant CHANGE_CMO. */
	private final static String CHANGE_CMO = "CHANGE_CMO";

	/** The doc. */
	private XDocument doc = null;

	/** The discount amount total. */
	private double totalDiscount = 0, totalFee = 0, totalCredit = 0, totalAmount = 0, gstAmount = 0, creditAmountTotal = 0, discountAmountTotal = 0;

	/** The group line count. */
	private int groupLineCount = 0;

	/** The trans value object. */
	private MembershipTransactionVO transVo;

	/**
	 * Instantiates a new membership advice request.
	 * 
	 * @param user the user
	 * @param printGroup the print group
	 * @param memVo the member value object
	 * @param email indicates whether email request or not
	 * @throws ReportException the report exception
	 * @throws RemoteException
	 */
	public MembershipAdviceRequest(User user, String printGroup, MembershipVO memVo, boolean email) throws ReportException, RemoteException {
		LogUtil.log(this.getClass(), "MembershipAdviceRequest start");
		try {
			initialSetting(user.getUsername(), printGroup);
		} catch (RemoteException ex) {
			throw new ReportException(ex);
		}
		doc = new XDocument();
		reportXML = (Element) doc.addNode(doc, "membership");
		doc.addLeaf(reportXML, "userName", user.getUsername());
		doc.addLeaf(reportXML, "userId", user.getUserID());
		doc.addLeaf(reportXML, "email", email ? "true" : "false");
		addMembershipData(memVo, reportXML, email);
		String x = doc.toXMLString();
		System.out.println(doc.toXMLString());

		// Do this here now so we get the right form for new letters while transitioning to the new letterhead MEM-435 gzs 19/2/2016
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		PrintForm printForm = cMgr.getPrintForm(this.getFormName(), this.printerGroup);
		this.destinationFileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.MEMBERSHIP);
	}

	/**
	 * Initialise printing destination and determine advice type
	 * 
	 * @param username the username
	 * @param printGroup the print group
	 * @throws RemoteException the remote exception
	 */
	private void initialSetting(String username, String printGroup) throws RemoteException {
		this.printerGroup = printGroup;
		// this.setFormName(formName);
		this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
		this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
		// Do this later so we can differentiate the print form while transitioning to the new letterhead MEM-435 gzs 19/2/2016
		// CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		// PrintForm printForm = cMgr.getPrintForm(this.getFormName(), this.printerGroup);
		// this.destinationFileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.MEMBERSHIP);
		this.setReportName("MembershipAdvice");
		//
		// This may be changed later on in getProductMessage. This is where it
		// decides what type of advice it is
		// and gets the appropriate product message. It is a little more
		// complicated than if product = 'ACCESS'
		//
		this.setReportTemplate(ROOT_FOLDER, ADVICE_OLD_MEMBERSHIP_ADVICE);
	}

	/**
	 * Adds membership data to the XML Document
	 * 
	 * @param memVO the member value object
	 * @param parent the parent node
	 * @throws ReportException the report exception
	 */
	private void addMembershipData(MembershipVO memVO, Node parent, boolean email) throws ReportException {
		boolean isAddressee = false;
		try {
			if (memVO == null) {
				throw new ReportException("MembershipVO null in " + this.getClass().toString() + ".initialSetting");
			}

			this.productName = memVO.getProduct().getDescription();

			// MEM-165 cater for downgrades
			if (memVO.getNextProductCode() != null) {
				// get next product
			}

			isAddressee = memVO.isGroupMember() && memVO.isPrimeAddressee() || !memVO.isGroupMember();

			memVO.getJoinDate();

			// sets the fee variable
			PayableItemVO pi = this.setTransactionDetails(memVO);
			putMemberDetails(memVO, parent);

			this.addProductMessageAndHeading(memVO, this.transVo, parent);

			// MEM-434 differentiate between new membership and all other advices.
			// Over time all advices will be migrated to updated templates - gzs
			// 13/1/16
			this.setFormName("memplus"); // default
			
			// Ensure all ROADSIDE and LIFESTYLE advices use the new no-letter form
			// LIFESTYLE will actually not generate an attachment
			if (this.adviceName.equals(CREATE_ROADSIDE) || this.adviceName.equals(CHANGE_ROADSIDE) || this.adviceName.equals(CREATE_LIFESTYLE)) {
				this.setFormName("memPlusNew");
				if (email) {
					this.setReportTemplate(ROOT_FOLDER, ADVICE_MEMBERSHIP_TAX_INVOICE);
//				} else {
//					this.setReportTemplate(ROOT_FOLDER, ADVICE_NEW_ROADSIDE_MEMBERSHIP_ADVICE);
				}
//			} else if (this.adviceName.equals(CREATE_LIFESTYLE)) {
//				this.setFormName("memPlusNew");
//				if (email) {
//					this.setReportTemplate(ROOT_FOLDER, ADVICE_NEW_LIFESTYLE_MEMBERSHIP_EMAIL_ADVICE);
//				} else {
//					this.setReportTemplate(ROOT_FOLDER, ADVICE_NEW_LIFESTYLE_MEMBERSHIP_ADVICE);
//				}
			}
			
			boolean isDD = (memVO.getDirectDebitAuthorityID() != null && memVO.getDirectDebitAuthorityID().intValue() > 0);

			if (!this.adviceName.equals("CANCELMEM")) {
				// this.addMemberDetails(memVO,
				// this.transVo,
				// doc.addNode(parent,"memberDetails"));
				if (memVO.isGroupMember() && memVO.isPrimeAddressee()) {
					this.addGroupDetails(memVO, doc.addNode(parent, "groupDetails"));
					this.setReportRequired(true);
				}
				if (isDD && memVO.isPrimeAddressee()) {
					this.addDirectDebitDetails(memVO, doc.addNode(parent, "directDebit"));
					this.setReportRequired(true);
				}
			}

			this.addNodePair("FEE:", CurrencyUtil.formatDollarValue(this.totalFee), "feeDetail", parent);

			this.gstAmount = this.totalFee * 1 / 11;

			this.addNodePair("including GST of", CurrencyUtil.formatDollarValue(this.gstAmount), "feeDetail", parent);
			
			if (this.creditAmountTotal > 0.00d) {
				this.addNodePair("CREDIT:", CurrencyUtil.formatDollarValue(this.creditAmountTotal), "feeDetail", parent);

			}

			this.discountAmountTotal = this.totalDiscount - this.creditAmountTotal;
			if (this.discountAmountTotal > 0.00d)
				this.addNodePair("DISCOUNT:", CurrencyUtil.formatDollarValue(this.discountAmountTotal), "feeDetail", parent);

			if (!isDD) {
				if (pi != null && pi instanceof PayableItemVO) {
					this.addNodePair("AMOUNT PAID:", CurrencyUtil.formatDollarValue(pi.getAmountPayable() - pi.getAmountOutstanding()), "feeDetail", parent);
				}
			}

			if (isAddressee)
			{
				doc.addLeaf(parent, "amountPayable", CurrencyUtil.formatDollarValue(this.totalAmount));
			}

			doc.addLeaf(parent, "amount", CurrencyUtil.formatDollarValue(this.totalAmount));
			doc.addLeaf(parent, "gst", CurrencyUtil.formatDollarValue(this.gstAmount));
		} catch (Exception ee) {
			throw new ReportException(ee);
		}
}

	/**
	 * Adds membership details (address etc) to the XML Document
	 * 
	 * @param memVO the member value object
	 * @param parent the parent node
	 * @throws ReportException the report exception
	 */
	private void putMemberDetails(MembershipVO memVO, Node parent) throws ReportException {
		try {
			ClientVO client = memVO.getClient();
			if (client == null) {
				throw new ReportException("ClientVO null in printDirectDebitSchedule");
			}

			PostalAddressVO address = null;
			String postalName = null;
			MembershipGroupVO membershipGroup = memVO.getMembershipGroup();
			if (membershipGroup != null && membershipGroup.getGroup() != null) {
				GroupVO group = membershipGroup.getGroup();
				address = group.getBillingAddress();
				postalName = group.getGroupName();
				if (group.getContactPerson() != null && !group.getContactPerson().trim().equals("")) {
					postalName += FileUtil.NEW_LINE + "C/O " + group.getContactPerson();
				}
				postalName = postalName.toUpperCase();
			} else {
				address = client.getPostalAddress();
				postalName = client.getPostalName().toUpperCase();
			}

			if (address == null) {
				throw new ReportException("PostalAddressVO is null in printDirectDebitSchedule");
			}

			String clientTitle = null;
			if (client instanceof PersonVO) {
				clientTitle = ((PersonVO) client).getTitle();
			}
			if (clientTitle == null) {
				clientTitle = "";
			}
			doc.addLeaf(parent, "clientTitle", clientTitle);

			doc.addLeaf(parent, "clientNo", StringUtil.padNumber(memVO.getClientNumber(), 7));

			String[] addressArray = new String[3];

			addressArray[0] = address.getAddressLine1();
			if (addressArray[0] == null) {
				addressArray[0] = "";
			}

			addressArray[1] = address.getAddressLine2();
			if (addressArray[1] == null) {
				addressArray[1] = "";
			}

			addressArray[2] = address.getSuburb() + " " + address.getState().toUpperCase() + " " + address.getPostcode().toString();

			StringUtil.removeBlankLines(addressArray, 0);
			doc.addLeaf(parent, "address1", addressArray[0].toUpperCase());
			doc.addLeaf(parent, "address2", addressArray[1].toUpperCase());
			doc.addLeaf(parent, "address3", addressArray[2].toUpperCase());
			doc.addLeaf(parent, "postalName", postalName);
			doc.addLeaf(parent, "addressTitle", client.getAddressTitle());
			doc.addLeaf(parent, "firstName", client.getFirstNameOnly()); // for Lifestyle letter MEM-434
			//
			// add membershipNo
			//
			doc.addLeaf(parent, "membershipNo", memVO.getMembershipNumber());

			boolean accessMembership = ProductVO.PRODUCT_ACCESS.equals(memVO.getProductCode());
			boolean lifestyleMembership = ProductVO.PRODUCT_LIFESTYLE.equals(memVO.getProductCode());
			this.addRefData(parent, "MEMBER NO:", StringUtil.padNumber(client.getClientNumber(), 7));
			if (!accessMembership) {
				if (!lifestyleMembership) {
					this.addRefData(parent, "ROADSIDE COVER:", this.productName);
				}
				this.addRefData(parent, "PRICING OPTION:", getPricingOptionText(memVO));
				this.addRefData(parent, "JOIN DATE:", memVO.getJoinDate().formatShortDate());
			}
			if (!accessMembership || (!MembershipProfileVO.PROFILE_GIFTED_INSURANCE.equals(memVO.getMembershipProfileCode()) && !MembershipProfileVO.PROFILE_GIFTED_TRAVEL.equals(memVO.getMembershipProfileCode()))) {
				this.addRefData(parent, "EXPIRY DATE:", memVO.getExpiryDate().formatShortDate());
			}

		} catch (Exception ex) {
			throw new ReportException(ex);
		}
	}

	/**
	 * Gets the pricing option text.
	 * 
	 * @param memVO the member value object
	 * @return the pricing option text
	 * @throws RemoteException the remote exception
	 */
	private String getPricingOptionText(MembershipVO memVO) throws RemoteException {
		int groupSize = 1;

		if (memVO.isGroupMember()) {
			groupSize = memVO.getMembershipGroup().getMemberCount();
			if (groupSize > MembershipHelper.SEGMENT_COUNT) {
				groupSize = MembershipHelper.SEGMENT_COUNT;
			}
		}
		if (groupSize < 0) {
			groupSize = 1;
		}
		return MembershipHelper.SEGMENT_TEXT[groupSize - 1];
	}

	/**
	 * Sets the financial transaction details
	 * 
	 * @param memVO the member value object
	 * @throws Exception the exception
	 */
	private PayableItemVO setTransactionDetails(MembershipVO memVO) throws Exception {
		MembershipTransactionVO transVO = null;
		MembershipTransactionTypeVO transType = null;
		MembershipMgr mgr = null;
		PayableItemVO pi = null;

		if (memVO.getMembershipID() != null) {
			mgr = MembershipEJBHelper.getMembershipMgr();
			transVO = memVO.getLastTransaction();
			this.transVo = transVO;
			transType = transVO.getTransactionType();
			transType.getTransactionTypeCode();
			transType.getDescription();

			if (transVO.getTransactionGroupID() > 0) {
				Collection<?> transGroupList = mgr.findMembershipTransactionByTransactionGroupID(transVO.getTransactionGroupID());
				Iterator<?> it = transGroupList.iterator();
				MembershipTransactionVO gpTransVO = null;
				while (it.hasNext()) {
					gpTransVO = (MembershipTransactionVO) it.next();
					this.totalDiscount += gpTransVO.getTotalDiscount() + gpTransVO.getDiscretionaryDiscountAmount().doubleValue();
					this.totalFee += gpTransVO.getGrossTransactionFee();
					this.setTotalCredit(this.getTotalCredit() + gpTransVO.getCreditDiscountApplied());
					pi = mgr.getPayableItem(gpTransVO.getPayableItemID());
					if (pi != null && pi instanceof PayableItemVO) {
						this.totalAmount = pi.getAmountOutstanding();
					} else {
						this.totalAmount += gpTransVO.getAmountPayable();
						// discountReason = dDisc.getDiscountReason();
					}
				}
			} else {
				// processing for single memberships - not transaction group
				this.totalDiscount = transVO.getTotalDiscount() + transVO.getDiscretionaryDiscountAmount().doubleValue();
				this.totalFee = transVO.getGrossTransactionFee();
				this.setTotalCredit(transVO.getCreditDiscountApplied());
				this.totalAmount = transVO.getAmountPayable();
				pi = mgr.getPayableItem(transVO.getPayableItemID());
				if (pi != null && pi instanceof PayableItemVO) {
					this.totalAmount = this.totalAmount - (pi.getAmountPayable() - pi.getAmountOutstanding());
//				} else {
//					this.totalAmount += transVO.getAmountPayable();
				}
			}
		}

		return pi;
	}

	/**
	 * Adds the product message and heading.
	 * 
	 * ## Note the product message will be incorporated in the letter template from hereon - MEM-434 15/1/2016 ##
	 * 
	 * @param memVO the member value object
	 * @param transVO the trans value object
	 * @param parent the parent node
	 * @throws Exception the exception
	 */
	private void addProductMessageAndHeading(MembershipVO memVO, MembershipTransactionVO transVO, Node parent) throws Exception {
		String ddString = "";
		String productMessage = "";
		Hashtable<String, String> varList = new Hashtable<String, String>();

		// get details of the most recent transaction
		if (memVO.getDirectDebitAuthorityID() != null && memVO.getDirectDebitAuthorityID().intValue() > 0 && memVO.isPrimeAddressee()) {
			// check if there is any direct debit to pay
			if (memVO.getAmountOutstanding(false, true) > 0) {
				ddString = "A copy of your Direct Debit Schedule is attached";
				doc.addLeaf(parent, "footerMessage", "You are paying by direct debit.  Do not send any money.");
			} else {
				doc.addLeaf(parent, "footerMessage", "");
			}

		} else {
			doc.addLeaf(parent, "footerMessage", "");
//			doc.addLeaf(parent, "footerMessage", "This notice when accompanied by a receipt will be your tax invoice for GST");
		}

		varList.put("directDebit", ddString);
		varList.put("commenceDate", memVO.getJoinDate().formatShortDate());
		varList.put("expiryDate", memVO.getExpiryDate().formatShortDate());
		varList.put("productName", memVO.getProduct().getDescription());
		if (memVO.getProduct().isVehicleBased()) {
			varList.put("make", memVO.getMake());
			varList.put("model", memVO.getModel());
			varList.put("rego", memVO.getRego());
			varList.put("vin", memVO.getVin());
			varList.put("year", memVO.getYear());
		}

		StringBuffer headingString = new StringBuffer();
		productMessage = this.determineAdviceType(memVO, transVO, headingString);
		productMessage = StringUtil.replaceTag(productMessage, "<", ">", varList);
		doc.addLeaf(parent, "prodMessage", productMessage);

		if (ADVICE_NAME_ACCESS.equalsIgnoreCase(this.adviceName)) {
			doc.addLeaf(parent, "heading", "Access Membership Advice");

			// body text is in xml format to allow flexible formatting
			// merge nodes into main document
			String bodyText = CommonEJBHelper.getCommonMgr().getGnText(SourceSystem.MEMBERSHIP.getAbbreviation(), DOCUMENT_TYPE_ADVICE, BODY_PART_ACCESS_CREATE);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(bodyText)));
			Node node = document.getFirstChild();
			parent.appendChild(doc.importNode(node, true)); // must import!!!

			String profileCode = memVO.getMembershipProfileCode();
			String profileMessage = null;
			try {
				profileMessage = CommonEJBHelper.getCommonMgr().getGnText(SourceSystem.MEMBERSHIP.getAbbreviation(), DOCUMENT_TYPE_ADVICE, ADVICE_NAME_ACCESS + " " + profileCode);
			} catch (Exception e) {
				// ignore
				LogUtil.warn(this.getClass(), e.getMessage());
			}
			if (profileMessage != null) {
				doc.addLeaf(parent, "profileMessage", profileMessage);
			}
		}
	}

	/**
	 * Determines Advice type.
	 * 
	 * Getting product message from database *deprecated* from MEM-434 - will gradually be retired in favour of template based message for ease of maintenance. Renamed to better reflect advice type selection functionality - was getProductMessage
	 * 
	 * @param memVO MembershipVO
	 * @param transVO MembershipTransactionVO
	 * @param headingString the heading string
	 * @return String product message
	 * @throws RemoteException the remote exception
	 * @throws SystemException the system exception
	 */
	private String determineAdviceType(MembershipVO memVO, MembershipTransactionVO transVO, StringBuffer headingString) throws RemoteException, SystemException {
		String prodCode = memVO.getProductCode();
		String transType = transVO.getTransactionGroupTypeCode();
		int groupCount = 1;

		if (memVO.isGroupMember()) {
			groupCount = memVO.getMembershipGroup().getMemberCount();
		}

		try {
			if (transType == null) {
				// historical data - produce vanilla report
				this.adviceName = CHANGE_ROADSIDE;
			} else if (ProductVO.PRODUCT_NON_MOTORING.equalsIgnoreCase(prodCode)) {
				if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE) || transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP) || transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN)) {
					this.adviceName = CREATE_NON_MOTORING;
				} else {
					this.adviceName = CHANGE_NON_MOTORING;
				}
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT) && ProductVO.PRODUCT_ADVANTAGE.equalsIgnoreCase(memVO.getNextProductCode())) {
				this.adviceName = CHANGEPROD;
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT) && ProductVO.PRODUCT_NON_MOTORING.equalsIgnoreCase(memVO.getNextProductCode())) {
				this.adviceName = CHANGE_NON_MOTORING;
			} else if (ProductVO.PRODUCT_ACCESS.equalsIgnoreCase(prodCode)) {
				this.adviceName = ADVICE_NAME_ACCESS;
				this.setReportTemplate(ROOT_FOLDER, ADVICE_ACCESS_MEMBERSHIP_ADVICE);
			} else if (ProductVO.PRODUCT_LIFESTYLE.equalsIgnoreCase(prodCode)) {
				// New Lifestyle Advice
				this.adviceName = ADVICE_NAME_LIFESTYLE;
				this.setReportTemplate(ROOT_FOLDER, ADVICE_NEW_LIFESTYLE_MEMBERSHIP_ADVICE);
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE) || transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP) || transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN)) {
				MembershipVO pa = null;
				if (memVO.isGroupMember()) {
					pa = memVO.getMembershipGroup().getMembershipGroupDetailForPA().getMembership();
				} else {
					pa = memVO;
				}

				MembershipVO oldPa = pa.getPreviousMembership();
				if (oldPa == null) {
					this.adviceName = CREATE_ROADSIDE;
				} else {
					this.adviceName = CHANGE_ROADSIDE;
				}
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT) // upgrade
					&& ProductVO.PRODUCT_ULTIMATE.equalsIgnoreCase(prodCode)) {
				this.adviceName = UPGRADE_ULTIMATE;
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP)) {
				if (groupCount == 1) {
					this.adviceName = CHANGE_ROADSIDE;
				} else if (groupContainsNewMember(memVO)) {
					this.adviceName = CREATE_ROADSIDE;
				} else {
					this.adviceName = CHANGE_ROADSIDE;
				}
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP)) {
				this.adviceName = CHANGE_ROADSIDE;
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL) && memVO.getStatus().equals(MembershipVO.STATUS_CANCELLED)) {
				this.adviceName = "CANCELMEM";
			} else if (transType.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER)) {
				this.adviceName = CREATE_ROADSIDE;
			} else {
				this.adviceName = CHANGE_ROADSIDE;
			}

			if ((this.adviceName.equals(CHANGE_ROADSIDE) || this.adviceName.equals(CREATE_ROADSIDE)) && memVO.getProductCode().equals(ProductVO.PRODUCT_CMO)) {
				this.adviceName = CHANGE_CMO;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("A data error (" + e.getMessage() + ") has occurred for membership number " + memVO.getClientNumber() + "." + "\nUnable to print advice. \nPlease contact Service Desk", e);

		}
		return CommonEJBHelper.getCommonMgr().getGnText(SourceSystem.MEMBERSHIP.getAbbreviation(), DOCUMENT_TYPE_ADVICE, this.adviceName);
	}

	/**
	 * Determines whether a group contains a new member
	 * 
	 * @param memVO MembershipVO
	 * @return boolean
	 * @throws RemoteException the remote exception
	 */
	private boolean groupContainsNewMember(MembershipVO memVO) throws RemoteException {
		MembershipVO thisVo = null;
		MembershipGroupDetailVO grpDetail = null;
		boolean newMem = false;
		if (memVO.isGroupMember()) {
			MembershipGroupVO memGroup = memVO.getMembershipGroup();
			Collection<?> groupList = memGroup.getMembershipGroupDetailList();
			Iterator<?> it = groupList.iterator();
			while (it.hasNext()) {
				grpDetail = (MembershipGroupDetailVO) it.next();
				thisVo = grpDetail.getMembership();
				if (thisVo.getPreviousMembership() == null) {
					newMem = true;
				}
			}
			return newMem;
		} else {
			return memVO.getPreviousMembership() == null;
		}
	}

	/**
	 * Adds the group details to the XML Document
	 * 
	 * @param memVO the member value object
	 * @param parent the parent node
	 * @throws Exception the exception
	 */
	private void addGroupDetails(MembershipVO memVO, Node parent) throws Exception {
		if (memVO.isGroupMember() && memVO.isPrimeAddressee()) {
			String groupMessage = CommonEJBHelper.getCommonMgr().getGnText(SourceSystem.MEMBERSHIP.getAbbreviation(), DOCUMENT_TYPE_ADVICE, "GROUP_MESSAGE");
			Hashtable<String, String> tagList = new Hashtable<String, String>();
			tagList.put("productName", this.productName);
			groupMessage = StringUtil.replaceTag(groupMessage, "<", ">", tagList);
			doc.addLeaf(parent, "groupMessage", groupMessage);

			Collection<?> groupList = memVO.getMembershipGroup().getMembershipGroupDetailList();
			Iterator<?> it = groupList.iterator();
			MembershipGroupDetailVO groupMemDetailVo = null;
			doc.addLeaf(parent, "groupCount", groupList.size() + "");
			Node gmNode = null;
			while (it.hasNext()) {
				groupMemDetailVo = (MembershipGroupDetailVO) it.next();
				gmNode = doc.addNode(parent, "groupMember");
				assembleGroupMemberLine(groupMemDetailVo, this.transVo.getTransactionGroupID(), gmNode);
			}
			doc.addLeaf(parent, "groupLineCount", this.groupLineCount + "");
		}
	}

	/**
	 * Adds a key value air to a node.
	 * 
	 * @param title the title
	 * @param value the value
	 * @param nodeName the node name
	 * @param parent the parent node node
	 */
	private void addNodePair(String title, String value, String nodeName, Node parent) {
		Node thisNode = doc.addNode(parent, nodeName);
		doc.addLeaf(thisNode, "title", title);
		doc.addLeaf(thisNode, "value", value);
	}

	/**
	 * Assemble group member line.
	 * 
	 * @param groupMemVO the group member value object
	 * @param transGroupID the trans group id
	 * @param parent the parent node
	 * @throws Exception the exception
	 */
	private void assembleGroupMemberLine(MembershipGroupDetailVO groupMemVO, int transGroupID, Node parent) throws Exception {
		MembershipVO memVO = groupMemVO.getMembership();
		ClientVO client = memVO.getClient();
		boolean firstLineAdded = false;
		doc.addLeaf(parent, "clientNo", StringUtil.padNumber(client.getClientNumber(), 7));
		doc.addLeaf(parent, "postalName", client.getDisplayName());
		doc.addLeaf(parent, "joinDate", groupMemVO.getGroupJoinDate().formatShortDate());
		Node extrasNode = null;
		this.groupLineCount++;
		if (!memVO.getMembershipProfileCode().equals(MembershipProfileVO.PROFILE_CLIENT) && memVO.getMembershipProfileCode().trim().length() > 0) {
			extrasNode = doc.addNode(parent, "extras");
			doc.addLeaf(extrasNode, "extra", memVO.getMembershipProfileCode().trim());
			firstLineAdded = true;

		}
		if (memVO.hasProductOption(MembershipProductOption.SOCIAL_OPTION)) {
			if (!firstLineAdded) {
				firstLineAdded = true;
				extrasNode = doc.addNode(parent, "extras");
			} else
				this.groupLineCount++;
			doc.addLeaf(extrasNode, "extra", "Social Member");

		}

		try {
			MembershipMgr mgr = MembershipEJBHelper.getMembershipMgr();
			MembershipTransactionVO transVO = mgr.findMembershipTransactionForMemberAndGroup(memVO.getMembershipID().intValue(), transGroupID);
			MembershipTransactionDiscount discount = null;
			Collection<?> feeList = transVO.getTransactionFeeList();
			Iterator<?> feeListIterator = feeList.iterator();
			Iterator<?> discountIterator = null;
			MembershipTransactionFee fee = null;
			ArrayList<?> discountList = null;
			while (feeListIterator.hasNext()) {
				fee = (MembershipTransactionFee) feeListIterator.next();
				discountList = fee.getDiscountList();
				discountIterator = discountList.iterator();

				while (discountIterator.hasNext()) {
					discount = (MembershipTransactionDiscount) discountIterator.next();
					if (discount.getDiscountAmount() == 0) {
						continue;
					}
					if (discount.getDiscountCode().equals(DiscountTypeVO.TYPE_UNEARNED_CREDIT)) {
						this.creditAmountTotal += discount.getDiscountAmount();
					} else {
						this.discountAmountTotal += discount.getDiscountAmount();
					}
					if (!firstLineAdded) {
						extrasNode = doc.addNode(parent, "extras");

					} else
						this.groupLineCount++;
					doc.addLeaf(extrasNode, "extra", discount.getDiscountTypeVO().getDescription() + ": (Discount " + CurrencyUtil.formatDollarValue(discount.getDiscountAmount()) + ")");
					this.groupLineCount++;

				}
			}
		} catch (Exception fe) { // this is not necessarily an error
			LogUtil.log(this.getClass(), "Unable to get transaction for group member " + fe.getMessage());
		}
	}

	/**
	 * Adds the direct debit details.
	 * 
	 * @param memVO the member value object
	 * @param parent the parent node
	 * @throws RemoteException the remote exception
	 */
	private void addDirectDebitDetails(MembershipVO memVO, Node parent) throws RemoteException {

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		DirectDebitSchedule ddSchedule = null;

		String ddMessage = CommonEJBHelper.getCommonMgr().getGnText(SourceSystem.DD.getAbbreviation(), DOCUMENT_TYPE_SCHEDULE, "SCHEDROADSIDE");
		Hashtable<String, String> varList = new Hashtable<String, String>();
		varList.put("productName", memVO.getProduct().getDescription());
		ddMessage = StringUtil.replaceTag(ddMessage, "<", ">", varList);
		doc.addLeaf(parent, "ddMessage", ddMessage);
		doc.addLeaf(parent, "includeSchedule", "Yes");
		// get direct debit information if there is any.
		StringBuffer directDebit = new StringBuffer();
		directDebit.append("");

		DirectDebitAuthority ddAuthority = null;
		try {
			ddAuthority = paymentMgr.getDirectDebitAuthority(memVO.getDirectDebitAuthorityID());
		} catch (PaymentException ex2) {
			throw new RemoteException("Unable to get direct debit authority.", ex2);
		}
		ddAuthority.getDayToDebit();
		DirectDebitFrequency frequency = ddAuthority.getDeductionFrequency();
		String paymentFrequency = frequency.getDescription();
		StringBuffer bankDetails1 = new StringBuffer();
		StringBuffer bankDetails2 = new StringBuffer();
		BankAccount bankAccount = ddAuthority.getBankAccount();
		if (bankAccount instanceof CreditCardAccount) {
			CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
			bankDetails1.append(ccAccount.getCardTypeDescription());
			bankDetails1.append(" ");
			bankDetails1.append(StringUtil.obscureFormattedCreditCardNumber(ccAccount.getCardNumber()));
			bankDetails2.append("Expires " + ccAccount.getCardExpiry());
			bankDetails2.append(" ");
			bankDetails2.append(ccAccount.getAccountName());
			// exp
			ccAccount.getCardExpiry();
		} else if (bankAccount instanceof DebitAccount) {
			DebitAccount dAccount = (DebitAccount) bankAccount;
			String branchName = paymentMgr.validateBsbNumber(dAccount.getBsbNumber());
			bankDetails1.append(branchName);
			bankDetails2.append("BSB " + dAccount.getBsbNumber());
			bankDetails2.append(" ");
			bankDetails2.append("Account " + StringUtil.obscureBankAccountNumber(dAccount.getAccountNumber()));
			bankDetails2.append(" ");
			bankDetails2.append(dAccount.getAccountName());
		}
		doc.addLeaf(parent, "bankAccountDetails1", bankDetails1.toString());
		doc.addLeaf(parent, "bankAccountDetails2", bankDetails2.toString());
		doc.addLeaf(parent, "paymentFrequency", paymentFrequency);

		// find the schedule(s)
		Vector<?> ddScheduleList = null;
		try {
			ddScheduleList = paymentMgr.getDirectDebitScheduleListByDirectDebitAuthority(ddAuthority);
		} catch (PaymentException ex3) {
			throw new RemoteException("Unable to get schedule list.", ex3);
		}
		// if there is a schedule
		Node pNode = null;
		int paymentCount = 0;
		if (ddScheduleList != null && ddScheduleList.size() > 0) {
			for (int x = 0; x < ddScheduleList.size(); x++) {
				ddSchedule = (DirectDebitSchedule) ddScheduleList.elementAt(x);
				Collection<?> payments = ddSchedule.getScheduleItemList();
				Iterator<?> ddIt = payments.iterator();
				while (ddIt.hasNext()) {
					DirectDebitScheduleItem item = (DirectDebitScheduleItem) ddIt.next();
					if (item.isScheduled()) {
						pNode = doc.addNode(parent, "payment");
						paymentCount++;
						doc.addLeaf(pNode, "paymentDate", item.getScheduledDate().formatShortDate());
						doc.addLeaf(pNode, "amount", CurrencyUtil.formatDollarValue(item.getAmount()));
						doc.addLeaf(pNode, "feeAmount", CurrencyUtil.formatDollarValue(item.getFeeAmount()));
						doc.addLeaf(pNode, "column", paymentCount > 6 ? "2" : "1");
						doc.addLeaf(pNode, "status", item.getPrintableStatus());
					}
				}
			}
		}
	}

	/**
	 * Adds reference data values to the node.
	 * 
	 * @param parent the parent node
	 * @param title the title
	 * @param value the value
	 */
	private void addRefData(Node parent, String title, String value) {
		Node thisNode = doc.addNode(parent, "refData");
		doc.addLeaf(thisNode, "title", title);
		doc.addLeaf(thisNode, "value", value);
	}

	/**
	 * Gets the total credit.
	 * 
	 * @return the total credit
	 */
	public double getTotalCredit() {
		return totalCredit;
	}

	/**
	 * Sets the total credit.
	 * 
	 * @param totalCredit the new total credit
	 */
	public void setTotalCredit(double totalCredit) {
		this.totalCredit = totalCredit;
	}

}