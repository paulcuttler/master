package com.ract.membership;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.DataSourceFactory;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SystemException;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;

/**
 * @todo Description of the Class
 * 
 * @author hollidayj
 * @created 1 August 2002
 */
@Stateless
@Remote({ MembershipRefMgr.class })
@Local({ MembershipRefMgrLocal.class })
public class MembershipRefMgrBean {

    @PersistenceContext(unitName = "master")
    EntityManager em;

    private DataSource dataSource;

    /**
     * Return the specified AdjustmentTyeVO.
     */
    public AdjustmentTypeVO getAdjustmentTypeVO(String adjTypeCode) throws RemoteException {
	AdjustmentTypeVO adjTypeVO = em.find(AdjustmentTypeVO.class, adjTypeCode);
	return adjTypeVO;
    }

    /**
     * Return a list of AdjustmentTypeVO contain all of the adjustment types.
     */
    public Collection getAdjustmentTypeList() throws RemoteException {
	Query adjTypeQuery = em.createQuery("select e from AdjustmentTypeVO e");
	return adjTypeQuery.getResultList();
    }

    /**
     * Return the list of fees, as FeeSpecificationVO, that apply for the
     * product when doing the specified transaction type on the specified
     * membership type with the specified number of memberships in the group and
     * as at the effective date.
     * 
     * @param productCode
     *            Description of the Parameter
     * @param memTypeCode
     *            Description of the Parameter
     * @param transTypeCode
     *            Description of the Parameter
     * @param numberInGroup
     *            Description of the Parameter
     * @param effectiveDate
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection findProductFee(String productCode, String memTypeCode, String transTypeCode, int numberInGroup, DateTime effectiveDate) throws RemoteException {

	// System.out.println("findProductFee() productCode = " + productCode);
	// System.out.println("findProductFee() memTypeCode = " + memTypeCode);
	// System.out.println("findProductFee() transTypeCode = " +
	// transTypeCode);
	// System.out.println("findProductFee() numberInGroup = " +
	// numberInGroup);
	// System.out.println("findProductFee() effectiveDate = " +
	// effectiveDate);
	return FeeSpecificationCache.getInstance().getFee(productCode, null, memTypeCode, transTypeCode, numberInGroup, effectiveDate);
    }

    /**
     * Return the list of fees that apply for the product when doing the
     * specified transaction type on the specified membership type with the
     * specified number of memberships in the group and as at the effective
     * date.
     * 
     * @param productBenefitCode
     *            Description of the Parameter
     * @param memTypeCode
     *            Description of the Parameter
     * @param transTypeCode
     *            Description of the Parameter
     * @param numberInGroup
     *            Description of the Parameter
     * @param effectiveDate
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection findProductBenefitFee(String productBenefitCode, String memTypeCode, String transTypeCode, int numberInGroup, DateTime effectiveDate) throws RemoteException {
	return FeeSpecificationCache.getInstance().getFee(null, productBenefitCode, memTypeCode, transTypeCode, numberInGroup, effectiveDate);
    }

    /**
     * Return the list of client type codes that are allowed for a membership
     * type.
     * 
     * @param membershipTypeCode
     *            Description of the Parameter
     * @return The clientTypeListByMembershipType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getClientTypeListByMembershipType(String membershipTypeCode) throws RemoteException {
	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	ArrayList clientTypeList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT client_type_code FROM PUB.mem_allowed_client_type WHERE membership_type_code = ?";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, membershipTypeCode);
	    ResultSet clientTypeListRS = statement.executeQuery();

	    while (clientTypeListRS.next()) {
		String clientTypeCode = clientTypeListRS.getString(1);
		if (clientTypeCode != null) {
		    clientTypeList.add(clientTypeCode);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return clientTypeList;
    }

    /**
     * Return the specified membership type from the cache.
     * 
     * @param membershipTypeCode
     *            Description of the Parameter
     * @return The membershipType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTypeVO getMembershipType(String membershipTypeCode) throws RemoteException {
	return MembershipTypeCache.getInstance().getMembershipType(membershipTypeCode);
    }

    /**
     * Return a list of all membership types from the cache.
     * 
     * @return The membershipTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getMembershipTypeList() throws RemoteException {
	return MembershipTypeCache.getInstance().getMembershipTypeList();
    }

    public void initialiseMembershipTypeList() throws RemoteException {
	MembershipTypeCache.getInstance().initialiseList();
    }

    /**
     * Return the fee type value object for the specified fee type code. Returns
     * null if the fee type could not be found.
     * 
     * @param feeTypeCode
     *            Description of the Parameter
     * @return The feeType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public FeeTypeVO getFeeType(String feeTypeCode) throws RemoteException {
	return FeeTypeCache.getInstance().getFeeType(feeTypeCode);
    }

    /**
     * Return a list of all fee type value objects. Returns a null or empty list
     * if there are no fee types.
     * 
     * @return The feeTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getFeeTypeList() throws RemoteException {
	return FeeTypeCache.getInstance().getFeeTypeList();
    }

    public Collection getDiscountFeeTypeList(String discountCode) throws RemoteException {
	if (discountCode == null) {
	    return null;
	}

	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	FeeTypeVO feeTypeVO = null;
	ArrayList feeTypeList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT fee_type_code FROM PUB.mem_allowed_fee_discount WHERE discount_type_code = ?";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, discountCode);
	    ResultSet feeTypeListRS = statement.executeQuery();

	    String feeTypeCode = null;
	    while (feeTypeListRS.next()) {
		feeTypeCode = feeTypeListRS.getString(1);

		feeTypeVO = getFeeType(feeTypeCode);
		if (feeTypeVO != null) {
		    feeTypeList.add(feeTypeVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return feeTypeList;
    }

    public void initialiseFeeTypeList() throws RemoteException {
	FeeTypeCache.getInstance().initialiseList();
    }

    /**
     * Return the specified fee specification from the cache.
     * 
     * @param feeSpecificationId
     *            Description of the Parameter
     * @return The feeSpecification value
     * @exception RemoteException
     *                Description of the Exception
     */
    public FeeSpecificationVO getFeeSpecification(Integer feeSpecificationId) throws RemoteException {
	return FeeSpecificationCache.getInstance().getFeeSpecification(feeSpecificationId);
    }

    /**
     * Return a list of all membership types from the cache.
     * 
     * @return The feeSpecificationList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getFeeSpecificationList() throws RemoteException {
	return FeeSpecificationCache.getInstance().getFeeSpecificationList();
    }

    public void initialiseFeeSpecificationList() throws RemoteException {
	FeeSpecificationCache.getInstance().initialiseList();
    }

    /**
     * Return the list of discounts, as DiscountTypeVO that are allowed for a
     * fee type.
     * 
     * @param feeTypeCode
     *            Description of the Parameter
     * @return The feeTypeDiscountList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getFeeTypeDiscountList(String feeTypeCode) throws RemoteException {
	if (feeTypeCode == null) {
	    throw new RemoteException("The fee type code cannot be null.");
	}

	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	DiscountTypeVO discTypeVO = null;
	ArrayList discountList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT discount_type_code FROM PUB.mem_allowed_fee_discount WHERE fee_type_code = ?";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, feeTypeCode);
	    ResultSet discountListRS = statement.executeQuery();

	    String discTypeCode = null;
	    while (discountListRS.next()) {
		discTypeCode = discountListRS.getString(1);
		// Get the discount type from the cache
		discTypeVO = getDiscountType(discTypeCode);
		if (discTypeVO != null) {
		    discountList.add(discTypeVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return discountList;
    }

    /**
     * Return a copy of the list of discounts, as DiscountTypeVO, that can be
     * applied to the fee at the given date. Initialise the list if this is the
     * first call.
     * 
     * @param feeTypeCode
     *            Description of the Parameter
     * @param effectivedate
     *            Description of the Parameter
     * @return The allowableDiscountList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getAllowableDiscountList(String feeTypeCode, DateTime effectivedate) throws RemoteException {
	// initialise the return list
	ArrayList discountList = new ArrayList();

	// get the fee type for this code
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	FeeTypeVO feeType = membershipMgr.getFeeType(feeTypeCode);
	// get the discount list from the fee type
	Collection allowableDiscountList = feeType.getAllowableDiscountList();
	if (allowableDiscountList != null && !allowableDiscountList.isEmpty()) {
	    Iterator allowableDiscountListIterator = allowableDiscountList.iterator();
	    DateTime today = new DateTime();

	    // filter the list to current active discounts
	    while (allowableDiscountListIterator.hasNext()) {
		DiscountTypeVO discountType = (DiscountTypeVO) allowableDiscountListIterator.next();
		if (DiscountTypeVO.STATUS_Active.equals(discountType.getStatus()) && (discountType.getEffectiveFromDate() == null || discountType.getEffectiveFromDate().onOrBeforeDay(today)) && (discountType.getEffectiveToDate() == null || discountType.getEffectiveToDate().onOrAfterDay(today))) {
		    discountList.add(discountType);
		}
	    }

	}
	return discountList;
    }

    /**
     * Return a list of MembershipType that are allowed to be issued to the
     * specified client type. Currently implemented as a lookup on the
     * membership types in database. Could also scan the cached membership types
     * for those that have the client type code in their list of allowed client
     * type codes.
     * 
     * @param clientTypeCode
     *            Description of the Parameter
     * @return The membershipTypeListByClientType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getMembershipTypeListByClientType(String clientTypeCode) throws RemoteException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	return membershipMgr.getMembershipTypeByClientType(clientTypeCode);
    }

    /**
     * Return the specified product type.
     * 
     * @param productCode
     *            Description of the Parameter
     * @return The product value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ProductVO getProduct(String productCode) throws RemoteException {
	return ProductCache.getInstance().getProduct(productCode);
    }

    /**
     * Return a list of all product types.
     * 
     * @return The productList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getProductList() throws RemoteException {
	return ProductCache.getInstance().getProductList();
    }

    public void initialiseProductList() throws RemoteException {
	ProductCache.getInstance().initialiseList();
    }

    /**
     * Return a list of all product types with status = parameter.
     * 
     * @param productStatus
     *            Description of the Parameter
     * @return The productListByStatus value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getProductListByStatus(String productStatus) throws RemoteException {
	ArrayList productListByStatus = new ArrayList();
	Collection fullProductList = this.getProductList();
	ProductVO productVO = null;

	if (fullProductList != null && !fullProductList.isEmpty()) {
	    Iterator fullProductListIT = fullProductList.iterator();
	    while (fullProductListIT.hasNext()) {
		productVO = (ProductVO) fullProductListIT.next();
		if (productVO.getStatus().equals(productStatus)) {
		    productListByStatus.add(productVO);
		}

	    }
	}

	return productListByStatus;
    }

    /**
     * Return the list of products that are allowed for a membership type.
     * 
     * @param membershipTypeCode
     *            Description of the Parameter
     * @return The productListByMembershipType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getProductListByMembershipType(String membershipTypeCode) throws RemoteException {
	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	ArrayList productList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT product_code FROM PUB.mem_allowed_product_type WHERE membership_type_code = ?";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, membershipTypeCode);
	    ResultSet productListRS = statement.executeQuery();

	    // Fetch the product benefit value object from the cache for each
	    // product benefit code returned.
	    while (productListRS.next()) {
		String productCode = productListRS.getString(1);
		ProductVO prodVO = getProduct(productCode);
		if (prodVO != null) {
		    productList.add(prodVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return productList;
    }

    /**
     * Return the list of Membership Types that are allowed for a Product.
     * 
     * @param productCode
     *            Description of the Parameter
     * @return The MembershipType By Product List value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getMembershipTypeByProductList(String productCode) throws RemoteException {
	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	ArrayList memTypeList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT membership_type_code FROM PUB.mem_allowed_product_type WHERE product_code = ?";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, productCode);
	    ResultSet productListRS = statement.executeQuery();

	    // Fetch the membership type value object from the cache for each
	    // product benefit code returned.
	    while (productListRS.next()) {
		String memType = productListRS.getString(1);
		MembershipTypeVO memTypeVO = getMembershipType(memType);
		if (memTypeVO != null) {
		    memTypeList.add(memTypeVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return memTypeList;
    }

    /**
     * Return the specified product benefit.
     * 
     * @param productBenefitCode
     *            Description of the Parameter
     * @return The productBenefitType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ProductBenefitTypeVO getProductBenefitType(String productBenefitCode) throws RemoteException {
	return ProductBenefitCache.getInstance().getProductBenefitType(productBenefitCode);
    }

    /**
     * Return a list of all product benefit types.
     * 
     * @return The productBenefitTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getProductBenefitTypeList() throws RemoteException {
	return ProductBenefitCache.getInstance().getProductBenefitTypeList();
    }

    public void initialiseProductBenefitTypeList() throws RemoteException {
	ProductBenefitCache.getInstance().initialiseList();
    }

    /**
     * Return the list of product benefits associated with a product.
     * 
     * @param productCode
     *            Description of the Parameter
     * @return The productBenefitList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getProductBenefitList(String productCode) throws RemoteException {
	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	ArrayList benefitList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT product_benefit_code FROM PUB.mem_product_benefit WHERE product_code = ? ORDER BY sort_order ASC";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, productCode);
	    ResultSet benefitListRS = statement.executeQuery();

	    // Fetch the product benefit value object from the cache for each
	    // product benefit code returned.
	    while (benefitListRS.next()) {
		String benefitCode = benefitListRS.getString(1);
		ProductBenefitTypeVO pbtVO = getProductBenefitType(benefitCode);
		if (pbtVO != null) {
		    benefitList.add(pbtVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return benefitList;
    }

    /**
     * Return the list of product benefits associated with a product benefit
     * type.
     * 
     * @param productBenefitCode
     *            Description of the Parameter
     * @return The productBenefitList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getProductBenefitListByBenefitType(String productBenefitCode) throws RemoteException {
	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	ArrayList benefitList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT product_code FROM PUB.mem_product_benefit WHERE product_benefit_code = ? ORDER BY sort_order ASC";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, productBenefitCode);
	    ResultSet benefitListRS = statement.executeQuery();

	    // Fetch the product value object from the cache for each
	    // product code returned.
	    while (benefitListRS.next()) {
		String productCode = benefitListRS.getString(1);
		ProductVO prodVO = getProduct(productCode);
		if (prodVO != null) {
		    benefitList.add(prodVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return benefitList;
    }

    /**
     * Return a membership account value object for the specified account ID.
     * Return null if the account could not be found.
     * 
     * @param accountID
     *            Description of the Parameter
     * @return The membershipAccount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getMembershipAccount(Integer accountID) throws RemoteException {
	return MembershipAccountCache.getInstance().getMembershipAccount(accountID);

	// MembershipAccountVO memAccountVO = null;
	// MembershipAccountHome memAccountHome =
	// MembershipEJBHelper.getMembershipAccountHome();
	// try
	// {
	// MembershipAccount memAccount =
	// memAccountHome.findByPrimaryKey(accountID);
	// if (memAccount != null)
	// {
	// memAccountVO = memAccount.getVO();
	// }
	// }
	// catch (FinderException fe)
	// {
	// throw new RemoteException(fe.getMessage(), fe);
	// }
	// return memAccountVO;
    }

    /**
     * Return the membership account tagged as the clearing account. Returns the
     * first account in the list that has been tagged as the clearing account.
     * There should only be one. Return null if no clearing account has been
     * tagged.
     * 
     * 4/11/02: It has been now decided that all journals initially go to the
     * cash clearing account (1340) regardless of payment method. The Direct
     * Debit System moves it from the cash clearing account (1340) to the DD
     * clearing account (1147).
     * 
     * @return The membership account tagged as the clearing account.
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getMembershipClearingAccount() throws RemoteException {
	MembershipAccountVO memAccountVO = null;
	Collection accountList = MembershipAccountCache.getInstance().getMembershipAccountList();
	if (accountList != null) {
	    MembershipAccountVO tmpAccount;
	    Iterator accountListIterator = accountList.iterator();
	    while (accountListIterator.hasNext() && memAccountVO == null) {
		tmpAccount = (MembershipAccountVO) accountListIterator.next();
		if (tmpAccount.isClearingAccount()) {
		    memAccountVO = tmpAccount;
		}
	    }
	}
	if (memAccountVO == null) {
	    throw new SystemException("Membership clearing account not found!");
	}

	return memAccountVO;
    }

    /**
     * Return the membership account tagged as the GST account. Returns the
     * first account in the list that has been tagged as the GST account. There
     * should only be one. Return null if no GST account has been tagged.
     * 
     * @return The membership account tagged as the GST account.
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getMembershipGSTAccount() throws RemoteException {
	MembershipAccountVO memAccountVO = null;
	Collection accountList = MembershipAccountCache.getInstance().getMembershipAccountList();
	if (accountList != null) {
	    MembershipAccountVO tmpAccount;
	    Iterator accountListIterator = accountList.iterator();
	    while (accountListIterator.hasNext() && memAccountVO == null) {
		tmpAccount = (MembershipAccountVO) accountListIterator.next();
		if (tmpAccount.isGstAccount()) {
		    memAccountVO = tmpAccount;
		}
	    }
	}
	return memAccountVO;
    }

    /**
     * Generic account getter
     * 
     * @param title
     *            The key to find the required account.
     * @return MembershipAccountVO
     * @throws RemoteException
     */
    public MembershipAccountVO getMembershipAccountByTitle(String title) throws RemoteException {
	MembershipAccountVO memAccountVO = null;
	Collection accountList = MembershipAccountCache.getInstance().getMembershipAccountList();
	if (accountList != null) {
	    MembershipAccountVO tmpAccount;
	    Iterator accountListIterator = accountList.iterator();
	    while (accountListIterator.hasNext() && memAccountVO == null) {
		tmpAccount = (MembershipAccountVO) accountListIterator.next();
		if (tmpAccount.getAccountTitle().trim().equals(title)) {
		    memAccountVO = tmpAccount;
		    break;
		}
	    }
	}
	return memAccountVO;
    }

    /**
     * Return the membership account tagged as the suspense account. Returns the
     * first account in the list that has been tagged as the suspense account.
     * There should only be one. Return null if no suspense account has been
     * tagged.
     * 
     * @return The membership account tagged as the suspense account.
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getMembershipSuspenseAccount() throws RemoteException {
	MembershipAccountVO memAccountVO = null;
	Collection accountList = MembershipAccountCache.getInstance().getMembershipAccountList();
	if (accountList != null) {
	    MembershipAccountVO tmpAccount;
	    Iterator accountListIterator = accountList.iterator();
	    while (accountListIterator.hasNext() && memAccountVO == null) {
		tmpAccount = (MembershipAccountVO) accountListIterator.next();
		if (tmpAccount.isSuspenseAccount()) {
		    memAccountVO = tmpAccount;
		}
	    }
	}
	return memAccountVO;
    }

    /**
     * get account by account number
     * 
     * @return
     * @throws RemoteException
     */
    public MembershipAccountVO getMembershipAccount(String accountNumber) throws RemoteException {
	MembershipAccountVO memAccountVO = null;
	Collection accountList = MembershipAccountCache.getInstance().getMembershipAccountList();
	if (accountList != null) {
	    MembershipAccountVO tmpAccount;
	    Iterator accountListIterator = accountList.iterator();
	    while (accountListIterator.hasNext() && memAccountVO == null) {
		tmpAccount = (MembershipAccountVO) accountListIterator.next();
		if (tmpAccount.getAccountNumber().equals(accountNumber)) {
		    memAccountVO = tmpAccount;
		}
	    }
	}
	return memAccountVO;
    }

    /**
     * Gets the membershipProfile attribute of the MembershipRefMgrBean object
     * 
     * @param profileCode
     *            Description of the Parameter
     * @return The membershipProfile value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipProfileVO getMembershipProfile(String profileCode) throws RemoteException {
	return MembershipProfileCache.getInstance().getMembershipProfile(profileCode);
    }

    /**
     * Return the default membership profile
     */
    public MembershipProfileVO getDefaultMembershipProfile() throws RemoteException {
	return MembershipProfileCache.getInstance().getDefaultMembershipProfile();
    }

    /**
     * Gets the membershipProfileList attribute of the MembershipRefMgrBean
     * object
     * 
     * @return The membershipProfileList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getMembershipProfileList() throws RemoteException {
	return MembershipProfileCache.getInstance().getMembershipProfileList();
    }

    public void initialiseMembershipProfileList() throws RemoteException {
	MembershipProfileCache.getInstance().initialiseList();
    }

    /**
     * Return the specified membership transaction type.
     * 
     * @param transactionTypeCode
     *            Description of the Parameter
     * @return The membershipTransactionType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTransactionTypeVO getMembershipTransactionType(String transactionTypeCode) throws RemoteException {
	return MembershipTransactionTypeCache.getInstance().getTransactionType(transactionTypeCode);
    }

    /**
     * Return a list of all membership transaction types.
     * 
     * @return The membershipTransactionTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getMembershipTransactionTypeList() throws RemoteException {
	return MembershipTransactionTypeCache.getInstance().getTransactionTypeList();
    }

    public void initialiseMembershipTransactionTypeList() throws RemoteException {
	MembershipTransactionTypeCache.getInstance().initialiseList();
    }

    /**
     * Return the specified discount type.
     * 
     * @param discountTypeCode
     *            Description of the Parameter
     * @return The discountType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public DiscountTypeVO getDiscountType(String discountTypeCode) throws RemoteException {
	return DiscountTypeCache.getInstance().getDiscountType(discountTypeCode);
    }

    /**
     * Return a list of all discount types.
     * 
     * @return The discountTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getDiscountTypeList() throws RemoteException {
	return DiscountTypeCache.getInstance().getDiscountTypeList();
    }

    /**
     * Return a list of all the active discount types that are in the effective
     * date range.
     * 
     * @return The discountTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */

    public Collection getActiveDiscountTypeList() throws RemoteException {
	ArrayList myList = DiscountTypeCache.getInstance().getDiscountTypeList();
	;
	Iterator it = myList.listIterator();

	while (it.hasNext()) {
	    if (((DiscountTypeVO) (it.next())).isNotInEffect()) {
		it.remove();
	    }
	}

	return myList;

    }

    public void initialiseDiscountTypeList() throws RemoteException {
	DiscountTypeCache.getInstance().initialiseList();
    }

    /**
     * Return the list of discount types associated with a fee. DOESN'T WORK
     * 
     * @param feeSpecificationID
     *            Description of the Parameter
     * @return The discountTypeListByFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getDiscountTypeListByFee(Integer feeSpecificationID) throws RemoteException {
	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	ArrayList discountList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT discount_type_code FROM PUB.mem_allowed_fee_discount WHERE fee_specification_id = ?";

	    statement = connection.prepareStatement(statementText);
	    statement.setInt(1, feeSpecificationID.intValue());
	    ResultSet discountListRS = statement.executeQuery();

	    // Fetch the product benefit value object from the cache for each
	    // product benefit code returned.
	    while (discountListRS.next()) {
		String discountCode = discountListRS.getString(1);
		DiscountTypeVO discTypeVO = getDiscountType(discountCode);
		if (discTypeVO != null) {
		    discountList.add(discTypeVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return discountList;
    }

    /**
     * Return the list of discount types associated with a fee.
     * 
     * @param feeSpecificationID
     *            Description of the Parameter
     * @return The discountTypeListByFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getDiscountTypeListByFee(String feeTypeCode) throws RemoteException {
	DataSource dataSource = null;
	Connection connection = null;
	PreparedStatement statement = null;
	String statementText = null;
	ArrayList discountList = new ArrayList();
	try {
	    try {
		dataSource = (DataSource) DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    connection = dataSource.getConnection();
	    statementText = "SELECT discount_type_code FROM PUB.mem_allowed_fee_discount WHERE fee_type_code = ?";

	    statement = connection.prepareStatement(statementText);
	    statement.setString(1, feeTypeCode);
	    ResultSet discountListRS = statement.executeQuery();

	    // Fetch the product benefit value object from the cache for each
	    // product benefit code returned.
	    while (discountListRS.next()) {
		String discountCode = discountListRS.getString(1);
		DiscountTypeVO discTypeVO = getDiscountType(discountCode);
		if (discTypeVO != null) {
		    discountList.add(discTypeVO);
		}
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
	return discountList;
    }

    /**
     * Return the list of all join reasons.
     * 
     * @return The joinReasonList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getJoinReasonList() throws RemoteException {
	return JoinReasonCache.getInstance().getJoinReasonList();
    }

    public void initialiseJoinReasonList() throws RemoteException {
	JoinReasonCache.getInstance().initialiseList();
    }

    /**
     * Gets the joinReason attribute of the MembershipRefMgrBean object
     * 
     * @param reasonCode
     *            Description of the Parameter
     * @return The joinReason value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ReferenceDataVO getJoinReason(String reasonCode) throws RemoteException {
	return JoinReasonCache.getInstance().getJoinReason(reasonCode);
    }

    /**
     * Return the rejoin reason reference data for the given reason code.
     * Returns null if the reason code could not be found.
     * 
     * @param reasonCode
     *            Description of the Parameter
     * @return The rejoinReason value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ReferenceDataVO getRejoinReason(String reasonCode) throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.getReferenceData(ReferenceDataVO.REF_TYPE_REJOIN_REASON, reasonCode);
    }

    /**
     * Return a list of all of the rejoin reasons as ReferenceDataVO. Returns an
     * empty list if no rejoin reasons can be found.
     * 
     * @return The rejoinReasonList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getRejoinReasonList() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.getReferenceDataListByType(ReferenceDataVO.REF_TYPE_REJOIN_REASON);
    }

    /**
     * Returns the specified rejoin type as ReferenceDataVO. Return null if the
     * rejoin type code was not found.
     * 
     * @param rejoinTypeCode
     *            Description of the Parameter
     * @return The rejoinType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ReferenceDataVO getRejoinType(String rejoinTypeCode) throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.getReferenceData(ReferenceDataVO.REF_TYPE_REJOIN_TYPE, rejoinTypeCode);
    }

    /**
     * Return a list of all of the rejoin types as ReferenceDataVO. Returns an
     * empty list if no rejoin types can be found.
     * 
     * @return The rejoinTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getRejoinTypeList() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.getReferenceDataListByType(ReferenceDataVO.REF_TYPE_REJOIN_TYPE);
    }

    /**
     * Return the list of affiliated motor clubs, as ReferenceDataVO, that are
     * affiliated with the RACT.
     * 
     * @return The affiliatedClubList value
     * @exception RemoteException
     *                Description of the Exception
     */
    // public Collection getAffiliatedClubList()
    // throws RemoteException
    // {
    // ArrayList affiliatedClubList = new ArrayList();
    // ReferenceDataHome refDataHome = CommonEJBHelper.getReferenceDataHome();
    // Collection clubList = null;
    // try
    // {
    // clubList =
    // refDataHome.findByReferenceType(ReferenceData.REF_TYPE_AFFILIATED_MOTOR_CLUB);
    // }
    // catch(FinderException fe)
    // {
    // throw new RemoteException(fe.getMessage());
    // }
    // if(clubList != null)
    // {
    // ReferenceData refData = null;
    // ReferenceDataVO refDataVO = null;
    // Iterator clubListIterator = clubList.iterator();
    // while(clubListIterator.hasNext())
    // {
    // refData = (ReferenceData)clubListIterator.next();
    // refDataVO = refData.getVO();
    // if(refDataVO != null && refDataVO.isActive())
    // {
    // affiliatedClubList.add(refDataVO);
    // }
    // }
    // }
    // return affiliatedClubList;
    // }

    /**
     * Return the cancellation reason
     * 
     * @param reasonCode
     *            Description of the Parameter
     * @return The cancelReason value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ReferenceDataVO getCancelReason(String reasonCode) throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	// System.out.println("getting can reason " +
	// ReferenceData.REF_TYPE_CANCEL_REASON);
	// System.out.println(reasonCode);
	ReferenceDataVO cancelReason = commonMgr.getReferenceData(ReferenceDataVO.REF_TYPE_CANCEL_REASON, reasonCode);
	return cancelReason;
    }

    /**
     * Return the list of cancellation reasons
     * 
     * @return The cancelReasonList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getCancelReasonList() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	Collection cancelReasonList = commonMgr.getReferenceDataListByType(ReferenceDataVO.REF_TYPE_CANCEL_REASON);
	return cancelReasonList;
    }

    /**
     * Sets the membershipType attribute of the MembershipRefMgrBean object
     * 
     * @param membershipType
     *            The new membershipType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setMembershipType(MembershipTypeVO membershipType) throws RemoteException {
	throw new RemoteException("Not implemented");
    }

    /**
     * Sets the product attribute of the MembershipRefMgrBean object
     * 
     * @param product
     *            The new product value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setProduct(ProductVO product) throws RemoteException {
	throw new RemoteException("Not implemented");
    }

    /**
     * Sets the productBenefitType attribute of the MembershipRefMgrBean object
     * 
     * @param productBenefit
     *            The new productBenefitType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setProductBenefitType(ProductBenefitTypeVO productBenefit) throws RemoteException {
	throw new RemoteException("Not implemented");
    }

    /**
     * Sets the membershipTransactionType attribute of the MembershipRefMgrBean
     * object
     * 
     * @param transactionType
     *            The new membershipTransactionType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setMembershipTransactionType(MembershipTransactionTypeVO transactionType) throws RemoteException {
	throw new RemoteException("Not implemented");
    }

    /**
     * Sets the discountType attribute of the MembershipRefMgrBean object
     * 
     * @param discountType
     *            The new discountType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDiscountType(DiscountTypeVO discountType) throws RemoteException {
	throw new RemoteException("Not implemented");
    }

    /**
     * Cache reset methods *********************
     * 
     * @exception RemoteException
     *                Description of the Exception
     */

    /**
     * Clear the cache of discount types. The next fetch on it will cause a
     * refresh from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetDiscountTypeCache() throws RemoteException {
	DiscountTypeCache.getInstance().reset();
    }

    /**
     * Clear the cache of discount types. The next fetch on it will cause a
     * refresh from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetFeeTypeCache() throws RemoteException {
	FeeTypeCache.getInstance().reset();
    }

    /**
     * Clear the cache of discount types. The next fetch on it will cause a
     * refresh from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetFeeSpecificationCache() throws RemoteException {
	FeeSpecificationCache.getInstance().reset();
    }

    /**
     * Clear the cache of join reason codes. The next fetch on it will cause a
     * refresh from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetJoinReasonCache() throws RemoteException {
	JoinReasonCache.getInstance().reset();
    }

    public void resetMembershipAccountCache() throws RemoteException {
	MembershipAccountCache.getInstance().reset();
    }

    /**
     * Clear the cache of products. The next fetch on it will cause a refresh
     * from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetProductCache() throws RemoteException {
	ProductCache.getInstance().reset();
    }

    /**
     * Clear the cache of product benefit types. The next fetch on it will cause
     * a refresh from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetProductBenefitTypeCache() throws RemoteException {
	ProductBenefitCache.getInstance().reset();
    }

    /**
     * Clear the cache of transaction types. The next fetch on it will cause a
     * refresh from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetMembershipTransactionTypeCache() throws RemoteException {
	MembershipTransactionTypeCache.getInstance().reset();
    }

    public void resetMembershipProfileCache() throws RemoteException {
	MembershipProfileCache.getInstance().reset();
    }

    /**
     * Clear the cache of membership types. The next fetch on it will cause a
     * refresh from the database
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void resetMembershipTypeCache() throws RemoteException {
	MembershipTypeCache.getInstance().reset();
    }

    public MembershipAccountVO getMembershipBankAccount() throws RemoteException {
	MembershipAccountVO memAccountVO = null;
	Collection accountList = MembershipAccountCache.getInstance().getMembershipAccountList();
	if (accountList != null) {
	    MembershipAccountVO tmpAccount;
	    Iterator accountListIterator = accountList.iterator();
	    while (accountListIterator.hasNext() && memAccountVO == null) {
		tmpAccount = (MembershipAccountVO) accountListIterator.next();
		if (tmpAccount.isGstAccount()) {
		    memAccountVO = tmpAccount;
		}
	    }
	}
	return memAccountVO;
    }

    /**
     * Retrieve the default fee for a given product
     * 
     * @param prodCode
     * @param membershipType
     * @param transactionType
     * @param groupCount
     * @param groupIndex
     * @param feeEffectiveDate
     * @return
     * @throws Exception
     */
    public double getFeeForProduct(String prodCode, String membershipType, String transactionType, int groupCount, int groupIndex, DateTime feeEffectiveDate) throws RemoteException {
	FeeSpecificationVO feeSpec = null;
	MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	Collection feeList = refMgr.findProductFee(prodCode, membershipType, transactionType, groupCount, feeEffectiveDate);

	Iterator feeListIterator = feeList.iterator();
	FeeSpecificationVO feeSpecVO = null;
	while (feeListIterator.hasNext() && feeSpec == null) {
	    feeSpecVO = (FeeSpecificationVO) feeListIterator.next();
	    if (feeSpecVO.getFeeTypeCode().equals(prodCode)) {
		feeSpec = feeSpecVO;
	    }
	}
	return feeSpec.getFeePerMember(groupIndex);
    }

}
// class MembershipTypeComparator implements Comparator{
// public int compare(Object o1, Object o2)
// {
// MembershipTypeVO memType1 = (MembershipTypeVO) o1;
// MembershipTypeVO memType2 = (MembershipTypeVO) o2;
//
// if (memType1.getS
//
// }
// }
