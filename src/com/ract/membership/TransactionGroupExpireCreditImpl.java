package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.client.ClientVO;
import com.ract.common.Account;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.Company;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.TaxRateVO;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.payment.PayableItemComponentVO;
import com.ract.payment.PayableItemGenerator;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PostingContainer;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.user.User;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Transaction group to represent the expiration of credit attached to the
 * membership.
 * 
 * @author jyh
 * @version 1.0
 */

public class TransactionGroupExpireCreditImpl extends TransactionGroup implements PayableItemGenerator {

    private MembershipVO membershipVO;
    private Integer transactionId;
    private DateTime transactionDate;

    public double getCreditRemoved() {
	return creditRemoved;
    }

    public TransactionGroupExpireCreditImpl(MembershipVO membershipVO, Integer transactionId, DateTime transactionDate, User user) throws RemoteException {
	super(MembershipTransactionTypeVO.TRANSACTION_TYPE_EXPIRE_CREDIT, user);
	this.membershipVO = membershipVO;
	this.transactionDate = transactionDate;
	this.transactionId = transactionId;
	// reset credit to be removed to 0
	this.creditRemoved = 0;
    }

    /**
     * Create a single expire credit transaction and add it to the transaction
     * group.
     * 
     * @throws RemoteException
     * @throws ValidationException
     */
    public void createTransactionsForSelectedClients() throws RemoteException, ValidationException

    {
	// create the transaction here
	MembershipTransactionVO memTransVO = null;
	MembershipVO oldMemVO = null;
	MembershipVO newMemVO = null;
	SelectedClient selectedClient;
	ClientVO clientVO = null;
	DateTime transDate = getTransactionDate();
	Collection clientList = this.getSelectedClientList();

	Iterator clientListIT = clientList.iterator();

	while (clientListIT.hasNext()) // there is only one, but what the?
	{
	    selectedClient = (SelectedClient) clientListIT.next();
	    clientVO = selectedClient.getClient();
	    oldMemVO = selectedClient.getMembership();
	    // Copy the membership being cancelled and set its status
	    newMemVO = oldMemVO.copy();

	    // update the credit amount
	    this.creditRemoved += oldMemVO.getCreditAmount();

	    // reduce the credit to 0 as this is the aim of this process!
	    newMemVO.setCreditAmount(0);

	    // Create a new transaction object for the membership transaction
	    memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.getTransactionGroupTypeCode(), this.getTransactionGroupTypeCode(), selectedClient.getMembershipGroupJoinDate());
	    memTransVO.setTransactionDate(transDate);
	    memTransVO.setUsername(this.getUserID());
	    memTransVO.setSalesBranchCode(this.getSalesBranchCode());
	    memTransVO.setMembership(oldMemVO);
	    memTransVO.setNewMembership(newMemVO);
	    memTransVO.setPrimeAddressee(true);

	    memTransVO.setEffectiveDate(transDate);
	    memTransVO.setEndDate(oldMemVO.getExpiryDate());

	    addMembershipTransaction(memTransVO);
	}

	LogUtil.log(this.getClass(), "creating transactions for " + clientVO.getClientNumber());
    }

    /**
     * Generate a payable item list.
     * 
     * @throws RemoteException
     * @return ArrayList
     */
    public ArrayList generatePayableItemList() throws RemoteException {
	ArrayList payableItemList = new ArrayList();

	PayableItemPostingVO pipVO = null;
	PostingContainer postingList = new PostingContainer();
	LogUtil.log(this.getClass(), "getting memVO");
	if (membershipVO == null) {
	    throw new RemoteException("Can't find membership for " + membershipVO.getMembershipID());
	}
	LogUtil.log(this.getClass(), "after getting memVO");

	try {
	    LogUtil.log(this.getClass(), "Processing " + membershipVO.getMembershipID() + " " + membershipVO.getClientNumber() + " " + transactionDate.formatMediumDate());

	    postingList = createExpiredMembershipCreditPostings(membershipVO);

	    // create component
	    PayableItemComponentVO payableItemComponent = new PayableItemComponentVO(postingList, new BigDecimal(0),// postingList.getSuspenseAmountTotal()
		    POSTING_NARRATION, null, SourceSystem.MEMBERSHIP.getAbbreviation());

	    ArrayList payableItemComponentList = new ArrayList();
	    // add component to list
	    payableItemComponentList.add(payableItemComponent);

	    PaymentMethod paymentMethod = new ReceiptingPaymentMethod(membershipVO.getClientNumber(), SourceSystem.MEMBERSHIP);

	    // create payable item
	    PayableItemVO payableItem = new PayableItemVO(null, membershipVO.getClientNumber(), paymentMethod, payableItemComponentList, SourceSystem.MEMBERSHIP, transactionId, // update
																						 // this
																						 // after
																						 // transaction
		    getUser().getSalesBranchCode(), membershipVO.getClient().getBranchNumber(), membershipVO.getProductCode(), getUser().getUserID(), this.getTransactionDate(), this.getTransactionDate(), POSTING_NARRATION);
	    // add payable item to list
	    payableItemList.add(payableItem);

	    LogUtil.log(this.getClass(), "payableItem=" + payableItem);

	} catch (PaymentException e) {
	    throw new SystemException(e);
	}

	return payableItemList;
    }

    /**
     * Default narration for postings.
     */
    private final String POSTING_NARRATION = "Expire membership credit";

    private double creditRemoved;

    /**
     * Create the set of postings to represent the removal of the membership
     * credit.
     * 
     * @param memVO
     *            MembershipVO
     * @throws RemoteException
     * @return PostingContainer
     */
    private PostingContainer createExpiredMembershipCreditPostings(MembershipVO memVO) throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	TaxRateVO taxRateVO = commonMgr.getCurrentTaxRate();

	Integer componentId = null;

	DateTime postAtDate = new DateTime().getDateOnly();

	MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	PostingContainer expireCreditPostings = new PostingContainer();
	FeeTypeVO feeType = MembershipEJBHelper.getMembershipRefMgr().getFeeType(memVO.getProductCode());

	Account suspenseAccount = refMgr.getMembershipSuspenseAccount();
	Account gstAccount = refMgr.getMembershipGSTAccount();
	Account earnedAccount = feeType.getEarnedAccount();
	String unearnedAccountId = feeType.getUnearnedAccount().getAccountNumber();

	double gstRate = taxRateVO.getGstRate();

	// double debitSuspenseAmount =
	// -suspensePostings.getSuspenseAmountTotal();//reverse the credit
	// the suspense amount may historically be incorrect! It is more
	// accurate to use the actual
	// credit amount stored. Note: this may lead to discrepancies between
	// the suspense total and the credit
	// amount.
	LogUtil.debug(this.getClass(), "creditRemoved=" + creditRemoved);
	double debitSuspenseAmount = this.getCreditRemoved();
	// add it to running total of credit removed
	LogUtil.debug(this.getClass(), "debitSuspenseAmount=" + debitSuspenseAmount);

	double gstAmount = CurrencyUtil.calculateGSTincluded(debitSuspenseAmount, gstRate);
	double unearnedAmount = CurrencyUtil.calculateGSTExclusiveAmount(debitSuspenseAmount, gstRate);

	gstAmount = -gstAmount;
	unearnedAmount = -unearnedAmount;

	LogUtil.debug(this.getClass(), "RLG ++++ earned " + earnedAccount.getAccountNumber() + " unearned " + unearnedAccountId + " credit " + memVO.getCreditAmount());
	try {
	    LogUtil.debug(this.getClass(), "RLG ++++ Suspense " + suspenseAccount.getAccountNumber() + " " + debitSuspenseAmount);
	    expireCreditPostings.add(new PayableItemPostingVO(debitSuspenseAmount, suspenseAccount, null, postAtDate, postAtDate, Company.RACT, POSTING_NARRATION, componentId));
	    LogUtil.debug(this.getClass(), "RLG ++++ GST " + gstAccount.getAccountNumber() + " " + gstAmount);
	    expireCreditPostings.add(new PayableItemPostingVO(gstAmount, gstAccount, null, postAtDate, postAtDate, Company.RACT, POSTING_NARRATION, componentId));
	    LogUtil.debug(this.getClass(), "RLG ++++ Earned " + earnedAccount.getAccountNumber() + " " + unearnedAmount);
	    expireCreditPostings.add(new PayableItemPostingVO(unearnedAmount, earnedAccount, null, postAtDate, postAtDate, Company.RACT, POSTING_NARRATION, componentId));

	} catch (PaymentException ex) {
	    throw new RemoteException(ex.getMessage());
	}

	return expireCreditPostings;

    }

    /**
     * Overrride default submit action.
     * 
     * @return Collection
     */
    public synchronized Collection submit() throws RemoteException, ValidationException {
	MembershipTransactionMgr memTxMgr = MembershipEJBHelper.getMembershipTransactionMgr();
	Collection completedTransactionList = memTxMgr.processExpireMembershipCredit(this);
	return completedTransactionList;
    }

    /**
     * Overrride default transaction fee implementation so that it does not
     * create any fees.
     * 
     * @throws RemoteException
     */
    public void createTransactionFees() throws RemoteException {
	// no fees applicable
    }

}
