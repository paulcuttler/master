package com.ract.membership;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.ract.common.SystemParameterVO;
import com.ract.payment.PaymentMgrLocal;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Roadside ledger report, daily breakdown for a given month.
 * 
 * @author newtong
 * 
 */
public class RoadsideLedgerReportDailyAction extends ActionSupport {

    @InjectEJB(name = "MembershipMgrBean")
    MembershipMgrLocal membershipMgrLocal;

    @InjectEJB(name = "PaymentMgrBean")
    PaymentMgrLocal paymentMgrLocal;

    private String start;
    private Calendar startDate;

    private List<Map<String, Map<String, BigDecimal>>> dailyBreakdown = new ArrayList<Map<String, Map<String, BigDecimal>>>();

    public String execute() throws Exception {

	// loop N days in month
	startDate.set(Calendar.DATE, 1);
	Calendar endDate = Calendar.getInstance();
	endDate.setTime(startDate.getTime());
	endDate.add(Calendar.MONTH, 1);

	Calendar endRange = Calendar.getInstance();

	BigDecimal postings;
	BigDecimal vouchers;
	BigDecimal credits;
	BigDecimal variance;
	String label;
	Map<String, BigDecimal> resultSet;
	Map<String, Map<String, BigDecimal>> holder;

	while (startDate.before(endDate)) {
	    endRange.setTime(startDate.getTime());
	    endRange.add(Calendar.DATE, 1);

	    DateTime startDt = new DateTime(startDate.getTime());
	    DateTime endDt = new DateTime(endRange.getTime());

	    label = (new DateTime(startDate.getTime())).formatShortDate() + ":" + (new DateTime(endRange.getTime())).formatShortDate();

	    LogUtil.log(getClass(), "Retrieving Total Postings between " + startDt + " and " + endDt);
	    postings = paymentMgrLocal.getTotalPostings(SystemParameterVO.CREDIT_ACCOUNT, startDt, endDt);
	    LogUtil.log(getClass(), "Total postings: " + postings);

	    LogUtil.log(getClass(), "Retrieving Total Vouchers between " + startDt + " and " + endDt);
	    vouchers = paymentMgrLocal.getTotalVouchers(startDt, endDt);
	    LogUtil.log(getClass(), "Total vouchers: " + vouchers);

	    LogUtil.log(getClass(), "Retrieving Total Credit between " + startDt + " and " + endDt);
	    credits = membershipMgrLocal.getTotalInstantaneousMembershipCredit(startDt, endDt);
	    LogUtil.log(getClass(), "Total credits: " + credits);

	    variance = postings.add(vouchers).add(credits);

	    resultSet = new HashMap<String, BigDecimal>();
	    resultSet.put("postings", postings);
	    resultSet.put("vouchers", vouchers);
	    resultSet.put("credits", credits);
	    resultSet.put("variance", variance);

	    holder = new HashMap<String, Map<String, BigDecimal>>();
	    holder.put(label, resultSet);

	    dailyBreakdown.add(holder);

	    startDate.add(Calendar.DATE, 1);
	}

	return SUCCESS;
    }

    public void validate() {
	try {
	    DateTime startDt = new DateTime(start);
	    startDate = Calendar.getInstance();
	    startDate.setTime(startDt);

	} catch (Exception e) {
	    this.addFieldError("start", "Please provide a valid start date");
	}
    }

    public List<Map<String, Map<String, BigDecimal>>> getDailyBreakdown() {
	return dailyBreakdown;
    }

    public void setDailyBreakdown(List<Map<String, Map<String, BigDecimal>>> dailyBreakdown) {
	this.dailyBreakdown = dailyBreakdown;
    }

    public String getStart() {
	return start;
    }

    @RequiredStringValidator(message = "Please provide a start date")
    public void setStart(String start) {
	this.start = start;
    }

}
