package com.ract.membership;

import java.util.Collection;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.common.CommonMgrLocal;
import com.ract.payment.FinanceVoucher;
import com.ract.security.Privilege;
import com.ract.security.SecurityMgrLocal;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

public class ShowManageCreditsAction extends ActionSupport implements SessionAware {

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    @InjectEJB(name = "SecurityMgrBean")
    private SecurityMgrLocal securityMgrLocal;

    private Map<String, Object> session = null;

    private boolean memberDetails;

    public boolean isMemberDetails() {
	return memberDetails;
    }

    public void setMemberDetails(boolean membershipDetails) {
	this.memberDetails = membershipDetails;
    }

    private ClientVO client;

    private MembershipVO memVO;

    private Collection refDataList;

    private Integer membershipId;

    private String disposalMode;

    public String getDisposalMode() {
	return disposalMode;
    }

    public void setDisposalMode(String disposalMode) {
	this.disposalMode = disposalMode;
    }

    private Collection approvingUserList;

    public Collection getApprovingUserList() {
	return approvingUserList;
    }

    public void setApprovingUserList(Collection approvingUserList) {
	this.approvingUserList = approvingUserList;
    }

    @Override
    public String execute() throws Exception {

	setMemberDetails(true);

	disposalMode = FinanceVoucher.MODE_CREDIT; //

	final String MASK_CURRENCY = "######0.00";

	// determine branch number from userSession
	UserSession userSession = (UserSession) this.getSession().get(LoginUIConstants.USER_SESSION);
	String branchId = Integer.toString(userSession.getUser().getBranchNumber());
	approvingUserList = securityMgrLocal.getUsersByPrivilegeAndBranch(Privilege.PRIVILEGE_APPROVE_CREDIT_DISPOSAL, branchId);

	refDataList = commonMgr.getReferenceDataListByType("MEMREFREASON"); // TODO
									    // use
									    // constant
	memVO = membershipMgr.getMembership(membershipId);
	client = memVO.getClient();

	double adminFeeAmount = 0;

	LogUtil.debug(this.getClass(), "adminFeeAmount=" + adminFeeAmount);
	feeAmount = NumberUtil.formatValue(adminFeeAmount, MASK_CURRENCY);
	LogUtil.debug(this.getClass(), "feeAmount=" + feeAmount);
	double credAmount = memVO.getCreditAmount();

	LogUtil.debug(this.getClass(), "credAmount=" + credAmount);
	creditAmount = NumberUtil.formatValue(credAmount, MASK_CURRENCY);

	LogUtil.debug(this.getClass(), "creditAmount=" + creditAmount);
	disposalAmount = NumberUtil.formatValue(credAmount - adminFeeAmount, MASK_CURRENCY);
	LogUtil.debug(this.getClass(), "disposalAmount=" + disposalAmount);
	// need to validate amount on save

	return SUCCESS;
    }

    public String getFeeAmount() {
	return feeAmount;
    }

    public void setFeeAmount(String feeAmount) {
	this.feeAmount = feeAmount;
    }

    public String getCreditAmount() {
	return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
	this.creditAmount = creditAmount;
    }

    private String feeAmount;

    private String creditAmount;

    private String disposalAmount;

    public String getDisposalAmount() {
	return disposalAmount;
    }

    public void setDisposalAmount(String disposalAmount) {
	this.disposalAmount = disposalAmount;
    }

    public ClientVO getClient() {
	return client;
    }

    public void setClient(ClientVO client) {
	this.client = client;
    }

    public MembershipVO getMemVO() {
	return memVO;
    }

    public void setMemVO(MembershipVO memVO) {
	this.memVO = memVO;
    }

    public Collection getRefDataList() {
	return refDataList;
    }

    public void setRefDataList(Collection refDataList) {
	this.refDataList = refDataList;
    }

    public Integer getMembershipId() {
	return membershipId;
    }

    public void setMembershipId(Integer membershipId) {
	this.membershipId = membershipId;
    }

    @Override
    public void setSession(Map<String, Object> session) {
	this.session = session;
    }

    public Map<String, Object> getSession() {
	return session;
    }

}
