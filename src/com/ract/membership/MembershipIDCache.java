package com.ract.membership;

import java.rmi.RemoteException;

/**
 * @author T. Bakker
 * @version 1.0
 */

public class MembershipIDCache {
    private static MembershipIDCache instance = new MembershipIDCache();

    private int lastDocumentID = 0;

    private int lastGroupID = 0;

    private int lastMembershipID = 0;

    private int lastQuoteNumber = 0;

    private int lastTransactionID = 0;

    private int lastTransactionGroupID = 0;

    // public MembershipIDCache()
    // {
    // }

    /**
     * Return a reference to the one and only instance of this class.
     */
    public static MembershipIDCache getInstance() {
	return instance;
    }

    /**
     * Return the next ID in the Membership Document ID sequence. Must be
     * synchronised so that two threads don't do the initialisation at the same
     * time.
     */
    public synchronized int getNextDocumentID(MembershipIDMgrBean idMgr) throws RemoteException {
	if (this.lastDocumentID == 0) {
	    // Date startTime = new Date();
	    this.lastDocumentID = idMgr.getLastDocumentID();
	    // long elapsedTime = new Date().getTime() - startTime.getTime();
	    // log("Initialising last membership document ID to : " +
	    // this.lastDocumentID + " in " + elapsedTime + " milliseconds.");
	}
	return ++this.lastDocumentID;
    }

    /**
     * Return the next ID in the Membership Document ID sequence. Must be
     * synchronised so that two threads don't do the initialisation at the same
     * time.
     */
    public synchronized int getNextGroupID(MembershipIDMgrBean idMgr) throws RemoteException {
	if (this.lastGroupID == 0) {
	    // Date startTime = new Date();
	    this.lastGroupID = idMgr.getLastGroupID();
	    // long elapsedTime = new Date().getTime() - startTime.getTime();
	    // log("Initialising last membership group ID to : " +
	    // this.lastGroupID + " in " + elapsedTime + " milliseconds.");
	}
	return ++this.lastGroupID;
    }

    /**
     * Return the next ID in the Membership ID sequence. Must be synchronised so
     * that two threads don't do the initialisation at the same time.
     */
    public synchronized int getNextMembershipID(MembershipIDMgrBean idMgr) throws RemoteException {
	if (this.lastMembershipID == 0) {
	    // Date startTime = new Date();
	    this.lastMembershipID = idMgr.getLastMembershipID();
	    // long elapsedTime = new Date().getTime() - startTime.getTime();
	    // log("Initialising last membership ID to : " +
	    // this.lastMembershipID + " in " + elapsedTime + " milliseconds.");
	}
	return ++this.lastMembershipID;
    }

    /**
     * Return the next ID in the quote number sequence. Must be synchronised so
     * that two threads don't do the initialisation at the same time.
     */
    public synchronized int getNextQuoteNumber(MembershipIDMgrBean idMgr) throws RemoteException {
	if (this.lastQuoteNumber == 0) {
	    // Date startTime = new Date();
	    this.lastQuoteNumber = idMgr.getLastQuoteNumber();
	    // long elapsedTime = new Date().getTime() - startTime.getTime();
	    // log("Initialising last quote number to : " + this.lastQuoteNumber
	    // + " in " + elapsedTime + " milliseconds.");
	}
	return ++this.lastQuoteNumber;
    }

    /**
     * Return the next ID in the Membership transaction ID sequence. Must be
     * synchronised so that two threads don't do the initialisation at the same
     * time.
     */
    public synchronized int getNextTransactionID(MembershipIDMgrBean idMgr) throws RemoteException {
	if (this.lastTransactionID == 0) {
	    // Date startTime = new Date();
	    this.lastTransactionID = idMgr.getLastTransactionID();
	    // long elapsedTime = new Date().getTime() - startTime.getTime();
	    // log("Initialising last membership transaction ID to : " +
	    // this.lastTransactionID + " in " + elapsedTime +
	    // " milliseconds.");
	}
	this.lastTransactionID++;
	return this.lastTransactionID;
    }

    /**
     * Return the next ID in the Membership transaction ID sequence. Must be
     * synchronised so that two threads don't do the initialisation at the same
     * time.
     */
    public synchronized int getNextTransactionGroupID(MembershipIDMgrBean idMgr) throws RemoteException {
	if (this.lastTransactionGroupID == 0) {
	    // Date startTime = new Date();
	    this.lastTransactionGroupID = idMgr.getLastTransactionGroupID();
	    // long elapsedTime = new Date().getTime() - startTime.getTime();
	    // log("Initialising last membership transaction group ID to : " +
	    // this.lastTransactionGroupID + " in " + elapsedTime +
	    // " milliseconds.");
	}
	this.lastTransactionGroupID++;
	return this.lastTransactionGroupID;
    }

    /**
     * Reset all of the ID so that they are initialised again
     */
    public void reset() {
	this.lastDocumentID = 0;
	this.lastGroupID = 0;
	this.lastMembershipID = 0;
	this.lastQuoteNumber = 0;
	this.lastTransactionID = 0;
	this.lastTransactionGroupID = 0;
    }
}
