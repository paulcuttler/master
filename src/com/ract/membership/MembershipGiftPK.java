package com.ract.membership;

/**
 * <p>
 * Primary key for a membership gift
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class MembershipGiftPK implements java.io.Serializable {

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((giftCode == null) ? 0 : giftCode.hashCode());
	result = prime * result + ((membershipId == null) ? 0 : membershipId.hashCode());
	result = prime * result + ((status == null) ? 0 : status.hashCode());
	result = prime * result + ((tierCode == null) ? 0 : tierCode.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof MembershipGiftPK))
	    return false;
	MembershipGiftPK other = (MembershipGiftPK) obj;
	if (giftCode == null) {
	    if (other.giftCode != null)
		return false;
	} else if (!giftCode.equals(other.giftCode))
	    return false;
	if (membershipId == null) {
	    if (other.membershipId != null)
		return false;
	} else if (!membershipId.equals(other.membershipId))
	    return false;
	if (status == null) {
	    if (other.status != null)
		return false;
	} else if (!status.equals(other.status))
	    return false;
	if (tierCode == null) {
	    if (other.tierCode != null)
		return false;
	} else if (!tierCode.equals(other.tierCode))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "MembershipGiftPK [giftCode=" + giftCode + ", membershipId=" + membershipId + ", status=" + status + ", tierCode=" + tierCode + "]";
    }

    /**
     * @hibernate.key-property column="tier_code"
     * @return String
     */
    public String getTierCode() {
	return tierCode;
    }

    public void setTierCode(String tierCode) {
	this.tierCode = tierCode;
    }

    private String giftCode;

    private Integer membershipId;

    private String status;

    /**
     * @hibernate.key-property column="membership_id"
     * @return Integer
     */
    public Integer getMembershipId() {
	return membershipId;
    }

    public void setGiftCode(String giftCode) {
	this.giftCode = giftCode;
    }

    public void setMembershipId(Integer membershipId) {
	this.membershipId = membershipId;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    /**
     * @hibernate.key-property column="gift_code"
     * @return String
     */
    public String getGiftCode() {
	return giftCode;
    }

    /**
     * @hibernate.key-property column="gift_status"
     * @return String
     */
    public String getStatus() {
	return status;
    }

    public MembershipGiftPK() {
	// default
    }

    private String tierCode;

}
