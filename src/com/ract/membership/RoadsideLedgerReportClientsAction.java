package com.ract.membership;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.ract.common.SystemParameterVO;
import com.ract.payment.PaymentMgrLocal;
import com.ract.util.DateTime;

public class RoadsideLedgerReportClientsAction extends ActionSupport {

    @InjectEJB(name = "MembershipMgrBean")
    MembershipMgrLocal membershipMgrLocal;

    @InjectEJB(name = "PaymentMgrBean")
    PaymentMgrLocal paymentMgrLocal;

    private String start;
    private String end;

    DateTime startDt;
    DateTime endDt;

    private Map<Integer, BigDecimal> memberCredit;
    private Map<Integer, BigDecimal> clientVouchers;
    private Map<Integer, BigDecimal> clientPostings;

    private Map<Integer, MembershipVO> membershipLookup = new HashMap<Integer, MembershipVO>();
    private Map<Integer, Map<String, BigDecimal>> clientBreakdown = new HashMap<Integer, Map<String, BigDecimal>>();

    public String execute() throws Exception {

	Map<String, BigDecimal> resultSet = new HashMap<String, BigDecimal>();

	// clients having postings
	clientPostings = paymentMgrLocal.getPostingTotalsForAccount(SystemParameterVO.CREDIT_ACCOUNT, startDt, endDt);
	for (Integer clientNumber : clientPostings.keySet()) {
	    resultSet = clientBreakdown.get(clientNumber);
	    if (resultSet == null) {
		resultSet = new HashMap<String, BigDecimal>();
	    }
	    resultSet.put("postings", clientPostings.get(clientNumber));

	    clientBreakdown.put(clientNumber, resultSet);
	}

	// clients having vouchers
	clientVouchers = paymentMgrLocal.getVouchersByRange(startDt, endDt);
	for (Integer clientNumber : clientVouchers.keySet()) {
	    resultSet = clientBreakdown.get(clientNumber);
	    if (resultSet == null) {
		resultSet = new HashMap<String, BigDecimal>();
	    }
	    resultSet.put("vouchers", clientVouchers.get(clientNumber));

	    clientBreakdown.put(clientNumber, resultSet);
	}

	// members having credit
	memberCredit = membershipMgrLocal.getInstantaneousMembershipCredit(startDt, endDt);
	for (Integer membershipID : memberCredit.keySet()) {

	    MembershipVO memVO = membershipMgrLocal.getMembership(membershipID);
	    membershipLookup.put(memVO.getClientNumber(), memVO);

	    resultSet = clientBreakdown.get(memVO.getClientNumber());
	    if (resultSet == null) {
		resultSet = new HashMap<String, BigDecimal>();
	    }
	    resultSet.put("credit", memberCredit.get(membershipID));

	    clientBreakdown.put(memVO.getClientNumber(), resultSet);
	}

	// determine variance
	for (Integer clientNumber : clientBreakdown.keySet()) {
	    resultSet = clientBreakdown.get(clientNumber);

	    BigDecimal postings = resultSet.get("postings");
	    if (postings == null) {
		postings = BigDecimal.ZERO;
	    }
	    BigDecimal vouchers = resultSet.get("vouchers");
	    if (vouchers == null) {
		vouchers = BigDecimal.ZERO;
	    }
	    BigDecimal credit = resultSet.get("credit");
	    if (credit == null) {
		credit = BigDecimal.ZERO;
	    }

	    resultSet.put("variance", postings.add(vouchers).add(credit));
	}

	for (Integer clientNumber : clientBreakdown.keySet()) {
	    if (!membershipLookup.containsKey(clientNumber)) {
		List<MembershipVO> members = (List<MembershipVO>) membershipMgrLocal.findMembershipByClientNumber(clientNumber);
		if (members.size() > 0) {
		    membershipLookup.put(clientNumber, members.get(0));
		}
	    }
	}

	return SUCCESS;
    }

    public String getStart() {
	return start;
    }

    public void validate() {
	try {
	    startDt = new DateTime(start);

	} catch (Exception e) {
	    this.addFieldError("start", "Please provide a valid start date");
	}

	try {
	    endDt = new DateTime(end);

	} catch (Exception e) {
	    this.addFieldError("end", "Please provide a valid end date");
	}
    }

    @RequiredStringValidator(message = "Please provide a start date")
    public void setStart(String start) {
	this.start = start;
    }

    public String getEnd() {
	return end;
    }

    @RequiredStringValidator(message = "Please provide an end date")
    public void setEnd(String end) {
	this.end = end;
    }

    public Map<Integer, MembershipVO> getMembershipLookup() {
	return membershipLookup;
    }

    public void setMembershipLookup(Map<Integer, MembershipVO> membershipLookup) {
	this.membershipLookup = membershipLookup;
    }

    public Map<Integer, Map<String, BigDecimal>> getClientBreakdown() {
	return clientBreakdown;
    }

    public void setClientBreakdown(Map<Integer, Map<String, BigDecimal>> clientBreakdown) {
	this.clientBreakdown = clientBreakdown;
    }

}