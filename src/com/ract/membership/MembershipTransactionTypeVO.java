package com.ract.membership;

import java.rmi.RemoteException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;

/**
 * Specifies a transaction type. These are fixed by the system and cannot be
 * changed by the operator.
 * 
 * @author T. Bakker. created 1 August 2002
 */
@Entity
@Table(name = "mem_transaction_type")
public class MembershipTransactionTypeVO extends ValueObject implements Writable, CachedItem {

    // These transaction types are saved in the membership transaction record.
    public static final String TRANSACTION_TYPE_CANCEL = "Cancel";

    public static final String TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST = "Card request";

    public static final String TRANSACTION_TYPE_CHANGE_GROUP_PA = "Change group PA";

    public static final String TRANSACTION_TYPE_CHANGE_PRODUCT = "Change product";

    public static final String TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION = "Change option";

    public static final String TRANSACTION_TYPE_CREATE = "Create";

    public static final String TRANSACTION_TYPE_EDIT_MEMBERSHIP = "Edit membership";

    public static final String TRANSACTION_TYPE_HOLD = "Hold";

    public static final String TRANSACTION_TYPE_REJOIN = "Rejoin";

    public static final String TRANSACTION_TYPE_RENEW = "Renew";

    public static final String TRANSACTION_TYPE_TRANSFER = "Transfer";

    public static final String TRANSACTION_TYPE_WRITE_OFF = "Write Off";

    public static final String TRANSACTION_TYPE_REFUND = "Refund";

    public static final String TRANSACTION_TYPE_TRANSFER_CREDIT = "Transfer Credit";

    public static final String TRANSACTION_TYPE_EXPIRE_CREDIT = "Expire Credit";

    // These transaction types provide an overall control for individual
    // membership transactions when dealing with a membership group.
    public static final String TRANSACTION_TYPE_CREATE_GROUP = "Create group";

    public static final String TRANSACTION_TYPE_CHANGE_GROUP = "Change group";

    public static final String TRANSACTION_TYPE_RENEW_GROUP = "Renew group";

    // These transaction types are tags that indicate some other change to
    // a membership that does not involve creating or updating the membership.
    // These transaction types are not saved.
    // public static final String TRANSACTION_TYPE_NO_CHANGE = "No change";
    public static final String TRANSACTION_TYPE_REMOVE_FROM_GROUP = "Remove from group";

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "MembershipTransactionType";

    /**
     * A unique id that identifies each type of transaction. The transaction
     * type ID can be used as the display name of the transaction type. e.g.
     * "Join", "Renewal".
     */
    @Id
    @Column(name = "transaction_type_code")
    protected String transactionTypeCode;

    /**
     * A description of the transaction type.
     */
    protected String description;

    /**
     * Some transactions generate a payable fee that is not treated as income
     * until it is earned. For example a membership fee is not treated as income
     * immediately. Periodically, as the service is delivered (i.e. time passes)
     * the revenue is moved from the unearned to income accounts. The
     * interfacing of Membership and Payments requires the knowledge that a
     * transaction type generates unearned income so it can be used for
     * calculating any current credits in the form of income that hasn't yet
     * been earned.
     */
    @Column(name = "generates_unearned_income")
    protected boolean generatesUnearnedIncome;

    /**
     * Constructors *************************
     */

    /**
     * Default constructor.
     */
    public MembershipTransactionTypeVO() {
    }

    /**
     * Initialise the membership transaction type with the data from the XML
     * element.
     * 
     * @param transactionTypeXML
     *            Description of the Parameter
     */
    public MembershipTransactionTypeVO(String transactionTypeXML) {
    }

    /**
     * Initialise the transaction type with the specified data.
     * 
     * @param transactionTypeCode
     *            Description of the Parameter
     * @param description
     *            Description of the Parameter
     */
    public MembershipTransactionTypeVO(String transactionTypeCode, String description) {
	this.transactionTypeCode = transactionTypeCode;
	this.description = description;
    }

    /**
     * Initialise the transaction type with the specified data.
     * 
     * @param transactionTypeCode
     *            Description of the Parameter
     * @param description
     *            Description of the Parameter
     * @param generatesUnearnedIncome
     *            Description of the Parameter
     */
    public MembershipTransactionTypeVO(String transactionTypeCode, String description, boolean generatesUnearnedIncome) {
	this.transactionTypeCode = transactionTypeCode;
	this.description = description;
	this.generatesUnearnedIncome = generatesUnearnedIncome;
    }

    /**
     * Getter methods *************************
     * 
     * @return The transactionTypeCode value
     */

    public String getTransactionTypeCode() {
	return this.transactionTypeCode;
    }

    /**
     * Gets the description attribute of the MembershipTransactionTypeVO object
     * 
     * @return The description value
     */
    public String getDescription() {
	return this.description;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public boolean generatesUnearnedIncome() {
	return this.generatesUnearnedIncome;
    }

    /**
     * Setter methods *************************
     * 
     * @param transactionTypeCode
     *            The new transactionTypeCode value
     * @exception RemoteException
     *                Description of the Exception
     */

    public void setTransactionTypeCode(String transactionTypeCode) throws RemoteException {
	checkReadOnly();
	this.transactionTypeCode = transactionTypeCode;
    }

    /**
     * Sets the description attribute of the MembershipTransactionTypeVO object
     * 
     * @param description
     *            The new description value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    /**
     * Description of the Method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
    }

    /**
     * Sets the generatesUnearnedIncome attribute of the
     * MembershipTransactionTypeVO object
     * 
     * @param generatesUnearnedIncome
     *            The new generatesUnearnedIncome value
     */
    protected void setGeneratesUnearnedIncome(boolean generatesUnearnedIncome) {
	this.generatesUnearnedIncome = generatesUnearnedIncome;
    }

    /**
     * CachedItem interface methods ***************
     * 
     * @return The key value
     */

    /**
     * Return the membership transaction type code as the cached item key.
     * 
     * @return The key value
     */
    public String getKey() {
	return this.getTransactionTypeCode();
    }

    /**
     * Determine if the specified key matches the transaction type code. Return
     * true if both the key and the transaction type code are null.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("transactionTypeCode", this.transactionTypeCode);
	cw.writeAttribute("description", this.description);
	cw.endClass();
    }
}