package com.ract.membership;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.Status;
import javax.transaction.UserTransaction;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.lob.SerializableClob;

import com.ract.client.Client;
import com.ract.client.ClientHelper;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientSubscription;
import com.ract.client.ClientVO;
import com.ract.common.CardHelper;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.MailMgrLocal;
import com.ract.common.PostalAddressVO;
import com.ract.common.Publication;
import com.ract.common.ReferenceDataVO;
import com.ract.common.RollBackException;
import com.ract.common.SMSManager;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportException;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.membership.reporting.MembershipRenewalAdviceRequest;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.payment.billpay.BillpayHelper;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitFrequency;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.directdebit.DirectDebitScheduleItem;
import com.ract.security.SecurityHelper;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.util.ConnectionUtil;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.ReportUtil;
import com.ract.util.StringUtil;

/**
 * Manage the renewal notice extraction for external printing.
 * 
 * @author dgk, jyh
 * @created 5 March 2003
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@Local({ RenewalMgrLocal.class })
@Remote({ RenewalMgr.class })
public class RenewalMgrBean {

	private static final String LABEL_EMAILED = "Emailed";

	private static final String LABEL_PRINTED = "Printed";

	@PersistenceContext(unitName = "master")
	Session hsession;

	@EJB
	private MembershipMgrLocal membershipMgrLocal;

	@EJB
	private MembershipRefMgrLocal membershipRefMgrLocal;

	@EJB
	private UserMgrLocal userMgr;

	@EJB
	private PaymentMgrLocal paymentMgrLocal;

	@EJB
	private ClientMgrLocal clientMgr;

	@EJB
	private CommonMgrLocal commonMgr;

	@EJB
	private PaymentMgrLocal paymentMgr;

	@EJB
	private MembershipTransactionMgrLocal memTransMgr;

	@EJB
	private MailMgrLocal mailMgrLocal;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource dataSource;

	@Resource
	private SessionContext sessionContext;

	protected static String DELIMITER = "|";

	/**
	 * The membership renewal notices are printed by an external agency. This method initiates the process of creating the files. It may be called manually, but more commonly is started by a scheduled process. This method is called without an effective date, the date is set to now and the createMembershipRenewalNotices(DateTime effectiveDate) method is called.
	 */
	public int createMembershipRenewalNotices() throws RemoteException, RenewalNoticeException {
		DateTime transactionDate = new DateTime();
		return createMembershipRenewalNotices(transactionDate, SecurityHelper.getSystemUser().getUserID());
	}

	final int COUNT_PRIMARY_RENEWAL_LINE = 35; // inc marketing flag/insert flag

	public Hashtable<String, Integer> emailRenewals(String renewalFile, boolean update, boolean test) throws ReportException {

		LogUtil.log(this.getClass(), "emailRenewals start");

		Hashtable<String, Integer> count = new Hashtable<String, Integer>();

		LogUtil.log(this.getClass(), "renewalFile=" + renewalFile);
		LogUtil.log(this.getClass(), "update=" + update);
		LogUtil.log(this.getClass(), "test=" + test);

		BufferedReader br = null;

		String line = null;
		Integer renewalClientNumber = null;
		String renewalNoticeType = "";
		String elements[] = null;
		int recordCount = 0;
		int emailCount = 0;
		int printCount = 0;
		int primaryLines = 0;
		int validLines = 0;
		int updateCount = 0;
		int noDeliveryMethod = 0;
		int testCounter = 0;
		String discountString = null;

		try {

			String outputDirectoryName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_LOG_DIR);
			if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') {
				outputDirectoryName += "/";
			}
			String errorFileName = outputDirectoryName + DateUtil.formatYYYYMMDD(new DateTime()) + "emailRenewalsError.csv";
			FileWriter fileWriter = new FileWriter(errorFileName);

			String overrideEmail = null;
			// only use overrideEmail in "test" mode
			if (test) {
			    overrideEmail = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILOVERRIDE");
//			    overrideEmail = "g.sharp@ract.com.au";
			}
			LogUtil.debug(this.getClass(), "overrideEmail=" + overrideEmail);

			String testLimitString = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "TESTEMAILLIMIT");
			LogUtil.debug(this.getClass(), "testLimitString=" + testLimitString);

//			final int COUNT_TEST_MAX = 50;
			final int COUNT_TEST_MAX = Integer.parseInt(testLimitString);
			LogUtil.debug(this.getClass(), "COUNT_TEST_MAX=" + COUNT_TEST_MAX);

			br = new BufferedReader(new InputStreamReader(new FileInputStream(renewalFile)));
			while ((line = br.readLine()) != null && (!test || (test && testCounter < COUNT_TEST_MAX))) {

				recordCount++;
				// for each line in the renewal file
				if (line.startsWith(LINE_TYPE_PRIMARY)) {
					primaryLines++;

					elements = line.split("[" + FileUtil.SEPARATOR_PIPE + "]", -1);

					renewalClientNumber = new Integer(elements[1]);
					
					renewalNoticeType = new String(elements[2]);

					for (int i = 0; i < elements.length; i++) {
						LogUtil.debug(this.getClass(), i + "=" + elements[i]);
					}

					int elementCount = elements.length;
					LogUtil.debug(this.getClass(), "elementCount=" + elementCount);
					// what is the length

					if (elementCount != COUNT_PRIMARY_RENEWAL_LINE) {
						LogUtil.warn(this.getClass(), LINE_TYPE_PRIMARY + " record at line " + recordCount + " does not contain " + COUNT_PRIMARY_RENEWAL_LINE + " elements (" + elementCount + ").");
						continue;
					}

					validLines++;

					String emailAddress = elements[elements.length - 4];
					LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);

					String emailString = elements[elements.length - 5];
					String printString = elements[elements.length - 6];
					LogUtil.debug(this.getClass(), "emailString=" + emailString);
					LogUtil.debug(this.getClass(), "printString=" + printString);

					boolean emailed = emailString.equals("Y");
					boolean printed = printString.equals("Y");
					LogUtil.debug(this.getClass(), "emailed=" + emailed);
					LogUtil.debug(this.getClass(), "printed=" + printed);
					
//					renewalClientNumber = new Integer(elements[1]);
//					LogUtil.debug(this.getClass(), "renewalClientNumber=" + renewalClientNumber);
					
					// Capture the discouht for Lifestyle eRenewal and 2020 "better together" Lifestyle members
					discountString = elements[19];

					/*
					 * eRenewal trial participant code change Jira APPS-321
					 * Check that client is listed in the erenewal_pilot table in rshsqlprd1.rac.com.au
					 * If the client is present set printed to false (which should not usually be the case as an active subscription cancels printing in the renewal file production step
					 */
					boolean isERenewalTrialParticipant = checkIsERenewalTrialParticipant(renewalClientNumber);
					
					if (isERenewalTrialParticipant) {
						printed = false;
					}

					// email count
					if (emailed) {
						emailCount++;
					}
					// print count
					if (printed) {
						printCount++;
					}

					if (!emailed && !printed) {
						noDeliveryMethod++;
					}

					LogUtil.debug(this.getClass(), "emailed=" + emailed);
					LogUtil.debug(this.getClass(), "printed=" + printed);
					LogUtil.debug(this.getClass(), "noDeliveryMethod=" + noDeliveryMethod);

					if (emailed) {
						try {
							LogUtil.debug(this.getClass(), "overrideEmail=" + overrideEmail);
							if (overrideEmail != null && overrideEmail.trim().length() > 0) {
								LogUtil.debug(this.getClass(), "overrideEmail=" + overrideEmail);
								LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);
								// check email address
								overrideEmail = StringUtil.validateEmail(overrideEmail, false);
								emailAddress = overrideEmail;
							} else {
								if (test) {
									throw new Exception("Override email address must be set if running a test.");
								}
							}
							LogUtil.debug(this.getClass(), "after override emailAddress=" + emailAddress);
							emailRenewal(renewalClientNumber, renewalNoticeType, emailAddress, update, test, printed, isERenewalTrialParticipant, discountString);
							if (test) {
								testCounter++;
							}
							if (update) {
								updateCount++;
							}
							LogUtil.debug(this.getClass(), "testCounter=" + testCounter);
							LogUtil.debug(this.getClass(), "updateCount=" + updateCount);

						} catch (Exception e) {
							LogUtil.fatal(this.getClass(), "Unable to email renewal for client " + renewalClientNumber + ": " + ExceptionHelper.getExceptionStackTrace(e));

							fileWriter.write(renewalClientNumber + "," + e.getMessage() + "\n");
						}
					}
				}
			}

			fileWriter.flush();
			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ReportException(e);
		}
		count.put(COUNT_RENEWAL_EMAIL_DOCUMENT_UPDATES, updateCount);
		count.put(COUNT_RENEWAL_TEST_EMAILS_SENT, testCounter);
		count.put(COUNT_RENEWAL_EMAIL, emailCount);
		count.put(COUNT_RENEWAL_PRINT, printCount);
		count.put(COUNT_RENEWAL_NO_DELIVERY_METHOD, noDeliveryMethod);
		count.put(COUNT_RENEWAL_PRIMARY_LINES, primaryLines);
		count.put(COUNT_RENEWAL_PRIMARY_LINES_VALID, validLines);

		LogUtil.log(this.getClass(), "emailRenewals end");

		return count;
	}
	
	/**
	 * eRenewal trial participant check
	 * @param renewalClientNumber
	 * @return 
	 */
	private boolean checkIsERenewalTrialParticipant(Integer renewalClientNumber) {
		Query isRenewalTrialParticipant = hsession.createSQLQuery("select 1 from dbo.erenewal_pilot where PersonID = " + renewalClientNumber);
		@SuppressWarnings("unchecked")
		List<Boolean> result = (List<Boolean>)isRenewalTrialParticipant.list();
		return !result.isEmpty();
	}

	public void emailRenewal(Integer renewalClientNumber, String emailAddress, String renewalNoticeType, boolean update, boolean test, boolean printed, boolean isERenewalTrialParticipant, String discountString) throws RemoteException, SystemException, ReportException, SQLException {

		LogUtil.log(this.getClass(), "emailRenewal start");

		LogUtil.debug(this.getClass(), "renewalClientNumber=" + renewalClientNumber);
		LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);
		LogUtil.debug(this.getClass(), "update=" + update);
		LogUtil.debug(this.getClass(), "test=" + test);

		// get membership
		MembershipVO membership = membershipMgrLocal.findMembershipByClientAndType(renewalClientNumber, MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
		LogUtil.debug(this.getClass(), "membership=" + membership);

		// get the document
		MembershipDocument doc = membershipMgrLocal.getCurrentDocument(membership.getMembershipID(), membership.getExpiryDate());
		LogUtil.debug(this.getClass(), "doc=" + doc);
		if (doc != null && doc.getEmailDate() != null) {
			throw new ReportException("Document already emailed " + DateUtil.formatMediumDate(doc.getEmailDate()) + ".");
		}

		// system user
		PaymentTransactionMgrLocal paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();

		User user = SecurityHelper.getRoadsideUser();
		LogUtil.debug(this.getClass(), "user=" + user);

		TransactionGroup renewalTransactionGroup = TransactionGroup.getRenewalTransactionGroup(membership, user, membership.expiryDate);
		LogUtil.debug(this.getClass(), "renewalTransactionGroup=" + renewalTransactionGroup);

		MembershipRenewalMgr renewalMgr = MembershipEJBHelper.getMembershipRenewalMgr();

		Properties renewalProperties = renewalMgr.produceIndividualRenewalNotice(membership, renewalTransactionGroup, user, paymentTransactionMgrLocal);
		LogUtil.debug(this.getClass(), "renewalProperties=" + renewalProperties);

		LogUtil.debug(this.getClass(), "user=" + user);
		String printGroup = user.getPrinterGroup();
		LogUtil.debug(this.getClass(), "printGroup=" + printGroup);

		/**
		 * This code retrieves the email message html from the renewals_html table in the database
		 * and serialises it for insertion in the email courtesy of the Scanner class.
		 * 
		 * HTML is stored as varchar(MAX)as it exceeds the standard varchar() 8000 bytes limit (approx 25K and 32K)
		 */
		boolean isDD = renewalProperties.keySet().contains("Direct Debit Schedule");
		boolean isLifestyle = renewalProperties.getProperty("prodName").equals("Lifestyle");
		boolean isLifestyleHalfPriceDiscount = discountString.equals("$19.50"); // half price discount for 2021 renewal of zero dollar 2020 lifestyle members - one off
		boolean isReminderNotice = renewalNoticeType.equals("Renewal Reminder");
		boolean isFinalNotice = renewalNoticeType.equals("Renewal Final Notice");
		
		String templateName = isDD?isLifestyle?"'lsrenewaldd'":"'renewaldd'":isLifestyle?isLifestyleHalfPriceDiscount?isReminderNotice?"'ls50pcreminderrct'":isFinalNotice?"'ls50pclastnoticerct'":"'lshprenewalrct'":"'lsrenewalrct'":"'renewalrct'";
		
		Query renewalHTMLQuery = hsession.createSQLQuery("select html from dbo.renewals_html where name = " + templateName);
		@SuppressWarnings("unchecked")
		List<Object[]> result = renewalHTMLQuery.list();
		Object varcharMaxHTML = result.get(0);
		SerializableClob clob = (SerializableClob)varcharMaxHTML;
		InputStream emailHTML = clob.getAsciiStream();
		Scanner scanner = new Scanner(emailHTML, "UTF-8").useDelimiter("\\A");
	        String emailMessage = scanner.hasNext() ? scanner.next() : "";
	        scanner.close();
		
		// Create the renewal advice pdf
	        ReportRequestBase rep = null;
		try {
			rep = new MembershipRenewalAdviceRequest(user, printGroup, renewalProperties, renewalTransactionGroup, true);
			LogUtil.debug(this.getClass(), "rep=" + rep);

			// report cache and time stamp
			String reportCache = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_REPORT_CACHE);
			rep.setDestinationFileName(reportCache + "/" + renewalClientNumber + "_" + Long.toString(new DateTime().getTime()) + "." + FileUtil.EXTENSION_PDF);
			rep.setRemoveFile(false);

		} catch (ReportException e) {
			throw new SystemException(e);
		}

		String fromName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILFROMNAME"); // eg.
		String fromTitle = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILFROMTITLE"); // eg.
		String subject = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILSUBJECT");
		String reminderSubject = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALREMINDEREMAILSUBJECT");
		String finalNoticeSubject = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALFINALNOTICEEMAILSUBJECT");
		String attachmentName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILATTACHMENTNAME");
		String returnEmail = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILRETURNPATH");

		// Replace Roadside with Lifestyle for that prodcut type
		if (!membership.productCode.equals("Roadside")) {
			subject = subject.replace("Roadside", membership.productCode);
			attachmentName = attachmentName.replace("Roadside", membership.productCode);
		}
		
		ClientVO client = membership.getClient();
		String firstName = client.getFirstNameOnly().toLowerCase();
		firstName = StringUtil.toProperCase(firstName, true);
		
		String addressTitle = ClientHelper.formatCustomerName(client.getTitle(), client.getGivenNames(), client.getSurname(), false, true, true, false);
		LogUtil.log(this.getClass(), "addressTitle=" + addressTitle);

		Hashtable<String, String> var = new Hashtable<String, String>();
		var.put("addressTitle", addressTitle);
		var.put("fromName", fromName);
		var.put("fromTitle", fromTitle);
		var.put("clientNumber", renewalClientNumber.toString());

		boolean isGroup = false;
		if (renewalTransactionGroup.isMembershipGroupTransaction()) isGroup = true;
		
		boolean under10 = true; // Placeholder for rewards code

		// Replace text placeholders with transaction values
		String body = emailMessage.replace("[member first name]", firstName)
			.replace("[invoice amount]", renewalProperties.getProperty("amount"))
			.replace("[invoice due date]", renewalProperties.getProperty("payBy"))
			.replace("[payment reference number]", renewalProperties.getProperty("ivrNumber"))
			.replace("[dd renewal date]", renewalProperties.getProperty("renewalDate"))
			.replace("[renewal_date]", renewalProperties.getProperty("renewalDate"))
			.replace("[member reward]", "0.00");
		
		// If group switch text to be group options, otherwise individual
		if (isGroup) {
		    body = body.replace("[your group has][you've]", "your group has ").replace("[their cards][your card]", "their cards ").replace("[group ]","group ").replace("[s]", "s");
		    body = body.replace("[that group members can use their cards][to use your card]", "that group members can use their cards").replace("[r group]", "r group").replace("[made][you make]", "made");
		} else {
		    body = body.replace("[your group has][you've]", "you've ").replace("[their cards][your card]", "your card ").replace("[group ]","").replace("[s]", "");
		    body = body.replace("[that group members can use their cards][to use your card]", "to use your card").replace("[r group]", "").replace("[made][you make]", "you make");
		}
		
		// If the recipient also gets a paper version show the paperless rows
		if (printed) {
		    body = body.replace("[ nodisplay]", "");
		    body = body.replace("[ erenew_nodisplay]", " style=\"display:none;\"");
		} else {
		    body = body.replace("[ nodisplay]", " style=\"display:none;\"");
		    
		    if(isERenewalTrialParticipant) {
			    body = body.replace("[ erenew_nodisplay]", "");
		    } else {
			    body = body.replace("[ erenew_nodisplay]", " style=\"display:none;\"");
		    }
		}
		
		// Toggle rewards display options depending on the amount returned.
		if (under10) {
		    body = body.replace("[ norewards]", " style=\"display:none;\"");
		    body = body.replace("[ over10]", " style=\"display:none;\"");
		    body = body.replace("[ under10]", "");
		} else {
		    body = body.replace("[ norewards]", "");
		    body = body.replace("[ over10]", "");
		    body = body.replace("[ under10]", " style=\"display:none;\"");
		}
		
		if (isLifestyleHalfPriceDiscount) {
		    body = body.replace("[ half price ]", " half price ");
		    body = body.replace("[ls50_style ]", "");
		} else {
		    body = body.replace("[ half price ]", "");
		    body = body.replace("[ls50_style ]", "style=\"display:none;\" ");
		}
		
		attachmentName = attachmentName + "." + FileUtil.EXTENSION_PDF;
		rep.setReportName(attachmentName);

		if (test) {
		    subject = subject += " - TEST ONLY";
		}

		// Create the mail message and attached the massaged HTML body
		MailMessage message = new MailMessage();
		message.setRecipient(emailAddress);
//		message.setBcc("g.sharp@ract.com.au");
		message.setMessage(body);
		message.setSubject(isReminderNotice?reminderSubject:isFinalNotice?finalNoticeSubject:subject);
		message.setHtml(true);
		message.setFrom(returnEmail);
		
		// Send email with report attachment
		ReportUtil.emailReport(rep, false, message);

		// update document
		if (update) {
			// update document with email address and date/time of email
			doc.setEmailAddress(emailAddress);
			doc.setEmailDate(new DateTime());

			UserTransaction userTx = null;
			try {
				userTx = sessionContext.getUserTransaction();
				userTx.begin();

				hsession.merge(doc);

				userTx.commit();
			} catch (Exception e) {
				try {
					userTx.rollback();
				} catch (Exception e1) {
					LogUtil.warn(this.getClass(), "Unable to rollback document changes. " + e1.getMessage());
				}
			}
		}
		LogUtil.log(this.getClass(), "emailRenewal end");
	}

	public final int COUNT_PRIMARY_MERGE_LINE = 33;

	public Hashtable<String, String> mergeRenewalFile(File marketingFile, File renewalFile, boolean defaultMarketing) throws RemoteException {

		Hashtable<String, String> mergeFile = new Hashtable<String, String>();

		final String COUNT_DOC_TYPE = "Document type count";
		final String COUNT_INSERT_TYPE = "Insert type count";
		final String COUNT_MESSAGE_TYPE = "Message type count";

		final String DEFAULT_OTHER_CODE = "0";
		final String DEFAULT_NOTICE_ADVICE_CODE = "1";
		final String DEFAULT_MSG = "0";

		StringBuffer fileContents = new StringBuffer();
		// eg. doc type
		Hashtable<String, Hashtable<String, Integer>> fileCounts = new Hashtable<String, Hashtable<String, Integer>>();

		BufferedReader br = null;
		BufferedReader mr = null;
		int recordCount = 0;

		Hashtable<Integer, String[]> marketingEntries = new Hashtable<Integer, String[]>();

		String messageCode = null;
		String insertCode = null;

		// load marketing file into memory for faster processing
		if (!defaultMarketing) {
			try {

				Integer marketingClientNumber = null;
				String marketingElements[] = null;
				String marketingLine = null;
				String insertMarketingFields[] = null;
				// open and search marketing file
				mr = new BufferedReader(new InputStreamReader(new FileInputStream(marketingFile)));
				while ((marketingLine = mr.readLine()) != null) {
					if (marketingLine.length() > 0) {
						marketingElements = marketingLine.split(FileUtil.SEPARATOR_COMMA, -1);
						LogUtil.debug(this.getClass(), "marketingLine=" + marketingLine + " " + marketingElements.length);
						marketingClientNumber = new Integer(marketingElements[0]);
						insertCode = marketingElements[1];
						messageCode = marketingElements[2];
						// immutable - trap for young players
						insertMarketingFields = new String[2];
						insertMarketingFields[0] = insertCode;
						insertMarketingFields[1] = messageCode;
						marketingEntries.put(marketingClientNumber, insertMarketingFields);
						LogUtil.debug(this.getClass(), "marketingEntries=" + insertMarketingFields[0] + " " + insertMarketingFields[1]);

					}
				}
				mr.close();
			} catch (IOException ex1) {
				throw new RemoteException("", ex1);
			}
		}

		LogUtil.debug(this.getClass(), "marketingEntries=" + marketingEntries);

		try {
			String line = null;
			String newLine = "";
			Integer renewalClientNumber = null;
			String elements[] = null;
			String docType = "";
			String productType = "";
			int matches = 0;
			String entries[] = null;

			br = new BufferedReader(new InputStreamReader(new FileInputStream(renewalFile)));
			while ((line = br.readLine()) != null) {
				// reset new line variable
				newLine = "";
				newLine += line;

				recordCount++;
				matches = 0;

				// for each line in the renewal file
				if (line.startsWith(LINE_TYPE_PRIMARY)) {

					elements = line.split("[" + FileUtil.SEPARATOR_PIPE + "]", -1);
					LogUtil.log(this.getClass(), recordCount + ": elements=" + elements.length);

					if (elements.length != COUNT_PRIMARY_MERGE_LINE) {
						throw new RemoteException("Line " + recordCount + " (" + elements.length + ") does not contain the required number of elements (" + COUNT_PRIMARY_MERGE_LINE + ")");
					}

					renewalClientNumber = new Integer(elements[1]);
					docType = elements[2];
					productType = elements[13];
					countMergeFileValues(COUNT_DOC_TYPE, fileCounts, docType + FileUtil.TAB + productType);

					// check if current client has any marketing offers
					if (!defaultMarketing) {
						entries = marketingEntries.get(renewalClientNumber);

						if (entries != null) {
							LogUtil.debug(this.getClass(), "entries=" + entries[0] + " " + entries[1]);
							matches++;

							insertCode = entries[0];
							// insert the insert code
							if (!insertCode.equals("")) {
								countMergeFileValues(COUNT_INSERT_TYPE, fileCounts, insertCode);
								newLine += FileUtil.SEPARATOR_PIPE + insertCode;
							} else {
								newLine += FileUtil.SEPARATOR_PIPE;
							}

							messageCode = entries[1];
							// insert the message code
							if (!messageCode.equals("")) {
								countMergeFileValues(COUNT_MESSAGE_TYPE, fileCounts, messageCode);
								newLine += FileUtil.SEPARATOR_PIPE + messageCode;
							} else {
								newLine += FileUtil.SEPARATOR_PIPE;
							}

						}
					} else { // default marketing
						matches++;
						if (docType.equals(MembershipDocument.RENEWAL_NOTICE) || docType.equals(MembershipDocument.RENEWAL_ADVICE)) {
							insertCode = DEFAULT_NOTICE_ADVICE_CODE;
							countMergeFileValues(COUNT_INSERT_TYPE, fileCounts, insertCode);
							newLine += FileUtil.SEPARATOR_PIPE + insertCode + FileUtil.SEPARATOR_PIPE + DEFAULT_MSG;
						} else {
							insertCode = DEFAULT_OTHER_CODE;
							countMergeFileValues(COUNT_INSERT_TYPE, fileCounts, insertCode);
							newLine += FileUtil.SEPARATOR_PIPE + insertCode + FileUtil.SEPARATOR_PIPE + DEFAULT_MSG;
						}
					}

					// if not in the file
					if (matches == 0) {
						// if no matches in file just add two delimiters to
						// denote empty fields
						newLine += FileUtil.SEPARATOR_PIPE + FileUtil.SEPARATOR_PIPE;
					}

				}

				fileContents.append(newLine + FileUtil.NEW_LINE); // new line???

			}
			br.close();
		} catch (IOException ex) {
			throw new RemoteException("Unable to process renewal file.", ex);
		}

		LogUtil.debug(this.getClass(), "fileContents=" + fileContents.toString());
		LogUtil.debug(this.getClass(), "fileCounts=" + fileCounts);
		LogUtil.debug(this.getClass(), "recordCount=" + recordCount);

		// format counts for display

		StringBuffer formattedFileCounts = new StringBuffer();
		Collection<String> keys = fileCounts.keySet();
		String fileKey = null;
		String attrKey = null;
		Hashtable vals = null;
		for (Iterator<String> i = keys.iterator(); i.hasNext();) {
			fileKey = i.next().toString();
			// heading
			formattedFileCounts.append(FileUtil.NEW_LINE + StringUtil.toProperCase(fileKey) + FileUtil.NEW_LINE);

			vals = fileCounts.get(fileKey);
			Collection attrKeys = vals.keySet();
			for (Iterator j = attrKeys.iterator(); j.hasNext();) {
				attrKey = j.next().toString();
				formattedFileCounts.append(attrKey + FileUtil.TAB + vals.get(attrKey).toString() + FileUtil.NEW_LINE);
			}
		}

		String reportCache = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_REPORT_CACHE);

		File mergedFile = null;
		File mergedFileCounts = null;
		try {
			mergedFile = FileUtil.writeFile(reportCache + "/" + "mergedFile.txt", fileContents.toString());
			mergedFileCounts = FileUtil.writeFile(reportCache + "/" + "fileCounts.txt", formattedFileCounts.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mergeFile.put(MERGE_FILE + ".txt", mergedFile.getAbsolutePath());
		mergeFile.put(MERGE_FILE_COUNTS + ".txt", mergedFileCounts.getAbsolutePath());

		// return new file
		return mergeFile;
	}

	public static String MERGE_FILE = "Merge file";
	public static String MERGE_FILE_COUNTS = "Merge file counts";

	private void countMergeFileValues(String category, Hashtable<String, Hashtable<String, Integer>> fileCounts, String value) {

		Hashtable<String, Integer> attrCounts = fileCounts.get(category);
		if (attrCounts == null) {
			attrCounts = new Hashtable<String, Integer>();
			// add current hashtable
			fileCounts.put(category, attrCounts);
		}

		Integer count = attrCounts.get(value);
		if (count == null) {
			attrCounts.put(value, new Integer(1)); // intialise at 1
		} else {
			int newVal = count.intValue() + 1;
			attrCounts.put(value, new Integer(newVal));
		}

	}

	/**
	 * The membership renewal notices are printed by an external agency. If there is there is no amount payable, or the membership is to 
	 * be renewed automatically using direct debit, a renewal advice is sent in lieu. 
	 * 
	 * There are 4 notice types: . Renewal . Advice . Reminder . Final Notice 
	 * The notices (and advices) may be accompanied by one or more replacement membership cards. The notices with cards are output to a separate 
	 * file from those without.
	 */
	public int createMembershipRenewalNotices(DateTime effectiveDate, String runUserId) throws RemoteException, RenewalNoticeException {
		LogUtil.log(this.getClass(), "Membership renewal run commenced " + new DateTime());
		User user = SecurityHelper.getSystemUser();
		LogUtil.log(this.getClass(), "effectiveDate=" + effectiveDate);
		LogUtil.log(this.getClass(), "runUserId=" + runUserId);
		// fetch system parameters
		RenewalParameters renewalParameters = new RenewalParameters(effectiveDate, user, runUserId);
		try {
			renewalParameters.writeLogLine("\n\nRENEWAL RUN " + renewalParameters.getRunNumber() + " COMMENCED " + new DateTime());
		} catch (IOException ioe) {
			LogUtil.log(this.getClass(), "Unable to write to log file " + ioe);
		}
		createBatch(renewalParameters); // create mem_renewal_run record
		/*
		 * Create final notices first, then reminders, then renewals. In this way it will not be possible to create a reminder on a renewal which has just been done, or a final notice for a reminder which has just been created. Unfortunately this will turn the summary upside down.
		 */

		LogUtil.log(this.getClass(), "Starting final notice processing (" + new DateTime() + ")");
		doFinalNotices(renewalParameters);
		LogUtil.log(this.getClass(), "Final notices complete");
		LogUtil.log(this.getClass(), "Starting reminder notice processing (" + new DateTime() + ")");
		doReminderNotices(renewalParameters);
		LogUtil.log(this.getClass(), "Reminder notices complete");
		LogUtil.log(this.getClass(), "Starting renewal notice processing (" + new DateTime() + ")");
		doRenewalNotices(renewalParameters);
		LogUtil.log(this.getClass(), "Renewal notices complete");
		LogUtil.log(this.getClass(), "Starting old expiries processing (" + new DateTime() + ")");
		doFindOldExpiries(renewalParameters);
		LogUtil.log(this.getClass(), "Old expiries search complete");
		// now close files and email results to the specified address(es)
		updateRunToComplete(renewalParameters.getRunNumber());
		renewalParameters.completeLogFiles();
		renewalParameters.emailResults();
		LogUtil.log(this.getClass(), "Membership renewal notice run complete: " + new DateTime());
		return renewalParameters.getCountWithCard() + renewalParameters.getCountWithoutCard();
	}

	/**
	 * Lookup memberships which appear to have slipped through the renewal net. These have expiry dates before the first date of this run, but no matching renewal document. There should not be any, so these are memberships where something funny has happened. Write the list to the unrenewed file
	 */
	private void doFindOldExpiries(RenewalParameters renParms) throws RenewalNoticeException {
		try {
			renParms.writeLogLine("\nStart old expiries processing " + new DateTime().getSecondsFromMidnight());
		} catch (IOException ioe) {
			throw new RenewalNoticeException("Unable to write to error file: " + ioe);
		}
		String sql = "\nselect client_number, expiry_date, \"client-status\", allow_to_lapse " + "\n from pub.mem_membership mem, pub.\"cl-master\" cl " + "\n where mem.expiry_date < ? " // firstDate
				+ "\n and mem.client_number = cl.\"client-no\" " + "\n and mem.membership_status in('" + MembershipVO.STATUS_ACTIVE + "','" + MembershipVO.STATUS_ONHOLD + "')" + "\n and mem.product_code not in('" + ProductVO.PRODUCT_CMO + "')" + "\n and not exists(select * from pub.mem_document doc" + "\n where doc.membership_id = mem.membership_id" + "\n and (doc.document_status <> '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "' or doc.document_status is null)" + "\n and doc.mem_date = mem.expiry_date " + "\n and doc.document_type_code in ('" + MembershipDocument.RENEWAL_ADVICE + "','" + MembershipDocument.RENEWAL_NOTICE + "','Dummy Renewal Notice'))" // ??
				+ "\n and not exists (select * from PUB.mem_membership_group grp" + "\n                 where grp.membership_id = mem.membership_ID" + "\n                 and grp.is_prime_addressee = 0)";

		// System.out.println("\n-------------------------------------------------"
		// + "\nStarting old expiries run "
		// + sql
		// + "expiring before " + renParms.firstDate);
		PreparedStatement statement = null;
		Connection connection = null;
		int clientNumber;
		String clientStatus;
		boolean allowToLapse;
		java.sql.Date expiryDate = null;
		int count = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setDate(1, renParms.getFirstDate().toSQLDate());
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				count++;
				clientNumber = rs.getInt(1);
				expiryDate = rs.getDate(2);
				clientStatus = rs.getString(3);
				allowToLapse = rs.getBoolean(4);
				try {
					renParms.writeUnrenewedLine(new Integer(clientNumber), DOCUMENT_TYPE_UNKNOWN, "Missed client in previous run.", new DateTime(expiryDate), ClientHelper.formatClientStatus(clientStatus), Boolean.toString(allowToLapse));
				} catch (Exception e) {
					LogUtil.log(this.getClass(), "Can't write to unrenewed file.." + e);
				}
			}
			rs.close();
			try {
				renParms.writeLogLine("\nUnrenewed members = " + count);
				renParms.writeLogLine("\nOld expiries extract FINISHED at " + new DateTime());
			} catch (Exception e) {
				LogUtil.log(this.getClass(), "Unable to write to summary file");
			}
		} catch (Exception e) {
			try {
				renParms.writeLogLine("\nError in Unrenewed extraction: " + e);
			} catch (IOException ioe) {
				// ignore
			}
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	/**
	 * Lookup renewals with a current renewal document and expiry date before reminder date. For each of these create a reminder document record and write reminder to withoutCard file.
	 */
	private void doReminderNotices(RenewalParameters renParms) throws RenewalNoticeException {
		try {
			renParms.writeLogLine("\nStart reminder notices processing " + new DateTime().getSecondsFromMidnight());
		} catch (IOException ioe) {
			throw new RenewalNoticeException("Unable to write to error file: " + ioe);
		}
		String sql = "\nselect mem.membership_id, mem.client_number, mem.expiry_date, pending_fee_id,amount, doc.fee_effective_date" + "\n from pub.mem_membership mem" + "\n INNER JOIN pub.[cl-master] ON pub.[cl-master].[client-no] = mem.client_number" + "\n, pub.mem_document doc, pub.pt_pending_fee fee" + "\n where mem.membership_id = doc.membership_id" + "\n and pub.[cl-master].[client-status] <> '" + Client.STATUS_DECEASED + "'" + "\n and mem.expiry_date = doc.mem_date " + "\n and mem.membership_status in('" + MembershipVO.STATUS_ACTIVE + "','" + MembershipVO.STATUS_ONHOLD + "')" + "\n and mem.product_code not in('" + ProductVO.PRODUCT_CMO + "')" + "\n and doc.document_type_code = '" + MembershipDocument.RENEWAL_NOTICE + "'" // no
		// reminders
		// for
		// direct
		// debit
				+ "\n and (doc.document_status <> '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "' or doc.document_status is null)" + "\n and fee.source_system_ref = mem.membership_id" + "\n and fee.source_system = 'MEM' " + "\n and fee.seq_number = doc.document_id" + "\n and mem.expiry_date < ? " // reminder
				// date
				+ "\n and not exists(select * from mem_document doc2" + "\n where doc2.membership_id = mem.membership_id" + "\n and doc2.mem_date = mem.expiry_date" + "\n and (doc2.document_status <> '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "'" + "\n      or doc2.document_status is null" + "\n      or doc2.document_status = '')" + "\n and doc2.document_type_code = '" + MembershipDocument.RENEWAL_REMINDER + "')" + "\n and not exists (select * from PUB.mem_membership_group grp" + "\n                 where grp.membership_id = mem.membership_ID" + "\n                 and grp.is_prime_addressee = 0)";
		LogUtil.log(this.getClass(), "Reminder notice sql: " + sql);
		constructNotices(renParms, sql, MembershipDocument.RENEWAL_REMINDER, null, renParms.getReminderDate());
	}

	/**
	 * Look for memberships due for a final notice. That is, expired by whatever the period is, and having a current reminder notice.
	 */

	private void doFinalNotices(RenewalParameters renParms) throws RenewalNoticeException {
		try {
			renParms.writeLogLine("\nStart final notices processing at " + new DateTime().getSecondsFromMidnight());
		} catch (IOException ioe) {
			throw new RenewalNoticeException("Unable to write to error file.", ioe);
		}
		String sql = "\nselect mem.membership_id, mem.client_number, mem.expiry_date, pending_fee_id, amount, doc.fee_effective_date" + "\n from pub.mem_membership mem" + "\n INNER JOIN pub.[cl-master] ON pub.[cl-master].[client-no] = mem.client_number" + "\n, pub.mem_document doc, pub.pt_pending_fee fee" + "\n where mem.membership_id = doc.membership_id" + "\n and pub.[cl-master].[client-status] <> '" + Client.STATUS_DECEASED + "'" + "\n and mem.expiry_date = doc.mem_date" + "\n and doc.document_type_code = '" + MembershipDocument.RENEWAL_NOTICE + "'" + "\n and mem.membership_status in('" + MembershipVO.STATUS_ACTIVE + "','" + MembershipVO.STATUS_ONHOLD + "')" + "\n and mem.product_code not in('" + ProductVO.PRODUCT_CMO + "')" + "\n and (doc.document_status <> '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "' or doc.document_status is null)" + "\n and fee.source_system_ref = mem.membership_id" + "\n and fee.source_system = 'MEM' " + "\n and fee.seq_number = doc.document_id" + "\n and mem.expiry_date < ? " // final
																																																																																																																																																																																																																																																																	// notice
																																																																																																																																																																																																																																																																	// date
				+ "\n and exists(select * from pub.mem_document doc1" + "\n            where doc1.membership_id = mem.membership_id" + "\n            and   doc1.mem_date      = mem.expiry_date" + "\n            and  (doc1.document_status <> '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "' or doc1.document_status is null)" + "\n            and   doc1.document_type_code = '" + MembershipDocument.RENEWAL_REMINDER + "')" + "\n and not exists(select * from mem_document doc2" + "\n where doc2.membership_id = mem.membership_id" + "\n and doc2.mem_date = mem.expiry_date" + "\n and (doc2.document_status <> '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "' or doc2.document_status is null)" + "\n and doc2.document_type_code = '" + MembershipDocument.RENEWAL_FINAL_NOTICE + "')" + "\n and not exists (select * from PUB.mem_membership_group grp" + "\n                 where grp.membership_id = mem.membership_ID" + "\n                 and grp.is_prime_addressee = 0)";

		LogUtil.log(this.getClass(), "Final notice sql: " + sql);
		constructNotices(renParms, sql, MembershipDocument.RENEWAL_FINAL_NOTICE, null, renParms.getFinalNoticeDate());
	}

	public static final String DOCUMENT_TYPE_UNKNOWN = "Unknown";

	/**
	 * Create renewal, reminder and final notices using the sql string supplied
	 */
	private void constructNotices(RenewalParameters renParms, String sql, String docType, DateTime firstDate, DateTime lastDate) throws RenewalNoticeException {

		int adviceCount[] = { 0, 0, 0, 0 };
		int noticeCount[] = { 0, 0, 0, 0 };
		int cardCount[] = { 0, 0, 0, 0 };
		int emailCount[] = { 0, 0, 0, 0 };
		int printCount[] = { 0, 0, 0, 0 };
		double value[] = { 0, 0, 0, 0 };
		int prodNum = 0; // 0 = advantage, 1 = ultimate, 2 = lifestyle, 3 = other

		int documentId = 0;
		int noCardCount = 0;
		String outputText = "";

		Hashtable<String, Comparable> directDebitDetails = null;
		String directDebitAuthorityDetails = null;
		String directDebitScheduleDetails = null;

		Boolean memberNeedsNewCard;
		boolean directDebit = false;
		boolean noAmountPayable = false;
		StringBuffer ddText = null;
		StringBuffer paText = null;
		StringBuffer memText = null;
		MembershipVO memVO = null;
		long billPayNumber = 0;
		int groupId = 0;
		int clientNumber = 0;
		DateTime expiryDate = null;
		DateTime feeEffectiveDate = null; // get date from document
		int groupSize = 0;
		Hashtable<String, Serializable> groupDetails = null;
		ArrayList unfinancialGroupMemberList = new ArrayList();
		int pricingSegment = 0;
		UserTransaction userTransaction = null;
		PaymentTransactionMgrLocal paymentTransactionMgrLocal = null;

		PreparedStatement statement = null;
		Connection connection = null;
		TransactionGroup transactionGroup = null;
		double oldAmount = 0;
		User user = renParms.getUser();
		boolean emailAll = false;
		String targetDocType = docType;

		try {

			// anyone subscribed to roadside email renewals.
			String emailAllString = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "EMAILRENALL");
			LogUtil.log(this.getClass(), "emailAllString=" + emailAllString);
			emailAll = emailAllString.equals("Y");
			LogUtil.log(this.getClass(), "emailAll=" + emailAll);

			MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
			// get membership mgr at the start

			connection = dataSource.getConnection();
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
			userTransaction = sessionContext.getUserTransaction();
			statement = connection.prepareStatement(sql);
			if (firstDate == null) // reminder/final notice
			{
				// no first date
				statement.setDate(1, lastDate.toSQLDate());
			} else // notice/advice
			{
				statement.setDate(1, firstDate.toSQLDate());
				statement.setDate(2, lastDate.toSQLDate());
			}
			ResultSet resultSet = statement.executeQuery();

			// ***********************************************************************

			String renewalEffDateOption = null;
			try {
				renewalEffDateOption = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_RENEWAL_EFFECTIVE_DATE_OPTION);
			} catch (RemoteException re) {
				throw new RenewalNoticeException("Failed to get system parameter '" + SystemParameterVO.CATEGORY_MEMBERSHIP + "," + SystemParameterVO.MEM_RENEWAL_EFFECTIVE_DATE_OPTION + "'", re);
			}

			// ***********************************************************************

			while (resultSet.next()) {
				// reset docType to reapply tests
				docType = targetDocType;
				directDebitDetails = null;
				noAmountPayable = false;
				LogUtil.debug(this.getClass(), "1");

				LogUtil.debug(this.getClass(), "getting memVO");
				memVO = membershipMgrLocal.getMembership(new Integer(resultSet.getString("membership_id")));
				LogUtil.debug(this.getClass(), "got memVO");

				try {
					clientNumber = resultSet.getInt("client_number");
					expiryDate = new DateTime(resultSet.getDate("expiry_date"));
				} catch (Exception ee) {
					LogUtil.log(this.getClass(), "Unable to get clientNumber or expiry date");
					try {
						renParms.writeErrorLine(new Integer(clientNumber), docType, SourceSystem.MEMBERSHIP.getAbbreviation(), ee.getMessage());
					} catch (Exception e) {
						// ignore error
					}
					continue;
				}
				LogUtil.debug(this.getClass(), "2: " + clientNumber);
				LogUtil.debug(this.getClass(), "3: " + expiryDate);
				try {

					// ********************************advice/notices

					if (MembershipDocument.RENEWAL_ADVICE.equals(docType) || MembershipDocument.RENEWAL_NOTICE.equals(docType)) {

						feeEffectiveDate = MembershipHelper.calculateRenewalEffectiveDate(expiryDate, renParms.getRunDate());

					} else if (MembershipDocument.RENEWAL_REMINDER.equals(docType) || MembershipDocument.RENEWAL_FINAL_NOTICE.equals(docType)) {

						// *******************************finals/reminders
						oldAmount = resultSet.getDouble("amount");
						feeEffectiveDate = new DateTime(resultSet.getDate("fee_effective_date"));
					}
					// else ???

					LogUtil.debug(this.getClass(), "4");

					// ******************* oldAmount may be null

//					LogUtil.debug(this.getClass(), "getting memVO");
//					memVO = membershipMgrLocal.getMembership(new Integer(resultSet.getString("membership_id")));
//					LogUtil.debug(this.getClass(), "got memVO");

					LogUtil.debug(this.getClass(), "5");

					if (isProblemRenewal(memVO)) {
						renParms.writeUnrenewedLine(new Integer(clientNumber), docType, "Problem renewal. " + docType + " not created", memVO.getExpiryDate(), ClientHelper.formatClientStatus(memVO.getClient().getStatus()), memVO.getAllowToLapseReasonCode());
						continue;
					}
					transactionGroup = null;
					LogUtil.debug(this.getClass(), "constructing renewal transaction group");
					try {
						transactionGroup = TransactionGroup.getRenewalTransactionGroup(memVO, user, feeEffectiveDate);
					} catch (ValidationException e) {
						// LogUtil.log(this.getClass()," Transaction group not created - "
						// + clientNumber + " not eligible for renewal\n" + e);
						renParms.writeUnrenewedLine(new Integer(clientNumber), docType, "No " + docType + " created. " + e.getMessage(), memVO.getExpiryDate(), ClientHelper.formatClientStatus(memVO.getClient().getStatus()), memVO.getAllowToLapseReasonCode());
						continue;
					}
					LogUtil.debug(this.getClass(), "6");
					LogUtil.debug(this.getClass(), "got renewal transaction group");

					String productCode = memVO.getNextProductCode();
					if (productCode == null) {
						productCode = memVO.getProductCode();
					}
					prodNum = getArrayProductIndex(productCode);
					paText = new StringBuffer();
					paText.append(LINE_TYPE_PRIMARY);
					paText.append(DELIMITER);
					String memberNumber = memVO.getMembershipNumber(7);
					paText.append(memberNumber);
					paText.append(DELIMITER);
					LogUtil.debug(this.getClass(), "7: " + productCode);
					// figure out what kind of notice is appropriate
					// (RenewalNotice, RenewalAdvice)
					if (docType.equals(MembershipDocument.RENEWAL_NOTICE)) {
						if (memVO.getDirectDebitAuthorityID() != null) {
							// direct debit
							LogUtil.debug(this.getClass(), "memVO.getDirectDebitAuthorityID()=" + memVO.getDirectDebitAuthorityID());
							docType = MembershipDocument.RENEWAL_ADVICE;
							directDebit = true;
						} else {
							// staff, life, etc.
							if (!(transactionGroup.getAmountPayable() > 0)) {
								docType = MembershipDocument.RENEWAL_ADVICE;
								// zero fee
								noAmountPayable = true;
							} else {
								// amount payable
								docType = MembershipDocument.RENEWAL_NOTICE;
							}
							directDebit = false;
						}
					} else {
						// only applicable for renewal notices
						directDebit = false;
					}

					LogUtil.debug(this.getClass(), "8: " + docType);
					LogUtil.debug(this.getClass(), "9: " + directDebit);

					LogUtil.debug(this.getClass(), ">>>>>>>>>>after notice check<<<<<<<<<<");
					paText.append(docType);
					LogUtil.log(this.getClass(), "memberNumber=" + memberNumber + ", docType=" + docType);
					paText.append(DELIMITER);
					addMemberDetails(memVO, paText);

					groupSize = getPricingOption(memVO, transactionGroup);
					// check that each member in the group can be renewed ie not
					// deceased or address unknown
					pricingSegment = groupSize;

					if (groupSize == 0) {
						pricingSegment = 1;
					} else if (groupSize > MembershipHelper.SEGMENT_COUNT) {
						pricingSegment = MembershipHelper.SEGMENT_COUNT;
					}

					paText.append(MembershipHelper.SEGMENT_TEXT[pricingSegment - 1]);
					paText.append(DELIMITER);
					paText.append(memVO.getExpiryDate().formatShortDate());
					paText.append(DELIMITER);

					Interval defaultMembershipTerm = null;
					try {
						defaultMembershipTerm = new Interval(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DEFAULT_MEMBERSHIP_TERM));
					} catch (ParseException e1) {
						throw new SystemException("Unable to construct default membership term.", e1);
					}

					paText.append(memVO.getExpiryDate().add(defaultMembershipTerm));
					paText.append(DELIMITER);
					addCostings(transactionGroup, paText);
					LogUtil.debug(this.getClass(), ">>>>>>>>>>b4 fee check<<<<<<<<<<");
					// *******************************finals/reminders
					if (MembershipDocument.RENEWAL_REMINDER.equals(docType) || MembershipDocument.RENEWAL_FINAL_NOTICE.equals(docType)) {

						if (transactionGroup.getAmountPayable() != oldAmount) {
							try {
								renParms.writeErrorLine(new Integer(clientNumber), docType, SourceSystem.MEMBERSHIP.getAbbreviation(), "Amount payable has changed for client. " + "Unable to create document " + docType + ". " + "(" + oldAmount + "/" + transactionGroup.getAmountPayable() + ")");
							} catch (IOException ioe) {
								LogUtil.log(this.getClass(), "Error writing to error file");
							}
							continue;
						}
					}
					// *******************************finals/reminders

				} // create the document
				catch (Exception ex) {
					LogUtil.warn(this.getClass(), "Error constructing notices for Client number " + clientNumber);
					LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(ex));
					renParms.writeErrorLine(new Integer(clientNumber), docType == null ? DOCUMENT_TYPE_UNKNOWN : docType, SourceSystem.MEMBERSHIP.getAbbreviation(), ex.getMessage());
					continue;
				}

				boolean printed = true;
				boolean emailed = false;
				String emailAddress = null;
				ClientVO client = memVO.getClient();

				LogUtil.debug(this.getClass(), "10");

				try {
					userTransaction.begin();

					LogUtil.debug(this.getClass(), "11: " + userTransaction.getStatus());

					// need to get one per loop
					paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();

					DateTime transactionDate = new DateTime().getDateOnly();
					Integer documentID = new Integer(idMgr.getNextDocumentID());

					// setup docvo and then save it
					int groupMemberCount = 1;
					if (groupSize > 0) {
						groupMemberCount = groupSize;
					}

					LogUtil.debug(this.getClass(), "12: " + userTransaction.getStatus());

					// *******************************finals/reminders START
					if (MembershipDocument.RENEWAL_REMINDER.equals(docType) || MembershipDocument.RENEWAL_FINAL_NOTICE.equals(docType)) {
						LogUtil.debug(this.getClass(), ">>>>>>>>>>THIS IS reminder/final<<<<<<<<<<");
						paText.append(resultSet.getString("pending_fee_id"));
						paText.append(DELIMITER);
						// direct debit paragraph
						paText.append(DELIMITER);
					} else if (docType.equals(MembershipDocument.RENEWAL_NOTICE)) {
						LogUtil.debug(this.getClass(), ">>>>>>>>>>THIS IS create pending<<<<<<<<<<");
						// create the pending fee if required
						String ivrNumber = "";

						// this is not direct debit
						String refNo = renParms.getIVRPrefix().toString();
						for (int x = documentID.toString().length(); x < 9; x++) {
							refNo += "0";
						}
						Interval feeEffectivePeriod = null;
						try {
							feeEffectivePeriod = new Interval(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_FEE_EFFECTIVE_PERIOD));
						} catch (ParseException e1) {
							throw new SystemException("Unable to construct fee effective period.", e1);
						}
						DateTime feeExpiryDate = expiryDate.add(feeEffectivePeriod);
						ivrNumber = BillpayHelper.createIvrNumber(documentID.intValue(), SourceSystem.MEMBERSHIP, false);

						paymentTransactionMgrLocal.createPendingFee(new Integer(clientNumber), ivrNumber, feeEffectiveDate, feeExpiryDate, SourceSystem.MEMBERSHIP.getAbbreviation(), memVO.getMembershipID(), renParms.getUser().getSalesBranchCode(), memVO.getNextProductCode() != null ? memVO.getNextProductCode() : memVO.getProductCode(), new Long(refNo), new Long(documentID.toString()), memVO.getMembershipNumber(), transactionGroup.getAmountPayable(), renParms.getUser().getUserID(), PaymentMgr.MEM_AUTO_RENEWAL);
						paText.append(ivrNumber);
						// only print billpay number if not direct debit
						paText.append(DELIMITER);
						// direct debit data if required
						paText.append(DELIMITER);
					} else if (directDebit) // ie. renewal advice
					{
						LogUtil.debug(this.getClass(), ">>>>>>>>>>THIS IS DD<<<<<<<<<<");
						// this is direct debit
						directDebitDetails = constructDirectDebitDetails(paymentTransactionMgrLocal, memVO, transactionGroup, FORMAT_EXTRACT); // paText

						DirectDebitSchedule ddSchedule = (DirectDebitSchedule) directDebitDetails.get(KEY_DD_SCHEDULE);
						// set on transaction group so it can be saved as XML.
						transactionGroup.setDirectDebitScheduleId(ddSchedule.getDirectDebitScheduleID());

						// non IVR payment number
						paText.append("");

						paText.append(DELIMITER);
						// add dd paragraph
						paText.append(DIRECT_DEBIT_PARAGRAPH);
						paText.append(DELIMITER);

					} else {
						LogUtil.debug(this.getClass(), ">>>>>>>>>>THIS IS freebie<<<<<<<<<<");
						// this is a freebie advice
						paText.append("");
						paText.append(DELIMITER);
						paText.append("");
						paText.append(DELIMITER);
					}

					LogUtil.debug(this.getClass(), "13: " + userTransaction.getStatus());

					// else blank
					// *******************************finals/reminders END

					Integer memGroupId = transactionGroup.getMembershipGroupID();
					if (memGroupId != null) {
						groupId = memGroupId.intValue();
					} else {
						groupId = 0;
					}

					LogUtil.log(this.getClass(), "adding group details");

					groupDetails = constructGroupDetails(transactionGroup, groupId, memVO, docType);

					LogUtil.debug(this.getClass(), "constructNotices groupDetails=" + groupDetails);

					/**
					 * @todo review rules for cards
					 */

					// requires a new card?
					memberNeedsNewCard = (Boolean) groupDetails.get(KEY_REQUIRES_NEW_CARD);

					// final notice only
					if (MembershipDocument.RENEWAL_FINAL_NOTICE.equals(docType)) {
						unfinancialGroupMemberList.addAll((ArrayList) groupDetails.get(KEY_UNFINANCIAL_GROUP_MEMBERS));
					}

					LogUtil.debug(this.getClass(), "emailAll=" + emailAll);
					LogUtil.debug(this.getClass(), "printed=" + printed);

					// only valid to email renewal advices and notices for non Lifestyle members but valid for all Lifestyle notices
					if (docType.equals(MembershipDocument.RENEWAL_ADVICE) || docType.equals(MembershipDocument.RENEWAL_NOTICE) ) {
//							|| (docType.equals(MembershipDocument.RENEWAL_REMINDER) || docType.equals(MembershipDocument.RENEWAL_FINAL_NOTICE) 
//							&& memVO.getProductCode().equals("Lifestyle") 
//							&& firstDate.onOrAfterDay(new DateTime("2021-06-01")) 
//							&& lastDate.onOrBeforeDay(new DateTime("2021-09-30")))) {
						ClientSubscription sub = clientMgr.getClientSubscription(memVO.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL);
						if (emailAll) {
							// If sub is inactive (opted out) disable emailing
							// If sub is active enable email and disable printing
							// in all other cases send an email - will also be printed
							if (sub != null && !sub.isActive()) {
								emailed = false;
								LogUtil.debug(this.getClass(), "client " + memVO.getClientNumber() + " has explicitly opted out, overridding emailAll.");
							} else if (sub != null && sub.isActive()) {
								emailed = true;
								printed = false;
							} else { // no preference, so email
								emailed = true;
							}
						} else {
							// do not email if a client has explicitly opted out
							if (sub != null) {
								if (sub.isActive()) { // opted in
									emailed = true;
									printed = false;
								} else { // opted out
									emailed = false;
									printed = true;
								}
							} else // no preference
							{
								List<MembershipDocument> emailedDocs = membershipMgrLocal.getERenewalDocumentList(memVO.getMembershipID());
								if (emailedDocs.isEmpty()) { // has never
									// received an
									// eRenewal
									LogUtil.debug(this.getClass(), "*** c/n " + memVO.getClientNumber() + " is candidate for once off eRenewal");
									emailed = true;
								} else { // has had the opportunity to opt in
									emailed = false;
								}
								printed = true;
							}
						}
					} else {
						emailed = false;
						printed = true;
					}

					if (emailed) {
						// is email valid?
						try {
							LogUtil.debug(this.getClass(), "emailAddress=" + client.getEmailAddress());
							emailAddress = StringUtil.validateEmail(client.getEmailAddress(), false);
							if (emailAddress == null || emailAddress.trim().length() == 0) {
								throw new GenericException("No email address has been specified.");
							}
							emailed = true;
						} catch (GenericException e) {
							renParms.writeErrorLine(clientNumber, docType, SourceSystem.CLIENT.getAbbreviation(), e.getMessage());
							// write to a file?
							emailed = false;
						}
					}

					// default to printed
					if (!emailed && !printed) {
						printed = true;
					}

					LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);

					LogUtil.debug(this.getClass(), "emailed=" + emailed);
					LogUtil.debug(this.getClass(), "printed=" + printed);
					LogUtil.debug(this.getClass(), "emailCount=" + emailCount);
					LogUtil.debug(this.getClass(), "printCount=" + printCount);

					if (emailed) {
						emailCount[prodNum]++;
					}
					if (printed) {
						printCount[prodNum]++;
					}

					// save document record to the database
					LogUtil.debug(this.getClass(), "14");
					try {
						LogUtil.debug(this.getClass(), "15: " + hsession.getSessionFactory());

						MembershipDocument memDocVO = new MembershipDocument(documentID, docType, transactionDate, memVO.getMembershipID(), null, renParms.getRunNumber(), memVO.getExpiryDate(), feeEffectiveDate, new BigDecimal(transactionGroup.getAmountPayable()), memVO.getNextProductCode() != null ? memVO.getNextProductCode() : memVO.getProductCode(), new Integer(groupSize), new BigDecimal(transactionGroup.getTotalDiscount()), memVO.getMembershipProfileCode(), printed, emailed, emailAddress, null, transactionGroup.toXML());
						hsession.save(memDocVO);
						LogUtil.debug(this.getClass(), "16");
					} catch (Exception ex1) {
						throw new SystemException(ex1);
					}

					LogUtil.debug(this.getClass(), "17: " + userTransaction.getStatus());

					// include no amount payable in counts/value
					if (docType.equals(MembershipDocument.RENEWAL_ADVICE)) {
						adviceCount[prodNum]++;
					} else {
						noticeCount[prodNum]++; // all other notices
					}
					value[prodNum] += transactionGroup.getAmountPayable();

					LogUtil.debug(this.getClass(), "18");

					// Commit the transaction
					userTransaction.commit();

					LogUtil.debug(this.getClass(), "19");

					// will only return to the pool not disconnect
					paymentTransactionMgrLocal.commit();
					LogUtil.debug(this.getClass(), "20");
				} catch (Exception e) {
					LogUtil.log(this.getClass(), "Renewal exception=" + ExceptionHelper.getExceptionStackTrace(e));
					// double check
					e.printStackTrace();
					LogUtil.log(this.getClass(), "connection=" + connection);
					LogUtil.log(this.getClass(), "userTx=" + userTransaction);
					LogUtil.log(this.getClass(), "paymentTxMgr=" + paymentTransactionMgrLocal);
					LogUtil.log(this.getClass(), "session=" + hsession);
					try {
						if (paymentTransactionMgrLocal != null) {
							paymentTransactionMgrLocal.rollback();
						}
					} catch (RemoteException re) {
						LogUtil.log(this.getClass(), "Unable to release adapters: " + re);
					}

					rollback(userTransaction);
					try {
						renParms.writeErrorLine(new Integer(clientNumber), docType, SourceSystem.MEMBERSHIP.getAbbreviation(), e.getMessage());
					} catch (IOException ioe) {
						LogUtil.log(this.getClass(), "unable to write error line for renewal exception, refer to system log for details: " + ioe);
					}
					continue;
				}

				// suppress output to the file if there is no amount payable
				if (!noAmountPayable) {

					// card details
					LogUtil.debug(this.getClass(), "client=" + memVO.getClientNumber());
					LogUtil.debug(this.getClass(), "memberNeedsNewCard=" + memberNeedsNewCard);
					try {
						if (memberNeedsNewCard.booleanValue()) {
							paText.append("TRUE");
							cardCount[prodNum]++;

						} else {
							paText.append("FALSE");
						}

						renParms.incrementCountWithoutCard();

						LogUtil.debug(this.getClass(), "Adding " + clientNumber + " to the renewal list.");

						paText.append(DELIMITER);

						LogUtil.debug(this.getClass(), "paText=" + paText + " " + (paText.toString().split(DELIMITER)).length);

						// add prime addressee data to file
						renParms.writeWithoutCardLine(paText.toString());

						// add direct debit authority details if they exist
						if (directDebitDetails != null) {
							directDebitAuthorityDetails = (String) directDebitDetails.get(KEY_DD_AUTHORITY);
							if (directDebitAuthorityDetails != null) {
								renParms.writeWithoutCardLine(directDebitAuthorityDetails); // new
								// line
								// on
								// end
							}
						} else {
							// write dummy details to make lines consistent
							StringBuffer ddFields = new StringBuffer();
							// branch name/ card type, card number
							ddFields.append(DELIMITER);
							// bsb, account number, account name/expiry date, cc
							// account name
							ddFields.append(DELIMITER);
							// paymentFrequency
							ddFields.append(DELIMITER);
							renParms.writeWithoutCardLine(ddFields.toString());
						}

						// end of primary line type
						String deliveryPreferences = getDeliveryPreferences(printed, emailed, emailAddress);

						renParms.writeWithoutCardLine(deliveryPreferences);

						// add new line at end of current line
						renParms.writeWithoutCardLine(FileUtil.NEW_LINE);

						ArrayList groupData = (ArrayList) groupDetails.get(KEY_GROUP_DATA_LIST);

						// add group data to file
						for (int x = 0; x < groupData.size(); x++) {
							renParms.writeWithoutCardLine((String) groupData.get(x) + FileUtil.NEW_LINE);
						}

						if (directDebitDetails != null) {
							// add direct debit schedule details if they exist
							directDebitScheduleDetails = (String) directDebitDetails.get(KEY_DD_SCHEDULE_SUMMARY);
							if (directDebitScheduleDetails != null) {
								renParms.writeWithoutCardLine(directDebitScheduleDetails);
							}
						}

					} catch (IOException e) {
						LogUtil.log(this.getClass(), "Error writing data to file for client " + clientNumber + ", however records have probably been created");
						// e.printStackTrace(pw);
						throw new RenewalNoticeException("Unable to write to file: " + e.getMessage());
					}
				} else {
					// no amount payable
				}

			}
			resultSet.close();

			// process unfinancial members for final notices only
			if (docType.equals(MembershipDocument.RENEWAL_FINAL_NOTICE)) {
				constructUnfinancialGroupMemberAdvices(renParms, unfinancialGroupMemberList);
			}

		} catch (SQLException e) {
			throw new EJBException("Error executing SQL " + sql + ".", e);
		} catch (RemoteException re) {
			throw new EJBException("Generic Error.", re);
		} catch (IOException ioe) {
			// this should never happen
			throw new EJBException("Error writing to files.", ioe);
		} finally {
			// close open resources
			ConnectionUtil.closeStatement(statement);
			ConnectionUtil.closeConnection(connection);
		}
		// write out summary information
		try {
			LogUtil.log(this.getClass(), "summary lines");
			renParms.writeSummaryLine("", ProductVO.PRODUCT_ADVANTAGE, ProductVO.PRODUCT_ULTIMATE, ProductVO.PRODUCT_LIFESTYLE, "Other");
			renParms.writeSummaryLine(targetDocType, noticeCount[0], noticeCount[1], noticeCount[2], noticeCount[3]);
			LogUtil.debug(this.getClass(), "adviceCount");
			// if advice count is not empty
			if (!isCountArrayEmpty(adviceCount)) {
				// add it to summary file
				renParms.writeSummaryLine(MembershipDocument.RENEWAL_ADVICE, adviceCount[0], adviceCount[1], adviceCount[2], adviceCount[3]);
			}
			LogUtil.debug(this.getClass(), "cardCount");
			// if card count is not empty
			if (!isCountArrayEmpty(cardCount)) {
				// add it to the summary file
				renParms.writeSummaryLine("Card Requests", cardCount[0], cardCount[1], cardCount[2], cardCount[3]);
			}
			LogUtil.debug(this.getClass(), "printCount");
			// if print count is not empty
			if (!isCountArrayEmpty(printCount)) {
				LogUtil.debug(this.getClass(), "printCount 2");
				// add it to the summary file
				renParms.writeSummaryLine(LABEL_PRINTED, printCount[0], printCount[1], printCount[2], printCount[3]);
			}
			LogUtil.debug(this.getClass(), "emailCount");
			// if print count is not empty
			if (!isCountArrayEmpty(emailCount)) {
				LogUtil.debug(this.getClass(), "emailCount 2");
				// add it to the summary file
				renParms.writeSummaryLine(LABEL_EMAILED, emailCount[0], emailCount[1], emailCount[2], emailCount[3]);
			}
			renParms.writeSummaryLine("Value", value[0], value[1], value[2], value[3]);
			renParms.writeLogLine(FileUtil.NEW_LINE + "Records processed = " + (noticeCount[0] + noticeCount[1] + noticeCount[2]));
			renParms.writeLogLine(FileUtil.NEW_LINE + "Finished " + docType + " processing at " + new DateTime().getSecondsFromMidnight());
		} catch (IOException ioe) {
			LogUtil.log(this.getClass(), "Error writing summary data. " + ioe);
		}
	}

	private String getDeliveryPreferences(boolean printed, boolean emailed, String emailAddress) throws RemoteException {
		StringBuffer deliveryPreferences = new StringBuffer();
		deliveryPreferences.append(printed ? "Y" : "N");
		deliveryPreferences.append(DELIMITER);
		deliveryPreferences.append(emailed ? "Y" : "N");
		deliveryPreferences.append(DELIMITER);
		deliveryPreferences.append(emailAddress != null ? emailAddress : "");
		deliveryPreferences.append(DELIMITER);
		return deliveryPreferences.toString();
	}

	private boolean isCountArrayEmpty(int array[]) {
		boolean empty = true;
		for (int i = 0; i < array.length && empty; i++) {
			LogUtil.debug(this.getClass(), "i=" + array[i]);
			if (array[i] > 0) {
				empty = false;
			}
		}
		return empty;
	}

	private int getArrayProductIndex(String productCode) {
		int prodNum;
		if (productCode.equals(ProductVO.PRODUCT_ADVANTAGE)) {
			prodNum = 0;
		} else if (productCode.equals(ProductVO.PRODUCT_ULTIMATE)) {
			prodNum = 1;
		} else if (productCode.equals(ProductVO.PRODUCT_LIFESTYLE)) {
			prodNum = 2;
		} else // access
		{
			prodNum = 3;
		}
		return prodNum;
	}

	/*-----------------------end of makeNotices---------------------------------*/

	private final String LINE_TYPE_PRIMARY = "002";

	private final String LINE_TYPE_SECONDARY = "003";

	private static final String LINE_TYPE_DIRECT_DEBIT = "004";

	private static final String DIRECT_DEBIT_PARAGRAPH = "as per attached schedule.";

	private static final String AUS_PREFIX = "+61";

	public Collection<MembershipVO> getSMSRenewalReminderList(DateTime targetDate) throws RemoteException {
		// check last SMS reminder date
		DateTime today = new DateTime().getDateOnly();

		// get the date component only
		targetDate = targetDate.getDateOnly();

		LogUtil.debug(this.getClass(), "targetDate=" + targetDate);

		final String SQL_RENEWAL_REMINDERS = "SELECT  distinct   mm.membership_id " + "FROM mem_membership mm INNER JOIN " + "mem_document md ON mm.membership_id = md.membership_id INNER JOIN " + "[cl-master] ON mm.client_number = [client-no] LEFT OUTER JOIN " + "mem_membership_group mg ON mm.membership_id = mg.membership_id " + "WHERE (mm.allow_to_lapse = 0)  " + "AND (mm.product_code IN ('" + ProductVO.PRODUCT_ADVANTAGE + "', '" + ProductVO.PRODUCT_ULTIMATE + "', '" + ProductVO.PRODUCT_LIFESTYLE + "')) " + "AND ((mg.is_prime_addressee IS NULL) OR (mg.is_Prime_Addressee = 1)) " + "AND ([mobile-phone] IS NOT NULL AND [mobile-phone] <> '') " + "AND smsAllowed = ?  " + "AND (mm.payment_method_id IS NULL) AND (md.document_date > ?) AND (mm.expiry_date = ?) " +
		// no SMS reminder on the same day
		"AND NOT exists " + "( " + "SELECT 'x' " + "FROM mem_document md " + "WHERE md.membership_id = mm.membership_id " + "AND document_date >= ? " + "AND document_type_code = '" + MembershipDocument.RENEWAL_REMINDER_SMS + "' " + ") " + " ";
		LogUtil.debug(this.getClass(), "SQL_RENEWAL_REMINDERS=" + SQL_RENEWAL_REMINDERS);
		PreparedStatement statement = null;
		Connection connection = null;
		ArrayList<MembershipVO> memberList = new ArrayList<MembershipVO>();
		MembershipVO membershipVO = null;
		// pre-renewal period
		String preRenewalPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.IN_RENEWAL_PERIOD);
		Interval preRenewalPeriod = null;
		try {
			preRenewalPeriod = new Interval(preRenewalPeriodSpec);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		DateTime renewalPeriodStart = targetDate.subtract(preRenewalPeriod);
		LogUtil.debug(this.getClass(), "renewalPeriodStart=" + renewalPeriodStart);
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_RENEWAL_REMINDERS);
			statement.setBoolean(1, true); // sms allowed
			statement.setDate(2, renewalPeriodStart.toSQLDate()); // notice date
			statement.setDate(3, targetDate.toSQLDate()); // expiry date
			statement.setDate(4, today.toSQLDate()); // document date
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				membershipVO = membershipMgrLocal.getMembership(new Integer(rs.getString(1)));
				memberList.add(membershipVO);
			}
			rs.close();
		} catch (Exception e) {
			throw new RemoteException("Error extracting sms reminder list: " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		LogUtil.debug(this.getClass(), "memberList=" + memberList.size());
		return memberList;
	}

	/**
	 * Generate and send SMS renewal reminders.
	 * 
	 * @param renewalParameters RenewalParameters
	 * @param membershipList Collection
	 */
	public void constructSMSRenewalReminders(DateTime targetDate, Collection<MembershipVO> membershipList) throws RenewalNoticeException, IOException, RemoteException {

		// get eligible list
		if (membershipList == null && targetDate != null) {
			membershipList = getSMSRenewalReminderList(targetDate);
		}
		LogUtil.log(this.getClass(), "membershipList=" + membershipList.size());

		UserTransaction userTransaction = sessionContext.getUserTransaction();
		int reminderCount[] = { 0, 0, 0 };
		DateTime now = new DateTime().getDateOnly();

		LogUtil.log(this.getClass(), "constructSMSRenewalReminders=" + now);

		String smsAddress = null;

		String docType = MembershipDocument.RENEWAL_REMINDER_SMS;

		final String SMS_TOKEN_SALUTATION = "<salutation/>";
		final String SMS_TOKEN_EXPIRY_DATE = "<expirydate/>";
		final String SMS_TOKEN_PAY_REF_NO = "<payrefno/>";

		final String SMS_SUFFIX = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.SMS_SUFFIX); // "@onlinesms.telstra.com"
		final String SMS_SUBJECT = docType;
		final String SMS_REPLY_TO_EMAIL = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.SMS_REPLY_TO_EMAIL);
		final String SMS_OVERRIDE = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.SMS_OVERRIDE);

		String smsMessage = null;

		final String SMS_MESSAGE = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.SMS_MESSAGE);

		int prodNum = 0;
		String pendingFeeId = null;
		String expiryDateString = null;
		MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
		int groupCount = 1;
		MembershipVO memVO = null;
		Integer runNumber = null;
		ClientVO client = null;
		String mobileNumber = null;
		MembershipDocument memDoc = null;
		String salutation = "";
		String givenNames = null;
		String givenName = null;
		PendingFee pendingFee = null;
		if (membershipList != null) {
			Iterator<MembershipVO> memIt = membershipList.iterator();
			LogUtil.log(this.getClass(), "2");
			while (memIt.hasNext()) {
				memVO = memIt.next();
				client = memVO.getClient();

				// reset variables
				smsMessage = SMS_MESSAGE;
				pendingFeeId = null;
				salutation = "";
				givenName = "";
				givenNames = "";

				LogUtil.log(this.getClass(), "Membership " + memVO.getClientNumber() + ": " + memVO.getExpiryDate());

				// skip if the membership active
				if (memVO.getExpiryDate().after(now)) {
					LogUtil.warn(this.getClass(), "Membership '" + memVO.getClientNumber() + "' is active.");
					continue;
				}

				// use of payment reference number
				memDoc = memVO.getCurrentRenewalDocument();
				LogUtil.log(this.getClass(), "memDoc=" + memDoc);

				salutation = ClientHelper.getSMSSalutation(client);

				if (salutation.length() > 0) {
					// convert to form "Dear <Salutation>, "
					smsMessage = StringUtil.replaceAll(smsMessage, SMS_TOKEN_SALUTATION, salutation);
					// remove pipe delimiter
					smsMessage = StringUtil.replace(smsMessage, "|", "");
				} else {
					// split at pipe delimiter
					smsMessage = smsMessage.split("[|]")[1];
					// capitalise the first letter.
					smsMessage = smsMessage.substring(0, 1).toUpperCase() + smsMessage.substring(1, smsMessage.length());
				}

				LogUtil.log(this.getClass(), "salutation=" + salutation);
				prodNum = getArrayProductIndex(memVO.getProductCode());
				LogUtil.log(this.getClass(), "prodNum=" + prodNum);
				try {
					userTransaction.begin();

					// get actual group count
					groupCount = memVO.isGroupMember() ? memVO.getMembershipGroup().getMemberCount() : 1;

					if (memDoc != null && memDoc.getDocumentTypeCode().equals(MembershipDocument.RENEWAL_NOTICE)) {
						pendingFee = paymentMgrLocal.getPendingFee(memDoc.getDocumentId(), SourceSystem.MEMBERSHIP);
						if (pendingFee != null) {
							pendingFeeId = pendingFee.getPendingFeeID();
						}
					}

					if (pendingFeeId == null) {
						throw new ValidationException("No pending fee attached or document does not exist for '" + memVO.getClientNumber() + "'.");
					}

					LogUtil.log(this.getClass(), "pendingFeeId=" + pendingFeeId);
					mobileNumber = client.getMobilePhone();
					LogUtil.log(this.getClass(), "mobileNumber=" + mobileNumber);
					if (mobileNumber == null || mobileNumber.trim().length() == 0 || mobileNumber.length() != 10) {
						throw new ValidationException("Mobile phone number '" + mobileNumber + "' for client '" + client.getClientNumber() + "' is not valid.");
					}

					expiryDateString = memVO.getExpiryDate().formatShortDate();
					LogUtil.log(this.getClass(), "expiryDateString=" + expiryDateString);
					// replace placeholders with values

					smsMessage = StringUtil.replaceAll(smsMessage, SMS_TOKEN_EXPIRY_DATE, expiryDateString);
					smsMessage = StringUtil.replaceAll(smsMessage, SMS_TOKEN_PAY_REF_NO, pendingFeeId);

					// valid override email
					if (SMS_OVERRIDE != null && SMS_OVERRIDE.indexOf("@") != -1) {
						smsAddress = SMS_OVERRIDE;
					} else {
						// construct sms number in correct format for MessageMedia
						// +61413777666
						if (mobileNumber.indexOf("0") == 0)
							mobileNumber = mobileNumber.substring(1);
						
						smsAddress = AUS_PREFIX + mobileNumber;// + SMS_SUFFIX;
					}
					// create a record for the document
					Integer documentID = new Integer(idMgr.getNextDocumentID());
					try {
						MembershipDocument memDocVO = new MembershipDocument(documentID, docType, new DateTime(), memVO.getMembershipID(), "", runNumber, memVO.getExpiryDate(), null, new BigDecimal("0"), // do
						// not
						// calculate
						memVO.getProductCode(), new Integer(groupCount), new BigDecimal("0"), memVO.getMembershipProfileCode(), false, false, null, null, null);
						hsession.save(memDocVO);
					} catch (HibernateException he) {
						throw new RenewalNoticeException("Error saving document. ", he);
					}

					LogUtil.log(this.getClass(), "Sending SMS " + smsMessage + " to " + smsAddress);
					LogUtil.log(this.getClass(), "Sending SMS " + smsMessage + " to " + smsAddress);

//					MailMessage message = new MailMessage();
//					message.setRecipient(smsAddress);
//					message.setSubject(SMS_SUBJECT);
//					message.setMessage(smsMessage);
//					message.setFrom(SMS_REPLY_TO_EMAIL);
//
//					// send the SMS as an email
//					mailMgrLocal.sendMail(message);

					// Create the SMS manager instance
					SMSManager smsManager = new SMSManager();
					
					// Create the parameters map and populate
					HashMap<String,String> smsParams = new HashMap<String,String>();
					smsParams.put(smsAddress, smsMessage);
					
					// Send the SMS to the Mule API
					smsManager.sendSMS(smsParams);
					
					reminderCount[prodNum]++;

					// don't add to renewal files
					if (userTransaction != null) {
						userTransaction.commit();
					}

				} catch (Exception e) {
					// rollback the document
					rollback(userTransaction);

					// log a warning
					LogUtil.warn(this.getClass(), "Unable to send SMS reminder to " + memVO.getClientNumber() + ": " + docType + " " + ExceptionHelper.getExceptionStackTrace(e));

					// write to file?

				}

			}
		}

		LogUtil.log(this.getClass(), StringUtil.rightPadString("", docType.length()) + " " + ProductVO.PRODUCT_ADVANTAGE + " " + ProductVO.PRODUCT_ULTIMATE + " " + ProductVO.PRODUCT_LIFESTYLE);
		LogUtil.log(this.getClass(), docType + " " + reminderCount[0] + " " + reminderCount[1] + " " + reminderCount[2]);

	}

	private void constructUnfinancialGroupMemberAdvices(RenewalParameters renewalParameters, Collection membershipList) throws RenewalNoticeException, IOException {
		UserTransaction userTransaction = sessionContext.getUserTransaction();

		StringBuffer memberText = null;
		String docType = MembershipDocument.RENEWAL_UNFINANCIAL_GROUP_MEMBER_ADVICE;

		int printCount[] = { 0, 0, 0, 0 };

		MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
		int adviceCount[] = { 0, 0, 0, 0 };
		MembershipVO memVO = null;
		int prodNum;
		LogUtil.log(this.getClass(), "1");
		if (membershipList != null) {
			Iterator memIt = membershipList.iterator();
			LogUtil.log(this.getClass(), "2");
			while (memIt.hasNext()) {
				memVO = (MembershipVO) memIt.next();
				prodNum = getArrayProductIndex(memVO.getProductCode());

				try {
					userTransaction.begin();

					// create a record for the document
					Integer documentID = new Integer(idMgr.getNextDocumentID());
					int groupMemberCount = 1;

					// user transaction?

					try {
						MembershipDocument memDocVO = new MembershipDocument(documentID, docType, new DateTime(), memVO.getMembershipID(), " ", renewalParameters.getRunNumber(), memVO.getExpiryDate(), null, new BigDecimal("0"), memVO.getProductCode(), new Integer(groupMemberCount), new BigDecimal("0"), memVO.getMembershipProfileCode(), true,// printed
						// only
						false, null, null, null);
						hsession.save(memDocVO);

						// if document saves ok then write to the file

						adviceCount[prodNum]++;
						LogUtil.log(this.getClass(), "3");

						// add primary
						memberText = new StringBuffer();
						memberText.append(LINE_TYPE_PRIMARY); // line type
						memberText.append(DELIMITER);
						String memberNumber = memVO.getMembershipNumber(7);
						memberText.append(memberNumber); // member number
						memberText.append(DELIMITER);
						memberText.append(docType); // doc type
						memberText.append(DELIMITER);
						addMemberDetails(memVO, memberText); // member details

						// default pricing option
						memberText.append(MembershipHelper.SEGMENT_TEXT[0]);
						memberText.append(DELIMITER);

						memberText.append(memVO.getExpiryDate().formatShortDate());
						memberText.append(DELIMITER);
						//
						memberText.append(DELIMITER);

						// add extra delimiters to be consistent with other
						// lines
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);

						// add print or email flag
						memberText.append("Y" + DELIMITER);// print
						memberText.append("N" + DELIMITER);// email
						memberText.append(DELIMITER);// email address

						// new line
						memberText.append(FileUtil.NEW_LINE);

						// add to file
						renewalParameters.writeWithoutCardLine(memberText.toString());
						// add one to count
						renewalParameters.incrementCountWithoutCard();
						LogUtil.log(this.getClass(), "4");

						// add secondary
						memberText = new StringBuffer();
						memberText.append(LINE_TYPE_SECONDARY);
						memberText.append(DELIMITER);
						memberText.append(memberNumber);
						memberText.append(DELIMITER);
						memberText.append(memberNumber);
						memberText.append(DELIMITER);
						// 3
						// add extra delimiters to be consistent with other
						// lines
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);
						memberText.append(DELIMITER);

						printCount[prodNum]++;

						// 17
						memberText.append(FileUtil.NEW_LINE);
						// add to file
						renewalParameters.writeWithoutCardLine(memberText.toString());

						// commit the document
						if (userTransaction != null) {
							userTransaction.commit();
						}

					} catch (HibernateException ex) {
						throw new RenewalNoticeException("Error saving document. ", ex);
					}
				} catch (Exception e) {
					// rollback the document
					rollback(userTransaction);

					renewalParameters.writeErrorLine(memVO.getClientNumber(), docType, SourceSystem.MEMBERSHIP.getAbbreviation(), e.getMessage());
					// log the exception to file
					LogUtil.warn(this.getClass(), e);
				}

			}

			renewalParameters.writeSummaryLine("", ProductVO.PRODUCT_ADVANTAGE, ProductVO.PRODUCT_ULTIMATE, ProductVO.PRODUCT_LIFESTYLE, "Other");
			renewalParameters.writeSummaryLine(MembershipDocument.RENEWAL_UNFINANCIAL_GROUP_MEMBER_ADVICE, adviceCount[0], adviceCount[1], adviceCount[2], adviceCount[3]);
			if (!isCountArrayEmpty(printCount)) {
				renewalParameters.writeSummaryLine(LABEL_PRINTED, printCount[0], printCount[1], printCount[2], printCount[3]);
			}
		}

	}

	public final static String RENEWAL_RUN_STATUS_BEGUN = "Begun";

	/**
	 * Create a mem_renewal_run record with status = "begun"
	 */
	private void createBatch(RenewalParameters renParms) throws RenewalNoticeException {
		String sql = "\ninsert into PUB.mem_renewal_run " + "\n (run_number, run_date, run_by, first_date, last_date, run_status)" + "\n values(?,?,?,?,?,'" + RENEWAL_RUN_STATUS_BEGUN + "')";

		PreparedStatement statement = null;
		Connection connection = null;
		UserTransaction userTx = null;

		try {
			userTx = sessionContext.getUserTransaction();
			userTx.begin();
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setInt(1, renParms.getRunNumber().intValue());
			statement.setDate(2, renParms.getRunDate().toSQLDate());
			statement.setString(3, renParms.getRunUserId());
			statement.setDate(4, renParms.getFirstDate().toSQLDate());
			statement.setDate(5, renParms.getLastDate().toSQLDate());
			statement.executeUpdate();
			userTx.commit();
		} catch (Exception se) {
			LogUtil.log(this.getClass(), "Error writing to renewal_run table: " + se);

			rollback(userTx);

			throw new RenewalNoticeException("Unable to store run details: " + se);
		} finally {
			try {
				ConnectionUtil.closeConnection(connection, statement);
			} catch (Exception e) {
				throw new RenewalNoticeException("Unable to close connection: " + e);
			}
		}
	}

	private void doRenewalNotices(RenewalParameters renParms) throws RenewalNoticeException {
		try {
			renParms.writeLogLine("\nStart processing for renewals at " + new DateTime().getSecondsFromMidnight());
		} catch (IOException ioe) {
			throw new RenewalNoticeException("Unable to write to error file: " + ioe);
		}
		String sql = "\nselect membership_id, client_number, expiry_date from pub.mem_membership mem" + "\nINNER JOIN pub.[cl-master] ON pub.[cl-master].[client-no] = mem.client_number" + "\n where expiry_date >= ? " + "\n AND [client-status] <> '" + Client.STATUS_DECEASED + "' " + "\n and expiry_date <= ? " + "\n and membership_status in('" + MembershipVO.STATUS_ACTIVE + "','" + MembershipVO.STATUS_ONHOLD + "')" + "\n and product_code not in('" + ProductVO.PRODUCT_CMO + "')" + "\n and not exists (select * from pub.mem_document doc " + "\n                 where doc.membership_id = mem.membership_id" + "\n                 and doc.mem_date = mem.expiry_date" + "\n                 and doc.document_type_code in ('" + MembershipDocument.RENEWAL_NOTICE + "','" + MembershipDocument.RENEWAL_ADVICE + "')" + "\n                 and (document_status <> '" + MembershipDocument.DOCUMENT_STATUS_CANCELLED + "' or document_status is null))" + "\n and not exists (select * from PUB.mem_membership_group grp" + "\n                 where grp.membership_id = mem.membership_ID" + "\n                 and grp.is_prime_addressee = 0)";
		LogUtil.debug(this.getClass(), "renewal notices=\n" + sql);

		// may be an advice but this will be determined later
		constructNotices(renParms, sql, MembershipDocument.RENEWAL_NOTICE, renParms.getFirstDate(), renParms.getLastDate());

	}

	/**
	 * Create and return a new card/card request for the given membership.
	 * 
	 * @param oldCardExpiryDate Description of the Parameter
	 * @param memVO Description of the Parameter
	 * @param IIN Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RenewalNoticeException Description of the Exception
	 */
	private MembershipCardVO createNewCard(MembershipCardVO oldCard, MembershipVO memVO, String IIN, String reasonCode, boolean request) throws RenewalNoticeException {
		DateTime oldCardExpiryDate = null;
		int issuanceNumber = 0;
		if (oldCard != null) {
			oldCardExpiryDate = oldCard.getExpiryDate();
			issuanceNumber = oldCard.getIssuanceNumber();
			issuanceNumber++;
		}

		// create new CardExpiryDate to align with membership expiry
		DateTime newCardExpiryDate = getNewCardExpiryDate(oldCardExpiryDate, memVO.getExpiryDate());

		DateTime currentDate = new DateTime().getDateOnly();

		// create the new card record
		MembershipCardVO cardVO = null;
		try {
			// Create a request for a new card
			cardVO = new MembershipCardVO(memVO.getMembershipID(), issuanceNumber, currentDate, ReferenceDataVO.REF_TYPE_MEMBERSHIP_CARD_REQUEST, reasonCode, memVO.getRenewalMembershipTier() != null ? memVO.getRenewalMembershipTier().getTierCode() : null, memVO.getJoinDate(), memVO.getRego());

			if (!request) {
				// Turn the card request into an issued card.
				cardVO.setProduct(memVO.getProduct());
				cardVO.setCardNumber(IIN, memVO.getMembershipNumber(7));
				cardVO.setExpiryDate(newCardExpiryDate);
				cardVO.setIssueDate(currentDate);
				cardVO.setCardName(CardHelper.getCardName(memVO.getClient(), memVO.getMembershipProfile().getNameSuffix()));
			}
			// Create the card record and generate a card ID.
			membershipMgrLocal.createMembershipCard(cardVO);
		} catch (Exception e) {
			throw new RenewalNoticeException("Error creating card : " + e.getMessage());
		}

		return cardVO;
	}

	/**
	 * // createNewCard
	 * 
	 * /** Get the expiry date for a new card. If an old card expiry date is given then base the new card expiry date on that otherwise base the new card expiry date on the membership expiry date.
	 * 
	 * @param oldCardExpiryDate Description of the Parameter
	 * @param membershipExpiryDate Description of the Parameter
	 * @return The newCardExpiryDate value
	 * @exception RenewalNoticeException Description of the Exception
	 */
	private DateTime getNewCardExpiryDate(DateTime oldCardExpiryDate, DateTime membershipExpiryDate) throws RenewalNoticeException {
		DateTime newExpiryDate = null;
		try {
			String memCardPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DEFAULT_CARD_PERIOD);
			Interval memCardPeriod = new Interval(memCardPeriodSpec);
			if (oldCardExpiryDate != null) {
				newExpiryDate = oldCardExpiryDate.add(memCardPeriod);
			} else if (membershipExpiryDate != null) {
				newExpiryDate = membershipExpiryDate.add(memCardPeriod);
			} else {
				throw new RenewalNoticeException("Failed to set Card Expiry date. An old card expiry date or the membership expiry date must be supplied.");
			}
		} catch (RenewalNoticeException rne) {
			throw rne;
		} catch (Exception e) {
			throw new RenewalNoticeException("Error setting Card Expiry date : " + e.getMessage());
		}

		return newExpiryDate;
	}

	// getNewCardExpiryDate

	/**
	 * makePaText Part of the make membership renewal notices process
	 * 
	 * @param memVO The feature to be added to the MemberDetails attribute
	 * @param paText The feature to be added to the MemberDetails attribute
	 * @exception RenewalNoticeException Description of the Exception
	 */
	private void addMemberDetails(MembershipVO memVO, StringBuffer paText) throws RenewalNoticeException {
		// write the prime addressee's name

		ClientVO client = null;
		PostalAddressVO address = null;

		try {
			// get client object
			client = memVO.getClient();
			MembershipGroupVO membershipGroup = memVO.getMembershipGroup();
			if (membershipGroup != null && membershipGroup.getGroup() != null) {
				address = membershipGroup.getGroup().getBillingAddress();
			} else {
				address = client.getPostalAddress();
			}
		} catch (RemoteException e1) {
			throw new RenewalNoticeException(e1);
		}
		String address1 = address.getProperty();
		String address2 = address.getPropertyQualifier();
		String suburb = address.getSuburb();
		String state = address.getState();
		String postcode = address.getPostcode();
		String title = client.getTitle();
		String initials = client.getInitials();
		String surname = client.getSurname();
		String addressTitle = client.getAddressTitle();
		String street = address.getStreet();
		String givenNames = client.getGivenNames();
		String clientStatus = client.getStatus();

		if (address2 == null) {
			address2 = "";
		}
		if (street != null) {
			address2 = address2 + " " + street;

		}
		if (address1 == null || address1.equals("")) {
			address1 = address2;
			address2 = "";
		}
		paText.append(StringUtil.quote(address1.trim())); // property
		paText.append(DELIMITER);
		paText.append(StringUtil.quote(address2.trim())); // street number
		paText.append(DELIMITER);
		paText.append(suburb.trim()); // suburb
		paText.append(DELIMITER);
		paText.append(state.trim()); // state
		paText.append(DELIMITER);
		paText.append(postcode); // postcode
		paText.append(DELIMITER);
		paText.append(title.trim()); // title
		paText.append(DELIMITER);
		paText.append(initials.trim()); // initials
		paText.append(DELIMITER);
		givenNames = extractFirstName(givenNames);
		paText.append(givenNames); // first name
		paText.append(DELIMITER);
		paText.append(surname.trim()); // surname
		paText.append(DELIMITER);
		clientStatus = ClientHelper.formatClientStatus(clientStatus); // client
		// status
		// -
		// active
		// if
		// empty
		paText.append(clientStatus);
		paText.append(DELIMITER);

		// write the prime addressee's product
		try {
			String productDesc = memVO.getProduct().getDescription();
			if (memVO.getNextProductCode() != null) {
				productDesc = membershipRefMgrLocal.getProduct(memVO.getNextProductCode()).getDescription();
			}
			paText.append(productDesc); // product
		} catch (RemoteException e) {
			throw new RenewalNoticeException("Unable to get product description: " + e.getMessage());
		}

		paText.append(DELIMITER);
	}

	private String extractFirstName(String givenNames) {
		givenNames = givenNames.trim();
		int pos = givenNames.indexOf(" ");
		if (pos > -1) {
			givenNames = givenNames.substring(0, pos);
		}
		return givenNames;
	}

	public final static String RENEWAL_RUN_STATUS_COMPLETE = "Complete";

	/**
	 * Store record
	 */
	private void updateRunToComplete(Integer runNumber) throws RenewalNoticeException {
		String sql = "update PUB.mem_renewal_run " + " set run_status = '" + RENEWAL_RUN_STATUS_COMPLETE + "'" + " where run_number = ? ";

		PreparedStatement statement = null;
		Connection connection = null;
		UserTransaction userTx = null;
		try {
			userTx = sessionContext.getUserTransaction();
			userTx.begin();
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setInt(1, runNumber.intValue());
			statement.executeUpdate();
			userTx.commit();
		} catch (Exception se) {
			LogUtil.log(this.getClass(), "Error writing to renewal_run table: " + se);
			try {
				rollback(userTx);
			} catch (Exception ex) {
				LogUtil.log(this.getClass(), "Unable to rollback");
			}
			throw new RenewalNoticeException("Unable to store run details: " + se);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
	}

	/**
	 * String getPricing option Looks for a group, and figures out pricing option. Appends the pricing option to the stringbuffer Returns the groupId
	 * 
	 * @param memVO Description of the Parameter
	 * @param transGroup Description of the Parameter
	 * @return The pricingOption value
	 * @exception RenewalNoticeException Description of the Exception
	 */
	private int getPricingOption(MembershipVO memVO, TransactionGroup transGroup) throws RenewalNoticeException {
		try {
			if (!memVO.isGroupMember()) {
				return 1;
			}
		} catch (RemoteException e) {
			throw new RenewalNoticeException("Unable to get group membership: " + e.getMessage());
		}

		int groupId = transGroup.getMembershipGroupID().intValue();
		String pricingOption = null;
		Connection connection = null;
		PreparedStatement statement = null;
		StringBuffer sql = new StringBuffer();
		sql.append("\n Select count(grp1.membership_id), grp1.group_id from PUB.mem_membership_group grp1 ");
		sql.append("\n where grp1.group_id = ?");
		sql.append("\n group by grp1.group_id");
		int groupSize = 1;
		try {
			connection = this.dataSource.getConnection();
			statement = connection.prepareStatement(sql.toString());
			statement.setInt(1, groupId);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				groupSize = rs.getInt(1);
			}
			rs.close();
		} catch (SQLException e) {
			// System.out.println("\n " + sql + "\n" + groupId);
			// ee.printStackTrace();
			throw new RenewalNoticeException("Can't get group status: " + e.getMessage());
		} finally {
			ConnectionUtil.closeConnection(connection);
			ConnectionUtil.closeStatement(statement);
		}
		return groupSize;
	}

	/**
	 * Remove a renewal batch all associated documents, pending fees, schedules, etc.
	 * 
	 * @param runNumber int
	 * @throws RemoteException
	 */
	public Hashtable<String, Integer> removeRenewalBatch(Integer runNumber) throws RemoteException {
		Hashtable<String, Integer> documentCounts = new Hashtable<String, Integer>();
		int totalDocumentCount = 0;
		int totalRemovedDocumentCount = 0;
		int totalBatchesRemovedCount = 0;
		Integer documentId = null;
		final String SQL_RENEWAL_BATCH_DOCUMENTS = "select document_id from PUB.mem_document where run_number = ?";
		final String SQL_DELETE_RENEWAL_BATCH = "delete from PUB.mem_renewal_run where run_number = ?";

		UserTransaction userTransaction = sessionContext.getUserTransaction();

		Connection connection = null;
		PreparedStatement statement = null;
		PreparedStatement deleteStatement = null;

		try {

			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_RENEWAL_BATCH_DOCUMENTS);
			statement.setInt(1, runNumber.intValue());

			PaymentTransactionMgrLocal paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				totalDocumentCount++;
				documentId = new Integer(rs.getInt(1));

				try {
					// start transaction
					userTransaction.begin();

					// delete document and all associated records.
					membershipMgrLocal.removeRenewalDocument(documentId, MembershipMgr.MODE_DELETE, paymentTransactionMgrLocal, false);

					// end transaction
					userTransaction.commit();
					paymentTransactionMgrLocal.commit();

					// increment removed
					totalRemovedDocumentCount++;
				} catch (Exception e) {
					rollback(userTransaction);
					if (paymentTransactionMgrLocal != null) {
						paymentTransactionMgrLocal.rollback();
					}
					LogUtil.warn(this.getClass(), "Unable to remove renewal document " + documentId);
					LogUtil.warn(this.getClass(), e);
				}
			}
			rs.close();
			// delete the batch record if total = deleted
			if (totalRemovedDocumentCount == totalDocumentCount) {
				try {
					userTransaction.begin();
					deleteStatement = connection.prepareStatement(SQL_DELETE_RENEWAL_BATCH);
					deleteStatement.setInt(1, runNumber.intValue());
					totalBatchesRemovedCount = deleteStatement.executeUpdate();
					userTransaction.commit();
				} catch (Exception ex) {
					rollback(userTransaction);
					throw new SystemException("Unable to delete renewal batch.", ex);
				}

			}
		} catch (Exception e) {
			throw new RemoteException("Error removing batch.", e);
		} finally {
			ConnectionUtil.closeConnection(connection);
			ConnectionUtil.closeStatement(statement);
		}
		// add counts
		documentCounts.put(COUNT_TOTAL_DOCUMENTS, new Integer(totalDocumentCount));
		documentCounts.put(COUNT_TOTAL_REMOVED_DOCUMENTS, new Integer(totalRemovedDocumentCount));
		documentCounts.put(COUNT_TOTAL_BATCHES_REMOVED, new Integer(totalBatchesRemovedCount));
		return documentCounts;
	}

	public int clearEmailSentFlag(Integer runNumber) throws RemoteException {
		UserTransaction userTransaction = sessionContext.getUserTransaction();
		final String SQL_RENEWAL_BATCH_DOCUMENTS = "select document_id from PUB.mem_document where run_number = ? and emailed = ? and emailAddress is not null";
		Connection connection = null;
		PreparedStatement statement = null;
		Integer documentId = null;
		MembershipDocument document = null;
		int emailFlagCleared = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_RENEWAL_BATCH_DOCUMENTS);
			statement.setInt(1, runNumber.intValue());
			statement.setBoolean(2, true);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				documentId = new Integer(rs.getInt(1));
				document = membershipMgrLocal.getMembershipDocument(documentId);
				if (document.isEmailed() && document.getEmailDate() != null) // double
				// check
				{
					try {
						userTransaction.begin();
						// update document email date so it is available to
						// email again.
						document.setEmailDate(null); // reset email address
						membershipMgrLocal.updateMembershipDocument(document); // save
						// to
						// DB
						emailFlagCleared++;
						userTransaction.commit();
					} catch (Exception e) {
						rollback(userTransaction);
						LogUtil.warn(this.getClass(), "Unable to remove renewal document " + documentId);
					}
				} // end if
			} // end while
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			throw new SystemException("Unable to update documents.", ex);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return emailFlagCleared;
	}

	public static final String COUNT_TOTAL_DOCUMENTS = "total";
	public static final String COUNT_TOTAL_REMOVED_DOCUMENTS = "total removed";
	public static final String COUNT_TOTAL_BATCHES_REMOVED = "batch removed";

	public static final String COUNT_RENEWAL_PRINT = "Renewals printed";
	public static final String COUNT_RENEWAL_EMAIL = "Renewals emailed";
	public static final String COUNT_RENEWAL_TEST_EMAILS_SENT = "Test renewal emails sent";
	public static final String COUNT_RENEWAL_PRIMARY_LINES = "Primary lines";
	public static final String COUNT_RENEWAL_PRIMARY_LINES_VALID = "Valid primary lines";
	public static final String COUNT_RENEWAL_EMAIL_DOCUMENT_UPDATES = "Renewal document updated with email attributes";
	public static final String COUNT_RENEWAL_NO_DELIVERY_METHOD = "No delivey method defined";

	/**
	 * Part of the renewal run process Adds the costing information from the transaction group
	 * 
	 * @param transGrp The feature to be added to the Costings attribute
	 * @param outText The feature to be added to the Costings attribute
	 * @exception RenewalNoticeException Description of the Exception
	 */
	private void addCostings(TransactionGroup transGrp, StringBuffer outText) throws RenewalNoticeException {
		try {
			outText.append(CurrencyUtil.formatDollarValue(transGrp.getGrossTransactionFee()));
			outText.append(DELIMITER);
			// rewards discounts when implemented
			outText.append(DELIMITER);
			double totalDiscounts = transGrp.getTotalDiscount();
			double totalCredits = transGrp.getTotalCredits();
			totalDiscounts -= totalCredits;
			outText.append(CurrencyUtil.formatDollarValue(totalDiscounts));
			outText.append(DELIMITER);
			outText.append(CurrencyUtil.formatDollarValue(totalCredits));
			outText.append(DELIMITER);
			outText.append(CurrencyUtil.formatDollarValue(transGrp.getAmountPayable()));
			outText.append(DELIMITER);
			outText.append(CurrencyUtil.formatDollarValue(transGrp.getGstAmount()));
			outText.append(DELIMITER);
		} catch (RemoteException e) {
			throw new RenewalNoticeException("Error adding costings: " + e.getMessage());
		}
	}

	public final static String FORMAT_EXTRACT = "extract";

	public final static String FORMAT_REPORT = "report";

	/**
	 * Construct the direct debit details to be added to the renewal extract file.
	 * 
	 * @param memVO MembershipVO
	 * @param transGroup TransactionGroup
	 * @throws RenewalNoticeException
	 * @throws RemoteException
	 * @return the direct debit authority and collection of scheduled items.
	 */
	public static Hashtable<String, Comparable> constructDirectDebitDetails(PaymentTransactionMgr paymentTxMgr, MembershipVO memVO, TransactionGroup transGroup, String format) throws RenewalNoticeException, RemoteException {

		LogUtil.debug("constructDirectDebitDetails", "format=" + format);

		Hashtable<String, Comparable> directDebitDetails = new Hashtable<String, Comparable>();

		PaymentMgr paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();

		String memberNumber = memVO.getMembershipNumber(7);

		StringBuffer directDebitAuthorityDetails = new StringBuffer();
		DirectDebitAuthority ddAuthority = null;
		try {
			ddAuthority = memVO.getDirectDebitAuthority();
		} catch (RemoteException re) {
			throw new RenewalNoticeException(re.getMessage());
		}

		if (ddAuthority == null) {
			throw new SystemException("Direct debit authority is null.");
		}

		LogUtil.debug("constructDirectDebitDetails", "ddAuthority=" + ddAuthority.toString());

		if (format.equals(FORMAT_EXTRACT)) {

			// get account details
			int dayToDebit = ddAuthority.getDayToDebit();
			DirectDebitFrequency frequency = ddAuthority.getDeductionFrequency();
			String paymentFrequency = frequency.getDescription();
			BankAccount bankAccount = ddAuthority.getBankAccount();
			if (bankAccount instanceof CreditCardAccount) {
				CreditCardAccount ccAccount = (CreditCardAccount) bankAccount;
				// line 1
				directDebitAuthorityDetails.append(ccAccount.getCardTypeDescription());
				// line 2
				directDebitAuthorityDetails.append(" ");
				// directDebitAuthorityDetails.append(StringUtil.formatCreditCardNumber(ccAccount.
				// getCardNumber()));
				directDebitAuthorityDetails.append(StringUtil.obscureFormattedCreditCardNumber(ccAccount.getCardNumber()));
				directDebitAuthorityDetails.append(DELIMITER);

				directDebitAuthorityDetails.append("Expires " + ccAccount.getCardExpiry());
				directDebitAuthorityDetails.append(" ");
				directDebitAuthorityDetails.append(ccAccount.getAccountName());
			} else if (bankAccount instanceof DebitAccount) {
				DebitAccount dAccount = (DebitAccount) bankAccount;
				String branchName = paymentMgrLocal.validateBsbNumber(dAccount.getBsbNumber());

				directDebitAuthorityDetails.append(branchName);

				directDebitAuthorityDetails.append(DELIMITER);

				directDebitAuthorityDetails.append("BSB " + dAccount.getBsbNumber());
				directDebitAuthorityDetails.append(" ");
				directDebitAuthorityDetails.append("Account " + StringUtil.obscureBankAccountNumber(dAccount.getAccountNumber()));
				directDebitAuthorityDetails.append(" ");
				directDebitAuthorityDetails.append(dAccount.getAccountName());
			}

			directDebitAuthorityDetails.append(DELIMITER);
			directDebitAuthorityDetails.append(paymentFrequency);
			directDebitAuthorityDetails.append(DELIMITER);

			// extract only
			if (directDebitAuthorityDetails.length() > 0) {
				directDebitDetails.put(KEY_DD_AUTHORITY, directDebitAuthorityDetails.toString());
			}
		} else if (format.equals(FORMAT_REPORT)) {
			// do nothing
		} else {
			throw new RenewalNoticeException("Unable to determine format '" + format + "' for constructing direct debit schedule.");
		}

		StringBuffer directDebitScheduleDetails = new StringBuffer();

		DirectDebitSchedule ddSchedule = null;

		// Is a schedule already attached?
		ddSchedule = transGroup.getDirectDebitSchedule();

		if (ddSchedule == null) {

			// Construct a direct debit schedule specification using the
			// authority from the membership.
			// Don't want to set the authority on the transaction group as this
			// changes it to being paid
			// by direct debit.
			try {
				ddSchedule = transGroup.getDirectDebitScheduleSpecification(ddAuthority);
				LogUtil.debug("constructDirectDebitDetails", "getDirectDebitScheduleSpecification ddSchedule=" + ddSchedule);
			} catch (RemoteException re) {
				throw new RenewalNoticeException(re.getMessage());
			}

			// Get a provisional direct debit schedule based on the
			// specification
			try {
				// replaced by renewal schedule
				ddSchedule.setRenewalVersion(true);
				ddSchedule.setEndDate(transGroup.getExpiryDate());
				// create renewal version of the schedule in the direct debit
				// system
				// will need to extract it out to save in transaction xml
				Integer ddScheduleId = null;
				try {
					ddScheduleId = paymentTxMgr.createRenewalDirectDebitSchedule(ddSchedule);
					LogUtil.debug("constructDirectDebitDetails", "ddScheduleId=" + ddScheduleId);
					// update dd schedule with item details
					ddSchedule = paymentMgrLocal.getDirectDebitSchedule(ddScheduleId);
					LogUtil.log("RenewalMgrBean.constructDirectDebitDetails", "\n     ddSchedule=" + ddSchedule + "\n startDate = " + ddSchedule.getStartDate() + "\n endDate   = " + ddSchedule.getEndDate());
				} catch (Exception ex) {
					// any exception will result in rollback
					throw new RenewalNoticeException(ExceptionHelper.getExceptionStackTrace(ex));
				}
				if (ddScheduleId == null) {
					throw new RollBackException("Direct debit schedule Id is null.");
				}

				LogUtil.debug("constructDirectDebitDetails", "ddScheduleId=" + ddScheduleId);
				// set schedule id on transaction group
				transGroup.setDirectDebitScheduleId(ddScheduleId);

				LogUtil.debug("constructDirectDebitDetails", "createRenewalDirectDebitSchedule ddSchedule=" + ddSchedule);

			} catch (RollBackException rex) {
				throw new RenewalNoticeException(ExceptionHelper.getExceptionStackTrace(rex));
			}
		}

		if (format.equals(FORMAT_EXTRACT)) {
			// construct a preliminary schedule in extract format
			directDebitScheduleDetails.append(RenewalMgrBean.constructDirectDebitItemSummary(ddSchedule, memberNumber));
		} else if (format.equals(FORMAT_REPORT)) {
			// do nothing and delegate to the report processor
		}

		LogUtil.debug("constructDirectDebitDetails", "constructDirectDebitDetails 1" + directDebitDetails.toString());

		if (format.equals(FORMAT_EXTRACT) && directDebitScheduleDetails.length() > 0) {
			directDebitDetails.put(KEY_DD_SCHEDULE_SUMMARY, directDebitScheduleDetails.toString());
			directDebitDetails.put(KEY_DD_SCHEDULE, ddSchedule);
		} else if (format.equals(FORMAT_REPORT)) {
			// add dd schedule object
			directDebitDetails.put(KEY_DD_SCHEDULE, ddSchedule);
		}

		// if report then it contains authority details and schedule details.
		// if extract then it contains only direct debit schedule object.

		LogUtil.debug("constructDirectDebitDetails", "constructDirectDebitDetails 2" + directDebitDetails.toString());
		return directDebitDetails;

	}

	/**
	 * Construct a list of direct debit items.
	 * 
	 * @param ddSchedule DirectDebitSchedule
	 * @return String
	 */
	private static String constructDirectDebitItemSummary(DirectDebitSchedule ddSchedule, String memberNumber) {
		StringBuffer directDebitItemSummary = new StringBuffer();

		ArrayList scheduleItemList = (ArrayList) ddSchedule.getScheduleItemList();
		// sort into ascending order (create date by default!!!)
		Collections.sort(scheduleItemList);
		DirectDebitScheduleItem ddItem = null;
		for (int i = 0; i < scheduleItemList.size(); i++) {
			// new line
			ddItem = (DirectDebitScheduleItem) scheduleItemList.get(i);
			if (ddItem != null) {
				directDebitItemSummary.append(LINE_TYPE_DIRECT_DEBIT); // 004
				directDebitItemSummary.append(DELIMITER);
				directDebitItemSummary.append(memberNumber); // 0123456
				directDebitItemSummary.append(DELIMITER);
				directDebitItemSummary.append(ddItem.getScheduledDate()); // 01/01/2005
				directDebitItemSummary.append(DELIMITER);
				directDebitItemSummary.append(CurrencyUtil.formatDollarValue(ddItem.getAmount())); // $51.00
				directDebitItemSummary.append(DELIMITER);
				directDebitItemSummary.append(ddItem.getStatus()); // Not
				// Scheduled
				directDebitItemSummary.append(DELIMITER);
				directDebitItemSummary.append(FileUtil.NEW_LINE);
			}
		}
		return directDebitItemSummary.toString();
	}

	private final static String KEY_REQUIRES_NEW_CARD = "Requires new card";

	private final static String KEY_UNFINANCIAL_GROUP_MEMBERS = "Unfinancial group members";

	public final static String KEY_DD_AUTHORITY = "Direct Debit Authority";

	public final static String KEY_DD_SCHEDULE_SUMMARY = "Direct Debit Schedule Summary";

	public final static String KEY_DD_SCHEDULE = "Direct Debit Schedule";

	private Hashtable<String, Serializable> constructGroupDetails(TransactionGroup transGrp, int groupId, MembershipVO memVO,
	// DateTime newMembershipExpiryDate,
	String docType) throws RenewalNoticeException {
		Hashtable<String, Serializable> groupDetails = new Hashtable<String, Serializable>();
		Hashtable<String, Serializable> cardDetails = new Hashtable<String, Serializable>();
		ArrayList<MembershipVO> unfinancialGroupMembers = new ArrayList<MembershipVO>();
		ArrayList<String> groupDataList = new ArrayList<String>();

		MembershipVO groupMemVO = null;
		double totalCredits = 0;
		StringBuffer cardData = null;
		int groupCount = 0;
		int membershipId = 0;
		StringBuffer groupMemberData = null;
		boolean groupNeedsCard = false;
		// boolean memberNeedsCard = false;
		String memberNumber = null;
		String clientStatus = null;
		String initials = null;
		String givenNames = null;
		String surname = null;
		String title = null;

		ClientVO client = null;

		// PersonVO personVO = null;
		String discounts = null;

		if (groupId > 0) {
			StringBuffer sql = new StringBuffer();
			sql.append("select mem.membership_id,surname,\"given-names\", \"client-status\",\"client-title\" ");
			sql.append("from PUB.mem_membership mem inner join PUB.mem_membership_group grp ");
			sql.append("on mem.membership_ID = grp.membership_id ");
			sql.append("inner join PUB.\"cl-master\" clt ");
			sql.append("on clt.\"client-no\" = mem.client_number ");
			sql.append("where grp.group_id = ? ");
			sql.append("order by is_prime_addressee desc ");
			Connection connection = null;
			PreparedStatement statement = null;

			try {
				connection = dataSource.getConnection();
				statement = connection.prepareStatement(sql.toString());
				statement.setInt(1, groupId);
				ResultSet rs = statement.executeQuery();
				// groupNeedsCard = false;

				while (rs.next()) {
					groupMemberData = new StringBuffer();
					groupMemberData.append(LINE_TYPE_SECONDARY);
					groupMemberData.append(DELIMITER);
					memberNumber = memVO.getMembershipNumber(7);
					groupMemberData.append(memberNumber);
					groupMemberData.append(DELIMITER);
					groupCount++;
					groupMemVO = membershipMgrLocal.getMembership(new Integer(rs.getString("membership_id")));
					// if the member is not the prime addresse and the run is
					// for final notice then add
					// it to a list for unfinancial members.
					if (!groupMemVO.isPrimeAddressee() && docType.equals(MembershipDocument.RENEWAL_FINAL_NOTICE)) {
						// LogUtil.log(this.getClass(),"adding unfinancial members");
						unfinancialGroupMembers.add(groupMemVO);
					}
					groupMemberData.append(groupMemVO.getMembershipNumber(7));
					groupMemberData.append(DELIMITER);

					// get group member name
					title = rs.getString(5);
					givenNames = rs.getString(3);
					surname = rs.getString(2);
					// name in form - Mr John Smith
					groupMemberData.append(formatGroupMemberName(title, givenNames, surname));

					groupMemberData.append(DELIMITER);
					groupMemberData.append(ClientHelper.formatClientStatus(rs.getString(4)));

					groupMemberData.append(DELIMITER);

					// check if a new card is needed
					if (isSocialMember(memVO.getMembershipID())) {
						groupMemberData.append(ProductBenefitTypeVO.PBT_SOCIAL);
					}
					groupMemberData.append(DELIMITER);
					cardDetails = constructCardDetails(groupMemVO, docType);
					cardData = (StringBuffer) cardDetails.get(KEY_CARD_DATA);
					// only check if false
					if (!groupNeedsCard) {
						groupNeedsCard = ((Boolean) cardDetails.get(KEY_REQUIRES_NEW_CARD)).booleanValue();
					}
					LogUtil.debug(this.getClass(), "groupNeedsCard=" + groupNeedsCard);
					// append the card details
					groupMemberData.append(cardData);

					discounts = getMemberDiscounts(groupMemVO.getMembershipID(), transGrp);
					groupMemberData.append(discounts);
					groupMemberData.append(DELIMITER);
					LogUtil.debug(this.getClass(), "groupMemberData=" + groupMemberData + " " + (groupMemberData.toString().split(DELIMITER)).length);
					groupDataList.add(groupMemberData.toString());
				}
				rs.close();
			} catch (Exception e) {
				throw new RenewalNoticeException("Error adding group details:" + e);
			} finally {
				ConnectionUtil.closeStatement(statement);
				ConnectionUtil.closeConnection(connection);
			}
		} else {
			// add details for the one member
			groupMemberData = new StringBuffer();
			groupMemberData.append(LINE_TYPE_SECONDARY);
			groupMemberData.append(DELIMITER);
			memberNumber = memVO.getMembershipNumber(7);
			groupMemberData.append(memberNumber);
			groupMemberData.append(DELIMITER);
			groupMemberData.append(memberNumber);
			groupMemberData.append(DELIMITER);
			try {
				// personVO = (PersonVO)memVO.getClient();
				client = memVO.getClient();
				groupMemberData.append(formatGroupMemberName(client.getTitle(), client.getGivenNames(), client.getSurname()));
				groupMemberData.append(DELIMITER);
				groupMemberData.append(ClientHelper.formatClientStatus(client.getStatus()));
				groupMemberData.append(DELIMITER);
			} catch (RemoteException e) {
				throw new RenewalNoticeException("Unable to get client's postal name: ", e);
			}

			/*
			 * addVehicleDetails(memVO.getMembershipID().intValue(),groupMemberData );
			 */
			try {
				if (isSocialMember(memVO.getMembershipID())) {
					groupMemberData.append(ProductBenefitTypeVO.PBT_SOCIAL);
				}
			} catch (RemoteException e) {
				throw new RenewalNoticeException("Unable to get membership ID: " + e.getMessage());
			}

			groupMemberData.append(DELIMITER);
			cardDetails = constructCardDetails(memVO, docType);
			LogUtil.debug(this.getClass(), "groupNeedsCard=" + groupNeedsCard);
			groupNeedsCard = ((Boolean) cardDetails.get(KEY_REQUIRES_NEW_CARD)).booleanValue();
			cardData = (StringBuffer) cardDetails.get(KEY_CARD_DATA);

			// append the card details
			groupMemberData.append(cardData);

			discounts = getMemberDiscounts(memVO.getMembershipID(), transGrp);
			groupMemberData.append(discounts);
			groupMemberData.append(DELIMITER);

			LogUtil.debug(this.getClass(), "groupMemberData=" + groupMemberData + " " + (groupMemberData.toString().split(DELIMITER)).length);

			groupDataList.add(groupMemberData.toString());
		}
		LogUtil.debug(this.getClass(), "groupDataList=" + groupDataList);
		LogUtil.debug(this.getClass(), "groupNeedsCard=" + groupNeedsCard);
		groupDetails.put(KEY_REQUIRES_NEW_CARD, new Boolean(groupNeedsCard));
		groupDetails.put(KEY_UNFINANCIAL_GROUP_MEMBERS, unfinancialGroupMembers);
		groupDetails.put(KEY_GROUP_DATA_LIST, groupDataList);
		LogUtil.debug(this.getClass(), "groupDetails=" + groupDetails);
		return groupDetails;
	}

	private final String KEY_GROUP_DATA_LIST = "Group Data List";

	private String formatGroupMemberName(String title, String givenNames, String surname) {
		StringBuffer groupMemberName = new StringBuffer();
		if (title != null) {
			groupMemberName.append(title.trim());
			groupMemberName.append(" ");
		}
		if (givenNames != null) {
			groupMemberName.append(extractFirstName(givenNames));
			groupMemberName.append(" ");
		}
		if (surname != null) {
			groupMemberName.append(surname.trim());
		}
		if (groupMemberName.length() > 0) {
			return groupMemberName.toString().toUpperCase();
		} else {
			return "??? UNKNOWN ???";
		}

	}

	private String getMemberDiscounts(Integer memberId, TransactionGroup transGrp) throws RenewalNoticeException {
		String discString = "";
		Vector groupTransList = transGrp.getTransactionList();
		MembershipTransactionVO trans = null;
		double discounts = 0;
		double credits = 0;
		{
			// try

			for (int x = 0; x < groupTransList.size(); x++) {
				trans = (MembershipTransactionVO) groupTransList.get(x);
				if (trans.getMembershipID().equals(memberId)) {
					// discounts = trans.getNonDiscretionaryDiscountApplied();
					// credits = trans.getCreditDiscountApplied();
					// discounts = discounts - credits;
					// if(discounts !=0) discString =
					// CurrencyUtil.formatDollarValue(discounts) +
					// " (Discounts) ";
					// if(credits != 0) discString +=
					// CurrencyUtil.formatDollarValue(credits) + " (Credits) ";
					try {
						discString = getDiscountDetails(trans);
					} catch (RemoteException e) {
						throw new RenewalNoticeException("Error gettingdiscount details: " + e.getMessage());
					}

					break;
				}
			}
		}

		if (!discString.equals("")) {
			discString = "(Fee includes: " + discString + ")";
		}
		return discString;
	}

	private String getDiscountDetails(MembershipTransactionVO memTransVo) throws RemoteException {
		String discountDetail = "";
		Collection feeList = memTransVo.getTransactionFeeList();
		if (feeList == null) {
			return discountDetail;
		}
		Iterator feeIt = feeList.iterator();
		MembershipTransactionFee memFee = null;
		ArrayList discountList = null;
		MembershipTransactionDiscount memTransDisc = null;
		String discDescription = null;
		DiscountTypeCache dtCache = DiscountTypeCache.getInstance();
		while (feeIt.hasNext()) {
			memFee = (MembershipTransactionFee) feeIt.next();
			discountList = memFee.getDiscountList();
			if (discountList != null) {
				for (int x = 0; x < discountList.size(); x++) {
					memTransDisc = (MembershipTransactionDiscount) discountList.get(x);
					discDescription = memTransDisc.getDiscountCode();
					discDescription = dtCache.getDiscountType(discDescription).getDescription();
					if (memTransDisc.getDiscountAmount() > 0.005)// ?
					{
						discountDetail += " " + discDescription + "(" + CurrencyUtil.formatDollarValue(memTransDisc.getDiscountAmount()) + ")";
					}
				}
			}
		}
		return discountDetail.trim();
	}

	/**
	 * appends vehicle details (or blank fields) to the stringBuffer provided for the specified membershipId
	 * 
	 * @param membershipId The feature to be added to the VehicleDetails attribute
	 * @param memText The feature to be added to the VehicleDetails attribute
	 * @exception Exception Description of the Exception
	 */
	/*
	 * private void addVehicleDetails(int membershipId, StringBuffer memText) throws Exception { StringBuffer vSql = new StringBuffer(); //select discrete columns vSql.append("Select * from PUB.mem_membership_vehicle"); vSql.append("where membership_id = ?"); PreparedStatement vStatement = null; Connection connection = null; int year = 0; String make = null; String model = null; try { connection = dataSource.getConnection(); vStatement = connection.prepareStatement(vSql.toString()); vStatement.setInt(1, membershipId); ResultSet vRs = vStatement.executeQuery(); if(vRs.next()) { year = vRs.getInt("year_built"); make = vRs.getString("make"); model = vRs.getString("model"); memText.append(year); memText.append(DELIMITER); memText.append(make); memText.append(DELIMITER); memText.append(model); memText.append(DELIMITER); //System.out.println("Vehicle " + year + " " + make + " " + model); } else { memText.append(DELIMITER); memText.append(DELIMITER); memText.append(DELIMITER); } } catch(Exception e) {
	 * e.printStackTrace(); } finally { ConnectionUtil.closeStatement(vStatement); ConnectionUtil.closeConnection(connection); } }
	 */

	/**
	 * Check to see if a new card will be required. Returns true if so Creates new card details and document.
	 * 
	 * @param memVO Description of the Parameter
	 * @param cardText Description of the Parameter
	 * @param newMembershipExpiryDate Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RenewalNoticeException Description of the Exception
	 */
	private Hashtable<String, Serializable> constructCardDetails(MembershipVO memVO, String docType) throws RenewalNoticeException {

		boolean memberNeedsCard = false;
		Hashtable<String, Serializable> cardDetails = new Hashtable<String, Serializable>();
		StringBuffer cardText = new StringBuffer();
		MembershipCardVO cardVO = null;

		Hashtable<String, Comparable> tier = getRenewalTierCodeAndDescription(memVO);

		// populate the values
		String tierCode = (String) tier.get(KEY_TIER_CODE);
		String tierDescription = (String) tier.get(KEY_TIER_DESCRIPTION);
		MembershipTier renewalTier = (MembershipTier) tier.get(KEY_RENEWAL_TIER);

		if (MembershipDocument.RENEWAL_ADVICE.equals(docType) || MembershipDocument.RENEWAL_NOTICE.equals(docType)) {

			LogUtil.debug(this.getClass(), "constructCardDetails: " + memVO.getClientNumber());

			boolean issueTierBasedCards = false;
			DateTime loyaltySchemeLaunchDate = null;
			boolean issueTierBasedCardsOnRenewal = false;
			DateTime now = new DateTime().getDateOnly();
			String IIN = null;
			try {
				IIN = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_CARD_IIN);

				issueTierBasedCardsOnRenewal = Boolean.valueOf(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_TIER_BASED_CARDS_ON_RENEWAL)).booleanValue();
				loyaltySchemeLaunchDate = new DateTime(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.LOYALTY_SCHEME_LAUNCH_DATE));
				issueTierBasedCards = Boolean.valueOf(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_TIER_BASED_CARDS)).booleanValue();

			} catch (Exception e) {
				throw new RenewalNoticeException("Error getting System Parameter : ", e);
			}

			LogUtil.log(this.getClass(), "IIN=" + IIN);
			LogUtil.log(this.getClass(), "issueTierBasedCards=" + issueTierBasedCards);
			LogUtil.log(this.getClass(), "loyaltySchemeLaunchDate=" + loyaltySchemeLaunchDate);

			ArrayList cardList = null;
			try {
				// Returns a list of MembershipCardVO objects
				cardList = membershipMgrLocal.getMembershipCardList(memVO.getMembershipID());
			} catch (RemoteException e) {
				throw new RenewalNoticeException("Error getting card list : " + e);
			}

			// check if they need a new card
			if (cardList == null || cardList.isEmpty()) {
				// don't create a request as part of the renewal anymore unless
				// it is tier related
				if (issueTierBasedCards && issueTierBasedCardsOnRenewal &&
				// is eligible for loyalty scheme
				renewalTier != null && renewalTier.isEligibleForLoyaltyScheme() &&
				// today is on or after program live date
				now.onOrAfterDay(loyaltySchemeLaunchDate)) {
					memberNeedsCard = true;
					cardVO = createNewCard(cardVO, memVO, IIN, ReferenceDataVO.CARD_REQUEST_TIER_CHANGE, false);
				}
			} else {
				// Sort the card list in descending request date order and get
				// the top
				// one of the list.
				Collections.reverse(cardList);
				cardVO = (MembershipCardVO) cardList.get(0);

				// If a card request already exists then turn it into an issued
				// card and send it with the renewal notice.
				if (!cardVO.isIssued() && !issueTierBasedCards // only applies
				// to before the
				// scheme
				) {
					memberNeedsCard = true;
					// Find the expiry date of the previously issued card if
					// there was one
					DateTime oldCardExpiryDate = null;
					if (cardList.size() > 1) {
						// The top card on the list was a request so the next
						// card should be
						// the last issued card.
						oldCardExpiryDate = ((MembershipCardVO) cardList.get(1)).getExpiryDate();
					}

					try {
						cardVO.setReadOnly(false);
						cardVO.setProduct(memVO.getProduct());
						cardVO.setCardNumber(IIN, memVO.getMembershipNumber(7));
						// Base the new card expiry date on the old card expiry
						// date if there
						// was one or use the memberships current expiry date.
						cardVO.setExpiryDate(getNewCardExpiryDate(oldCardExpiryDate, memVO.getExpiryDate()));
						cardVO.setIssueDate(new DateTime());
						cardVO.setJoinDate(memVO.getJoinDate());
						cardVO.setCardName(CardHelper.getCardName(memVO.getClient(), memVO.getMembershipProfile().getNameSuffix()));
						// tier code
						cardVO.setTierCode(tierCode); // what if null?
						// Save the changes
						membershipMgrLocal.updateMembershipCard(cardVO);
					} catch (ValidationException ve) {
						throw new RenewalNoticeException("Error updating membership card request : " + ve);
					} catch (RemoteException re) {
						throw new RenewalNoticeException("Error updating membership card request : " + re);
					}
				} else if (cardVO.isIssued() && // last card is issued
				issueTierBasedCards && // if issuing enabled
				issueTierBasedCardsOnRenewal) // issuing enabled on
				// renewal
				{

					try {
						LogUtil.debug(this.getClass(), memVO.getClientNumber() + ": Tier tests");
						LogUtil.debug(this.getClass(), "1: " + (renewalTier != null));
						LogUtil.debug(this.getClass(), "1A: " + renewalTier.getTierCode());
						LogUtil.debug(this.getClass(), "2: " + (!renewalTier.getTierCode().equals(cardVO.getTierCode())));
						LogUtil.debug(this.getClass(), "3: " + renewalTier.isEligibleForLoyaltyScheme());
						LogUtil.debug(this.getClass(), "4: " + cardVO.getIssueDate().onOrBeforeDay(loyaltySchemeLaunchDate));

						LogUtil.debug(this.getClass(), "5: " + cardVO.getIssueDate());
						LogUtil.debug(this.getClass(), "6: " + cardVO.getTierCode());
					} catch (Exception ex1) {
						LogUtil.warn(this.getClass(), "Unable to log tier tests. " + ex1);
					}

					// Issue one if the tier has changed and they don't already
					// have a corresponding tier card.
					if (renewalTier != null &&
					// The tier for their last card does not equal the renewal
					// tier - assume card data is converted.
					(cardVO.getTierCode() != null && !renewalTier.getTierCode().equals(cardVO.getTierCode()) ||
					// tier is greater or equal to 25 years
					(renewalTier.isEligibleForLoyaltyScheme() &&
					// last card issue date was on or before the launch
					// of the scheme
					cardVO.getIssueDate().onOrBeforeDay(loyaltySchemeLaunchDate)))) {
						LogUtil.debug(this.getClass(), "New card required.");
						memberNeedsCard = true;
						cardVO = createNewCard(cardVO, memVO, IIN, ReferenceDataVO.CARD_REQUEST_TIER_CHANGE, false);
					}

				} 
				else if (cardVO.isIssued() && renewalTier != null && renewalTier.isEligibleForLoyaltyScheme()) 
				{
					try 
					{
					    	if ( cardVO.getJoinDate() == null )
					    	{
					    	    cardVO.setJoinDate(memVO.getJoinDate());
					    	}
					    	
						if (!cardVO.getTier().isEligibleForLoyaltyScheme()) 
						{
							cardVO = createNewCard(cardVO, memVO, IIN, ReferenceDataVO.CARD_REQUEST_TIER_CHANGE, true);
						}
						
					} 
					catch (RemoteException ex2) 
					{
						throw new RenewalNoticeException(ex2);
					}
				}

				// CR118 Do not reissue a card based on expiry date.
				// // Create a new card if the current card will expire in the
				// next membership term
				// else
				// if(cardVO.getExpiryDate().beforeDay(newMembershipExpiryDate))
				// {
				// memberNeedsCard = true;
				//
				// // Create a new card
				// cardVO = createNewCard(cardVO.getExpiryDate(), memVO, IIN);
				// }
			}
		}
		if (memberNeedsCard) {
			String joined = Integer.toString(DateUtil.getYear(memVO.getJoinDate()));
			try {
				// card details for output file
				cardText.append(cardVO.getCardNumber() + DELIMITER + cardVO.getCardName() + DELIMITER + memVO.getMembershipNumber() + DELIMITER + joined + DELIMITER + tierCode + DELIMITER + tierDescription + DELIMITER);

				// add mag track 1
				cardText.append("%B" + cardVO.getCardNumber() + "^"
				// + cardTrackName
				+ CardHelper.getTrackName(memVO.getClient()) + "^" + DateUtil.formatDate(cardVO.getExpiryDate(), "MMyy") + "711" + memVO.getProduct().getProductIdentifier() + tierCode + joined + "?" + DELIMITER);

				// add mag track 2
				cardText.append(cardVO.getCardNumber() + "=" + DateUtil.formatDate(cardVO.getExpiryDate(), "MMyy") + "711" + memVO.getProduct().getProductIdentifier() + tierCode + "00000000000" + DELIMITER);
			} catch (RemoteException e) {
				throw new RenewalNoticeException("Error getting client's details: " + e.getMessage());
			}
		} else {
			cardText.append(DELIMITER + DELIMITER + DELIMITER + DELIMITER + tierCode + DELIMITER + tierDescription + DELIMITER + DELIMITER + DELIMITER);
		}

		cardDetails.put(KEY_REQUIRES_NEW_CARD, new Boolean(memberNeedsCard));
		cardDetails.put(KEY_CARD_DATA, cardText);

		return cardDetails;
	}

	// end of check card currency
	private final String KEY_CARD_DATA = "Card Data";

	public boolean isSocialMember(Integer memId) throws RemoteException

	{
		String sql = "select * from PUB.mem_membership_product_option ";
		sql += " where effective_date < ? ";
		sql += " and product_benefit_code = ?";
		sql += " and membership_id  = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		DateTime today = new DateTime();
		today = today.getDateOnly();
		boolean isSocial = false;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setDate(1, today.toSQLDate());
			statement.setString(2, ProductBenefitTypeVO.PBT_SOCIAL);
			statement.setInt(3, memId.intValue());

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				isSocial = true;
			}
		} catch (SQLException e) {
			throw new RemoteException("Error getting Social Membership: " + e.getMessage());
		} finally {
			ConnectionUtil.closeConnection(connection);
			ConnectionUtil.closeStatement(statement);

		}
		return isSocial;
	}

	private void rollback(UserTransaction userTransaction) {
		try {
			if (userTransaction != null && userTransaction.getStatus() != Status.STATUS_NO_TRANSACTION) {
				userTransaction.rollback();
			}
		} catch (Exception ex) {
			LogUtil.warn(this.getClass(), ex);
		}
	}

	public static final String KEY_TIER_DESCRIPTION = "description";
	public static final String KEY_TIER_CODE = "code";
	public static final String KEY_RENEWAL_TIER = "renewalTier";

	public static Hashtable<String, Comparable> getRenewalTierCodeAndDescription(MembershipVO memVO) throws RenewalNoticeException {
		String tierDescription = "";
		String tierCode = "";
		Hashtable<String, Comparable> tier = new Hashtable<String, Comparable>();
		MembershipTier renewalTier = null;

		try {
			renewalTier = memVO.getRenewalMembershipTier(); // tier at expiry

			if (renewalTier != null) {
				tierCode = renewalTier.getTierCode();
				// append tier description if there is one
				if (renewalTier.getTierDescription() != null && !renewalTier.getTierDescription().trim().equals("")) {
					// LogUtil.debug(this.getClass(),"tierDescription 1");
					tierDescription = renewalTier.getTierDescription() + ". ";
					// LogUtil.debug(this.getClass(),"tierDescription 2"+tierDescription);
				}
			} else {
				tierCode = String.valueOf(MembershipTier.TIER_CODE_DEFAULT);
			}

		} catch (RemoteException ex) {
			throw new RenewalNoticeException("Error getting renewal membership tier : " + ex.getMessage());
		}

		tierDescription += "Member since " + memVO.getJoinDate().getDateYear() + ". ";

		tier.put(KEY_TIER_DESCRIPTION, tierDescription);
		tier.put(KEY_TIER_CODE, tierCode);
		if (renewalTier != null) {
			tier.put(KEY_RENEWAL_TIER, renewalTier);
		}
		LogUtil.debug("getRenewalTierCodeAndDescription", "tier=" + tier.toString());
		return tier;
	}

	public Vector<RenewalBatchVO> getAllRenewalBatches() throws RemoteException {
		Vector<RenewalBatchVO> batchList = new Vector<RenewalBatchVO>();
		String sql = "select run_number from PUB.mem_renewal_run order by run_number desc";
		final int RECORD_LIMIT = 50;
		PreparedStatement statement = null;
		Connection connection = null;
		RenewalBatchVO batch = null;
		int runNumber = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			// LogUtil.log(this.getClass(),"getAllRenewalBatches 1A");
			ResultSet rs = statement.executeQuery();
			// LogUtil.log(this.getClass(),"getAllRenewalBatches 1B");
			int counter = 0;
			while (rs.next() && counter < RECORD_LIMIT) {
				counter++;
				runNumber = rs.getInt("run_number");
				batch = this.getBatchVO(runNumber, false);
				batchList.add(batch);
			}
			// LogUtil.log(this.getClass(),"getAllRenewalBatches 2");
		} catch (Exception e) {
			throw new RemoteException("Unable to get list of renewal run batches: " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return batchList;
	}

	/**
	 * Currently checks for life members as part of a group
	 */
	private boolean isProblemRenewal(MembershipVO memVO) throws RemoteException {
		boolean isProblem = false;
		MembershipVO thisMem = null;
		if (memVO.isGroupMember()) {
			Collection group = memVO.getMembershipGroup().getMembershipGroupDetailList();
			Iterator it = group.iterator();
			while (it.hasNext()) {
				thisMem = ((MembershipGroupDetailVO) it.next()).getMembership();
				if (MembershipProfileVO.STATUS_LIFE.equals(thisMem.getMembershipProfileCode())) {
					isProblem = true;
					break;
				}
			}
		}
		return isProblem;
	}

	public RenewalBatchVO getBatchVO(int runNumber, boolean loadBatchTotals) throws RemoteException {
		// LogUtil.log(this.getClass(),"getBatchVO 1 "+runNumber);
		final String SQL_BATCH = "select * from pub.mem_renewal_run where run_number = ?";
		final String SQL_BATCH_COUNT = "select document_type_code, printed, emailed, emailDate from pub.mem_document" + " where run_number = ?";
		PreparedStatement statement = null;
		Connection connection = null;
		RenewalBatchVO batch = null;
		java.sql.Date tDate = null;
		String tRunStatus = null;
		String tDocTypeCode = null;
		java.sql.Date emailDate;
		boolean emailed = false;
		boolean printed = false;
		int counter = 0;
		int tCount = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_BATCH);
			statement.setInt(1, runNumber);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				batch = new RenewalBatchVO();
				batch.setRunNumber(new Integer(runNumber));
				batch.setRunBy(rs.getString("run_by"));
				tDate = rs.getDate("run_date");
				if (tDate != null) {
					batch.setRunDate(new DateTime(tDate));
				}
				tDate = rs.getDate("first_Date");
				if (tDate != null) {
					batch.setFirstDate(new DateTime(tDate));
				}
				tDate = rs.getDate("last_date");
				if (tDate != null) {
					batch.setLastDate(new DateTime(tDate));
				}
				tRunStatus = rs.getString("run_status");
				if (tRunStatus == null) {
					batch.setRunStatus("Not Known");
				} else {
					batch.setRunStatus(tRunStatus);
				}
				rs.close();

				if (loadBatchTotals) {
					statement = connection.prepareStatement(SQL_BATCH_COUNT);
					statement.setInt(1, runNumber);
					rs = statement.executeQuery();
					while (rs.next()) {
						counter++;

						LogUtil.log(this.getClass(), "counter=" + counter);

						printed = rs.getBoolean(2);
						emailed = rs.getBoolean(3);
						emailDate = rs.getDate(4);
						tDocTypeCode = rs.getString(1);
						if (tDocTypeCode.equals(MembershipDocument.RENEWAL_NOTICE)) {
							batch.setNoticeCount(batch.getNoticeCount() + 1);
						} else if (tDocTypeCode.equals(MembershipDocument.RENEWAL_ADVICE)) {
							batch.setAdviceCount(batch.getAdviceCount() + 1);
						} else if (tDocTypeCode.equals(MembershipDocument.RENEWAL_REMINDER)) {
							batch.setReminderCount(batch.getReminderCount() + 1);
						} else if (tDocTypeCode.equals(MembershipDocument.RENEWAL_FINAL_NOTICE)) {
							batch.setFinalsCount(batch.getFinalsCount() + 1);
						} else if (tDocTypeCode.equals(MembershipDocument.RENEWAL_UNFINANCIAL_GROUP_MEMBER_ADVICE)) {
							batch.setUnfinancialsCount(batch.getUnfinancialsCount() + 1);
						}
						if (printed) {
							batch.setPrintCount(batch.getPrintCount() + 1);
						}
						if (emailed) {
							batch.setEmailCount(batch.getEmailCount() + 1);
							if (emailDate != null) {
								batch.setEmailSentCount(batch.getEmailSentCount() + 1);
							}
						}

						LogUtil.log(this.getClass(), "batch=" + batch);

					}
					rs.close();
				}

			}

		} catch (Exception e) {
			throw new RemoteException("Error extracting batch data: " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
			// ConnectionUtil.closeStatement(statement);
		}
		// LogUtil.log(this.getClass(),"getBatchVO 3");
		return batch;
	}

	/**
	 * lookup previous renewal run to get last date and run number, increments both and sets runNumber and lastDate for this run
	 */
	public RenewalBatchVO getLastRun() throws RenewalNoticeException, RemoteException {
		RenewalBatchVO batch = null;
		String sql = "select max(run_number) " + " from pub.mem_renewal_run";
		PreparedStatement statement = null;
		Connection connection = null;
		int runNumber = 0;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				runNumber = rs.getInt(1);
				batch = getBatchVO(runNumber, false);
			} // if not found, use defaults
			rs.close();
		} catch (SQLException sqle) {
			throw new RenewalNoticeException("Error retrieving previous run record " + sqle);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return batch;
	}

	/*
	 * *************** end of internal class*************************************
	 */
	/**
	 * @todo determine if this includes free memberships?
	 */
	public Collection<MembershipVO> getAutoRenewables(DateTime firstDate, DateTime lastDate) throws RemoteException {
		LogUtil.debug(this.getClass(), "firstDate=" + firstDate);
		LogUtil.debug(this.getClass(), "lastDate=" + lastDate);
		final String SQL_AUTO_RENEWABLES = "\n select distinct mem.membership_id from mem_membership mem, mem_document doc" + "\n where doc.membership_id = mem.membership_id" + "\n and mem.expiry_date > ? and mem.expiry_date < ? " // before
		// today
				+ "\n and (doc.document_status is null or doc.document_status = '')" + "\n and doc.document_type_code = '" + MembershipDocument.RENEWAL_ADVICE + "'" // dd
				// only
				+ "\n and doc.mem_date = mem.expiry_date" + "\n and mem.membership_status in ('" + MembershipVO.STATUS_ACTIVE + "', '" + MembershipVO.STATUS_ONHOLD + "') " + "\n and mem.product_code not in('" + ProductVO.PRODUCT_CMO + "')" + "\n and (allow_to_lapse = ? or allow_to_lapse is null) "; // not
		// allowed
		// to
		// lapse
		LogUtil.debug(this.getClass(), "sql=" + SQL_AUTO_RENEWABLES);
		PreparedStatement statement = null;
		Connection connection = null;
		ArrayList<MembershipVO> memberList = new ArrayList<MembershipVO>();
		MembershipVO membershipVO = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_AUTO_RENEWABLES);
			statement.setDate(1, firstDate.toSQLDate());
			statement.setDate(2, lastDate.toSQLDate());
			statement.setBoolean(3, false);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				membershipVO = membershipMgrLocal.getMembership(new Integer(rs.getString(1)));
				memberList.add(membershipVO);
			}
			rs.close();
		} catch (Exception e) {
			throw new RemoteException("Error extracting renewables list: " + e);
		} finally {
			ConnectionUtil.closeConnection(connection, statement);
		}
		return memberList;
	}

	/**
	 * <p>
	 * Automatically renew memberships that:
	 * </p>
	 * 
	 * <ul>
	 * <li>Will expire between the specified dates inclusive.</li>
	 * <li>Are paid by direct debit or</li>
	 * <li>Are discounted so that they will not incur a fee; or</li>
	 * <li>Are coming off "On hold"
	 * </ul>
	 * 
	 * <p>
	 * If the from date is not specified it will default to the last run date.
	 * </p>
	 * 
	 * <p>
	 * The last run date defaults to today if it is not set in the system parameters. If the to date is not specified it will default to today.
	 * </p>
	 */
	public void processAutomaticRenewals(DateTime fromDate, DateTime toDate) throws RemoteException {

		LogUtil.log(this.getClass(), "Process Automatic renewals..................." + fromDate + " -> " + toDate);
		DateTime today = DateUtil.clearTime(new DateTime());
		if (fromDate == null) {
			// Renew memberships that have expired since the last run date
			String lastRunDateStr = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.LAST_AUTO_RENEW_RUN_DATE);
			if (lastRunDateStr == null) {
				// If there was no last run date then initialise it to today
				fromDate = today;
			} else {
				try {
					// Use the day after the last run date as the start of this
					// run
					fromDate = new DateTime(lastRunDateStr);
					fromDate = fromDate.add(new Interval("0:0:1"));
				} catch (ParseException pe) {
					// The last run date is invalid. Log the exception and use
					// today instead
					LogUtil.warn(this.getClass(), "Failed to convert automatic renewal last run date '" + lastRunDateStr + "' from system parameters. " + pe.getMessage());
					fromDate = today;
				}
			}
		}
		// Default the to date if it was not specified.
		if (toDate == null) {
			toDate = today;
		}
		try {
			doAutomaticRenewalProcessing(fromDate, toDate);
		} catch (IOException ioe) {
			throw new RemoteException("Error writing to output files.  Renewal run aborted.", ioe);
		}
	}

	private void doAutomaticRenewalProcessing(DateTime fromDate, DateTime toDate) throws RemoteException, IOException {
		LogUtil.log(this.getClass(), "doAutomaticRenewalProcessing " + fromDate + " " + toDate);
		final String RENEWAL_REASON_DD = "Direct Debit";
		final String RENEWAL_REASON_NO_AMOUNT_PAYABLE = "No amount payable";
		final String FILE_AUDIT = "autoren_audit.txt";
		final String FILE_ERROR = "autoren_error.txt";

		StringBuffer autoRenewalSummary = new StringBuffer();
		autoRenewalSummary.append("Automatic renewal run commenced ");
		autoRenewalSummary.append(DateUtil.formatLongDate(new DateTime()) + " for the period ");
		autoRenewalSummary.append(fromDate.formatShortDate() + " to " + toDate.formatShortDate() + "." + FileUtil.NEW_LINE);
		String emailTo = null;
		try {
			emailTo = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.EMAIL_AUTO_RENEWAL_RUN);
			if (emailTo == null || emailTo.equals("")) { // no email address
				throw new RemoteException("No valid email address specified.");
			}
		} catch (Exception e) {
			throw new SystemException(e);
		}

		String outputDirectoryName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DIRECTORY_RENEWAL_NOTICE);

		String todayString = DateUtil.formatYYYYMMDD(new DateTime());

		String filenameRenewals = todayString + FILE_AUDIT;
		String filenameErrors = todayString + FILE_ERROR;
		FileWriter renewals = null;
		FileWriter errors = null;
		File renewalFile = new File(outputDirectoryName, filenameRenewals);
		File errorFile = new File(outputDirectoryName, filenameErrors);
		try {
			renewals = new FileWriter(renewalFile);
			errors = new FileWriter(errorFile);
		} catch (IOException ioe) {
			LogUtil.fatal(this.getClass(), "Unable to open output files: " + ioe);
			throw new RemoteException("Unable to open output files: " + ioe);
		}

		LogUtil.log(this.getClass(), "Commencing AutoRenewal Run at " + DateUtil.formatLongDate(new DateTime()));

		// write summary data directly to the body of the email message
		int ddCount = 0;
		int noFeeCount = 0;
		int unrenewedCount = 0;
		// Find the memberships that will expire on the effective date.
		Collection<MembershipVO> membershipList = null;
		try {
			membershipList = getAutoRenewables(fromDate, toDate);
		} catch (Exception fe) {
			// throw new SystemException(fe);
			autoRenewalSummary.append("Error retrieving memberships eligible for auto renewal. " + fe.getMessage() + FileUtil.NEW_LINE);
		}
		if (!(membershipList == null) && !membershipList.isEmpty()) {
			LogUtil.log(this.getClass(), "Memberships found to auto renew: " + membershipList.size());
			MembershipVO memVO;
			TransactionGroup transGroup;
			User user = SecurityHelper.getRoadsideUser();// Roadside User with
			// MEM sales branch.

			DirectDebitAuthority ddAuthority = null;
			String renewalString = null;
			Iterator<MembershipVO> membershipListIterator = membershipList.iterator();
			while (membershipListIterator.hasNext()) {
				// LogUtil.debug(this.getClass(),"1");
				memVO = membershipListIterator.next();
				// Filter out non-prime addressees memberships. These will be
				// picked
				// up as part of membership groups that are automatically
				// renewed.
				// We should only have prime addressees here
				LogUtil.log(this.getClass(), " Renewing member number " + memVO.getClientNumber() + ".");
				if (!memVO.isPrimeAddressee()) {
					errors.write(constructAutoRenewalErrorLine(memVO.getMembershipNumber(), "Not a prime addressee."));
					unrenewedCount++;
					continue;
				}
				// Do a renewal transaction on the membership
				try {
					// this does create transactions for selected clients.
					transGroup = TransactionGroup.getRenewalTransactionGroup(memVO, user, memVO.getExpiryDate());
				} catch (Exception ve) {
					LogUtil.warn(this.getClass(), "Unable to get transaction group.");
					LogUtil.warn(this.getClass(), ve);
					// The membership was not valid for a renewal so skip it.
					errors.write(constructAutoRenewalErrorLine(memVO.getMembershipNumber(), "Unable to auto renew membership. Error: " + ve.getMessage()));
					unrenewedCount++;
					continue;
				}

				// Skip it if nothing was returned and no exception was raised.
				// Shouldn't happen.
				if (transGroup == null) {
					LogUtil.warn(this.getClass(), "No transaction group.");
					errors.write(constructAutoRenewalErrorLine(memVO.getMembershipNumber(), "No transaction group created for membership number."));
					unrenewedCount++;
					continue;
				}
				// Create the credit discounts
				// transGroup.createCreditDiscounts();

				// Calculate the amount payable
				// transGroup.calculateTransactionFee();
				ddAuthority = memVO.getDirectDebitAuthority();
				renewalString = null;
				if (ddAuthority != null) {
					// The membership has a direct debit payment method.
					// It can be renewed automatically.
					try {
						transGroup.setDirectDebitAuthority(ddAuthority, true);
					} catch (Exception e) {
						LogUtil.warn(this.getClass(), e);
						errors.write(constructAutoRenewalErrorLine(memVO.getMembershipNumber(), "Unable to set the direct debit authority. " + e.getMessage()));
						continue;
					}
					ddCount++;
					renewalString = constructAutoRenewalAuditLine(RENEWAL_REASON_DD, memVO, transGroup);

				} else if (transGroup.getAmountPayable() == 0) {
					// If the fee for the renewal is zero it can be renewed
					// automatically

					// Include Access members
					renewalString = constructAutoRenewalAuditLine(RENEWAL_REASON_NO_AMOUNT_PAYABLE, memVO, transGroup);
					noFeeCount++;
				} else {
					LogUtil.warn(this.getClass(), "Not payable by DD.");
					// catch any other conditions
					errors.write(constructAutoRenewalErrorLine(memVO.getMembershipNumber(), "Membership is not payable by direct debit and amount payable > 0."));
					unrenewedCount++;
					continue;
				}
				try {
					// new transaction.
					memTransMgr.processAutoRenewalTransaction(transGroup);
				} catch (Exception e) {
					LogUtil.warn(this.getClass(), "Unable to process transaction.");
					errors.write(constructAutoRenewalErrorLine(memVO.getMembershipNumber(), "Unable to process transaction. " + e.getMessage()));
					unrenewedCount++;
					continue;
				}
				renewals.write(renewalString);
				// flush files on each loop
				errors.flush();
				renewals.flush();
			}
		} else {
			LogUtil.warn(this.getClass(), "No memberships to renew.");
		}
		// Update the last run date.
		LogUtil.log(this.getClass(), "Updating last run date to " + toDate.formatShortDate());
		commonMgr.setSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.LAST_AUTO_RENEW_RUN_DATE, toDate.formatShortDate());
		final int RIGHT_PAD = 40;
		autoRenewalSummary.append(FileUtil.NEW_LINE);
		autoRenewalSummary.append(StringUtil.rightPadString("Direct debit memberships renewed:", RIGHT_PAD) + ddCount);
		autoRenewalSummary.append(FileUtil.NEW_LINE);
		autoRenewalSummary.append(StringUtil.rightPadString("No fee memberships renewed:", RIGHT_PAD) + noFeeCount);
		autoRenewalSummary.append(FileUtil.NEW_LINE);
		autoRenewalSummary.append(StringUtil.rightPadString("Not renewed:", RIGHT_PAD) + unrenewedCount);
		// close the files
		errors.close();
		renewals.close();

		LogUtil.info(this.getClass(), "Finished automatic renewal run at " + new DateTime().formatLongDate() + ".");
		LogUtil.info(this.getClass(), autoRenewalSummary.toString());

		// now email the files to ?
		Hashtable<String, String> files = new Hashtable<String, String>();
		files.put(filenameRenewals, renewalFile.getAbsolutePath());
		files.put(filenameErrors, errorFile.getAbsolutePath());

		try {
			MailMessage message = new MailMessage();
			message.setRecipient(emailTo);
			message.setSubject("Automatic Membership Renewals " + todayString);
			message.setMessage(autoRenewalSummary.toString());
			message.setFiles(files);
			mailMgrLocal.sendMail(message);
		} catch (Exception e) {
			throw new RemoteException("Unable to email auto renewal summary.", e);
		}
	}

	private String constructAutoRenewalAuditLine(String auditDescription, MembershipVO memVO, TransactionGroup transGroup) throws RemoteException {
		StringBuffer auditString = new StringBuffer();
		auditString.append(memVO.getMembershipNumber());
		auditString.append(FileUtil.SEPARATOR_COMMA);
		auditString.append(auditDescription);
		auditString.append(FileUtil.SEPARATOR_COMMA);
		auditString.append(memVO.getExpiryDate());
		auditString.append(FileUtil.SEPARATOR_COMMA);
		auditString.append(String.valueOf(transGroup.getAmountPayable()));
		auditString.append(FileUtil.SEPARATOR_COMMA);
		auditString.append((memVO.getMembershipGroup() == null ? "1" : String.valueOf(memVO.getMembershipGroup().getMemberCount())));
		auditString.append(FileUtil.SEPARATOR_COMMA);
		auditString.append(memVO.getProductCode());
		auditString.append(FileUtil.NEW_LINE);
		return auditString.toString();
	}

	private String constructAutoRenewalErrorLine(String membershipNumber, String errorDescription) {
		StringBuffer errorString = new StringBuffer();
		errorString.append(membershipNumber);
		errorString.append(FileUtil.SEPARATOR_COMMA);
		errorString.append(errorDescription);
		errorString.append(FileUtil.NEW_LINE);
		return errorString.toString();
	}

	public final static String LAST_RUN_END_DATE = "Last Run End";

	public final static String LAST_RUN_DATE = "Last Run End";

	public final static String LAST_RUN_START_DATE = "Last Run Start";

	public final static String LAST_RUN_NUMBER = "Last Run Number";

	public final static String LAST_RUN_STATUS = "Last Run Status";

}

/**
 * Internal class RenewalParameters Holds FileWriters and other values which are used throughout the renewal process
 */
class RenewalParameters implements Serializable {

	private FileWriter logFile = null;

	private String logFileName = null;

	private FileWriter withCardFile = null;

	private int countWithCard = 0;

	public int getCountWithCard() {
		return this.countWithCard;
	}

	public void writeLogLine(String line) throws IOException {
		this.logFile.write(line);
	}

	private String formatClientNumberForExtract(Integer clientNumber) {
		final String CLIENT_NUMBER_UNKNOWN = "Unknown";
		String clientNumberString = null;
		if (clientNumber == null || clientNumber.intValue() == 0) {
			clientNumberString = CLIENT_NUMBER_UNKNOWN;
		} else {
			clientNumberString = clientNumber.toString();
		}
		return clientNumberString;
	}

	public void writeErrorLine(Integer clientNumber, String docType, String errorCategory, String errorMessage) throws IOException {
		StringBuffer line = new StringBuffer();
		line.append(formatClientNumberForExtract(clientNumber));
		line.append(RenewalMgrBean.DELIMITER);
		line.append(docType);
		line.append(RenewalMgrBean.DELIMITER);
		line.append(errorCategory);
		line.append(RenewalMgrBean.DELIMITER);
		line.append(errorMessage);
		line.append(FileUtil.NEW_LINE); // new line

		this.errorsFile.write(line.toString());
	}

	public void writeUnrenewedLine(Integer clientNumber, String docType, String message, DateTime expiryDate, String clientStatus, String allowToLapse) throws IOException {
		StringBuffer line = new StringBuffer();
		line.append(formatClientNumberForExtract(clientNumber));
		line.append(RenewalMgrBean.DELIMITER);
		if (docType != null) {
			line.append(docType);
		} else {
			line.append(RenewalMgrBean.DOCUMENT_TYPE_UNKNOWN);
		}
		line.append(RenewalMgrBean.DELIMITER);
		line.append(expiryDate);
		line.append(RenewalMgrBean.DELIMITER);
		line.append(clientStatus);
		line.append(RenewalMgrBean.DELIMITER);
		if (allowToLapse != null) {
			line.append(allowToLapse);
		}
		line.append(RenewalMgrBean.DELIMITER);
		line.append(message);
		line.append(FileUtil.NEW_LINE); // new line

		this.unrenewedFile.write(line.toString());
	}

	public void writeSummaryLine(String col1, double col2, double col3, double col4, double col5) throws IOException {
		writeSummaryLine(col1, String.valueOf(col2), String.valueOf(col3), String.valueOf(col4), String.valueOf(col5));
	}

	public void writeSummaryLine(String col1, int col2, int col3, int col4, int col5) throws IOException {
		writeSummaryLine(col1, String.valueOf(col2), String.valueOf(col3), String.valueOf(col4), String.valueOf(col5));
	}

	public void writeSummaryLine(String col1, String col2, String col3, String col4, String col5) throws IOException {
		LogUtil.debug(this.getClass(), "col1=" + col1);
		StringBuffer summaryLine = new StringBuffer();
		summaryLine.append(col1);
		summaryLine.append(RenewalMgrBean.DELIMITER);
		summaryLine.append(col2);
		summaryLine.append(RenewalMgrBean.DELIMITER);
		summaryLine.append(col3);
		summaryLine.append(RenewalMgrBean.DELIMITER);
		summaryLine.append(col4);
		summaryLine.append(RenewalMgrBean.DELIMITER);
		summaryLine.append(col5);
		summaryLine.append(FileUtil.NEW_LINE);
		this.summaryFile.write(summaryLine.toString());
	}

	public void writeWithCardLine(String line) throws IOException {
		this.withCardFile.write(line);
	}

	public void writeWithoutCardLine(String line) throws IOException {
		this.withoutCardFile.write(line);
	}

	private FileWriter withoutCardFile = null;

	public FileWriter getWithoutCardFile() {
		return this.withoutCardFile;
	}

	public FileWriter getWithCardFile() {
		return this.withCardFile;
	}

	private int countWithoutCard = 0;

	public int getCountWithoutCard() {
		return this.countWithoutCard;
	}

	public void incrementCountWithoutCard() {
		this.countWithoutCard++;
	}

	public void incrementCountWithCard() {
		this.countWithCard++;
	}

	private FileWriter unrenewedFile = null;

	private int countUnrenewed = 0;

	private FileWriter errorsFile = null;

	private FileWriter summaryFile = null;

	private User user = null;

	public User getUser() {
		return this.user;
	}

	private DateTime firstDate = null;

	public DateTime getFirstDate() {
		return this.firstDate;
	}

	private DateTime lastDate = null;

	public DateTime getLastDate() {
		return this.lastDate;
	}

	private DateTime reminderDate = null;

	public DateTime getReminderDate() {
		return this.reminderDate;
	}

	private DateTime finalNoticeDate = null;

	public DateTime getFinalNoticeDate() {
		return this.finalNoticeDate;
	}

	private String emailTo = null;

	private String ccTo = null;

	private Integer runNumber = null;

	public Integer getRunNumber() {
		return this.runNumber;
	}

	private DateTime runDate = null;

	public DateTime getRunDate() {
		return this.runDate;
	}

	private String runUserId = null;

	public String getRunUserId() {
		return this.runUserId;
	}

	private String outputDirectoryName = null;

	private String withCardsFileName = null;

	private String withoutCardsFileName = null;

	private String unrenewedFileName = null;

	private String errorsFileName = null;

	private String summaryFileName = null;

	private Integer ivrPrefix = null;

	public Integer getIVRPrefix() {
		return this.ivrPrefix;
	}

	/**
	 * RenewalParameters Public constructor looks up values and initialises itself
	 */
	public RenewalParameters(DateTime lastDate, User user, String runUserId) throws RenewalNoticeException, RemoteException {
		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		LogUtil.log(this.getClass(), "lastDate=" + lastDate);
		if (ivrPrefix == null) {
			try {
				ivrPrefix = commonMgrLocal.getSystemParameter(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_BILLPAY_PREFIX).getNumericValue1().intValue();
			} catch (Exception e) {
				throw new RenewalNoticeException("Error retrieving BillPay prefix : " + e);
			}
			if (ivrPrefix == null) {
				throw new RenewalNoticeException("Unable to retrieve BillPay prefix from gn-table.");
			}
		}

		String intervalSpec = null;
		this.runUserId = runUserId;
		this.runDate = new DateTime();
		this.user = user;
		Interval inRenewalPeriod = null;
		this.emailTo = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.EMAIL_MEMBERSHIP_RENEWAL_RUN);
		try {
			this.ccTo = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEMBERSHIP_RENEWAL_RUN_CCTO);
		} catch (Exception e) {
			// do nothing. If not found, leave ccTo null
		}

		intervalSpec = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.IN_RENEWAL_PERIOD);
		try {
			inRenewalPeriod = new Interval(intervalSpec);
		} catch (Exception e) {
			throw new SystemException(e);
		}

		// lookup previous mem_renewal_run to get firstDate of next run
		// and next run number
		RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
		RenewalBatchVO lastRunDetails = renewalMgr.getLastRun();

		int lastRunNumber = lastRunDetails.getRunNumber().intValue();
		DateTime lastRunStartDate = lastRunDetails.getFirstDate();
		DateTime lastRunEndDate = lastRunDetails.getLastDate();
		// required for external access
		this.lastDate = lastDate;
		String lastRunStatus = lastRunDetails.getRunStatus();

		LogUtil.log(this.getClass(), "lastRunNumber=" + lastRunNumber);
		LogUtil.log(this.getClass(), "lastRunStartDate=" + lastRunStartDate);
		LogUtil.log(this.getClass(), "lastRunEndDate=" + lastRunEndDate);
		LogUtil.log(this.getClass(), "lastDate=" + this.lastDate);
		LogUtil.log(this.getClass(), "lastRunStatus=" + lastRunStatus);

		this.runNumber = new Integer(lastRunNumber + 1);

		if (lastRunEndDate == null) {
			this.firstDate = new DateTime();
		} else {
			try {
				// add one day
				this.firstDate = lastRunEndDate.add(new Interval(0, 0, 1, 0, 0, 0, 0));
			} catch (Exception e) {
				throw new SystemException(e);
			}
		}
		LogUtil.log(this.getClass(), "firstDate=" + this.firstDate);

		if (!RenewalMgrBean.RENEWAL_RUN_STATUS_COMPLETE.equals(lastRunStatus.trim())) {
			this.firstDate = lastRunStartDate;
		}
		LogUtil.log(this.getClass(), "lastDate=" + this.lastDate);
		if (this.lastDate == null) // ie not supplied as a parameter
		{
			this.lastDate = this.runDate.add(inRenewalPeriod);
		} else if (this.lastDate.onOrAfterDay(this.firstDate)) // if it is
		// supplied, it
		// must be valid
		{
			// this.lastDate = lastDate;
		} else {
			throw new RenewalNoticeException("The Date specified " + this.firstDate + " is not before the last renewal run end date " + this.lastDate);
		}
		LogUtil.log(this.getClass(), "runDate=" + this.runDate);
		try {
			// 7 days.
			this.reminderDate = runDate.subtract(new Interval(0, 0, 7, 0, 0, 0, 0));
		} catch (Exception e) {
			throw new SystemException(e);
		}

		try {
			// 21 days.
			this.finalNoticeDate = runDate.subtract(new Interval(0, 0, 21, 0, 0, 0, 0));
		} catch (Exception e) {
			throw new SystemException(e);
		}

		try {
			this.outputDirectoryName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DIRECTORY_RENEWAL_NOTICE);
			if (this.outputDirectoryName.charAt(this.outputDirectoryName.length() - 1) != '/') {
				this.outputDirectoryName += "/";
			}
			this.withCardsFileName = DateUtil.formatYYYYMMDD(this.runDate) + "renWithCards.dat";
			this.withoutCardsFileName = DateUtil.formatYYYYMMDD(this.runDate) + "renWithoutCards.dat";
			String outputFileWithCardName = this.outputDirectoryName + this.withCardsFileName;
			String outputFileWithoutCardName = this.outputDirectoryName + this.withoutCardsFileName;
			this.withCardFile = new FileWriter(outputFileWithCardName);
			this.withoutCardFile = new FileWriter(outputFileWithoutCardName);
			this.summaryFileName = DateUtil.formatYYYYMMDD(this.runDate) + "renewalSummary.dat";
			this.errorsFileName = DateUtil.formatYYYYMMDD(this.runDate) + "renewalErrors.txt";
			this.unrenewedFileName = DateUtil.formatYYYYMMDD(this.runDate) + "unrenewed.txt";
			this.summaryFile = new FileWriter(this.outputDirectoryName + this.summaryFileName);
			this.errorsFile = new FileWriter(this.outputDirectoryName + this.errorsFileName);
			this.unrenewedFile = new FileWriter(this.outputDirectoryName + this.unrenewedFileName);
			this.logFileName = this.outputDirectoryName + "renewalRun.log";
			this.logFile = new FileWriter(this.logFileName, true);
		} catch (IOException e) {
			throw new RenewalNoticeException("Error initialising RenewalParameters object: " + e.getMessage());
		}

		LogUtil.log(this.getClass(), "after construction: " + this.toString());

	}

	public void completeLogFiles() throws RenewalNoticeException {
		try {
			this.writeWithCardLine("099" + RenewalMgrBean.DELIMITER + "End of file" + RenewalMgrBean.DELIMITER + this.getCountWithCard());
			this.writeWithoutCardLine("099" + RenewalMgrBean.DELIMITER + "End of file" + RenewalMgrBean.DELIMITER + this.getCountWithoutCard());

			this.unrenewedFile.write("\nEnd of file");
			this.errorsFile.write("\nEnd of file");

			this.withCardFile.close();
			this.withoutCardFile.close();

			this.unrenewedFile.close();
			this.errorsFile.close();
			this.summaryFile.close();
			this.writeLogLine("FINISHED run " + this.getRunNumber() + " at " + DateUtil.formatLongDate(new DateTime()));

			this.logFile.close();

		} catch (IOException e) {
			throw new RenewalNoticeException("Unable to write to output file.\n " + e);
		}
	}

	public void emailResults() throws RenewalNoticeException {
		String headerText = "Membership Renewal Run " + this.runNumber + " run on " + this.runDate;
		String bodyText = "Run Number = " + this.runNumber + FileUtil.NEW_LINE + "Run Date =   " + this.runDate + FileUtil.NEW_LINE + "For the period " + this.firstDate + " to " + this.lastDate + FileUtil.NEW_LINE + FileUtil.NEW_LINE + this.toString();
		// removed files from email
		// Hashtable files = new Hashtable();
		// files.put(this.errorsFileName, this.outputDirectoryName +
		// this.errorsFileName);
		// files.put(this.summaryFileName, this.outputDirectoryName +
		// this.summaryFileName);
		// files.put(this.unrenewedFileName, this.outputDirectoryName +
		// this.unrenewedFileName);
		// files.put(this.withCardsFileName, this.outputDirectoryName +
		// this.withCardsFileName);
		// files.put(this.withoutCardsFileName, this.outputDirectoryName +
		// this.withoutCardsFileName);
		try {
			if (this.emailTo == null || this.emailTo.equals("")) { // no email
				// address
				LogUtil.fatal(this.getClass(), "No email address________");
				throw new RenewalNoticeException("No valid email address");
			}
			MailMessage message = new MailMessage();
			message.setRecipient(emailTo);
			message.setSubject(headerText);
			message.setMessage(bodyText);
			MailMgr mailMgr = CommonEJBHelper.getMailMgr();
			mailMgr.sendMail(message);

			if (this.ccTo != null && !this.ccTo.equals("")) {
				message.setBcc(ccTo);
				mailMgr.sendMail(message);
			}
		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), "Unable to email results: " + e);
			throw new RenewalNoticeException("Unable to email results: " + e);
		}
	}

	public String toString() {
		String desc = "\nRenewalParameters__________________________________________________" + "\nUser =                 " + this.runUserId + "\nrunDate =              " + DateUtil.formatShortDate(this.runDate) + "\nfirstDate =            " + DateUtil.formatShortDate(this.firstDate) + "\nlastDate =             " + DateUtil.formatShortDate(this.lastDate) + "\nreminderDate =         " + DateUtil.formatShortDate(this.reminderDate) + "\nfinalNoticeDate =      " + DateUtil.formatShortDate(this.finalNoticeDate) + "\nemailTo =              " + this.emailTo + "\nccTo =                 " + this.ccTo + "\nrunNumber =            " + this.runNumber + "\noutputDirectoryName =  " + this.outputDirectoryName + "\nfileWithCardsName =    " + this.withCardsFileName + "\nfileWithoutCardsName = " + this.withoutCardsFileName + "\nfilenameUnrenewed =    " + this.unrenewedFileName + "\nfilenameErrors =       " + this.errorsFileName + "\nfilenameSummary =      " + this.summaryFileName + "\nIVRPREFIX =            " + this.ivrPrefix + "\n___________________________________________________________________\n";
		return desc;
	}
}
