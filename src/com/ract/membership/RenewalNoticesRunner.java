package com.ract.membership;

import java.util.Date;

import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.DateTime;

/**
 * RenewalNoticesRunner for producing renewal notices in batch
 * 
 * @author Glen Wigg, jyh
 * @created 1 August 2002
 * @version 1.0
 */

public class RenewalNoticesRunner extends Exception {
    /**
     * Runs the Renewal Notice Run
     * 
     * @param providerURL
     *            Description of the Parameter
     */
    public RenewalNoticesRunner(String host, int port, DateTime lastDate) throws Exception {

	RACTPropertiesProvider.setContextURL(host, port);
	RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	String userID = "xxx";
	if (lastDate == null) {
	    lastDate = new DateTime().getDateOnly();
	}
	int numRenewals = renewalMgr.createMembershipRenewalNotices(lastDate, userID);
	System.out.println(numRenewals + " renewals processed");

    }

    /**
     * The main program for the RenewalNoticesRunner class
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(String[] args) {
	System.out.println("\n\nRenewal run commenced " + new Date());
	String host = args[0];
	int port = Integer.parseInt(args[1]);
	DateTime lastDate = null;
	try {
	    if (args.length == 3) {
		lastDate = new DateTime(args[2]);
	    }

	    new RenewalNoticesRunner(host, port, lastDate);
	    System.out.println("FINISHED " + new Date());
	} catch (Exception ex1) {
	    ex1.printStackTrace();
	    // System.err.println(ExceptionHelper.getExceptionStackTrace(ex1));
	}
    }
}
