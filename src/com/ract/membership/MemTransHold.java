package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.ValueObject;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * Title: Master Project Description: Provides specific functionality for
 * creating a "Hold" type of transaction. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @hibernate.subclass discriminator-value="HOLD"
 * 
 * @author T. Bakker
 * @version 1.0
 */
public class MemTransHold extends MembershipTransactionVO implements MemTransCreator {

    public MemTransHold() {
    }

    public MemTransHold(String transGroupTypeCode, DateTime transDate, MembershipVO oldMemVO, User user, boolean primeAddressee, DateTime memGroupJoinDate) throws RemoteException {
	super(ValueObject.MODE_CREATE, transGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD, memGroupJoinDate);

	MembershipVO newMemVO = oldMemVO.copy();
	newMemVO.setStatus(MembershipVO.STATUS_ONHOLD);
	newMemVO.setAllowToLapse(Boolean.valueOf(false));
	setTransactionDate(transDate);
	setUsername(user.getUserID());
	setSalesBranchCode(user.getSalesBranchCode());
	setMembership(oldMemVO);
	setNewMembership(newMemVO);
	setPrimeAddressee(primeAddressee);

	// Work out the unearned credit that can be used on the next transaction
	// UnearnedValue unearnedValue =
	// oldMemVO.getPaidRemainingMembershipValue();
	// LogUtil.debug(this.getClass(),"unearnedValue="+unearnedValue);
	// if(unearnedValue.getUnearnedAmount().doubleValue() > 0.0)
	// {
	// double creditAmount = newMemVO.getCreditAmount();
	// LogUtil.debug(this.getClass(),"creditAmount="+creditAmount);
	// newMemVO.setCreditAmount(creditAmount +
	// unearnedValue.getUnearnedAmount().doubleValue());
	// }

	setEffectiveDate(this.transactionDate);
	setEndDate(newMemVO.getExpiryDate());

    }

    /**
     * Set details about the period that the membership is being put on hold
     * for.
     */
    public void setHoldPeriod(DateTime endHoldDate, int holdYears, Interval remainingMembershipPeriod) throws RemoteException {
	MembershipVO newMemVO = getNewMembership();
	newMemVO.setExpiryDate(endHoldDate);
	newMemVO.setCreditPeriod(remainingMembershipPeriod);
	if (holdYears < 1) {
	    holdYears = 1;
	}
	this.holdYears = holdYears;
    }

    /**
     * Create the transaction fees applicable to a membership being put on hold.
     */
    public void createTransactionFees(int chargeableMemberCount, int memberCount) throws RemoteException {
	// join fee not applicable
	MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	MembershipVO newMemVO = getNewMembership();
	clearTransactionFeeList();

	LogUtil.log(this.getClass(), " >>------> feeList  " + getTransactionTypeCode());
	LogUtil.log(this.getClass(), " >>------> feeList  " + getProductCode());
	LogUtil.log(this.getClass(), " >>------> feeList  " + chargeableMemberCount);
	Collection feeList = refMgr.findProductFee(newMemVO.getProductCode(), newMemVO.getMembershipTypeCode(), getTransactionTypeCode(), chargeableMemberCount, getTransactionDate());

	LogUtil.log(this.getClass(), " >>------> feeList has " + feeList.size());
	// Charge each of the fees that apply
	if (feeList != null) {
	    FeeSpecificationVO feeSpecVO;
	    double feePerMember = 0.0;
	    double feePerVehicle = 0.0;
	    Iterator feeIterator = feeList.iterator();
	    while (feeIterator.hasNext()) {
		feeSpecVO = (FeeSpecificationVO) feeIterator.next();
		LogUtil.log(this.getClass(), " >>------> feeSpecVO = " + feeSpecVO.getFeeTypeCode());
		LogUtil.log(this.getClass(), " >>------> feeSpecVO.getFeePerMember() = " + feeSpecVO.getFeePerMember());
		LogUtil.log(this.getClass(), " >>------> holdYears = " + holdYears);
		feePerMember = feeSpecVO.getFeePerMember() * this.holdYears;
		feePerVehicle = feeSpecVO.getFeePerVehicle() * this.holdYears;
		MembershipTransactionFee memTransFee = new MembershipTransactionFee(feeSpecVO.getFeeSpecificationID(), feePerMember, feePerVehicle, newMemVO.getVehicleCount());
		addTransactionFee(memTransFee);
	    } // while (feeListIT.hasNext())
	} // if (feeList != null)
    }

    /**
     * 
     * The number of years (part or whole) that the membership is to be put on
     * hold.
     */
    protected int holdYears;
}
