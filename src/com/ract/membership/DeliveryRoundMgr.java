package com.ract.membership;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

/**
 * Title: Membership system Description: Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public interface DeliveryRoundMgr extends EJBObject {
    public void extractDeliveryList(String annualReportFileName, String motorNewsFileName, String accessMembersFileName, String errorFileName, String emailAddress) throws RemoteException;

}
