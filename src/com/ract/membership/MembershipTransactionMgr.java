package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import javax.ejb.Remote;
import javax.ejb.RemoveException;

import com.ract.common.ValidationException;
import com.ract.common.notifier.NotificationMgrLocal;
import com.ract.payment.FinanceVoucher;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.billpay.IVRPayment;
import com.ract.user.UserSession;

@Remote
public interface MembershipTransactionMgr {
    public void notifyPendingFeePaid(IVRPayment ivr) throws RemoteException;

    public void removeMembershipTransaction(Integer transID, UserSession userSession) throws RemoteException, com.ract.common.SecurityException, RemoveException, ValidationException;

    public Collection processTransaction(TransactionGroup transGroup, NotificationMgrLocal notificationMgrLocal, PaymentTransactionMgrLocal paymentTransactionMgrLocal) throws RemoteException, ValidationException;

    public Collection processTransaction(UndoTransactionGroup transGroup) throws RemoteException, ValidationException;

    public Collection processTransaction(TransactionGroup transGroup) throws RemoteException, ValidationException;

    public Vector getMembershipTransactionFeeList(Integer transID) throws RemoteException;

    public Vector getMembershipTransactionAdjustmentList(Integer transID) throws RemoteException;

    public Collection processAutoRenewalTransaction(TransactionGroup transGroup) throws RemoteException, ValidationException;

    public void processVoucher(FinanceVoucher creditDisposal) throws RemoteException;

    public Collection createCreditDisposal(TransactionGroup transGroup, FinanceVoucher creditDisposal) throws RemoteException;

    public ArrayList processExpireMembershipCredit(TransactionGroup transGroup) throws RemoteException;

}
