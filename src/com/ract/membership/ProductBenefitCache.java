package com.ract.membership;

//import javax.naming.*;
//import javax.rmi.PortableRemoteObject;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.CacheBase;

/**
 * This class caches a list of product benefits in memory. It uses the Singleton
 * design pattern. Use the following code to call the methods on this class.
 * 
 * ProductBenefitCache.getInstance().getProductBenefitType(<membershipTypeCode>)
 * ; ProductBenefitCache.getInstance().getProductBenefitTypeList();
 */

public class ProductBenefitCache extends CacheBase {
    private static ProductBenefitCache instance = new ProductBenefitCache();

    /**
     * Implement the getItemList method of the super class. Return the list of
     * cached items. Add items to the list if it is empty.
     */
    public void initialiseList() throws RemoteException {
	// log("Initialising ProductBenefitType cache.");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection dataList = membershipMgr.getProductBenefitTypeList();
	this.addAll(dataList);
	sort();

    }

    /**
     * Return a reference the instance of this class.
     */
    public static ProductBenefitCache getInstance() {
	return instance;
    }

    /**
     * Return the specified product benefit type value object.
     */
    public ProductBenefitTypeVO getProductBenefitType(String productBenefitTypeCode) throws RemoteException {
	return (ProductBenefitTypeVO) this.getItem(productBenefitTypeCode);
    }

    /**
     * Return the list of product benefits. A null or an empty list may be
     * returned.
     */
    public ArrayList getProductBenefitTypeList() throws RemoteException {
	ArrayList list = this.getItemList();
	if (list != null) {
	    return new ArrayList(list);
	} else {
	    return null;
	}
    }

}
