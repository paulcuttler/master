package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.CacheBase;

public class FeeTypeCache extends CacheBase {
    private static FeeTypeCache instance = new FeeTypeCache();

    /**
     * Initialise the list of cached items. Add items to the list if it is
     * empty.
     */
    public void initialiseList() throws RemoteException {
	// log("Initialising Fee type cache.");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection dataList = membershipMgr.getFeeTypes();
	this.addAll(dataList);
    }

    /**
     * Return a reference the instance of this class.
     */
    public static FeeTypeCache getInstance() {
	return instance;
    }

    /**
     * Return the specified fee type value object. Search the list of fee types
     * for one that has the same code.
     */
    public FeeTypeVO getFeeType(String feeTypeCode) throws RemoteException {
	return (FeeTypeVO) this.getItem(feeTypeCode);
    }

    /**
     * Return the list of fee types. A null or empty list may be returned
     */
    public ArrayList getFeeTypeList() throws RemoteException {
	return this.getItemList();
    }

}