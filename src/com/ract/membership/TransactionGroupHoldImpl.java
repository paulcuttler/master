package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.Account;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.Company;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.membership.ui.MembershipUIHelper;
import com.ract.payment.PayableItemComponentVO;
import com.ract.payment.PayableItemGenerator;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PostingContainer;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.user.User;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Provides implementation of methods specific to the "Hold" transaction.
 * 
 * @author T. Bakker,JYH
 * @version 1.0
 */

public class TransactionGroupHoldImpl extends TransactionGroup implements PayableItemGenerator {

    public TransactionGroupHoldImpl(User user) throws RemoteException {
	super(MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD, user);
	this.paymentMethodListToRemove = new ArrayList();
    }

    private boolean writeLog = true;

    /**
     * Generate a list of payable items that need to be sent to the payment
     * manager. The returned list will contain one new payable item which
     * contains the information required to cancel the membership. The list may
     * contain one or more existing payable items which need to be updated so
     * that they no longer reflect an amount owed or to be earned.
     */
    public ArrayList generatePayableItemList() throws RemoteException, RollBackException {
	ArrayList newPayableItemList = new ArrayList();
	Collection activeTransactionList = null;

	PayableItemVO oldPayableItem = null;
	PayableItemVO newPayableItem = null;
	PayableItemComponentVO newComponent = null;
	PostingContainer postingList = null;

	PayableItemPostingVO gstPosting = null;

	ArrayList newComponentList = null;
	MembershipTransactionVO memTxVO = null;

	// old transaction
	MembershipTransactionVO oldMemTxVO = null;

	MembershipVO memVO = null;

	PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	BigDecimal gstRate = new BigDecimal(commonMgr.getCurrentTaxRate().getGstRate());
	MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	Account gstAccount = refMgr.getMembershipGSTAccount();

	BigDecimal netTxFee = null;

	// for each membership transaction
	Collection memTxList = this.getTransactionList();
	if (memTxList == null || memTxList.size() == 0) {
	    throw new SystemException("No membership transactions found!");
	} else if (memTxList.size() != 1) {
	    throw new SystemException("A hold transaction may not be done in the context of other transactions.");
	}

	Iterator memTxListIterator = memTxList.iterator();
	if (memTxListIterator.hasNext()) {
	    memTxVO = (MembershipTransactionVO) memTxListIterator.next();

	    // get the fee for the transaction
	    netTxFee = new BigDecimal(memTxVO.getNetTransactionFee());

	    // only process the membership actually being held
	    if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(memTxVO.getTransactionTypeCode())) {
		throw new SystemException("The transaction type should be " + MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD);
	    }
	    // get the old membership
	    memVO = memTxVO.getMembership();

	    memVO = memTxVO.getMembership();

	    LogUtil.log(this.getClass(), "mem " + memVO.getMembershipNumber());

	    activeTransactionList = memVO.getCurrentTransactionList();

	    LogUtil.log(this.getClass(), "Cancel size " + (activeTransactionList != null ? activeTransactionList.size() : 0));
	    PostingContainer discList = new PostingContainer();

	    postingList = new PostingContainer();
	    if (activeTransactionList != null && activeTransactionList.size() > 0) {
		// create a new array list to store ALL postings
		double amountOutstanding = 0;
		Iterator activeTransactionListIterator = activeTransactionList.iterator();
		while (activeTransactionListIterator.hasNext()) {
		    boolean fullyUnpaid = false;
		    oldMemTxVO = (MembershipTransactionVO) activeTransactionListIterator.next();

		    LogUtil.log(this.getClass(), "test i");

		    // get payableItem

		    // potential lock?
		    oldPayableItem = oldMemTxVO.getPayableItemForPA();

		    if (oldPayableItem != null) {
			amountOutstanding = oldPayableItem.getAmountOutstanding();

			LogUtil.log(this.getClass(), "o/s=" + amountOutstanding + ",ap=" + oldPayableItem.getAmountPayable());
			if (amountOutstanding > 0.0) {
			    fullyUnpaid = oldPayableItem.isFullyUnpaid();

			    PaymentMethod paymentMethod = oldPayableItem.getPaymentMethod();
			    paymentMethod.setClientNumber(oldPayableItem.getClientNo());
			    this.addPaymentMethodToRemove(paymentMethod);
			}
		    }

		    Collection cancPostings = createHoldPostings(oldMemTxVO, fullyUnpaid);
		    LogUtil.log(this.getClass(), "createCancellationPostings 2 cancPostings=" + cancPostings);
		    postingList.addAll(cancPostings);
		    LogUtil.log(this.getClass(), "createCancellationPostings 3 postingList=" + postingList);

		    LogUtil.log(this.getClass(), "postingList =" + postingList.size());

		}
	    }

	    // hold postings only to extend the period of hold
	    postingList.addAll(createPostingsForHoldFee(memTxVO, gstRate, gstPosting, gstAccount), false);

	    // now create the component
	    if (postingList != null && postingList.size() > 0) {
		String description = "Place membership " + memTxVO.getMembership().getMembershipNumber() + " on hold.  Transaction Ref: " + memTxVO.getTransactionID();

		// Create a component for each Membership Transaction.
		try {
		    // initialise the new component list
		    newComponentList = new ArrayList();

		    newComponent = new PayableItemComponentVO(postingList, netTxFee.setScale(2, BigDecimal.ROUND_HALF_UP), description, null // componentID
			    , memTxVO.getNewMembership().getMembershipNumber() // the
									       // component's
									       // source
									       // system
									       // reference
			    , null // supersedingComponentID
		    );

		    // and add it to the list to attach to the payable item
		    newComponentList.add(newComponent);
		} catch (PaymentException e) {
		    throw new SystemException("Error creating component : " + e.getMessage(), e);
		}
	    }

	    // check for discretionary discount to process
	    ArrayList discComponentList = new ArrayList();
	    if (this.hasAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT)) {
		try {
		    PayableItemComponentVO discountComponent = paymentMgr.createPostingsForDiscretionaryDiscount(memTxVO.getTransactionID(), (TransactionGroup) this, memTxVO.getNewMembership().getMembershipNumber(), PaymentMgr.DISCRETIONARY_DISCOUNT_ADJUSTMENT);
		    if (discountComponent != null) // and it should never be
		    {
			discComponentList.add(discountComponent);
		    }
		} catch (Exception ex) {
		    throw new SystemException("Error creating postings for discretionary discount" + ex);
		}
	    }

	    // add the comp list to the new comp list
	    if (newComponentList != null) {
		newComponentList.addAll(discComponentList);
	    }

	    // now create the payable item
	    if (newComponentList != null && newComponentList.size() > 0) {
		// The product period that the payable item is for starts on the
		// preferred commence date unless overridden below.
		DateTime startDate = this.getPeriodStartDate();
		DateTime endDate = this.getPeriodEndDate();

		// the payment method is always receipting
		PaymentMethod paymentMethod = new ReceiptingPaymentMethod(memVO.getClientNumber(), SourceSystem.MEMBERSHIP);
		Integer clientBranchNumber = this.getContextClient().getBranchNumber();
		String description = MembershipUIHelper.getTransactionHeadingText(this.getTransactionGroupTypeCode());
		Integer payableItemID = null;
		try {
		    newPayableItem = new PayableItemVO(payableItemID, memVO.getClientNumber(), paymentMethod, newComponentList, SourceSystem.MEMBERSHIP, memTxVO.getTransactionID() // sourceSystemReferenceID
			    , memTxVO.getSalesBranchCode(), clientBranchNumber, memTxVO.getNewMembership().getProductCode(), memTxVO.getUsername(), this.getPeriodStartDate(), this.getPeriodEndDate(), description);

		    newPayableItemList.add(newPayableItem);
		} catch (PaymentException e) {
		    throw new SystemException("Error creating payable item : " + e.getMessage(), e);
		}

	    }
	    // now write the membership
	    // memVO.setReadOnly(true);
	}
	if (writeLog) {
	    LogUtil.log(this.getClass(), " >>------> Final postings : " + postingList);

	}
	return newPayableItemList;
    }

    private Collection createHoldPostings(MembershipTransactionVO mtVO, boolean fullyUnpaid) throws RemoteException, RollBackException {
	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	String referenceNumber = mtVO.getMembership().getMembershipNumber();
	return pMgr.createCancelTypePostings(referenceNumber, mtVO, MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD, fullyUnpaid);
    }

    /**
     * create the postings for the hold fee only.
     */
    private Collection createPostingsForHoldFee(MembershipTransactionVO memTxVO, BigDecimal gstRate, PayableItemPostingVO gstPosting, Account gstAccount// ,
    /* Collection postingList */) throws SystemException, RemoteException {

	PostingContainer postings = new PostingContainer();
	MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	Collection feeList = memTxVO.getTransactionFeeList();
	BigDecimal netTxFee = new BigDecimal(memTxVO.getNetTransactionFee());
	DateTime createDate = new DateTime();
	if (feeList != null) {
	    if (feeList.size() > 1) {
		throw new SystemException("There is more than one fee attached to a hold transaction.");
	    }

	    MembershipTransactionFee txFee = (MembershipTransactionFee) feeList.iterator().next();
	    LogUtil.debug(this.getClass(), "netTxFee=" + netTxFee);
	    // create the postings if there is a transaction fee
	    if (netTxFee.compareTo(new BigDecimal(0)) > 0) {

		if (feeList.size() < 1) {
		    throw new SystemException("There are no fees attached to a hold transaction but there is a net transaction fee.");
		}

		// the fee
		BigDecimal adminFee = CurrencyUtil.calculateGSTExclusiveAmount(netTxFee.doubleValue(), gstRate.doubleValue(), 2);
		LogUtil.debug(this.getClass(), "adminFee=" + adminFee);
		try {
		    PayableItemPostingVO newPosting = new PayableItemPostingVO(adminFee.negate().doubleValue(), txFee.getFeeSpecificationVO().getEarnedAccount(), null, createDate, createDate, Company.RACT, "Hold of membership " + memTxVO.getMembership().getMembershipNumber(), null);
		    postings.add(newPosting);
		} catch (PaymentException e) {
		    throw new SystemException("Error creating admin fee posting : " + e.getMessage(), e);
		}

		// the GST on the fee
		BigDecimal gstInFee = netTxFee.subtract(adminFee).setScale(2, BigDecimal.ROUND_HALF_UP);
		if (gstPosting != null) {
		    try {
			gstPosting.setAmount(new BigDecimal(gstPosting.getAmount()).add(gstInFee).doubleValue());
		    } catch (PaymentException e) {
			throw new SystemException("Error updating amount on GST posting : " + e.getMessage(), e);
		    }
		} else {
		    try {
			PayableItemPostingVO newPosting = new PayableItemPostingVO(gstInFee.negate().doubleValue(), gstAccount, null, createDate, createDate, Company.RACT, "GST on hold of membership " + memTxVO.getMembership().getMembershipNumber(), null);
			postings.add(newPosting);
		    } catch (PaymentException e) {
			throw new SystemException("Error creating admin fee posting : " + e.getMessage(), e);
		    }
		}

		MembershipAccountVO memClearingAccount = refMgr.getMembershipClearingAccount();

		try {
		    PayableItemPostingVO newPosting = new PayableItemPostingVO(netTxFee.doubleValue(), memClearingAccount, null, createDate, createDate, Company.RACT, "Payable fee for hold of membership " + memTxVO.getMembership().getMembershipNumber(), null);
		    postings.add(newPosting);
		} catch (PaymentException e) {
		    throw new SystemException("Error creating clearing posting : " + e.getMessage(), e);
		}
	    }
	}
	return postings;
    }

}
