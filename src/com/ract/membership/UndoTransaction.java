package com.ract.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

import com.ract.payment.FinanceVoucher;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.directdebit.DirectDebitItemStatus;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.directdebit.DirectDebitScheduleItem;
import com.ract.payment.finance.FinanceMgr;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * Holds details about the transaction being undone. The transaction list in the
 * UndoTransactionGroup class has a list of objects instantiated from this
 * class.
 * 
 * @author bakkert
 * @created 14 January 2003
 */
public class UndoTransaction implements Serializable {

    /**
     * The membership transaction being undone.
     */
    public MembershipTransactionVO undoTransVO = null;

    /**
     * The membership transaction to revert to.
     */
    public MembershipTransactionVO revertToTransVO = null;

    /**
     * The membership state being undone. This will be the current membership.
     */
    public MembershipVO undoMemVO = null;

    /**
     * The membership state to revert to. This will be a reconstruction of the
     * membership state from the history file associated with the membership
     * transaction that is being reverted back to.
     */
    public MembershipVO revertToMemVO = null;

    /**
     * Indicates if this transaction is for a payee. i.e. it has a payable item
     * ID.
     */
    private boolean payee = false;

    /**
     * Indicates if this transaction is the context transaction that the undo
     * was initated from.
     */
    private boolean contextTransaction = false;

    /**
     * Holds the payable item if the transaction being undone was a payee.
     */
    private PayableItemVO payableItemVO = null;

    /**
     * A list of reasons, as String, that the transaction cannot be undone. Null
     * if the transaction to be undone has not been analysed yet. Empty if the
     * transaction to be undone has been analysed.
     */
    private ArrayList undoFailureReasonList = null;

    /**
     * Constructor for the UndoTransaction object
     * 
     * @param undoMemTransVO
     *            Description of the Parameter
     * @param isContext
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public UndoTransaction(MembershipTransactionVO undoMemTransVO, boolean isContext) throws RemoteException {
	LogUtil.log(this.getClass(), "UndoTransaction 1");
	this.undoTransVO = undoMemTransVO;
	this.undoMemVO = this.undoTransVO.getMembership();
	this.payee = this.undoTransVO.getPayableItemID() != null;
	this.contextTransaction = isContext;
	if (this.payee) {
	    Integer payableItemID = this.undoTransVO.getPayableItemID();
	    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	    try {
		this.payableItemVO = paymentMgr.getPayableItem(payableItemID);
	    } catch (PaymentException ex) {
		throw new RemoteException("Unable to get payable item.", ex);
	    }
	}
	LogUtil.log(this.getClass(), "UndoTransaction 1a");
	findRevertToTransaction();
	LogUtil.log(this.getClass(), "UndoTransaction 2");
    }

    /**
     * Description of the Method
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    private void findRevertToTransaction() throws RemoteException {
	LogUtil.log(this.getClass(), "findRevertToTransaction 1");
	if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.undoTransVO.getTransactionTypeCode())) {
	    // Create transactions are treated differently from other
	    // transactions
	    // as there is no previous state to revert back to. Leave the revert
	    // to transaction null.
	} else {
	    // For all other transactions find the previous transaction
	    // that the membership can be reverted back to.

	    // Get the list of transactions for the membership that the undo
	    // transaction relates to.
	    LogUtil.log(this.getClass(), "findRevertToTransaction 1a");
	    MembershipVO memVO = this.undoTransVO.getMembership();
	    Collection transList = memVO.getTransactionList();

	    // Search back through the transactions for the membership for
	    // one that is prior to the transaction being undone.
	    if (transList != null && !transList.isEmpty()) {
		LogUtil.log(this.getClass(), "findRevertToTransaction 1b");
		ArrayList memTransList = new ArrayList(transList);
		// Sort the transaction list in reverse date order.
		MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE, true);
		Collections.sort(memTransList, mtc);
		LogUtil.log(this.getClass(), "findRevertToTransaction 1c");
		boolean foundUndoTrans = false;
		MembershipTransactionVO memTransVO;
		Iterator transListIterator = memTransList.iterator();
		while (transListIterator.hasNext() && this.revertToTransVO == null) {
		    memTransVO = (MembershipTransactionVO) transListIterator.next();

		    if (foundUndoTrans) {
			// In a previous pass we found the transaction we are
			// undoing
			// so now we can find the transaction to revert back to.
			if (!memTransVO.isUndone()) {
			    // We can revert back to this transaction
			    this.revertToTransVO = memTransVO;
			    LogUtil.log(this.getClass(), "findRevertToTransaction 1d");
			    // Fetch the membership state to revert back to.
			    MembershipHistory memHistory = memTransVO.getMembershipHistory();
			    LogUtil.log(this.getClass(), "findRevertToTransaction 1e");
			    if (memHistory != null) {
				// This is a little bit dodgy and is only
				// possible because
				// we know that a membership history object is
				// implemented by
				// a membership value object. Realy should have
				// a constructor
				// on MembershipVO that takes a
				// MembershipHistory object.
				this.revertToMemVO = (MembershipVO) memHistory;
			    }
			}
		    }

		    // See if we have found the transaction we are undoing
		    if (memTransVO.getTransactionID().equals(this.undoTransVO.getTransactionID())) {
			foundUndoTrans = true;
		    }
		}
	    }
	}
	LogUtil.log(this.getClass(), "findRevertToTransaction 2");
    }

    /**
     * Return true if the membership transaction to be undone can in fact be
     * undone.
     * 
     * @return The undoable value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean isUndoable() throws RemoteException {
	return getUndoFailureReasonList().isEmpty();
    }

    /**
     * Return a list of strings with the reasons why the transaction cannot be
     * undone. The list will be empty if the transaction can be undone.
     * 
     * @return The undoFailureReasonList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getUndoFailureReasonList() throws RemoteException {
	LogUtil.log(this.getClass(), "getUndoFailureReasonList 1");
	if (this.undoFailureReasonList == null) {
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 2");
	    this.undoFailureReasonList = new ArrayList();

	    checkPreviousTransaction();
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 3");
	    checkPreviousMembershipState();
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 4");
	    checkPayment();
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 5");
	    if (this.payee && this.payableItemVO != null && this.payableItemVO.postingsHaveBeenTransferred()) {
		this.undoFailureReasonList.add("The postings for the payable item have been posted.");
	    }
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 6");
	    checkCardRequests();
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 7");
	    checkMembershipGroup();
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 8");
	    checkVouchers();
	    LogUtil.log(this.getClass(), "getUndoFailureReasonList 9");
	    if (undoFailureReasonList == null) {
		undoFailureReasonList = new ArrayList();
	    }
	}
	return this.undoFailureReasonList;
    }

    /**
     * Gets the contextTransaction attribute of the UndoTransaction object
     * 
     * @return The contextTransaction value
     */
    public boolean isContextTransaction() {
	return this.contextTransaction;
    }

    /**
     * Gets the payee attribute of the UndoTransaction object
     * 
     * @return The payee value
     */
    public boolean isPayee() {
	return this.payee;
    }

    /**
     * Make sure that a previous transaction exists if we are not undoing a
     * create. And make sure that the previous transaction was created since the
     * undo facility was installed. Otherwise we cannot guarantee the state of
     * the membership group details stored in the previous transactions
     * membership history file.
     */
    private void checkPreviousTransaction() {
	LogUtil.log(this.getClass(), "checkPreviousTransaction 1");
	// boolean transferred = false;
	// try
	// {
	// transferred =
	// MembershipVO.STATUS_TRANSFERRED.equals(undoMemVO.getStatus());
	// }
	// catch(RemoteException ex)
	// {
	// this.undoFailureReasonList.add("Unable to identify if membership has a status of transferred.");
	// }

	if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.undoTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.undoTransVO.getTransactionTypeCode())) {
	    // Don't need a previous transaction
	} else {
	    // Must have a previous transaction
	    if (this.revertToTransVO == null) {
		this.undoFailureReasonList.add("The previous transaction for the membership is not available.");
	    } else {
		// If the previous transaction has a transaction group ID but
		// does not have a transaction group type code then it was
		// created
		// before the undo facility was installed and we can't guarantee
		// that the state of the membership group is accurate.
		if (this.revertToTransVO.getTransactionGroupID() > 0 && this.revertToTransVO.getTransactionGroupTypeCode() == null) {
		    this.undoFailureReasonList.add("The previous state of the membership cannot be guaranteed to be correct as the transaction group type code is missing.");
		}

		// If the last transaction was direct debit and the previous
		// transaction
		// does not have a payable item or has a payable item that is
		// not direct debit
		// then we can't undo directly.
		// Not valid for creates, etc.

		// if (this.payee)
		// {
		// try
		// {
		// if(this.payableItemVO != null &&
		// this.payableItemVO.isDirectDebit())
		// {
		// PayableItemVO revertToPayableItem =
		// revertToTransVO.getPayableItem();
		// if(revertToPayableItem == null
		// || !revertToPayableItem.isDirectDebit())
		// {
		// this.undoFailureReasonList.add(
		// "The transaction being undone which is payable by direct debit did not directly replace the previous transaction.");
		// }
		// }
		// }
		// catch(RemoteException ex)
		// {
		// this.undoFailureReasonList.add(
		// "Unable to get the payable item for the previous transaction. "
		// + ex.getMessage());
		// }
		// }

	    }
	}
	LogUtil.log(this.getClass(), "checkPreviousTransaction 2");
    }

    /**
     * Make sure that the previous membership state is valid. If the transaction
     * being undone is a "Create" transaction then no previous state is
     * required.
     */
    private void checkPreviousMembershipState() {
	LogUtil.log(this.getClass(), "checkPreviousMembershipState 1");
	// boolean transferred = false;
	// try
	// {
	// transferred =
	// MembershipVO.STATUS_TRANSFERRED.equals(undoMemVO.getStatus());
	// }
	// catch(RemoteException ex)
	// {
	// this.undoFailureReasonList.add("Unable to identify if membership has a status of transferred.");
	// }

	// Must have a previous membership state if not undoing a create.
	if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.undoTransVO.getTransactionTypeCode()) && !MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.undoTransVO.getTransactionTypeCode())) {
	    if (this.revertToMemVO == null) {
		this.undoFailureReasonList.add("The previous state of the membership is not available.");
	    }
	}

	if (this.revertToMemVO != null && this.revertToMemVO.hasException()) {
	    Vector exceptionList = this.revertToMemVO.getExceptionList();
	    Exception e;
	    for (int i = 0; i < exceptionList.size(); i++) {
		e = (Exception) exceptionList.get(i);
		this.undoFailureReasonList.add(e.getMessage());
	    }
	}
	LogUtil.log(this.getClass(), "checkPreviousMembershipState 2");
    }

    /**
     * Make sure that the transaction has not been paid.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    private void checkPayment() throws RemoteException {
	LogUtil.log(this.getClass(), "checkPayment 1");
	if (this.payee) {
	    if (this.payableItemVO == null) {
		this.undoFailureReasonList.add("Failed to get payable item for payee.");
	    } else {
		BigDecimal payable = NumberUtil.roundHalfUp(this.payableItemVO.getAmountPayable(), 2);
		BigDecimal outstanding = new BigDecimal(this.payableItemVO.getAmountOutstanding());
		if (payable.compareTo(outstanding) > 0) {
		    this.undoFailureReasonList.add("A payment has been received for the transaction.");
		}

		// If the payment method is direct debit then check that none of
		// the
		// scheduled deductions have been sent to the bank.
		// try {
		PaymentMethod paymentMethod = this.payableItemVO.getPaymentMethod();
		if (paymentMethod != null && paymentMethod.isDirectDebitPaymentMethod()) {
		    PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		    DirectDebitSchedule ddSchedule = null;
		    try {
			ddSchedule = paymentMgr.getDirectDebitSchedule(new Integer(paymentMethod.getPaymentMethodID()));
		    } catch (PaymentException ex) {
			throw new RemoteException("Unable to get direct debit schedule.", ex);
		    }
		    if (ddSchedule != null) {
			BigDecimal scheduleAmount = ddSchedule.getTotalAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal amountPayable = new BigDecimal(this.payableItemVO.getAmountPayable()).setScale(2, BigDecimal.ROUND_HALF_UP);
			LogUtil.log(this.getClass(), "scheduleAmount=" + scheduleAmount + ", amountPayable " + amountPayable);
			if (scheduleAmount.compareTo(amountPayable) < 0) {
			    this.undoFailureReasonList.add("The direct debit payment for the transaction modified a previous direct debit payment which cannot be undone at this time.");
			} else {
			    DirectDebitScheduleItem ddScheduleItem = null;
			    boolean found = false;
			    int itemCount = ddSchedule.getScheduledItemCount();
			    for (int i = 0; i < itemCount && !found; i++) {
				ddScheduleItem = (DirectDebitScheduleItem) ddSchedule.getScheduleItem(i);
				found = (ddScheduleItem != null && !DirectDebitItemStatus.SCHEDULED.equals(ddScheduleItem.getStatus()));
			    }
			    if (found) {
				this.undoFailureReasonList.add("A scheduled direct debit payment has been processed.");
			    }
			}
		    }
		}
		// }
		// catch (PaymentException pe)
		// {
		// throw new SystemException(pe);
		// }
	    }
	}
	LogUtil.log(this.getClass(), "checkPayment 2");
    }

    private void checkVouchers() throws RemoteException {
	Integer creditDisposalId = this.undoTransVO.getDisposalId();
	if (creditDisposalId != null) {
	    FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	    FinanceVoucher voucher = financeMgr.getVoucher(creditDisposalId);
	    if (voucher.getStatus() != null) {
		this.undoFailureReasonList.add("Voucher " + voucher.getDisposalId() + " has been processed.");
	    }
	}
    }

    /**
     * Make sure that a new membership card request has not been issued on or
     * after the date of the transaction being undone.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    private void checkCardRequests() throws RemoteException {
	LogUtil.log(this.getClass(), "checkCardRequests 1");
	Collection cardList = this.undoMemVO.getMembershipCardList(true);
	if (cardList != null && !cardList.isEmpty()) {
	    boolean found = false;
	    MembershipCardVO cardVO;
	    Iterator cardIterator = cardList.iterator();
	    while (cardIterator.hasNext() && !found) {
		cardVO = (MembershipCardVO) cardIterator.next();
		found = this.undoTransVO.getTransactionDate().onOrBeforeDay(cardVO.getIssueDate());

		// if issued same day then potential problem
	    }

	    if (found) {
		LogUtil.log(this.getClass(), "xyz " + this.undoMemVO.getMembershipNumber() + " " + this.undoTransVO.getTransactionID());
		LogUtil.log(this.getClass(), "xyz this.undoTransVO.getTransactionDate()" + this.undoTransVO.getTransactionDate());
		this.undoFailureReasonList.add("A membership card has been issued on or after the transaction date.");
	    }
	}
	LogUtil.log(this.getClass(), "checkCardRequests 2");
    }

    /**
     * If the membership belonged to a membership group at the end of the
     * transaction being undone then make sure that none of the other members in
     * the group have been changed since. Also make sure that the context
     * membership is the PA of the group. If the membership didn't belong to a
     * membership group at the end of the transaction being undone then check
     * the state of the membership group they might have been in at the end of
     * the transaction being reverted to.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    private void checkMembershipGroup() throws RemoteException {
	LogUtil.debug(this.getClass(), "checkMembershipGroup 1");
	// Get the definition of the membership group at the end of the
	// transaction
	// being undone from the membership history file. Can't use the current
	// definition of the membership group because changes to other group
	// members,
	// such as cancelling them, may have removed them from the group.

	// MembershipHistory memHist = this.undoTransVO.getMembershipHistory();
	// if(memHist == null)
	// {
	// throw new
	// SystemException("Failed to get membership history for transaction ID "
	// + this.undoTransVO.getTransactionID());
	// }

	LogUtil.debug(this.getClass(), "checkMembershipGroup 1a");
	MembershipGroupVO memGroupVO = this.undoMemVO.getMembershipGroup();
	LogUtil.debug(this.getClass(), "checkMembershipGroup 1b");
	if (memGroupVO != null) {
	    Collection groupDetailList = memGroupVO.getMembershipGroupDetailList();
	    LogUtil.debug(this.getClass(), "checkMembershipGroup 1c");
	    if (groupDetailList != null) {
		// Search the transactions for the other members of the group to
		// see if any of them have a transaction that was done later
		// than
		// the transaction we are undoing
		Iterator groupDetailIterator = groupDetailList.iterator();
		MembershipGroupDetailVO memGroupDetailVO;
		MembershipVO memVO;
		boolean found = false;
		while (groupDetailIterator.hasNext() && !found) {
		    LogUtil.debug(this.getClass(), "checkMembershipGroup 1d");
		    // Get the detail of the previous membership group
		    memGroupDetailVO = (MembershipGroupDetailVO) groupDetailIterator.next();

		    if (this.undoMemVO.getMembershipID().equals(memGroupDetailVO.getMembershipGroupDetailPK().getMembershipID())) {
			LogUtil.debug(this.getClass(), "checkMembershipGroup 1e");
			// Make sure that the context membership is the PA
			if (this.contextTransaction && !memGroupDetailVO.isPrimeAddressee()) {
			    this.undoFailureReasonList.add("The undo of the transaction must be initiated from the prime addressee of the membership group.");
			}
		    } else {
			LogUtil.debug(this.getClass(), "checkMembershipGroup 1f");
			// Get the transactions of the other group membership

			memVO = memGroupDetailVO.getMembership();
			LogUtil.debug(this.getClass(), "checkMembershipGroup 1g");
			// Now search their transactions for a transaction that
			// is
			// not undone and was done later than the transaction we
			// are undoing.
			Collection transList = memVO.getTransactionList();
			LogUtil.debug(this.getClass(), "checkMembershipGroup 1h");
			if (transList != null) {
			    LogUtil.debug(this.getClass(), "checkMembershipGroup 1i");
			    Iterator transIterator = transList.iterator();
			    while (transIterator.hasNext() && !found) {
				LogUtil.debug(this.getClass(), "checkMembershipGroup 1j");
				MembershipTransactionVO memTransVO = (MembershipTransactionVO) transIterator.next();
				found = !memTransVO.isUndone() && memTransVO.getTransactionDate().after(this.undoTransVO.getTransactionDate());
				LogUtil.debug(this.getClass(), "checkMembershipGroup 1k");
				if (found) {
				    this.undoFailureReasonList.add("Membership " + memVO.getMembershipNumber() + " of the membership group has since been changed.");
				}
			    }
			}
		    }
		}
	    }
	} else {
	    LogUtil.debug(this.getClass(), "checkMembershipGroup else");
	    checkPreviousMembershipGroup();
	}
	LogUtil.debug(this.getClass(), "checkMembershipGroup 2");
    }

    /**
     * Make sure that if the state the membership is to revert to puts them back
     * into a membership group then there have been no subsequent changes made
     * to that membership group.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    private void checkPreviousMembershipGroup() throws RemoteException {
	LogUtil.debug(this.getClass(), "checkPreviousMembershipGroup 1");
	// Get the membership group ID of the previous membership group.
	if (this.revertToMemVO != null) {
	    MembershipGroupVO prevMemGroupVO = this.revertToMemVO.getMembershipGroup();
	    if (prevMemGroupVO != null) {
		Integer prevMemGroupID = prevMemGroupVO.getMembershipGroupID();

		// If they are not currently in a group or the previous
		// membership
		// group was different then get all of the current members of
		// the
		// previous membership group and see if any of them have had a
		// transaction done on them after this transaction that is being
		// undone.

		MembershipGroupVO curMemGroupVO = this.undoMemVO.getMembershipGroup();
		if (curMemGroupVO == null || !prevMemGroupID.equals(curMemGroupVO.getMembershipGroupID())) {
		    Collection groupDetailList = prevMemGroupVO.getMembershipGroupDetailList();
		    if (groupDetailList != null) {
			// Use the membership IDs from the previous membership
			// group detail
			// to get the current state of the membership that was
			// in the previous group.
			// Remember that the previous membership group details
			// come from the
			// history file which does not contain the details of
			// each member in the group
			// at that time.
			MembershipMgrLocal memMgr = MembershipEJBHelper.getMembershipMgrLocal();
			Iterator groupDetailIterator = groupDetailList.iterator();
			MembershipVO groupMemVO;
			MembershipGroupDetailVO memGroupDetailVO;
			boolean found = false;
			while (groupDetailIterator.hasNext() && !found) {
			    // Get the detail of the previous membership group
			    memGroupDetailVO = (MembershipGroupDetailVO) groupDetailIterator.next();

			    // Get the current state of a membership from the
			    // previous membership group
			    groupMemVO = memMgr.getMembership(memGroupDetailVO.getMembershipGroupDetailPK().getMembershipID());

			    // Now search their transactions for a transaction
			    // that is
			    // not undone and was done later than the
			    // transaction we are undoing.
			    Collection transList = groupMemVO.getTransactionList();
			    if (transList != null) {
				Iterator transIterator = transList.iterator();
				while (transIterator.hasNext() && !found) {
				    MembershipTransactionVO memTransVO = (MembershipTransactionVO) transIterator.next();
				    found = !memTransVO.isUndone() && memTransVO.getTransactionDate().after(this.undoTransVO.getTransactionDate());
				    if (found) {
					this.undoFailureReasonList.add("Undoing the transaction will cause the membership to be rejoined to a group and membership " + groupMemVO.getMembershipNumber() + " of the group has since been changed.");
				    }
				}
			    }
			}
			// while (groupDetailIterator.hasNext() && !found)
		    }
		    // if (groupDetailList != null)
		}
		// if (curMemGroupVO == null ||...
	    }
	    // if (prevMemGroupVO != null)
	}
	// if (this.revertToMemVO != null)
	LogUtil.debug(this.getClass(), "checkPreviousMembershipGroup 2");
    }
    // private void checkPreviousMembershipGroup()
}
