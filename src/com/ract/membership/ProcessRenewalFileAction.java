package com.ract.membership;

import java.io.File;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.io.FileUtils;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.CommonEJBHelper;
import com.ract.common.ExceptionHelper;
import com.ract.common.MailMgrLocal;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.util.LogUtil;

public class ProcessRenewalFileAction extends ShowProcessRenewalFileAction implements Preparable {

    @InjectEJB(name = "RenewalMgrBean")
    private RenewalMgr renewalMgr;

    @InjectEJB(name = "MailMgrBean")
    private MailMgrLocal mailMgr;

    public Hashtable<String, Integer> counts;

    private String emailAddress;
    private File marketingFile;

    private File renewalFile;
    private String renewalFileFileName;
    private String renewalFileContentType;

    private File localRenewalFile;

    public Integer emailCount;
    public Integer printCount;
    public Integer validLineCount;
    public Integer primaryLineCount;
    public Integer noDeliveryCount;
    public Integer updateCount;
    public Integer testEmailsSent;

    private boolean defaultMarketing;

    @Override
    public String execute() throws Exception {

	if (renewalFile == null) {
	    addActionError("Renewal file has not been selected.");
	    return ERROR;
	} else {
	    String outputDirectoryName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_LOG_DIR);
	    if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') {
		outputDirectoryName += "/";
	    }

	    // write renewalFile to disk
	    try {
		localRenewalFile = new File(outputDirectoryName + getRenewalFileFileName());
		FileUtils.copyFile(renewalFile, localRenewalFile);

		if (!localRenewalFile.exists()) {
		    addActionError("Renewal file could not be saved.");
		    return ERROR;
		}

	    } catch (Exception e) {
		addActionError("Renewal file could not be saved.");
		return ERROR;
	    }
	}

	LogUtil.debug(this.getClass(), "renewalFile=" + renewalFile);
	LogUtil.debug(this.getClass(), "marketingFile=" + marketingFile);
	LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);
	LogUtil.debug(this.getClass(), "process=" + process);
	LogUtil.debug(this.getClass(), "emailOption=" + emailOption);

	if (PROCESS_MERGE_RENEWALS.equals(process)) {

	    if (!defaultMarketing && marketingFile == null) {
		addActionError("Marketing file has not been selected.");
		return ERROR;
	    }

	    if (emailAddress == null || emailAddress.isEmpty()) {
		addActionError("Email address has not been entered.");
		return ERROR;
	    }

	    Hashtable<String, File> mergeFiles = renewalMgr.mergeRenewalFile(marketingFile, localRenewalFile, defaultMarketing);
	    MailMessage message = new MailMessage();
	    message.setRecipient(emailAddress);
	    message.setSubject(SUBJECT_AUDIT_EMAIL);
	    message.setMessage("Merged file attached.");
	    message.setFiles(mergeFiles);

	    mailMgr.sendMail(message);

	} else if (PROCESS_EMAIL_RENEWALS.equals(process)) {
	    LogUtil.debug(this.getClass(), "email renewals");

	    boolean updateDoc = emailOption.equals(UPDATE_DOCS);
	    boolean testEmails = emailOption.equals(TEST_EMAILS);
	    boolean countOnly = emailOption.equals(COUNT_ONLY);

	    // not possible as the UI control is a radio button but won't hurt
	    // to check
	    if (updateDoc && testEmails) {
		addActionError("You can only send test emails OR update documents.");
		return ERROR;
	    }

	    if (!countOnly) {
		JobDetail jobDetail = new JobDetail("EmailRenewalJob", Scheduler.DEFAULT_GROUP, EmailRenewalJob.class);
		jobDetail.setDescription("Email Renewals");

		JobDataMap dataMap = jobDetail.getJobDataMap();
		dataMap.put("renewalFile", localRenewalFile.getAbsolutePath());
		dataMap.put("updateDoc", updateDoc);
		dataMap.put("testEmails", testEmails);

		// schedule the job immediately
		SimpleTrigger trigger = new SimpleTrigger("EmailRenewalTrigger", Scheduler.DEFAULT_GROUP, new Date());

		try {
		    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
		    scheduleMgr.scheduleJob(jobDetail, trigger);
		    addActionMessage("Email Renewals Job started.");
		} catch (SchedulerException ex) {
		    addActionError("Unable to email renewals. " + ExceptionHelper.getExceptionStackTrace(ex));
		    return ERROR;
		}
	    } else { // counts

		counts = renewalMgr.emailRenewals(localRenewalFile.getAbsolutePath(), updateDoc, testEmails);

		emailCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_EMAIL);
		printCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_PRINT);
		updateCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_EMAIL_DOCUMENT_UPDATES);
		noDeliveryCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_NO_DELIVERY_METHOD);
		primaryLineCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_PRIMARY_LINES);
		validLineCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_PRIMARY_LINES_VALID);
		testEmailsSent = counts.get(RenewalMgrBean.COUNT_RENEWAL_TEST_EMAILS_SENT);

		LogUtil.log(this.getClass(), "Counts only.");
	    }
	}
	return SUCCESS;
    }

    public Integer getValidLineCount() {
	return validLineCount;
    }

    public void setValidLineCount(Integer validLineCount) {
	this.validLineCount = validLineCount;
    }

    public Integer getPrimaryLineCount() {
	return primaryLineCount;
    }

    public void setPrimaryLineCount(Integer primaryLineCount) {
	this.primaryLineCount = primaryLineCount;
    }

    public Integer getNoDeliveryCount() {
	return noDeliveryCount;
    }

    public void setNoDeliveryCount(Integer noDeliveryCount) {
	this.noDeliveryCount = noDeliveryCount;
    }

    public Integer getUpdateCount() {
	return updateCount;
    }

    public void setUpdateCount(Integer updateCount) {
	this.updateCount = updateCount;
    }

    public Integer getEmailCount() {
	return emailCount;
    }

    public void setEmailCount(Integer emailCount) {
	this.emailCount = emailCount;
    }

    public Integer getPrintCount() {
	return printCount;
    }

    public void setPrintCount(Integer printCount) {
	this.printCount = printCount;
    }

    public Integer getTestEmailsSent() {
	return testEmailsSent;
    }

    public void setTestEmailsSent(Integer testEmailsSent) {
	this.testEmailsSent = testEmailsSent;
    }

    public Hashtable<String, Integer> getCounts() {
	return counts;
    }

    public void setCounts(Hashtable<String, Integer> counts) {
	this.counts = counts;
    }

    public String getEmailAddress() {
	return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
    }

    public File getMarketingFile() {
	return marketingFile;
    }

    public void setMarketingFile(File marketingFile) {
	this.marketingFile = marketingFile;
    }

    public File getRenewalFile() {
	return renewalFile;
    }

    public void setRenewalFile(File renewalFile) {
	this.renewalFile = renewalFile;
    }

    public String getRenewalFileFileName() {
	return renewalFileFileName;
    }

    public void setRenewalFileFileName(String renewalFileFileName) {
	this.renewalFileFileName = renewalFileFileName;
    }

    public String getRenewalFileContentType() {
	return renewalFileContentType;
    }

    public void setRenewalFileContentType(String renewalFileContentType) {
	this.renewalFileContentType = renewalFileContentType;
    }

    public boolean isDefaultMarketing() {
	return defaultMarketing;
    }

    public void setDefaultMarketing(boolean defaultMarketing) {
	this.defaultMarketing = defaultMarketing;
    }
}
