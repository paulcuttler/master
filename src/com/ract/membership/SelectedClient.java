package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.client.ClientFactory;
import com.ract.client.ClientVO;
import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.XMLHelper;

/**
 * Holds a client or client membership that has been selected to participate in
 * the transaction.
 * 
 * @author bakkert
 * @created 17 April 2003
 */
public class SelectedClient implements Serializable, Writable, Comparable {
    public static final String WRITABLE_CLASSNAME = "SelectedClient";

    /**
     * The client that has been selected. Will never be null.
     */
    private ClientVO clientVO;

    /**
     * The membership that has been selected. May be null if the selected client
     * does not have a membership.
     */
    private MembershipVO membershipVO;

    /**
     * The reason that the client/membership is not valid for use with the
     * transaction being performed. Will be null if the the client/membership
     * can be used.
     */
    private String invalidReason;

    /**
     * Indicates if the membership is to be removed from the membership group.
     */
    private boolean removedFromGroup = false;

    /**
     * The date and time that the client will join the membership group. Set
     * this to the memberships existing membership group join date or if the
     * client is not part of a membership group then set this to a new
     * DateTime().
     */
    private DateTime membershipGroupJoinDate = null;

    public SelectedClient(MembershipVO memVO, DateTime memGroupJoinDate) throws RemoteException {
	if (memVO == null) {
	    throw new SystemException("A membership must be specified.");
	}
	if (memGroupJoinDate == null) {
	    throw new SystemException("A membership group join date must be specified.");
	}

	this.membershipVO = memVO;
	this.clientVO = memVO.getClient();
	this.membershipGroupJoinDate = memGroupJoinDate;
    }

    public SelectedClient(ClientVO clientVO, DateTime memGroupJoinDate) throws RemoteException {
	if (clientVO == null) {
	    throw new SystemException("A client must be specified.");
	}
	if (memGroupJoinDate == null) {
	    throw new SystemException("A membership group join date must be specified.");
	}

	this.clientVO = clientVO;
	this.membershipGroupJoinDate = memGroupJoinDate;
    }

    public SelectedClient(Node selectedClientNode) throws RemoteException {
	if (selectedClientNode == null || !selectedClientNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
	    throw new SystemException("Failed to create SelectedClient from XML node. The node is not valid.");
	}

	NodeList elementList = selectedClientNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

	    if ("client".equals(nodeName) && writableNode != null) {
		this.clientVO = ClientFactory.getClientVO(writableNode);
	    } else if ("invalidReason".equals(nodeName) && valueText != null) {
		this.invalidReason = valueText;
	    } else if ("membership".equals(nodeName) && writableNode != null) {
		this.membershipVO = new MembershipVO(writableNode, ValueObject.MODE_LOADING);
	    } else if ("membershipGroupJoinDate".equals(nodeName) && valueText != null) {
		try {
		    this.membershipGroupJoinDate = new DateTime(valueText);
		} catch (java.text.ParseException pe) {
		    throw new SystemException("The membership group join date '" + valueText + "' is not a valid date : " + pe.getMessage());
		}
	    } else if ("removedFromGroup".equals(nodeName) && valueText != null) {
		this.removedFromGroup = Boolean.valueOf(valueText).booleanValue();
	    }
	}

	// Do some validation.
	// Must have a client.
	if (this.clientVO == null) {
	    throw new SystemException("Failed to construct a SelectedClient object from xml data.  The clientVO is null.");
	}

	// Must have a membership group join date
	if (this.membershipGroupJoinDate == null) {
	    throw new SystemException("Failed to construct a SelectedClient object from xml data.  The membership group join date is null.");
	}
    }

    /**
     * Return a key that would identify the client if they were a selected
     * client.
     * 
     * @return The key value
     */
    public static String getKey(ClientVO cVO) {
	return cVO.getClientNumber().toString() + "_";
    }

    /**
     * Return a key that would identify the client that owns the membership if
     * they were a selected client.
     * 
     * @return The key value
     */
    public static String getKey(MembershipVO memVO) {
	return memVO.getClientNumber().toString() + memVO.getMembershipID().toString();
    }

    /**
     * Return a key that identifies the selected client in the selected client
     * list. The key is constructed from the client number (which must exist)
     * concatenated with the membership number if a membership exists or an
     * underscore if the membership does not exist.
     * 
     * @return The key value
     */
    public String getKey() {
	if (this.membershipVO != null) {
	    return getKey(this.membershipVO);
	} else {
	    return getKey(this.clientVO);
	}
    }

    public MembershipVO getMembership() {
	return this.membershipVO;
    }

    public ClientVO getClient() {
	return this.clientVO;
    }

    public String getInvalidReason() {
	return this.invalidReason;
    }

    public DateTime getMembershipGroupJoinDate() {
	return this.membershipGroupJoinDate;
    }

    public boolean isCreating() {
	return this.membershipVO == null;
    }

    public boolean isRemovedFromGroup() {
	return this.removedFromGroup;
    }

    public boolean isUpdating() {
	return this.membershipVO != null;
    }

    public boolean isGroupMember() throws RemoteException {
	if (this.membershipVO != null) {
	    return this.membershipVO.isGroupMember();
	}
	return false;
    }

    public void setInvalidReason(String reason) {
	this.invalidReason = reason;
    }

    public void setRemovedFromGroup(boolean removed) {
	this.removedFromGroup = removed;
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("client", this.clientVO);
	cw.writeAttribute("invalidReason", this.invalidReason);
	cw.writeAttribute("membership", this.membershipVO);
	cw.writeAttribute("membershipGroupJoinDate", DateUtil.formatDate(this.membershipGroupJoinDate, "dd/MM/yyyy HH:mm:ss:SS"));
	cw.writeAttribute("removedFromGroup", this.removedFromGroup);
	cw.endClass();
    }

    /**
     * The natural sort order for a selected client is by group join date.
     */
    public int compareTo(Object obj) {
	// LogUtil.debug(this.getClass(),"compareTo()");
	if (obj != null && obj instanceof SelectedClient) {
	    SelectedClient selectedClient = (SelectedClient) obj;
	    int ret = this.membershipGroupJoinDate.compareTo(selectedClient.getMembershipGroupJoinDate());
	    // LogUtil.debug(this.getClass(),"this.membershipGroupJoinDate="+DateUtil.formatDate(this.membershipGroupJoinDate,"dd/MM/yyyy HH:mm:ss:SS")+", selectedClient.getMembershipGroupJoinDate()="+DateUtil.formatDate(selectedClient.getMembershipGroupJoinDate(),"dd/MM/yyyy HH:mm:ss:SS")+", ret="+ret);
	    return ret;
	} else {
	    return 0;
	}
    }

}
