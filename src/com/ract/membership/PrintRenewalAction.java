package com.ract.membership;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.ClientVO;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.membership.reporting.MembershipReportHelper;
import com.ract.membership.ui.MembershipUIHelper;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class PrintRenewalAction extends ActionSupport implements SessionAware, ServletRequestAware {

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    public static final String ACTION_CREATE_RENEWAL_DOCUMENT = "create";

    public static final String ACTION_PRINT_EXISTING_RENEWAL_DOCUMENT = "print";

    private Map session;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    @InjectEJB(name = "MembershipRenewalMgrBean")
    private MembershipRenewalMgrLocal membershipRenewalMgr;

    private Integer membershipId;

    private MembershipVO membershipVO;

    private MembershipDocument document;

    private String message;

    private HashMap<String, String> actionList;

    private String documentMembershipStatus;

    private String documentClientName;

    private String documentResiAddress;

    private String documentPostAddress;

    private String currentMembershipStatus;

    private String noticeAction;

    public String getNoticeAction() {
	return noticeAction;
    }

    public void setNoticeAction(String noticeAction) {
	this.noticeAction = noticeAction;
    }

    private String currentClientName;

    private String currentResiAddress;

    private String currentPostAddress;

    public HashMap<String, String> getActionList() {
	return actionList;
    }

    public void setActionList(HashMap<String, String> actionList) {
	this.actionList = actionList;
    }

    public String getDocumentMembershipStatus() {
	return documentMembershipStatus;
    }

    public void setDocumentMembershipStatus(String documentMembershipStatus) {
	this.documentMembershipStatus = documentMembershipStatus;
    }

    public String getDocumentClientName() {
	return documentClientName;
    }

    public void setDocumentClientName(String documentClientName) {
	this.documentClientName = documentClientName;
    }

    public String getDocumentResiAddress() {
	return documentResiAddress;
    }

    public void setDocumentResiAddress(String documentResiAddress) {
	this.documentResiAddress = documentResiAddress;
    }

    public String getDocumentPostAddress() {
	return documentPostAddress;
    }

    public void setDocumentPostAddress(String documentPostAddress) {
	this.documentPostAddress = documentPostAddress;
    }

    public String getCurrentMembershipStatus() {
	return currentMembershipStatus;
    }

    public void setCurrentMembershipStatus(String currentMembershipStatus) {
	this.currentMembershipStatus = currentMembershipStatus;
    }

    public String getCurrentClientName() {
	return currentClientName;
    }

    public void setCurrentClientName(String currentClientName) {
	this.currentClientName = currentClientName;
    }

    public String getCurrentResiAddress() {
	return currentResiAddress;
    }

    public void setCurrentResiAddress(String currentResiAddress) {
	this.currentResiAddress = currentResiAddress;
    }

    public String getCurrentPostAddress() {
	return currentPostAddress;
    }

    public void setCurrentPostAddress(String currentPostAddress) {
	this.currentPostAddress = currentPostAddress;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    /**
     * @return
     */
    public String execute() {
	UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	try {
	    membershipVO = membershipMgr.getMembership(membershipId);
	    // LogUtil.log(this.getClass(),
	    // "PrintRenewalAction membershipVO="+membershipVO);
	    document = membershipVO.getCurrentRenewalDocument();
	    // LogUtil.log(this.getClass(),
	    // "PrintRenewalAction document="+document);
	    DateTime feeEffectiveDate = MembershipHelper.calculateRenewalEffectiveDate(membershipVO.getExpiryDate(), null);
	    // LogUtil.log(this.getClass(),
	    // "PrintRenewalAction feeEffectiveDate="+feeEffectiveDate);
	    TransactionGroup transactionGroup = TransactionGroup.getRenewalTransactionGroup(membershipVO, userSession.getUser(), feeEffectiveDate, true);
	    // LogUtil.log(this.getClass(),
	    // "\n\ntransactionGroup="+transactionGroup);
	    if (document != null) {

		actionList = new HashMap<String, String>();
		actionList.put(ACTION_PRINT_EXISTING_RENEWAL_DOCUMENT, "Print the renewal document exactly as the membership and client details appeared on " + document.getDocumentDate());
		actionList.put(ACTION_CREATE_RENEWAL_DOCUMENT, "Print the renewal document using the current membership and client details.");

		noticeAction = ACTION_CREATE_RENEWAL_DOCUMENT;

		// current details
		ClientVO client = membershipVO.getClient();
		currentMembershipStatus = MembershipUIHelper.displayMembershipLine(membershipVO, false, false);
		currentClientName = client.getDisplayName();
		currentResiAddress = client.getResidentialAddress().getSingleLineAddress();
		currentPostAddress = client.getPostalAddress().getSingleLineAddress();

		// document details
		MembershipVO documentMembership = transactionGroup.getMembershipTransactionForPA().getMembership();
		documentMembershipStatus = MembershipUIHelper.displayMembershipLine(documentMembership, false, false);
		ClientVO docClient = documentMembership.getClient();
		documentClientName = docClient.getDisplayName();
		documentResiAddress = docClient.getResidentialAddress().getSingleLineAddress();
		documentPostAddress = docClient.getPostalAddress().getSingleLineAddress();

		// go to page with option to either preserve details or generate
		// a new one
		return "document";
	    } else {
		User user = userSession.getUser();
		String printGroup = user.getPrinterGroup();
		LogUtil.log(this.getClass(), "PrintRenewalAction 1");
		Properties renewalProperties = membershipRenewalMgr.produceIndividualRenewalNotice(membershipVO, transactionGroup, user, null);
		LogUtil.log(this.getClass(), "PrintRenewalAction 2");

		MembershipReportHelper.printMembershipRenewal(user, printGroup, renewalProperties, transactionGroup);

		message = "Renewal document has been successfully printed.";

		servletRequest.setAttribute("membershipVO", membershipVO);

		return SUCCESS;
	    }
	} catch (Exception e) {
	    LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
	    addActionError(e.getMessage());
	    return ERROR;
	}

    }

    public Integer getMembershipId() {
	return membershipId;
    }

    public void setMembershipId(Integer membershipId) {
	this.membershipId = membershipId;
    }

    public MembershipVO getMembershipVO() {
	return membershipVO;
    }

    public void setMembershipVO(MembershipVO membership) {
	this.membershipVO = membership;
    }

    public MembershipDocument getDocument() {
	return document;
    }

    public void setDocument(MembershipDocument document) {
	this.document = document;
    }
}