package com.ract.membership;

import com.ract.common.ValueObject;
import com.ract.util.DateTime;

/**
 * Title: RenewalBatchVO Description: Holds information relating to a single
 * renewal notice run (mem_renewal_run) Copyright: Copyright (c) 2003 Company:
 * RACT
 * 
 * @author dgk
 * @version 1.0 1/7/2003
 */

public class RenewalBatchVO extends ValueObject {
    private Integer runNumber = null;

    private DateTime runDate = null;

    private String runBy = null;

    private DateTime firstDate = null;

    private DateTime lastDate = null;

    private String runStatus = null;

    /**
     * Get a total of all documents in a batch.
     * 
     * @return int
     */
    public int getBatchCount() {
	return this.getAdviceCount() + this.getNoticeCount() + this.getReminderCount() + this.getFinalsCount() + this.getUnfinancialsCount();
    }

    @Override
    public String toString() {
	return "RenewalBatchVO [adviceCount=" + adviceCount + ", emailCount=" + emailCount + ", emailSentCount=" + emailSentCount + ", finalsCount=" + finalsCount + ", firstDate=" + firstDate + ", lastDate=" + lastDate + ", noticeCount=" + noticeCount + ", printCount=" + printCount + ", reminderCount=" + reminderCount + ", runBy=" + runBy + ", runDate=" + runDate + ", runNumber=" + runNumber + ", runStatus=" + runStatus + ", unfinancialsCount=" + unfinancialsCount + "]";
    }

    private int noticeCount;

    private int adviceCount;

    public int getEmailCount() {
	return emailCount;
    }

    public void setEmailCount(int emailCount) {
	this.emailCount = emailCount;
    }

    public int getPrintCount() {
	return printCount;
    }

    public void setPrintCount(int printCount) {
	this.printCount = printCount;
    }

    private int reminderCount;

    private int finalsCount;

    private int unfinancialsCount;

    private int emailCount;

    private int emailSentCount;

    public int getEmailSentCount() {
	return emailSentCount;
    }

    public void setEmailSentCount(int emailSentCount) {
	this.emailSentCount = emailSentCount;
    }

    private int printCount;

    public RenewalBatchVO() {
    }

    public RenewalBatchVO(Integer runNumber, DateTime runDate, String runBy, DateTime firstDate, DateTime lastDate, String runStatus) {
	this.runNumber = runNumber;
	this.runDate = runDate;
	this.runBy = runBy;
	this.firstDate = firstDate;
	this.lastDate = lastDate;
	this.runStatus = runStatus;
    }

    public void setRunNumber(Integer runNumber) {
	this.runNumber = runNumber;
    }

    public DateTime getFirstDate() {
	return firstDate;
    }

    public DateTime getLastDate() {
	return lastDate;
    }

    public String getRunBy() {
	return runBy;
    }

    public DateTime getRunDate() {
	return runDate;
    }

    public Integer getRunNumber() {
	return runNumber;
    }

    public void setFirstDate(DateTime firstDate) {
	this.firstDate = firstDate;
    }

    public void setLastDate(DateTime lastDate) {
	this.lastDate = lastDate;
    }

    public void setRunBy(String runBy) {
	this.runBy = runBy;
    }

    public void setRunDate(DateTime runDate) {
	this.runDate = runDate;
    }

    public String getRunStatus() {
	return runStatus;
    }

    public void setRunStatus(String runStatus) {
	this.runStatus = runStatus;
    }

    public int getAdviceCount() {
	return adviceCount;
    }

    public void setAdviceCount(int adviceCount) {
	this.adviceCount = adviceCount;
    }

    public int getFinalsCount() {
	return finalsCount;
    }

    public void setFinalsCount(int finalsCount) {
	this.finalsCount = finalsCount;
    }

    public int getNoticeCount() {
	return noticeCount;
    }

    public void setNoticeCount(int noticeCount) {
	this.noticeCount = noticeCount;
    }

    public int getReminderCount() {
	return reminderCount;
    }

    public int getUnfinancialsCount() {
	return unfinancialsCount;
    }

    public void setReminderCount(int reminderCount) {
	this.reminderCount = reminderCount;
    }

    public void setUnfinancialsCount(int unfinancialsCount) {
	this.unfinancialsCount = unfinancialsCount;
    }

}
