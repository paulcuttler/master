package com.ract.membership;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.MailMgr;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMgr;
import com.ract.payment.finance.FinanceException;
import com.ract.payment.finance.FinanceMgr;
import com.ract.payment.finance.GLTransaction;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

public class LedgerReportJob implements Job {

    public LedgerReportJob() {
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {

	PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
	FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	boolean emailAlert = false;

	String returnEmail = null;
	try {
	    returnEmail = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.EMAIL_FINANCIAL_TRANSFER);
	} catch (RemoteException e) {
	}

	String subject = "General Ledger Report";
	StringBuilder body = new StringBuilder();

	Interval oneMonth = new Interval();
	try {
	    oneMonth.setMonths(1);
	} catch (Exception e1) {
	} // should never happen

	DateTime startDate = new DateTime();
	startDate = startDate.subtract(oneMonth);
	DateTime endDate = new DateTime();

	String subSystem = "GLRS";
	String baseAccount = SystemParameterVO.CREDIT_ACCOUNT.toString();
	String glEntity = "10000";
	String glAccount = glEntity + baseAccount;

	BigDecimal glTotal = BigDecimal.ZERO;
	BigDecimal pTotal = BigDecimal.ZERO;

	body.append("Verifying ");
	body.append(baseAccount);
	body.append(" ledgers for period ");
	body.append(startDate);
	body.append(" - ");
	body.append(endDate);
	body.append("\n\n");

	body.append("FinanceOne Total: ");
	try {
	    List<GLTransaction> glTxns = (List<GLTransaction>) financeMgr.getGLTransactions(startDate, endDate, startDate, endDate, subSystem, glAccount, glEntity);

	    for (GLTransaction gl : glTxns) {
		glTotal = glTotal.add(gl.getAmount());
	    }
	} catch (FinanceException e) {
	    body.append(e.getMessage());
	} catch (RemoteException e) {
	    body.append(e.getMessage());
	}
	body.append("");
	body.append(CurrencyUtil.formatDollarValue(glTotal));
	body.append("\n");

	body.append("Roadside Total: ");
	try {
	    pTotal = paymentMgr.getTotalPostings(Integer.parseInt(baseAccount), startDate, endDate);
	} catch (RemoteException e) {
	    body.append(e.getMessage());
	}

	body.append("");
	body.append(CurrencyUtil.formatDollarValue(pTotal));
	body.append("\n");

	if (glTotal.setScale(2, RoundingMode.HALF_UP).doubleValue() != pTotal.setScale(2, RoundingMode.HALF_UP).doubleValue()) { // round
																 // to
																 // 2dp
	    subject += " - FAILED";
	    emailAlert = true;
	}

	// only send email if totals do not match
	if (emailAlert) {
	    MailMessage message = new MailMessage();
	    message.setRecipient(returnEmail);
	    message.setMessage(body.toString());
	    message.setSubject(subject);
	    message.setFrom(returnEmail);

	    try {
		MailMgr mailMgr = CommonEJBHelper.getMailMgr();
		mailMgr.sendMail(message);
	    } catch (Exception e) {
		LogUtil.fatal(getClass(), "Unable to send ledger report email, " + e);
	    }
	}

    }
}
