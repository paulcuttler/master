package com.ract.membership;

import java.util.Comparator;

/**
 * <p>
 * Sort documents on date
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class MembershipDocumentComparator implements Comparator {

    public MembershipDocumentComparator() {
    }

    public int compare(Object o1, Object o2) {
	if (o1 != null && o1 instanceof MembershipDocument && o2 != null && o2 instanceof MembershipDocument) {
	    MembershipDocument memDoc1 = (MembershipDocument) o1;
	    MembershipDocument memDoc2 = (MembershipDocument) o2;
	    if (memDoc1.getDocumentDate().before(memDoc2.getDocumentDate())) {
		return 1;
	    } else {
		return -1;
	    }
	} else {
	    return 0;
	}
    }

    public boolean equals(Object obj) {
	throw new java.lang.UnsupportedOperationException("Method equals() not yet implemented.");
    }

}
