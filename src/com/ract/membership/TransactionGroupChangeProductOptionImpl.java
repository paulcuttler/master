package com.ract.membership;

/**
 * Provides an implementation of the TransactionGroup class specifically
 * for managing the "Change product option" primary membership transaction.
 *
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author tb
 * @version 1.0
 */

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class TransactionGroupChangeProductOptionImpl extends TransactionGroup {

    public TransactionGroupChangeProductOptionImpl(User user) throws RemoteException {
	super(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION, user);
    }

    /**
     * Create a membership transaction for each of the clients that have been
     * selected to be transacted. If an existing membership group has been
     * identified then additional transactions may also need to be generated for
     * the group members even though they do not directly partake in this
     * transaction.
     */
    public void createTransactionsForSelectedClients() throws RemoteException, ValidationException {
	if (getSelectedClientCount() < 1) {
	    throw new SystemException("Cannot create membership transactions. The list of selected clients is empty.");
	}

	SelectedClient selectedClient;
	MembershipTransactionVO memTransVO = null;
	MembershipVO oldMemVO = null;
	MembershipVO newMemVO = null;
	clearTransactionList();
	DateTime transDate = getTransactionDate();
	Collection clientList = getSelectedClientList();
	Iterator clientListIterator = clientList.iterator();
	while (clientListIterator.hasNext()) {
	    selectedClient = (SelectedClient) clientListIterator.next();

	    oldMemVO = selectedClient.getMembership();
	    memTransVO = new MemTransChangeProductOption(getTransactionGroupTypeCode(), getTransactionDate(), oldMemVO, getUser(), selectedClient.getKey().equals(getSelectedClientKeyForPA()), selectedClient.getMembershipGroupJoinDate());

	    if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
		setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
	    } else {
		memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
	    }

	    addMembershipTransaction(memTransVO);
	}
    }

    /**
     * Override the TransactionGroup.createTransactionFees() method to provide a
     * simpler implementation that is applicable to the "Change product option"
     * primary transaction. The only thing you can do in this transaction is
     * change the product options so these are the only fees that need to be
     * applied.
     */
    public void createTransactionFees() throws RemoteException {
	if (getMembershipType() == null) {
	    throw new RemoteException("The membership type must be set before the transaction fees can be calculated.");
	}

	if (getSelectedProduct() == null) {
	    throw new RemoteException("The product must be set before the transaction fees can be calculated.");
	}

	Vector transList = getTransactionList();
	if (transList == null) {
	    throw new RemoteException("The membership transactions must be added before the transaction fees can be calculated.");
	}

	// The number of chargeable members is the number of members that will
	// be remaining in the membership group.
	int chargeableMemberCount = getActualGroupMemberCount();

	// For each membership transaction clear the existing transaction fees.
	// Then set the new fee list for the new product on the membership
	// transaction.
	MembershipTransactionVO memTransVO = null;
	Collections.sort(transList);
	Iterator transIterator = transList.iterator();
	while (transIterator.hasNext()) {
	    memTransVO = (MembershipTransactionVO) transIterator.next();
	    memTransVO.clearTransactionFeeList();
	    LogUtil.log(this.getClass(), "createTransactionFee() 3");

	    // if the first payment has been received then
	    // recalculate fees otherwise calculate the product option only and
	    // pay
	    // via receipting.
	    if (this.hasPAPaidFirstPayment(memTransVO.getMembership())) {
		LogUtil.log(this.getClass(), "createTransactionFee() 3a");
		// Just create product option fees. These fees aren't earned.
		createProductOptionFees(memTransVO, chargeableMemberCount);
	    } else {
		LogUtil.log(this.getClass(), "createTransactionFee() 3b");
		// creates production option fees also
		createTransactionFees(chargeableMemberCount, chargeableMemberCount, memTransVO);
	    }
	}

	super.createDiscounts();
    } // createTransactionFees

}
