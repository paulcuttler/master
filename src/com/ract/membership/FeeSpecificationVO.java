package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.ServletUtil;

/**
 * The fee that is charged for the linked item. The fee must be linked to either
 * a BaseProductType or a ProductBenefit. Fees have two components, a fixed fee
 * and a fixed fee per vehicle. If the membershipTypeID is empty then the fee
 * applies to all membership types. If the transactionTypeID is empty then the
 * fee applies to all transactions. A fee specification is always instantiated
 * in the context of a base product or a product benefit.
 * 
 * @author T. Bakker.
 * @created 1 August 2002
 */
@Entity
@Table(name = "mem_fee_specification")
public class FeeSpecificationVO extends ValueObject implements Writable, CachedItem {
    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "FeeSpecification";

    /**
     * Description of the Field
     */
    @Id
    @Column(name = "fee_specification_id")
    protected Integer feeSpecificationID;

    /**
     * Description of the Field
     */
    @Column(name = "fee_type_code")
    protected String feeTypeCode;

    @Transient
    private FeeTypeVO feeType;

    /**
     * The code for the base product type that the fee specification relates to.
     */
    @Column(name = "product_code")
    protected String productCode;

    /**
     * The code for the product benfit that the fee specification relates to.
     * Note that all the rates are GST exclusive, but the getters add GST.
     */
    @Column(name = "product_benefit_code")
    protected String productBenefitCode;

    /**
     * Description of the Field
     */
    @Column(name = "membership_type_code")
    protected String membershipTypeCode;

    /**
     * Description of the Field
     */
    @Column(name = "transaction_type_code")
    protected String transactionTypeCode;

    /**
     * Description of the Field
     */
    @Column(name = "fee_per_member")
    protected double feePerMember;

    /**
     * Description of the Field
     */
    @Column(name = "fee_per_vehicle")
    protected double feePerVehicle;

    /**
     * Description of the Field
     */
    @Column(name = "additional_member_fee")
    protected double additionalMemberFee;

    /**
     * Description of the Field
     */
    @Column(name = "group_number")
    protected int groupNumber;

    /**
     * Description of the Field
     */
    @Column(name = "minimum_fee")
    protected double minimumFee;

    /**
     * Description of the Field
     */
    @Column(name = "effective_from_date")
    protected DateTime effectiveFromDate;

    /**
     * Description of the Field
     */
    @Column(name = "effective_to_date")
    protected DateTime effectiveToDate;

    /**
     * Constructors *************************
     */

    /**
     * Default constructor. Required for entity bean.
     */
    public FeeSpecificationVO() {
    }

    /**
     * Constructor for the FeeSpecificationVO object
     * 
     * @param feeSpecID
     *            Description of the Parameter
     */
    public FeeSpecificationVO(Integer feeSpecID) {
	this.feeSpecificationID = feeSpecID;
    }

    /**
     * Getter methods *************************
     * 
     * @return The feeSpecificationID value
     */

    public Integer getFeeSpecificationID() {
	return this.feeSpecificationID;
    }

    /**
     * Gets the status attribute of the FeeSpecificationVO object
     * 
     * @return The status value
     * @exception RemoteException
     *                Description of the Exception
     */
    public String getStatus() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getStatus();
    }

    /**
     * Gets the description attribute of the FeeSpecificationVO object
     * 
     * @return The description value
     * @exception RemoteException
     *                Description of the Exception
     */
    public String getDescription() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getDescription();
    }

    /**
     * Gets the feeTypeCode attribute of the FeeSpecificationVO object
     * 
     * @return The feeTypeCode value
     */
    public String getFeeTypeCode() {
	return this.feeTypeCode;
    }

    /**
     * Gets the feeType attribute of the FeeSpecificationVO object
     * 
     * @return The feeType value
     * @exception RemoteException
     *                Description of the Exception
     */
    public FeeTypeVO getFeeType() throws RemoteException {
	if (this.feeType == null && this.feeTypeCode != null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.feeType = refMgr.getFeeType(this.feeTypeCode);
	    if (this.feeType == null) {
		throw new RemoteException("Failed to get fee type '" + this.feeTypeCode + "'. It does not exist!");
	    }
	}
	return this.feeType;
    }

    /**
     * Gets the productCode attribute of the FeeSpecificationVO object
     * 
     * @return The productCode value
     */
    public String getProductCode() {
	return this.productCode;
    }

    /**
     * Gets the productBenefitCode attribute of the FeeSpecificationVO object
     * 
     * @return The productBenefitCode value
     */
    public String getProductBenefitCode() {
	return this.productBenefitCode;
    }

    /**
     * Return the value of a membership according to this fee specification. If
     * the fee specification is for a fee type that specifies an unearned and
     * earned account then the fee specification can be used to determine a
     * membership value. Return the fee per member, for the number nth member in
     * the group plus the sum of the fees per vehicle for the number of vehicles
     * attached to the membership.
     */
    public double getMembershipValue(int memberCount, int vehicleCount) throws RemoteException {
	double membershipValue = 0.0;
	FeeTypeVO feeTypeVO = getFeeType();
	if (feeTypeVO.isValueFee()) {
	    membershipValue = getFeePerMember(memberCount) + (getFeePerVehicle() * vehicleCount);
	}
	return membershipValue;
    }

    /**
     * Gets the membershipTypeCode attribute of the FeeSpecificationVO object
     * 
     * @return The membershipTypeCode value
     */
    public String getMembershipTypeCode() {
	return this.membershipTypeCode;
    }

    /**
     * Gets the transactionTypeCode attribute of the FeeSpecificationVO object
     * 
     * @return The transactionTypeCode value
     */
    public String getTransactionTypeCode() {
	return this.transactionTypeCode;
    }

    /**
     * The fee is stored inclusive of GST.
     * 
     * @return The feePerMember value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getFeePerMember() throws RemoteException {
	return this.feePerMember;
    }

    /**
     * Return the fee per member for the n'th member (memberCount) in the group.
     * Return the fee per member for members up to the number of members in the
     * group that the fee specifies. Return the additonal fee per memer for
     * members over the number of members in the group that the fee specifies.
     * If the additional fee per member has not been specified, this is
     * indicated by a value less than zero, then use the fee per member anyway.
     * 
     * @return The feePerMember value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getFeePerMember(int memberCount) throws RemoteException {
	double feeCharged = this.feePerMember;

	if (memberCount > this.groupNumber && this.additionalMemberFee >= 0.0) {
	    feeCharged = this.additionalMemberFee;

	}
	return feeCharged;
    }

    /**
     * The fee is stored inclusive of GST.
     * 
     * @return The feePerVehicle value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getFeePerVehicle() throws RemoteException {
	return this.feePerVehicle;
    }

    /**
     * The fee is stored inclusive of GST.
     * 
     * @return The additionalMemberFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getAdditionalMemberFee() throws RemoteException {
	return this.additionalMemberFee;
    }

    /**
     * The fee is stored inclusive of GST.
     * 
     * @return The minimumFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getMinimumFee() throws RemoteException {
	return this.minimumFee;
    }

    /**
     * Gets the groupNumber attribute of the FeeSpecificationVO object
     * 
     * @return The groupNumber value
     */
    public int getGroupNumber() {
	return this.groupNumber;
    }

    /**
     * Gets the earnedAccountID attribute of the FeeSpecificationVO object
     * 
     * @return The earnedAccountID value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Integer getEarnedAccountID() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getEarnedAccountID();
    }

    /**
     * Gets the earnedAccount attribute of the FeeSpecificationVO object
     * 
     * @return The earnedAccount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getEarnedAccount() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getEarnedAccount();
    }

    /**
     * Gets the unearnedAccountID attribute of the FeeSpecificationVO object
     * 
     * @return The unearnedAccountID value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Integer getUnearnedAccountID() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getUnearnedAccountID();
    }

    /**
     * Gets the unearnedAccount attribute of the FeeSpecificationVO object
     * 
     * @return The unearnedAccount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getUnearnedAccount() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getUnearnedAccount();
    }

    /**
     * Gets the effectiveFromDate attribute of the FeeSpecificationVO object
     * 
     * @return The effectiveFromDate value
     */
    public DateTime getEffectiveFromDate() {
	return this.effectiveFromDate;
    }

    /**
     * Gets the effectiveToDate attribute of the FeeSpecificationVO object
     * 
     * @return The effectiveToDate value
     */
    public DateTime getEffectiveToDate() {
	return this.effectiveToDate;
    }

    /**
     * Return a copy of the list of discounts, as DiscountTypeVO, that are
     * allowed for this fee. These are the discounts that can be applied to the
     * fee
     * 
     * @return The allowableDiscountList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getAllowableDiscountList() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getAllowableDiscountList();
    }

    /**
     * Returs the specified discount type if it is an allowed discount type for
     * the fee. Returns null if the discount type is not an allowable discount
     * type for the fee.
     */
    public DiscountTypeVO getAllowableDiscountType(String discountTypeCode) throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.getAllowableDiscountType(discountTypeCode);
    }

    /**
     * Return true if the fee type is allowed to be discounted.
     * 
     * 
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean hasAllowedDiscounts(boolean includeAutomaticDiscounts) throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.hasAllowedDiscounts(includeAutomaticDiscounts);
    }

    /**
     * Return true if this fee specification has the specified accounts defined.
     * 
     * @param earnedAcc
     *            Description of the Parameter
     * @param unearnedAcc
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean hasAccounts(MembershipAccountVO earnedAcc, MembershipAccountVO unearnedAcc) throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	return feeTypeVO.hasAccounts(earnedAcc, unearnedAcc);
    }

    /**
     * Return true if the fee is considered to be earned immediately. The fee is
     * earned immediately if there is no unearned account defined.
     * 
     * @return The earned value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean isEarned() throws RemoteException {
	FeeTypeVO feeTypeVO = getFeeType();
	// LogUtil.log(this.getClass(),"type "+feeType.toString());
	return feeTypeVO.isEarned();
    }

    /**
     * Return true if the fee is used to set the membership value. The fee is a
     * value fee if there is no unearned account defined.
     */
    public boolean isValueFee() throws RemoteException {
	return this.getFeeType().isValueFee();
    }

    /**
     * Setter methods ****************************
     * 
     * @param feeSpecID
     *            The new feeSpecificationID value
     * @exception RemoteException
     *                Description of the Exception
     */

    public void setFeeSpecificationID(Integer feeSpecID) throws RemoteException {
	checkReadOnly();
	this.feeSpecificationID = feeSpecID;
    }

    /**
     * Sets the feeTypeCode attribute of the FeeSpecificationVO object
     * 
     * @param feeTypeCode
     *            The new feeTypeCode value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setFeeTypeCode(String feeTypeCode) throws RemoteException {
	checkReadOnly();
	this.feeTypeCode = feeTypeCode;
    }

    /**
     * Sets the productCode attribute of the FeeSpecificationVO object
     * 
     * @param prodCode
     *            The new productCode value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setProductCode(String prodCode) throws RemoteException {
	checkReadOnly();
	this.productCode = prodCode;
    }

    /**
     * Sets the productBenefitCode attribute of the FeeSpecificationVO object
     * 
     * @param prodBenefitCode
     *            The new productBenefitCode value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setProductBenefitCode(String prodBenefitCode) throws RemoteException {
	checkReadOnly();
	this.productBenefitCode = prodBenefitCode;
    }

    /**
     * Sets the membershipTypeCode attribute of the FeeSpecificationVO object
     * 
     * @param memTypeCode
     *            The new membershipTypeCode value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setMembershipTypeCode(String memTypeCode) throws RemoteException {
	checkReadOnly();
	this.membershipTypeCode = memTypeCode;
    }

    /**
     * Sets the transactionTypeCode attribute of the FeeSpecificationVO object
     * 
     * @param transTypeCode
     *            The new transactionTypeCode value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setTransactionTypeCode(String transTypeCode) throws RemoteException {
	checkReadOnly();
	this.transactionTypeCode = transTypeCode;
    }

    /**
     * Sets the feePerMember attribute of the FeeSpecificationVO object
     * 
     * @param aFee
     *            The new feePerMember value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setFeePerMember(double aFee) throws RemoteException {
	checkReadOnly();
	this.feePerMember = aFee;
    }

    /**
     * Sets the feePerVehicle attribute of the FeeSpecificationVO object
     * 
     * @param aFee
     *            The new feePerVehicle value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setFeePerVehicle(double aFee) throws RemoteException {
	checkReadOnly();
	this.feePerVehicle = aFee;
    }

    /**
     * Sets the additionalMemberFee attribute of the FeeSpecificationVO object
     * 
     * @param aFee
     *            The new additionalMemberFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setAdditionalMemberFee(double aFee) throws RemoteException {
	checkReadOnly();
	this.additionalMemberFee = aFee;
    }

    /**
     * Sets the minimumFee attribute of the FeeSpecificationVO object
     * 
     * @param aFee
     *            The new minimumFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setMinimumFee(double aFee) throws RemoteException {
	checkReadOnly();
	this.minimumFee = aFee;
    }

    /**
     * Sets the groupNumber attribute of the FeeSpecificationVO object
     * 
     * @param groupNumber
     *            The new groupNumber value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setGroupNumber(int groupNumber) throws RemoteException {
	checkReadOnly();
	this.groupNumber = groupNumber;
    }

    /**
     * Sets the effectiveFromDate attribute of the FeeSpecificationVO object
     * 
     * @param effectiveFromDate
     *            The new effectiveFromDate value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setEffectiveFromDate(DateTime effectiveFromDate) throws RemoteException {
	checkReadOnly();
	this.effectiveFromDate = effectiveFromDate;
    }

    /**
     * Sets the effectiveToDate attribute of the FeeSpecificationVO object
     * 
     * @param effectiveToDate
     *            The new effectiveToDate value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setEffectiveToDate(DateTime effectiveToDate) throws RemoteException {
	checkReadOnly();
	this.effectiveToDate = effectiveToDate;
    }

    public void updateFromRequest(HttpServletRequest request) throws RemoteException {

	this.setFeeSpecificationID(ServletUtil.getIntegerParam("inpId", request));
	this.setMembershipTypeCode(ServletUtil.getStringParam("selectMemberType", request));
	String p0 = ServletUtil.getStringParam("selectProduct", request);
	String p1 = p0.substring(p0.indexOf("]") + 1);
	// See if we have product or product benefit
	if (p0.indexOf("[pb]") > -1) {
	    this.setProductBenefitCode(p1);
	    this.setProductCode(null);
	} else {
	    this.setProductCode(p1);
	    this.setProductBenefitCode(null);
	}

	this.setTransactionTypeCode(ServletUtil.getStringParam("selectTransaction", request));
	this.setFeeTypeCode(ServletUtil.getStringParam("selectFeeType", request));
	this.setGroupNumber(ServletUtil.getIntParam("selectGroupNo", request));
	this.setEffectiveFromDate(ServletUtil.getDateParam("inpFromDate", request));
	this.setEffectiveToDate(ServletUtil.getDateParam("inpToDate", request));
	this.setFeePerMember(ServletUtil.getDoubleParam("inpMemFee", request));
	this.setFeePerVehicle(ServletUtil.getDoubleParam("inpVehFee", request));
	this.setAdditionalMemberFee(ServletUtil.getDoubleParam("inpAddFee", request));
	this.setMinimumFee(ServletUtil.getDoubleParam("inpMinFee", request));
    }

    /**
     * CachedItem interface methods ***************
     * 
     * @return The key value
     */

    /**
     * Return the product benefit type code as the cached item key.
     * 
     * @return The key value
     */
    public String getKey() {
	return this.feeSpecificationID.toString();
    }

    /**
     * Determine if the specified key matches the product benefit code. Return
     * true if both the key and the product benefit code are null.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public String toString() {
	return this.feeSpecificationID + ": " + this.membershipTypeCode + ", " + this.productCode + ", " + this.productBenefitCode + ", " + this.transactionTypeCode + ", " + this.feeTypeCode + ", " + this.groupNumber + ", " + this.additionalMemberFee + ", " + this.effectiveFromDate + ", " + this.effectiveToDate + ", " + this.feePerMember + ", " + this.feePerVehicle + ", " + this.minimumFee;
    }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);

	if (this.effectiveFromDate != null) {
	    cw.writeAttribute("effectiveFromDate", this.effectiveFromDate);
	}
	if (this.effectiveToDate != null) {
	    cw.writeAttribute("effectiveToDate", this.effectiveToDate);
	}
	if (this.feePerMember > 0.0) {
	    cw.writeAttribute("feePerMember", this.feePerMember);
	}
	if (this.feePerVehicle > 0.0) {
	    cw.writeAttribute("feePerVehicle", this.feePerVehicle);
	}

	cw.writeAttribute("feeSpecificationID", this.feeSpecificationID);
	cw.writeAttribute("feeTypeCode", this.feeTypeCode);
	cw.writeAttribute("groupNumber", this.groupNumber);

	if (this.membershipTypeCode != null && this.membershipTypeCode.length() > 0) {
	    cw.writeAttribute("membershipTypeCode", this.membershipTypeCode);
	}
	if (this.minimumFee > 0.0) {
	    cw.writeAttribute("minimumFee", this.minimumFee);
	}
	if (this.productBenefitCode != null && this.productBenefitCode.length() > 0) {
	    cw.writeAttribute("productBenefitCode", this.productBenefitCode);
	}
	if (this.productCode != null && this.productCode.length() > 0) {
	    cw.writeAttribute("productCode", this.productCode);
	}
	if (this.transactionTypeCode != null && this.transactionTypeCode.length() > 0) {
	    cw.writeAttribute("transactionTypeCode", this.transactionTypeCode);
	}

	// cw.writeAttribute("feeType",getFeeType());

	cw.endClass();
    }
}
