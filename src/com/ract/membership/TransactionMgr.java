package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.Remote;

@Remote
public interface TransactionMgr {

    public Collection processTransaction(TransactionGroup transGroup) throws RemoteException;

    public void notifyPendingFeePaid(String pendingFeeId, Integer sequenceNo, double amountPaid, double amountOutstanding, String description, String logonId, String receiptNo) throws RemoteException;

    public void notifyPayableFeePaid(String payableItemId, Integer sequenceNo, double amountPaid, double amountOutstanding, String description, String logonId, String receiptNo) throws RemoteException;

    public void commit() throws RemoteException;

    public void rollback() throws RemoteException;

}
