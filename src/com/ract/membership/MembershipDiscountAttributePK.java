package com.ract.membership;

import java.io.Serializable;

public class MembershipDiscountAttributePK implements Serializable {

    /**
     * @hibernate.key-property column="discount_type_code"
     * @return String
     */
    public String getDiscountTypeCode() {
	return discountTypeCode;
    }

    public void setAttributeName(String attributeName) {
	this.attributeName = attributeName;
    }

    public void setDiscountTypeCode(String discountTypeCode) {
	this.discountTypeCode = discountTypeCode;
    }

    /**
     * @hibernate.key-property column="attribute_name"
     * @return String
     */
    public String getAttributeName() {
	return attributeName;
    }

    public MembershipDiscountAttributePK() {
    }

    private String discountTypeCode;

    private String attributeName;

    public boolean equals(Object obj) {
	if (obj instanceof MembershipDiscountAttributePK) {
	    MembershipDiscountAttributePK that = (MembershipDiscountAttributePK) obj;
	    return this.discountTypeCode.equals(that.discountTypeCode) && this.attributeName.equals(that.attributeName);
	}
	return false;
    }

    public int hashCode() {
	return this.discountTypeCode.hashCode() + this.attributeName.hashCode();
    }

    public String toString() {
	return this.discountTypeCode + "/" + this.attributeName;
    }

}
