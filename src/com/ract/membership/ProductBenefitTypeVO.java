package com.ract.membership;

import java.rmi.RemoteException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.ServletUtil;

/**
 * A benefit obtained with a base membership product. Non-optional benefits are
 * automatically received with the base membership product they are attached to.
 * Optional benefits may be added to the base membership product they are
 * attached to and are not automatically received. Optional benefits may incure
 * an additonal fee. Non-optional benefits do not have associated fees as there
 * cost has already been figured into the base product fee.
 * 
 * @author T. Bakker.
 */
@Entity
@Table(name = "mem_product_benefit_type")
public class ProductBenefitTypeVO extends ValueObject implements Writable, CachedItem, Comparable {

    // These are the statuses applicable to a product benefit type.
    public static final String STATUS_ACTIVE = "Active";

    public static final String STATUS_INACTIVE = "Inactive";

    // The product benefit type code for Motor News
    public static final String PBT_MOTOR_NEWS = "MotorNews";

    // The product benefit type code for social
    public static final String PBT_SOCIAL = "Social";

    // Roadside Assistance
    public static final String PBT_RSA = "24 hr R/S Assist";

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "ProductBenefitType";

    /**
     * A unique string that identifies each membership benefit type. This can be
     * used as the display name of the product option. e.g. "Roadside Help",
     * "Social facilities".
     */
    @Id
    @Column(name = "product_benefit_code")
    protected String productBenefitCode;

    /**
     * A description of the product benefit.
     */
    protected String description;

    /**
     * Indicates if the product benefit is optional. Optional benefits may be
     * added to the standard benefits available with the base membership
     * product.
     */
    protected boolean optional = false;

    /**
     * The status of the product benefit.
     */
    @Column(name = "product_benefit_status")
    protected String status;

    /**
     * Indicates if the benefit is selected by default.
     */
    @Column(name = "selected_by_default")
    protected boolean selectedByDefault;

    /**
     * The ID of the privilege that a user is required to have before this
     * product option can be granted to a membership.
     */
    @Column(name = "priv_id")
    protected String privilegeID;

    /*************************** Constructors ********************************/

    /**
     * Default constructor.
     */
    public ProductBenefitTypeVO() {
    }

    /**
     * Initialise the object with the specified values.
     */
    public ProductBenefitTypeVO(String productBenefitCode, String status, String description, boolean optional, boolean selectedByDefault, String privID) {
	this.productBenefitCode = productBenefitCode;
	this.status = status;
	this.description = description;
	this.optional = optional;
	this.selectedByDefault = selectedByDefault;
	this.privilegeID = privID;
    }

    // /**
    // * Initialise the product benefit with the data from the XML element.
    // * @todo implement constructor for XML
    // */
    // public ProductBenefitTypeVO(String productBenefitXML)
    // {
    //
    // }

    /******************************** Getter methods *************************/

    public String getProductBenefitCode() {
	return this.productBenefitCode;
    }

    public String getDescription() {
	return this.description;
    }

    public String getStatus() {
	return this.status;
    }

    public String getPrivilegeID() {
	return this.privilegeID;
    }

    public boolean isOptional() {
	return this.optional;
    }

    public boolean isSelectedByDefault() {
	return selectedByDefault;
    }

    /**************************** Setter methods ****************************/

    public void setProductBenefitCode(String productBenefitCode) throws RemoteException {
	checkReadOnly();
	this.productBenefitCode = productBenefitCode;
    }

    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    public void setOptional(boolean optional) throws RemoteException {
	checkReadOnly();
	this.optional = optional;
    }

    public void setStatus(String status) throws RemoteException {
	checkReadOnly();
	this.status = status;
    }

    public void setSelectedByDefault(boolean selectedByDefault) throws RemoteException {
	checkReadOnly();
	this.selectedByDefault = selectedByDefault;
    }

    public void setPrivilegeID(String privID) throws RemoteException {
	checkReadOnly();
	this.privilegeID = privID;
    }

    /************************ Comparable interface methods **********************/
    /**
     * Compare this product sort order to another products sort order
     */
    public int compareTo(Object obj) {
	if (obj != null && obj instanceof ProductBenefitTypeVO) {
	    ProductBenefitTypeVO prodBenefitVO = (ProductBenefitTypeVO) obj;
	    return this.productBenefitCode.compareTo(prodBenefitVO.getProductBenefitCode());
	} else {
	    return 0;
	}
    }

    public boolean equals(Object obj) {
	if (obj instanceof ProductBenefitTypeVO) {
	    ProductBenefitTypeVO that = (ProductBenefitTypeVO) obj;
	    return this.productBenefitCode.equals(that.productBenefitCode);
	}
	return false;
    }

    /**
     * Update the VO from the HTTP Request
     */
    public void updateFromRequest(HttpServletRequest request) throws RemoteException {
	this.setProductBenefitCode(request.getParameter("productBenefitCode"));
	this.setDescription(request.getParameter("description"));
	this.setStatus(request.getParameter("benefitStatus"));
	this.setOptional(ServletUtil.getBooleanParam("isOptional", request));
	this.setPrivilegeID(request.getParameter("privID"));
	this.setSelectedByDefault(Boolean.valueOf("false").booleanValue());
    }

    /********************** CachedItem interface methods ****************/

    /**
     * Return the product benefit type code as the cached item key.
     */
    public String getKey() {
	return this.getProductBenefitCode();
    }

    /**
     * Determine if the specified key matches the product benefit code. Return
     * true if both the key and the product benefit code are null.
     */
    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("optional", this.optional);
	cw.writeAttribute("productBenefitCode", this.productBenefitCode);
	cw.writeAttribute("selectedByDefault", this.selectedByDefault);
	cw.writeAttribute("status", this.status);
	cw.endClass();
    }
}
