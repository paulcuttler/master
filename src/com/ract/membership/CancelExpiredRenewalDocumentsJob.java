package com.ract.membership;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class CancelExpiredRenewalDocumentsJob implements Job {

    public CancelExpiredRenewalDocumentsJob() {
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
	MembershipMgrLocal membershipMgr = MembershipEJBHelper.getMembershipMgrLocal();
	membershipMgr.cancelExpiredRenewalDocuments();
    }

}
