package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.user.User;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;

/**
 * A base product that may be issued to a membership. For example, "Standard",
 * "Enhanced" and "Non-Motoring".
 * 
 * @hibernate.class table="mem_product" lazy="false"
 * 
 * @author T.Bakker
 * @since JDK1.3
 * @version 1.0, 6 March 2002
 */
public class ProductVO implements Writable, CachedItem, Comparable,
	Serializable
{

    /**
     * @hibernate.property
     * @return
     */
    public boolean isDirectDebitAllowed()
    {
	return directDebitAllowed;
    }

    public void setDirectDebitAllowed(boolean directDebitAllowed)
    {
	this.directDebitAllowed = directDebitAllowed;
    }

    // These are the statuses applicable to a product.
    public static final String STATUS_ACTIVE = "Active";

    public static final String STATUS_INACTIVE = "Inactive";

    public static final String PRODUCT_NON_MOTORING = "Non-Motoring";

    public static final String PRODUCT_ADVANTAGE = "Advantage";

    public static final String PRODUCT_ULTIMATE = "Ultimate";

    public static final String PRODUCT_ACCESS = "Access"; // 4

    public static final String PRODUCT_LIFESTYLE = "Lifestyle"; // 4

    public static final String PRODUCT_CMO = "CMO";

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "Product";

    public static final String AFFILIATE_PRODUCT_LEVEL_BASIC = "Basic";

    public static final String AFFILIATE_PRODUCT_LEVEL_PREMIUM = "Premium";

    /**
     * A unique string that identifies each base product type. This can be used
     * as the display name of the base product type. e.g. "Standard",
     * "Enhanced", "Non-Motoring".
     */
    protected String productCode;

    /**
     * The product that can be used as a credit against this product. Applies to
     * affiliated motor club transfers. The "Standard" product is creditied.
     */
    protected String creditProductCode;

    /**
     * The product identifier to use on membership cards for this product. Valid
     * values are 0 to 9.
     */
    protected int productIdentifier = 0;

    /**
     * A description of what the base product represents. This is for
     * documantation purposes so that in the future people know how the base
     * product was intended to be used.
     */
    protected String description;

    /**
     * The status of the product. e.g. "Active", "Inactive" (which means new
     * memberships cannot be issued with the product.)
     */
    protected String status;

    /**
     * Flag to indicate if this product should be selected by default.
     */
    protected boolean isDefault = false;

    /**
     * The order in which to sort products. Also used to indicate the relative
     * risk of products. A product with a lower sort order has a higher risk
     * that a product with a higher sort order.
     */
    protected int sortOrder;

    /**
     * A list of product benefits associated with this product. Use the
     * getProductBenefitList() method to populate.
     */
    protected Collection productBenefitList;

    /**
     * Holds a reference to the membership reference manager.
     */
    private MembershipRefMgr myMembershipRefMgr = null;

    private boolean directDebitAllowed;

    /**
     * Is this a vehicle-based product? Cannot be altered via UI.
     */
    private boolean vehicleBased;

    /**
     * Can this product be upgraded/downgraded Cannot be altered via UI.
     */
    private boolean upgradable;

    /***************************** Constructors ****************************/
    /**
     * Default constructor. Required by entity bean which extends this vo.
     */
    public ProductVO()
    {

    }

    public String toString()
    {
	return this.productCode + " " + this.status;
    }

    /**
     * Initialise the product with the given product code.
     */
    public ProductVO(String productCode)
    {
	this.productCode = productCode;
    }

    /*************************** Getter methods *****************************/

    /**
     * @hibernate.id column="product_code" generator-class="assigned"
     * @return String
     */
    public String getProductCode()
    {
	return this.productCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="credit_product_code"
     */
    public String getCreditProductCode()
    {
	return this.creditProductCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="product_identifier"
     */
    public int getProductIdentifier()
    {
	return this.productIdentifier;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="sort_order"
     */
    public int getSortOrder()
    {
	return this.sortOrder;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="description"
     */
    public String getDescription()
    {
	return this.description;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="product_status"
     */
    public String getStatus()
    {
	return this.status;
    }

    /**
     * Return a reference to the list of all of the product benefits, as
     * ProductBenefitTypeVO, that can be obtained with this product. The list
     * will include both active and inactive benefits as well as optional and
     * non-optional benefits. Initialise the list if this is the first call.
     * Can't have multiple threads initialising the list at the same time so
     * must be synchronized.
     */
    public synchronized Collection getProductBenefitList()
	    throws RemoteException
    {
	if (this.productBenefitList == null)
	{
	    MembershipRefMgr refMgr = getMembershipRefMgr();
	    this.productBenefitList = refMgr
		    .getProductBenefitList(this.productCode);
	}
	return this.productBenefitList;
    }

    /**
     * Return a new list of product benefits, as ProductBenefitTypeVO. If the
     * activeOnly flag is set then only active benefits will be included in the
     * list. If the optionalOnly flag is set then only optional benefits will be
     * included in the list. The activeOnly and optionalOnly flags can be
     * combined.
     */
    public Collection getProductBenefitList(boolean activeOnly,
	    boolean optionalOnly) throws RemoteException
    {
	Collection fullList = getProductBenefitList();
	ArrayList newList = new ArrayList();

	if (fullList != null && !fullList.isEmpty())
	{
	    Iterator fullListIT = fullList.iterator();
	    while (fullListIT.hasNext())
	    {
		ProductBenefitTypeVO benefit = (ProductBenefitTypeVO) fullListIT
			.next();
		if (activeOnly
			&& !ProductBenefitTypeVO.STATUS_ACTIVE.equals(benefit
				.getStatus()))
		{
		    // don't include
		}
		else if (optionalOnly && !benefit.isOptional())
		{
		    // don't include
		}
		else
		{
		    newList.add(benefit);
		}
	    }
	}
	return newList;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="is_default"
     */
    public boolean isDefault()
    {
	return this.isDefault;
    }

    /************************* Setter methods *******************************/

    public void setCreditProductCode(String creditProductCode)
	    throws ValidationException, RemoteException
    {
	this.creditProductCode = creditProductCode;
    }

    public void setProductIdentifier(int productIdentifier)
	    throws ValidationException, RemoteException
    {
	this.productIdentifier = productIdentifier;
    }

    public void setSortOrder(int sortOrder) throws ValidationException,
	    RemoteException
    {
	this.sortOrder = sortOrder;
    }

    public void setDescription(String description) throws ValidationException,
	    RemoteException
    {
	this.description = description;
    }

    /**
     * Set the product status.
     */
    public void setStatus(String status) throws ValidationException,
	    RemoteException
    {
	this.status = status;
    }

    public void setDefault(boolean isDefault) throws ValidationException,
	    RemoteException
    {
	this.isDefault = isDefault;
    }

    public void setProductBenefitList(Collection prodBenefitList)
	    throws ValidationException, RemoteException
    {
	this.productBenefitList = prodBenefitList;
    }

    /****************************** Utility methods *************************/
    /**
     * Return true if the product has the specified product benefit.
     */
    public boolean hasBenefit(String benefitCode) throws RemoteException
    {
	boolean benefitFound = false;
	Collection benefitList = getProductBenefitList();
	if (benefitCode != null && benefitList != null
		&& !benefitList.isEmpty())
	{
	    ProductBenefitTypeVO benefitTypeVO = null;
	    Iterator benefitListIT = benefitList.iterator();
	    while (benefitListIT.hasNext() && !benefitFound)
	    {
		benefitTypeVO = (ProductBenefitTypeVO) benefitListIT.next();
		// benefit code starts with specified code
		if (benefitTypeVO.getProductBenefitCode().startsWith(
			benefitCode))
		{
		    benefitFound = true;
		}
	    }
	}
	return benefitFound;
    }

    /**
     * Return true if the product has Active optional benefits that can be
     * selected.
     */
    public boolean hasOptionalBenefits() throws RemoteException
    {
	Collection optionalBenefitList = getProductBenefitList(true, true);
	return optionalBenefitList != null && !optionalBenefitList.isEmpty();
    }

    /**
     * //TODO should be stored in product table
     * 
     * @return boolean
     */
    public boolean hasCard()
    {
	if (this.getProductIdentifier() == 1 || // Advantage
		this.getProductIdentifier() == 2 || // Ultimate
		this.getProductIdentifier() == 4 || // Access
		this.getProductIdentifier() == 5) // CMO
	{
	    return true;
	}
	else
	{
	    return false;
	}
    }

    private boolean renewable;

    /**
     * @hibernate.property
     * @hibernate.column name="renewable"
     */
    public boolean isRenewable()
    {
	LogUtil.debug(this.getClass(), "isRenewable");
	return this.renewable;
    }

    public boolean includesRoadsideAssistance()
    {
	boolean includesRoadsideAssistance = false;
	try
	{
	    if (hasBenefit(ProductBenefitTypeVO.PBT_RSA))
	    {
		includesRoadsideAssistance = true;
	    }
	}
	catch (RemoteException ex)
	{
	    LogUtil.warn(this.getClass(),
		    "Unable to determine if product includes Roadside Assistance. "
			    + ex.getMessage());
	}
	return includesRoadsideAssistance;
    }

    private boolean groupable;

    /**
     * @hibernate.property
     * @hibernate.column name="groupable"
     */
    public boolean isGroupable()
    {
	LogUtil.debug(this.getClass(), "isGroupable");
	return this.groupable;
    }

    private boolean tierApplicable;

    private String affiliateProductLevel;

    private String productRanking;

    /**
     * @hibernate.property
     * @hibernate.column name="tier_applicable"
     */
    public boolean isTierApplicable()
    {
	LogUtil.debug(this.getClass(), "isTierApplicable");
	return this.tierApplicable;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="affiliate_product_level"
     */
    public String getAffiliateProductLevel()
    {
	return affiliateProductLevel;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="product_ranking"
     */
    public String getProductRanking()
    {
	return productRanking;
    }

    /**
     * Return true if the product has Active optional benefits that can be
     * selected by the specified user.
     */
    public boolean hasOptionalBenefits(User user) throws RemoteException
    {
	boolean found = false;
	Collection optionalBenefitList = getProductBenefitList(true, true);
	if (optionalBenefitList != null && !optionalBenefitList.isEmpty())
	{
	    ProductBenefitTypeVO benefitTypeVO;
	    String privilegeID;
	    Iterator benefitIterator = optionalBenefitList.iterator();
	    while (!found && benefitIterator.hasNext())
	    {
		benefitTypeVO = (ProductBenefitTypeVO) benefitIterator.next();
		privilegeID = benefitTypeVO.getPrivilegeID();
		// Return true if we find any optional benefit that does not
		// require
		// a privilege or if we find one that does require a privilege
		// and the user has that privilege.
		if (privilegeID == null
			|| (privilegeID != null && user
				.isPrivilegedUser(benefitTypeVO
					.getPrivilegeID())))
		{
		    found = true;
		}
	    }
	}
	return found;
    }

    public void updateFromRequest(HttpServletRequest request)
	    throws RemoteException
    {

	this.setProductCode(ServletUtil.getStringParam("productCode", request));
	this.setDescription(ServletUtil.getStringParam("description", request));
	this.setDefault(ServletUtil.getBooleanParam("isDefault", request));
	this.setStatus(ServletUtil.getStringParam("selectStatus", request));
	this.setCreditProductCode(ServletUtil.getStringParam(
		"creditProductCode", request));
	this.setSortOrder(ServletUtil.getIntParam("sortOrder", request));
	this.setProductIdentifier(ServletUtil
		.getIntParam("identifier", request));

    }

    public boolean equals(Object obj)
    {
	boolean equals = false;
	if (obj != null && obj instanceof ProductVO)
	{
	    ProductVO prodVO = (ProductVO) obj;
	    equals = prodVO.getProductCode().equals(this.productCode);
	}
	return equals;
    }

    /**
     * Return a reference to the membership reference manager session bean.
     * Initialise the reference if this is the first call.
     */
    public MembershipRefMgr getMembershipRefMgr() throws RemoteException
    {
	if (this.myMembershipRefMgr == null)
	{
	    this.myMembershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();
	}
	return this.myMembershipRefMgr;
    }

    protected void validateMode(String newMode) throws ValidationException
    {
    }

    /********************** CachedItem interface methods ****************/

    /**
     * Return the product code as the cached item key.
     */
    public String getKey()
    {
	return this.getProductCode();
    }

    /**
     * Determine if the specified key matches the product code. Return true if
     * both the key and the product code are null.
     */
    public boolean keyEquals(String key)
    {
	String myKey = getKey();
	if (key == null)
	{
	    // See if the product code is also null.
	    if (myKey == null)
	    {
		return true;
	    }
	    else
	    {
		return false;
	    }
	}
	else
	{
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /************************ Comparable interface methods **********************/
    /**
     * Compare this product sort order to another products sort order
     */
    public int compareTo(Object obj)
    {
	// this.setDebug(true);
	// log("compareTo() : start");
	if (obj != null && obj instanceof ProductVO)
	{
	    // log("obj is ProductVO");
	    ProductVO that = (ProductVO) obj;
	    // log("this.prodCode=" + this.productCode + ", that.prodCode=" +
	    // that.getProductCode());
	    // log("this.sortOrder=" + this.sortOrder + ", that.sortOrder=" +
	    // that.getSortOrder());
	    if (this.sortOrder < that.getSortOrder())
	    {
		return -1;
	    }
	    else if (this.sortOrder > that.getSortOrder())
	    {
		return 1;
	    }
	    else
	    {
		return 0;
	    }
	}
	else
	{
	    return 0;
	}
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName()
    {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException
    {
	cw.startClass(WRITABLE_CLASSNAME);

	if (this.creditProductCode != null
		&& this.creditProductCode.length() > 0)
	{
	    cw.writeAttribute("creditProductCode", this.creditProductCode);

	}
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("isDefault", this.isDefault);
	cw.writeAttribute("productCode", this.productCode);
	cw.writeAttribute("productIdentifier", this.productIdentifier);
	cw.writeAttribute("sortOrder", this.sortOrder);
	cw.writeAttribute("status", this.status);
	cw.writeAttributeList("productBenefitList", getProductBenefitList());
	cw.endClass();
    }

    public void setProductCode(String productCode)
    {
	this.productCode = productCode;
    }

    public void setTierApplicable(boolean tierApplicable)
    {
	this.tierApplicable = tierApplicable;
    }

    public void setGroupable(boolean groupable)
    {
	this.groupable = groupable;
    }

    public void setRenewable(boolean renewable)
    {
	this.renewable = renewable;
    }

    public void setAffiliateProductLevel(String affiliateProductLevel)
    {
	this.affiliateProductLevel = affiliateProductLevel;
    }

    public void setProductRanking(String productRanking)
    {
	this.productRanking = productRanking;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="vehicle_based"
     */
    public boolean isVehicleBased()
    {
	return vehicleBased;
    }

    public void setVehicleBased(boolean vehicleBased)
    {
	this.vehicleBased = vehicleBased;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="upgradable"
     */
    public boolean isUpgradable()
    {
	return upgradable;
    }

    public void setUpgradable(boolean upgradable)
    {
	this.upgradable = upgradable;
    }

}
