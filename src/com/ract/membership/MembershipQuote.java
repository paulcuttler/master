package com.ract.membership;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.rmi.RemoteException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientVO;
import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * Holds the details of a quote recorded for a membership transaction. The
 * details of the transaction state at the time the quote was created are stored
 * as an xml document in the transactionXML attribute. Some of the details in
 * the transactionXML are de-normalised into quote attributes for convenience.
 * 
 * @hibernate.class table="mem_membership_quote"
 * 
 * @author T. Bakker created 8 April 2003
 * @version 1.0
 */

public class MembershipQuote
// extends ValueObject
	implements Writable, Comparable, Serializable {
    public final static String WRITABLE_CLASSNAME = "MembershipQuote";

    protected Integer quoteNumber;

    protected DateTime quoteDate;

    protected String transactionGroupTypeCode;

    protected Integer membershipID; // The context membership that the quote
				    // transaction was done against. May be null
				    // when quoting a create.

    protected Integer clientNumber; // The client the quote was issued to.

    protected String transactionXML;

    protected double amountPayable;

    private ClientVO clientVO;

    private MembershipTransactionVO attachedTransaction;

    private TransactionGroup transactionGroup;

    /**
     * Indicates if this quote is current from today. A quote is current if it
     * was not issued more than 14 days ago.
     */
    private Boolean quoteIsCurrent;

    private String userId;

    private String salesBranchCode;

    // Default constructor required for entity bean.
    public MembershipQuote() {
    }

    // Use this constructor when creating a new VO which will then get sent
    // to the entity bean
    public MembershipQuote(TransactionGroup transGroup) throws RemoteException {
	// try
	// {
	this.quoteDate = transGroup.getTransactionDate();
	this.transactionGroupTypeCode = transGroup.getTransactionGroupTypeCode();
	MembershipVO paNewMemVO = transGroup.getMembershipTransactionForPA().getNewMembership();
	this.membershipID = paNewMemVO.getMembershipID();
	this.clientNumber = paNewMemVO.getClientNumber();
	// ensure transaction fee has been calculated
	transGroup.calculateTransactionFee();
	this.amountPayable = transGroup.getAmountPayable();
	this.userId = transGroup.getUserID();
	this.salesBranchCode = transGroup.getSalesBranchCode();

	this.transactionXML = transGroup.toXML();

	// }
	// catch(RemoteException re)
	// {
	// throw new CreateException(re.getMessage());
	// }

    }

    // Use this constructor when creating a VO from the entity bean
    public MembershipQuote(Integer quoteNum, DateTime quoteDate, String transGroupTypeCode, Integer memID, Integer clientNum, double amtPayable, String transXML) {
	this.quoteNumber = quoteNum;
	this.quoteDate = quoteDate;
	this.transactionGroupTypeCode = transGroupTypeCode;
	this.membershipID = memID;
	this.clientNumber = clientNum;
	this.amountPayable = amtPayable;
	this.transactionXML = transXML;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="client_number"
     */
    public Integer getClientNumber() {
	return clientNumber;
    }

    public ClientVO getClientVO() throws RemoteException {
	if (this.clientVO == null) {
	    this.clientVO = ClientEJBHelper.getClientMgr().getClient(this.clientNumber);
	}
	return this.clientVO;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="membership_id"
     */
    public Integer getMembershipID() {
	return membershipID;
    }

    public String getProductCode() throws RemoteException {
	String prodCode = null;
	TransactionGroup transGroup = getTransactionGroup();
	if (transGroup != null && transGroup.getSelectedProduct() != null) {
	    prodCode = transGroup.getSelectedProduct().getProductCode();
	}
	return prodCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="quote_date"
     */
    public DateTime getQuoteDate() {
	return quoteDate;
    }

    /**
     * @hibernate.id column="quote_number" generator-class="assigned"
     */
    public Integer getQuoteNumber() {
	return quoteNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="transaction_group_type_code"
     */
    public String getTransactionGroupTypeCode() {
	return transactionGroupTypeCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="amount_payable"
     */
    public double getAmountPayable() {
	return this.amountPayable;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="transaction_xml"
     */
    public String getTransactionXML() {
	return transactionXML;
    }

    /**
     * Reconstitute a transaction group from the transaction group xml data and
     * return it. Returns null if the transaction group could not be obtained
     * because it does not exist or is corrupt.
     */
    public TransactionGroup getTransactionGroup() throws RemoteException {
	if (this.transactionGroup == null && this.transactionXML != null) {
	    try {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(this.transactionXML)));
		// Should only be one TransactionGroup node in the document.
		Node node = document.getFirstChild();
		if (node != null && node.getNodeName().equals(TransactionGroup.WRITABLE_CLASSNAME)) {
		    this.transactionGroup = new TransactionGroup(node);
		} else {
		    LogUtil.warn(this.getClass(), "The transaction group XML data for membership quote '" + this.quoteNumber + "' is not valid. It does not contain a TransactionGroup node.");
		    this.transactionGroup = null;
		}
	    } catch (ParserConfigurationException pce) {
		throw new SystemException(pce);
	    } catch (IOException ioe) {
		throw new SystemException(ioe);
	    } catch (SAXException se) {
		// This exceptions means that the transaction group xml data is
		// corrupt
		// Which means that the quote cannot be used. Log the corruption
		// and return
		// a null to indicate to the caller that the transaction group
		// could not be
		// obtained.
		LogUtil.warn(this.getClass(), "Failed to parse transaction group xml data in membership quote '" + this.quoteNumber + "'. " + se.getMessage());
		this.transactionGroup = null;
	    }
	}
	return this.transactionGroup;
    }

    public void setClientNumber(Integer clientNumber) throws RemoteException {
	// checkReadOnly();
	this.clientNumber = clientNumber;
	// setModified(true);
    }

    public void setMembershipID(Integer membershipID) throws RemoteException {
	// checkReadOnly();
	this.membershipID = membershipID;
	// setModified(true);
    }

    public void setQuoteDate(DateTime quoteDate) throws RemoteException {
	// checkReadOnly();
	this.quoteDate = quoteDate;
	// setModified(true);
    }

    public void setQuoteNumber(Integer quoteNumber) throws RemoteException {
	// checkReadOnly();
	this.quoteNumber = quoteNumber;
	// setModified(true);
    }

    public void setTransactionGroupTypeCode(String transactionGroupTypeCode) throws RemoteException {
	// checkReadOnly();
	this.transactionGroupTypeCode = transactionGroupTypeCode;
	// setModified(true);
    }

    public void setAmountPayable(double amtPayable) throws RemoteException {
	// checkReadOnly();
	this.amountPayable = amtPayable;
	// setModified(true);
    }

    public void setTransactionXML(String transactionXML) throws RemoteException {
	// checkReadOnly();
	this.transactionXML = transactionXML;
	// setModified(true);
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public MembershipTransactionVO getAttachedTransaction() throws RemoteException {
	if (this.attachedTransaction == null && this.quoteNumber != null) {
	    // MembershipTransaction memTrans =
	    // MembershipEJBHelper.getMembershipTransactionHome().
	    // findByQuoteNumber(this.quoteNumber);
	    MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();

	    this.attachedTransaction = memMgr.findMembershipTransactionByQuoteNumber(this.quoteNumber);

	}
	return attachedTransaction;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="sales_branch_code"
     */
    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="user_id"
     */
    public String getUserId() {
	return userId;
    }

    /**
     * Return the specified adjustment from the transaction group data.
     */
    public MembershipTransactionAdjustment getAdjustment(String adjTypeCode) throws RemoteException {
	MembershipTransactionAdjustment adjustment = null;
	TransactionGroup transGroup = getTransactionGroup();
	adjustment = transGroup.getAdjustment(adjTypeCode);
	return adjustment;
    }

    /**
     * Return true if this quote is attached to a transaction.
     */
    public boolean isAttached() throws RemoteException {
	MembershipTransactionVO memTransVO = getAttachedTransaction();
	return memTransVO != null;
    }

    /**
     * A quote is "current" if the date the quote was issued is not more than 14
     * days from today.
     */
    public boolean isCurrent() throws RemoteException {
	if (this.quoteIsCurrent == null) {
	    this.quoteIsCurrent = new Boolean(isCurrent(new DateTime()));
	}
	return this.quoteIsCurrent.booleanValue();
    }

    /**
     * A quote is "current" if the date the quote was issued is not more than 14
     * days from the date specified.
     */
    public boolean isCurrent(DateTime effectiveDate) throws RemoteException {
	boolean current = false;
	DateTime pastDate = MembershipHelper.getQuoteEffectiveDate(effectiveDate);
	DateTime qDate = DateUtil.clearTime(this.quoteDate);
	current = pastDate.compareTo(qDate) <= 0;
	return current;
    }

    // public MembershipQuote copy()
    // {
    // MembershipQuote memQuote =
    // new MembershipQuote(
    // this.quoteNumber,
    // this.quoteDate,
    // this.transactionGroupTypeCode,
    // this.membershipID,
    // this.clientNumber,
    // this.amountPayable,
    // this.transactionXML);
    // return memQuote;
    // }

    /**
     * The default sort order for a membership quote is by quote number.
     */
    public int compareTo(Object o) {
	if (o instanceof MembershipQuote) {
	    MembershipQuote that = (MembershipQuote) o;
	    int ret = this.quoteNumber.compareTo(that.getQuoteNumber());
	    return ret;
	} else {
	    return 0;
	}
    }

    public String toString() {
	StringBuffer str = new StringBuffer();
	if (this.quoteNumber != null) {
	    str.append("Quote ");
	    str.append(this.quoteNumber);
	}
	if (this.quoteDate != null) {
	    if (str.length() > 0) {
		str.append(" - ");
	    }
	    str.append(this.quoteDate.formatShortDate());
	}
	if (this.transactionGroupTypeCode != null) {
	    if (str.length() > 0) {
		str.append(" : ");
	    }
	    str.append(this.transactionGroupTypeCode);
	}
	if (str.length() > 0) {
	    str.append(", ");
	}
	str.append(CurrencyUtil.formatDollarValue(this.amountPayable));
	return str.toString();
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("amountPayable", NumberUtil.formatValue(this.amountPayable, "########0.00"));
	cw.writeAttribute("clientNumber", this.clientNumber);
	cw.writeAttribute("membershipID", this.membershipID);
	cw.writeAttribute("quoteDate", this.quoteDate);
	cw.writeAttribute("quoteNumber", this.quoteNumber);
	cw.writeAttribute("transactionGroupTypeCode", this.transactionGroupTypeCode);
	cw.writeAttribute("transactionXML", this.transactionXML);
	cw.writeAttribute("userId", this.userId);
	cw.writeAttribute("salesBranchCode", this.salesBranchCode);
	cw.endClass();
    }

}
