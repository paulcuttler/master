package com.ract.membership;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ract.client.Client;
import com.ract.client.ClientVO;
import com.ract.common.AddressVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.HTMLUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;
import com.ract.util.StringUtil;

/**
 * Contains utility methods for implementing membership functionality See each
 * method for what it does
 * 
 * @author bakkert
 * @created 17 February 2003
 */
public class MembershipHelper {

    public final static int SEGMENT_SINGLE = 0;

    public final static int SEGMENT_JOINT = 1;

    public final static int SEGMENT_FAMILY = 2;

    public final static int SEGMENT_FAMILY_PLUS = 3;

    public final static int SEGMENT_COUNT = 4;

    public final static String SEGMENT_TEXT[] = { "Personal", "Joint", "Family", "Family+" };

    public final static String PRODUCTS[] = { "Advantage", "Ultimate", "Non-Motoring" };

    public final static int[] PRODUCT_SEGMENTS = { 4, 4, 1 };

    public final static int PRODUCT_COUNT = 3;

    public static DateTime scheduleGMVoucherLetters(HttpServletRequest request) throws Exception {

	JobDetail jobDetail = new JobDetail("GMVoucherLetters", null, GoldMedallionVoucherLetterJob.class);
	jobDetail.setDescription("Gold Medallion Voucher Letters");

	String dateString = request.getParameter("runDate");
	String hoursString = request.getParameter("runHours");
	String minsString = request.getParameter("runMins");
	DateTime runTime = new DateTime(dateString);

	GregorianCalendar gc = new GregorianCalendar();
	gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(hoursString), Integer.parseInt(minsString), 0);

	// schedule the job
	SimpleTrigger trigger = new SimpleTrigger("GMVoucherLettersTrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);

	com.ract.user.UserSession userSession = ServletUtil.getUserSession(request, true);

	JobDataMap dataMap = jobDetail.getJobDataMap();
	dataMap.put("effectiveDate", request.getParameter("effectiveDate"));
	dataMap.put("userId", userSession.getUserId());

	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	} catch (ServiceLocatorException ex1) {
	    throw new RemoteException("Unable to find ScheduleMgr.", ex1);
	}
	return new DateTime(gc.getTime());
    }

    public static DateTime scheduleCreateAccessMemberships(HttpServletRequest request) throws Exception {

	JobDetail jobDetail = new JobDetail("CreateAccessMemberships", null, CreateAccessMembershipsJob.class);
	jobDetail.setDescription("Create Access Memberships");

	String dateString = request.getParameter("runDate");
	String hoursString = request.getParameter("runHours");
	String minsString = request.getParameter("runMins");
	DateTime runTime = new DateTime(dateString);

	GregorianCalendar gc = new GregorianCalendar();
	gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(hoursString), Integer.parseInt(minsString), 0);

	// schedule the job
	SimpleTrigger trigger = new SimpleTrigger("CreateAccessMembershipsTrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);

	com.ract.user.UserSession userSession = ServletUtil.getUserSession(request, true);

	JobDataMap dataMap = jobDetail.getJobDataMap();
	dataMap.put("effectiveDate", request.getParameter("effectiveDate"));
	dataMap.put("createMemberships", request.getParameter("createMemberships"));
	dataMap.put("userId", userSession.getUserId());

	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	} catch (ServiceLocatorException ex1) {
	    throw new RemoteException("Unable to find ScheduleMgr.", ex1);
	}
	return new DateTime(gc.getTime());
    }

    public static void scheduleCancelExpiredMembershipDocuments(String cronExpression) throws Exception {

	JobDetail jobDetail = new JobDetail("CancelExpiredMembershipDocuments", null, CancelExpiredRenewalDocumentsJob.class);
	jobDetail.setDescription("Cancel Expired Membership Documents");

	// schedule the job
	CronTrigger trigger = null;
	try {
	    trigger = new CronTrigger("CancelledExpiredMembershipDocumentsTrigger", null, cronExpression);
	} catch (ParseException ex2) {
	    throw new RemoteException("Unable to determine schedule.", ex2);
	}

	trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT);

	try {
	    ServiceLocator serviceLocator = ServiceLocator.getInstance();
	    ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
	    scheduleMgr.scheduleJob(jobDetail, trigger);
	} catch (SchedulerException ex) {
	    throw new RemoteException("Unable to schedule job.", ex);
	} catch (ServiceLocatorException ex1) {
	    throw new RemoteException("Unable to find ScheduleMgr.", ex1);
	}
    }

    /**
     * Disband all membership groups that this member belongs to. Should only be
     * one.
     * 
     * @param membershipID
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public static void disbandMembersGroup(Integer membershipID) throws RemoteException {
	LogUtil.log("MembershipHelper", "disbandMembersGroup start " + membershipID);
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection otherGroupMembers = null;
	Integer groupId = null;
	LogUtil.log("MembershipHelper", "disbandMembersGroup 1");
	Collection membersGroupList = membershipMgr.findMembershipGroupsByMembershipID(membershipID);
	LogUtil.log("MembershipHelper", "disbandMembersGroup 2 " + membersGroupList);
	if (membersGroupList != null && !membersGroupList.isEmpty()) {
	    MembershipGroupDetailVO groupMember = null;
	    for (Iterator<MembershipGroupDetailVO> i = membersGroupList.iterator(); i.hasNext();) {
		// each group/member combination
		groupMember = i.next();
		LogUtil.log("MembershipHelper", "disbandMembersGroup 3");
		// get the number remaining in the group
		groupId = groupMember.getMembershipGroupDetailPK().getGroupID();
		LogUtil.log("MembershipHelper", "disbandMembersGroup 4 " + groupId);
		membershipMgr.deleteMembershipGroup(groupId);
		LogUtil.log("MembershipHelper", "disbandMembersGroup 5");
	    }
	}
	LogUtil.log("MembershipHelper", "disbandMembersGroup end " + membershipID);
    }

    public static DateTime calculateRenewalEffectiveDate(DateTime expiryDate, DateTime transactionDate) throws RemoteException, SystemException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	// Generate the renewal notice with the correct fee effective date.
	String renewalEffDateOption = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_RENEWAL_EFFECTIVE_DATE_OPTION);

	DateTime feeEffectiveDate = null;
	if (SystemParameterVO.MEM_RENEWAL_EFFECTIVE_DATE_OPTION_TRANSACTION_DATE.equals(renewalEffDateOption)) {
	    if (transactionDate == null) {
		feeEffectiveDate = new DateTime();
	    } else {
		feeEffectiveDate = transactionDate;
	    }
	} else {
	    if (SystemParameterVO.MEM_RENEWAL_EFFECTIVE_DATE_OPTION_EXPIRY_DATE.equals(renewalEffDateOption)) {
		feeEffectiveDate = expiryDate;
	    } else {
		throw new SystemException("Invalid value for system parameter '" + SystemParameterVO.CATEGORY_MEMBERSHIP + "," + SystemParameterVO.MEM_RENEWAL_EFFECTIVE_DATE_OPTION + "' = " + (renewalEffDateOption == null ? "<null>" : renewalEffDateOption));
	    }
	}
	return feeEffectiveDate;
    }

    public static DateTime getQuoteEffectiveDate(DateTime effectiveDate) throws RemoteException {
	if (effectiveDate == null) {
	    effectiveDate = new DateTime();
	}
	String currentPeriodSpec = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_QUOTE_CURRENT_PERIOD);
	Interval currentPeriod = null;
	try {
	    currentPeriod = new Interval(currentPeriodSpec);
	} catch (java.text.ParseException pe) {
	    throw new SystemException(pe);
	}
	DateTime pastDate = DateUtil.clearTime(effectiveDate.subtract(currentPeriod));
	return pastDate;
    }

    /**
     * Remove the specified membership from a specific group or all membership
     * groups if the group id is null. If there is only one remaining member in
     * the group then disband the group altogether.
     * 
     * @param membershipGroupID
     *            Description of the Parameter
     * @param membershipID
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public static MembershipGroupDetailVO removeFromAllGroups(Integer membershipID) throws RemoteException {
	LogUtil.debug("MembershipHelper", "removeFromAllGroups start " + membershipID);
	MembershipMgrLocal membershipMgr = MembershipEJBHelper.getMembershipMgrLocal();
	Collection<MembershipGroupDetailVO> otherGroupMembers = null;
	Integer groupId = null;
	MembershipGroupDetailVO membershipGroupDetail = null;
	LogUtil.debug("MembershipHelper", "removeFromAllGroups 1");
	Collection<MembershipGroupDetailVO> membersGroupList = membershipMgr.findMembershipGroupsByMembershipID(membershipID);
	LogUtil.debug("MembershipHelper", "removeFromAllGroups 2 " + membersGroupList);
	if (membersGroupList != null && !membersGroupList.isEmpty()) {
	    // should only belong to one group
	    if (membersGroupList.size() > 1) {
		LogUtil.log("MembershipHelper", "removeFromAllGroups 3");
		throw new ValidationException("Member " + membershipID + " belongs to more than one membership group. " + membersGroupList);
	    } else {
		LogUtil.debug("MembershipHelper", "removeFromAllGroups 4");
		membershipGroupDetail = membersGroupList.iterator().next();
		LogUtil.debug("MembershipHelper", "removeFromAllGroups 5 " + membershipGroupDetail);
		groupId = membershipGroupDetail.getMembershipGroupDetailPK().getGroupID();
		LogUtil.debug("MembershipHelper", "removeFromAllGroups 6 " + groupId);

		LogUtil.debug("MembershipHelper", "removeFromAllGroups 7");
		// delete group membership record
		membershipMgr.removeMembershipGroupDetail(membershipGroupDetail);
		LogUtil.debug("MembershipHelper", "removeFromAllGroups 8");
		// get the other group members that belong to the group
		otherGroupMembers = membershipMgr.findMembershipGroupsByGroupID(groupId);
		// if there is only one left
		// if (otherGroupMembers.size() == 1)
		// {
		// LogUtil.log("MembershipHelper","removeFromAllGroups 9 "+groupId);
		// membershipMgr.deleteMembershipGroup(groupId);
		// }
		LogUtil.debug("MembershipHelper", "removeFromAllGroups 10");

	    }
	}
	LogUtil.debug("MembershipHelper", "removeFromAllGroups end " + membershipID);
	return membershipGroupDetail;
    }

    public static String getMembershipHTMLSummary(MembershipVO membershipVO) {
	StringBuffer summary = new StringBuffer();
	summary.append(membershipVO.getMembershipID() == null ? "" : membershipVO.getMembershipID().toString());
	summary.append(HTMLUtil.ANCHOR_NEW_LINE);
	// add the value line
	try {
	    summary.append("Value = " + CurrencyUtil.formatDollarValue(membershipVO.getMembershipValue(true)));
	} catch (RemoteException ex) {
	    summary.append("Value could not be obtained.");
	}
	summary.append(HTMLUtil.ANCHOR_NEW_LINE);
	if (membershipVO.getAffiliatedClubCode() != null && !membershipVO.getAffiliatedClubCode().equals("")) {
	    // affiliated club details only if a code exists
	    summary.append("Affiliated Club details =  ");
	    summary.append(membershipVO.getAffiliatedClubCode() + " ");
	    summary.append(membershipVO.getAffiliatedClubId() + " ");
	    summary.append(membershipVO.getAffiliatedClubProductLevel() + " ");
	    summary.append(membershipVO.getAffiliatedClubExpiryDate());
	    summary.append(HTMLUtil.ANCHOR_NEW_LINE);
	}
	summary.append("Unearned credit amount = " + CurrencyUtil.formatDollarValue(membershipVO.getCreditAmount()) + HTMLUtil.ANCHOR_NEW_LINE);
	summary.append("Inception sales branch = " + (membershipVO.getInceptionSalesBranch() != null ? membershipVO.getInceptionSalesBranch() : "none"));
	return summary.toString();
    }

    /**
     * Is the membership still active using the three month active rule. Only
     * really applicable to memberships with an expired status.
     * 
     * @param expiryDate
     *            DateTime
     * @throws RemoteException
     * @return boolean
     */
    public static boolean isMembershipActive(DateTime expiryDate) throws RemoteException {
	boolean active = false;
	// The membership can be rejoined if it has been expired for more than
	// three months
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	String postExpireRejoinPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.POST_EXPIRE_REJOIN_PERIOD);
	Interval postExpireRejoinPeriod = null;
	try {
	    postExpireRejoinPeriod = new Interval(postExpireRejoinPeriodSpec);
	} catch (ParseException pe) {
	    throw new SystemException(pe.getMessage());
	}
	DateTime pastDate = new DateTime().subtract(postExpireRejoinPeriod);
	if (expiryDate.afterDay(pastDate)) {
	    active = true;
	}
	return active;
    }

    /**
     * Method to obtain ratio.
     * 
     * @param effectiveDays
     * @param totalDays
     * @return
     */
    public static BigDecimal getUnearnedRatio(int effectiveDays, int totalDays) {
	BigDecimal unearnedRatio = new BigDecimal(0);
	BigDecimal effDays = new BigDecimal(effectiveDays);
	BigDecimal totDays = new BigDecimal(totalDays);
	LogUtil.debug("getUnearnedRatio", "effective days " + effDays);
	LogUtil.debug("getUnearnedRatio", "total days " + totDays);
	unearnedRatio = effDays.divide(totDays, 10, BigDecimal.ROUND_HALF_UP);
	return unearnedRatio;

    }

    /**
     * Validate the client for the given transaction. i.e. Return a reason why
     * the client cannot be used to perform the transaction. Returns a
     * description of why the client cannot be used for the transaction. Returns
     * a null description if the client can be used.
     * 
     * @param clientVO
     *            Description of the Parameter
     * @param transTypeCode
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static String validateClientForTransaction(ClientVO clientVO, String transTypeCode, MembershipVO membershipVO) throws RemoteException {
	if (clientVO == null) {
	    return "Failed to validate client. ClientVO is null.";
	}

	String clientStatus = clientVO.getStatus();
	// You can cancel, refund or write off a membership with any client
	// state.
	if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equalsIgnoreCase(transTypeCode) || transTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER_CREDIT) || transTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REFUND) || transTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_WRITE_OFF)) {
	    return null;
	}

	// if address unknown and not the prime addressee then it is not a fatal
	// error.
	// the status will appear on any documentation to the client.
	if (Client.STATUS_ADDRESS_UNKNOWN.equalsIgnoreCase(clientStatus) && (membershipVO != null && !membershipVO.isPrimeAddressee())) {
	    return null;
	}

	// The client must be active.
	if (!Client.STATUS_ACTIVE.equalsIgnoreCase(clientStatus)) {
	    return "The client has a status of '" + clientStatus + "'.  No changes may be made.";
	}

	// The client cannot be a "group" client
	if (clientVO.getGroupClient() != null && clientVO.getGroupClient()) {
	    return "Client " + clientVO.getClientNumber() + " is a 'group' client and cannot be given a membership.";
	}

	// The client cannot be an organisation
	if (clientVO.getClientType().equals(ClientVO.CLIENT_TYPE_ORGANISATION)) {
	    return "Client " + clientVO.getClientNumber() + " is a 'Organisation' client and cannot be given a personal membership.";
	}

	// The client must have a street address
	AddressVO streetAddress = clientVO.getResidentialAddress();
	if (streetAddress == null || streetAddress.getStreetSuburbID() == null) {
	    return "The client must have a street address.";
	}

	if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equalsIgnoreCase(transTypeCode)) {
	    // The client must have vehicle information when creating.
	    // This can be turned off with a system parameter.
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    String clientRequiresVehicle = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.CLIENT_REQUIRES_VEHICLE);
	    if (clientRequiresVehicle == null) {
		return "Failed to get system parameter '" + SystemParameterVO.CATEGORY_MEMBERSHIP + "', '" + SystemParameterVO.CLIENT_REQUIRES_VEHICLE + "'";
	    } else if ("Y".equalsIgnoreCase(clientRequiresVehicle)) {
		Collection vehicleList = clientVO.getVehicleList();
		if (vehicleList == null || vehicleList.isEmpty()) {
		    return "Vehicle information for the client must be entered first.";
		}
	    }
	}

	return null;
    }

    /**
     * Look for a personal membership already belonging to the client. If none
     * is found then return true otherwise false.
     * 
     * @param clientNumber
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public static boolean clientHasPersonalMembership(Integer clientNumber) throws RemoteException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	MembershipVO membership = membershipMgr.findMembershipByClientAndType(clientNumber, MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
	return (membership != null);
    }

    /**
     * Examine a transaction group to find the type of transaction and the
     * clients associated with the membership.
     * 
     * @todo is it relevant.
     */
    public static int getTransAndGroupData(StringBuffer typeText, StringBuffer groupList, TransactionGroup transGroup) throws SystemException, RemoteException {
	LogUtil.debug("getTransAndGroupData", "getTransAndGroupData");
	// what kind of transaction is this? Single new, Upgrade to ultimate,
	// group, addmember etc
	LogUtil.debug("getTransAndGroupData", "transGroup=" + transGroup);
	// String transactionType = transGroup.getTransactionGroupTypeCode();
	Vector transList = transGroup.getTransactionList();
	LogUtil.debug("getTransAndGroupData", "transList=" + transList);
	int clientCount = 0;
	Integer clientNo = null;
	ClientVO client = null;
	int digits = 0;
	MembershipTransactionVO trans = null;
	MembershipVO groupMemVo = null;
	String transGroupTypeCode = transGroup.getTransactionGroupTypeCode();
	boolean changeProd = false;
	boolean addMember = false;
	boolean rejoin = false;
	Hashtable clientList = new Hashtable();
	for (int x = 0; x < transList.size(); x++) {
	    trans = (MembershipTransactionVO) transList.get(x);
	    LogUtil.debug("getTransAndGroupData", "trans=" + trans);
	    if (trans.getTransactionTypeCode().equals((MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE)) || trans.getTransactionTypeCode().equals((MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN)) || trans.getTransactionTypeCode().equals((MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT)) || trans.getTransactionTypeCode().equals((MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP)))
	    // ||
	    // trans.getTransactionTypeCode().equals((MembershipTransactionTypeVO.TRANSACTION_TYPE_NO_CHANGE)))
	    {

		if (trans.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE) || trans.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN)) {
		    addMember = true;
		}
		if (trans.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT) || trans.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION)) {
		    changeProd = true;
		}
	    }
	    LogUtil.debug("getTransAndGroupData", "x=" + x);
	    groupMemVo = trans.getNewMembership();
	    LogUtil.debug("getTransAndGroupData", "groupMemVo=" + groupMemVo);
	    client = groupMemVo.getClient();
	    LogUtil.debug("getTransAndGroupData", "client=" + client);
	    LogUtil.debug("getTransAndGroupData", "cn=" + client.getClientNumber());
	    LogUtil.debug("getTransAndGroupData", "clientList=" + clientList);
	    if (!clientList.containsKey(client.getClientNumber())) {
		LogUtil.debug("getTransAndGroupData", "a1");
		clientList.put(client.getClientNumber(), "");
		LogUtil.debug("getTransAndGroupData", "a2");
		clientCount++;
		LogUtil.debug("getTransAndGroupData", "a3");
		groupList.append(StringUtil.padNumber(client.getClientNumber(), 7));
		groupList.append("   ");
		LogUtil.debug("getTransAndGroupData", "a4");
		groupList.append(client.getPostalName() + "\n");
		LogUtil.debug("getTransAndGroupData", "a5");
	    }
	}
	LogUtil.debug("getTransAndGroupData", "b");
	String suffix = "CRE";
	if (clientCount > 1) {
	    typeText.append("GR");
	} else {
	    typeText.append("S");
	}
	if (transGroupTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP) || transGroupTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE)) {
	    suffix = "CRE";
	} else if (changeProd) {
	    suffix = "UPG";
	} else if (addMember) {
	    if (clientCount > 1) {
		suffix = "ADD";
	    } else {
		suffix = "CRE";
	    }
	}
	LogUtil.debug("getTransAndGroupData", "c");
	typeText.append(suffix);
	return clientCount;
    }

    /**
     * Write the transaction group to file
     * 
     * NOTE: for testing only
     * 
     * @param transGroup
     * @throws RemoteException
     */
    public static void writeTransactionGroupToFile(TransactionGroup transGroup) throws RemoteException {
	if (transGroup != null) {
	    int txGroupID = transGroup.getTransactionGroupID();
	    String fileName = txGroupID + "transgroup.xml";
	    String dir = "/temp/";
	    String path = dir + fileName;
	    try {
		FileWriter fw = new FileWriter(path, false);
		fw.write(transGroup.toXML());
		fw.flush();
		fw.close();
	    } catch (IOException ex) {
		throw new SystemException("Unable to write file '" + path + "'", ex);
	    }
	}
    }

    /**
     * Get a transactionGroup given an xml document
     * 
     * @param transactionGroupXML
     * @return
     * @throws RemoteException
     */
    public static TransactionGroup getTransactionGroup(String transactionGroupXML) throws RemoteException {
	TransactionGroup transactionGroup = null;
	try {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setValidating(false);
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document document = builder.parse(new InputSource(new StringReader(transactionGroupXML)));
	    // Should only be one TransactionGroup node in the document.
	    Node node = document.getFirstChild();
	    if (node != null && node.getNodeName().equals(TransactionGroup.WRITABLE_CLASSNAME)) {
		transactionGroup = new TransactionGroup(node);
	    } else {
		LogUtil.log("MembershipHelper", "The transaction group XML data is not valid. It does not contain a TransactionGroup node.");
		transactionGroup = null;
	    }
	} catch (ParserConfigurationException pce) {
	    throw new SystemException(pce);
	} catch (IOException ioe) {
	    throw new SystemException(ioe);
	} catch (SAXException se) {
	    // This exceptions means that the transaction group xml data is
	    // corrupt
	    // Log the corruption and return
	    // a null to indicate to the caller that the transaction group could
	    // not be
	    // obtained.
	    LogUtil.log("MembershipHelper", "Failed to parse transaction group xml data. " + se.getMessage());
	    transactionGroup = null;
	}
	return transactionGroup;

    }

}
