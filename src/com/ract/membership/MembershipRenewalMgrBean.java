package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Properties;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientSubscription;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.common.PostalAddressVO;
import com.ract.common.Publication;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.membership.reporting.MembershipReportHelper;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.billpay.BillpayHelper;
import com.ract.payment.billpay.PendingFee;
import com.ract.user.User;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

@Stateless
@Remote({ MembershipRenewalMgr.class })
@Local({ MembershipRenewalMgrLocal.class })
public class MembershipRenewalMgrBean {

	@PersistenceContext(unitName = "master")
	Session hsession;

	/**
	 * Return a set of properties for printing an individual renewal notice.
	 */
	public Properties produceIndividualRenewalNotice(MembershipVO oldMembership, TransactionGroup transactionGroup, User user, PaymentTransactionMgr paymentTransactionMgr) throws SystemException, RemoteException {
		boolean commitPayment = paymentTransactionMgr == null;
		RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();

		LogUtil.debug(this.getClass(), "produceIndividualRenewalNotice");
		DateTime transactionDate = new DateTime().getDateOnly();
		DateTime firstDate = new DateTime();
		DateTime lastDate = new DateTime();
		DateTime feeEffectiveDate = null;
		Properties renewalReportParameters = new Properties();
		boolean existingRenewal = false;
		MembershipGroupDetailVO memGrpDetailVO = null;

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		Interval feeEffectivePeriod = null;
		try {
			feeEffectivePeriod = new Interval(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_FEE_EFFECTIVE_PERIOD));
		} catch (ParseException e1) {
			throw new SystemException("Unable to construct fee effective period.", e1);
		}

		boolean printed = false;
		boolean emailed = false;

		MembershipTransactionVO transVO = transactionGroup.getMembershipTransactionForPA();
		MembershipVO newMembership = transVO.getNewMembership();
		ClientVO client = newMembership.getClient();
		if (client == null) {
			throw new SystemException("Failed to get clientVO for PA membership " + newMembership.getMembershipNumber() + ".");
		}

		ClientMgrLocal clientMgrLocal = ClientEJBHelper.getClientMgrLocal();
		ClientSubscription sub = clientMgrLocal.getActiveClientSubscription(oldMembership.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL);
		if (sub != null) {
			emailed = true;
		} else {
			printed = true;
		}

		boolean isDirectDebit = oldMembership.getDirectDebitAuthorityID() != null;
		firstDate = transactionDate.subtract(feeEffectivePeriod);
		lastDate = transactionDate.add(feeEffectivePeriod);

		LogUtil.debug(this.getClass(), "transactionDate=" + transactionDate);
		LogUtil.debug(this.getClass(), "firstDate=" + firstDate);
		LogUtil.debug(this.getClass(), "lastDate=" + lastDate);

		if (oldMembership.getExpiryDate().after(lastDate)) {
			throw new SystemException("Membership " + oldMembership.getMembershipNumber() + " is already renewed to " + oldMembership.getExpiryDate() + ".");
		}

		feeEffectiveDate = MembershipHelper.calculateRenewalEffectiveDate(oldMembership.getExpiryDate(), transactionDate);

		LogUtil.debug(this.getClass(), "feeEffectiveDate=" + feeEffectiveDate);

		// is already renewed
		if (oldMembership.isGroupMember()) {
			// get the prime addressee
			memGrpDetailVO = oldMembership.getMembershipGroup().getMembershipGroupDetailForPA();
			oldMembership = memGrpDetailVO.getMembership();
		}

		ProductVO product = transactionGroup.getSelectedProduct();
		String productCode = product.getProductCode();
		String productDescription = product.getDescription();

		DateTime expiryDate = transactionGroup.getExpiryDate();

		SystemParameterVO sysParam = commonMgr.getSystemParameter(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_BILLPAY_PREFIX);

		int prefix = sysParam.getNumericValue1().intValue();

		// there will only be a pending fee if this is not direct debit
		// If it is direct debit, only look for a RenewalAdvice document.
		// BillPayMgr billPayMgr = PaymentEJBHelper.getBillPayMgr();
		PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();
		String documentType = null;
		if (isDirectDebit) {
			documentType = MembershipDocument.RENEWAL_ADVICE;
		} else {
			documentType = MembershipDocument.RENEWAL_NOTICE;
		}

		MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();

		MembershipDocument document = oldMembership.getCurrentRenewalDocument();

		LogUtil.debug(this.getClass(), "document=" + document);
		PendingFee pendingFee = null;
		String billpayReference = "";

		// existing renewal if a renewal document can be found
		existingRenewal = (document != null);
		LogUtil.debug(this.getClass(), "existingRenewal=" + existingRenewal);
		if (!isDirectDebit && existingRenewal) {

			// there should be a pending fee
			billpayReference = BillpayHelper.createIvrNumber(document.getDocumentId().intValue(), SourceSystem.MEMBERSHIP, false);
			try {
				pendingFee = paymentMgrLocal.getPendingFee(billpayReference);
			} catch (PaymentException e) {
				throw new SystemException(e.getMessage());
			}
			// pendingFee = billPayMgr.getRenewalData(billpayReference);
			if (pendingFee == null) {
				// Try generating the reference using the old 0 concat method
				String oldBillpayReference = BillpayHelper.createIvrNumber(document.getDocumentId().intValue(), SourceSystem.MEMBERSHIP, true);
				try {
					pendingFee = paymentMgrLocal.getPendingFee(oldBillpayReference);
				} catch (PaymentException e) {
					throw new SystemException(e.getMessage());
				}
				if (pendingFee == null) {
					throw new SystemException("Unable to find pending fee record for document: " + document + ", ivrNumber: " + billpayReference + "/" + oldBillpayReference);
				}
			}

			// Add an automatic discretionary discount if the new amount payable
			// is more than
			// the pending fee amount. This deals with the situation where the
			// pending fee
			// was previously generated with an old fee schedule and the renewal
			// transaction
			// created now is using a new fee schedule. TB 2/10/2003
			BigDecimal adjAmount = null;
			MembershipTransactionAdjustment memTransAdj = null;
			if (transactionGroup.getAmountPayable() > pendingFee.getAmount()) {
				adjAmount = new BigDecimal(transactionGroup.getAmountPayable() - pendingFee.getAmount());
				memTransAdj = new MembershipTransactionAdjustment(AdjustmentTypeVO.RENEWAL_NOTICE_DISCOUNT, adjAmount, "Discount to honour original renewal fee.");
			} else if (transactionGroup.getAmountPayable() < pendingFee.getAmount()) {
				adjAmount = new BigDecimal(pendingFee.getAmount() - transactionGroup.getAmountPayable());
				memTransAdj = new MembershipTransactionAdjustment(AdjustmentTypeVO.RENEWAL_NOTICE_ADJUSTMENT, adjAmount, "Adjustment to honour original renewal fee.");
			}

			if (memTransAdj != null) {
				transactionGroup.addAdjustment(memTransAdj);
				transactionGroup.calculateTransactionFee();
			}

		}

		String ddBody = "";
		try {

			// start a transaction
			if (paymentTransactionMgr == null) {
				paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			}

			Integer documentId = null;
			// no active renewal
			if (!existingRenewal) {
				LogUtil.log(this.getClass(), "documentId is null");

				MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
				// generate a document id
				documentId = new Integer(idMgr.getNextDocumentID());
				LogUtil.debug(this.getClass(), "produceIndividualRenewalNotice documentId=" + documentId);
				// create the pending fee if required
				if (!isDirectDebit) {
					DateTime feeExpiryDate = oldMembership.getExpiryDate().add(feeEffectivePeriod);
					String refNo = prefix + "";
					for (int x = documentId.toString().length(); x < 9; x++) {
						refNo += "0"; // append "0" to RHS of value
					}
					billpayReference = BillpayHelper.createIvrNumber(documentId.intValue(), SourceSystem.MEMBERSHIP, false);
					renewalReportParameters.put("billpayReference", billpayReference);
					LogUtil.debug(this.getClass(), "not DD");
					try {
						paymentTransactionMgr.createPendingFee(oldMembership.getClientNumber(), billpayReference, feeEffectiveDate, feeExpiryDate, SourceSystem.MEMBERSHIP.getAbbreviation(), oldMembership.getMembershipID(), user.getSalesBranchCode(), productCode, new Long(refNo), new Long(documentId.toString()), oldMembership.getMembershipNumber(), transactionGroup.getAmountPayable(), user.getUserID(), PaymentMgr.MEM_MANUAL_RENEWAL);
					} catch (PaymentException e) {
						throw new Exception("Unable to create the Pending Fee for manual renewal: " + e.getMessage());
					}
					LogUtil.debug(this.getClass(), "not DD 2");
				}

			}

			String adviceHeading = "";
			if (isDirectDebit) {
				LogUtil.debug(this.getClass(), ">>>>>>>>>>THIS IS DD 2<<<<<<<<<<");
				// this is a direct debit client
				try {
					Hashtable directDebitDetails = RenewalMgrBean.constructDirectDebitDetails(paymentTransactionMgr, oldMembership, transactionGroup, RenewalMgrBean.FORMAT_REPORT);

					// add to renewal properties
					renewalReportParameters.put(RenewalMgrBean.KEY_DD_SCHEDULE, directDebitDetails.get(RenewalMgrBean.KEY_DD_SCHEDULE));

				} catch (RenewalNoticeException ex) {
					throw new SystemException(ex);
				}

				ddBody = commonMgr.getGnText("MEM", "MAN_REN", "DDBODY");
				renewalReportParameters.put("ddText", ddBody);

				adviceHeading = "Renewal Advice";
			} else {
				LogUtil.debug(this.getClass(), "not DD 3");
				renewalReportParameters.put("ddText", "");
				LogUtil.debug(this.getClass(), "not DD 4");
				adviceHeading = "Renewal Notice";

				String offerMessage = "";
				String offerText = "";
				if (oldMembership.isGroupMember()) {
					// OFFERSINGLENODD
					offerMessage = commonMgr.getGnText("MEM", "RENEWAL", "OFFERGROUPNODD");
					offerText = commonMgr.getGnText("MEM", "RENEWAL", "OFFERTEXTGROUPNODD");
				} else {
					offerMessage = commonMgr.getGnText("MEM", "RENEWAL", "OFFERSINGLENODD");
					offerText = commonMgr.getGnText("MEM", "RENEWAL", "OFFERTEXTSINGLENODD");
				}
				renewalReportParameters.put("offerMessage", offerMessage);
				renewalReportParameters.put("offerText", offerText);

			}

			if (newMembership.getProductCode().equals(ProductVO.PRODUCT_LIFESTYLE) || newMembership.getProductCode().equals(ProductVO.PRODUCT_ACCESS) || newMembership.getProductCode().equals(ProductVO.PRODUCT_NON_MOTORING)) {
				String endMessage = "";
				renewalReportParameters.put("endMessage", endMessage);
			} else {
				String endMessage = commonMgr.getGnText("MEM", "RENEWAL", "ENDMESSAGE");
				renewalReportParameters.put("endMessage", endMessage);
			}
			
			LogUtil.debug(this.getClass(), "after DD 1");
			renewalReportParameters.put("adviceHeading", adviceHeading);
			LogUtil.debug(this.getClass(), "after DD 2");
			renewalReportParameters.put("ivrNumber", billpayReference);

			LogUtil.debug(this.getClass(), "before doc");

			// needs dd details to be generated for transaction xml to be
			// complete

			// needs an active user transaction!!!

			if (!existingRenewal) {
				String emailAddress = null;
				if (emailed) {
					try {
						emailAddress = StringUtil.validateEmail(client.getEmailAddress(), false);
					} catch (Exception e) {
						emailed = false;
						printed = true;
					}
				}

				try {
					LogUtil.debug(this.getClass(), "get group number");
					Integer groupSize = (oldMembership.isGroupMember() ? new Integer(oldMembership.getMembershipGroup().getMemberCount()) : new Integer(1));
					LogUtil.debug(this.getClass(), "get transaction xml");
					String transactionXML = transactionGroup.toXML();
					LogUtil.debug(this.getClass(), "create doc");
					// create new document
					MembershipDocument membershipDocument = new MembershipDocument(documentId, documentType, transactionDate, oldMembership.getMembershipID(), "", null, // no
					// run
					// number
					// as
					// it
					// is
					// an
					// individual
					// print
					oldMembership.getExpiryDate(), feeEffectiveDate, new BigDecimal(transactionGroup.getAmountPayable()), productCode,
					// group size or 1
					groupSize, new BigDecimal(transactionGroup.getTotalDiscount()), oldMembership.getMembershipProfileCode(), printed, emailed, emailAddress, null, transactionXML);
					hsession.save(membershipDocument);
				} catch (HibernateException ex1) {
					throw new SystemException("Unable to save document.", ex1);
				}
			}

			LogUtil.debug(this.getClass(), "after doc");

			if (paymentTransactionMgr != null && commitPayment) {
				paymentTransactionMgr.commit();
			}

		} catch (Exception e) {
			// if there's an error, roll it all back
			if (paymentTransactionMgr != null && commitPayment) {
				paymentTransactionMgr.rollback();
			}
			LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
			throw new SystemException(e);
		} finally {
			// release if used
			if (paymentTransactionMgr != null && commitPayment) {
				paymentTransactionMgr.release();
			}
		}
		LogUtil.debug(this.getClass(), "a");
		StringBuffer typeText = new StringBuffer();
		StringBuffer groupList = new StringBuffer();
		// TODO convert to OO
		MembershipHelper.getTransAndGroupData(typeText, groupList, transactionGroup);
		LogUtil.debug(this.getClass(), "b1");
		renewalReportParameters.put("typeText", typeText.toString());
		LogUtil.debug(this.getClass(), "b");
		if (oldMembership.isGroupMember()) {
			renewalReportParameters.put("groupText", groupList.toString());
		} else {
			renewalReportParameters.put("groupText", "");
		}
		LogUtil.debug(this.getClass(), "c");

		LogUtil.debug(this.getClass(), "d");
		String clientTitle = client.getTitle();
		LogUtil.debug(this.getClass(), "e");
		if (clientTitle != null) {
			renewalReportParameters.put("clientTitle", clientTitle);
		}

		// postalVO has address details already processed.
		PostalAddressVO address = null;

		// needs to be in EDI extract as well!
		String postalName = null;
		MembershipGroupVO membershipGroup = newMembership.getMembershipGroup();
		if (membershipGroup != null && membershipGroup.getGroup() != null) {
			address = membershipGroup.getGroup().getBillingAddress();
			GroupVO group = membershipGroup.getGroup();
			address = group.getBillingAddress();
			postalName = group.getGroupName();
			if (group.getContactPerson() != null && !group.getContactPerson().trim().equals("")) {
				postalName += FileUtil.NEW_LINE + "C/O " + group.getContactPerson();
			}
			postalName = postalName.toUpperCase();
		} else {
			address = client.getPostalAddress();
			postalName = client.getPostalName().toUpperCase();
		}

		if (address == null) {
			throw new SystemException("Failed to get PostalAddressVO for client " + client.getClientNumber());
		}
		String[] addressArray = new String[3];
		addressArray[0] = address.getAddressLine1();
		if (addressArray[0] == null) {
			addressArray[0] = "";
		}
		addressArray[1] = address.getAddressLine2();
		if (addressArray[1] == null) {
			addressArray[1] = "";
		}
		addressArray[2] = address.getSuburb() + " " + address.getState().toUpperCase() + " " + address.getPostcode().toString();
		LogUtil.debug(this.getClass(), "f");
		StringUtil.removeBlankLines(addressArray, 0);
		renewalReportParameters.put("address1", addressArray[0].toUpperCase());
		renewalReportParameters.put("address2", addressArray[1].toUpperCase());
		renewalReportParameters.put("address3", addressArray[2].toUpperCase());
		renewalReportParameters.put("postalName", postalName);
		renewalReportParameters.put("addressTitle", client.getAddressTitle());
		renewalReportParameters.put("prodName", productDescription); // full
		// description
		// -
		// memVO.getProduct().getDescription().toUpperCase()
		renewalReportParameters.put("fee", CurrencyUtil.formatDollarValue(transactionGroup.getGrossTransactionFee()));
		renewalReportParameters.put("gst", CurrencyUtil.formatDollarValue(transactionGroup.getGstAmount()));
		renewalReportParameters.put("amount", CurrencyUtil.formatDollarValue(transactionGroup.getAmountPayable()));
		BigDecimal feeAdjustment = NumberUtil.roundHalfUp(transactionGroup.getAmountPayable() - transactionGroup.getGrossTransactionFee(), 2);
		LogUtil.log(this.getClass(), "produceIndividualRenewalNotice() feeAdjustment=" + feeAdjustment.toString());
		if (feeAdjustment.compareTo(new BigDecimal(0.0)) != 0) {
			renewalReportParameters.put("adjustment", CurrencyUtil.formatDollarValue(feeAdjustment));
		} else {
			renewalReportParameters.put("adjustment", "");
		}

		LogUtil.debug(this.getClass(), "g");
		renewalReportParameters.put("rewardPts", "");
		if (oldMembership.getCreditAmount() != 0) {
			renewalReportParameters.put("credit", CurrencyUtil.formatDollarValue(newMembership.getCreditAmount()));
		} else {
			renewalReportParameters.put("credit", "");
		}

		double totalDiscounts = transactionGroup.getTotalDiscount();
		if (totalDiscounts != 0) {
			renewalReportParameters.put("discounts", CurrencyUtil.formatDollarValue(totalDiscounts));
		} else {
			renewalReportParameters.put("discounts", "");
		}

		LogUtil.debug(this.getClass(), "h");
		renewalReportParameters.put("reference", StringUtil.padNumber(new Integer(oldMembership.getMembershipNumber()), 7));
		renewalReportParameters.put("renewalDate", oldMembership.getExpiryDate().toString());

		renewalReportParameters.put("payBy", oldMembership.getExpiryDate().toString());

		if (oldMembership.getStatus().equals(MembershipVO.STATUS_ONHOLD)) {
			renewalReportParameters.put("renewTo", transactionGroup.getPeriodEndDate().toString());
		} else {
			renewalReportParameters.put("renewTo", transactionGroup.getExpiryDate().toString());
		}
		renewalReportParameters.put("userName", user.getUsername());

		try {
			String tierDescription = (String) RenewalMgrBean.getRenewalTierCodeAndDescription(oldMembership).get(RenewalMgrBean.KEY_TIER_DESCRIPTION);
			renewalReportParameters.put("renewalTierDescription", tierDescription);
		} catch (RenewalNoticeException ex2) {
			throw new SystemException(ex2.getMessage());
		}

		// Pricing segment
		int pricingSegment = 0;
		if (newMembership.isGroupMember()) {
			pricingSegment = oldMembership.getMembershipGroup().getMemberCount() - 1;
		}
		if (pricingSegment > MembershipHelper.SEGMENT_COUNT - 1) {
			pricingSegment = MembershipHelper.SEGMENT_COUNT - 1;
		}
		LogUtil.debug(this.getClass(), "i");
		String groupType = MembershipHelper.SEGMENT_TEXT[pricingSegment];
		renewalReportParameters.put("pricingSegment", groupType);
		String letter = "";
		if (newMembership.isGroupMember()) {
			letter = commonMgr.getGnText("MEM", "MAN_REN", "BODYGROUP");
		} else if (newMembership.getProductCode().equals(ProductVO.PRODUCT_LIFESTYLE) || newMembership.getProductCode().equals(ProductVO.PRODUCT_ACCESS) || newMembership.getProductCode().equals(ProductVO.PRODUCT_NON_MOTORING)) {
			letter = commonMgr.getGnText("MEM", "MAN_REN", "BODYLIFESTYLE");
		} else {
			letter = commonMgr.getGnText("MEM", "MAN_REN", "BODYSINGLE");
			letter += commonMgr.getGnText("MEM", "MAN_REN", "BODYSINGLE2");
		}

		letter += FileUtil.NEW_LINE;

		String social = "";
		boolean isSocial = false;
		try {
			isSocial = renewalMgr.isSocialMember(newMembership.getMembershipID());
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("Cannot establish whether client " + oldMembership.getClientNumber() + " has social benefits.", e);
		}
		if (isSocial) {
			social = "and social membership ";
		}
		LogUtil.debug(this.getClass(), "j");

		Hashtable varList = new Hashtable();
		varList.put("salutation", client.getAddressTitle());
		varList.put("product", productDescription);
		varList.put("expiryDate", oldMembership.getExpiryDate().toString());
		varList.put("directDebitText", ddBody);
		varList.put("pricingOption", groupType);
		varList.put("social", social);
		renewalReportParameters.put("letterText", StringUtil.replaceTag(letter, "<", ">", varList));
		if (pendingFee != null) {
			if (transactionGroup.getAmountPayable() != pendingFee.getAmount()) {
				throw new SystemException("Transaction Amount Payable does not match Pending Amount payable");
			}
		}
		// LogUtil.log(this.getClass(),"renewal properties = "+renewalReportParameters.toString());
		return renewalReportParameters;
	}

	/**
	 * Create and print a new renewal document for a given membership The document will only be printed if the membership is within the normal renewal period. Otherwise does nothing Does not check to see if there is an existing renewal document (Largely lifted from PrintRenewalAction)
	 * 
	 * @param memVO The membership for which the document is to be printed.
	 * @param user The user creating the document
	 * @throws Exception
	 */
	public void makeRenewalNotice(MembershipVO memVO, User user) throws Exception {
		try {
			DateTime feeEffectiveDate = MembershipHelper.calculateRenewalEffectiveDate(memVO.getExpiryDate(), null);

			TransactionGroup transactionGroup = TransactionGroup.getRenewalTransactionGroup(memVO, user, feeEffectiveDate, feeEffectiveDate, null, true);
			String printGroup = user.getPrinterGroup();
			Properties renewalProperties = produceIndividualRenewalNotice(memVO, transactionGroup, user, null);
			MembershipReportHelper.printMembershipRenewal(user, printGroup, renewalProperties, transactionGroup);
		} catch (Exception e) {
			LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
		}
	}

	/**
	 * IF there was a previous renewal notice which has been cancelled in the same period, create a new renewal notice Only create a replacement if the original notice is present and cancelled
	 * 
	 * @param memVO The membership to which this applies
	 * @param user User doing the transaction
	 * @throws Exception
	 */
	/*
	 * public void replaceRenewalNotice(MembershipVO memVO, User user) throws Exception { this.replaceRenewalNotice(memVO, memVO, user); }
	 */

	/**
	 * IF there was a previous renewal notice which has been cancelled in the same period, create a new renewal notice Only create a replacement if the original notice is present and cancelled This version allows for change of PA
	 * 
	 * @param oldPaMemVO The original membership - against which the renewal document would be held
	 * @param newPaMemVO The new membership - against which to make the new document
	 * @param user User doing the transaction
	 * @throws Exception
	 */
	/*
	 * public void replaceRenewalNotice(MembershipVO oldPaMemVO, MembershipVO newPaMemVO, User user) throws Exception { MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr(); Integer documentId = memMgr.getDocumentId(oldPaMemVO.getMembershipID(), MembershipDocument.RENEWAL_NOTICE, oldPaMemVO.getProductEffectiveDate(), oldPaMemVO.getExpiryDate(), true); //include cancelled if(documentId==null) { documentId = memMgr.getDocumentId(oldPaMemVO.getMembershipID(), MembershipDocument.RENEWAL_ADVICE, oldPaMemVO.getProductEffectiveDate(), oldPaMemVO.getExpiryDate(), true); //include cancelled }
	 * 
	 * if(documentId==null) { return; } else { MembershipDocument oldDoc = memMgr.getMembershipDocument(documentId); if(!oldDoc.getDocumentStatus().equals (MembershipDocument.DOCUMENT_STATUS_CANCELLED)) { //if it is not cancelled already, then cancel it memMgr.cancelRenewalDocument(documentId); } else { System.out.println( ">>---> MembershipRenewalMgrBean.replaceRenewalNotice for member " + newPaMemVO.getClientNumber()); makeRenewalNotice(newPaMemVO,user); } }
	 * 
	 * }
	 */
	/*
	 * public void replaceRenewalNotice(MembershipVO oldPaMemVO, TransactionGroup transGroup, User user)throws Exception { //First check if there is a renewal document to replace MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr(); Integer documentId = memMgr.getDocumentId(oldPaMemVO.getMembershipID(), MembershipDocument.RENEWAL_NOTICE, oldPaMemVO.getProductEffectiveDate(), oldPaMemVO.getExpiryDate(), true); //include cancelled if(documentId==null) { documentId = memMgr.getDocumentId(oldPaMemVO.getMembershipID(), MembershipDocument.RENEWAL_ADVICE, oldPaMemVO.getProductEffectiveDate(), oldPaMemVO.getExpiryDate(), true); //include cancelled }
	 * 
	 * if(documentId==null) { //no existing renewal document, therefore nothing to do return; } else { MembershipDocument oldDoc = memMgr.getMembershipDocument(documentId); if(!oldDoc.getDocumentStatus().equals (MembershipDocument.DOCUMENT_STATUS_CANCELLED)) { //if it is not cancelled already, then cancel it memMgr.cancelRenewalDocument(documentId); } else { replaceRenewalNotice(transGroup,user); } } }
	 */
	/**
	 * Create and print a new renewal document for a given membership The document will only be printed if the membership is within the normal renewal period. Otherwise does nothing Does not check to see if there is an existing renewal document (Largely lifted from PrintRenewalAction) USe this in the context of some other transaction (change group, edit group, change group PA etc) where there is a transaction group. Create the renewal transaction group from the content of the supplied transaction group
	 * 
	 * @param MembershipVO The membership for which the document is to be printed.
	 * @param MembershipVO The membership to which the replaced document was attached. This is required for "change PA" transactions
	 * @param User The user creating the document
	 * @throws Exception
	 */
	public void replaceRenewalNotice(/* TransactionGroup transGroup, */
	MembershipVO memVO, MembershipVO oldMemVO, User user) throws Exception {
		try {
			// I think we just check if we are in the renewal period
			// First check if there is a renewal document to replace
			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
			Integer documentId = memMgr.getDocumentId(oldMemVO.getMembershipID(), MembershipDocument.RENEWAL_NOTICE, oldMemVO.getProductEffectiveDate(), oldMemVO.getExpiryDate(), true); // include cancelled
			if (documentId == null) {
				documentId = memMgr.getDocumentId(oldMemVO.getMembershipID(), MembershipDocument.RENEWAL_ADVICE, oldMemVO.getProductEffectiveDate(), oldMemVO.getExpiryDate(), true); // include cancelled
			}
			// if there is a renewal document, cancel it.
			if (documentId != null) {
				memMgr.cancelRenewalDocument(documentId, true);
				// no existing renewal document, therefore nothing to do
			}

			// if the fee effective date falls in the current
			// renewal period, ie today + 42 days, then
			// create a new renewal document
			CommonMgrLocal cMgr = CommonEJBHelper.getCommonMgrLocal();
			String preRenewalPeriodSpec = cMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.IN_RENEWAL_PERIOD);
			Interval preRenewalPeriod = null;
			try {
				preRenewalPeriod = new Interval(preRenewalPeriodSpec);
			} catch (Exception e) {
				throw new RemoteException(e.getMessage());
			}
			DateTime renewIfBefore = new DateTime();
			renewIfBefore = renewIfBefore.add(preRenewalPeriod);
			if (memVO.getExpiryDate().afterDay(renewIfBefore)) {
				return;
			}

			TransactionGroup transactionGroup = TransactionGroup.getReplaceRenewalTransactionGroup(/*
																									 * transGroup ,
																									 */
			memVO, user /*
						 * , feeEffectiveDate
						 */);
			String printGroup = user.getPrinterGroup();
			Properties renewalProperties = produceIndividualRenewalNotice(transactionGroup.getMembershipGroupPA(), transactionGroup, user, null);
			MembershipReportHelper.printMembershipRenewal(user, printGroup, renewalProperties, transactionGroup);
		} catch (Exception e) {
			LogUtil.warn(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
		}
	}

}
