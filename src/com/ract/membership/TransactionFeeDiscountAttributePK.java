package com.ract.membership;

import java.io.Serializable;

public class TransactionFeeDiscountAttributePK implements Serializable {

    /**
     * @hibernate.key-property column="fee_specification_id"
     * @return String
     */
    public Integer getFeeSpecificationID() {
	return feeSpecificationID;
    }

    /**
     * @hibernate.key-property column="transaction_id"
     * @return String
     */
    public Integer getTransactionID() {
	return transactionID;
    }

    /**
     * @hibernate.key-property column="discount_type_code"
     * @return String
     */
    public String getDiscountTypeCode() {
	return discountTypeCode;
    }

    public void setAttributeName(String attributeName) {
	this.attributeName = attributeName;
    }

    public void setFeeSpecificationID(Integer feeSpecificationID) {
	this.feeSpecificationID = feeSpecificationID;
    }

    public void setTransactionID(Integer transactionID) {
	this.transactionID = transactionID;
    }

    public void setDiscountTypeCode(String discountTypeCode) {
	this.discountTypeCode = discountTypeCode;
    }

    /**
     * @hibernate.key-property column="attribute_name"
     * @return String
     */
    public String getAttributeName() {
	return attributeName;
    }

    public TransactionFeeDiscountAttributePK() {
    }

    /**
     * The ID of the fee specification that the transaction fee is associated
     * with.
     */
    private Integer feeSpecificationID;

    /**
     * The code for the discount type that the transaction discount is
     * associated with.
     */
    private String discountTypeCode;

    private Integer transactionID;

    private String attributeName;

    public boolean equals(Object obj) {
	if (obj instanceof TransactionFeeDiscountAttributePK) {
	    TransactionFeeDiscountAttributePK that = (TransactionFeeDiscountAttributePK) obj;
	    return this.transactionID.equals(that.transactionID) && this.feeSpecificationID.equals(that.feeSpecificationID) && this.discountTypeCode.equals(that.discountTypeCode) && this.attributeName.equals(that.attributeName);
	}
	return false;
    }

    public int hashCode() {
	return this.transactionID.hashCode() + this.feeSpecificationID.hashCode() + this.discountTypeCode.hashCode() + this.attributeName.hashCode();
    }

    public String toString() {
	return this.transactionID + "/" + this.feeSpecificationID + "/" + this.discountTypeCode + "/" + this.attributeName;
    }

}
