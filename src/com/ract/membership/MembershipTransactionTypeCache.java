package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.CacheBase;

/**
 * This class caches a list of products in memory. It uses the Singleton design
 * pattern. Use the following code to call the methods on this class.
 * 
 * ProductCache.getInstance().getProduct(<membershipTypeCode>);
 * ProductCache.getInstance().getProductList();
 */

public class MembershipTransactionTypeCache extends CacheBase {
    private static MembershipTransactionTypeCache instance = new MembershipTransactionTypeCache();

    /**
     * Implement the getItemList method of the super class. Return the list of
     * cached items. Add items to the list if it is empty.
     */
    public void initialiseList() throws RemoteException {
	// log("Initialising MembershipTransactionType cache.");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection dataList = membershipMgr.getMembershipTransactionTypes();
	this.addAll(dataList);
    }

    /**
     * Return a reference the instance of this class.
     */
    public static MembershipTransactionTypeCache getInstance() {
	return instance;
    }

    /**
     * Return the specified Membership Transaction Type value object. Search the
     * list of Membership Transaction Types for one that has the same code. Use
     * the getList() method to make sure the list has been initialised.
     */
    public MembershipTransactionTypeVO getTransactionType(String transactionTypeCode) throws RemoteException {
	return (MembershipTransactionTypeVO) this.getItem(transactionTypeCode);
    }

    /**
     * Return the list of products. Use the getList() method to make sure the
     * list has been initialised. A null or an empty list may be returned.
     */
    public ArrayList getTransactionTypeList() throws RemoteException {
	return this.getItemList();
    }

}
