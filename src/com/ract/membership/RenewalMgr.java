package com.ract.membership;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;

import javax.ejb.Remote;

import com.ract.common.SystemException;
import com.ract.common.reporting.ReportException;
import com.ract.util.DateTime;

/**
 * Title: Master Project Description: Brings all of the projects together into
 * one master project for deployment. Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @created 5 March 2003
 * @version 1.0
 */
@Remote
public interface RenewalMgr {

    public void constructSMSRenewalReminders(DateTime targetDate, Collection membershipList) throws RenewalNoticeException, IOException, RemoteException;

    public int createMembershipRenewalNotices() throws RemoteException, RenewalNoticeException;

    public int createMembershipRenewalNotices(DateTime effectiveDate, String userId) throws RemoteException, RenewalNoticeException;

    public boolean isSocialMember(Integer memId) throws Exception, RemoteException;

    public Hashtable removeRenewalBatch(Integer runNumber) throws RemoteException;

    public Collection getSMSRenewalReminderList(DateTime targetDate) throws RemoteException;

    public int clearEmailSentFlag(Integer runNumber) throws RemoteException;

    /**
     * Create a renewal notice for the selected member.
     * 
     * @param memVO
     *            Description of the Parameter
     * @param transactionGroup
     *            Description of the Parameter
     * @param userSession
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception SystemException
     *                Description of the Exception
     * @exception RemoteException
     *                Description of the Exception
     */
    // public Properties produceIndividualRenewalNotice(
    // MembershipVO memVO,
    // TransactionGroup transactionGroup,
    // User user)
    // throws SystemException, RemoteException;

    public Hashtable<String, File> mergeRenewalFile(File marketingFile, File renewalFile, boolean defaultMarketing) throws RemoteException;

    public Hashtable<String, Integer> emailRenewals(String renewalFile, boolean update, boolean test) throws ReportException;

    /**
     * Returns a collection of RenewalBatchVO objects
     */
    public Vector getAllRenewalBatches() throws RemoteException;

    public RenewalBatchVO getBatchVO(int runNumber, boolean loadBatchTotals) throws RemoteException;

    public void emailRenewal(Integer renewalClientNumber, String emailAddress, boolean update, boolean test) throws RemoteException, SystemException, ReportException;

    public RenewalBatchVO getLastRun() throws RenewalNoticeException, RemoteException;

    public Collection getAutoRenewables(DateTime firstDate, DateTime lastDate) throws RemoteException;

    public void processAutomaticRenewals(DateTime fromDate, DateTime toDate) throws RemoteException;

    // public Hashtable constructDirectDebitDetails(MembershipVO memVO,
    // TransactionGroup transGroup, String format)
    // throws RenewalNoticeException, RemoteException;

}
