package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.RemoveException;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.Session;

import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientNote;
import com.ract.client.ClientSubscription;
import com.ract.client.ClientSubscriptionPK;
import com.ract.client.ClientVO;
import com.ract.client.NoteType;
import com.ract.common.Account;
import com.ract.common.BusinessType;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.Company;
import com.ract.common.DataSourceFactory;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.Publication;
import com.ract.common.ReferenceDataVO;
import com.ract.common.RollBackException;
import com.ract.common.SequenceMgr;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.mail.MailMessage;
import com.ract.common.notifier.NotificationMgr;
import com.ract.common.notifier.NotificationMgrLocal;
import com.ract.membership.notifier.MembershipChangeNotificationEvent;
import com.ract.membership.reporting.MembershipReportHelper;
import com.ract.membership.ui.MembershipUIHelper;
import com.ract.payment.FinanceVoucher;
import com.ract.payment.PayableItemComponentVO;
import com.ract.payment.PayableItemGenerator;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.PostingContainer;
import com.ract.payment.billpay.IVRPayment;
import com.ract.payment.billpay.PendingFee;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.directdebit.ProgressDDAdapter;
import com.ract.payment.finance.FinanceMgr;
import com.ract.payment.finance.FinanceMgrLocal;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.security.Privilege;
import com.ract.security.SecurityHelper;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.user.UserMgr;
import com.ract.user.UserSession;
import com.ract.util.ConnectionUtil;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;

/**
 * The membership transaction session bean. Handles all of the transaction processing for membership.
 * 
 * @author bakkert,hollidayj,wiggg
 * @created 20 October 2002
 */
@Stateless
@Remote({ MembershipTransactionMgr.class })
@Local({ MembershipTransactionMgrLocal.class })
public class MembershipTransactionMgrBean {

	@EJB
	private MembershipMgrLocal membershipMgrLocal;

	@EJB
	private ClientMgrLocal clientMgrLocal;

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	private DataSource clientDataSource;

	@PersistenceContext(unitName = "master")
	Session hsession;

	/**
	 * Process an undo transaction
	 * 
	 * @param transGroup Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException Description of the Exception
	 * @exception ValidationException Description of the Exception
	 */
	public Collection processTransaction(UndoTransactionGroup transGroup) throws RemoteException, ValidationException {
		LogUtil.log(this.getClass(), "processTransaction(undo) start");
		if (!transGroup.isUndoable()) {
			throw new ValidationException("The transaction cannot be undone.");
		}

		// Just a few variable declarations outside of the loop.
		DateTime transactionDate = new DateTime();
		Integer groupId = null;

		GroupVO group = null;
		UserSession userSession = transGroup.getUserSession();
		ArrayList completedTransactionList = new ArrayList();
		MembershipTransactionVO undoTransVO;
		MembershipTransactionVO revertToTransVO;
		MembershipVO undoMemVO;
		MembershipVO revertToMemVO;
		Collection documentList;
		MembershipGroupDetailVO memGroupDetail;
		// Collection groupDetailList;
		Iterator groupDetailIterator;
		MembershipGroupVO memGroupVO;
		MembershipGroupDetailVO memGroupDetailVO;
		PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
		MembershipChangeNotificationEvent notificationEvent = null;
		NotificationMgr notificationMgr = CommonEJBHelper.getNotificationMgr();
		FinanceMgrLocal financeMgr = PaymentEJBHelper.getFinanceMgrLocal();

		try {

			for (int i = 0; i < transGroup.getTransactionListSize(); i++) {
				undoTransVO = transGroup.getUndoTransactionVO(i);
				revertToTransVO = transGroup.getRevertToTransactionVO(i);
				undoMemVO = transGroup.getUndoMembershipVO(i);
				revertToMemVO = transGroup.getRevertToMembershipVO(i);

				LogUtil.log(this.getClass(), "undoTransVO=" + undoTransVO);
				LogUtil.log(this.getClass(), "revertToTransVO=" + revertToTransVO);

				LogUtil.log(this.getClass(), "processTransaction(undo) 1");

				// Put the membership group records back the way they were.
				// First remove the membership from all groups by finding the
				// list of groups the member
				// belongs to first
				memGroupDetailVO = MembershipHelper.removeFromAllGroups(undoTransVO.getMembershipID());
				LogUtil.log(this.getClass(), "processTransaction(undo) 2 " + memGroupDetailVO);
				// Create transactions and Membership that are transferred are
				// treated differently from other transactions
				// as there is no previous membership state to revert back to.
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(undoTransVO.getTransactionTypeCode()) ||
				// rejoin following an undo
				(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(undoTransVO.getTransactionTypeCode()) && revertToTransVO == null)) {
					// The membership status is changed to "Undone".
					undoMemVO.setStatus(MembershipVO.STATUS_UNDONE);
					// expiry is set to commence date
					undoMemVO.setExpiryDate(undoMemVO.getCommenceDate());

					// reset all affiliated club details
					undoMemVO.setAffiliatedClubCode(null);
					undoMemVO.setAffiliatedClubId(null);
					undoMemVO.setAffiliatedClubExpiryDate(null);
					undoMemVO.setAffiliatedClubProductLevel(null);

					membershipMgrLocal.updateMembership(undoMemVO);

					undoTransVO.setNewMembership(undoMemVO);

					// Remove all card requests.
					removeMembershipCardRequests(undoMemVO);

					// Remove any membership documents
					membershipMgrLocal.removeDocumentList(undoTransVO.getMembershipID());

					// Undoing the create of a membership is equivalent to
					// removing it. Sort of.
					notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_REMOVE, undoMemVO);
				} else {
					// For all transaction types other than a "Create" revert
					// the
					// membership state to a valid state prior to the
					// transaction being undone.
					membershipMgrLocal.updateMembership(revertToMemVO);

					undoTransVO.setNewMembership(revertToMemVO);

					// Remove all card request created on or after the revert to
					// transaction date
					removeMembershipCardRequests(undoMemVO);

					LogUtil.log(this.getClass(), "processTransaction(undo) 2 " + memGroupDetailVO);

					// Insert the previous membership group detail
					memGroupVO = revertToMemVO.getMembershipGroup();

					// currently a group
					if (memGroupDetailVO != null) {
						LogUtil.log(this.getClass(), "processTransaction(undo) 3 " + memGroupDetailVO);
						// previously a group
						if (memGroupVO != null) {
							LogUtil.log(this.getClass(), "processTransaction(undo) 4 " + memGroupVO);
							// update the details
							MembershipGroupDetailVO groupDetail = memGroupVO.getMembershipGroupDetail(undoTransVO.getMembershipID());
							LogUtil.log(this.getClass(), "processTransaction(undo) 4a " + groupDetail);
							if (groupDetail != null) {

								LogUtil.log(this.getClass(), "processTransaction(undo) 5");
								memGroupDetailVO.setGroupJoinDate(groupDetail.getGroupJoinDate());
								memGroupDetailVO.setPrimeAddressee(groupDetail.isPrimeAddressee());
								LogUtil.log(this.getClass(), "processTransaction(undo) 6");
								membershipMgrLocal.createMembershipGroupDetail(memGroupDetailVO);
							}
						}
					}
					// not currently a group
					else {
						LogUtil.log(this.getClass(), "processTransaction(undo) 7");
						// previously a group
						if (memGroupVO != null) {
							LogUtil.log(this.getClass(), "processTransaction(undo) 8");
							memGroupDetailVO = memGroupVO.getMembershipGroupDetail(undoTransVO.getMembershipID());
							LogUtil.log(this.getClass(), "processTransaction(undo) 8a");
							if (memGroupDetailVO != null) {
								LogUtil.log(this.getClass(), "processTransaction(undo) 9");
								membershipMgrLocal.createMembershipGroupDetail(memGroupDetailVO);
							}
						}
						// if not previously a group then no group records will
						// be created
					}

					// Reinstate the membership in prosper
					notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE, revertToMemVO);

				}

				LogUtil.log(this.getClass(), "processTransaction(undo) 10");

				// remove unprocessed credit disposal if one exists
				Integer disposalId = undoTransVO.getDisposalId();
				if (disposalId != null) {
					FinanceVoucher voucher = financeMgr.getVoucher(disposalId);
					if (voucher != null) {
						if (voucher.getStatus() != null) {
							throw new SystemException("Credit disposal has been processed.");
						}
						financeMgr.deleteVoucher(voucher);
					}
				}

				// If the transaction being undone is the payee then remove the
				// payable item.
				if (undoTransVO.getPayableItemID() != null) {

					if (revertToTransVO != null) {

						// effectively cancellations
						if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(undoTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(undoTransVO.getTransactionTypeCode())) {
							if (revertToTransVO == null) {
								throw new SystemException("Revert transaction is not available.");
							}
							paymentTxMgr.undoPayableItem(undoTransVO.getPayableItemID(), revertToTransVO.getPayableItemID(), PaymentTransactionMgr.ACTION_UNCANCEL, userSession);
						} else {
							paymentTxMgr.undoPayableItem(undoTransVO.getPayableItemID(), revertToTransVO.getPayableItemID(), userSession);
						}

					} else {
						paymentTxMgr.undoPayableItem(undoTransVO.getPayableItemID(), null, userSession);
					}
				}

				// The transaction is tagged as undone and disconnected
				// from the payable item which has now been removed.
				undoTransVO.setReadOnly(false);
				undoTransVO.setUndoneDate(transactionDate);
				undoTransVO.setUndoneUsername(userSession.getUserId());
				undoTransVO.setPayableItemID(null);
				membershipMgrLocal.updateMembershipTransaction(undoTransVO);

				// Publish the notification event
				if (notificationEvent != null) {
					notificationMgr.notifyEvent(notificationEvent);
				}

				// Read the updated transaction back and add it to the returned
				// list
				// of completed transactions.
				completedTransactionList.add(undoTransVO);

			}

			// Commit the membership transaction
			paymentTxMgr.commit();
			notificationMgr.commit();

			LogUtil.log(this.getClass(), "processTransaction(undo) end");
		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), "Undo: Error processing undo transaction. ");
			e.printStackTrace();

			// Rollback the payment system updates
			if (paymentTxMgr != null) {
				try {
					paymentTxMgr.rollback();
				} catch (Exception pe) {
					LogUtil.fatal(this.getClass(), "Undo: Failed to rollback payment system for undo of transaction.");
				}
			}

			try {
				if (notificationMgr != null) {
					notificationMgr.rollback();
				}

			} catch (Exception se) {
				LogUtil.fatal(this.getClass(), "Undo: Failed to roll back published notification events.");
				se.printStackTrace();
			}

			// The transaction has been rolled back.
			// Throw the exception back up so the UI can handle it.
			if (e instanceof ValidationException) {
				throw (ValidationException) e;
			} else if (e instanceof SystemException) {
				throw (SystemException) e;
			} else if (e instanceof RemoteException) {
				throw (RemoteException) e;

			} else {

				throw new SystemException(e);
			}
		}
		return completedTransactionList;
	}

	private void createSnapshotForOldMemberships(TransactionGroup transGroup) throws SystemException {
		Collection transactionList = transGroup.getTransactionList();
		try {
			MembershipVO memVO = null;
			MembershipTransactionVO memTransVO = null;

			Iterator transListIT = transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				LogUtil.debug(this.getClass(), "createSnapshotForOldMemberships=" + memTransVO.toString());
				memVO = memTransVO.getMembership(); // getMembership - old
				if (memVO != null) {
					createMembershipXMLForLastTransaction(memVO);
				}
			}
		} catch (Exception e) {
			throw new SystemException("Unable to create snapshots for existing membership state.", e);
		}
	}

	public Collection processTransaction(TransactionGroup transGroup) throws RemoteException, ValidationException {
		return processTransaction(transGroup, null, null);
	}

	/**
	 * Process the transaction and save the data to the database. Return a list of all of the memberships involved in the transaction whether they were created/updated or not.
	 * 
	 * @param transGroup Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException Description of the Exception
	 * @exception ValidationException Description of the Exception
	 */
	public Collection processTransaction(TransactionGroup transGroup, NotificationMgrLocal notificationMgrLocal, PaymentTransactionMgrLocal paymentTransactionMgrLocal) throws RemoteException, ValidationException {

		boolean externallyInitiatedTransaction = (notificationMgrLocal != null && paymentTransactionMgrLocal != null);
		boolean renewalCancelled = false;
		boolean replaceRenewalDocument = false;
		MembershipTransactionVO memTransVO;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;

		TransactionHelper transactionHelper = new TransactionHelper();

		if (transGroup == null) {
			throw new SystemException("Unable to process transaction. Transaction object is null.");
		}

		// logging
		Integer memTransCount = null;
		Integer paClientNumber = null;
		String userID = "";

		try {
			memTransCount = new Integer(transGroup.getMembershipTransactionCount());
			paClientNumber = transGroup.getClientForPA().getClientNumber();
			userID = transGroup.getUserID();
		} catch (Exception exp) {
			// ignore
		}
		Vector transactionList = transGroup.getTransactionList();
		if (transactionList == null || transactionList.isEmpty()) {
			throw new SystemException("Transaction list is empty. Cannot process " + transGroup.getTransactionGroupTypeCode() + " transaction.");
		}

		// The returned list of memberships that were involved in the
		// transaction.
		ArrayList completedTransactionList = new ArrayList();
		MembershipChangeNotificationEvent notificationEvent = null;

		try {
			// create membership snapshot XML documents for old membership
			// that don't have one so that we can undo this transaction.
			// This can be done in a stand alone transaction
			createSnapshotForOldMemberships(transGroup);

			// Start the main transaction block
			if (paymentTransactionMgrLocal == null) {
				paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
			}
			if (notificationMgrLocal == null) {
				notificationMgrLocal = CommonEJBHelper.getNotificationMgrLocal();
			}

			// Set the transaction date to the exact time the transaction is
			// saved.
			transGroup.setTransactionDate(new DateTime());

			// Set the transaction group ID if more than one membership
			// transaction is being processed.
			transGroup.setTransactionGroupID();

			// Set the ID of the membership transactions before sending the fees
			// to the payment system
			transGroup.setMembershipTransactionIDs();

			// Get the membership group ID. Will be null if a membership group
			// is not being created or updated.
			Integer membershipGroupID = transactionHelper.getMembershipGroupID(transGroup);

			// hold the saved membership so that it doesn't overwrite transient
			// attributes.
			Integer clientNumber;
			String transTypeCode;
			String cardReasonCode = null;

			ListIterator transListIT = transactionList.listIterator();

			int groupCount = transGroup.getActualGroupMemberCount();
			if (groupCount == 0) {
				groupCount = 1;
			}

			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				transTypeCode = memTransVO.getTransactionTypeCode();
				oldMemVO = memTransVO.getMembership();

				newMemVO = memTransVO.getNewMembership();
				LogUtil.debug(this.getClass(), "newMemVO=" + newMemVO);
				LogUtil.debug(this.getClass(), "oldMemVO=" + oldMemVO);
				clientNumber = newMemVO.getClientNumber();

				// set other transaction attributes
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(transTypeCode)) {
					// Leave the product code alone if removing from group
					memTransVO.setProductCode(oldMemVO.getProductCode());
				} else {
					memTransVO.setProductCode(newMemVO.getProductCode());
				}
				memTransVO.setGroupCount(new Integer(groupCount));
				notificationEvent = null;

				/**
				 * @todo this should already be set when the transactions are created in the transaction group
				 */
				memTransVO.setTransactionGroupTypeCode(transGroup.getTransactionGroupTypeCode());

				// Update the Motor News mailing list for the membership
				// RLG Change Request 76 updateMotorNewsMailingList(newMemVO);

				// If there is no existing membership then do a create
				// otherwise do an update
				if (memTransVO.isCreatingMembership()) {
					// Set the motor news send option ONLY FOR CREATE
					updateMotorNewsMailingList(newMemVO);

					// if creating roadside, record date
					if (transGroup.getSelectedProduct().includesRoadsideAssistance()) {
						LogUtil.debug(getClass(), "\n**\n* Saving roadside date for client no: " + newMemVO.getClientNumber() + "\n**");
						newMemVO.setRoadsideCreateDate(new DateTime());
					}

					newMemVO = createMembership(newMemVO, memTransVO.getTransactionDate());

					LogUtil.debug(this.getClass(), "isCreatingMembership newMemVO=" + newMemVO);
					// Set the membership ID on the membership transaction now
					// that we have it.
					// CHANGED 30/11/2004 JH
					memTransVO.setNewMembership(newMemVO);
					// side effect is that it clears the transient attributes

					notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_CREATE, newMemVO);

				} else {

					// if changing product to roadside (from Access or Lifestyle (MEM-461 gzs 11/03/2016)), or
					// rejoining, update create date
					if (((MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transTypeCode) && (oldMemVO.getProductCode().equals(ProductVO.PRODUCT_ACCESS) || oldMemVO.getProductCode().equals(ProductVO.PRODUCT_LIFESTYLE))) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) && transGroup.getSelectedProduct().includesRoadsideAssistance()) {
						LogUtil.log(getClass(), "\n**\n* Updating roadside date for client no: " + newMemVO.getClientNumber() + "\n**");
						newMemVO.setRoadsideCreateDate(new DateTime());
					}

					// id already on membership
					newMemVO = membershipMgrLocal.updateMembership(newMemVO);

					if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(transTypeCode)) {
						// The membership has not changed directly so the action
						// on the
						// membership change notification event indicates this.
						notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE_GROUP, memTransVO.getMembership());
						replaceRenewalDocument = true;
					} else {
						notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE, newMemVO);
					}

				}
				// if renewing and DD is null, remove DD reference from member
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(transTypeCode)) {
					if (newMemVO.isPrimeAddressee() && transGroup.getDirectDebitAuthority() == null) {
						newMemVO.setDirectDebitAuthorityID(null);
					}
				}

				LogUtil.debug(this.getClass(), "b4 updateMembershipGroup");
				// Update the membership group records for this membership
				updateMembershipGroup(memTransVO, membershipGroupID);
				LogUtil.debug(this.getClass(), "after updateMembershipGroup");

				// store txn reason if we haven't already
				if (memTransVO.getTransactionReason() == null && transGroup.getTransactionReason() != null) {
					String reasonCode = transGroup.getTransactionReason().getReferenceDataPK().getReferenceCode();
					String reasonType = transGroup.getTransactionReason().getReferenceDataPK().getReferenceType();
					memTransVO.setTransactionReason(reasonType, reasonCode);
				}

				// Save the membership transaction.
				// A new membership transaction value object is returned with
				// all
				// references to other objects set to null.
				memTransVO = transactionHelper.createMembershipTransaction(membershipMgrLocal, memTransVO);

				LogUtil.debug(this.getClass(), "after createMembershipTransaction");
				// Workout a reason for requesting a new card.
				// A null reason code means that the
				// createMembershipCardRequest()
				// method will work it out.
				cardReasonCode = null;
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transTypeCode) || ((MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(transTypeCode)) && !oldMemVO.getProductCode().equals(newMemVO.getProductCode()))) {
					// A renewal involved a change of product. Show the reason
					// as a change of product.
					cardReasonCode = ReferenceDataVO.CARD_REQUEST_CHANGE_PRODUCT;
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST.equals(transTypeCode)) {
					// Use the transaction reason code
					cardReasonCode = memTransVO.getTransactionReasonCode();
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode)) {
					// Use the transaction reason code
					cardReasonCode = ReferenceDataVO.CARD_REQUEST_NEW_MEMBERSHIP;
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) {
					// Use the transaction reason code
					cardReasonCode = ReferenceDataVO.CARD_REQUEST_REJOIN_MEMBERSHIP;
				}

				if (newMemVO.getProduct().isVehicleBased()) {
					if (oldMemVO != null && oldMemVO.getRego() != null && !oldMemVO.getRego().equalsIgnoreCase(newMemVO.getRego())) {
						cardReasonCode = ReferenceDataVO.CARD_REQUEST_CHANGE_REGO;
					}
				}

				/*
				 * Remove any pending fees and payments pending for all members if there is an old membership and it is not a card request, or if direct debit authority has been removed (i.e. changed to pay by cash)
				 */
				if (oldMemVO != null && (transGroup.replacesTransaction() || (oldMemVO.isPrimeAddressee() && oldMemVO.getDirectDebitAuthorityID() != null && newMemVO != null && newMemVO.isPrimeAddressee() && newMemVO.getDirectDebitAuthority() == null) || replaceRenewalDocument)) {
					LogUtil.log(this.getClass(), "cancelling Membership Document for PA");
					boolean ignoreErrors = transGroup.isBatch(); // ignore
					// payment
					// errors for
					// batch
					// processing
					renewalCancelled = cancelMembershipDocument(oldMemVO, paymentTransactionMgrLocal, ignoreErrors);
				}
				LogUtil.debug(this.getClass(), "after cancelMembershipDocument");

				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(transTypeCode)) {
					// Remove any unissued card requests
					removeMembershipCardRequests(oldMemVO);
				} else if ((MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP.equals(transTypeCode) && !newMemVO.getProduct().isVehicleBased()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_WRITE_OFF.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REFUND.equals(transTypeCode)) {
					// Ignore request
				} else {
					// Create a request for a new membership card if the state
					// of
					// the membership indicates that a new card is required.
					transactionHelper.createMembershipCardRequest(membershipMgrLocal, newMemVO, cardReasonCode, userID, MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST.equals(transTypeCode));
				}

				LogUtil.debug(this.getClass(), "after createMembershipCardRequest");

				// Publish the notification event
				if (notificationEvent != null) {
					notificationMgrLocal.notifyEvent(notificationEvent);
				}

				LogUtil.debug(this.getClass(), "after notifications");

				// Add the membership to the returned list of memberships that
				// were
				// affected by this transaction.

				completedTransactionList.add(memTransVO);

				// produce a manual renewal notice if the membership is due to
				// expire within prescribed time
				CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

				Interval renewalGenerationPeriod = new Interval(commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.TRANSFER_MINIMUM_PERIOD));

				LogUtil.debug(this.getClass(), "renewalGenerationPeriod=" + renewalGenerationPeriod);
				DateTime generateRenewal = transGroup.getTransactionDate().add(renewalGenerationPeriod);
				LogUtil.debug(this.getClass(), "generateRenewal=" + generateRenewal);
				// if a create or rejoin in the context of a transfer and the
				// expiry date is within the period
				// that should automatically generate a renewal notice, print a
				// membership renewal.
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(transGroup.getTransactionGroupTypeCode()) && (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) && newMemVO.expiryDate.onOrBeforeDay(generateRenewal))
				// || renewalCancelled && replaceRenewalDocument)
				{
					User user = transGroup.getUser();
					LogUtil.debug(this.getClass(), "user=" + user);
					TransactionGroup renewalTransactionGroup = TransactionGroup.getRenewalTransactionGroup(newMemVO, user, newMemVO.expiryDate);
					LogUtil.debug(this.getClass(), "r 1");

					// will lock up trying to read the card requests?!!!!
					MembershipRenewalMgr renewalMgr = MembershipEJBHelper.getMembershipRenewalMgr();
					Properties renewalProperties = renewalMgr.produceIndividualRenewalNotice(newMemVO, renewalTransactionGroup, user, paymentTransactionMgrLocal);
					MembershipReportHelper.printMembershipRenewal(user, user.getPrinterGroup(), renewalProperties, renewalTransactionGroup);
				}

				LogUtil.debug(this.getClass(), "after produceIndividualRenewalNotice");

				// If the membership is Lifestyle - create an email renewal subscription
				if (newMemVO.productCode.equals("Lifestyle")) 
					createEmailRenewalSubscription(newMemVO);

			} // transactions

			// don't remove the pending fee if we are attempting to repair it.
			String repairPaymentMethodID = transGroup.getRepairPaymentMethodId();

			LogUtil.log(this.getClass(), "sendAmountPayable 1");

			// Send the amount payable to the payment system.
			// check that this transaction should have postings
			PayableItemVO payableItem = null;
			if (transGroup.requiresPostings()) {
				LogUtil.log(this.getClass(), "sendAmountPayable 2");
				payableItem = sendAmountPayable(transGroup, paymentTransactionMgrLocal);
			}

			LogUtil.debug(this.getClass(), "update the payable item details");
			transactionHelper.setMembershipTransactions(membershipMgrLocal, transGroup, payableItem);

			LogUtil.debug(this.getClass(), "update the saved transactions and set the membershipXML attribute");
			Iterator completedListIterator = completedTransactionList.iterator();
			while (completedListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) completedListIterator.next();
				transactionHelper.updateMembershipTransactionMembershipXML(membershipMgrLocal, memTransVO);
			}

			LogUtil.log(getClass(), "start completing Membership Transaction");

			if (!externallyInitiatedTransaction) {
				LogUtil.log(getClass(), "start payment Transaction");
				paymentTransactionMgrLocal.commit();
				LogUtil.log(getClass(), "end payment Transaction");

				notificationMgrLocal.commit();
			}

			LogUtil.log(getClass(), "end completing Membership Transaction");

		} catch (Exception e) {
			e.printStackTrace();

			LogUtil.fatal(this.getClass(), "Error processing transaction. " + e);
			LogUtil.fatal(this.getClass(), "Transaction Rolled Back in processTransaction");
			LogUtil.fatal(this.getClass(), "UserID " + userID + ", Prime Addressee client number = " + paClientNumber + ", membership transaction count = " + memTransCount);

			// rollback the payment transactions
			try {
				if (!externallyInitiatedTransaction && paymentTransactionMgrLocal != null) {
					paymentTransactionMgrLocal.rollback();
				}
			} catch (Exception pe) {
				LogUtil.fatal(this.getClass(), "Failed to roll back payment information. " + pe);
			}

			// rollback the published events
			try {
				if (!externallyInitiatedTransaction && notificationMgrLocal != null) {
					notificationMgrLocal.rollback();
				}
			} catch (Exception pe) {
				LogUtil.fatal(this.getClass(), "Failed to roll back events published to notification manager. " + pe);
			}

			// The transaction has been rolled back.
			// Throw the exception back up so the UI can handle it.
			if (e instanceof ValidationException) {
				throw (ValidationException) e;
			} else if (e instanceof SystemException) {
				throw (SystemException) e;
			} else if (e instanceof RemoteException) {
				throw (RemoteException) e;
			} else {
				throw new SystemException(e);
			}
		}

		LogUtil.log(getClass(), "end processTransaction");

		return completedTransactionList;
	} // processTransaction

	/**
	 * Create a new membership.
	 * 
	 * @param memVO Description of the Parameter
	 * @param transactionDate Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException Description of the Exception
	 * @throws GenericException 
	 */
	private MembershipVO createMembership(MembershipVO memVO, DateTime transactionDate) throws RemoteException, GenericException {

		// Make sure that the client only has one 'Personal' membership.
		String memTypeCode = memVO.getMembershipTypeCode();
		if (MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL.equals(memTypeCode)) {
			MembershipVO membership = membershipMgrLocal.findMembershipByClientAndType(memVO.getClientNumber(), memTypeCode);
			if (membership != null) {
				// This should already have been checked so make it a system
				// error.
				throw new SystemException("A client may only have one " + MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL + " membership.");
			}
		}

		membershipMgrLocal.createMembership(memVO);

		// If the membership is Lifestyle - create an email renewal subscription
		if (memVO.productCode.equals("Lifestyle")) 
			createEmailRenewalSubscription(memVO);
		
		LogUtil.debug(this.getClass(), "memVO=" + memVO);
		return memVO;
	}

	/**
	 * createEmailRenewalSubscription
	 * 
	 * Create a new email renewal subscription.
	 * 
	 * @throws RemoteException 
	 * @throws GenericException 
	 * 
	 */
	private void createEmailRenewalSubscription(MembershipVO memVO) throws RemoteException, GenericException {
		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();

		ClientVO client = memVO.getClient();
		String emailAddress = client.getEmailAddress();
		if (emailAddress == null || emailAddress.trim().length() == 0) {
			throw new GenericException("There is currently no email address for client " + client.getClientNumber());
		} else {
			StringUtil.validateEmail(emailAddress, false);
		}

		ClientSubscription sub = clientMgr.getActiveClientSubscription(memVO.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL);
		if (sub != null) {
			// remove old subscription if its already there - note that this is NOT a toggle
			clientMgr.deleteClientSubscription(sub.getClientSubscriptionPK());
		}

		ClientSubscriptionPK pk = new ClientSubscriptionPK(memVO.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL, ClientSubscription.STATUS_ACTIVE, new DateTime());
		ClientSubscription clientSubscription = new ClientSubscription();
		clientSubscription.setClientSubscriptionPK(pk);
		clientSubscription.setUserId("daemon");

		// create subscription
		clientMgr.createClientSubscription(clientSubscription);
	}

	/**
	 * Process a payment which has been made using Telstra pay by phone. Renews the membership, creates receipting records and marks them as paid Marks the relevant pending fee record as having been paid
	 * 
	 * @IVRPayment ivr Description of the Parameter
	 * @exception RemoteException
	 */
	public void notifyPendingFeePaid(IVRPayment ivr) throws RemoteException {

		TransactionHelper transactionHelper = new TransactionHelper();

		// Get the MembershipVO from the PendingFee
		Integer membershipID = new Integer(ivr.getReferenceId());
		MembershipVO contextMemVO = membershipMgrLocal.getMembership(membershipID);

		if (contextMemVO == null) {
			ivr.addStatus("Membership Not Found");
		}

		LogUtil.debug(this.getClass(), "ivr=" + ivr.toString());

		// Get last transaction date

		// Date for fee calculation
		MembershipDocument renVO = membershipMgrLocal.getRenewalDocument(membershipID, contextMemVO.getExpiryDate());

		User user = SecurityHelper.getIVRUser();
		TransactionGroup transGroup = TransactionGroup.getRenewalTransactionGroup(contextMemVO, user, renVO.getFeeEffectiveDate());
		// transGroup.createCreditDiscounts();
		// transGroup.calculateTransactionFee();
		double amountPayable = transGroup.getAmountPayable();
		// double ivrAmount = ivr.getAmount();

		if (amountPayable != ivr.getAmount()) {
			throw new RemoteException("Balance Error. Required " + amountPayable + " have " + ivr.getAmount());
		}

		transGroup.getMembershipTransactionForPA().setPendingFeeID(ivr.getPhonePayNumber());

		// Process Transaction Group
		PaymentTransactionMgr paymentTxMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
		NotificationMgrLocal notificationMgrLocal = CommonEJBHelper.getNotificationMgrLocal();

		ArrayList completedTransactionList = null;
		createSnapshotForOldMemberships(transGroup);

		try {
			completedTransactionList = transactionHelper.processRenewalTransaction(membershipMgrLocal, transGroup, notificationMgrLocal); // ,
			// "IVR");
			// create Payable item
			PayableItemVO payableItem = transactionHelper.processRenewalAmountPayable(transGroup, ivr.getAmount(), amountPayable - ivr.getAmount(), ivr.getOpSequenceNumber(), paymentTxMgrLocal, null); // empty
			// string

			// update the saved transactions and set the membershipXML attribute
			MembershipTransactionVO memTransVO = null;

			Iterator completedListIterator = completedTransactionList.iterator();
			while (completedListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) completedListIterator.next();
				transactionHelper.updateMembershipTransactionMembershipXML(membershipMgrLocal, memTransVO);
			}

			transactionHelper.setMembershipTransactions(membershipMgrLocal, transGroup, payableItem);

			paymentTxMgrLocal.commit();
			notificationMgrLocal.commit();

		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), "notifyPendingFeePaid(IVRPayment) : Error processing transaction. " + e);

			try {
				if (paymentTxMgrLocal != null) {
					paymentTxMgrLocal.rollback();
				}
			} catch (Exception pe) {
				LogUtil.fatal(this.getClass(), "notifyPendingFeePaid(IVRPayment) : Failed to roll back payment information. " + pe);
			}

			try {
				if (notificationMgrLocal != null) {
					notificationMgrLocal.rollback();
				}
			} catch (Exception se) {
				LogUtil.fatal(this.getClass(), "notifyPendingFeePaid(IVRPayment) : Failed to roll back published events." + se);
			}

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	 * Update the membership group that the member is to belong to. If the member is not to be joined to a group then delete any existing group records. If there will only be one membership left in the group then delete it's membership group record as well. If the member already belongs to a group but it is a different group than the new one then delete and recreate the membership group record. If the member already belongs to the new group then update the prime addressee flag.
	 */
	private void updateMembershipGroup(MembershipTransactionVO memTransVO, Integer membershipGroupID) throws RemoteException {
		MembershipGroupDetailVO memGroupDetail = null;

		// fails for reconstituted memberships
		Integer membershipID = memTransVO.getMembershipID();
		LogUtil.debug(this.getClass(), "updateMembershipGroup membershipID=" + membershipID + ", membershipGroupID=" + membershipGroupID + ", memTransVO=" + memTransVO.getTransactionID());
		LogUtil.debug(this.getClass(), "updateMembershipGroup 1 memTransVO=" + memTransVO);
		if (membershipID != null) {
			String transTypeCode = memTransVO.getTransactionTypeCode();
			LogUtil.debug(this.getClass(), "updateMembershipGroup 2 " + transTypeCode);
			if (membershipGroupID == null) {
				LogUtil.debug(this.getClass(), "updateMembershipGroup 3 " + membershipID);
				// group is to be disbanded
				MembershipHelper.disbandMembersGroup(membershipID);
			} else {
				LogUtil.debug(this.getClass(), "updateMembershipGroup 4 " + membershipID + "," + membershipGroupID);
				memGroupDetail = MembershipHelper.removeFromAllGroups(membershipID);

				LogUtil.debug(this.getClass(), "updateMembershipGroup 5 " + memGroupDetail);
				// Add the membership to the new group if a new group has been
				// specified
				// and the membership is not being cancelled
				// and the membership is not being put on hold
				// and the membership is not being removed from a membership
				// group
				if (!(MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(transTypeCode))) {
					LogUtil.debug(this.getClass(), "updateMembershipGroup 6 " + memGroupDetail);

					// if the memGroupDetail we've removed doesn't match the
					// provided membershipGroupID
					// then don't reuse the groupID
					if (memGroupDetail != null && memGroupDetail.getMembershipGroupDetailPK().getGroupID() != membershipGroupID) {
						LogUtil.debug(getClass(), "updateMembershipGroup 6a - forcing creation of new memGroupDetail");
						memGroupDetail = null;
					}

					if (memGroupDetail == null) {
						// create a new record
						LogUtil.debug(this.getClass(), "updateMembershipGroup 7");
						// Adding the member to the group
						memGroupDetail = new MembershipGroupDetailVO(new MembershipGroupDetailPK(membershipID, membershipGroupID), memTransVO.getMembershipGroupJoinDate(), memTransVO.isPrimeAddressee());
					} else {
						LogUtil.debug(this.getClass(), "updateMembershipGroup 8");
						// set only changed details, primary key will not change
						// - ie. membershipid/membershipgroupid
						memGroupDetail.setPrimeAddressee(memTransVO.isPrimeAddressee());
						memGroupDetail.setGroupJoinDate(memTransVO.getMembershipGroupJoinDate());
					}
					LogUtil.debug(this.getClass(), "updateMembershipGroup 9 " + memGroupDetail);
					membershipMgrLocal.createMembershipGroupDetail(memGroupDetail);
				} else {
					LogUtil.log(this.getClass(), "updateMembershipGroup don't create - cancel, hold or remove from group");
				}
			}
		}
	}

	/**
	 * If the membership eligible to receive the Motor News publication then set the Motor News option type to normal on the client that owns the membership. This will cause one person in the household where the client lives to receive the publication. If the membership is not eligible to receive the Motor News publication then set the Motor News option type to "no send" on the client. This will cause the client to not receive the publication.
	 * 
	 * @param memVO The membershipVO object for which the motor news mailing data is to be updated
	 * @exception RemoteException Description of the Exception
	 */
	private void updateMotorNewsMailingList(MembershipVO memVO) throws RemoteException {
		ClientVO client = memVO.getClient();
		client.setMotorNewsSendOption(client.getMotorNewsSendOption());
		client.setMotorNewsDeliveryMethod(Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL);
		clientMgrLocal.updateClient(client);
	}

	/**
	 * Remove unissued card request for a member
	 */
	private void removeMembershipCardRequests(MembershipVO memVO) throws RemoteException {
		LogUtil.debug(this.getClass(), "removeMembershipCardRequests " + memVO);
		MembershipCardVO memCard = null;
		Collection unissuedCardReqs = membershipMgrLocal.findMembershipCardRequests(memVO.getMembershipID());
		LogUtil.debug(this.getClass(), "removeMembershipCardRequests 1" + unissuedCardReqs);
		if (unissuedCardReqs != null && !unissuedCardReqs.isEmpty()) {
			LogUtil.debug(this.getClass(), "removeMembershipCardRequests 1a");
			Iterator it = unissuedCardReqs.iterator();
			while (it.hasNext()) {
				memCard = (MembershipCardVO) it.next();
				LogUtil.debug(this.getClass(), "removeMembershipCardRequests 1b" + memCard);
				membershipMgrLocal.removeMembershipCard(memCard);
			}
		}

	}

	/**
	 * Send the amount payable to the payment system
	 */
	private PayableItemVO sendAmountPayable(TransactionGroup transGroup, PaymentTransactionMgr paymentTxMgr) throws RollBackException, PaymentException, RemoteException {
		// Decide which transactions need to be sent to the payment system.
		String transGroupTypeCode = transGroup.getTransactionGroupTypeCode();
		PayableItemVO payableItem = null;

		/**
		 * @todo Trace all sources of income for membership credits. The current process is: If a membership was cancelled that had discounts or subsidies providing alternative sources of income (e.g. discretionary discount, safety drive, etc.) and a credit is attached to the membership and the membership is subsequently restored using the credit as part payment, the original income source for the discount/subsidy is not traced. The impact is that the funding of the discount comes from the membership account and not the correct account.
		 */

		if (transGroup instanceof PayableItemGenerator) {
			PayableItemGenerator payableItemGenerator = (PayableItemGenerator) transGroup;
			ArrayList payableItemList = payableItemGenerator.generatePayableItemList();

			/**
			 * @todo I think this should be done in the PayableItemGenerator as it knows if it applies to the transaction type.
			 * 
			 *       The payableItemList should contain one new payable item which can be returned by the sendAmountPayable() method.
			 * 
			 *       All the payable items in the list need to be sent to the payment transaction manager to update existing payable items.
			 */
			Integer newPayableItemID = null;
			PayableItemVO tempPayableItem = null;
			// check if there is a populated list
			if (payableItemList != null && payableItemList.size() > 0) {
				Iterator payableItemListIterator = payableItemList.iterator();
				Collection componentList = null;

				// there can only be one new PI so stop looking once it is found
				while (newPayableItemID == null && payableItemListIterator.hasNext()) {
					tempPayableItem = (PayableItemVO) payableItemListIterator.next();
					if (tempPayableItem.getPayableItemID() == null) {
						// store and return the new payable item
						payableItem = paymentTxMgr.addPayableItem(tempPayableItem);
						LogUtil.log(this.getClass(), "after add payable item");
					}
				}
			}
			// remove payment method list
			removePaymentMethodList(transGroup, paymentTxMgr);
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transGroupTypeCode)) {
			// Only send to the payment system if there is an amount payable.
			double grossTransactionFee = transGroup.getGrossTransactionFee();
			if (NumberUtil.roundDouble(grossTransactionFee, 2) > 0) {
				payableItem = processAmountPayable(transGroup, paymentTxMgr);
			}
		} else {
			PaymentEJBHelper.getPaymentMgr().getPaymentTypes();
			payableItem = processAmountPayable(transGroup, paymentTxMgr);
		}

		return payableItem;
	} // sendAmountPayable

	private void removePaymentMethodList(TransactionGroup transGroup, PaymentTransactionMgr paymentTxMgr) throws RollBackException, RemoteException {
		// remove any schedules/oi-payables if necessary
		Collection payMethodList = transGroup.getPaymentMethodListToRemove();
		if (payMethodList != null && payMethodList.size() > 0) {
			PaymentMethod payMethod = null;
			Iterator it = payMethodList.iterator();
			while (it.hasNext()) {
				payMethod = (PaymentMethod) it.next();
				paymentTxMgr.cancelPaymentMethod(payMethod, SourceSystem.MEMBERSHIP);
			}
		}
	}

	/**
	 * Process the amount payable from the transaction group.
	 */
	private PayableItemVO processAmountPayable(TransactionGroup transGroup, PaymentTransactionMgr paymentTxMgr) throws RollBackException, PaymentException, RemoteException {
		double netTxFee = transGroup.getNetTransactionFee();

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

		PayableItemVO newPayableItem = null;
		PayableItemVO payableItem = null;

		MembershipTransactionVO primeMemTransVO = transGroup.getMembershipTransactionForPA();
		Integer transactionReference = primeMemTransVO.getTransactionID();

		// and the correct branch for the prime addressee
		MembershipVO primeMemVO = primeMemTransVO.getNewMembership();

		// get the paymentmethod to replace if the transaction is replacing - dd
		// supported only
		String ddScheduleIdToReplace = null;
		if (transGroup.replacesTransaction()) {
			LogUtil.log(this.getClass(), "replacesTransaction");
			ddScheduleIdToReplace = transGroup.getDirectDebitScheduleIdToReplace();
		}

		Integer payableItemID = null; // No payable item id yet. This gets
		// returned
		Collection memTxList = transGroup.getTransactionList();
		Vector componentList = new Vector();
		MembershipTransactionVO memTxVO = null;
		String description = "";
		Iterator memTxListIterator = memTxList.iterator();
		PayableItemComponentVO compCreditComponent;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		PostingContainer postingList = null;
		// for each transaction in the transaction group
		while (memTxListIterator.hasNext()) {
			memTxVO = (MembershipTransactionVO) memTxListIterator.next();
			// for some transactions, e.g. Cancel, only the membership affected
			// has a txID
			if (transactionReference == null) {
				transactionReference = memTxVO.getTransactionID();
			}

			String txType = memTxVO.getTransactionTypeCode();

			LogUtil.log(this.getClass(), "tx in processAP  " + txType);

			oldMemVO = memTxVO.getMembership();
			newMemVO = memTxVO.getNewMembership();

			boolean createPostingsForChangeGroup = false;

			// postings not required for a group group PA transaction
			boolean postingsRequired = !MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(txType);

			boolean hasTransactionFees = !(memTxVO.getTransactionFeeList() == null);

			boolean removeFromGroup = MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(txType);

			// boolean transfer =
			// MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(txType);
			LogUtil.log(this.getClass(), "hasTransactionFees=" + hasTransactionFees);
			// include valid group transactions
			if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(txType)) {
				if (oldMemVO != null && newMemVO != null && oldMemVO.getCreditAmount() != newMemVO.getCreditAmount()) {
					LogUtil.log(this.getClass(), "create postings for change group  " + txType);
					createPostingsForChangeGroup = true;
				}
			}
			if (createPostingsForChangeGroup || postingsRequired) {
				// double check
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(txType) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(txType)) {
					throw new SystemException("Should not be calling create postings for Cancel or Hold.");
				}

				// create the postings for the transaction
				if (hasTransactionFees || createPostingsForChangeGroup || removeFromGroup) {
					LogUtil.log(this.getClass(), "create postings list  " + txType);
					postingList = new MembershipPostingContainer(memTxVO, transactionReference, transGroup);

				}
			}

			if (postingList != null && postingList.size() > 0) {
				// get the fee for the transaction
				BigDecimal amountPayable = new BigDecimal(memTxVO.getNetTransactionFee());
				// if the NetTransactionFee is zero there must have been an
				// adjustment or there wouldn't have been any postings.
				BigDecimal adjustmentTotal = memTxVO.getNonDiscretionaryAdjustmentTotal();
				if (new BigDecimal(0).compareTo(amountPayable) == 0) {
					amountPayable = adjustmentTotal;
				} else if (new BigDecimal(0).compareTo(adjustmentTotal) != 0) {
					amountPayable = amountPayable.add(adjustmentTotal);
				}
				description = txType + " membership  Ref : " + transactionReference;
				Integer componentID = null;
				// This is set by the payment system.
				// Create a component for each Membership Transaction.
				PayableItemComponentVO component = new PayableItemComponentVO(postingList, amountPayable.setScale(2, BigDecimal.ROUND_HALF_UP), description, componentID, memTxVO.getNewMembership().getMembershipNumber(), null); // supersedingComponentID

				// and add it to the list to attach to the payable item
				componentList.add(component);

			}

			// complimentary credit
			if (memTxVO.hasComplimentaryCredit()) {
				// for each transaction
				compCreditComponent = paymentMgr.createPostingsForDiscretionaryDiscount(transactionReference, transGroup, newMemVO.getMembershipNumber(), PaymentMgr.DISCRETIONARY_DISCOUNT_COMP_CREDIT);
				if (compCreditComponent != null) // and it should never be
				{
					componentList.add(compCreditComponent);
				} else {
					throw new SystemException("Unable to create postings for complimentary credit.");
				}
			}
		}

		// discretionary discount
		if (transGroup.hasAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT)) {
			try {
				PayableItemComponentVO discountComponent = paymentMgr.createPostingsForDiscretionaryDiscount(transactionReference, transGroup, primeMemVO.getMembershipNumber(), PaymentMgr.DISCRETIONARY_DISCOUNT_ADJUSTMENT);
				if (discountComponent != null) // and it should never be
				{
					componentList.add(discountComponent);
				}
			} catch (Exception ex) {
				throw new PaymentException("Error creating postings for discretionary discount" + ex);
			}
		}

		/**
		 * If no components were created throw an exception as the transaction group has a positive amount payable or discretionary discount
		 */
		if (componentList.size() > 0) {

			// The product period that the payable item is for starts on the
			// preferred commence date unless overridden below.
			DateTime startDate = transGroup.getPeriodStartDate();
			DateTime endDate = transGroup.getPeriodEndDate();

			PaymentMethod paymentMethod = null;
			// Use a receipting payment method if direct debit has not been
			// chosen.
			if (transGroup.getDirectDebitAuthority() == null) {
				paymentMethod = new ReceiptingPaymentMethod(primeMemVO.getClientNumber(), SourceSystem.MEMBERSHIP);
			} else {
				// there is an authority

				// is there a renewal version of the schedule attached?
				paymentMethod = transGroup.getDirectDebitSchedule();
				// if not create a specification for a new schedule
				if (paymentMethod == null) {
					paymentMethod = transGroup.getDirectDebitScheduleSpecification();
				}
			}

			Integer clientBranchNumber = primeMemVO.getClient().getBranchNumber();
			description = MembershipUIHelper.getTransactionHeadingText(transGroup.getTransactionGroupTypeCode());
			payableItem = new PayableItemVO(payableItemID, primeMemTransVO.getNewMembership().getClientNumber(), paymentMethod, componentList, SourceSystem.MEMBERSHIP, transactionReference, primeMemTransVO.getSalesBranchCode(), // sourceSystemReferenceID
			clientBranchNumber, primeMemTransVO.getNewMembership().getProductCode(), primeMemTransVO.getUsername(), transGroup.getPeriodStartDate(), transGroup.getPeriodEndDate(), description);

			// Determine if we are replacing this payable item in the
			// payment system by setting the payment method id on the payable
			// item.

			// The direct debit schedule id to replace
			if (ddScheduleIdToReplace != null) {
				// sanity checking
				if (payableItem.getPaymentMethodID() != null && payableItem.getPaymentMethod() != null && payableItem.getPaymentMethod().isDirectDebitPaymentMethod() && ((DirectDebitSchedule) payableItem.getPaymentMethod()).isRenewalVersion() && !payableItem.getPaymentMethodID().equals(ddScheduleIdToReplace)) {
					throw new RemoteException("The direct debit schedule to replace " + "is not the same as the one specified and the schedule is a renewal version.");
				}
				// Setting the payment method will also set the payment method
				// id!!
				payableItem.setPaymentMethodID(ddScheduleIdToReplace);
			}
			// If a repair is being done then link the payable item to
			// an existing record in the receipting system.
			if (transGroup.getRepairPaymentMethodId() != null) {
				LogUtil.log(this.getClass(), "Repair " + primeMemTransVO.getNewMembership().getClientNumber());
				payableItem.setRepairPaymentMethodID(transGroup.getRepairPaymentMethodId().toString());
			}
			PaymentEJBHelper.getPaymentMgr().getPaymentTypes();
			LogUtil.log(this.getClass(), "YYYYYY");

			// DD repair
			String repairDDPaymentMethodID = transGroup.getRepairDDPaymentMethodId();
			if (repairDDPaymentMethodID == null) {
				newPayableItem = paymentTxMgr.addPayableItem(payableItem);
			} else {
				// DD payment method already exists
				payableItem.setPaymentMethodID(transGroup.getRepairDDPaymentMethodId());
				newPayableItem = paymentTxMgr.addPayableItem(payableItem, false, transGroup.getTransactionDate(), ProgressDDAdapter.SYSTEM_PROGRESS_DIRECT_DEBIT);
			}

			if (newPayableItem == null) {
				throw new SystemException("Failed to get updated payable item returned from payment manager when adding a new payable item.");
			}
		} else if (componentList.size() == 0 && netTxFee != 0) {
			throw new SystemException("No payable item components created and there is an amount payable." + CurrencyUtil.formatDollarValue(netTxFee));
		}

		// return the payable item so we can update dependant objects
		return newPayableItem;
	} // processAmountPayable

	/**
	 * Gets the membershipTransactionFeeList attribute of the MembershipTransactionMgrBean object
	 * 
	 * @param transID Description of the Parameter
	 * @return The membershipTransactionFeeList value
	 * @exception RemoteException Description of the Exception
	 */
	public Vector getMembershipTransactionAdjustmentList(Integer transID) throws RemoteException {
		Vector memTransAdjustmentList = new Vector();
		Connection connection = null;
		PreparedStatement statement = null;
		String statementText = null;
		if (transID != null) {
			try {
				connection = clientDataSource.getConnection();
				statementText = "SELECT adjustment_type_code, adjustment_amount, adjustment_reason FROM PUB.mem_transaction_adjustment WHERE transaction_id = ?";
				statement = connection.prepareStatement(statementText);
				statement.setInt(1, transID.intValue());
				ResultSet rs = statement.executeQuery();
				String adjTypeCode = null;
				BigDecimal adjAmount = null;
				String adjReason = null;
				MembershipTransactionAdjustment memTransAdj = null;
				double adjAmountDouble = 0;
				while (rs.next()) {
					adjTypeCode = rs.getString(1);
					adjAmountDouble = rs.getDouble(2);
					adjAmount = new BigDecimal(adjAmountDouble).setScale(2, BigDecimal.ROUND_HALF_UP);
					// adjAmount =
					// rs.getBigDecimal(2).setScale(2,BigDecimal.ROUND_HALF_UP);

					adjReason = rs.getString(3);
					memTransAdj = new MembershipTransactionAdjustment(adjTypeCode, adjAmount, adjReason);
					memTransAdjustmentList.add(memTransAdj);
				}
			} catch (SQLException e) {
				throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
			} finally {
				ConnectionUtil.closeConnection(connection, statement);
			}
		}
		return memTransAdjustmentList;
	}

	/**
	 * Gets the membershipTransactionFeeList attribute of the MembershipTransactionMgrBean object
	 * 
	 * @param transID Description of the Parameter
	 * @return The membershipTransactionFeeList value
	 * @exception RemoteException Description of the Exception
	 */
	public Vector getMembershipTransactionFeeList(Integer transID) throws RemoteException {
		LogUtil.debug(this.getClass(), "transID=" + transID);
		Vector memTransFeeList = new Vector();
		try {
			Connection connection = null;
			PreparedStatement statement = null;
			String statementText = null;
			if (transID != null) {
				try {
					connection = clientDataSource.getConnection();
					statementText = "SELECT fee_specification_id, fee_per_member, fee_per_vehicle, vehicle_count FROM PUB.mem_transaction_fee WHERE transaction_id = ?";
					statement = connection.prepareStatement(statementText);
					statement.setInt(1, transID.intValue());
					ResultSet rs = statement.executeQuery();
					while (rs.next()) {
						Integer feeSpecID = new Integer(rs.getInt(1));
						LogUtil.debug(this.getClass(), "feeSpecID=" + feeSpecID);
						double feePerMember = rs.getDouble(2);
						double feePerVehicle = rs.getDouble(3);
						int vehicleCount = rs.getInt(4);
						MembershipTransactionFee mtf = new MembershipTransactionFee(feeSpecID, feePerMember, feePerVehicle, vehicleCount);
						LogUtil.debug(this.getClass(), " mtf=" + mtf);
						mtf.setTransactionID(transID);
						memTransFeeList.add(mtf);
					}
				} catch (SQLException e) {
					throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
				} finally {
					ConnectionUtil.closeConnection(connection, statement);
				}
			}
		} catch (Exception e) {
			throw new RemoteException("Unable to get membership transaction fee list. " + e);
		}
		return memTransFeeList;
	}

	/**
	 * Description of the Method
	 */
	public void ejbCreate() {
	}

	/**
	 * Description of the Method
	 * 
	 * @exception RemoteException Description of the Exception
	 */
	public void ejbRemove() throws RemoteException {
	}

	/**
	 * Description of the Method
	 * 
	 * @exception RemoteException Description of the Exception
	 */
	public void ejbActivate() throws RemoteException {
		this.clientDataSource = null;
	}

	/**
	 * When the bean is passivated we need to release the references to other entity beans so that they are re-initialised when the bean is activated.
	 * 
	 * @exception RemoteException Description of the Exception
	 */
	public void ejbPassivate() throws RemoteException {
	}

	/**
	 * Sets the sessionContext attribute of the MembershipTransactionMgrBean object
	 * 
	 * @param sessionContext The new sessionContext value
	 * @exception RemoteException Description of the Exception
	 */
	public void setSessionContext(SessionContext sessionContext) throws RemoteException {
		this.sessionContext = sessionContext;
		try {
			this.clientDataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
		} catch (NamingException e) {
			LogUtil.fatal(this.getClass(), e);
		}
	}

	/**
	 * Search the database for the most recent transaction associated with the given membership Id and find the value. If the member is a group member but not the prime addressee, return 0.
	 * 
	 * @param memVo Description of the Parameter
	 * @return double
	 * @exception Exception Description of the Exception
	 * @exception RemoteException Description of the Exception
	 */
	public double getLastTransactionValue(MembershipVO memVo) throws Exception, RemoteException {
		StringBuffer sqlTrans = new StringBuffer();
		StringBuffer sqlGrp = new StringBuffer();
		StringBuffer sqlFee = new StringBuffer();
		double fee = 0;
		String transactionList = "";
		if (memVo.isGroupMember()) {
			if (!memVo.isPrimeAddressee()) {
				return 0;
			} else {
				sqlTrans.append("select transaction_id, transaction_group_id");
				sqlTrans.append(" from (PUB.mem_membership_group GRP1 inner join PUB.mem_membership_group GRP2");
				sqlTrans.append(" on GRP1.group_id = GRP2.group_id)");
				sqlTrans.append(" inner join PUB.mem_transaction TRANS on GRP2.membership_id = TRANS.membership_id");
				sqlTrans.append(" where GRP1.membership_id = ?");
				sqlTrans.append(" order by transaction_id desc");
			}
		} else {
			sqlTrans.append("select transaction_id,transaction_group_id ");
			sqlTrans.append(" from PUB.mem_transaction ");
			sqlTrans.append(" where membership_id = ?");
			sqlTrans.append(" order by transaction_id desc");
		}
		sqlGrp.append("select transaction_id");
		sqlGrp.append(" from PUB.mem_transaction ");
		sqlGrp.append(" where transaction_group_id = ?");

		int transactionId;

		int transactionGroupId;
		Connection connection = null;
		PreparedStatement statement = null;
		PreparedStatement grpStatement = null;
		PreparedStatement feeStatement = null;
		PreparedStatement discountStatement = null;
		try {

			connection = this.clientDataSource.getConnection();
			statement = connection.prepareStatement(sqlTrans.toString());
			statement.setInt(1, memVo.getMembershipID().intValue());
			ResultSet rsTrans = statement.executeQuery();
			// only want the most recent - the one with the highest
			// transaction_id
			if (rsTrans.next()) {
				transactionId = rsTrans.getInt(1);
				transactionGroupId = rsTrans.getInt(2);
				// if there is a transaction group, get all the transactions in
				// the group
				if (transactionGroupId > 0) {
					grpStatement = connection.prepareStatement(sqlGrp.toString());
					grpStatement.setInt(1, transactionGroupId);
					ResultSet rsGrp = grpStatement.executeQuery();
					while (rsGrp.next()) {
						if (!transactionList.equals("")) {
							transactionList += ",";
						}
						transactionList += rsGrp.getInt(1);
					}
					rsGrp.close();
				}
				// if not a group, just use this one
				else {
					transactionList += rsTrans.getInt(1);
				}
			}
			// now, if there are any transactions, look them up and total the
			// amounts
			if (!transactionList.equals("")) {
				sqlFee.append("select fee_per_member");
				sqlFee.append(" from PUB.mem_transaction_fee ");
				sqlFee.append(" where transaction_id in (" + transactionList + ")");
				feeStatement = connection.prepareStatement(sqlFee.toString());
				ResultSet rsFee = feeStatement.executeQuery();
				while (rsFee.next()) {
					fee += rsFee.getDouble(1);
				}
				rsFee.close();
				String discountSql = "Select discount_amount from PUB.mem_transaction_fee_discount";
				discountSql += " where transaction_id in (" + transactionList + ")";
				discountStatement = connection.prepareStatement(discountSql);
				ResultSet rsDiscount = discountStatement.executeQuery();
				while (rsDiscount.next()) {
					fee -= rsDiscount.getDouble(1);
				}
				rsDiscount.close();
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw e;
		} finally {
			ConnectionUtil.closeStatement(feeStatement);
			ConnectionUtil.closeStatement(grpStatement);
			ConnectionUtil.closeStatement(statement);
			ConnectionUtil.closeStatement(discountStatement);
			ConnectionUtil.closeConnection(connection);
		}
		return fee;
	}

	/**
	 * Remove a membership transaction. Not to be taken lightly.
	 * 
	 * @param Integer transID
	 * @param UserSession userSession
	 * @throws RemoteException
	 * @throws SecurityException
	 * @throws RemoveException
	 * @throws ValidationException
	 */
	public void removeMembershipTransaction(Integer transID, UserSession userSession) throws RemoteException, com.ract.common.SecurityException, RemoveException, ValidationException {
		// make sure that the user has the privilege to remove membership
		// transactions.
		if (userSession == null || !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_REMOVE_TRANSACTION)) {
			throw new com.ract.common.SecurityException("You do not have the privilege to remove membership transactions.");
		}

		MembershipTransactionVO memTransVO = membershipMgrLocal.getMembershipTransaction(transID);

		// direct debit

		// Make sure that no payments have been made on the payable item.
		Integer payableItemID = memTransVO.getPayableItemID();
		if (payableItemID != null) {
			PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
			PayableItemVO payableItemVO = null;
			try {
				payableItemVO = paymentMgr.getPayableItem(payableItemID);
			} catch (PaymentException ex) {
				throw new RemoteException("Error getting payable.", ex);
			}

			if (payableItemVO != null) {
				double amountPayable = payableItemVO.getAmountPayable();
				double amountOutStanding = payableItemVO.getAmountOutstanding();

				PaymentMethod pMethod = payableItemVO.getPaymentMethod();

				if (amountPayable > 0.0 && amountOutStanding < amountPayable && pMethod instanceof ReceiptingPaymentMethod) {
					throw new ValidationException("The payable item associated with the transaction (" + payableItemID + ") has been paid by receipting. Amount payable = " + amountPayable + ", amount outstanding = " + amountOutStanding + ".");
				}
			}
		}

		membershipMgrLocal.removeMembershipTransaction(memTransVO);

	}

	/**
	 * Get a list of pending fee remote interface to use to remove.
	 */
	private boolean cancelMembershipDocument(MembershipVO memVO, PaymentTransactionMgrLocal paymentTransactionMgrLocal, boolean ignoreErrors) throws RemoteException {
		boolean docCancelled = false;
		boolean tCanc = false;
		try {
			List<MembershipDocument> membershipDocumentList = membershipMgrLocal.getCurrentDocuments(memVO.getMembershipID(), memVO.getExpiryDate());
			// handle multiple current documents by removing them
			if (membershipDocumentList != null) {
				for (MembershipDocument membershipDocument : membershipDocumentList) {
					tCanc = membershipMgrLocal.removeRenewalDocument(membershipDocument.getDocumentId(), MembershipMgr.MODE_CANCEL, paymentTransactionMgrLocal, ignoreErrors);
					if (tCanc) {
						docCancelled = true;
					}
				}
			}
		} catch (Exception ex) {
			// ignore errors but log them
			LogUtil.warn(this.getClass(), ex);
		}
		return docCancelled;
	}

	private void cancelMembershipDocument(MembershipVO memVO, PaymentTransactionMgrLocal paymentTransactionMgrLocal) throws RemoteException {
		cancelMembershipDocument(memVO, paymentTransactionMgrLocal, false);
	}

	/**
	 * Generate history snapshot for a vo and save it to the most recent transaction
	 * 
	 * @param memVO the membership to generate history for.
	 */
	private void createMembershipXMLForLastTransaction(MembershipVO memVO) throws RemoteException, SystemException {
		ArrayList txList = new ArrayList(memVO.getTransactionList());

		if (txList.size() == 0) {
			LogUtil.warn(this.getClass(), "Failed to get list of transactions for membership " + memVO.getMembershipID());
			return;
		}

		// sort in reverse date order
		MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE, true);
		Collections.sort(txList, mtc);

		Iterator txListIterator = txList.iterator();
		// get first transaction which is the actually the last transacted.
		MembershipTransactionVO memTxVO = (MembershipTransactionVO) txListIterator.next();
		// if no history file is available
		if (memTxVO != null && memTxVO.getMembershipXML() == null) {
			// append the transaction id and XML extension
			String memXML = memVO.toXML();

			memTxVO.setMembershipXML(memXML);
			membershipMgrLocal.updateMembershipTransaction(memTxVO);
			// set readonly again.
		}
	}

	/**
	 * transaction processing for automatic renewals Copied from processTransaction. Some unnecessary and unhelpful bits removed
	 * 
	 * @param TransactionGroup transGroup
	 * @return Collection ArrayList of MembershipTransactionVO objects
	 * @throws RemoteException
	 * @throws ValidationException
	 */
	public Collection processAutoRenewalTransaction(TransactionGroup transGroup) throws RemoteException, ValidationException {
		TransactionHelper transactionHelper = new TransactionHelper();

		if (transGroup == null) {
			throw new SystemException("Unable to process transaction. Transaction object is null.");
		}
		String transGroupTypeCode = transGroup.getTransactionGroupTypeCode();

		// logging
		Integer memTransCount = null;
		Integer paClientNumber = null;
		String userID = "";

		try {
			memTransCount = new Integer(transGroup.getMembershipTransactionCount());
			paClientNumber = transGroup.getClientForPA().getClientNumber();
			userID = transGroup.getUserID();
		} catch (Exception exp) {
			// ignore
		}
		Vector transactionList = transGroup.getTransactionList();
		if (transactionList == null || transactionList.isEmpty()) {
			throw new SystemException("Transaction list is empty. Cannot process " + transGroup.getTransactionGroupTypeCode() + " transaction.");
		}

		// ArrayList pendingFeeList = new ArrayList();
		// The returned list of memberships that were involved in the
		// transaction.
		ArrayList completedTransactionList = new ArrayList();
		PaymentTransactionMgr paymentTxMgr = null;
		MembershipChangeNotificationEvent notificationEvent = null;
		NotificationMgr notificationMgr = null;

		try {
			// create membership snapshot XML documents for old membership
			// that don't have one so that we can undo this transaction.
			// This can be done in a stand alone transaction
			createSnapshotForOldMemberships(transGroup);

			// Start the main transaction block
			paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			notificationMgr = CommonEJBHelper.getNotificationMgr();

			// Set the transaction date to the exact time the transaction is
			// saved.
			if (transGroup.getOverrideTransactionDate() == null) {
				transGroup.setTransactionDate(new DateTime());
			} else { // force txn date
				transGroup.setTransactionDate(transGroup.getOverrideTransactionDate());
			}

			// Set the transaction group ID if more than one membership
			// transaction is being processed.
			transGroup.setTransactionGroupID();

			// Set the ID of the membership transactions before sending the fees
			// to the payment system
			transGroup.setMembershipTransactionIDs();

			// Get the membership group ID. Will be null if a membership group
			// is not being created or updated.
			Integer membershipGroupID = transactionHelper.getMembershipGroupID(transGroup);

			MembershipTransactionVO memTransVO;
			MembershipVO oldMemVO;
			MembershipVO newMemVO;
			Integer clientNumber;
			String transTypeCode;
			String cardReasonCode = null;
			boolean primeAddressee = false;
			PendingFee pendingFee = null;
			ListIterator transListIT = transactionList.listIterator();
			int groupCount = transGroup.getActualGroupMemberCount();
			if (groupCount == 0) {
				groupCount = 1;
			}
			while (transListIT.hasNext()) {

				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.setTransactionGroupTypeCode(transGroup.getTransactionGroupTypeCode());
				transTypeCode = memTransVO.getTransactionTypeCode();
				oldMemVO = memTransVO.getMembership();
				newMemVO = memTransVO.getNewMembership();
				clientNumber = newMemVO.getClientNumber();

				// set other transaction attributes
				memTransVO.setProductCode(newMemVO.getProductCode());
				memTransVO.setGroupCount(new Integer(groupCount));
				notificationEvent = null;
				newMemVO = membershipMgrLocal.updateMembership(newMemVO);
				notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE, newMemVO);
				notificationMgr.notifyEvent(notificationEvent);
				// Set the membership ID on the membership transaction now that
				// we have it.
				memTransVO.setNewMembership(newMemVO);
				// Save the membership transaction.
				// A new membership transaction value object is returned with
				// all
				// references to other objects set to null.
				memTransVO = transactionHelper.createMembershipTransaction(membershipMgrLocal, memTransVO);
				// Add the membership to the returned list of memberships that
				// were
				// affected by this transaction.
				completedTransactionList.add(memTransVO);
			}

			// Send the amount payable to the payment system.
			// check that this transaction should have postings
			PayableItemVO payableItem = null;
			if (transGroup.getAmountPayable() > 0.0 || transGroup.hasOffsetDiscountsApplied() || transGroup.hasCreditDiscountsApplied() || transGroup.hasUnearnedDiscount()) {
				payableItem = sendAmountPayable(transGroup, paymentTxMgr);
			}

			// update the payable item details
			transactionHelper.setMembershipTransactions(membershipMgrLocal, transGroup, payableItem);

			// update the saved transactions and set the membershipXML attribute
			Iterator completedListIterator = completedTransactionList.iterator();
			while (completedListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) completedListIterator.next();
				transactionHelper.updateMembershipTransactionMembershipXML(membershipMgrLocal, memTransVO);
			}

			paymentTxMgr.commit();
			notificationMgr.commit();
		} catch (Exception e) {
			e.printStackTrace();

			LogUtil.fatal(this.getClass(), "Error processing transaction. " + e);
			LogUtil.fatal(this.getClass(), "Transaction Rolled Back in processTransaction");
			LogUtil.fatal(this.getClass(), "UserID " + userID + ", Prime Addressee client number = " + paClientNumber + ", membership transaction count = " + memTransCount);

			// Roll back the payment transactions
			try {
				if (paymentTxMgr != null) {
					paymentTxMgr.rollback();
				}
			} catch (Exception pe) {
				LogUtil.fatal(this.getClass(), "Failed to roll back payment information. " + pe);
			}

			// Rollback the published events
			try {
				if (notificationMgr != null) {
					notificationMgr.rollback();
				}
			} catch (Exception pe) {
				LogUtil.fatal(this.getClass(), "Failed to roll back events published to notification manager. " + pe);
			}

			// The transaction has been rolled back.
			// Throw the exception back up so the UI can handle it.
			if (e instanceof ValidationException) {
				throw (ValidationException) e;
			} else if (e instanceof SystemException) {
				throw (SystemException) e;
			} else if (e instanceof RemoteException) {
				throw (RemoteException) e;
			} else {
				throw new SystemException(e);
			}
		}
		return completedTransactionList;
	} // processTransactionForAutoRenewal

	/**
	 * Process refunds and write offs.
	 * 
	 * @param transGroup
	 * @param creditDisposal
	 * @throws RemoteException
	 */
	public Collection createCreditDisposal(TransactionGroup transGroup, FinanceVoucher creditDisposal) throws RemoteException {
		FinanceMgrLocal financeMgr = PaymentEJBHelper.getFinanceMgrLocal();
		SequenceMgr sequenceMgr = CommonEJBHelper.getSequenceMgr();
		MailMgr mailMgr = CommonEJBHelper.getMailMgr();
		UserMgr userMgr = UserEJBHelper.getUserMgr();

		LogUtil.debug(this.getClass(), "processCreditDisposal 1 start");
		TransactionHelper transactionHelper = new TransactionHelper();
		try {
			createSnapshotForOldMemberships(transGroup);

			// disposal id
			Integer disposalId = sequenceMgr.getNextID(SequenceMgr.SEQUENCE_PAY_CREDIT_DISPOSAL);
			creditDisposal.setDisposalId(disposalId);

			MembershipTransactionVO memTransVO = (MembershipTransactionVO) transGroup.getTransactionList().get(0);
			transGroup.setTransactionDate(new DateTime());
			transGroup.setMembershipTransactionIDs();

			MembershipVO newMemVO = memTransVO.getNewMembership();

			memTransVO.setDisposalId(creditDisposal.getDisposalId());

			memTransVO.setNewMembership(newMemVO);
			memTransVO = transactionHelper.createMembershipTransaction(membershipMgrLocal, memTransVO);

			transactionHelper.updateMembershipTransactionMembershipXML(membershipMgrLocal, memTransVO);

			creditDisposal.setTransactionReference(memTransVO.getTransactionID().toString());

			if (creditDisposal.isFinanceSystem()) {
				DecimalFormat voucherFormat = new DecimalFormat("000000");
				Integer nextVoucher = sequenceMgr.getNextID(SequenceMgr.SEQUENCE_PAY_VOUCHER);

				// Create a voucher number in the form RR012345
				String voucherId = FinanceMgr.VOUCHER_PREFIX + voucherFormat.format(nextVoucher);
				creditDisposal.setVoucherId(voucherId);

				String payeeNo = null;
				if (creditDisposal.isThirdParty()) // adhoc
				{
					DecimalFormat adhocPayeeFormat = new DecimalFormat("9000000"); // R9001234
					Integer nextPayeeNumber = sequenceMgr.getNextID(SequenceMgr.SEQUENCE_PAY_PAYEE);
					payeeNo = FinanceMgr.PAYEE_PREFIX_ROADSIDE + adhocPayeeFormat.format(nextPayeeNumber);
				} else // client
				{
					DecimalFormat clientPayeeFormat = new DecimalFormat("0000000"); // R0001234
					payeeNo = FinanceMgr.PAYEE_PREFIX_ROADSIDE + clientPayeeFormat.format(new Integer(creditDisposal.getClientNumber()));
				}

				creditDisposal.setPayeeNo(payeeNo);

			}

			financeMgr.createVoucher(creditDisposal);

			// send an email to the approving user
			User user = userMgr.getUser(creditDisposal.getApprovingUserId());
			String subject = creditDisposal.getDisposalType() + " has been created. Ref: " + creditDisposal.getDisposalId();
			String messageBody = creditDisposal.getDisposalType() + " has been created which requires your approval. " + FileUtil.NEW_LINE + "You will need to go into the process credit disposal area to approve or reject it.";

			MailMessage message = new MailMessage();
			message.setRecipient(user.getEmailAddress());
			message.setSubject(subject);
			message.setMessage(messageBody.toString());

			mailMgr.sendMail(message);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RemoteException("Error handling transaction for refund/write-off: ", e);
		}
		LogUtil.debug(this.getClass(), "processCreditDisposal 1 end");
		return transGroup.getTransactionList();
	}

	/**
	 * After approval by branch manager
	 */
	public void processVoucher(FinanceVoucher voucher) throws RemoteException {

		PaymentTransactionMgrLocal paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgrLocal();

		MembershipTransactionVO memTransVO = membershipMgrLocal.getMembershipTransaction(new Integer(voucher.getTransactionReference()));
		String transactionTypeCode = voucher.getTransactionTypeCode();
		String userId = voucher.getUserId();

		// adjust credit
		MembershipVO oldMemVO = membershipMgrLocal.getMembership(new Integer(voucher.getSourceSystemReference()));
		ClientVO client = oldMemVO.getClient();
		double newCreditAmount = NumberUtil.roundDouble(oldMemVO.getCreditAmount() - voucher.getTotalAmount().doubleValue(), 2); // remaining
		// credit
		// amount
		LogUtil.debug(this.getClass(), "new creditAmount=" + newCreditAmount);
		MembershipVO newMemVO = oldMemVO.copy();
		newMemVO.setCreditAmount(newCreditAmount);
		newMemVO = membershipMgrLocal.updateMembership(newMemVO);

		// update tfr member with the transferred credit - fees are not added to
		// this credit
		if (voucher.getTfrMembershipNumber() != null && voucher.getTfrMembershipNumber().length() > 0) {
			LogUtil.debug(this.getClass(), "creditDisposal.getTfrMembershipNumber()=" + voucher.getTfrMembershipNumber());
			Collection<MembershipVO> memberships = membershipMgrLocal.findMembershipByClientNumber(new Integer(voucher.getTfrMembershipNumber()));
			MembershipVO tfrMembership = null;
			if (memberships != null && memberships.size() > 0) {
				tfrMembership = memberships.iterator().next();
			}
			double tfrCreditAmount = tfrMembership.getCreditAmount();
			tfrCreditAmount += voucher.getDisposalAmount().doubleValue();
			LogUtil.debug(this.getClass(), "tfrCreditAmount=" + tfrCreditAmount);
			tfrMembership.setCreditAmount(tfrCreditAmount);
			// TODO can this be undone?
			membershipMgrLocal.updateMembership(tfrMembership);

			// create an information note
			ClientNote note = new ClientNote();
			note.setClientNumber(new Integer(voucher.getTfrMembershipNumber()));
			note.setNoteType(NoteType.TYPE_INFORMATION);
			note.setCreated(new DateTime());
			note.setCreateId(voucher.getUserId());
			note.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
			note.setNoteText(CurrencyUtil.formatDollarValue(voucher.getDisposalAmount()) + " of membership credit has been transferred from " + voucher.getClientNumber() + " to " + voucher.getTfrMembershipNumber() + ". See transaction for client " + voucher.getClientNumber() + " for details.");
			clientMgrLocal.createClientNote(note);
		}

		// update transaction with payable item reference

		LogUtil.debug(this.getClass(), "processCreditDisposal start");
		SourceSystem sourceSystem = SourceSystem.MEMBERSHIP;
		PayableItemVO payableItem = null;
		PayableItemVO newPayableItem = null;
		Vector componentList = new Vector();
		String description = "";
		Integer transactionReference = new Integer(voucher.getTransactionReference());
		DateTime transactionDate = voucher.getCreateDate();
		Vector postingList = new Vector();
		PayableItemPostingVO posting = null;
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		MembershipAccountVO memSuspenseAccount = refMgr.getMembershipSuspenseAccount();
		MembershipAccountVO sendToAccount = null;
		Company company = Company.RACT;

		LogUtil.debug(this.getClass(), "transactionDate=" + transactionDate);
		LogUtil.debug(this.getClass(), "transactionReference=" + transactionReference);
		LogUtil.debug(this.getClass(), "transactionTypeCode=" + transactionTypeCode);
		LogUtil.debug(this.getClass(), "company=" + company);
		LogUtil.debug(this.getClass(), "memSuspenseAccount=" + memSuspenseAccount);

		// ledgers for refunds are done by the creditor invoice import
		boolean requiresLedgers = false;
		// is a writeoff
		if (transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_WRITE_OFF)) {
			sendToAccount = refMgr.getMembershipAccount(voucher.getWriteOffAccountNumber());
			sendToAccount.setCostCenter(voucher.getCostCentre());

			description = "Write-off of membership credit. Ref: " + transactionReference;
			requiresLedgers = true;
		} else if (transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER_CREDIT)) {
			sendToAccount = memSuspenseAccount; // in an out of suspense

			description = "Transfer of membership credit to " + voucher.getTfrMembershipNumber() + ". Ref: " + transactionReference;
			requiresLedgers = true;
		} else if (transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REFUND)) {
			// no ledgers required
			description = "Refund of membership credit. Ref: " + transactionReference;
		} else {
			throw new SystemException("Unknown transaction type '" + transactionTypeCode + "' when processing refund.");
		}
		LogUtil.debug(this.getClass(), "sendToAccount=" + sendToAccount);
		LogUtil.debug(this.getClass(), "description=" + description);
		LogUtil.debug(this.getClass(), "requiresLedgers=" + requiresLedgers);

		// fee amount

		if (voucher.getFeeAmount().compareTo(new BigDecimal(0)) > 0) {

			try {
				// debit the suspense account for the full amount
				posting = new PayableItemPostingVO(voucher.getFeeAmount().doubleValue(), memSuspenseAccount, null, transactionDate, transactionDate, company, description, null);
				postingList.add(posting);

				// create income ledgers
				Account feeAccount = refMgr.getMembershipAccountByTitle(MembershipAccountVO.MEMBERSHIP_REFUND_FEE_ACCOUNT);
				posting = new PayableItemPostingVO(voucher.getFeeAmount().negate().doubleValue(), feeAccount, null, transactionDate, transactionDate, company, description, null);
				postingList.add(posting);
			} catch (PaymentException e) {
				throw new SystemException(e);
			}
		}

		try {
			if (requiresLedgers) {
				// debit the suspense account for the full amount
				posting = new PayableItemPostingVO(voucher.getDisposalAmount().doubleValue(), memSuspenseAccount, null, transactionDate, transactionDate, company, description, null);
				postingList.add(posting);

				posting = new PayableItemPostingVO(voucher.getDisposalAmount().negate().doubleValue(), sendToAccount, null, transactionDate, transactionDate, company, description, null);
				postingList.add(posting);
			}

			if (postingList.size() > 0) {

				// component
				PayableItemComponentVO component = new PayableItemComponentVO(postingList, voucher.getTotalAmount().setScale(2, BigDecimal.ROUND_HALF_UP), description, null, voucher.getClientNumber().toString(), null);

				if (!component.doPostingsBalance()) {
					throw new SystemException("Component postings do not balance. " + component);
				}

				componentList.add(component);

				Integer clientBranchNumber = client.getBranchNumber();
				PaymentMethod paymentMethod = new ReceiptingPaymentMethod(new Integer(voucher.getClientNumber()), sourceSystem);
				// payable item
				payableItem = new PayableItemVO(null, client.getClientNumber(), paymentMethod, componentList, sourceSystem, transactionReference, memTransVO.getSalesBranchCode(), clientBranchNumber, oldMemVO.getProductCode(), voucher.getUserId(), transactionDate, transactionDate, description);
				payableItem.setCreateDateTime(transactionDate);
				payableItem.setAmountPayable(0);

				newPayableItem = paymentTxMgr.addPayableItem(payableItem);

			}

		} catch (RollBackException ex) {
			throw new SystemException(ex);
		} catch (PaymentException pex) {
			throw new SystemException("Error processing the amount payable.", pex);
		}

		if (newPayableItem != null) {
			// update payable item reference
			memTransVO.setPayableItemID(newPayableItem.getPayableItemID());
		}
		// update XML with revised credit amount
		memTransVO.setMembershipXML(newMemVO.toXML());
		membershipMgrLocal.updateMembershipTransaction(memTransVO);

		if (transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REFUND)) {
			LogUtil.debug(this.getClass(), "performRefund");
			// create cheque/eft refund records in finance system
			paymentTxMgr.performRefund(voucher);
		} else // is a write-off
		{
			LogUtil.debug(this.getClass(), "write off - no action");
			// ledgers created in payment system above
		}
	}

	public ArrayList processExpireMembershipCredit(TransactionGroup transGroup) throws RemoteException {
		LogUtil.debug(this.getClass(), "processMembershipCredit start");
		TransactionHelper transactionHelper = new TransactionHelper();

		PaymentTransactionMgr paymentTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
		ArrayList completedTransactionList = new ArrayList();

		try {
			createSnapshotForOldMemberships(transGroup);

			// only one transaction
			MembershipTransactionVO memTransVO = (MembershipTransactionVO) transGroup.getTransactionList().get(0);
			transGroup.setTransactionDate(new DateTime());
			transGroup.setMembershipTransactionIDs();

			MembershipVO newMemVO = memTransVO.getNewMembership();
			newMemVO = membershipMgrLocal.updateMembership(newMemVO);
			memTransVO.setNewMembership(newMemVO);
			memTransVO = transactionHelper.createMembershipTransaction(membershipMgrLocal, memTransVO);
			LogUtil.debug(this.getClass(), "after createMembershipTransaction");
			// as transGroup implements payable item generator it will use the
			// subclasses implementation
			// of generate payable item list.
			PayableItemVO payableItem = sendAmountPayable(transGroup, paymentTxMgr);
			LogUtil.debug(this.getClass(), "after sendAmountPayable");
			// update transaction with payable item reference
			completedTransactionList.add(memTransVO);

			LogUtil.debug(this.getClass(), "after completedTransactionList");

			transactionHelper.updateMembershipTransactionMembershipXML(membershipMgrLocal, memTransVO);

			LogUtil.debug(this.getClass(), "after updateMembershipTransactionMembershipXML");
			transactionHelper.setMembershipTransactions(membershipMgrLocal, transGroup, payableItem);
			LogUtil.debug(this.getClass(), "after setMembershipTransactions");

			paymentTxMgr.commit();
		} catch (Exception ex) {
			try {
				if (paymentTxMgr != null) {
					paymentTxMgr.rollback();
				}
			} catch (Exception e) {
				throw new RemoteException("Error rolling back transactions: " + e);
			}
			ex.printStackTrace();
			throw new RemoteException("Error handling transaction for expired credit: ", ex);

		}
		LogUtil.debug(this.getClass(), "processMembershipCredit end");
		return completedTransactionList;
	}
} // Class MembershipTransactionMgrBean
