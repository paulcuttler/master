package com.ract.membership;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.util.Interval;

/**
 * <p>
 * Represent free credit to a member.
 * </p>
 * <p>
 * For example, free membership as a result of insufficient membership following
 * a remove from group transaction
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class ComplimentaryCredit implements TransactionCredit, Serializable {
    /**
     * construct the object
     * 
     * @param amount
     *            complimentary credit amount
     * @param period
     * @param description
     */
    public ComplimentaryCredit(BigDecimal amount, Interval period, String description) {
	this.setComplimentaryCreditPeriod(period);
	this.setTransactionCreditAmount(amount);
	this.setCreditDescription(description);
    }

    /**
     * The free credit dollar value
     */
    private BigDecimal complimentaryCreditAmount;

    /**
     * The free credit time value
     */
    private Interval complimentaryCreditPeriod;

    private String creditDescription;

    public BigDecimal getTransactionCreditAmount() {
	return complimentaryCreditAmount;
    }

    public void setTransactionCreditAmount(BigDecimal credit) {
	this.complimentaryCreditAmount = credit;
    }

    public Interval getComplimentaryCreditPeriod() {
	return complimentaryCreditPeriod;
    }

    public void setComplimentaryCreditPeriod(Interval complimentaryCreditPeriod) {
	this.complimentaryCreditPeriod = complimentaryCreditPeriod;
    }

    public String getCreditDescription() {
	return creditDescription;
    }

    public void setCreditDescription(String creditDescription) {
	this.creditDescription = creditDescription;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append("Complimentary Credit Period (years:months:days:hours:minutes:seconds:milliseconds) = " + this.getComplimentaryCreditPeriod() + ", ");
	desc.append("Transaction Credit Amount = " + this.getTransactionCreditAmount() + ", ");
	desc.append("Credit Description = " + this.getCreditDescription() + ".");
	return desc.toString();
    }
}
