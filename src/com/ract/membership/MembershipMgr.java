package com.ract.membership;

import java.io.File;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import com.ract.common.GenericException;
import com.ract.common.RollBackException;
import com.ract.common.notifier.NotificationEvent;
import com.ract.membership.club.AffiliatedClubVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.user.User;
import com.ract.util.DateTime;

@Remote
public interface MembershipMgr {

    public static String MODE_DELETE = "delete";

    public static String MODE_CANCEL = "cancel";

    public Collection getProductBenefitTypeList();

    public Collection<MembershipTypeVO> getMembershipTypeByClientType(String clientTypeCode);

    public void convertAccessMembers(int limit) throws RemoteException;

    public void convertAccessMembership(MembershipVO membership, ProductVO accessProduct, User roadsideUser, Hashtable<Integer, String> accessMembers) throws RemoteException;

    public Collection getMembershipTransactionTypes();

    public Collection getMembershipTypes();

    public Collection getProfileDiscountList(String profileCode) throws RemoteException;

    public ProductBenefitTypeVO getProductBenefitType(String productBenefitTypeCode);

    public MembershipTypeVO getMembershipType(String membershipTypeCode);

    public boolean removeRenewalDocument(Integer documentId, String mode, PaymentTransactionMgrLocal paymentTransactionMgrLocal, boolean ignoreErrors) throws RemoteException, RenewalNoticeException;

    public void updateRenewalDocument(MembershipDocument document) throws RemoteException;

    public void deleteMembershipFromGroups(Integer membershipId);

    public void deleteMembershipGroup(Integer groupID);

    public Collection<FeeSpecificationVO> getFeeSpecifications(String productCode, String productBenefitCode, String membershipTypeCode, String transactionTypeCode, Integer groupNumber) throws GenericException;

    public Collection<FeeSpecificationVO> getOrderedFeeSpecifications();

    public Collection<FeeSpecificationVO> getFeeSpecifications(String productCode, String productBenefitCode, String membershipTypeCode, String transactionTypeCode, String feeTypeCode, Integer groupNumber) throws GenericException;

    public void createClub(AffiliatedClubVO club);

    public void updateClub(AffiliatedClubVO club);

    public Collection<FeeSpecificationVO> getFeeSpecifications();

    public FeeSpecificationVO getFeeSpecification(Integer feeSpecificationId);

    public void createProductBenefitType(ProductBenefitTypeVO productBenefitType);

    public void deleteProductBenefitType(ProductBenefitTypeVO productBenefitType);

    public void deleteMembershipAccount(MembershipAccountVO memAccount);

    public void deleteClub(AffiliatedClubVO club);

    public Collection<FeeTypeVO> getFeeTypes();

    public FeeTypeVO getFeeType(String feeTypeCode);

    public void deleteFeeType(FeeTypeVO feeType);

    public FeeTypeVO updateFeeType(FeeTypeVO feeType);

    public void createFeeType(FeeTypeVO feeType);

    public MembershipAccountVO updateMembershipAccount(MembershipAccountVO memAccount);

    public MembershipAccountVO createMembershipAccount(MembershipAccountVO memAccount);

    public Collection<FeeSpecificationVO> getFeeSpecification(String feeTypeCode);

    public Collection<MembershipAccountVO> getMembershipAccounts();

    public MembershipAccountVO getMembershipAccount(Integer accountId);

    public void updateProductBenefitType(ProductBenefitTypeVO productBenefitType);

    public void deleteFeeSpecification(FeeSpecificationVO feeSpec);

    public FeeSpecificationVO updateFeeSpecification(FeeSpecificationVO feeSpec);

    public void createFeeSpecification(FeeSpecificationVO feeSpec);

    public ArrayList getEligibleAccessMembershipClients(DateTime effectiveDate, int limit, String logFileName, String auditFileName, String errorFileName) throws RemoteException;

    public MembershipProfileVO getMembershipProfile(String profileCode);

    public Collection<MembershipProfileVO> getMembershipProfiles();

    public void createMembershipProfile(MembershipProfileVO profile) throws RemoteException;

    public MembershipProfileVO updateMembershipProfile(MembershipProfileVO profile) throws RemoteException;

    public void deleteMembershipProfile(MembershipProfileVO profile) throws RemoteException;

    // public void notifyMembership(NotificationMgr notificationMgr, Collection
    // membershipList, boolean delete)
    // throws RemoteException;

    public Collection getDiscountList(Integer membershipID) throws RemoteException;

    public MembershipCardVO getMembershipCardByCardNumber(String cardNumber) throws RemoteException;

    public Collection getDiscountTypeByAccountId(Integer accountID) throws RemoteException;

    public Collection getProducts(String affiliateProductLevel) throws RemoteException;

    public void transferMembership(Connection connection, Integer duplicateClientNumber, Integer masterClientNumber, MembershipVO duplicateClientMembership, MembershipVO masterClientMembership) throws RemoteException;

    public DiscountTypeVO getDiscountType(String discountTypeCode) throws RemoteException;

    public Collection getDiscountTypes() throws RemoteException;

    public Collection getDiscountTypeByFeeSpecificationId(Integer feeSpecificationID) throws RemoteException;

    public Collection getAgentList(String agentId, String agentName, String location, Boolean roadside, Boolean towing, Boolean battery) throws RemoteException;

    public Collection getAgentLocationList() throws RemoteException;

    public int uploadAgentsFile(File file) throws RemoteException;

    public Integer getDocumentId(Integer membershipId, String documentTypeCode, DateTime firstDate, DateTime lastDate, boolean includeCancelled) throws RemoteException;

    public MembershipDocument getMembershipDocument(Integer documentId) throws RemoteException;

    public MembershipTier getMembershipTier(String tierCode) throws RemoteException;
    
    public MembershipTier getMembershipTierByJoinDate ( DateTime joinDate ) throws RemoteException;

    public void createGroup(GroupVO group) throws RemoteException;

    public void removeGroup(GroupVO group) throws RemoteException;

    public Collection getTransactionFeeDiscountAttributes(Integer transID, Integer feeSpecID, String discountTypeCode) throws RemoteException;

    public void produceGMVoucherLetters(DateTime effectiveDate) throws RemoteException;

    public void updateGroup(GroupVO group) throws RemoteException;

    public void createDiscountType(DiscountTypeVO discountType) throws RemoteException;

    public void updateDiscountType(DiscountTypeVO discountType) throws RemoteException;

    public void removeDiscountType(DiscountTypeVO discountType) throws RemoteException;

    public void createEligibleAccessMembershipClients(DateTime effectiveDate, boolean createMembership, int limit, String userId) throws RemoteException;

    public Collection getDiscountAttributes(String discountTypeCode) throws RemoteException;

    public Collection getProducts() throws RemoteException;

    public ProductVO getProduct(String productCode) throws RemoteException;

    public void createProduct(ProductVO product) throws RemoteException;

    public void updateProduct(ProductVO product) throws RemoteException;

    public void undoAccessMemberships(DateTime effectiveDate, boolean testMode, int limit) throws RemoteException;

    public void undoAccessMembership(DateTime effectiveDate, boolean testMode, Integer membershipId) throws RemoteException, UndoAccessMemberException;

    public void createAccessMembership(Integer clientNumber, Integer membershipId, DateTime effectiveDate, String joinReasonCode) throws RemoteException;

    public void createCMOMembership(Integer clientNumber, Integer membershipId, DateTime effectiveDate, String rego, String vin, String make, String model, String year, String profile, DateTime expiryDate) throws RemoteException;

    public Collection getDocumentList(Integer membershipID) throws RemoteException;

    public List<MembershipDocument> getERenewalDocumentList(Integer membershipId) throws RemoteException;

    public ArrayList getMembershipCardList(Integer membershipID) throws RemoteException;

    public Collection getDiscountListByTier(String tierCode) throws RemoteException;

    public void removeMembershipTransaction(MembershipTransactionVO memTransVO) throws RemoteException;

    public void createMembershipTransaction(MembershipTransactionVO memTransVO) throws RemoteException;

    public void updateMembershipTransaction(MembershipTransactionVO memTransVO) throws RemoteException;

    public MembershipTransactionVO findMembershipTransactionByQuoteNumber(Integer quoteNum) throws RemoteException;

    public Collection getClubList() throws RemoteException;

    public AffiliatedClubVO getClub(String clubCode) throws RemoteException;

    // public Collection findMembershipTransactionByMembershipID(Integer memID)
    // throws RemoteException;

    public Collection findMembershipTransactionByTransactionGroupID(int transGroupID) throws RemoteException;

    public MembershipTransactionVO getMembershipTransaction(Integer transID) throws RemoteException;

    public Collection findMembershipTransactionByPayableItemID(Integer payItemID) throws RemoteException;

    public MembershipTransactionVO getLastMembershipTransaction(Integer membershipId) throws RemoteException;

    public MembershipVO getMembership(Integer membershipID) throws RemoteException;

    public MembershipTransactionVO findTransactionByTransactionGroupandMembership(Integer membershipId, Integer transactionGroupId) throws RemoteException;

    public Collection getMembershipGiftByMembershipID(Integer membershipId) throws RemoteException;

    public void createMembershipGift(MembershipGift memGift) throws RemoteException;

    public MembershipGift getMembershipGift(MembershipGiftPK membershipGiftPK) throws RemoteException;

    public void removeMembershipGift(MembershipGift membershipGift) throws RemoteException;

    public void updateMembershipGift(MembershipGift memGift) throws RemoteException;

    public Collection getProductOptionList(Integer membershipID) throws RemoteException;

    public Collection getTransactionList(Integer membershipID) throws RemoteException;

    public MembershipDocument getCurrentDocument(Integer membershipId, DateTime expiryDate) throws RemoteException;

    public List<MembershipDocument> getCurrentDocuments(Integer membershipId, DateTime expiryDate) throws RemoteException, GenericException;

    public Collection getVehicleList(Integer membershipID) throws RemoteException;

    public boolean isCardRequested(Integer membershipId) throws RemoteException;

    // public Collection getMembershipCardListToRemove(Integer membershipID)
    // throws RemoteException;

    public MembershipTransactionVO findMembershipTransactionForMemberAndGroup(int membershipID, int transactionGroupID) throws RemoteException;

    // public Collection getMembershipCardListToRemove(Integer membershipID,
    // DateTime issuedFromDate)
    // throws RemoteException;

    public void createMembershipCards(Collection cardVOList) throws RemoteException;

    public void removeDocumentList(Integer membershipId) throws RemoteException;

    public void createMembershipDocuments(Collection docVOList) throws RemoteException;

    public Collection findMembershipTransactionsByTransactionID(Integer transID) throws RemoteException;

    // public boolean isPrimeAddressee(Integer membershipID)
    // throws RemoteException;

    public void processNotificationEvent(NotificationEvent event) throws RemoteException;

    public MembershipVO updateMembershipDirectDebitAuthority(DirectDebitAuthority newAuthority, MembershipVO membershipVO, String userID) throws RemoteException;

    // public Collection getMembershipGroup(Integer membershipId)
    // throws RemoteException;

    public Collection findMembershipByClientNumber(Integer clientNumber) throws RemoteException;

    public Collection findMembershipCardsByMembership(Integer membershipId) throws RemoteException;

    public Collection findMembershipCardsByIssueDate(Integer membershipId, DateTime issueDate) throws RemoteException;

    public Collection findMembershipCardRequests(Integer membershipId) throws RemoteException;

    public void removeMembershipCard(MembershipCardVO membershipCard) throws RemoteException;

    public void removeMembershipCard(Integer cardId) throws RemoteException;

    public void removeMembershipGift(MembershipGiftPK membershipGiftPK) throws RemoteException;

    public void createMembership(MembershipVO membership) throws RemoteException;

    public MembershipVO updateMembership(MembershipVO membership) throws RemoteException;

    public MembershipCardVO getMembershipCard(Integer cardId) throws RemoteException;

    public void updateMembershipCard(MembershipCardVO memCard) throws RemoteException;

    public void removeMembership(MembershipVO membership) throws RemoteException;

    public MembershipVO findMembershipByClientAndType(Integer clientNumber, String memTypeCode) throws RemoteException;

    public void createMembershipCard(MembershipCardVO memCard) throws RemoteException;

    public MembershipTier getMembershipTierByMembershipYears(int membershipYears) throws RemoteException;

    public Collection getMembershipTierList() throws RemoteException;

    // public Collection getMembershipGroupDetailListForMember(Integer
    // membershipId)
    // throws RemoteException;

    public MembershipGroupVO getMembershipGroupVO(Integer membershipID) throws RemoteException;

    // public Collection getMembershipGroupsByMembershipID(Integer membershipID)
    // throws RemoteException;

    public Vector getMembershipQuoteList() throws RemoteException;

    public Collection getCurrentTransactionList(Integer membershipId) throws RemoteException;

    public Collection getCurrentQuoteList(Integer clientNumber) throws RemoteException;

    public void updateMembershipQuotesWithUserDetails() throws RemoteException;

    public GroupVO getGroupByGroupId(Integer groupId) throws RemoteException;

    // public Collection getGroupMembersByGroupID(Integer groupID)
    // throws RemoteException;

    public Collection findMembershipGroupsByMembershipID(Integer membershipID) throws RemoteException;

    public Collection findMembershipGroupsByGroupID(Integer groupID) throws RemoteException;

    public void createMembershipGroupDetail(MembershipGroupDetailVO membershipGroupDetail) throws RemoteException;

    public void removeMembershipGroupDetail(MembershipGroupDetailVO membershipGroupDetail) throws RemoteException;

    public void updateMembershipQuote(MembershipQuote memQuote) throws RemoteException;

    public void createMembershipQuote(MembershipQuote memQuote) throws RemoteException;

    // public void removeFromGroup(Integer membershipID, Integer groupID)
    // throws RemoteException;

    public double removeExpiredCredit(Integer transId, MembershipVO memVO, DateTime transactionDate, User systemUser) throws RemoteException;

    public MembershipDocument getRenewalDocument(Integer memID, DateTime expiryDate) throws RemoteException;

    public void cancelMembership(String reasonCode, MembershipVO memVO, User systemUser) throws RemoteException;

    public void cancelRenewalDocument(Integer documentId) throws RemoteException, RenewalNoticeException;

    public void cancelRenewalDocument(Integer documentId, boolean ignoreErrors) throws RemoteException, RenewalNoticeException;

    public void cancelExpiredRenewalDocuments();

    public Vector getMembershipQuoteListByClientNumber(Integer clientNumber) throws RemoteException;

    public Vector getMembershipQuoteListByClientNumber(Integer clientNumber, boolean includeAttached) throws RemoteException;

    public Vector getMembershipQuoteListByMembershipID(Integer membershipID) throws RemoteException;

    public MembershipQuote getMembershipQuote(Integer quoteNumber) throws RemoteException;

    /**
     * Remove the credit amount from membership records that vahe been cancelled
     * for more than a specified period of time
     */
    public Hashtable removeExpiredMembershipCredits() throws RemoteException;

    /**
     * Gets the membershipTransactionDiscountList attribute of the
     * MembershipTransactionMgrBean object
     * 
     * @param transID
     *            Description of the Parameter
     * @param feeSpecID
     *            Description of the Parameter
     * @return The membershipTransactionDiscountList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Vector getMembershipTransactionDiscountList(Integer transID, Integer feeSpecID) throws RemoteException;

    /**
     * Auto cancel memberships that have been marked as allow to lapse and are
     * expired.
     * 
     * @throws RemoteException
     */
    public List<Integer> autoCancelAllowToLapse() throws RemoteException;

    /**
     * Auto cancel memberships that have been expired for a specific time.
     * 
     * @throws RemoteException
     */
    public List<Integer> autoCancelExpired() throws RemoteException;

    /**
     * Auto cancel memberships that are linked to deceased clients.
     * 
     * @throws RemoteException
     */
    public List<Integer> autoCancelDeceased() throws RemoteException;

    public void updateMembershipDocument(MembershipDocument document) throws RemoteException;

    public void transferFailedDirectDebit(Integer clientNumber) throws RemoteException, PaymentException, RollBackException;

    public void transferFailedDirectDebit(Integer clientNumber, User user) throws RemoteException, PaymentException, RollBackException;

    public List<MembershipVO> getMembershipsWithRenewalAdvices(DateTime expiryDate) throws RemoteException;

    public Map<String, String> getRenewalAdvicesWithoutDDSchedules(DateTime expiryDate) throws RemoteException;

    public Map<Integer, BigDecimal> getMembershipsWithUnbalancedCredit(DateTime startDate, DateTime endDate) throws RemoteException;

    public BigDecimal getTotalCredit() throws RemoteException;

    public Map<Integer, BigDecimal> getInstantaneousMembershipCredit(DateTime startDate, DateTime endDate) throws RemoteException;

    public BigDecimal getTotalInstantaneousMembershipCredit(DateTime startDate, DateTime endDate) throws RemoteException;

    public List<Map<String, Object>> getMembershipsWithCredit() throws RemoteException;

    public List<Map<String, Object>> getMembershipsWithCreditPostings() throws RemoteException;

    public List<Map<String, Object>> getMembershipsWithVouchers() throws RemoteException;

    public List<MembershipVO> getMembershipsWithoutCards() throws RemoteException;

    public List<Map<String, Object>> getAutoRenAuditEntries() throws RemoteException;

    public void deleteGroupsOfOne() throws RemoteException;

    public List<MembershipVO> findMembershipByVin(String vin, String productCode) throws RemoteException;

    public List<MembershipVO> findMembershipByVin(String vin, String productCode, String status) throws RemoteException;

    public List<MembershipVO> findMembershipByRego(String rego) throws RemoteException;

    public List<MembershipVO> getMembershipsByProduct(String productCode) throws RemoteException;

    public Long getMembershipChangesCount(DateTime startDate, DateTime endDate) throws RemoteException;

    public Boolean isMembershipChangesExist(DateTime startDate, DateTime endDate) throws RemoteException;

    public Collection<Integer> getMembershipChangesIdentifiers(DateTime startDate, DateTime endDate) throws RemoteException;

    public Collection<MembershipVO> getMembershipChanges(DateTime startDate, DateTime endDate) throws RemoteException;
    
    public PayableItemVO getPayableItem(Integer payableItemID) throws RemoteException;

    /**
     * Look for the existence of a particular discount on a given membership.
     * Return true if the discount is present. Return false if not present.
     * 
     * @param Integer
     *            membership Id
     * @param String
     *            Discount type code
     * @param DateTime
     *            End date of the period
     * @return
     * @throws RemoteException
     */
    public boolean hasDiscount(Integer memId, String discountTypeCode, DateTime endDate) throws RemoteException;

    /**
     * Lookup the database for a all memberships related to the given membership
     * number. Does NOT look in any cache If this is a group membership, return
     * the group. If an individual, return a single membership.
     * 
     * @param Integer
     *            membership Id
     * @return ArrayList<MembershipVO> Collection of membership objects
     * @throws RemoteException
     */
    public ArrayList<MembershipVO> getMembershipGroup(Integer memId) throws RemoteException;

}
