package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.ClassWriter;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.vehicle.VehicleVO;

/**
 * Holds information about vehicles attached to a membership. This is a normal
 * java class that is populated and stored when the MembershipBean is populated
 * and stored.
 * 
 * @author T. Bakker.
 */
public class MembershipVehicle implements Writable, Serializable {
    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "MembershipVehicle";

    /**
     * The date from which the vehicle may be used to claim membership benefits.
     * This may be different from the membership commence date if 1. the
     * membership uses the "Un-common expiry" management method, or 2. if the
     * vehicle is attached to the membership after the membership has commenced.
     */
    private DateTime commenceDate;

    /**
     * The date on which the vehicle may no longer be used to claim membership
     * benefits. This may be different from the membership expiry date if the
     * "Un-common expiry" management method is used. It should never be greater
     * than the membership expiry date.
     */
    private DateTime expiryDate;

    private Integer vehicleID;

    /**
     * A reference to the vehicle that is attached to the membership.
     */
    private VehicleVO vehicleVO;

    /**
     * Default constructor.
     */
    public MembershipVehicle() {

    }

    public Integer getVehileID() {
	return this.vehicleID;
    }

    public DateTime getCommenceDate() {
	return this.commenceDate;
    }

    public DateTime getExpiryDate() {
	return this.expiryDate;
    }

    public VehicleVO getVehicle() {
	return this.vehicleVO;
    }

    public void setVehicleID(Integer vehicleID) {
	this.vehicleID = vehicleID;
    }

    public void setCommenceDate(DateTime comDate) {
	this.commenceDate = comDate;
    }

    public void setExpiryDate(DateTime expDate) {
	this.expiryDate = expDate;
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, false);
    }

    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("vehicleID", this.vehicleID);
	cw.writeAttribute("commenceDate", this.commenceDate);
	cw.writeAttribute("expiryDate", this.expiryDate);

	if (deepWrite) {
	    cw.writeAttribute("vehicle", getVehicle());
	}

	cw.endClass();
    }
}
