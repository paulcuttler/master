package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.Writable;

/**
 * Specifies a discount that recurs for the membership.
 */
public class MembershipDiscount implements Writable, Serializable {

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "MembershipDiscount";

    /**
     * Identifies the discount type that applies to the membership.
     */
    private String discountTypeCode;

    /**
     * The discount that has been attached to the membership.
     */
    private DiscountTypeVO discountType;

    /******************************* Constructors ***************************/
    /**
     * Default constructor.
     */
    public MembershipDiscount() {
    }

    /**
     * Data constructor
     */
    public MembershipDiscount(String discountTypeCode) {
	this.discountTypeCode = discountTypeCode;
    }

    public MembershipDiscount(DiscountTypeVO discTypeVO) {
	this.discountType = discTypeVO;
	this.discountTypeCode = discTypeVO.getDiscountTypeCode();
    }

    public MembershipDiscount(Node node) {
	NodeList elementList = node.getChildNodes();
	Node elementNode;
	String valueText;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);

	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;

	    }
	    if (valueText != null) {
		if ("discountTypeCode".equals(elementNode.getNodeName())) {
		    this.discountTypeCode = valueText;
		}
	    }
	}
    }

    /***************************** Getter methods ***************************/

    public String getDiscountTypeCode() {
	return discountTypeCode;
    }

    /**
     * Return the discount type value object that this membership discount type
     * maps to.
     */
    public DiscountTypeVO getDiscountType() throws RemoteException {
	if (this.discountTypeCode != null && this.discountType == null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.discountType = refMgr.getDiscountType(this.discountTypeCode);
	    if (this.discountType == null) {
		throw new RemoteException("Failed to get discount type '" + this.discountTypeCode + "'. It does not exist!");
	    }
	}
	return this.discountType;
    }

    /***************************** Setter methods ***************************/

    public void setDiscountTypeCode(String discountTypeCode) {
	this.discountTypeCode = discountTypeCode;
    }

    /***************************** Utility methods ***************************/

    public boolean equals(Object obj) {
	boolean equals = false;
	if (obj != null && obj instanceof MembershipDiscount) {
	    MembershipDiscount that = (MembershipDiscount) obj;
	    equals = this.discountTypeCode.equals(that.getDiscountTypeCode());
	}
	return equals;
    }

    public MembershipDiscount copy() {
	return new MembershipDiscount(this.discountTypeCode);
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, false);
    }

    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("discountTypeCode", this.discountTypeCode);

	if (deepWrite) {
	    cw.writeAttribute("discountType", getDiscountType());
	}

	cw.endClass();
    }
}
