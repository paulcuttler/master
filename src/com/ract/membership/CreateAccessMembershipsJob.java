package com.ract.membership;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * <p>
 * Run the access membership creation program.
 * </p>
 * 
 * @author newtong
 * @version 1.0
 */
public class CreateAccessMembershipsJob implements Job {

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();
	try {
	    DateTime effectiveDate = new DateTime(dataMap.getString("effectiveDate"));
	    String createMemberships = new String(dataMap.getString("createMemberships"));
	    boolean createMembership = Boolean.valueOf(createMemberships).booleanValue();
	    String userId = new String(dataMap.getString("userId"));

	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	    membershipMgr.createEligibleAccessMembershipClients(effectiveDate, createMembership, -1, userId);
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }

}
