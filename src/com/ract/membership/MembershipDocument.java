package com.ract.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.XMLHelper;

/**
 * A document sent to a member.
 * 
 * @hibernate.class table="mem_document" lazy="false"
 * 
 * @author Terry Bakker.
 */
public class MembershipDocument implements Writable, Comparable, Serializable {

    public static final String RENEWAL_NOTICE = "Renewal Notice";

    public static final String RENEWAL_ADVICE = "Renewal Advice";

    public static final String RENEWAL_REMINDER = "Renewal Reminder";

    public static final String RENEWAL_FINAL_NOTICE = "Renewal Final Notice";

    public static final String RENEWAL_UNFINANCIAL_GROUP_MEMBER_ADVICE = "Renewal Unfinancial Group Member Advice";

    public static final String RENEWAL_REMINDER_SMS = "SMS Renewal Reminder";

    public static final String DOCUMENT_STATUS_CANCELLED = "Cancelled";

    public static final String VOUCHER_LETTER_TIER_CHANGE = "GM Tier Change ";

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public static final String WRITABLE_CLASSNAME = "MembershipDocument";

    /**
     * A unique identifier for this document.
     */
    protected Integer documentId;

    /**
     * The ID of the membership that was sent the document.
     */
    protected Integer membershipId;

    /**
     * The type of document sent to the member.
     */
    protected String documentTypeCode;

    /**
     * The date that the document was sent to the member.
     */
    protected DateTime documentDate;

    /**
     * The the status of the document.
     */
    protected String documentStatus;

    private DateTime cancelDate;

    private boolean printed;

    private boolean emailed;

    private String emailAddress;

    private DateTime emailDate;

    /**
     * @hibernate.property
     */
    public boolean isPrinted() {
	return printed;
    }

    public void setPrinted(boolean printed) {
	this.printed = printed;
    }

    /**
     * @hibernate.property
     */
    public boolean isEmailed() {
	return emailed;
    }

    public void setEmailed(boolean emailed) {
	this.emailed = emailed;
    }

    /**
     * @hibernate.property
     */
    public String getEmailAddress() {
	return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
    }

    /**
     * @hibernate.property
     */
    public DateTime getEmailDate() {
	return emailDate;
    }

    public void setEmailDate(DateTime emailDate) {
	this.emailDate = emailDate;
    }

    /**
     * @hibernate.property
     */
    public DateTime getCancelDate() {
	return cancelDate;
    }

    public void setCancelDate(DateTime cancelDate) {
	this.cancelDate = cancelDate;
    }

    /**
     * The membership run which produced this document
     */
    protected Integer runNumber;

    /**
     * The expiry date of membership when this document was created
     */
    protected DateTime membershipDate;

    /**
     * The date at which fees are to be calculated
     */
    protected DateTime feeEffectiveDate;

    protected BigDecimal amountPayable;

    protected String productCode;

    protected Integer groupNumber;

    protected BigDecimal discounts;

    protected String profileCode;

    private String transactionXML;

    /****************************** Constructors ****************************/
    /**
     * Default constructor. Required for entity bean.
     */
    public MembershipDocument() {
    }

    public MembershipDocument(Integer docID, String docTypeCode, DateTime docDate, Integer memID) {
	this.documentId = docID;
	this.documentTypeCode = docTypeCode;
	this.documentDate = docDate;
	this.membershipId = memID;
	this.documentStatus = null;
	// setModified(false);
    }

    public MembershipDocument(Integer docID, String docTypeCode, DateTime docDate, Integer memID, String documentStatus, Integer runNumber, DateTime memDate) {
	this.documentId = docID;
	this.documentTypeCode = docTypeCode;
	this.documentDate = docDate;
	this.membershipId = memID;
	this.documentStatus = documentStatus;
	this.runNumber = runNumber;
	this.membershipDate = memDate;
	// setModified(false);
    }

    public MembershipDocument(Integer docID, String docTypeCode, DateTime docDate, Integer memID, String documentStatus, Integer runNumber, DateTime memDate, DateTime feeEffectiveDate, BigDecimal amountPayable, String productCode, Integer groupNumber, BigDecimal discounts, String profileCode, boolean printed, boolean emailed, String emailAddress, DateTime emailDate, String transactionXML) {
	this.documentId = docID;
	this.documentTypeCode = docTypeCode;
	this.documentDate = docDate;
	this.membershipId = memID;
	this.documentStatus = documentStatus;
	this.runNumber = runNumber;
	this.membershipDate = memDate;
	this.amountPayable = amountPayable;
	this.productCode = productCode;
	this.groupNumber = groupNumber;
	this.discounts = discounts;
	this.profileCode = profileCode;
	this.feeEffectiveDate = feeEffectiveDate;
	this.printed = printed;
	this.emailed = emailed;
	this.emailAddress = emailAddress;
	this.emailDate = emailDate;
	this.transactionXML = transactionXML;
    }

    public MembershipDocument(Node node) {
	NodeList elementList = node.getChildNodes();
	Node elementNode;
	String valueText;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);

	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;

	    }
	    if (valueText != null) {
		if ("documentDate".equals(elementNode.getNodeName())) {
		    try {
			this.documentDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			// ignore
		    }
		} else if ("documentID".equals(elementNode.getNodeName())) {
		    this.documentId = new Integer(valueText);
		} else if ("documentTypeCode".equals(elementNode.getNodeName())) {
		    this.documentTypeCode = valueText;
		} else if ("membershipID".equals(elementNode.getNodeName())) {
		    this.membershipId = new Integer(valueText);
		} else if ("runNumber".equals(elementNode.getNodeName())) {
		    this.runNumber = new Integer(valueText);
		} else if ("status".equals(elementNode.getNodeName())) {
		    this.documentStatus = valueText;
		} else if ("membershipDate".equals(elementNode.getNodeName())) {
		    try {
			this.membershipDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			// ignore
		    }
		} else if ("feeEffectiveDate".equals(elementNode.getNodeName())) {
		    try {
			this.feeEffectiveDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			// ignore
		    }
		} else if ("amountPayable".equals(elementNode.getNodeName())) {
		    this.amountPayable = new BigDecimal(valueText);
		} else if ("discounts".equals(elementNode.getNodeName())) {
		    this.discounts = new BigDecimal(valueText);
		} else if ("productCode".equals(elementNode.getNodeName())) {
		    this.productCode = valueText;
		} else if ("profileCode".equals(elementNode.getNodeName())) {
		    this.profileCode = valueText;
		} else if ("groupNumber".equals(elementNode.getNodeName())) {
		    this.groupNumber = new Integer(valueText);
		}

	    }
	}
    }

    /****************************** Getter methods ****************************/

    /**
     * @hibernate.id column="document_id" generator-class="assigned"
     */
    public Integer getDocumentId() {
	return this.documentId;
    }

    public void setDocumentId(Integer documentId) {
	this.documentId = documentId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="document_type_code"
     */
    public String getDocumentTypeCode() {
	return this.documentTypeCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="document_date"
     */
    public DateTime getDocumentDate() {
	return this.documentDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="membership_id"
     */
    public Integer getMembershipId() {
	return this.membershipId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="document_status"
     */
    public String getDocumentStatus() {
	return this.documentStatus;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="run_number"
     */
    public Integer getRunNumber() {
	return this.runNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="mem_date"
     */
    public DateTime getMembershipDate() {
	return this.membershipDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="fee_effective_date"
     */
    public DateTime getFeeEffectiveDate() {
	return this.feeEffectiveDate;
    }

    /************************** Setter methods *******************************/

    public void setDocumentDate(DateTime documentDate) throws RemoteException {
	// checkReadOnly();
	this.documentDate = documentDate;
	// this.setModified(true);
    }

    public void setDocumentStatus(String documentStatus) {
	// checkReadOnly();
	this.documentStatus = documentStatus;
	// this.setModified(true);
    }

    protected void validateMode(String newMode) throws ValidationException {
    }

    public void setMembershipDate(DateTime memDate) {
	this.membershipDate = memDate;
    }

    public void setRunNumber(Integer runNum) {
	this.runNumber = runNum;
    }

    public void setFeeEffectiveDate(DateTime feeDate) {
	this.feeEffectiveDate = feeDate;
    }

    /********************** Writable interface methods ************************/

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("documentDate", this.documentDate);
	cw.writeAttribute("documentID", this.documentId);
	cw.writeAttribute("documentTypeCode", this.documentTypeCode);
	cw.writeAttribute("membershipID", this.membershipId);
	cw.writeAttribute("runNumber", this.runNumber);
	cw.writeAttribute("status", this.documentStatus);
	cw.writeAttribute("membershipDate", this.membershipDate);
	cw.writeAttribute("feeEffectiveDate", this.feeEffectiveDate);
	cw.writeAttribute("amountPayable", this.amountPayable);
	cw.writeAttribute("productCode", this.productCode);
	cw.writeAttribute("groupNumber", this.groupNumber);
	cw.writeAttribute("discounts", this.discounts);
	cw.writeAttribute("profileCode", this.profileCode);
	cw.endClass();
    }

    /************************** Utility methods *******************************/

    public MembershipDocument copy() {
	MembershipDocument memDocVO = new MembershipDocument(this.documentId, this.documentTypeCode, this.documentDate, this.membershipId, this.documentStatus, this.runNumber, this.membershipDate, this.feeEffectiveDate, this.amountPayable, this.productCode, this.groupNumber, this.discounts, this.profileCode, this.printed, this.emailed, this.emailAddress, this.emailDate, this.transactionXML);
	return memDocVO;
    }

    /************************ Comparable interface methods **********************/

    /**
     * Compare this document ID to another document's ID
     */
    public int compareTo(Object obj) {
	if (obj != null && obj instanceof MembershipDocument) {
	    MembershipDocument that = (MembershipDocument) obj;
	    return this.documentId.compareTo(that.getDocumentId());
	} else {
	    return 0;
	}
    }

    /**
     * @hibernate.property
     * @hibernate.column name="group_number"
     */
    public Integer getGroupNumber() {
	return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
	this.groupNumber = groupNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="product_code"
     */
    public String getProductCode() {
	return productCode;
    }

    public void setProductCode(String productCode) {
	this.productCode = productCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="profile_code"
     */
    public String getProfileCode() {
	return profileCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="amount_payable"
     */
    public BigDecimal getAmountPayable() {
	return amountPayable;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="discounts"
     */
    public BigDecimal getDiscounts() {
	return discounts;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="transaction_xml"
     */
    public String getTransactionXML() {
	return transactionXML;
    }

    public void setProfileCode(String profileCode) {
	this.profileCode = profileCode;
    }

    public void setDocumentTypeCode(String documentTypeCode) {
	this.documentTypeCode = documentTypeCode;
    }

    public void setMembershipId(Integer membershipId) {
	this.membershipId = membershipId;
    }

    public void setAmountPayable(BigDecimal amountPayable) {
	this.amountPayable = amountPayable;
    }

    public void setDiscounts(BigDecimal discounts) {
	this.discounts = discounts;
    }

    public void setTransactionXML(String transactionXML) {
	this.transactionXML = transactionXML;
    }

    public String toXML() throws SystemException {
	return XMLHelper.toXML(null, this);
    }

    @Override
    public String toString() {
	return "MembershipDocument [amountPayable=" + amountPayable + ", cancelDate=" + cancelDate + ", discounts=" + discounts + ", documentDate=" + documentDate + ", documentId=" + documentId + ", documentStatus=" + documentStatus + ", documentTypeCode=" + documentTypeCode + ", emailAddress=" + emailAddress + ", emailDate=" + emailDate + ", emailed=" + emailed + ", feeEffectiveDate=" + feeEffectiveDate + ", groupNumber=" + groupNumber + ", membershipDate=" + membershipDate + ", membershipId=" + membershipId + ", printed=" + printed + ", productCode=" + productCode + ", profileCode=" + profileCode + ", runNumber=" + runNumber + "]";
    }
}
