package com.ract.membership;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ract.common.Account;
import com.ract.common.ClassWriter;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SalesBranchVO;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.payment.AmountOutstanding;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.MethodCounter;
import com.ract.util.NumberUtil;
import com.ract.util.XMLHelper;

/**
 * Transaction object
 * 
 * @hibernate.class table="mem_transaction" lazy="false" discriminator-value="OTHER"
 * @hibernate.discriminator type="string" formula= "case when transaction_type_code = 'Hold' then 'HOLD' when transaction_type_code  = 'Change product' then 'CHANGE PRODUCT' when transaction_type_code  = 'Change option' then 'CHANGE PRODUCT OPTION' else 'OTHER' end"
 * 
 * @author bakkert created 1 August 2002
 */
public class MembershipTransactionVO extends ValueObject implements Comparable, Writable {

	boolean printRenewalDocument = false;

	public void setRenewalPrint(boolean renPrint) {
		this.printRenewalDocument = renPrint;
	}

	/**
	 * @hibernate.property
	 * @return
	 */
	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((transactionID == null) ? 0 : transactionID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MembershipTransactionVO other = (MembershipTransactionVO) obj;
		if (transactionID == null) {
			if (other.transactionID != null)
				return false;
		} else if (!transactionID.equals(other.transactionID))
			return false;
		return true;
	}

	/**
	 * The name to call this class when writing the class data using a ClassWriter
	 */
	public final static String WRITABLE_CLASSNAME = "MembershipTransaction";

	/**
	 * A unique identifier maintained by the system to reference the transaction by.
	 */
	protected Integer transactionID;

	/**
	 * The ID of the group that the membership belonged to when the transaction was performmed. Zero means no group.
	 */
	protected int transactionGroupID = 0;

	/**
	 * The number of memberships involved in the transaction group.
	 */
	protected Integer groupCount = new Integer(0);

	/**
	 * A code that identifies the transaction type. may be used for display.
	 */
	protected String transactionTypeCode;

	/**
	 * The type of group transaction that was being performed that caused the particular membership transaction. Will be null if no transaction group was involved.
	 */
	protected String transactionGroupTypeCode;

	/**
	 * Holds a reference to the transaction type corresponding to the transaction type code. Use the getTransactionType() method to populate.
	 */
	private MembershipTransactionTypeVO transactionType = null;

	/**
	 * The date/time that the transaction took place.
	 */
	protected DateTime transactionDate;

	/**
	 * The date that the client specified as the preferred date that changes to the membership are to take effect on.
	 */
	protected DateTime preferredCommenceDate;

	/**
	 * The ID of the membership that the transaction was carried out on.
	 */
	protected Integer membershipID;

	/**
	 * Holds a reference to the current membership data. If the transaction is being processed then the newMembership attribute holds a reference to the updated membership data. This attribute will be null if the membership is being created.
	 */
	private MembershipVO membership = null;

	/**
	 * Holds a reference to the new view of the membership data that is being changed as part of this transaction. Will only point to a membership while processing the new transaction.
	 */
	private MembershipVO newMembership = null;

	/**
	 * The username of the user who did the transaction.
	 */
	protected String username;

	/**
	 * The sales branch of the user when they did the transaction.
	 */
	protected String salesBranchCode;

	private SalesBranchVO salesBranchVO;

	/**
	 * The payment item ID returned by the payment system for the payable item. Will be null if no payable item is related to the transaction.
	 */
	protected Integer payableItemID;

	private PayableItemVO payableItemVO;

	protected String productCode;

	/**
	 * The name of the file that the membership snapshot is stored in. NOTE: This is now redundant. Use the membershipXML attribute instead
	 */
	protected String membershipHistoryFilename;

	private DateTime productEffectiveDate;

	/**
	 * only available when saving
	 */
	public DateTime getProductEffectiveDate() {
		return this.productEffectiveDate;
	}

	/**
	 * Contains an XML document that represents the state of the membership at the end of the transaction.
	 */
	protected String membershipXML;

	/**
	 * A reference to the read only snapshot of the membership data as it existed after the transaction completed bases on the xml stored against the transaction.
	 */
	private MembershipHistory membershipHistory;

	/**
	 * The reason why the member did the transaction.
	 */
	protected String transactionReasonType;

	/**
	 * Description of the Field
	 */
	protected String transactionReasonCode;

	/**
	 * The web transaction reference
	 */
	protected String transactionReference;

	/**
	 * The amount that is payable for the transaction. For memberships that are not in a membership group this will be the fully discounted transaction fee plus the gst. For memberships that are in a membership group and where the membership is NOT the prime addressee this will be zero. For memberships that are in a membership group and where the membership IS the prime addressee this will be fully discounted transaction fee for all memberships in the group plus the gst.
	 */
	protected double amountPayable;

	/**
	 * The GST fee charged for the transaction. For memberships that are not in a membership group this will be calculated as a percentage of the fully discounted transaction fee. For memberships that are in a membership group and where the membership is NOT the prime addressee this will be zero. For memberships that are in a membership group and where the membership IS the prime addressee this will be calculated as a percentage of the sum of the fully discounted transaction fees for all memberships in the group.
	 */
	protected double gstFee;

	/**
	 * Holds the list of fees applied to this transaction.
	 */
	protected Vector transactionFeeList;

	/**
	 * Holds the list of adjustments that apply to the transaction. e.g. Discretionary discount, quote adjustment. These are NOT the discounts that apply to the transaction fees.
	 */
	protected Vector adjustmentList;

	// /**
	// * Temporarily holds the user session when removing the entity. The
	// ejbRemove
	// * method needs the user session to pass to the payments manager so that
	// an
	// * audit trail of who did the removal can be kept.
	// */
	// protected UserSession userSession;

	/**
	 * The date that the transaction was undone. Null if the transaction has not been undone.
	 */
	protected DateTime undoneDate;

	/**
	 * The user who undid the transaction. Null if the transaction has not been undone.
	 */
	protected String undoneUsername;

	/**
	 * The quote that is attached to this transaction.
	 */
	protected Integer quoteNumber;

	private MembershipQuote attachedQuote;

	/**
	 * Indicates if the attached membership is the prime addressee of a group. This attribute is only used during transaction processing to indicate the prime addressee prior to the MembershipGroup records being created.
	 */
	private boolean primeAddressee = false;

	/**
	 * When doing a rejoin transaction this is the type of rejoin that was performed
	 */
	private String rejoinTypeCode;

	private ReferenceDataVO rejoinType;

	/**
	 * Pending Fee Id
	 */
	private String pendingFeeID;

	/**
	 * The date and time that the client will join the membership group. Set this to the memberships existing membership group join date or if the client is not part of a membership group then set this to a new DateTime().
	 */
	private DateTime membershipGroupJoinDate = null;

	/**
	 * The start date of the transaction.
	 */
	protected DateTime effectiveDate;

	/**
	 * Constructors **************************
	 */

	private Integer disposalId;

	/**
	 * @hibernate.property
	 */
	public Integer getDisposalId() {
		return disposalId;
	}

	public void setDisposalId(Integer disposalId) {
		this.disposalId = disposalId;
	}

	/**
	 * Default constructor. Required for entity bean.
	 */
	public MembershipTransactionVO() {
	}

	/**
	 * Does the transaction contain a join fee?
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public boolean hasJoinFee() throws RemoteException {
		boolean joinFeeFound = false;
		Collection feeList = this.getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeListIt = feeList.iterator();
			while (feeListIt.hasNext() && !joinFeeFound) {
				memTransFee = (MembershipTransactionFee) feeListIt.next();
				// to replace
				if (memTransFee.isJoinFee()) {
					joinFeeFound = true;
				}
			}
		}
		return joinFeeFound;
	}

	public double getJoinFeeAmount() throws RemoteException {
		double joinFeeAmount = 0;
		Collection feeList = this.getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeListIt = feeList.iterator();
			while (feeListIt.hasNext() && joinFeeAmount == 0) {
				memTransFee = (MembershipTransactionFee) feeListIt.next();
				// to replace
				if (memTransFee.isJoinFee()) {
					joinFeeAmount = joinFeeAmount + memTransFee.getNetTransactionFee();
				}
			}
		}
		return joinFeeAmount;
	}

	/**
	 * Constructor for the MembershipTransactionVO object
	 * 
	 * @param mode
	 *            Description of the Parameter
	 * @param transGroupTypeCode
	 *            Description of the Parameter
	 * @param transTypeCode
	 *            Description of the Parameter
	 */
	public MembershipTransactionVO(String mode, String transGroupTypeCode, String transTypeCode, DateTime memGroupJoinDate) {

		// Setting the mode to create will stop lazy fetches on related data
		// until after the membership transaction has been saved.
		try {
			setMode(mode);
		} catch (ValidationException ve) {
			LogUtil.warn(this.getClass(), "MembershipTransactionVO() : " + ve.getMessage());
		}
		this.transactionGroupTypeCode = transGroupTypeCode;
		this.transactionTypeCode = transTypeCode;
		this.membershipGroupJoinDate = memGroupJoinDate;
	}

	public MembershipTransactionVO(Node transNode) throws RemoteException {
		if (transNode == null || !transNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
			throw new SystemException("Failed to create MembershipTransactionVO from XML node. The node is not valid.");
		}
		LogUtil.debug(this.getClass(), "Loading transaction node 1.");
		// Initialise the lists
		this.adjustmentList = new Vector();
		this.transactionFeeList = new Vector();

		NodeList elementList = transNode.getChildNodes();
		Node elementNode = null;
		Node writableNode;
		String nodeName = null;
		String valueText = null;
		for (int i = 0; i < elementList.getLength(); i++) {
			LogUtil.debug(this.getClass(), "Loading transaction node 2.");
			elementNode = elementList.item(i);
			nodeName = elementNode.getNodeName();

			writableNode = XMLHelper.getWritableNode(elementNode);
			valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

			if ("amountPayable".equals(nodeName) && valueText != null) {
				this.amountPayable = Double.parseDouble(valueText);
			}
			// else if ("grossTransactionFee".equals(nodeName) && valueText !=
			// null)
			// {
			// // This is a calculated value.
			// }
			else if ("effectiveDate".equals(nodeName) && valueText != null) {
				try {
					this.effectiveDate = new DateTime(valueText);
				} catch (ParseException ex) {
					throw new SystemException("The effective date '" + valueText + "' is not a valid date : " + ex.getMessage());
				}
			} else if ("endDate".equals(nodeName) && valueText != null) {
				try {
					this.endDate = new DateTime(valueText);
				} catch (ParseException ex) {
					throw new SystemException("The end date '" + valueText + "' is not a valid date : " + ex.getMessage());
				}
			} else if ("gstFee".equals(nodeName) && valueText != null) {
				this.gstFee = Double.parseDouble(valueText);
			} else if ("groupCount".equals(nodeName) && valueText != null) {
				this.groupCount = new Integer(valueText);
			} else if ("membership".equals(nodeName) && writableNode != null) {
				MembershipVO memVO = new MembershipVO(writableNode, MODE_LOADING);
				this.setMembership(memVO);
			}
			// else if ("netTransactionFee".equals(nodeName) && valueText !=
			// null)
			// {
			// // This is a calculated value.
			// }
			else if ("membershipGroupJoinDate".equals(nodeName) && valueText != null) {
				try {
					this.membershipGroupJoinDate = new DateTime(valueText);
				} catch (ParseException ex1) {
					this.membershipGroupJoinDate = null;
				}
			} else if ("newMembership".equals(nodeName) && writableNode != null) {
				MembershipVO newMemVO = new MembershipVO(writableNode, MODE_LOADING);
				this.setNewMembership(newMemVO);
			} else if ("payableItemID".equals(nodeName) && valueText != null) {
				this.payableItemID = new Integer(valueText);
			} else if ("preferredCommenceDate".equals(nodeName) && valueText != null) {
				try {
					this.preferredCommenceDate = new DateTime(valueText);
				} catch (ParseException pe) {
					throw new SystemException("The preferred commence date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("primeAddressee".equals(nodeName) && valueText != null) {
				LogUtil.debug(this.getClass(), "loading pa=" + valueText);
				LogUtil.debug(this.getClass(), "loading pa='" + Boolean.valueOf(valueText).booleanValue());
				this.primeAddressee = Boolean.valueOf(valueText).booleanValue();
				LogUtil.debug(this.getClass(), "loading pa lv=" + this.primeAddressee);
			} else if ("productCode".equals(nodeName) && valueText != null) {
				this.productCode = valueText;
			} else if ("quoteNumber".equals(nodeName) && valueText != null) {
				this.quoteNumber = new Integer(valueText);
			} else if ("rejoinTypeCode".equals(nodeName) && valueText != null) {
				this.rejoinTypeCode = valueText;
			} else if ("salesBranchCode".equals(nodeName) && valueText != null) {
				this.salesBranchCode = valueText;
			} else if ("transactionDate".equals(nodeName) && valueText != null) {
				try {
					this.transactionDate = new DateTime(valueText);
				} catch (ParseException pe) {
					throw new SystemException("The transaction date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("transactionGroupID".equals(nodeName) && valueText != null) {
				this.transactionGroupID = Integer.parseInt(valueText);
			} else if ("transactionGroupTypeCode".equals(nodeName) && valueText != null) {
				this.transactionGroupTypeCode = valueText;
			} else if ("transactionID".equals(nodeName) && valueText != null) {
				this.transactionID = new Integer(valueText);
			} else if ("transactionReasonCode".equals(nodeName) && valueText != null) {
				this.transactionReasonCode = valueText;
			} else if ("transactionReasonType".equals(nodeName) && valueText != null) {
				this.transactionReasonType = valueText;
			} else if ("transactionReference".equals(nodeName) && valueText != null) {
				this.transactionReference = valueText;
			} else if ("transactionTypeCode".equals(nodeName) && valueText != null) {
				this.transactionTypeCode = valueText;
			} else if ("username".equals(nodeName) && valueText != null) {
				this.username = valueText;
			} else if ("undoneDate".equals(nodeName) && valueText != null) {
				try {
					this.undoneDate = new DateTime(valueText);
				} catch (ParseException pe) {
					throw new SystemException("The undone date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("undoneUsername".equals(nodeName) && valueText != null) {
				this.undoneUsername = valueText;
			} else if ("adjustmentList".equals(nodeName)) {
				NodeList selectList = elementNode.getChildNodes();
				if (selectList != null) {
					for (int j = 0; j < selectList.getLength(); j++) {
						Node node = selectList.item(j);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							MembershipTransactionAdjustment memTransAdj = new MembershipTransactionAdjustment(node);
							this.adjustmentList.add(memTransAdj);
						}
					}
				}
			} else if ("feeList".equals(nodeName)) {
				NodeList selectList = elementNode.getChildNodes();
				if (selectList != null) {
					for (int j = 0; j < selectList.getLength(); j++) {
						Node node = selectList.item(j);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							MembershipTransactionFee memTransFee = new MembershipTransactionFee(node);
							this.transactionFeeList.add(memTransFee);
						}
					}
				}
			}
		}

		// Do some validation
		// Must have a group transaction type code
		if (this.transactionGroupTypeCode == null) {
			throw new SystemException("Failed to construct MembershipTransactionVO. Transaction group type code is null.");
		}
		// Must have a transaction type code
		if (this.transactionTypeCode == null) {
			throw new SystemException("Failed to construct MembershipTransactionVO. Transaction type code is null.");
		}
		// Must have a transaction date
		if (this.transactionDate == null) {
			throw new SystemException("Failed to construct MembershipTransactionVO. Transaction date is null.");
		}
		// Must have an old membership (identified by its ID) or a new
		// membership
		if (this.membershipID == null && this.newMembership == null) {
			throw new SystemException("Failed to construct MembershipTransactionVO. Must have either a membership ID or a new membership attached.");
		}

		LogUtil.debug(this.getClass(), "Loaded node=" + this.toString());
	}

	/**
	 * Getter methods **************************
	 * 
	 * @hibernate.id column="transaction_id" generator-class="assigned"
	 * 
	 * @return The transactionID value
	 */

	public Integer getTransactionID() {
		return this.transactionID;
	}

	/**
	 * Get the pending fee id of the transaction
	 * 
	 * @return The pendingFeeID value
	 */
	public String getPendingFeeID() {
		return this.pendingFeeID;
	}

	/**
	 * Gets the transactionGroupID attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="transaction_group_id"
	 * 
	 * @return The transactionGroupID value
	 */
	public int getTransactionGroupID() {
		return this.transactionGroupID;
	}

	/**
	 * Gets the transactionGroupTypeCode attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="transaction_group_type_code"
	 * 
	 * @return The transactionGroupTypeCode value
	 */
	public String getTransactionGroupTypeCode() {
		return this.transactionGroupTypeCode;
	}

	private PayableItemVO payableItemForPA;

	/**
	 * Get the payable item for the prime addressee of a group of transactions or an individual to provide the effective date and end date of a financial transaction.
	 */
	public PayableItemVO getPayableItemForPA() throws RemoteException {
		MethodCounter.addToCounter("MembershipTransactionVO", "getPayableItemForPA");
		LogUtil.debug(this.getClass(), "getPayableItemForPA" + this.transactionID);

		MembershipTransactionVO txVO = null;
		// PayableItemVO payableItem = null;
		MembershipHistory memHist = null;
		boolean foundPayable = false;
		if (this.payableItemForPA == null) {
			Collection txGroupList = this.getTransactionListForGroup();
			if (txGroupList != null && txGroupList.size() > 0) {
				LogUtil.debug(this.getClass(), "tx size" + txGroupList.size());
				Iterator txGrpIt = txGroupList.iterator();
				// for each transaction in the group
				while (txGrpIt.hasNext() && !foundPayable) // only one payable
				// attached to the
				// PAs transaction
				{
					txVO = (MembershipTransactionVO) txGrpIt.next();
					LogUtil.debug(this.getClass(), "getting payableitem for txVO " + txVO.getTransactionID() + "  " + txVO.getPayableItemID());
					// are they the prime addressee at that point in time?
					if (txVO != null && txVO.getPayableItemID() != null) // will
					// work
					// for
					// group
					// or
					// individual
					{
						LogUtil.debug(this.getClass(), "getting payableitem for txVO " + txVO.getTransactionID());
						foundPayable = true;
						payableItemForPA = txVO.getPayableItem(); // may be null
					}
				}
			}
		}
		return payableItemForPA;
	}

	/**
	 * the transaction list
	 */
	private Collection transactionList;

	/**
	 * Get all transactions that are part of the "transaction group".
	 */
	public Collection getTransactionListForGroup() throws RemoteException {
		MethodCounter.addToCounter("MembershipTransactionVO", "getTransactionListForGroup");
		LogUtil.debug(this.getClass(), "*** getTransactionListForGroup start");
		if (this.transactionList == null) {
			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
			Collection txGroupList = null;
			// ArrayList txGroupVOList = new ArrayList();
			// MembershipTransactionVO memTxVO = null;
			try {
				LogUtil.debug(this.getClass(), "*** getTransactionListForGroup transactionGroupID=" + transactionGroupID);
				// must be a group. ie not 0.
				if (this.transactionGroupID > 0) {
					// get all in group except the current one.
					txGroupList = memMgr.findMembershipTransactionByTransactionGroupID(this.getTransactionGroupID());
				} else {
					LogUtil.debug(this.getClass(), "*** getTransactionListForGroup txGroupList " + txGroupList);
					// null if not a group
					if (txGroupList == null) {
						txGroupList = new ArrayList();
						LogUtil.debug(this.getClass(), "*** getTransactionListForGroup adding current tx " + txGroupList);
						txGroupList.add(this);
					}
				}
				LogUtil.debug(this.getClass(), "*** getTransactionListForGroup end ");
				LogUtil.debug(this.getClass(), "*** getTransactionListForGroup end " + txGroupList);

			} catch (Exception fe) {
				throw new RemoteException("Unable to get the other transactions for the group.", fe);
			}
			this.transactionList = txGroupList;
		}
		return this.transactionList;
	}

	/**
	 * get the transaction for the PA
	 * 
	 * @return
	 * @throws RemoteException
	 */
	// public MembershipTransactionVO getTransactionForPA()
	// throws RemoteException
	// {
	// Collection txList = this.getTransactionListForGroup();
	// MembershipTransactionVO paMemTransVO = null;
	// MembershipTransactionVO tmpMemTransVO = null;
	// if (txList != null)
	// {
	// Iterator txListIt = txList.iterator();
	// while (txListIt.hasNext() && paMemTransVO == null)
	// {
	// tmpMemTransVO = (MembershipTransactionVO)txListIt.next();
	// if (tmpMemTransVO.getMembership().isPrimeAddressee())
	// {
	// paMemTransVO = tmpMemTransVO;
	// }
	// }
	// }
	// else
	// {
	// paMemTransVO = this;
	// }
	// return paMemTransVO;
	// }

	/**
	 * Gets the transactionTypeCode attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="transaction_type_code"
	 * 
	 * @return The transactionTypeCode value
	 */
	public String getTransactionTypeCode() {
		return this.transactionTypeCode;
	}

	/**
	 * Gets the transactionType attribute of the MembershipTransactionVO object
	 * 
	 * @return The transactionType value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public MembershipTransactionTypeVO getTransactionType() throws RemoteException {
		if (this.transactionTypeCode != null && this.transactionType == null) {
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			this.transactionType = refMgr.getMembershipTransactionType(this.transactionTypeCode);
			if (this.transactionType == null) {
				throw new RemoteException("Failed to get transaction type '" + this.transactionTypeCode + "'. It does not exist!");
			}
		}
		return this.transactionType;
	}

	/**
	 * Return a <b>copy</b> of the transaction date to prevent inadvertantly updating it.
	 * 
	 * @hibernate.property type="com.ract.util.DateTimeCombined"
	 * @hibernate.column name="transaction_date"
	 * @hibernate.column name="transaction_time"
	 * 
	 * @return The transactionDate value
	 */
	public DateTime getTransactionDate() {
		return this.transactionDate;
		// DateTime dateCopy = this.transactionDate.copy();
		// return dateCopy;
	}

	/**
	 * Gets the membershipID attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="membership_id"
	 * 
	 * @return The membershipID value
	 */
	public Integer getMembershipID() {
		return this.membershipID;
	}

	/**
	 * Get the paid ratio for this transaction.
	 * 
	 * @throws RemoteException
	 * @return BigDecimal
	 */
	public BigDecimal getPaidRatio() throws RemoteException {

		// consider this in light of other changes...

		AmountOutstanding amountOutstanding = this.getUnearnedAmountOutstanding();
		LogUtil.debug(this.getClass(), "amountOutstanding=" + amountOutstanding);
		// nothing paid
		if (amountOutstanding.isFullyUnpaid()) {
			return new BigDecimal(0);
		}
		// fully paid
		else if (amountOutstanding.getAmountOutstanding().doubleValue() == 0) {
			return new BigDecimal(1);
		}
		BigDecimal amountPayable = new BigDecimal(this.getEarnedIncomeAmount());// unearned
		// only?
		LogUtil.debug(this.getClass(), "amountPayable=" + amountPayable);
		BigDecimal amountPaid = amountPayable.subtract(amountOutstanding.getAmountOutstanding());
		LogUtil.debug(this.getClass(), "amountPaid=" + amountPaid);
		BigDecimal paidRatio = amountPaid.divide(amountPayable, 10, BigDecimal.ROUND_HALF_UP);
		LogUtil.debug(this.getClass(), "paidRatio=" + paidRatio);
		return paidRatio;
	}

	/**
	 * Get the total transaction fee discount applied to transaction fees attached to this transaction.
	 * 
	 * @throws RemoteException
	 * @return double
	 */
	public BigDecimal getTransactionFeeDiscountTotal() throws RemoteException {
		Collection transactionFeeList = this.getTransactionFeeList();
		BigDecimal discountTotal = new BigDecimal(0);
		if (transactionFeeList != null) {
			MembershipTransactionFee txFee = null;
			Iterator feeIt = transactionFeeList.iterator();
			while (feeIt.hasNext()) {
				txFee = (MembershipTransactionFee) feeIt.next();
				discountTotal = discountTotal.add(new BigDecimal(txFee.getTotalDiscount()));
				LogUtil.debug(this.getClass(), "discountTotal=" + discountTotal);
			}
		}
		LogUtil.debug(this.getClass(), "discountTotal=" + discountTotal);
		return discountTotal;
	}

	/**
	 * Get the ratio of this membership in the transaction to other membership transactions.
	 * 
	 * @throws RemoteException
	 * @return AmountOutstanding
	 */
	public BigDecimal getTransactionUnearnedRatio() throws RemoteException {
		// int piCounter = 0;
		BigDecimal membershipRatio = new BigDecimal(0);
		BigDecimal transactionTotal = null;
		BigDecimal groupTransactionTotal = null;

		groupTransactionTotal = new BigDecimal(this.getGroupEarnedTotal());
		transactionTotal = new BigDecimal(this.getEarnedIncomeAmount());
		LogUtil.debug(this.getClass(), "groupTransactionTotal" + groupTransactionTotal);
		LogUtil.debug(this.getClass(), "transactionTotal" + transactionTotal);
		if (transactionTotal.doubleValue() == 0 || groupTransactionTotal.doubleValue() == 0) {
			return new BigDecimal(0);
		}
		membershipRatio = transactionTotal.divide(groupTransactionTotal, 10, BigDecimal.ROUND_HALF_UP);
		LogUtil.debug(this.getClass(), "membershipRatio = " + membershipRatio);

		// per trans
		return membershipRatio;
	}

	/**
	 * Actual amount outstanding.
	 * 
	 * @throws RemoteException
	 * @return AmountOutstanding
	 */
	public AmountOutstanding getAmountOutstanding() throws RemoteException {
		AmountOutstanding amountOs = new AmountOutstanding();
		BigDecimal amountOutstanding = null;
		PayableItemVO payableItem = null;
		BigDecimal feeRatio = null;
		// if being removed from a group there is nothing outstanding.
		boolean removeFromGroup = MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(this.getTransactionTypeCode());
		if (!removeFromGroup) {
			payableItem = this.getPayableItemForPA();
			if (payableItem != null) {
				if (!payableItem.isFullyUnpaid()) {
					return this.getUnearnedAmountOutstanding();
				}

				amountOutstanding = new BigDecimal(payableItem.getAmountOutstanding()); // 229.00
				// //99.50
				LogUtil.debug(this.getClass(), "Amount outstanding" + amountOutstanding);
				if (amountOutstanding.doubleValue() > 0) {
					// caused problems where discounts were included in the
					// transaction
					// feeRatio = new
					// BigDecimal(this.getGrossTransactionFee()).divide(new
					// BigDecimal(this.
					// getGroupGrossTransactionFeeTotal()), 10,
					// BigDecimal.ROUND_HALF_UP); //129.50
					feeRatio = new BigDecimal(this.getNetTransactionFee()).divide(new BigDecimal(this.getGroupNetTransactionFeeTotal()), 10, BigDecimal.ROUND_HALF_UP); // 129.50

					LogUtil.debug(this.getClass(), "feeRatio=" + feeRatio);
					amountOutstanding = feeRatio.multiply(amountOutstanding);
					LogUtil.debug(this.getClass(), "amountOutstanding=" + amountOutstanding);
					amountOs.setFullyUnpaid(payableItem.isFullyUnpaid());
					amountOs.setAmountOutstanding(amountOutstanding);
				}
			}
		}
		return amountOs;
	}

	/**
	 * Get the amount outstanding for this transaction. This is the individual's portion.
	 * 
	 * @throws RemoteException
	 * @return AmountOutstanding
	 */
	public AmountOutstanding getUnearnedAmountOutstanding() throws RemoteException {
		AmountOutstanding amountOs = new AmountOutstanding();
		BigDecimal transactionUnearnedRatio = null;
		BigDecimal membershipUnpaidAmount = new BigDecimal(0);
		BigDecimal unearnedAmountOutstanding = null;
		PayableItemVO payableItem = null;
		// if being removed from a group there is nothing outstanding.
		boolean removeFromGroup = MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(this.getTransactionTypeCode());
		if (!removeFromGroup) {
			payableItem = this.getPayableItemForPA();
			if (payableItem != null) {
				if (payableItem.isFullyUnpaid()) {
					unearnedAmountOutstanding = new BigDecimal(this.getGroupEarnedTotal());
				} else {
					// this will typically include a join fee. eg. $229 (inc.
					// $30 join fee) $199
					unearnedAmountOutstanding = new BigDecimal(payableItem.getAmountOutstanding());
				}
				LogUtil.debug(this.getClass(), "Amount outstanding" + unearnedAmountOutstanding);
				if (unearnedAmountOutstanding.doubleValue() > 0) {
					// member ratio will never include a join fee.
					transactionUnearnedRatio = this.getTransactionUnearnedRatio(); // eg.
					// 0.5
					// (if
					// two)
					// -
					// this
					// is
					// for
					// unearned
					// only
					LogUtil.debug(this.getClass(), "getTransactionUnearnedRatio" + transactionUnearnedRatio);
					membershipUnpaidAmount = transactionUnearnedRatio.multiply(unearnedAmountOutstanding);
					LogUtil.debug(this.getClass(), "membershipUnpaidAmount" + membershipUnpaidAmount);
					// set the amount os attribute
					amountOs.setAmountOutstanding(membershipUnpaidAmount);
					// is fully unpaid only that are affected by immediate fees.
					amountOs.setFullyUnpaid(payableItem.isFullyUnpaid());
				}
			}
		}
		// outstanding for remaining should be $99.50 not $114.50
		LogUtil.debug(this.getClass(), "amountOs" + amountOs.formatForDisplay());
		// per trans
		return amountOs;
	}

	/**
	 * Return a reference to the membership associated with the transaction. Initialise the reference if this is the first fetch
	 * 
	 * @return The membership value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public MembershipVO getMembership() throws RemoteException {
		// Only fetch the membership from the database if we are not creating
		// the transaction
		if (!ValueObject.MODE_CREATE.equals(getMode()) && this.membershipID != null && this.membership == null) {
			LogUtil.debug(this.getClass(), "getMembership=" + getMode() + " " + membershipID + " " + this);
			MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
			this.membership = membershipMgrLocal.getMembership(this.membershipID);
		}
		return this.membership;
	}

	/**
	 * Return a reference to the new view of the membership. This will only return a non-null reference while the transaction is being processed.
	 * 
	 * @return The newMembership value
	 */
	public MembershipVO getNewMembership() {
		return this.newMembership;
	}

	/**
	 * Gets the transactionReasonType attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="reason_type"
	 * 
	 * @return The transactionReasonType value
	 */
	public String getTransactionReasonType() {
		return this.transactionReasonType;
	}

	/**
	 * Gets the transactionReasonCode attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="reason_code"
	 * 
	 * @return The transactionReasonCode value
	 */
	public String getTransactionReasonCode() {
		return this.transactionReasonCode;
	}

	/**
	 * Gets the transactionReason attribute of the MembershipTransactionVO object
	 * 
	 * @return The transactionReason value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public ReferenceDataVO getTransactionReason() throws RemoteException {
		ReferenceDataVO transReason = null;
		if (this.transactionReasonCode != null) {
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionTypeCode)) {
				transReason = refMgr.getJoinReason(this.transactionReasonCode);
				if (transReason == null) {
					throw new RemoteException("Failed to get join reason '" + this.transactionReasonCode + "'. It does not exist!");
				}
			}
			// else if
			// (MembershipConstants.TRANSACTION_TYPE_REJOIN.equals(this.transactionTypeCode))
			// {
			// transReason = refMgr.getRejoinReason(this.transactionReasonCode);
			// }
			else {
				// commented out - seems excessive to abort if type isn't
				// "create" ???
				// throw new
				// RemoteException("Unable to match transaction reason to transaction type!");
			}
		}
		return transReason;
	}

	/**
	 * Return a list of distinct fee types used on the membership transaction fees. Returns an empty list if there are no membership transactions.
	 * 
	 * @return The distinctFeeTypeList value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public Collection getDistinctFeeTypeList() throws RemoteException {
		Hashtable distinctFeeTypeList = new Hashtable();
		Collection feeTypeList = getFeeTypeList();
		if (feeTypeList != null) {
			String feeTypeCode = null;
			Iterator listIterator = feeTypeList.iterator();
			while (listIterator.hasNext()) {
				feeTypeCode = (String) listIterator.next();
				if (!distinctFeeTypeList.containsKey(feeTypeCode)) {
					distinctFeeTypeList.put(feeTypeCode, feeTypeCode);
				}
			}
		}
		return distinctFeeTypeList.values();
	}

	/**
	 * Return a list of fee types used on the membership transaction fees. The list may contain duplicates. Returns an empty list if there are no membership transactions.
	 * 
	 * @return The feeTypeList value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public Collection getFeeTypeList() throws RemoteException {
		ArrayList feeTypeList = new ArrayList();
		Collection memTransFeeList = getTransactionFeeList();
		if (memTransFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator listIterator = memTransFeeList.iterator();
			while (listIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) listIterator.next();
				feeTypeList.add(memTransFee.getFeeSpecificationVO().getFeeTypeCode());
			}
		}
		return feeTypeList;
	}

	/**
	 * Return the gross transaction fee. This is defined as the sum of the full transaction fees without subtracting any discounts
	 * 
	 * @return The grossTransactionFee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getGrossTransactionFee() throws RemoteException {
		double grossFee = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			// Loop through the membership transaction fees and sum up
			// the gross fees.
			MembershipTransactionFee memTransFee = null;
			Iterator transactionFeeListIT = feeList.iterator();
			while (transactionFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transactionFeeListIT.next();
				grossFee += memTransFee.getGrossFee();
				// System.out.println("\nMembershipTransactionVO.getGrossTransactionFee"
				// + "\nFee " + memTransFee.getFeeDescription()
				// + " = " + memTransFee.getGrossFee());
			}
		}
		LogUtil.debug(this.getClass(), "grossFee" + grossFee);
		return grossFee;
	}

	/**
	 * Return the net transaction fee. The net transaction fee is defined as the sum of the gross transaction fees less the non discretionary discounts.
	 * 
	 * @return The netTransactionFee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getNetTransactionFee() throws RemoteException {
		double netFee = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			// Loop through the membership transaction fees and sum
			// up the net transaction fees.
			MembershipTransactionFee memTransFee = null;
			Iterator transactionFeeListIT = feeList.iterator();
			while (transactionFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transactionFeeListIT.next();
				netFee += memTransFee.getNetTransactionFee();
			}
		}
		return netFee;
	}

	/**
	 * Return the net transaction fee for membership transaction fees that associate to a fee specification that is of the specified fee type.
	 * 
	 * @param feeTypeCode
	 *            Description of the Parameter
	 * @return The netTransactionFee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getNetTransactionFee(String feeTypeCode) throws RemoteException {
		double netFee = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			// Loop through the membership transaction fees and sum
			// up the net transaction fees.
			MembershipTransactionFee memTransFee = null;
			Iterator transactionFeeListIT = feeList.iterator();
			while (transactionFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transactionFeeListIT.next();
				netFee += memTransFee.getNetTransactionFee(feeTypeCode);
			}
		}
		return netFee;
	}

	/**
	 * Gets the netTransactionFee attribute of the MembershipTransactionVO object
	 * 
	 * @param earnedAccount
	 *            Description of the Parameter
	 * @param unearnedAccount
	 *            Description of the Parameter
	 * @return The netTransactionFee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getNetTransactionFee(MembershipAccountVO earnedAccount, MembershipAccountVO unearnedAccount) throws RemoteException {
		return getNetTransactionFee(earnedAccount, unearnedAccount, true);
	}

	/**
	 * Return the net transaction fee for membership transaction fees that associate to a fee specification that has the specified earned account and unearned account.
	 * 
	 * @param earnedAccount
	 *            Description of the Parameter
	 * @param unearnedAccount
	 *            Description of the Parameter
	 * @param includeOffsetDiscounts
	 *            Description of the Parameter
	 * @return The netTransactionFee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getNetTransactionFee(MembershipAccountVO earnedAccount, MembershipAccountVO unearnedAccount, boolean includeOffsetDiscounts) throws RemoteException {
		double netFee = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			// Loop through the membership transaction fees and sum
			// up the net transaction fees.
			MembershipTransactionFee memTransFee = null;
			Iterator transactionFeeListIT = feeList.iterator();
			while (transactionFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transactionFeeListIT.next();
				netFee += memTransFee.getNetTransactionFee(earnedAccount, unearnedAccount, includeOffsetDiscounts);
			}
		}
		return netFee;
	}

	/**
	 * The amount payable will only be greater than zero if the member is the prime addressee of the membership group.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="transaction_fee"
	 * 
	 * @return The amountPayable value
	 */
	public double getAmountPayable() {
		return this.amountPayable;
	}

	/**
	 * Get the sum of each individuals earned amounts.
	 * 
	 * @throws RemoteException
	 * @return double
	 */
	private double getGroupEarnedTotal() throws RemoteException {
		LogUtil.debug(this.getClass(), "getGroupUnearnedTotal 1");
		Collection transList = this.getTransactionListForGroup();
		double unearnedTotal = 0;
		if (transList != null) {
			// LogUtil.debug(this.getClass(), "getGroupEarnedTotal  tx size " +
			// transList.size());
			MembershipTransactionVO tmpMemTxVO = null;
			Iterator transListIt = transList.iterator();
			while (transListIt.hasNext()) {
				tmpMemTxVO = (MembershipTransactionVO) transListIt.next();
				unearnedTotal += tmpMemTxVO.getEarnedIncomeAmount();
			}
		}
		LogUtil.debug(this.getClass(), "getGroupUnearnedTotal" + unearnedTotal);
		return unearnedTotal;

	}

	private double getGroupGrossTransactionFeeTotal() throws RemoteException {
		LogUtil.debug(this.getClass(), "getGroupGrossTransactionFeeTotal 1");
		Collection transList = this.getTransactionListForGroup();
		double grossTransactionFeeTotal = 0;
		if (transList != null) {
			MembershipTransactionVO tmpMemTxVO = null;
			Iterator transListIt = transList.iterator();
			while (transListIt.hasNext()) {
				tmpMemTxVO = (MembershipTransactionVO) transListIt.next();
				grossTransactionFeeTotal += tmpMemTxVO.getGrossTransactionFee();
			}
		}
		LogUtil.debug(this.getClass(), "getGroupGrossTransactionFeeTotal" + grossTransactionFeeTotal);
		return grossTransactionFeeTotal;
	}

	private double getGroupNetTransactionFeeTotal() throws RemoteException {
		LogUtil.debug(this.getClass(), "getGroupNetTransactionFeeTotal 1");
		Collection transList = this.getTransactionListForGroup();
		double netTransactionFeeTotal = 0;
		if (transList != null) {
			MembershipTransactionVO tmpMemTxVO = null;
			Iterator transListIt = transList.iterator();
			while (transListIt.hasNext()) {
				tmpMemTxVO = (MembershipTransactionVO) transListIt.next();
				netTransactionFeeTotal += tmpMemTxVO.getNetTransactionFee();
			}
		}
		LogUtil.debug(this.getClass(), "getGroupNetTransactionFeeTotal" + netTransactionFeeTotal);
		return netTransactionFeeTotal;
	}

	/**
	 * Return the gst payable for the fully discounted transaction fee. The gst is only applicable to the prime addressee of the group.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="gst_fee"
	 * 
	 * @return The gstFee value
	 */
	public double getGstFee() {
		return this.gstFee;
	}

	/**
	 * Return the total amount of discount given. Sum up the discounts given for each transaction fee.
	 * 
	 * @return double
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getTotalDiscount() throws RemoteException {
		double discountAmount = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeListIT = feeList.iterator();
			while (feeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeListIT.next();
				discountAmount += memTransFee.getTotalDiscount();
			}
		}
		return discountAmount;
	}

	/**
	 * Return the amount of credit discount given. Sum up the credit discounts given for each transaction fee.
	 * 
	 * @return double
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getCreditDiscountApplied() throws RemoteException {
		double discountAmount = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeListIT = feeList.iterator();
			while (feeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeListIT.next();
				discountAmount += memTransFee.getCreditDiscount();
			}
		}
		return discountAmount;
	}

	/**
	 * Return the amount of paid and unearned credit that has been created in this transaction. Sum up the unearned credits given for each transaction fee. There should only be one unearned credit but you never know.
	 * 
	 * @return double
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getUnearnedCreditApplied() throws RemoteException {
		double unearnedCreditAmount = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeListIT = feeList.iterator();
			while (feeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeListIT.next();
				unearnedCreditAmount += memTransFee.getUnearnedCredit();
			}
		}
		return unearnedCreditAmount;
	}

	public double getTransferCreditApplied() throws RemoteException {
		double transferCreditAmount = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeListIT = feeList.iterator();
			while (feeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeListIT.next();
				transferCreditAmount += memTransFee.getTransferCredit();
			}
		}
		return transferCreditAmount;
	}

	/**
	 * Return the total amount of fees that are considered to be earned. If the fee is considered to be earned the sum of the net transaction fees is returned. This is the gross less the non-discretionary discounts.
	 * 
	 * @return The earnedIncomeAmount value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getEarnedIncomeAmount() throws RemoteException {
		double earnedAmount = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			LogUtil.debug(this.getClass(), "feeList" + feeList.size());
			MembershipTransactionFee memTransFee = null;
			Iterator iterator = feeList.iterator();
			while (iterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) iterator.next();

				LogUtil.debug(this.getClass(), this.transactionID + "---->" + memTransFee.getFeeDescription() + "  " + memTransFee.getEarnedIncomeAmount());

				earnedAmount += memTransFee.getEarnedIncomeAmount();
			}
		}
		return earnedAmount;
	}

	/**
	 * Return the total amount of fees that are considered to be unearned. If the fee is considered to be unearned the sum of the net transaction fees is returned. This is the gross less the non-discretionary discounts.
	 * 
	 * @return The unearnedIncomeAmount value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public double getUnearnedIncomeAmount() throws RemoteException {
		double unearnedAmount = 0.0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator iterator = feeList.iterator();
			while (iterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) iterator.next();
				unearnedAmount += memTransFee.getUnearnedIncomeAmount();
			}
		}
		return unearnedAmount;
	}

	public double getValueFeeDiscountTotal() throws RemoteException {
		double valueFeeDiscountTotal = 0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator iterator = feeList.iterator();
			while (iterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) iterator.next();
				valueFeeDiscountTotal += memTransFee.getValueFeeDiscountTotal();
			}
		}
		return valueFeeDiscountTotal;
	}

	public double getValueFeeTotal() throws RemoteException {
		double valueFeeTotal = 0;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			LogUtil.debug(this.getClass(), "feeList=" + feeList);
			MembershipTransactionFee memTransFee = null;
			Iterator iterator = feeList.iterator();
			while (iterator.hasNext()) {
				LogUtil.debug(this.getClass(), "memTransFee=" + memTransFee);
				memTransFee = (MembershipTransactionFee) iterator.next();
				if (memTransFee.isValueFee()) {
					LogUtil.debug(this.getClass(), "memTransFee.isValueFee()=" + memTransFee.isValueFee());
					valueFeeTotal += memTransFee.getGrossFee();
					LogUtil.debug(this.getClass(), "valueFeeTotal=" + valueFeeTotal);
				}
			}
		}
		return valueFeeTotal;
	}

	/**
	 * Return the maximum period of discount given as part of the discounts applied to the transaction fees.
	 * 
	 * @return The discountPeriod value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public Interval getMaxDiscountPeriod() throws RemoteException {
		Interval maxDiscountPeriod = null;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Interval tmpPeriod = null;
			Iterator feeListIterator = feeList.iterator();
			while (feeListIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeListIterator.next();
				tmpPeriod = memTransFee.getMaxDiscountPeriod();
				if (tmpPeriod != null) {
					if (maxDiscountPeriod == null || tmpPeriod.compareTo(maxDiscountPeriod) > 0) {
						maxDiscountPeriod = tmpPeriod;
					}
				}
			}
		}
		return maxDiscountPeriod;
	}

	/**
	 * Return the maximum discretionary discount amount that can be given to the transaction fees.
	 * 
	 */
	public double getMaxDiscretionaryDiscountAmount() throws RemoteException {
		double maxDiscount = 0.0;
		// Sum up the net amount of the fees applied to the transaction.
		// This is the gross fee minus the fee discounts.
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeIterator = feeList.iterator();
			while (feeIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeIterator.next();
				maxDiscount += memTransFee.getNetTransactionFee();
			}
		}
		return maxDiscount;
	}

	/**
	 * Return the maximum discretionary discount amount that can be given to the transaction fees for the specified fee type.
	 * 
	 */
	public double getMaxDiscretionaryDiscountAmount(String feeTypeCode) throws RemoteException {
		double maxDiscount = 0.0;
		// Sum up the net amount of the fees applied to the transaction
		// for the specific fee type.
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeIterator = feeList.iterator();
			while (feeIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeIterator.next();
				if (memTransFee.getFeeSpecificationVO().getFeeTypeCode().equals(feeTypeCode)) {
					maxDiscount += memTransFee.getNetTransactionFee();
				}
			}
		}
		return maxDiscount;
	}

	/**
	 * Gets the preferred commence date specified for the membership. If no specific preferred commence date has been set then returns the transaction date.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="preferred_commence_date"
	 * 
	 * @return The preferredCommenceDate value
	 */
	public DateTime getPreferredCommenceDate() {
		DateTime prefComDate = this.preferredCommenceDate;
		if (prefComDate == null) {
			prefComDate = this.transactionDate;
		}

		return prefComDate;
	}

	/**
	 * Gets the rejoinTypeCode attribute of the MembershipTransactionVO object
	 * 
	 * @return The rejoinTypeCode value
	 */
	public String getRejoinTypeCode() {
		return this.rejoinTypeCode;
	}

	/**
	 * Gets the salesBranchCode attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="sales_branch"
	 * 
	 * @return The salesBranchCode value
	 */
	public String getSalesBranchCode() {
		return this.salesBranchCode;
	}

	/**
	 * Return the sales branch value object for the value object code. Initialise the reference if this is the first call. Return null if the sales branch code has not been set. Returns null if the sales branch code could not be found.
	 * 
	 * @return The salesBranch value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public SalesBranchVO getSalesBranch() throws RemoteException {
		if (this.salesBranchVO == null && this.salesBranchCode != null) {
			CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
			this.salesBranchVO = commonMgr.getSalesBranch(this.salesBranchCode);
			if (this.salesBranchVO == null) {
				throw new RemoteException("Failed to get sales branch '" + this.salesBranchCode + "'. It does not exist!");
			}
		}
		return this.salesBranchVO;
	}

	/**
	 * Gets the username attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="username"
	 * 
	 * @return The username value
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="membership_xml"
	 * 
	 * @return String
	 */
	public String getMembershipXML() {
		return this.membershipXML;
	}

	/**
	 * Gets the membershipHistoryFilename attribute of the MembershipTransactionVO object
	 * 
	 * @return The membershipHistoryFilename value
	 */
	public String xxxgetMembershipHistoryFilename() {
		return this.membershipHistoryFilename;
	}

	/**
	 * return null if no history rather than
	 * 
	 * @return The membershipHistory value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public MembershipHistory getMembershipHistory() throws RemoteException {
		if (this.membershipHistory == null) {
			if (this.membershipXML == null) {
				LogUtil.warn(this.getClass(), "Unable to get membership history on transaction " + this.transactionID + ". The membership xml document does not exist!");
				return null;
			}

			// Now parse the file into an XML document object model.
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setValidating(false);
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = builder.parse(new InputSource(new StringReader(this.membershipXML)));
				// Should only be one membership node in the document.
				Node node = document.getFirstChild();
				LogUtil.debug(this.getClass(), "reading" + this.membershipXML);
				if (node != null && node.getNodeName().equals(MembershipVO.WRITABLE_CLASSNAME)) {
					LogUtil.debug(this.getClass(), "reading1");
					this.membershipHistory = new MembershipVO(node, MODE_HISTORY);
					LogUtil.debug(this.getClass(), "reading2");
				} else {
					throw new SystemException("The membership history xml document is not valid. It does not contain a membership node.");
				}
			} catch (ParserConfigurationException pce) {
				throw new RemoteException(pce.getMessage(), pce);
			} catch (IOException ioe) {
				throw new RemoteException(ioe.getMessage(), ioe);
			} catch (SAXException se) {
				throw new SystemException("Failed to parse membership history xml document.", se);
			}
		}
		return this.membershipHistory;
	}

	/**
	 * Return a copy of the list of membership transaction fees as MembershipTransactionFee.
	 * 
	 * Returns a null list if no fees have been added and the transaction ID has not been set.
	 * 
	 * Returns an empty list if no fees have been added and the transaction ID has been set but there are no fees attached. Fetches the fee list from the database if no fees have been added, the transaction ID has been set and the fees have not already been fetched.
	 * 
	 * @return The transactionFeeList value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public Collection getTransactionFeeList() throws RemoteException {
		ArrayList feeList = null;

		LogUtil.debug(this.getClass(), "mode " + this.getMode());
		LogUtil.debug(this.getClass(), "transactionFeeList " + this.transactionFeeList);
		LogUtil.debug(this.getClass(), "transactionID " + this.transactionID);

		// Only fetch the fee list from the database if we are not creating the
		// transaction.
		if (!ValueObject.MODE_CREATE.equals(this.getMode()) && this.transactionFeeList == null && this.transactionID != null) {
			MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
			this.transactionFeeList = memTransMgr.getMembershipTransactionFeeList(this.transactionID);
		}
		if (this.transactionFeeList != null) {
			feeList = new ArrayList(this.transactionFeeList);
		}
		/*
		 * if(feeList!=null && feeList.size() >0) { System.out.println("MembershipTransactonVO.getTransactionFeeList"). for(int xx = 0;xx<feeList.size();xx++) { MembershipTransactionFee memFee = (MembershipTransactionFee)feeList.get(xx); System.out.println(this.transactionID + " Fee " + memFee.getGrossFee() + "  " + memFee.getFeeDescription()); ArrayList dl = memFee.getDiscountList(); if(dl!=null) { for(int yy = 0; yy<dl.size();yy++) { MembershipTransactionDiscount disc = (MembershipTransactionDiscount)dl.get(yy); System.out.println("     discount " + disc.getDiscountAmount() + "  " + disc.getDiscountCode()); } } Collection ad = memFee.getAllowableDiscountList(); if(ad!=null) { Iterator id = ad.iterator(); while(id.hasNext()) { DiscountTypeVO md = (DiscountTypeVO)id.next(); System.out.println("Allowable Discount = " + md.getDiscountTypeCode()); } } } }
		 */

		return feeList;
	}

	/**
	 * Return the membership transaction fee that corresponds to the fee specification ID. Return null if a corresponding transaction fee could not be found.
	 * 
	 * @param feeSpecID
	 *            Description of the Parameter
	 * @return The transactionFee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public MembershipTransactionFee getTransactionFee(Integer feeSpecID) throws RemoteException {
		MembershipTransactionFee memTransFee = null;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee tmp = null;
			Iterator listIterator = feeList.iterator();
			while (listIterator.hasNext() && memTransFee == null) {
				tmp = (MembershipTransactionFee) listIterator.next();
				if (tmp.getFeeSpecificationID().equals(feeSpecID)) {
					memTransFee = tmp;
				}
			}
		}
		return memTransFee;
	}

	/**
	 * Return the membership transaction fees, as membershipTransactionFee, for the fees that apply to the product selected for the membership. Returns an empty list if there are no fees.
	 * 
	 * @return The transactionFeeListForProduct value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public Collection getTransactionFeeListForProduct() throws RemoteException {
		ArrayList productFeeList = new ArrayList();
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			// Determine the product code. If we are processing a transaction
			// then get it from the new membership. If we are viewing a
			// completed
			// transaction then get it from the current membership. Use the
			// getMembership()
			// method to populate the current membership attribute.
			String prodCode = null;
			if (this.newMembership != null) {
				prodCode = this.newMembership.getProductCode();
			} else {
				prodCode = getMembership().getProductCode();
			}

			if (prodCode != null) {
				MembershipTransactionFee memTransFee = null;
				Iterator transFeeListIT = feeList.iterator();
				while (transFeeListIT.hasNext()) {
					memTransFee = (MembershipTransactionFee) transFeeListIT.next();
					if (prodCode.equals(memTransFee.getFeeSpecificationVO().getProductCode())) {
						productFeeList.add(memTransFee);
					}
				}
			}
		}
		return productFeeList;
	}

	/**
	 * Return the membership transaction fees, as membershipTransactionFee, for the fees that apply to the specified product option. Returns an empty list if there are no fees.
	 * 
	 * @param prodOptionCode
	 *            Description of the Parameter
	 * @return The transactionFeeListForProductOption value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public Collection getTransactionFeeListForProductOption(String prodOptionCode) throws RemoteException {
		ArrayList productOptionFeeList = new ArrayList();
		Collection feeList = getTransactionFeeList();
		if (feeList != null && prodOptionCode != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator transFeeListIT = feeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				if (prodOptionCode.equals(memTransFee.getFeeSpecificationVO().getProductBenefitCode())) {
					productOptionFeeList.add(memTransFee);
				}
			}
		}
		return productOptionFeeList;
	}

	public ArrayList getAdjustmentList() throws RemoteException {
		initialiseAdjustmentList();

		// Return a copy of the list as the list cannot be maintained outside of
		// this class.
		ArrayList returnList = null;
		if (this.adjustmentList != null) {
			returnList = new ArrayList(this.adjustmentList);
		} else {
			returnList = new ArrayList();
		}
		return returnList;
	}

	public BigDecimal getAdjustmentTotal() throws RemoteException {
		BigDecimal adjustmentTotal = new BigDecimal(0.0);
		Collection adjustList = getAdjustmentList();
		if (adjustList != null) {
			Iterator adjustmentListIterator = adjustList.iterator();
			MembershipTransactionAdjustment adjustment = null;
			while (adjustmentListIterator.hasNext()) {
				adjustment = (MembershipTransactionAdjustment) adjustmentListIterator.next();
				adjustmentTotal = adjustmentTotal.add(adjustment.getSignedAdjustmentAmount());
				LogUtil.debug(this.getClass(), "adjustment total " + adjustmentTotal);
			}
		}
		return adjustmentTotal;
	}

	public BigDecimal getNonDiscretionaryAdjustmentTotal() throws RemoteException {
		BigDecimal adjustmentTotal = new BigDecimal(0.0);
		Collection adjustList = getAdjustmentList();
		if (adjustList != null) {
			Iterator adjustmentListIterator = adjustList.iterator();
			MembershipTransactionAdjustment adjustment = null;
			while (adjustmentListIterator.hasNext()) {
				adjustment = (MembershipTransactionAdjustment) adjustmentListIterator.next();
				if (!AdjustmentTypeVO.DISCRETIONARY_DISCOUNT.equals(adjustment.getAdjustmentTypeCode())) {
					adjustmentTotal = adjustmentTotal.add(adjustment.getSignedAdjustmentAmount());
				}
			}
		}
		return adjustmentTotal;
	}

	private void initialiseAdjustmentList() throws RemoteException {
		// Only fetch the adjustment list from the database if we are not
		// creating the transaction.
		if (!ValueObject.MODE_CREATE.equals(getMode()) && this.adjustmentList == null && this.transactionID != null) {
			MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
			this.adjustmentList = memTransMgr.getMembershipTransactionAdjustmentList(this.transactionID);
		}
	}

	/**
	 * Return the payableItemID recorded with the transaction. Return null if the transaction does not relate to a payable item.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="payable_item_id"
	 * 
	 * @return The payableItemID value
	 */
	public Integer getPayableItemID() {
		return this.payableItemID;
	}

	/**
	 * Return the payable item associated with the transaction. The payable item will not be retrieved and a null will be returned if no payable item is associated or the transaction is in create mode. Throws an exception if the payable item could not be retrieved. This method should only be called once the transaction has been committed and the payable item has been set to the payment manager and committed.
	 * 
	 * @return The payableItem value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public PayableItemVO getPayableItem() throws RemoteException {
		if (this.payableItemID != null && !ValueObject.MODE_CREATE.equals(this.getMode()) && this.payableItemVO == null) {
			PaymentMgr payMgr = PaymentEJBHelper.getPaymentMgr();
			try {
				this.payableItemVO = payMgr.getPayableItem(this.payableItemID);
			} catch (PaymentException ex) {
				throw new RemoteException("Unable to get payable item.", ex);
			}
		}
		return this.payableItemVO;
	}

	/**
	 * Gets the undoneDate attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="undone_date"
	 * 
	 * @return The undoneDate value
	 */
	public DateTime getUndoneDate() {
		return this.undoneDate;
	}

	/**
	 * Gets the undoneUsername attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="undone_username"
	 * 
	 * @return The undoneUsername value
	 */
	public String getUndoneUsername() {
		return this.undoneUsername;
	}

	/**
	 * Returns true if one of the membership transaction fee for the specified fee type has the specified discount type attached to it.
	 * 
	 * @param feeTypeCode
	 *            Description of the Parameter
	 * @param discCode
	 *            Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean hasDiscountType(String feeTypeCode, String discCode) throws RemoteException {
		boolean found = false;
		Collection feeList = getTransactionFeeList();
		if (feeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator feeListIterator = feeList.iterator();
			while (!found && feeListIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeListIterator.next();
				if (memTransFee.getFeeSpecificationVO().getFeeTypeCode().equals(feeTypeCode)) {
					found = memTransFee.hasDiscountType(discCode);
				}
			}
		}
		return found;
	}

	/**
	 * Return true if at least one of the fees attached to the transaction has Offset Discounts applied. Offset Discounts are discounts that provide an alternative income source to bank or direct debit. They are identified by a non-null discount account and include discretionary discounts.
	 * 
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean hasOffsetDiscountsApplied() throws RemoteException {
		boolean hasOffsetDiscount = false;
		Collection txFeeList = getTransactionFeeList();
		if (txFeeList != null) {
			MembershipTransactionFee memTxFee = null;
			Iterator txFeeListIterator = txFeeList.iterator();
			while (!hasOffsetDiscount && txFeeListIterator.hasNext()) {
				memTxFee = (MembershipTransactionFee) txFeeListIterator.next();
				Collection discountList = memTxFee.getDiscountList();
				if (discountList != null) {
					Iterator discountListIterator = discountList.iterator();
					while (!hasOffsetDiscount && discountListIterator.hasNext()) {
						MembershipTransactionDiscount discount = (MembershipTransactionDiscount) discountListIterator.next();
						if (discount.getDiscountTypeVO().getDiscountAccount() != null) {
							return true;
						}
					}
				}
			}
		}
		return hasOffsetDiscount;
	}

	/**
	 * Return true if at least one of the fees attached to the transaction has Credit Discounts applied. Credit Discounts are discounts that draw some or all the fee from the suspense account.
	 * 
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean hasCreditDiscountsApplied() throws RemoteException {
		boolean hasCreditDiscount = false;
		Collection txFeeList = getTransactionFeeList();
		if (txFeeList != null) {
			MembershipTransactionFee memTxFee = null;
			Iterator txFeeListIterator = txFeeList.iterator();
			while (!hasCreditDiscount && txFeeListIterator.hasNext()) {
				memTxFee = (MembershipTransactionFee) txFeeListIterator.next();
				Collection discountList = memTxFee.getDiscountList();
				if (discountList != null) {
					Iterator discountListIterator = discountList.iterator();
					while (!hasCreditDiscount && discountListIterator.hasNext()) {
						MembershipTransactionDiscount discount = (MembershipTransactionDiscount) discountListIterator.next();
						if (discount.getDiscountTypeVO().getDiscountTypeCode().equals(DiscountTypeVO.TYPE_CREDIT)) {
							hasCreditDiscount = true;
						}
					}
				}
			}
		}
		return hasCreditDiscount;
	}

	/**
	 * Return true if at least one of the fees attached to the transaction has an unearned credit applied.
	 * 
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean hasUnearnedDiscount() throws RemoteException {
		boolean hasUnearnedCredit = false;
		Collection txFeeList = getTransactionFeeList();
		if (txFeeList != null) {
			MembershipTransactionFee memTxFee = null;
			Iterator txFeeListIterator = txFeeList.iterator();
			while (!hasUnearnedCredit && txFeeListIterator.hasNext()) {
				memTxFee = (MembershipTransactionFee) txFeeListIterator.next();
				hasUnearnedCredit = memTxFee.hasDiscountType(DiscountTypeVO.TYPE_UNEARNED_CREDIT);
			}
		}
		return hasUnearnedCredit;
	}

	/**
	 * Should correspond to discounts applied through createAutomaticDiscounts in transaction group. Consider a flag on the discount type.
	 * 
	 * @throws RemoteException
	 * @return boolean
	 */
	public boolean hasAutomaticDiscountApplied() throws RemoteException {
		boolean hasAutomaticDiscount = false;
		Collection txFeeList = getTransactionFeeList();
		if (txFeeList != null) {
			MembershipTransactionFee memTxFee = null;
			Iterator txFeeListIterator = txFeeList.iterator();
			while (!hasAutomaticDiscount && txFeeListIterator.hasNext()) {
				memTxFee = (MembershipTransactionFee) txFeeListIterator.next();
				Collection discountList = memTxFee.getDiscountList();
				if (discountList != null) {
					Iterator discountListIterator = discountList.iterator();
					while (!hasAutomaticDiscount && discountListIterator.hasNext()) {
						MembershipTransactionDiscount discount = (MembershipTransactionDiscount) discountListIterator.next();
						if (!discount.getDiscountTypeVO().getDiscountTypeCode().equals(DiscountTypeVO.TYPE_CREDIT) && !discount.getDiscountTypeVO().isRecurringDiscount() && discount.getDiscountTypeVO().isAutomaticDiscount()) {
							hasAutomaticDiscount = true;
						}
					}
				}
			}
		}
		return hasAutomaticDiscount;

	}

	/**
	 * Return true if at least one of the fees attached to the transaction has a transfer credit discount.
	 * 
	 * @throws RemoteException
	 * @return boolean
	 */
	public boolean hasTransferDiscount() throws RemoteException {
		boolean hasTransferDiscount = false;
		Collection txFeeList = getTransactionFeeList();
		if (txFeeList != null) {
			MembershipTransactionFee memTxFee = null;
			Iterator txFeeListIterator = txFeeList.iterator();
			while (!hasTransferDiscount && txFeeListIterator.hasNext()) {
				memTxFee = (MembershipTransactionFee) txFeeListIterator.next();
				hasTransferDiscount = memTxFee.hasDiscountType(DiscountTypeVO.TYPE_TRANSFER_CREDIT);
			}
		}
		return hasTransferDiscount;
	}

	/**
	 * Does the transaction have a credit amount attached?
	 * 
	 * @return boolean
	 */
	// public boolean hasTransferCredit()
	// {
	// MembershipVO newMemVO = this.getNewMembership();
	// AffiliatedClubCredit affiliatedClubCredit =
	// (AffiliatedClubCredit)newMemVO.getTransactionCredit(
	// TransactionCredit.CREDIT_AFFILIATED_CLUB);
	//
	// if(affiliatedClubCredit != null &&
	// affiliatedClubCredit.getTransactionCreditAmount().doubleValue() > 0)
	// {
	// LogUtil.debug(this.getClass(),"hasTransferCredit");
	// return true;
	// }
	// else
	// {
	// return false;
	// }
	// }

	/**
	 * Does the transaction have complimentary credit attached?
	 * 
	 * @return
	 */
	public boolean hasComplimentaryCredit() {
		MembershipVO newMemVO = getNewMembership();
		ComplimentaryCredit compCredit = (ComplimentaryCredit) newMemVO.getTransactionCredit(TransactionCredit.CREDIT_COMPLIMENTARY);
		// LogUtil.debug(this.getClass(), "compCredit: " + compCredit);
		return (compCredit != null ? true : false);
	}

	/**
	 * Description of the Method
	 * 
	 * @param amount
	 *            Description of the Parameter
	 * @return Description of the Return Value
	 */
	public boolean hasFeeWithAmount(double amount) {
		boolean found = false;
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee;
			double fee = 0.0;
			Iterator listIterator = this.transactionFeeList.iterator();
			while (!found && listIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) listIterator.next();
				fee = memTransFee.getFeePerMember();
				found = (fee == amount);
			}
		}
		return found;
	}

	/**
	 * Return true if at least one of the fees attached to the transaction is for a fee type that allows non-discretionary discounts.
	 * 
	 * @param includeAutomaticDiscounts
	 *            Description of the Parameter
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean canHaveDiscounts(boolean includeAutomaticDiscounts) throws RemoteException {
		boolean found = false;
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee;
			Iterator listIterator = this.transactionFeeList.iterator();
			while (!found && listIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) listIterator.next();
				found = memTransFee.canHaveDiscounts(includeAutomaticDiscounts);
			}
		}
		return found;
	}

	/**
	 * Return true if there is a (non-zero) fee that can be non discretionarily discounted.
	 * 
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean canBeDiscounted() throws RemoteException {
		boolean discountableFeeFound = false;
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee;
			Iterator transFeeListIterator = this.transactionFeeList.iterator();
			while (!discountableFeeFound && transFeeListIterator.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIterator.next();
				discountableFeeFound = memTransFee.canBeDiscounted();
			}
		}
		return discountableFeeFound;
	}

	/**
	 * Return true if a history file name has been specified and the history file is available to be read by this instance.
	 * 
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean isHistoryAvailable() throws RemoteException {
		return (this.membershipXML != null && this.membershipXML.trim().length() > 0);
	}

	/**
	 * Return true if the member has been flagged as the prime addressee of the membership group. This attribute is not saved as part of the membership transaction. It only has meaning while the membership transaction is being created for the first time. Use the MembershipVO.isPrimeAddressee() method to determine if member has been flagged as the prime addressee of the membership group once the transaction has been saved.
	 * 
	 * @return The primeAddressee value
	 */
	public boolean isPrimeAddressee() {
		return this.primeAddressee;
	}

	/**
	 * Return true if the transaction is creating a new membership. This is the case when a new membership is attached to the transaction and there is no current membership.
	 * 
	 * @return The creatingMembership value
	 */
	public boolean isCreatingMembership() {
		return (this.newMembership != null && this.membership == null) ? true : false;
	}

	/**
	 * Return true if the membership is being updated. This is the case when a current membership and a new membership are attached to the transaction.
	 * 
	 * @return The updatingMembership value
	 */
	public boolean isUpdatingMembership() {
		if (this.newMembership != null && this.membership != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return true if the transaction has been undone.
	 * 
	 * @return The undone value
	 */
	public boolean isUndone() {
		return this.undoneDate != null;
	}

	/**
	 * Returns true if the product has changed between the old version of the membership and the new version of the membership.
	 * 
	 * @return The productChanged value
	 */
	public boolean isProductChanged() {
		boolean changed = false;

		if (this.membership == null && this.newMembership != null) {
			changed = true;
		}

		if (this.membership != null && this.newMembership != null && !this.membership.getProductCode().equals(this.newMembership.getProductCode())) {
			changed = true;
		}

		return changed;
	}

	/**
	 * Is the transaction active as at the specified date.
	 * 
	 * @param asAtDate
	 *            DateTime
	 * @return boolean
	 */
	public boolean isActive(DateTime asAtDate) {
		boolean active = false;
		if (this.getEndDate().onOrAfterDay(asAtDate)) {
			active = true;
		}
		return active;
	}

	/**
	 * Does the transaction type supersede existing transactions? The transaction type by itself does not indicate whether or not it is a superseding transaction. It relies on the payment method of the transaction.
	 */
	public boolean isSuperseding(String paymentMethod) {
		LogUtil.debug(this.getClass(), "isSuperseding " + this.getTransactionTypeCode() + " " + this.getTransactionGroupTypeCode());
		boolean directDebit = PaymentMethod.DIRECT_DEBIT.equals(paymentMethod);// false
		// if
		// null
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(this.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.getTransactionTypeCode()) // upgrade
				// downgrade
				// only
				// determined
				// in
				// comparison
				// to
				// subsequent
				// transaction
				|| MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.getTransactionGroupTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(this.getTransactionTypeCode())
				// ||
				// (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(this.getTransactionTypeCode())
				// && directDebit) //never superseding as even dd must refer to
				// previous transaction
				|| (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(this.getTransactionGroupTypeCode())
				/* && directDebit */) || (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(this.getTransactionGroupTypeCode())
				/* && directDebit */) // not paid immediate amount
				|| MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.getTransactionTypeCode())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Product attribute
	 */
	private ProductVO product;

	/**
	 * The end of the effective transaction range.
	 */
	protected DateTime endDate;

	/**
	 * Get the product for the transaction.
	 */
	public synchronized ProductVO getProduct() throws RemoteException {
		if (this.productCode != null && this.product == null && this.isNotHistory()) {
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			this.product = refMgr.getProduct(this.productCode);
			if (this.product == null) {
				throw new SystemException("Failed to get membership productVO '" + this.productCode + "'. It does not exist!");
			}
		}
		return this.product;
	}

	/**
	 * Get the ratio of total membership value
	 * 
	 * @return BigDecimal
	 */
	public BigDecimal getUnearnedRatio(DateTime asAtDate) {
		LogUtil.debug(this.getClass(), this.toString());
		// if the transaction has been superseded this may not be correct. eg
		// upgrade as at a date effectively
		// supersedes the previous transaction.
		return MembershipHelper.getUnearnedRatio(this.getEffectiveRange(asAtDate).getTotalDays(), this.getEffectiveRange().getTotalDays());
	}

	/**
	 * Get the maximum membership period to determine if the membership was shortened
	 * 
	 * @return int
	 */
	public Interval getMaximumPotentialRange() {
		Interval membershipTerm = null;
		// Get the default term of membership from system parameters
		try {
			// String defaultMembershipTermText =
			// CommonEJBHelper.getCommonMgr().
			// getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,
			// SystemParameterVO.DEFAULT_MEMBERSHIP_TERM);
			// membershipTerm = new Interval(defaultMembershipTermText);
			MembershipVO memVO = this.getMembership();
			membershipTerm = memVO.getMembershipTerm();
		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), "The default membership term specification from system parameters is not valid. : " + e.getMessage());
		}
		/*
		 * Interval maximumPotential = null; DateTime maxExpiryDate = this.getEffectiveDate().add(membershipTerm); try { maximumPotential = new Interval(this.getEffectiveDate(),maxExpiryDate); } catch(Exception ex) { LogUtil.fatal(this.getClass(),ex); } return maximumPotential;
		 */
		return membershipTerm;
	}

	/**
	 * Get the effective range of the transaction.
	 * 
	 * @return Interval
	 */
	public Interval getEffectiveRange() {
		return this.getEffectiveRange(this.getEffectiveDate());
	}

	/**
	 * Get the effective range of a transaction as at a date
	 * 
	 * @param asAtDate
	 *            DateTime
	 * @return Interval
	 */
	public Interval getEffectiveRange(DateTime asAtDate) {
		Interval effectiveRange = null;
		try {
			// if as at date is before the effective date then default it.
			if (asAtDate.onOrBeforeDay(this.getEffectiveDate())) {
				asAtDate = this.effectiveDate;
			}

			effectiveRange = new Interval(DateUtil.clearTime(asAtDate), DateUtil.clearTime(this.getEndDate()));
		} catch (Exception ex) {
			LogUtil.fatal(this.getClass(), "Unable to get effective range for transaction '" + this.transactionID + "'" + ex.getMessage());
		}
		return effectiveRange;
	}

	/**
	 * Return true if the product on an existing membership has been changed to a lower risk product. If the membership is being created this method will return false. This method only returns a meaningful value while the transaction is being processed. Once the transaction has been saved this method should not be called because we no longer have the old and new versions of the membership to compare.
	 * 
	 * @return The lowerRiskProductChange value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean isLowerRiskProductChange() throws RemoteException {
		boolean lowerRiskProduct = false;
		LogUtil.debug(this.getClass(), "p new" + this.newMembership);
		LogUtil.debug(this.getClass(), "p new" + this.newMembership.getProduct());
		LogUtil.debug(this.getClass(), "p old" + this.membership.getProduct());
		if (this.membership != null && this.newMembership != null) {
			lowerRiskProduct = this.newMembership.getProduct().compareTo(this.membership.getProduct()) > 0;
		}
		return lowerRiskProduct;
	}

	public static final String TYPE_UNEARNED = "unearned";
	public static final String TYPE_EARNED = "earned";

	/**
	 * Get account for required type from the attached fees. Assumes only one unearned fee.
	 * 
	 * @throws RemoteException
	 * @return boolean
	 */
	public Account getAccount(String accountType) throws RemoteException {
		FeeTypeVO feeType = null;
		Account account = null;
		boolean hasUnearnedAmount = false;
		Collection transactionFeeList = this.getTransactionFeeList();
		if (transactionFeeList != null) {
			MembershipTransactionFee mtFee = null;
			Iterator feeIt = transactionFeeList.iterator();
			while (feeIt.hasNext() && !hasUnearnedAmount) {
				mtFee = (MembershipTransactionFee) feeIt.next();
				LogUtil.debug(this.getClass(), "mtFee.getEarnedIncomeAmount() " + mtFee.getEarnedIncomeAmount());
				if (mtFee.getEarnedIncomeAmount() > 0 && !hasUnearnedAmount) {
					hasUnearnedAmount = true;

					// get the fee type to get the account
					feeType = mtFee.getFeeSpecificationVO().getFeeType();

					LogUtil.debug(this.getClass(), "feeType" + feeType.getDescription());

					if (accountType.equals(this.TYPE_UNEARNED)) {
						// get the unearned account
						account = feeType.getUnearnedAccount();
					} else {
						// get the earned account
						account = feeType.getEarnedAccount();
					}
				}
			}
		}
		try {
			LogUtil.debug(this.getClass(), "accountType=" + accountType + ", account=" + (account == null ? "empty" : account.toString()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return account;

	}

	/**
	 * Return true if the transaction caused a change to the membership that requires the CARES system to be notified. Notifiable changes include: Creating new memberships Rejoining a membership Renewing a membership Changing the base product on a membership Cancelling a membership Putting a membership on hold.
	 * 
	 * @return The notifyableUpdate value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public boolean isNotifiableUpdate() throws RemoteException {
		boolean notifyUpdate = false;

		// Notify the update if doing a create, rejoin, renew, cancel otr hold
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(this.transactionTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(this.transactionTypeCode)) {
			notifyUpdate = true;
		}

		// Notify the update if the base product has changed.
		MembershipVO oldMemVO = this.getMembership();
		MembershipVO newMemVO = this.getNewMembership();
		if (oldMemVO != null && newMemVO != null && !oldMemVO.getProductCode().equals(newMemVO.getProductCode())) {
			notifyUpdate = true;
		}

		return notifyUpdate;
	}

	/**
	 * Returns a string representation of the data in the transaction. Each data attribute is separated by a comma.
	 * 
	 * @return Description of the Return Value
	 */
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("\n----------- MembershipTransactionVO -----------");
		str.append("\n transactionID=" + this.transactionID);
		str.append("\n membershipID=" + this.membershipID);
		str.append("\n transactionTypeCode=" + this.transactionTypeCode);
		str.append("\n transactionGroupTypeCode=" + this.transactionGroupTypeCode);
		str.append("\n transactionGroupID=" + this.transactionGroupID);
		str.append("\n productCode=" + this.productCode);
		str.append("\n transactionDate=" + this.transactionDate);// .formatLongDate());
		str.append("\n username=" + this.username);
		str.append("\n salesBranchCode=" + this.salesBranchCode);
		str.append("\n transactionGroupID=" + this.transactionGroupID);
		str.append("\n amountPayable=" + this.amountPayable);
		str.append("\n gstFee=" + this.gstFee);
		str.append("\n preferredCommenceDate=" + this.preferredCommenceDate);
		str.append("\n membershipHistoryFilename=" + this.membershipHistoryFilename);
		str.append("\n transactionReasonType=" + this.transactionReasonType);
		str.append("\n transactionReasonCode=" + this.transactionReasonCode);
		str.append("\n payableItemID=" + this.payableItemID);
		str.append("\n quoteNumber=" + this.quoteNumber);
		str.append("\n payableItemID=" + this.payableItemID);
		str.append("\n effectiveDate=" + this.effectiveDate);
		str.append("\n endDate=" + this.endDate);
		str.append("\n isPrimeaddressee=" + this.primeAddressee);
		str.append("\n membership=" + this.membership);
		str.append("\n newMembership=" + this.newMembership);
		str.append("\n----------- ------------------------ -----------");

		return str.toString();
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="quote_number"
	 * @return Integer
	 */
	public Integer getQuoteNumber() {
		return this.quoteNumber;
	}

	public void setQuoteNumber(Integer quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public MembershipQuote getAttachedQuote() throws RemoteException {
		if (this.attachedQuote == null && this.quoteNumber != null) {
			this.attachedQuote = MembershipEJBHelper.getMembershipMgr().getMembershipQuote(this.quoteNumber);
		}
		return this.attachedQuote;
	}

	/**
	 * Attach a quote to this transaction. A quote can only be attached while creating the transaction
	 */
	public void setAttachQuote(MembershipQuote q) throws ValidationException, RemoteException {
		checkReadOnly();
		// if quote is null then relax other rules as we are just removing quote
		// references
		this.attachedQuote = q;
		if (this.attachedQuote != null) {
			if (!(ValueObject.MODE_CREATE.equals(getMode()) || ValueObject.MODE_LOADING.equals(getMode()))) {
				throw new SystemException("A quote may only be attached to a transaction when the transaction is created.  Current mode is " + getMode());
			}
			this.quoteNumber = this.attachedQuote.getQuoteNumber();
		} else {
			this.quoteNumber = null;
		}
	}

	/**
	 * Setter methods *************************
	 * 
	 * @param transID
	 *            The new transactionID value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */

	public void setTransactionID(Integer transID) throws ValidationException, RemoteException {
		checkReadOnly();
		if (transID == null || transID.intValue() < 1) {
			throw new ValidationException("The transaction ID is not valid.");
		}

		this.transactionID = transID;
	}

	/**
	 * Set the pendingFeeID
	 * 
	 * @param pendingFeeID
	 *            The new pendingFeeID value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setPendingFeeID(String pendingFeeID) throws ValidationException, RemoteException {
		checkReadOnly();

		this.pendingFeeID = pendingFeeID;
	}

	/**
	 * Sets the transactionGroupID attribute of the MembershipTransactionVO object
	 * 
	 * @param transactionGroupID
	 *            The new transactionGroupID value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setTransactionGroupID(int transactionGroupID) throws ValidationException, RemoteException {
		checkReadOnly();
		this.transactionGroupID = transactionGroupID;
	}

	/**
	 * Sets the transactionGroupTypeCode attribute of the MembershipTransactionVO object
	 * 
	 * @param transTypeCode
	 *            The new transactionGroupTypeCode value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setTransactionGroupTypeCode(String transTypeCode) throws ValidationException, RemoteException {
		checkReadOnly();
		this.transactionGroupTypeCode = transTypeCode;
	}

	/**
	 * Sets the transactionTypeCode attribute of the MembershipTransactionVO object
	 * 
	 * @param transTypeCode
	 *            The new transactionTypeCode value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setTransactionTypeCode(String transTypeCode) throws ValidationException, RemoteException {
		checkReadOnly();
		this.transactionTypeCode = transTypeCode;
		this.transactionType = null;
	}

	/**
	 * Sets the transactionDate attribute of the MembershipTransactionVO object
	 * 
	 * @param transDate
	 *            The new transactionDate value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setTransactionDate(DateTime transDate) throws ValidationException, RemoteException {
		checkReadOnly();
		this.transactionDate = transDate;
	}

	/**
	 * Set the ID of the membership that this transaction is for. If the ID has changed then reset the reference to the membership value object so that it is re-fetched.
	 * 
	 * @param memID
	 *            The new membershipID value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setMembershipID(Integer memID) throws ValidationException, RemoteException {
		checkReadOnly();
		if (memID != null) {
			if (!memID.equals(this.membershipID)) {
				this.membership = null;
			}
			this.membershipID = memID;
		} else {
			this.membershipID = memID;
			this.membership = null;
		}
	}

	/**
	 * Set the reference to the membership that this transaction is for. Reset the membership ID attribute as well. This could be set to null if the membership is being created. A validation exception is thrown if you try to attach a membership with a different membership ID than the current one recorded on the transaction. Set the membership ID to null first if you want to change the membership ID.
	 * 
	 * @param memVO
	 *            The new membership value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setMembership(MembershipVO memVO) throws ValidationException, RemoteException {
		if (memVO != null) {
			if (this.membershipID != null) {
				// Make sure that the right membership is being attached to the
				// transaction.
				if (!this.membershipID.equals(memVO.getMembershipID())) {
					throw new ValidationException("Membership ID does not match!");
				}

				// The transactions membership ID matches the memberships
				// membership ID
				// so we are not modifying the transaction and don't need to set
				// the modified flag.
				this.membership = memVO;
				this.membership.setReadOnly(true);
			} else {
				// We are associating a new membership with the transaction.
				// This changes the transactions data so set the modified flag.
				checkReadOnly();
				this.membershipID = memVO.getMembershipID();
				this.membership = memVO;
				this.membership.setReadOnly(true);
			}
		} else {
			if (this.membershipID != null) {
				// Set the membership ID to null as well which modifies the
				// transaction
				// objects data so set the modified flag.
				checkReadOnly();
				this.membershipID = null;
				this.membership = null;
			} else {
				// The membership VO is null and so is the transactions
				// membership ID
				// so we are not realy changing the transaction objects data
				// and don't need to set the modified flag.
				this.membership = null;
			}
		}
	}

	/**
	 * Set the reference to the new membership that is being processed. Reset the membership ID attribute as well. This could be set to null if the membership is being created. The new membership does not change the membership transaction attributes that are stored in the database so the modified flag does not need to be set.
	 * 
	 * @param memVO
	 *            The new newMembership value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setNewMembership(MembershipVO memVO) throws RemoteException {
		checkReadOnly();
		if (memVO != null) {
			if (this.membershipID != null) {
				// Make sure that the right membership is being attached to the
				// transaction.
				if (!this.membershipID.equals(memVO.getMembershipID())) {
					throw new SystemException("Transaction membership ID '" + this.membershipID + "' does not match membership being set '" + memVO.getMembershipID() + "'");
				}
			}

			// Make sure that the new version of the membership is updatable
			memVO.setReadOnly(false);

			// Also set the membership ID if the new membership has one.
			this.membershipID = memVO.getMembershipID();
		} else {
			// If the old version of the membership is also null then
			// clear the membership ID.
			if (this.membership == null) {
				this.membershipID = null;
			}
		}

		this.newMembership = memVO;
	}

	/**
	 * Set the preferred commence date. Convert the String to a date and then call the overloaded method.
	 * 
	 * @param prefComDate
	 *            The new preferredCommenceDate value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	// public void setPreferredCommenceDate(String prefComDate)
	// throws ValidationException, RemoteException
	// {
	// checkReadOnly();
	// DateTime theDate = null;
	// if(prefComDate != null && prefComDate.length() > 0)
	// {
	// try
	// {
	// Date aDate = DateUtil.parseDate(prefComDate);
	// if(aDate != null)
	// {
	// theDate = new DateTime(aDate);
	// }
	// }
	// catch(ParseException pe)
	// {
	// throw new
	// ValidationException("The preferred commence date is not a valid date : "
	// + pe.getMessage());
	// }
	// }
	// setPreferredCommenceDate(theDate);
	// }

	/**
	 * Set the preferred commence date of the transaction. Validation is disabled if we have a mode of loading. The mode is set to loading when the value object is loaded with data from an entity bean in the getVO() method. This allows us to load saved data that may no longer be valid. Setting a preferred method otherwise implies a create or rejoin transaction in which case the commence, product effective date and product option effective dates must be synchronised with the preferred commence date. If rejoining then also synchronise the join date to the preferrd commence date.
	 * 
	 * @param preferredComDate
	 *            The new preferredCommenceDate value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setPreferredCommenceDate(DateTime preferredComDate) throws RemoteException {
		checkReadOnly();
		LogUtil.debug(this.getClass(), "setting preferred commence date");

		// this.preferredCommenceDate = preferredComDate;
		setPreferredCommenceDate(preferredComDate, false);
	}

	/**
	 * Validate the preferred commence date
	 * 
	 * @param preferredComDate
	 *            Date
	 */
	public void setPreferredCommenceDate(DateTime preferredComDate, boolean setDates) throws RemoteException, ValidationException {
		LogUtil.debug(this.getClass(), "setDates=" + setDates);
		if (preferredComDate != null && setDates) {
			if (!(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionTypeCode))) {
				throw new SystemException("A preferred commence date may only be set during a CREATE or REJOIN transaction.");
			}

			CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

			if (this.newMembership == null) {
				throw new SystemException("Trying to set a preferred commence date on a membership transaction without a new membership.");
			}
			DateTime today = DateUtil.clearTime(new DateTime());
			if (preferredComDate.before(today) && this.getTransactionReasonCode() != null && !(this.getTransactionReasonCode().equals(ReferenceDataVO.TRANSFER_FINANCIAL.getReferenceDataPK().getReferenceCode()))) {
				throw new ValidationException("The preferred commence date cannot be earlier than today.");
			}

			// Make sure that the date is not more than three months in the
			// future.
			DateTime futureDate = null;
			String delayedCommencePeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DELAYED_COMMENCE_PERIOD);
			Interval delayedCommencePeriod = null;
			try {
				delayedCommencePeriod = new Interval(delayedCommencePeriodSpec);
			} catch (Exception e) {
				throw new SystemException(e.getMessage(), e);
			}
			futureDate = today.add(delayedCommencePeriod);
			if (preferredComDate.after(futureDate) && this.getTransactionReasonCode() != null && !(this.getTransactionReasonCode().equals(ReferenceDataVO.TRANSFER_FINANCIAL.getReferenceDataPK().getReferenceCode()))) {
				throw new ValidationException("The preferred commence date cannot be more than " + delayedCommencePeriod.formatForDisplay(Interval.FORMAT_MONTH, Interval.FORMAT_MONTH) + " in the future.");
			}

			// If we are creating a new membership then also set the join date
			// to the preferred commence date.
			if (this.membership == null) {
				this.newMembership.setJoinDate(preferredComDate);
			}

			this.newMembership.setCommenceDate(preferredComDate);
			this.newMembership.setProductEffectiveDate(preferredComDate);
			this.newMembership.setProductOptionEffectiveDates(preferredComDate);
		}
		this.preferredCommenceDate = preferredComDate;
	}

	/**
	 * Set the product effective date on the new membership. If the membership is being created or rejoined then the product becomes effective on the preferred commence date. If the membership is being renewed: If the membership is coming off "on hold" then the product becomes effective on the transaction date. If the product has changed and the membership is "in renewal" then the product becomes effective on the transaction date If the product has changed and the membership is not "in renewal" then the product becomes effective the day after the old expiry date. If the product has not changed then the product effective date does not change.
	 * 
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setProductEffectiveDate() throws RemoteException {
		productEffectiveDate = this.transactionDate;
		LogUtil.debug(this.getClass(), "productEffectiveDate=" + productEffectiveDate);
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionTypeCode)) {
			this.newMembership.setProductEffectiveDate(getPreferredCommenceDate());
			this.newMembership.setProductOptionEffectiveDates(getPreferredCommenceDate());
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionTypeCode)) {

			if (MembershipVO.STATUS_ONHOLD.equals(this.membership.getStatus())) {
				// The membership is coming off "on hold" so the product becomes
				// effective on the transaction date.
				if (printRenewalDocument) {
					productEffectiveDate = this.membership.getExpiryDate();
				} else {
					productEffectiveDate = this.transactionDate;
				}
			}
			// should be superseded!
			else if (isProductChanged()) {
				LogUtil.debug(this.getClass(), "prod change");
				// The product has changed. See if its in renewal.
				if (MembershipVO.STATUS_INRENEWAL.equals(this.membership.getStatus())) {
					LogUtil.debug(this.getClass(), "prod change in renewal");
					// The membership is in renewal so the product becomes
					// effective
					// on the transaction date
					productEffectiveDate = this.transactionDate;
				} else {
					LogUtil.debug(this.getClass(), "prod change not in renewal");
					if (isLowerRiskProductChange()) {
						LogUtil.debug(this.getClass(), "downgrade");

						// The membership is not in renewal so the product
						// becomes effective
						// after the old expiry date if downgrade.
						try {
							productEffectiveDate = this.membership.getExpiryDate().add(new Interval("0:0:1"));
						} catch (ParseException pe) {
							throw new SystemException(pe.getMessage(), pe);
						}
					} else {
						LogUtil.debug(this.getClass(), "upgrade");
						// upgrade effective on the transaction date. JH
						// 25/05/2004
						productEffectiveDate = this.transactionDate;
					}
				}
			} else {
				// The product has not changed so the effective date stays the
				// same.
				productEffectiveDate = this.membership.getProductEffectiveDate();
			}
			this.newMembership.setProductEffectiveDate(productEffectiveDate);

			// Now set the effective date on new product options
			MembershipProductOption newMemProdOption;
			Collection newProductOptionList = this.newMembership.getProductOptionList();
			Iterator newProductOptionListIterator = newProductOptionList.iterator();
			while (newProductOptionListIterator.hasNext()) {
				newMemProdOption = (MembershipProductOption) newProductOptionListIterator.next();
				if (!this.membership.hasProductOption(newMemProdOption.getProductBenefitTypeCode())) {
					newMemProdOption.setEffectiveDate(productEffectiveDate);
				}
			}
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.transactionTypeCode)) {
			LogUtil.debug(this.getClass(), "change product");
			if (isProductChanged()) {
				LogUtil.debug(this.getClass(), "product changed");
				if (isLowerRiskProductChange()) {
					LogUtil.debug(this.getClass(), "downgrade");

					// The membership is not in renewal so the product becomes
					// effective
					// after the old expiry date.
					try {
						if (MembershipVO.STATUS_ONHOLD.equals(this.membership.getStatus()) || MembershipVO.STATUS_CANCELLED.equals(this.membership.getStatus()) || MembershipVO.STATUS_UNDONE.equals(this.membership.getStatus())) {
							// The membership is coming off "on hold" so the
							// product becomes
							// effective on the transaction date.
							productEffectiveDate = this.transactionDate;
						} else {
							productEffectiveDate = this.membership.getExpiryDate().add(new Interval("0:0:1"));
						}
					} catch (ParseException pe) {
						throw new SystemException(pe.getMessage(), pe);
					}
				} else {
					LogUtil.debug(this.getClass(), "upgrade");
					// upgrade effective on the transaction date. JH 25/05/2004
					productEffectiveDate = this.transactionDate;
					
//					if (this.effectiveDate != null && this.effectiveDate.after(productEffectiveDate) ) {
//						productEffectiveDate = this.effectiveDate;
//					}
				}

			} else {
				LogUtil.debug(this.getClass(), "this.membership.getProductEffectiveDate()");
				// is this an error???
				productEffectiveDate = this.membership.getProductEffectiveDate();
			}

			this.newMembership.setProductEffectiveDate(productEffectiveDate);

			// Now set the effective date on new product options
			MembershipProductOption newMemProdOption;
			Collection newProductOptionList = this.newMembership.getProductOptionList();
			Iterator newProductOptionListIterator = newProductOptionList.iterator();
			while (newProductOptionListIterator.hasNext()) {
				newMemProdOption = (MembershipProductOption) newProductOptionListIterator.next();
				if (!this.membership.hasProductOption(newMemProdOption.getProductBenefitTypeCode())) {
					newMemProdOption.setEffectiveDate(productEffectiveDate);
				}
			}
		}
	}

	/**
	 * Set the transaction reason code. It is assumed that the reason code is valid for the type of transaction.
	 * 
	 * @param reasonType
	 *            The new transactionReason value
	 * @param reasonCode
	 *            The new transactionReason value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setTransactionReason(String reasonType, String reasonCode) throws ValidationException, RemoteException {
		checkReadOnly();
		this.transactionReasonType = reasonType;
		this.transactionReasonCode = reasonCode;
	}

	public void setTransactionReasonCode(String reasonCode) {
		this.transactionReasonCode = reasonCode;
	}

	public void setTransactionReasonType(String reasonType) {
		this.transactionReasonType = reasonType;
	}

	/**
	 * Sets the rejoinTypeCode attribute of the MembershipTransactionVO object
	 * 
	 * @param rejTypeCode
	 *            The new rejoinTypeCode value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setRejoinTypeCode(String rejTypeCode) throws ValidationException, RemoteException {
		checkReadOnly();
		// Make sure the rejoin type valid and active.
		this.rejoinType = MembershipEJBHelper.getMembershipRefMgr().getRejoinType(rejTypeCode);
		if (this.rejoinType == null) {
			throw new SystemException("The rejoin type '" + rejTypeCode + "' is not valid.");
		}
		if (!this.rejoinType.isActive()) {
			throw new SystemException("The rejoin type '" + rejTypeCode + "' is not active and cannot be used.");
		}
		this.rejoinTypeCode = rejTypeCode;
	}

	/**
	 * Sets the rejoinType attribute of the MembershipTransactionVO object
	 * 
	 * @param rejoinTypeVO
	 *            The new rejoinType value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setRejoinType(ReferenceDataVO rejoinTypeVO) throws ValidationException, RemoteException {
		checkReadOnly();
		this.rejoinType = rejoinTypeVO;
		if (this.rejoinType == null) {
			this.rejoinTypeCode = null;
		} else {
			if (!this.rejoinType.isActive()) {
				throw new SystemException("The rejoin '" + this.rejoinType.getReferenceDataPK().getReferenceCode() + "' is not active and cannot be used.");
			}

			this.rejoinTypeCode = rejoinTypeVO.getReferenceDataPK().getReferenceCode();

			if (rejoinTypeVO.getReferenceDataPK().getReferenceCode().equals(ReferenceDataVO.REJOIN_TYPE_NEW)) {
				// Set the join date and commence date to the preferred commence
				// date.
				if (this.preferredCommenceDate == null) {
					throw new RemoteException("The preferred commence date must be set on the membership transaction before rejoin type");
				}
				this.newMembership.setJoinDate(this.preferredCommenceDate);
				LogUtil.debug(this.getClass(), "setRejoinType NEW joinDate=" + newMembership.getJoinDate());
			} else if (this.membership != null) {
				// Set the join date date back to the old values
				// in case they have been changed.
				this.newMembership.setJoinDate(this.membership.getJoinDate());
				LogUtil.debug(this.getClass(), "setRejoinType OTHER joinDate=" + newMembership.getJoinDate());
			}
		}
	}

	/**
	 * Sets the salesBranchCode attribute of the MembershipTransactionVO object
	 * 
	 * @param salesBranchCode
	 *            The new salesBranchCode value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setSalesBranchCode(String salesBranchCode) throws ValidationException, RemoteException {
		checkReadOnly();
		this.salesBranchCode = salesBranchCode;
		this.salesBranchVO = null;
	}

	/**
	 * Sets the username attribute of the MembershipTransactionVO object
	 * 
	 * @param uname
	 *            The new username value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setUsername(String uname) throws ValidationException, RemoteException {
		checkReadOnly();
		this.username = uname;
	}

	// /**
	// * Sets the userSession attribute of the MembershipTransactionVO object
	// *
	// *@param us The new userSession value
	// *@exception RemoteException Description of the Exception
	// *@exception ValidationException Description of the Exception
	// */
	// public void setUserSession(UserSession us)
	// throws RemoteException, ValidationException
	// {
	// if (us == null) {
	// throw new
	// RemoteException("Failed to set user session. UserSession is null!");
	// }
	//
	// this.userSession = us;
	//
	// //set the user name to the user id.
	// setUsername(us.getUserId());
	// setSalesBranchCode(us.getSalesBranchCode());
	// }

	public void setMembershipXML(String xml) throws RemoteException {
		this.membershipXML = xml;
	}

	/**
	 * Sets the membershipHistoryFilename attribute of the MembershipTransactionVO object
	 * 
	 * @param filename
	 *            The new membershipHistoryFilename value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void xxxsetMembershipHistoryFilename(String filename) throws ValidationException, RemoteException {
		// LogUtil.debug(this.getClass(),"setMembershipHistoryFilename 1");
		// checkReadOnly();
		// LogUtil.debug(this.getClass(),"setMembershipHistoryFilename 2");
		this.membershipHistoryFilename = filename;
		// LogUtil.debug(this.getClass(),"setMembershipHistoryFilename 3");
		// LogUtil.debug(this.getClass(),"setMembershipHistoryFilename 4");
	}

	/**
	 * The gst is only applicable to the prime addressee of the group.
	 * 
	 * @param gstFee
	 *            The new gstFee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setGstFee(double gstFee) throws RemoteException {
		checkReadOnly();
		this.gstFee = gstFee;
	}

	public void setAdjustmentList(Hashtable list) throws RemoteException {
		checkReadOnly();
		if (list != null) {
			this.adjustmentList = new Vector(list.values());
		} else {
			this.adjustmentList = null;
		}
	}

	/**
	 * The amount payable is only applicable to the prime addressee of the group.
	 * 
	 * @param amountPayable
	 *            The new amountPayable value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setAmountPayable(double amountPayable) throws RemoteException {
		checkReadOnly();
		this.amountPayable = amountPayable;
	}

	/**
	 * Sets the transactionFeeList attribute of the MembershipTransactionVO object
	 * 
	 * @param transFeeList
	 *            The new transactionFeeList value
	 * @exception ValidationException
	 *                Description of the Exception
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setTransactionFeeList(Collection transFeeList) throws ValidationException, RemoteException {
		checkReadOnly();
		if (transFeeList != null) {
			this.transactionFeeList = new Vector(transFeeList);
		} else {
			this.transactionFeeList = null;
		}

	}

	/**
	 * Sets the primeAddressee attribute of the MembershipTransactionVO object. Does not check to see if the membership transaction can be updated and does not set the modified flag to true as the prime addressee flag is not saved as part of the membership transaction data.
	 * 
	 * @param isPrimeAddressee
	 *            The new primeAddressee value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setPrimeAddressee(boolean isPrimeAddressee) throws RemoteException {
		this.primeAddressee = isPrimeAddressee;
	}

	/**
	 * Sets the payableItemID attribute of the MembershipTransactionVO object
	 * 
	 * @param payItemID
	 *            The new payableItemID value
	 * @param overrideReadOnly
	 *            The new payableItemID value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setPayableItemID(Integer payItemID, boolean overrideReadOnly) throws RemoteException {
		if (!overrideReadOnly) {
			checkReadOnly();
		}
		this.payableItemID = payItemID;
	}

	/**
	 * Sets the payableItemID attribute of the MembershipTransactionVO object
	 * 
	 * @param payItemID
	 *            The new payableItemID value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setPayableItemID(Integer payItemID) throws RemoteException {
		checkReadOnly();
		this.payableItemID = payItemID;
	}

	/**
	 * Transient
	 * 
	 * @param payableItem
	 */
	public void setPayableItem(PayableItemVO payableItem) {
		this.payableItemVO = payableItem;
	}

	/**
	 * Sets the undoneDate attribute of the MembershipTransactionVO object
	 * 
	 * @param uDate
	 *            The new undoneDate value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setUndoneDate(DateTime uDate) throws RemoteException {
		checkReadOnly();
		this.undoneDate = uDate;
	}

	/**
	 * Sets the undoneUsername attribute of the MembershipTransactionVO object
	 * 
	 * @param uName
	 *            The new undoneUsername value
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void setUndoneUsername(String uName) throws RemoteException {
		checkReadOnly();
		this.undoneUsername = uName;
	}

	/**
	 * Utility methods *************************
	 * 
	 * @param key
	 *            Description of the Parameter
	 * @return The creditAmount value
	 */

	/**
	 * Add the membership transaction fee to the transaction fee list. Does not do any checking to make sure that the fee does not already exist. Initialises the tranaction fee list if it has not been created.
	 * 
	 * @param memTransFee
	 *            The feature to be added to the TransactionFee attribute
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void addTransactionFee(MembershipTransactionFee memTransFee) throws RemoteException {
		checkReadOnly();
		if (memTransFee == null) {
			throw new SystemException("Trying to add a null transaction fee to the transaction fee list");
		}
		// Only add the fee if the fee without discounts subtracted is greater
		// than zero.
		if (memTransFee.getGrossFee() > 0.0) {
			if (this.transactionFeeList == null) {
				this.transactionFeeList = new Vector();
			}

			// Make sure we are using the same transaction ID.
			if (this.transactionID != null && memTransFee.getTransactionID() != null && !this.transactionID.equals(memTransFee.getTransactionID())) {
				throw new RemoteException("Trying to add a membership transaction fee with ID '" + memTransFee.getTransactionID() + "' to membership transaction '" + this.transactionID + "'!");
			}

			memTransFee.setTransactionID(this.transactionID);

			this.transactionFeeList.add(memTransFee);
		}
	}

	/**
	 * Clear all of the membership transaction fees and there associated membership transaction discounts.
	 */
	public void clearTransactionFeeList() {
		this.transactionFeeList = null;
	}

	/**
	 * Remove any fees associated with the product option code.
	 * 
	 * @param prodOptionCode
	 *            Description of the Parameter
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void removeProductOptionFees(String prodOptionCode) throws RemoteException {
		if (prodOptionCode != null && this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			FeeSpecificationVO feeSpecVO = null;
			ListIterator feeListIT = this.transactionFeeList.listIterator();
			while (feeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) feeListIT.next();
				feeSpecVO = memTransFee.getFeeSpecificationVO();
				if (prodOptionCode.equals(feeSpecVO.getProductBenefitCode())) {
					feeListIT.remove();
				}
			}
		}
	}

	/**
	 * Add the specified discount to the membership transaction fees which are for the given fee specification. The membership transaction fees fee specification ID must match the given fee specifications ID. The discount will only be added if the discount type has not already been added.
	 * 
	 * @param feeTypeCode
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @param discTypeVO
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	// public void addTransactionFeeDiscount(Integer feeSpecID, DiscountTypeVO
	// discTypeVO) throws RemoteException
	// {
	// if (this.transactionFeeList != null)
	// {
	// MembershipTransactionFee memTransFee = null;
	// MembershipTransactionDiscount memTransDisc = null;
	// Iterator transFeeListIT = this.transactionFeeList.iterator();
	// while (transFeeListIT.hasNext())
	// {
	// memTransFee = (MembershipTransactionFee) transFeeListIT.next();
	// if (memTransFee.getFeeSpecificationID().equals(feeSpecID))
	// memTransFee.addDiscount(discTypeVO);
	// }
	// }
	// }

	/**
	 * Add the specified discount to the membership transaction fees which are for the given fee type. The discount will only be added if the discount type has not already been added. The discount is not added if the fee type cannot be found.
	 * 
	 * @param feeTypeCode
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @param discTypeVO
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void addTransactionFeeDiscount(String feeTypeCode, DiscountTypeVO discTypeVO) throws RemoteException {
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator transFeeListIT = this.transactionFeeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				if (memTransFee.getFeeSpecificationVO().getFeeTypeCode().equals(feeTypeCode)) {
					memTransFee.setFeeEffectiveDate(this.effectiveDate);
					memTransFee.addDiscount(discTypeVO);
				}
			}
		}
	}

	public void addTransactionFeeDiscountAttribute(DiscountTypeVO discTypeVO, TransactionFeeDiscountAttribute transactionFeeDiscountAttribute) throws RemoteException {
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			MembershipTransactionDiscount memTransDisc = null;

			Iterator transFeeListIT = this.transactionFeeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				memTransDisc = memTransFee.getDiscountType(discTypeVO.getDiscountTypeCode());
				LogUtil.debug(this.getClass(), "addTransactionFeeDiscountAttribute=" + discTypeVO + "|" + memTransFee + "|" + memTransDisc);

				if (memTransDisc != null) {
					/** @todo set the fee specification */
					transactionFeeDiscountAttribute.getTransactionFeeDiscountAttributePK().setFeeSpecificationID(memTransFee.getFeeSpecificationID());

					// add the discount attribute
					if (transactionFeeDiscountAttribute.getAttributeValue() == null) {
						throw new ValidationException("Transaction fee discount attribute may not be empty.");
					}

					LogUtil.debug(this.getClass(), "transactionFeeDiscountAttribute=" + transactionFeeDiscountAttribute);
					memTransDisc.addDiscountAttribute(transactionFeeDiscountAttribute, this);
				}
			}
		}
	}

	/**
	 * Adds a feature to the TransactionFeeDiscount attribute of the MembershipTransactionVO object
	 * 
	 * @param feeTypeCode
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @param discTypeVO
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @param discountAmount
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @param discountReason
	 *            The feature to be added to the TransactionFeeDiscount attribute
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void addTransactionFeeDiscount(String feeTypeCode, DiscountTypeVO discTypeVO, double discountAmount, String discountReason) throws RemoteException {
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator transFeeListIT = this.transactionFeeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				if (memTransFee.getFeeSpecificationVO().getFeeTypeCode().equals(feeTypeCode)) {
					memTransFee.setFeeEffectiveDate(this.effectiveDate);
					memTransFee.addDiscount(discTypeVO, discountAmount, discountReason);
				}
			}
		}
	}

	/**
	 * Remove all discounts attached to the transaction fees.
	 */
	public void removeAllTransactionFeeDiscounts() {
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator transFeeListIT = this.transactionFeeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				memTransFee.clearDiscountList();
			}
		}
	}

	/**
	 * Remove the specified discounts attached to the transaction fees which are for the specified fee type code.
	 * 
	 * @param feeTypeCode
	 *            Description of the Parameter
	 * @param discountCode
	 *            Description of the Parameter
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void removeDiscountForFeeType(String feeTypeCode, String discountCode) throws RemoteException {
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator transFeeListIT = this.transactionFeeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				if (memTransFee.getFeeSpecificationVO().getFeeTypeCode().equals(feeTypeCode)) {
					memTransFee.removeDiscount(discountCode);
				}
			}
		}
	}

	/**
	 * Remove any occurrences of the specified discount type from all of the transaction fees attached to the membership transaction.
	 * 
	 * @param discountCode
	 *            Description of the Parameter
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void removeDiscountForAllFees(String discountCode) throws RemoteException {
		if (this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			Iterator transFeeListIT = this.transactionFeeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				memTransFee.removeDiscount(discountCode);
			}
		}
	}

	/**
	 * create the membership profile discounts
	 * 
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void createMemberProfileDiscounts() throws RemoteException {
		LogUtil.debug(this.getClass(), "createMemberProfileDiscounts start");
		// Must have a new membership to find profile discounts
		if (this.transactionFeeList != null && this.newMembership != null && this.newMembership.getMembershipProfile() != null && this.newMembership.getMembershipProfile().getDiscountList() != null && !this.newMembership.getMembershipProfile().getDiscountList().isEmpty()) {
			LogUtil.debug(this.getClass(), "createMemberProfileDiscounts discounts");
			MembershipTransactionFee memTransFee = null;
			Collection allowedDiscountList = null;
			Iterator allowedDiscListIT = null;
			DiscountTypeVO profileDiscountTypeVO = null;
			DiscountTypeVO feeDiscountTypeVO = null;
			Iterator transFeeListIT = null;

			// Loop through the list of discounts available on the members
			// profile.
			Iterator profileDiscountListIterator = this.newMembership.getMembershipProfile().getDiscountList().iterator();
			while (profileDiscountListIterator.hasNext()) {
				profileDiscountTypeVO = (DiscountTypeVO) profileDiscountListIterator.next();

				LogUtil.debug(this.getClass(), "createMemberProfileDiscounts discount " + profileDiscountTypeVO);

				// Loop through the list of allowed discounts for the fee types
				// associated with the membership transaction fees.
				// If the profile discount is an allowed discount for the fee
				// type
				// then apply it.
				transFeeListIT = this.transactionFeeList.iterator();
				while (transFeeListIT.hasNext()) {
					memTransFee = (MembershipTransactionFee) transFeeListIT.next();
					LogUtil.debug(this.getClass(), "createMemberProfileDiscounts memTransFee " + memTransFee);
					allowedDiscountList = memTransFee.getFeeSpecificationVO().getAllowableDiscountList();
					if (allowedDiscountList != null) {
						// Loop through the list of allowed discounts for the
						// fee type
						allowedDiscListIT = allowedDiscountList.iterator();
						while (allowedDiscListIT.hasNext()) {
							feeDiscountTypeVO = (DiscountTypeVO) allowedDiscListIT.next();
							LogUtil.debug(this.getClass(), "createMemberProfileDiscounts feeDiscountTypeVO " + feeDiscountTypeVO);
							if (profileDiscountTypeVO.equals(feeDiscountTypeVO)) {
								LogUtil.debug(this.getClass(), "createMemberProfileDiscounts adding discount");
								memTransFee.setFeeEffectiveDate(this.effectiveDate);
								memTransFee.addDiscount(feeDiscountTypeVO);
							}
						}
					}
				}
			}
		}
		LogUtil.debug(this.getClass(), "createMemberProfileDiscounts end");
	}

	/**
	 * Create transaction fee discounts where the membership has recurring discounts on the old version of ther membership and those discounts apply to the transaction fee. This method does not remove any existing discounts first.
	 * 
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void createMemberRecurringDiscounts() throws RemoteException {
		// Must have an existing membership to find recurring discounts
		if (this.membership != null && this.transactionFeeList != null) {
			MembershipTransactionFee memTransFee = null;
			FeeSpecificationVO feeSpecVO = null;
			Collection allowedDiscountList = null;
			Iterator allowedDiscListIT = null;
			DiscountTypeVO discTypeVO = null;
			Iterator transFeeListIT = this.transactionFeeList.iterator();
			while (transFeeListIT.hasNext()) {
				memTransFee = (MembershipTransactionFee) transFeeListIT.next();
				feeSpecVO = memTransFee.getFeeSpecificationVO();
				allowedDiscountList = feeSpecVO.getAllowableDiscountList();
				if (allowedDiscountList != null) {
					allowedDiscListIT = allowedDiscountList.iterator();
					while (allowedDiscListIT.hasNext()) {
						discTypeVO = (DiscountTypeVO) allowedDiscListIT.next();

						if (discTypeVO.isRecurringDiscount()) {
							// LogUtil.debug(this.getClass(),"discType="+discTypeVO+", recurring="+discTypeVO.isRecurring());
							// Remove it if it was added previously
							memTransFee.removeDiscount(discTypeVO.getDiscountTypeCode());

							// Add it if it is a recurring discount attached to
							// the membership.
							if (this.membership.hasRecurringDiscount(discTypeVO.getDiscountTypeCode())) {
								// LogUtil.debug(this.getClass(),"adding recurring discount");
								memTransFee.setFeeEffectiveDate(this.effectiveDate);
								memTransFee.addDiscount(discTypeVO);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Return a read only copy value object of this membership transaction.
	 * 
	 * @return Description of the Return Value
	 * @exception RemoteException
	 *                Description of the Exception
	 * @exception ValidationException
	 *                Description of the Exception
	 */
	public MembershipTransactionVO copy() throws RemoteException, ValidationException {
		MembershipTransactionVO transVO = null;
		transVO = new MembershipTransactionVO(ValueObject.MODE_LOADING, this.transactionGroupTypeCode, this.transactionTypeCode, this.membershipGroupJoinDate);

		transVO.setTransactionID(this.transactionID);
		transVO.setTransactionGroupID(this.transactionGroupID);
		transVO.setTransactionDate(this.transactionDate);
		transVO.setPreferredCommenceDate(this.preferredCommenceDate);
		transVO.setMembershipID(this.membershipID);
		transVO.setUsername(this.username);
		transVO.setSalesBranchCode(this.salesBranchCode);
		// transVO.setMembershipHistoryFilename(this.membershipHistoryFilename);
		transVO.setTransactionReason(this.transactionReasonType, this.transactionReasonCode);
		transVO.setAmountPayable(this.amountPayable);
		transVO.setGstFee(this.gstFee);
		transVO.setPayableItemID(this.payableItemID);
		transVO.setUndoneDate(this.undoneDate);
		transVO.setUndoneUsername(this.undoneUsername);
		transVO.setProductCode(this.productCode);
		transVO.setGroupCount(this.groupCount);
		transVO.setAttachQuote(getAttachedQuote());
		transVO.setMembershipXML(this.membershipXML);
		transVO.setEffectiveDate(this.effectiveDate);
		transVO.setEndDate(this.endDate);
		transVO.setTransactionReference(this.transactionReference);
		transVO.setMode(ValueObject.MODE_NORMAL);
		transVO.setReadOnly(true);

		return transVO;
	}

	/**
	 * Compare the membership transaction dates to determine the natural sort order.
	 * 
	 * @param obj
	 *            Description of the Parameter
	 * @return Description of the Return Value
	 */
	public int compareTo(Object obj) {
		if (obj != null && obj instanceof MembershipTransactionVO) {
			MembershipTransactionVO memTransVO = (MembershipTransactionVO) obj;
			return this.transactionDate.compareTo(memTransVO.getTransactionDate());
		} else {
			return 0;
		}
	}

	/**
	 * Writable interface methods ***********************
	 * 
	 * @exception RemoteException
	 *                Description of the Exception
	 */

	public String getWritableClassName() {
		return this.WRITABLE_CLASSNAME;
	}

	public void write(ClassWriter cw) throws RemoteException {
		write(cw, true);
	}

	/**
	 * Description of the Method
	 * 
	 * @param cw
	 *            Description of the Parameter
	 * @param deepWrite
	 *            Description of the Parameter
	 * @exception RemoteException
	 *                Description of the Exception
	 */
	public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
		cw.startClass(WRITABLE_CLASSNAME);
		cw.writeAttribute("amountPayable", NumberUtil.formatValue(this.amountPayable, "########0.00"));
		cw.writeAttribute("effectiveDate", this.effectiveDate);
		cw.writeAttribute("endDate", this.endDate);
		cw.writeAttribute("grossTransactionFee", NumberUtil.formatValue(this.getGrossTransactionFee(), "########0.00"));
		cw.writeAttribute("gstFee", NumberUtil.formatValue(this.gstFee, "########0.00"));
		cw.writeAttribute("groupCount", this.groupCount);
		// cw.writeAttribute("membershipHistoryFilename",
		// this.membershipHistoryFilename);
		// LogUtil.debug(this.getClass(),"b4 old mem");
		cw.writeAttribute("membership", this.membership);
		// LogUtil.debug(this.getClass(),"after old mem");
		cw.writeAttribute("membershipGroupJoinDate", this.membershipGroupJoinDate);
		cw.writeAttribute("membershipXML", this.membershipXML);
		cw.writeAttribute("netTransactionFee", NumberUtil.formatValue(this.getNetTransactionFee(), "########0.00"));
		// LogUtil.debug(this.getClass(),"before old mem");
		cw.writeAttribute("newMembership", this.newMembership);
		// LogUtil.debug(this.getClass(),"after old mem");
		cw.writeAttribute("payableItemID", this.payableItemID);
		cw.writeAttribute("preferredCommenceDate", this.preferredCommenceDate);
		cw.writeAttribute("primeAddressee", this.primeAddressee);
		cw.writeAttribute("productCode", this.productCode);
		cw.writeAttribute("quoteNumber", this.quoteNumber);
		cw.writeAttribute("rejoinTypeCode", this.rejoinTypeCode);
		cw.writeAttribute("salesBranchCode", this.salesBranchCode);
		cw.writeAttribute("transactionDate", this.transactionDate);
		cw.writeAttribute("transactionGroupID", this.transactionGroupID);
		cw.writeAttribute("transactionGroupTypeCode", this.transactionGroupTypeCode);
		cw.writeAttribute("transactionID", this.transactionID);
		cw.writeAttribute("transactionReasonCode", this.transactionReasonCode);
		cw.writeAttribute("transactionReasonType", this.transactionReasonType);
		cw.writeAttribute("transactionReference", this.transactionReference);
		cw.writeAttribute("transactionTypeCode", this.transactionTypeCode);
		cw.writeAttribute("username", this.username);
		cw.writeAttribute("undoneDate", this.undoneDate);
		cw.writeAttribute("undoneUsername", this.undoneUsername);
		if (deepWrite) {
			// Use the getter methods to make sure that the lists have been
			// loaded.
			Collection adjList = getAdjustmentList();
			if (adjList != null) {
				cw.writeAttributeList("adjustmentList", adjList);
			}

			Collection transFeeList = getTransactionFeeList();
			if (transFeeList != null) {
				cw.writeAttributeList("feeList", transFeeList);
			}
		}
		cw.endClass();
	}

	public DateTime getMembershipGroupJoinDate() {
		return this.membershipGroupJoinDate;
	}

	public void setProductCode(String prodCode) throws RemoteException {
		checkReadOnly();
		this.productCode = prodCode;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="product_code"
	 * 
	 * @return String
	 */
	public String getProductCode() {
		return this.productCode;
	}

	public void setGroupCount(Integer groupCount) throws RemoteException {
		checkReadOnly();
		this.groupCount = groupCount;
	}

	/**
	 * Gets the groupCount attribute of the MembershipTransactionVO object
	 * 
	 * @hibernate.property
	 * @hibernate.column name="group_count"
	 * 
	 * @return The groupCount value
	 */
	public Integer getGroupCount() {
		return groupCount;
	}

	/**
	 * Looks up the transaction adjustment list to see if there are any discretionary discounts. If found, return the amount
	 * 
	 * @return double The discount amount. Returns zero if not found.
	 * 
	 */
	public BigDecimal getDiscretionaryDiscountAmount() {
		BigDecimal amount = new BigDecimal(0);
		ArrayList adjustList = null;
		try {
			adjustList = this.getAdjustmentList();
		} catch (RemoteException ex) {
			return amount;
		}
		MembershipTransactionAdjustment adjustment = null;
		if (adjustList != null) {
			for (int x = 0; x < adjustList.size(); x++) {
				adjustment = (MembershipTransactionAdjustment) adjustList.get(x);
				if (adjustment.getAdjustmentTypeCode().equals(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT)) {
					amount = amount.add(adjustment.getAdjustmentAmount());
				}
			}
		}
		return amount;
	}

	public boolean selectProductOptionByDefault() {
		return (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(this.getTransactionTypeCode()));
	}

	public boolean modifyMotoNewsTransaction() {
		return (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(this.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.getTransactionTypeCode()));
	}

	/**
	 * Set the effective date of the transaction. This is set to the start date of the payable
	 */
	public void setEffectiveDate(DateTime effDate) throws RemoteException {
		LogUtil.debug(this.getClass(), "############## effectiveDate=" + effDate);
		checkReadOnly();
		this.effectiveDate = effDate;
	}

	/**
	 * Get the effective date of the transaction.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="effective_date"
	 */
	public DateTime getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEndDate(DateTime endDate) throws RemoteException {
		checkReadOnly();
		LogUtil.debug(this.getClass(), " endDate=" + endDate);
		this.endDate = endDate;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="end_date"
	 * @return DateTime
	 */
	public DateTime getEndDate() {
		return endDate;
	}

}
