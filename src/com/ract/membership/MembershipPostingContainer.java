package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import com.ract.common.Account;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.Company;
import com.ract.common.RollBackException;
import com.ract.common.SalesBranchVO;
import com.ract.common.SystemException;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PostingContainer;
import com.ract.user.User;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * <p>
 * Posting container for membership ledgers. Constructor will process a transaction group and create the necessary postings to return for sending to the finance system.
 * </p>
 * 
 * @author GW, remodeled by JH
 * @version 1.0
 */

public class MembershipPostingContainer extends PostingContainer {

	/**
	 * Constructor that sets internal variables to create postings.
	 * 
	 * @param membershipTransaction the membership transaction
	 * @param transactionReference the transaction reference
	 * @param transGroup the transaction group
	 * @throws RemoteException
	 */
	public MembershipPostingContainer(MembershipTransactionVO membershipTransaction, Integer transactionReference, TransactionGroup transGroup) throws RemoteException {
		super();

		this.membershipTransaction = membershipTransaction;
		this.transactionReference = transactionReference;
		this.transGroup = transGroup;

		// create the postings
		if (transGroup.getOverrideTransactionDate() != null) {
			this.createPostings(transGroup.getOverrideTransactionDate().getDateOnly());
		} else {
			this.createPostings();
		}
	}

	/**
	 * The actual transaction that requires postings to be created.
	 */
	private MembershipTransactionVO membershipTransaction;

	/**
	 * The transaction group with the set of transactions
	 */
	private TransactionGroup transGroup;

	/**
	 * List of transaction fees attached to the transaction group.
	 */
	private Collection txFeeList;

	/**
	 * The base description to display on the posting
	 */
	private String description;

	/**
	 * The membership number
	 */
	private String membershipNumber;

	/**
	 * Has the membership been shortened by the transaction.
	 */
	private boolean membershipShortened = false;

	/**
	 * The totalFee is the sum of the fees and discounts
	 */
	private double totalFee = 0;

	/**
	 * The reference mgr
	 */
	private MembershipRefMgr refMgr;

	/**
	 * The common mgr
	 */
	// private CommonMgr commonMgr;

	/**
	 * The payment mgr
	 */
	private PaymentMgr paymentMgr;

	/**
	 * The list of accounts that have a fee specification for the transactiion fee list.
	 */
	private Collection accountList;

	/**
	 * Total of all attached adjustments.
	 */
	private double adjustmentTotal = 0;

	/**
	 * Create the postings collection required for a membership transaction. Delegate the processing to another private method.
	 * 
	 * @exception SystemException Description of the Exception
	 * @exception RemoteException Description of the Exception
	 */
	private void createPostings() throws SystemException, RemoteException {
		createPostings(new DateTime().getDateOnly());
	}

	/**
	 * Create the postings collection required for a membership transaction. Delegate the processing to another private method.
	 * 
	 * @param DateTime postAtDateOverride Override Post At date.
	 * @exception SystemException Description of the Exception
	 * @exception RemoteException Description of the Exception
	 */
	private void createPostings(DateTime postAtDateOverride) throws SystemException, RemoteException {
		LogUtil.log(this.getClass(), "createPostings start");

		transactionGroupTypeCode = transGroup.getTransactionGroupTypeCode();
		transactionTypeCode = membershipTransaction.getTransactionTypeCode();

		LogUtil.debug(this.getClass(), "transactionGroupTypeCode=" + transactionGroupTypeCode);
		LogUtil.debug(this.getClass(), "transactionTypeCode=" + transactionTypeCode);

		gstRate = transGroup.getGstRate();

		LogUtil.debug(this.getClass(), "gstRate=" + gstRate);

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		refMgr = MembershipEJBHelper.getMembershipRefMgr();

		gstAccount = refMgr.getMembershipGSTAccount();

		LogUtil.debug(this.getClass(), "gstAccount=" + gstAccount);

		// effective date of posting
		postAtDate = postAtDateOverride;

		// assume it is RACT
		company = Company.RACT;

		description = transactionTypeCode + " membership. Ref: " + transactionReference;

		LogUtil.debug(this.getClass(), "description=" + description);

		oldMembership = membershipTransaction.getMembership();
		LogUtil.debug(this.getClass(), "oldMembership=" + oldMembership);

		newMembership = membershipTransaction.getNewMembership();
		LogUtil.debug(this.getClass(), "newMembership=" + newMembership);

		membershipNumber = newMembership.getMembershipNumber();

		paymentMgr = PaymentEJBHelper.getPaymentMgr();

		// get the fee list for the transaction
		txFeeList = membershipTransaction.getTransactionFeeList();

		// some transaction have no fees yet require postings. For example, the
		// PA
		// receives a credit if there is a positive adjustment to the
		// transaction
		// even though they may have no fees. This must be credited to the
		// suspense
		// account and debited from the clearing account, but no other postings
		// are
		// required. In addition, if a member is removed from a group and
		// nothing has been
		// paid then reversing ledgers are required.
		transactionHasFees = (txFeeList != null && txFeeList.size() > 0);
		LogUtil.debug(this.getClass(), "transactionHasFees=" + transactionHasFees);
		// all existing active postings need to be reversed and credit
		// put into suspense account.
		LogUtil.log(this.getClass(), "createRemoveUnearnedPostings" + this);
		createRemoveUnearnedPostings();

		// create postings for credit held against the old membership or
		// unearned credit
		LogUtil.log(this.getClass(), "createSuspensePostings" + this);
		createSuspensePostings();

		// create postings for discounts attached to the fees
		LogUtil.log(this.getClass(), "createAlternativeDiscountPostings" + this);
		createAlternativeDiscountPostings();

		// calculate the net transaction fee.
		netTxFee = NumberUtil.roundDouble(membershipTransaction.getGrossTransactionFee() - membershipTransaction.getTotalDiscount(), 2);
		LogUtil.debug(this.getClass(), "netTxFee=" + netTxFee);

		// create postings for the income account
		LogUtil.log(this.getClass(), "createIncomePostings" + this);
		createIncomePostings();

		// create postings for adjustments attached to the transaction but
		// exclude discretionary discounts
		LogUtil.log(this.getClass(), "createNonDiscretionaryDiscountPostings" + this);
		createNonDiscretionaryDiscountPostings();

		// remove the alternative income and the discretionary discount, adding
		// GST
		LogUtil.debug(this.getClass(), "1 totalFee=" + totalFee);

		// subtract sum of gst exclusive discounts (adding gst) plus the unpaid
		// amount from the total fee
		totalFee -= (alternativeIncome * (1 + gstRate)) + unpaidAmount;
		LogUtil.debug(this.getClass(), "2 totalFee=" + totalFee);

		// subtract any credits against the old membership
		totalFee -= membershipTransaction.getCreditDiscountApplied();
		LogUtil.debug(this.getClass(), "3 totalFee=" + totalFee);

		totalFee = NumberUtil.roundDouble(totalFee, 2);

		LogUtil.log(this.getClass(), "4 totalFee=" + totalFee);

		// create clearing postings
		LogUtil.log(this.getClass(), "createClearingPostings" + this);
		createClearingPostings();

		LogUtil.log(this.getClass(), "postingAdjustment" + this);
		// difference between total of postings and the actual transaction fee
		if (postingAdjustment != 0) {
			usePostingAdjustment();
		}

		LogUtil.log(this.getClass(), "postingTotal" + this);
		// handle the rounding error.
		if (postingTotal != 0) {
			handleRoundingErrors();
		}

		// remove any postings with a zero amount
		this.removePostingsWithZeroAmount();

		LogUtil.log(this.getClass(), "createPostings end " + this);

	} // createPostings

	public String toString() {
		return "postingTotal=" + postingTotal + FileUtil.NEW_LINE + super.toString();
	}

	/**
	 * Handle rounding errors that cause the posting total not to be equal to 0.
	 * 
	 * @throws SystemException
	 */
	private void handleRoundingErrors() throws SystemException {

		final double ROUNDING_THRESHOLD = 0.02; // max of 2 cents rounding per
		// posting
		double roundingThreshold = 0;
		for (int x = 0; x < this.size(); x++) {
			roundingThreshold += ROUNDING_THRESHOLD;
		}
		LogUtil.debug(this.getClass(), "roundingThreshold=" + roundingThreshold);
		double roundingAmount = 0;
		try {
			if (incomePosting != null) {
				roundingAmount = NumberUtil.roundDouble(incomePosting.getAmount() - postingTotal, 2);
				LogUtil.debug(this.getClass(), "roundingAmount=" + roundingAmount);
				incomePosting.setAmount(roundingAmount);
				LogUtil.debug(this.getClass(), "handling rounding errors income" + this);
			} else if (suspensePosting != null) {
				roundingAmount = NumberUtil.roundDouble(suspensePosting.getAmount() - postingTotal, 2);
				LogUtil.debug(this.getClass(), "roundingAmount=" + roundingAmount);
				suspensePosting.setAmount(NumberUtil.roundDouble(roundingAmount, 2));
				LogUtil.debug(this.getClass(), "handling rounding errors income" + this);
			} else {
				throw new SystemException("Unable to process rounding error '" + postingTotal + "' " + (this == null ? "no postings" : this.toString()) + ".");
			}
		} catch (PaymentException e) {
			throw new SystemException("Error updating income posting.", e);
		}

		// sanity checking
		if (Math.abs(roundingAmount) > roundingThreshold) {
			throw new SystemException("Rounding amount exceeds maximum = " + postingTotal);
		}
	}

	/**
	 * If the adjustment is greater than 0 then it is assumed that there has been a rounding problem on the income posting. If the posting adjustment is less than 0 then it is assumed there has been a gst rounding problem.
	 * 
	 * @throws SystemException
	 */
	private void usePostingAdjustment() throws SystemException {
		LogUtil.log(this.getClass(), "postingAdjustment=" + postingAdjustment);
		try {
			// income
			if (postingAdjustment > 0) {
				LogUtil.debug(this.getClass(), "incomePosting=" + incomePosting.getAmount());
				LogUtil.debug(this.getClass(), "postingAdjustment=" + postingAdjustment);
				incomePosting.setAmount(NumberUtil.roundDouble(incomePosting.getAmount() + postingAdjustment, 2));
				LogUtil.debug(this.getClass(), "using posting adjustment" + this);
				postingTotal += postingAdjustment;
			}
			// gst
			else if (postingAdjustment < 0) {
				gstPosting.setAmount(NumberUtil.roundDouble(gstPosting.getAmount() - postingAdjustment, 2));
				postingTotal -= postingAdjustment;
			}
		} catch (PaymentException e) {
			throw new SystemException("Error updating income posting : ", e);
		}
	}

	/**
	 * Total of the alternative income.
	 */
	private double alternativeIncome = 0;

	/**
	 * For testing the postings balance. The difference between the sum of the postings and the clearance posting.
	 */
	private double postingAdjustment = 0;

	/**
	 * Create a balancing posting to take the money out of the membership clearing account.
	 * 
	 * @throws RemoteException
	 */
	private void createClearingPostings() throws RemoteException {
		LogUtil.debug(this.getClass(), "totalFee=" + totalFee);
		LogUtil.debug(this.getClass(), "suspenseAmount=" + suspenseAmount);
		LogUtil.debug(this.getClass(), "netTxFee=" + netTxFee);
		LogUtil.debug(this.getClass(), "incomePosting=" + (incomePosting != null ? CurrencyUtil.formatDollarValue(incomePosting.getAmount()) : "null"));

		// get the correct clearing account depending on the payment method
		MembershipAccountVO memClearingAccountVO = null;
		memClearingAccountVO = refMgr.getMembershipClearingAccount();
		Account clearingAccount = memClearingAccountVO;

		// create the clearing account posting if there is a fee and a credit income posting or a suspense amount
		if ((netTxFee != 0 && (incomePosting != null && incomePosting.getAmount() < 0)) || suspenseAmount != 0) { // if there are no fees

			// the posting adjustment is equal to the total of the postings plus
			// the net transaction fee.
			postingAdjustment = NumberUtil.roundDouble(postingTotal + netTxFee, 2);

			LogUtil.debug(this.getClass(), "postingAdjustment=" + postingAdjustment);

			// then do the clearing posting
			double postingAmount = 0;
			if (netTxFee != 0) {
				postingAmount = netTxFee;
			} else if (oldCredit != netTxFee) {
				LogUtil.debug(this.getClass(), "the suspense posting effectively is the clearing posting???");
				// the suspense posting effectively is the clearing posting?
				// postingAmount = -suspenseAmount;
				/**
				 * @todo determine if this affects anything?
				 */
			}

			if (postingAmount != 0) {
				try {
					posting = new PayableItemPostingVO(NumberUtil.roundDouble(postingAmount, 2), clearingAccount, null, postAtDate, postAtDate, company, "Balancing post for membership. Ref: " + transactionReference, containerID);
				} catch (PaymentException e) {
					throw new SystemException("Error creating posting for Clearing Account : ", e);
				}
				this.add(posting);
				// add the clearing posting amount to the posting total
				postingTotal += postingAmount;
				postingTotal = NumberUtil.roundDouble(postingTotal, 2);

				LogUtil.debug(this.getClass(), "postingAmount=" + postingAmount);
				LogUtil.debug(this.getClass(), "postingTotal after postingAmount=" + postingTotal);

			}
		}
		// round the total
		postingTotal = NumberUtil.roundDouble(postingTotal, 2);

		LogUtil.debug(this.getClass(), "postingTotal after createClearingPostings=" + postingTotal);
	}

	/**
	 * Create the income postings for each account that is associated with the fees.
	 * 
	 * @throws RemoteException
	 */
	private void createIncomePostings() throws RemoteException {
		// reset the posting so we don't get a carry over
		posting = null;

		LogUtil.debug(this.getClass(), "postingTotal=" + postingTotal);

		// Get a list of distinct account pairs, as the private class
		// FeeSpecificationAccounts defined below, for the transaction fees.
		accountList = getDistinctAccountList(membershipTransaction, transGroup);

		// the netFee is the amount to be posted to the income account
		double netFee = 0;

		// the net fee with GST included
		double netFeeIncGST = 0;

		// the txFee is the gross fee for each fee spec
		double txFee = 0;

		// the gst on the transaction fee
		double gstOnTxFee = 0;

		// For each distinct account pair sum up the net transaction fees
		if (accountList != null) {
			FeeSpecificationAccounts feeSpecAccounts = null;
			Iterator accountListIterator = accountList.iterator();
			Collection transList = null;
			/**
			 * @todo Needs rewriting.
			 * 
			 *       The getDistinctAccountList method creates a list of distinct pairs of earned and unearned accounts. The data model allows for more than one transaction fee type to use the same account pair. This will cause only the first fee to be processed and the rest ignored. The simple fix may be to iterate on the transaction fees rather than the account list. Otherwise the account list key needs to be reviewed.
			 */
			while (accountListIterator.hasNext()) {
				feeSpecAccounts = (FeeSpecificationAccounts) accountListIterator.next();
				// get the transaction fee less non-discretionary discounts
				// except unearned credits
				double creditAdjustment = 0;

				txFeeList = membershipTransaction.getTransactionFeeList();
				Iterator txFeeListIterator = txFeeList.iterator();
				boolean foundFee = false;
				while (!foundFee && txFeeListIterator.hasNext()) {
					MembershipTransactionFee memTxFee = (MembershipTransactionFee) txFeeListIterator.next();

					LogUtil.debug(this.getClass(), "" + memTxFee.toString());
					LogUtil.debug(this.getClass(), "membershipShortened=" + membershipShortened);
					LogUtil.debug(this.getClass(), "gross=" + memTxFee.getGrossFee());
					LogUtil.debug(this.getClass(), "nonoffsetdiscount=" + memTxFee.getNonOffsetDiscount());
					LogUtil.debug(this.getClass(), "totaldiscount=" + memTxFee.getTotalDiscount());
					LogUtil.debug(this.getClass(), "creditdiscount=" + memTxFee.getCreditDiscount());
					LogUtil.debug(this.getClass(), "unearnedcredit=" + memTxFee.getUnearnedCredit());
					LogUtil.debug(this.getClass(), "transfercredit=" + memTxFee.getTransferCredit());
					LogUtil.debug(this.getClass(), "earnedincomeamount=" + memTxFee.getEarnedIncomeAmount());
					LogUtil.debug(this.getClass(), "nettxfee=" + memTxFee.getNetTransactionFee());
					LogUtil.debug(this.getClass(), "unearnedincomeaccount=" + memTxFee.getUnearnedIncomeAmount());
					LogUtil.debug(this.getClass(), "memTxFee.getIncomeDiscount()=" + memTxFee.getIncomeDiscount());

					// if the gross fee is fully discounted by a non-accountable
					// discount apart from a credit discount or unearned credit
					// then don't account
					// for it with a set of ledgers.
					if ((memTxFee.getGrossFee() == memTxFee.getNonOffsetDiscount()) && memTxFee.getCreditDiscount() == 0 && memTxFee.getUnearnedCredit() == 0) {
						txFee = 0;
						continue;
					}

					// search for an applicable fee
					if (feeSpecAccounts.getEarnedAccount().getAccountNumber().equals(memTxFee.getFeeSpecificationVO().getEarnedAccount().getAccountNumber())) {
						foundFee = true;
						if (membershipShortened) {
							txFee = memTxFee.getGrossFee();
						} else {
							// get the gross fee minus the income discount -
							// doesn't include transfers
							txFee = memTxFee.getGrossFee() - memTxFee.getIncomeDiscount();
						}
					}
				}

				totalFee += txFee;

				LogUtil.debug(this.getClass(), "totalFee=" + totalFee);
				LogUtil.debug(this.getClass(), "txFee=" + txFee);

				// hold transactions do not get processed by this class
				if (membershipShortened) {
					// only use the transaction fee, the unearned amount is an
					// adjustment
					netFeeIncGST = txFee;
				} else {
					LogUtil.log(this.getClass(), "unearned =  " + unearnedAmount);
					LogUtil.log(this.getClass(), "new credit amount " + newMembership.getCreditAmount());
					// credit equals unearned amounts minus new credit amount
					double creditAmount = (unearnedAmount - newMembership.getCreditAmount());
					LogUtil.log(this.getClass(), "unearnedAmount =  " + unearnedAmount);
					LogUtil.log(this.getClass(), "creditAmount =  " + creditAmount);
					if (creditAmount > 0) {
						// add the unearned amount
						netFeeIncGST = txFee + creditAmount;
					} else {
						// credit held over - becomes the difference between the
						// current credit
						// and the new suspense amount
						netFeeIncGST = txFee;
					}
				}

				// GST exclusive total
				gstOnTxFee = CurrencyUtil.calculateGSTincluded(netFeeIncGST, gstRate);
				netFee = CurrencyUtil.calculateGSTExclusiveAmount(netFeeIncGST, gstRate);

				LogUtil.debug(this.getClass(), "gstOnTxFee=" + gstOnTxFee);
				LogUtil.debug(this.getClass(), "netFee=" + netFee);

				// If the fee is greater than zero then create a posting
				if (netFee > 0) {
					// Turn the netFee into a negative amount so that it gets
					// credited to the account
					netFee = -NumberUtil.roundDouble(netFee, 2);

					// int daysToEarn = 0;
					// Work out which accounts need to be posted to.
					if (feeSpecAccounts.getUnearnedAccount() == null) {
						account = feeSpecAccounts.getEarnedAccount();
						incomeAccount = null;
					} else {
						account = feeSpecAccounts.getUnearnedAccount();
						incomeAccount = feeSpecAccounts.getEarnedAccount();

						// calculate the post at date and days to earn
						if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(membershipTransaction.getTransactionTypeCode())) {
							if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(transGroup.getTransactionGroupTypeCode())) {

								DateTime newExpiryDate = membershipTransaction.getNewMembership().getExpiryDate();
								Interval membershipPeriod = null;

								try {
									membershipPeriod = new Interval(new DateTime(), newExpiryDate);
								} catch (Exception e) {
									throw new SystemException("Error calculating membership period if: ", e);
								}
								// daysToEarn = membershipPeriod.getTotalDays();
							} else {
								DateTime fromDate = null;
								if (MembershipVO.STATUS_ONHOLD.equals(oldMembership.getStatus())) {
									fromDate = DateUtil.clearTime(new DateTime());
								} else {
									/**
									 * @todo see business rule document section 2.28
									 * 
									 *       currently this will not happen due to changes to the add member button for groups.
									 * 
									 */
									fromDate = oldMembership.getExpiryDate();
								}

								DateTime newExpiryDate = membershipTransaction.getNewMembership().getExpiryDate();
								Interval membershipPeriod = null;

								try {
									if (membershipShortened) {
										membershipPeriod = new Interval(new DateTime(), newExpiryDate);
									} else {
										membershipPeriod = new Interval(fromDate, newExpiryDate);
									}
								} catch (Exception e) {
									throw new SystemException("Error calculating membership period else: " + e.getMessage(), e);
								}
							}
						} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(membershipTransaction.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(membershipTransaction.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(membershipTransaction.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(membershipTransaction.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(membershipTransaction.getTransactionTypeCode())) {

							LogUtil.debug(this.getClass(), "in CP");

							DateTime newExpiryDate = membershipTransaction.getNewMembership().getExpiryDate();
							Interval membershipPeriod = null;
							try {
								// from the commence date
								membershipPeriod = new Interval(membershipTransaction.getTransactionDate(), newExpiryDate);
							} catch (Exception e) {
								throw new SystemException("Error calculating membership period not renew: " + e.getMessage(), e);
							}
						}
					}

					netFee = NumberUtil.roundDouble(netFee, 2);

					LogUtil.debug(this.getClass(), "createIncomePostings 1");
					LogUtil.debug(this.getClass(), "account=" + account);
					LogUtil.debug(this.getClass(), "incomeAccount=" + incomeAccount);

					try {
						posting = new PayableItemPostingVO(netFee, account, incomeAccount, postAtDate, postAtDate, company, description, containerID);

					} catch (PaymentException e) {
						throw new SystemException("Error creating : " + e.getMessage(), e);
					}
					this.add(posting);

					incomePosting = posting;

					LogUtil.debug(this.getClass(), "after income " + this.toString());

					postingTotal += netFee;

					LogUtil.debug(this.getClass(), "postingTotal after incomePosting=" + postingTotal);

					postingTotal = NumberUtil.roundDouble(postingTotal, 2);

					// create the gst posting for the other portion of the
					// income posting
					createGSTPosting(-gstOnTxFee, "GST on membership.");

				} else if (netFee < 0 && suspensePosting != null) {
					account = feeSpecAccounts.getUnearnedAccount();
					postAtDate = new DateTime();
					try {
						posting = new PayableItemPostingVO(-suspensePosting.getAmount(), account, null, postAtDate, postAtDate, company, description, containerID);
					} catch (PaymentException e) {
						throw new SystemException("Error creating posting.", e);
					}
					this.add(posting);

					postingTotal += posting.getAmount();
					postingTotal = NumberUtil.roundDouble(postingTotal, 2);
				}
				// else if net fee = 0 then ignore
			}
		}
		LogUtil.log(this.getClass(), "here ");
		// Done the income postings. if not null then we already have a
		// reference.
		if (incomePosting == null) {
			incomePosting = posting;
		}

		LogUtil.debug(this.getClass(), "incomePosting = " + incomePosting);
		LogUtil.debug(this.getClass(), "postingTotal after createIncomePostings=" + postingTotal);

	}

	/*
	 * Create a posting for the GST amount Note: fees are GST inclusive - should be done at the time of the income posting <tag> GST posting </tag>
	 */
	private void createGSTPosting(double gstAmount, String desc) throws RemoteException {
		if (gstAmount != 0) {
			try {
				posting = new PayableItemPostingVO(gstAmount, gstAccount, null, postAtDate, postAtDate, company, desc + " Ref: " + transactionReference, containerID);
			} catch (PaymentException e) {
				throw new SystemException("Error creating posting to GST Account.", e);
			}
			this.add(posting);
			if (gstPosting == null) {
				gstPosting = posting;
			}

			postingTotal += gstAmount;
			gstTotal += gstAmount;

			LogUtil.debug(this.getClass(), "gstAmount=" + gstAmount);
			LogUtil.debug(this.getClass(), "postingTotal createIncomeGSTPostings=" + postingTotal);

		}
	}

	/**
	 * Create postings for the discounts that provide alternative income sources
	 * 
	 * MEM-216 - now includes tranfer credit
	 * 
	 * @throws RemoteException
	 */
	private void createAlternativeDiscountPostings() throws RemoteException {
		Iterator txFeeListIterator = null;

		if (transactionHasFees) {
			txFeeListIterator = txFeeList.iterator();
			while (txFeeListIterator.hasNext()) {
				MembershipTransactionFee memTxFee = (MembershipTransactionFee) txFeeListIterator.next();
				Collection discountList = memTxFee.getDiscountList();
				if (discountList != null) {
					Iterator discountListIterator = discountList.iterator();
					while (discountListIterator.hasNext()) {
						MembershipTransactionDiscount discount = (MembershipTransactionDiscount) discountListIterator.next();
						DiscountTypeVO discountType = discount.getDiscountTypeVO();
						LogUtil.debug(this.getClass(), "disc type = " + discountType.getDiscountTypeCode() + " , " + discountType.getDiscountAccountID());

						boolean suspenseAccount = false;
						if (discountType.getDiscountAccount() != null) {
							suspenseAccount = discountType.getDiscountAccount().isSuspenseAccount();
							if (suspenseAccount) {
								throw new SystemException("Discounts cannot be funded from a suspense account.");
							}
						}
						LogUtil.log(this.getClass(), "discountType postings");
						LogUtil.log(this.getClass(), "discountType=" + discountType);
						// create discount postings if an offset account and not
						// a credit type discount.
						if (discountType.isOffsetDiscount() && !DiscountTypeVO.TYPE_CREDIT.equals(discountType.getDiscountTypeCode())) {
							LogUtil.debug(this.getClass(), "offset discount");
							MembershipAccountVO memAccount = discountType.getDiscountAccount();
							LogUtil.log(this.getClass(), "memAccount=" + memAccount);
							if (memAccount != null) {
								account = memAccount;
								LogUtil.log(this.getClass(), "account=" + account);
								double discountAmount = discount.getDiscountAmount();
								// Deduct the GST component
								double postingAmount = NumberUtil.roundDouble(CurrencyUtil.calculateGSTExclusiveAmount(discountAmount, gstRate), 2);

								alternativeIncome += postingAmount;
								postingTotal += postingAmount;
								try {
									posting = new PayableItemPostingVO(postingAmount, account, null, postAtDate, postAtDate, company, "Discount on membership. Ref: " + transactionReference, containerID);
								} catch (PaymentException e) {
									throw new SystemException("Error creating discount posting : " + e.getMessage(), e);
								}
								// subtract discount amount from the total fee
								totalFee -= postingAmount;
								this.add(posting);

								LogUtil.debug(this.getClass(), "totalFee=" + totalFee);
								LogUtil.debug(this.getClass(), "postingAmount=" + postingAmount);
								LogUtil.debug(this.getClass(), "postingTotal after discount=" + postingTotal);

								/** @todo GST posting? */
								double discGSTAmount = discountAmount - postingAmount;
								String gstDesc = "Discount GST on membership. Ref: " + transactionReference;
								createGSTPosting(discGSTAmount, gstDesc);

							}
						}
						// regression test effect of the following
						else {
							LogUtil.debug(this.getClass(), "non-offset discount");
						}

					}
				}
			} // done the alternative income postings
		}
	}

	/**
	 * Return a distinct list of accounts, as MembershipAccountVO, from the transaction fee list.
	 * 
	 * @param transGroup Description of the Parameter
	 * @param membershipTransaction Description of the Parameter
	 * @return The disctinctAccountList value
	 * @exception RemoteException Description of the Exception
	 */
	private Collection getDistinctAccountList(MembershipTransactionVO membershipTransaction, TransactionGroup transGroup) throws RemoteException {
		Hashtable accountList = new Hashtable();
		Collection feeList = membershipTransaction.getTransactionFeeList();
		if (feeList != null) {
			FeeSpecificationVO feeSpecVO = null;
			FeeSpecificationAccounts feeSpecAccounts = null;
			String key = null;
			Iterator feeListIterator = feeList.iterator();
			// for each fee
			while (feeListIterator.hasNext()) {
				MembershipTransactionFee memTxFee = (MembershipTransactionFee) feeListIterator.next();
				// get the fee spec associated to the fee
				feeSpecVO = memTxFee.getFeeSpecificationVO();

				feeSpecAccounts = new FeeSpecificationAccounts();

				// get the earned account
				MembershipAccountVO earnedAccount = feeSpecVO.getEarnedAccount();

				// look up the profit centre for the user's sales branch to set
				// the sub-account
				User user = transGroup.getUser();

				// user should never be null
				SalesBranchVO salesBranch = user.getSalesBranch();

				// update the earned account's subaccount
				earnedAccount.setSubAccount(salesBranch.getProfitCentre());

				feeSpecAccounts.setEarnedAccount(earnedAccount);
				feeSpecAccounts.setUnearnedAccount(feeSpecVO.getUnearnedAccount());

				key = feeSpecAccounts.getKey();

				// add the fee specification accounts to the list if it is not
				// already there
				if (!accountList.contains(key)) {
					accountList.put(key, feeSpecAccounts);
				}
			}
		}
		return accountList.values();
	}// getDistinctAccountList

	/**
	 * The total amount to suspense
	 */
	private double suspenseAmount;

	/**
	 * The old credit attached to the membership
	 */
	private double oldCredit;

	/**
	 * The rate of GST
	 */
	private double gstRate;

	/**
	 * The total of the set of postings
	 */
	private double postingTotal;

	/**
	 * The total amount to post to the GST
	 */
	private double gstTotal;

	/**
	 * The amount that has been unpaid
	 */
	private double unpaidAmount;

	// complex structures

	/**
	 * The company that postings are for
	 */
	private Company company;

	/**
	 * The effective date of the transaction
	 */
	private DateTime postAtDate;

	/**
	 * The transaction reference for the postings
	 */
	private Integer transactionReference;

	/**
	 * The id of the component to contain the postings
	 */
	private Integer containerID;

	/**
	 * Generic posting placeholder
	 */
	private PayableItemPostingVO posting;

	/**
	 * The suspense posting placeholder
	 */
	private PayableItemPostingVO suspensePosting;

	/**
	 * The GST posting placeholder
	 */
	private PayableItemPostingVO gstPosting;

	/**
	 * The income posting placeholder
	 */
	private PayableItemPostingVO incomePosting;

	/**
	 * The account to post to
	 */
	private Account account;

	/**
	 * The income account to post to
	 */
	private Account incomeAccount;

	private Account gstAccount;

	/**
	 * Does the transaction have any fees associated with it?
	 */
	private boolean transactionHasFees;

	/**
	 * The old membership that is attached to the transaction
	 */
	private MembershipVO oldMembership;

	/**
	 * The new membership that is attached to the transaction
	 */
	private MembershipVO newMembership;

	/**
	 * Create the suspense postings for credit stored against a membership and unearned credit used during a transaction.
	 * 
	 * Pool all credit together.
	 * 
	 * @throws RemoteException
	 */
	private void createSuspensePostings() throws RemoteException {
		refMgr = MembershipEJBHelper.getMembershipRefMgr();
		PayableItemPostingVO posting = null;
		Account suspenseAccount = refMgr.getMembershipSuspenseAccount();

		if (oldMembership != null) {
			oldCredit = oldMembership.getCreditAmount();
		}

		// initialise the suspense amount
		suspenseAmount = 0;

		// suspense amount is equal to the old credit minus the new credit
		suspenseAmount = oldCredit - newMembership.getCreditAmount();

		LogUtil.debug(this.getClass(), "unearnedAmount=" + unearnedAmount);
		LogUtil.debug(this.getClass(), "old credit amount=" + oldCredit);
		LogUtil.debug(this.getClass(), "new credit amount=" + newMembership.getCreditAmount());

		// createAffiliatedClubSuspensePostings();

		if (suspenseAmount != 0) {
			// typically unearned
			// double gstExclusiveAmount =
			// CurrencyUtil.calculateGSTExclusiveAmount(suspenseAmount,
			// gstRate);
			// double gstOnSuspense = suspenseAmount - gstExclusiveAmount;
			try {
				posting = new PayableItemPostingVO(NumberUtil.roundDouble(suspenseAmount, 2), suspenseAccount, null, postAtDate, postAtDate, company, "Adjust membership suspense account. Ref: " + transactionReference, containerID);
			} catch (PaymentException e) {
				throw new SystemException("Error creating posting to Suspense Account : ", e);
			}
			this.add(posting);

			// global
			suspensePosting = posting;

			// null reference
			posting = null;

			// add gst to the posting total
			postingTotal += suspenseAmount;

			LogUtil.debug(this.getClass(), "postingTotal after suspense amount=" + postingTotal);

		} // done the suspense posting

		// gst inclusive
		if (unearnedAmount != 0) {

			// typically unearned
			// double gstExclusiveAmount =
			// CurrencyUtil.calculateGSTExclusiveAmount(unearnedAmount,
			// gstRate);
			// double gstOnSuspense = suspenseAmount - gstExclusiveAmount;
			try {
				posting = new PayableItemPostingVO(NumberUtil.roundDouble(unearnedAmount, 2), suspenseAccount, null, postAtDate, postAtDate, company, "Adjust membership suspense account for unearned and paid amount. Ref: " + transactionReference, containerID);
			} catch (PaymentException e) {
				throw new SystemException("Error creating posting to Suspense Account : ", e);
			}
			this.add(posting); // combine with existing suspense posting

			// global
			if (suspensePosting == null) {
				suspensePosting = posting;
			}

			posting = null;

			// add unearned to the posting total
			postingTotal += unearnedAmount;

		}

		LogUtil.debug(this.getClass(), "postingTotal after createSuspensePostings=" + postingTotal + " " + this);

	}

	/**
	 * The transaction group type code attached to the transaction group
	 */
	private String transactionGroupTypeCode;

	/**
	 * The transaction type code attached to the individual transaction
	 */
	private String transactionTypeCode;

	/**
	 * The unearned amount
	 */
	private double unearnedAmount;

	/**
	 * The net transaction fee
	 */
	private double netTxFee;

	/**
	 * Non discretionary adjustments - this applies to the PA only
	 * 
	 * @throws RemoteException
	 */
	private void createNonDiscretionaryDiscountPostings() throws RemoteException {
		LogUtil.log(this.getClass(), "createNonDiscretionaryDiscountPostings 1");
		LogUtil.log(this.getClass(), "postingTotal=" + postingTotal);
		if (transactionHasFees) {
			Collection adjustmentList = membershipTransaction.getAdjustmentList();
			if (adjustmentList != null) {
				Iterator adjustmentListIterator = adjustmentList.iterator();
				MembershipTransactionAdjustment adjustment = null;
				while (adjustmentListIterator.hasNext()) {
					adjustment = (MembershipTransactionAdjustment) adjustmentListIterator.next();
					LogUtil.debug(this.getClass(), "adjustment=" + adjustment);

					// don't process discretionary adjustments as they're
					// managed separately.
					if (!AdjustmentTypeVO.DISCRETIONARY_DISCOUNT.equals(adjustment.getAdjustmentTypeCode())) {
						BigDecimal adjustmentAmount = adjustment.getSignedAdjustmentAmount();
						// the adjustment needs to be added to the transaction
						// fee
						netTxFee += adjustmentAmount.doubleValue();
						LogUtil.debug(this.getClass(), "adjustmentAmount=" + adjustmentAmount);
						LogUtil.debug(this.getClass(), "netTxFee=" + netTxFee);

						// adjustment amount < 0. ie. a discount
						if (new BigDecimal(0).compareTo(adjustmentAmount) > 0) {

							if (adjustment.getAdjustmentTypeVO().getAdjustmentAccount() != null) {
								// discount adjustments should have the GST
								// removed
								// represented as a credit to the adjustment
								// account

								// TODO where is GST done?
								adjustmentAmount = CurrencyUtil.calculateGSTExclusiveAmount(adjustmentAmount.doubleValue(), gstRate, 2);
								LogUtil.debug(this.getClass(), "adjustmentAmount=" + adjustmentAmount);
								try {
									posting = new PayableItemPostingVO(adjustmentAmount.negate().doubleValue(), adjustment.getAdjustmentTypeVO().getAdjustmentAccount(), null, postAtDate, postAtDate, company, adjustment.getAdjustmentTypeVO().getDescription() + " for Membership " + membershipNumber, null);
								} catch (PaymentException e) {
									throw new SystemException("Error creating adjustment posting: ", e);
								}

								totalFee -= posting.getAmount();
								postingTotal += posting.getAmount();
								this.add(posting);

								// TODO this must currently rely on
								// usepostingadjustments to get it right?

							}

						}// adjustment > 0. eg quote adjustment up to a price
						else {

							if (adjustment.getAdjustmentTypeVO().getAdjustmentAccount() == null) {

								// TODO adjust income posting? has income
								// posting been created yet?
								double adjTotal = NumberUtil.roundDouble(incomePosting.getAmount() - adjustmentAmount.doubleValue(), 2);
								LogUtil.debug(this.getClass(), "adjTotal=" + adjTotal);
								try {
									incomePosting.setAmount(adjTotal);
								} catch (PaymentException e) {
									// TODO Auto-generated catch block
									throw new RemoteException("Unable to set amount on income posting.", e);
								}

								totalFee += adjustmentAmount.doubleValue();
								postingTotal -= adjustmentAmount.doubleValue();
							}

							adjustmentTotal += adjustmentAmount.doubleValue();

							LogUtil.debug(this.getClass(), "netTxFee=" + netTxFee);
							LogUtil.debug(this.getClass(), "totalFee=" + totalFee);
							LogUtil.debug(this.getClass(), "adjustmentAmount=" + adjustmentAmount);
							LogUtil.debug(this.getClass(), "adjustmentTotal=" + adjustmentTotal);
							LogUtil.debug(this.getClass(), "postingTotal after adjustments=" + postingTotal);
						}

					}
				}
			}
		}
		LogUtil.debug(this.getClass(), "createNonDiscretionartDiscountPostings 2" + this);
	}

	/**
	 * Create the postings to remove outstanding unearned.
	 * 
	 * @throws RemoteException
	 */
	private void createRemoveUnearnedPostings() throws RemoteException {
		PostingContainer postings = null;
		PayableItemVO oldPayableItem = null;
		Collection activeTransactionList = null;
		Iterator transIt = null;

		LogUtil.debug(this.getClass(), "transactionTypeCode = " + transactionTypeCode);
		LogUtil.debug(this.getClass(), "membershipTransaction.getTransactionTypeCode() = " + membershipTransaction.getTransactionTypeCode());

		boolean removedFromGroup = MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(membershipTransaction.getTransactionTypeCode());

		// Get the components that have outstanding earnings.
		// If the member is being removed from a group then everything should be
		// resolved here first.
		if (transactionHasFees || removedFromGroup) {
			try {
				if (oldMembership != null) {
					LogUtil.debug(this.getClass(), "oldMem = " + oldMembership);

					activeTransactionList = oldMembership.getCurrentTransactionList();

				}
			} catch (Exception e) {
				throw new SystemException(e);
			}
		}

		if (activeTransactionList != null) {
			// don't check for credits on renewals, transfers , creates? or
			// rejoins
			if (oldMembership != null) {
				membershipShortened = oldMembership.getExpiryDate().afterDay(newMembership.getExpiryDate());
			}
			if (oldMembership != null && transGroup.replacesTransaction()) {
				transIt = activeTransactionList.iterator();
				MembershipTransactionVO oldMemTxVO = null;

				// only one for replace type transactions
				while (transIt.hasNext()) {
					boolean fullyUnpaid = false;

					oldMemTxVO = (MembershipTransactionVO) transIt.next();
					// create the set of postings to remove the unearned and
					// unpaid amounts

					oldPayableItem = oldMemTxVO.getPayableItemForPA();

					if (oldPayableItem != null) {
						fullyUnpaid = oldPayableItem.isFullyUnpaid();
					}

					try {
						// if (fullyUnpaid)
						// {
						// //reverse all postings
						// postings =
						// (PostingContainer)paymentMgr.reverseAllPostings(oldMemTxVO,this.transactionTypeCode);
						// }
						// else
						// {

						// pro rata postings
						postings = (PostingContainer) paymentMgr.createCancelTypePostings(this.transactionReference.toString(), oldMemTxVO, this.transactionTypeCode, fullyUnpaid);

						LogUtil.debug(this.getClass(), "removedFromGroup = " + removedFromGroup);
						LogUtil.debug(this.getClass(), "posting sus = " + postings.getSuspenseAmountTotal());

						if (removedFromGroup) {
							// determine if change of expiry date by suspense
							// amount
							if (postings.getSuspenseAmountTotal() < 0) {
								LogUtil.debug(this.getClass(), "there is sus");
								// put in current transaction
								this.createRemoveFromGroupPostings(postings.getSuspenseAmountTotal(), oldMemTxVO, membershipTransaction);
							}
							// }

						}
						// "Outstanding unearned on membership. Ref: "
						LogUtil.debug(this.getClass(), "Remove unearned " + postings.toString());
					} catch (RollBackException ex) {
						throw new SystemException(ex);
					}

					// add the cancel type postings to the posting list.
					this.addAll(postings);

					// unearned amount is the total to the suspense account
					unearnedAmount += -postings.getSuspenseAmountTotal();

					// this is accounted for in the createSuspensePostings
					// section
					// postingTotal += unearnedAmount;

					LogUtil.debug(this.getClass(), "unearnedAmount=" + unearnedAmount);
					LogUtil.debug(this.getClass(), "postingTotal after unearnedAmount=" + postingTotal);

				}
			} // cancelled the unearned postings
		}

	}

	/**
	 * Create remove from group postings. Paid and earned is negative.
	 * 
	 * @param paidEarned double
	 * @param oldTransaction MembershipTransactionVO
	 * @throws RemoteException
	 */
	private void createRemoveFromGroupPostings(double paidEarned, MembershipTransactionVO oldTransaction, MembershipTransactionVO newTransaction) throws RemoteException {
		Account memSuspenseAccount = refMgr.getMembershipSuspenseAccount();
		Account unearnedAccount = oldTransaction.getAccount(MembershipTransactionVO.TYPE_UNEARNED);
		Account earnedAccount = oldTransaction.getAccount(MembershipTransactionVO.TYPE_EARNED);
		// int daysToEarn = newTransaction.getEffectiveRange().getTotalDays();

		LogUtil.debug(this.getClass(), "in sus");

		// suspense amount
		try {
			posting = new PayableItemPostingVO(-paidEarned, memSuspenseAccount, null, postAtDate, postAtDate, company, "Adjust membership suspense account. Ref: " + transactionReference, null);
		} catch (PaymentException e) {
			throw new SystemException("Error creating posting to Suspense Account : ", e);
		}
		this.add(posting);

		// unearned amount
		double creditUnearned = CurrencyUtil.calculateGSTExclusiveAmount(paidEarned, gstRate);
		try {
			posting = new PayableItemPostingVO(creditUnearned, unearnedAccount, earnedAccount, postAtDate, postAtDate, company, "Credit unearned account paid portion. Ref: " + transactionReference, null);

		} catch (PaymentException e) {
			throw new SystemException("Error creating posting to unearned account.", e);
		}
		this.add(posting);

		// gst amount
		double gstAmount = CurrencyUtil.calculateGSTincluded(paidEarned, gstRate);
		try {
			posting = new PayableItemPostingVO(gstAmount, gstAccount, null, postAtDate, postAtDate, company, "Credit gst account paid portion. Ref: " + transactionReference, null);

		} catch (PaymentException e) {
			throw new SystemException("Error creating posting to unearned account.", e);
		}
		this.add(posting);

		LogUtil.debug(this.getClass(), "after remove " + this.toString());
	}
}

/**
 * Class to represent a fee to account mapping.
 */
class FeeSpecificationAccounts {

	/**
	 * The earned account
	 */
	private MembershipAccountVO earnedAccount;

	/**
	 * The unearned account
	 */
	private MembershipAccountVO unearnedAccount;

	public MembershipAccountVO getEarnedAccount() {
		return earnedAccount;
	}

	public MembershipAccountVO getUnearnedAccount() {
		return unearnedAccount;
	}

	public void setUnearnedAccount(MembershipAccountVO unearnedAccount) {
		this.unearnedAccount = unearnedAccount;
	}

	public void setEarnedAccount(MembershipAccountVO earnedAccount) {
		this.earnedAccount = earnedAccount;
	}

	/**
	 * Get the fee mapping key
	 * 
	 * @return The key value
	 */
	public String getKey() {
		StringBuffer key = new StringBuffer();
		if (this.earnedAccount != null) {
			key.append(this.earnedAccount.getAccountID().toString());
		}

		key.append("_");

		if (this.unearnedAccount != null) {
			key.append(this.unearnedAccount.getAccountID().toString());
		}

		return key.toString();
	}

	public String toString() {
		String feeSpecString = "";
		if (this.unearnedAccount != null) {
			feeSpecString += this.unearnedAccount.toString();
		}
		if (this.earnedAccount != null) {
			feeSpecString += this.earnedAccount.toString();
		}
		return feeSpecString;
	}

}
