package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.user.UserSession;
import com.ract.util.LogUtil;
import com.ract.util.MethodCounter;

/**
 * A transaction container specifically for storing the transactions that
 * constitute an undo transaction.
 * 
 * @author bakkert
 * @created 14 January 2003
 */
public class UndoTransactionGroup extends TransactionContainer implements Serializable {

    private int contextTransactionIndex = -1;

    private int payeeTransactionIndex = -1;

    private UserSession userSession;

    /**
     * Holds a list of UndoTransaction objects. The UndoTransaction class is a
     * private inner class to this class.
     */
    private Vector transactionList = new Vector();

    /**
     * Constructor for the UndoTransactionGroup object
     * 
     * @param transID
     *            Description of the Parameter
     * @param uSession
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public UndoTransactionGroup(Integer transID, UserSession uSession) throws RemoteException {
	this.userSession = uSession;
	findTransactions(transID);
	LogUtil.log(this.getClass(), MethodCounter.outputCounts());
    }

    /**
     * Description of the Method
     * 
     * @param transID
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    private void findTransactions(Integer transID) throws RemoteException {
	UndoTransaction undoTransaction;
	// MembershipTransactionMgr memTransMgr =
	// MembershipEJBHelper.getMembershipTransactionMgr();
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	MembershipTransactionVO contextMemTransVO = membershipMgr.getMembershipTransaction(transID); // memTransMgr.getMembershipTransactionVO(transID);

	// Get the list of other membership transactions that were created at
	// the same time.
	int transGroupID = contextMemTransVO.getTransactionGroupID();
	if (transGroupID > 0) {
	    Collection transList = membershipMgr.findMembershipTransactionByTransactionGroupID(transGroupID);
	    // Hold the membership transactions in our private UndoTransaction
	    // class
	    MembershipTransactionVO memTransVO;
	    if (transList != null) {
		Iterator memTxIt = transList.iterator();
		while (memTxIt.hasNext()) {
		    memTransVO = (MembershipTransactionVO) memTxIt.next();
		    undoTransaction = new UndoTransaction(memTransVO, (memTransVO.getTransactionID().equals(contextMemTransVO.getTransactionID())));
		    this.transactionList.add(undoTransaction);
		}
	    }
	}

	// If no other transactions were involved just process the context
	// transaction.
	if (this.transactionList == null || this.transactionList.isEmpty()) {
	    undoTransaction = new UndoTransaction(contextMemTransVO, true);
	    this.transactionList.add(undoTransaction);
	}
    }

    /**
     * The payee transaction is the transaction with a payable item ID. There
     * should only be one in the transaction group. The payee will most likely
     * be the prime addressee of the membership group (if a group is involved)
     * but not always which is why we use this method and don't just find the
     * PA.
     * 
     * @return The transactionListSize value
     */
    // private void findPayeeTransaction() throws RemoteException
    // {
    // UndoTransaction undoTransaction;
    // for (int i = 0; i < this.transactionList.size() &&
    // this.payeeTransactionIndex == 0; i++)
    // {
    // undoTransaction = (UndoTransaction) this.transactionList.get(i);
    // if (undoTransaction.isPayee())
    // this.payeeTransactionIndex = i;
    // }
    // }

    public int getTransactionListSize() {
	return this.transactionList.size();
    }

    /**
     * Return the index to the context transaction
     * 
     * @return The contextTransactionIndex value
     */
    public int getContextTransactionIndex() {
	// Find the context transaction if it has not been found yet.
	if (this.contextTransactionIndex < 0) {
	    UndoTransaction undoTransaction;
	    for (int i = 0; i < this.transactionList.size() && this.contextTransactionIndex < 0; i++) {
		undoTransaction = (UndoTransaction) this.transactionList.get(i);
		if (undoTransaction.isContextTransaction()) {
		    this.contextTransactionIndex = i;
		}
	    }
	}
	return this.contextTransactionIndex;
    }

    /**
     * Return the index to the payee transaction
     * 
     * @return Description of the Return Value
     */
    public int xxxgetPayeeTransactionIndex() {
	if (this.payeeTransactionIndex < 0) {
	    UndoTransaction undoTransaction;
	    for (int i = 0; i < this.transactionList.size() && this.payeeTransactionIndex < 0; i++) {
		undoTransaction = (UndoTransaction) this.transactionList.get(i);
		if (undoTransaction.isPayee()) {
		    this.payeeTransactionIndex = i;
		}
	    }
	}
	return this.payeeTransactionIndex;
    }

    /**
     * Return the membership state that is being undone.
     * 
     * @param index
     *            Description of the Parameter
     * @return The undoMembershipVO value
     */
    public MembershipVO getUndoMembershipVO(int index) {
	UndoTransaction undoTransaction = (UndoTransaction) this.transactionList.get(index);
	return undoTransaction.undoMemVO;
    }

    /**
     * Return the membership state that is being reverted to. Will be null if
     * the transaction being undone is a create transaction or transfer
     * transaction or the membership state to revert to could not be obtained.
     * 
     * @param index
     *            Description of the Parameter
     * @return The revertToMembershipVO value
     */
    public MembershipVO getRevertToMembershipVO(int index) {
	UndoTransaction undoTransaction = (UndoTransaction) this.transactionList.get(index);
	return undoTransaction.revertToMemVO;
    }

    /**
     * Return the membership transaction that is being reverted to. Will be null
     * if the transaction being undone is a create transaction.
     * 
     * @param index
     *            Description of the Parameter
     * @return The revertToTransactionVO value
     */
    public MembershipTransactionVO getRevertToTransactionVO(int index) {
	UndoTransaction undoTransaction = (UndoTransaction) this.transactionList.get(index);
	return undoTransaction.revertToTransVO;
    }

    /**
     * Return the membership transaction that is being undone.
     * 
     * @param index
     *            Description of the Parameter
     * @return The undoTransactionVO value
     */
    public MembershipTransactionVO getUndoTransactionVO(int index) {
	UndoTransaction undoTransaction = (UndoTransaction) this.transactionList.get(index);
	return undoTransaction.undoTransVO;
    }

    /**
     * Return the undo failure reason list for the specified transaction in the
     * list. The reason list will be empty if the transaction can be undone.
     * 
     * @param index
     *            Description of the Parameter
     * @return The undoFailureReasonList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getUndoFailureReasonList(int index) throws RemoteException {
	UndoTransaction undoTransaction = (UndoTransaction) this.transactionList.get(index);
	return undoTransaction.getUndoFailureReasonList();
    }

    /**
     * Gets the userSession attribute of the UndoTransactionGroup object
     * 
     * @return The userSession value
     */
    public UserSession getUserSession() {
	return this.userSession;
    }

    /**
     * Return true if the transaction at the specified index is a payee
     * transaction
     * 
     * @param index
     *            Description of the Parameter
     * @return The payeeTransaction value
     */
    public boolean isPayeeTransaction(int index) {
	return ((UndoTransaction) this.transactionList.get(index)).isPayee();
    }

    /**
     * Return true if the transaction can be undone. The transaction can be
     * undone if all of the transactions in the transaction list can be undone.
     * 
     * @return The undoable value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean isUndoable() throws RemoteException {
	boolean undoable = true;
	for (int i = 0; i < this.transactionList.size() && undoable; i++) {
	    undoable = ((UndoTransaction) this.transactionList.get(i)).isUndoable();
	}
	return undoable;
    }

    /**
     * Submit the transaction for processing.
     * 
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection submit() throws RemoteException {
	Collection transactionList = null;
	if (!this.submitted) {
	    MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
	    try {
		this.submitted = true;
		transactionList = memTransMgr.processTransaction(this);
	    } catch (Exception e) {
		this.submitted = false;
		if (e instanceof ValidationException) {
		    throw (ValidationException) e;
		} else if (e instanceof SystemException) {
		    throw (SystemException) e;
		} else if (e instanceof RemoteException) {
		    throw (RemoteException) e;
		} else {
		    throw new SystemException(e);
		}
	    }
	}
	return transactionList;
    }
}