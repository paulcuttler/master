package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;

/**
 * <p>
 * Wrapper class around
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class MembershipGroupVO extends ValueObject implements Writable {
    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "MembershipGroup";

    protected Integer membershipGroupID;

    protected DateTime membershipGroupExpiryDate;

    protected String membershipGroupProductCode;

    protected ProductVO membershipGroupProduct;

    protected MembershipGroupDetailVO membershipGroupDetailForPA;

    protected Collection membershipGroupDetailList;

    public MembershipGroupVO() {
    }

    // Data constructor
    public MembershipGroupVO(Collection groupDetailList) throws RemoteException {
	setMembershipGroupDetailList(groupDetailList);
    }

    /**
     * Return the exception messages concatenated together. Return an empty
     * string if there are no exceptions.
     * 
     * @return The membershipGroupExceptionMessages value
     */
    public String getMembershipGroupExceptionMessages() {
	StringBuffer msg = new StringBuffer();
	Vector exceptionList = getExceptionList();
	if (!exceptionList.isEmpty()) {
	    Exception mge;
	    for (int i = 0; i < exceptionList.size(); i++) {
		mge = (Exception) exceptionList.get(i);
		if (i == 0) {
		    msg.append(mge.getMessage());
		} else {
		    msg.append(" : " + mge.getMessage());
		}
	    }
	}
	return msg.toString();
    }

    public Integer getMembershipGroupID() {
	return membershipGroupID;
    }

    public DateTime getMembershipGroupExpiryDate() {
	return membershipGroupExpiryDate;
    }

    /**
     * Return the date that the member joined the membership group. Returns null
     * if the member is not in the membership group.
     * 
     * @param membershipID
     *            Description of the Parameter
     * @return The groupJoinDate value
     */
    public DateTime getGroupJoinDate(Integer membershipID) {
	DateTime joinDate = null;
	MembershipGroupDetailVO memGroupDetailVO;
	Iterator iterator = this.membershipGroupDetailList.iterator();
	while (iterator.hasNext() && joinDate == null) {
	    memGroupDetailVO = (MembershipGroupDetailVO) iterator.next();
	    if (memGroupDetailVO.getMembershipGroupDetailPK().getMembershipID().equals(membershipID)) {
		joinDate = memGroupDetailVO.getGroupJoinDate();
	    }
	}
	return joinDate;
    }

    public String getMembershipGroupProductCode() {
	return membershipGroupProductCode;
    }

    /**
     * Return the status of the membership group. This is the status of the
     * membership for the PA of the group.
     * 
     * @return The membershipGroupStatus value
     * @exception RemoteException
     *                Description of the Exception
     */
    public String getMembershipGroupStatus() throws RemoteException {
	return getMembershipGroupDetailForPA().getMembership().getStatus();
    }

    public ProductVO getMembershipGroupProduct() throws RemoteException {
	if (this.membershipGroupProduct == null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.membershipGroupProduct = refMgr.getProduct(this.membershipGroupProductCode);

	    if (this.membershipGroupProduct == null) {
		throw new RemoteException("Failed to get product details for product code " + this.membershipGroupProductCode);
	    }
	}
	return membershipGroupProduct;
    }

    /**
     * Get the group header record.
     * 
     * @throws RemoteException
     * @return GroupVO
     */
    public GroupVO getGroup() throws RemoteException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	GroupVO groupVO = null;
	try {
	    LogUtil.log(this.getClass(), "this.membershipGroupID=" + this.membershipGroupID);
	    groupVO = membershipMgr.getGroupByGroupId(this.membershipGroupID);
	} catch (RemoteException ex) {
	    // no action
	    LogUtil.warn(this.getClass(), ex.getMessage());
	}
	return groupVO;
    }

    /**
     * Return the group detail for the specified membership. Returns null if the
     * member is not in the membership group.
     * 
     * @param membershipID
     *            Description of the Parameter
     * @return The membershipGroupDetail value
     */
    public MembershipGroupDetailVO getMembershipGroupDetail(Integer membershipID) {
	MembershipGroupDetailVO tmpDetailVO = null;
	MembershipGroupDetailVO memGroupDetailVO = null;
	Iterator iterator = this.membershipGroupDetailList.iterator();
	while (iterator.hasNext() && memGroupDetailVO == null) {
	    tmpDetailVO = (MembershipGroupDetailVO) iterator.next();
	    if (tmpDetailVO.getMembershipGroupDetailPK().getMembershipID().equals(membershipID)) {
		memGroupDetailVO = tmpDetailVO;
	    }
	}
	return memGroupDetailVO;
    }

    public MembershipGroupDetailVO getMembershipGroupDetailForPA() {
	return membershipGroupDetailForPA;
    }

    public Collection getMembershipGroupDetailList() {
	LogUtil.debug(this.getClass(), "getMembershipGroupDetailList=" + membershipGroupDetailList);
	return membershipGroupDetailList;
    }

    /**
     * Return the number of memberships that are in the membership group.
     * 
     * @return The memberCount value
     */
    public int getMemberCount() {
	if (this.membershipGroupDetailList != null) {
	    return this.membershipGroupDetailList.size();
	} else {
	    return 0;
	}
    }

    /**
     * Return true if the membership is the prime addressee of the membership
     * group.
     * 
     * @param membershipID
     *            Description of the Parameter
     * @return The primeAddressee value
     */
    public boolean isPrimeAddressee(Integer membershipID) throws RemoteException {
	boolean isPA = false;

	// MembershipMgr mMgr = MembershipEJBHelper.getMembershipMgr();
	// isPA = mMgr.isPrimeAddressee(membershipID);
	if (this.membershipGroupDetailForPA != null) {
	    isPA = this.membershipGroupDetailForPA.getMembershipGroupDetailPK().getMembershipID().equals(membershipID);
	}

	return isPA;
    }

    /**
     * Return true if the membership is a member of the membership group.
     * 
     * @param membershipID
     *            Description of the Parameter
     * @return The membershipInMembershipGroup value
     */
    public boolean isMembershipInMembershipGroup(Integer membershipID) {
	boolean found = false;
	MembershipGroupDetailVO memGroupDetailVO;
	Iterator iterator = this.membershipGroupDetailList.iterator();
	while (!found && iterator.hasNext()) {
	    memGroupDetailVO = (MembershipGroupDetailVO) iterator.next();
	    found = memGroupDetailVO.getMembershipGroupDetailPK().getMembershipID().equals(membershipID);
	}
	return found;
    }

    public boolean isMembershipGroupExpired() {
	return isMembershipGroupExpired(new DateTime());
    }

    public boolean isMembershipGroupExpired(DateTime effectiveDate) {
	return this.membershipGroupExpiryDate.beforeDay(effectiveDate);
    }

    public void setMembershipGroupID(Integer groupID) throws RemoteException {
	checkReadOnly();
	if (this.membershipGroupDetailList != null) {
	    throw new SystemException("The identity of the membership group cannot be changed once the group details have been set.");
	}

	this.membershipGroupID = groupID;
    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(this.membershipGroupDetailForPA);
	desc.append(" ");
	desc.append(this.membershipGroupExpiryDate);
	desc.append(" ");
	desc.append(this.membershipGroupID);
	desc.append(" ");
	desc.append(this.membershipGroupProductCode);
	desc.append(" ");
	desc.append(this.membershipGroupDetailList == null ? "" : String.valueOf(this.membershipGroupDetailList.size()));
	return desc.toString();
    }

    public void setMembershipGroupDetailList(Collection groupDetailList) throws RemoteException {
	LogUtil.debug(this.getClass(), "setMembershipGroupDetailList " + (groupDetailList != null ? groupDetailList.size() : 0));
	checkReadOnly();
	LogUtil.debug(this.getClass(), "g1");
	if (groupDetailList == null || groupDetailList.isEmpty()) {
	    addException(new MembershipGroupException("The membership group detail list is empty."));
	    return;
	}
	LogUtil.debug(this.getClass(), "g2");
	// Clear the derived membership group attributes in case they were
	// previously set.
	this.membershipGroupID = null;
	this.membershipGroupDetailForPA = null;
	this.membershipGroupExpiryDate = null;
	this.membershipGroupProduct = null;
	this.membershipGroupProductCode = null;

	LogUtil.debug(this.getClass(), "g3");
	MembershipGroupDetailVO groupDetVO;
	Iterator iterator = groupDetailList.iterator();
	while (iterator.hasNext()) {
	    groupDetVO = (MembershipGroupDetailVO) iterator.next();
	    LogUtil.debug(this.getClass(), "g3x groupDetVOhistory=" + groupDetVO.isHistory());
	    // Determine from the detail objects if the membership group should
	    // be
	    // in history mode.
	    if (groupDetVO.isHistory() && this.isNotHistory()) {
		this.setMode(MODE_HISTORY);
	    }

	    LogUtil.debug(this.getClass(), "g4 history=" + isHistory());

	    // Identify the membership group ID and make sure the list only
	    // contains
	    // the details of one membership group.
	    if (this.membershipGroupID == null) {
		LogUtil.debug(this.getClass(), "g4a");
		this.membershipGroupID = groupDetVO.getMembershipGroupDetailPK().getGroupID();
	    }
	    // TODO how can this occur?
	    else if (groupDetVO.getMembershipGroupDetailPK().getGroupID() == null) {
		LogUtil.debug(this.getClass(), "g4b");
		groupDetVO.getMembershipGroupDetailPK().setGroupID(this.membershipGroupID);
	    } else if (!this.membershipGroupID.equals(groupDetVO.getMembershipGroupDetailPK().getGroupID())) {
		LogUtil.debug(this.getClass(), "g4c");
		addException(new MembershipGroupException("The membership group detail list contains details for different membership groups. " + this.membershipGroupID + " and " + groupDetVO.getMembershipGroupDetailPK().getGroupID()));
		return;
	    }
	    LogUtil.debug(this.getClass(), "g5" + this.membershipGroupExpiryDate);
	    // Extract the membership groups expiry date and make sure that all
	    // memberships have the same expiry date.
	    try {
		DateTime expiryDate = null;
		if (this.membershipGroupExpiryDate == null) {
		    if (this.isNotHistory()) {
			LogUtil.debug(this.getClass(), "g5a1");
			expiryDate = DateUtil.clearTime(groupDetVO.getMembership().getExpiryDate());
			LogUtil.debug(this.getClass(), "g5a");
		    }
		    this.membershipGroupExpiryDate = expiryDate;
		}

		if (this.membershipGroupExpiryDate != null && expiryDate != null && !this.membershipGroupExpiryDate.equals(expiryDate)) {
		    LogUtil.debug(this.getClass(), "g5b");
		    addException(new MembershipGroupException("The membership group detail list for membership group ID " + this.membershipGroupID + " contains memberships with different expiry dates."));
		    LogUtil.debug(this.getClass(), "g5bb");
		}
		LogUtil.debug(this.getClass(), "g6");
	    } catch (RemoteException ex) {
		throw new SystemException("erorr", ex);
	    }
	    // Extract the prime addressee of the membership group and make sure
	    // that only one has been defined.
	    if (this.membershipGroupDetailForPA == null && groupDetVO.isPrimeAddressee()) {
		this.membershipGroupDetailForPA = groupDetVO;
	    } else if (this.membershipGroupDetailForPA != null && groupDetVO.isPrimeAddressee()) {
		addException(new MembershipGroupException("The membership group ID " + this.membershipGroupID + " has at least two prime addressees defined. Membership ID " + this.membershipGroupDetailForPA.getMembershipGroupDetailPK().getMembershipID() + " and membership ID " + groupDetVO.getMembershipGroupDetailPK().getMembershipID()));
	    }
	    LogUtil.debug(this.getClass(), "g7");

	    String productCode = null;

	    // Extract the base product of the membership group and make sure
	    // that
	    // all memberships share the same base product.
	    if (this.membershipGroupProductCode == null) {
		if (this.isNotHistory()) {
		    productCode = groupDetVO.getMembership().getProductCode();
		}
		this.membershipGroupProductCode = productCode;
	    }

	    if (productCode != null && this.membershipGroupProductCode != null && !this.membershipGroupProductCode.equals(productCode)) {
		addException(new MembershipGroupException("The memberships in the membership group " + this.membershipGroupID + " do not all have the same product."));
	    }

	    LogUtil.debug(this.getClass(), "g8");

	}

	// Make sure the group ID was identified.
	if (this.membershipGroupID == null) {
	    addException(new MembershipGroupException("The membership group ID could not be identified."));
	}
	LogUtil.debug(this.getClass(), "g10");
	// Make sure the group expiry date was identified.
	if (this.membershipGroupExpiryDate == null && this.isNotHistory()) {
	    addException(new MembershipGroupException("The expiry date for membership group " + this.membershipGroupID + " could not be identified."));
	}
	LogUtil.debug(this.getClass(), "g11");
	// Make sure the PA for the group was identified.
	if (this.membershipGroupDetailForPA == null) {
	    addException(new MembershipGroupException("The prime addressee for membership group " + this.membershipGroupID + " has not been defined."));
	}
	LogUtil.debug(this.getClass(), "g12");
	// Make sure that the product for the membership group has been
	// identified.
	if (this.membershipGroupProductCode == null && this.isNotHistory()) {
	    addException(new MembershipGroupException("The product for membership group " + this.membershipGroupID + " could not be identified."));
	}
	LogUtil.debug(this.getClass(), "g13");
	// All ok so set the list of membership group details
	this.membershipGroupDetailList = groupDetailList;

    }

    /**
     * Return a read only value object of this membership group.
     * 
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipGroupVO copy() throws RemoteException {
	MembershipGroupVO memGroupVO = new MembershipGroupVO(this.membershipGroupDetailList);
	memGroupVO.setReadOnly(true);
	return memGroupVO;
    }

    /**
     * If the PA for the group has not been set then arbitrarily set one.
     * 
     * @exception RemoteException
     *                Description of the Exception
     */
    public void fixProblems() throws RemoteException {
	if (this.membershipGroupDetailForPA == null) {
	    if (this.membershipGroupDetailList == null || this.membershipGroupDetailList.isEmpty()) {
		throw new SystemException("Failed to fix membership group.  Detail list is empty!");
	    } else {
		// Try and find a member who has a Direct Debit Authority
		Iterator detailIterator = this.membershipGroupDetailList.iterator();
		MembershipGroupDetailVO memGroupDetailVO = null;
		while (detailIterator.hasNext()) {
		    memGroupDetailVO = (MembershipGroupDetailVO) detailIterator.next();
		    if (!memGroupDetailVO.isHistory()) {
			if (memGroupDetailVO.getMembership().getDirectDebitAuthority() != null) {
			    this.membershipGroupDetailForPA = memGroupDetailVO;
			}
		    }
		}

		// If the PA has still not been determined then use the last
		// member accessed.
		if (this.membershipGroupDetailForPA == null) {
		    this.membershipGroupDetailForPA = memGroupDetailVO;
		}

		if (this.membershipGroupDetailForPA != null) {
		    this.membershipGroupDetailForPA.setPrimeAddressee(true);
		}
	    }
	}
    }

    /**
     * Writable interface methods ***********************
     * 
     */

    /**
     * Write the classes data using the specified class writter.
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	LogUtil.debug(this.getClass(), "getMembershipGroupExceptionMessages=" + getMembershipGroupExceptionMessages());
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttributeList(WRITABLE_CLASSNAME, getMembershipGroupDetailList());
	cw.endClass();
    }

}
