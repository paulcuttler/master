package com.ract.membership;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.DataSourceFactory;
import com.ract.common.SequenceMgr;
import com.ract.util.ConnectionUtil;

@Stateless
@Remote({ MembershipIDMgr.class })
public class MembershipIDMgrBean {

    /**
     * Return the next ID in the Membership Document ID sequence.
     */
    public int getNextDocumentID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_MEM_DOCUMENT);
	// return MembershipIDCache.getInstance().getNextDocumentID(this);
    }

    /**
     * Return the next ID in the Membership Group ID sequence.
     */
    public int getNextGroupID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_MEM_GROUP);
	// return MembershipIDCache.getInstance().getNextGroupID(this);
    }

    /**
     * Return the next ID in the Membership ID sequence.
     */
    public int getNextMembershipID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_MEM_MEMBERSHIP);
	// return MembershipIDCache.getInstance().getNextMembershipID(this);
    }

    /**
     * Return the next quote number in the quote number sequence.
     */
    public int getNextQuoteNumber() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_MEM_QUOTE);
	// return MembershipIDCache.getInstance().getNextQuoteNumber(this);
    }

    /**
     * Return the next ID in the Membership transaction ID sequence.
     */
    public int getNextTransactionID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_MEM_TRANSACTION);
	// return MembershipIDCache.getInstance().getNextTransactionID(this);
    }

    /**
     * Return the next ID in the Membership transaction group ID sequence.
     */
    public int getNextTransactionGroupID() throws RemoteException {
	return CommonEJBHelper.getSequenceMgr().getNextID(SequenceMgr.SEQUENCE_MEM_TRANSACTION_GROUP);
	// return
	// MembershipIDCache.getInstance().getNextTransactionGroupID(this);
    }

    /**
     * Reset all of the ID so that they are initialised again.
     */
    public void reset() {
	MembershipIDCache.getInstance().reset();
    }

    /**************************** SQL methods ********************************/

    public int getLastDocumentID() throws RemoteException {
	int lastID = 0;
	Connection connection = null;
	String statementText = "select max(document_id) from PUB.mem_document";
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = getDataSource();
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		lastID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return lastID;
    }

    public int getLastGroupID() throws RemoteException {
	int lastID = 0;
	Connection connection = null;
	String statementText = "select max(group_id) from PUB.mem_membership_group";
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = getDataSource();
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		lastID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return lastID;
    }

    public int getLastMembershipID() throws RemoteException {
	int lastID = 0;
	Connection connection = null;
	String statementText = "select max(membership_id) from PUB.mem_membership";
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = getDataSource();
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		lastID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return lastID;
    }

    public int getLastQuoteNumber() throws RemoteException {
	int lastID = 0;
	Connection connection = null;
	String statementText = "select max(quote_number) from PUB.mem_membership_quote";
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = getDataSource();
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		lastID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return lastID;
    }

    public int getLastTransactionID() throws RemoteException {
	int lastID = 0;
	Connection connection = null;
	String statementText = "select max(transaction_id) from PUB.mem_transaction";
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = getDataSource();
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		lastID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return lastID;
    }

    public int getLastTransactionGroupID() throws RemoteException {
	int lastID = 0;
	Connection connection = null;
	String statementText = "select max(transaction_group_id) from PUB.mem_transaction";
	PreparedStatement statement = null;
	try {
	    DataSource dataSource = getDataSource();
	    connection = dataSource.getConnection();
	    statement = connection.prepareStatement(statementText);
	    ResultSet resultSet = statement.executeQuery();
	    if (resultSet.next()) {
		lastID = resultSet.getInt(1);
	    }
	} catch (SQLException e) {
	    throw new EJBException("Error executing SQL " + statementText + " : " + e.toString());
	} finally {
	    ConnectionUtil.closeStatement(statement);
	    ConnectionUtil.closeConnection(connection);
	}
	return lastID;
    }

    /**
     * Return a reference to the data source for the database.
     */
    private DataSource getDataSource() throws RemoteException {
	DataSource dataSource = null;
	if (dataSource == null) {
	    try {
		dataSource = DataSourceFactory.getDataSource(CommonConstants.DATASOURCE_CLIENT);
	    } catch (Exception e) {
		throw new EJBException("Error looking up dataSource: " + e.toString());
	    }
	}
	return dataSource;
    }

}
