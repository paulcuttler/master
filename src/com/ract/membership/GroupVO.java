package com.ract.membership;

import com.ract.common.PostalAddressVO;
import com.ract.common.ValueObject;

/**
 * <p>
 * The group value object
 * </p>
 * 
 * @hibernate.class table="mem_group" lazy="false"
 * @author jyh
 * @version 1.0
 */
public class GroupVO extends ValueObject {
    protected Integer groupId;
    protected String groupTypeCode;
    protected String groupName;

    /**
     * //TODO change to propertyQualifier
     */
    protected String billingAddressPropertyQualifier;
    protected Integer billingAddressStreetSuburbId;
    protected String contactPerson;

    protected String billingAddressProperty;

    public GroupVO() {
    }

    public GroupVO(Integer groupId, String groupTypeCode, String groupName, String billingAddressProperty, String billingAddressPropertyQualifier, Integer billingAddressStreetSuburbId, String contactPerson) {
	this.groupId = groupId;
	this.groupTypeCode = groupTypeCode;
	this.groupName = groupName;
	this.billingAddressProperty = billingAddressProperty;
	this.billingAddressPropertyQualifier = billingAddressPropertyQualifier;
	this.billingAddressStreetSuburbId = billingAddressStreetSuburbId;
	this.contactPerson = contactPerson;
    }

    public final static String GROUP_TYPE_CORPORATE = "Corporate";

    public GroupVO copy() {
	GroupVO groupVO = new GroupVO(this.groupId, this.groupTypeCode, this.getGroupName(), this.getBillingAddressProperty(), this.getBillingAddressPropertyQualifier(), this.getBillingAddressStreetSuburbId(), this.getContactPerson());
	return groupVO;
    }

    /**
     * Get the actual billing address.
     * 
     * @return PostalAddressVO
     */
    public PostalAddressVO getBillingAddress() {
	return new PostalAddressVO(this.getBillingAddressProperty(), this.getBillingAddressPropertyQualifier(), this.getBillingAddressStreetSuburbId());
    }

    /**
     * @hibernate.id column="group_id" generator-class="assigned"
     */
    public Integer getGroupId() {
	return groupId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="group_type_code"
     */
    public String getGroupTypeCode() {
	return groupTypeCode;
    }

    public void setGroupTypeCode(String groupTypeCode) {
	this.groupTypeCode = groupTypeCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="group_name"
     */
    public String getGroupName() {
	return groupName;
    }

    public void setGroupName(String groupName) {
	this.groupName = groupName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="billing_address_street_number"
     */
    public String getBillingAddressPropertyQualifier() {
	return billingAddressPropertyQualifier;
    }

    public void setBillingAddressPropertyQualifier(String billingAddressPropertyQualifier) {
	this.billingAddressPropertyQualifier = billingAddressPropertyQualifier;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="billing_address_street_suburb_id"
     */
    public Integer getBillingAddressStreetSuburbId() {
	return billingAddressStreetSuburbId;
    }

    public void setBillingAddressStreetSuburbId(Integer billingAddressStreetSuburbId) {
	this.billingAddressStreetSuburbId = billingAddressStreetSuburbId;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="contact_person"
     */
    public String getContactPerson() {
	return contactPerson;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="billing_address_property"
     */
    public String getBillingAddressProperty() {
	return billingAddressProperty;
    }

    public void setContactPerson(String contactPerson) {
	this.contactPerson = contactPerson;
    }

    public void setBillingAddressProperty(String billingAddressProperty) {
	this.billingAddressProperty = billingAddressProperty;
    }

    public void setGroupId(Integer groupId) {
	this.groupId = groupId;
    }

}
