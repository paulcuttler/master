package com.ract.membership;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.ract.common.SystemParameterVO;
import com.ract.payment.PayableItemPostingVO;
import com.ract.payment.PaymentMgrLocal;
import com.ract.util.DateTime;
import com.ract.util.Interval;

/**
 * List 2310 postings per client.
 * 
 * @author newtong
 * 
 */
public class RoadsideLedgerReportPostingsAction extends ActionSupport {

    @InjectEJB(name = "PaymentMgrBean")
    PaymentMgrLocal paymentMgrLocal;

    private Integer clientNumber;
    private List<PayableItemPostingVO> postings;

    private String start;
    private String end;

    protected DateTime startDate;
    protected DateTime endDate;

    public String execute() throws Exception {

	postings = paymentMgrLocal.getPayableItemPostingsByClientAndAccount(clientNumber, SystemParameterVO.CREDIT_ACCOUNT, startDate, endDate);

	return SUCCESS;
    }

    public void validate() {
	if (start != null && end != null) {

	    try {
		startDate = new DateTime(start);
	    } catch (Exception e) {
		this.addFieldError("startDate", "Start date is not a valid date");
		return;
	    }

	    try {
		endDate = new DateTime(end);
	    } catch (Exception e) {
		this.addFieldError("endDate", "End date is not a valid date");
		return;
	    }

	    if (startDate.after(endDate)) {
		this.addFieldError("startDate", "Start date must be before end date");
		return;
	    }

	    try {
		DateTime oneYear = new DateTime(start);
		oneYear = oneYear.add(new Interval("1:0:0:0:0:0"));
		if (endDate.after(oneYear)) {
		    this.addFieldError("startDate", "Date range cannot be longer than one year");
		}
	    } catch (Exception e) {
	    }

	}
    }

    public String getStart() {
	return start;
    }

    // @RequiredStringValidator(message = "Please provide a start date",
    // shortCircuit = true)
    public void setStart(String start) {
	this.start = start;
    }

    public String getEnd() {
	return end;
    }

    // @RequiredStringValidator(message = "Please provide an end date",
    // shortCircuit = true)
    public void setEnd(String end) {
	this.end = end;
    }

    public Integer getClientNumber() {
	return clientNumber;
    }

    @RequiredFieldValidator(message = "Please provide a client number")
    public void setClientNumber(Integer clientNumber) {
	this.clientNumber = clientNumber;
    }

    public List<PayableItemPostingVO> getPostings() {
	return postings;
    }

    public void setPostings(List<PayableItemPostingVO> postings) {
	this.postings = postings;
    }

}
