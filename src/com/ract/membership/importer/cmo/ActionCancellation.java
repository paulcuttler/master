package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;

import com.ract.common.ReferenceDataVO;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipRefMgrLocal;
import com.ract.util.DateTime;

public class ActionCancellation extends Action {

	private MembershipMgr memMgr;
	private MembershipRefMgr refMgr;
	
	public ActionCancellation(MembershipMgr mem, MembershipRefMgr rf) {
		super();
		this.memMgr = mem;
		this.refMgr = rf;
	}
	
	@Override
	public void execute(CMOLineItem lineItem) {
		// TODO Auto-generated method stub
		CMOMembership membership = CMOMembership.FindByVin(this.memMgr, lineItem.getVin(), true);
		
		if(membership.exists()) {
		
			this.cancelMembership(membership, lineItem);
			
			this.result.add((new DateTime()).formatLongDate() 
							+ ": Cancelled Membership (T" + lineItem.get(CMOLineItem.FIELD_TRANS_TYPE) 
							+ ") " + membership.getClientNumber() 
							+ " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
			
		} else {
			this.error.add((new DateTime()).formatLongDate() 
							+ ": Unable to find membership by VIN: " 
							+ lineItem.get(CMOLineItem.FIELD_VIN) + " for Nomination ID: "
							+ lineItem.get(CMOLineItem.FIELD_NOMINATION_ID) + ", VIN: " 
							+ lineItem.get(CMOLineItem.FIELD_VIN) + ", Product Code: " 
							+ lineItem.get(CMOLineItem.FIELD_PROGRAM_CODE));
		}
	}
	
	private void cancelMembership(CMOMembership membership, CMOLineItem lineItem) {
		try {
			
			ReferenceDataVO cancellationReason = this.refMgr.getCancelReason("OTH");
			membership.cancel(cancellationReason);
			
		} catch (RemoteException e) {
			
			this.error.add((new DateTime()).formatLongDate() 
					   + ": unable to cancel membership : (T" + lineItem.getTransactionType() + ") " 
					   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}
	}
}
