package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import sun.util.logging.resources.logging;

import com.ract.client.ClientVO;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SystemException;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.TransactionGroup;
import com.ract.membership.TransactionGroupFactory;
import com.ract.security.SecurityHelper;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class CMOMembership {
	
	private MembershipMgr memMgr;
	private MembershipVO memVO;
	private CMOLineItem lineItem;
	
	/**
	 * Get a membership by Vin number
	 * 
	 * @param membershipMgr
	 * @param vin
	 * @param includeCancelled
	 * @return
	 */
	public static CMOMembership FindByVin(MembershipMgr memMgr2, String vin, boolean includeCancelled) {
		List<MembershipVO> memList;
		CMOMembership mem;
		
		try {
			
			memList = memMgr2.findMembershipByVin(vin, ProductVO.PRODUCT_CMO);
			
			//We got at least 1 membership. 
			if (memList != null && !memList.isEmpty()) {
				MembershipVO memVO = memList.get(0);
				mem = new CMOMembership(memMgr2, memVO);
			}
			else {
				//First check if we are looking to include cancelled memberships 
				if(includeCancelled) {
					memList = memMgr2.findMembershipByVin(vin, ProductVO.PRODUCT_CMO, MembershipVO.STATUS_CANCELLED);
					
					//We only want to incude a cancelled memberships if there is one, 
					//otherwise pretend that none of the cancelled memberships exist.
					if (memList != null && memList.size() != 1) {
						mem = new CMOMembership(memMgr2);
					} else {
						MembershipVO memVO = memList.get(0);
						mem = new CMOMembership(memMgr2, memVO);
					}
				} else {
					mem = new CMOMembership(memMgr2);
				}
			}
			
		} catch (RemoteException e) {
			mem = new CMOMembership(memMgr2);
		}
		
		return mem;
	}
	
	/** 
	 * Build an new Membership object. It is responsible for storing the interface for working with memberships
	 * 
	 * @param membershipMgr
	 */
	private CMOMembership(MembershipMgr membershipMgr) {
		this.memMgr = membershipMgr;
		this.memVO = null;
	}
	
	/**
	 * Construct a new membership from a CMOLineItem. We cannot create the actual membership
	 * beacuse we still do not have a client number. this will be provided in the call to create.
	 * 
	 * @param membershipMgr
	 * @param lineItem
	 */
	public CMOMembership(MembershipMgr membershipMgr, CMOLineItem lineItem) {
		this.memMgr = membershipMgr;
		this.lineItem = lineItem;
	}
	
	/** 
	 * Build an new Membership object. It is responsible for storing the interface for working with memberships
	 * 
	 * @param membershipMgr
	 */
	private CMOMembership(MembershipMgr membershipMgr, MembershipVO memVO) {
		this.memMgr = membershipMgr;
		this.memVO = memVO;
	}
	
	
	/**
	 * Create a new membership from a line item, but where we haven't injected the lineItem yet.
	 * 
	 * @param line
	 */
	public void create(Integer clientNumber, CMOLineItem lineItem) throws RemoteException {
		this.lineItem = lineItem;
		this.create(clientNumber);
	}
	
	/**
	 * Create a new membership from a line item
	 * 
	 * @param line
	 */
	public void create(Integer clientNumber) throws RemoteException {
		this.memMgr.createCMOMembership(
			clientNumber, 
			null, 
			this.lineItem.mustGetNominationDate(), 
			this.lineItem.get(CMOLineItem.FIELD_REGO), 
			this.lineItem.get(CMOLineItem.FIELD_VIN), 
			this.lineItem.get(CMOLineItem.FIELD_MAKE), 
			this.lineItem.get(CMOLineItem.FIELD_MODEL), 
			this.lineItem.get(CMOLineItem.FIELD_YEAR),
			this.lineItem.get(CMOLineItem.FIELD_PROGRAM_CODE), null
		);
		
		ArrayList<MembershipVO> memList = (ArrayList<MembershipVO>) this.memMgr.findMembershipByVin(this.lineItem.get(CMOLineItem.FIELD_VIN), ProductVO.PRODUCT_CMO);
		
		//We got at least 1 membership. 
		if (!memList.isEmpty()) {
			MembershipVO memVO = memList.get(0);
			this.memVO = memVO;
		}
	}
	
	/**
	 * Get the internal MembershipVO
	 * 
	 * @return
	 */
	public MembershipVO get() {
		return this.memVO;
	}
	
	/**
	 * Does the membership contain an actual internal member
	 * 
	 * @return
	 */
	public boolean exists() {
		return (this.memVO != null);
	}
	
	public ClientVO getClient() {
		ClientVO cVO = null;

		try {
			cVO = memVO.getClient();
		} catch (RemoteException e) {
			//There was a problem, but i am unsure how to handle this specific problem
			//TODO: how do we handle a null client
		}
		
		return cVO;
	}

	public void setExpiryDate(DateTime to) {
		
	}
	
	public void activate() {
		
	}
	
	public void update(CMOLineItem line) throws RemoteException {
		TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP, SecurityHelper.getRoadsideUser());
		
		String msg = transGroup.addSelectedClient(this.memVO);
		if (msg != null && !msg.isEmpty()) {
			throw new SystemException(msg);
		}
		transGroup.setContextClient(this.memVO);

		// Create a membership transaction for the selected client.
		transGroup.createTransactionsForSelectedClients();

		MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(this.memVO.getClientNumber());

		MembershipVO newMemVO = contextMemTransVO.getNewMembership();

		newMemVO.setRego(line.get(CMOLineItem.FIELD_REGO));
		newMemVO.setVin(line.get(CMOLineItem.FIELD_VIN));
		newMemVO.setMake(line.get(CMOLineItem.FIELD_MAKE));
		newMemVO.setModel(line.get(CMOLineItem.FIELD_MODEL));
		newMemVO.setYear(line.get(CMOLineItem.FIELD_YEAR));
		newMemVO.setMembershipProfileCode(line.get(CMOLineItem.FIELD_PROGRAM_CODE));

		// submit transaction
		transGroup.submit();
	}
	
	public void cancel(ReferenceDataVO reason) throws RemoteException {
		if (this.memVO.getStatus().equals(MembershipVO.STATUS_CANCELLED)) {
			return;
		}

		MembershipHelper.removeFromAllGroups(this.memVO.getMembershipID());

		// Create a cancel TransactionGroup object to hold the common data in
		// the transaction
		TransactionGroup transGroup = TransactionGroupFactory.getTransactionGroupForCancel(SecurityHelper.getRoadsideUser());

		transGroup.setMembershipType(this.memVO.getMembershipType());

		String msg = transGroup.addSelectedClient(this.memVO);
		if (msg != null && !msg.isEmpty()) {
			throw new RemoteException();
		}
		
		transGroup.setContextClient(this.memVO);

		// Create transactions for all selected clients
		transGroup.createTransactionsForSelectedClients();

		// Set the reason for the cancellation.
		transGroup.setTransactionReason(reason);
		transGroup.calculateTransactionFee();

		// submit transaction
		transGroup.submit();
	}

	public Integer getClientNumber() {
		return this.memVO.getClientNumber();
	}
}
