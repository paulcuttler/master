package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;

import com.ract.client.ClientMgr;
import com.ract.membership.MembershipMgr;
import com.ract.util.DateTime;

public class ActionChangeDetail extends Action {

	private MembershipMgr memMgr;
	private ClientMgr clMgr;
	
	public ActionChangeDetail(MembershipMgr mem, ClientMgr cl) {
		super();
		this.memMgr = mem;
		this.clMgr = cl;
	}
	
	@Override
	public void execute(CMOLineItem lineItem) {
		// TODO Auto-generated method stub
		CMOMembership membership = CMOMembership.FindByVin(this.memMgr, lineItem.getVin(), false);
		
		if(membership.exists()) {
			CMOClient client = new CMOClient(this.clMgr, membership.getClient());
			
			//Its the same client, lets just sync it up.
			this.updateClient(client, lineItem);
			this.updateMembership(membership, lineItem);
			
		} else {
			this.error.add((new DateTime()).formatLongDate() 
					+ ": Unable to find membership by VIN: " + lineItem.get(CMOLineItem.FIELD_VIN) 
					+ " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID) 
					+ ", VIN: " + lineItem.get(CMOLineItem.FIELD_VIN) 
					+ ", Product Code: " + lineItem.get(CMOLineItem.FIELD_PROGRAM_CODE));
		}
	}
	
	private void updateClient(CMOClient client, CMOLineItem lineItem) {		
		try {
			String changes = client.getChangeLog(lineItem);

			if (!changes.isEmpty()) {
				this.result.add((new DateTime()).formatLongDate() 
							  + ": Client update required (" + client.getClientNumber() 
							  + ") " + changes);
			}
		} catch (RemoteException e) {
			this.error.add((new DateTime()).formatLongDate() 
					  + ": Unable to update client (" + client.getClientNumber() 
					  + ") ");
		}
	}
	
	private void updateMembership(CMOMembership membership, CMOLineItem lineItem) {
		try {
			membership.update(lineItem);
			
			this.result.add((new DateTime()).formatLongDate() 
						   + ": Updated Membership (T" + lineItem.get(CMOLineItem.FIELD_TRANS_TYPE) 
						   + ") " + membership.getClientNumber() 
						   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
			
		} catch (RemoteException e) {
			this.error.add((new DateTime()).formatLongDate() 
						  + ": Unable to update membership  (T" + lineItem.get(CMOLineItem.FIELD_TRANS_TYPE) 
						  + ") " + membership.getClientNumber() 
						  + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}				
	}
	
}
