package com.ract.membership.importer.cmo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActionType {
	public static final String TRANS_TYPE_NEW_VEHICLE = "11";
	public static final String TRANS_TYPE_DEMO_VEHICLE = "14";
	public static final String TRANS_TYPE_CHANGE_DETAIL = "21";
	public static final String TRANS_TYPE_CHANGE_OWNER = "22";
	public static final String TRANS_TYPE_CANCELLATION = "61";
	public static final String TRANS_TYPE_SERVICE = "97";
	
	public static final String PRODUCT_CODE_SUBARU_CM = "SCM";
	public static final String PRODUCT_CODE_FORD_CM = "FCM";
	public static final String PRODUCT_CODE_FORD_DC = "FDC";
	public static final String PRODUCT_CODE_HOLDEN_CM = "HCM";
	public static final String PRODUCT_CODE_HOLDEN_MY = "HMY";
	public static final String PRODUCT_CODE_HOLDEN_DC = "HDC";
	
	public static final int TRANS_TYPE_NEW_VEHICLE_CODE = 1;
	public static final int TRANS_TYPE_DEMO_VEHICLE_CODE = 2;
	public static final int TRANS_TYPE_CHANGE_DETAIL_CODE = 3;
	public static final int TRANS_TYPE_CHANGE_OWNER_CODE = 4;
	public static final int TRANS_TYPE_CANCELLATION_CODE = 5;
	public static final int TRANS_TYPE_SERVICE_CODE = 6;
	
	public static final int PRODUCT_CODE_SUBARU_CM_CODE = 1;
	public static final int PRODUCT_CODE_FORD_CM_CODE = 2;
	public static final int PRODUCT_CODE_FORD_DC_CODE = 3;
	public static final int PRODUCT_CODE_HOLDEN_CM_CODE = 4;
	public static final int PRODUCT_CODE_HOLDEN_MY_CODE = 5;
	public static final int PRODUCT_CODE_HOLDEN_DC_CODE = 6;
	
	private static final HashMap<String, Integer> transactionTypeList = new HashMap<String, Integer>(6);
	private static final HashMap<String, Integer> productCodeList = new HashMap<String, Integer>(6);
	
	static {
		transactionTypeList.put(TRANS_TYPE_NEW_VEHICLE, TRANS_TYPE_NEW_VEHICLE_CODE);
		transactionTypeList.put(TRANS_TYPE_DEMO_VEHICLE, TRANS_TYPE_DEMO_VEHICLE_CODE);
		transactionTypeList.put(TRANS_TYPE_CHANGE_DETAIL, TRANS_TYPE_CHANGE_DETAIL_CODE);
		transactionTypeList.put(TRANS_TYPE_CHANGE_OWNER, TRANS_TYPE_CHANGE_OWNER_CODE);
		transactionTypeList.put(TRANS_TYPE_CANCELLATION, TRANS_TYPE_CANCELLATION_CODE);
		transactionTypeList.put(TRANS_TYPE_SERVICE, TRANS_TYPE_SERVICE_CODE);
	
		productCodeList.put(PRODUCT_CODE_SUBARU_CM, PRODUCT_CODE_SUBARU_CM_CODE);
		productCodeList.put(PRODUCT_CODE_FORD_CM, PRODUCT_CODE_FORD_CM_CODE);
		productCodeList.put(PRODUCT_CODE_FORD_DC, PRODUCT_CODE_FORD_DC_CODE);
		productCodeList.put(PRODUCT_CODE_HOLDEN_CM, PRODUCT_CODE_HOLDEN_CM_CODE);
		productCodeList.put(PRODUCT_CODE_HOLDEN_MY, PRODUCT_CODE_HOLDEN_MY_CODE);
		productCodeList.put(PRODUCT_CODE_HOLDEN_DC, PRODUCT_CODE_HOLDEN_DC_CODE);
	}
	
	private String transactionType;
	private String productCode;
	
	public ActionType(String tt, String pc) {
		this.transactionType = tt;
		this.productCode = pc;
	}
	
	public int transactionTypeCode() {
		return transactionTypeList.get(this.transactionType);
	}
	
	public int productTypeCode() {
		return productCodeList.get(this.productCode);
	}
}
