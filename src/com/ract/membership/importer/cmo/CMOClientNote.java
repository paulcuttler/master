package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;
import java.text.ParseException;

import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientNote;
import com.ract.client.ClientVO;
import com.ract.client.NoteType;
import com.ract.common.BusinessType;
import com.ract.security.SecurityHelper;
import com.ract.util.DateTime;
import com.ract.util.Interval;

public class CMOClientNote {

	private ClientNote note;
	private ClientMgrLocal clientMgrLocal;
	private ClientVO client;
	
	public static CMOClientNote Create(ClientMgrLocal clMgr, ClientVO client) throws RemoteException {
		CMOClientNote note = new CMOClientNote(clMgr, client);
		note.create();
		return note;
	}
	
	public CMOClientNote(ClientMgrLocal clMgr, ClientVO client) {
		this.note = new ClientNote();
		this.clientMgrLocal = clMgr;
		this.client = client;
	}
	
	public void create() throws RemoteException  {
		DateTime due = new DateTime();
		
		try {
			due.add(new Interval("0:0:2:0:0:0")); // 2 days
		} catch (ParseException e) {
			// this can never happen as the interval string is correct and
			// is a hard coded value
		}
	
		note.setClientNumber(this.client.getClientNumber());
		note.setNoteType(NoteType.TYPE_FOLLOW_UP);
		note.setCategoryCode("other");
		note.setCreated(new DateTime());
		note.setCreateId(SecurityHelper.getRoadsideUser().getUserID());
		note.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
		note.setNoteText("Guessed gender \"Male\" for CMO import from Title");
		note.setDue(due);
		
		this.clientMgrLocal.createClientNote(note);
	}
	
	
}
