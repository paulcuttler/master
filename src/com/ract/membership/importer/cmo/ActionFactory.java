package com.ract.membership.importer.cmo;

import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipRefMgrLocal;

public class ActionFactory {

	private ClientMgr clientMgr;
	private CommonMgr commonMgr;
	private MembershipMgr membershipMgr;
	private MembershipRefMgr membershipRefMgr;
	 
	public ActionFactory(ClientMgr clml, CommonMgr cml, MembershipMgr mml, MembershipRefMgr mrml) {
		this.clientMgr = clml;
		this.commonMgr = cml;
		this.membershipMgr = mml;
		this.membershipRefMgr = mrml;
	}
	
	public Action build(String type, String productCode) throws CMOInvalidTypeCode {
		
		ActionType at = new ActionType(type, productCode);
		
		switch(at.transactionTypeCode()) {
			case ActionType.TRANS_TYPE_NEW_VEHICLE_CODE:
				return this.buildNewAction();

			case ActionType.TRANS_TYPE_DEMO_VEHICLE_CODE:
				return this.buildNewAction();

			case ActionType.TRANS_TYPE_CHANGE_DETAIL_CODE:
				return this.buildChangeDetailAction();
				
			case ActionType.TRANS_TYPE_CHANGE_OWNER_CODE:
				return this.buildChangeOwnerAction();
				
			case ActionType.TRANS_TYPE_CANCELLATION_CODE:
				return this.buildCancellationAction();
				
			case ActionType.TRANS_TYPE_SERVICE_CODE:
				return this.buildServiceAction();
				
			default: 
				throw new CMOInvalidTypeCode("unable to get action for trans type:" + type);
		}
	}

	private Action buildChangeOwnerAction() {
		return new ActionChangeOwner(this.clientMgr, this.membershipMgr, this.membershipRefMgr);
	}

	private Action buildServiceAction() {
		return new ActionService(this.clientMgr, this.membershipMgr, this.membershipRefMgr);
	}

	private Action buildCancellationAction() {
		return new ActionCancellation(this.membershipMgr, this.membershipRefMgr);
	}

	private Action buildChangeDetailAction() {
		return new ActionChangeDetail(this.membershipMgr, this.clientMgr);
	}

	private Action buildNewAction() {
		return new ActionNew(this.clientMgr, this.membershipMgr);
	}

	public ActionGeneral buildGeneral(String string) {
		return new ActionCheckDuplicates(this.clientMgr);
	}
	
}
