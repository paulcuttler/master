package com.ract.membership.importer.cmo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class CMOStreetAddress {
	
	private String address;
	private String streetName;
	private String streetType;
	private String streetNumber;
	
	public CMOStreetAddress(String streetAddressString) {
		this.address = streetAddressString;
		this.build();
	}

	public int getId() {
		return -1;
	}
	
	public String getStreetName() {
		return this.streetName;
	}
	
	public String getStreetType() {
		return this.streetType;
	}
	
	public String getStreetNumber() {
		return this.streetNumber;
	}
	
	private void build() {
		//Order is important
		this.extractStreetName();
		this.extractStreetNumber();
		this.extractStreetType();
	}

	/**
	 * Taking the address and extract the street name 
	 * example: U 7/ 2a Wyndham Rd => WYNDHAM RD
	 */
	private void extractStreetName() {
		List<String> streetNameParts = new ArrayList<String>();
		String words[] = this.address.split(" ");

		//Working BACKWARDS through the address, 
		for (int i = words.length - 1; i >= 0; i--) {
			if (Pattern.compile("\\d").matcher(words[i]).find()) {
				break;
			} else {
				streetNameParts.add(words[i]);
			}
		}

		//Because we put the words in backwards, need to put back in original order. 
		Collections.reverse(streetNameParts);
		
		//Rebuild the streetName from parts
		StringBuffer streetNameBuilder = new StringBuffer("");
		for (String part : streetNameParts) {
			streetNameBuilder.append(part).append(" ");
		}

		this.streetName = streetNameBuilder.toString().trim();
	}
	
	/**
	 * Take the address and extract just the street number. Requires that the 
	 * street name has already been extracted
	 * example:  U 7/ 2a Wyndham Road =>  WYNDHAM RD
	 */
	private void extractStreetType() {
		String[] parts = streetName.split(" ");
		String streetTypePart = parts[parts.length - 1].toUpperCase();
		
		if (CMOStreetAddressAbbreviation.Has(streetTypePart)) {
			streetTypePart = CMOStreetAddressAbbreviation.Get(streetTypePart);
			
			StringBuffer streetNameWithAbbr = new StringBuffer("");
			for (String s : parts) {
				streetNameWithAbbr.append(s).append(" ");
			}
			
			this.streetName = streetNameWithAbbr.toString().trim();
		}
		
		
		this.streetType = streetTypePart;
	}
	
	/**
	 * Take the address and extract just the street number. Requires that the 
	 * street name has already been extracted
	 * example:  U 7/ 2a Wyndham Rd =>  U 7/ 2A 
	 */
	private void extractStreetNumber() {
		if(this.streetName == null) {
			this.extractStreetName();
		}
		
		String streetNumberRaw = this.address.split(this.streetName)[0];
		
		this.streetNumber = streetNumberRaw.trim().toUpperCase();
	}
	
}
