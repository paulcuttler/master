package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.ract.common.AddressVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.GenericException;
import com.ract.common.StreetSuburbVO;
import com.ract.util.DateTime;

public class CMOLineItem {
	
	public static final String FIELD_CLUB_PLAN_CODE = "ClubPlanCode";
	public static final String FIELD_CLUB_PLAN = "ClubPlan";
	public static final String FIELD_PLAN_CODE = "PlanCode";
	public static final String FIELD_REPLACEMENT_VEHICLE_REGO = "ReplacementVehicleRego";
	public static final String FIELD_EXISTING_CLUB_MEMBERSHIP_NUMBER = "ExistingClubMembershipNumber";
	public static final String FIELD_EXISTING_CLUB_MEMBERSHIP = "ExistingClubMembership";
	public static final String FIELD_STATEOF_MEMBERSHIP = "StateofMembership";
	public static final String FIELD_CONSENT = "Consent";
	public static final String FIELD_PRIVACY = "Privacy";
	public static final String FIELD_ABN = "ABN";
	public static final String FIELD_TRANSMISSION = "Transmission";
	public static final String FIELD_COLOUR = "Colour";
	public static final String FIELD_VARIANT = "Variant";
	public static final String FIELD_SERIES = "Series";
	public static final String FIELD_BODY_TYPE = "BodyType";
	public static final String FIELD_DESCRIPTION = "Description";
	public static final String FIELD_CLIENT_CODE = "ClientCode";
	public static final String FIELD_BATCH_TYPE = "Batch_Type";
	public static final String FIELD_BATCH_NO = "Batch_No";
	public static final String FIELD_IDENTIFIER = "Identifier";
	public static final String FIELD_PROGRAM_CODE = "ProgramCode";
	public static final String FIELD_TRANS_TYPE = "Trans_Type";
	public static final String FIELD_NOMINATION_DATE = "NominationDate";
	public static final String FIELD_SERVICE_DATE = "ServiceDate";
	public static final String FIELD_EXPIRY_DATE = "ExpiryDate";
	public static final String FIELD_VIN = "VIN";
	public static final String FIELD_MAKE = "Make";
	public static final String FIELD_MODEL = "Model";
	public static final String FIELD_REGO = "Rego";
	public static final String FIELD_YEAR = "YearOfRelease";
	public static final String FIELD_OWNER_TYPE = "OwnerType";
	public static final String FIELD_TITLE = "Title";
	public static final String FIELD_FIRST_NAME = "FirstName";
	public static final String FIELD_SURNAME = "Surname";
	public static final String FIELD_BUSINESS_NAME = "BusinessName";
	public static final String FIELD_DOB = "DOB";
	public static final String FIELD_HOME_PHONE = "HomePhone";
	public static final String FIELD_BUSINESS_PHONE = "BusinessPhone";
	public static final String FIELD_MOBILE_PHONE = "MobilePhone";
	public static final String FIELD_EMAIL = "Email";
	public static final String FIELD_RESI_ADDRESS1 = "AddressLine1";
	public static final String FIELD_RESI_ADDRESS2 = "AddressLine2";

	public static final String FIELD_RESI_ADDRESS = "Address";
	public static final String FIELD_RESI_STREET = "AddressStreet";
	public static final String FIELD_RESI_STREET_NUM = "AddressStreetNum";

	public static final String FIELD_RESI_CITY = "City";
	public static final String FIELD_RESI_STATE = "State";
	public static final String FIELD_RESI_POSTCODE = "Postcode";
	public static final String FIELD_POST_ADDRESS1 = "PostalAddressLine1";
	public static final String FIELD_POST_ADDRESS2 = "PostalAddressLine2";

	public static final String FIELD_POST_ADDRESS = "PostalAddress";
	public static final String FIELD_POST_STREET = "PostalAddressStreet";
	public static final String FIELD_POST_STREET_NUM = "PostalAddressStreetNum";

	public static final String FIELD_POST_CITY = "PostalCity";
	public static final String FIELD_POST_STATE = "PostalState";
	public static final String FIELD_POST_POSTCODE = "PostalPostcode";
	public static final String FIELD_NOMINATION_ID = "NominationID";
	
	private final String TRANS_TYPE_NEW_VEHICLE = "11";
	private final String TRANS_TYPE_DEMO_VEHICLE = "14";
	private final String TRANS_TYPE_CHANGE_DETAIL = "21";
	private final String TRANS_TYPE_CHANGE_OWNER = "22";
	private final String TRANS_TYPE_CANCELLATION = "61";
	private final String TRANS_TYPE_SERVICE = "97";
	
	private final String[] FIELD_ORDER_LIST = {
		FIELD_IDENTIFIER, 
		FIELD_BATCH_NO, 
		FIELD_BATCH_TYPE, 
		FIELD_PROGRAM_CODE, 
		FIELD_CLIENT_CODE, 
		FIELD_TRANS_TYPE, 
		FIELD_NOMINATION_DATE, 
		FIELD_SERVICE_DATE, 
		FIELD_VIN, 
		FIELD_DESCRIPTION, 
		FIELD_MAKE, 
		FIELD_MODEL, 
		FIELD_BODY_TYPE, 
		FIELD_SERIES, 
		FIELD_VARIANT, 
		FIELD_COLOUR, 
		FIELD_TRANSMISSION, 
		FIELD_YEAR, 
		FIELD_REGO, 
		FIELD_OWNER_TYPE, 
		FIELD_TITLE, 
		FIELD_FIRST_NAME, 
		FIELD_SURNAME, 
		FIELD_BUSINESS_NAME, 
		FIELD_ABN, 
		FIELD_DOB, 
		FIELD_HOME_PHONE, 
		FIELD_BUSINESS_PHONE, 
		FIELD_MOBILE_PHONE, 
		FIELD_EMAIL, 
		FIELD_PRIVACY, 
		FIELD_CONSENT, 
		FIELD_STATEOF_MEMBERSHIP, 
		FIELD_EXISTING_CLUB_MEMBERSHIP, 
		FIELD_EXISTING_CLUB_MEMBERSHIP_NUMBER, 
		FIELD_REPLACEMENT_VEHICLE_REGO, 
		FIELD_RESI_ADDRESS1, 
		FIELD_RESI_ADDRESS2, 
		FIELD_RESI_CITY, 
		FIELD_RESI_STATE, 
		FIELD_RESI_POSTCODE, 
		FIELD_POST_ADDRESS1, 
		FIELD_POST_ADDRESS2, 
		FIELD_POST_CITY, 
		FIELD_POST_STATE, 
		FIELD_POST_POSTCODE, 
		FIELD_NOMINATION_ID, 
		FIELD_PLAN_CODE, 
		FIELD_CLUB_PLAN, 
		FIELD_CLUB_PLAN_CODE 
	};
	
	private final static String REGEXP_PO_BOX = "P ?O Box";
	private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	private Map<String, String> fields;
	private String[] line;
	private List<String> errors;
	
	private DateTime _nominationDate = null;
	private DateTime _dobDate = null;
	private DateTime _serviceDate = null;
	public CMOStreetAddress _streetAddress = null;
	private Integer _streetAddressId;
	private CMOStreetAddress _postalAddress = null;
	private Integer _postalAddressId;
	
	/**
	 * Create a new CMOLineItem from the separated line from a csv file
	 * @param line
	 */
	public CMOLineItem(String[] line) {
		this.line = line;
		this.errors = new ArrayList<String>();
		this.parseInputLine();
		

		if(this.fields.get(FIELD_IDENTIFIER).equals("02")){
			this.parseAddresses();
		}
	}
	
	/**
	 * Get the residential street number 
	 * 
	 * @return
	 */
	public String getResidentialStreetNum() {
		return this._streetAddress.getStreetNumber();
	}
	
	/**
	 * Get the residential street code. This is the link to a row in the street-suburb table
	 * 
	 * @return
	 */
	public Integer getResidentialStreetCode() {
		return this._streetAddressId;
	}
	
	/**
	 * Get the Residential street name.
	 * 
	 * @return
	 */
	public String getResidenitalSreet() {
		return this._streetAddress.getStreetName();
	}
	
	/**
	 * Get the post street name.
	 * 
	 * @return
	 */
	public String getPostalStreet() {
		return this._postalAddress.getStreetName();
	}
	
	/**
	 * Get the postal street number. If no postal address is provided it will return the residential 
	 * address street number instead.
	 * 
	 * @return
	 */
	public String getPostalStreetNum() {
		return this._postalAddress.getStreetNumber();
	}
	
	/**
	 * Get the residential street code. This is the link to a row in the street-suburb table. 
	 * If no postal address is provided it will return the residential address street number instead.
	 * 
	 * @return
	 */
	public Integer getPostalStreetCode() {
		return this._postalAddressId;
	}
	
	public String getVin() {
		return this.fields.get(FIELD_VIN);
	}

	/**
	 * Validate that the content provided in the line contains at least the required information 
	 * and that it is expected range of values
	 * 
	 * @return
	 * @throws CMOValidationError
	 */
	public boolean validateLine(String validProductCodes) {
		return this.validateStateIsTasmania()
			&& this.validateRequireTitle()
			&& this.validateDenyPOBoxForResidentialAddress()
			&& this.validateRequireFirstName() 
			&& this.validateRequireSurname()
			&& this.validateRequireCorrectProductCode(validProductCodes)
			&& this.validateRequireValidDateOfBirthDate()
			&& this.validateRequireValidNominationDate();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<String> errors() {
		return this.errors;
	}
	
	/**
	 * Get an internal field value
	 * 
	 * @param code
	 * @return csv field value
	 */
	public String get(String code) {
		return fields.get(code);
	}
	
	/**
	 * Get the nomination date that the member started on. 
	 * 
	 * @return
	 * @throws CMOValidationError
	 */
	public DateTime getNominationDate() throws CMOValidationError {
		if(this._nominationDate == null) {
			try {
				this._nominationDate = new DateTime(df.parse(fields.get(FIELD_NOMINATION_DATE)));
			} catch (ParseException e) {
				throw new CMOValidationError("Unable to validate: FIELD_NOMINATION_DATE - " + "");
			}
		}
		return this._nominationDate;
	}
	
	public DateTime mustGetNominationDate() {
		return this._nominationDate;
	}
	
	/**
	 * Get the date of birth as a DateTime object, converting to a string if necessary
	 * 
	 * @return
	 * @throws CMOValidationError
	 */
	public DateTime getDobDate() throws CMOValidationError {
		if(this._dobDate == null) {
			try {
				this._dobDate = new DateTime(df.parse(fields.get(FIELD_DOB)));
			} catch (ParseException e) {
				throw new CMOValidationError("Unable to validate: FIELD_NOMINATION_DATE - " + fields.get(FIELD_DOB) + "");
			}
		}
		return this._dobDate;
	}
	
	public DateTime mustGetDobDate() {
		return this._dobDate;
	}
	
	/**
	 * 
	 * @return
	 * @throws CMOValidationError
	 */
	public DateTime getServiceDate() throws CMOValidationError {
		if(this._serviceDate == null) {
			try {
				this._serviceDate = new DateTime(df.parse(fields.get(FIELD_SERVICE_DATE)));
			} catch (ParseException e) {
				throw new CMOValidationError("Unable to validate: FIELD_NOMINATION_DATE - " + "");
			}
		}
		
		return this._serviceDate;
	}
	
	public DateTime mustGetServiceDate() {
		return this._nominationDate;
	}
	
	/**
	 * Does this line item require further processing 
	 * 
	 * @return boolean 
	 */
	public boolean requiresProcessing() {
		return fields.get(FIELD_IDENTIFIER).equals("02");
	}
	
	public String getTransactionType() {
		return fields.get(FIELD_TRANS_TYPE);
	}
	
	public boolean isTransactionTypeService() {
		return fields.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_SERVICE);
	}
	
	public boolean isTransactionTypeDemo() {
		return fields.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_DEMO_VEHICLE);
	}
	
	public boolean isTransactionTypeNew() {
		return fields.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_NEW_VEHICLE);
	}
	
	public boolean isTransactionTypeChangeDetail() {
		return fields.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_CHANGE_DETAIL);
	}
	
	public boolean isTransactionTypeCancellation() {
		return fields.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_CANCELLATION);
	}
	
	public boolean isTransactionTypeChangeOwner() {
		return fields.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_CHANGE_OWNER);
	}
	
	public String getProductCode() {
		return fields.get(FIELD_PROGRAM_CODE);
	}
	
	/**
	 * Business Rule - Service date must be set to a valid date using the format yyyy-MM-dd
	 * 
	 * @return boolean
	 */
	public boolean validateRequireValidServiceDate() {
		boolean valid = true;
		
		try {
			this.getServiceDate();
		} catch (CMOValidationError e) {
			//Not a valid serviceDate so fail validation
			valid = false;
			
			String err = (new DateTime()).formatLongDate() + ": Unable to service date record: " 
					 + e.getMessage() + ": for Nomination ID: " 
				     + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
					 + fields.get(FIELD_VIN) + ", Product Code: " 
				     + fields.get(FIELD_PROGRAM_CODE);
		
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Service date must be set to a valid date using the format yyyy-MM-dd
	 * 
	 * @return boolean
	 */
	public boolean validateRequireValidDateOfBirthDate() {
		boolean valid = true;
		
		try {
			this.getDobDate();
		} catch (CMOValidationError e) {
			//Not a valid date of birth so fail validation
			valid = false;
			
			String err = (new DateTime()).formatLongDate() + ": Unable to parse date of birth: " 
					 + e.getMessage() + ": for Nomination ID: " 
				     + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
					 + fields.get(FIELD_VIN) + ", Product Code: " 
				     + fields.get(FIELD_PROGRAM_CODE);
		
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Service date must be set to a valid date using the format yyyy-MM-dd
	 * 
	 * @return boolean
	 */
	public boolean validateRequireValidNominationDate() {
		boolean valid = true;
		
		try {
			this.getNominationDate();
		} catch (CMOValidationError e) {
			//Not a valid nomination so fail validation
			valid = false;
			
			String err = (new DateTime()).formatLongDate() + ": Unable to parse nomination: " 
						 + e.getMessage() + ": for Nomination ID: " 
					     + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
						 + fields.get(FIELD_VIN) + ", Product Code: " 
					     + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Require State to be Tasmania
	 * 
	 * @return
	 */
	public boolean validateStateIsTasmania() {
		boolean valid = fields.get(FIELD_RESI_STATE).equalsIgnoreCase(AddressVO.STATE_TAS);
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": Non-Tasmanian addresses are not supported: " 
						 + fields.get(FIELD_RESI_STATE) + " for Nomination ID: " 
						 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
						 + fields.get(FIELD_VIN) + ", Product Code: " 
						 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Require residential address to be a non PO box
	 * 
	 * @return
	 */
	public boolean validateDenyPOBoxForResidentialAddress() {
		boolean valid = !Pattern.compile(REGEXP_PO_BOX, Pattern.CASE_INSENSITIVE).matcher(fields.get(FIELD_RESI_ADDRESS1)).find();
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": PO Box-style addresses are not permitted for Residential address: " 
						 + fields.get(FIELD_RESI_ADDRESS1) + " for Nomination ID: "
						 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
						 + fields.get(FIELD_VIN) + ", Product Code: " 
						 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Require a title
	 * 
	 * @return
	 */
	public boolean validateRequireTitle() {
		boolean valid = !fields.get(FIELD_TITLE).isEmpty();
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": Title cannot be blank for Nomination ID: " 
						 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
						 + fields.get(FIELD_VIN) + ", Product Code: " 
						 + fields.get(FIELD_PROGRAM_CODE);
 
					
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Require a first name
	 * 
	 * @return
	 */
	public boolean validateRequireFirstName() {
		boolean valid = !fields.get(FIELD_FIRST_NAME).isEmpty();
			
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": First name cannot be blank for Nomination ID: " 
						 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
						 + fields.get(FIELD_VIN) + ", Product Code: " 
						 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Require a surname
	 * 
	 * @return
	 */
	public boolean validateRequireSurname() {
		boolean valid = !fields.get(FIELD_SURNAME).isEmpty();
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": Surname cannot be blank for Nomination ID: " 
						 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
						 + fields.get(FIELD_VIN) + ", Product Code: " 
						 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Business Rule - Require correct CMO product code 
	 * 
	 * @return 
	 */
	public boolean validateRequireCorrectProductCode(String validProductCodes) {
		boolean valid = validProductCodes.indexOf(fields.get(FIELD_PROGRAM_CODE)) != -1;
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": Unknown Program Code: " 
						 + fields.get(FIELD_PROGRAM_CODE) + ", for Nomination ID: " 
						 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
						 + fields.get(FIELD_VIN) + ", Product Code: " 
						 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;	
	}
	
	private boolean validateRequireStreetNameId() {
		boolean valid = this._streetAddressId != null;
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": Unknown Street: " 
					 + fields.get(this._streetAddress.getStreetName()) + ", for Nomination ID: " 
					 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
					 + fields.get(FIELD_VIN) + ", Product Code: " 
					 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	private boolean validateRequireStreetNumber() {
		boolean valid = this._streetAddress.getStreetNumber() != null;
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": Unknown Street Number: " 
					 + fields.get(this._streetAddress.getStreetNumber()) + ", for Nomination ID: " 
					 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
					 + fields.get(FIELD_VIN) + ", Product Code: " 
					 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	private boolean validateRequirePostalAddressId() {
		boolean valid = this._postalAddressId != null;
		
		if(!valid) {
			String err = (new DateTime()).formatLongDate() + ": Unknown Postal Street Id: " 
					 + fields.get(this._postalAddressId) + ", for Nomination ID: " 
					 + fields.get(FIELD_NOMINATION_ID) + ", VIN: " 
					 + fields.get(FIELD_VIN) + ", Product Code: " 
					 + fields.get(FIELD_PROGRAM_CODE);
			
			this.errors.add(err);
		}
		
		return valid;
	}
	
	/**
	 * Parse the input and save parse map
	 */
	private void parseInputLine() {
		this.fields = new HashMap<String, String>();

		for (int i = 0; i < this.line.length; i++) {
			fields.put(FIELD_ORDER_LIST[i], this.line[i].trim());
		}
	}
	
	/**
	 * Parse the Address and save it internaly.
	 */
	private void parseAddresses() {		
		String residentalAddress = fields.get(FIELD_RESI_ADDRESS1) + " " + fields.get(FIELD_RESI_ADDRESS2);
		this._streetAddress = new CMOStreetAddress(residentalAddress);
				
		CommonMgr cmnMgr = CommonEJBHelper.getCommonMgr();
		
		List<StreetSuburbVO> ssList;
		
		try {
			ssList = (List<StreetSuburbVO>) cmnMgr.getStreetSuburbList(
				this._streetAddress.getStreetName(), 
				fields.get(FIELD_RESI_CITY), 
				AddressVO.STATE_TAS
			);
			
			if (!ssList.isEmpty()) {
				this._streetAddressId = ssList.get(0).getStreetSuburbID();
			} 	
			
		} catch (RemoteException e) {
			//Ignore, this is a business rule failure in disguise
		}
		
		if(fields.get(FIELD_POST_ADDRESS1) != null && !fields.get(FIELD_POST_ADDRESS1).isEmpty()) {
			String postalAddress = fields.get(FIELD_POST_ADDRESS1) + " " + fields.get(FIELD_POST_ADDRESS2);
			this._postalAddress = new CMOStreetAddress(postalAddress);
			
			try {
				ssList = (List<StreetSuburbVO>) cmnMgr.getStreetSuburbList(
					this._postalAddress.getStreetName(), 
					fields.get(FIELD_RESI_CITY), 
					AddressVO.STATE_TAS
				);
				
				if (!ssList.isEmpty()) {
					this._streetAddressId = ssList.get(0).getStreetSuburbID();
				} 	
				
			} catch (RemoteException e) {
				//Ignore, this is a business rule failure in disguise
			}
		} else {
			this._postalAddress = this._streetAddress;
			this._postalAddressId = this._streetAddressId;
		}
		
	}


}
