package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.SystemParameterVO;
import com.ract.util.DateTime;

public class Importer {
	
	private String validProductCodes = "";
	
	private boolean success = false;
	
	private ArrayList<String> errorLog = new ArrayList<String>();
	private ArrayList<String> resultLog = new ArrayList<String>();
	private String duplicateLog;
	
	private ActionFactory actionFactory;
	
	public Importer(CommonMgr cmnMgr, ActionFactory actionFactory) {
		
		try {
			this.actionFactory = actionFactory;
			this.validProductCodes = cmnMgr.getSystemParameterValue(
				SystemParameterVO.CATEGORY_MEMBERSHIP, 
				SystemParameterVO.MEM_CMO_PROFILE
			);
		} catch (RemoteException e) {
			// We cant do anything, lets do nothing
			this.success = false;
		}
	}
	
	public void execute(String filePath) {
		CMOFile file = new CMOFile(filePath);
		
		Iterator<CMOLineItem> itr = file.lineIterator();
		while(itr.hasNext()) {
			CMOLineItem lineItem = itr.next();
			if(!lineItem.get(CMOLineItem.FIELD_IDENTIFIER).equals("02")) {
				//Not actually a line. 
				continue;
			}
			
			
			if(!lineItem.validateLine(this.validProductCodes)) {
				// The provided line item was not valid. log the FIRST error and continue to the next item
				errorLog.add(lineItem.errors().get(0));
				continue;
			}
		
			Action action = null;
			
			try {
				
				action = this.actionFactory.build(lineItem.getTransactionType(), lineItem.getProductCode());
			} catch (CMOInvalidTypeCode e) {
				
				// This is means that the provided type (product or transaction)
				String err = (new DateTime()).formatLongDate() 
						  + ": Invalid transaction type (T" + lineItem.getTransactionType() 
						  + ") for VIN: " + lineItem.getVin()
						  + ", Product Code: " + lineItem.getProductCode();
						
				errorLog.add(err);
				return;
			}
			
			action.execute(lineItem);
			
			if(action.hasResult()) {
				String[] a = action.result().toArray(new String[action.result().size()]);
				String str = Arrays.toString(a);
				resultLog.add(str);
			}
			
			if(action.hasError()) {
				errorLog.add(Arrays.toString(action.result().toArray(new String[action.error().size()])));
			}
		}
		
		this.actionDuplicates();
		
		this.success = true;
	}
	
	public boolean succeeded() {
		return this.success;
	}
	
	public boolean hasErrorLog() {
		return !this.errorLog.isEmpty();
	}
	
	public String getErrorLog() {
		StringBuilder builder = new StringBuilder("");
		Iterator<String> errorItr = this.errorLog.iterator();
		
		while(errorItr.hasNext()) {
			builder
				.append(errorItr.next())
				.append("\n");
		}
		
		return builder.toString();
	}
	
	public boolean hasResultLog() {
		return !this.resultLog.isEmpty();
	}
	
	public String getResultLog() {
		StringBuilder builder = new StringBuilder("");
		Iterator<String> resultItr = this.resultLog.iterator();
		
		while(resultItr.hasNext()) {
			builder
				.append(resultItr.next())
				.append("\n");
		}
		
		return builder.toString();
	}
	
	public boolean hasDuplicateLog() {
		return this.duplicateLog != null;
	}
	
	public String getDuplicateLog() {
		return this.duplicateLog;
	}

	private void actionDuplicates() {
		ActionGeneral action = this.actionFactory.buildGeneral("duplicates");
		action.execute();
		
		if(action.hasResult()) {
			this.duplicateLog = Arrays.toString(action.result().toArray(new String[action.result().size()]));
		}
	}	
}
