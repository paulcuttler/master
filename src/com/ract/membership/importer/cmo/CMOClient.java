package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;
import java.util.List;

import com.ract.client.Client;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.security.SecurityHelper;
import com.ract.util.DateTime;

/**
 * MUST VALIDATE LINE ITEM BEFORE CREATION
 * 
 * @author dukes
 */
public class CMOClient
{
    private ClientVO clientVO;
    private CMOLineItem line;
    private ClientMgr clientMgr;
    private String requiredUpdates;

    public CMOClient(ClientMgr mgr, CMOLineItem line)
    {
	this.clientMgr = mgr;
	this.clientVO = null;
	this.line = line;

	this.initialize();
    }

    public CMOClient(ClientMgr mgr, ClientVO cl)
    {
	this.clientMgr = mgr;
	this.clientVO = cl;
    }

    public ClientVO get()
    {
	return this.clientVO;
    }

    public void initializeDefaultClient()
    {
	ClientVO clientVO = new ClientVO();

	try
	{
	    clientVO.setMadeID(SecurityHelper.getRoadsideUser().getUserID());
	    clientVO.setMadeDate(new DateTime());
	    clientVO.setStatus(Client.STATUS_ACTIVE);
	    clientVO.setSourceSystem(SourceSystem.MEMBERSHIP);
	    clientVO.setAddressTitle("");
	    clientVO.setBirthDateString("");
	    clientVO.setContactPersonName("");
	    clientVO.setContactPersonPhone("");
	    clientVO.setDriversLicenseNumber("");
	    clientVO.setGroupClient(false);
	    clientVO.setInitials("");
	    clientVO.setMaritalStatus(false);
	    clientVO.setMarket(false);
	    clientVO.setMemoNumber(0);
	    clientVO.setMotorNewsDeliveryMethod("");
	    clientVO.setMotorNewsSendOption("");
	    clientVO.setOrganisationPhone("");
	    clientVO.setPhonePassword("");
	    clientVO.setPhoneQuestion("");
	    clientVO.setPostalName("");
	    clientVO.setPostProperty("");
	    clientVO.setResiProperty("");
	    clientVO.setResiStreetChar("");
	    clientVO.setSalutation("");
	    clientVO.setSmsAllowed(false);
	}
	catch (RemoteException e)
	{
	    // If this fails, what should we do it clearly failed. Its not a
	    // business rule failure, and likely
	    // will not work for any others as the whole thing is broken.
	}

	this.clientVO = clientVO;
    }

    private void initialize()
    {

	this.initializeDefaultClient();

	try
	{

	    if (line.get(CMOLineItem.FIELD_TITLE).equalsIgnoreCase("Mrs")
		    || line.get(CMOLineItem.FIELD_TITLE).equalsIgnoreCase(
			    "Miss")
		    || line.get(CMOLineItem.FIELD_TITLE).equalsIgnoreCase("Ms"))
	    {

		clientVO.setGender("Female");
		clientVO.setSex(false);

	    }
	    else if (line.get(CMOLineItem.FIELD_TITLE).equalsIgnoreCase("Mr"))
	    {

		clientVO.setGender("Male");
		clientVO.setSex(true);

	    }
	    else
	    {

		clientVO.setGender("Male");
		clientVO.setSex(true);

	    }

	    clientVO.setBirthDate(line.mustGetDobDate());
	    clientVO.setGivenNames(line.get(CMOLineItem.FIELD_FIRST_NAME)
		    .toUpperCase());
	    clientVO.setHomePhone(line.get(CMOLineItem.FIELD_HOME_PHONE));
	    clientVO.setWorkPhone(line.get(CMOLineItem.FIELD_BUSINESS_PHONE));
	    clientVO.setMobilePhone(line.get(CMOLineItem.FIELD_MOBILE_PHONE));
	    clientVO.setEmailAddress(line.get(CMOLineItem.FIELD_EMAIL));
	    clientVO.setOrganisationName(line
		    .get(CMOLineItem.FIELD_BUSINESS_NAME));
	    clientVO.setTitle(line.get(CMOLineItem.FIELD_TITLE).toUpperCase());
	    clientVO.setSurname(line.get(CMOLineItem.FIELD_SURNAME)
		    .toUpperCase());
	    clientVO.setLastUpdate(new DateTime());

	    clientVO.setResiStreetChar(line.getResidentialStreetNum());
	    clientVO.setResiStsubId(line.getResidentialStreetCode());
	    clientVO.setPostStreetChar(line.getPostalStreetNum());
	    clientVO.setPostStsubId(line.getPostalStreetCode());

	}
	catch (ValidationException e)
	{
	    // Failed to set something
	    e.printStackTrace();
	}
	catch (RemoteException e)
	{
	    // This should never be called. Treating ClientVO as a Data Object
	    e.printStackTrace();
	}

    }

    public void create() throws RemoteException
    {
	if (!this.existsRemote())
	{
	    this.forceCreate();
	}
    }

    public void forceCreate() throws RemoteException
    {
	ClientVO newClientVO = clientMgr.createClient(this.clientVO,
		SecurityHelper.getRoadsideUser().getUserID(),
		Client.HISTORY_CREATE, "HOB", "CMO Import",
		SourceSystem.CMO.getSystemName());

	this.clientVO = newClientVO;
    }

    public boolean existsRemote() throws RemoteException
    {

	try
	{
	    List<ClientVO> matches = (List<ClientVO>) this.clientMgr
		    .findClientsWithoutRoadside(this.clientVO);

	    if (matches != null && !matches.isEmpty())
	    {
		return true;
	    }
	}
	catch (SystemException e)
	{
	    // Something went wrong with the sql statement included in the call
	    // to findClientsWithoutRoadside.
	    // This means one of two things. EITHER there was an issue
	    // connecting to the database in which case forget it,
	    // this will continue to cause issues OR there was a problem
	    // preparing the statement and some data was put in incorrectly
	    // in which case we should throw an error to say whoops, error with
	    // the data.
	    throw new RemoteException(
		    "Cannot correctly process call to client table");
	}

	return false;
    }

    public Integer getClientNumber()
    {
	return this.clientVO.getClientNumber();
    }

    public boolean isDifferent(CMOLineItem lineItem)
    {
	// TODO Auto-generated method stub
	return false;
    }

    public String getChangeLog(CMOLineItem lineItem) throws RemoteException
    {
	StringBuilder changeReport = new StringBuilder("");

	if (!lineItem.get(CMOLineItem.FIELD_TITLE).trim()
		.equalsIgnoreCase(this.clientVO.getTitle().trim()))
	{
	    changeReport.append("Title="
		    + lineItem.get(CMOLineItem.FIELD_TITLE) + ",");
	}

	if (!lineItem.get(CMOLineItem.FIELD_FIRST_NAME).trim()
		.equalsIgnoreCase(this.clientVO.getGivenNames().trim()))
	{
	    changeReport.append("First name="
		    + lineItem.get(CMOLineItem.FIELD_FIRST_NAME) + ",");
	}

	if (!lineItem.get(CMOLineItem.FIELD_SURNAME).trim()
		.equalsIgnoreCase(this.clientVO.getSurname().trim()))
	{
	    changeReport.append("Surname="
		    + lineItem.get(CMOLineItem.FIELD_SURNAME) + ",");
	}

	if (!lineItem.mustGetDobDate().equals(this.clientVO.getBirthDate()))
	{
	    changeReport.append("DOB=" + lineItem.get(CMOLineItem.FIELD_DOB)
		    + ",");
	}

	if (!lineItem.get(CMOLineItem.FIELD_EMAIL).trim().isEmpty()
		&& !lineItem
			.get(CMOLineItem.FIELD_EMAIL)
			.trim()
			.equalsIgnoreCase(
				this.clientVO.getEmailAddress().trim()))
	{
	    changeReport.append("Email="
		    + lineItem.get(CMOLineItem.FIELD_EMAIL) + ",");
	}

	if (!lineItem.get(CMOLineItem.FIELD_HOME_PHONE).trim().isEmpty()
		&& !lineItem.get(CMOLineItem.FIELD_HOME_PHONE).trim()
			.equalsIgnoreCase(this.clientVO.getHomePhone().trim()))
	{
	    changeReport.append("Home phone="
		    + lineItem.get(CMOLineItem.FIELD_HOME_PHONE) + ",");
	}

	if (!lineItem.get(CMOLineItem.FIELD_BUSINESS_PHONE).trim().isEmpty()
		&& !lineItem.get(CMOLineItem.FIELD_BUSINESS_PHONE).trim()
			.equalsIgnoreCase(this.clientVO.getWorkPhone().trim()))
	{
	    changeReport.append("Work phone="
		    + lineItem.get(CMOLineItem.FIELD_BUSINESS_PHONE) + ",");
	}

	if (!lineItem.get(CMOLineItem.FIELD_MOBILE_PHONE).trim().isEmpty()
		&& !lineItem
			.get(CMOLineItem.FIELD_MOBILE_PHONE)
			.trim()
			.equalsIgnoreCase(this.clientVO.getMobilePhone().trim()))
	{
	    changeReport.append("Mobile phone="
		    + lineItem.get(CMOLineItem.FIELD_MOBILE_PHONE) + ",");
	}

	if (!lineItem.getResidentialStreetNum().trim()
		.equalsIgnoreCase(this.clientVO.getResiStreetChar().trim()))
	{
	    changeReport.append("Residential Street Num="
		    + lineItem.get(CMOLineItem.FIELD_RESI_STREET_NUM) + ",");
	}

	if (!lineItem
		.getResidenitalSreet()
		.trim()
		.equalsIgnoreCase(
			this.clientVO.getResidentialAddress().getStreet()))
	{
	    changeReport.append("Residential Street="
		    + lineItem.get(CMOLineItem.FIELD_RESI_STREET) + ",");
	}

	if (!lineItem
		.get(CMOLineItem.FIELD_RESI_CITY)
		.trim()
		.equalsIgnoreCase(
			this.clientVO.getResidentialAddress().getSuburb()))
	{
	    changeReport.append("Residential Suburb="
		    + lineItem.get(CMOLineItem.FIELD_RESI_CITY) + ",");
	}

	if (!lineItem
		.get(CMOLineItem.FIELD_RESI_POSTCODE)
		.trim()
		.equalsIgnoreCase(
			this.clientVO.getResidentialAddress().getPostcode()))
	{
	    changeReport.append("Residential Postcode="
		    + lineItem.get(CMOLineItem.FIELD_RESI_POSTCODE) + ",");
	}

	if (lineItem.get(CMOLineItem.FIELD_POST_ADDRESS1) != null
		&& !lineItem.get(CMOLineItem.FIELD_POST_ADDRESS1).isEmpty())
	{
	    if (!lineItem.getPostalStreet().trim()
		    .equalsIgnoreCase(this.clientVO.getResiStreetChar().trim()))
	    {
		changeReport
			.append("Postal Street Num="
				+ lineItem
					.get(CMOLineItem.FIELD_POST_STREET_NUM)
				+ ",");
	    }
	    if (!lineItem
		    .getPostalStreetNum()
		    .trim()
		    .equalsIgnoreCase(
			    this.clientVO.getPostalAddress().getStreet()))
	    {
		changeReport.append("Postal Street="
			+ lineItem.get(CMOLineItem.FIELD_POST_STREET) + ",");
	    }
	    if (!lineItem
		    .get(CMOLineItem.FIELD_POST_CITY)
		    .trim()
		    .equalsIgnoreCase(
			    this.clientVO.getPostalAddress().getSuburb()))
	    {
		changeReport.append("Postal Suburb="
			+ lineItem.get(CMOLineItem.FIELD_POST_CITY) + ",");
	    }
	    if (!lineItem
		    .get(CMOLineItem.FIELD_POST_POSTCODE)
		    .trim()
		    .equalsIgnoreCase(
			    this.clientVO.getPostalAddress().getPostcode()))
	    {
		changeReport.append("Postal Postcode="
			+ lineItem.get(CMOLineItem.FIELD_POST_POSTCODE) + ",");
	    }
	}

	return changeReport.toString();
    }
}
