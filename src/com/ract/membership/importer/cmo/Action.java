package com.ract.membership.importer.cmo;

import java.util.ArrayList;
import java.util.List;

public abstract class Action {

	protected ArrayList<String> result;
	protected ArrayList<String> error;
	
	public Action() {
		this.result = new ArrayList<String>();
		this.error = new ArrayList<String>();
	}
	
	abstract public void execute(CMOLineItem lineItem);
	
	public ArrayList<String> result() {
		return this.result;
	}
	
	public ArrayList<String> error() {
		return this.error;
	}
	
	public boolean hasResult() {
		return this.result != null && this.result.size() > 0;
	}
	
	public boolean hasError() {
		return this.error != null && this.error.size() > 0;
	}
}