package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;

import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.common.ReferenceDataVO;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipRefMgrLocal;
import com.ract.util.DateTime;

public class ActionService extends Action {

	private MembershipMgr memMgr;
	private ClientMgr clMgr;
	private MembershipRefMgr refMgr;
	
	public ActionService(ClientMgr clientMgr, MembershipMgr membershipMgr, MembershipRefMgr membershipRegMgr) {
		super();
		this.memMgr = membershipMgr;
		this.clMgr = clientMgr;
		this.refMgr = membershipRegMgr;
	}
	
	@Override
	public void execute(CMOLineItem lineItem) {
		CMOMembership membership = CMOMembership.FindByVin(this.memMgr, lineItem.getVin(), true);
		
		if(membership.exists()) {
			CMOClient client = new CMOClient(this.clMgr, membership.getClient());
			
			if(client.isDifferent(lineItem)) {
				//If it is a different client cancel then create another new client
				this.cancelMembership(membership, lineItem);
				this.createMembership(lineItem);
				
			} else {
				//Its the same client, lets just sync it up.
				this.updateClient(client, lineItem);
				this.updateMembership(membership, lineItem);
			}
		} else {
			this.createMembership(lineItem);
			
		}
	}

	private void updateMembership(CMOMembership membership, CMOLineItem lineItem) {
		try {
			membership.update(lineItem);
			
			this.result.add((new DateTime()).formatLongDate() 
						   + ": Updated Membership (T" + lineItem.get(CMOLineItem.FIELD_TRANS_TYPE) 
						   + ") " + membership.getClientNumber() 
						   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
			
		} catch (RemoteException e) {
			this.error.add((new DateTime()).formatLongDate() 
						  + ": Unable to update membership  (T" + lineItem.get(CMOLineItem.FIELD_TRANS_TYPE) 
						  + ") " + membership.getClientNumber() 
						  + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}				
	}

	private void cancelMembership(CMOMembership membership, CMOLineItem lineItem) {
		try {
			
			ReferenceDataVO cancellationReason = this.refMgr.getCancelReason("OTH");
			membership.cancel(cancellationReason);
			
		} catch (RemoteException e) {
			
			this.error.add((new DateTime()).formatLongDate() 
					   + ": unable to cancel membership : (T" + lineItem.getTransactionType() + ") " 
					   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}
	}

	private void createMembership(CMOLineItem lineItem) {
		try {
			
			//Create the client first
			CMOClient newClient = new CMOClient(this.clMgr, lineItem);
			newClient.forceCreate();
			
			//We havent got a member lets create one. 
			CMOMembership newMembership = new CMOMembership(this.memMgr, lineItem);
			newMembership.create(newClient.getClientNumber());

			this.result.add((new DateTime()).formatLongDate() 
						   + ": Created Membership (T" + lineItem.getTransactionType() + ") " 
						   + newClient.getClientNumber() + " for Nomination ID: " 
						   + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
			
		} catch (RemoteException e) {
			//Couldnt connect to the database :( bummer. 
			//just let someone know. 
			this.error.add((new DateTime()).formatLongDate() 
						   + ": Failed to connect to database : (T" + lineItem.getTransactionType() + ") " 
						   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}
	}
	
	private void updateClient(CMOClient client, CMOLineItem lineItem) {		
		try {
			String changes = client.getChangeLog(lineItem);

			if (!changes.isEmpty()) {
				this.result.add((new DateTime()).formatLongDate() 
							  + ": Client update required (" + client.getClientNumber() 
							  + ") " + changes);
			}
		} catch (RemoteException e) {
			this.error.add((new DateTime()).formatLongDate() 
					  + ": Unable to update client (" + client.getClientNumber() 
					  + ") ");
		}
	}

}
