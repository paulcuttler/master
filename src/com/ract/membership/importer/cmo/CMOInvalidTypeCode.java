package com.ract.membership.importer.cmo;

public class CMOInvalidTypeCode extends Exception {

	public CMOInvalidTypeCode(String message) {
		super(message);
	}

}
