package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;

import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.common.ReferenceDataVO;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipRefMgrLocal;
import com.ract.util.DateTime;

public class ActionChangeOwner extends Action {

	private MembershipMgr memMgr;
	private ClientMgr clMgr;
	private MembershipRefMgr refMgr;
	
	public ActionChangeOwner(ClientMgr cl, MembershipMgr mem, MembershipRefMgr rf) {
		super();
		this.memMgr = mem;
		this.clMgr = cl;
		this.refMgr = rf;
	}
	
	@Override
	public void execute(CMOLineItem lineItem) {
		CMOMembership membership = CMOMembership.FindByVin(this.memMgr, lineItem.getVin(), true);
		
		if(membership.exists()) {
		
			this.cancelMembership(membership, lineItem);
			CMOMembership newMembership = this.createMembership(lineItem);
			
			this.result.add((new DateTime()).formatLongDate() 
							+ ": Replaced Membership (T" + lineItem.get(CMOLineItem.FIELD_TRANS_TYPE) 
							+ ") " + membership.getClientNumber() 
							+ " with " + newMembership.getClientNumber() 
							+ " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
			
		} else {
			this.error.add((new DateTime()).formatLongDate() 
							+ ": Unable to find membership by VIN: " 
							+ lineItem.get(CMOLineItem.FIELD_VIN) + " for Nomination ID: "
							+ lineItem.get(CMOLineItem.FIELD_NOMINATION_ID) + ", VIN: " 
							+ lineItem.get(CMOLineItem.FIELD_VIN) + ", Product Code: " 
							+ lineItem.get(CMOLineItem.FIELD_PROGRAM_CODE));
		}
	}
	
	private CMOMembership createMembership(CMOLineItem lineItem) {
		CMOMembership newMembership = null;
		
		try {
			
			//Create the client first
			CMOClient newClient = new CMOClient(this.clMgr, lineItem);
			newClient.forceCreate();
			
			//We havent got a member lets create one. 
			newMembership = new CMOMembership(this.memMgr, lineItem);
			newMembership.create(newClient.getClientNumber());

			this.result.add((new DateTime()).formatLongDate() 
						   + ": Created Membership (T" + lineItem.getTransactionType() + ") " 
						   + newClient.getClientNumber() + " for Nomination ID: " 
						   + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
			
			
		} catch (RemoteException e) {
			//Couldnt connect to the database :( bummer. 
			//just let someone know. 
			this.error.add((new DateTime()).formatLongDate() 
						   + ": Failed to connect to database : (T" + lineItem.getTransactionType() + ") " 
						   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}
		
		return newMembership;
	}
	
	private void cancelMembership(CMOMembership membership, CMOLineItem lineItem) {
		try {
			
			ReferenceDataVO cancellationReason = this.refMgr.getCancelReason("OTH");
			membership.cancel(cancellationReason);
			
		} catch (RemoteException e) {
			
			this.error.add((new DateTime()).formatLongDate() 
					   + ": unable to cancel membership : (T" + lineItem.getTransactionType() + ") " 
					   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}
	}
}
