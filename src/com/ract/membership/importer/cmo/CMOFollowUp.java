package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;
import java.text.ParseException;

import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientNote;
import com.ract.client.ClientVO;
import com.ract.client.FollowUp;
import com.ract.security.SecurityHelper;
import com.ract.util.DateTime;
import com.ract.util.Interval;

public class CMOFollowUp {
	
	private ClientMgrLocal clientMgrLocal;
	private FollowUp followUp;
	private ClientVO client;
	private ClientVO newClient;
	
	public static CMOFollowUp Create(ClientMgrLocal clMgr, ClientVO client, ClientVO newClient) throws RemoteException {
		CMOFollowUp followUp = new CMOFollowUp(clMgr, client, newClient);
		followUp.create();
		return followUp;
	}
	
	public CMOFollowUp(ClientMgrLocal clMgr, ClientVO client, ClientVO newClient) {
		this.clientMgrLocal = clMgr;
		this.client = client;
		this.newClient = newClient;
	}
	
	public void create() throws RemoteException {
		DateTime due = new DateTime();
		
		try {
			due.add(new Interval("0:0:2:0:0:0")); // 2 days
		} catch (ParseException e) {
			// this can never happen as the interval string is correct and
			// is a hard coded value
		}
		
		this.followUp = new FollowUp(this.newClient, this.client, new DateTime(), SecurityHelper.getRoadsideUser().getUserID());
	
		this.followUp.setFollowUpDate(due);
		this.followUp.setStatus(FollowUp.STATUS_FLAG);
		this.followUp.setNotes("Address change due to CMO Import");
		
		clientMgrLocal.createFollowUp(this.followUp);
	}
}
