package com.ract.membership.importer.cmo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import java.util.Iterator;

import au.com.bytecode.opencsv.CSVReader;

public class CMOFile {
	
	private String inputFile;
	private ArrayList<CMOLineItem> items;
	
	/**
	 * The CMO file is responsible for taking a string file name and creating CMOLineItems from 
	 * each line in the csv (which is what
	 * @param in
	 */
	public CMOFile(String in) {
		this.inputFile = in;
		this.items = new ArrayList<CMOLineItem>(20);
				
		CSVReader reader = null;
		try {

			reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));
			
			String[] line = null;
			
			while ((line = reader.readNext()) != null) {
				items.add(new CMOLineItem(line));
			}
			
		} catch (FileNotFoundException e) {
			// There was a problem, The file was not found (indicates the file was moved after it was initially read 
			// this might happen if the there has been a double execution, in which case we do not care about it specifically
			e.printStackTrace();
		} catch (IOException e) {
			//There was a problem loading the file. Something went wrong reading bytes. 
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public Iterator<CMOLineItem> lineIterator() {
		return this.items.iterator();
	}
	
	public Integer lineCount() {
		return items.size();
	}
	
}
