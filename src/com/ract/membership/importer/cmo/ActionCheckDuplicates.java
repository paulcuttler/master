package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.util.DateTime;

public class ActionCheckDuplicates extends ActionGeneral {
	
	private ClientMgr mgr;
	
	public ActionCheckDuplicates(ClientMgr clMgr) {
		super();
		this.mgr = clMgr;
	}
	
	@Override
	public void execute() {
		try {
			List<Map<String, String>> duplicates = this.mgr.getCmoDuplicateSuggestions();
			
			if (duplicates != null && !duplicates.isEmpty()) {				
				StringBuilder dupLog = new StringBuilder("");
				dupLog.append("clientNo,masterClientNo,givenNames,masterGivenNames,surname,resiStreetChar,masterResiStreetChar,street,suburb,birthDate,productCode,profileCode");
				dupLog.append("\n");
				
				for (Map<String, String> record : duplicates) {
					dupLog.append(record.get("clientNo") + "," + record.get("masterClientNo") + "," + record.get("givenNames") + "," + record.get("masterGivenNames") + "," + record.get("surname") + "," + record.get("resiStreetChar") + "," + record.get("masterResiStreetChar") + "," + record.get("street") + "," + record.get("suburb") + "," + record.get("birthDate") + "," + record.get("productCode") + "," + record.get("profileCode"));
					dupLog.append("\n");
				}
				
				this.result.add(dupLog.toString());
			}
		} catch (RemoteException e) {
			// We can't connect to the database, therefore there must be a problem. Its ok though because it will
			// get picked up in the next time the CMO's are imported. 
			this.error.add((new DateTime()).formatLongDate() + " Unable to process duplicates for file");
		}
	}

}
