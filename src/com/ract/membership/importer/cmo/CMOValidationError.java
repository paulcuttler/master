package com.ract.membership.importer.cmo;

public class CMOValidationError extends Exception {
	
	public CMOValidationError(String string) {
		super(string);
	}

}
