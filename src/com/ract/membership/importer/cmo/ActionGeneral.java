package com.ract.membership.importer.cmo;

import java.util.ArrayList;
import java.util.List;

public abstract class ActionGeneral {
	
	protected List<String> result;
	protected List<String> error;
	
	public ActionGeneral() {
		this.result = new ArrayList<String>();
		this.error = new ArrayList<String>();
	}
	
	public abstract void execute();
	
	public List<String> result() {
		return this.result;
	}
	
	public List<String> error() {
		return this.error;
	}
	
	public boolean hasResult() {
		return this.result != null && this.result.size() > 0;
	}
	
	public boolean hasError() {
		return this.error != null && this.error.size() > 0;
	}
}
