package com.ract.membership.importer.cmo;

import java.rmi.RemoteException;

import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.util.Context;
import com.ract.util.DateTime;
import com.ract.util.ThreadContext;

public class ActionNew extends Action {
	
	private ClientMgr clMgr;
	private MembershipMgr memMgr;
	
	public ActionNew(ClientMgr cl, MembershipMgr mem) {
		super();
		this.clMgr = cl;
		this.memMgr = mem;
	}
	
	@Override
	public void execute(CMOLineItem lineItem) {
		Context context = new Context();
		ThreadContext.set(context);
		System.out.println(context.Get());

		CMOMembership membership = CMOMembership.FindByVin(this.memMgr, lineItem.getVin(), false);
		
		if(membership.exists()) {
			this.error.add((new DateTime()).formatLongDate() 
						  + ": Cannot create membership (T" + lineItem.getTransactionType() 
						  + ") for VIN: " + lineItem.getVin()
						  + ", " + membership.get().getClientNumber()
						  + " already exists for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID) 
						  + ", VIN: " + lineItem.getVin() 
						  + ", Product Code: " + lineItem.getProductCode());
			
			return;
		}
		
		
		try {
			//Create the client first
			CMOClient newClient = new CMOClient(this.clMgr, lineItem);
			newClient.forceCreate();
			
			//We havent got a member lets create one. 
			CMOMembership newMembership = new CMOMembership(this.memMgr, lineItem);
			newMembership.create(newClient.getClientNumber());
			
			this.result.add((new DateTime()).formatLongDate() 
						   + ": Created Membership (T" + lineItem.getTransactionType() + ") " 
						   + newClient.getClientNumber() + " for Nomination ID: " 
						   + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
			
		} catch (RemoteException e) {
			//Couldnt connect to the database :( bummer. 
			//just let someone know. 
			this.error.add((new DateTime()).formatLongDate() 
						   + ": Failed to connect to database : (T" + lineItem.getTransactionType() + ") " 
						   + " for Nomination ID: " + lineItem.get(CMOLineItem.FIELD_NOMINATION_ID));
		}
		
		ThreadContext.unset();
	}
	
}
