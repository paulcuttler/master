package com.ract.membership.importer;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;
import com.ract.util.LogUtil;

/**
 * The Class FcmoImporter.
 */
public class FcmoImporter implements Job {

    /** The import mgr. */
    ImportMgr importMgr;
    
    /** The common mgr local. */
    CommonMgr commonMgrLocal;

    /** Instantiates a new fcmo importer.
     */
    public FcmoImporter() {
	System.out.println("*** instantiating FcmoImporter");
    }

    /* (non-Javadoc)
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {

	commonMgrLocal = CommonEJBHelper.getCommonMgr();
	importMgr = ImportEJBHelper.getImportMgr();

	String outputDirectoryName = null;
	try {
	    outputDirectoryName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.CMO_BASE_DIR);
	} catch (RemoteException e) {
	    LogUtil.fatal(getClass(), "Unable to determine CMO Base Dir: " + e.getMessage());
	    return;
	}
	if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') 
	{
	    outputDirectoryName += "/";
	}

	String inFolder = outputDirectoryName + "incoming" + "/";
	String inputFile = null;
	String outFolder = outputDirectoryName + "processed" + "/";

	System.out.println("Incoming path : " + inFolder);

	File folder = new File(inFolder);

	// read file
	for (File file : folder.listFiles()) 
	{
	    inputFile = file.getAbsolutePath();

	    try 
	    {
		// process file
		importCmoMembers(inputFile);

		// move file
		file.renameTo(new File(outFolder + file.getName()));
	    } catch (Exception e) {
		LogUtil.fatal(getClass(), "Could not process CMO file: " + inputFile + " : " + e);
	    }
	}

	try {
	    importMgr.emailResults();
	} catch (IOException e) {
	    LogUtil.fatal(getClass(), "Could not email results of CMO Import: " + e);
	}
    }

    /** Import cmo members.
     * 
     * @param inputFile
     *            the input file
     * @throws RemoteException
     *             the remote exception
     */
    private void importCmoMembers(String inputFile) throws RemoteException {
	System.out.println(" ======================================================== ");
	System.out.println(" CMO Member file      " + inputFile);
	System.out.println(" Starting             " + new Date().toString());
	System.out.println(" ======================================================== ");

	importMgr.processCmoFile(inputFile);

	System.out.println(" CMO Import Finished             " + new Date().toString());
    }

//rlg
    public static void main()
    {
	JobExecutionContext jec = null;
	
	FcmoImporter fc = new FcmoImporter();
	try
	{
	    fc.execute(jec);
	}
	catch ( Exception e)
	{
	    System.out.println (e.getMessage());
	}
    }
//rlg  
}
