package com.ract.membership.importer;

import com.ract.common.ServiceLocator;
import com.ract.common.ServiceLocatorException;

public class ImportEJBHelper {
    public static ImportMgr getImportMgr() {
	ImportMgr importMgr = null;
	try {
	    importMgr = (ImportMgr) ServiceLocator.getInstance().getObject("ImportMgrBean/remote");
	} catch (ServiceLocatorException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return importMgr;
    }

}
