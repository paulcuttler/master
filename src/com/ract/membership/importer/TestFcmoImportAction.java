package com.ract.membership.importer;

import com.opensymphony.xwork2.ActionSupport;

public class TestFcmoImportAction extends ActionSupport {

	public String execute() throws Exception {
		 try {
			 FcmoImporter fcmoImporter = new FcmoImporter();
			 fcmoImporter.execute(null);
		 } catch(Exception e) {
			 System.out.println(e);
		 }
		 
		 return SUCCESS;
	 }
		
}
