package com.ract.membership.importer;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Date;

public class TestImportMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/** The import mgr. */
		ImportMgr importMgr;

		/**
		 * Instantiates a new fcmo importer.
		 */
		System.out.println("*** instantiating FcmoImporter");

		importMgr = ImportEJBHelper.getImportMgr();

		String inFolder = "c:/devtools/test";
		String inputFile = null;

		System.out.println("Incoming path : " + inFolder);

		File folder = new File(inFolder);

		// read file
		for (File file : folder.listFiles()) {
			inputFile = file.getAbsolutePath();

			// process file
			System.out.println(" ======================================================== ");
			System.out.println(" CMO Member file      " + inputFile);
			System.out.println(" Starting             " + new Date().toString());
			System.out.println(" ======================================================== ");

			try {
				importMgr.processCmoFile(inputFile);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println(" CMO Import Finished             " + new Date().toString());
		}
	}
}
