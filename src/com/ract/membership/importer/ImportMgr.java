package com.ract.membership.importer;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.ejb.Remote;

@Remote
public interface ImportMgr {

    public void processCmoFile(String inputFile) throws RemoteException;

    public void generateExportXML() throws IOException;

    public void emailResults() throws IOException;
}
