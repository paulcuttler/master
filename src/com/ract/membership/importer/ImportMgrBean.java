package com.ract.membership.importer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;
import javax.transaction.RollbackException;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.client.Client;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientNote;
import com.ract.client.ClientVO;
import com.ract.client.FollowUp;
import com.ract.client.NoteType;
import com.ract.common.AddressVO;
import com.ract.common.BusinessType;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SourceSystem;
import com.ract.common.StreetSuburbVO;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.cad.CadMembershipData;
import com.ract.common.cad.fleet.FleetClientData;
import com.ract.common.mail.MailMessage;
import com.ract.insurance.Policy;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipRefMgrLocal;
import com.ract.membership.MembershipTransactionMgrLocal;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.TransactionGroup;
import com.ract.membership.TransactionGroupFactory;
import com.ract.security.SecurityHelper;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.web.insurance.InsAdapter;

@Stateless
@Remote({ ImportMgr.class })
@Local({ ImportMgrLocal.class })
public class ImportMgrBean {

	@Resource
	private SessionContext sessionContext;

	@Resource(mappedName = "java:/ClientDS")
	public DataSource dataSource;

	@EJB
	private ClientMgrLocal clientMgrLocal;

	@EJB
	private CommonMgrLocal commonMgrLocal;

	@EJB
	private MembershipTransactionMgrLocal txnMgrLocal;

	@EJB
	private MembershipMgrLocal membershipMgrLocal;

	@EJB
	private MembershipRefMgrLocal refMgrLocal;

	private final String TRANS_TYPE_NEW_VEHICLE = "11";
	private final String TRANS_TYPE_DEMO_VEHICLE = "14";
	private final String TRANS_TYPE_CHANGE_DETAIL = "21";
	private final String TRANS_TYPE_CHANGE_OWNER = "22";
	private final String TRANS_TYPE_SERVICE = "97";

	private final String OWNER_TYPE_PRIVATE = "P";
	private final String OWNER_TYPE_SMALL_BUSINESS = "B1";

	private final String FIELD_IDENTIFIER = "Identifier";
	private final String FIELD_PROGRAM_CODE = "ProgramCode";
	private final String FIELD_TRANS_TYPE = "Trans_Type";
	private final String FIELD_NOMINATION_DATE = "NominationDate";
	private final String FIELD_SERVICE_DATE = "ServiceDate";
	private final String FIELD_START_DATE = "StartDate";
	private final String FIELD_EXPIRATION_DATE = "ExpirationDate";
	private final String FIELD_VIN = "VIN";
	private final String FIELD_MAKE = "Make";
	private final String FIELD_MODEL = "Model";
	private final String FIELD_REGO = "Rego";
	private final String FIELD_YEAR = "YearOfRelease";
	private final String FIELD_OWNER_TYPE = "OwnerType";
	private final String FIELD_TITLE = "Title";
	private final String FIELD_FIRST_NAME = "FirstName";
	private final String FIELD_SURNAME = "Surname";
	private final String FIELD_BUSINESS_NAME = "BusinessName";
	private final String FIELD_DOB = "DOB";
	private final String FIELD_HOME_PHONE = "HomePhone";
	private final String FIELD_BUSINESS_PHONE = "BusinessPhone";
	private final String FIELD_MOBILE_PHONE = "MobilePhone";
	private final String FIELD_EMAIL = "Email";
	private final String FIELD_RESI_ADDRESS1 = "AddressLine1";
	private final String FIELD_RESI_ADDRESS2 = "AddressLine2";

	private final String FIELD_RESI_ADDRESS = "Address";
	private final String FIELD_RESI_STREET = "AddressStreet";
	private final String FIELD_RESI_STREET_NUM = "AddressStreetNum";

	private final String FIELD_RESI_CITY = "City";
	private final String FIELD_RESI_STATE = "State";
	private final String FIELD_RESI_POSTCODE = "Postcode";
	private final String FIELD_POST_ADDRESS1 = "PostalAddressLine1";
	private final String FIELD_POST_ADDRESS2 = "PostalAddressLine2";

	private final String FIELD_POST_ADDRESS = "PostalAddress";
	private final String FIELD_POST_STREET = "PostalAddressStreet";
	private final String FIELD_POST_STREET_NUM = "PostalAddressStreetNum";

	private final String FIELD_POST_CITY = "PostalCity";
	private final String FIELD_POST_STATE = "PostalState";
	private final String FIELD_POST_POSTCODE = "PostalPostcode";
	private final String FIELD_NOMINATION_ID = "NominationID";

	private final String FILE_SUFFIX_WARN = "-warn";
	private final String FILE_SUFFIX_ERROR = "-error";
	private final String FILE_SUFFIX_DUPLICATES = "-duplicates";

	private Interval oneYear;
	private Interval zeroYear;

	public static Map<String, String> streetTypeAbbr;
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	public final static String REGEXP_PO_BOX = "P ?O Box";

	private FileWriter fileWriter;
	private FileWriter warnFileWriter;
	private FileWriter errorFileWriter;
	private FileWriter duplicatesFileWriter;
	private Map<String, String> headerLine = new LinkedHashMap<String, String>();

	public void processCmoFile(String inputFile) throws IOException {

		try {
			oneYear = new Interval(1, 0, 0, 0, 0, 0, 0);
		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), "Unable to create oneYear interval ");
			throw new IOException(e);
		}

		try {
			zeroYear = new Interval(0, 0, 0, 0, 0, 0, 0);
		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), "Unable to create zeroYear interval ");
			throw new IOException(e);
		}

		fileWriter = null;
		warnFileWriter = null;
		errorFileWriter = null;
		duplicatesFileWriter = null;
		LogUtil.log(this.getClass(), (new DateTime()).formatLongDate() + ": processing file: " + inputFile);

		getLogFileWriter().write((new DateTime()).formatLongDate() + ": processing file: " + inputFile + '\n');

		CSVReader reader = null;
		headerLine.clear(); // the files have different formats

		try {
			// Open the file
			reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFile)));

			String[] line = null;

			// Process the file line by line

			while ((line = reader.readNext()) != null) {
				try {
					processLine(line);
				} catch (Exception e) {
					// getErrorFileWriter().write("Unable to parse record: " +
					// (line.length > 8 ? line[8] : "(invalid line)") + " : " +
					// e.getMessage() + ": " + '\n');
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.fatal(this.getClass(), "Unable to read the CMO import file: " + inputFile + ": " + e);
		} finally {
			try {
				reader.close();
			} catch (Exception e) {
				// ignore
			}
		}

		// processDuplicates();

		if (fileWriter != null) {
			fileWriter.flush();
			fileWriter.close();
		}
		if (warnFileWriter != null) {
			warnFileWriter.flush();
			warnFileWriter.close();
		}
		if (errorFileWriter != null) {
			errorFileWriter.flush();
			errorFileWriter.close();
		}
		if (duplicatesFileWriter != null) {
			duplicatesFileWriter.flush();
			duplicatesFileWriter.close();
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void processLine(String[] line) throws IOException, RollbackException {

		if (headerLine.isEmpty()) {
			// Create Header Map
			headerLine = createHeader(line);
		} else {
			Map<String, String> workingHeaderLine = new LinkedHashMap<String, String>();

			// Populate the working header line with base (working line gets
			// modified later)
			final Set<Map.Entry<String, String>> entries = headerLine.entrySet();
			for (Map.Entry<String, String> entry : entries) {
				workingHeaderLine.put(entry.getKey(), "");
			}

			parseInputLine(line, workingHeaderLine);
			if (workingHeaderLine.get(FIELD_IDENTIFIER).equals("02")) {
				try {
					processThisMembership(workingHeaderLine);
				} catch (ParseException e) {
					getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Unable to parse record: " + e.getMessage() + ": for Nomination ID: " + workingHeaderLine.get(FIELD_NOMINATION_ID) + ", VIN: " + workingHeaderLine.get(FIELD_VIN) + ", Product Code: " + workingHeaderLine.get(FIELD_PROGRAM_CODE) + '\n');
				} catch (GenericException g) {
					getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Unable to process record: " + g.getMessage() + ": for Nomination ID: " + workingHeaderLine.get(FIELD_NOMINATION_ID) + ", VIN: " + workingHeaderLine.get(FIELD_VIN) + ", Product Code: " + workingHeaderLine.get(FIELD_PROGRAM_CODE) + '\n');

					throw new RollbackException(g.getMessage());
					// throw new SystemException (g.getMessage());
				}

			} else {
				System.out.println("Ignoring non-data row.");
			}
		}
	}

	/**
	 * Process line of membership data.
	 * 
	 * @param membership
	 * @throws ParseException
	 * @throws IOException
	 * @throws GenericException
	 */

	private void processThisMembership(Map<String, String> membership) throws ParseException, IOException, GenericException {

		if (!membership.get(FIELD_RESI_STATE).equalsIgnoreCase(AddressVO.STATE_TAS)) {
			getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Non-Tasmanian addresses are not supported: " + membership.get(FIELD_RESI_STATE) + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			return;
		}
		if (Pattern.compile(REGEXP_PO_BOX, Pattern.CASE_INSENSITIVE).matcher(membership.get(FIELD_RESI_ADDRESS1)).find()) {
			getErrorFileWriter().write((new DateTime()).formatLongDate() + ": PO Box-style addresses are not permitted for Residential address: " + membership.get(FIELD_RESI_ADDRESS1) + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			return;
		}
		if (membership.get(FIELD_TITLE).isEmpty()) {
			getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Title cannot be blank for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			return;
		}
		if (membership.get(FIELD_FIRST_NAME).isEmpty()) {
			getErrorFileWriter().write((new DateTime()).formatLongDate() + ": First name cannot be blank for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			return;
		}
		if (membership.get(FIELD_SURNAME).isEmpty()) {
			getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Surname cannot be blank for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			return;
		}

		String allowedProgramCodes = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_CMO_PROFILE);
		if (allowedProgramCodes.indexOf(membership.get(FIELD_PROGRAM_CODE)) == -1) {
			getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Unknown Program Code: " + membership.get(FIELD_PROGRAM_CODE) + ", for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			return;
		}

		DateTime nominationDate = new DateTime(df.parse(membership.get(FIELD_NOMINATION_DATE)));
		DateTime dobDate = new DateTime(df.parse(membership.get(FIELD_DOB)));
		DateTime serviceDate = null;
		DateTime cmoExpiryDate = null;

		LogUtil.log(this.getClass(), "Processing " + membership.get(FIELD_SURNAME) + " " + membership.get(FIELD_REGO));

		// Create new membership
		if (membership.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_NEW_VEHICLE) || membership.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_DEMO_VEHICLE)) {

			LogUtil.log(this.getClass(), "New Vehicle " + membership.get(FIELD_PROGRAM_CODE) + ":" + membership.get(FIELD_SURNAME) + " " + membership.get(FIELD_REGO));

			List<MembershipVO> memList = membershipMgrLocal.findMembershipByVin(membership.get(FIELD_VIN), ProductVO.PRODUCT_CMO);
			// shouldn't exist
			if (!memList.isEmpty()) {
				MembershipVO memVO = memList.get(0);
				getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Cannot create membership (T" + membership.get(FIELD_TRANS_TYPE) + ") for VIN: " + membership.get(FIELD_VIN) + ", " + memVO.getClientNumber() + " already exists for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
				return;
			}

			cmoExpiryDate = calcExpiryDate(membership);

			if (cmoExpiryDate.before(nominationDate)) {
				getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Cannot create membership (T" + membership.get(FIELD_TRANS_TYPE) + ") for VIN: " + membership.get(FIELD_VIN) + " Cannot calculate expiry date " + cmoExpiryDate.formatLongDate() + ". Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
				return;
			}
			// create client
			ClientVO newClientVO = createClient(membership);

			// create membership
			membershipMgrLocal.createCMOMembership(newClientVO.getClientNumber(), null, nominationDate, membership.get(FIELD_REGO), membership.get(FIELD_VIN), membership.get(FIELD_MAKE), membership.get(FIELD_MODEL), membership.get(FIELD_YEAR), membership.get(FIELD_PROGRAM_CODE), cmoExpiryDate);

			getLogFileWriter().write((new DateTime()).formatLongDate() + ": Created Membership (T" + membership.get(FIELD_TRANS_TYPE) + ") " + newClientVO.getClientNumber() + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + '\n');

			// Update expiry on existing membership
		} else if (membership.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_SERVICE)) {

			LogUtil.log(this.getClass(), "Update " + membership.get(FIELD_SURNAME) + " " + membership.get(FIELD_REGO));
			serviceDate = new DateTime(df.parse(membership.get(FIELD_SERVICE_DATE)));

			// lookup membership
			List<MembershipVO> memList = membershipMgrLocal.findMembershipByVin(membership.get(FIELD_VIN), ProductVO.PRODUCT_CMO);
			if (memList.isEmpty()) { // No Current CMO, try single Cancelled
										// CMO?
				memList = membershipMgrLocal.findMembershipByVin(membership.get(FIELD_VIN), ProductVO.PRODUCT_CMO, MembershipVO.STATUS_CANCELLED);
				if (memList != null && memList.size() != 1) {
					memList.clear(); // non-unique Cancelled CMO
				}
			}

			if (!memList.isEmpty()) {
				MembershipVO memVO = memList.get(0);
				ClientVO clientVO = memVO.getClient();

				// has owner changed - surname & DOB
				if (!clientVO.getSurname().equalsIgnoreCase(membership.get(FIELD_SURNAME)) && !clientVO.getBirthDate().equals(dobDate)) {
					replaceMembership(membership, memVO, serviceDate);
				} else {
					// update details on existing client
					updateClient(membership, clientVO);

					// update expiry add 1 year

					DateTime expiryDate = calcExpiryDate(membership);
					// memVO.setExpiryDate(expiryDate);
					// memVO.setStatus(MembershipVO.STATUS_ACTIVE);

					// update membership details
					updateMembership(membership, memVO, expiryDate);
				}

			} else { // could not find member, create new
				LogUtil.log(this.getClass(), "Creating Membership " + membership.get(FIELD_SURNAME) + " " + membership.get(FIELD_REGO));
				ClientVO newClientVO = createClient(membership);

				// create membership
				membershipMgrLocal.createCMOMembership(newClientVO.getClientNumber(), null, serviceDate, membership.get(FIELD_REGO), membership.get(FIELD_VIN), membership.get(FIELD_MAKE), membership.get(FIELD_MODEL), membership.get(FIELD_YEAR), membership.get(FIELD_PROGRAM_CODE), null);

				getLogFileWriter().write((new DateTime()).formatLongDate() + ": Created Membership (T" + membership.get(FIELD_TRANS_TYPE) + ") " + newClientVO.getClientNumber() + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + '\n');
			}

			// Change Detail
		} else if (membership.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_CHANGE_DETAIL)) {
			LogUtil.log(this.getClass(), "Change " + membership.get(FIELD_SURNAME) + " " + membership.get(FIELD_REGO));
			// lookup by VIN
			List<MembershipVO> memList = membershipMgrLocal.findMembershipByVin(membership.get(FIELD_VIN), ProductVO.PRODUCT_CMO);
			if (!memList.isEmpty()) {
				MembershipVO memVO = memList.get(0);
				ClientVO clientVO = memVO.getClient();

				// update details on existing client
				updateClient(membership, clientVO);

				// update membership details
				updateMembership(membership, memVO, null);

			} else { // could not find member
				getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Unable to find membership by VIN: " + membership.get(FIELD_VIN) + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			}

			// Change Owner
		} else if (membership.get(FIELD_TRANS_TYPE).equals(TRANS_TYPE_CHANGE_OWNER)) {
			LogUtil.log(this.getClass(), "New Owner " + membership.get(FIELD_SURNAME) + " " + membership.get(FIELD_REGO));
			// lookup by VIN
			List<MembershipVO> memList = membershipMgrLocal.findMembershipByVin(membership.get(FIELD_VIN), ProductVO.PRODUCT_CMO);
			if (!memList.isEmpty()) {
				MembershipVO memVO = memList.get(0);
				replaceMembership(membership, memVO, nominationDate);

			} else { // could not find member
				getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Unable to find membership by VIN: " + membership.get(FIELD_VIN) + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
			}

		} else {
			getErrorFileWriter().write((new DateTime()).formatLongDate() + ": Unknown transaction type: " + membership.get(FIELD_TRANS_TYPE) + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + ", VIN: " + membership.get(FIELD_VIN) + ", Product Code: " + membership.get(FIELD_PROGRAM_CODE) + '\n');
		}
	}

	/**
	 * Replace membership by cancelling previous version.
	 * 
	 * @param membership new membership details
	 * @param memVO old membership being replaced
	 * @param effectiveDate
	 * @throws IOException
	 * @throws ParseException
	 * @throws GenericException
	 */
	private void replaceMembership(Map<String, String> membership, MembershipVO memVO, DateTime effectiveDate) throws IOException, ParseException, GenericException {
		// cancel membership
		cancelMembership(memVO);

		// create new client
		ClientVO newClientVO = initClientDetails(membership);
		newClientVO = clientMgrLocal.createClient(newClientVO, SecurityHelper.getRoadsideUser().getUserID(), Client.HISTORY_CREATE, "HOB", "CMO Import", SourceSystem.CMO.getSystemName());

		DateTime cmoExpiryDate = calcExpiryDate(membership);

		// create new membership

		membershipMgrLocal.createCMOMembership(newClientVO.getClientNumber(), null, effectiveDate, membership.get(FIELD_REGO), membership.get(FIELD_VIN), membership.get(FIELD_MAKE), membership.get(FIELD_MODEL), membership.get(FIELD_YEAR), membership.get(FIELD_PROGRAM_CODE), cmoExpiryDate);

		getLogFileWriter().write((new DateTime()).formatLongDate() + ": Replaced Membership (T" + membership.get(FIELD_TRANS_TYPE) + ") " + memVO.getClientNumber() + " with " + newClientVO.getClientNumber() + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + '\n');
	}

	/**
	 * Create client with provided data. Create follow-ups as required.
	 * 
	 * @param membership
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 * @throws GenericException
	 */
	private ClientVO createClient(Map<String, String> membership) throws IOException, ParseException, GenericException {
		ClientVO newClientVO = initClientDetails(membership);

		// check for duplicates with no Roadside
		List<ClientVO> matches = (List<ClientVO>) clientMgrLocal.findClientsWithoutRoadside(newClientVO);
		if (matches != null && !matches.isEmpty()) {
			newClientVO = matches.get(0);

			if (matches.size() > 1) { // more than one match, pick one with INS
										// business
				InsAdapter insAdapter = new InsAdapter();
				for (ClientVO c : matches) {
					Collection<Policy> policies = insAdapter.getClientPolicies(c.getClientNumber(), true, false, false);
					if (policies != null && !policies.isEmpty()) {
						newClientVO = c;
						break;
					}
				}
			}

			LogUtil.info(getClass(), "Reusing clientNo: " + newClientVO.getClientNumber());

		} else { // no duplicates, create new
			newClientVO = clientMgrLocal.createClient(newClientVO, SecurityHelper.getRoadsideUser().getUserID(), Client.HISTORY_CREATE, "HOB", "CMO Import", SourceSystem.CMO.getSystemName());
			checkFollowUp(null, newClientVO);
		}

		return newClientVO;
	}

	/**
	 * Update client with provided data. Create follow-ups as required.
	 * 
	 * @param membership
	 * @param oldClientVO
	 * @throws IOException
	 * @throws ParseException
	 * @throws GenericException
	 */
	private void updateClient(Map<String, String> membership, ClientVO oldClientVO) throws IOException, ParseException, GenericException {

		String systemName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);

		StringBuilder changeReport = new StringBuilder("");

		// report on changed client details
		if (!membership.get(FIELD_TITLE).trim().equalsIgnoreCase(oldClientVO.getTitle().trim())) {
			changeReport.append("Title=" + membership.get(FIELD_TITLE) + ",");
		}

		if (!membership.get(FIELD_FIRST_NAME).trim().equalsIgnoreCase(oldClientVO.getGivenNames().trim())) {
			changeReport.append("First name=" + membership.get(FIELD_FIRST_NAME) + ",");
		}

		if (!membership.get(FIELD_SURNAME).trim().equalsIgnoreCase(oldClientVO.getSurname().trim())) {
			changeReport.append("Surname=" + membership.get(FIELD_SURNAME) + ",");
		}

		if (!membership.get(FIELD_DOB).trim().equals(df.format(oldClientVO.getBirthDate()))) {
			changeReport.append("DOB=" + membership.get(FIELD_DOB) + ",");
		}

		try {
			// only set if Production
			if (systemName.equals(CommonConstants.SYSTEM_PRODUCTION)) {
				if (!membership.get(FIELD_EMAIL).trim().isEmpty() && !membership.get(FIELD_EMAIL).trim().equalsIgnoreCase(oldClientVO.getEmailAddress().trim())) {
					changeReport.append("Email=" + membership.get(FIELD_EMAIL) + ",");
				}
			} else {
				LogUtil.info(this.getClass(), "Ignoring email as environment is " + systemName);
			}
		} catch (Exception e) {
		}

		if (!membership.get(FIELD_HOME_PHONE).trim().isEmpty() && !membership.get(FIELD_HOME_PHONE).trim().equalsIgnoreCase(oldClientVO.getHomePhone().trim())) {
			changeReport.append("Home phone=" + membership.get(FIELD_HOME_PHONE) + ",");
		}

		if (!membership.get(FIELD_BUSINESS_PHONE).trim().isEmpty() && !membership.get(FIELD_BUSINESS_PHONE).trim().equalsIgnoreCase(oldClientVO.getWorkPhone().trim())) {
			changeReport.append("Work phone=" + membership.get(FIELD_BUSINESS_PHONE) + ",");
		}

		try {
			// only set if Production
			if (systemName.equals(CommonConstants.SYSTEM_PRODUCTION)) {
				if (!membership.get(FIELD_MOBILE_PHONE).trim().isEmpty() && !membership.get(FIELD_MOBILE_PHONE).trim().equalsIgnoreCase(oldClientVO.getMobilePhone().trim())) {
					changeReport.append("Mobile phone=" + membership.get(FIELD_MOBILE_PHONE) + ",");
				}
			} else {
				LogUtil.info(this.getClass(), "Ignoring mobile phone as environment is " + systemName);
			}
		} catch (Exception e) {
		}

		membership.put(FIELD_RESI_ADDRESS, membership.get(FIELD_RESI_ADDRESS1) + " " + membership.get(FIELD_RESI_ADDRESS2));
		membership = cleanAddress(membership, FIELD_RESI_ADDRESS, FIELD_RESI_STREET, FIELD_RESI_STREET_NUM);

		if (!membership.get(FIELD_RESI_STREET_NUM).trim().equalsIgnoreCase(oldClientVO.getResiStreetChar().trim())) {
			changeReport.append("Residential Street Num=" + membership.get(FIELD_RESI_STREET_NUM) + ",");
		}
		if (!membership.get(FIELD_RESI_STREET).trim().equalsIgnoreCase(oldClientVO.getResidentialAddress().getStreet())) {
			changeReport.append("Residential Street=" + membership.get(FIELD_RESI_STREET) + ",");
		}
		if (!membership.get(FIELD_RESI_CITY).trim().equalsIgnoreCase(oldClientVO.getResidentialAddress().getSuburb())) {
			changeReport.append("Residential Suburb=" + membership.get(FIELD_RESI_CITY) + ",");
		}
		if (!membership.get(FIELD_RESI_POSTCODE).trim().equalsIgnoreCase(oldClientVO.getResidentialAddress().getPostcode())) {
			changeReport.append("Residential Postcode=" + membership.get(FIELD_RESI_POSTCODE) + ",");
		}

		if (membership.get(FIELD_POST_ADDRESS1) != null && !membership.get(FIELD_POST_ADDRESS1).isEmpty()) {
			membership.put(FIELD_POST_ADDRESS, membership.get(FIELD_POST_ADDRESS1) + " " + membership.get(FIELD_POST_ADDRESS2));
			membership = cleanAddress(membership, FIELD_POST_ADDRESS, FIELD_POST_STREET, FIELD_POST_STREET_NUM);

			if (!membership.get(FIELD_POST_STREET_NUM).trim().equalsIgnoreCase(oldClientVO.getResiStreetChar().trim())) {
				changeReport.append("Postal Street Num=" + membership.get(FIELD_POST_STREET_NUM) + ",");
			}
			if (!membership.get(FIELD_POST_STREET).trim().equalsIgnoreCase(oldClientVO.getPostalAddress().getStreet())) {
				changeReport.append("Postal Street=" + membership.get(FIELD_POST_STREET) + ",");
			}
			if (!membership.get(FIELD_POST_CITY).trim().equalsIgnoreCase(oldClientVO.getPostalAddress().getSuburb())) {
				changeReport.append("Postal Suburb=" + membership.get(FIELD_POST_CITY) + ",");
			}
			if (!membership.get(FIELD_POST_POSTCODE).trim().equalsIgnoreCase(oldClientVO.getPostalAddress().getPostcode())) {
				changeReport.append("Postal Postcode=" + membership.get(FIELD_POST_POSTCODE) + ",");
			}
		}

		if (!changeReport.toString().isEmpty()) {
			getLogFileWriter().write((new DateTime()).formatLongDate() + ": Client update required (" + oldClientVO.getClientNumber() + ") " + changeReport.toString() + "\n");
		}

		// update details on existing client
		/*
		 * oldClientVO = setClientDetails(membership, oldClientVO); ClientVO newClientVO = clientMgrLocal.updateClient(oldClientVO, SecurityHelper.getRoadsideUser().getUserID(), Client.HISTORY_UPDATE, "HOB", "CMO Import", SourceSystem.CMO.getSystemName());
		 * 
		 * checkFollowUp(oldClientVO, newClientVO);
		 */

	}

	/**
	 * Update membership with provided data.
	 * 
	 * @param membership
	 * @param memVO
	 * @throws IOException
	 */
	private void updateMembership(Map<String, String> membership, MembershipVO memVO, DateTime expiryDate) throws IOException, GenericException {

		TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP, SecurityHelper.getRoadsideUser());

		String msg = transGroup.addSelectedClient(memVO);
		if (msg != null && !msg.isEmpty()) {
			/*
			 * getErrorFileWriter().write( (new DateTime()).formatLongDate() + ": Can't Update Membership (T" + membership.get(FIELD_TRANS_TYPE) + ") " + memVO.getClientNumber() + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + " : " + msg + '\n');
			 */
			throw new GenericException(msg);
		}
		transGroup.setContextClient(memVO);

		// Create a membership transaction for the selected client.
		transGroup.createTransactionsForSelectedClients();

		MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(memVO.getClientNumber());

		MembershipVO oldMemVO = contextMemTransVO.getMembership();
		MembershipVO newMemVO = contextMemTransVO.getNewMembership();

		if (expiryDate != null) {
			newMemVO.setExpiryDate(expiryDate);
			newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
		}

		newMemVO.setRego(membership.get(FIELD_REGO));
		newMemVO.setVin(membership.get(FIELD_VIN));
		newMemVO.setMake(membership.get(FIELD_MAKE));
		newMemVO.setModel(membership.get(FIELD_MODEL));
		newMemVO.setYear(membership.get(FIELD_YEAR));
		newMemVO.setMembershipProfileCode(membership.get(FIELD_PROGRAM_CODE));

		// submit transaction
		transGroup.submit();

		getLogFileWriter().write((new DateTime()).formatLongDate() + ": Updated Membership (T" + membership.get(FIELD_TRANS_TYPE) + ") " + memVO.getClientNumber() + " for Nomination ID: " + membership.get(FIELD_NOMINATION_ID) + '\n');
	}

	/**
	 * Cancel membership.
	 * 
	 * @param memVO
	 * @throws RemoteException
	 */
	private void cancelMembership(MembershipVO memVO) throws RemoteException {

		if (memVO.getStatus().equals(MembershipVO.STATUS_CANCELLED)) {
			LogUtil.warn(getClass(), "Did not cancel membership for clientNo: " + memVO.getClientNumber() + " because it is already cancelled.");
			return;
		}

		MembershipHelper.removeFromAllGroups(memVO.getMembershipID());

		// Create a cancel TransactionGroup object to hold the common data in
		// the transaction
		TransactionGroup transGroup = TransactionGroupFactory.getTransactionGroupForCancel(SecurityHelper.getRoadsideUser());

		transGroup.setMembershipType(memVO.getMembershipType());

		String msg = transGroup.addSelectedClient(memVO);
		if (msg != null && !msg.isEmpty()) {
			throw new SystemException(msg);
		}
		transGroup.setContextClient(memVO);

		// Create transactions for all selected clients
		transGroup.createTransactionsForSelectedClients();

		// Set the reason for the cancellation.
		ReferenceDataVO cancellationReason = refMgrLocal.getCancelReason("OTH");

		transGroup.setTransactionReason(cancellationReason);
		transGroup.calculateTransactionFee();

		LogUtil.debug(this.getClass(), "txList=" + transGroup.getTransactionList());

		// transGroup.setBatch(true);

		// submit transaction
		transGroup.submit();
	}

	/**
	 * Initialise basic client data.
	 * 
	 * @param membership
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 * @throws GenericException
	 */
	private ClientVO initClientDetails(Map<String, String> membership) throws IOException, ParseException, GenericException {
		ClientVO clientVO = new ClientVO();

		clientVO.setMadeID(SecurityHelper.getRoadsideUser().getUserID());
		clientVO.setMadeDate(new DateTime());
		clientVO.setStatus(Client.STATUS_ACTIVE);
		clientVO.setSourceSystem(SourceSystem.MEMBERSHIP);
		clientVO.setAddressTitle("");
		clientVO.setBirthDateString("");
		clientVO.setContactPersonName("");
		clientVO.setContactPersonPhone("");
		clientVO.setDriversLicenseNumber("");
		clientVO.setGroupClient(false);
		clientVO.setInitials("");
		clientVO.setMaritalStatus(false);
		clientVO.setMarket(false);
		clientVO.setMemoNumber(0);
		clientVO.setMotorNewsDeliveryMethod("");
		clientVO.setMotorNewsSendOption("");
		clientVO.setOrganisationPhone("");
		clientVO.setPhonePassword("");
		clientVO.setPhoneQuestion("");
		clientVO.setPostalName("");
		clientVO.setPostProperty("");
		clientVO.setResiProperty("");
		clientVO.setResiStreetChar("");
		clientVO.setSalutation("");
		clientVO.setSmsAllowed(false);

		clientVO = setClientDetails(membership, clientVO);

		// System.out.println(clientVO);

		return clientVO;
	}

	/**
	 * Set client details based on provided Map.
	 * 
	 * @param membership
	 * @param clientVO
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 * @throws GenericException
	 */
	private ClientVO setClientDetails(Map<String, String> membership, ClientVO clientVO) throws IOException, ParseException, GenericException {

		String systemName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);

		if (membership.get(FIELD_TITLE).equalsIgnoreCase("Mrs") || membership.get(FIELD_TITLE).equalsIgnoreCase("Miss") || membership.get(FIELD_TITLE).equalsIgnoreCase("Ms")) {
			clientVO.setGender("Female");
			clientVO.setSex(false);
		} else if (membership.get(FIELD_TITLE).equalsIgnoreCase("Mr")) {
			clientVO.setGender("Male");
			clientVO.setSex(true);
		} else { // guess male
					// getWarnFileWriter().write("Guessing gender \"Male\" from title: \""
					// + membership.get(FIELD_TITLE) + "\" for Nomination ID: " +
					// membership.get(FIELD_NOMINATION_ID) + '\n');
			clientVO.setGender("Male");
			clientVO.setSex(true);
		}

		clientVO.setBirthDate(new DateTime(df.parse(membership.get(FIELD_DOB))));

		// only set if Production
		if (systemName.equals(CommonConstants.SYSTEM_PRODUCTION)) {
			clientVO.setEmailAddress(membership.get(FIELD_EMAIL));
		} else {
			LogUtil.info(this.getClass(), "Ignoring email address as environment is " + systemName);
		}

		clientVO.setGivenNames(membership.get(FIELD_FIRST_NAME).toUpperCase());
		try {
			clientVO.setHomePhone(membership.get(FIELD_HOME_PHONE));
		} catch (Exception e) {
		}
		try {
			clientVO.setWorkPhone(membership.get(FIELD_BUSINESS_PHONE));
		} catch (Exception e) {
		}
		try {
			// only set if Production
			if (systemName.equals(CommonConstants.SYSTEM_PRODUCTION)) {
				clientVO.setMobilePhone(membership.get(FIELD_MOBILE_PHONE));
			} else {
				LogUtil.info(this.getClass(), "Ignoring mobile phone as environment is " + systemName);
			}
		} catch (Exception e) {
		}

		clientVO.setOrganisationName(membership.get(FIELD_BUSINESS_NAME));
		clientVO.setTitle(membership.get(FIELD_TITLE).toUpperCase());
		clientVO.setSurname(membership.get(FIELD_SURNAME).toUpperCase());

		clientVO.setLastUpdate(new DateTime());

		clientVO = linkAddress(membership, clientVO);

		return clientVO;
	}

	/**
	 * Check if follow-ups are required and create.
	 * 
	 * @param oldClientVO
	 * @param newClientVO
	 * @throws RemoteException
	 * @throws ParseException
	 */
	private void checkFollowUp(ClientVO oldClientVO, ClientVO newClientVO) throws RemoteException, ParseException {
		DateTime due = new DateTime();
		due.add(new Interval("0:0:2:0:0:0")); // 2 days

		// guessed gender from title
		if (!newClientVO.getTitle().equalsIgnoreCase("Mr") && !newClientVO.getTitle().equalsIgnoreCase("Mrs") && !newClientVO.getTitle().equalsIgnoreCase("Ms") && !newClientVO.getTitle().equalsIgnoreCase("Miss")) {
			ClientNote note = new ClientNote();
			note.setClientNumber(newClientVO.getClientNumber());
			note.setNoteType(NoteType.TYPE_FOLLOW_UP);
			note.setCategoryCode("other");
			note.setCreated(new DateTime());
			note.setCreateId(SecurityHelper.getRoadsideUser().getUserID());
			note.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
			note.setNoteText("Guessed gender \"Male\" for CMO import from Title");
			note.setDue(due);
			clientMgrLocal.createClientNote(note);
		}

		// address change
		if (oldClientVO != null) {
			if (!newClientVO.getResiStsubId().equals(oldClientVO.getResiStsubId()) || !newClientVO.getPostStsubId().equals(oldClientVO.getPostStsubId())) {
				FollowUp followUp = new FollowUp(newClientVO, oldClientVO, new DateTime(), SecurityHelper.getRoadsideUser().getUserID());

				followUp.setFollowUpDate(due);
				followUp.setStatus(FollowUp.STATUS_FLAG);
				followUp.setNotes("Address change due to CMO Import");
				clientMgrLocal.createFollowUp(followUp);
			}
		}
	}

	/**
	 * Link StreetSuburbVO to ClientVO.
	 * 
	 * @param membership
	 * @param clientVO
	 * @return
	 * @throws GenericException
	 * @throws IOException
	 */
	private ClientVO linkAddress(Map<String, String> membership, ClientVO clientVO) throws GenericException, IOException {

		membership.put(FIELD_RESI_ADDRESS, membership.get(FIELD_RESI_ADDRESS1) + " " + membership.get(FIELD_RESI_ADDRESS2));
		membership = cleanAddress(membership, FIELD_RESI_ADDRESS, FIELD_RESI_STREET, FIELD_RESI_STREET_NUM);

		if (membership.get(FIELD_RESI_STREET).trim().isEmpty()) {
			throw new GenericException("Blank Residential Street is not permitted: " + membership.get(FIELD_RESI_ADDRESS1));
		}

		List<StreetSuburbVO> ssList = (List<StreetSuburbVO>) commonMgrLocal.getStreetSuburbList(membership.get(FIELD_RESI_STREET), membership.get(FIELD_RESI_CITY), AddressVO.STATE_TAS);
		if (!ssList.isEmpty()) {
			clientVO.setResiStsubId(ssList.get(0).getStreetSuburbID());
		} else {
			throw new GenericException("Residential Street/Suburb not recognised: " + membership.get(FIELD_RESI_STREET) + ", " + membership.get(FIELD_RESI_CITY));
			/*
			 * // try to find suburb List<StreetSuburbVO> suburbList = commonMgrLocal.getSuburbList(membership.get(FIELD_RESI_CITY), AddressVO.STATE_TAS); if (!suburbList.isEmpty()) { // create street/suburb QASAddress q = new QASAddress(); q.setPostcode(membership.get(FIELD_RESI_POSTCODE)); q.setStreet(membership.get(FIELD_RESI_STREET)); q.setSuburb(membership.get(FIELD_RESI_CITY)); q.setState(AddressVO.STATE_TAS); q.setUserid(SecurityHelper.getRoadsideUser().getUserID()); try { int ssid = StreetSuburbFactory.getStreetSuburbAdapter(). createProgressStreetSuburb(q); clientVO.setResiStsubId(ssid); getWarnFileWriter().write("Created new StreetSuburb: " + ssid + ": " + membership.get(FIELD_RESI_STREET) + ", " + membership.get(FIELD_RESI_CITY) + '\n'); } catch (Exception e) { throw new GenericException("Cannot create Residential Street/Suburb: " + membership.get(FIELD_RESI_STREET) + ", " + membership.get(FIELD_RESI_CITY) + ": " + e); } } else { throw new GenericException("Unrecognised Residential Suburb: " +
			 * membership.get(FIELD_RESI_CITY)); }
			 */
		}

		if (membership.get(FIELD_RESI_STREET_NUM) == null || membership.get(FIELD_RESI_STREET_NUM).trim().isEmpty()) {
			throw new GenericException("Blank Residential Street Number is not permitted: " + membership.get(FIELD_RESI_ADDRESS1));
		}
		clientVO.setResiStreetChar(membership.get(FIELD_RESI_STREET_NUM));

		if (membership.get(FIELD_POST_ADDRESS1) != null && !membership.get(FIELD_POST_ADDRESS1).isEmpty()) {
			membership.put(FIELD_POST_ADDRESS, membership.get(FIELD_POST_ADDRESS1) + " " + membership.get(FIELD_POST_ADDRESS2));
			membership = cleanAddress(membership, FIELD_POST_ADDRESS, FIELD_POST_STREET, FIELD_POST_STREET_NUM);

			ssList = (List<StreetSuburbVO>) commonMgrLocal.getStreetSuburbList(membership.get(FIELD_POST_STREET), membership.get(FIELD_POST_CITY), AddressVO.STATE_TAS);
			if (!ssList.isEmpty()) {
				clientVO.setPostStsubId(ssList.get(0).getStreetSuburbID());
			} else {
				throw new GenericException("Postal Street/Suburb not recognised: " + membership.get(FIELD_POST_STREET) + ", " + membership.get(FIELD_POST_CITY));
				/*
				 * List<StreetSuburbVO> suburbList = commonMgrLocal.getSuburbList(membership.get(FIELD_POST_CITY), AddressVO.STATE_TAS); if (!suburbList.isEmpty()) { // create street/suburb QASAddress q = new QASAddress(); q.setPostcode(membership.get(FIELD_POST_POSTCODE)); q.setStreet(membership.get(FIELD_POST_STREET)); q.setSuburb(membership.get(FIELD_POST_CITY)); q.setState(AddressVO.STATE_TAS); q.setUserid(SecurityHelper.getRoadsideUser().getUserID()); try { int ssid = StreetSuburbFactory.getStreetSuburbAdapter(). createProgressStreetSuburb(q); clientVO.setPostStsubId(ssid); getWarnFileWriter().write("Created new StreetSuburb: " + ssid + ": " + membership.get(FIELD_POST_STREET) + ", " + membership.get(FIELD_POST_CITY) + '\n'); } catch (Exception e) { throw new GenericException("Cannot create Postal Street/Suburb: " + membership.get(FIELD_POST_STREET) + ", " + membership.get(FIELD_POST_CITY) + ": " + e); } } else { throw new GenericException("Unrecognised Postal Suburb: " +
				 * membership.get(FIELD_POST_CITY)); }
				 */
			}

			clientVO.setPostStreetChar(membership.get(FIELD_POST_STREET_NUM));

			if (membership.get(FIELD_POST_STREET).isEmpty()) {
				clientVO.setPostProperty(membership.get(FIELD_POST_ADDRESS1)); // PO
				// Box..?
			}

		} else { // copy Resi to Post
			clientVO.setPostStsubId(clientVO.getResiStsubId());
			clientVO.setPostStreetChar(clientVO.getResiStreetChar());
		}

		return clientVO;
	}

	/**
	 * Extract street name from street address field.
	 * 
	 * @param membership
	 * @param inputKey target street address input field
	 * @param outputKey target street name output field
	 * @param numberOutputKey target street number output field
	 * @return
	 */
	private Map<String, String> cleanAddress(Map<String, String> membership, String inputKey, String outputKey, String numberOutputKey) {
		String addressRaw = membership.get(inputKey);
		addressRaw = addressRaw.replaceAll("\\s+", " "); // reduce multiple
		// spaces to single
		membership.put(inputKey, addressRaw);

		String streetName;
		StringBuffer streetNameBuilder = new StringBuffer("");
		List<String> keepers = new ArrayList<String>();

		// System.out.println("*** inputKey = " + membership.get(inputKey));

		String parts[] = addressRaw.split(" ");

		// grab all the words from the street address before a digit found
		for (int i = parts.length - 1; i >= 0; i--) {
			if (Pattern.compile("\\d").matcher(parts[i]).find()) { // quit once
																	// we
																	// encounter
																	// a digit
				break;
			} else {
				keepers.add(parts[i]);
			}
		}

		// reverse words
		Collections.reverse(keepers);
		for (String s : keepers) {
			streetNameBuilder.append(s).append(" ");
		}

		streetName = streetNameBuilder.toString().trim();

		// extract street number
		if (!membership.get(inputKey).equals(streetName)) {
			String streetNumberRaw = membership.get(inputKey).split(streetName)[0];
			membership.put(numberOutputKey, streetNumberRaw.trim().toUpperCase());
		}

		// now attempt to abbreviate street type
		parts = streetName.split(" ");
		if (streetTypeAbbr.containsKey(parts[parts.length - 1].toUpperCase())) {
			parts[parts.length - 1] = streetTypeAbbr.get(parts[parts.length - 1].toUpperCase());
		}
		StringBuffer abbrev = new StringBuffer();
		for (String s : parts) {
			abbrev.append(s).append(" ");
		}

		membership.put(outputKey, abbrev.toString().trim());

		// System.out.println("*** outputKey = " + membership.get(outputKey));
		// System.out.println("*** numberOutputKey = " +
		// membership.get(numberOutputKey));

		return membership;
	}

	/**
	 * Create the header map.
	 */
	private Map<String, String> createHeader(String[] line) {

		for (int i = 0; i < line.length; i++) {
			if (i == 0)
				headerLine.put(FIELD_IDENTIFIER, "");
			else
				headerLine.put(line[i], "");
		}
		return headerLine;
	}

	/**
	 * Parses a line from the CMO file. If there were no errors, it returns a Membership details map.
	 */
	private void parseInputLine(String[] line, Map<String, String> headerLineCopy) {

		final Set<Map.Entry<String, String>> entries = headerLine.entrySet();

		int it = 0;
		for (Map.Entry<String, String> entry : entries) {
			String key = entry.getKey();
			headerLineCopy.put(key, line[it]);
			LogUtil.debug("parseInputLine ", key + " is " + line[it]);
			it++;
		}
	}

	/**
	 * Generate XML for RAA.
	 * 
	 * @throws IOException
	 */
	public void generateExportXML() throws IOException {

		String outputDirectoryName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_EXTRACT_DIRS, SystemParameterVO.CAD_CMO);
		if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') {
			outputDirectoryName += "/";
		}

		String fileName = outputDirectoryName + SystemParameterVO.CAD_CMO + "_FULL_" + DateUtil.formatDate(new DateTime(), "MMddHHmm") + ".xml";

		FileWriter fileWriter = new FileWriter(fileName, false); // overwrite

		fileWriter.write("<?xml version=\"1.0\" encoding=\"windows-1252\" ?>\n");
		fileWriter.write("<TABLE xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:od=\"urn:schemas-microsoft-com:officedata\" xsi:noNamespaceSchemaLocation=\"RACT_Import_Schema.xsd\">\n");

		for (MembershipVO memVO : membershipMgrLocal.getMembershipsByProduct(ProductVO.PRODUCT_CMO)) {

			FleetClientData clientData = new FleetClientData(memVO.getClientNumber());
			CadMembershipData membershipData = new CadMembershipData(memVO);

			fileWriter.write("<RACT.IMPORT>");
			fileWriter.write(clientData.getClientXML());
			fileWriter.write(membershipData.getXML());
			fileWriter.write("</RACT.IMPORT>\n");
		}

		fileWriter.write("</TABLE>\n");
		fileWriter.flush();
		fileWriter.close();

		// zip it up
		zipFile(fileName);
	}

	/**
	 * Moves a file designated by fileName to a gz archive and deletes original.
	 * 
	 * @param fileName
	 * @throws IOException
	 */
	private void zipFile(String fileName) throws IOException {
		FileInputStream fin = new FileInputStream(fileName);
		FileOutputStream fos = new FileOutputStream(fileName + ".gz");
		GZIPOutputStream zout = new GZIPOutputStream(fos);
		File fileHandle = new File(fileName);

		// zout.putNextEntry(new ZipEntry(fileHandle.getName()));

		int length;
		byte[] buffer = new byte[1024];

		while ((length = fin.read(buffer)) > 0) {
			zout.write(buffer, 0, length);
		}

		// zout.closeEntry();

		fin.close();
		zout.close();

		// delete xml file
		fileHandle.delete();
	}

	/**
	 * Initialise a fileWriter to write to file.
	 * 
	 * @param suffix
	 * @return
	 * @throws IOException
	 */
	private FileWriter initLogFileWriter(String suffix) throws IOException {

		String fileName = getLogFilePath() + getLogFileName(suffix);

		FileWriter myfileWriter = new FileWriter(fileName, true);

		return myfileWriter;
	}

	private String getLogFilePath() throws RemoteException {
		String outputDirectoryName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_LOG_DIR);
		if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') {
			outputDirectoryName += "/";
		}

		return outputDirectoryName;
	}

	private String getLogFileName(String suffix) throws RemoteException {

		String ext = "log";

		if (suffix.equals(FILE_SUFFIX_DUPLICATES)) {
			ext = "csv";
		}

		String fileName = DateUtil.formatYYYYMMDD(new DateTime()) + "-cmo" + suffix + "." + ext;

		return fileName;
	}

	private FileWriter getLogFileWriter() throws IOException {
		if (fileWriter == null) { // lazy init
			fileWriter = initLogFileWriter("");
		}
		return fileWriter;
	}

	private FileWriter getWarnFileWriter() throws IOException {
		if (warnFileWriter == null) { // lazy init
			warnFileWriter = initLogFileWriter(FILE_SUFFIX_WARN);
		}
		return warnFileWriter;
	}

	private FileWriter getErrorFileWriter() throws IOException {
		if (errorFileWriter == null) { // lazy init
			errorFileWriter = initLogFileWriter(FILE_SUFFIX_ERROR);
		}
		return errorFileWriter;
	}

	private FileWriter getDuplicatesFileWriter() throws IOException {
		if (duplicatesFileWriter == null) { // lazy init
			duplicatesFileWriter = initLogFileWriter(FILE_SUFFIX_DUPLICATES);
		}
		return duplicatesFileWriter;
	}

	/**
	 * Email results if any warnings or errors generated.
	 * 
	 * @throws IOException
	 */
	public void emailResults() throws IOException {
		String headerText = "CMO Import Warnings/Errors";
		String bodyText = "CMO Import has completed at " + (new DateTime()) + '\n';
		String emailTo = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.EMAIL_MEMBERSHIP_RENEWAL_RUN);

		String infoFilePath = getLogFilePath() + getLogFileName("");
		File infoFile = new File(infoFilePath);

		String warnFilePath = getLogFilePath() + getLogFileName(FILE_SUFFIX_WARN);
		File warnFile = new File(warnFilePath);

		String errorFilePath = getLogFilePath() + getLogFileName(FILE_SUFFIX_ERROR);
		File errorFile = new File(errorFilePath);

		String duplicatesFilePath = getLogFilePath() + getLogFileName(FILE_SUFFIX_DUPLICATES);
		File duplicatesFile = new File(duplicatesFilePath);

		Hashtable<String, String> files = new Hashtable<String, String>();
		if (infoFile.exists()) {
			files.put(getLogFileName(""), infoFilePath);
		}
		if (warnFile.exists()) {
			files.put(getLogFileName(FILE_SUFFIX_WARN), warnFilePath);
		}
		if (errorFile.exists()) {
			files.put(getLogFileName(FILE_SUFFIX_ERROR), errorFilePath);
		}
		if (duplicatesFile.exists()) {
			files.put(getLogFileName(FILE_SUFFIX_DUPLICATES), duplicatesFilePath);
		}

		if (!files.isEmpty()) {
			try {
				if (emailTo == null || emailTo.equals("")) { // no email address
					LogUtil.fatal(this.getClass(), "No email address________");
				}
				MailMessage message = new MailMessage();
				message.setRecipient(emailTo);
				message.setSubject(headerText);
				message.setMessage(bodyText);
				message.setFiles(files);
				MailMgr mailMgr = CommonEJBHelper.getMailMgr();
				mailMgr.sendMail(message);

			} catch (Exception e) {
				LogUtil.fatal(this.getClass(), "Unable to email results: " + e);
				e.printStackTrace();
			}
		}
	}

	private DateTime calcExpiryDate(Map<String, String> membership) throws ParseException {
		DateTime cmoExpiryDate = null;
		String[] dateOrder = { FIELD_EXPIRATION_DATE, FIELD_START_DATE, FIELD_SERVICE_DATE, FIELD_NOMINATION_DATE };
		Interval[] advanceYears = { zeroYear, oneYear, oneYear, oneYear };

		for (int i = 0; i < dateOrder.length; i++) {
			try {
				LogUtil.debug(this.getClass(), dateOrder[i] + " " + membership.get(dateOrder[i]) + " " + i + " " + advanceYears[i].toString());
				cmoExpiryDate = new DateTime(df.parse(membership.get(dateOrder[i]))).add(advanceYears[i]);
				break;
			} catch (ParseException e) {
				// go on to next one
			} catch (NullPointerException n) {

			}
		}
		;

		return cmoExpiryDate;
	}

	/**
	 * Write a log file of suspected duplicates based on today's updates.
	 * 
	 * @throws IOException
	 */
	private void processDuplicates() throws IOException {

		List<Map<String, String>> duplicates = clientMgrLocal.getCmoDuplicateSuggestions();

		if (duplicates != null && !duplicates.isEmpty()) {
			getDuplicatesFileWriter().write("clientNo,masterClientNo,givenNames,masterGivenNames,surname,resiStreetChar,masterResiStreetChar,street,suburb,birthDate,productCode,profileCode\n");

			for (Map<String, String> record : duplicates) {
				getDuplicatesFileWriter().write(record.get("clientNo") + "," + record.get("masterClientNo") + "," + record.get("givenNames") + "," + record.get("masterGivenNames") + "," + record.get("surname") + "," + record.get("resiStreetChar") + "," + record.get("masterResiStreetChar") + "," + record.get("street") + "," + record.get("suburb") + "," + record.get("birthDate") + "," + record.get("productCode") + "," + record.get("profileCode") + "\n");
			}
		}

	}

	static {
		streetTypeAbbr = new HashMap<String, String>();
		streetTypeAbbr.put("ALLEY", "ALLY");
		streetTypeAbbr.put("APPROACH", "APP");
		streetTypeAbbr.put("ARCADE", "ARC");
		streetTypeAbbr.put("AVENUE", "AVE");
		streetTypeAbbr.put("BOULEVARD", "BVD");
		streetTypeAbbr.put("BROW", "BROW");
		streetTypeAbbr.put("BYPASS", "BYPA");
		streetTypeAbbr.put("CAUSEWAY", "CWAY");
		streetTypeAbbr.put("CIRCLE", "CIR");
		streetTypeAbbr.put("CIRCUIT", "CCT");
		streetTypeAbbr.put("CIRCUS", "CIRC");
		streetTypeAbbr.put("CLOSE", "CL");
		streetTypeAbbr.put("COPSE", "CPSE");
		streetTypeAbbr.put("CORNER", "CNR");
		streetTypeAbbr.put("COVE", "COVE");
		streetTypeAbbr.put("COURT", "CT");
		streetTypeAbbr.put("CRESCENT", "CRES");
		streetTypeAbbr.put("DRIVE", "DR");
		streetTypeAbbr.put("END", "END");
		streetTypeAbbr.put("ESPLANANDE", "ESP");
		streetTypeAbbr.put("FLAT", "FLAT");
		streetTypeAbbr.put("FREEWAY", "FWAY");
		streetTypeAbbr.put("FRONTAGE", "FRNT");
		streetTypeAbbr.put("GARDENS", "GDNS");
		streetTypeAbbr.put("GLADE", "GLD");
		streetTypeAbbr.put("GLEN", "GLEN");
		streetTypeAbbr.put("GREEN", "GRN");
		streetTypeAbbr.put("GROVE", "GR");
		streetTypeAbbr.put("HEIGHTS", "HTS");
		streetTypeAbbr.put("HIGHWAY", "HWY");
		streetTypeAbbr.put("LANE", "LANE");
		streetTypeAbbr.put("LINK", "LINK");
		streetTypeAbbr.put("LOOP", "LOOP");
		streetTypeAbbr.put("MALL", "MALL");
		streetTypeAbbr.put("MEWS", "MEWS");
		streetTypeAbbr.put("PACKET", "PCKT");
		streetTypeAbbr.put("PARADE", "PDE");
		streetTypeAbbr.put("PARK", "PARK");
		streetTypeAbbr.put("PARKWAY", "PKWY");
		streetTypeAbbr.put("PLACE", "PL");
		streetTypeAbbr.put("PROMENADE", "PROM");
		streetTypeAbbr.put("RESERVE", "RES");
		streetTypeAbbr.put("RETREAT", "RTT");
		streetTypeAbbr.put("RIDGE", "RDGE");
		streetTypeAbbr.put("RISE", "RISE");
		streetTypeAbbr.put("ROAD", "RD");
		streetTypeAbbr.put("ROW", "ROW");
		streetTypeAbbr.put("SQUARE", "SQ");
		streetTypeAbbr.put("STREET", "ST");
		streetTypeAbbr.put("STRIP", "STRP");
		streetTypeAbbr.put("TARN", "TARN");
		streetTypeAbbr.put("TERRACE", "TCE");
		streetTypeAbbr.put("THOROUGHFARE", "TFRE");
		streetTypeAbbr.put("TRACK", "TRAC");
		streetTypeAbbr.put("TRUNKWAY", "TWAY");
		streetTypeAbbr.put("VIEW", "VIEW");
		streetTypeAbbr.put("VISTA", "VSTA");
		streetTypeAbbr.put("WALK", "WALK");
		streetTypeAbbr.put("WAY", "WAY");
		streetTypeAbbr.put("WALKWAY", "WWAY");
		streetTypeAbbr.put("YARD", "YARD");
	}
}
