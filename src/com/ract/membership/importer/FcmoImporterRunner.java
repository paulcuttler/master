package com.ract.membership.importer;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Properties;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;
import com.ract.util.LogUtil;

/**
 * The Class FcmoImporter.
 */
public class FcmoImporterRunner
{
	ImportMgr importMgr;
	CommonMgr commonMgrLocal;
     /**
     * Import cmo members.
     *    
     * @param inputFile
     *            the input file
     * @throws RemoteException
     *             the remote exception
     */
    public static void main(String[] args)
    {
	    String providerURL = args[0];
	    String initialContext = "org.jnp.interfaces.NamingContextFactory";
	    Properties environment = new Properties();
	    environment.setProperty(javax.naming.Context.PROVIDER_URL,providerURL);
	    environment.setProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY,initialContext);

	    CommonMgr commonMgrLocal = CommonEJBHelper.getCommonMgr();
	    ImportMgr importMgr = ImportEJBHelper.getImportMgr();

	String outputDirectoryName = null;
	try
	{
	    outputDirectoryName = commonMgrLocal.getSystemParameterValue(
		    SystemParameterVO.CATEGORY_MEMBERSHIP,
		    SystemParameterVO.CMO_BASE_DIR);
	}
	catch (RemoteException e)
	{
	    LogUtil.fatal("RLG" , "Unable to determine CMO Base Dir: " + e.getMessage());
	    return;
	}
	if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/')
	{
	    outputDirectoryName += "/";
	}

	String inFolder = outputDirectoryName + "incoming" + "/";
	String inputFile = null;
	String outFolder = outputDirectoryName + "processed" + "/";

	System.out.println("Incoming path : " + inFolder);

	File folder = new File(inFolder);

	// read file
	for (File file : folder.listFiles())
	{
	    inputFile = file.getAbsolutePath();

	    try
	    {
		// process file
		importCmoMembers(importMgr,inputFile);

		// move file
		file.renameTo(new File(outFolder + file.getName()));
	    }
	    catch (Exception e)
	    {
		LogUtil.fatal("RLG", "Could not process CMO file: "
			+ inputFile + " : " + e);
	    }
	}

	try
	{
	    importMgr.emailResults();
	}
	catch (IOException e)
	{
	    LogUtil.fatal("RLG", "Could not email results of CMO Import: "
		    + e);
	}
	
    } 
    private static void importCmoMembers(ImportMgr importMgr, String inputFile) throws RemoteException
    {
	System.out.println(" ======================================================== ");
	System.out.println(" CMO Member file      " + inputFile);
	System.out.println(" Starting             " + new Date().toString());
	System.out.println(" ======================================================== ");


	importMgr.processCmoFile(inputFile);

	System.out.println(" CMO Import Finished             "	+ new Date().toString());
	
    }

  
}
