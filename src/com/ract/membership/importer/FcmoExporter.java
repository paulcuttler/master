package com.ract.membership.importer;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class FcmoExporter implements Job {

    public FcmoExporter() {
	System.out.println("*** instantiating FcmoExporter");
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
	exportCmoMembers();
    }

    /**
     * 
     * @param host
     *            String
     * @param port
     *            String
     * @param inputFile
     *            String
     */
    private void exportCmoMembers() {

	ImportMgr importMgr = null;
	try {
	    importMgr = ImportEJBHelper.getImportMgr();
	} catch (Exception ne) {
	    ne.printStackTrace();
	    System.out.println("Failed initialising access to Import Manager interface: " + ne.getMessage());
	}

	try {
	    System.out.println(" ======================================================== ");
	    System.out.println(" CMO Export");
	    System.out.println(" Starting             " + new Date().toString());
	    System.out.println(" ======================================================== ");

	    importMgr.generateExportXML();

	    System.out.println(" CMO Export Finished             " + new Date().toString());

	} catch (Exception e) {
	    System.out.println("FcmoExporter +++++++ Exception " + e.getMessage());
	    System.out.println("FcmoExporter +++++++ FINISHING");
	}

    }

}
