package com.ract.membership.importer;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.namespace.QName;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.insight4.ws.AddCardRequestResult;
import com.insight4.ws.CardManagementService;
import com.insight4.ws.ICardManagementService;
import com.ract.util.ConnectionUtil;
import com.ract.util.LogUtil;

/**
 * The Class FcmoImporterCardRequestRetry.
 */
public class FcmoImporterCardRequestRetry implements Job {

    /**
     * Instantiates a new fcmo importer card request retry.
     */
    public FcmoImporterCardRequestRetry() {
	LogUtil.info(getClass(), "*** Retrying all failed new card requests from CMO import");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {

	// ********** Start TEMPORARY for testing only ***********
	// Force the service url to tst - dev is not currently operational
	URL baseUrl;
	baseUrl = com.insight4.ws.CardManagementService.class.getResource(".");
	URL actualUrl = null;
	try {
	    actualUrl = new URL(baseUrl, "http://cardmanagementservicetst.ract.com.au/CardManagementService.svc?wsdl");
	} catch (MalformedURLException e1) {
	    LogUtil.warn(getClass(), "Could not create web service url");
	    e1.printStackTrace();
	}

	// Create the service pointing to tst service
	CardManagementService service = new CardManagementService(actualUrl, new QName("http://tempuri.org/", "CardManagementService"));

	// TODO Reinstate for normal testing and production
	// CardManagementService service = new CardManagementService(actualUrl, new QName("http://tempuri.org/", "CardManagementService"));
	// ********** End TEMPORARY for testing only ***********

	ICardManagementService portType = service.getBasicHttpBindingICardManagementService();

	PreparedStatement statement = null;
	Connection connection = null;
	try {
	    Connection conn = null;
	    ResultSet rs = null;
	    String url = "jdbc:jtds:sqlserver://torana.ract.com.au:1433/ract_dev";
	    String driver = "net.sourceforge.jtds.jdbc.Driver";
	    String userName = "sa";
	    String password = "system";
	    try {
		Class.forName(driver).newInstance();
		conn = DriverManager.getConnection(url, userName, password);

		// Get all the current card requests that have not been successfully processed
		statement = conn.prepareStatement("select * from dbo.cmo_import_card_request_retry where processed = 0");
		rs = statement.executeQuery();
		while (rs.next()) {

		    // Call the service
		    AddCardRequestResult result = portType.addCardRequest(rs.getString("client_number"), (Boolean) rs.getBoolean("is_prime_addressee"), (Integer) rs.getInt("tier_code"), (Boolean) rs.getBoolean("staff"), rs.getString("rego"), rs.getString("product_identifier"), new Boolean(true), rs.getString("reason_code"));

		    Integer errorCode = result.getErrorCode();
		    String errorMsg = result.getErrorMessage().getValue();
		    if (errorCode == -1) {
			// All OK - set the record as processed and record the card number that was returned
			statement = conn.prepareStatement("update dbo.cmo_import_card_request_retry set processed = 1,  assigned_card_no = '" + result.getCardNumber().getValue() + "'");
			rs = statement.executeQuery();
			LogUtil.info(getClass(), "Successfully created card for client number " + rs.getString("client_number"));
		    } else {
			// Client still not in MRM or other error - update the retry count
			LogUtil.warn(getClass(), "Unable to create card for non-existent client # " + rs.getString("client_number") + " in MRM" + ": " + errorMsg);

			statement = conn.prepareStatement("update dbo.cmo_import_card_request_retry set retry_count = retry_count + 1");
			rs = statement.executeQuery();
		    }
		}

	    } catch (Exception e) {
		LogUtil.fatal(getClass(), "Unable to create card for client number " + rs.getString("client_number") + " due to system error: Exception e=" + e);
	    } catch (Error r) {
		LogUtil.fatal(getClass(), "Unable to create card for client number " + rs.getString("client_number") + " due to system error: Error r=" + r);
	    } catch (Throwable t) {
		LogUtil.fatal(getClass(), "Unable to create card for client number " + rs.getString("client_number") + " due to system error: Throwable t=" + t);
	    } finally {
		conn.close();
		rs.close();
	    }

	} catch (SQLException e) {
	    LogUtil.fatal(getClass(), "Unable to create card due to system error: SQLException e=" + e);
	} finally {
	    ConnectionUtil.closeConnection(connection, statement);
	}
    }
}
