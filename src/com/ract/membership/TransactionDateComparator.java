package com.ract.membership;

import java.sql.Date;
import java.util.Comparator;
import java.util.Map;

public class TransactionDateComparator implements Comparator<Map<String, Object>> {
    @Override
    public int compare(Map<String, Object> m1, Map<String, Object> m2) {
	return ((Date) m1.get("txnDate")).compareTo((Date) m2.get("txnDate"));
    }
}