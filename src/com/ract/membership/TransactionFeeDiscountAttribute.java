package com.ract.membership;

import java.io.Serializable;

/**
 * Attributes that are associated with a discount. eg. GM Client Number.
 * 
 * @hibernate.class table="mem_transaction_fee_discount_attribute" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class TransactionFeeDiscountAttribute implements Serializable {

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public TransactionFeeDiscountAttributePK getTransactionFeeDiscountAttributePK() {
	return this.transactionFeeDiscountAttributePK;
    }

    public void setAttributeValue(String attributeValue) {
	this.attributeValue = attributeValue;
    }

    public void setTransactionFeeDiscountAttributePK(TransactionFeeDiscountAttributePK transactionFeeDiscountAttributePK) {
	this.transactionFeeDiscountAttributePK = transactionFeeDiscountAttributePK;
    }

    /**
     * @hibernate.property column="attribute_value"
     * @return String
     */
    public String getAttributeValue() {
	return attributeValue;
    }

    public TransactionFeeDiscountAttribute() {
    }

    private TransactionFeeDiscountAttributePK transactionFeeDiscountAttributePK;

    private String attributeValue;

    public String toString() {
	return this.getTransactionFeeDiscountAttributePK().toString() + " " + this.attributeValue;
    }

}
