package com.ract.membership;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface MembershipIDMgrHome extends EJBHome {
    public MembershipIDMgr create() throws RemoteException, CreateException;
}