package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.Remote;

import com.ract.common.ReferenceDataVO;
import com.ract.util.DateTime;

/**
 * The membership reference manager manages the caching of reference data for
 * fast retrieval and the refreshing of the cached data when it changes. All
 * changes to reference data must be don through the Reference managers methods.
 * 
 * @author T. Bakker.
 * @created 1 August 2002
 */
@Remote
public interface MembershipRefMgr {

    public AdjustmentTypeVO getAdjustmentTypeVO(String adjTypeCode) throws RemoteException;

    public Collection getAdjustmentTypeList() throws RemoteException;

    public Collection findProductFee(String productCode, String memTypeCode, String transTypeCode, int numberInGroup, DateTime effectiveDate) throws RemoteException;

    public Collection findProductBenefitFee(String productBenefitCode, String memTypeCode, String transTypeCode, int numberInGroup, DateTime effectiveDate) throws RemoteException;

    public Collection getAllowableDiscountList(String feeTypeCode, DateTime effectivedate) throws RemoteException;

    public Collection getClientTypeListByMembershipType(String membershipTypeCode) throws RemoteException;

    public DiscountTypeVO getDiscountType(String discountTypeCode) throws RemoteException;

    public Collection getDiscountTypeList() throws RemoteException;

    public Collection getActiveDiscountTypeList() throws RemoteException;

    public Collection getDiscountTypeListByFee(Integer feeSpecificationID) throws RemoteException;

    public FeeTypeVO getFeeType(String feeTypeCode) throws RemoteException;

    public Collection getFeeTypeList() throws RemoteException;

    public Collection getDiscountFeeTypeList(String discountCode) throws RemoteException;

    public FeeSpecificationVO getFeeSpecification(Integer feeSpecificationId) throws RemoteException;

    public Collection getFeeSpecificationList() throws RemoteException;

    public Collection getFeeTypeDiscountList(String feeTypeCode) throws RemoteException;

    public Collection getJoinReasonList() throws RemoteException;

    public ReferenceDataVO getJoinReason(String reasonCode) throws RemoteException;

    public MembershipTransactionTypeVO getMembershipTransactionType(String transactionTypeCode) throws RemoteException;

    public MembershipAccountVO getMembershipAccount(Integer accountID) throws RemoteException;

    public void resetMembershipProfileCache() throws RemoteException;

    /**
     * Return the membership account tagged as the clearing account. Returns the
     * first account in the list that has been tagged as the clearing account.
     * There should only be one. Return null if no clearing account has been
     * tagged.
     * 
     * 4/11/02: It has been now decided that all journals initially go to the
     * cash clearing account (1340) regardless of payment method. The Direct
     * Debit System moves it from the cash clearing account (1340) to the DD
     * clearing account (1147).
     * 
     * @return The membership account tagged as the clearing account.
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getMembershipClearingAccount() throws RemoteException;

    /**
     * Return the membership account tagged as the suspense account. Returns the
     * first account in the list that has been tagged as the suspense account.
     * There should only be one. Return null if no suspense account has been
     * tagged.
     * 
     * @return The membership account tagged as the suspense account.
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getMembershipSuspenseAccount() throws RemoteException;

    /**
     * Return the membership account tagged as the GST account. Returns the
     * first account in the list that has been tagged as the GST account. There
     * should only be one. Return null if no GST account has been tagged.
     * 
     * @return The membership account tagged as the GST account.
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getMembershipGSTAccount() throws RemoteException;

    public MembershipProfileVO getMembershipProfile(String profileCode) throws RemoteException;

    public MembershipProfileVO getDefaultMembershipProfile() throws RemoteException;

    public Collection getMembershipProfileList() throws RemoteException;

    public Collection getMembershipTransactionTypeList() throws RemoteException, Exception;

    public MembershipTypeVO getMembershipType(String membershipTypeCode) throws RemoteException;

    public Collection getMembershipTypeList() throws RemoteException, Exception;

    public Collection getMembershipTypeListByClientType(String clientTypeCode) throws RemoteException;

    public ProductVO getProduct(String productCode) throws RemoteException;

    public Collection getProductList() throws RemoteException;

    public Collection getProductListByMembershipType(String membershipTypeCode) throws RemoteException;

    public Collection getMembershipTypeByProductList(String productCode) throws RemoteException;

    public Collection getProductListByStatus(String productStatus) throws RemoteException;

    public Collection getProductBenefitList(String productCode) throws RemoteException;

    public ProductBenefitTypeVO getProductBenefitType(String productBenefitCode) throws RemoteException;

    public Collection getProductBenefitTypeList() throws RemoteException, Exception;

    public Collection getRejoinReasonList() throws RemoteException;

    public ReferenceDataVO getRejoinReason(String reasonCode) throws RemoteException;

    public ReferenceDataVO getRejoinType(String rejoinTypeCode) throws RemoteException;

    public Collection getRejoinTypeList() throws RemoteException;

    public ReferenceDataVO getCancelReason(String reasonCode) throws RemoteException;

    public Collection getCancelReasonList() throws RemoteException;

    public void resetDiscountTypeCache() throws RemoteException;

    public void resetFeeTypeCache() throws RemoteException;

    public void resetFeeSpecificationCache() throws RemoteException;

    public void resetJoinReasonCache() throws RemoteException;

    public void resetMembershipAccountCache() throws RemoteException;

    public void resetMembershipTransactionTypeCache() throws RemoteException;

    public void resetMembershipTypeCache() throws RemoteException;

    public void resetProductCache() throws RemoteException;

    public void resetProductBenefitTypeCache() throws RemoteException;

    public void setDiscountType(DiscountTypeVO discountType) throws RemoteException;

    public void setMembershipTransactionType(MembershipTransactionTypeVO transactionTypeCode) throws RemoteException;

    public void setMembershipType(MembershipTypeVO membershipTypeCode) throws RemoteException;

    public void setProduct(ProductVO productCode) throws RemoteException;

    public void setProductBenefitType(ProductBenefitTypeVO productBenefitCode) throws RemoteException;

    public void initialiseFeeSpecificationList() throws RemoteException;

    public void initialiseFeeTypeList() throws RemoteException;

    public void initialiseJoinReasonList() throws RemoteException;

    public void initialiseMembershipProfileList() throws RemoteException;

    public void initialiseMembershipTransactionTypeList() throws RemoteException;

    public void initialiseMembershipTypeList() throws RemoteException;

    public void initialiseProductBenefitTypeList() throws RemoteException;

    public void initialiseProductList() throws RemoteException;

    public MembershipAccountVO getMembershipAccountByTitle(String title) throws RemoteException;

    public MembershipAccountVO getMembershipAccount(String accountNumber) throws RemoteException;

    public Collection getDiscountTypeListByFee(String feeTypeCode) throws RemoteException;

    /**
     * Retrieve the default fee for a given product
     * 
     * @param prodCode
     * @param membershipType
     * @param transactionType
     * @param groupCount
     * @param groupIndex
     * @param feeEffectiveDate
     * @return
     * @throws Exception
     */
    public double getFeeForProduct(String prodCode, String membershipType, String transactionType, int groupCount, int groupIndex, DateTime feeEffectiveDate) throws RemoteException;
}
