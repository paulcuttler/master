package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.client.ClientVO;
import com.ract.util.DateTime;
import com.ract.util.Interval;

/**
 * Defines the inferface for retrieving data from a membership history object.
 */
public interface MembershipHistory {

    public Integer getMembershipID();

    public String getMembershipNumber();

    public String getMembershipTypeCode();

    public MembershipTypeVO getMembershipType() throws RemoteException;

    public String getMembershipProfileCode();

    public MembershipProfileVO getMembershipProfile() throws RemoteException;

    public String getBaseStatus();

    public boolean isActive() throws RemoteException;

    public Integer getClientNumber();

    public ClientVO getClient() throws RemoteException;

    public String getProductCode();

    public ProductVO getProduct() throws RemoteException;

    public DateTime getProductEffectiveDate();

    public String getNextProductCode();

    public DateTime getJoinDate();

    public DateTime getCommenceDate();

    public DateTime getExpiryDate();

    public Interval getMembershipTerm();

    public Interval getActualMembershipTerm();

    public Interval getMembershipYears();

    public Interval getMembershipYears(DateTime effectiveDate);

    public double getCreditAmount();

    public Interval getCreditPeriod();

    public String getAffiliatedClubCode();

    public String getAffiliatedClubId();

    public String getAffiliatedClubProductLevel();

    public DateTime getAffiliatedClubExpiryDate();

    public int getVehicleCount();

    public String getVehicleExpiryType();

    public ArrayList getVehicleList() throws RemoteException;

    public Collection getMembershipCardList(boolean load) throws RemoteException;

    public Collection getDocumentList(boolean load) throws RemoteException;

    public ArrayList getProductOptionList() throws RemoteException;

    public ArrayList getProductOptionList(boolean forceFetch) throws RemoteException;

    public ArrayList getDiscountList() throws RemoteException;

    public MembershipGroupVO getMembershipGroup() throws RemoteException;

    public Collection getTransactionList() throws RemoteException;

    public boolean hasProductOption(String prodOptionCode) throws RemoteException;

    public boolean hasRecurringDiscount(String discTypeCode) throws RemoteException;

    public boolean membershipHasProductOption(String benefitTypeCode);

    public boolean isPrimeAddressee() throws RemoteException;

    public boolean isGroupMember() throws RemoteException;

    public int getBranchNo() throws RemoteException;

    public BigDecimal getMembershipValue(boolean includeGST) throws RemoteException;

    public BigDecimal getMembershipValue(boolean includeGST, boolean multiplyTerms) throws RemoteException;

    public UnearnedValue getPaidRemainingMembershipValue /* ForOneYear */() throws RemoteException;

    public String getRego();

    public String getMake();

    public String getModel();

    public String getVin();

    public String getYear();
}
