package com.ract.membership;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.util.LogUtil;

/**
 * Run delivery in scheduled mode.
 * 
 * @author dgk, JYH
 * @created 31 July 2002
 * @version 1.0
 * 
 *          Rewritten to use the Job interface
 */

public class DeliveryRoundJob implements Job {

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	String instName = context.getJobDetail().getName();
	String instGroup = context.getJobDetail().getGroup();

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();

	try {
	    // LogUtil.log(this.getClass(),"trigger "+context.getTrigger().getStartTime());
	    String annualReportFileName = dataMap.getString("annualReportFile");
	    String motorNewsFileName = dataMap.getString("motorNewsFile");
	    String accessMembersFileName = dataMap.getString("accessMembersFile");
	    String errorFileName = dataMap.getString("errorFile");
	    String emailAddress = dataMap.getString("emailAddress");

	    DeliveryRoundMgr dr = MembershipEJBHelper.getDeliveryRoundMgr();
	    dr.extractDeliveryList(annualReportFileName, motorNewsFileName, accessMembersFileName, errorFileName, emailAddress);

	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }

}
