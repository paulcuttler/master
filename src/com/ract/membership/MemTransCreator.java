package com.ract.membership;

/**
 * Title:        MemTransCreator
 * Description:  Tags a class that implements this interface as being able to
 *               provide specialised facilities for creating a transaction.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author
 * @version 1.0
 */

import java.rmi.RemoteException;

public interface MemTransCreator {
    public void createTransactionFees(int chargeableMemberCount, int memberCount) throws RemoteException;
}
