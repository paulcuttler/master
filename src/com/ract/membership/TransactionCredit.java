package com.ract.membership;

import java.math.BigDecimal;

/**
 * Title: TransactionCredit Description: Objects that are put into the credit
 * map on a membership must implement this interface. Copyright: Copyright (c)
 * 2003 Company: RACT
 * 
 * @author
 * @version 1.0
 */

public interface TransactionCredit {
    /**
     * These are the allowed keys for amounts held in the credit amount map.
     */

    /**
     * UnearnedCredit is typically implemented by a class of type UnearnedValue.
     */
    public final static String CREDIT_UNEARNED = "UnearnedCredit";

    /**
     * QuoteCredit is typically implemented by a class of type
     * MembershipTransactionAdjustment.
     */
    public final static String CREDIT_QUOTE = "QuoteCredit";

    /**
     * Additional credit given to membership for no charge.
     */
    public final static String CREDIT_COMPLIMENTARY = "ComplimentaryCredit";

    /**
     * Credit to represent affiliated club transfer credit.
     */
    public final static String CREDIT_AFFILIATED_CLUB = "AffiliatedClubCredit";

    public BigDecimal getTransactionCreditAmount();

    public void setTransactionCreditAmount(BigDecimal creditAmount);

}
