package com.ract.membership;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

/**
 * Title: Membership system Description: Copyright: Copyright (c) 2002 Company:
 * RACT
 * 
 * @author
 * @version 1.0
 */

public interface DeliveryRoundMgrHome extends EJBHome {
    public DeliveryRoundMgr create() throws RemoteException, CreateException;
}