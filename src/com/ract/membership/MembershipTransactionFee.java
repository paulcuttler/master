package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.Writable;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * Identifies the specific fee that applied to the transaction.
 * 
 * @author hollidayj
 * @created 1 August 2002
 */
public class MembershipTransactionFee implements Cloneable, Serializable, Writable {

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "MembershipTransactionFee";

    private Integer transactionID;

    /**
     * The ID of the fee specification that the transaction fee is associated
     * with.
     */
    private Integer feeSpecificationID;

    /**
     * Caches a reference to the fee specification vo. If null it needs to be
     * initialised.
     */
    private FeeSpecificationVO feeSpecificationVO;

    public DateTime getFeeEffectiveDate() {
	return feeEffectiveDate;
    }

    public void setFeeEffectiveDate(DateTime feeEffectiveDate) {
	this.feeEffectiveDate = feeEffectiveDate;
    }

    /**
     * Date to be used for fee/discount lookup
     */
    private DateTime feeEffectiveDate;

    /**
     * The fee that should have been charged. Copied from the fee specification.
     */
    private double feePerMember;

    /**
     * The fee per vehicle that should have been charged. Copied from the fee
     * specification.
     */
    private double feePerVehicle;

    /**
     * The number of vehicles that was used to calculate the charged amount.
     */
    private int vehicleCount;

    private String feeDescription;

    /**
     * A list of discounts that applied or were chosen for the fee that applied
     * to the transaction.
     */
    private Vector discountList;

    /**
     * Constructors *****************
     * 
     * @param transID
     *            Description of the Parameter
     * @param feeSpecID
     *            Description of the Parameter
     * @param feePerMember
     *            Description of the Parameter
     * @param feePerVehicle
     *            Description of the Parameter
     * @param vehicleCnt
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public MembershipTransactionFee(Integer feeSpecID, double feePerMember, double feePerVehicle, int vehicleCnt) throws RemoteException {
	if (feeSpecID == null || feeSpecID.intValue() < 1) {
	    throw new RemoteException("Trying to create membership transaction fee with invalid fee specification ID '" + feeSpecID + "'");
	}

	this.feeSpecificationID = feeSpecID;
	this.vehicleCount = vehicleCnt;
	this.feePerMember = NumberUtil.roundDouble(feePerMember, 2);
	this.feePerVehicle = NumberUtil.roundDouble(feePerVehicle, 2);
    }

    /**
     * Constructor for the MembershipTransactionFee object
     * 
     * @param transID
     *            Description of the Parameter
     * @param feeSpecVO
     *            Description of the Parameter
     * @param feePerMember
     *            Description of the Parameter
     * @param feePerVehicle
     *            Description of the Parameter
     * @param vehicleCnt
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTransactionFee(FeeSpecificationVO feeSpecVO, double feePerMember, double feePerVehicle, int vehicleCnt) throws RemoteException {
	if (feeSpecVO == null) {
	    throw new RemoteException("The fee specification VO cannot be null when creating a membership transaction fee");
	}

	this.feeSpecificationVO = feeSpecVO;
	this.feeSpecificationID = feeSpecVO.getFeeSpecificationID();
	this.vehicleCount = vehicleCnt;
	this.feePerMember = NumberUtil.roundDouble(feePerMember, 2);
	this.feePerVehicle = NumberUtil.roundDouble(feePerVehicle, 2);
    }

    // Construct the membership transaction fee getting the fee per member and
    // fee per vehicle from the fee specification.
    /**
     * Constructor for the MembershipTransactionFee object
     * 
     * @param transID
     *            Description of the Parameter
     * @param feeSpecVO
     *            Description of the Parameter
     * @param vehicleCnt
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTransactionFee(FeeSpecificationVO feeSpecVO, int vehicleCnt) throws RemoteException {
	if (feeSpecVO == null) {
	    throw new RemoteException("A fee specification must be specified when creating a membership transaction fee.");
	}

	this.feeSpecificationVO = feeSpecVO;
	this.feeSpecificationID = feeSpecVO.getFeeSpecificationID();
	this.feePerMember = feeSpecVO.getFeePerMember();
	this.feePerVehicle = feeSpecVO.getFeePerVehicle();
	this.vehicleCount = vehicleCnt;
    }

    public boolean isJoinFee() throws RemoteException {
	boolean joinFee = false;
	FeeSpecificationVO feeSpec = null;
	feeSpec = this.getFeeSpecificationVO();
	if (feeSpec.getFeeType().isJoinType()) {
	    joinFee = true;
	}
	// LogUtil.debug(this.getClass(),this.toString()+" "+joinFee);
	return joinFee;
    }

    public MembershipTransactionFee(Node feeNode) throws RemoteException {
	if (feeNode == null || !feeNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
	    throw new SystemException("Failed to create MembershipTransactionFee from XML node. The node is not valid.");
	}

	// Initialise the lists
	this.discountList = new Vector();

	NodeList elementList = feeNode.getChildNodes();
	Node elementNode = null;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();
	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;
	    }

	    if ("discountList".equals(nodeName)) {
		NodeList selectList = elementNode.getChildNodes();
		if (selectList != null) {
		    for (int j = 0; j < selectList.getLength(); j++) {
			Node node = selectList.item(j);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
			    MembershipTransactionDiscount memTransDisc = new MembershipTransactionDiscount(node);
			    this.discountList.add(memTransDisc);
			}
		    }
		}
	    } else if ("feeDescription".equals(nodeName) && valueText != null) {
		this.feeDescription = valueText;
	    } else if ("feePerMember".equals(nodeName) && valueText != null) {
		this.feePerMember = Double.parseDouble(valueText);
	    } else if ("feePerVehicle".equals(nodeName) && valueText != null) {
		this.feePerVehicle = Double.parseDouble(valueText);
	    } else if ("feeSpecificationID".equals(nodeName) && valueText != null) {
		this.feeSpecificationID = new Integer(valueText);
	    } else if ("transactionID".equals(nodeName) && valueText != null) {
		this.transactionID = new Integer(valueText);
	    } else if ("vehicleCount".equals(nodeName) && valueText != null) {
		this.vehicleCount = Integer.parseInt(valueText);
	    }
	}
    }

    /**
     * Getter methods *****************************
     * 
     * @return The transactionID value
     */
    public Integer getTransactionID() {
	return this.transactionID;
    }

    /**
     * Gets the feeSpecificationID attribute of the MembershipTransactionFee
     * object
     * 
     * @return The feeSpecificationID value
     */
    public Integer getFeeSpecificationID() {
	return this.feeSpecificationID;
    }

    /**
     * Adds a feature to the Discount attribute of the MembershipTransactionFee
     * object
     * 
     * @param discountTypeVO
     *            The feature to be added to the Discount attribute
     * @param discountAmount
     *            The feature to be added to the Discount attribute
     * @param discountReason
     *            The feature to be added to the Discount attribute
     * @exception RemoteException
     *                Description of the Exception
     */
    public void addDiscount(DiscountTypeVO discountTypeVO, double discountAmount, String discountReason) throws RemoteException {
	// create a MembershipTransactionDiscount object using the passed in
	// values
	MembershipTransactionDiscount membershipTxDiscount = new MembershipTransactionDiscount(discountTypeVO, discountAmount, discountReason);

	// then add it to the discount list
	addDiscount(membershipTxDiscount);
    }

    public void addDiscount(DiscountTypeVO discountTypeVO, double discountAmount, String discountReason, String description) throws RemoteException {
	// create a MembershipTransactionDiscount object using the passed in
	// values
	MembershipTransactionDiscount membershipTxDiscount = new MembershipTransactionDiscount(discountTypeVO, discountAmount, discountReason);
	membershipTxDiscount.setDiscountDescription(description);
	// then add it to the discount list
	addDiscount(membershipTxDiscount);
    }

    /**
     * Add a discount to the transaction fee for the discount type. Calculate
     * the amount of discount to be given based on the transaction fee. Can only
     * be used for non user defined discount types. Does not add the discount if
     * the discount has already been add. Does not add the discount if the
     * discount amount is zero.
     * 
     * @param discountTypeVO
     *            The feature to be added to the Discount attribute
     * @exception RemoteException
     *                Description of the Exception
     */
    public void addDiscount(DiscountTypeVO discountTypeVO) throws RemoteException {
	if (discountTypeVO == null) {
	    throw new RemoteException("A discount type must be specified when adding a discount to a transaction fee.");
	}
	// Calculate the amount of discount to give.
	// Don't give more than the remaining discountable fee.
	double discountAmount = discountTypeVO.getDiscountAmount();
	double discountPercentage = discountTypeVO.getDiscountPercentage();
	if (discountPercentage > 1) {
	    throw new SystemException("Discount type '" + discountTypeVO.getDiscountTypeCode() + "' has a decimal representing percentage specified that is greater than 1");
	}

	// if this is a driver Training (100%) discount then
	// discount amount should always be equal to (100% of) the Advantage fee
	if (discountTypeVO.getPercentageOf() != null) {
	    if (feeEffectiveDate == null) {
		feeEffectiveDate = new DateTime();
	    }
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    double tempFee = refMgr.getFeeForProduct(discountTypeVO.getPercentageOf(), MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, 1,// groupCount,
		    1, // groupIndex,
		    feeEffectiveDate);
	    discountAmount += tempFee * discountPercentage;
	} else {
	    discountAmount += getGrossFee() * discountPercentage;
	}

	double netFee = getNetTransactionFee();

	// LogUtil.debug(this.getClass(),"desc = "+discountTypeVO.getDescription());

	if (discountAmount > netFee) {
	    discountAmount = netFee;
	}
	// Only add the discount if it is greater than zero
	if (discountAmount > 0.0 || discountTypeVO.getDiscountPeriod() != null) {
	    // create a MembershipTransactionDiscount object using the passed in
	    // values
	    MembershipTransactionDiscount membershipTxDiscount = new MembershipTransactionDiscount(discountTypeVO, discountAmount, null);

	    membershipTxDiscount.setDiscountDescription(discountTypeVO.getDescription());

	    // then add it to the discount list
	    addDiscount(membershipTxDiscount);
	}
    }

    /**
     * Add a membership transaction discount to the transaction fee. Does not
     * add the discount if the discount type has already been add.
     * 
     * @param transDiscount
     *            The feature to be added to the Discount attribute
     * @exception RemoteException
     *                Description of the Exception
     */
    public void addDiscount(MembershipTransactionDiscount transDiscount) throws RemoteException {
	if (transDiscount == null) {
	    throw new RemoteException("Trying to add a null membership transaction discount!");
	}

	// check that the discount list has been initialised
	if (this.discountList == null) {
	    this.discountList = new Vector();
	}

	if (!hasDiscountType(transDiscount.getDiscountCode())) {
	    this.discountList.add(transDiscount);
	}
    }

    /**
     * Returns true if the membership transaction fee has the specified discount
     * type attached to it.
     * 
     * @param discCode
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public boolean hasDiscountType(String discCode) throws RemoteException {
	boolean found = false;
	Collection discList = getDiscountList();
	if (discList != null) {
	    MembershipTransactionDiscount memTransDisc = null;
	    Iterator discListIterator = discList.iterator();
	    while (!found && discListIterator.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) discListIterator.next();
		found = memTransDisc.getDiscountCode().equals(discCode);
	    }
	}
	return found;
    }

    public MembershipTransactionDiscount getDiscountType(String discCode) throws RemoteException {
	LogUtil.log(this.getClass(), "discCode=" + discCode);

	boolean found = false;
	Collection discList = getDiscountList();
	MembershipTransactionDiscount memTransDisc = null;
	MembershipTransactionDiscount tmpMemTransDisc = null;
	if (discList != null) {
	    Iterator discListIterator = discList.iterator();
	    while (!found && discListIterator.hasNext()) {
		tmpMemTransDisc = (MembershipTransactionDiscount) discListIterator.next();
		LogUtil.log(this.getClass(), "loop memTransDisc=" + memTransDisc);
		found = tmpMemTransDisc.getDiscountCode().equals(discCode);
		if (found) {
		    memTransDisc = tmpMemTransDisc;
		}
	    }
	}
	LogUtil.log(this.getClass(), "memTransDisc=" + memTransDisc);
	return memTransDisc;
    }

    /**
     * Get the discount total for a fee by the discount type code.
     * 
     * @param discCode
     *            String
     * @throws RemoteException
     * @return double
     */
    public double getDiscountTotal(String discCode) throws RemoteException {

	double discountTotal = 0;
	Collection discList = getDiscountList();
	if (discList != null) {
	    MembershipTransactionDiscount memTransDisc = null;
	    Iterator discListIterator = discList.iterator();
	    while (discListIterator.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) discListIterator.next();
		if (memTransDisc.getDiscountCode().equals(discCode)) {
		    discountTotal += memTransDisc.getDiscountAmount();
		}
	    }
	}
	return discountTotal;

    }

    /**
     * Return true if the fee specification that the membership transaction fee
     * is an instance of allows any discounts.
     */
    public boolean canHaveDiscounts(boolean includeAutomaticDiscounts) throws RemoteException {
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	return feeSpecVO.hasAllowedDiscounts(includeAutomaticDiscounts);
    }

    /**
     * Return true if the fee can be discounted. A fee can be discounted if the
     * fee less the fee discounts are greater than zero.
     */
    public boolean canBeDiscounted() throws RemoteException {

	return getNetTransactionFee() > 0.0 && canHaveDiscounts(false);
    }

    /**
     * @param discountTypeCode
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void removeDiscount(String discountTypeCode) throws RemoteException {
	// LogUtil.debug(this.getClass(),"removeDiscount="+discountTypeCode);

	if (discountTypeCode == null || discountTypeCode.length() < 1) {
	    throw new RemoteException("Trying to remove an empty discount type code.");
	}

	// Initialise the list if required.
	Collection discList = getDiscountList();

	// Now work with the list directly as the above method returns a copy
	// only.
	if (this.discountList != null && this.discountList.size() > 0) {
	    ListIterator discountListIterator = this.discountList.listIterator();
	    boolean found = false;
	    while (discountListIterator.hasNext() && !found) {
		MembershipTransactionDiscount memTxDiscount = (MembershipTransactionDiscount) discountListIterator.next();
		if (memTxDiscount.getDiscountCode().equals(discountTypeCode)) {
		    discountListIterator.remove();
		    found = true;
		}
	    }
	}
    }

    /**
     * Clears the list of discounts associated with the transaction fee.
     */
    public void clearDiscountList() {
	if (this.discountList != null) {
	    this.discountList.clear();
	}
    }

    /**
     * Return the fee per member.
     * 
     * @return double
     */
    public double getFeePerMember() {
	return this.feePerMember;
    }

    /**
     * Return the fee per vehicle.
     * 
     * @return double
     */
    public double getFeePerVehicle() {
	return this.feePerVehicle;
    }

    /**
     * Return the number of vehicles used to calculate the total vehicle fee.
     * 
     * @return int
     */
    public int getVehicleCount() {
	return this.vehicleCount;
    }

    /**
     * If no fee description has been set thenreturn a standard description.
     * 
     * @return The feeDescription value
     */
    public String getFeeDescription() {
	String feeDesc = this.feeDescription;
	if (feeDesc == null) {
	    StringBuffer sb = new StringBuffer();
	    sb.append(CurrencyUtil.formatDollarValue(this.feePerMember));
	    sb.append(" + ");
	    sb.append(this.vehicleCount);
	    sb.append((this.vehicleCount == 1 ? " vehicle" : " vehicles"));
	    sb.append(" @ ");
	    sb.append(CurrencyUtil.formatDollarValue(this.feePerVehicle));
	    sb.append(" per vehicle");
	    feeDesc = sb.toString();
	}
	return feeDesc;
    }

    /**
     * Return the total fee for the number of vehicles.
     * 
     * @return double
     */
    public double getTotalVehicleFee() {
	return this.vehicleCount * this.feePerVehicle;
    }

    /**
     * Returns the full fee. The full fee is the fee per member plus the fee per
     * vehicle times the number of vehicles attached to the membership.
     * 
     * @return double
     */
    public double getGrossFee() {
	return this.feePerMember + getTotalVehicleFee();
    }

    /**
     * Return the total amount of discount given. This amount is capped to the
     * difference between the fee and the minimum fee. Plus sum of the discounts
     * that can cause the fee to be reduced below the minimum fee.
     * 
     * @return double
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getTotalDiscount() throws RemoteException {
	double cappedDiscount = 0.0;
	double nonCappedDiscount = 0.0;
	double discountCapAmount = 0.0;
	double totalDiscount = 0.0;

	Collection discList = getDiscountList();
	if (discList != null && !discList.isEmpty()) {
	    FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	    MembershipTransactionDiscount memTransDisc = null;
	    DiscountTypeVO discTypeVO = null;
	    Iterator it = discList.iterator();
	    while (it.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) it.next();
		discTypeVO = memTransDisc.getDiscountTypeVO();

		// LogUtil.debug(this.getClass(),">>>disc = "+discTypeVO);

		if (discTypeVO.isMinimumFeeApplicable()) {
		    cappedDiscount += memTransDisc.getDiscountAmount();
		} else {
		    nonCappedDiscount += memTransDisc.getDiscountAmount();
		}
		// LogUtil.debug(this.getClass(),">>>cappedDiscount = "+cappedDiscount);
		// LogUtil.debug(this.getClass(),">>>nonCappedDiscount = "+nonCappedDiscount);
	    }
	    // Cap the sum of the discounts that are subject to the minimum fee.
	    discountCapAmount = getGrossFee() - feeSpecVO.getMinimumFee();
	    // LogUtil.debug(this.getClass(),">>>discountCapAmount = "+discountCapAmount);
	    // Make sure the cap amount is not less than zero.
	    discountCapAmount = discountCapAmount < 0 ? 0 : discountCapAmount;
	    // LogUtil.debug(this.getClass(),">>>discountCapAmount = "+discountCapAmount);
	    if (cappedDiscount > discountCapAmount) {
		cappedDiscount = discountCapAmount;
	    }
	}
	totalDiscount = cappedDiscount + nonCappedDiscount;
	// LogUtil.debug(this.getClass(),">>>totalDiscount = "+totalDiscount);
	// Make sure that the total discount is not more than the gross fee.
	totalDiscount = totalDiscount > getGrossFee() ? getGrossFee() : totalDiscount;
	// LogUtil.debug(this.getClass(),">>>after totalDiscount = "+totalDiscount);
	return NumberUtil.roundDouble(totalDiscount, 2);
    }

    /**
     * Return the non-offset amount of discount given.
     * 
     * @return double
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getNonOffsetDiscount() throws RemoteException {
	double cappedDiscount = 0.0;
	double nonCappedDiscount = 0.0;
	double discountCapAmount = 0.0;
	double totalDiscount = 0.0;

	Collection discList = getDiscountList();
	if (discList != null && !discList.isEmpty()) {
	    FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	    MembershipTransactionDiscount memTransDisc = null;
	    DiscountTypeVO discTypeVO = null;
	    Iterator it = discList.iterator();
	    while (it.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) it.next();
		discTypeVO = memTransDisc.getDiscountTypeVO();
		if (!discTypeVO.isOffsetDiscount()) {
		    // LogUtil.debug(this.getClass(),"non offset only");
		    if (discTypeVO.isMinimumFeeApplicable()) {
			cappedDiscount += memTransDisc.getDiscountAmount();
		    } else {
			nonCappedDiscount += memTransDisc.getDiscountAmount();
		    }
		}
	    }
	    // Cap the sum of the discounts that are subject to the minimum fee.
	    discountCapAmount = getGrossFee() - feeSpecVO.getMinimumFee();
	    // Make sure the cap amount is not less than zero.
	    discountCapAmount = discountCapAmount < 0 ? 0 : discountCapAmount;

	    if (cappedDiscount > discountCapAmount) {
		cappedDiscount = discountCapAmount;
	    }
	}
	totalDiscount = cappedDiscount + nonCappedDiscount;

	// Make sure that the total discount is not more than the gross fee.
	totalDiscount = totalDiscount > getGrossFee() ? getGrossFee() : totalDiscount;

	return NumberUtil.roundDouble(totalDiscount, 2);
    }

    /**
     * Return the amount of discount given that effects the income total. This
     * is identified by the dicsount not having an account attached.
     * 
     * @return double
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getIncomeDiscount() throws RemoteException {
	double discountAmount = 0.0;
	Collection discList = getDiscountList();
	if (discList != null) {
	    MembershipTransactionDiscount memTransDisc = null;
	    Iterator discListIT = discList.iterator();
	    while (discListIT.hasNext()) {

		memTransDisc = (MembershipTransactionDiscount) discListIT.next();

		// LogUtil.debug(this.getClass(), memTransDisc.toString());
		Integer accountID = memTransDisc.getDiscountTypeVO().getDiscountAccountID();
		if ((accountID == null || accountID.intValue() == 0)
		// unearned and transfer credit are both sourced from suspense
			&& !DiscountTypeVO.TYPE_CREDIT.equals(memTransDisc.getDiscountCode()) && !DiscountTypeVO.TYPE_TRANSFER_CREDIT.equals(memTransDisc.getDiscountCode())) {
		    discountAmount += memTransDisc.getDiscountAmount();
		}
	    }
	}
	return discountAmount;
    }

    /**
     * Return the amount of credit discount given.
     * 
     * @return double
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getCreditDiscount() throws RemoteException {
	double discountAmount = 0.0;
	Collection discList = getDiscountList();
	if (discList != null) {
	    MembershipTransactionDiscount memTransDisc = null;
	    Iterator discListIT = discList.iterator();
	    while (discListIT.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) discListIT.next();
		if (DiscountTypeVO.TYPE_CREDIT.equals(memTransDisc.getDiscountCode())) {
		    discountAmount += memTransDisc.getDiscountAmount();
		}
	    }
	}
	return discountAmount;
    }

    /**
     * Get the transaction fee discount representing the unearned credit.
     * 
     * @throws RemoteException
     * @return MembershipTransactionDiscount
     */
    public MembershipTransactionDiscount getUnearnedCreditDiscount() throws RemoteException {
	Collection discList = getDiscountList();
	MembershipTransactionDiscount memTransDisc = null;
	if (discList != null) {
	    boolean found = false;
	    Iterator discListIT = discList.iterator();
	    while (discListIT.hasNext() && !found) {
		memTransDisc = (MembershipTransactionDiscount) discListIT.next();
		if (DiscountTypeVO.TYPE_UNEARNED_CREDIT.equals(memTransDisc.getDiscountCode())) {
		    found = true;
		}
	    }
	}
	// LogUtil.debug(this.getClass(), "in disc" + memTransDisc);
	return memTransDisc;
    }

    /**
     * Return the amount of paid and unearned credit that has been created in
     * this transaction. Search the selected discount list for a discount of
     * TYPE_UNEARNED_CREDIT. If there is more than one (there shouldn't be but
     * you never know) then add the amounts together.
     * 
     * @return double
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getUnearnedCredit() throws RemoteException {
	double creditAmount = 0.0;
	Collection discList = getDiscountList();
	if (discList != null) {
	    MembershipTransactionDiscount memTransDisc = null;
	    Iterator discListIT = discList.iterator();
	    while (discListIT.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) discListIT.next();
		if (DiscountTypeVO.TYPE_UNEARNED_CREDIT.equals(memTransDisc.getDiscountCode())) {
		    creditAmount += memTransDisc.getDiscountAmount();
		}
	    }
	}
	return creditAmount;
    }

    /**
     * Get the transfer credit.
     * 
     * @throws RemoteException
     * @return double
     */
    public double getTransferCredit() throws RemoteException {
	double transferAmount = 0.0;
	Collection discList = getDiscountList();
	if (discList != null) {
	    MembershipTransactionDiscount memTransDisc = null;
	    Iterator discListIT = discList.iterator();
	    while (discListIT.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) discListIT.next();
		if (DiscountTypeVO.TYPE_TRANSFER_CREDIT.equals(memTransDisc.getDiscountCode())) {
		    transferAmount += memTransDisc.getDiscountAmount();
		}
	    }
	}
	return transferAmount;
    }

    /**
     * Return the net transaction fee. The net transaction fee is defined as the
     * gross transaction fees less the non discretionary discounts.
     * 
     * @return The netTransactionFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getNetTransactionFee() throws RemoteException {
	return getNetTransactionFee(true);
    }

    public double getNetTransactionFee(boolean includeOffsetDiscounts) throws RemoteException {

	if (includeOffsetDiscounts) {
	    return getGrossFee() - getTotalDiscount();
	} else {
	    // LogUtil.debug(this.getClass(),"non offset");
	    return getGrossFee() - getNonOffsetDiscount();
	}
    }

    /**
     * Return the net transaction fee if the membership transaction fee is
     * associated to a fee specification that is of the specified fee type.
     * 
     * @param feeTypeCode
     *            Description of the Parameter
     * @return The netTransactionFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getNetTransactionFee(String feeTypeCode) throws RemoteException {
	double netFee = 0.0;
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	if (feeSpecVO.getFeeTypeCode().equals(feeTypeCode)) {
	    netFee = getNetTransactionFee();
	}

	return netFee;
    }

    /**
     * Return the net transaction fee if the membership transaction fee is
     * associated to a fee specification that has the specified earned account
     * and unearned account.
     * 
     * @param earnedAccount
     *            Description of the Parameter
     * @param unearnedAccount
     *            Description of the Parameter
     * @return The netTransactionFee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getNetTransactionFee(MembershipAccountVO earnedAccount, MembershipAccountVO unearnedAccount, boolean includeOffsetDiscounts) throws RemoteException {
	double netFee = 0.0;
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	if (feeSpecVO.hasAccounts(earnedAccount, unearnedAccount)) {
	    netFee = getNetTransactionFee(includeOffsetDiscounts);
	}

	// LogUtil.debug(this.getClass(),
	// "NetTransactionFee for accounts: "
	// + (earnedAccount == null ? "null" : earnedAccount.toString()) + ","
	// + (unearnedAccount == null ? "null" : unearnedAccount.toString()) +
	// " = "
	// + netFee);

	return netFee;
    }

    /**
     * Return the amount of earned income associated with this fee. If the
     * associated fee specification does not have an unearned account defined
     * then the transaction fee is considered to be earned immediately. If the
     * fee is considered earned then the net transaction fee is returned
     * otherwise zero.
     * 
     * @return The earnedIncomeAmount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getEarnedIncomeAmount() throws RemoteException {
	double earnedAmount = 0.0;
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	// LogUtil.debug(this.getClass(),"feeSpecVO+++"+feeSpecVO.getDescription());
	if (!feeSpecVO.isEarned()) {

	    earnedAmount = getNetTransactionFee(false);
	    // LogUtil.debug(this.getClass(),"earnedAmount+++"+earnedAmount);
	}

	return earnedAmount;
    }

    /**
     * Determine if the fee is a value fee. ie. has unearned value.
     * 
     * @throws RemoteException
     * @return boolean
     */
    public boolean isValueFee() throws RemoteException {
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	LogUtil.debug(this.getClass(), "feeSpecVO=" + feeSpecVO);
	return feeSpecVO.isValueFee();
    }

    /**
     * Get discount total for the value fees only
     * 
     * @throws RemoteException
     * @return double
     */
    public double getValueFeeDiscountTotal() throws RemoteException {
	double valueFeeDiscountTotal = 0;
	// fee is a value fee
	if (this.isValueFee()) {
	    ArrayList discountList = this.getDiscountList();
	    if (discountList != null && discountList.size() > 0) {
		MembershipTransactionDiscount transDiscount = null;
		Iterator discountListIterator = discountList.iterator();
		while (discountListIterator.hasNext()) {
		    transDiscount = (MembershipTransactionDiscount) discountListIterator.next();
		    valueFeeDiscountTotal += transDiscount.getDiscountAmount();
		}
	    }
	}
	return valueFeeDiscountTotal;
    }

    /**
     * Return the amount of unearned income associated with this fee. If the
     * associated fee specification does have an unearned account defined then
     * the transaction fee is considered to be unearned. It will be earned over
     * the life of the membership. If the fee is considered unearned then the
     * net transaction fee is returned otherwise zero.
     * 
     * @return The unearnedIncomeAmount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public double getUnearnedIncomeAmount() throws RemoteException {
	double unearnedAmount = 0.0;
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	// LogUtil.debug(this.getClass(), "feeSpec" + this.feeDescription + " "
	// + feeSpecVO.toString());
	if (feeSpecVO.isEarned()) {
	    unearnedAmount = getNetTransactionFee(false);
	    // LogUtil.debug(this.getClass(), "unearnedAmount" +
	    // unearnedAmount);
	}

	return unearnedAmount;
    }

    /**
     * Return the maximum discount period from the discounts applied to this
     * transaction fee.
     * 
     * @return The discountPeriod value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Interval getMaxDiscountPeriod() throws RemoteException {
	Interval maxDiscountPeriod = null;
	Collection discountList = getDiscountList();
	if (discountList != null) {
	    MembershipTransactionDiscount memTransDisc = null;
	    Interval tmpPeriod = null;
	    Iterator discListIterator = discountList.iterator();
	    while (discListIterator.hasNext()) {
		memTransDisc = (MembershipTransactionDiscount) discListIterator.next();
		tmpPeriod = memTransDisc.getDiscountPeriod();
		// LogUtil.debug(this.getClass(),"Fee type = " +
		// this.getFeeSpecificationVO().getFeeTypeCode() +
		// ", Discount period = " + (tmpPeriod== null ? "" :
		// tmpPeriod.toString()));
		if (tmpPeriod != null) {
		    if (maxDiscountPeriod == null || tmpPeriod.compareTo(maxDiscountPeriod) > 0) {
			maxDiscountPeriod = tmpPeriod;
		    }
		}
	    }
	}
	// LogUtil.debug(this.getClass(),"Fee type = " +
	// this.getFeeSpecificationVO().getFeeTypeCode() +
	// ", Max discount period = " + (maxDiscountPeriod == null ? "" :
	// maxDiscountPeriod.toString()));
	return maxDiscountPeriod;
    }

    /**
     * Return the fee specification value object that corresponds to the fee
     * specification ID. Initialise the cached reference to the fee
     * specification VO if this is the first call.
     * 
     * @return The feeSpecificationVO value
     * @exception RemoteException
     *                Description of the Exception
     */
    public FeeSpecificationVO getFeeSpecificationVO() throws RemoteException {
	LogUtil.debug(this.getClass(), "getFeeSpecificationVO=" + feeSpecificationID + "," + feeSpecificationVO);
	if (this.feeSpecificationID != null && this.feeSpecificationVO == null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.feeSpecificationVO = refMgr.getFeeSpecification(this.feeSpecificationID);
	}
	if (this.feeSpecificationVO == null) {
	    throw new RemoteException("Failed to get fee specification '" + this.feeSpecificationID + "'!");
	}

	return this.feeSpecificationVO;
    }

    /**
     * Return the list of discounts that are allowed to be attached to the fee
     * type that this membership transaction fee relates to.
     */
    public Collection getAllowableDiscountList() throws RemoteException {
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	return feeSpecVO.getAllowableDiscountList();
    }

    /**
     * Returns the specified discount type if it is an allowed discount type for
     * the fee. Returns null if the discount type is not an allowable discount
     * type for the fee.
     */
    public DiscountTypeVO getAllowableDiscountType(String discountTypeCode) throws RemoteException {
	FeeSpecificationVO feeSpecVO = getFeeSpecificationVO();
	return feeSpecVO.getAllowableDiscountType(discountTypeCode);
    }

    /**
     * Return a copy of the list of discounts applied to the transaction fee.
     * Returns a null list if no discounts have been added, the transaction ID,
     * is null and the fee specification ID is null. Returns an empty list if no
     * discounts have been added, the transaction ID is not null and the fee
     * specification ID is not null and there are no discounts.
     * 
     * @return The discountList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getDiscountList() throws RemoteException {
	ArrayList discList = null;
	if (this.discountList == null && this.transactionID != null && this.feeSpecificationID != null) {
	    // MembershipTransactionMgr memTransMgr =
	    // MembershipEJBHelper.getMembershipTransactionMgr();
	    MembershipMgr mMgr = MembershipEJBHelper.getMembershipMgr();
	    this.discountList = mMgr.getMembershipTransactionDiscountList(this.transactionID, this.feeSpecificationID);
	}
	if (this.discountList != null) {
	    discList = new ArrayList(this.discountList);
	}
	return discList;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTransactionFee copy() throws RemoteException {
	MembershipTransactionFee memTxFee = null;
	try {
	    memTxFee = (MembershipTransactionFee) this.clone();
	} catch (CloneNotSupportedException e) {
	    /**
	     * @todo This should throw an exception.
	     */
	    throw new RemoteException("Cannot copy MembershipTransactionFee", e);
	}
	return memTxFee;
    }

    /**
     * Checks that this fee adds value to the membership and then reduces the
     * value by any discounts that are flagged to reduce the membership value
     */
    public double getMembershipValue() throws RemoteException {
	boolean isValueFee;
	try {
	    isValueFee = this.getFeeSpecificationVO().isValueFee();
	} catch (RemoteException e) {
	    throw new RemoteException("Error checking fee spec for value fee : " + e.getMessage(), e);
	}

	if (isValueFee) {
	    double value = this.getGrossFee();
	    Collection discountList = this.getDiscountList();
	    if (discountList != null) {
		Iterator discountListIterator = discountList.iterator();
		while (discountListIterator.hasNext()) {
		    MembershipTransactionDiscount discount = (MembershipTransactionDiscount) discountListIterator.next();
		    if (discount.getDiscountTypeVO().isMembershipValueReducing()) {
			value -= discount.getDiscountAmount();
		    }
		}
	    }
	    return value;
	} else {
	    return 0.0;
	}
    }

    /**
     * Setter methods *****************************
     * 
     * @param transID
     *            The new transactionID value
     */

    public void setTransactionID(Integer transID) {
	this.transactionID = transID;
    }

    /**
     * Sets the feePerVehicle attribute of the MembershipTransactionFee object
     * 
     * @param feePerVehicle
     *            The new feePerVehicle value
     */
    public void setFeePerVehicle(double feePerVehicle) {
	this.feePerVehicle = feePerVehicle;
    }

    /**
     * Sets the feePerMember attribute of the MembershipTransactionFee object
     * 
     * @param feePerMember
     *            The new feePerMember value
     */
    public void setFeePerMember(double feePerMember) {
	this.feePerMember = feePerMember;
    }

    /**
     * Sets the feeDescription attribute of the MembershipTransactionFee object
     * 
     * @param feeDesc
     *            The new feeDescription value
     */
    public void setFeeDescription(String feeDesc) {
	this.feeDescription = feeDesc;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public String toString() {
	StringBuffer sb = new StringBuffer();
	sb.append("transactionID=");
	sb.append((this.transactionID == null ? "null" : this.transactionID.toString()));
	sb.append("feeSpecificationID=");
	sb.append((this.feeSpecificationID == null ? "null" : this.feeSpecificationID.toString()));
	sb.append(", feeDescription=");
	sb.append((this.feeDescription == null ? "null" : this.feeDescription));
	sb.append(", feePerMember=");
	sb.append(this.feePerMember);
	sb.append(", feePerVehicle=");
	sb.append(this.feePerVehicle);
	sb.append(", vehicleCount=");
	sb.append(this.vehicleCount);
	return sb.toString();
    }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, true);
    }

    /**
     * Description of the Method
     * 
     * @param cw
     *            Description of the Parameter
     * @param deepWrite
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("feeDescription", this.feeDescription);
	cw.writeAttribute("feePerMember", NumberUtil.formatValue(this.feePerMember, "########0.00"));
	cw.writeAttribute("feePerVehicle", NumberUtil.formatValue(this.feePerVehicle, "########0.00"));
	cw.writeAttribute("feeSpecificationID", this.feeSpecificationID);
	cw.writeAttribute("feeTypeCode", getFeeSpecificationVO().getFeeTypeCode());
	cw.writeAttribute("grossFee", NumberUtil.formatValue(this.getGrossFee(), "########0.00"));
	cw.writeAttribute("netFee", NumberUtil.formatValue(this.getNetTransactionFee(), "########0.00"));
	cw.writeAttribute("transactionID", this.transactionID);
	cw.writeAttribute("vehicleCount", this.vehicleCount);
	if (deepWrite) {
	    if (this.discountList != null) {
		cw.writeAttributeList("discountList", this.discountList);
	    }
	}
	cw.endClass();
    }

}
