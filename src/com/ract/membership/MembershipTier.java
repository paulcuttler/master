package com.ract.membership;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.SystemException;

/**
 * <p>
 * Class representing the membership tier. Based on membership years.
 * </p>
 * 
 * @hibernate.class table="mem_membership_tier"
 * @author jyh
 * @version 1.0
 */
public class MembershipTier implements Comparable, Serializable {

    /**
     * The tier code that represents significant membership tiers.
     */
    public final static int YEARS_TWENTY_FIVE = 25;
    public final static int YEARS_THIRTY = 30;
    public final static int YEARS_THIRTY_FIVE = 35;
    public final static int YEARS_FORTY = 40;
    public final static int YEARS_FORTY_FIVE = 45;
    public final static int YEARS_FIFTY = 50;

    /**
     * The default tier code if one does not already exist.
     */
    public final static int TIER_CODE_DEFAULT = 1;

    public final static int TIER_CODE_LOYALTY = 2;

    // public final static int TIER_CODE_INELIGIBLE = 0;

    private String tierCode;

    private String tierName;

    private String tierDescription;

    private Integer membershipYears;

    /**
     * @hibernate.property
     * @hibernate.column name="membership_years"
     */
    public Integer getMembershipYears() {
	return membershipYears;
    }

    public int compareTo(Object object) throws ClassCastException {
	MembershipTier membershipTierVO = (MembershipTier) object;
	return this.getMembershipYears().compareTo(membershipTierVO.getMembershipYears());
    }

    /**
     * @hibernate.property
     * @hibernate.column name="tier_description"
     */
    public String getTierDescription() {
	return tierDescription;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="tier_name"
     */
    public String getTierName() {
	return tierName;
    }

    public void setTierCode(String tierCode) {
	this.tierCode = tierCode;
    }

    public void setMembershipYears(Integer membershipYears) {
	this.membershipYears = membershipYears;
    }

    public void setTierDescription(String tierDescription) {
	this.tierDescription = tierDescription;
    }

    public void setTierName(String tierName) {
	this.tierName = tierName;
    }

    /**
     * @hibernate.id column="tier_code" generator-class="assigned"
     */
    public String getTierCode() {
	return tierCode;
    }

    private Collection discountList;

    /**
     * Get the discount list associated with the membership tier.
     * 
     * @throws SystemException
     * @return Collection
     */
    public Collection getDiscountList() throws SystemException {
	if (discountList == null) {
	    MembershipMgr membershipMgr = null;
	    try {
		membershipMgr = MembershipEJBHelper.getMembershipMgr();
		discountList = membershipMgr.getDiscountListByTier(this.getTierCode());
	    } catch (RemoteException ex) {
		throw new SystemException("Unable to get discount list.", ex);
	    }
	}
	return discountList;

    }

    /**
     * Greater than 25 years membership.
     * 
     * @return boolean
     */
    public boolean isEligibleForLoyaltyScheme() {
	boolean eligible = false;
	if (this.getMembershipYears().compareTo(new Integer(YEARS_TWENTY_FIVE)) > 0) {
	    eligible = true;
	}
	return eligible;
    }

    public MembershipTier(String tierCode, String tierName, String tierDescription, Integer membershipYears) {
	this.setTierCode(tierCode);
	this.setTierName(tierName);
	this.setTierDescription(tierDescription);
	this.setMembershipYears(membershipYears);
    }

    public MembershipTier() {

    }

    public String toString() {
	StringBuffer desc = new StringBuffer();
	desc.append(tierCode);
	desc.append(": ");
	desc.append(tierName);
	desc.append(" ");
	desc.append(tierDescription);
	desc.append(" ");
	desc.append(membershipYears);
	try {
	    ArrayList discList = (ArrayList) this.getDiscountList();
	    if (discList != null && discList.size() > 0) {
		for (int i = 0; i < discList.size(); i++) {
		    desc.append(discList.get(i).toString()); // append
							     // description
		}
	    }
	} catch (SystemException ex) {
	    // ignore
	}
	return desc.toString();
    }
}
