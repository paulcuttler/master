package com.ract.membership;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * <p>
 * Run the gold medallion voucher letter program.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class GoldMedallionVoucherLetterJob implements Job {

    /**
     * The job method
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();
	try {
	    DateTime effectiveDate = new DateTime(dataMap.getString("effectiveDate"));
	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	    membershipMgr.produceGMVoucherLetters(effectiveDate);
	} catch (Exception e) {
	    LogUtil.fatal(this.getClass(), e);
	    throw new JobExecutionException(e, false);
	}
    }

}
