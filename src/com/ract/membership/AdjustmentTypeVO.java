package com.ract.membership;

import java.rmi.RemoteException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.ClassWriter;
import com.ract.common.SystemException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.XMLHelper;

/**
 * An adjustment type defines the adjustments that may be made to the total
 * transaction fee charged in a transaction.
 * 
 * @author bakkert
 * @created 8 April 2003
 * @version 1.0
 */
@Entity
@Table(name = "mem_adjustment_type")
public class AdjustmentTypeVO extends ValueObject implements Writable {

    public static final String DISCRETIONARY_DISCOUNT = "Discretionary Discount";

    public static final String QUOTE_ADJUSTMENT = "Quote Adjustment";

    public static final String QUOTE_DISCOUNT = "Quote Discount";

    public static final String ADVERTISED_FEE_ADJUSTMENT = "Advertised Fee Adjustment";

    public static final String ADVERTISED_FEE_DISCOUNT = "Advertised Fee Discount";

    /**
     * A Renewal Notice Adjustment is used to adjust the transaction amount when
     * printing a renewal notice and a previous renewal notice was generated
     * that created a pending fee with a different amount payable. This would
     * occur if the schedule of fees has been changed between creating the
     * pending fee and printing the second renewal notice. i.e. we honour the
     * original fee generated. TB 1/10/2003
     */
    public static final String RENEWAL_NOTICE_ADJUSTMENT = "Renewal Notice Adjustment";

    public static final String RENEWAL_NOTICE_DISCOUNT = "Renewal Notice Discount";

    public final static String WRITABLE_CLASSNAME = "AdjustmentType";

    /**
     * The unique identifier for the adjustment.
     */
    @Id
    @Column(name = "adjustment_type_code")
    protected String adjustmentTypeCode;

    /**
     * Set to 'Active' if the adjustment can be used.
     */
    @Column(name = "adjustment_status")
    protected String adjustmentStatus;

    /**
     * The optional account ID if this adjustment is accounted for financially.
     * If no account is linked then the adjustment simply adjusts the fee.
     */
    @Column(name = "account_id")
    protected Integer accountID;

    @Transient
    private MembershipAccountVO account;

    /**
     * A description of what the adjustment is for.
     */
    protected String description;

    /**
     * Indicates if the adjustment is a debit (true) to the transaction fee or a
     * credit (false) to the transaction fee.
     */
    @Column(name = "debit_adjustment")
    protected boolean debitAdjustment = true;

    // Default constructor required for entity bean
    public AdjustmentTypeVO() {
    }

    // Use this constructor when copying the VO.
    public AdjustmentTypeVO(String adjTypeCode, String adjStatus, Integer accID, boolean debit, String desc) {
	this.adjustmentTypeCode = adjTypeCode;
	this.adjustmentStatus = adjStatus;
	this.accountID = accID;
	this.debitAdjustment = debit;
	this.description = desc;
    }

    // Use this constructor when rebuilding from an XML document
    public AdjustmentTypeVO(Node adjTypeNode) throws RemoteException {
	if (adjTypeNode == null || !adjTypeNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
	    throw new SystemException("Failed to create AdjustmentTypeVO from XML node. The node is not valid.");
	}

	NodeList elementList = adjTypeNode.getChildNodes();
	Node elementNode = null;
	Node writableNode;
	String nodeName = null;
	String valueText = null;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);
	    nodeName = elementNode.getNodeName();

	    writableNode = XMLHelper.getWritableNode(elementNode);
	    valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;
	    // LogUtil.debug(this.getClass(),"nodeName="+nodeName+", valueText="+valueText);

	    if ("accountID".equals(nodeName) && valueText != null) {
		this.accountID = new Integer(valueText);
	    } else if ("adjustmentStatus".equals(nodeName) && valueText != null) {
		this.adjustmentStatus = valueText;
	    } else if ("adjustmentTypeCode".equals(nodeName) && valueText != null) {
		this.adjustmentTypeCode = valueText;
	    } else if ("debitAdjustment".equals(nodeName) && valueText != null) {
		this.debitAdjustment = Boolean.valueOf(valueText).booleanValue();
	    } else if ("description".equals(nodeName) && valueText != null) {
		this.description = valueText;
	    }
	}

	// LogUtil.debug(this.getClass(),"adjustmentTypeCode="+this.adjustmentTypeCode+", debitAdjustment="+this.debitAdjustment);
	// Must have an adjustment type code
	if (this.adjustmentTypeCode == null) {
	    throw new SystemException("Failed to create AdjustmentTypeVO from XML data.  The adjustmentTypeCode is null.");
	}

    }

    public Integer getAccountID() {
	return accountID;
    }

    public MembershipAccountVO getAdjustmentAccount() throws RemoteException {
	if (this.account == null && this.accountID != null && this.accountID.intValue() > 0) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.account = refMgr.getMembershipAccount(this.accountID);
	}
	return this.account;
    }

    public String getAdjustmentStatus() {
	return adjustmentStatus;
    }

    public String getAdjustmentTypeCode() {
	return adjustmentTypeCode;
    }

    public String getDescription() {
	return description;
    }

    public boolean isDebitAdjustment() {
	return this.debitAdjustment;
    }

    public boolean isCreditAdjustment() {
	return !isDebitAdjustment();
    }

    public void setAccountID(Integer accountID) throws RemoteException {
	checkReadOnly();
	this.accountID = accountID;
    }

    public void setAdjustmentStatus(String adjustmentStatus) throws RemoteException {
	checkReadOnly();
	this.adjustmentStatus = adjustmentStatus;
    }

    public void setAdjustmentTypeCode(String adjustmentTypeCode) throws RemoteException {
	checkReadOnly();
	this.adjustmentTypeCode = adjustmentTypeCode;
    }

    public void setDebitAdjustment(boolean debit) {
	this.debitAdjustment = debit;
    }

    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    public AdjustmentTypeVO copy() {
	AdjustmentTypeVO adjTypeVO = new AdjustmentTypeVO(this.adjustmentTypeCode, this.adjustmentStatus, this.accountID, this.debitAdjustment, this.description);
	return adjTypeVO;
    }

    public String toString() {
	StringBuffer sb = new StringBuffer(this.adjustmentTypeCode);
	sb.append(", ");
	sb.append(this.adjustmentStatus);
	sb.append(", ");
	sb.append(this.debitAdjustment);
	sb.append(", ");
	sb.append(this.accountID);
	return sb.toString();
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("accountID", this.accountID);
	cw.writeAttribute("adjustmentStatus", this.adjustmentStatus);
	cw.writeAttribute("adjustmentTypeCode", this.adjustmentTypeCode);
	cw.writeAttribute("debitAdjustment", this.debitAdjustment);
	cw.writeAttribute("description", this.description);
	cw.endClass();
    }

}
