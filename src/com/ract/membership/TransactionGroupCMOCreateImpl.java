package com.ract.membership;

import java.rmi.RemoteException;

import com.ract.user.User;
import com.ract.util.DateTime;

public class TransactionGroupCMOCreateImpl extends TransactionGroupCreateImpl {

    public TransactionGroupCMOCreateImpl(User user, String transactionTypeCode, String joinReasonCode, DateTime effectiveDate, String profileCode) throws RemoteException {
	super(user, transactionTypeCode, joinReasonCode, effectiveDate, profileCode);
    }

}
