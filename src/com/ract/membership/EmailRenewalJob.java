package com.ract.membership;

import java.util.Hashtable;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.MailMgr;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.util.LogUtil;

public class EmailRenewalJob implements Job {

    public EmailRenewalJob() {
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {

	// get the job data map
	JobDataMap dataMap = context.getJobDetail().getJobDataMap();

	RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

	String renewalFile = (String) dataMap.get("renewalFile");
	Boolean updateDoc = (Boolean) dataMap.get("updateDoc");
	Boolean testEmails = (Boolean) dataMap.get("testEmails");

	try {
	    Hashtable<String, Integer> counts = renewalMgr.emailRenewals(renewalFile, updateDoc, testEmails);

	    Integer emailCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_EMAIL);
	    Integer printCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_PRINT);
	    Integer updateCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_EMAIL_DOCUMENT_UPDATES);
	    Integer noDeliveryCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_NO_DELIVERY_METHOD);
	    Integer primaryLineCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_PRIMARY_LINES);
	    Integer validLineCount = counts.get(RenewalMgrBean.COUNT_RENEWAL_PRIMARY_LINES_VALID);
	    Integer testEmailsSent = counts.get(RenewalMgrBean.COUNT_RENEWAL_TEST_EMAILS_SENT);

	    String returnEmail = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILRETURNPATH");

	    StringBuilder body = new StringBuilder();
	    body.append("Records Processed: ");
	    body.append(primaryLineCount);
	    body.append("\nValid records processed: ");
	    body.append(validLineCount);
	    body.append("\nTest emails sent: ");
	    body.append(testEmailsSent);
	    body.append("\nDocuments updated and emails sent: ");
	    body.append(updateCount);
	    body.append("\nRecords with no delivery preference: ");
	    body.append(noDeliveryCount);
	    body.append("\nEmail delivery preference: ");
	    body.append(emailCount);
	    body.append("\nPrint delivery preference: ");
	    body.append(printCount);

	    MailMessage message = new MailMessage();
	    message.setRecipient(returnEmail);
	    message.setMessage(body.toString());
	    message.setSubject("Email renewals complete");
	    message.setFrom(returnEmail);

	    MailMgr mailMgr = CommonEJBHelper.getMailMgr();
	    mailMgr.sendMail(message);

	} catch (Exception e) {

	    try {
		String returnEmail = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILRETURNPATH");
		MailMessage message = new MailMessage();
		message.setRecipient(returnEmail);
		message.setMessage("Email renewals failed: " + e);
		message.setSubject("Email renewals failed");
		message.setFrom(returnEmail);

		MailMgr mailMgr = CommonEJBHelper.getMailMgr();
		mailMgr.sendMail(message);

	    } catch (Exception ex) {
		LogUtil.fatal(this.getClass(), "===================================================");
		LogUtil.fatal(this.getClass(), "Email renewals failed, unable to send alert email! " + ex);
		LogUtil.fatal(this.getClass(), "===================================================");
	    }

	}

    }
}
