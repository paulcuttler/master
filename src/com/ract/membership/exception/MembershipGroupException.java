package com.ract.membership.exception;

import java.rmi.RemoteException;

/**
 * An exception class to tag problems with loading membership group details so
 * that they can be handled nicer.
 */
public class MembershipGroupException extends RemoteException {
    public MembershipGroupException() {
	super();
    }

    public MembershipGroupException(String msg) {
	super(msg);
    }

    public MembershipGroupException(Throwable chainedException) {
	super("", chainedException);
    }

    public MembershipGroupException(String msg, Throwable chainedException) {
	super(msg, chainedException);
    }
}