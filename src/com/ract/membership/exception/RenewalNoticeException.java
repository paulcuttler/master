package com.ract.membership.exception;

import com.ract.common.GenericException;

/**
 * Custom renewal exception.
 * 
 * @author GWigg
 * @version 1.0
 */

public class RenewalNoticeException extends GenericException {

    public RenewalNoticeException() {
    }

    public RenewalNoticeException(String message) {
	super(message);
    }

    public RenewalNoticeException(String msg, Exception chainedException) {
	super(msg, chainedException);
    }
}