package com.ract.membership;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.util.DateTime;
import com.ract.util.StringUtil;
import com.ract.web.campaign.Campaign;
import com.ract.web.campaign.CampaignEntry;
import com.ract.web.campaign.CampaignMgrLocal;
import com.ract.web.security.User;
import com.ract.web.security.UserSecurityMgrLocal;

public class DrawCampaignAction extends ActionSupport implements Preparable {

    @InjectEJB(name = "CampaignMgr")
    CampaignMgrLocal campaignMgrLocal;

    @InjectEJB(name = "UserSecurityMgr")
    UserSecurityMgrLocal userSecurityMgrLocal;

    private List<Campaign> campaignList;
    private List<Campaign> drawnCampaignList;
    private List<String> winnerList;

    private String ref;
    private boolean clear;

    private Integer numWinners;

    public void prepare() {
	campaignList = campaignMgrLocal.getUndrawnCampaigns();
	drawnCampaignList = campaignMgrLocal.getDrawnCampaigns();
    }

    public String execute() throws Exception {
	if (ref != null) {

	    if (clear) {
		// clear winners & drawn date
		Campaign campaign = campaignMgrLocal.retrieveCampaign(ref);
		campaign.setDrawnDate(null);
		campaign.setWinners(null);
		campaignMgrLocal.updateCampaign(campaign);
	    }

	    if (numWinners != null && numWinners > 0) {
		List<CampaignEntry> entries = campaignMgrLocal.getEntriesByReference(ref);
		List<Integer> clientNumbers = new ArrayList<Integer>();

		// get unique client numbers
		for (CampaignEntry c : entries) {
		    if (c.getClientNumber() != null && !clientNumbers.contains(c.getClientNumber())) {
			clientNumbers.add(c.getClientNumber());
		    }
		}

		winnerList = new ArrayList<String>();

		User webUser = null;
		Integer clientNo = null;

		// pick winners
		while (winnerList.size() <= numWinners && winnerList.size() <= clientNumbers.size()) {
		    int index = new Random().nextInt(clientNumbers.size()); // random
									    // index

		    clientNo = clientNumbers.remove(index);
		    webUser = userSecurityMgrLocal.getUserByClientNumber(clientNo);

		    // valid web account?
		    if (webUser != null) {
			winnerList.add(clientNo.toString());
		    }
		}

		// update winners & drawn date
		Campaign campaign = campaignMgrLocal.retrieveCampaign(ref);
		campaign.setDrawnDate(new DateTime());
		campaign.setWinners(StringUtil.join(winnerList, ','));
		campaignMgrLocal.updateCampaign(campaign);
	    }

	    // update campaign lists
	    campaignList = campaignMgrLocal.getUndrawnCampaigns();
	    drawnCampaignList = campaignMgrLocal.getDrawnCampaigns();
	}

	return SUCCESS;
    }

    public List<Campaign> getCampaignList() {
	return campaignList;
    }

    public void setCampaignList(List<Campaign> campaignList) {
	this.campaignList = campaignList;
    }

    public String getRef() {
	return ref;
    }

    public void setRef(String ref) {
	this.ref = ref;
    }

    public Integer getNumWinners() {
	return numWinners;
    }

    public void setNumWinners(Integer numWinners) {
	this.numWinners = numWinners;
    }

    public List<String> getWinnerList() {
	return winnerList;
    }

    public void setWinnerList(List<String> winnerList) {
	this.winnerList = winnerList;
    }

    public List<Campaign> getDrawnCampaignList() {
	return drawnCampaignList;
    }

    public void setDrawnCampaignList(List<Campaign> drawnCampaignList) {
	this.drawnCampaignList = drawnCampaignList;
    }

    public boolean isClear() {
	return clear;
    }

    public void setClear(boolean clear) {
	this.clear = clear;
    }
}
