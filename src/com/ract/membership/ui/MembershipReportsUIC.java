package com.ract.membership.ui;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;

import com.ract.common.ApplicationServlet;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.MailMgr;
import com.ract.common.PrintForm;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.extracts.ExtractHelper;
import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportGenerator;
import com.ract.common.reporting.ReportRegister;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.common.ui.CommonUIConstants;
import com.ract.membership.CardMgr;
import com.ract.membership.DeliveryRoundJob;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipGroupDetailVO;
import com.ract.membership.MembershipGroupVO;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipProfileVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.RenewalBatchVO;
import com.ract.membership.RenewalMgr;
import com.ract.membership.RenewalNoticesCronJob;
import com.ract.membership.RenewalNoticesJob;
import com.ract.membership.TransactionGroup;
import com.ract.membership.reporting.BranchSalesReportRequest;
import com.ract.membership.reporting.MembershipAdviceRequest;
import com.ract.membership.reporting.MembershipDDRequest;
import com.ract.membership.reporting.MembershipMovementReportRequest;
import com.ract.membership.reporting.MembershipQuoteRequest;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.PrintUtil;

/**
 * Reports UIC controls the creation of standard reports including
 * <ol>
 * <li>advices
 * <li>quotes
 * <li>statistical reports
 * </ol>
 * 
 * 
 * @author dgk,jyh
 * @created 31 July 2002
 * @version 1.0
 */

public class MembershipReportsUIC extends ApplicationServlet {
	private DataSource dataSource = null;

	private static final int MAX_MEMBERS_ON_FRONT = 4;

	private final static String NL = "";

	/**
	 * flag to control printing for DEVELOPER use only
	 */
	private final static boolean PRINTING_ENABLED = true;

	/**
	 * @todo move into system parameter a4 and manual feed at the time of writing
	 */
	private final static String REPORT_A4_PORTRAIT_MANUAL_TRAY = "MEMPLUS";

	private final static String USER_REPORT_DEFAULT = "Guest";

	private String reportCache;

	// private static String SEPARATOR = "|";

	/**
	 * Initialize global variables
	 * 
	 * @exception ServletException Description of the Exception
	 */
	public void init() throws ServletException {
		try {
			CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
			reportCache = comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_REPORT_CACHE);
		} catch (RemoteException e) {
			throw new ServletException("Unable to load the report cache. " + e.toString());
		}
	}

	/**
	 * Clean up resources
	 */
	public void destroy() {
	}

	/* ============================Extracts================== */

	public void handleEvent_Menu_BranchSalesReport(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		Collection salesBranchList = CommonEJBHelper.getCommonMgr().getCachedSalesBranchList();
		request.setAttribute("salesBranchList", salesBranchList);
		forwardRequest(request, response, MembershipUIConstants.PAGE_MEMBERSHIP_BRANCH_SALES_REPORT_PARAMETERS);
	}

	/**
	 * Handle the Maintain extract event Forward the request to the Maintain extract List page to start the maintenance process
	 */
	public void handleEvent_selectRoadsideExtracts(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		SortedSet extractList = new TreeSet();
		try {
			extractList = CommonEJBHelper.getCommonMgr().getAllRoadSideExtracts();
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}

		//
		// Add some things to the request;
		//
		request.setAttribute("showAddButton", "No");
		request.setAttribute("useFullWidth", "Yes");
		//
		request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, extractList);
		request.getSession().setAttribute("pageTitle", "Roadside Extracts");
		request.getSession().setAttribute("ExtractOrReport", "RoadsideExtract");
		request.getSession().setAttribute("buttonMessage", "Do Not Show");
		request.getSession().setAttribute("createEvent", "");
		request.getSession().setAttribute("selectEvent", "getExtractParameters");
		request.getSession().setAttribute("codeName", "extractCode");
		request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MembershipReportsUIC);
		request.getSession().setAttribute("origCode", "");

		forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
	}

	public void handleEvent_getExtractParameters(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String whatExtract = request.getParameter("extractCode");

		Hashtable extractParams = CommonEJBHelper.getCommonMgr().getExtractParameters(whatExtract);

		request.getSession().setAttribute("params", extractParams);

		forwardRequest(request, response, MembershipUIConstants.PAGE_ROADSIDE_EXTRACTS);
	}

	public void handleEvent_RoadsideExtract_okButton(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		Hashtable extractParams = ExtractHelper.getSqlParameters(request);

		HttpServletRequest req = ExtractHelper.launchJob(request, extractParams);

		this.forwardRequest(req, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_RoadsideExtract_cancelButton(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	/**
	 * cancel renewal statistics report
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_MembershipRenewalStatisticsReport_Cancel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setAttribute("heading", "Report Cancelled");
		String cancelText = "You have chosen to cancel the Membership Renewal Statistics Report. \n" + "You may now continue with any other process.";
		request.setAttribute("message", cancelText);

		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	public void handleEvent_Menu_MembershipMovementReport(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		forwardRequest(request, response, MembershipUIConstants.PAGE_MembershipMovementReportParameters);
	}

	/**
	 * The OK button on the Membership Movement Report Parameter page was clicked. Run the Membership Movement Report with the given parameters.
	 */
	public void handleEvent_MembershipMovementReportParameters_okButton(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// retrieve the parameters
		String fromDateStr = request.getParameter(MembershipMovementReportRequest.PARAMETER_FROM_DATE);
		String toDateStr = request.getParameter(MembershipMovementReportRequest.PARAMETER_TO_DATE);
		String salesBranchListStr = request.getParameter(MembershipMovementReportRequest.PARAMETER_SALES_BRANCH_LIST);
		// Validate the parameters
		DateTime fromDate = null;
		DateTime toDate = null;

		try {
			fromDate = new DateTime(fromDateStr);
		} catch (ParseException pe) {
			throw new ValidationException("The from date '" + fromDateStr + "' is not a valid date.");
		}

		try {
			toDate = new DateTime(toDateStr);
		} catch (ParseException pe) {
			throw new ValidationException("The to date '" + toDateStr + "' is not a valid date.");
		}

		if (fromDate.after(toDate)) {
			throw new ValidationException("The from date must be on or before the to date.");
		}

		if (salesBranchListStr == null || salesBranchListStr.trim().length() < 1) {
			throw new ValidationException("Atleast one sales branch must be selected.");
		}

		UserSession userSession = UserSession.getUserSession(request.getSession());

		// Stow the parameters in a parameter list
		Hashtable parameterList = new Hashtable();
		parameterList.put(MembershipMovementReportRequest.PARAMETER_FROM_DATE, fromDate.formatShortDate());
		parameterList.put(MembershipMovementReportRequest.PARAMETER_TO_DATE, toDate.formatShortDate());
		// salesBranchListStr += ",0";
		parameterList.put(MembershipMovementReportRequest.PARAMETER_SALES_BRANCH_LIST, salesBranchListStr);
		parameterList.put(MembershipMovementReportRequest.PARAMETER_USERID, userSession.getUserId());
		parameterList.put("createDate", DateUtil.formatShortDate(new DateTime()));
		// Retrieve the delivery method parameters and run the report.

		// runReport(request, response, new MembershipMovementReportRequest(),
		// parameterList);

	}

	private void sendReport(String reportPath, String reportName, String messageBody, String emailAddress) throws RemoteException {
		MailMgr mailMgr = CommonEJBHelper.getMailMgr();

		Hashtable files = new Hashtable();
		files.put(reportName, reportPath);

		if (emailAddress == null) {
			throw new SystemException("Failed to send report by email. The email address was not specified!");
		}
		LogUtil.debug(this.getClass(), "email = " + emailAddress.trim());

		MailMessage message = new MailMessage();
		message.setRecipient(emailAddress);
		message.setSubject(reportName);
		message.setMessage(messageBody.toString());
		message.setFiles(files);

		mailMgr.sendMail(message);
	}

	/**
	 * extract filename from fullpath
	 */
	private String getFileName(String outPath) {
		File f = new File(outPath);
		return f.getName();
	}

	/**
	 * construct the Journeys Magazine extract
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_MotorNews_DoExtract(HttpServletRequest request, HttpServletResponse response) throws Exception {

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String mailTo = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.EMAIL_MOTOR_NEWS);

		String dateString = request.getParameter("runDate");
		String hoursString = request.getParameter("runHours");
		String minsString = request.getParameter("runMins");
		DateTime runTime = new DateTime(dateString);

		GregorianCalendar gc = new GregorianCalendar();
		gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(hoursString), Integer.parseInt(minsString), 0);

		ScheduleMgr sMgr = CommonEJBHelper.getScheduleMgr();

		JobDetail jobDetail = new JobDetail("motornews", Scheduler.DEFAULT_GROUP, DeliveryRoundJob.class);
		jobDetail.setDescription("Journeys Magazine Extract");

		SimpleTrigger trigger = new SimpleTrigger("motornewstrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);

		JobDataMap dataMap = jobDetail.getJobDataMap();
		// should be constants
		dataMap.put("annualReportFile", request.getParameter("annualReportFileName"));
		dataMap.put("motorNewsFile", request.getParameter("motorNewsFileName"));
		dataMap.put("accessMembersFile", request.getParameter("accessMembersFileName"));
		dataMap.put("errorFile", request.getParameter("errorFileName"));
		dataMap.put("emailAddress", mailTo);
		dataMap.put("createDateTime", DateUtil.formatLongDate(new DateTime()));
		String userId = this.getUserSession(request).getUserId();
		dataMap.put("userId", userId);
		String messageText = null;

		try {

			sMgr.scheduleJob(jobDetail, trigger);

			request.setAttribute("heading", "Journeys Magazine Extract Scheduled");
			messageText = "The Journeys Magazine extract has been scheduled to run at" + "<br/>" + (new DateTime(gc.getTime())).formatLongDate();
			request.setAttribute("message", messageText);
		} catch (Exception e) {
			request.setAttribute("heading", "WARNING: Journeys Magazine Extract not scheduled");
			messageText = "The system was unable to schedule the Journeys Magazine extract: <br/>" + e;
			request.setAttribute("message", messageText);
		}
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	/**
	 * cancel the motor news extract
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	// public void handleEvent_MotorNews_Cancel(HttpServletRequest request,
	// HttpServletResponse response)
	// throws
	// Exception
	// {
	// request.setAttribute("heading", "Motor News Extract Cancelled");
	// String cancelText =
	// "You have chosen to cancel the Motor News Extract. \n"
	// + "You may now continue with any other process.";
	// request.setAttribute("message", cancelText);
	// this.forwardRequest(request, response,
	// CommonUIConstants.PAGE_ConfirmProcess);
	// }

	/**
	 * cancel the branch sales report
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_BranchSales_Cancel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setAttribute("heading", "Report Cancelled");
		String cancelText = "The membership Branch Sales report has been cancelled at your request.";
		request.setAttribute("message", cancelText);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	/**
	 * construct the branch sales report
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_BranchSales_DoReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String periodStartDateString = request.getParameter(BranchSalesReportRequest.PARAMETER_PERIOD_START_DATE);
		String periodEndDateString = request.getParameter(BranchSalesReportRequest.PARAMETER_PERIOD_END_DATE);
		String salesBranchList = request.getParameter(BranchSalesReportRequest.PARAMETER_SALES_BRANCH_LIST);

		String excludeWeb = request.getParameter(BranchSalesReportRequest.PARAMETER_SALES_EXCLUDE_WEB);
		boolean excWeb = Boolean.valueOf(excludeWeb != null ? excludeWeb.equalsIgnoreCase("Y") : false);
		String transactionSalesBranch = request.getParameter(BranchSalesReportRequest.PARAMETER_SALES_GROUP_BY_TRANSACTION_SALES_BRANCH);
		boolean grpTxSalesBranch = Boolean.valueOf(transactionSalesBranch != null ? transactionSalesBranch.equalsIgnoreCase("Y") : false);

		outputRequestParameters(request);
		outputRequestAttributes(request);
		outputSessionAttributes(request);

		if (salesBranchList == null || salesBranchList.equals("")) {
			throw new SystemException("No sales branch has been specified to report by.");
		}

		String selectedBranchListStr = request.getParameter(BranchSalesReportRequest.PARAMETER_SALES_BRANCH_LIST);

		Hashtable parameterList = new Hashtable();
		parameterList.put(BranchSalesReportRequest.PARAMETER_PERIOD_START_DATE, periodStartDateString);
		parameterList.put(BranchSalesReportRequest.PARAMETER_PERIOD_END_DATE, periodEndDateString);
		parameterList.put(BranchSalesReportRequest.PARAMETER_SALES_BRANCH_LIST, selectedBranchListStr);
		UserSession session = this.getUserSession(request);
		String userName = session.getUser().getUsername();
		String printGroup = session.getUser().getPrinterGroup();

		Hashtable qParameterList = new Hashtable();
		qParameterList.put(BranchSalesReportRequest.PARAMETER_SALES_EXCLUDE_WEB, excWeb);
		qParameterList.put(BranchSalesReportRequest.PARAMETER_SALES_GROUP_BY_TRANSACTION_SALES_BRANCH, grpTxSalesBranch);
		qParameterList.put(BranchSalesReportRequest.PARAMETER_PERIOD_START_DATE, periodStartDateString);
		qParameterList.put(BranchSalesReportRequest.PARAMETER_PERIOD_END_DATE, periodEndDateString);
		qParameterList.put(BranchSalesReportRequest.PARAMETER_SALES_BRANCH_LIST, selectedBranchListStr);

		BranchSalesReportRequest rep = new BranchSalesReportRequest(null, parameterList, qParameterList, null);
		rep.initialSetting(userName, printGroup);

		response.setContentType(rep.getMIMEType());
		BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());
		rep.setOutputStream(outStream);
		String reportKeyName = ReportRegister.addReport(rep);
		rep.setReportKeyName(reportKeyName);
		rep.constructURL();
		rep.addParameter("URL", rep.getReportDataURL());
		rep.setReportWorkspace("Ract");
		new ReportGenerator().runReport(rep);
		outStream.flush();
		outStream.close();
		ReportRegister.removeReport(reportKeyName);
	}

	/**
	 * Handle the Motor News Extract menu option. Opens a jsp for the entry of run parameters.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_Menu_MotorNewsExtract(HttpServletRequest request, HttpServletResponse response) throws Exception {
		forwardRequest(request, response, MembershipUIConstants.PAGE_MotorNewsExtract);
	}

	/**
	 * menu option for discretionary discounts
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_Menu_BranchDiscountReport(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		forwardRequest(request, response, MembershipUIConstants.PAGE_BranchDiscountReportParameters);
	}

	/**
	 * cancel the capitation fees report
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_CapitationFees_Cancel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setAttribute("heading", "Report Cancelled");
		String cancelText = "The Membership Capitation Fees report has been cancelled at your request. ";
		request.setAttribute("message", cancelText);

		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

	}

	/**
	 * print a membership quote report
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_printMembershipQuote(HttpServletRequest request, HttpServletResponse response) throws RemoteException {
		LogUtil.log(this.getClass(), "print mem quote");
		// String transGroupKey = request.getParameter("transactionGroupKey");
		String quoteNumber = request.getParameter("quoteNumber");
		TransactionGroup transGroup = null;
		UserSession session = this.getUserSession(request);
		String userName = session.getUser().getUsername();
		String printGroup = session.getUser().getPrinterGroup();
		Integer quoteNo;
		if (quoteNumber == null) {
			throw new ValidationException("Quote number is empty");
		}
		quoteNo = new Integer(quoteNumber);

		try {
			ReportRequestBase rep = new MembershipQuoteRequest(userName, printGroup, quoteNo, false);
			OutputStream fStream = FileUtil.openStreamForWriting(rep.getDestinationFileName(), true);
			rep.setOutputStream(fStream);
			String reportKeyName = ReportRegister.addReport(rep);
			rep.setReportKeyName(reportKeyName);
			rep.constructURL();
			rep.addParameter("URL", rep.getReportDataURL());
			new ReportGenerator().runReport(rep);
			viewPrintingConfirmation(request, response);
			ReportRegister.removeReport(reportKeyName);
		} catch (Exception e) {
			throw new SystemException("Unable to print quote. ", e);
		}
	}

	/**
	 * Print membership advice from the Membership Transaction complete screen
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception Exception Description of the Exception
	 */
	public void handleEvent_transactionComplete_PrintAdvices(HttpServletRequest request, HttpServletResponse response) throws SystemException {
		final String DELIMITER = ",";
		String membershipIdList = request.getParameter("membershipIdList");
		Integer membershipId = null;
		try {
			// MembershipHome memHome = MembershipEJBHelper.getMembershipHome();
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			ArrayList membershipList = new ArrayList();
			// process list of membership ids
			if (membershipIdList.indexOf(DELIMITER) == -1) {
				membershipList.add(new Integer(membershipIdList)); // only one
			} else {
				// StringTokenizer membershipTokenizer = new
				// StringTokenizer(membershipIdList,DELIMITER);
				String membershipTokenizer[] = membershipIdList.split(DELIMITER);
				// while(membershipTokenizer.hasMoreTokens())
				for (int i = 0; i < membershipTokenizer.length; i++) {
					membershipList.add(new Integer(membershipTokenizer[i]));
				}
			}
			// print list of advices
			for (int x = 0; x < membershipList.size(); x++) {
				membershipId = (Integer) membershipList.get(x);
				// Membership mem = memHome.findByPrimaryKey(membershipId);
				// MembershipVO memVO = mem.getVO();
				MembershipVO memVO = membershipMgr.getMembership(membershipId);
				printMembershipAdvice(request, response, memVO);

			}
			viewPrintingConfirmation(request, response);

		} catch (Exception e) {
			throw new SystemException("Unable to print membership transaction advice. ", e);
		}
	}

	private void printMembershipAdvice(HttpServletRequest request, HttpServletResponse response, MembershipVO memVO) throws RemoteException, ServletException {
		LogUtil.log(this.getClass(), "printMembershipAdvice start");
		User user = this.getUserSession(request).getUser();
		String printGroup = this.getUserSession(request).getUser().getPrinterGroup();
		try {
			ReportRequestBase rep = new MembershipAdviceRequest(user, printGroup, memVO, false);
			OutputStream fStream = FileUtil.openStreamForWriting(rep.getDestinationFileName(), true);
			rep.setOutputStream(fStream);
			String reportKeyName = ReportRegister.addReport(rep);
			rep.setReportKeyName(reportKeyName);
			rep.constructURL();
			rep.addParameter("URL", rep.getReportDataURL());

			new ReportGenerator().runReport(rep);
			ReportRegister.removeReport(reportKeyName);
		} catch (Exception rex) {
			rex.printStackTrace();
			throw new RemoteException(rex + "");
		}
		LogUtil.log(this.getClass(), "printMembershipAdvice end");
	}

	/**
	 * Get the report name in a standard way.
	 * 
	 * @param reportName String
	 * @param reportDirectory String
	 * @return String
	 */
	private String getReportName(String reportName, String reportDirectory) {
		int fileTime = new DateTime().getMillisecondsFromMidnight();
		StringBuffer fileName = new StringBuffer();
		if (reportDirectory != null) {
			fileName.append(reportDirectory);
			fileName.append("/");
		}
		fileName.append(reportName);
		fileName.append(fileTime);
		fileName.append(".");
		fileName.append(FileUtil.EXTENSION_PDF);
		return fileName.toString();
	}

	/**
	 * print the Membership advice form.
	 * 
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 * @exception ValidationException Description of the Exception
	 * @exception RemoteException Description of the Exception
	 */
	public void handleEvent_printMembershipAdvice(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		MembershipVO memVO = null;
		memVO = (MembershipVO) request.getAttribute("membershipVO");
		if (memVO == null) {
			String memID = (String) request.getParameter("membershipID");
			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
			memVO = memMgr.getMembership(new Integer(memID));
		}
		printMembershipAdvice(request, response, memVO);

		viewPrintingConfirmation(request, response);
		/*
		 * User us = this.getUserSession(request).getUser(); String pcIdentifier = this.getUserSession(request).getIp(); try { OPALServerSocket.requestOpal(memVO.getClientNumber(), "MEM", memVO.getMembershipID(), "MembershipId", "MEM", memVO.getMembershipTypeCode(), pcIdentifier, us.getSalesBranchCode(), us.getUserID()); } catch (Exception e) { // TODO Auto-generated catch block e.printStackTrace(); // do nothing }
		 */
	}

	public void handleEvent_printMembershipAccountDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		String membershipId = request.getParameter("membershipId");

		User user = this.getUserSession(request).getUser();
		String printGroup = this.getUserSession(request).getUser().getPrinterGroup();

		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		MembershipVO memVO = membershipMgr.getMembership(new Integer(membershipId));

		ReportRequestBase rep = null;
		try {

			rep = new MembershipDDRequest(user, printGroup, memVO, false, null, false);
			OutputStream fStream = FileUtil.openStreamForWriting(rep.getDestinationFileName(), true);
			rep.setOutputStream(fStream);
			rep.setReportKeyName(ReportRegister.addReport(rep));
			rep.constructURL();
			rep.addParameter("URL", rep.getReportDataURL());

			new ReportGenerator().runReport(rep);
			ReportRegister.removeReport(rep.getReportKeyName());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ServletException(ex);
		}

		viewPrintingConfirmation(request, response);
	}

	/**
	 * Print a direct debit schedule.
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException
	 * @throws ValidationException
	 * @throws RemoteException
	 */
	public void handleEvent_printMembershipDirectDebitSchedule(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		User user = this.getUserSession(request).getUser();
		String printGroup = this.getUserSession(request).getUser().getPrinterGroup();
		String clientNumberString = (String) request.getParameter("clientNumber");
		String directDebitScheduleIdString = (String) request.getParameter("directDebitScheduleID");
		Integer directDebitScheduleId = new Integer(directDebitScheduleIdString);
		// MembershipHome memHome = MembershipEJBHelper.getMembershipHome();
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		Collection membershipList = null;

		membershipList = membershipMgr.findMembershipByClientNumber(new Integer(clientNumberString));

		MembershipVO memVO = null;
		Iterator membershipIterator = membershipList.iterator();
		// only expect one
		if (membershipIterator.hasNext()) {
			memVO = (MembershipVO) membershipIterator.next();
		}

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

		// get schedule
		DirectDebitSchedule ddSchedule = null;
		if (directDebitScheduleId != null) {
			try {
				ddSchedule = paymentMgr.getDirectDebitSchedule(directDebitScheduleId);
			} catch (PaymentException ex) {
				throw new RemoteException("Unable to get direct debit schedule.", ex);
			}
		}

		if (ddSchedule == null) {
			throw new RemoteException("No schedule found to print with id '" + directDebitScheduleId + "'.");
		}

		ReportRequestBase rep = null;
		try {

			rep = new MembershipDDRequest(user, printGroup, memVO, true, ddSchedule, false);
			OutputStream fStream = FileUtil.openStreamForWriting(rep.getDestinationFileName(), true);
			rep.setOutputStream(fStream);
			rep.setReportKeyName(ReportRegister.addReport(rep));
			rep.constructURL();
			rep.addParameter("URL", rep.getReportDataURL());

			new ReportGenerator().runReport(rep);
			ReportRegister.removeReport(rep.getReportKeyName());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ServletException(ex);
		}
		viewPrintingConfirmation(request, response);
	}

	/**
	 * View page with print confirmation
	 * 
	 * @param reportName Description of the Parameter
	 * @param request Description of the Parameter
	 * @param response Description of the Parameter
	 * @exception ServletException Description of the Exception
	 */
	private void viewPrintingConfirmation(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String heading = "Printing Complete";
		String text = "The document(s) have been printed successfully.";
		request.setAttribute("heading", heading);
		request.setAttribute("message", text);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmWindow);
	}

	private final String KEY_RENEWAL_NOTICE_REPORT_REQUEST = "Renewal Advice Request";

	private final String KEY_RENEWAL_NOTICE_DIRECT_DEBIT_SCHEDULE = "Renewal Schedule";

	/**
	 * get the report identifier
	 */
	private String getReportID(UserSession user) {
		String repId = user.getUserId() + (new DateTime()).getSecondsFromMidnight();
		return repId;
	}

	public void handleEvent_Menu_CardExtract(HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.forwardRequest(request, response, MembershipUIConstants.PAGE_ProduceCards);
	}

	public void handleEvent_Menu_ProduceRenewalNotices(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		// LogUtil.log(this.getClass(),"handleEvent_Menu_ProduceRenewalNotices 1");
		RenewalMgr renMgr = MembershipEJBHelper.getRenewalMgr();
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		RenewalBatchVO lastBatch = null;
		DateTime previousLast = null;
		boolean alreadyRun = false;
		DateTime lastDate = null;
		DateTime firstDate = null;
		Vector batchList = renMgr.getAllRenewalBatches();
		// LogUtil.log(this.getClass(),"handleEvent_Menu_ProduceRenewalNotices 2");
		if (batchList != null && !batchList.isEmpty()) {
			lastBatch = (RenewalBatchVO) batchList.elementAt(0);
			previousLast = lastBatch.getLastDate();
			if (lastBatch.getRunDate().onOrAfterDay(new DateTime())) {
				alreadyRun = true;
			}
			try {
				lastDate = previousLast.add(new Interval(0, 0, 14, 0, 0, 0, 0));
			} catch (Exception e) {
				throw new SystemException(e);
			}
			// if the previous batch is incomplete, run over the same dates to
			// pick up any that
			// were not picked up in the previous (incomplete) run
			if (lastBatch.getRunStatus().trim().equalsIgnoreCase("Begun")) {
				firstDate = lastBatch.getFirstDate();
			} else {
				try {
					firstDate = previousLast.add(new Interval(0, 0, 1, 0, 0, 0, 0));
				} catch (Exception e) {
					throw new SystemException(e);
				}
			}
		}
		request.setAttribute("batchList", batchList);
		request.setAttribute("alreadyRun", new Boolean(alreadyRun));
		request.setAttribute("firstDate", firstDate);
		request.setAttribute("lastDate", lastDate);

		// LogUtil.log(this.getClass(),"handleEvent_Menu_ProduceRenewalNotices 3");

		String emailTo = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.EMAIL_MEMBERSHIP_RENEWAL_RUN);
		request.setAttribute("emailTo", emailTo);
		try {
			String ccTo = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEMBERSHIP_RENEWAL_RUN_CCTO);
			request.setAttribute("ccTo", ccTo);
		} catch (Exception e) {
			// do nothing. If not found, leave ccTo null
		}

		// See if scheduled for automatic run
		ScheduleMgr sMgr = CommonEJBHelper.getScheduleMgr();

		String jobNames[] = null;
		try {
			jobNames = sMgr.getJobNames(Scheduler.DEFAULT_GROUP);
		} catch (SchedulerException se) {
			throw new SystemException(se);
		}

		boolean autoRun = false;
		for (int x = 0; x < jobNames.length; x++) {
			if (jobNames[x].equals("renewalNoticesCronJob")) {
				autoRun = true;
			}
		}
		request.setAttribute("autoRun", autoRun + "");

		// LogUtil.log(this.getClass(),"handleEvent_Menu_ProduceRenewalNotices 1");
		forwardRequest(request, response, MembershipUIConstants.PAGE_ProduceRenewalNotices);
	}

	/**
	 * Produce renewal notices event
	 */
	public void handleEvent_RenewalNoticesRun(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String lastDateString = request.getParameter("lastDate");
		String runDateString = request.getParameter("runDate");
		String runHoursString = request.getParameter("runHours");
		String runMinsString = request.getParameter("runMins");
		String userId = this.getUserSession(request).getUserId();
		DateTime lastDate = null;

		ScheduleMgr sMgr = null;
		GregorianCalendar gc = null;
		try {
			lastDate = new DateTime(lastDateString);
			DateTime runTime = new DateTime(runDateString);
			gc = new GregorianCalendar();
			gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(runHoursString), Integer.parseInt(runMinsString), 0);
			sMgr = CommonEJBHelper.getScheduleMgr();
		} catch (Exception e) {
			throw new ValidationException("Error in dates: " + e);
		}

		JobDetail jobDetail = new JobDetail("renewalNoticesJob", Scheduler.DEFAULT_GROUP, RenewalNoticesJob.class);
		jobDetail.setDescription("Create Renewal Notices");

		SimpleTrigger trigger = new SimpleTrigger("renewalNoticesJobTrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, 0, 0);
		JobDataMap dataMap = jobDetail.getJobDataMap();
		dataMap.put("lastDate", DateUtil.formatShortDate(lastDate));
		dataMap.put("userId", userId);
		dataMap.put("createDateTime", DateUtil.formatLongDate(new DateTime()));
		String mText = null;
		try {
			// sched.start();
			sMgr.scheduleJob(jobDetail, trigger);
			mText = "The production of renewal notices will commence at " + runHoursString + ":" + runMinsString + " on " + runDateString + ", using the parameters you have set." + "\n\nThe resulting files will be emailed to the" + " specified addresses when the run is complete.";
		} catch (Exception e) {
			mText = "WARNING:  Your request has not been scheduled. \n " + e;

		}

		request.setAttribute("heading", "Produce Renewal Notices");
		request.setAttribute("message", mText);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	public void handleEvent_RenewalNoticesCancel(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		request.setAttribute("heading", "Renewal notices run cancelled");
		String cancelText = "You have chosen to cancel the extract of renewal notices. \n" + "You may now continue with any other process.";
		request.setAttribute("message", cancelText);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	public void handleEvent_RenewalNoticesViewPrevious(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String runNumberString = request.getParameter("runNumber");
		int runNumber = 0;
		try {
			runNumber = Integer.parseInt(runNumberString);
			RenewalMgr renMgr = MembershipEJBHelper.getRenewalMgr();
			RenewalBatchVO renBatch = renMgr.getBatchVO(runNumber, true);
			request.setAttribute("renewalBatch", renBatch);
			request.setAttribute("heading", "Batch Details");
			this.forwardRequest(request, response, MembershipUIConstants.PAGE_RenewalBatch);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save changes renewal run parameters. If there is an existing cron job, delete it If the auto run flag is set, create a new cron job with the updated parameters
	 */
	public void handleEvent_RenewalNoticesSaveParameters(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		String emailTo = request.getParameter("runEmailTo");
		String ccTo = request.getParameter("runCcTo");
		if (ccTo == null) {
			ccTo = "";
		}
		String autoRunString = request.getParameter("autoRun");
		String nextRunDateString = request.getParameter("nextRunDate");
		String runTimeHoursString = request.getParameter("runTimeHours");
		String runTimeMinsString = request.getParameter("runTimeMins");
		String runIntervalString = request.getParameter("runInterval");
		String mText = null;
		boolean autoRun;
		if (autoRunString == null) {
			autoRun = false;
		} else {
			autoRun = autoRunString.equals("on");

		}
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

		SystemParameterVO gnTableVO = new SystemParameterVO(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENNOTEMAIL");
		gnTableVO.setValue(emailTo);

		commonMgr.updateSystemParameter(gnTableVO);
		gnTableVO.getSystemParameterPK().setParameter("RENNOTCCTO");
		gnTableVO.setValue(ccTo);
		commonMgr.updateSystemParameter(gnTableVO);

		ScheduleMgr sMgr = null;

		sMgr = CommonEJBHelper.getScheduleMgr();
		// stop any existing cron job
		try {
			sMgr.deleteJob("renewalNoticesCronJob", Scheduler.DEFAULT_GROUP);
		} catch (SchedulerException se) {
			LogUtil.log(this.getClass(), "Unable to cancel old renewal run cron job: \n" + se);
		}
		if (autoRun) {
			// stsrt cron job if required
			GregorianCalendar gc = null;
			int repeatInterval = 0;
			try {
				DateTime runTime = new DateTime(nextRunDateString);
				gc = new GregorianCalendar();
				gc.set(runTime.getDateYear(), runTime.getMonthOfYear(), runTime.getDayOfMonth(), Integer.parseInt(runTimeHoursString), Integer.parseInt(runTimeMinsString), 0);
				// get repeat interval in milliseconds
				repeatInterval = Integer.parseInt(runIntervalString) * 86400000;
			} catch (Exception e) {
				throw new ValidationException("Error in dates: " + e);
			}
			JobDetail jobDetail = new JobDetail("renewalNoticesCronJob", Scheduler.DEFAULT_GROUP, RenewalNoticesCronJob.class);
			jobDetail.setDescription("Automatic Renewal Notice Production");
			SimpleTrigger trigger = new SimpleTrigger("renewalNoticesCronTrigger", Scheduler.DEFAULT_GROUP, gc.getTime(), null, SimpleTrigger.REPEAT_INDEFINITELY, repeatInterval);
			JobDataMap dataMap = jobDetail.getJobDataMap();
			dataMap.put("renewalInterval", Integer.parseInt(runIntervalString));
			dataMap.put("createDateTime", DateUtil.formatLongDate(new DateTime()));
			String userId = this.getUserSession(request).getUserId();
			dataMap.put("userId", userId);
			try {
				// sched.start();
				sMgr.scheduleJob(jobDetail, trigger);
				mText = "You have set renewal notices to run every " + runIntervalString + " days " + ", commencing on " + nextRunDateString + ".  Each run will commence at " + runTimeHoursString + ":" + runTimeMinsString + " and the resulting files will be emailed to the" + " specified addresses when the run is complete.";
			} catch (Exception e) {
				mText = "WARNING:  Your request has not been scheduled. \n " + e;
			}
		} // if autoRun
		else {
			mText = "Automatic production of renewal notices has been switched off.  Other parameters have been saved";
		}
		request.setAttribute("message", mText);
		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

	}

	/**
	 * Produce membership card event.
	 */
	public void handleEvent_produceCards(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		UserSession userSession = getUserSession(request);
		String[] productCodes = request.getParameterValues("productCodes");
		String mode = request.getParameter("mode");

		ArrayList productCodeList = null;
		if (productCodes != null && productCodes.length > 0) {
			productCodeList = new ArrayList(Arrays.asList(productCodes));
		} else {
			throw new ValidationException("No products have been selected.");
		}

		boolean eligibleLoyaltyScheme = "loyalty".equals(mode);
		boolean newAccessCardsOnly = "newAccess".equals(mode);

		// @todo should be more generic
		boolean cmoCardsOnly = "cmo".equals(mode);

		if (cmoCardsOnly) {
			productCodeList = new ArrayList();
			productCodeList.add(ProductVO.PRODUCT_CMO);
		}

		CardMgr cardMgr = MembershipEJBHelper.getCardMgr();
		Hashtable extractCounts = cardMgr.doCardExtract(productCodeList, userSession.getUserId(), eligibleLoyaltyScheme, newAccessCardsOnly, cmoCardsOnly);

		request.setAttribute("heading", "Membership Card Extract");

		if (extractCounts != null) {
			request.setAttribute("tableData", extractCounts);
		}
		String messageText = "Membership card extract is now complete.";
		request.setAttribute("message", messageText);

		this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	private String pricingOptionText(MembershipVO memVO) throws RemoteException {
		int groupSize = 1;
		if (memVO.isGroupMember()) {
			groupSize = memVO.getMembershipGroup().getMemberCount();
			if (groupSize > MembershipHelper.SEGMENT_COUNT) {
				groupSize = MembershipHelper.SEGMENT_COUNT;
			}
		}
		if (groupSize < 0) {
			groupSize = 1;
		}
		return MembershipHelper.SEGMENT_TEXT[groupSize - 1];
	}

	private String memberProfileText(MembershipVO memVO) throws RemoteException {
		String pText = "";
		String pCode = memVO.getMembershipProfileCode();
		if (!pCode.equals(MembershipProfileVO.PROFILE_CLIENT)) {
			pText = memVO.getMembershipProfile().getDescription();
		}
		return pText;
	}

	/**
	 * Use the transactionVO and membershipVO to decide what is the appropriate type of wording for a particular membershipAdvice Must choose between create/change/cancel Add/remove group member Roadside/Non motoring Product Change (upgrade/downgrade) New group members from existing members/clean skins What chance is there of getting this right?
	 * 
	 * @param MembershipVO memVO The membership for which the advice is being printed
	 * @param TransactionVO transVO The most recent transaction
	 * @param PrintData printData PrintData object which carries advice name etc
	 * @return String The appropriate text string for the combination of inputs
	 * @throws RemoteException
	 */
	/*
	 * private String selectCorrectProductMessage(MembershipVO memVO, MembershipTransactionVO transVO, StringBuffer headingString, PrintData printData) throws RemoteException, SystemException { printData.adviceName = ""; String prodCode = memVO.getProductCode(); String transType = transVO.getTransactionGroupTypeCode(); // boolean isGroup = memVO.isGroupMember(); int groupCount = 1; if(memVO.isGroupMember()) { groupCount = memVO.getMembershipGroup().getMemberCount(); } try { if(transType == null) //historical data - produce vanilla report { printData.adviceName = "CHANGE_ROADSIDE"; headingString.append(""); } else if(Product.PRODUCT_NON_MOTORING.equalsIgnoreCase(prodCode)) { if(transType.equals(MembershipTransactionType.TRANSACTION_TYPE_CREATE) || transType.equals(MembershipTransactionType.TRANSACTION_TYPE_CREATE_GROUP) || transType.equals(MembershipTransactionType.TRANSACTION_TYPE_REJOIN)) { printData.adviceName = "CREATE_NON_MOTORING"; headingString.append(""); } else { printData.adviceName =
	 * "CHANGE_NON_MOTORING"; headingString.append(""); } } else if(transType.equals(MembershipTransactionType. TRANSACTION_TYPE_CHANGE_PRODUCT) && Product.PRODUCT_ADVANTAGE.equalsIgnoreCase(memVO.getNextProductCode())) { printData.adviceName = "CHANGEPROD"; } else if(transType.equals(MembershipTransactionType. TRANSACTION_TYPE_CHANGE_PRODUCT) && Product.PRODUCT_NON_MOTORING.equalsIgnoreCase (memVO.getNextProductCode())) { printData.adviceName = "CHANGE_NON_MOTORING"; } else if(transType.equals(MembershipTransactionType. TRANSACTION_TYPE_CREATE) || transType.equals(MembershipTransactionType.TRANSACTION_TYPE_CREATE_GROUP) || transType.equals(MembershipTransactionType.TRANSACTION_TYPE_REJOIN) ) { MembershipVO pa = null; if(memVO.isGroupMember()) { pa = memVO.getMembershipGroup().getMembershipGroupDetailForPA(). getMembership(); } else { pa = memVO; } MembershipVO oldPa = pa.getPreviousMembership(); if(oldPa == null) { printData.adviceName = "CREATE_ROADSIDE"; headingString.append(""); } else {
	 * printData.adviceName = "CHANGE_ROADSIDE"; headingString.append(""); } } else if( //upgrade transType.equals(MembershipTransactionType. TRANSACTION_TYPE_CHANGE_PRODUCT) && Product.PRODUCT_ULTIMATE.equalsIgnoreCase(prodCode)) { printData.adviceName = "UPGRADE_ULTIMATE"; headingString.append(""); } else if(transType.equals(MembershipTransactionType. TRANSACTION_TYPE_CHANGE_GROUP)) { if(groupCount == 1) { printData.adviceName = "CHANGE_ROADSIDE"; headingString.append(""); } else if(groupContainsNewMember(memVO)) { printData.adviceName = "CREATE_ROADSIDE"; headingString.append(""); } else { printData.adviceName = "CHANGE_ROADSIDE"; headingString.append(""); } } else if(transType.equals(MembershipTransactionType. TRANSACTION_TYPE_REMOVE_FROM_GROUP)) { printData.adviceName = "CHANGE_ROADSIDE"; headingString.append(""); } else if(transType.equals(MembershipTransactionType. TRANSACTION_TYPE_CANCEL) && memVO.getStatus().equals(MembershipVO.STATUS_CANCELLED)) { printData.adviceName = "CANCELMEM";
	 * headingString.append(""); } else { printData.adviceName = "CHANGE_ROADSIDE"; headingString.append(""); } } catch(Exception e) { e.printStackTrace(); throw new SystemException( "A data error has occurred for membership number " + memVO.getClientNumber() + "." + "\nUnable to print advice. \n", e); } return CommonEJBHelper.getCommonMgr().getGnText("MEM", "ADVICE", printData.adviceName); }
	 */

	private boolean groupContainsNewMember(MembershipVO memVO) throws RemoteException {
		MembershipVO thisVo = null;
		MembershipGroupDetailVO grpDetail = null;
		boolean newMem = false;
		if (memVO.isGroupMember()) {
			MembershipGroupVO memGroup = memVO.getMembershipGroup();
			Collection groupList = memGroup.getMembershipGroupDetailList();
			Iterator it = groupList.iterator();
			while (it.hasNext()) {
				grpDetail = (MembershipGroupDetailVO) it.next();
				thisVo = grpDetail.getMembership();
				if (thisVo.getPreviousMembership() == null) {
					newMem = true;
				}
			}
			return newMem;
		} else {
			return memVO.getPreviousMembership() == null;
		}
	}

	private OutputStream getOutputStream(ReportRequestBase rep, HttpServletRequest req) throws Exception {
		OutputStream oStream = null;
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		UserSession userSession = UserSession.getUserSession(req.getSession());

		PrintForm printForm = cMgr.getPrintForm(rep.getFormName(), userSession.getUser().getPrinterGroup());
		String fileName = PrintUtil.getFileNameForPrinting(printForm, SourceSystem.MEMBERSHIP);
		oStream = new FileOutputStream(fileName);
		return oStream;
	}

}
