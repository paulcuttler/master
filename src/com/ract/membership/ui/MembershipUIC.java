package com.ract.membership.ui;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.Vector;

import javax.ejb.EJB;
import javax.ejb.RemoveException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.TransactionRolledbackException;

import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientNote;
import com.ract.client.ClientVO;
import com.ract.client.NoteType;
import com.ract.client.ui.ClientUIConstants;
import com.ract.common.ApplicationServlet;
import com.ract.common.BusinessType;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.DirectDebitFailedValidationException;
import com.ract.common.ExpiredSessionException;
import com.ract.common.Gift;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SequenceMgr;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.cad.CadExportMgr;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.common.ui.CommonUIConstants;
import com.ract.membership.AdjustmentTypeVO;
import com.ract.membership.DiscountTypeVO;
import com.ract.membership.FeeTypeVO;
import com.ract.membership.GroupVO;
import com.ract.membership.MemTransChangeProduct;
import com.ract.membership.MemTransChangeProductOption;
import com.ract.membership.MemTransHold;
import com.ract.membership.MembershipCardVO;
import com.ract.membership.MembershipDiscountAttribute;
import com.ract.membership.MembershipDocument;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipGift;
import com.ract.membership.MembershipGiftPK;
import com.ract.membership.MembershipGroupVO;
import com.ract.membership.MembershipHelper;
import com.ract.membership.MembershipHistory;
import com.ract.membership.MembershipIDMgr;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipProductOption;
import com.ract.membership.MembershipProfileVO;
import com.ract.membership.MembershipQuote;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipRenewalMgr;
import com.ract.membership.MembershipRenewalMgrLocal;
import com.ract.membership.MembershipRepairMgr;
import com.ract.membership.MembershipSessionUtil;
import com.ract.membership.MembershipTransactionAdjustment;
import com.ract.membership.MembershipTransactionMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductBenefitTypeVO;
import com.ract.membership.ProductVO;
import com.ract.membership.RenewalNoticeException;
import com.ract.membership.SelectedClient;
import com.ract.membership.TransactionFeeDiscountAttribute;
import com.ract.membership.TransactionFeeDiscountAttributePK;
import com.ract.membership.TransactionGroup;
import com.ract.membership.TransactionGroupAffiliatedClubTransferImpl;
import com.ract.membership.TransactionGroupCancelImpl;
import com.ract.membership.TransactionGroupFactory;
import com.ract.membership.TransactionHelper;
import com.ract.membership.UndoTransactionGroup;
import com.ract.membership.club.AffiliatedClubHelper;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.PaymentTypeVO;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitFrequency;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.receipting.Payable;
import com.ract.security.Privilege;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.HTMLUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;
import com.ract.util.StringUtil;

/**
 * Membership User Interface Controller
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class MembershipUIC extends ApplicationServlet {

	@EJB
	private CommonMgrLocal commonMgrLocal;

	/**
	 * Initialize global variables
	 */
	public void init() throws ServletException {
		LogUtil.debug(this.getClass(), "init");
	}

	/**
	 * Process the HTTP Post request
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("\n*********************************************" + "\n\nMembershipUIC - request = " + request.getParameter(PARAMETER_EVENT) + "\n\n*********************************************");

		// Check for minimum privileges to view membership data
		UserSession userSession = UserSession.getUserSession(request.getSession());
		if (userSession == null || !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_VIEW)) {
			request.setAttribute("exception", new com.ract.common.SecurityException("You do not have the privilege to view membership data."));
			// request.setAttribute("rootCauseException", rootCauseException);
			forwardRequest(request, response, "/error/SecurityError.jsp");
		} else {
			super.doPost(request, response);
		}
	}

	/************************* Event handler methods **************************/

	public void handleEvent_useFastTrack_next(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		// Its a bad idea to remove the pending fee record here as the user is
		// not
		// aware that they are changing the system data by pushing the next
		// button.
		// Also stops a failed fast track payment from being repaired as the
		// pending
		// fee cannot be removed if it has been paid. TB 03/10/2003
		// String pfId = request.getParameter("pendingFeeId");
		//
		// try
		// {
		// PaymentTransactionMgr payTxMgr =
		// PaymentEJBHelper.getPaymentTransactionMgr();
		// payTxMgr.removePendingFee(pfId);
		// }
		// catch (Exception ex)
		// {
		// throw new RemoteException
		// ("useFastTrack_remove. Can't remove pending fee. " +
		// ex.getMessage());
		// }

		UserSession userSession = getUserSession(request);
		String key = request.getParameter("transGroupKey");

		// Add the transaction group to the session
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request, key);

		showPage_confirmTransaction(request, response, transGroup);
	}

	public void handleEvent_useFastTrack_remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String pfId = request.getParameter("pendingFeeId");
		request.setAttribute("memId", request.getParameter("membershipID"));

		try {
			PaymentTransactionMgr payTxMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			payTxMgr.removePendingFee(pfId);
		} catch (Exception ex) {
			throw new RemoteException("useFastTrack_remove. Can't remove pending fee. " + ex.getMessage());
		}

		action_viewMembership(request, response, "No");
	}

	/**
	 * @todo remove when finished id test method
	 */
	public void handleEvent_testSequence(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		SequenceMgr sMgr = CommonEJBHelper.getSequenceMgr();
		Integer x = sMgr.getNextID(SequenceMgr.SEQUENCE_MEM_MEMBERSHIP);

		try {
			PrintWriter out = response.getWriter();
			out.println("next id is " + x);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * Handle the transferMembership event raised by the client product list page. Membership type is defaulted to "Personal" since only these can be transferred.
	 * 
	 * @todo implement setting of profile code
	 */
	public void handleEvent_transferMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		action_transferMembership(request, response);

	} // handleEvent_productList_transferMembership

	/**
	 * Handle the NEXT/Process button from the SelectAffiliatedClub page
	 */
	public void handleEvent_selectAffiliatedClub_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		String pageName = null;

		MembershipVO oldMemVO = null;
		Collection memList = null;

		String clubCode = ServletUtil.getStringParam("clubCode", request);
		String clubMembershipId = ServletUtil.getStringParam("clubMembershipId", request);
		DateTime clubJoinDate = ServletUtil.getDateParam("clubJoinDate", request);
		DateTime clubExpiryDate = ServletUtil.getDateParam("clubExpiryDate", request);
		String clubProductLevel = request.getParameter("productLevel");
		String clientNumber = ServletUtil.getStringParam("clientNumber", request);
		ClientVO clientVO = getClient(clientNumber);

		// find the membership
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		memList = membershipMgr.findMembershipByClientNumber(clientVO.getClientNumber());

		String clientTypeCode = clientVO.getClientType();
		MembershipRefMgr membershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();
		Collection allowedMembershipTypeList = membershipRefMgr.getMembershipTypeListByClientType(clientTypeCode);

		// If the list is empty then this client type cannot be issued with a
		// membership
		if (allowedMembershipTypeList == null || allowedMembershipTypeList.isEmpty()) {
			throw new ValidationException("This client is of a type (" + (clientTypeCode == null || clientTypeCode.length() < 1 ? "?" : clientTypeCode) + ") that cannot be issued with a membership.");
		}

		// Create a transaction group
		UserSession userSession = getUserSession(request);

		TransactionGroupAffiliatedClubTransferImpl transGroup = new TransactionGroupAffiliatedClubTransferImpl(userSession.getUser());

		if (memList == null | memList.isEmpty()) // new member so start process
		{
			// add client only as we have no context membership
			transGroup.addSelectedClient(clientVO);
			transGroup.setContextClient(clientVO);
		} else // existing member so store credit
		{
			if (memList.size() > 1) {
				throw new SystemException("Not allowed more than one membership.");
			}
			Iterator memListIT = memList.iterator();

			oldMemVO = (MembershipVO) memListIT.next();

			// add old membership
			transGroup.addSelectedClient(oldMemVO);
			transGroup.setContextClient(oldMemVO);
		}

		MembershipTypeVO memTypeVO = null;
		if (oldMemVO != null) {
			memTypeVO = oldMemVO.getMembershipType();
		}

		// See if we can finalise the decision on what the membership type will
		// be.
		// Also select the next page to display.
		if (memTypeVO != null) {
			// A membership type was selected and it is an allowed membership
			// type for this client type.
			transGroup.setMembershipType(memTypeVO);
			pageName = MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS;
		} else if (allowedMembershipTypeList.size() == 1) {
			// An allowed membership type was not specified.
			// There is only one allowed membership type in the list.
			// Assume the allowed membership type.
			Iterator listIterator = allowedMembershipTypeList.iterator();
			memTypeVO = (MembershipTypeVO) listIterator.next();
			transGroup.setMembershipType(memTypeVO);
			pageName = MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS;
		} else {
			// An allowed membership type was not specified and there
			// is more than one in the allowed membership type list.
			pageName = MembershipUIConstants.PAGE_SelectMembershipType;
			request.setAttribute("allowedMembershipTypeList", allowedMembershipTypeList);
		}

		transGroup.setAffiliatedClubCode(clubCode);
		transGroup.setAffiliatedClubMemberId(clubMembershipId);
		transGroup.setAffiliatedClubJoinDate(clubJoinDate);
		transGroup.setAffiliatedClubExpiryDate(clubExpiryDate);
		transGroup.setAffiliatedProductLevel(clubProductLevel);

		MembershipSessionUtil.addTransactionContainer(request, (TransactionGroup) transGroup);

		request.setAttribute("transactionGroup", transGroup);

		forwardRequest(request, response, pageName);

	}

	/**
	 * Handle the NEXT button from the SelectAffiliatedClub page
	 */
	/*
	 * public void handleEvent_selectAffiliatedClub_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	 * 
	 * DateTime joinDate = ServletUtil.getDateParam("clubJoinDate", request); DateTime expiryDate = ServletUtil.getDateParam("clubExpiryDate", request); String clubCode = request.getParameter("selectedClubCode"); String membershipId = request.getParameter("clubMembershipId"); String clientNumber = request.getParameter("clientNumber"); String clubProductLevel = request.getParameter("productLevel");
	 * 
	 * //this.outputRequestParameters(request);
	 * 
	 * if((clubCode == null || clubCode.trim().length() < 1)) { throw new ValidationException("Select an affiliated club."); }
	 * 
	 * // SortedSet affClubsList = AffiliatedClubHelper.getSortedClubList(); AffiliatedClubVO affiliatedClubVO = AffiliatedClubHelper.getAffiliatedClub(clubCode); if (affiliatedClubVO == null) { throw new SystemException("No affiliated club matches code '"+clubCode+"'."); }
	 * 
	 * ClientVO clientVO = getClientVO(clientNumber); if(clientVO == null) { throw new SystemException("Client '"+clientNumber+"' not found!"); }
	 * 
	 * if(membershipId == null || membershipId.length() < 1) { throw new ValidationException ("The affiliated club membership Id has not been entered."); }
	 * 
	 * if(joinDate == null || joinDate.toString().length() < 1) { throw new ValidationException ("The join date for the affiliated club has not been entered."); }
	 * 
	 * if(expiryDate == null || expiryDate.toString().length() < 1) { throw new ValidationException ("The expiry date for the affiliated club has not been entered."); }
	 * 
	 * DateTime transactionDate = new DateTime().getDateOnly();
	 * 
	 * // AffiliatedClubCredit affiliatedClubCredit = AffiliatedClubHelper.calculateAffiliatedClubCredit (affiliatedClubVO,transactionDate,expiryDate); //LogUtil.log(this.getClass (),"creditAmount "+affiliatedClubCredit.getTransactionCreditAmount());
	 * 
	 * // MembershipHome memHome = MembershipEJBHelper.getMembershipHome(); MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	 * 
	 * LogUtil.log(this.getClass(),"get mem");
	 * 
	 * Collection memList = membershipMgr.findMembershipByClientNumber(clientVO.getClientNumber());
	 * 
	 * if (memList.isEmpty()) { //no memberships } else if(memList.size() > 1) { throw new SystemException( "Client has more than one membership keyed to the client number '" + clientVO.getClientNumber() + "'."); } else { MembershipVO membershipVO = (MembershipVO)memList.iterator().next(); request.setAttribute("membership", membershipVO); }
	 * 
	 * LogUtil.debug(this.getClass(),"set attr");
	 * 
	 * request.setAttribute("affiliatedClub", affiliatedClubVO); request.setAttribute("client", clientVO); request.setAttribute("membershipId", membershipId); request.setAttribute("joinDate", joinDate); request.setAttribute("expiryDate", expiryDate); // request.setAttribute("creditAmount", affiliatedClubCredit.getTransactionCreditAmount()); // request.setAttribute("daysRemaining", affiliatedClubCredit.getDaysRemaining());
	 * 
	 * LogUtil.log(this.getClass(),"at end");
	 * 
	 * 
	 * 
	 * forwardRequest(request, response, MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS); }
	 */

	/**
	 * Cancel Personal Membership - SAVE button
	 */
	public void handleEvent_cancelMembership_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		String membershipIDString = request.getParameter("membershipID");
		String primeAddresseeMemIDString = request.getParameter("primeAddresseeMemID");
		String cancelReasonCode = request.getParameter("cancelReason");
		if (cancelReasonCode == null)
			cancelReasonCode = (String) request.getAttribute("cancelReason");
		// check for blocking

		String blockTransactions = request.getParameter("blockTransactions");
		if (blockTransactions == null)
			blockTransactions = (String) request.getAttribute("blockTransactions");

		String noteText = request.getParameter("blockReasonText");
		if (noteText == null)
			noteText = (String) request.getAttribute("blockReasonText");

		String forceCancelDueToFailedDD = request.getParameter("forceCancelDueToFailedDD");
		boolean forceCancel = false;
		try {
			forceCancel = Boolean.parseBoolean(forceCancelDueToFailedDD);
		} catch (Exception e) {
		}

		boolean block = false;
		if (blockTransactions != null && blockTransactions.equals("true")) {
			LogUtil.debug(this.getClass(), "blockReasonText=" + noteText);
			if (noteText != null && noteText.trim().length() == 0) {
				throw new ValidationException("A block reason must be supplied if you require future transactions to be blocked.");
			}
			block = true;
		}
		LogUtil.debug(this.getClass(), "block=" + block);

		// Fetch the membership to be cancelled.
		MembershipVO oldMemVO = getMembershipVO(membershipIDString);

		// Create a new transaction group
		MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
		UserSession userSession = getUserSession(request);

		// Create a TransactionGroup object to hold the common data in the
		// transaction
		TransactionGroup transactionGroup = TransactionGroupFactory.getTransactionGroupForCancel(userSession.getUser());
		TransactionGroupCancelImpl transGroup = (TransactionGroupCancelImpl) transactionGroup;

		transGroup.setForceCancelDueToFailedDD(forceCancel);

		// set to block transactions
		transGroup.setTransactionBlocking(block);

		transGroup.setMembershipType(oldMemVO.getMembershipType());

		// Add the clients membership being cancelled to the transaction
		// group. This will also add the other members of the membership group
		// that they might belong to.
		transGroup.addSelectedClient(oldMemVO);
		transGroup.setContextClient(oldMemVO);

		// Create transactions for all selected clients
		transGroup.createTransactionsForSelectedClients();

		if (primeAddresseeMemIDString != null && primeAddresseeMemIDString.length() > 1) {
			Integer primeAddresseeMemID = new Integer(primeAddresseeMemIDString);
			transGroup.setPrimeAddressee(primeAddresseeMemID);
		}

		// Set the reason for the cancellation
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		ReferenceDataVO cancellationReason = refMgr.getCancelReason(cancelReasonCode);
		if (cancellationReason == null) {
			throw new SystemException("Failed to get cancellation reason '" + cancelReasonCode + "'");
		}

		transGroup.setTransactionReason(cancellationReason);

		// transGroup.calculateTransactionFee();

		Collection transactionList = transGroup.submit();
		if (transactionList == null) {
			throw new ValidationException("The transaction has already been submitted.<p>You won't be able to see the result so try looking at the membership to see if the changes have been made.");
		}

		// TODO replace
		if (block) {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();

			DateTime now = new DateTime();
			ClientNote clientNote = new ClientNote();
			// clientNote.setReferenceId(membershipIDString);
			clientNote.setClientNumber(oldMemVO.getClientNumber());
			clientNote.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
			clientNote.setCreated(now);
			clientNote.setCreateId(userSession.getUserId());

			// get first note type with attribute - blocks transactions
			Collection<NoteType> noteTypes = clientMgr.getNoteType(true);
			clientNote.setNoteType(noteTypes.iterator().next().getNoteType());
			clientNote.setNoteText(noteText);

			LogUtil.log(this.getClass(), "clientNote=" + clientNote);

			// populate a new membership note object

			clientMgr.createClientNote(clientNote);
		}

		request.setAttribute("contextClientVO", oldMemVO.getClient());
		request.setAttribute("transactionList", transactionList);
		forwardRequest(request, response, MembershipUIConstants.PAGE_TransactionComplete);
	} // handleEvent_cancelMembership_btnSave

	/**
	 * Hold Personal Membership - Next button
	 */
	public void handleEvent_holdMembership_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		DateTime startHoldDate = null;
		try {
			startHoldDate = new DateTime(request.getParameter("startDate"));
		} catch (ParseException pe) {
			throw new ValidationException("The start date of the hold period is not valid : " + pe.getMessage());
		}

		DateTime endHoldDate = null;
		try {
			endHoldDate = new DateTime(request.getParameter("endDate"));
		} catch (ParseException pe) {
			throw new ValidationException("The end date of the hold period is not valid : " + pe.getMessage());
		}

		DateTime today = new DateTime();

		Interval remainingMembershipPeriod = null;
		try {
			remainingMembershipPeriod = new Interval(request.getParameter("remainingMembership"));
		} catch (ParseException pe) {
			throw new ValidationException("The credit period after hold is not valid");
		}

		// multiply the base fee by no of years (& part years) on hold
		Interval holdPeriod = null;
		try {
			holdPeriod = new Interval(startHoldDate, endHoldDate);
		} catch (Exception e) {
			throw new ValidationException("The hold period is not valid.");
		}

		// LogUtil.log ( this.getClass (), "holdPeriod = " + holdPeriod );

		int holdYears = holdPeriod.getTotalYears();

		// Interval years = null;
		// try
		// {
		// years = new Interval(holdYears,0,0,0,0,0,0);
		// }
		// catch (Exception e)
		// {
		// throw new ValidationException("Error calculating number of years.");
		// }
		// if ( holdPeriod.getDays() > years.getDays()
		// || holdYears < 1
		// )
		// {
		// holdYears += 1;
		// }

		// LogUtil.log ( this.getClass (), "x holdYears = " + holdYears );

		MembershipTransactionVO memTransVO = (MembershipTransactionVO) transGroup.getMembershipTransactionForClient(transGroup.getContextClient().getClientNumber());
		if (!(memTransVO instanceof MemTransHold)) {
			throw new SystemException("The context client (clientID=" + memTransVO.getMembership().getClientNumber() + ") transaction type (" + memTransVO.getTransactionTypeCode() + ") has not been set correctly. The MembershipTransactionVO should be an instance of MemTransHold.");
		}
		MemTransHold holdTransaction = (MemTransHold) memTransVO;
		holdTransaction.setHoldPeriod(endHoldDate, holdYears, remainingMembershipPeriod);

		transGroup.createTransactionFees();

		showPage_confirmTransaction(request, response, transGroup);
	} // handleEvent_holdMembership_btnNext

	public void handleEvent_transactionComplete_viewMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		String memTypeCode = memVO.getMembershipTypeCode();
		String pageName = null;

		// Add the membership to the request so that the view pages can use it.
		request.setAttribute("membershipVO", memVO);

		// Get the view membership page to refresh the clients product list ???
		request.setAttribute("refreshProductList", "Yes");

		// Determine which view page to show.
		if (memTypeCode.equals(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL)) {
			pageName = MembershipUIConstants.PAGE_ViewPersonalMembership;
		} else if (memTypeCode.equals(MembershipTypeVO.MEMBERSHIP_TYPE_FLEET)) {
			// pageName = MembershipUIConstants.PAGE_ViewFleetMembership;
			// Send it to the personal memnership page for the moment
			pageName = MembershipUIConstants.PAGE_ViewPersonalMembership;
		} else {
			throw new SystemException("Unknown membership type '" + memTypeCode + "'");
		}
		forwardRequest(request, response, pageName);
	}

	/**
	 * Handle the event of selecting the a menu item on the view membership page.
	 */
	public void handleEvent_viewMembership_menuItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException, com.ract.common.SecurityException {
		String menuItemName = request.getParameter("menuItemName");
		LogUtil.debug(this.getClass(), "handleEvent_viewMembership_menuItem(" + menuItemName + ")");
		if ("mnuRenew".equals(menuItemName)) {
			action_renewMembership(request, response);
		} else if ("mnuRejoin".equals(menuItemName)) {
			action_rejoinMembership(request, response);
		} else if ("mnuHold".equals(menuItemName)) {
			action_holdMembership(request, response);
		} else if ("mnuCancel".equals(menuItemName)) {
			action_cancelMembership(request, response);
		} else if ("mnuChangeProduct".equals(menuItemName)) {
			action_changeMembershipProduct(request, response);
		} else if ("mnuChangeProductOption".equals(menuItemName)) {
			action_changeMembershipProductOption(request, response);
		} else if ("mnuCreateGroup".equals(menuItemName)) {
			String membershipIDStr = request.getParameter("membershipID");
			action_createGroup(request, response, null, membershipIDStr, "No");
		} else if ("mnuChangeGroup".equals(menuItemName)) {
			action_changeGroup(request, response);
		} else if ("mnuEditMembership".equals(menuItemName)) {
			action_editMembership(request, response);
		} else if ("mnuRenewGroup".equals(menuItemName)) {
			action_renewMembership(request, response);
		} else if ("mnuRequestCard".equals(menuItemName)) {
			action_requestCard(request, response);
		} else if ("mnuSendToCAD".equals(menuItemName)) {
			action_sendToCAD(request, response);
		}
		// else if("mnuManageCredit".equals(menuItemName))
		// {
		// action_manageCredit(request, response);
		// }
		else if ("mnuChangeGroupPA".equals(menuItemName)) {
			action_changeGroupPA(request, response);
		} else if ("mnuTransfer".equals(menuItemName)) {
			action_transferMembership(request, response);
		} else if ("mnuManageGifts".equals(menuItemName)) {
			action_manageGifts(request, response);
		} else {
			throw new SystemException("The view membership menu item name '" + menuItemName + "' is not valid.");
		}
	}

	public void handleEvent_saveGroupDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		this.outputRequestParameters(request);

		String groupIdString = request.getParameter("groupId");
		Integer groupId = new Integer(groupIdString);
		String groupTypeCode = request.getParameter("groupTypeCode");
		String groupName = request.getParameter("groupName");
		String streetSuburbProperty = request.getParameter("postProperty");
		String streetSuburbNumber = request.getParameter("postStreetChar");
		String streetSuburbIdString = request.getParameter("postStsubId");
		Integer streetSuburbId = new Integer(streetSuburbIdString);
		String contactPerson = request.getParameter("contactPerson");

		GroupVO newGroup = new GroupVO(groupId, groupTypeCode, groupName, streetSuburbProperty, streetSuburbNumber, streetSuburbId, contactPerson);

		String membershipIDStr = request.getParameter("membershipID");
		Integer membershipID = new Integer(membershipIDStr);
		try {
			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();

			GroupVO group = memMgr.getGroupByGroupId(groupId);
			if (group == null) {
				memMgr.createGroup(newGroup);
			} else {
				memMgr.updateGroup(newGroup);
			}

			MembershipVO contextMemVO = memMgr.getMembership(membershipID);
			MembershipGroupVO memGroupVO = contextMemVO.getMembershipGroup();
			if (memGroupVO != null) {
				request.setAttribute("membershipGroupVO", memGroupVO);
			}
		} catch (RemoteException ex) {
			// ignore
		}

		request.setAttribute("membershipID", membershipID);

		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewGroupMembers);
	}

	public void handleEvent_viewMembership_viewGroupMembers(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String membershipIDStr = request.getParameter("membershipID");
		Integer membershipID = new Integer(membershipIDStr);
		try {

			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
			MembershipVO contextMemVO = memMgr.getMembership(membershipID);
			MembershipGroupVO memGroupVO = contextMemVO.getMembershipGroup();
			if (memGroupVO != null) {
				request.setAttribute("membershipGroupVO", memGroupVO);
			} else {
				throw new ServletException("Unable to find group for membership ID " + membershipID);
			}
		} catch (RemoteException ex) {
			// ignore
		}
		request.setAttribute("membershipID", membershipID);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewGroupMembers);
	}

	public void handleEvent_viewGroupMembers_btnMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		handleEvent_productList_viewMembership(request, response);
	}

	public void handleEvent_viewGroupMembers_btnEditGroupDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String groupIdString = request.getParameter("groupId");
		String membershipId = request.getParameter("membershipID");
		Integer groupId = new Integer(groupIdString);

		request.setAttribute("groupId", groupId);
		request.setAttribute("membershipID", membershipId);

		// attempt to find and attach group vo if one exists.
		GroupVO groupVO = null;
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			groupVO = membershipMgr.getGroupByGroupId(groupId);
		} catch (RemoteException re) {
			// no group attached yet
			// throw new ServletException("Unable to find group "+groupId,re);
		}

		request.setAttribute("groupVO", groupVO);
		request.setAttribute("displayPostalAddress", new Boolean(true));

		forwardRequest(request, response, MembershipUIConstants.PAGE_EDIT_GROUP_DETAILS);
	}

	public void handleEvent_viewMembership_viewVehicles(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID);

		// Add the membership to the request so that the view pages can use it.
		request.setAttribute("membershipVO", memVO);

		String pageName = MembershipUIConstants.PAGE_ViewVehicles;
		forwardRequest(request, response, pageName);
	}

	public void handleEvent_viewMembershipVehicles_btnMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		handleEvent_productList_viewMembership(request, response);
	}

	public void handleEvent_viewMembershipVehicles_btnChange(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		throw new SystemException("This feature has not been implemented yet.");
	}

	public void handleEvent_viewMembership_clearAmountOutstanding(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		MembershipVO memVO = getMembershipVO(request.getParameter("membershipID"));
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String clearOutstandingEnabledStr = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_CLEAR_OUTSTANDING_AMOUNT);
		if ("Yes".equalsIgnoreCase(clearOutstandingEnabledStr)) {
			PaymentTransactionMgr paymentTransactionMgr = PaymentEJBHelper.getPaymentTransactionMgr();
			int rows = paymentTransactionMgr.clearAmountOutstandingForClient(SourceSystem.MEMBERSHIP, memVO.getClientNumber());
			paymentTransactionMgr.commit();
		} else {
			throw new SystemException("Clearing of outstanding amounts is not enabled.");
		}
		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPersonalMembership);
	}

	public void handleEvent_viewMembership_viewCards(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		request.setAttribute("membershipVO", memVO);
		String pageName = MembershipUIConstants.PAGE_ViewCards;
		forwardRequest(request, response, pageName);
	}

	/**
	 * handle the event of the user removing a card request
	 */
	public void handleEvent_viewMembershipCards_btnRemove(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		String cardIdString = request.getParameter("cardID");
		MembershipMgrLocal memMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();

		MembershipCardVO memCard = memMgrLocal.getMembershipCard(new Integer(cardIdString));
		memMgrLocal.removeMembershipCard(memCard.getCardID());

		MembershipVO memVO = memMgrLocal.getMembership(memCard.getMembershipID());
		request.setAttribute("membershipVO", memVO);

		// forward back to the page
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewCards);
	}

	/**
	 * Handle the event of the user requesting a new card be issued
	 */
	public void handleEvent_viewMembershipCards_btnRequestNew(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO contextMemVO = getMembershipVO(membershipID); // Throws an
		// exception
		// if it cant
		// find the
		// membership

		// Check that there are no "pending" card requests in case the user has
		// used back button to get here
		Collection memCardList = contextMemVO.getMembershipCardList(true);
		boolean outstandingIssue = false;
		if (!memCardList.isEmpty()) {
			Iterator memCardListIT = memCardList.iterator();
			while (memCardListIT.hasNext()) {
				MembershipCardVO memCardVO = (MembershipCardVO) memCardListIT.next();
				if (memCardVO.getIssueDate() == null) {
					outstandingIssue = true;
				}
			}
		}
		if (outstandingIssue == true) {
			throw new ValidationException("There is already a card request pending.");
		}

		UserSession userSession = getUserSession(request);

		request.setAttribute("membershipVO", contextMemVO);

		forwardRequest(request, response, MembershipUIConstants.PAGE_MemberCardRequest);
	}

	public void handleEvent_viewMembershipCards_btnMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		handleEvent_productList_viewMembership(request, response);
	}

	public void handleEvent_viewMembership_viewHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewHistory);
	}

	/**
	 * Show the membership for the prime addressee of the membership group.
	 */
	public void handleEvent_viewMembership_viewPaymentMethod(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership

		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPaymentMethod);
	}

	/**
	 * Show the membership for the prime addressee of the membership group.
	 */
	public void handleEvent_viewMembership_viewGroupPA(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		request.setAttribute("membershipVO", memVO);
		request.setAttribute("refreshProductList", "Yes");
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPersonalMembership);
	}

	/**
	 * Generate history for the last transaction.
	 */
	public void xxxhandleEvent_viewMembershipHistory_btnCreateHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// String membershipID = request.getParameter("membershipID");
		// MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it cant find the membership
		//
		// MembershipTransactionMgr memTxMgr =
		// MembershipEJBHelper.getMembershipTransactionMgr();
		// memTxMgr.createTransactionHistoryForMembership(memVO);
		//
		// request.setAttribute("membershipVO",memVO);
		// forwardRequest(request,response,MembershipUIConstants.PAGE_ViewHistory);
	}

	public void handleEvent_viewMembershipHistory_btnRemove(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, com.ract.common.SecurityException, ValidationException {
		// Get the transaction ID to remove.
		String transactionIDString = request.getParameter("transactionID");
		Integer transID = null;
		try {
			transID = new Integer(transactionIDString);
		} catch (Exception e) {
			throw new SystemException("The transaction ID '" + transactionIDString + "' is not a valid integer.");
		}

		UserSession userSession = getUserSession(request);

		MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
		try {
			memTransMgr.removeMembershipTransaction(transID, userSession);
		} catch (RemoveException re) {
			throw new SystemException(re);
		}

		// Show the history page again
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewHistory);
	}

	public void handleEvent_viewMembershipHistory_showUndone(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// Show the history page again
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewHistory);
	}

	public void handleEvent_viewMembershipHistory_btnUndo(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// Get the transaction ID to undo.
		String transactionIDString = request.getParameter("transactionID");
		Integer transID = null;
		try {
			transID = new Integer(transactionIDString);
		} catch (Exception e) {
			throw new SystemException("The transaction ID '" + transactionIDString + "' is not a valid integer.");
		}

		UserSession userSession = getUserSession(request);

		UndoTransactionGroup transGroup = new UndoTransactionGroup(transID, userSession);

		request.setAttribute("undoTransactionGroup", transGroup);
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		forwardRequest(request, response, MembershipUIConstants.PAGE_UndoTransaction);
	}

	public void handleEvent_editTransaction_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// Get the transaction ID.
		String transactionIDStr = request.getParameter("transactionID");
		Integer transactionID = null;
		try {
			transactionID = new Integer(transactionIDStr);
		} catch (Exception e) {
			throw new SystemException("The transaction ID '" + transactionIDStr + "' is not a valid integer.");
		}

		String productCode = request.getParameter("productCode");
		if (productCode != null) {
			productCode = productCode.trim();
			if (productCode.length() > 0) {
				ProductVO prodVO = MembershipEJBHelper.getMembershipRefMgr().getProduct(productCode);
				if (prodVO == null) {
					throw new ValidationException("The product code '" + productCode + "' is not valid.");
				}
			} else {
				productCode = null;
			}
		}

		int groupCount = 1;
		String groupCountStr = request.getParameter("groupCount");
		if (groupCountStr != null && groupCountStr.trim().length() > 0) {
			try {
				groupCount = Integer.parseInt(groupCountStr);
			} catch (Exception e) {
				throw new ValidationException("The group count '" + groupCountStr + "' is not a valid number.");
			}
		}

		String memXML = request.getParameter("membershipXML");
		if (memXML != null) {
			memXML = memXML.trim();
			if (memXML.length() < 1) {
				memXML = null;
			}
		}

		DateTime effectiveDate = null;
		DateTime endDate = null;

		String effectiveDateString = request.getParameter("effectiveDate");
		LogUtil.log(this.getClass(), "effectiveDateString " + effectiveDateString);
		if (effectiveDateString != null) {
			try {
				effectiveDate = new DateTime(effectiveDateString);
			} catch (ParseException ex) {
				throw new SystemException("Effective date has not been provided in a valid format.", ex);
			}
		}

		String endDateString = request.getParameter("endDate");
		LogUtil.log(this.getClass(), "endDateString " + endDateString);
		if (endDateString != null) {
			try {
				endDate = new DateTime(endDateString);
			} catch (ParseException ex1) {
				throw new SystemException("End date has not been provided in a valid format.", ex1);
			}
		}

		String transGroupTypeCodeString = request.getParameter("transGroupTypeCode");
		String transTypeCodeString = request.getParameter("transTypeCode");

		String payableItemIDString = request.getParameter("payableItemID");
		Integer payableItemID = null;
		if (payableItemIDString != null && !payableItemIDString.equals("")) {
			payableItemID = new Integer(payableItemIDString);
		}

		LogUtil.log(this.getClass(), "transGroupTypeCodeString=" + transGroupTypeCodeString);
		LogUtil.log(this.getClass(), "payableItemID=" + payableItemID);

		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		MembershipTransactionVO memTransVO = membershipMgr.getMembershipTransaction(transactionID);
		memTransVO.setProductCode(productCode);
		memTransVO.setGroupCount(new Integer(groupCount));
		memTransVO.setMembershipXML(memXML);
		memTransVO.setEffectiveDate(effectiveDate);
		memTransVO.setEndDate(endDate);
		memTransVO.setTransactionGroupTypeCode(transGroupTypeCodeString);
		memTransVO.setTransactionTypeCode(transTypeCodeString);
		memTransVO.setPayableItemID(payableItemID);

		membershipMgr.updateMembershipTransaction(memTransVO);

		forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	public void handleEvent_viewMembershipHistory_btnEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// Get the transaction ID to undo.
		String transactionIDString = request.getParameter("transactionID");
		Integer transID = null;
		try {
			transID = new Integer(transactionIDString);
		} catch (Exception e) {
			throw new SystemException("The transaction ID '" + transactionIDString + "' is not a valid integer.");
		}

		request.setAttribute("transactionID", transID);

		forwardRequest(request, response, MembershipUIConstants.PAGE_EditTransaction);
	}

	public void handleEvent_viewMembershipHistory_btnReverse(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// Get the transaction ID to remove.
		String transactionIDString = request.getParameter("transactionID");
		Integer transID = null;
		try {
			transID = new Integer(transactionIDString);
		} catch (Exception e) {
			throw new SystemException("The transaction ID '" + transactionIDString + "' is not a valid integer.");
		}

		UserSession userSession = getUserSession(request);

		MembershipRepairMgr membershipRepairMgr = MembershipEJBHelper.getMembershipRepairMgr();
		membershipRepairMgr.reverseTransaction(transID, "Reverse posting for reverse transaction. Ref:", userSession.getUser().getUserID(), false, false, false);

		// Show the history page again
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewHistory);

	}

	public void handleEvent_viewMembershipHistory_btnMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		handleEvent_productList_viewMembership(request, response);
	}

	public void handleEvent_viewMembershipDocument_detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		Integer documentId = new Integer(request.getParameter("documentId"));
		LogUtil.debug(this.getClass(), "documentId=" + documentId);
		MembershipDocument memDocVO = MembershipEJBHelper.getMembershipMgr().getMembershipDocument(documentId);
		if (memDocVO == null) {
			throw new SystemException("Failed to get membership document Id " + documentId);
		}
		LogUtil.debug(this.getClass(), "memDocVO=" + memDocVO);

		if (memDocVO.getTransactionXML() == null) {
			throw new ValidationException("Unable to view membership document as XML does not exist.");
		}

		request.setAttribute("membershipDocumentVO", memDocVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_VIEW_MEMBERSHIP_DOCUMENT_DETAIL);
	}

	public void handleEvent_viewMembershipQuote_detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		Integer quoteNumber = new Integer(request.getParameter("quoteNumber"));
		MembershipQuote memQuote = MembershipEJBHelper.getMembershipMgr().getMembershipQuote(quoteNumber);
		if (memQuote == null) {
			throw new SystemException("Failed to get membership quote number " + quoteNumber);
		}

		request.setAttribute("membershipQuote", memQuote);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewMembershipQuoteDetail);
	}

	public void handleEvent_viewMembershipQuoteList_viewQuote(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		action_viewQuoteDetail(request, response);
	}

	public void handleEvent_viewMembership_viewDocuments(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// cant find the
		// membership
		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewDocuments);
	}

	public void handleEvent_viewMembershipDocuments_btnMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		handleEvent_productList_viewMembership(request, response);
	}

	public void handleEvent_viewMembershipDocuments_btnCancelRenewal(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

		Integer membershipId = new Integer(request.getParameter("membershipID"));
		Integer documentId = new Integer(request.getParameter("documentId"));
		LogUtil.debug(this.getClass(), "documentId=" + documentId);
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		PaymentTransactionMgrLocal paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
		try {
			membershipMgr.removeRenewalDocument(documentId, MembershipMgr.MODE_CANCEL, paymentTransactionMgrLocal, false);
			// commit the payment changes
			paymentTransactionMgrLocal.commit();
		} catch (Exception ex) {
			paymentTransactionMgrLocal.rollback();
			throw new ValidationException("Unable to cancel renewal document. " + ex.getMessage(), ex);
		}

		MembershipVO memVO = membershipMgr.getMembership(membershipId);
		request.setAttribute("membershipVO", memVO);

		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewDocuments);
	}

	public void handleEvent_viewMembershipDocuments_btnCancelRenewalIgnoreErrors(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

		Integer membershipId = new Integer(request.getParameter("membershipID"));
		Integer documentId = new Integer(request.getParameter("documentId"));
		LogUtil.debug(this.getClass(), "documentId=" + documentId);
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		PaymentTransactionMgrLocal paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
		try {
			membershipMgr.removeRenewalDocument(documentId, MembershipMgr.MODE_CANCEL, paymentTransactionMgrLocal, true);
			// commit the payment changes
			paymentTransactionMgrLocal.commit();
		} catch (Exception ex) {
			paymentTransactionMgrLocal.rollback();
			throw new ValidationException("Unable to cancel renewal document. " + ex.getMessage(), ex);
		}

		MembershipVO memVO = membershipMgr.getMembership(membershipId);
		request.setAttribute("membershipVO", memVO);

		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewDocuments);
	}

	/**
	 * Handle the select history event on the view membership history page. Get the MembershipHistory object from the membership transaction and display it using the view membership page.
	 * 
	 * @todo Handle viewing different membership types.
	 */
	public void handleEvent_viewMembershipHistory_selectHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

		String transactionIDText = request.getParameter("transactionID");
		Integer transactionID = null;
		try {
			transactionID = new Integer(transactionIDText);
		} catch (Exception e) {
			throw new SystemException("The transaction ID '" + transactionIDText + "' is not a valid integer.", e);
		}

		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		MembershipTransactionVO memTransVO = membershipMgr.getMembershipTransaction(transactionID);

		MembershipHistory memHist = memTransVO.getMembershipHistory();
		request.setAttribute("membershipHistory", memHist);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPersonalMembership);
	}

	/**
	 * Handle the select amount payable event on the view membership history page. Get the membership transaction and show the view transaction fees page.
	 */
	public void handleEvent_viewMembershipHistory_selectAmountPayable(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

		String transactionIDText = request.getParameter("transactionID");
		Integer transactionID = null;
		Collection groupTxList = null;
		try {
			transactionID = new Integer(transactionIDText);
		} catch (Exception e) {
			throw new SystemException("The transaction ID '" + transactionIDText + "' is not a valid integer.", e);
		}

		MembershipTransactionVO memTxVO = null;
		Integer membershipID = null;
		try {
			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
			memTxVO = memMgr.getMembershipTransaction(transactionID);
			membershipID = memTxVO.getMembershipID();
			if (memTxVO == null) {
				throw new SystemException("The membership transaction is null for ID '" + transactionID + "'.");
			}

			if (memTxVO != null) {
				// get list of transactions for the group
				groupTxList = memTxVO.getTransactionListForGroup();

				// locate the prime addressee
				MembershipTransactionVO tmpMemTxVO = null;
				MembershipHistory memHist = null;
				boolean paFound = false;
				// find the pa
				if (groupTxList != null && groupTxList.size() > 0) {
					Iterator txIt = groupTxList.iterator();
					while (txIt.hasNext() && !paFound) {
						tmpMemTxVO = (MembershipTransactionVO) txIt.next();
						// get this from history
						if (tmpMemTxVO != null && tmpMemTxVO.isHistoryAvailable()) {
							memHist = tmpMemTxVO.getMembershipHistory();
							if (memHist.isPrimeAddressee()) {
								paFound = true;
								request.setAttribute("primeAddresseeTransaction", tmpMemTxVO);
							}
						}
					}
				}

			}

		} catch (RemoteException fe) {
			throw new SystemException("Failed to get transaction list for group " + transactionIDText + ". " + fe.toString());
		}

		request.setAttribute("groupTxList", groupTxList);
		request.setAttribute("membershipID", membershipID);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewTransactionFees);
	}

	public void handleEvent_viewGroupMembers_selectMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		String pageName = null;
		MembershipVO membershipVO = this.getMembershipVO(membershipID);

		if (membershipVO == null) {
			throw new SystemException("The membership ID '" + membershipID + "' is not valid.");
		}

		// Add the membership to the request so that the view pages can use it.
		request.setAttribute("membershipVO", membershipVO);
		request.setAttribute("refreshProductList", "Yes");

		// Determine which view page to show.
		if (MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL.equals(membershipVO.getMembershipTypeCode())) {
			pageName = MembershipUIConstants.PAGE_ViewPersonalMembership;
		} else if (MembershipTypeVO.MEMBERSHIP_TYPE_FLEET.equals(membershipVO.getMembershipTypeCode())) {
			pageName = MembershipUIConstants.PAGE_ViewFleetMembership;
		} else {
			throw new SystemException("Unknown membership type '" + membershipVO.getMembershipTypeCode() + "'");
		}
		forwardRequest(request, response, pageName);
	}

	public void handleEvent_viewPaymentMethod_btnEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO membershipVO = getMembershipVO(membershipID);

		// Add the membership to the request so that the view pages can use it.
		request.setAttribute("membershipVO", membershipVO);

		DirectDebitAuthority dda = null;
		try {
			dda = membershipVO.getDirectDebitAuthority();
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
		StringBuffer ddPolicyMessage = constructDDPolicyMessage(dda);
		request.setAttribute("ddPolicyMessage", ddPolicyMessage.toString());

		forwardRequest(request, response, MembershipUIConstants.PAGE_EditPaymentMethod);
	}

	public void handleEvent_viewPaymentMethod_btnMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		handleEvent_productList_viewMembership(request, response);
	}

	public void handleEvent_viewTransactionFees_btnMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		handleEvent_productList_viewMembership(request, response);
	}

	public void handleEvent_editMembership_AddButton(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ValidationException, RemoteException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		ClientVO contextClientVO = transGroup.getContextClient();
		MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(contextClientVO.getClientNumber());
		MembershipVO oldMemVO = contextMemTransVO.getMembership();
		MembershipVO newMemVO = contextMemTransVO.getNewMembership();
		String selectedDiscountTypeCode = request.getParameter("selectedDiscountTypeCode");
		if (selectedDiscountTypeCode != null && selectedDiscountTypeCode.length() > 0) {
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			DiscountTypeVO discountTypeVO = refMgr.getDiscountType(selectedDiscountTypeCode);
			if (discountTypeVO == null) {
				throw new SystemException("Failed to get discount type VO'" + selectedDiscountTypeCode + "'");
			}

			if (!DiscountTypeVO.STATUS_Active.equals(discountTypeVO.getStatus())) {
				throw new SystemException("Discount type '" + selectedDiscountTypeCode + "' is not active.");
			}

			if (!discountTypeVO.isRecurringDiscount()) {
				throw new SystemException("Discount type '" + selectedDiscountTypeCode + "' is not a recurring type of discount.");
			}

			newMemVO.addRecurringDiscount(discountTypeVO);
		}
		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_EditMembership);
	}

	public void handleEvent_editMembership_CancelButton(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ValidationException, RemoteException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		ClientVO contextClientVO = transGroup.getContextClient();
		MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(contextClientVO.getClientNumber());
		MembershipVO oldMemVO = contextMemTransVO.getMembership();
		MembershipSessionUtil.removeTransactionContainer(request, transGroup.getTransactionGroupKey());
		request.setAttribute("membershipVO", oldMemVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPersonalMembership);
	}

	public void handleEvent_editMembership_RemoveButton(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ValidationException, RemoteException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		ClientVO contextClientVO = transGroup.getContextClient();
		MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(contextClientVO.getClientNumber());
		MembershipVO oldMemVO = contextMemTransVO.getMembership();
		MembershipVO newMemVO = contextMemTransVO.getNewMembership();
		String removedDiscountTypeCode = request.getParameter("removedDiscountTypeCode");

		newMemVO.removeRecurringDiscount(removedDiscountTypeCode);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_EditMembership);
	}

	public void handleEvent_editMembership_ChangeProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ValidationException, RemoteException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		;
		ClientVO contextClientVO = transGroup.getContextClient();
		MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(contextClientVO.getClientNumber());
		MembershipVO oldMemVO = contextMemTransVO.getMembership();
		MembershipVO newMemVO = contextMemTransVO.getNewMembership();

		String selectedProfileCode = request.getParameter("selectedProfileCode");
		newMemVO.setMembershipProfileCode(selectedProfileCode);

		request.setAttribute("contextClientVO", contextClientVO);
		request.setAttribute("transactionGroup", transGroup);

		forwardRequest(request, response, MembershipUIConstants.PAGE_EditMembership);
	}

	public void handleEvent_editMembershipQuote(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		String quoteNumberString = request.getParameter("quoteNumber");
		Integer quoteNumber = new Integer(quoteNumberString);
		MembershipMgr mtMgr = MembershipEJBHelper.getMembershipMgr();
		MembershipQuote memQuoteVO = mtMgr.getMembershipQuote(quoteNumber);
		request.setAttribute("memQuote", memQuoteVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_EDIT_QUOTE);

	}

	public void handleEvent_editMembershipQuote_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

		// Update a quote if required
		String quoteNumberStr = (String) request.getParameter("quoteNumber");
		String transactionXML = (String) request.getParameter("transactionXML");
		StringBuffer heading = new StringBuffer("Quote Save Status");
		StringBuffer message = new StringBuffer();
		MembershipQuote memQuote = null;
		// MembershipQuoteHome memQuoteHome =
		// MembershipEJBHelper.getMembershipQuoteHome();
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		if (quoteNumberStr != null && transactionXML != null) {
			try {
				memQuote = membershipMgr.getMembershipQuote(new Integer(quoteNumberStr));
				memQuote.setTransactionXML(transactionXML);
				membershipMgr.updateMembershipQuote(memQuote);
				message.append("Quote " + quoteNumberStr + " has been updated successfully.");
			} catch (Exception e) {
				message.append("Quote " + quoteNumberStr + " has not been updated. " + e.getMessage());
			}
		}
		request.setAttribute("heading", heading.toString());
		request.setAttribute("message", message.toString());
		forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);
	}

	public void handleEvent_editMembership_SaveButton(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ValidationException, RemoteException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		ClientVO contextClientVO = transGroup.getContextClient();
		MembershipTransactionVO contextMemTransVO = transGroup.getMembershipTransactionForClient(contextClientVO.getClientNumber());
		MembershipVO oldMemVO = contextMemTransVO.getMembership();
		MembershipVO newMemVO = contextMemTransVO.getNewMembership();

		String joinDateStr = request.getParameter("joinDate");
		if (joinDateStr == null) {
			throw new SystemException("Join date is not available.");
		}
		DateTime joinDate = null;
		try {
			joinDate = DateUtil.convertStringToDateTime(joinDateStr);
		} catch (Exception e) {
			throw new ValidationException("The join date is not a valid date. " + e.getMessage());
		}

		if (joinDate.afterDay(newMemVO.getCommenceDate())) {
			newMemVO.setCommenceDate(joinDate);
		}
		if (joinDate.afterDay(newMemVO.getProductEffectiveDate())) {
			newMemVO.setProductEffectiveDate(joinDate);
		}
		newMemVO.setJoinDate(joinDate);

		String expiryDateStr = request.getParameter("expiryDate");
		if (expiryDateStr == null) {
			throw new SystemException("Expiry date is not available.");
		}
		DateTime expiryDate = null;
		try {
			expiryDate = DateUtil.convertStringToDateTime(expiryDateStr);
		} catch (Exception e) {
			throw new ValidationException("The expiry date is not a valid date. " + e.getMessage());
		}
		newMemVO.setExpiryDate(expiryDate);

		boolean allowToLapse = "Yes".equals(request.getParameter("allowToLapse"));
		newMemVO.setAllowToLapse(Boolean.valueOf(allowToLapse));

		String allowToLapseReasonCode = request.getParameter("allowToLapseReason");
		if (allowToLapse && (allowToLapseReasonCode == null || allowToLapseReasonCode.equals(""))) {
			throw new SystemException("Allow to lapse reason is required when allowing a membership to lapse.");
		} else if (!allowToLapse && allowToLapseReasonCode != null && !allowToLapseReasonCode.equals("")) {
			throw new SystemException("Allow to lapse reason is entered but the membership is not marked as allow to lapse.");
		}
		newMemVO.setAllowToLapseReasonCode(allowToLapseReasonCode);

		String creditAmountString = request.getParameter("creditAmount");
		double creditAmount = 0;
		if (creditAmountString != null && !creditAmountString.equals("")) {
			creditAmount = Double.parseDouble(creditAmountString);
		}
		newMemVO.setCreditAmount(creditAmount);

		String ddAuthorityIDString = request.getParameter("ddAuthorityID");
		Integer ddAuthorityID = null;
		if (ddAuthorityIDString != null && !ddAuthorityIDString.equals("")) {
			ddAuthorityID = new Integer(ddAuthorityIDString);
		}
		newMemVO.setDirectDebitAuthorityID(ddAuthorityID);

		if (newMemVO.getProduct().isVehicleBased()) {
			String rego = request.getParameter("rego");
			if (rego == null || rego.isEmpty()) {
				throw new SystemException("Vehicle registration cannot be blank");
			}
			newMemVO.setRego(rego);

			String vin = request.getParameter("vin");
			if (vin == null || vin.isEmpty()) {
				throw new SystemException("Vehicle VIN cannot be blank");
			}
			newMemVO.setVin(vin);

			String year = request.getParameter("year");
			if (year == null || year.isEmpty()) {
				throw new SystemException("Vehicle year cannot be blank");
			}
			newMemVO.setYear(year);

			String make = request.getParameter("make");
			if (make == null || make.isEmpty()) {
				throw new SystemException("Vehicle make cannot be blank");
			}
			newMemVO.setMake(make);

			String model = request.getParameter("model");
			if (model == null || model.isEmpty()) {
				throw new SystemException("Vehicle model cannot be blank");
			}
			newMemVO.setModel(model);
		}

		Collection completedTransactionList = transGroup.submit();
		request.setAttribute("transactionList", completedTransactionList);
		request.setAttribute("contextClientVO", contextClientVO);
		if (!allowToLapse) {
			try {
				UserSession us = getUserSession(request);
				User user = us.getUser();
				MembershipRenewalMgr rMgr = MembershipEJBHelper.getMembershipRenewalMgr();
				rMgr.replaceRenewalNotice(newMemVO, newMemVO, user);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new SystemException("Unable to create new renewal document");
			}
		}
		MembershipSessionUtil.removeTransactionContainer(request, transGroup.getTransactionGroupKey());
		forwardRequest(request, response, MembershipUIConstants.PAGE_TransactionComplete);
	}

	/**
	 * Most of this should not occur in the controller
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException
	 * @throws SystemException
	 * @throws ValidationException
	 * @throws RemoteException
	 */
	public void handleEvent_editPaymentMethod_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException, ValidationException, RemoteException {

		MembershipVO membershipVO = null;
		Integer newAuthorityID = null;
		PaymentTransactionMgr paymentTxMgr = null;

		String membershipID = request.getParameter("membershipID");
		membershipVO = getMembershipVO(membershipID); // with current payment
		// method attached

		String paymentTypeStr = request.getParameter("paymentType");

		DateTime today = new DateTime();

		boolean autoRenew = false;

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

		PaymentTypeVO paymentType = paymentMgr.getPaymentType(paymentTypeStr);
		if (paymentType == null) {
			throw new SystemException("Failed to get payment type '" + paymentTypeStr + "'.");
		}

		DirectDebitAuthority oldAuthority = membershipVO.getDirectDebitAuthority();
		DirectDebitAuthority newAuthority = null;

		MembershipMgr mMgr = MembershipEJBHelper.getMembershipMgr();

		UserSession userSession = getUserSession(request);

		String cashTypeCode = paymentType.getCashTypeCode();
		if (!PaymentTypeVO.CASHTYPE_CASH.equals(cashTypeCode)) {

			// GH 14/04/2004 unable to change to direct debit payment method if
			// outstanding
			// amounts exist in the receipting system.
			try {
				double recAmountOutstanding = membershipVO.getAmountOutstanding(true, false);
				if (recAmountOutstanding > 0) {
					throw new SystemException("Unable to change payment method to direct debit. There is an amount outstanding of " + CurrencyUtil.formatDollarValue(recAmountOutstanding) + " in the receipting system.");
				}
			} catch (PaymentException ex1) {
				throw new SystemException("Unable to determine if there are amounts outstanding in receipting.", ex1);
			}

			// Paying by direct debit. get the details.
			BankAccount bankAccount;
			String accountName = request.getParameter("accountName");
			if (accountName == null) {
				throw new SystemException("Failed to get accountName field from page.");
			}

			if (PaymentTypeVO.CASHTYPE_DIRECTDEBIT.equals(cashTypeCode)) {
				// Get debit account info
				String bsbNumber = request.getParameter("bsbNumber");
				if (bsbNumber == null) {
					throw new SystemException("Failed to get bsbNumber field from page.");
				}

				bsbNumber = StringUtil.replaceAll(bsbNumber, " ", "");
				bsbNumber = StringUtil.replaceAll(bsbNumber, "-", "");

				String accountNumber = request.getParameter("accountNumber");
				if (accountNumber == null) {
					throw new SystemException("Failed to get accountNumber field from page.");
				}

				accountNumber = StringUtil.replaceAll(accountNumber, " ", "");
				accountNumber = StringUtil.replaceAll(accountNumber, "-", "");
				bankAccount = new DebitAccount(bsbNumber, accountNumber, accountName);

			} else if (PaymentTypeVO.CASHTYPE_CREDITCARD.equals(cashTypeCode)) {
				// Get credit card account info
				String cardNumber = request.getParameter("cardNumber");
				HttpSession session = request.getSession();
				String oldCardNumber = (String) session.getAttribute("oldCCNum");
				String oldCardString = (String) session.getAttribute("oldCCString");
				if (cardNumber == null) {
					throw new SystemException("Failed to get cardNumber field from page.");
				}
				if (oldCardString != null && oldCardString.equals(cardNumber)) {
					cardNumber = oldCardNumber;
				}

				String cardExpiry = request.getParameter("cardExpiry");
				if (cardExpiry == null) {
					throw new SystemException("Failed to get cardExpiry field from page.");
				}

				/*
				 * String cardVerificationValue = request.getParameter("cardVerificationValue"); if(cardVerificationValue == null) { throw new SystemException( "Failed to get cardVerificationValue field from page."); }
				 */

				CreditCardAccount ccAccount = new CreditCardAccount();
				ccAccount.setAccountName(accountName);
				ccAccount.setPaymentType(paymentType);
				ccAccount.setCardNumber(cardNumber);
				ccAccount.setCardExpiry(cardExpiry);
				/* ccAccount.setVerificationValue(cardVerificationValue); */
				bankAccount = ccAccount;
			} else {
				throw new SystemException("Invalid cash type code '" + cashTypeCode + "' on payment type '" + paymentType.getDescription() + "'.");
			}

			// Get the isOwner field value
			boolean isOwner = "Yes".equals(request.getParameter("isOwner"));

			// Get the frequency to do the debits
			DirectDebitFrequency frequency = DirectDebitFrequency.getFrequency(request.getParameter("frequencyOfDebit"));

			String dayToDebitStr = request.getParameter("dayToDebit");
			if (dayToDebitStr == null) {
				throw new SystemException("Failed to get 'dayToDebit' field from page.");
			}

			int dayToDebit = 0;
			try {
				dayToDebit = Integer.parseInt(dayToDebitStr.trim());
			} catch (NumberFormatException nfe) {
				throw new ValidationException("The day of the month on which to do the debit is not a valid number.");
			}

			boolean isApproved = "Yes".equals(request.getParameter("isApproved"));
			LogUtil.debug(this.getClass(), "isApproved" + isApproved + " " + request.getParameter("isApproved"));
			if (oldAuthority != null) {
				isApproved = oldAuthority.isApproved();
			}

			// String ddrDateStr = request.getParameter("ddrDate");
			// if(ddrDateStr == null)
			// {
			// throw new
			// SystemException("Failed to get 'ddrDate' field from page.");
			// }
			//
			// DateTime ddrDate = null;
			// try
			// {
			// ddrDate = DateUtil.convertStringToDateTime(ddrDateStr);
			// }
			// catch(Exception e)
			// {
			// throw new
			// ValidationException("The DDR date is not a valid date.");
			// }
			//
			// The DDR Date is no longer entered by the user. see MEM-184

			DateTime ddrDate = DateUtil.nextDay();
			LogUtil.debug(this.getClass(), "RLG ++++++++++ " + ddrDate);
			newAuthority = new DirectDebitAuthority((oldAuthority == null ? null : oldAuthority.getDirectDebitAuthorityID()), membershipVO.getClientNumber(), userSession.getUserId(), userSession.getUser().getSalesBranchCode(), today, isOwner, dayToDebit, isApproved, ddrDate, frequency, bankAccount, SourceSystem.MEMBERSHIP, membershipVO.getMembershipID());

			// The ddr date must be validated if a new direct debit authority is
			// required.
			// A new direct debit authority is not required if an existing
			// direct debit
			// payment method has not been significantly changed. Otherwise it
			// is required.
			if (oldAuthority == null || oldAuthority.hasSignificantlyChanged(newAuthority)) {
				// A new direct debit authorisation is required so the
				// ddr date must be after today.
				//

				if (ddrDate.beforeDay(today)) {
					throw new ValidationException("The ddr date must be after today.");
				}

			}

		}
		// Receipting
		else {
			// GH 24/02/2004 unable to change to receipting payment method if
			// outstanding
			// amounts exist in the direct debit system. Use OCR to pay direct
			// debit schedule
			// if they wish to pay now.
			try {
				double ddAmountOutstanding = membershipVO.getAmountOutstanding(false, true);
				if (ddAmountOutstanding > 0) {
					throw new SystemException("Unable to change payment method to receipting. There is an amount outstanding of " + CurrencyUtil.formatDollarValue(ddAmountOutstanding) + " in the direct debit system.");
				}
			} catch (PaymentException ex1) {
				throw new SystemException("Unable to determine if there are amounts outstanding in direct debit.", ex1);
			}
		}

		boolean createNewRenewalNotice = false;

		// cancel active membership documents
		MembershipDocument doc = membershipVO.getCurrentRenewalDocument(); // this
		// cancels
		// all
		// but
		// current
		if (doc != null) {
			try {
				mMgr.cancelRenewalDocument(doc.getDocumentId());
				createNewRenewalNotice = true;

			} catch (RenewalNoticeException e) {
				throw new SystemException("Unable to cancel renewal document: " + doc.getDocumentId());
			}
		}

		// do the update and return a fresh copy
		membershipVO = mMgr.updateMembershipDirectDebitAuthority(newAuthority, membershipVO, userSession.getUserId());

		// create new renewal doc
		if (createNewRenewalNotice) {
			DateTime feeEffectiveDate = MembershipHelper.calculateRenewalEffectiveDate(membershipVO.getExpiryDate(), null);
			TransactionGroup transactionGroup = TransactionGroup.getRenewalTransactionGroup(membershipVO, userSession.getUser(), feeEffectiveDate);
			MembershipRenewalMgrLocal renewalMgrLocal = MembershipEJBHelper.getMembershipRenewalMgrLocal();
			// pass null PaymentTransactionMgr as no payment required
			renewalMgrLocal.produceIndividualRenewalNotice(membershipVO, transactionGroup, userSession.getUser(), null);
		}

		// add the updated membership value object to the request.
		request.setAttribute("membershipVO", membershipVO);

		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPaymentMethod);

	}

	/**
	 * Handle the createMembership event raised by the client product list page. If the membership type was specified go straight to the select product page. Otherwise, given the client type determine how many membership types the client can be issued with. If one then set it and show the select product page. If more than one then show the select membership type page. If none then throw a wobbly.
	 */
	public void handleEvent_productList_createMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		action_createMembership(request, response, "No");
	} // handleEvent_productList_createMembership

	/**
	 * Handle the createGroup event raised by the client product list page.
	 */
	public void handleEvent_productList_createGroup(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		// Get the client data
		String clientNumberStr = request.getParameter("clientNumber");
		action_createGroup(request, response, clientNumberStr, null, "No");
	} // handleEvent_productList_createMembership

	/**
	 * Handle the transferMembership event raised by the client product list page. Membership type is defaulted to "Personal" since only these can be transferred.
	 * 
	 * @todo implement setting of profile code
	 */
	public void handleEvent_confirmAffiliatedClub_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		action_createMembership(request, response, "Yes");
	} // handleEvent_productList_transferMembership

	public void handleEvent_issueGift(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		outputRequestParameters(request);

		UserSession userSession = this.getUserSession(request);

		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

		MembershipGift membershipGift = new MembershipGift();
		MembershipGiftPK mgPK = new MembershipGiftPK();

		Integer membershipId = new Integer(request.getParameter("membershipId"));
		MembershipVO membership = membershipMgr.getMembership(membershipId);

		if (!membership.getRenewalMembershipTier().isEligibleForLoyaltyScheme()) {
			throw new ValidationException("Membership is not eligible for loyalty scheme.");
		}

		String notes = request.getParameter("notes");

		String status = Gift.STATUS_GIFT_ACTIVE;
		String tierCode = request.getParameter("tierCode");

		String giftType = request.getParameter("giftType");
		String giftCode = null;

		// check for duplicates
		// Collection giftList =
		// membershipMgr.getMembershipGiftByMembershipID(membershipId);
		// if (giftList != null)
		// {
		// Iterator giftIt = giftList.iterator();
		// MembershipGift tmpGift = null;
		// while(giftIt.hasNext())
		// {
		// tmpGift = (MembershipGift)giftIt.next();
		// //matching gift type and is active
		// if(tmpGift.getGift().getGiftType().equals(giftType) &&
		// tmpGift.getMembershipGiftPK().getStatus().equals(Gift.STATUS_GIFT_ACTIVE)
		// &&
		// !tmpGift.getGift().getGiftType().equals(Gift.TYPE_GIFT_FREE_MEMBERSHIP))
		// //not free membership
		// {
		// throw new ValidationException("A gift of type '" + giftType
		// + "' has already been issued.");
		// }
		// }
		// }

		if (giftType.equals(Gift.TYPE_GIFT_GOLD_MEDALLION)) {
			giftCode = request.getParameter("gift");

		} else if (giftType.equals(Gift.TYPE_GIFT_FREE_MEMBERSHIP)) {
			giftCode = request.getParameter("freeMembership");
			String membershipNumber = request.getParameter("membershipNumber");
			if (membershipNumber == null || membershipNumber.equals("")) {
				throw new ValidationException("No associated membership number has been entered.");
			}
			// check member
			MembershipVO mem = membershipMgr.findMembershipByClientAndType(new Integer(membershipNumber), MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
			if (membership != null) {
				if (!mem.getStatus().equals(MembershipVO.STATUS_ACTIVE)) {
					throw new ValidationException("Specified membership is not active.");
				}
				if (mem.getMembershipID().equals(membership.getMembershipID())) {
					throw new ValidationException("Specified membership is the same as the member recording the gift.");
				}
			} else {
				throw new ValidationException("Specified membership does not exist.");
			}

			membershipGift.setMembershipNumber(membershipNumber);
		} else if (giftType.equals(Gift.TYPE_GIFT_ACCOMMODATION_GUIDE)) {
			giftCode = request.getParameter("accommodation");
		}
		mgPK.setGiftCode(giftCode);
		mgPK.setMembershipId(membershipId);
		mgPK.setStatus(status);
		mgPK.setTierCode(tierCode);

		membershipGift.setMembershipGiftPK(mgPK);

		membershipGift.setCreateDate(new DateTime());
		membershipGift.setNotes(notes);
		membershipGift.setUserId(userSession.getUserId());
		membershipGift.setSalesBranchCode(userSession.getUser().getSalesBranchCode());

		// check for duplicate gifts
		MembershipGift memGift = membershipMgr.getMembershipGift(mgPK);
		if (memGift != null) {
			throw new ValidationException("Membership gift has already been issued. Choose another gift type, or delete/return gift first.");
		}

		membershipMgr.createMembershipGift(membershipGift);

		request.setAttribute("membership", membership);
		request.setAttribute("client", membership.getClient());

		forwardRequest(request, response, MembershipUIConstants.PAGE_MANAGE_MEMBERSHIP_GIFTS);
	}

	public void handleEvent_deleteGift(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		outputRequestParameters(request);
		String selectedGift = request.getParameter("selectedGift");
		Integer membershipId = new Integer(request.getParameter("membershipId"));

		String gift[] = selectedGift.split(",");
		String giftCode = gift[1];
		String status = gift[2];
		String tierCode = gift[3];

		MembershipGiftPK membershipGiftPK = new MembershipGiftPK();
		membershipGiftPK.setMembershipId(membershipId);
		membershipGiftPK.setGiftCode(giftCode);
		membershipGiftPK.setStatus(status);
		membershipGiftPK.setTierCode(tierCode);

		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();

		MembershipGift membershipGift = membershipMgrLocal.getMembershipGift(membershipGiftPK);

		String notes = membershipGift.getNotes();

		if (membershipGift != null && (membershipGift.getMembershipGiftPK().getStatus().equals(Gift.STATUS_GIFT_ACTIVE) || membershipGift.getMembershipGiftPK().getStatus().equals(Gift.STATUS_GIFT_RETURNED))) {

			// delete
			membershipMgrLocal.removeMembershipGift(membershipGift.getMembershipGiftPK());

			String deleteNotes = request.getParameter("returnNotes");
			if (deleteNotes != null) {
				notes += FileUtil.SEPARATOR_PIPE + " " + deleteNotes;
			}

			membershipGift.getMembershipGiftPK().setStatus(Gift.STATUS_GIFT_DELETED);
			membershipGift.setNotes(notes + " Deleted on " + new DateTime());

			// create
			membershipMgrLocal.createMembershipGift(membershipGift);
		} else {
			throw new ValidationException("Gift not found or not active.");
		}

		MembershipVO membership = membershipMgrLocal.getMembership(membershipId);

		request.setAttribute("membership", membership);
		request.setAttribute("client", membership.getClient());

		// update to D
		forwardRequest(request, response, MembershipUIConstants.PAGE_MANAGE_MEMBERSHIP_GIFTS);
	}

	public void handleEvent_viewMembership_viewNotes(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		Integer membershipId = new Integer(request.getParameter("membershipId"));
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

		MembershipVO membership = membershipMgr.getMembership(membershipId);

		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
		Collection membershipNoteList = clientMgr.getClientNoteList(SourceSystem.MEMBERSHIP.getAbbreviation(), membershipId.toString());

		request.setAttribute("membership", membership);
		request.setAttribute("membershipNoteList", membershipNoteList);

		forwardRequest(request, response, MembershipUIConstants.PAGE_VIEW_MEMBERSHIP_NOTES);

	}

	public void handleEvent_scheduleCancelExpiredMembershipDocuments(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String cronExpression = request.getParameter("cronExpression");
		try {
			MembershipHelper.scheduleCancelExpiredMembershipDocuments(cronExpression);
		} catch (Exception e) {
			throw new ServletException(e);
		}
		forwardRequest(request, response, CommonUIConstants.PAGE_viewScheduledJobs);
	}

	public void handleEvent_returnGift(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		outputRequestParameters(request);
		String selectedGift = request.getParameter("selectedGift");
		Integer membershipId = new Integer(request.getParameter("membershipId"));

		LogUtil.debug(this.getClass(), "selectedGift=" + selectedGift);

		String gift[] = selectedGift.split(",");
		String giftCode = gift[1];
		String status = gift[2];
		String tierCode = gift[3];

		MembershipGiftPK membershipGiftPK = new MembershipGiftPK();
		membershipGiftPK.setMembershipId(membershipId);
		membershipGiftPK.setGiftCode(giftCode);
		membershipGiftPK.setStatus(status);
		membershipGiftPK.setTierCode(tierCode);

		MembershipMgrLocal membershipMgrLocal = MembershipEJBHelper.getMembershipMgrLocal();
		MembershipGift membershipGift = membershipMgrLocal.getMembershipGift(membershipGiftPK);
		String notes = membershipGift.getNotes();

		if (membershipGift != null && membershipGift.getMembershipGiftPK().getStatus().equals(Gift.STATUS_GIFT_ACTIVE)) {
			// check if returnable
			if (!membershipGift.getGift().isReturnable().booleanValue()) {
				throw new ValidationException("Gift may not be returned.");
			}

			// delete
			membershipMgrLocal.removeMembershipGift(membershipGiftPK);

			String returnNotes = request.getParameter("returnNotes");
			if (returnNotes != null) {
				notes += FileUtil.SEPARATOR_PIPE + " " + returnNotes;
			}

			membershipGift.getMembershipGiftPK().setStatus(Gift.STATUS_GIFT_RETURNED);
			membershipGift.setNotes(notes + " Returned on " + new DateTime());

			// create
			membershipMgrLocal.createMembershipGift(membershipGift);
		} else {
			throw new ValidationException("Gift not found or not active.");
		}

		MembershipVO membership = membershipMgrLocal.getMembership(membershipId);

		request.setAttribute("membership", membership);
		request.setAttribute("client", membership.getClient());

		// update to R
		forwardRequest(request, response, MembershipUIConstants.PAGE_MANAGE_MEMBERSHIP_GIFTS);
	}

	public void handleEvent_productList_viewMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		action_viewMembership(request, response, "No");
	}

	public void handleEvent_productList_viewQuote(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		action_viewQuoteDetail(request, response);
	}

	public void handleEvent_productList_viewQuoteList(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		action_viewQuoteList(request, response, "No");
	}

	public void handleEvent_undoTransaction_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		UndoTransactionGroup transGroup = MembershipSessionUtil.getUndoTransactionGroup(request);

		if (transGroup.isSubmitted()) {
			throw new ValidationException("The transaction has already been submitted.<p>You won't be able to see the result now so try looking at the membership to see if the changes have been made.");
		}

		Collection transactionList = transGroup.submit();
		request.setAttribute("transactionList", transactionList);

		// Remove the transaction container from the session now that we have
		// finished with it.
		MembershipSessionUtil.removeTransactionContainer(request, transGroup.getTransactionGroupKey());
		forwardRequest(request, response, MembershipUIConstants.PAGE_TransactionComplete);
	} // handleEvent_undoTransaction_btnSave

	public void handleEvent_viewClientSummary_createMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		action_createMembership(request, response, "Yes");
	} // handleEvent_productList_createMembership

	/**
	 * Handle the createGroup event raised by the client product list page.
	 */
	public void handleEvent_viewClientSummary_createGroup(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		// Get the client data
		String clientNumberStr = request.getParameter("clientNumber");
		action_createGroup(request, response, clientNumberStr, null, "Yes");
	} // handleEvent_productList_createMembership

	public void handleEvent_viewClientSummary_viewMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, SystemException {
		action_viewMembership(request, response, "Yes");
	}

	public void handleEvent_viewClientSummary_viewQuote(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		action_viewQuoteDetail(request, response);
	}

	public void handleEvent_viewClientSummary_viewQuoteList(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		action_viewQuoteList(request, response, "Yes");
	}

	/**
	 * Handle the submit button event on the select membership type page.
	 */
	public void handleEvent_selectMembershipType_btnNext(HttpServletRequest request, HttpServletResponse response) throws Exception {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		String pageName = MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS;

		// Get the selected membership type code.
		String memTypeCode = request.getParameter("membershipTypeCode");
		if (memTypeCode != null) {
			// Make sure the membership type is valid
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			MembershipTypeVO memTypeVO = refMgr.getMembershipType(memTypeCode);
			if (memTypeVO != null) {
				// If the membership type being created is "Personal",
				// and the transaction type is a Create then make sure that
				// the context client does not already have a personal
				// membership.
				// If they do then throw an exception.
				if (memTypeVO.getMembershipTypeCode().equals(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL) && transGroup.getTransactionGroupTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE)) {
					ClientVO clientVO = transGroup.getContextClient();
					if (MembershipHelper.clientHasPersonalMembership(clientVO.getClientNumber())) {
						throw new ValidationException("The client already has a personal membership. A new one cannot be created.");
					}
				}
				transGroup.setMembershipType(memTypeVO);
			} else {
				throw new SystemException("The selected membership type '" + memTypeCode + "' is not valid!");
			}
		} else {
			throw new ValidationException("A membership type must be selected.");
		}

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, pageName);
	} // handleEvent_selectMembershipType_btnNext

	/**
	 * Handle the event of clicking an add button for a client in the search result list. The selected clients value object should be in the client list stored in the session. Also get all of the other clients that are in the same membership group as the selected client. Add all of the clients to the transaction group if they are not already there. Redisplay the select group members page.
	 * 
	 * @todo Handle selecting clients from different groups. Not allowed.
	 */
	public void handleEvent_selectGroupMembers_btnAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		// Clean up the transaction list if the user has gone back a page.
		transGroup.clearTransactionList();
		// Change the prime addressee if required.
		String paSelectedClientKey = request.getParameter("paSelectedClientKey");
		transGroup.setPrimeAddresseeSelectedClientKey(paSelectedClientKey);
		// Get the client number for the client to add.
		String selectedClientKey = request.getParameter("selectedClientKey");

		// Get the selected client from the client search list which should be
		// stored in the session.
		Vector clientSearchList = transGroup.getClientSearchList();
		if (clientSearchList == null) {
			throw new SystemException("Unable to get the client list resulting from the search.");
		}
		// Find the client in the list
		SelectedClient selectedClient = null;
		boolean found = false;
		Iterator clientSearchListIT = clientSearchList.iterator();
		while (clientSearchListIT.hasNext() && !found) {
			selectedClient = (SelectedClient) clientSearchListIT.next();
			found = selectedClient.getKey().equals(selectedClientKey);
		}

		MembershipVO selectedMembership = selectedClient.getMembership();

		// Did we find the client in the list?
		if (!found) {
			throw new SystemException("Could not find the client in the client list.");
		}

		String message = null;
		String errorMessage = null;

		double amountOutstanding = 0;
		if (selectedMembership != null) {
			DateTime memGroupExpiryDate = transGroup.getMembershipGroupExpiryDate();
			try {
				amountOutstanding = selectedMembership.getAmountOutstanding(true, true);
			} catch (PaymentException ex1) {
				throw new SystemException(ex1);
			} catch (RemoteException ex2) {
				throw new SystemException(ex2);
			}
			if (amountOutstanding > 0) {
				errorMessage = "The client you are adding has an amount outstanding of " + CurrencyUtil.formatDollarValue(amountOutstanding) + "." + "<br/>The amount outstanding must be cleared before adding the member.";
			} else {
				message = transGroup.addSelectedClient(selectedClient);
				// Remove the selected client from the client search list.
				clientSearchList.remove(selectedClient);
			}
		} else {
			message = transGroup.addSelectedClient(selectedClient);
			// Remove the selected client from the client search list.
			clientSearchList.remove(selectedClient);
		}
		request.setAttribute("message", message);
		request.setAttribute("errorMessage", errorMessage);
		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectGroupMembers);

	} // handleEvent_selectMembershipType_btnAdd

	/**
	 * Handle the event of clicking a remove button for a client already selected in a group. Remove the clients membership and membership transaction from the transaction group and redisplay the select group members page.
	 */
	public void handleEvent_selectGroupMembers_btnRemove(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);

		// Clean up the transaction list if the user has gone back a page.
		transGroup.clearTransactionList();

		// Change the prime addressee if required.
		String paSelectedClientKey = request.getParameter("paSelectedClientKey");
		transGroup.setPrimeAddresseeSelectedClientKey(paSelectedClientKey);

		// Get the selected client to remove
		String selectedClientKey = request.getParameter("selectedClientKey");
		SelectedClient selectedClient = transGroup.getSelectedClient(selectedClientKey);
		if (selectedClient == null) {
			throw new SystemException("Failed to remove selected client with key = " + selectedClientKey + ". Selected client not found.");
		}

		// If the selected client is a member of the selected membership group
		// then flag them for removal from the membership group otherwise get
		// rid of them.
		if (selectedClient.getMembership() != null && transGroup.isMembershipInMembershipGroup(selectedClient.getMembership().getMembershipID())) {
			selectedClient.setRemovedFromGroup(true);
		} else {
			transGroup.removeSelectedClient(selectedClientKey);
		}

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectGroupMembers);
	} // handleEvent_selectGroupMembers_btnRemove

	/**
	 * Handle the event of clicking an Undo button for a selcted client.
	 */
	public void handleEvent_selectGroupMembers_btnUndo(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);

		// Clean up the transaction list if the user has gone back a page.
		transGroup.clearTransactionList();

		// Change the prime addressee if required.
		String paSelectedClientKey = request.getParameter("paSelectedClientKey");
		transGroup.setPrimeAddresseeSelectedClientKey(paSelectedClientKey);

		// Get the selected client key for the client to undo.
		String selectedClientKey = request.getParameter("selectedClientKey");

		// Get the selected client from the transaction group
		SelectedClient selectedClient = transGroup.getSelectedClient(selectedClientKey);
		if (selectedClient == null) {
			throw new SystemException("Failed to undo remove on selected client with key = " + selectedClientKey + ". Selected client not found.");
		}

		// Undo the the remove from group flag.
		selectedClient.setRemovedFromGroup(false);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectGroupMembers);
	} // handleEvent_selectMembershipType_btnUndo

	/**
	 * Handle the event of clicking an Undo button for a selcted client.
	 */
	// public void handleEvent_selectGroupMembers_paSelectedClientKey_clicked (
	// HttpServletRequest request, HttpServletResponse response )
	// throws ServletException, RemoteException, ValidationException
	// {
	// TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup (
	// request );
	//
	// // Clean up the transaction list if the user has gone back a page.
	// transGroup.clearTransactionList ();
	//
	// // Change the prime addressee if required.
	// String paSelectedClientKey = request.getParameter ( "paSelectedClientKey"
	// );
	// transGroup.setPrimeAddresseeSelectedClientKey ( paSelectedClientKey );
	//
	// request.setAttribute ( "transactionGroup", transGroup );
	// forwardRequest ( request, response,
	// MembershipUIConstants.PAGE_SelectGroupMembers );
	// } // handleEvent_selectMembershipType_btnUndo
	//

	/**
	 * Handle the search button click event on the select group members page. Get the search criteria entered and call the client manager to conduct the search. Redisplay the select group members page with the results.
	 */
	public void handleEvent_selectGroupMembers_btnSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);

		// Clean up the transaction list if the user has gone back a page.
		transGroup.clearTransactionList();

		// Change the prime addressee if required.
		String paSelectedClientKey = request.getParameter("paSelectedClientKey");
		transGroup.setPrimeAddresseeSelectedClientKey(paSelectedClientKey);

		String clientSearchType = request.getParameter("clientSearchType");
		String clientSearchValue = request.getParameter("clientSearchValue").trim();

		String message = null;
		int searchType = 0;

		try {
			searchType = Integer.parseInt(clientSearchType);
		} catch (NumberFormatException nfe) {
			throw new SystemException("The search type '" + clientSearchType + "' is not valid.", nfe);
		}

		Vector clientSearchList = new Vector();
		Collection clientSearchResultList = null; // A list of ClientVO objects.
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		// MembershipHome membershipHome =
		// MembershipEJBHelper.getMembershipHome();
		// switch (searchType){
		// case ClientUIConstants.SEARCHBY_CLIENT_NUMBER:
		// case ClientUIConstants.SEARCHBY_CLIENT_SURNAME:
		// case ClientUIConstants.SEARCHBY_POLICY_NUMBER:
		// case ClientUIConstants.SEARCHBY_PHONE_NUMBER:
		// case ClientUIConstants.SEARCHBY_SMART_SEARCH:
		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();

		// Catch validation exceptions and display them as a message on the page
		// Let all other exceptions propogate up.
		try {
			clientSearchResultList = clientMgr.searchForClient(searchType, clientSearchValue);
		} catch (Exception e) {
			// log("clientMgr.searchForClient() threw Exception : " +
			// e.getMessage());
			// See if there is a validation exception in the exception stack
			Throwable tmpException = e;
			while (message == null) {
				if (tmpException instanceof ValidationException) {
					message = tmpException.getMessage();
				} else if (tmpException instanceof RemoteException) {
					RemoteException re = (RemoteException) tmpException;
					tmpException = re.detail;
				} else if (tmpException instanceof TransactionRolledbackException) {
					TransactionRolledbackException trbe = (TransactionRolledbackException) tmpException;
					tmpException = trbe.detail;
				} else {
					throw new SystemException(e);
				}
			}
		}

		// Turn the list of clients into a list of SelectedClient objects.
		// For each client find any existing memberships of the same type
		// being transacted.
		if (clientSearchResultList != null) {
			ClientVO clientVO;
			SelectedClient selectedClient;
			Collection membershipList;
			Iterator membershipListIterator;
			MembershipVO membershipVO;
			String membershipType = transGroup.getMembershipType().getMembershipTypeCode();
			Iterator clientListIterator = clientSearchResultList.iterator();
			while (clientListIterator.hasNext()) {
				clientVO = (ClientVO) clientListIterator.next();
				membershipVO = membershipMgr.findMembershipByClientAndType(clientVO.getClientNumber(), membershipType);
				if (membershipVO != null) {
					selectedClient = new SelectedClient(membershipVO, new DateTime());
					clientSearchList.add(selectedClient);
				} else {
					// No memberships for the client so add the client to the
					// list.
					selectedClient = new SelectedClient(clientVO, new DateTime());
					clientSearchList.add(selectedClient);
				}
			}

		}

		// break;
		// case ClientUIConstants.SEARCHBY_MEMBERSHIP_NUMBER:
		// // Find a membership by the membership number.
		// // Try and convert the search value into an integer
		// Integer membershipNumber = Integer.valueOf(clientSearchValue);
		// try
		// {
		// Membership membership =
		// membershipHome.findByMembershipNumber(clientSearchValue.trim());
		// if (membership != null)
		// {
		// SelectedClient selectedClient = new
		// SelectedClient(membership.getVO(),new DateTime());
		// clientSearchList.add(selectedClient);
		// }
		// }
		// catch (FinderException fe)
		// {
		// throw new SystemException(fe);
		// }
		// break;
		// default:
		// throw new SystemException("Invalid client search type = " +
		// searchType);
		// }

		if (clientSearchList == null || clientSearchList.isEmpty()) {
			// Work out a message to display.
			// Don't overwrite the validation exception message set above
			// Otherwise make the message meaningful to the search type.
			if (message == null) {
				switch (searchType) {
				case ClientUIConstants.SEARCHBY_CLIENT_NUMBER:
					message = "Unable to find client number " + clientSearchValue + ".";
					break;
				case ClientUIConstants.SEARCHBY_CLIENT_SURNAME:
					message = "Unable to find client with surname " + clientSearchValue + ".";
					break;
				case ClientUIConstants.SEARCHBY_MEMBERSHIP_NUMBER:
					message = "Unable to find client with membership number " + clientSearchValue + ".";
					break;
				case ClientUIConstants.SEARCHBY_POLICY_NUMBER:
					message = "Unable to find client with policy number " + clientSearchValue + ".";
					break;
				case ClientUIConstants.SEARCHBY_HOME_PHONE:
					message = "Unable to find client with phone number " + clientSearchValue + ".";
					break;
				default:
					message = "Unable to find a matching client using '" + clientSearchValue + "'.";
				}
			}
		}

		// If the message is still null then we found something.
		if (message == null) {
			message = "Searched for \"" + clientSearchValue + "\".";
		}

		// Save the search type in the session so we can select it the next
		// time a client search page is displayed.
		request.getSession().setAttribute("searchType", new Integer(searchType));

		// Add the client list to the session so we can easily view full client
		// details when running down the search result list.
		transGroup.setClientSearchList(clientSearchList);

		request.setAttribute("searchValue", clientSearchValue);
		request.setAttribute("message", message);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectGroupMembers);
	} // handleEvent_selectGroupMembers_btnSearch

	/**
	 * Handle the event of saving changes to the group PA.
	 */
	public void handleEvent_changeGroupPA_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);

		// Get the new prime addressee of the group
		String paSelectedClientKey = request.getParameter("paSelectedClientKey");
		String oldPAClientKey = request.getParameter("oldPAClientKey");
		LogUtil.log(this.getClass(), "paSelectedClientKey = " + paSelectedClientKey);

		if (paSelectedClientKey.equals(oldPAClientKey)) {
			throw new SystemException("Unable to change prime addressee as the old prime addressee is the same as the new prime addressee.");
		}

		SelectedClient oldPA = transGroup.getSelectedClient(oldPAClientKey);

		LogUtil.log(this.getClass(), "paSelectedClientKey=" + paSelectedClientKey);

		// Set the new prime addressee of the group
		transGroup.setPrimeAddresseeSelectedClientKey(paSelectedClientKey);

		// Turn the list of selected clients into a list of membership
		// transactions
		transGroup.createTransactionsForSelectedClients();

		// outstanding amounts

		MembershipVO oldPAMemVO = oldPA.getMembership();
		try {
			double amountOutstanding = oldPAMemVO.getAmountOutstanding(true, true);
			if (amountOutstanding > 0) {
				throw new SystemException("Unable to change prime addressee as the original prime addressee has an outstanding amount of '" + CurrencyUtil.formatDollarValue(amountOutstanding) + ".'");
			}
		} catch (PaymentException ex) {
			throw new SystemException("Unable to determine if an amount outstanding exists for membership '" + oldPAMemVO.getMembershipNumber() + ".", ex);
		}

		Collection transList = transGroup.submit();
		LogUtil.log(this.getClass(), transList != null ? "There are " + transList.size() + " transactions." : "null");
		request.setAttribute("transactionList", transList);

		MembershipSessionUtil.removeTransactionContainer(request, transGroup.getTransactionGroupKey());
		// create new renewal document if required
		try {
			UserSession us = this.getUserSession(request);
			User user = us.getUser();
			MembershipRenewalMgr rMgr = MembershipEJBHelper.getMembershipRenewalMgr();
			SelectedClient sClt = transGroup.getSelectedClient(transGroup.getPaSelectedClientKey());

			rMgr.replaceRenewalNotice(oldPAMemVO, sClt.getMembership(), user);
		} catch (Exception e) {
			//
			e.printStackTrace();
		}
		forwardRequest(request, response, MembershipUIConstants.PAGE_TransactionComplete);
	} // handleEvent_selectGroupMembers_btnNext

	/**
	 * Handle the event of clicking the next button on the select group members page. Convert the list of selected clients into a list of membership transactions. There must be atleast one client selected to proceed. We don't need to check for more than one personal membership as the method will pick up the existing personal membership and modify it even though the transaction type indicates a create.
	 * 
	 * @todo fix setting of profile code.
	 */
	public void handleEvent_selectGroupMembers_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		// Get the new prime addressee of the group
		String paSelectedClientKey = request.getParameter("paSelectedClientKey");
		LogUtil.log(this.getClass(), "paSelectedClientKey = " + paSelectedClientKey);
		// first check that it hasn't been removed
		Collection selectedClientList = transGroup.getSelectedClientList();
		Iterator selectedClientListIterator = selectedClientList.iterator();
		SelectedClient selectedClient = null;
		while (selectedClientListIterator.hasNext()) {
			selectedClient = (SelectedClient) selectedClientListIterator.next();
			if (selectedClient.isRemovedFromGroup() && selectedClient.getKey().equals(paSelectedClientKey)) {
				throw new SystemException("Cannot remove prime addressee from group.");
			}
		}

		// Set the new prime addressee of the group
		transGroup.setPrimeAddresseeSelectedClientKey(paSelectedClientKey);

		// Turn the list of selected clients into a list of membership
		// transactions
		// transGroup.createTransactionsForSelectedClients();
		// transGroup.logMembershipTransactions("handleEvent_selectGroupMembers_btnNext");

		// Attach the transaction group to the request for easy reference by the
		// next page.
		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectProduct);
	} // handleEvent_selectGroupMembers_btnNext

	/**
	 * Handle the submit button event on the select product page.
	 */
	public void handleEvent_selectProduct_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		String prodCode = (String) request.getParameter("productSelected");
		ProductVO prodVO = refMgr.getProduct(prodCode);
		if (prodVO == null) {
			throw new SystemException("Failed to get product. The product '" + prodCode + "' is not valid.");
		}

		String rego = request.getParameter("rego");
		String vin = request.getParameter("vin");
		String make = request.getParameter("make");
		String model = request.getParameter("model");
		String year = request.getParameter("year");

		if (prodVO.isVehicleBased()) {
			if (rego == null || rego.isEmpty()) {
				throw new SystemException("Please provide a vehicle registration");
			}
			if (vin == null || vin.isEmpty()) {
				throw new SystemException("Please provide a vehicle VIN");
			}
			if (make == null || make.isEmpty()) {
				throw new SystemException("Please provide a vehicle make");
			}
			if (model == null || model.isEmpty()) {
				throw new SystemException("Please provide a vehicle model");
			}
			if (model == null || model.isEmpty()) {
				throw new SystemException("Please provide a vehicle model");
			}
			if (year == null || year.isEmpty()) {
				throw new SystemException("Please provide a vehicle year");
			}
		}

		transGroup.setSelectedProduct(prodVO);
		transGroup.createTransactionsForSelectedClients();

		// process the transaction
		Collection transactionList = transGroup.submit();
		if (transactionList == null) {
			throw new ValidationException("The transaction has already been submitted.<p>You won't be able to see the result so try looking at the membership to see if the changes have been made.");
		}

		request.setAttribute("transactionList", transactionList);
		request.setAttribute("contextClientVO", transGroup.getContextClient());

		// Remove the transaction group from the session now that we have
		// finished with it.
		MembershipSessionUtil.removeTransactionContainer(request, transGroup.getTransactionGroupKey());

		forwardRequest(request, response, MembershipUIConstants.PAGE_TransactionComplete);
	} // handleEvent_selectProduct_btnSave

	/**
	 * Handle the submit button event on the select product page.
	 */
	public void handleEvent_selectProduct_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		LogUtil.debug(this.getClass(), "in handleEvent_selectProduct_btnNext");
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		String transTypeCode = null;
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();

		String oldProductCode = transGroup.getSelectedProduct() != null ? transGroup.getSelectedProduct().getProductCode() : null;

		String prodCode = (String) request.getParameter("productSelected");

		String rego = request.getParameter("rego_" + prodCode);
		String vin = request.getParameter("vin_" + prodCode);
		String make = request.getParameter("make_" + prodCode);
		String model = request.getParameter("model_" + prodCode);
		String year = request.getParameter("year_" + prodCode);

		ProductVO prodVO = refMgr.getProduct(prodCode);
		if (prodVO == null) {
			throw new SystemException("Failed to get product '" + prodCode + "'.");
		}

		LogUtil.debug(this.getClass(), "prodCode=" + prodCode);
		LogUtil.debug(this.getClass(), "oldProductCode=" + oldProductCode);

		LogUtil.debug(this.getClass(), "transGroup.hasTransactions=" + transGroup.hasTransactions());

		transGroup.setSelectedProduct(prodVO);

		if (prodVO.isVehicleBased()) {
			if (rego == null || rego.isEmpty()) {
				throw new SystemException("Please provide a vehicle registration");
			}
			if (vin == null || vin.isEmpty()) {
				throw new SystemException("Please provide a vehicle VIN");
			}
			if (make == null || make.isEmpty()) {
				throw new SystemException("Please provide a vehicle make");
			}
			if (model == null || model.isEmpty()) {
				throw new SystemException("Please provide a vehicle model");
			}
			if (model == null || model.isEmpty()) {
				throw new SystemException("Please provide a vehicle model");
			}
			if (year == null || year.isEmpty()) {
				throw new SystemException("Please provide a vehicle year");
			}
		}

		// Depending on how we got to this page we may or may not have created the membership transactions yet.
		// Create, renew, rejoin create the transactions before this page is shown.
		// Change base product does not.
		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		String cacheTxnsStr = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.CACHE_TRANSACTIONS);
		boolean createTxns = false;
		boolean cacheTxns = true;
		if (cacheTxnsStr != null) {
			cacheTxns = cacheTxnsStr.equalsIgnoreCase("Y");
		}

		if (!cacheTxns) { // if no caching - generate
			createTxns = true;
		} else if (!transGroup.hasTransactions()) { // if we are caching, and no
			// txns yet - generate
			createTxns = true;
		}

		if (createTxns) {
			LogUtil.log(this.getClass(), "creating transactions handleEvent_selectProduct_btnNext.");
			transGroup.createTransactionsForSelectedClients();
		}

		MembershipTransactionVO memTransVO = null;
		MembershipTransactionVO newMemTransVO = null;
		Vector memTransList = transGroup.getTransactionList();
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;

		// Loop through the membership transactions and look for a product
		// change
		// that is being recorded as a renew or rejoin MEM-7
		for (int transIndex = 0; transIndex < memTransList.size(); transIndex++) {
			LogUtil.debug(this.getClass(), "transList");
			memTransVO = (MembershipTransactionVO) memTransList.get(transIndex);
			transTypeCode = memTransVO.getTransactionTypeCode();
			LogUtil.debug(this.getClass(), "transTypeCode=" + transTypeCode);
			// if renew or rejoin and the product has changed then replace it in the list
			// may indicate a create group but regardless we need to be able to determine a change via the old product code
			if ((MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(memTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(memTransVO.getTransactionTypeCode())) && oldProductCode != null && !prodVO.getProductCode().equals(oldProductCode) && (memTransVO.getMembership() != null) && !MembershipVO.STATUS_UNDONE.equals(memTransVO.getMembership().getStatus())) {
				// previous state not undone
				LogUtil.debug(this.getClass(), "renew tx");
				oldMemVO = memTransVO.getMembership();
				newMemVO = memTransVO.getNewMembership();

				// only change groups as individual will require an explicit
				// change product transaction.
				if (newMemVO.isGroupMember()) {
					// We need to change the default transaction type. MEM-7 16/08/2006
					newMemTransVO = new MemTransChangeProduct(transGroup.getTransactionGroupTypeCode(), memTransVO.getTransactionTypeCode(), transGroup.getUser(), transGroup.isRejoinTypeNew(), oldMemVO, newMemVO, transGroup.getSelectedProduct(), transGroup.getMembershipTerm(), memTransVO.getMembershipGroupJoinDate(), transGroup.getTransactionDate(), memTransVO.isPrimeAddressee(), transGroup.getPreferredCommenceDate(), transGroup.getExpiryDate());

					// Replace the membership transaction in the list with the new one.
					memTransList.set(transIndex, newMemTransVO);
				}
			}

			if (prodVO.isVehicleBased()) {
				newMemVO = memTransVO.getNewMembership();
				newMemVO.setRego(rego);
				newMemVO.setVin(vin);
				newMemVO.setMake(make);
				newMemVO.setModel(model);
				newMemVO.setYear(year);
			}
		}

		// Check if the transaction group is affiliated club
		if (transGroup instanceof TransactionGroupAffiliatedClubTransferImpl) {
			// check that the product is appropriate to the affiliated product
			// level.
			TransactionGroupAffiliatedClubTransferImpl transGroupAff = (TransactionGroupAffiliatedClubTransferImpl) transGroup;
			String affiliateProductLevel = transGroupAff.getAffiliatedProductLevel();

			// get products by product level
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			Collection products = membershipMgr.getProducts(affiliateProductLevel);
			if (products.isEmpty() || !products.contains(prodVO)) {
				throw new SystemException("The selected product '" + prodVO.getProductCode() + "' is not available for the affiliated product level of '" + affiliateProductLevel + "'.");
			}
		}

		transGroup.logMembershipTransactions("selectProduct.jsp");

		// Decide on which page to go to next.
		// If the product has options that can be selected then go to the select product option page.
		// If there are no product options and there are discounts that can be selected for the product
		// then go to the group discount page.
		// The product fees are created after the product options have been selected.
		// The automatic discounts must be created after the group discount page has been shown
		UserSession userSession = getUserSession(request);
		if (prodVO.hasOptionalBenefits(userSession.getUser())) {
			// Show the product options if there are any optional product benefits selectable by the user.
			request.setAttribute("transactionGroup", transGroup);
			forwardRequest(request, response, MembershipUIConstants.PAGE_SelectProductOptions);
		} else {
			// No optional benefits for the product so create the fees
			// and see if we can go to a discount page
			transGroup.createTransactionFees();
			double netFee = transGroup.getNetTransactionFee();
			if (netFee > 0.0 && transGroup.canHaveDiscounts(false)) {
				request.setAttribute("transactionGroup", transGroup);
				forwardRequest(request, response, MembershipUIConstants.PAGE_NonDiscretionaryDiscount);
			} else if (netFee > 0.0) {
				request.setAttribute("transactionGroup", transGroup);
				forwardRequest(request, response, MembershipUIConstants.PAGE_DiscretionaryDiscount);
			} else if (netFee > 0.0 && transGroup.canAttachQuote()) {
				request.setAttribute("transactionGroup", transGroup);
				forwardRequest(request, response, MembershipUIConstants.PAGE_SelectTransactionQuote);
			} else {
				showPage_confirmTransaction(request, response, transGroup);
			}
		}
	} // handleEvent_selectProduct_btnNext

	/**
	 * Handle the next button event on the select product options page.
	 */
	public void handleEvent_selectProductOptions_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		LogUtil.log(this.getClass(), "in handleEvent_selectProductOptions_btnNext");
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();

		Vector memTransList = transGroup.getTransactionList();
		MembershipTransactionVO memTransVO = null;
		MembershipTransactionVO newMemTransVO = null;
		String transTypeCode;
		MembershipVO newMemVO = null, oldMemVO = null;
		Integer clientNumber = null;
		ProductVO productVO = transGroup.getSelectedProduct();
		Collection optionalBenefitList = productVO.getProductBenefitList(true, true);
		Iterator optionalBenefitListIT = null;
		ProductBenefitTypeVO prodBenefitTypeVO = null;
		Iterator memberProductOptionListIT = null;
		Collection memberProductOptionList = null;
		MembershipProductOption memProdOption = null;
		String checkBoxName = null;
		String checkBoxValue = null;

		// Loop through the membership transactions and look for each of the
		// optional product benefits on a per client basis
		for (int transIndex = 0; transIndex < memTransList.size(); transIndex++) {
			memTransVO = (MembershipTransactionVO) memTransList.get(transIndex);
			transTypeCode = memTransVO.getTransactionTypeCode();
			// Can't change the product options on a membership being removed
			// froma group
			if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(transTypeCode)) {
				oldMemVO = memTransVO.getMembership();
				newMemVO = memTransVO.getNewMembership();

				// Remove any existing product options first.
				newMemVO.clearProductOptionList();
				clientNumber = newMemVO.getClientNumber();
				optionalBenefitListIT = optionalBenefitList.iterator();
				// For each client loop through the optional product benefits
				// and look for a selected product benefit.
				while (optionalBenefitListIT.hasNext()) {
					prodBenefitTypeVO = (ProductBenefitTypeVO) optionalBenefitListIT.next();
					checkBoxName = "pb_" + clientNumber + "_" + HTMLUtil.cleanString(prodBenefitTypeVO.getProductBenefitCode());
					checkBoxValue = request.getParameter(checkBoxName);

					if ("YES".equalsIgnoreCase(checkBoxValue)) { // If the check
						// box does not
						// exist the
						// check box
						// value will
						// be null.
						// Comparing a
						// string to a
						// null won't
						// throw an
						// exception.
						// Create a
						// membership
						// product
						// option
						// object.
						// Setting the
						// product
						// benefit will
						// also set the
						// product
						// benefit type
						// code.
						memProdOption = new MembershipProductOption(prodBenefitTypeVO, new DateTime());

						// If the transaction type is one of the following and
						// the selected
						// product option does not exist on the old membership
						// then we need
						// to change the transaction type so that fees are
						// created.
						// The following transaction types only indicate that
						// the membership
						// was involved in a membership group that changed but
						// the membership
						// itself did not change. These transaction types are
						// ignored when
						// creating fees.

						if ((MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(transTypeCode)) && !oldMemVO.hasProductOption(memProdOption.getProductBenefitTypeCode())) {
							// We need to change the transaction type.
							newMemTransVO = new MemTransChangeProductOption(transGroup.getTransactionGroupTypeCode(), transGroup.getTransactionDate(), oldMemVO, transGroup.getUser(), memTransVO.isPrimeAddressee(), memTransVO.getMembershipGroupJoinDate());
							// Changing the transaction type creates a new
							// instance
							// of the new membership VO.
							newMemVO = newMemTransVO.getNewMembership();
							// Replace the membership transaction in the list
							// with the new one.
							memTransList.set(transIndex, newMemTransVO);
						}

						newMemVO.addProductOption(memProdOption);
					}
				}
			}
		}
		transGroup.setProductEffectiveDates();
		transGroup.createTransactionFees();

		double netFee = transGroup.getNetTransactionFee();

		if (netFee > 0.0 && transGroup.canHaveDiscounts(false)) {
			request.setAttribute("transactionGroup", transGroup);
			forwardRequest(request, response, MembershipUIConstants.PAGE_NonDiscretionaryDiscount);
		} else if (netFee > 0.0) {
			request.setAttribute("transactionGroup", transGroup);
			forwardRequest(request, response, MembershipUIConstants.PAGE_DiscretionaryDiscount);
		} else {
			showPage_confirmTransaction(request, response, transGroup);
		}
	} // handleEvent_selectProductOptions_btnNext

	/**
	 * Handle the submit button event on the select membership options page.
	 */
	public void handleEvent_selectMembershipOptions_btnNext(HttpServletRequest request, HttpServletResponse response) throws Exception {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		String transTypeCode = transGroup.getTransactionGroupTypeCode();
		DateTime transDate = transGroup.getTransactionDate();
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

		// LogUtil.log(this.getClass(),"credit="+transGroup.getMembershipGroupPA().getCreditAmount());

		// force the transactions to be recreated
		if (transGroup.hasTransactions()) {
			transGroup.clearTransactionList();
		}

		DateTime expiryDate = null;
		Interval membershipTerm = null;

		// Get the selected membership term
		// Only applicable to a create or rejoin transaction for a single
		// membership.
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) {
			// membershipTerm = transGroup.getMembershipTerm();

			// The membership term is specified in months on the page.
			String termText = null;
			try {
				termText = request.getParameter("membershipTerm");
				int months = Integer.parseInt(termText);
				membershipTerm = new Interval(0, months, 0, 0, 0, 0, 0);
			} catch (Exception e) {
				throw new ValidationException("The term of membership (" + termText + ") is not valid.");
			}
			transGroup.setMembershipTerm(membershipTerm);

		}

		LogUtil.log(this.getClass(), "membership term=" + transGroup.getMembershipTerm());

		// Get the membership profile
		String profileCode = request.getParameter("profile");
		if (profileCode != null && profileCode.length() > 0) {
			transGroup.setMembershipProfile(profileCode);

			// Get the preferred commence date.
			// Only applicable to a create or rejoin transaction for a single
			// membership.
			// Only applicable to memberships with a normal client profile.
		}
		if ((MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) && MembershipProfileVO.PROFILE_CLIENT.equals(transGroup.getMembershipProfile().getProfileCode())) {
			// Parse the preferred commence date and set the expiry date
			String preferredCommenceDateText = request.getParameter("preferredCommenceDate");
			DateTime preferredCommenceDate = null;
			if (preferredCommenceDateText != null) {
				try {
					preferredCommenceDate = DateUtil.convertStringToDateTime(preferredCommenceDateText);
					if (preferredCommenceDate == null) {
						throw new ValidationException("The preferred commence date is not valid");
					}
				} catch (ParseException pe) {
					throw new ValidationException("The preferred commence date is not valid : " + pe.getMessage());
				}
				LogUtil.log(this.getClass(), "setPreferredCommenceDate=" + preferredCommenceDate);
				// The preferred commence date cannot be more than two months
				// into the future.
				// This check is done in the membership transaction.
				transGroup.setPreferredCommenceDate(preferredCommenceDate);
			}

			// Calculate and set the expiry date. If the preferred commence date
			// was not set then the transaction date will be used instead.
			LogUtil.log(this.getClass(), "membership term=" + transGroup.getMembershipTerm());
			LogUtil.log(this.getClass(), "transGroup.getPreferredCommenceDate()=" + transGroup.getPreferredCommenceDate());
			expiryDate = transGroup.getPreferredCommenceDate().add(membershipTerm);
			transGroup.setExpiryDate(expiryDate);
		}

		// Set the transaction reason
		String reasonCode = request.getParameter("reasonCode");

		if (reasonCode != null) {
			ReferenceDataVO reasonVO = null;
			if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode)) {
				reasonVO = refMgr.getJoinReason(reasonCode);
				if (reasonVO == null) {
					throw new SystemException("Failed to get join reason '" + reasonCode + "'");
				}
				transGroup.setTransactionReason(reasonVO);
			} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) {
				reasonVO = refMgr.getRejoinReason(reasonCode);
				if (reasonVO == null) {
					throw new SystemException("Failed to get rejoin reason '" + reasonCode + "'");
				}
				transGroup.setTransactionReason(reasonVO);
			}
		}
		LogUtil.log(this.getClass(), "reasonCode=" + reasonCode);
		// Get the rejoin type
		// Only applicable to a rejoin transaction for a single membership.
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) {
			String rejoinTypeCode = request.getParameter("rejoinTypeCode");
			LogUtil.log(this.getClass(), "rejoinTypeCode=" + rejoinTypeCode);
			if (rejoinTypeCode == null || rejoinTypeCode.length() < 1) {
				throw new SystemException("The rejoin type code is not valid.");
			}

			ReferenceDataVO rejoinTypeVO = refMgr.getRejoinType(rejoinTypeCode);
			if (rejoinTypeVO == null) {
				throw new SystemException("Failed to get rejoin type '" + rejoinTypeCode + "'");
			}

			transGroup.setRejoinType(rejoinTypeVO);
		}
		transGroup.logMembershipTransactions("selectMembershipOptions.jsp");
		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectProduct);
	} // handleEvent_selectMembershipOptions_btnNext

	public void handleEvent_selectTransactionQuote_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		String selectedQuoteNumber = request.getParameter("selectedQuote");
		Integer quoteNumber = new Integer(selectedQuoteNumber);
		if (quoteNumber.intValue() > 0) {
			MembershipQuote memQuote = MembershipEJBHelper.getMembershipMgr().getMembershipQuote(quoteNumber);
			if (memQuote == null) {
				throw new SystemException("Failed to get membership quote " + quoteNumber + ".");
			}
			transGroup.setAttachedQuote(memQuote);
		} else {
			transGroup.setAttachedQuote(null);
		}
		showPage_confirmTransaction(request, response, transGroup);
	}

	/**
	 * The user has set the member's individual discounts and is to move on to the discretionary discounts.
	 */
	public void handleEvent_nonDiscretionaryDiscount_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		// store the selections from the Member Discount screen
		// get the group transaction
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		Collection distinctFeeTypeList = null;
		Iterator distinctFeeTypeListIterator = null;
		ArrayList selectedDiscountList = new ArrayList();
		ArrayList unselectedDiscountList = new ArrayList();
		Iterator selectedDiscountListIterator = null;
		Iterator unselectedDiscountListIterator = null;
		DiscountTypeVO discTypeVO = null;
		String feeTypeCode = null;
		FeeTypeVO feeTypeVO = null;
		Collection allowedDiscountList = null;
		Iterator allowedDiscountListIterator = null;
		Integer clientNumber = null;
		String discountSelected;
		String discountLabel = null;

		// get the Membership Transactions
		Collection membershipTxList = transGroup.getTransactionList();

		double netFee = 0;

		// for each membership transaction
		Iterator membershipTxListIterator = membershipTxList.iterator();
		while (membershipTxListIterator.hasNext()) {

			// get the transaction fee list from the transaction VO
			MembershipTransactionVO membershipTxVO = (MembershipTransactionVO) membershipTxListIterator.next();
			LogUtil.log(this.getClass(), "membershipTxVO=" + membershipTxVO);

			clientNumber = membershipTxVO.getNewMembership().getClientNumber();

			selectedDiscountList.clear();
			unselectedDiscountList.clear();

			distinctFeeTypeList = membershipTxVO.getDistinctFeeTypeList();

			// for each membership transaction fee
			if (distinctFeeTypeList != null) {
				distinctFeeTypeListIterator = distinctFeeTypeList.iterator();
				while (distinctFeeTypeListIterator.hasNext()) {
					feeTypeCode = (String) distinctFeeTypeListIterator.next();
					// get the current discounts for this fee code

					feeTypeVO = refMgr.getFeeType(feeTypeCode);
					LogUtil.log(this.getClass(), "feeTypeVO=" + feeTypeVO);

					netFee = membershipTxVO.getNetTransactionFee(feeTypeCode);

					LogUtil.log(this.getClass(), "netFee=" + netFee);

					allowedDiscountList = refMgr.getAllowableDiscountList(feeTypeCode, new DateTime());
					if (allowedDiscountList != null) {
						allowedDiscountListIterator = allowedDiscountList.iterator();
						while (allowedDiscountListIterator.hasNext()) {
							discTypeVO = (DiscountTypeVO) allowedDiscountListIterator.next();
							LogUtil.log(this.getClass(), "discTypeVO=" + discTypeVO);
							LogUtil.log(this.getClass(), "isAuto=" + discTypeVO.isAutomaticDiscount());

							// Automatic discounts don't appear on the page so
							// don't remove them if they can't be found.
							// if netfee for the fee is 0 before discounts then
							// don't modify existing discounts
							if (!discTypeVO.isAutomaticDiscount()) {

								membershipTxVO.removeDiscountForFeeType(feeTypeCode, discTypeVO.getDiscountTypeCode());
								netFee = membershipTxVO.getNetTransactionFee(feeTypeCode);

								LogUtil.log(this.getClass(), "netFee=" + netFee);

								if (netFee > 0) {

									discountLabel = MembershipUIHelper.getDiscountLabel(clientNumber, feeTypeCode, discTypeVO.getDiscountTypeCode());
									discountSelected = request.getParameter(discountLabel);

									LogUtil.log(this.getClass(), "discountLabel=" + discountLabel);
									LogUtil.log(this.getClass(), "discountSelected=" + discountSelected);

									// If the discount type was selected then
									// add it to the membership
									// transaction fee for the fee type.
									// Otherwise remove it.
									// If the selected discount type is a
									// recurring type of discount
									// then add it to the list of selected
									// recurring discounts.
									// Otherwise add it to the list of
									// unselected recurring discounts.
									if ("Yes".equals(discountSelected)) {

										LogUtil.log(this.getClass(), "adding discount for fee [" + feeTypeCode + "] [" + discTypeVO + "]");

										membershipTxVO.addTransactionFeeDiscount(feeTypeCode, discTypeVO);

										// attributes
										LogUtil.debug(this.getClass(), "getDiscountAttributes");
										Collection discountAttributes = discTypeVO.getDiscountAttributes();
										LogUtil.debug(this.getClass(), discTypeVO + ", attr=" + discountAttributes.size());
										if (discountAttributes != null && !discountAttributes.isEmpty()) {
											String attributeName = null;
											String attributeParameterName = null;
											String attributeValue = null;
											Iterator daIt = discountAttributes.iterator();
											while (daIt.hasNext()) {
												MembershipDiscountAttribute mda = (MembershipDiscountAttribute) daIt.next();
												LogUtil.debug(this.getClass(), "mda=" + mda);
												attributeName = mda.getMembershipDiscountAttributePK().getAttributeName();
												attributeParameterName = MembershipUIHelper.getDiscountAttributeLabel(clientNumber, feeTypeCode, discTypeVO.getDiscountTypeCode(), mda.getMembershipDiscountAttributePK().getAttributeName());
												attributeValue = request.getParameter(attributeParameterName);
												LogUtil.debug(this.getClass(), attributeName + ": " + attributeValue);
												// add transaction fee discount
												// attributes if indicated
												TransactionFeeDiscountAttribute tfda = new TransactionFeeDiscountAttribute();
												TransactionFeeDiscountAttributePK transactionFeeDiscountAttributePK = new TransactionFeeDiscountAttributePK();

												transactionFeeDiscountAttributePK.setDiscountTypeCode(discTypeVO.getDiscountTypeCode());
												transactionFeeDiscountAttributePK.setTransactionID(membershipTxVO.getTransactionID());
												/**
												 * @todo need to set fee spec (set on add to discount)
												 */
												transactionFeeDiscountAttributePK.setAttributeName(attributeName);
												tfda.setTransactionFeeDiscountAttributePK(transactionFeeDiscountAttributePK);
												tfda.setAttributeValue(attributeValue);
												LogUtil.log(this.getClass(), "adding transaction fee discount attribute ");
												// add the discount attribute to
												// the transaction
												membershipTxVO.addTransactionFeeDiscountAttribute(discTypeVO, tfda);
												LogUtil.log(this.getClass(), "added transaction fee discount attribute " + tfda);
											}
										}

										LogUtil.log(this.getClass(), "1");

										if (discTypeVO.isRecurringDiscount()) {
											LogUtil.log(this.getClass(), "adding to selected " + discTypeVO);
											selectedDiscountList.add(discTypeVO);
										}
									} else {
										LogUtil.log(this.getClass(), "removing discount for fee=" + feeTypeCode + " " + discTypeVO.getDiscountTypeCode());
										membershipTxVO.removeDiscountForFeeType(feeTypeCode, discTypeVO.getDiscountTypeCode());
										if (discTypeVO.isRecurringDiscount()) {
											LogUtil.log(this.getClass(), "adding to unselected " + discTypeVO);
											unselectedDiscountList.add(discTypeVO);
										}
									}
								}
							}
						}
					}
				}
			}

		}

		LogUtil.log(this.getClass(), "2");

		// determine the next page to call
		if (transGroup.getNetTransactionFee() > 0.0) {
			LogUtil.log(this.getClass(), "3");
			request.setAttribute("transactionGroup", transGroup);
			forwardRequest(request, response, MembershipUIConstants.PAGE_DiscretionaryDiscount);
		} else if (transGroup.getNetTransactionFee() > 0.0 && transGroup.canAttachQuote()) {
			request.setAttribute("transactionGroup", transGroup);
			forwardRequest(request, response, MembershipUIConstants.PAGE_SelectTransactionQuote);
		} else {
			showPage_confirmTransaction(request, response, transGroup);
		}
	} // handleEvent_nonDiscretionaryDiscount_btnNext

	/**
	 * The user has set the reason for requesting a new card and any discounts discretionary discounts. similar to "handleEvent_discretionaryDiscount_btnNext" which follows
	 */
	public void handleEvent_membershipCardRequest_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		String membershipID = request.getParameter("membershipID");
		String requestReason = request.getParameter("selReason");
		UserSession userSession = getUserSession(request);
		MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
		MembershipVO memVO = memMgr.getMembership(new Integer(membershipID));

		TransactionHelper transactionHelper = new TransactionHelper();
		// based on reason use the current tier or renewal tier
		transactionHelper.createMembershipCardRequest(memMgr, memVO, requestReason, userSession.getUserId(), true);

		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewCards);
	}

	// public void handleEvent_uploadMembershipFile(HttpServletRequest request,
	// HttpServletResponse response)
	// throws ServletException, SystemException
	// {
	//
	// try
	// {
	//
	// FileItem fileItem = (FileItem)request.getAttribute("upfile");
	// BufferedReader br = new BufferedReader(new
	// InputStreamReader(fileItem.getInputStream()));
	// String line = null;
	// ArrayList membershipList = new ArrayList();
	// String clientNumber = null;
	// MembershipVO mem = null;
	// MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	// while ((line = br.readLine()) != null)
	// {
	// clientNumber = line.split(",")[0];
	// mem = (MembershipVO)membershipMgr.findMembershipByClientNumber(new
	// Integer(clientNumber)).iterator().next();
	// membershipList.add(mem);
	// }
	//
	// RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	// renewalMgr.constructSMSRenewalReminders(null, membershipList);
	//
	// }
	// catch(Exception ex)
	// {
	// ex.printStackTrace();
	// throw new ServletException(ex);
	// }
	//
	// this.forwardRequest(request,response,"/membership/UploadMembershipFile.jsp");
	// }

	/**
	 * The user has set the discretionary discounts and is to move on.
	 */
	public void handleEvent_discretionaryDiscount_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		double discountAmount = 0.0;
		String discountAmountText = request.getParameter("discountAmountText");
		String discountReason = request.getParameter("discountReason");
		if (discountAmountText != null && discountAmountText.length() > 0) {
			try {
				discountAmount = Double.parseDouble(discountAmountText);
			} catch (Exception e) {
				throw new ValidationException("The discount amount '" + discountAmountText + "' is not valid.");
			}
			if (discountAmount > 0.0 && (discountReason == null || discountReason.length() < 1)) {
				throw new ValidationException("A discount reason must be entered.");
			}

			// Can't give a greater discretionary discount than the maximum
			// allowed.
			double maxDiscretionaryDiscountAmount = transGroup.getMaxDiscretionaryDiscountAmount();
			if (discountAmount > maxDiscretionaryDiscountAmount) {
				discountAmount = maxDiscretionaryDiscountAmount;
			}
		} // if

		// Set the discretionary discount whether it was entered or not.
		// If a discount was entered then this will cause a discretionary
		// discount
		// adjustment to be added to the transaction (or replace an existing
		// one).
		// If no discount was entered then this will cause the discretionary
		// discount
		// adjustment to be removed.
		transGroup.setDiscretionaryDiscount(discountAmount, discountReason);

		if (transGroup.getNetTransactionFee() > 0.0 && transGroup.canAttachQuote()) {
			request.setAttribute("transactionGroup", transGroup);
			forwardRequest(request, response, MembershipUIConstants.PAGE_SelectTransactionQuote);
		} else {

			// transGroup.logMembershipTransactions("to confirm transaction screen");
			showPage_confirmTransaction(request, response, transGroup);
		}
	} // handleEvent_DiscretionaryDiscount_btnNext

	/**
	 * Handle the save button event on the confirm transaction page. Process the transaction and save it to the database. Show the transaction complete page.
	 * @throws ParseException 
	 */
	public void handleEvent_confirmTransaction_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException, ParseException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		Vector<MembershipTransactionVO> memTransList = transGroup.getTransactionList();

		// vv This code block is a workaround for non-motoring -> Lifestyle change ONLY vv
		String newExpiryDate = request.getParameter("expiryDate");
		String newEffectiveDate = request.getParameter("effectiveDate");
		boolean isNewExpiryDate = request.getParameter("newExpiryDate").equals("true");
		
		DateTime newJavaExpiryDate = null;
		DateTime newJavaEffectiveDate = null;

		if (isNewExpiryDate) {
			newJavaExpiryDate = new DateTime(newExpiryDate);
			newJavaEffectiveDate = new DateTime(newEffectiveDate);
			transGroup.setExpiryDate(newJavaExpiryDate);
			transGroup.setFeeEffectiveDate(newJavaEffectiveDate);
		}
		// ^^ This code block is a workaround for non-motoring -> Lifestyle change ONLY ^^

		for (MembershipTransactionVO memTransVO : memTransList) {
			if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(memTransVO.getTransactionTypeCode())) {
				if (memTransVO.getNewMembership().getProduct().isVehicleBased()) {
					throw new ValidationException("This type of membership cannot be rejoined. You will need to select another product type.");
				}
			}

			// vv This code block is a workaround for non-motoring -> Lifestyle change ONLY vv
			if (isNewExpiryDate) {
				memTransVO.setEndDate(newJavaExpiryDate);
				memTransVO.setEffectiveDate(newJavaEffectiveDate);
//				memTransVO.setProductEffectiveDate();
			}
			// ^^ This code block is a workaround for non-motoring -> Lifestyle change ONLY ^^
		}

		Collection transactionList = transGroup.submit();
		if (transactionList == null) {
			throw new ValidationException("The transaction has already been submitted. You won't be able to see the result so try looking at the membership to verify if the changes have been made.");
		}

		request.setAttribute("transactionList", transactionList);
		request.setAttribute("contextClientVO", transGroup.getContextClient());

		// Remove the transaction group from the session now that we have
		// finished with it.
		MembershipSessionUtil.removeTransactionContainer(request, transGroup.getTransactionGroupKey());

		// create new renewal document if required
		try {
			MembershipVO memVO = transGroup.getMembershipGroupPA();
			UserSession us = this.getUserSession(request);
			User user = us.getUser();
			MembershipRenewalMgr rMgr = MembershipEJBHelper.getMembershipRenewalMgr();
			rMgr.replaceRenewalNotice(memVO, memVO, user);
		} catch (Exception e) {
			//
			e.printStackTrace();
		}

		forwardRequest(request, response, MembershipUIConstants.PAGE_TransactionComplete);
	}

	public void handleEvent_createAccessMemberships(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		outputRequestParameters(request);

		String effDate = request.getParameter("effectiveDate");
		DateTime effectiveDate = null;
		try {
			effectiveDate = new DateTime(effDate);
		} catch (ParseException ex) {
			// ignore
		}

		String createMemberships = request.getParameter("createMemberships");
		boolean createMembership = Boolean.valueOf(createMemberships).booleanValue();

		String message = null;
		String heading = "Create Access Memberships";
		String runType = request.getParameter("runType");
		UserSession userSession = getUserSession(request);

		if (runType != null && runType.equalsIgnoreCase(ScheduleMgr.MODE_RUN_NOW)) {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			membershipMgr.createEligibleAccessMembershipClients(effectiveDate, createMembership, -1, userSession.getUserId());
			message = "Access memberships have now being created for effective date " + effectiveDate + ". Please check files for results.";
		} else {
			try {
				DateTime scheduledDate = MembershipHelper.scheduleCreateAccessMemberships(request);
				message = "The Access membership creation process has been scheduled for " + scheduledDate.formatLongDate() + ".";
			} catch (Exception ex1) {
				message = "Unable to schedule the Access membership creation process " + ex1;
			}
		}

		request.setAttribute("heading", heading);
		request.setAttribute("message", message);

		forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

	}

	public void handleEvent_produceGMVoucherLetters(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		String effDate = request.getParameter("effectiveDate");
		DateTime effectiveDate = null;
		try {
			effectiveDate = new DateTime(effDate);
		} catch (ParseException ex) {
			// ignore
		}
		//
		String runType = request.getParameter("runType");

		String message = null;
		String heading = "GM Voucher Letters";

		if (runType != null && runType.equalsIgnoreCase(ScheduleMgr.MODE_RUN_NOW)) {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			membershipMgr.produceGMVoucherLetters(effectiveDate);
			message = "The GM voucher letter process is now running.";
		} else {
			try {
				DateTime scheduledDate = MembershipHelper.scheduleGMVoucherLetters(request);
				message = "The GM voucher letter process has been scheduled for " + scheduledDate.formatLongDate() + ".";
			} catch (Exception ex1) {
				message = "Unable to schedule the GM voucher letter process " + ex1;
			}
		}

		request.setAttribute("heading", heading);
		request.setAttribute("message", message);

		forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

	}

	public void handleEvent_confirmTransaction_btnSaveAsQuote(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		if (transGroup == null) {
			throw new ExpiredSessionException("The transaction cannot be found.");
		}

		// Strip out any quote adjustment before saving as a quote.
		MembershipTransactionAdjustment quoteAdjustment = transGroup.getAdjustment(AdjustmentTypeVO.QUOTE_ADJUSTMENT);
		MembershipTransactionAdjustment quoteDiscount = transGroup.getAdjustment(AdjustmentTypeVO.QUOTE_DISCOUNT);

		transGroup.removeAdjustment(AdjustmentTypeVO.QUOTE_ADJUSTMENT);
		transGroup.removeAdjustment(AdjustmentTypeVO.QUOTE_DISCOUNT);

		MembershipQuote memQuote = new MembershipQuote(transGroup);

		MembershipEJBHelper.getMembershipMgrLocal().createMembershipQuote(memQuote);

		// Now add the quote adjustments back.
		if (quoteAdjustment != null) {
			transGroup.addAdjustment(quoteAdjustment);
		}
		if (quoteDiscount != null) {
			transGroup.addAdjustment(quoteDiscount);
		}

		request.setAttribute("quoteSaved", "Yes");
		request.setAttribute("transactionGroup", transGroup);
		request.setAttribute("quoteNumber", memQuote.getQuoteNumber());
		forwardRequest(request, response, MembershipUIConstants.PAGE_ConfirmTransaction);
	}

	/**
	 * Handle the change details button event on the confirm transaction page. Show the select group members page. Create the list of clients in the group from the existing membership transactions.
	 */
	public void handleEvent_confirmTransaction_btnChange(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		LogUtil.log(this.getClass(), "transGroup=" + transGroup.getTransactionList());

		request.setAttribute("transactionGroup", transGroup);

		// Determine which page to call next.
		String pageName = null;
		String transTypeCode = transGroup.getTransactionGroupTypeCode();
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(transTypeCode) || (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transTypeCode) && !transGroup.isMembershipGroupTransaction())) {
			// clear the transaction list to allow recalculation
			transGroup.clearTransactionList();
			pageName = MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS;
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(transTypeCode)) {
			// clear the transaction list to allow recalculation
			transGroup.clearTransactionList();
			pageName = MembershipUIConstants.PAGE_SelectProductOptions;
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(transTypeCode)) {
			// don't clear the transaction list
			pageName = MembershipUIConstants.PAGE_DiscretionaryDiscount;
		} else {
			// clear the transaction list to allow recalculation
			transGroup.clearTransactionList();
			pageName = MembershipUIConstants.PAGE_SelectGroupMembers;
		}
		forwardRequest(request, response, pageName);
	}

	public void handleEvent_searchAgents(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String agentId = request.getParameter("agentId");
		String agentName = request.getParameter("agentName");
		String location = request.getParameter("location");
		String roadsideString = request.getParameter("roadside");
		String towingString = request.getParameter("towing");
		String batteryString = request.getParameter("battery");

		Boolean roadside = Boolean.valueOf(roadsideString != null ? roadsideString.equalsIgnoreCase("Y") : false);
		Boolean towing = Boolean.valueOf(towingString != null ? towingString.equalsIgnoreCase("Y") : false);
		Boolean battery = Boolean.valueOf(batteryString != null ? batteryString.equalsIgnoreCase("Y") : false);

		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		Collection agentList = membershipMgr.getAgentList(agentId, agentName, location, roadside, towing, battery);
		LogUtil.debug(this.getClass(), "agentList=" + agentList.size());
		LogUtil.debug(this.getClass(), "agentList=" + agentList);
		request.setAttribute("agentList", agentList);

		request.setAttribute("agentId", agentId);
		request.setAttribute("agentName", agentName);
		request.setAttribute("location", location);
		request.setAttribute("roadside", roadside);
		request.setAttribute("towing", towing);
		request.setAttribute("battery", battery);

		forwardRequest(request, response, MembershipUIConstants.PAGE_SEARCH_AGENTS);
	}

	/**
	 * Handle the next button event on the confirm transaction page. Show the transaction complete page select payment method page.
	 * @throws ParseException 
	 */
	public void handleEvent_confirmTransaction_btnNext(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException, ParseException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);

		/* ********************************************************
		 * Check for outstanding dd amounts. Note that any amount in direct debit already includes the relevant gst.
		 */
		
		// vv This code block is a workaround for non-motoring -> Lifestyle change ONLY vv
		String newExpiryDate = request.getParameter("expiryDate");
		String newEffectiveDate = request.getParameter("effectiveDate");
		boolean isNewExpiryDate = request.getParameter("newExpiryDate").equals("true");
		
		DateTime newJavaExpiryDate = null;
		DateTime newJavaEffectiveDate = null;

		if (isNewExpiryDate) {
			newJavaExpiryDate = new DateTime(newExpiryDate);
			newJavaEffectiveDate = new DateTime(newEffectiveDate);
			transGroup.setExpiryDate(newJavaExpiryDate);
			transGroup.setFeeEffectiveDate(newJavaEffectiveDate);
		}
		// ^^ This code block is a workaround for non-motoring -> Lifestyle change ONLY ^^

		Collection transactionList = transGroup.getTransactionList();
		if (transactionList == null || transactionList.isEmpty()) {
			throw new SystemException("The transaction list is null or empty.");
		}
		/* get outstanding dd's and credits if there are any */
		MembershipVO memVO = null;
		MembershipVO newMemVO = null;
		MembershipTransactionVO memTransVO = null;
		Iterator transactionListIT = transactionList.iterator();
		double payableTotal = 0;
		while (transactionListIT.hasNext()) {
			memTransVO = (MembershipTransactionVO) transactionListIT.next();
			payableTotal += memTransVO.getAmountPayable();
			// If a new membership has not been set then the membership is part
			// of a
			// membership group and it is not being changed. Use the current
			// membership instead.
			newMemVO = memTransVO.getNewMembership();
			memVO = memTransVO.getMembership();
			payableTotal += memTransVO.getAmountPayable();

			// vv This code block is a workaround for non-motoring -> Lifestyle change ONLY vv
			if (isNewExpiryDate) {
				memTransVO.setEndDate(newJavaExpiryDate);
				memTransVO.setEffectiveDate(newJavaEffectiveDate);
//				memTransVO.setProductEffectiveDate();
			}
			// ^^ This code block is a workaround for non-motoring -> Lifestyle change ONLY ^^
		}

		/* ********************************************************** */

		request.setAttribute("transactionGroup", transGroup);
		// determine the next page to call
		String pageName = "";

		// if (transGroup.getAmountPayable() > 0)
		if (payableTotal > 0) {
			DirectDebitAuthority dda = transGroup.getDirectDebitAuthority();
			StringBuffer ddPolicyMessage = constructDDPolicyMessage(dda);
			request.setAttribute("ddPolicyMessage", ddPolicyMessage.toString());
			pageName = MembershipUIConstants.PAGE_SelectPaymentMethod;
		} else {
			pageName = MembershipUIConstants.PAGE_TransactionComplete;
		}
		// then call it
		forwardRequest(request, response, pageName);
	}

	private StringBuffer constructDDPolicyMessage(DirectDebitAuthority dda) throws SystemException {
		LogUtil.log(this.getClass(), "constructDDPolicyMessage 1");
		StringBuffer ddPolicyMessage = new StringBuffer();
		if (dda != null) {
			PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
			Collection policies;
			try {
				LogUtil.log(this.getClass(), "dda.getDirectDebitAuthorityID()=" + dda.getDirectDebitAuthorityID());
				policies = paymentMgr.getPoliciesOnAccount(dda.getDirectDebitAuthorityID());
				LogUtil.log(this.getClass(), "policies=" + policies);
			} catch (RemoteException e) {
				throw new SystemException(e);
			}
			LogUtil.log(this.getClass(), "policies.size()=" + policies.size());
			if (policies.size() > 0) {
				ddPolicyMessage.append("There may be other direct debit items that need to be updated.\\n");
				ddPolicyMessage.append("Policies: ");
				LogUtil.log(this.getClass(), "ddPolicyMessage=" + ddPolicyMessage);
				int counter = 0;
				for (Iterator i = policies.iterator(); i.hasNext();) {
					counter++;
					if (counter > 1) {
						ddPolicyMessage.append(" ");
					}
					ddPolicyMessage.append(i.next().toString());
				}
				LogUtil.log(this.getClass(), "ddPolicyMessage=" + ddPolicyMessage);
			}
		}
		LogUtil.log(this.getClass(), "constructDDPolicyMessage 2" + ddPolicyMessage);
		return ddPolicyMessage;
	}

	/**
	 * The user has set the Payment Methods and is to move on, who knows where.
	 */
	public void handleEvent_selectPaymentMethod_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		TransactionGroup transGroup = MembershipSessionUtil.getTransactionGroup(request);
		String userID = transGroup.getUserID();
		String salesBranchCode = transGroup.getSalesBranchCode();

		MembershipTransactionVO paMemTransVO = transGroup.getMembershipTransactionForPA();
		if (paMemTransVO == null) {
			throw new SystemException("Failed to get membership transaction VO for PA when setting direct debit payment method.");
		}

		// LogUtil.log(this.getClass(),"paMemTransVO="+paMemTransVO);
		// LogUtil.log(this.getClass(),"userID="+userID);
		// LogUtil.log(this.getClass(),"salesBranchCode="+salesBranchCode);

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

		DateTime today = new DateTime();
		String cashTypeCode = null;

		String paymentTypeStr = request.getParameter("paymentType");
		String accountName = request.getParameter("accountName");
		String bsbNumber = request.getParameter("bsbNumber");
		String cardNumber = request.getParameter("cardNumber");
		String cardExpiry = request.getParameter("cardExpiry");
		String accountNumber = request.getParameter("accountNumber");
		String dayToDebitStr = request.getParameter("dayToDebit");
		String cardVerificationValue = request.getParameter("cardVerificationValue");
		boolean isOwner = "Yes".equals(request.getParameter("isOwner"));
		// String ddrDateStr = request.getParameter("ddrDate");
		String ddFrequency = request.getParameter("frequencyOfDebit");
		boolean autoRenew = "Yes".equalsIgnoreCase(request.getParameter("autoRenew"));
		String sequenceNumber = request.getParameter("sequenceNumber");
		HttpSession sess = request.getSession();
		String oldCardNumber = (String) sess.getAttribute("oldCardNumber");
		if (oldCardNumber != null && !oldCardNumber.equals("") && StringUtil.obscureFormattedCreditCardNumber(oldCardNumber).equals(cardNumber)) {
			cardNumber = oldCardNumber;
		}

		// start DD

		String ddrDateStr = DateUtil.nextDay().formatShortDate();
		cashTypeCode = associateDirectDebit(transGroup, userID, salesBranchCode, paMemTransVO, today, cashTypeCode, paymentTypeStr, accountName, bsbNumber, cardNumber, cardExpiry, accountNumber, dayToDebitStr, cardVerificationValue, isOwner, ddrDateStr, ddFrequency, autoRenew);

		// end dd

		// start repair

		repairTransaction(transGroup, paMemTransVO, paymentMgr, cashTypeCode, sequenceNumber);

		// end repair

		Collection transactionList = transGroup.submit();
		if (transactionList == null) {
			throw new ValidationException("The transaction has already been submitted.<p>You won't be able to see the result so try looking at the membership to see if the changes have been made.");
		}

		request.setAttribute("transactionList", transactionList);
		request.setAttribute("contextClientVO", transGroup.getContextClient());

		// Remove the transaction group from the session now that we have
		// finished with it.
		MembershipSessionUtil.removeTransactionContainer(request, transGroup.getTransactionGroupKey());

		// create new renewal document if required
		try {
			MembershipVO memVO = transGroup.getMembershipGroupPA();
			UserSession us = this.getUserSession(request);
			User user = us.getUser();
			MembershipRenewalMgr rMgr = MembershipEJBHelper.getMembershipRenewalMgr();
			rMgr.replaceRenewalNotice(memVO, memVO, user);
		} catch (Exception e) {
			//
			e.printStackTrace();
		}

		if (transactionList != null) {
			MembershipVO membershipVO = null;
			MembershipTransactionVO memTransVO = null;
			Iterator transactionListIterator = transactionList.iterator();
			while (transactionListIterator.hasNext()) {

				memTransVO = (MembershipTransactionVO) transactionListIterator.next();
				membershipVO = memTransVO.getNewMembership();

				// MEM-431 - Remove link to DDA if PBCN is selected
				// gzs - 5/1/16
				if (PaymentTypeVO.CASHTYPE_CASH.equals(cashTypeCode)) {
					String memNo = "unknown member number";
					try {
						memNo = membershipVO.getMembershipNumber();
						membershipVO.setDirectDebitAuthority(null);
					} catch (Exception e) {
						LogUtil.log(this.getClass(), "Could not remove DDA link for " + memNo);
					}
				}
			}
		}

		forwardRequest(request, response, MembershipUIConstants.PAGE_TransactionComplete);
	}// handleEvent_selectPaymentMethod_btnSave

	private static void repairTransaction(TransactionGroup transGroup, MembershipTransactionVO paMemTransVO, PaymentMgr paymentMgr, String cashTypeCode, String sequenceNumber) throws RemoteException {
		/*
		 * Handle the nasties.
		 * 
		 * The "REPAIR" option is for incomplete transactions where the payment has been received in the receipting system, but not completed in the membership system. In this case do all the stuff required EXCEPT CREATING RECEIPTING records, and use the number supplied by the user as the paymentMethodID in the payableItem This option will only be available to administrators. Let's hope they know what they are doing
		 */
		if (cashTypeCode.equals(PaymentTypeVO.CASHTYPE_REPAIR)) {

			// check that the receipt exists.
			Integer clientNumber = paMemTransVO.getNewMembership().getClientNumber();
			Payable payable = null;
			try {
				payable = paymentMgr.getPayable(clientNumber, sequenceNumber, null, SourceSystem.MEMBERSHIP);
			} catch (PaymentException ex) {
				// ignore as catered for below
			}
			if (payable == null) {
				throw new ValidationException("Receipt " + sequenceNumber + " for client " + clientNumber + " does not exist.");
			}

			BigDecimal recAmountPayable = payable.getAmount().setScale(CurrencyUtil.DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
			BigDecimal amountPayable = new BigDecimal(transGroup.getAmountPayable()).setScale(CurrencyUtil.DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
			// check that the amount calculated now equals the amount receipted.
			if (recAmountPayable.compareTo(amountPayable) != 0) {
				throw new ValidationException("The amount payable of " + CurrencyUtil.formatDollarValue(amountPayable) + "does not equal the amount in the receipting system of " + CurrencyUtil.formatDollarValue(recAmountPayable));
			}

			// Check that the receipt has been fully paid.
			BigDecimal recAmountOutstanding = payable.getAmountOutstanding();
			if (recAmountOutstanding != null && recAmountOutstanding.doubleValue() > 0) {
				throw new ValidationException("Receipt " + sequenceNumber.toString() + " for client " + clientNumber + " has not been fully paid. Amount outstanding = " + CurrencyUtil.formatDollarValue(recAmountOutstanding));
			}

			// Make sure that the receipt is not linked to any other payable
			// items
			Collection payableItemList = paymentMgr.getPayableItemsByClientReceipt(clientNumber, sequenceNumber);
			if (payableItemList != null && !payableItemList.isEmpty()) {
				Iterator payableItemIterator = payableItemList.iterator();
				PayableItemVO payableItemVO = (PayableItemVO) payableItemIterator.next();
				throw new ValidationException("Receipt " + sequenceNumber.toString() + " for client " + clientNumber + " is already linked to payable item " + payableItemVO.getPayableItemID());
			}
			transGroup.setRepairPaymentMethodId(sequenceNumber);
		}
	}

	private static String associateDirectDebit(TransactionGroup transGroup, String userID, String salesBranchCode, MembershipTransactionVO paMemTransVO, DateTime today, String cashTypeCode, String paymentTypeStr, String accountName, String bsbNumber, String cardNumber, String cardExpiry, String accountNumber, String dayToDebitStr, String cardVerificationValue, boolean isOwner, String ddrDateStr, String ddFrequency, boolean autoRenew) throws RemoteException {
		LogUtil.debug("", "associateDirectDebit 1");
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

		boolean validate = false;

		DirectDebitAuthority newAuthority = null;
		DirectDebitAuthority oldAuthority = null;

		if (paMemTransVO.getMembership() != null) {
			oldAuthority = paMemTransVO.getMembership().getDirectDebitAuthority();
		}

		PaymentTypeVO paymentType = paymentMgr.getPaymentType(paymentTypeStr);
		if (paymentType == null) {
			throw new SystemException("Failed to get payment type '" + paymentTypeStr + "'.");
		}
		cashTypeCode = paymentType.getCashTypeCode();

		LogUtil.debug("", "paymentType=" + paymentType.getPaymentTypeCode());
		LogUtil.debug("", "cashTypeCode" + cashTypeCode);

		if (!PaymentTypeVO.CASHTYPE_CASH.equals(cashTypeCode) && !PaymentTypeVO.CASHTYPE_REPAIR.equals(cashTypeCode)) {

			DateTime ddrDate = validateDDRDate(ddrDateStr);

			// The ddr date must be validated if a new direct debit authority is
			// required.
			// A new direct debit authority is not required if an existing
			// direct debit
			// payment method has not been significantly changed. Otherwise it
			// is required.
			if (oldAuthority == null || oldAuthority.hasSignificantlyChanged(newAuthority)) {

				validate = true;

				// A new direct debit authorisation is required so the
				// ddr date must be after today.
				if (ddrDate.beforeDay(today)) {
					throw new ValidationException("The ddr date must be after today.");
				}
			}

			// Paying by direct debit. Get the details.
			BankAccount bankAccount;

			if (accountName == null) {
				throw new SystemException("Failed to get accountName field from page.");
			}

			if (PaymentTypeVO.CASHTYPE_DIRECTDEBIT.equals(cashTypeCode)) {
				// Get debit account info

				if (bsbNumber == null) {
					throw new SystemException("Failed to get bsbNumber field from page.");
				}
				// move to DebitAccount
				bsbNumber = StringUtil.replaceAll(bsbNumber, " ", "");
				bsbNumber = StringUtil.replaceAll(bsbNumber, "-", "");

				if (accountNumber == null) {
					throw new SystemException("Failed to get accountNumber field from page.");
				}
				// move to DebitAccount
				accountNumber = StringUtil.replaceAll(accountNumber, " ", "");
				accountNumber = StringUtil.replaceAll(accountNumber, "-", "");

				bankAccount = new DebitAccount(bsbNumber, accountNumber, accountName);

			} else if (PaymentTypeVO.CASHTYPE_CREDITCARD.equals(cashTypeCode)) {
				// Get credit card account info

				if (cardNumber == null) {
					throw new SystemException("Failed to get cardNumber field from page.");
				}

				if (cardExpiry == null) {
					throw new SystemException("Failed to get cardExpiry field from page.");
				}

				if (cardVerificationValue == null) {
					throw new SystemException("Failed to get cardVerificationValue field from page.");
				}

				CreditCardAccount ccAccount = new CreditCardAccount();
				ccAccount.setValidate(validate);
				ccAccount.setAccountName(accountName);
				ccAccount.setPaymentType(paymentType);
				ccAccount.setCardNumber(cardNumber);
				ccAccount.setCardExpiry(cardExpiry);
				ccAccount.setVerificationValue(cardVerificationValue);
				bankAccount = ccAccount;
			} else {
				throw new SystemException("Invalid cash type code '" + cashTypeCode + "' on payment type '" + paymentTypeStr + "'.");
			}

			// Get the frequency to do the debits
			DirectDebitFrequency frequency = DirectDebitFrequency.getFrequency(ddFrequency);

			if (dayToDebitStr == null) {
				throw new SystemException("Day of debit does not exist.");
			}
			int dayToDebit = 0;
			try {
				dayToDebit = Integer.parseInt(dayToDebitStr.trim());
			} catch (NumberFormatException nfe) {
				throw new ValidationException("The day of the month on which to do the debit is not a valid number.");
			}

			boolean isApproved = false;
			if (oldAuthority != null) {
				isApproved = oldAuthority.isApproved();
			}

			if (ddrDateStr == null) {
				throw new SystemException("Failed to get 'ddrDate' field from page.");
			}

			newAuthority = new DirectDebitAuthority((oldAuthority == null ? null : oldAuthority.getDirectDebitAuthorityID()), paMemTransVO.getNewMembership().getClientNumber(), userID, salesBranchCode, today, isOwner, dayToDebit, isApproved, ddrDate, frequency, bankAccount, SourceSystem.MEMBERSHIP, null); // Havn't
			// got
			// a
			// membership
			// ID
			// yet!

			LogUtil.log("associatedDirectDebit", "newAuthority=" + newAuthority);

		}

		// validate the authority?

		transGroup.setDirectDebitAuthority(newAuthority, autoRenew);
		return cashTypeCode;
	}

	private static DateTime validateDDRDate(String ddrDateStr) throws ValidationException {
		DateTime ddrDate = null;
		DateTime today = new DateTime();

		try {
			ddrDate = DateUtil.convertStringToDateTime(ddrDateStr);
		} catch (Exception e) {
			throw new ValidationException("The DDR date is not a valid date.");
		}

		return ddrDate;
	}

	private void action_renewMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));
		if (contextMemVO == null) {
			throw new SystemException("Failed to get membership for renew. Membership ID '" + request.getParameter("membershipID") + "' not found!");
		} else {
			UserSession userSession = getUserSession(request);
			// use expiry date to determine fees
			TransactionGroup transGroup = TransactionGroup.getRenewalTransactionGroup(contextMemVO, userSession.getUser(), contextMemVO.getExpiryDate());

			// Add the transaction group to the session
			MembershipSessionUtil.addTransactionContainer(request, transGroup);

			showPage_confirmTransaction(request, response, transGroup);
		}
	} // action_renewMembership

	/**
	 * select rejoin action
	 */
	private void action_rejoinMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));
		if (contextMemVO == null) {
			throw new SystemException("Failed to get membership for rejoin. Membership ID '" + request.getParameter("membershipID") + "' not found!");
		}

		String invalidTransactionReason = contextMemVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN);
		if (invalidTransactionReason != null) {
			throw new ValidationException("The membership cannot be rejoined. " + invalidTransactionReason);
		}

		// Get a transaction ID to reference the transaction group with while
		// processing this transaction.
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
		UserSession userSession = getUserSession(request);

		// Create a transaction group.
		// The transaction date, preferred commence date and product effective
		// dates are initialised to today when constructed.
		TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN, userSession.getUser());

		transGroup.setMembershipType(contextMemVO.getMembershipType());
		transGroup.addSelectedClient(contextMemVO);
		transGroup.setContextClient(contextMemVO);
		transGroup.createTransactionsForSelectedClients();

		// Default the rejoin type
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		String defaultRejoinTypeCode = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DEF_REJ_TYPE);
		if (defaultRejoinTypeCode == null) {
			throw new SystemException("Failed to get default rejoin type code.");
		}

		ReferenceDataVO defaultRejoinType = null;
		// if(contextMemVO != null &&
		// MembershipVO.STATUS_TRANSFERRED.equals(contextMemVO.getStatus()))
		// {
		// defaultRejoinType =
		// commonMgr.getReferenceData(ReferenceDataVO.REF_TYPE_REJOIN_TYPE,
		// ReferenceDataVO.REJOIN_TYPE_RESET_EXPIRY);
		// }
		// else
		// {
		defaultRejoinType = commonMgr.getReferenceData(ReferenceDataVO.REF_TYPE_REJOIN_TYPE, defaultRejoinTypeCode);
		// }

		if (defaultRejoinType == null) {
			throw new SystemException("Failed to get rejoin type for rejoin type code = '" + defaultRejoinTypeCode + "'");
		}
		transGroup.setRejoinType(defaultRejoinType);

		// Default the rejoin reason.
		String defaultRejoinReasonCode = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DEFAULT_REJOIN_REASON);
		if (defaultRejoinReasonCode == null) {
			throw new SystemException("Failed to get default rejoin reason code");
		}
		ReferenceDataVO defaultRejoinReason = refMgr.getRejoinReason(defaultRejoinReasonCode);
		if (defaultRejoinReason == null) {
			throw new SystemException("Failed to get rejoin reason for rejoin reason code = '" + defaultRejoinReasonCode + "'");
		}
		transGroup.setTransactionReason(defaultRejoinReason);

		if (contextMemVO.getProduct() == null) {
			// Add the transaction group to the session
			MembershipSessionUtil.addTransactionContainer(request, transGroup);

			request.setAttribute("transactionGroup", transGroup);
			forwardRequest(request, response, MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS);

		} else {
			// Must set the product after adding the transactions because
			// the number of memberships being transacted affects the product
			// fee.
			transGroup.setSelectedProduct(contextMemVO.getProduct());

			// Create transaction fees
			transGroup.createTransactionFees();

			// Add the transaction group to the session
			MembershipSessionUtil.addTransactionContainer(request, transGroup);

			showPage_confirmTransaction(request, response, transGroup);

		}

	} // action_rejoinMembership

	private void action_holdMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		// get the membership object
		String membershipID = null;
		MembershipVO contextMemVO = (MembershipVO) request.getAttribute("membershipVO");
		if (contextMemVO == null) {
			contextMemVO = getMembershipVO(request.getParameter("membershipID"));
		}

		if (contextMemVO == null) {
			throw new SystemException("Failed to get membership to place on hold.");
		}

		String invalidTransactionReason = contextMemVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD);
		if (invalidTransactionReason != null) {
			throw new ValidationException("The membership cannot be put on hold. " + invalidTransactionReason);
		}

		// Looks like the membership is valid to place on hold.
		UserSession userSession = getUserSession(request);

		TransactionGroup transGroup = TransactionGroupFactory.getTransactionGroupForHold(userSession.getUser());
		transGroup.setMembershipType(contextMemVO.getMembershipType());
		transGroup.addSelectedClient(contextMemVO);
		transGroup.setContextClient(contextMemVO);
		transGroup.setSelectedProduct(contextMemVO.getProduct());
		transGroup.createTransactionsForSelectedClients();

		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("transactionGroup", transGroup);

		forwardRequest(request, response, MembershipUIConstants.PAGE_HoldPersonalMembership);
	} // action_holdMembership

	private void action_changeMembershipProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		// Get the membership from the request.
		MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));
		if (contextMemVO == null) {
			throw new SystemException("Failed to get membership " + request.getParameter("membershipID") + ".");
		}

		// Check to see if the product can be changed.
		String invalidTransactionReason = contextMemVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT);
		if (invalidTransactionReason != null) {
			throw new ValidationException("The product on the membership cannot be changed. " + invalidTransactionReason);
		}

		UserSession userSession = getUserSession(request);
		TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, userSession.getUser());

		transGroup.setMembershipType(contextMemVO.getMembershipType());
		transGroup.setSelectedProduct(contextMemVO.getProduct());
		transGroup.addSelectedClient(contextMemVO);
		transGroup.setContextClient(contextMemVO);

		// Add the transaction group to the session
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectProduct);
	} // action_changeMembershipProduct

	private void action_changeMembershipProductOption(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		// Get the membership from the request.
		MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));

		String invalidForTransactionReason = contextMemVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION);
		if (invalidForTransactionReason != null) {
			throw new ValidationException(invalidForTransactionReason);
		}

		UserSession userSession = getUserSession(request);
		TransactionGroup transGroup = TransactionGroupFactory.getTransactionGroupForChangeProductOption(userSession.getUser());
		transGroup.setMembershipType(contextMemVO.getMembershipType());
		transGroup.setSelectedProduct(contextMemVO.getProduct());
		transGroup.addSelectedClient(contextMemVO);
		transGroup.setContextClient(contextMemVO);
		transGroup.createTransactionsForSelectedClients();

		// Add the transaction group to the session
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectProductOptions);
	} // action_changeMembershipProductOption

	private void action_cancelMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		String membershipID = (String) request.getParameter("membershipID");
		MembershipVO memVO = (MembershipVO) getMembershipVO(membershipID);

		// Make sure that the membership is not already cancelled.
		if (MembershipVO.STATUS_CANCELLED.equals(memVO.getStatus())) {
			throw new ValidationException("The membership is already cancelled.");
		}

		// Display the cancel membership page.
		request.setAttribute("membershipVO", memVO);
		forwardRequest(request, response, MembershipUIConstants.PAGE_CancelPersonalMembership);
	} // action_cancelMembership

	/**
	 * Handle 1 click cancellation from the direct debit screen ONLY FOR INDIVIDUAL MEMBERSHIPS
	 * 
	 * @param request
	 * @param response
	 */
	public void handleEvent_cancelMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		request.setAttribute("cancelReason", "HON");
		request.setAttribute("blockTransactions", "true");
		request.setAttribute("blockReasonText", "DD dishonoured letter sent, no success, cancel membership");
		handleEvent_cancelMembership_btnSave(request, response);
	}

	private void action_createMembership(HttpServletRequest request, HttpServletResponse response, String refreshProductList) throws ServletException, RemoteException, ValidationException {
		String pageName = null;
		// Get the client data
		String clientNumber = request.getParameter("clientNumber");
		ClientVO clientVO = getClient(clientNumber);

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

		if (clientVO == null) {
			throw new SystemException("Client number '" + clientNumber + "' not found!");
		}

		// Validate the client for the transaction.
		String invalidReason = MembershipHelper.validateClientForTransaction(clientVO, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, null);
		if (invalidReason != null) {
			throw new ValidationException(invalidReason);
		}

		// Get the list of membership types that this client type is allowed to
		// have.
		String clientTypeCode = clientVO.getClientType();
		MembershipRefMgr membershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();
		Collection allowedMembershipTypeList = membershipRefMgr.getMembershipTypeListByClientType(clientTypeCode);

		// If the list is empty then this client type cannot be issued with a
		// membership
		if (allowedMembershipTypeList == null || allowedMembershipTypeList.isEmpty()) {
			throw new ValidationException("This client is of a type (" + (clientTypeCode == null || clientTypeCode.length() < 1 ? "?" : clientTypeCode) + ") that cannot be issued with a membership.");
		}

		// Get a transaction ID to reference the transaction group with while
		// processing this transaction.
		MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();

		UserSession userSession = getUserSession(request);
		TransactionGroup transGroup = null;

		// If a membership type was specified then make sure it is one of the
		// allowed types
		// for the client type.
		MembershipTypeVO memTypeVO = null;
		String memTypeCode = request.getParameter("membershipType");

		// get the membership type object
		if (memTypeCode != null) {
			// Find the membership type in the allowed membership type list
			// If we don't find it then memTypeVO will not be set and the
			// select membership type page will be displayed.
			MembershipTypeVO tmpVO = null;
			Iterator it = allowedMembershipTypeList.iterator();
			while (it.hasNext() && memTypeVO == null) {
				tmpVO = (MembershipTypeVO) it.next();
				memTypeVO = (memTypeCode.equals(tmpVO.getMembershipTypeCode()) ? tmpVO : null);
			}
		}
		// normal new membership
		transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, userSession.getUser());

		// See if we can finalise the decision on what the membership type will
		// be.
		// Also select the next page to display.
		if (memTypeVO != null) {
			// A membership type was selected and it is an allowed membership
			// type for this client type.
			transGroup.setMembershipType(memTypeVO);
			pageName = MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS;
		} else if (allowedMembershipTypeList.size() == 1) {
			// An allowed membership type was not specified.
			// There is only one allowed membership type in the list.
			// Assume the allowed membership type.
			Iterator listIterator = allowedMembershipTypeList.iterator();
			memTypeVO = (MembershipTypeVO) listIterator.next();
			transGroup.setMembershipType(memTypeVO);
			pageName = MembershipUIConstants.PAGE_SELECT_MEMBERSHIP_OPTIONS;
		} else {
			// An allowed membership type was not specified and there
			// is more than one in the allowed membership type list.
			pageName = MembershipUIConstants.PAGE_SelectMembershipType;
			request.setAttribute("allowedMembershipTypeList", allowedMembershipTypeList);
		}

		// If the membership type being created is "Personal", make sure that
		// the client does not already have a personal membership.
		// If they do then throw an exception.
		if (memTypeVO != null && memTypeVO.getMembershipTypeCode().equals(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL)) {
			if (MembershipHelper.clientHasPersonalMembership(clientVO.getClientNumber())) {
				throw new ValidationException("The client already has a personal membership. A new one cannot be created.");
			}

			//
			// Set the motor news send option to NORMAL
			//
			LogUtil.debug(this.getClass(), "RLG  Setting motor news  " + Client.MOTOR_NEWS_MEMBERSHIP_SEND);
			clientVO.setMotorNewsSendOption(Client.MOTOR_NEWS_MEMBERSHIP_SEND);
			clientVO.setMotorNewsDeliveryMethod(Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL);

		}
		/*
		 * }
		 */
		// Add the selected client, and any other clients that they may already
		// be
		// in a membership group with, to the transaction group.
		// The prime addressee will automatically be determined.
		transGroup.addSelectedClient(clientVO);
		transGroup.setContextClient(clientVO);

		// Create a membership transaction for the selected client.
		transGroup.createTransactionsForSelectedClients();

		// Add the transaction group to the session
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("refreshProductList", refreshProductList);
		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, pageName);
	} // action_createMembership

	private void action_editMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException, com.ract.common.SecurityException {
		String membershipIDStr = request.getParameter("membershipID");
		MembershipVO contextMemVO = getMembershipVO(membershipIDStr);

		// make sure that the current user can edit membership data.
		UserSession userSession = getUserSession(request);
		if (!userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_EDIT)) {
			throw new com.ract.common.SecurityException("You do not have the privilege to edit memberships!");
		}

		// Validate the membership for the transaction.
		String invalidReason = contextMemVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP);
		if (invalidReason != null) {
			throw new ValidationException(invalidReason);
		}

		// Create a TransactionGroup object to hold the common data in the
		// transaction
		TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP, userSession.getUser());

		// Add the selected client, and any other clients that they may already
		// be
		// in a membership group with, to the transaction group.
		// The prime addressee will automatically be determined.
		transGroup.addSelectedClient(contextMemVO);
		transGroup.setContextClient(contextMemVO);

		// Create a membership transaction for the selected client.
		transGroup.createTransactionsForSelectedClients();

		// Add the transaction group to the session
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_EditMembership);
	} // action_editMembership

	private void action_transferMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		outputRequestParameters(request);

		String clientNumber = request.getParameter("clientNumber");
		String membershipId = request.getParameter("membershipID");

		ClientVO client = null;
		if (membershipId != null && !membershipId.equals("")) {
			MembershipVO contextMemVO = getMembershipVO(membershipId);
			client = contextMemVO.getClient();
		} else {
			client = getClient(clientNumber);
		}
		UserSession userSession = getUserSession(request);
		SortedSet clubList = AffiliatedClubHelper.getSortedClubList();

		request.setAttribute("clubList", clubList);
		request.setAttribute("client", client);

		forwardRequest(request, response, MembershipUIConstants.PAGE_SELECT_AFFILIATED_CLUB);
	} // action_transferMembership

	private void action_manageGifts(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

		MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));

		// Get the client data
		ClientVO clientVO = contextMemVO.getClient();

		// UserSession userSession = getUserSession(request);

		request.setAttribute("client", clientVO);
		request.setAttribute("membership", contextMemVO);

		forwardRequest(request, response, MembershipUIConstants.PAGE_MANAGE_MEMBERSHIP_GIFTS);
	} // action_transferMembership

	private void action_createGroup(HttpServletRequest request, HttpServletResponse response, String clientNumberStr, String membershipIDStr, String refreshProductList) throws ServletException, ValidationException, RemoteException {
		MembershipMgrLocal membershipMgr = MembershipEJBHelper.getMembershipMgrLocal();

		MembershipVO contextMemVO = null;
		ClientVO contextClientVO = null;

		// Get the membership if the ID was specified.
		if (membershipIDStr != null) {
			contextMemVO = getMembershipVO(membershipIDStr);
			if (contextMemVO == null) {
				throw new SystemException("Failed to get membership with ID = " + membershipIDStr);
			}

			contextClientVO = contextMemVO.getClient();
		} else {
			// Otherwise get the client data
			contextClientVO = getClient(clientNumberStr);
			if (contextClientVO == null) {
				throw new SystemException("Client number '" + clientNumberStr + "' not found!");
			}

			// See if the client has a membership
			contextMemVO = membershipMgr.findMembershipByClientAndType(contextClientVO.getClientNumber(), MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);

		}

		// See if a membership group can be created with the client/membership.
		String invalidForTransactionReason = null;
		if (contextMemVO != null) {
			invalidForTransactionReason = contextMemVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP);
		} else {
			invalidForTransactionReason = MembershipHelper.validateClientForTransaction(contextClientVO, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP, null);
		}

		if (invalidForTransactionReason != null) {
			throw new ValidationException("A membership group cannot be created. " + invalidForTransactionReason);
		}

		// Get a transaction ID to reference the transaction group with while
		// processing this transaction.
		// MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
		UserSession userSession = getUserSession(request);

		// Create a TransactionGroup object to hold the common data in the
		// transaction
		TransactionGroup transGroup = new TransactionGroup(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP, userSession.getUser());

		if (contextMemVO != null) {
			transGroup.addSelectedClient(contextMemVO);
			transGroup.setContextClient(contextMemVO);
		} else {
			transGroup.addSelectedClient(contextClientVO);
			transGroup.setContextClient(contextClientVO);
		}

		// Get the personal membership type and set in on the transaction group.
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		MembershipTypeVO memTypeVO = refMgr.getMembershipType(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
		transGroup.setMembershipType(memTypeVO);

		// Add the transaction group to the session
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectGroupMembers);
	} // action_createGroup

	/**
	 * change group action
	 */
	private void action_changeGroup(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));
		if (contextMemVO == null) {
			throw new SystemException("Failed to get membership for change of group. Membership ID '" + request.getParameter("membershipID") + "' not found!");
		}

		UserSession userSession = getUserSession(request);
		String transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP;

		// Make sure that the membership can be renewed.
		String invalidTransactionReason = contextMemVO.getInvalidForTransactionReason(transTypeCode);
		if (invalidTransactionReason != null) {
			throw new ValidationException("The membership group cannot be changed. " + invalidTransactionReason);
		}
		// Create a transaction group.
		// The transaction date, preferred commence date and product effective
		// dates are initialised to today when constructed.
		TransactionGroup transGroup = new TransactionGroup(transTypeCode, userSession.getUser());

		transGroup.setMembershipType(contextMemVO.getMembershipType());
		transGroup.addSelectedClient(contextMemVO); // Also adds other group
		// members
		transGroup.setContextClient(contextMemVO);
		transGroup.setSelectedProduct(contextMemVO.getProduct());

		// Add the transaction group to the session
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_SelectGroupMembers);
	} // action_changeGroup

	/**
	 * change group action
	 */
	private void action_changeGroupPA(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));
		if (contextMemVO == null) {
			throw new SystemException("Failed to get membership for change of group. Membership ID '" + request.getParameter("membershipID") + "' not found!");
		}

		UserSession userSession = getUserSession(request);
		String transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA;

		// Make sure that the membership can be renewed.
		String invalidTransactionReason = contextMemVO.getInvalidForTransactionReason(transTypeCode);
		if (invalidTransactionReason != null) {
			throw new ValidationException("The membership group cannot be changed. " + invalidTransactionReason);
		}
		// Create a transaction group.
		// The transaction date, preferred commence date and product effective
		// dates are initialised to today when constructed.
		TransactionGroup transGroup = new TransactionGroup(transTypeCode, userSession.getUser());

		transGroup.setMembershipType(contextMemVO.getMembershipType());
		transGroup.addSelectedClient(contextMemVO); // Also adds other group
		// members
		transGroup.setContextClient(contextMemVO);
		transGroup.setSelectedProduct(contextMemVO.getProduct());

		// Add the transaction group to the session
		MembershipSessionUtil.addTransactionContainer(request, transGroup);

		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ChangeGroupPA);
	} // action_changeGroup

	/**
	 * Send new CAD record to the cad-export table for transfer to CARES.
	 */
	private void action_sendToCAD(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
		String pageName = null;
		UserSession userSession = UserSession.getUserSession(request.getSession());

		if (userSession != null && userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CAD_EXPORT)) {
			MembershipVO contextMemVO = getMembershipVO(request.getParameter("membershipID"));
			CadExportMgr ceMgr = CommonEJBHelper.getCadExportMgr();
			ceMgr.notifyMembershipChangeToCAD(contextMemVO, new DateTime());
			pageName = MembershipUIConstants.PAGE_ViewPersonalMembership;
		} else {
			request.setAttribute("exception", new com.ract.common.SecurityException("You do not have the privilege export to CARES."));
			pageName = "/error/SecurityError.jsp";
		}

		forwardRequest(request, response, pageName);
	}

	private void action_viewMembership(HttpServletRequest request, HttpServletResponse response, String refreshProductList) throws ServletException, SystemException {
		String membershipID = request.getParameter("membershipID");
		String clientNumber = request.getParameter("clientNumber");
		String xferDD = request.getParameter("xferDD");
		UserSession session;
		try {
			session = UserSession.getUserSession(request.getSession());
		} catch (RemoteException e1) {
			throw new SystemException(e1);
		}
		User user = session.getUser();
		MembershipVO memVO = getMembershipVO(membershipID, clientNumber); // Throws an exception if it can't find the membership

		if (memVO == null) {
			// redirect to ViewClient
			String url = "ClientUIC?event=viewClientSummary&clientNumber=" + clientNumber + "&clientType=Person";
			try {
				response.sendRedirect(url);
			} catch (IOException e) {
				LogUtil.warn(this.getClass(), "Unable to redirect to " + url);
			}
			return;
		}

		String memTypeCode = memVO.getMembershipTypeCode();
		String pageName = null;

		// Added common manager support to get system environment - see ## below
		CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
		String systemName = "";
		try {
			systemName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);
		} catch (RemoteException e2) {
			LogUtil.warn(this.getClass(), "Unable to get Common Manager");
		}
				
		try {
			memVO.getAmountOutstanding(true, false);
		} catch (DirectDebitFailedValidationException e) {

			// Show in all cases
			request.setAttribute("showTransferFailedDDButton", "true");

			// transfer Failed DD to Receipting System
			if (xferDD != null && xferDD.equals("y")) {
				try {
					LogUtil.log(getClass(), "Transferring Failed DD to Receptor for cn: " + memVO.getClientNumber());
					MembershipEJBHelper.getMembershipMgrLocal().transferFailedDirectDebit(memVO.getClientNumber(), user);
					memVO = getMembershipVO(membershipID); // re-initialise
					// memVO

				} catch (Exception r) {
					throw new SystemException(r);
				}
			} else { // alert CSO to failed DD amounts
				LogUtil.warn(getClass(), "Membership: " + membershipID + " has failed amounts in DD");
				// request.setAttribute("showTransferFailedDDButton", "true");
			}

		} catch (RemoteException e) {
			// ## WF24000 refers - prevent no dd payable error from being thrown if non prod environment
			if (systemName.equals(CommonConstants.SYSTEM_PRODUCTION)) {
				throw new SystemException(e);
			}
		} catch (PaymentException e) {
			throw new SystemException(e);
		}

		// Add the membership to the request so that the view pages can use it.
		request.setAttribute("membershipVO", memVO);

		boolean askClientQuestions = false;

		ClientVO client;
		try {
			client = memVO.getClient();
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
		if ((client.getLastUpdate() == null && !client.getMadeDate().getDateOnly().equals(new DateTime().getDateOnly())) || // not
		// created
		// today
		(client.getLastUpdate() != null && !client.getLastUpdate().getDateOnly().equals(new DateTime().getDateOnly()))) // if
		// the
		// last
		// change
		// was
		// not
		// made
		// today
		{
			// check if we need to ask questions
			ClientMgrLocal clientMgr = ClientEJBHelper.getClientMgrLocal();
			if (!askClientQuestions)
				askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_BIRTH_DATE, client.getBirthDate() != null ? client.getBirthDate().formatShortDate() : client.getBirthDateString());
			if (!askClientQuestions)
				askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_EMAIL, client.getEmailAddress());
			if (!askClientQuestions)
				askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_HOME_PHONE, client.getHomePhone());
			if (!askClientQuestions)
				askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_MOBILE_PHONE, client.getMobilePhone());
			if (!askClientQuestions)
				askClientQuestions = clientMgr.isAskable(client.getClientNumber(), ClientVO.FIELD_NAME_WORK_PHONE, client.getWorkPhone());
		}

		request.setAttribute("askClientQuestions", askClientQuestions);

		// Determine which view page to show.
		if (memTypeCode.equals(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL)) {
			pageName = MembershipUIConstants.PAGE_ViewPersonalMembership;
		} else if (memTypeCode.equals(MembershipTypeVO.MEMBERSHIP_TYPE_FLEET)) {
			// pageName = MembershipUIConstants.PAGE_ViewFleetMembership;
			// Send it to the personal memnership page for the moment
			pageName = MembershipUIConstants.PAGE_ViewPersonalMembership;
		} else {
			throw new SystemException("Unknown membership type '" + memTypeCode + "'");
		}

		String memberCardDataStore = MembershipCardVO.DATA_STORE_DATABASE;
		try {
			memberCardDataStore = FileUtil.getProperty("master", "memberCardDataStore");
		} catch (Exception e1) {
			LogUtil.warn(MembershipUIHelper.class, "Could not determine member card datastore, defaulting to: " + memberCardDataStore);
		}

		String memberCardForceDB = "false";
		try {
			memberCardForceDB = FileUtil.getProperty("master", "memberCardForceDB");
		} catch (Exception e1) {
			LogUtil.warn(MembershipUIHelper.class, "Could not determine member card force DB, defaulting to: " + memberCardForceDB);
		}
		Boolean forceDB = Boolean.parseBoolean(memberCardForceDB);

		if (forceDB || memberCardDataStore.equals(MembershipCardVO.DATA_STORE_DATABASE)) {
			request.setAttribute("showMemberCards", true);
		} else {
			request.setAttribute("showMemberCards", false);
		}

		request.setAttribute("refreshProductList", refreshProductList);
		forwardRequest(request, response, pageName);
	}

	private void action_viewQuote(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		Integer quoteNumber = new Integer(request.getParameter("quoteNumber"));
		MembershipQuote memQuote = MembershipEJBHelper.getMembershipMgr().getMembershipQuote(quoteNumber);
		if (memQuote == null) {
			throw new SystemException("Failed to get membership quote number " + quoteNumber);
		}

		// Add the membership to the request so that the view pages can use it.
		request.setAttribute("membershipQuote", memQuote);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewMembershipQuote);
	}

	private void action_viewQuoteDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
		Integer quoteNumber = new Integer(request.getParameter("quoteNumber"));
		MembershipQuote memQuote = MembershipEJBHelper.getMembershipMgr().getMembershipQuote(quoteNumber);
		if (memQuote == null) {
			throw new SystemException("Failed to get membership quote number " + quoteNumber);
		}

		// Add the membership to the request so that the view pages can use it.
		request.setAttribute("membershipQuote", memQuote);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewMembershipQuoteDetail);
	}

	private void action_viewQuoteList(HttpServletRequest request, HttpServletResponse response, String refreshProductList) throws ServletException, RemoteException {
		Integer clientNumber = new Integer(request.getParameter("clientNumber"));
		ClientVO clientVO = ClientEJBHelper.getClientMgr().getClient(clientNumber);
		if (clientVO == null) {
			throw new SystemException("Failed to get client number " + clientNumber);
		}

		request.setAttribute("client", clientVO);
		request.setAttribute("refreshProductList", refreshProductList);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewMembershipQuoteList);
	}

	/**
	 * Request a new membership card
	 */
	private void action_requestCard(HttpServletRequest request, HttpServletResponse response) throws ServletException, ValidationException, RemoteException {
		String membershipID = request.getParameter("membershipID");
		MembershipVO memVO = getMembershipVO(membershipID); // Throws an
		// exception if it
		// can't find the
		// membership
		String memTypeCode = memVO.getMembershipTypeCode();
		String pageName = null;

		// Check that there is not already a card request current
		MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
		if (memMgr.isCardRequested(new Integer(membershipID))) {

			request.setAttribute("message", "There is already an outstanding card request for this member");
			request.setAttribute("membershipVO", memVO);
			pageName = MembershipUIConstants.PAGE_ViewCards;
		} else {
			// Add the membership to the request so that the view pages can use
			// it.
			request.setAttribute("membershipVO", memVO);
			UserSession userSession = getUserSession(request);
			String transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST;

			// Make sure that the membership can be renewed.
			// String invalidTransactionReason =
			// memVO.getInvalidForTransactionReason(transTypeCode);
			// if (invalidTransactionReason != null)
			// throw new
			// ValidationException("The membership group cannot be changed. " +
			// invalidTransactionReason);

			// Create a transaction group.
			// The transaction date, preferred commence date and product
			// effective
			// dates are initialised to today when constructed.
			TransactionGroup transGroup = new TransactionGroup(transTypeCode, userSession.getUser());

			transGroup.setMembershipType(memVO.getMembershipType());
			transGroup.addSelectedClient(memVO); // Also adds other group
			// members
			transGroup.setContextClient(memVO);
			transGroup.createTransactionsForSelectedClients();

			// Add the transaction group to the session
			MembershipSessionUtil.addTransactionContainer(request, transGroup);

			request.setAttribute("transactionGroup", transGroup);

			pageName = MembershipUIConstants.PAGE_MemberCardRequest;
		}
		forwardRequest(request, response, pageName);
	}

	private void showPage_confirmTransaction(HttpServletRequest request, HttpServletResponse response, TransactionGroup transGroup) throws ServletException, RemoteException {
		transGroup.calculateTransactionFee();
		request.setAttribute("transactionGroup", transGroup);
		forwardRequest(request, response, MembershipUIConstants.PAGE_ConfirmTransaction);
	}

	/**************************** Utility methods *****************************/

	/**
	 * Get a membership value object for the specified membership id.
	 */
	private MembershipVO getMembershipVO(String membershipID) throws ServletException, SystemException {
		return getMembershipVO(membershipID, null);
	}

	/**
	 * Get a membership value object for the specified membership id or clientNumber.
	 */
	private MembershipVO getMembershipVO(String membershipID, String clientNumber) throws ServletException, SystemException {
		MembershipVO memVO = null;
		if (membershipID != null && membershipID.length() > 0) {
			try {
				memVO = MembershipEJBHelper.getMembershipMgr().getMembership(new Integer(membershipID));
			} catch (Exception e) {
				throw new SystemException("Failed to get membership with ID '" + membershipID + "' : " + e.getMessage(), e);
			}
		} else if (clientNumber != null && !clientNumber.isEmpty()) {
			try {
				List<MembershipVO> results = (List<MembershipVO>) MembershipEJBHelper.getMembershipMgr().findMembershipByClientNumber(new Integer(clientNumber));
				if (results != null && results.size() == 1) {
					memVO = results.get(0);
				}
			} catch (Exception e) {
				throw new SystemException("Failed to get membership with clientNumber '" + clientNumber + "' : " + e.getMessage(), e);
			}
		} else {
			throw new SystemException("Failed to get membership with ID '" + membershipID + "' : ID is not valid.");
		}
		return memVO;
	}

	/* Get a client value object for the specified client id. */
	private ClientVO getClient(String clientNumberText) throws ServletException, SystemException, RemoteException {
		ClientVO clientVO = null;
		if (clientNumberText != null && clientNumberText.length() > 0) {
			ClientMgr cMgr = ClientEJBHelper.getClientMgr();
			clientVO = cMgr.getClient(new Integer(clientNumberText));
		}
		return clientVO;
	}

	/**
	 * Clean up resources
	 */
	public void destroy() {
	}

	/**
	 * Cancellation method for 1 click cancel of Memberships with failed direct debits. Limited to single memberships cancel direct debits Cancel membership Creates a blocker note. Requires "Cancel dd" privilege (MA)
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws RemoteException
	 * @throws ValidationException
	 * @throws PaymentException
	 */
	public void handleEvent_cancelFailedMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException, PaymentException {
		UserSession userSession = this.getUserSession(request);
		User user = userSession.getUser();
		if (!user.isPrivilegedUser(Privilege.PRIVILEGE_MEM_CANCEL_DD)) {
			throw new ValidationException("Your do not have the required privileges to perform this action");
		}

		// pass membership on
		String membershipIDStr = request.getParameter("membershipID");
		Integer membershipID = Integer.parseInt(membershipIDStr);
		MembershipMgrLocal membershipMgr = MembershipEJBHelper.getMembershipMgrLocal();
		MembershipVO memVo = membershipMgr.getMembership(membershipID);
		if (memVo.isGroupMember()) {
			throw new RemoteException("Only available for individual memberships");
		}
		if (MembershipVO.STATUS_CANCELLED.equals(memVo.getStatus())) {
			throw new ValidationException("The membership is already cancelled.");
		}

		String cancelReasonCode = "HON";
		String blockTransactions = request.getParameter("blockTransactions");
		String noteText = "DD dishonoured letter sent.  No success, cancel membership";
		boolean block = true;

		// cancel the direct debits

		String ddScheduleIDStr = request.getParameter("ddScheduleID");
		Integer ddScheduleID = Integer.parseInt(ddScheduleIDStr);

		PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();
		DirectDebitSchedule sched = paymentMgrLocal.getDirectDebitSchedule(ddScheduleID);
		PaymentTransactionMgrLocal paymentTxnMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();

		if (sched.isRenewalVersion()) { // remove
			try {
				paymentTxnMgrLocal.removeDirectDebitSchedule(ddScheduleID);
				paymentTxnMgrLocal.commit();
			} catch (Exception re) {
				paymentTxnMgrLocal.rollback();
				throw new ServletException(re);
			}
		} else { // cancel
			try {
				paymentTxnMgrLocal.cancelDirectDebitSchedule(ddScheduleID, user.getUserID());
				paymentTxnMgrLocal.commit();
			} catch (Exception re) {
				paymentTxnMgrLocal.rollback();
				throw new ServletException(re);
			}
		}

		// Create a new transaction group
		MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();

		// Create a TransactionGroup object to hold the common data in the
		// transaction
		TransactionGroup transactionGroup = TransactionGroupFactory.getTransactionGroupForCancel(user);
		TransactionGroupCancelImpl transGroup = (TransactionGroupCancelImpl) transactionGroup;

		// set to block transactions
		transGroup.setTransactionBlocking(block);

		transGroup.setMembershipType(memVo.getMembershipType());

		// Add the clients membership being cancelled to the transaction
		// group.
		transGroup.addSelectedClient(memVo);
		transGroup.setContextClient(memVo);

		// Create transactions for all selected clients
		transGroup.createTransactionsForSelectedClients();
		// Set the reason for the cancellation
		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		ReferenceDataVO cancellationReason = refMgr.getCancelReason(cancelReasonCode);
		if (cancellationReason == null) {
			throw new SystemException("Failed to get cancellation reason '" + cancelReasonCode + "'");
		}

		transGroup.setTransactionReason(cancellationReason);
		Collection transactionList = transGroup.submit();
		if (transactionList == null) {
			throw new ValidationException("The transaction has already been submitted.<p>You won't be able to see the result so try looking at the membership to see if the changes have been made.");
		}

		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();

		DateTime now = new DateTime();
		ClientNote clientNote = new ClientNote();
		clientNote.setClientNumber(memVo.getClientNumber());
		clientNote.setBusinessType(BusinessType.BUSINESS_TYPE_MEMBERSHIP);
		clientNote.setCreated(now);
		clientNote.setCreateId(user.getUserID());

		// get first note type with attribute - blocks transactions
		Collection<NoteType> noteTypes = clientMgr.getNoteType(true);
		clientNote.setNoteType(noteTypes.iterator().next().getNoteType());
		clientNote.setNoteText(noteText);
		clientMgr.createClientNote(clientNote);

		request.setAttribute("contextClientVO", memVo.getClient());
		request.setAttribute("transactionList", transactionList);
		request.setAttribute("membershipVO", memVo);

		forwardRequest(request, response, MembershipUIConstants.PAGE_ViewPaymentMethod);
	}
}
