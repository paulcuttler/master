package com.ract.membership.ui;

/**
 * Title:        Membership system
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author
 * @version 1.0
 */

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.ract.client.Client;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.cad.CadExportVO;
import com.ract.membership.DiscountTypeVO;
import com.ract.membership.MembershipCardVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.TransactionGroup;
import com.ract.membership.club.AffiliatedClubVO;
import com.ract.payment.PaymentTypeVO;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitFrequency;
import com.ract.security.Privilege;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.HTMLUtil;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;
import com.ract.util.StringUtil;

public class MembershipUIHelper {

    public static String getAgentSelectFromArrayList(String searchTerm) {
	String name = "location";
	Collection agentLocationList = null;
	try {
	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	    agentLocationList = membershipMgr.getAgentLocationList();

	} catch (RemoteException ex) {
	    LogUtil.warn("getAgentSelectFromArrayList", "Unable to get agent location list " + ex);
	}
	return HTMLUtil.getSelectList(0, searchTerm, agentLocationList, name, null);
    }

    public static void updateFromRequest(HttpServletRequest request, DiscountTypeVO discountType) throws RemoteException {

	discountType.setDiscountTypeCode(request.getParameter("discountCode"));

	discountType.setDiscountPeriod(request.getParameter("discountPeriodYear") + ":" + request.getParameter("discountPeriodMonth") + ":" + request.getParameter("discountPeriodDay") + ":0:0:0:0");

	discountType.setDescription(request.getParameter("description"));
	discountType.setStatus(request.getParameter("status"));
	discountType.setAutomaticDiscount(ServletUtil.getBooleanParam("automatic", request));
	discountType.setMinimumFeeApplicable(ServletUtil.getBooleanParam("minimumFeeApplicable", request));
	discountType.setMembershipValueReducing(ServletUtil.getBooleanParam("reducesMembershipValue", request));
	discountType.setRecurringDiscount(ServletUtil.getBooleanParam("recurring", request));
	discountType.setSystemControlled(ServletUtil.getBooleanParam("systemControlled", request));
	discountType.setDiscountAmount(ServletUtil.getDoubleParam("discountAmount", request));
	discountType.setDiscountPercentage(ServletUtil.getDoubleParam("discountPercentage", request));
	discountType.setDiscountAccountID(ServletUtil.getIntegerParam("discountAccount", request));
	discountType.setEffectiveFromDate(ServletUtil.getDateParam("effectiveFrom", request));
	discountType.setEffectiveToDate(ServletUtil.getDateParam("effectiveTo", request));
	String po = request.getParameter("percentageOf");
	if (po != null && po.equals(""))
	    po = null;
	discountType.setPercentageOf(po);

    }

    public static void updateFromRequest(HttpServletRequest request, AffiliatedClubVO club) {
	club.setClubCode(ServletUtil.getStringParam("clubCode", request));
	club.setDescription(ServletUtil.getStringParam("clubDescription", request));
	club.setCreateDate(ServletUtil.getDateParam("clubCreateDate", request));
	club.setActive(new Boolean(ServletUtil.getBooleanParam("clubActive", request)));
    }

    public static String getDiscountLabel(Integer clientNumber, String feeTypeCode, String discountTypeCode) {
	return "disc_" + clientNumber + "_" + HTMLUtil.cleanString(feeTypeCode) + "_" + HTMLUtil.cleanString(discountTypeCode);
    }

    public static String buildMenu(MembershipVO memVO, UserSession userSession, ProductVO prodVO, double amountUnpaid) throws Exception {

	// Set the popup menu list depending on the membership
	StringBuffer menuOptions = new StringBuffer();
	// StringBuffer reasonList = new StringBuffer();

	menuOptions.append("<div dojoType=\"dijit.form.DropDownButton\"><span>Actions</span>");
	menuOptions.append(FileUtil.NEW_LINE);
	menuOptions.append("<div dojoType=\"dijit.Menu\" id=\"menu\">");
	menuOptions.append(FileUtil.NEW_LINE);

	boolean renewable = false;
	String invalidReason = null;

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW);
	renewable = invalidReason == null;
	appendMenuItem(menuOptions, "Renew", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN);
	appendMenuItem(menuOptions, "Rejoin", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL);
	appendMenuItem(menuOptions, "Cancel", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP);
	if (!memVO.isGroupMember()) {
	    invalidReason = appendInvalidReason(invalidReason, "Member is not part of a group.");
	} else {
	    if (!memVO.isPrimeAddressee()) {
		invalidReason = appendInvalidReason(invalidReason, "Member is not the prime addressee.");
	    }
	}
	appendMenuItem(menuOptions, "Change group", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA);
	MembershipUIHelper.appendMenuItem(menuOptions, "Change group PA", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT);
	MembershipUIHelper.appendMenuItem(menuOptions, "Change product", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION);
	if (prodVO != null && !prodVO.hasOptionalBenefits(userSession.getUser())) {
	    invalidReason = "Product does not have optional benefits.";
	}
	appendMenuItem(menuOptions, "Change product option", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP);
	MembershipUIHelper.appendMenuItem(menuOptions, "Create group", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP);
	if (userSession != null && !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_EDIT)) {
	    invalidReason = appendInvalidReason(invalidReason, "You do not have the required privilege.");
	}
	appendMenuItem(menuOptions, "Edit membership", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD);
	appendMenuItem(menuOptions, "Hold", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP);
	if (!memVO.isGroupMember()) {
	    invalidReason = appendInvalidReason(invalidReason, "Member is not part of a group.");
	} else {
	    if (!memVO.isPrimeAddressee()) {
		invalidReason = appendInvalidReason(invalidReason, "Member is not the prime addressee.");
	    }
	}
	appendMenuItem(menuOptions, "Renew group", invalidReason);
	if (!renewable) {
	    renewable = invalidReason == null;
	}

	invalidReason = null;
	// require a valid product and not to be undone
	if (prodVO == null) {
	    invalidReason = appendInvalidReason(invalidReason, "Membership does not have a valid product.");
	}
	if (MembershipVO.STATUS_UNDONE.equals(memVO.getStatus())) {
	    invalidReason = appendInvalidReason(invalidReason, "Membership is undone.");
	}

	if (memVO.getClient().getStatus().equals(Client.STATUS_DECEASED)) {
	    invalidReason = appendInvalidReason(invalidReason, "Client is deceased.");
	}
	
	DirectDebitAuthority ddAuthority = memVO.getDirectDebitAuthority();
	if (ddAuthority != null || renewable) {
		appendStartSubMenuItem(menuOptions, "Print");

		// Disable the ability to print the general advice - APPS-296 GS
	//	appendMenuItem(menuOptions, "General advice", "mnuPrintAdvice", invalidReason);
	
		if (ddAuthority != null) {
		    appendMenuItem(menuOptions, "DD Schedule advice", "mnuPrintDDAdvice", invalidReason);
		}
		if (renewable) {
		    appendMenuItem(menuOptions, "Renewal notice", "mnuPrintRenewalNotice", invalidReason);
		}
		appendEndSubMenuItem(menuOptions);
	}

	appendStartSubMenuItem(menuOptions, "Email");
	appendMenuItem(menuOptions, "General advice", "mnuEmailGeneralAdvice", invalidReason);
	if (ddAuthority != null) {
	    appendMenuItem(menuOptions, "DD Schedule advice", "mnuEmailDDAdvice", invalidReason);
	}
	if (renewable) {
	    appendMenuItem(menuOptions, "Renewal advice", "mnuEmailRenewalAdvice", invalidReason);
	}
	appendEndSubMenuItem(menuOptions);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST);
	if (userSession != null && !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_EDIT)) {
	    invalidReason = appendInvalidReason(invalidReason, "You do not have the required privilege.");
	}

	String memberCardDataStore = MembershipCardVO.DATA_STORE_DATABASE;
	try {
	    memberCardDataStore = FileUtil.getProperty("master", "memberCardDataStore");
	} catch (Exception e1) {
	    LogUtil.warn(MembershipUIHelper.class, "Could not determine member card datastore, defaulting to: " + memberCardDataStore);
	}

	String memberCardForceDB = "false";
	try {
	    memberCardForceDB = FileUtil.getProperty("master", "memberCardForceDB");
	} catch (Exception e1) {
	    LogUtil.warn(MembershipUIHelper.class, "Could not determine member card force DB, defaulting to: " + memberCardForceDB);
	}
	Boolean forceDB = Boolean.parseBoolean(memberCardForceDB);

	if (forceDB || memberCardDataStore.equals(MembershipCardVO.DATA_STORE_DATABASE)) {
	    appendMenuItem(menuOptions, "Request card", invalidReason);
	}

	invalidReason = null;
	if (userSession != null && !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CAD_EXPORT)) {
	    invalidReason = appendInvalidReason(invalidReason, "You do not have the required privilege.");
	}
	if (MembershipVO.STATUS_UNDONE.equals(memVO.getStatus())) {
	    invalidReason = "Membership is undone.";
	}
	appendMenuItem(menuOptions, "Send to CAD", invalidReason);

	invalidReason = null;
	if (memVO.getCreditAmount() == 0) {
	    invalidReason = appendInvalidReason(invalidReason, "No credit available to manage.");
	}
	if (!userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_MANAGE_CREDIT)) {
	    invalidReason = appendInvalidReason(invalidReason, "You do not have the required privilege.");
	}
	appendMenuItem(menuOptions, "Manage credit", invalidReason);

	invalidReason = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER);
	appendMenuItem(menuOptions, "Transfer", invalidReason);

	// Not a "transaction" per se.

	// either no message exists, non blocking message exists or blocking but
	// have administrator privilege.
	invalidReason = null;
	appendMenuItem(menuOptions, "Add note", invalidReason);

	invalidReason = null;
	appendMenuItem(menuOptions, "Toggle email renewals", invalidReason);

	invalidReason = null;
	if (memVO != null && MembershipVO.STATUS_CANCELLED.equals(memVO.getStatus())) {
	    invalidReason = appendInvalidReason(invalidReason, "You can't manage gifts on a cancelled membership.");
	}

	invalidReason = null;
	if (amountUnpaid > 0) {
	    invalidReason = appendInvalidReason(invalidReason, "There is an oustanding amount.");
	}
	appendMenuItem(menuOptions, "Manage gifts", invalidReason);

	menuOptions.append("</div>");
	menuOptions.append(FileUtil.NEW_LINE);
	menuOptions.append("</div>");
	menuOptions.append(FileUtil.NEW_LINE);

	return menuOptions.toString();
    }

    private static String appendInvalidReason(String currentReason, String invalidReason) {
	if (invalidReason != null) {
	    if (currentReason == null) {
		currentReason = "";
	    }
	    if (currentReason.trim().length() > 0) {
		currentReason += " " + invalidReason;// multiple
	    }
	}
	return currentReason;
    }

    public static String getDiscountAttributeLabel(Integer clientNumber, String feeTypeCode, String discountTypeCode, String discountAttributeName) {
	return getDiscountLabel(clientNumber, feeTypeCode, discountTypeCode) + "_" + HTMLUtil.cleanString(discountAttributeName);
    }

    public static String displayMembershipLine(MembershipVO memVO, boolean link, boolean format) {
	String membershipLine = "";
	if (link) {
	    membershipLine += "<a class=\"actionItem\" target=\"rightFrame\" ";
	    membershipLine += "title=\"View the details of this membership.\" ";
	    membershipLine += "href=" + MembershipUIConstants.PAGE_MembershipUIC + "?event=viewClientSummary_viewMembership&membershipID=" + memVO.getMembershipID() + ">";
	}
	membershipLine += memVO.getMembershipTypeCode() + (memVO.getProductCode() == null ? "" : " - ");
	membershipLine += memVO.getProductCode() + ": ";
	try {
	    membershipLine += (format ? MembershipUIHelper.formatMembershipStatusHTML(memVO.getStatus()) + "&nbsp;" : memVO.getStatus()) + " Expires: ";
	} catch (RemoteException e) {
	    e.printStackTrace();
	}
	if (format) {
	    membershipLine += MembershipUIHelper.formatMembershipExpiryDateHTML(memVO.getExpiryDate(), new DateTime());
	} else {
	    membershipLine += memVO.getExpiryDate().formatShortDate();
	}
	if (link) {
	    membershipLine += "</a>";
	}
	return membershipLine;
    }

    /**
     * Return the membership status formatted for HTML. If the status is Active
     * the show it as a normal data value. Any other status is shown in red.
     */
    public static String formatMembershipStatusHTML(String status) {
	String formattedStatus;
	if (status == null) {
	    status = "??????";
	}
	if (MembershipVO.STATUS_ACTIVE.equals(status) || MembershipVO.STATUS_FUTURE.equals(status)) {
	    formattedStatus = "<span class=\"dataValueGreen\">" + status + "</span>";
	} else if (MembershipVO.STATUS_INRENEWAL.equals(status)) {
	    formattedStatus = "<span class=\"dataValueAmber\">" + status + "</span>";
	} else {
	    formattedStatus = "<span class=\"dataValueRed\">" + status + "</span>";
	}
	return formattedStatus;
    }

    /**
     * Append the menu item to the list of menu items. Append a comma first if
     * the list is not empty.
     * 
     */
    public static void appendMenuItem(StringBuffer sb, String menuItem, String invalidReason) {
	appendMenuItem(sb, menuItem, null, invalidReason);
    }

    public static void appendMenuItem(StringBuffer sb, String menuItem, String menuItemName, String invalidReason) {
	boolean disabled = invalidReason != null;
	String message = invalidReason;

	if (menuItemName == null) {
	    menuItemName = "";

	    String[] menuItems = menuItem.split(" ");
	    for (int i = 0; i < menuItems.length; i++) {
		menuItemName += menuItems[i].substring(0, 1).toUpperCase() + menuItems[i].substring(1, menuItems[i].length());
	    }

	    menuItemName = "mnu" + menuItemName;
	}

	if (sb.length() > 0) {
	    sb.append(FileUtil.NEW_LINE);
	}
	sb.append("<div dojoType=\"dijit.MenuItem\"");
	sb.append(disabled ? " disabled=\"true\"" : "");
	sb.append(" onClick=\"doMenuItem('");
	sb.append(menuItemName);
	sb.append("');\"");
	if (message != null && message.trim().length() > 0) {
	    sb.append(" title=\"" + message + "\"");
	}
	sb.append(">");
	sb.append(menuItem);
	sb.append("</div>");
    }

    /**
     * Append the start of a sub menu container to the list of menu items.
     */
    public static void appendStartSubMenuItem(StringBuffer sb, String menuItem) {
	if (sb.length() > 0) {
	    sb.append(FileUtil.NEW_LINE);
	}
	sb.append("<div dojoType=\"dijit.PopupMenuItem\">\n<span>");
	sb.append(menuItem);
	sb.append("</span>\n<div dojoType=\"dijit.Menu\">");
    }

    /**
     * Append the end of a sub menu container to the list of menu items.
     */
    public static void appendEndSubMenuItem(StringBuffer sb) {
	sb.append("</div>\n</div>");
    }

    /**
     * Return a HTML formatted string with the expiry date in it. If the
     * effective date is null it will default to today. If the expiry date is
     * after the effective date it will be displayed in black. If the expiry
     * date is before the effective date it will be displayed in red. If the
     * expiry date is on the same day as the effective date it will be displayed
     * in amber.
     */
    public static String formatMembershipExpiryDateHTML(DateTime expiryDate, DateTime effectiveDate) {
	String formattedDate = "";
	if (effectiveDate == null) {
	    effectiveDate = new DateTime();
	}
	if (expiryDate == null) {
	    formattedDate = "<span class=\"dataValueRed\">??????</span>";
	} else if (expiryDate.afterDay(effectiveDate)) {
	    formattedDate = "<span class=\"dataValue\">" + expiryDate.formatShortDate() + "</span>";
	} else if (expiryDate.beforeDay(effectiveDate)) {
	    formattedDate = "<span class=\"dataValueRed\">" + expiryDate.formatShortDate() + "</span>";
	} else {
	    formattedDate = "<span class=\"dataValueAmber\">" + expiryDate.formatShortDate() + "</span>";
	}
	return formattedDate;
    }

    public static String getTransactionHeadingText(String transTypeCode) {
	String headingText = "Unknown";
	if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode)) {
	    headingText = "New membership";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(transTypeCode)) {
	    headingText = "Renew membership";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) {
	    headingText = "Rejoin membership";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(transTypeCode)) {
	    headingText = "Transfer membership";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(transTypeCode)) {
	    headingText = "Hold membership";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(transTypeCode)) {
	    headingText = "Cancel membership";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transTypeCode)) {
	    headingText = "Change product";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(transTypeCode)) {
	    headingText = "Change product option";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(transTypeCode)) {
	    headingText = "Create membership group";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(transTypeCode)) {
	    headingText = "Change membership group";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(transTypeCode)) {
	    headingText = "Renew membership group";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(transTypeCode)) {
	    headingText = "Remove from membership group";
	} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(transTypeCode)) {
	    headingText = "Change prime addressee";
	}
	return headingText;
    }

    /**
     * Log the parameters on the http request.
     */
    // public static void logParameters(String className, HttpServletRequest
    // request)
    // {
    // if(request != null)
    // {
    // for(Enumeration e = request.getParameterNames(); e.hasMoreElements(); )
    // {
    // String pName = (String)e.nextElement();
    // log(className, pName + " = " + request.getParameter(pName));
    // }
    // }
    // }

    /**
     * Get the HTML select box given a member count and frequency.
     * 
     * @param memberCount
     * @param frequency
     * @return
     */
    public static String getFrequencyHTML(int memberCount, String frequency, String productCode) {
	StringBuffer html = new StringBuffer();
	if (productCode != null && productCode.equals("Lifestyle")) {
	    html.append(getFrequencyOptionLabelHTML(frequency, DirectDebitFrequency.ANNUALLY));
	} else {
		if (memberCount <= 1) {
		    html.append("<select name=\"frequencyOfDebit\">");
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.MONTHLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.QUARTERLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.HALF_YEARLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.ANNUALLY));
		    html.append("</select>");
		} else if (memberCount == 2) {
		    html.append("<select name=\"frequencyOfDebit\">");
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.MONTHLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.QUARTERLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.HALF_YEARLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.ANNUALLY));
		    html.append("</select>");
		} else {
		    html.append("<select name=\"frequencyOfDebit\">");
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.MONTHLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.QUARTERLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.HALF_YEARLY));
		    html.append(getFrequencyOptionHTML(frequency, DirectDebitFrequency.ANNUALLY));
		    html.append("</select>");
		}
	}
	LogUtil.debug("getFrequencyHTML", html.toString());
	return html.toString();
    }

    private static String getFrequencyOptionHTML(String targetFrequency, DirectDebitFrequency frequency) {
	return "<option value=\"" + frequency.getDescription() + "\" " + (targetFrequency == null || frequency.getDescription().equals(targetFrequency) ? "selected" : "") + ">" + frequency.getDescription() + "</option>";
    }

    private static String getFrequencyOptionLabelHTML(String targetFrequency, DirectDebitFrequency frequency) {
    	return "<input name=\"frequencyOfDebit\" readonly=\"readonly\" style=\"border-style:none;font-weight:bold;\" value=\"" + frequency.getDescription() + "\"</input>";
    }

    // <option value="<%=payType.getPaymentTypeCode()%>"
    // <%=selected%>><%=description +
    // StringUtil.toProperCase(payType.getDescription())%></option>
    private static String getPaymentTypeOptionHTML(PaymentTypeVO payType, boolean selected, String description) {
	return "<option value=\"" + payType.getPaymentTypeCode() + "\" " + (selected ? "selected" : "") + ">" + description + StringUtil.toProperCase(payType.getDescription()) + "</option>";
    }

    /**
     * Get the payment type select box
     * 
     * @param paymentTypeList
     * @param paymentTypeCode
     * @param memTxVO
     * @param paidImmediateAmount
     * @return
     */
    public static String getPaymentTypeHTML(Collection paymentTypeList, String paymentTypeCode, String transactionTypeCode, MembershipVO primeMemVO, TransactionGroup transGroup, boolean isAdmin, boolean editMode) {

	LogUtil.debug("getPaymentTypeHTML", "paymentTypeCode" + paymentTypeCode);
	LogUtil.debug("getPaymentTypeHTML", "transactionTypeCode" + transactionTypeCode);
	StringBuffer paymentTypeSelect = new StringBuffer();

	boolean paidImmediateAmount = false;
	if (primeMemVO != null) {
	    try {
		paidImmediateAmount = transGroup.hasPAPaidFirstPayment(primeMemVO);
	    } catch (Exception ex) {
		LogUtil.warn("com.ract.membership.ui.MembershipUIHelper", ex.getMessage());
	    }
	}

	int membershipTermYears = 1;
	if (transGroup != null) {
	    membershipTermYears = transGroup.getMembershipTerm() != null ? transGroup.getMembershipTerm().getTotalYears() : 1; // default
															       // to
															       // 1
	}

	boolean selected;
	String description;
	String payTypeCashCode;
	Iterator payTypeListIterator = paymentTypeList.iterator();
	boolean validPaymentMethod = true;
	boolean setDefaultPaymentMethod = false;
	PaymentTypeVO payType = null;
	ArrayList validPaymentTypeList = new ArrayList();
	boolean directDebitAllowed = false;
	try {
	    directDebitAllowed = primeMemVO.getProduct().isDirectDebitAllowed();
	} catch (RemoteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	paymentTypeSelect.append("<select name=\"paymentType\" size=\"3\" onchange=\"showPaymentType(document.all.paymentType.options[document.all.paymentType.selectedIndex].value);\" >");

	while (payTypeListIterator.hasNext()) {
	    validPaymentMethod = true;
	    payType = (PaymentTypeVO) payTypeListIterator.next();

	    // LogUtil.log("getPaymentTypeHTML",payType.toString());

	    if (payType.getPaymentTypeCode().equals(paymentTypeCode)) {
		selected = true;
	    } else {
		selected = false;
	    }

	    payTypeCashCode = payType.getCashTypeCode();

	    if (PaymentTypeVO.CASHTYPE_CASH.equals(payTypeCashCode)) {
		description = "Pay now by ";
	    } else if (PaymentTypeVO.CASHTYPE_DIRECTDEBIT.equals(payTypeCashCode) || PaymentTypeVO.CASHTYPE_CREDITCARD.equals(payTypeCashCode)) {
		description = "Direct debit by ";
		/**
		 * @todo dd is invalid if membership term greater than 1 year.
		 */
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(transactionTypeCode) || (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(transactionTypeCode) && paidImmediateAmount) || membershipTermYears > 1 || !directDebitAllowed) {
		    validPaymentMethod = false;

		    // this may be selected
		    if (selected) {
			// set cash as default payment type
			setDefaultPaymentMethod = true;
		    }
		}
	    } else {
		description = "??? Unknown ??? ";
	    }

	    if (validPaymentMethod) {
		validPaymentTypeList.add(new ValidPaymentType(payType, selected, description));
	    }

	}

	ValidPaymentType vPayType = null;
	for (int i = 0; i < validPaymentTypeList.size(); i++) {

	    vPayType = (ValidPaymentType) validPaymentTypeList.get(i);
	    // LogUtil.log("zzzz","vPayType"+vPayType.getPaymentType().getPaymentTypeCode()+" "+vPayType.getSelected()+" : "+vPayType.getDescription());
	    if ((vPayType.getPaymentType().getCashTypeCode().equals(PaymentTypeVO.CASHTYPE_CASH)) && setDefaultPaymentMethod) {
		// LogUtil.log("","cash");
		// cash
		paymentTypeSelect.append(getPaymentTypeOptionHTML(vPayType.getPaymentType(), true, vPayType.getDescription()));
	    } else {
		paymentTypeSelect.append(getPaymentTypeOptionHTML(vPayType.getPaymentType(), vPayType.isSelected(), vPayType.getDescription()));
	    }
	}

	if (isAdmin && !editMode) {
	    paymentTypeSelect.append("<option value=\"Repair\">Repair</option>");
	}

	paymentTypeSelect.append("</select>");

	return paymentTypeSelect.toString();
    }

    public static String getCadStatusHTML(ClientVO clientVO, ArrayList warningList) {
	String cadStatusHTML = "";
	// See if there is an outstanding membership data update waiting to go
	// to CAD
	try {
	    ArrayList cadList = CommonEJBHelper.getCadExportMgr().getCadExportList(clientVO.getClientNumber());
	    if (cadList != null && !cadList.isEmpty()) {
		CadExportVO cadVO;
		boolean found = false;
		// Try and find a cad record without a tran-no.
		// the tran-no gets updated when the record is actually sent to
		// CAD.
		for (int i = 0; i < cadList.size() && !found; i++) {
		    cadVO = (CadExportVO) cadList.get(i);
		    found = cadVO.getTranNo() == null || cadVO.getTranNo().intValue() < 1;
		}
		cadStatusHTML = found ? "<span class=\"helpTextSmall\">CAD update pending</span>" : "";
	    }
	    // rely on CAD synch program for this
	    // else
	    // {
	    // cadStatusHTML =
	    // "<span class=\"dataValueRed\">Not in CAD!</span>";
	    // }
	} catch (Exception e) {
	    warningList.add(e.getMessage());
	    cadStatusHTML = "<span class=\"dataValueRed\">CAD lookup failed!</span>";
	}
	return cadStatusHTML;
    }

}

/**
 * Internal convenience class
 * 
 * @author jyh
 * @version 1.0
 */
class ValidPaymentType {
    public ValidPaymentType(PaymentTypeVO payType, boolean sel, String desc) {
	this.paymentType = payType;
	this.selected = sel;
	this.description = desc;
	LogUtil.debug(this.getClass(), "desc = " + desc);
    }

    private PaymentTypeVO paymentType;

    public PaymentTypeVO getPaymentType() {
	return this.paymentType;
    }

    private boolean selected;

    public boolean isSelected() {
	return this.selected;
    }

    private String description;

    public String getDescription() {
	return this.description;
    }
}
