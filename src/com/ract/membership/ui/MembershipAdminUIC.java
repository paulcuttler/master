package com.ract.membership.ui;

/**
 * Title:         Membership Administration User Interface Controller
 * Description:
 * Company:       RACT
 * @author        Leigh Giles
 * @version 1.0
 */

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.common.ApplicationServlet;
import com.ract.common.CommonEJBHelper;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.admin.AdminCodeDescription;
import com.ract.common.ui.CommonUIConstants;
import com.ract.membership.DiscountTypeVO;
import com.ract.membership.FeeSpecificationVO;
import com.ract.membership.FeeTypeVO;
import com.ract.membership.MembershipAccountVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipProfileVO;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipRepairMgr;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductBenefitTypeVO;
import com.ract.membership.ProductVO;
import com.ract.membership.RenewalMgr;
import com.ract.membership.RenewalMgrBean;
import com.ract.membership.club.AffiliatedClubHelper;
import com.ract.membership.club.AffiliatedClubVO;
import com.ract.security.Privilege;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.ServletUtil;
import com.ract.util.StringUtil;
import com.ract.util.TreeList;

public class MembershipAdminUIC extends ApplicationServlet {

    private static final String CONTENT_TYPE_HTML = "text/html";

    private static final String CONTENT_TYPE_XML = "text/xml";

    // /**
    // * Process the HTTP Get request
    // */
    // public void doGet(HttpServletRequest request, HttpServletResponse
    // response)
    // throws ServletException, IOException
    // {
    // doPost(request, response);
    // }
    //
    // /**
    // * Process the HTTP Post request
    // */
    // public void doPost(HttpServletRequest request, HttpServletResponse
    // response)
    // throws ServletException, IOException
    // {
    // this.setDebug(true);
    // String event = request.getParameter("event");
    // handleEvent(event, request, response);
    // }

    /************************* Event handler methods **************************/
    /* ================================Affiliated Club========================== */

    /**
     * Handle the Maintain Affiliated Club event Forward the request to the
     * Maintain Affiliated Club List page to start the maintenance process
     */
    public void handleEvent_selectMaintainAffiliatedClub(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, AffiliatedClubHelper.getSortedClubList());
	request.getSession().setAttribute("pageTitle", "Affiliated Clubs");
	request.getSession().setAttribute("buttonMessage", "Add a new AffiliatedClub.");
	request.getSession().setAttribute("createEvent", "maintainAffiliatedClub_createAffiliatedClub");
	request.getSession().setAttribute("selectEvent", "maintainAffiliatedClub_selectAffiliatedClub");
	request.getSession().setAttribute("codeName", "clubCode");
	request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MembershipAdminUIC);
	request.getSession().setAttribute("origCode", "");

	request.setAttribute("Refresh", "No");
	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
    }

    /**
     * Handle the Maintain AffiliatedClub List create AffiliatedClub event.
     * Forward the request straight to the Maintain AffiliatedClub page for new
     */
    public void handleEvent_maintainAffiliatedClub_createAffiliatedClub(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("Refresh", "No");
	gotoAffiliatedClubPage(request, response, "", new AffiliatedClubVO(), " ");
    }

    /**
     * Handle the Maintain AffiliatedClub List select AffiliatedClub event.
     * Forward the request straight to the Maintain AffiliatedClub page for
     * existing
     */
    public void handleEvent_maintainAffiliatedClub_selectAffiliatedClub(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	String clubCode = request.getParameter("clubCode");

	AffiliatedClubVO affiliatedClubVO = null;
	try {
	    affiliatedClubVO = AffiliatedClubHelper.getAffiliatedClub(clubCode);
	} catch (Exception oe) {
	    throw new SystemException("Unable to get AffiliatedClub for account id " + clubCode, oe);
	}

	request.setAttribute("Refresh", "No");
	gotoAffiliatedClubPage(request, response, clubCode, affiliatedClubVO, " ");
    }

    /**
     * Handle the save button event on the Maintain AffiliatedClub page. Process
     * the transaction and save it to the database. Show the transaction
     * complete page.
     */
    public void handleEvent_maintainAffiliatedClub_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
	outputRequestParameters(request);
	outputRequestAttributes(request);

	// get code
	String clubCode = request.getParameter("clubCode");
	AffiliatedClubVO club = (AffiliatedClubVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_AFFILIATED_CLUB_VO);
	LogUtil.log(this.getClass(), "club=" + club);
	MembershipUIHelper.updateFromRequest(request, club);

	// are we creating?
	String origCode = (String) request.getSession().getAttribute("origCode");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	StringBuffer displayText = new StringBuffer();

	if (StringUtil.isNull(clubCode)) {
	    displayText.append("Code is SPACES. No Action taken.");
	} else {

	    if (StringUtil.isNull(origCode)) {
		try {
		    // create
		    membershipMgr.createClub(club);
		    displayText.append("Created successfully");
		    request.setAttribute("Refresh", "Yes");
		    request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, AffiliatedClubHelper.getSortedClubList());
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    // update
		    membershipMgr.updateClub(club);
		    displayText.append("Saved successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    }
	}

	gotoAffiliatedClubPage(request, response, clubCode, club, displayText.toString());
    }

    /**
     * Handle the delete button event on the Maintain AffiliatedClub page.
     * Process the transaction and delete it from the database. Show the
     * transaction complete page.
     */
    public void handleEvent_maintainAffiliatedClub_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	AffiliatedClubVO club = (AffiliatedClubVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_AFFILIATED_CLUB_VO);

	MembershipUIHelper.updateFromRequest(request, club);

	String clubCode = club.getClubCode();
	StringBuffer displayText = new StringBuffer();

	try {
	    // delete
	    MembershipEJBHelper.getMembershipMgr().deleteClub(club);
	    displayText.append("Club data deleted successfully");
	    this.removeFromAdminList(request, clubCode, club.getDescription());
	    request.setAttribute("Refresh", "Yes");
	} catch (Exception r) {
	    displayText.append(r.getMessage());
	}

	// Now return to the Maintain Discount List page
	gotoAffiliatedClubPage(request, response, "", new AffiliatedClubVO(), displayText.toString());
    }

    /* ============================FEE SPECIFICATIONS================== */
    /**
     * Handle the Maintain Fee Specification event Forward the request to the
     * Tree List page to start the maintenance process
     */
    public void handleEvent_selectMaintainFeeSpecification(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection<FeeSpecificationVO> tableList = membershipMgr.getOrderedFeeSpecifications();

	TreeList tree = new TreeList("Fee Specification Table", MembershipUIConstants.PAGE_MembershipAdminUIC, "addFeeSpecification");

	int id = 0;
	int root = 0;
	int transactionBranch = 0;
	int feeBranch = 0;

	FeeSpecificationVO tableVO = null;
	String url = null;
	Integer thisId = new Integer(0);
	String rootName = "";
	String thisRootName = "";
	String transactionType = "";
	String thisTransactionType = "";
	String feeType = "";
	String thisFeeType = "";
	String groupNo = "";
	String thisGroupNo = "";

	if (tableList.size() > 0) {
	    for (Iterator<FeeSpecificationVO> i = tableList.iterator(); i.hasNext();) {
		tableVO = i.next();
		LogUtil.log(this.getClass(), "tableVO=" + tableVO);
		thisId = tableVO.getFeeSpecificationID();
		LogUtil.log(this.getClass(), "thisId=" + thisId);
		thisRootName = tableVO.getProductCode();

		if (thisRootName == null) {
		    thisRootName = tableVO.getProductBenefitCode();
		}
		LogUtil.log(this.getClass(), "thisRootName=" + thisRootName);
		thisTransactionType = tableVO.getTransactionTypeCode();
		if ((thisTransactionType == null) || (thisTransactionType.length() == 0)) {
		    thisTransactionType = "Default";
		}
		LogUtil.log(this.getClass(), "thisTransactionType=" + thisTransactionType);

		thisFeeType = tableVO.getFeeTypeCode();
		thisGroupNo = new Integer(tableVO.getGroupNumber()).toString();

		LogUtil.log(this.getClass(), "thisFeeType=" + thisFeeType);
		LogUtil.log(this.getClass(), "thisGroupNo=" + thisGroupNo);

		if (!rootName.equals(thisRootName.trim())) {
		    LogUtil.log(this.getClass(), "new root");
		    // new root
		    root = tree.addRoot(thisRootName.trim());
		    rootName = thisRootName.trim();
		    transactionType = "";
		}

		if (!transactionType.equals(thisTransactionType.trim())) {
		    LogUtil.log(this.getClass(), "new branch transactionType");
		    // new branch
		    transactionType = thisTransactionType.trim();
		    transactionBranch = tree.addBranch(transactionType, root);
		    feeType = "";
		}

		if (!feeType.equals(thisFeeType.trim())) {
		    LogUtil.log(this.getClass(), "new branch feeType");
		    // new branch
		    feeType = thisFeeType.trim();
		    feeBranch = tree.addBranch(feeType, transactionBranch);
		    groupNo = "";
		}

		if (!groupNo.equals(thisGroupNo)) {
		    LogUtil.log(this.getClass(), "groupNo=" + groupNo);
		    url = MembershipUIConstants.PAGE_MembershipAdminUIC + "?event=selectFeeSpecification" + "&feeSpecId=" + thisId.toString();
		    tree.addLeaf(thisGroupNo, feeBranch, url);
		    groupNo = thisGroupNo;
		}
	    }
	}

	forwardRequest(request, response, tree.deploy(request));
    }

    public void handleEvent_selectFeeSpecification(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	String feeSpecId = request.getParameter("feeSpecId");
	FeeSpecificationVO feeSpecificationVO = null;
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection feeSpecList = null;
	try {
	    Integer fs = new Integer(feeSpecId);
	    feeSpecificationVO = membershipMgr.getFeeSpecification(fs);
	    LogUtil.log(this.getClass(), "feeSpecificationVO=" + feeSpecificationVO);
	    feeSpecList = membershipMgr.getFeeSpecifications(feeSpecificationVO.getProductCode(), feeSpecificationVO.getProductBenefitCode(), feeSpecificationVO.getMembershipTypeCode(), feeSpecificationVO.getTransactionTypeCode(), feeSpecificationVO.getFeeTypeCode(), feeSpecificationVO.getGroupNumber());
	    LogUtil.log(this.getClass(), "feeSpecList=" + feeSpecList);
	} catch (Exception oe) {
	    throw new SystemException("Unable to get FeeSpecification for ID " + feeSpecId, oe);
	}

	gotoFeeSpecificationPage(request, response, feeSpecId, feeSpecList, " ");
    }

    public void handleEvent_addFeeSpecification(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("Refresh", "No");
	gotoFeeSpecificationPage(request, response, null, new ArrayList(), " ");
    }

    /**
     * Handle the save button event on the Maintain FeeSpecification page.
     * Process the transaction and save it to the database. Show the transaction
     * complete page.
     */
    public void handleEvent_maintainFeeSpecification_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {

	Collection feeSpecList = (Collection) request.getSession().getAttribute("FeeSpecificationList");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	String feeSpecId = ServletUtil.getStringParam("inpId", request);
	String thisId = "";

	String pc = ServletUtil.getStringParam("selectProduct", request);

	FeeSpecificationVO fsVO = null;
	if (feeSpecId == null || feeSpecId.length() < 1) {
	    fsVO = new FeeSpecificationVO();
	} else {
	    // find the corresponding FeeSpecificationVO from the List
	    Iterator fsIt = feeSpecList.iterator();
	    while (fsIt.hasNext() && !(thisId.equalsIgnoreCase(feeSpecId))) {
		fsVO = ((FeeSpecificationVO) fsIt.next());
		thisId = fsVO.getFeeSpecificationID().toString();
	    }
	}

	fsVO.updateFromRequest(request);

	StringBuffer displayText = new StringBuffer();
	String origCode = feeSpecId;

	if (StringUtil.isNull(origCode)) {
	    try {
		membershipMgr.createFeeSpecification(fsVO);
		displayText.append("Created successfully");
		request.setAttribute("Refresh", "Yes");
	    } catch (Exception e) {
		displayText.append(e.getMessage());
	    }
	} else {
	    try {
		membershipMgr.updateFeeSpecification(fsVO);
		displayText.append("Saved successfully");
		request.setAttribute("Refresh", "No");
	    } catch (Exception e) {
		displayText.append(e.getMessage());
	    }
	}

	// Adjust the from and to dates
	adjustToDateForFeeSpecs(membershipMgr, fsVO);

	try {
	    feeSpecList = membershipMgr.getFeeSpecifications(fsVO.getProductCode(), fsVO.getProductBenefitCode(), fsVO.getMembershipTypeCode(), fsVO.getTransactionTypeCode(), fsVO.getFeeTypeCode(), fsVO.getGroupNumber());
	} catch (Exception e) {
	    feeSpecList = null;
	    displayText.append(e.getMessage());
	}

	gotoFeeSpecificationPage(request, response, feeSpecId, feeSpecList, displayText.toString());
    }

    public void handleEvent_maintainFeeSpecification_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {

	Integer feeSpecId = ServletUtil.getIntegerParam("inpId", request);
	StringBuffer displayText = new StringBuffer();
	Collection feeSpecList = (Collection) request.getAttribute("FeeSpecificationList");

	FeeSpecificationVO fsVO = null;

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	fsVO = membershipMgr.getFeeSpecification(feeSpecId);

	if (fsVO != null) {
	    membershipMgr.deleteFeeSpecification(fsVO);
	    displayText.append("Fee specification deleted successfully");
	    request.setAttribute("Refresh", "Yes");
	} else {
	    displayText.append("Can't find Fee Specification. " + feeSpecId.toString());
	}

	adjustToDateForFeeSpecs(membershipMgr, fsVO);

	if (!(fsVO == null)) {
	    feeSpecList = membershipMgr.getFeeSpecifications(fsVO.getProductCode(), fsVO.getProductBenefitCode(), fsVO.getMembershipTypeCode(), fsVO.getTransactionTypeCode(), fsVO.getFeeTypeCode(), fsVO.getGroupNumber());
	}

	// Now return to the Maintain FeeSpecification List page
	gotoFeeSpecificationPage(request, response, "", feeSpecList, displayText.toString());
    }

    // TODO review this method
    private void adjustToDateForFeeSpecs(MembershipMgr membershipMgr, FeeSpecificationVO fsVO) {

	// assume that list is in reverse sort order
	try {
	    Collection feeSpecList = membershipMgr.getFeeSpecifications(fsVO.getProductCode(), fsVO.getProductBenefitCode(), fsVO.getMembershipTypeCode(), fsVO.getTransactionTypeCode(), fsVO.getFeeTypeCode(), fsVO.getGroupNumber());
	    Iterator<FeeSpecificationVO> fsIt = feeSpecList.iterator();

	    FeeSpecificationVO fs = null;
	    // TODO why this date?
	    DateTime toDate = new DateTime("31/12/2050");

	    while (fsIt.hasNext()) {
		fs = fsIt.next();

		fs.setEffectiveToDate(toDate);

		toDate = fs.getEffectiveFromDate();

		if (toDate != null) {
		    toDate = toDate.subtract(new Interval(0, 0, 1, 0, 0, 0, 0));
		} else {
		    toDate = new DateTime("02/01/2001");
		    fs.setEffectiveFromDate(new DateTime("01/01/2001"));
		}

		// Write this record back to database?

		membershipMgr.updateFeeSpecification(fs);
	    }

	} catch (Exception ex) {
	    // TODO throw?
	    ex.printStackTrace();
	}

    }

    /* ============================DISCOUNT================== */

    /**
     * Handle the Maintain Discount event Forward the request to the Maintain
     * Discount List page to start the maintenance process
     */
    public void handleEvent_selectMaintainDiscounts(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	Collection discountList = null;
	try {
	    discountList = MembershipEJBHelper.getMembershipMgr().getDiscountTypes();
	} catch (Exception e) {
	    throw new RemoteException(e.getMessage());
	}

	SortedSet s = new TreeSet();
	int maxLen = 0;

	if (discountList != null) {
	    Iterator it = discountList.iterator();
	    while (it.hasNext()) {
		DiscountTypeVO discount = (DiscountTypeVO) it.next();
		if (maxLen < discount.getDiscountTypeCode().length()) {
		    maxLen = discount.getDiscountTypeCode().length();
		}
		s.add(new AdminCodeDescription(discount.getDiscountTypeCode(), discount.getDiscountTypeCode()));
	    }
	}
	//
	// Add some things to the request;
	//
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
	request.getSession().setAttribute("pageTitle", "Discounts");
	request.getSession().setAttribute("buttonMessage", "Add a new discount.");
	request.getSession().setAttribute("createEvent", "maintainDiscountList_createDiscount");
	request.getSession().setAttribute("selectEvent", "maintainDiscountList_selectDiscount");
	request.getSession().setAttribute("codeName", "discountCode");
	request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MEMBERSHIP_DISCOUNT_ADMIN_UIC);
	request.getSession().setAttribute("origCode", "");

	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
    }

    /**
     * Handle the Maintain Discount List select Product event. Forward the
     * request straight to the Maintain Discount page for existing
     */
    public void handleEvent_maintainDiscountList_selectDiscount(HttpServletRequest request, HttpServletResponse response) throws Exception {
	String discountTypeCode = request.getParameter("discountCode");
	DiscountTypeVO discountTypeVO = null;
	try {
	    discountTypeVO = MembershipEJBHelper.getMembershipMgr().getDiscountType(discountTypeCode);
	} catch (RemoteException oe) {
	    throw new SystemException("Unable to get discountType for discountType code " + discountTypeCode, oe);
	}

	gotoDiscountPage(request, response, discountTypeCode, discountTypeVO, " ");
    }

    /**
     * Handle the Maintain Discount List create Product event. Forward the
     * request straight to the Maintain Discount page for existing
     */
    public void handleEvent_maintainDiscountList_createDiscount(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("Refresh", "No");
	gotoDiscountPage(request, response, "", new DiscountTypeVO(), " ");
    }

    /**
     * Handle the save button event on the Maintain Discount page. Process the
     * transaction and save it to the database. Show the transaction complete
     * page.
     */
    public void handleEvent_maintainDiscount_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {

	DiscountTypeVO discountTypeVO = (DiscountTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_DISCOUNT_VO);
	MembershipUIHelper.updateFromRequest(request, discountTypeVO);

	String discountTypeCode = discountTypeVO.getDiscountTypeCode();

	// validate
	if ((!discountTypeVO.isOffsetDiscount() && !discountTypeVO.isMembershipValueReducing()) || (discountTypeVO.isOffsetDiscount() && discountTypeVO.isMembershipValueReducing())) {
	    throw new ValidationException("A discount must either be associated with an offset account or reduce the value of the membership.");
	}

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	StringBuffer displayText = new StringBuffer();
	String origCode = (String) request.getSession().getAttribute("origCode");

	if (StringUtil.isNull(discountTypeCode)) {
	    displayText.append("Code is SPACES. No Action taken.");
	} else {

	    if (StringUtil.isNull(origCode)) {
		try {
		    membershipMgr.createDiscountType(discountTypeVO);
		    displayText.append("Created successfully");
		    request.setAttribute("Refresh", "Yes");
		    this.addToAdminList(request, discountTypeCode, discountTypeVO.getDiscountTypeCode());
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    membershipMgr.updateDiscountType(discountTypeVO);
		    displayText.append("Saved successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    }
	}
	/*
	 * If percentageOf is entered, it must point to a valid fee type
	 */
	String percentOf = request.getParameter("percentageOf");
	if (percentOf != null && !percentOf.equals("") && membershipMgr.getFeeType(percentOf) == null) {
	    throw new ValidationException("Must be a percentage of a valid fee type");
	}
	String discountPercent = request.getParameter("discountPercentage");
	if (percentOf != null && !percentOf.equals("") && (discountPercent == null || discountPercent.equals(""))) {
	    throw new ValidationException("Percent Of field must be empty if no percentage is specified");
	}

	gotoDiscountPage(request, response, discountTypeCode, discountTypeVO, displayText.toString());
    }

    /**
     * Handle the delete button event on the Maintain Discount page. Process the
     * transaction and delete it from the database. Show the transaction
     * complete page.
     */
    public void handleEvent_maintainDiscount_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	DiscountTypeVO discountTypeVO = (DiscountTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_DISCOUNT_VO);
	MembershipUIHelper.updateFromRequest(request, discountTypeVO);

	StringBuffer displayText = new StringBuffer();

	// DiscountTypeHome discountHome =
	// MembershipEJBHelper.getDiscountTypeHome();
	// DiscountType discount = discountHome.findByPrimaryKey(discountCode);
	try {
	    MembershipEJBHelper.getMembershipMgr().removeDiscountType(discountTypeVO);
	    displayText.append("Discount deleted successfully");
	    this.removeFromAdminList(request, discountTypeVO.getDiscountTypeCode(), discountTypeVO.getDiscountTypeCode());
	    request.setAttribute("Refresh", "Yes");
	} catch (Exception r) {
	    displayText.append(r.getMessage());
	}

	// Now return to the Maintain Discount List page
	gotoDiscountPage(request, response, "", new DiscountTypeVO(), displayText.toString());

    }

    /**
   *
   */
    public void handleEvent_maintainDiscountFeeTypes_btnAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	DiscountTypeVO discountVO = (DiscountTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_DISCOUNT_VO);
	String thisCode = (String) request.getSession().getAttribute("origCode");
	String feeTypeCode = request.getParameter("listCode");

	MembershipUIHelper.updateFromRequest(request, discountVO);
	Collection myList = discountVO.getFeeTypeList();
	FeeTypeVO ft = MembershipEJBHelper.getMembershipRefMgr().getFeeType(feeTypeCode);

	myList.add(ft);

	discountVO.setFeeTypeList(myList);

	request.setAttribute("Refresh", "No");
	gotoDiscountPage(request, response, thisCode, discountVO, "");

    }

    public void handleEvent_maintainDiscountFeeTypes_btnRemove(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

	DiscountTypeVO discountVO = (DiscountTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_DISCOUNT_VO);
	String feeTypeCode = request.getParameter("listCode");
	String thisCode = (String) request.getSession().getAttribute("origCode");

	MembershipUIHelper.updateFromRequest(request, discountVO);

	FeeTypeVO ft = MembershipEJBHelper.getMembershipRefMgr().getFeeType(feeTypeCode);

	Collection myList = discountVO.getFeeTypeList();

	myList.remove(ft);

	discountVO.setFeeTypeList(myList);

	request.setAttribute("Refresh", "No");
	gotoDiscountPage(request, response, thisCode, discountVO, "");

    }

    /* =====================MAIN MENU==================== */

    public void handleEvent_mainMenu(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	String menuItem = request.getParameter("menuItem");
	if ("resetAllCaches".equals(menuItem)) {
	    resetAllCaches(request, response);
	} else {
	    throw new ServletException("Invalid menu item name '" + menuItem + "'");
	}
    }

    /* ============================PRODUCTS================== */
    /**
     * Handle the Maintain Product List select Product event. Forward the
     * request straight to the Maintain Product page for existing
     */
    public void handleEvent_maintainProductList_selectProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
	String productCode = request.getParameter("productCode");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	ProductVO prodVO = membershipMgr.getProduct(productCode);
	gotoProductPage(request, response, productCode, prodVO, "");

    }

    /**
     * Handle the Maintain Product List create Product event. Forward the
     * request straight to the Maintain Product page for existing
     */
    public void handleEvent_maintainProductList_createProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("Refresh", "No");
	gotoProductPage(request, response, "", new ProductVO(), "");
    }

    /**
     * Handle the Maintain Products event Forward the request to the Maintain
     * Product List page to start the maintenance process
     */
    public void handleEvent_selectMaintainProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection prodList = null;
	SortedSet s = new TreeSet();
	int maxLen = 0;

	prodList = membershipMgr.getProducts();

	if (prodList != null) {
	    Iterator it = prodList.iterator();
	    while (it.hasNext()) {
		ProductVO product = (ProductVO) it.next();
		if (maxLen < product.getProductCode().length()) {
		    maxLen = product.getProductCode().length();
		}
		s.add(new AdminCodeDescription(product.getProductCode(), product.getProductCode()));
	    }
	}
	//
	// Add somethings to the request;
	//
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
	request.getSession().setAttribute("pageTitle", "Products");
	request.getSession().setAttribute("buttonMessage", "Add a new Product.");
	request.getSession().setAttribute("createEvent", "maintainProductList_createProduct");
	request.getSession().setAttribute("selectEvent", "maintainProductList_selectProduct");
	request.getSession().setAttribute("codeName", "productCode");
	request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MembershipAdminUIC);
	request.getSession().setAttribute("origCode", "");

	request.setAttribute("Refresh", "No");
	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
    }

    /**
     * Handle the Maintain Product Add a benefit event. Forward the request back
     * to Maintain Product
     */
    public void handleEvent_maintainProduct_Benefit_btnAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
	ProductVO productVO = (ProductVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_PRODUCT_VO);
	String thisCode = request.getParameter("listCode");
	String origCode = (String) request.getSession().getAttribute("origCode");

	productVO.updateFromRequest(request);

	Collection thisList = productVO.getProductBenefitList();
	thisList.add(MembershipEJBHelper.getMembershipRefMgr().getProductBenefitType(thisCode));
	productVO.setProductBenefitList(thisList);

	request.setAttribute("Refresh", "No");
	gotoProductPage(request, response, origCode, productVO, "");
    }

    /**
     * Handle the Maintain Product Remove a benefit event. Forward the request
     * back to Maintain Product
     */
    public void handleEvent_maintainProduct_Benefit_btnRemove(HttpServletRequest request, HttpServletResponse response) throws Exception {
	ProductVO productVO = (ProductVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_PRODUCT_VO);
	String thisCode = request.getParameter("listCode");
	String origCode = (String) request.getSession().getAttribute("origCode");

	productVO.updateFromRequest(request);

	Collection thisList = productVO.getProductBenefitList();
	thisList.remove(MembershipEJBHelper.getMembershipRefMgr().getProductBenefitType(thisCode));
	productVO.setProductBenefitList(thisList);

	request.setAttribute("Refresh", "No");
	gotoProductPage(request, response, origCode, productVO, "");

    }

    /**
     * Handle the save button event on the Maintain Product page. Process the
     * transaction and save it to the database. Show the transaction complete
     * page.
     */
    public void handleEvent_maintainProduct_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
	// get the original code so we know whether new or existing
	String origCode = (String) request.getSession().getAttribute("origCode");

	// Get the ProductVO from the session and get the current list of
	// Benefits
	ProductVO productVO = (ProductVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_PRODUCT_VO);

	productVO.updateFromRequest(request);

	String productCode = productVO.getProductCode();

	StringBuffer displayText = new StringBuffer();

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	if (StringUtil.isBlank(productCode)) {
	    displayText.append("Code is SPACES. No Action taken.");
	} else {

	    if (StringUtil.isBlank(origCode)) {
		try {
		    membershipMgr.createProduct(productVO);
		    displayText.append("Created successfully");
		    request.setAttribute("Refresh", "Yes");
		    addToAdminList(request, productVO.getProductCode(), productVO.getProductCode());

		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    membershipMgr.updateProduct(productVO);
		    displayText.append("Saved successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    }
	}

	/** @todo not sure about this? */
	if (productVO.isDefault()) {
	    Collection prodList = membershipMgr.getProducts();
	    Iterator it = prodList.iterator();
	    while (it.hasNext()) {
		ProductVO tmpProduct = (ProductVO) it.next();
		// The 'if' is not strictly necessary as the setDefault is done
		// later anyway
		if (!tmpProduct.getProductCode().equals(origCode)) {
		    tmpProduct.setDefault(false);
		    membershipMgr.updateProduct(tmpProduct);
		}
	    }
	}

	gotoProductPage(request, response, productCode, productVO, displayText.toString());
    }

    /**
     * Handle the delete button event on the Maintain Product page. Process the
     * transaction and delete it from the database. Show the transaction
     * complete page.
     */
    public void handleEvent_maintainProduct_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	// Get the ProductVO from the session and get the current list of
	// Benefits
	ProductVO productVO = (ProductVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_PRODUCT_VO);
	productVO.updateFromRequest(request);
	String productCode = productVO.getProductCode();

	// Now return to the Maintain Product List page
	request.setAttribute("Refresh", "Yes");
	gotoProductPage(request, response, productCode, productVO, "Products can not be deleted - mark as Inactive.");
    }

    /* ==========================Product Benefits====================== */

    /**
     * Handle the Maintain Product Benefits event Forward the request to the
     * Maintain Product Benefits List page to start the maintenance process
     */
    public void handleEvent_selectMaintainProductBenefit(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection productBenefitTypeList = membershipMgr.getProductBenefitTypeList();
	SortedSet s = new TreeSet();
	int maxLen = 0;

	if (productBenefitTypeList != null) {
	    Iterator it = productBenefitTypeList.iterator();
	    while (it.hasNext()) {
		ProductBenefitTypeVO benefitType = (ProductBenefitTypeVO) it.next();
		if (maxLen < benefitType.getProductBenefitCode().length()) {
		    maxLen = benefitType.getProductBenefitCode().length();
		}
		s.add(new AdminCodeDescription(benefitType.getProductBenefitCode(), benefitType.getProductBenefitCode()));
	    }
	}
	//
	// Add somethings to the request;
	//
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
	request.getSession().setAttribute("pageTitle", "Product Benefit Types");
	request.getSession().setAttribute("buttonMessage", "Add a new Product Benefit Type.");
	request.getSession().setAttribute("createEvent", "maintainProductBenefitList_createBenefit");
	request.getSession().setAttribute("selectEvent", "maintainProductBenefitList_selectBenefit");
	request.getSession().setAttribute("codeName", "productBenefitCode");
	request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MembershipAdminUIC);
	request.getSession().setAttribute("origCode", "");

	request.setAttribute("Refresh", "No");
	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
    }

    /**
     * Handle the Maintain ProductBenefit List select Benefit event. Forward the
     * request straight to the Maintain ProductBenefit page for existing
     */
    public void handleEvent_maintainProductBenefitList_selectBenefit(HttpServletRequest request, HttpServletResponse response) throws Exception {
	String productBenefitCode = request.getParameter("productBenefitCode");
	ProductBenefitTypeVO productBenefitTypeVO = MembershipEJBHelper.getMembershipMgr().getProductBenefitType(productBenefitCode);
	gotoProductBenefitPage(request, response, productBenefitCode, productBenefitTypeVO, "");

    }

    /**
     * Handle the Maintain ProductBenefit List create Product event. Forward the
     * request straight to the Maintain ProductBenefit page for existing
     */
    public void handleEvent_maintainProductBenefitList_createBenefit(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("Refresh", "No");
	gotoProductBenefitPage(request, response, "", new ProductBenefitTypeVO(), "");
    }

    /**
     * Handle the save button event on the Maintain ProductBenefit page. Process
     * the transaction and save it to the database. Show the transaction
     * complete page.
     */
    public void handleEvent_maintainProductBenefit_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {

	ProductBenefitTypeVO productBenefitType = (ProductBenefitTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_PRODUCT_BENEFIT_VO);
	// TODO externalise this translation
	productBenefitType.updateFromRequest(request);

	String productBenefitCode = productBenefitType.getProductBenefitCode();

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	StringBuffer displayText = new StringBuffer();
	String origCode = (String) request.getSession().getAttribute("origCode");

	if (StringUtil.isNull(productBenefitCode)) {
	    displayText.append("Code is SPACES. No Action taken.");
	} else {

	    if (StringUtil.isNull(origCode)) {
		try {
		    membershipMgr.createProductBenefitType(productBenefitType);
		    addToAdminList(request, productBenefitType.getProductBenefitCode(), productBenefitType.getProductBenefitCode());
		    request.setAttribute("Refresh", "Yes");
		    displayText.append("Created successfully");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    membershipMgr.updateProductBenefitType(productBenefitType);
		    displayText.append("Saved successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    }
	}

	gotoProductBenefitPage(request, response, productBenefitCode, productBenefitType, displayText.toString());

    }

    /**
     * Handle the delete button event on the Maintain ProductBenefit page.
     * Process the transaction and delete it from the database. Show the
     * transaction complete page.
     */
    public void handleEvent_maintainProductBenefit_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	ProductBenefitTypeVO productBenefitType = (ProductBenefitTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_PRODUCT_BENEFIT_VO);
	productBenefitType.updateFromRequest(request);

	String productBenefitCode = productBenefitType.getProductBenefitCode();
	StringBuffer displayText = new StringBuffer();

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	membershipMgr.deleteProductBenefitType(productBenefitType);
	displayText.append("Product benefit deleted successfully");

	// Success - clear the VO and code

	productBenefitType = new ProductBenefitTypeVO();
	productBenefitCode = "";

	// Now return to the Maintain ProductBenefit List page

	gotoProductBenefitPage(request, response, productBenefitCode, productBenefitType, displayText.toString());

    }

    /* ================================Accounts========================== */

    /**
     * Handle the Maintain Membership Account event Forward the request to the
     * Maintain Membership Account List page to start the maintenance process
     */
    public void handleEvent_selectMaintainMembershipAccount(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {

	//
	// Add somethings to the request;
	//
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, getAllAccounts());
	request.getSession().setAttribute("pageTitle", "Membership Accounts");
	request.getSession().setAttribute("buttonMessage", "Add a new Account.");
	request.getSession().setAttribute("createEvent", "maintainAccountList_createAccount");
	request.getSession().setAttribute("selectEvent", "maintainAccountList_selectAccount");
	request.getSession().setAttribute("codeName", "accountId");
	request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MembershipAdminUIC);
	request.getSession().setAttribute("origCode", "");

	request.setAttribute("Refresh", "No");
	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
    }

    private SortedSet getAllAccounts() throws RemoteException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	SortedSet s = new TreeSet();
	int maxLen = 0;

	Collection<MembershipAccountVO> accountList = membershipMgr.getMembershipAccounts();

	if (accountList != null) {
	    Iterator<MembershipAccountVO> it = accountList.iterator();
	    while (it.hasNext()) {
		MembershipAccountVO thisAccount = it.next();
		String acctId = thisAccount.getAccountID().toString();
		if (maxLen < acctId.length()) {
		    maxLen = acctId.length();
		}
		s.add(new AdminCodeDescription(acctId, thisAccount.getAccountTitle(), thisAccount.getAccountTitle()));
	    }
	}

	return s;
    }

    /**
     * Handle the Maintain Account List create Account event. Forward the
     * request straight to the Maintain Account page for new
     */
    public void handleEvent_maintainAccountList_createAccount(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("Refresh", "No");
	gotoAccountPage(request, response, "", new MembershipAccountVO(), " ");
    }

    /**
     * Handle the Maintain Account List select Account event. Forward the
     * request straight to the Maintain Account page for existing
     */
    public void handleEvent_maintainAccountList_selectAccount(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	Integer accountId = new Integer(request.getParameter("accountId"));
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	MembershipAccountVO accountVO = null;
	accountVO = membershipMgr.getMembershipAccount(accountId);
	request.setAttribute("Refresh", "No");
	gotoAccountPage(request, response, accountId.toString(), accountVO, " ");
    }

    /**
     * Handle the save button event on the Maintain Account page. Process the
     * transaction and save it to the database. Show the transaction complete
     * page.
     * 
     * @todo needs to include DD clearing account
     */
    public void handleEvent_maintainAccount_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
	MembershipAccountVO accountVO = (MembershipAccountVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_ACCOUNT_VO);
	accountVO.updateFromRequest(request);

	String accountId = accountVO.getAccountID().toString();

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	String newIsTaxable = request.getParameter("isTaxable");

	boolean taxable = false;
	if ("true".equals(newIsTaxable)) {
	    taxable = true;
	}

	StringBuffer displayText = new StringBuffer();
	String origCode = (String) request.getSession().getAttribute("origCode");

	if (StringUtil.isNull(accountId)) {
	    displayText.append("Code is SPACES. No Action taken."); // what does
								    // this
								    // mean?!!
	} else {

	    if (StringUtil.isNull(origCode)) {
		try {
		    accountVO = membershipMgr.createMembershipAccount(accountVO);
		    displayText.append("Created successfully");
		    request.setAttribute("Refresh", "Yes");
		    request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, getAllAccounts());
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    membershipMgr.updateMembershipAccount(accountVO);
		    displayText.append("Saved successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    }
	}

	gotoAccountPage(request, response, accountId, accountVO, displayText.toString());
    }

    public void handleEvent_btnRemoveRenewalBatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	Integer runNumber = new Integer(request.getParameter("runNumber"));

	RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	Hashtable documentCounts = renewalMgr.removeRenewalBatch(runNumber);

	Integer totalDocumentsRemoved = (Integer) documentCounts.get(RenewalMgrBean.COUNT_TOTAL_REMOVED_DOCUMENTS);
	Integer totalDocuments = (Integer) documentCounts.get(RenewalMgrBean.COUNT_TOTAL_DOCUMENTS);
	Integer totalBatchesRemovedCount = (Integer) documentCounts.get(RenewalMgrBean.COUNT_TOTAL_BATCHES_REMOVED);

	request.setAttribute("heading", "Renewal Batch Removed");
	String message = "";
	message += (totalBatchesRemovedCount.intValue() > 0 ? "Renewal batch run number " + runNumber + " has been removed." : "Renewal batch run number " + runNumber + " has not been removed as documents still exist.");
	message += " " + totalDocumentsRemoved + " document(s) removed of " + totalDocuments + " total document(s).";
	request.setAttribute("message", message);

	this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

    }

    public void handleEvent_btnClearEmailSentFlag(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	Integer runNumber = new Integer(request.getParameter("runNumber"));

	RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();
	int emailFlagCleared = renewalMgr.clearEmailSentFlag(runNumber);

	request.setAttribute("heading", "Email Flags Cleared");
	String message = "";
	message += emailFlagCleared + " document(s) have had the email date cleared.";
	request.setAttribute("message", message);

	this.forwardRequest(request, response, CommonUIConstants.PAGE_ConfirmProcess);

    }

    /**
     * Handle the delete button event on the Maintain Account page. Process the
     * transaction and delete it from the database. Show the transaction
     * complete page.
     */
    public void handleEvent_maintainAccount_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	MembershipAccountVO accountVO = (MembershipAccountVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_ACCOUNT_VO);
	accountVO.updateFromRequest(request);
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Integer accountCode = accountVO.getAccountID();
	StringBuffer displayText = new StringBuffer();

	try {
	    membershipMgr.deleteMembershipAccount(accountVO);
	    displayText.append("Membership account deleted successfully");
	    this.removeFromAdminList(request, accountCode.toString(), accountVO.getAccountTitle());
	    request.setAttribute("Refresh", "Yes");
	} catch (Exception r) {
	    displayText.append(r.getMessage());
	}

	// Now return to the Maintain Discount List page
	gotoAccountPage(request, response, "", new MembershipAccountVO(), displayText.toString());
    }

    /************************* Membership Fee Types ******************************/
    /**
     * Handle the Maintain Membership Fee Type event Forward the request to the
     * Maintain Membership Fee Type List page to start the maintenance process
     */
    public void handleEvent_selectMaintainMembershipFeeType(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection<FeeTypeVO> feeTypeList = membershipMgr.getFeeTypes();
	SortedSet s = new TreeSet();
	int maxLen = 0;

	if (feeTypeList != null) {
	    Iterator<FeeTypeVO> it = feeTypeList.iterator();
	    while (it.hasNext()) {
		FeeTypeVO feeType = it.next();
		if (maxLen < feeType.getFeeTypeCode().length()) {
		    maxLen = feeType.getFeeTypeCode().length();
		}
		s.add(new AdminCodeDescription(feeType.getFeeTypeCode(), feeType.getFeeTypeCode()));
	    }
	}
	//
	// Add some things to the request;
	//
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
	request.getSession().setAttribute("pageTitle", "Fee Types");
	request.getSession().setAttribute("buttonMessage", "Add a new FeeType.");
	request.getSession().setAttribute("createEvent", "maintainFeeTypeList_createFeeType");
	request.getSession().setAttribute("selectEvent", "maintainFeeTypeList_selectFeeType");
	request.getSession().setAttribute("codeName", "feeTypeCode");
	request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MembershipAdminUIC);
	request.getSession().setAttribute("origCode", "");

	request.setAttribute("Refresh", "No");
	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);

	// forwardRequest(request,response,MembershipUIConstants.PAGE_MaintainMembershipFeeType);
    }

    /**
     * Handle the Maintain Fee Type List create Fee Type event. Forward the
     * request straight to the Maintain Fee Type page for new
     */
    public void handleEvent_maintainFeeTypeList_createFeeType(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	request.setAttribute("Refresh", "No");
	gotoFeeTypePage(request, response, "", new FeeTypeVO(), " ");
    }

    /**
     * Handle the Maintain Fee Type List select Fee Type event. Forward the
     * request straight to the Maintain Fee Type page for existing
     */
    public void handleEvent_maintainFeeTypeList_selectFeeType(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	String feeTypeCode = request.getParameter("feeTypeCode");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	FeeTypeVO feeTypeVO = membershipMgr.getFeeType(feeTypeCode);
	LogUtil.log(this.getClass(), "feeTypeVO=" + feeTypeVO);
	request.setAttribute("Refresh", "No");
	gotoFeeTypePage(request, response, feeTypeCode, feeTypeVO, " ");
    }

    /**
     * Handle the save button event on the Maintain Fee page. Process the
     * transaction and save it to the database. Show the transaction complete
     * page.
     */
    public void handleEvent_maintainFeeType_btnSave(HttpServletRequest request, HttpServletResponse response) throws Exception {

	FeeTypeVO feeTypeVO = (FeeTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_FEE_TYPE_VO);

	feeTypeVO.updateFromRequest(request);

	String feeTypeCode = feeTypeVO.getFeeTypeCode();

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	StringBuffer displayText = new StringBuffer();
	String origCode = (String) request.getSession().getAttribute("origCode");

	if (StringUtil.isNull(feeTypeCode)) {
	    displayText.append("Code is SPACES. No Action taken.");
	} else {

	    if (StringUtil.isNull(origCode)) {
		try {
		    membershipMgr.createFeeType(feeTypeVO);
		    displayText.append("Created successfully");
		    this.addToAdminList(request, feeTypeVO.getFeeTypeCode(), feeTypeVO.getFeeTypeCode());
		    request.setAttribute("Refresh", "Yes");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    membershipMgr.updateFeeType(feeTypeVO);
		    displayText.append("Saved successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    }
	}

	gotoFeeTypePage(request, response, feeTypeCode, feeTypeVO, displayText.toString());
    }

    /**
     * Handle the delete button event on the Maintain FeeType page. Process the
     * transaction and delete it from the database. Show the transaction
     * complete page.
     */
    public void handleEvent_maintainFeeType_btnDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	FeeTypeVO feeType = (FeeTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_FEE_TYPE_VO);
	feeType.updateFromRequest(request);

	String feeTypeCode = feeType.getFeeTypeCode();
	StringBuffer displayText = new StringBuffer();
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	membershipMgr.deleteFeeType(feeType);
	this.removeFromAdminList(request, feeTypeCode, feeTypeCode);
	displayText.append("FeeType Type Data Deleted successfully");

	request.setAttribute("Refresh", "Yes");
	gotoFeeTypePage(request, response, "", new FeeTypeVO(), displayText.toString());
    }

    /** 
   *
   */
    public void handleEvent_maintainFeeTypeDiscount_btnAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	FeeTypeVO feeTypeVO = (FeeTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_FEE_TYPE_VO);
	String discountCode = request.getParameter("listCode");

	feeTypeVO.updateFromRequest(request);

	feeTypeVO.setAllowableDiscountList(addDiscountToList(feeTypeVO.getAllowableDiscountList(), discountCode));
	request.setAttribute("Refresh", "No");
	gotoFeeTypePage(request, response, request.getParameter("origCode"), feeTypeVO, "");

    }

    public void handleEvent_maintainFeeTypeDiscount_btnRemove(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

	FeeTypeVO feeTypeVO = (FeeTypeVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_FEE_TYPE_VO);
	String removeDiscountCode = request.getParameter("listCode");

	feeTypeVO.updateFromRequest(request);

	DiscountTypeVO dt = MembershipEJBHelper.getMembershipRefMgr().getDiscountType(removeDiscountCode);

	Collection myList = feeTypeVO.getAllowableDiscountList();

	myList.remove(dt);

	feeTypeVO.setAllowableDiscountList(myList);

	request.setAttribute("Refresh", "No");
	gotoFeeTypePage(request, response, request.getParameter("origCode"), feeTypeVO, "");

    }

    /************************* Membership Profiles ******************************/
    /**
     * Handle the Maintain Membership Profile event Forward the request to the
     * Maintain Membership Profile List page
     */
    public void handleEvent_selectMaintainMembershipProfiles(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection profileList = null;
	SortedSet s = new TreeSet();
	int maxLen = 0;

	profileList = membershipMgr.getMembershipProfiles();
	if (profileList != null) {
	    Iterator<MembershipProfileVO> it = profileList.iterator();
	    while (it.hasNext()) {
		MembershipProfileVO profile = it.next();
		if (maxLen < profile.getProfileCode().length()) {
		    maxLen = profile.getProfileCode().length();
		}
		s.add(new AdminCodeDescription(profile.getProfileCode(), profile.getProfileCode()));
	    }
	}
	//
	// Add some things to the request;
	//
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
	request.getSession().setAttribute("pageTitle", "Membership Profile");
	request.getSession().setAttribute("buttonMessage", "Add a new MembershipProfile.");
	request.getSession().setAttribute("createEvent", "maintainProfileList_createProfile");
	request.getSession().setAttribute("selectEvent", "maintainProfileList_selectProfile");
	request.getSession().setAttribute("codeName", "profileCode");
	request.getSession().setAttribute("UICPage", MembershipUIConstants.PAGE_MembershipAdminUIC);
	request.getSession().setAttribute("origCode", "");

	request.setAttribute("Refresh", "No");
	forwardRequest(request, response, CommonUIConstants.PAGE_MAINTAIN_ADMIN_LIST);
    }

    /**
   *
   */
    public void handleEvent_maintainProfileList_createProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	request.setAttribute("Refresh", "No");
	gotoProfilePage(request, response, "", new MembershipProfileVO(), " ");
    }

    /**
   *
   */
    public void handleEvent_maintainProfileList_selectProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

	String profileCode = request.getParameter("profileCode");
	request.setAttribute("statusMessage", " ");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	MembershipProfileVO profile = null;
	try {
	    profile = membershipMgr.getMembershipProfile(profileCode);
	} catch (Exception oe) {
	    throw new SystemException("Unable to get profile for profile code " + profileCode, oe);
	}
	request.setAttribute("refresh", "No");
	gotoProfilePage(request, response, profileCode, profile, "");

    }

    public void handleEvent_maintainProfile_btnSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

	MembershipProfileVO profile = (MembershipProfileVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_MEMBERSHIP_PROFILE_VO);

	profile.updateFromRequest(request);

	String profileCode = profile.getProfileCode();

	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	StringBuffer displayText = new StringBuffer();
	String origCode = (String) request.getSession().getAttribute("origCode");

	if (StringUtil.isNull(profileCode)) {
	    displayText.append("Code is SPACES. No Action taken.");
	} else {

	    if (StringUtil.isNull(origCode)) {
		try {
		    membershipMgr.createMembershipProfile(profile);
		    displayText.append("Created successfully");
		    this.addToAdminList(request, profile.getProfileCode(), profile.getProfileCode());
		    request.setAttribute("Refresh", "Yes");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    } else {
		try {
		    membershipMgr.updateMembershipProfile(profile);
		    displayText.append("Saved successfully");
		    request.setAttribute("Refresh", "No");
		} catch (Exception e) {
		    displayText.append(e.getMessage());
		}
	    }
	}

	gotoProfilePage(request, response, profileCode, profile, displayText.toString());
    }

    public void handleEvent_maintainProfile_btnDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipProfileVO profile = (MembershipProfileVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_MEMBERSHIP_PROFILE_VO);

	StringBuffer displayText = new StringBuffer();
	request.setAttribute("Refresh", "No");
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

	try {
	    membershipMgr.deleteMembershipProfile(profile);
	    this.removeFromAdminList(request, profile.getProfileCode(), profile.getProfileCode());
	    request.setAttribute("Refresh", "Yes");
	} catch (Exception e) {
	    LogUtil.warn(this.getClass(), e);
	    displayText.append("Unable to delete. " + e.getMessage());
	}

	gotoProfilePage(request, response, profile.getProfileCode(), profile, displayText.toString());

    }

    public void handleEvent_maintainProfileDiscount_btnAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipProfileVO profileVO = (MembershipProfileVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_MEMBERSHIP_PROFILE_VO);
	String origCode = (String) request.getSession().getAttribute("origCode");

	String thisCode = request.getParameter("listCode");

	profileVO.updateFromRequest(request);
	Collection thisList = profileVO.getDiscountList();

	thisList.add(MembershipEJBHelper.getMembershipRefMgr().getDiscountType(thisCode));
	profileVO.setDiscountList(thisList);

	request.setAttribute("refresh", "No");
	gotoProfilePage(request, response, origCode, profileVO, "");

    }

    public void handleEvent_maintainProfileDiscount_btnRemove(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {

	MembershipProfileVO profileVO = (MembershipProfileVO) request.getSession().getAttribute(MembershipUIConstants.MAINTAIN_MEMBERSHIP_PROFILE_VO);
	String origCode = (String) request.getSession().getAttribute("origCode");

	String thisCode = request.getParameter("listCode");

	profileVO.updateFromRequest(request);

	Collection thisList = profileVO.getDiscountList();
	boolean a = thisList.remove(MembershipEJBHelper.getMembershipRefMgr().getDiscountType(thisCode));

	profileVO.setDiscountList(thisList);

	request.setAttribute("refresh", "No");
	gotoProfilePage(request, response, origCode, profileVO, "");

    }

    /************************** Reset Caches ************************************/

    /**
     * Reset all of the caches so that they are re-initialised from the database
     * the next time they are used. Caches maintained by the MembershipRefMgr
     * session bean and CommonMgr session bean are reset.
     */
    private void resetAllCaches(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	forwardRequest(request, response, CommonUIConstants.PAGE_ResetCaches);
    }

    public void handleEvent_Menu_extractToCARES(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	UserSession userSession = UserSession.getUserSession(request.getSession());
	if (userSession == null || !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_CAD_ADMIN)) {
	    request.setAttribute("exception", new com.ract.common.SecurityException("You do not have the privilege to administer the CARES extract."));
	    forwardRequest(request, response, "/error/SecurityError.jsp");
	}

	forwardRequest(request, response, MembershipUIConstants.PAGE_ExtractToCARES);
    }

    public void handleEvent_extractToCARES_btnCancel(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	CommonEJBHelper.getCadExportMgr().cancelMembershipRefresh();
	forwardRequest(request, response, MembershipUIConstants.PAGE_ExtractToCARES);
    }

    public void handleEvent_extractToCARES_btnExtract(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException, ValidationException {
	CommonEJBHelper.getCadExportMgr().refreshFromRoadside();
	forwardRequest(request, response, MembershipUIConstants.PAGE_ExtractToCARES);
    }

    public void handleEvent_extractToCARES_btnRefresh(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	forwardRequest(request, response, MembershipUIConstants.PAGE_ExtractToCARES);
    }

    public void handleEvent_Menu_RepairMembership(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	UserSession userSession = UserSession.getUserSession(request.getSession());
	if (userSession == null || !userSession.getUser().isPrivilegedUser(Privilege.PRIVILEGE_MEM_REPAIR)) {
	    request.setAttribute("exception", new com.ract.common.SecurityException("You do not have the privilege to repair membership data."));
	    forwardRequest(request, response, "/error/SecurityError.jsp");
	}

	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_RepairMembership_btnRepairCardJoinDate(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipEJBHelper.getMembershipRepairMgr().repairMembershipCardJoinDate();
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_RepairMembership_btnRepairTransferredStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	UserSession userSession = UserSession.getUserSession(request.getSession());
	MembershipEJBHelper.getMembershipRepairMgr().repairMembershipsWithTransferredStatus(userSession.getUserId());
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_RepairMembership_btnRepairDupTrans(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipEJBHelper.getMembershipRepairMgr().repairDuplicateTransactions();
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_RepairMembership_btnRepairAccessMembers(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipEJBHelper.getMembershipRepairMgr().repairAccessMemberMarketing();
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_RepairMembership_btnRepairTrans(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipEJBHelper.getMembershipRepairMgr().repairTransactionDetails();
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_RepairMembership_btnRepairCardName(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipEJBHelper.getMembershipRepairMgr().repairMembershipCardName();
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_RepairMembership_btnRepairOneMemberGroups(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipEJBHelper.getMembershipRepairMgr().repairOneMemberGroups();
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    public void handleEvent_Menu_InvalidQuotes(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	forwardRequest(request, response, MembershipUIConstants.PAGE_InvalidQuotes);
    }

    public void handleEvent_RepairMembership_btnConvertHistoryFiles(HttpServletRequest request, HttpServletResponse response) throws ServletException, RemoteException {
	MembershipRepairMgr repairMgr = MembershipEJBHelper.getMembershipRepairMgr();
	int convertCount = repairMgr.convertMembershipHistoryFiles();
	request.setAttribute("convertHistoryFileCount", new Integer(convertCount));
	forwardRequest(request, response, MembershipUIConstants.PAGE_RepairMembership);
    }

    /**************************** Utility methods *****************************/

    /**
     * Get a membership value object for the specified membership id.
     */
    private MembershipVO getMembershipVO(String membershipID) throws ServletException {
	MembershipVO memVO = null;
	if (membershipID != null && membershipID.length() > 0) {
	    try {
		// Membership membership = null;
		// MembershipHome membershipHome =
		// MembershipEJBHelper.getMembershipHome();
		// membership = membershipHome.findByPrimaryKey(new
		// Integer(membershipID));
		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
		memVO = membershipMgr.getMembership(new Integer(membershipID));
	    } catch (Exception e) {
		throw new ServletException("Failed to get membership with ID '" + membershipID + "' : " + e.getMessage(), e);
	    }
	} else {
	    throw new ServletException("Failed to get membership with ID '" + membershipID + "' : ID is not valid.");
	}
	return memVO;
    }

    /**
     * Add nominated discount to the Allowed Discount list
     */
    private Collection addDiscountToList(Collection discountList, String discountCode) throws RemoteException {

	MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	ArrayList availableDiscounts = (ArrayList) refMgr.getDiscountTypeList();
	DiscountTypeVO discountType = null;

	Collection myDiscountList = new ArrayList();
	if (!(discountList == null)) {
	    myDiscountList = discountList;
	}

	ListIterator it = availableDiscounts.listIterator();
	while (it.hasNext()) {
	    discountType = (DiscountTypeVO) it.next();
	    if (discountType.getDiscountTypeCode().equalsIgnoreCase(discountCode)) {
		myDiscountList.add(discountType);
	    }
	}

	return myDiscountList;

    }

    /**
     * Create a new MembershipProfileVO from the request
     */
    private MembershipProfileVO updateMembershipProfileVO(HttpServletRequest request, MembershipProfileVO inMPVO) throws RemoteException {

	MembershipProfileVO mpVO = new MembershipProfileVO();

	mpVO.setProfileCode(request.getParameter("ProfileCode"));
	mpVO.setDescription(request.getParameter("description"));
	mpVO.setProfileStatus(request.getParameter("profileStatus"));
	mpVO.setNameSuffix(request.getParameter("nameSuffix"));

	mpVO.setAdminOnly(Boolean.valueOf(request.getParameter("adminOnly")).booleanValue());

	mpVO.setDiscountList(inMPVO.getDiscountList());

	return mpVO;

    }

    /**************************** Utility methods *****************************/

    private void addToAdminList(HttpServletRequest request, String code, String desc) {
	// Add this to the list of Codes
	SortedSet s = (SortedSet) request.getSession().getAttribute(CommonUIConstants.MAINTAIN_CODELIST);
	s.add(new AdminCodeDescription(code, desc));
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
    }

    private void removeFromAdminList(HttpServletRequest request, String code, String desc) {
	// Add this to the list of Codes
	SortedSet s = (SortedSet) request.getSession().getAttribute(CommonUIConstants.MAINTAIN_CODELIST);
	s.remove(new AdminCodeDescription(code, desc));
	request.getSession().setAttribute(CommonUIConstants.MAINTAIN_CODELIST, s);
    }

    private void gotoPage(HttpServletRequest request, HttpServletResponse response, String oCode, Object object, String VOType, String statMsg, String page) throws ServletException {
	request.getSession().setAttribute("origCode", oCode);
	request.getSession().setAttribute(VOType, object);
	request.setAttribute("statusMessage", statMsg);
	forwardRequest(request, response, page);
    }

    private void gotoProductBenefitPage(HttpServletRequest request, HttpServletResponse response, String oCode, ValueObject VO, String statMsg) throws ServletException {
	gotoPage(request, response, oCode, VO, MembershipUIConstants.MAINTAIN_PRODUCT_BENEFIT_VO, statMsg, MembershipUIConstants.PAGE_MaintainProductBenefit);
    }

    private void gotoProductPage(HttpServletRequest request, HttpServletResponse response, String oCode, ProductVO VO, String statMsg) throws ServletException {

	gotoPage(request, response, oCode, VO, MembershipUIConstants.MAINTAIN_PRODUCT_VO, statMsg, MembershipUIConstants.PAGE_MaintainProduct);
    }

    private void gotoAccountPage(HttpServletRequest request, HttpServletResponse response, String oCode, MembershipAccountVO account, String statMsg) throws ServletException {

	gotoPage(request, response, oCode, account, MembershipUIConstants.MAINTAIN_ACCOUNT_VO, statMsg, MembershipUIConstants.PAGE_MaintainAccount);
    }

    private void gotoAffiliatedClubPage(HttpServletRequest request, HttpServletResponse response, String oCode, ValueObject VO, String statMsg) throws ServletException {

	gotoPage(request, response, oCode, VO, MembershipUIConstants.MAINTAIN_AFFILIATED_CLUB_VO, statMsg, MembershipUIConstants.PAGE_MaintainAffiliatedClub);
    }

    private void gotoFeeTypePage(HttpServletRequest request, HttpServletResponse response, String oCode, ValueObject VO, String statMsg) throws ServletException {

	gotoPage(request, response, oCode, VO, MembershipUIConstants.MAINTAIN_FEE_TYPE_VO, statMsg, MembershipUIConstants.PAGE_MaintainFeeType);
    }

    private void gotoProfilePage(HttpServletRequest request, HttpServletResponse response, String oCode, ValueObject VO, String statMsg) throws ServletException {

	gotoPage(request, response, oCode, VO, MembershipUIConstants.MAINTAIN_MEMBERSHIP_PROFILE_VO, statMsg, MembershipUIConstants.PAGE_MaintainMembershipProfile);
    }

    private void gotoDiscountPage(HttpServletRequest request, HttpServletResponse response, String oCode, ValueObject VO, String statMsg) throws ServletException {
	gotoPage(request, response, oCode, VO, MembershipUIConstants.MAINTAIN_DISCOUNT_VO, statMsg, MembershipUIConstants.PAGE_MaintainDiscount);
    }

    private void gotoFeeSpecificationPage(HttpServletRequest request, HttpServletResponse response, String oCode, Collection feeSpecList, String statMsg) throws ServletException {
	request.getSession().setAttribute("origCode", oCode);
	request.getSession().setAttribute("FeeSpecificationList", feeSpecList);
	request.setAttribute("statusMessage", statMsg);
	forwardRequest(request, response, MembershipUIConstants.PAGE_MaintainFeeSpecification);

    }

    /**
     * Clean up resources
     */
    public void destroy() {
    }

}
