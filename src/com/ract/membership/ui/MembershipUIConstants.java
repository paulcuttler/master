package com.ract.membership.ui;

/**
 * Membership constants
 *
 * @author Terry Bakker
 * @version 1.0
 */

import com.ract.common.CommonConstants;

public class MembershipUIConstants {
    // These constants define the names of the JSP pages.
    // CommonConstants.DIR_MEMBERSHIP +
    public static final String PAGE_MembershipUIC = "/MembershipUIC";

    public static final String PAGE_MembershipAdminUIC = "/MembershipAdminUIC";

    public static final String PAGE_MembershipReportsUIC = "/MembershipReportsUIC";

    public static final String PAGE_MEMBERSHIP_DISCOUNT_ADMIN_UIC = "/MembershipAdminUIC";

    public static final String PAGE_SELECT_AFFILIATED_CLUB = CommonConstants.DIR_MEMBERSHIP + "/SelectAffiliatedClub.jsp";

    public static final String PAGE_VIEW_MEMBERSHIP_NOTES = CommonConstants.DIR_MEMBERSHIP + "/viewMembershipNoteList.jsp";

    public static final String PAGE_ADD_MEMBERSHIP_NOTE = CommonConstants.DIR_MEMBERSHIP + "/addMembershipNote.jsp";

    public static final String PAGE_CONFIRM_AFFILIATED_CLUB = CommonConstants.DIR_MEMBERSHIP + "/ConfirmAffiliatedClubTransfer.jsp";

    public static final String PAGE_EDIT_QUOTE = CommonConstants.DIR_MEMBERSHIP + "/EditQuote.jsp";

    public static final String PAGE_EDIT_GROUP_DETAILS = CommonConstants.DIR_MEMBERSHIP + "/EditGroupDetails.jsp";

    public static final String PAGE_MANAGE_MEMBERSHIP_GIFTS = CommonConstants.DIR_MEMBERSHIP + "/ManageMembershipGifts.jsp";

    public static final String PAGE_SEARCH_AGENTS = "/cad/searchAgents.jsp";

    public static final String PAGE_UPLOAD_AGENTS_FILE = "/cad/uploadAgentsFile.jsp";

    /**
     * Added for CR26 - rlg - 16/01/03
     */
    // public static final String PAGE_ChangeCardDistributionAddress =
    // CommonConstants.DIR_MEMBERSHIP
    // + "/ChangeCardDistributionAddress.jsp";

    public static final String PAGE_ConfirmGroupChange = CommonConstants.DIR_MEMBERSHIP + "/ConfirmGroupChange.jsp";

    public static final String PAGE_ConfirmTransaction = CommonConstants.DIR_MEMBERSHIP + "/ConfirmTransaction.jsp";

    public static final String PAGE_ChangePersonalMembership = CommonConstants.DIR_MEMBERSHIP + "/ChangePersonalMembership.jsp";

    public static final String PAGE_CancelPersonalMembership = CommonConstants.DIR_MEMBERSHIP + "/CancelPersonalMembership.jsp";

    public static final String PAGE_EditPaymentMethod = CommonConstants.DIR_MEMBERSHIP + "/EditPaymentMethod.jsp";

    public static final String PAGE_EditMembership = CommonConstants.DIR_MEMBERSHIP + "/EditMembership.jsp";

    public static final String PAGE_EditTransaction = CommonConstants.DIR_MEMBERSHIP + "/EditTransaction.jsp";

    public static final String PAGE_ExtractToCARES = CommonConstants.DIR_MEMBERSHIP + "/ExtractToCares.jsp";

    public static final String PAGE_DataSaved = CommonConstants.DIR_MEMBERSHIP + "/DataSaved.jsp";

    public static final String PAGE_DiscretionaryDiscount = CommonConstants.DIR_MEMBERSHIP + "/DiscretionaryDiscount.jsp";

    public static final String PAGE_HoldPersonalMembership = CommonConstants.DIR_MEMBERSHIP + "/HoldPersonalMembership.jsp";

    public static final String PAGE_InvalidQuotes = CommonConstants.DIR_MEMBERSHIP + "/InvalidQuotes.jsp";

    public static final String PAGE_MaintainMembershipProfileList = CommonConstants.DIR_MEMBERSHIP + "/MaintainMembershipProfileList.jsp";

    public static final String PAGE_MaintainMembershipProfile = CommonConstants.DIR_MEMBERSHIP + "/MaintainMembershipProfile.jsp";

    public static final String PAGE_MaintainDiscountList = CommonConstants.DIR_MEMBERSHIP + "/MaintainDiscountList.jsp";

    public static final String PAGE_MaintainDiscount = CommonConstants.DIR_MEMBERSHIP + "/MaintainDiscount.jsp";

    public static final String PAGE_MaintainAccount = CommonConstants.DIR_MEMBERSHIP + "/MaintainAccount.jsp";

    public static final String PAGE_MaintainFeeType = CommonConstants.DIR_MEMBERSHIP + "/MaintainFeeType.jsp";

    public static final String PAGE_MaintainProduct = CommonConstants.DIR_MEMBERSHIP + "/MaintainProduct.jsp";

    public static final String PAGE_MaintainMembershipAccount = CommonConstants.DIR_MEMBERSHIP + "/MaintainAccountList.jsp";

    public static final String PAGE_MaintainMembershipFeeType = CommonConstants.DIR_MEMBERSHIP + "/MaintainFeeTypeList.jsp";

    public static final String PAGE_MaintainProductBenefit = CommonConstants.DIR_MEMBERSHIP + "/MaintainProductBenefit.jsp";

    public static final String PAGE_MaintainProductBenefitList = CommonConstants.DIR_MEMBERSHIP + "/MaintainProductBenefitList.jsp";

    public static final String PAGE_MaintainProductList = CommonConstants.DIR_MEMBERSHIP + "/MaintainProductList.jsp";

    public static final String PAGE_MaintainFeeSpecification = CommonConstants.DIR_MEMBERSHIP + "/MaintainFeeSpecification.jsp";

    public static final String PAGE_MaintainAffiliatedClub = CommonConstants.DIR_MEMBERSHIP + "/MaintainAffiliatedClub.jsp";

    public static final String PAGE_MEMBERSHIP_BRANCH_SALES_REPORT_PARAMETERS = CommonConstants.DIR_MEMBERSHIP_REPORTS + "/BranchSalesReportParameters.jsp";

    public static final String PAGE_MemberCardRequest = CommonConstants.DIR_MEMBERSHIP + "/MembershipCardRequest.jsp";

    // public static final String PAGE_MemberDiscount =
    // CommonConstants.DIR_MEMBERSHIP + "/MemberDiscount.jsp";
    public static final String PAGE_MotorNewsExtract = CommonConstants.DIR_MEMBERSHIP + "/MotorNewsExtract.jsp";

    public static final String PAGE_NonDiscretionaryDiscount = CommonConstants.DIR_MEMBERSHIP + "/NonDiscretionaryDiscount.jsp";

    public static final String PAGE_ReportParameters_Region = CommonConstants.DIR_MEMBERSHIP_REPORTS + "/ReportParameters_Region.jsp";

    public static final String PAGE_ReportParameters_RegionBatch = CommonConstants.DIR_MEMBERSHIP_REPORTS + "/ReportParameters_RegionBatch.jsp";

    public static final String PAGE_RepairMembership = CommonConstants.DIR_MEMBERSHIP + "/RepairMembership.jsp";

    public static final String PAGE_ProduceCards = CommonConstants.DIR_MEMBERSHIP + "/ProduceCards.jsp";

    public static final String PAGE_ProduceRenewalNotices = CommonConstants.DIR_MEMBERSHIP + "/ProduceRenewalNotices.jsp";

    public static final String PAGE_RenewPersonalMembership = CommonConstants.DIR_MEMBERSHIP + "/RenewPersonalMembership.jsp";

    public static final String PAGE_SelectGroupMembers = CommonConstants.DIR_MEMBERSHIP + "/SelectGroupMembers.jsp";

    public static final String PAGE_SelectMembershipType = CommonConstants.DIR_MEMBERSHIP + "/SelectMembershipType.jsp";

    public static final String PAGE_SELECT_MEMBERSHIP_OPTIONS = CommonConstants.DIR_MEMBERSHIP + "/SelectMembershipOptions.jsp";

    public static final String PAGE_SelectPaymentMethod = CommonConstants.DIR_MEMBERSHIP + "/SelectPaymentMethod.jsp";

    public static final String PAGE_SelectProduct = CommonConstants.DIR_MEMBERSHIP + "/SelectProduct.jsp";

    public static final String PAGE_SelectProductOptions = CommonConstants.DIR_MEMBERSHIP + "/SelectProductOptions.jsp";

    public static final String PAGE_SelectRejoinType = CommonConstants.DIR_MEMBERSHIP + "/SelectRejoinType.jsp";

    public static final String PAGE_SelectTransactionQuote = CommonConstants.DIR_MEMBERSHIP + "/SelectTransactionQuote.jsp";

    public static final String PAGE_TransactionInclude = CommonConstants.DIR_MEMBERSHIP + "/TransactionInclude.jsp";

    public static final String PAGE_TransactionComplete = CommonConstants.DIR_MEMBERSHIP + "/TransactionComplete.jsp";

    public static final String PAGE_UndoTransaction = CommonConstants.DIR_MEMBERSHIP + "/UndoTransaction.jsp";

    public static final String PAGE_VerifyUndo = CommonConstants.DIR_MEMBERSHIP + "/test/VerifyUndo.jsp";

    public static final String PAGE_ViewPersonalMembership = CommonConstants.DIR_MEMBERSHIP + "/ViewPersonalMembership.jsp";

    public static final String PAGE_ViewPaymentMethod = CommonConstants.DIR_MEMBERSHIP + "/ViewPaymentMethod.jsp";

    public static final String PAGE_ViewFleetMembership = CommonConstants.DIR_MEMBERSHIP + "/ViewFleetMembership.jsp";

    public static final String PAGE_ViewGroupMembers = CommonConstants.DIR_MEMBERSHIP + "/ViewGroupMembers.jsp";

    public static final String PAGE_ViewGroupMembersInclude = CommonConstants.DIR_MEMBERSHIP + "/ViewGroupMembersInclude.jsp";

    public static final String PAGE_ViewCards = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipCards.jsp";

    public static final String PAGE_ViewHistory = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipHistory.jsp";

    public static final String PAGE_ViewMembershipQuote = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipQuote.jsp";

    public static final String PAGE_ViewMembershipQuoteDetail = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipQuoteDetail.jsp";

    public static final String PAGE_VIEW_MEMBERSHIP_DOCUMENT_DETAIL = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipDocument.jsp";

    public static final String PAGE_ViewMembershipQuoteList = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipQuoteList.jsp";

    public static final String PAGE_ViewVehicles = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipVehicles.jsp";

    public static final String PAGE_ViewDocuments = CommonConstants.DIR_MEMBERSHIP + "/ViewMembershipDocuments.jsp";

    public static final String PAGE_ViewTransactionFees = CommonConstants.DIR_MEMBERSHIP + "/ViewTransactionFees.jsp";

    public static final String PAGE_ManageCredits = CommonConstants.DIR_MEMBERSHIP + "/ManageCredits.jsp";

    public static final String PAGE_UseFastTrack = CommonConstants.DIR_MEMBERSHIP + "/UseFastTrack.jsp";

    public static final String PAGE_ChangeGroupPA = CommonConstants.DIR_MEMBERSHIP + "/ChangeGroupPA.jsp";

    // Pages used for reporting
    public static final String PAGE_BranchDiscountReportParameters = CommonConstants.DIR_MEMBERSHIP_REPORTS + "/BranchDiscountReportParameters.jsp";

    public static final String PAGE_RenewalBatch = CommonConstants.DIR_MEMBERSHIP + "/RenewalBatch.jsp";

    public static final String PAGE_MembershipMovementReportParameters = CommonConstants.DIR_MEMBERSHIP_REPORTS + "/MembershipMovementReportParameters.jsp";

    public static final String PAGE_MembershipRenewalsDueExtractParameters = CommonConstants.DIR_MEMBERSHIP_EXTRACTS + "/MembershipRenewalsDueExtract.jsp";

    public static final String PAGE_ROADSIDE_EXTRACTS = CommonConstants.DIR_MEMBERSHIP_EXTRACTS + "/RoadsideExtract.jsp";

    // public static final String

    // These constants define the tags used to hold objects in the tomcat
    // session.
    public static final String SESSION_TRANSACTION_CONTAINER_LIST = "transactionContainerList";

    // public static final String SESSION_CLIENT_SEARCH_LIST =
    // "memClientSearchList";

    public static final String MAINTAIN_MEMBERSHIP_PROFILE_VO = "MembershipProfileVO";

    public static final String MAINTAIN_FEE_TYPE_VO = "FeeTypeVO";

    public static final String MAINTAIN_DISCOUNT_VO = "DiscountTypeVO";

    public static final String MAINTAIN_PRODUCT_BENEFIT_VO = "ProductBenefitTypeVO";

    public static final String MAINTAIN_PRODUCT_VO = "ProductVO";

    public static final String MAINTAIN_ACCOUNT_VO = "AccountVO";

    public static final String MAINTAIN_FEESPECIFICATION_VO = "FeeSpecificationVO";

    public static final String MAINTAIN_AFFILIATED_CLUB_VO = "AffiliatedClubVO";

    // These constants define the text to be displayed on the top of pages when
    // performing transactions.
    public static final String HEADING_CreateMembership = "Create membership";

    public static final String HEADING_RenewMembership = "Renew membership";

    public static final String HEADING_RejoinMembership = "Rejoin membership";

    public static final String HEADING_TransferMembership = "Transfer membership";

    public static final String HEADING_ChangeMembership = "Change Membership";

    public static final String HEADING_CancelMembership = "Cancel Membership";

}
