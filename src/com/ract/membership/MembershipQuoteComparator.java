package com.ract.membership;

import java.util.Comparator;

/**
 * Title: Master Project Description: Compares two MembershipQuoteVO objects and
 * orders by quote number. The default sort order is ascending. This can be
 * reversed by specifying true to the constructor. Copyright: Copyright (c) 2003
 * Company: RACT
 * 
 * @author
 * @version 1.0
 */

public class MembershipQuoteComparator implements Comparator {
    public static final int SORT_BY_QUOTE_NUMBER = 0;

    public static final int SORT_BY_CLIENT_NUMBER = 1;

    private boolean reverse = false;

    private int sortMode = SORT_BY_QUOTE_NUMBER;

    public MembershipQuoteComparator() {
    }

    public MembershipQuoteComparator(boolean useReverseOrder) {
	this.reverse = useReverseOrder;
    }

    public void setSortMode(int mode) {
	this.sortMode = mode;
    }

    public int compare(Object o1, Object o2) {
	if (o1 instanceof MembershipQuote && o2 instanceof MembershipQuote) {
	    MembershipQuote q1 = (MembershipQuote) o1;
	    MembershipQuote q2 = (MembershipQuote) o2;
	    switch (this.sortMode) {
	    case SORT_BY_QUOTE_NUMBER:
		return applySortOrder(q1.getQuoteNumber().compareTo(q2.getQuoteNumber()));
	    case SORT_BY_CLIENT_NUMBER:
		return applySortOrder(q1.getClientNumber().compareTo(q2.getClientNumber()));
	    default:
		return applySortOrder(q1.getQuoteNumber().compareTo(q2.getQuoteNumber()));
	    }
	} else if (o1 instanceof MembershipQuote && o2 instanceof MembershipQuote) {
	    MembershipQuote q1 = (MembershipQuote) o1;
	    MembershipQuote q2 = (MembershipQuote) o2;
	    try {
		switch (this.sortMode) {
		case SORT_BY_QUOTE_NUMBER:
		    return applySortOrder(q1.getQuoteNumber().compareTo(q2.getQuoteNumber()));
		case SORT_BY_CLIENT_NUMBER:
		    return applySortOrder(q1.getClientNumber().compareTo(q2.getClientNumber()));
		default:
		    return applySortOrder(q1.getQuoteNumber().compareTo(q2.getQuoteNumber()));
		}
	    } catch (Exception e) {
		return 0;
	    }
	}
	return 0;
    }

    public boolean equals(Object obj) {
	if (obj instanceof MembershipQuoteComparator) {
	    MembershipQuoteComparator mqc = (MembershipQuoteComparator) obj;
	    return this.reverse == mqc.isReverse();
	}

	return false;
    }

    /**
     * The compareTo methods return a result for sorting in ascending order by
     * default. A negative integer means that the second object was less than
     * the first. A zero means that they were the same. A positive integer means
     * that the second object was greater than the first. If we want a reverse
     * sorted list then invert the result.
     */
    private int applySortOrder(int compareResult) {
	return (this.reverse ? compareResult * -1 : compareResult);
    }

    public boolean isReverse() {
	return this.reverse;
    }

    public void setReverse(boolean useReverseSortOrder) {
	this.reverse = useReverseSortOrder;
    }

}
