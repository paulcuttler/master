package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.ejb.ObjectNotFoundException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientFactory;
import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientNote;
import com.ract.client.ClientSubscription;
import com.ract.client.ClientVO;
import com.ract.common.ClassWriter;
import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.DirectDebitFailedValidationException;
import com.ract.common.ExceptionHelper;
import com.ract.common.Publication;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.payment.AmountOutstanding;
import com.ract.payment.FinanceVoucher;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.directdebit.DirectDebitAdapter;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.finance.FinanceMgr;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.MethodCounter;
import com.ract.util.NumberUtil;
import com.ract.util.XMLHelper;

/**
 * <p>
 * Holds the data for a membership. In the future a client may have many memberships. At present the client number is used as the membership number. However a unique membership ID is used to identify each membership independant of the membership number.
 * </p>
 * <p>
 * This object also provides the implementation for holding membership history through the MembershipHistory interface. A constructor is provided that takes an XML DOM Node object. When the class is constructed from the XML DOM Node the mode of the value object is set to "History" and the read only flag is set to true.
 * </p>
 * <p>
 * In history mode the value object does not do any lazy fetching of data from the database. If the data is not provided in the XML DOM node then it is not available.
 * </p>
 * <p>
 * When set to read only the value object will not permit any updating of the value objects data.
 * </p>
 * 
 * @hibernate.class table="mem_membership" lazy="false"
 * 
 * 
 * @author T. Bakker.
 * @see MembershipBean, MembershipBeanBMP
 */
public class MembershipVO extends ValueObject implements MembershipHistory, Writable {

	public final static String MESSAGE_EXPIRED_TOO_LONG = "The membership has been expired for too long.";

	/**
	 * @hibernate.property
	 * @return
	 */
	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	/**
	 * @hibernate.property
	 * @return
	 */
	public String getInceptionSalesBranch() {
		return inceptionSalesBranch;
	}

	public void setInceptionSalesBranch(String inceptionSalesBranch) {
		this.inceptionSalesBranch = inceptionSalesBranch;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((membershipID == null) ? 0 : membershipID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MembershipVO other = (MembershipVO) obj;
		if (membershipID == null) {
			if (other.membershipID != null)
				return false;
		} else if (!membershipID.equals(other.membershipID))
			return false;
		return true;
	}

	/**
	 * The membership has a future status when the base status of the membership is Active but the current system date is before the commence date of the membership. The future status value is not stored in the database. It is derived when the status of the membership is queried.
	 */
	public final static String STATUS_FUTURE = "Future";

	/**
	 * The membership has a transferred status when the base status of the membership is transferred. The transferred status is stored in the database.
	 */
	public final static String STATUS_TRANSFERRED = "Transferred";

	/**
	 * The membership is in an Active status when the base status of the membership is Active and the current system date is between the commence date of the membership and the expiry date of the membership. The Active status value is stored in the database.
	 */
	public final static String STATUS_ACTIVE = "Active";

	/**
	 * The membership has a status of Expired when the base status is Active but the current system date is after the membership expiry date. The Expired status value is not stored in the database. It is derived when the status of the membership is queried.
	 */
	public final static String STATUS_EXPIRED = "Expired";

	/**
	 * The Cancelled status means that the membership can no longer be used. Membership benefits will no longer be provided and the client will not be sent renewal notices.
	 */
	public final static String STATUS_CANCELLED = "Cancelled";

	/**
	 * The Terminate status means that the membership can no longer be used. Membership benefits will no longer be provided and the client will not be sent renewal notices.
	 */
	public final static String STATUS_TERMINATE = "Terminate";

	/**
	 * The On hold status means that the membership has been suspended for a period of time. Membership benefits will not be provided and the client will not be sent a renewal notice.
	 */
	public final static String STATUS_ONHOLD = "On hold";

	/**
	 * The in renewal status means that the membership is due for renewal and has been sent the first renewal notice.
	 */
	public final static String STATUS_INRENEWAL = "In Renewal";

	/**
	 * The undone status means that the create of the membership was undone and the membership record has been retained for audit purposes.
	 */
	public final static String STATUS_UNDONE = "Undone";

	/**
	 * The name to call this class when writing the class data using a ClassWriter
	 */
	public static final String WRITABLE_CLASSNAME = "Membership";

	/**
	 * A unique number used internally to identify each membership record.
	 */
	protected Integer membershipID;

	/**
	 * A unique number that is used by the member to reference the membership.
	 */
	protected String membershipNumber;

	/**
	 * The membership type. Can be used for display.
	 */
	protected String membershipTypeCode;

	/**
	 * A reference to the membership type value object. Use the getMembershipType() method to populate.
	 */
	private MembershipTypeVO membershipType;

	/**
	 * The membership profile. Can be used for display.
	 */
	protected String profileCode = MembershipProfileVO.PROFILE_CLIENT;

	/**
	 * A reference to the membership profile value object. Use the getMembershipProfile() method to populate.
	 */
	private MembershipProfileVO profile;

	private String transactionReference;

	/**
	 * The base status of the membership. e.g. "Active", "Cancelled" "On Hold". Set the default status to active.
	 */
	protected String baseStatus = MembershipVO.STATUS_ACTIVE;

	/**
	 * The derived status of the membership. e.g. "Pending", "Active", "In renewal", "Expired".
	 */
	private String virtualStatus = null;

	/**
	 * Indicates if the membership will lapse and not be renewed when it expires.
	 */
	protected Boolean allowToLapse = Boolean.valueOf(false);

	/**
	 * The unique identifier for the client who owns the membership.
	 */
	protected Integer clientNumber;

	/**
	 * Was marked as static! This causes issue with hibernate dirty checking
	 * 
	 * @see http://java.sun.com/blueprints/qanda/ejb_tier/restrictions.html
	 */
	private String affiliatedClubProductLevel;

	/**
	 * Set to true if this member is prime addressee of a group
	 */
	private Boolean groupPrimeAddressee = null;

	/**
	 * @hibernate.property
	 * @hibernate.column name="affliated_club_product_level"
	 * @return String
	 */
	public String getAffiliatedClubProductLevel() {
		return this.affiliatedClubProductLevel;
	}

	public void setAffiliatedClubProductLevel(String productLevel) {
		this.affiliatedClubProductLevel = productLevel;
	}

	/**
	 * A reference to the client value object that owns this membership. Use the getClient() method to populate.
	 */
	private ClientVO client;

	/**
	 * The unique identifier of the base product type. Can be used for display.
	 */
	protected String productCode;

	/**
	 * A reference to the base product type that the membership has been issued with.
	 */
	private ProductVO product;

	private String currentProductCode;

	private DateTime currentProductEffectiveDate;

	/**
	 * A code corresponding to a reason for allowing the membership to lapse.
	 */
	protected String allowToLapseReasonCode;

	/**
	 * @hibernate.property
	 * @hibernate.column name="allow_to_lapse_reason_code"
	 * @return String
	 */
	public String getAllowToLapseReasonCode() {
		return this.allowToLapseReasonCode;
	}

	public void setAllowToLapseReasonCode(String allowToLapseReasonCode) throws RemoteException {
		checkUpdateAllowed();
		this.allowToLapseReasonCode = allowToLapseReasonCode;
	}

	/**
	 * A list of members in the group.
	 */
	protected ArrayList groupMembers;

	/**
	 * The date that the base product becomes effective. Initially this will be the same as the membership commence date. But if the base product is changed during the period of membership this date gets set to the date that the benefits applicable to the new base product can be claimed from. This date could be in the future if the member specifies a preferred commence date when doing the product upgrade transaction. If this date is in the future then you need to look back through the transaction history to obtain a membership snapshot that specifies the base product that currently applies.
	 */
	protected DateTime productEffectiveDate;

	/**
	 * The ID of the direct debit authority last used to pay the membership. Will only be populated where direct debit payment is made for a prime addressee.
	 */
	protected Integer directDebitAuthorityID;

	/**
	 * The direct debit authority corresponding to the direct debit authority ID.
	 */
	private DirectDebitAuthority directDebitAuthority;

	private DirectDebitSchedule currentDirectDebitSchedule;

	/**
	 * The identifier of the next base product to apply at the next renewal transaction. Used when down grading the base product mid term. The down graded product is not applied until the next renewal.
	 */
	protected String nextProductCode;

	/**
	 * The term that the membership was taken out for. This attribute is not persisted to the database, it is only set while doing a transaction that can effect the membership term.
	 */
	protected Interval membershipTerm;

	/**
	 * The date that the member first took out membership. This is used to calculate the number of membership years.
	 */
	protected DateTime joinDate;

	/**
	 * The date that the membership starts from. This is set to the greater of the transaction date or the Preferred commence date if it is specified. The Commence date is used to calculate the membership expiry date.
	 */
	protected DateTime commenceDate;

	/**
	 * The date on which the membership expires and no further claims on membership services or benefits can be made. The expiry date should be the same as the membership commence date plus the number of renewal years.
	 */
	protected DateTime expiryDate;

	/**
	 * The name of the affiliated motor club that the members transferred from.
	 */
	protected String affiliatedClubCode;

	/**
	 * The membership id / card id for the affiliated motor club that the member transferred from.
	 */
	protected String affiliatedClubId;

	/**
	 * The expiry date of the affiliated motor club membership.
	 */
	protected DateTime affiliatedClubExpiryDate;

	/**
	 * The period of membership that can be credited at the next appropriate transaction.
	 */
	protected Interval creditPeriod;

	/**
	 * A dollar value that may be creditied against the next appropriate transaction. The credit amount is usually derived from the remaining paid value of the membership. When a transaction is processed the paid unearned value of the membership is determined. This amount is applied to the fees as an "unearned credit" type of discount when the automatic discounts are created. If there is any paid unearned credit left over the remainder is added to the membership as a credit to be used on the next transaction. Once all other discounts have been applied to the transaction and there is still an amount to be paid then any credit amounts held on the membership are applied to the fees as a "credit" type of discount. When re-applying credit discounts care must be taken to account for any unused unearned credits as well.
	 * 
	 * NOTE: The credit amount is GST INCLUSIVE.
	 */
	protected double creditAmount = 0.0;

	/**
	 * When a client transfers from an interstate or overseas club a dollar value may be creditied against the next appropriate transaction. The credit amount is calculated from the remaining part of the other membership based on a value in mem_interstate_club. When a transaction is processed the paid unearned value of the membership is determined. This amount is applied to the fees as an "unearned credit" type of discount when the automatic discounts are created. If there is any paid unearned credit left over the remainder is added to the membership as a credit to be used on the next transaction. Once all other discounts have been applied to the transaction and there is still an amount to be paid then any credit amounts held on the membership are applied to the fees as a "credit" type of discount. When re-applying credit discounts care must be taken to account for any unused unearned credits as well.
	 * 
	 * NOTE: The affiliated credit amount is GST INCLUSIVE???????.
	 */
	// protected BigDecimal affiliatedClubCreditAmount = new BigDecimal(0);

	/**
	 * The credit value of the membership used in calculating the discount for re-instating held and cancelled memberships.
	 * 
	 * NOTE: The suspense amount is GST INCLUSIVE.
	 */
	// protected double suspenseAmount = 0.0;

	/**
	 * The client specified number of vehicles that the membership is to cover. If this value is greater than zero then the list of vehicles is ignored when doing fee calculations.
	 */
	protected int vehicleCount = 0;

	/**
	 * The default expiry type used for vehicles attached to the membership. "Common expiry" means that all attached vehicles expire on the same date as the membership. "Un-common expiry" means that each vehicle expires at its own time.
	 */
	protected String vehicleExpiryType;

	/**
	 * The list of vehicles that correspond to the vehicle ids. This is a temporary cache of vehicle value objects (VehicleVO).
	 */
	protected Vector vehicleList;

	/**
	 * The list of product options that correspond to the list of product option codes. This is a temporary cache of product options (ProductBenefitTypeVO).
	 */
	protected Vector productOptionList;

	/**
	 * The list of discounts that correspond to the discount code list. This is a temporary cache of discount types (DiscountTypeVO).
	 */
	protected Vector discountList;

	/**
	 * The perceived value of the membership to the client. This is used to calculate any rebate on the membership during subsequent transactions and is not based on the actual amount paid for the membership.
	 */
	protected BigDecimal membershipValue;

	/**
	 * The list of members of the group, as MembershipGroupDetailVO, to which this member belongs. Use the getMembershipGroupList() method to populate The membership that holds this list will also be defined in the list. This list is not maintained as part of the membership.
	 */
	// private Collection membershipGroupList;
	private MembershipGroupVO membershipGroupVO;

	/**
	 * A list of documents that have been issued for this membership. Use the getDocumentList() method to populate. This list is not maintained as part of the membership.
	 */
	private Vector documentList;

	/**
	 * A list of membership cards, as MembershipCardVO, that have been issued to the membership. Use the getCardList() method to populate. This list is not maintained as part of the membership.
	 */
	private Vector membershipCardList;

	/**
	 * A list of transactions, as MembershipTransactionVO, that have been performed on the membership. Use the getTransactionList() method to populate. This list is not maintained as part of the membership.
	 */
	private Collection transactionList;

	/**
	 * A period of free membership that is to be added to the membership expiry date. This attribute will only be set while processing a transaction where a discount type that has a period of free membership has been applied. The effect is to extend the calculated expiry date while the transaction is being processed. When the transaction is saved the expiry date is saved as the extended expiry date so that when the membership is reloaded the discount period is incorporated.
	 */
	private Interval discountPeriod;

	/**
	 * Holds a keyed list of credits that are to be added to the membership credit amount when processing a transaction. These amounts get added credit when the Membership entity bean is set from the VO.
	 */
	private Hashtable creditMap = new Hashtable();

	protected String pendingFeeId = null;

	/**
	 * The date a roadside account was created.
	 */
	private DateTime roadsideCreateDate;

	private boolean outstandingReducedCredit;

	/**
	 * Vehicle attributes.
	 */
	private String rego;

	private String vin;

	private String make;

	private String model;

	private String year;

	private DateTime lastUpdate;

	/************ Constructors ***************/

	/**
	 * Default constructor.
	 */
	public MembershipVO() {
	}

	/**
	 * Initialise the membership with the specified membership ID. All other attribute initialisation must be done through the setter methods.
	 */
	public MembershipVO(Integer membershipID) {
		this.membershipID = membershipID;
	}

	/**
	 * Initialise the membership attributes and sub components with the data from the membership snapshot XML node. //TODO complete construction from XML
	 */
	public MembershipVO(Node membershipNode, String mode) throws RemoteException {
		LogUtil.debug(this.getClass(), "XXXXXXXXXXXXXXXXXX test in here" + mode);
		MethodCounter.addToCounter("MembershipVO", "Constructor");
		if (membershipNode == null || !membershipNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
			throw new SystemException("Failed to create membership history from XML node. The node is not valid.");
		}

		// Initialise the lists
		this.discountList = new Vector();
		this.documentList = new Vector();
		this.membershipCardList = new Vector();
		this.productOptionList = new Vector();
		this.transactionList = new ArrayList();
		this.vehicleList = new Vector();

		NodeList elementList = membershipNode.getChildNodes();
		Node elementNode = null;
		Node writableNode;
		String nodeName = null;
		String valueText = null;
		for (int i = 0; i < elementList.getLength(); i++) {
			elementNode = elementList.item(i);
			nodeName = elementNode.getNodeName();

			writableNode = XMLHelper.getWritableNode(elementNode);
			valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

			if ("affiliatedClubExpiryDate".equals(nodeName) && valueText != null) {
				try {
					this.affiliatedClubExpiryDate = new DateTime(valueText);
				} catch (ParseException pe) {
					addWarning("The affiliated club expiry date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("affiliatedClubId".equals(nodeName) && valueText != null) {
				this.affiliatedClubId = valueText;
			} else if ("affiliatedClubCode".equals(nodeName) && valueText != null) {
				this.affiliatedClubCode = valueText;
			} else if ("affiliatedClubProductLevel".equals(nodeName) && valueText != null) {
				this.affiliatedClubProductLevel = valueText;
			} else if ("allowToLapse".equals(nodeName) && valueText != null) {
				this.allowToLapse = Boolean.valueOf(valueText);
			} else if ("allowToLapseReasonCode".equals(nodeName) && valueText != null) {
				this.allowToLapseReasonCode = valueText;
			} else if ("clientNumber".equals(nodeName) && valueText != null) {
				this.clientNumber = new Integer(valueText);
			} else if ("commenceDate".equals(nodeName) && valueText != null) {
				try {
					this.commenceDate = new DateTime(valueText);
				} catch (ParseException pe) {
					addWarning("The commence date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("creditAmount".equals(nodeName) && valueText != null) {
				this.creditAmount = Double.parseDouble(valueText);
			} else if ("creditPeriod".equals(nodeName) && valueText != null) {
				try {
					this.creditPeriod = new Interval(valueText);
				} catch (Exception e) {
					addWarning("The credit period '" + valueText + "' is not a valid interval. " + e.getMessage());
				}
			} else if ("expiryDate".equals(nodeName) && valueText != null) {
				try {
					this.expiryDate = new DateTime(valueText);
				} catch (ParseException pe) {
					addWarning("The expiry date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("joinDate".equals(nodeName) && valueText != null) {
				try {
					this.joinDate = new DateTime(valueText);
				} catch (ParseException pe) {
					addWarning("The join date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("membershipID".equals(nodeName) && valueText != null) {
				this.membershipID = new Integer(valueText);
			} else if ("membershipNumber".equals(nodeName) && valueText != null) {
				this.membershipNumber = valueText;
			} else if ("membershipTerm".equals(nodeName) && valueText != null) {
				try {
					this.membershipTerm = new Interval(valueText);
				} catch (Exception e) {
					addWarning("The membership term '" + valueText + "' is not a valid interval. " + e.getMessage());
				}
			} else if ("membershipTypeCode".equals(nodeName) && valueText != null) {
				this.membershipTypeCode = valueText;
			} else if ("membershipValue".equals(nodeName) && valueText != null) {
				this.membershipValue = new BigDecimal(valueText);
			} else if ("nextProductCode".equals(nodeName) && valueText != null) {
				this.nextProductCode = valueText;
			} else if ("directDebitAuthorityID".equals(nodeName) && valueText != null) {
				this.directDebitAuthorityID = new Integer(valueText);
			} else if ("paymentMethodID".equals(nodeName) && valueText != null) {
				// The direct debit authority ID used to be called a payment
				// method ID.
				// In the older history files this is what it will be called.
				this.directDebitAuthorityID = new Integer(valueText);
			} else if ("productCode".equals(nodeName) && valueText != null) {
				LogUtil.debug(this.getClass(), "node productCode=" + valueText);
				this.productCode = valueText;
			} else if ("productEffectiveDate".equals(nodeName) && valueText != null) {
				try {
					this.productEffectiveDate = new DateTime(valueText);
				} catch (ParseException pe) {
					addWarning("The product effective date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("profileCode".equals(nodeName) && valueText != null) {
				this.profileCode = valueText;
			} else if ("rego".equals(nodeName) && valueText != null) {
				this.rego = valueText;
			} else if ("vin".equals(nodeName) && valueText != null) {
				this.vin = valueText;
			} else if ("make".equals(nodeName) && valueText != null) {
				this.make = valueText;
			} else if ("model".equals(nodeName) && valueText != null) {
				this.model = valueText;
			} else if ("year".equals(nodeName) && valueText != null) {
				this.year = valueText;
			} else if ("status".equals(nodeName) && valueText != null) {
				this.baseStatus = valueText;
			} else if ("transactionReference".equals(nodeName) && valueText != null) {
				this.transactionReference = valueText;
			} else if ("inceptionSalesBranch".equals(nodeName) && valueText != null) {
				this.inceptionSalesBranch = valueText;
			} else if ("vehicleCount".equals(nodeName) && valueText != null) {
				this.vehicleCount = Integer.parseInt(valueText);
			} else if ("vehicleExpiryType".equals(nodeName) && valueText != null) {
				this.vehicleExpiryType = valueText;
			} else if ("client".equals(nodeName) && writableNode != null) {
				this.client = ClientFactory.getClientVO(writableNode);
				LogUtil.debug(this.getClass(), "client" + this.client);
			} else if ("membershipType".equals(nodeName)) // object
			{
			} else if ("product".equals(nodeName)) // object
			{
			} else if ("profile".equals(nodeName)) // object
			{
			} else if ("discountList".equals(nodeName)) {
				NodeList discountListNodeList = elementNode.getChildNodes();
				if (discountListNodeList != null) {
					for (int j = 0; j < discountListNodeList.getLength(); j++) {
						Node discountNode = discountListNodeList.item(j);
						if (discountNode.getNodeType() == Node.ELEMENT_NODE) {
							MembershipDiscount memDisc = new MembershipDiscount(discountNode);
							this.discountList.add(memDisc);
						}
					}
				}
			} else if ("documentList".equals(nodeName)) {
				NodeList documentListNodeList = elementNode.getChildNodes();
				if (documentListNodeList != null) {
					for (int j = 0; j < documentListNodeList.getLength(); j++) {
						Node documentNode = documentListNodeList.item(j);
						if (documentNode.getNodeType() == Node.ELEMENT_NODE) {
							MembershipDocument memDocVO = new MembershipDocument(documentNode);
							this.documentList.add(memDocVO);
						}
					}
				}
			} else if ("membershipCardList".equals(nodeName)) {
				NodeList cardListNodeList = elementNode.getChildNodes();
				if (cardListNodeList != null) {
					for (int j = 0; j < cardListNodeList.getLength(); j++) {
						Node cardNode = cardListNodeList.item(j);
						if (cardNode.getNodeType() == Node.ELEMENT_NODE) {
							MembershipCardVO memCardVO = new MembershipCardVO(cardNode);
							try {
								memCardVO.setMode(ValueObject.MODE_HISTORY);
								this.membershipCardList.add(memCardVO);
							} catch (ValidationException ve) {
								addWarning("Failed to set card value object mode to '" + ValueObject.MODE_HISTORY + "'. " + ve.getMessage());
							}
						}
					}
				}
			} else if ("transactionList".equals(nodeName)) {
				// not written in XML
			} else if ("membershipGroupList".equals(nodeName)) {
				LogUtil.debug(this.getClass(), "end of load 3");
				NodeList groupListNodeList = elementNode.getChildNodes();
				if (groupListNodeList != null) {
					LogUtil.debug(this.getClass(), "end of load 3a");
					ArrayList groupDetailList = new ArrayList();
					for (int j = 0; j < groupListNodeList.getLength(); j++) {
						LogUtil.debug(this.getClass(), "end of load 3b");
						Node groupNode = groupListNodeList.item(j);
						if (groupNode.getNodeType() == Node.ELEMENT_NODE) {
							LogUtil.debug(this.getClass(), "end of load 3c");
							MembershipGroupDetailVO groupDetVO = new MembershipGroupDetailVO(groupNode);
							try {
								groupDetVO.setMode(ValueObject.MODE_HISTORY);
								LogUtil.debug(this.getClass(), "end of load 3d");
							} catch (ValidationException ve) {
								LogUtil.debug(this.getClass(), "end of load 3e");
								addWarning("Failed to set group detail value object mode to '" + ValueObject.MODE_HISTORY + "'. " + ve.getMessage());
							}
							LogUtil.debug(this.getClass(), "end of load 3f");
							groupDetailList.add(groupDetVO);
							LogUtil.debug(this.getClass(), "end of load 3g");
						}
					}
					if (!groupDetailList.isEmpty()) {
						LogUtil.debug(this.getClass(), "end of load 3h");
						this.membershipGroupVO = new MembershipGroupVO(groupDetailList);
					}
					LogUtil.debug(this.getClass(), "end of load 4");
				}
				LogUtil.debug(this.getClass(), "end of load 5a");
			} else if ("productOptionList".equals(nodeName)) {
				NodeList optionListNodeList = elementNode.getChildNodes();
				if (optionListNodeList != null) {
					for (int j = 0; j < optionListNodeList.getLength(); j++) {
						Node optionNode = optionListNodeList.item(j);
						if (optionNode.getNodeType() == Node.ELEMENT_NODE) {
							MembershipProductOption option = new MembershipProductOption(optionNode);
							this.productOptionList.add(option);
						}
					}
				}
			} else if ("vehicleList".equals(nodeName)) {
			}
		}
		LogUtil.debug(this.getClass(), "end of load 5");

		// Now do some checking to make sure we have atleast the minimum data
		// required for a membership.
		if (this.membershipID == null) {
			addWarning("The membership ID is null.");

		}
		if (this.membershipTypeCode == null) {
			addWarning("The membership type code is null.");

		}
		if (this.membershipNumber == null) {
			addWarning("The membership number is null.");

		}
		if (this.clientNumber == null) {
			addWarning("The client number is null.");

		}

		if (this.joinDate == null) {
			addWarning("The join date is null.");

		}
		if (this.commenceDate == null) {
			addWarning("The commence date is null.");

		}
		if (this.expiryDate == null) {
			addWarning("The expiry date is null.");

		}
		if (this.baseStatus == null) {
			addWarning("The membership status is null.");
		}

		if (this.productCode == null || this.productEffectiveDate == null) {
			addWarning("The product code or product effective date is null.");
		}

		if (this.membershipTerm == null) {
			addWarning("The membership term is null.");

		}
		if (this.profileCode == null) {
			addWarning("The membership profile code is null.");

		}
		LogUtil.debug(this.getClass(), "end of load 6");
		try {
			if (mode == null) {
				mode = this.MODE_HISTORY;
			}
			this.setMode(mode);

			if (this.isHistory()) {
				// Membership history should not be able to be changed so make
				// the
				// value object read only and set the mode to history.
				setReadOnly(true);
			}
			LogUtil.debug(this.getClass(), "end of load");
		} catch (ValidationException ve) {
			// Should be setting it to a valid value so pass the exception
			// back as a system exception
			throw new SystemException(ve.getMessage());
		}

		// Address bug where a history record retrieves the current state of the
		// membership group and incorrectly tries to
		// insert the records. This is due to the absence of a group node if
		// there is no group.
		if (this.membershipGroupVO == null) {
			this.membershipGroupVO = new MembershipGroupVO();
		}

		/**
		 * 
		 * Initialise the current transaction list if it has not already been initialised. This is due to an issue with refetching during a transaction.
		 */
		LogUtil.warn(this.getClass(), "Initialising current transaction list: " + this.getClientNumber());
		this.getCurrentTransactionList();
	}

	/************************ getter methods *************************/

	/**
	 * @hibernate.id column="membership_id" generator-class="assigned"
	 */
	public Integer getMembershipID() {
		return this.membershipID;
	}

	/**
	 * Return the membership number padded out with leading zeros to the desired length. NOTE: The membership number cannot be guaranteed to be a number.
	 */
	public String getMembershipNumber(int length) {
		StringBuffer memNum = new StringBuffer();
		if (this.membershipNumber != null) {
			for (int i = this.membershipNumber.length(); i < length; i++) {
				memNum.append("0");
			}
			memNum.append(this.membershipNumber);
		}
		return memNum.toString();
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="membership_number"
	 */
	public String getMembershipNumber() {
		return this.membershipNumber;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="membership_type_code"
	 */
	public String getMembershipTypeCode() {
		return this.membershipTypeCode;
	}

	/**
	 * Return a reference to the membership type value object. Initialise the reference if this is the first fetch.
	 */
	public MembershipTypeVO getMembershipType() throws RemoteException {
		if (this.membershipType == null && this.membershipTypeCode != null && this.isNotHistory()) {
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			this.membershipType = refMgr.getMembershipType(this.membershipTypeCode);
			if (this.membershipType == null) {
				throw new RemoteException("Failed to get membership type '" + this.membershipTypeCode + "'. It does not exist!");
			}
		}
		return this.membershipType;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="profile_code"
	 */
	public String getMembershipProfileCode() {
		return this.profileCode;
	}

	/**
	 * Return the profile value object for the profile code. Can't have multiple threads initialising the reference at the same time so must be synchronized.
	 */
	public synchronized MembershipProfileVO getMembershipProfile() throws RemoteException {
		if (this.profile == null && this.profileCode != null) {
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			this.profile = refMgr.getMembershipProfile(this.profileCode);
		}
		return this.profile;
	}

	/**
	 * Retrun the remaining term of membership from today to the expiry date. If the membership is expired then a zero term is returned.
	 */
	public Interval getRemainingTerm() throws SystemException {
		return getRemainingTerm(new DateTime());
	}

	/**
	 * Retrun the remaining term of membership from the effective date to the expiry date. If the membership is expired then a zero term is returned.
	 */
	public Interval getRemainingTerm(DateTime effectiveDate) throws SystemException {
		Interval remainingTerm = new Interval();
		if (effectiveDate != null && this.expiryDate != null && this.expiryDate.afterDay(effectiveDate)) {
			try {
				remainingTerm.setIntervalDays(effectiveDate, this.expiryDate);
			} catch (Exception e) {
				throw new SystemException(e);
			}
		}
		return remainingTerm;
	}

	/**
	 * The status returned depends on the base status, commence date, expiry date and renewal notices sent.
	 */
	public String getStatus() throws RemoteException {

		if (this.virtualStatus == null) {
			// If the base status is active then the virtual status may be
			// modified
			// depending on the current system date and the membership commence
			// and
			// expiry dates.
			if (MembershipVO.STATUS_ACTIVE.equals(this.baseStatus)) {
				DateTime today = DateUtil.clearTime(new DateTime());

				if (today.before(this.commenceDate)) {
					this.virtualStatus = MembershipVO.STATUS_FUTURE;
				} else if (today.after(this.expiryDate)) {

					/**
					 * //TODO consider inclusion of virtual status to represent expired less than three months. ie. result of isActive.
					 */

					this.virtualStatus = MembershipVO.STATUS_EXPIRED;
				} else {
					// See if the membership is in renewal.
					// TODO check PA

					if (getCurrentRenewalDocument() != null) {
						this.virtualStatus = MembershipVO.STATUS_INRENEWAL;
					}

					// If it is still not set then use the base status.
					if (this.virtualStatus == null) {
						this.virtualStatus = this.baseStatus;
					}
				}
			} else {
				this.virtualStatus = this.baseStatus;
			}
		}
		return this.virtualStatus;
	}

	/**
	 * Gets a membership renewal document that was generated on or after the start of the renewal period.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public MembershipDocument getCurrentRenewalDocument() throws RemoteException {
		LogUtil.debug(this.getClass(), "getCurrentRenewalDocument start");
		MembershipDocument currentDocument = null;
		try {
			Integer targetMembershipId = null;
			LogUtil.debug(this.getClass(), "getCurrentRenewalDocument 1");
			if (!this.isGroupMember()) {
				LogUtil.debug(this.getClass(), "getCurrentRenewalDocument 2a " + this.membershipID);
				targetMembershipId = this.membershipID;
				LogUtil.debug(this.getClass(), "getCurrentRenewalDocument 2b " + targetMembershipId);
			} else {
				targetMembershipId = this.getMembershipGroup().getMembershipGroupDetailForPA().getMembershipGroupDetailPK().getMembershipID();
				LogUtil.debug(this.getClass(), "getCurrentRenewalDocument 3 " + targetMembershipId);
			}
			LogUtil.debug(this.getClass(), "getCurrentRenewalDocument 4 " + targetMembershipId);
			currentDocument = MembershipEJBHelper.getMembershipMgr().getCurrentDocument(targetMembershipId, this.expiryDate);
			LogUtil.debug(this.getClass(), "getCurrentRenewalDocument 5 " + currentDocument);
		} catch (Exception ex) {
			LogUtil.warn(this.getClass(), "Unable to get the current renewal document. " + ex);
		}
		LogUtil.debug(this.getClass(), "getCurrentRenewalDocument end");
		return currentDocument;
	}

	/**
	 * Return the actual status of the membership as opposed to the drived status returned by the getStatus() method.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="membership_status"
	 * 
	 */
	public String getBaseStatus() {
		LogUtil.debug(this.getClass(), "getBaseStatus() baseStatus = " + this.baseStatus);
		return this.baseStatus;
	}

	/**
	 * Return the branch number attached to the client.
	 */
	public int getBranchNo() throws RemoteException {
		ClientVO clientVO = getClient();
		return clientVO.getBranchNumber().intValue();
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="client_number"
	 */
	public Integer getClientNumber() {
		return this.clientNumber;
	}

	/**
	 * Fetch the client value object for the client number. Hold a reference to the client VO once fetched. Can't have multiple threads initialising the reference at the same time so must be synchronized.
	 */
	public synchronized ClientVO getClient() throws RemoteException {

		if (this.clientNumber != null && this.client == null/*
															 * && this.isNotHistory ()
															 */) {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			try {
				this.client = clientMgr.getClient(this.clientNumber);
			} catch (ValidationException ve) {
				throw new RemoteException("Failed to get client with number '" + clientNumber + "'\n", ve);
			}
		}
		return this.client;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="product_code"
	 */
	public String getProductCode() {
		return this.productCode;
	}

	/**
	 * Return the ID of the direct debit authority used to pay the membership. Return null if no authority has been specified.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="payment_method_id"
	 */
	public Integer getDirectDebitAuthorityID() {
		return this.directDebitAuthorityID;
	}

	/**
	 * Determine if the membership has amount outstanding in the direct debit system. If membership is paid by receipting it will be ignored and return false.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public boolean hasDirectDebitDebt() throws RemoteException {
		try {
			double amountUnpaid = this.getAmountOutstanding(false, true);
			if (amountUnpaid > 0) {
				return true;
			} else {
				return false;
			}
		} catch (PaymentException ex) {
			return false;
		}
	}

	public MembershipTier getMembershipTier() throws RemoteException {
		MembershipTier membershipTier = null;
		LogUtil.debug(this.getClass(), "getMembershipTier product=" + this.getProduct());
		if (this.getProduct() != null && this.getProduct().isTierApplicable()) {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			membershipTier = membershipMgr.getMembershipTierByMembershipYears(this.getMembershipYears().getTotalYears());
		}
		return membershipTier;
	}

	public MembershipTier getRenewalMembershipTier() throws RemoteException {
		MembershipTier membershipTier = null;
		if (this.getProduct() != null && this.getProduct().isTierApplicable()) {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			membershipTier = membershipMgr.getMembershipTierByMembershipYears(this.getRenewalMembershipYears().getTotalYears());
		}
		return membershipTier;
	}

	/**
	 * Return the direct debit authority last used to pay the membership. If an authority is attached then it is payable by dd.
	 */
	public DirectDebitAuthority getDirectDebitAuthority() throws RemoteException {
		// try
		// {
		if (this.directDebitAuthority == null && this.directDebitAuthorityID != null) {
			PaymentMgr payMgr = PaymentEJBHelper.getPaymentMgr();
			try {
				this.directDebitAuthority = payMgr.getDirectDebitAuthority(this.directDebitAuthorityID);
			} catch (PaymentException ex) {
				// ignore
			}

		}
		// }
		// catch (PaymentException pe)
		// {
		// throw new SystemException(pe.getMessage(),pe);
		// }
		return this.directDebitAuthority;
	}

	/**
	 * Helper method to get the current direct debit schedule.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	@SuppressWarnings("unchecked")
	public DirectDebitSchedule getCurrentDirectDebitSchedule() throws RemoteException {
		if (this.currentDirectDebitSchedule == null) {
			DirectDebitAuthority auth = this.getDirectDebitAuthority();
			if (auth != null) {
				List<DirectDebitSchedule> schedules = (List<DirectDebitSchedule>) auth.getScheduleList();
				if (!schedules.isEmpty()) {
					this.currentDirectDebitSchedule = schedules.get(schedules.size() - 1);
				}
			}
		}
		return this.currentDirectDebitSchedule;
	}

	/**
	 * Return a reference to the product object that corresponds to the product code. Initialise it if this is the first time it has been referenced. Can't have multiple threads initialising the reference at the same time so must be synchronized.
	 */
	public synchronized ProductVO getProduct() throws RemoteException {
		LogUtil.debug(this.getClass(), "productCode=" + this.productCode);
		if (this.productCode != null && this.product == null /*
															 * && this.isNotHistory ()
															 */) {
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			this.product = refMgr.getProduct(this.productCode);
			if (this.product == null) {
				throw new SystemException("Failed to get membership productVO '" + this.productCode + "'. It does not exist!");
			}
			LogUtil.debug(this.getClass(), "got product=" + this.product);
		}
		return this.product;
	}

	private String inceptionSalesBranch;

	/**
	 * Return the product that is current now. If the product effective date is in the future and the commence date is in the past then we need to look in the membership history to find the product that is effective now.
	 */
	public String getCurrentProductCode() throws RemoteException {

		DateTime effectiveDate = new DateTime(); // The date from which to
		// determine if the product is
		// current

		if (this.currentProductCode == null // Have not already got the current
				// product
				&& this.productEffectiveDate != null // The product effective
				// date exists
				&& this.productEffectiveDate.afterDay(effectiveDate) // The
				// product
				// is not
				// yet
				// effective
				&& this.commenceDate != null // The membership commence date
				// exists
				&& this.commenceDate.onOrBeforeDay(effectiveDate)) // The
		// membership
		// has
		// commenced
		{
			// The product is not current as of the effective date.
			// Look in the history to find a product that is current.
			Collection transList = getTransactionList();
			if (transList != null && !transList.isEmpty()) {
				MembershipTransactionVO memTransVO;
				MembershipHistory memHistory;
				Iterator transListIterator = transList.iterator();
				while (this.currentProductCode == null && transListIterator.hasNext()) {
					memTransVO = (MembershipTransactionVO) transListIterator.next();
					try {
						memHistory = memTransVO.getMembershipHistory();
					} catch (RemoteException re) {
						// Some error occured getting the history so ignore it
						memHistory = null;
					}
					if (memHistory != null) {
						if (effectiveDate.onOrAfterDay(memHistory.getProductEffectiveDate())) {
							this.currentProductCode = memHistory.getProductCode();
							this.currentProductEffectiveDate = memHistory.getProductEffectiveDate();
						}
					}
				}
			}
		}

		// If we didn't manage to find a product in the history then use the one
		// set.
		if (this.currentProductCode == null) {
			this.currentProductCode = this.productCode;
			this.currentProductEffectiveDate = this.productEffectiveDate;
		}
		return this.currentProductCode;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="product_date"
	 */
	public DateTime getProductEffectiveDate() {
		return this.productEffectiveDate;
	}

	public DateTime getCurrentProductEffectiveDate() throws RemoteException {
		// causes the current product effective date to be set.
		// String curProdCode = getCurrentProductCode();
		return this.currentProductEffectiveDate;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="next_product_code"
	 */
	public String getNextProductCode() {
		return this.nextProductCode;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="join_date"
	 */
	public DateTime getJoinDate() {
		return this.joinDate;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="commence_date"
	 */
	public DateTime getCommenceDate() {
		return this.commenceDate;
	}

	/**
	 * Return the expiry date with the discount period added to it. This will effectively return the same as the base expiry date unless a transaction is being processed which includes a period of free membership.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="expiry_date"
	 * 
	 */
	public DateTime getExpiryDate() {
		DateTime expDate = null;
		if (this.expiryDate != null && this.discountPeriod != null) {
			expDate = this.expiryDate.add(this.discountPeriod);
		} else {
			expDate = this.expiryDate;

		}
		return expDate;
	}

	/**
	 * Return the Expiry date --without-- the discount period added to it. This will return the same as the getExpiryDate() method unless a transaction is being processed which includes discounts with discount periods applied to the transaction fees.
	 */
	public DateTime getBaseExpiryDate() {
		return this.expiryDate;
	}

	/**
	 * Return the specified term (Interval) that the membership has been renewed for. Copy the interval as it is not immutable. Return null if no membership term is specified and if an error occurs copying the term.
	 * 
	 * @hibernate.property
	 * @hibernate.column name="membership_term"
	 * 
	 */
	public Interval getMembershipTerm() {
		Interval term = null;
		if (this.membershipTerm != null) {
			try {
				term = new Interval(this.membershipTerm);
			} catch (Exception e) {
				// Return a null if an error occurs
			}
		}
		return term;
	}

	/**
	 * Return the actual term of membership by subtracting the commence date from the expiry date. Return null if the term could not be determined.
	 */
	public Interval getActualMembershipTerm() {
		Interval term = null;
		if (this.commenceDate != null && this.expiryDate != null) {
			try {
				term = new Interval(this.commenceDate, this.expiryDate);
			} catch (Exception e) { // Ignore errors
			}
		}
		return term;
	}

	public ClientNote getLastClientNote() throws RemoteException {
		ClientNote clientNote = null;
		if (this.membershipID != null) {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			clientNote = clientMgr.getLastClientNote(this.clientNumber);
		}
		return clientNote;
	}

	/**
	 * The membership years are calculated as the number of whole years between the join date and the current date
	 * 
	 * @return int
	 */
	public Interval getMembershipYears() {
		// Initialise a date to the current system date.
		DateTime effectiveDate = new DateTime();
		return getMembershipYears(effectiveDate);
	}

	private Interval getRenewalMembershipYears() {
		// Renewal anniversary is the current expiry date.
		return getMembershipYears(this.getExpiryDate());
	}

	/**
	 * The membership years are calculated as the number of years between the join date and the specified date. A part year is counted as one year. If the join date is after the specified date then the number of membership years is zero. Returns 0 if the years could not be determined because either the join date or the effective date are null.
	 * 
	 * @return int The number of membership years.
	 */
	public Interval getMembershipYears(DateTime effectiveDate) {
		Interval years = null;
		LogUtil.debug(this.getClass(), "joinDate=" + joinDate);
		LogUtil.debug(this.getClass(), "effectiveDate=" + effectiveDate);
		try {
			if (this.joinDate != null && effectiveDate != null && this.joinDate.onOrBeforeDay(effectiveDate)) {
				years = new Interval(this.joinDate, effectiveDate);
			} else {
				years = new Interval();
			}
		} catch (Exception e) {
			years = new Interval();
		}

		LogUtil.debug(this.getClass(), "years=" + years.getTotalYears());

		return years;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="credit_amount"
	 */
	public double getCreditAmount() {
		return this.creditAmount;
	}

	/**
	 * @return Interval
	 */
	public Interval getCreditPeriod() {
		return this.creditPeriod;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="affiliated_club_code"
	 */
	public String getAffiliatedClubCode() {
		return this.affiliatedClubCode;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="affiliated_club_id"
	 */
	public String getAffiliatedClubId() {
		return this.affiliatedClubId;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="affiliated_club_expiry_date"
	 */
	public DateTime getAffiliatedClubExpiryDate() {
		LogUtil.debug(this.getClass(), "getAff = " + affiliatedClubExpiryDate);
		return this.affiliatedClubExpiryDate;
	}

	/**
	 * Is membership automatically renewable? Does it have an authority attached?
	 * 
	 * @return status
	 * @throws RemoteException
	 */
	public boolean isAutoRenewable() throws RemoteException {
		DirectDebitAuthority ddAuthority = this.getDirectDebitAuthority();
		if (ddAuthority != null) {
			return true;
		} else {
			return false;
		}
	}

	public BigDecimal getMembershipValue(boolean includeGST) throws RemoteException {
		return getMembershipValue(includeGST, false);
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="membership_value"
	 */
	public BigDecimal getMembershipValue() {
		return this.membershipValue;
	}

	/**
	 * Return the un-rounded membership value. If the includeGST flag is set then the gst will be calculated at the current rate and included in the returned amount.
	 */
	public BigDecimal getMembershipValue(boolean includeGST, boolean multiplyTerms) throws RemoteException {
		// LogUtil.log(this.getClass(),"this.membershipValue"+this.membershipValue);
		BigDecimal memValue = null;
		if (this.membershipValue != null) {
			memValue = new BigDecimal(this.membershipValue.doubleValue());
		} else {
			memValue = new BigDecimal(0.0);
			// LogUtil.log(this.getClass(),"memValue"+memValue);
		}

		if ((memValue.compareTo(new BigDecimal(0)) > 0) && multiplyTerms) {
			int membershipTermYears = this.getMembershipTerm().getTotalYears();
			memValue = memValue.multiply(new BigDecimal(membershipTermYears));
		}

		if ((memValue.compareTo(new BigDecimal(0)) > 0) && includeGST) {
			try {
				CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
				BigDecimal gstRate = new BigDecimal(commonMgr.getCurrentTaxRate().getGstRate());
				BigDecimal gst = gstRate.multiply(memValue);
				memValue = gst.add(memValue);
				LogUtil.debug(this.getClass(), "memValue with GST=" + memValue);

			} catch (RemoteException e) {
				throw new SystemException("Error getting GST rate : " + e.getMessage(), e);
			}
		}
		return memValue;
	}

	/**
	 * Has a status that can potentially generate unearned value.
	 */
	private boolean hasUnearnedValue() throws RemoteException {
		boolean hasUnearnedValue = true;
		String statusString = getStatus();

		// If the membership has been undone then it has no remaining value.
		if (MembershipVO.STATUS_UNDONE.equals(statusString) || MembershipVO.STATUS_CANCELLED.equals(statusString) || MembershipVO.STATUS_ONHOLD.equals(statusString)) {
			hasUnearnedValue = false;
		}
		return hasUnearnedValue;
	}

	/**
	 * Returns the dollar value of remaining membership that has been paid. This is the difference between the paid ratio and the used ratio multiplied by the value. If the value is negative, it returns zero.
	 * 
	 * Note: This should represent what occurs in paymentmgrbean createCancelTypePostings
	 */
	public UnearnedValue getPaidRemainingMembershipValue() throws RemoteException {
		MethodCounter.addToCounter("MembershipVO", "getPaidRemainingMembershipValue");
		UnearnedValue unearnedValue = new UnearnedValue();
		BigDecimal paidRemainingValue = new BigDecimal(0);
		DateTime transactionDate = DateUtil.clearTime(new DateTime());
		MembershipTransactionVO mtVO = null;
		MembershipTransactionVO prevMtVO = null;
		MembershipHistory memHistory = null;
		// daily rate of membership
		BigDecimal dailyRate = null;

		StringBuffer valueDescription = new StringBuffer();

		LogUtil.debug(this.getClass(), "getPaidRemainingMembershipValue = " + this.clientNumber);

		// has a status that can still return value
		boolean hasUnearnedValue = hasUnearnedValue();
		LogUtil.debug(this.getClass(), "     has unearned  value = " + hasUnearnedValue);
		// does it have unearned value?
		if (!hasUnearnedValue) {
			return unearnedValue;
		}

		// if the membership is not current return zero
		if (expiryDate.onOrBeforeDay(transactionDate)) {
			return unearnedValue;
		}
		int daysRemaining = 0;

		// As the membership
		// value may have also changed, the old membership needs to be restored
		// from
		// history to get the true value to credit.

		// get list of transactions that are still "active"
		ArrayList txList = (ArrayList) this.getCurrentTransactionList();

		BigDecimal memTxValue = null;
		if (txList != null && txList.size() > 0) {

			Iterator it = null;
			it = txList.listIterator();
			// iterate through the transactions
			while (it.hasNext()) {
				mtVO = (MembershipTransactionVO) it.next();

				if (DateUtil.clearTime(mtVO.getEndDate()).after(DateUtil.clearTime(this.expiryDate))) {
					continue;
				}
				// look for history
				if (!mtVO.isHistoryAvailable()) {
					// don't process it/add it
					valueDescription.append("Unable to retrieve history for transaction '" + mtVO.getTransactionID() + "' the value will not be added.|");

					continue;
				} else {
					LogUtil.debug(this.getClass(), "his1");
					memHistory = mtVO.getMembershipHistory();
					LogUtil.debug(this.getClass(), "his2");
				}

				LogUtil.debug(this.getClass(), "mtVO" + mtVO);

				valueDescription.append("Total value from '" + mtVO.getTransactionTypeCode() + "' transaction with ID '" + mtVO.getTransactionID() + "'" + " = ");

				// get mem value at a point in time including GST * terms
				memTxValue = memHistory.getMembershipValue(true, true);

				valueDescription.append(CurrencyUtil.formatDollarValue(memTxValue) + ".|");

				valueDescription.append("Number of terms (years) = " + memHistory.getMembershipTerm().getTotalYears() + ".|");

				LogUtil.log(this.getClass(), "memTxValue" + memTxValue);

				// get amount os from transaction
				AmountOutstanding amountOs = mtVO.getUnearnedAmountOutstanding();
				BigDecimal unearnedRatio = null;

				// maximum days in the membership period
				Interval maximumRange = mtVO.getMaximumPotentialRange();
				valueDescription.append("Maximum range = " + maximumRange.getTotalDays() + " day(s).|");

				LogUtil.debug(this.getClass(), "maximumRange = " + maximumRange.getTotalDays());

				Interval effectiveRange = null;
				if (amountOs.isFullyUnpaid()) {
					// full range of transaction
					effectiveRange = mtVO.getEffectiveRange();
				} else {
					// from transaction to end date
					effectiveRange = mtVO.getEffectiveRange(transactionDate);
				}

				unearnedRatio = MembershipHelper.getUnearnedRatio(effectiveRange.getTotalDays(), maximumRange.getTotalDays());

				valueDescription.append("Unearned ratio = " + unearnedRatio + ".|");

				LogUtil.debug(this.getClass(), "\neffectiveRange = " + effectiveRange.getTotalDays() + "\nmaximumRange = " + maximumRange.getTotalDays() + "\nunearnedRatio = " + unearnedRatio);

				valueDescription.append("\nEffective range = " + effectiveRange.getTotalDays() + " day(s).|");

				dailyRate = memTxValue.divide(new BigDecimal(maximumRange.getTotalDays()), CommonConstants.MAX_PRECISION, BigDecimal.ROUND_HALF_UP);

				// pro rata
				memTxValue = unearnedRatio.multiply(memTxValue);

				valueDescription.append("\nProrated value = " + CurrencyUtil.formatDollarValue(memTxValue) + ".|");

				valueDescription.append("\nDaily rate = " + dailyRate + " per day.|");
				// add the remaining membership value
				paidRemainingValue = paidRemainingValue.add(memTxValue);

				LogUtil.debug(this.getClass(), "paidRemainingValue=" + paidRemainingValue);

				// if fully unpaid then deduct discounts and adjustments
				if (amountOs.isFullyUnpaid()) {
					// don't include
					BigDecimal valueFeeDiscountTotal = new BigDecimal(mtVO.getValueFeeDiscountTotal());
					valueDescription.append("\nDiscount total = " + CurrencyUtil.formatDollarValue(valueFeeDiscountTotal.doubleValue()) + ".|");
					LogUtil.debug(this.getClass(), "fully unpaid = " + valueFeeDiscountTotal);

					// subtract discounts
					paidRemainingValue = paidRemainingValue.subtract(valueFeeDiscountTotal);

					BigDecimal adjustmentTotal = mtVO.getAdjustmentTotal();
					valueDescription.append("\nAdjustment total = " + CurrencyUtil.formatDollarValue(adjustmentTotal.doubleValue()) + ".|");
					LogUtil.debug(this.getClass(), "fully unpaid = " + adjustmentTotal);

					// subtract adjustments
					paidRemainingValue = paidRemainingValue.subtract(adjustmentTotal);
				}
				// if there is an amount outstanding then flag that the amount
				// should be cleared as
				// it has been used to calculate the credit.
				if (amountOs.getAmountOutstanding().doubleValue() > 0) {

					LogUtil.debug(this.getClass(), "amountOs");

					valueDescription.append("\nAmount outstanding = " + amountOs.formatForDisplay() + ".|");

					// Subtract any outstanding amount in payment system from
					// the credit.
					// In transaction complete the debt will have to be cleared
					// in the
					// payment system.
					// eg. $50.00 - $100.00 = -$50.00
					paidRemainingValue = paidRemainingValue.subtract(amountOs.getAmountOutstanding());

					LogUtil.debug(this.getClass(), valueDescription.toString());

					// should never be less than 0
					if (paidRemainingValue.doubleValue() <= 0) {
						// the client can't owe more than what is asked for.
						paidRemainingValue = new BigDecimal(0);
						daysRemaining += 0; // ???
					} else {
						// calc days remaining as:
						// membership value divided by number of days in the
						// period - GST Inc
						// paidRemaining (after amounts outstanding) removed
						// divided by daily rate.
						// round up for the member's benefit.
						BigDecimal paidDays = paidRemainingValue.divide(dailyRate, CommonConstants.MAX_PRECISION, BigDecimal.ROUND_HALF_UP);
						valueDescription.append("\nDaily rate = " + dailyRate + ".|");
						daysRemaining += paidDays.intValue();
					}

					// We have used the amount outstanding to reduce the credit
					// so flag
					// that it has adjusted the credit.
					this.setOutstandingReducedCredit(true);

					valueDescription.append("\nUnearned value after outstanding = " + CurrencyUtil.formatDollarValue(paidRemainingValue) + ".|");

				} else {
					daysRemaining += effectiveRange.getTotalDays();
				}

				valueDescription.append("\nDays remaining = " + daysRemaining + ".");

				// determine days remaining
				LogUtil.debug(this.getClass(), "daysRemaining" + daysRemaining);
				// set previous transaction for next loop
				prevMtVO = mtVO;
			}
		}

		// round it before storing
		paidRemainingValue.setScale(2, BigDecimal.ROUND_HALF_UP);

		LogUtil.debug(this.getClass(), "end of getPaidrem");

		unearnedValue.setUnearnedAmount(paidRemainingValue);
		unearnedValue.setDaysRemaining(daysRemaining);
		unearnedValue.setDescription(valueDescription.toString());

		return unearnedValue;
	} // getPaidRemainingMembershipValueForOneYear

	/**
	 * Return a copy of the list of membership cards, as MembershipCardVO, issued to the membership. Initialise the list if this is the first call. Will not load the list of membership cards if no membership ID has been set or if the VO is operating in history mode or if the load parameter is noit set to true.
	 */
	public Collection getMembershipCardList(boolean load) throws RemoteException {
		LogUtil.debug(this.getClass(), "load=" + load);
		if ((this.membershipID != null && this.membershipCardList == null && isNotHistory()) || (this.membershipID != null && load)) {
			LogUtil.debug(this.getClass(), "getMembershipCardList is null");
			initialiseMembershipCardList();
		}
		if (this.membershipCardList == null) {
			LogUtil.debug(this.getClass(), "membershipCardList is null");
			return null;
		} else {
			return new ArrayList(this.membershipCardList);
		}
	}

	/**
	 * Can't have multiple threads initialising the list at the same time so must be synchronized.
	 */
	private synchronized void initialiseMembershipCardList() throws RemoteException {
		LogUtil.debug(this.getClass(), "initialiseMembershipCardList");
		MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
		Collection cardList = memMgr.getMembershipCardList(this.membershipID);
		LogUtil.debug(this.getClass(), "initialiseMembershipCardList 1");
		if (cardList != null) {
			this.membershipCardList = new Vector(cardList);
			LogUtil.debug(this.getClass(), "initialiseMembershipCardList2 " + this.membershipCardList);
		}
	}

	/**
	 * Return the list of documents, as MembershipDocumentVO, created for the membership. Initialise the list if this is the first call.
	 */
	public Collection getDocumentList(boolean load) throws RemoteException {
		LogUtil.debug(this.getClass(), "load=" + load);
		if ((this.membershipID != null && this.documentList == null && this.isNotHistory()) || (this.membershipID != null && load)) {
			LogUtil.debug(this.getClass(), "getDocumentList is null");
			initialiseDocumentList();
		}
		if (this.documentList == null) {
			LogUtil.debug(this.getClass(), "documentList is null");
			return null;
		} else {
			LogUtil.debug(this.getClass(), "docs" + this.documentList.size());
			return new Vector(this.documentList);
		}
	}

	public void setMembershipID(Integer membershipID) {
		this.membershipID = membershipID;
	}

	/**
	 * Can't have multiple threads initialising the list at the same time so must be synchronized.
	 */
	private synchronized void initialiseDocumentList() throws RemoteException {
		MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
		Collection docList = memMgr.getDocumentList(this.membershipID);
		if (docList != null) {
			this.documentList = new Vector(docList);
			LogUtil.debug(this.getClass(), "initialiseDocumentList=" + this.documentList);
		}
	}

	/**
	 * Return the a copy of the list of product options, as MembershipProductOption, that are attached to the membership. Initialise the list if this is the first call. A membership ID must have been set before the list can be initialised. Don't force a refresh of the list if it has already been initialised..
	 */
	public ArrayList getProductOptionList() throws RemoteException {
		return getProductOptionList(false);
	}

	/**
	 * Return a copy of the list of product options, as MembershipProductOption, that are attached to the membership. Initialise the list if this is the first call. A membership ID must have been set before the list can be initialised. Refresh the list if the force fetch flag is true. Can't have multiple threads initialising the list at the same time so must be synchronized.
	 */
	public synchronized ArrayList getProductOptionList(boolean load) throws RemoteException {
		if ((this.membershipID != null && this.productOptionList == null) || (this.membershipID != null && load)) {
			// Don't fetch product options if the membership VO is operating in
			// history mode.
			if (isNotHistory()) {
				MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
				Collection list = memMgr.getProductOptionList(this.membershipID);
				if (list != null) {
					if (list instanceof Vector) {
						this.productOptionList = (Vector) list;
					} else {
						this.productOptionList = new Vector(list);
					}
				}
			}

			// Initialise the list to an empty list so that an attempt
			// to fetch it is not done again (unless forced to).
			if (this.productOptionList == null) {
				this.productOptionList = new Vector();
			}
		}
		if (this.productOptionList == null) {
			return null;
		} else {
			return new ArrayList(this.productOptionList);
		}
	}

	/**
	 * Return a copy of the list of recurring discounts, as MembershipDiscount, attached to the membership. Initialise the list if this is the first call. Will always return an initialised list even if there are no recurring discounts. Can't have multiple threads initialising the list at the same time so must be synchronized.
	 */
	public synchronized ArrayList getDiscountList() throws RemoteException {
		if (this.membershipID != null && this.discountList == null && this.isNotHistory()) {
			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
			Collection list = memMgr.getDiscountList(this.membershipID);
			if (list != null) {
				if (list instanceof Vector) {
					this.discountList = (Vector) list;
				} else {
					this.discountList = new Vector(list);
				}
			} else {
				this.discountList = new Vector(); // No discounts so itialise to
				// an empty list so the fetch
				// is not done again.
			}
		}
		if (this.discountList == null) {
			return new ArrayList();
		} else {
			return new ArrayList(this.discountList);
		}
	}

	public MembershipGroupVO getMembershipGroup() throws RemoteException {
		return getMembershipGroup(false);
	}

	public void resetMembershipGroup() {
		this.membershipGroupVO = null;
	}

	/**
	 * Return the membership group that the membership belongs to. Returns null if the membership does not belong to a membership group. Calling this method is only valid once the membership and the membership group details have been created.
	 */
	public MembershipGroupVO getMembershipGroup(boolean load) throws RemoteException {
		MethodCounter.addToCounter("MembershipVO", "getMembershipGroup");
		LogUtil.debug(this.getClass(), "getMembershipGroup membershipID=" + this.membershipID);
		LogUtil.debug(this.getClass(), "getMembershipGroup membershipGroupVO=" + this.membershipGroupVO);
		LogUtil.debug(this.getClass(), "getMembershipGroup load=" + load);
		if (this.membershipGroupVO == null || (this.membershipGroupVO != null && load)) {
			LogUtil.debug(this.getClass(), "group vo is null");
			if (this.membershipID == null) {
				LogUtil.debug(this.getClass(), "memid is null");
				this.membershipGroupVO = new MembershipGroupVO(); // leave empty
			} else {
				MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
				LogUtil.debug(this.getClass(), "xxx membershipGroupVO " + this.membershipGroupVO);
				this.membershipGroupVO = memMgr.getMembershipGroupVO(this.membershipID);
				LogUtil.debug(this.getClass(), "xxx membershipGroupVO " + this.membershipGroupVO);
				if (this.membershipGroupVO == null) {
					LogUtil.debug(this.getClass(), "membershipGroupVO is still null");
					this.membershipGroupVO = new MembershipGroupVO(); // leave
					// empty
				}
			}
		}
		MethodCounter.addToCounter("MembershipVO", "membershipGroupVO" + membershipGroupVO.toString());
		LogUtil.debug(this.getClass(), "membershipGroupVO xy " + membershipGroupVO);
		// Return null if the membership group is empty.
		if (this.membershipGroupVO.getMembershipGroupDetailList() == null) {
			return null;
		} else {
			return this.membershipGroupVO;
		}
	}

	private Collection getEffectiveTransactionList() throws RemoteException {
		if (this.membershipID != null) {
			return MembershipEJBHelper.getMembershipMgr().getCurrentTransactionList(this.membershipID);
		} else {
			return null;
		}
	}

	/**
	 * Cache the current transaction list
	 * 
	 * @throws RemoteException
	 * @return Collection
	 */
	private Collection currentTransactionList;

	/**
	 * Get transaction list for transactions that have a membership effective date in the future. These represent the transactions that have full membership terms.
	 */
	public Collection getCurrentTransactionList() throws RemoteException {
		MethodCounter.addToCounter("MembershipVO", "getCurrentTransactionList");
		LogUtil.debug(this.getClass(), "getCurrentTransactionList start " + this.clientNumber);

		if (currentTransactionList == null) {
			currentTransactionList = new ArrayList();

			LogUtil.debug(this.getClass(), "getCurrentTransactionList 1");
			// current date
			DateTime transactionDate = new DateTime().getDateOnly();

			MembershipTransactionVO txVO = null;
			MembershipTransactionVO prevTxVO = null;

			// ArrayList txList = (ArrayList)this.getTransactionList();
			Collection effectiveTxList = getEffectiveTransactionList();
			LogUtil.debug(this.getClass(), "effectiveTxList=" + effectiveTxList);
			LogUtil.debug(this.getClass(), "getCurrentTransactionList 1a" + effectiveTxList);
			DateTime effectiveDate = null;
			DateTime endDate = null;
			PayableItemVO piVO = null;
			// last transaction affecting value for this membership
			boolean lastTransaction = false;
			// add the transaction?
			boolean addTransaction = false;

			// LogUtil.debug(this.getClass(),"A");

			if (effectiveTxList != null && effectiveTxList.size() > 0) {
				// sort in reverse transaction date order - default

				MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE, true);
				Collections.sort((List) effectiveTxList, mtc);

				Iterator txIt = effectiveTxList.iterator();
				// for each transaction
				while (txIt.hasNext() && !lastTransaction) {
					// reset flags/variables
					addTransaction = false;

					effectiveDate = null;
					endDate = null;

					txVO = (MembershipTransactionVO) txIt.next();
					if (txVO == null) {
						throw new RemoteException("Transaction may not be null.");
					}
					LogUtil.debug(this.getClass(), "txVO = " + txVO);
					// if transaction undone then skip it
					if (txVO.isUndone()) {
						LogUtil.debug(this.getClass(), "Transaction " + txVO.getTransactionID() + " has been undone.");
						continue;
					}
					// get the payable item attached
					try {
						LogUtil.debug(this.getClass(), "getCurrentTransactionList 1b");
						piVO = txVO.getPayableItemForPA();
						LogUtil.debug(this.getClass(), "get the pi" + piVO);
					} catch (RemoteException re) {
						LogUtil.debug(this.getClass(), "Unable to get the payable item for transaction " + txVO.getTransactionID() + ". " + re.getMessage());
						continue;
					}
					String paymentMethod = "";
					if (piVO != null) // else it is null
					{
						LogUtil.debug(this.getClass(), "got pi");
						PaymentMethod pMethod = piVO.getPaymentMethod();
						paymentMethod = pMethod == null ? "" : pMethod.getDescription();
					}

					LogUtil.debug(this.getClass(), txVO != null ? txVO.toString() : "txVO is null.");

					// get the effective date and end date from the transaction.
					effectiveDate = txVO.getEffectiveDate();
					endDate = txVO.getEndDate();

					// if effective date or end date is null then we will need
					// to get it from the payable item
					// as it is historical data
					if (effectiveDate == null || endDate == null) {
						if (piVO != null) {
							// should not be very common
							LogUtil.debug(this.getClass(), "Retrieved data from payable item for transaction '" + txVO.getTransactionID() + "'.");
							effectiveDate = new DateTime(piVO.getStartDate());
							endDate = new DateTime(piVO.getEndDate());

							// set read only to false so we can update
							// the effective date and end date if they are not
							// on the
							// transaction. should flag this as dirty!
							txVO.setReadOnly(false);
							txVO.setEffectiveDate(effectiveDate);
							txVO.setEndDate(endDate);
							txVO.setReadOnly(true);
						} else {
							// invalid start and end dates
							LogUtil.debug(this.getClass(), "Effective date or end date is null.");

							// ignore and stop - exception?
							addTransaction = false;
							lastTransaction = true;
							continue;
						}
					}
					// if superseding
					if (txVO.isSuperseding(paymentMethod)) {
						LogUtil.debug(this.getClass(), txVO.getTransactionID() + " is superseding");
						// if cancel or hold then finish processing as the
						// credit has been
						// processed previously.
						if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(txVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(txVO.getTransactionTypeCode())) {
							addTransaction = false;// if hold has an associated
							// fee this will mean that it
							// is overlooked.
							lastTransaction = true;
						} else if (prevTxVO != null && MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(prevTxVO.getTransactionTypeCode())) {
							LogUtil.debug(this.getClass(), txVO.getTransactionID() + " is superseding");

							// if downgrade then it is not superseding
							if (!isHigherRiskProductChange(prevTxVO, txVO)) {
								// if downgrade then don't add the transaction
								// as it is effectively
								// a flag but keep processing.
								addTransaction = false;
								lastTransaction = false;
							} else {
								// upgrade
								addTransaction = true;
								lastTransaction = true;
							}
						} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(txVO.getTransactionTypeCode())) {
							addTransaction = true;
							lastTransaction = true;
						} else {
							LogUtil.debug(this.getClass(), "in else." + txVO.getValueFeeTotal());
							// change group
							// if fee with unearned account attached. ie.
							// value-adding transactions
							/** //TODO fix for change group */

							if (txVO.getValueFeeTotal() > 0) {
								// generally dd
								addTransaction = true;
								lastTransaction = true;
							} else {
								addTransaction = false;
								lastTransaction = false;
							}
						}
					}
					// not superseding
					else // renew
					{

						// removes,etc!!!
						if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(txVO.getTransactionTypeCode())) {
							addTransaction = true;
							lastTransaction = false;
						}
						// ignore remove,card req,change group pa,edit,other
						// unimportant transactions in terms
						// of membership value.
					}

					// active as at transaction date
					if (!txVO.isActive(transactionDate)) {
						LogUtil.debug(this.getClass(), "is not active");
						addTransaction = false;
						lastTransaction = true;
					}
					LogUtil.debug(this.getClass(), "addTransaction=" + addTransaction);
					LogUtil.debug(this.getClass(), "lastTransaction=" + lastTransaction);

					// add the transaction
					if (addTransaction) {
						LogUtil.debug(this.getClass(), "add tx.");
						currentTransactionList.add(txVO);
					}
					// set the prev TxVO for comparison to the next
					prevTxVO = txVO;
				}
				LogUtil.debug(this.getClass(), "currentTransactionList = " + (currentTransactionList != null ? currentTransactionList.size() : 0));
			}
			LogUtil.debug(this.getClass(), "getCurrentTransactionList 2");
		}
		LogUtil.debug(this.getClass(), "getCurrentTransactionList end" + currentTransactionList);
		return currentTransactionList;
	}

	/**
	 * Does the previous transaction compared to a previous transaction indicate an upgrade?
	 * 
	 * @param prevTxVO MembershipTransactionVO
	 * @param txVO MembershipTransactionVO
	 * @throws RemoteException
	 * @return boolean
	 */
	public boolean isHigherRiskProductChange(MembershipTransactionVO prevTxVO, MembershipTransactionVO txVO) throws RemoteException {
		boolean higherRiskProduct = false;
		LogUtil.debug(this.getClass(), "tx" + (txVO == null ? "" : txVO.toString()));
		LogUtil.debug(this.getClass(), "prevTxVO" + (prevTxVO == null ? "" : prevTxVO.toString()));
		ProductVO prevProduct = prevTxVO.getProduct();
		LogUtil.debug(this.getClass(), "prevProduct" + prevProduct);
		higherRiskProduct = prevProduct.compareTo(txVO.getProduct()) > 0;
		return higherRiskProduct;
	}

	/**
	 * Get a direct debit schedule that is payable. If the membership is paid by receipting it will return null. If the membership is not the prime addressee it will return null. If there is no schedules payable it will return null.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public Collection getPayableSchedules(boolean includeWaiting) throws RemoteException {
		ArrayList payableScheduleList = new ArrayList();
		Collection payableItemList = null;
		DirectDebitSchedule ddSchedule = null;
		LogUtil.debug(this.getClass(), "getPayableSchedule0" + this.clientNumber);
		// should only be present if the PA
		payableItemList = this.getCurrentPayableItemList();
		LogUtil.debug(this.getClass(), "getPayableSchedule1");
		if (payableItemList != null) {
			PayableItemVO payableItem = null;
			PaymentMethod payMethod = null;
			Iterator payableIt = payableItemList.iterator();

			// int ddCounter = 0;
			// should only be one!!!
			while (payableIt.hasNext()) {
				LogUtil.debug(this.getClass(), "getPayableSchedule2");
				payableItem = (PayableItemVO) payableIt.next();
				LogUtil.debug(this.getClass(), "getPayableSchedule3");
				// is there a join fee?
				payMethod = payableItem.getPaymentMethod();
				if (payMethod != null && payMethod.isDirectDebitPaymentMethod()) {
					LogUtil.debug(this.getClass(), "getPayableSchedule4 ");
					ddSchedule = (DirectDebitSchedule) payMethod;
					LogUtil.debug(this.getClass(), "getPayableSchedule5 " + ddSchedule);
					if (!ddSchedule.isPaid() || // excludes renewal versions
							// also
							(includeWaiting && ddSchedule.containsAmountWaiting())) // include
					// waiting
					// as
					// payable
					{
						// add to list of payable schedules
						payableScheduleList.add(ddSchedule);
					}
				}
			}
		}
		LogUtil.debug(this.getClass(), "getPayableSchedule5");
		return payableScheduleList;
	}

	public Collection getPayableSchedules() throws RemoteException {
		return getPayableSchedules(false);
	}

	/**
	 * Store the old payable item list that this membership is directly responsible for.
	 */
	private Collection oldPayableItemList;

	/**
	 * Get the list of current payable items for this membership.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public Collection getCurrentPayableItemList() throws RemoteException {
		LogUtil.debug(this.getClass(), "getCurrentPayableItemList0");
		if (this.oldPayableItemList == null) {
			LogUtil.debug(this.getClass(), "getCurrentPayableItemList1");
			Collection payableItemList = getCurrentPayableItemList(false);
			this.oldPayableItemList = new ArrayList();
			// check if null
			if (payableItemList != null) {
				LogUtil.debug(this.getClass(), "getCurrentPayableItemList2");
				this.oldPayableItemList.addAll(payableItemList);
			}
		}
		return this.oldPayableItemList;
	}

	/**
	 * Get a list of current payable items attached to this membership. //TODO move to the paymentmgrbean to get a list of payable items given a list of transactions
	 * 
	 * @param incPA boolean uses transaction group to identify the PA of the transaction to obtain payable items that include this membership.
	 * @throws RemoteException
	 */
	public Collection getCurrentPayableItemList(boolean incPA) throws RemoteException {
		Collection payableItemList = null;
		DateTime now = new DateTime().getDateOnly();
		PayableItemVO payableItemVO = null;
		MembershipTransactionVO memTxVO = null;

		LogUtil.debug(this.getClass(), "getCurrentPayableItemListA");

		// get the list of transactions.
		ArrayList txList = (ArrayList) this.getCurrentTransactionList();

		LogUtil.debug(this.getClass(), "getCurrentPayableItemListB" + txList);

		if (txList != null && txList.size() > 0) {
			// LogUtil.log(this.getClass(),"txs "+txList.size());
			// boolean superseded = false;
			payableItemList = new ArrayList();

			// reverse them in chronological order
			MembershipTransactionComparator mtc = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_TRANSACTION_DATE, true);
			Collections.sort(txList, mtc);

			Iterator txIt = txList.iterator();
			while (txIt.hasNext()/* && !superseded */) {
				memTxVO = (MembershipTransactionVO) txIt.next();
				LogUtil.debug(this.getClass(), "getCurrentPayableItemListC1 " + memTxVO.getTransactionID());

				if (incPA) {
					LogUtil.debug(this.getClass(), "getCurrentPayableItemListD");
					// get the payable item for the group PA
					payableItemVO = memTxVO.getPayableItemForPA(); // includes
					// individuals.
				} else {
					LogUtil.debug(this.getClass(), "getCurrentPayableItemListE");
					// get the payable item
					payableItemVO = memTxVO.getPayableItem();
				}
				// LogUtil.log(this.getClass(),"is one here"+payableItemVO);

				// is there a payable item
				if (payableItemVO != null) {
					LogUtil.debug(this.getClass(), "getCurrentPayableItemListF");

					LogUtil.debug(this.getClass(), "getCurrentPayableItemListG" + payableItemVO.getPayableItemID());
					payableItemList.add(payableItemVO);
				}
			}
		}
		return payableItemList;
	}

	/**
	 * Return the list of transactions, as MembershipTransactionVO, that have been performed on the membership. Initialise the list if this is the first call. Can't have multiple threads initialising the list at the same time so must be synchronized.
	 */
	public synchronized Collection getTransactionList() throws RemoteException {
		MethodCounter.addToCounter("MembershipVO", "getTransactionList");
		LogUtil.debug(this.getClass(), "getTransactionList1" + this.isNotHistory() + " " + this.membershipID + " " + this.transactionList);
		if (this.membershipID != null && this.transactionList == null && this.isNotHistory()) {
			LogUtil.debug(this.getClass(), "getTransactionList2");
			MembershipMgrLocal memMgr = MembershipEJBHelper.getMembershipMgrLocal();
			this.transactionList = memMgr.getTransactionList(this.membershipID);

			LogUtil.debug(this.getClass(), "getTransactionList3");
		}
		if (this.transactionList == null) {
			return null;
		} else {
			return new ArrayList(this.transactionList);
		}
	}

	/**
	 * Get a transaction given the transaction ID
	 * 
	 * @param transactionID
	 * @return
	 * @throws RemoteException
	 */
	public MembershipTransactionVO getMembershipTransaction(Integer transactionID) throws RemoteException {
		MembershipTransactionVO memTxVO = null;
		Collection transactionList = this.getTransactionList();
		if (transactionList != null && transactionList.size() > 0) {
			Iterator txIt = transactionList.iterator();
			while (txIt.hasNext()) {
				memTxVO = (MembershipTransactionVO) txIt.next();
				if (memTxVO.getTransactionID().equals(transactionID)) {
					return memTxVO;
				}
			}
		}
		return null;
	}

	/**
	 * Get latest transaction by date
	 */
	public MembershipTransactionVO getLastTransaction() throws RemoteException {
		return MembershipEJBHelper.getMembershipMgr().getLastMembershipTransaction(this.membershipID);
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="vehicle_count"
	 */
	public int getVehicleCount() {
		return this.vehicleCount;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="vehicle_expiry_type"
	 */
	public String getVehicleExpiryType() {
		return this.vehicleExpiryType;
	}

	/**
	 * Return a copy of the list of vehicles, as MembershipVehicle, attached to the membership. Initialise the list if this is the first call. Can't have multiple threads initialising the list at the same time so must be synchronized.
	 */
	public synchronized ArrayList getVehicleList() throws RemoteException {
		if (this.membershipID != null && this.vehicleList == null && this.isNotHistory()) {
			MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
			Collection list = memMgr.getVehicleList(this.membershipID);
			if (list != null) {
				if (list instanceof Vector) {
					this.vehicleList = (Vector) list;
				} else {
					this.vehicleList = new Vector(list);
				}
			}
		}
		if (this.vehicleList == null) {
			return null;
		} else {
			return new ArrayList(this.vehicleList);
		}
	}

	/**
	 * Return true if the membership has an existing card request that has not been issued yet.
	 */
	public boolean hasCardRequest() throws RemoteException {
		boolean hasRequest = false;
		Collection cardList = getMembershipCardList(true);
		if (cardList != null) {
			MembershipCardVO cardVO = null;
			Iterator cardListIterator = cardList.iterator();
			while (!hasRequest && cardListIterator.hasNext()) {
				cardVO = (MembershipCardVO) cardListIterator.next();
				hasRequest = !cardVO.isIssued();
			}
		}
		return hasRequest;
	}

	/**
	 * Return true if the membership has the specified product option attached to it. Return false otherwise.
	 */
	public boolean hasProductOption(String prodOptionCode) throws RemoteException {
		boolean hasOption = false;
		Collection optionList = getProductOptionList();
		if (optionList != null) {
			MembershipProductOption option = new MembershipProductOption(prodOptionCode);
			hasOption = optionList.contains(option);
		}
		return hasOption;
	}

	/**
	 * Return true if the membership has the specified recurring discount attached to it.
	 */
	public boolean hasRecurringDiscount(String discTypeCode) throws RemoteException {
		boolean hasDisc = false;
		ArrayList discList = getDiscountList();
		if (discList != null) {
			Iterator it = discList.iterator();
			while (!hasDisc && it.hasNext()) {
				MembershipDiscount membershipDiscount = (MembershipDiscount) it.next();
				hasDisc = membershipDiscount.getDiscountTypeCode().equals(discTypeCode);
			}
		}
		return hasDisc;
	}

	/**
	 * Remove the discount type from the list of recurring discounts attached to the membership.
	 */
	public void removeRecurringDiscount(String discTypeCode) throws RemoteException {
		checkUpdateAllowed();
		// Initialise the discount list if required. This returns a copy
		// of the list which we can't use.
		// ArrayList list = getDiscountList();

		// Now work with the list directly.
		if (this.discountList != null) {
			boolean found = false;
			ListIterator it = this.discountList.listIterator();
			while (!found && it.hasNext()) {
				MembershipDiscount membershipDiscount = (MembershipDiscount) it.next();
				if (membershipDiscount.getDiscountTypeCode().equals(discTypeCode)) {
					it.remove();
					found = true;
				}
			}
		}
	}

	/**
	 * Return the period of free membership that is to be applied.
	 */
	public Interval getDiscountPeriod() {
		return this.discountPeriod;
	}

	/**
	 * Search through the list of optional product benefits attached to the membership and return true if the specified benefit is contained in the list. Return false otherwise.
	 */
	public boolean membershipHasProductOption(String benefitTypeCode) {
		boolean found = false;
		if (benefitTypeCode != null) {
			if (this.productOptionList != null) {
				Iterator it = this.productOptionList.iterator();
				while (it.hasNext() && !found) {
					MembershipProductOption option = (MembershipProductOption) it.next();
					if (benefitTypeCode.equals(option.getProductBenefitTypeCode())) {
						found = true;
					}
				}
			}
		}
		return found;
	}

	public void setPrimeAddressee(boolean isAddressee) {
		this.groupPrimeAddressee = new Boolean(isAddressee);
	}

	/**
	 * Return true if this membership is the prime addressee of a group. Return false if the membership does not have a membership ID. If so we must be creating a new membership and so we can't determine who is the prime addressee. Return true if the membership does not belong to a group. They are considered to be the prime addressee of them selves. Return false otherwise. Calling this method is only valid once the membership and membership group details have been saved.
	 * 
	 */
	public boolean isPrimeAddressee() throws RemoteException {
		if (this.groupPrimeAddressee != null)
			return this.groupPrimeAddressee.booleanValue();
		MethodCounter.addToCounter("MembershipVO", "isPrimeAddressee");

		// {
		// MembershipMgr memMgr = MembershipEJBHelper.getMembershipMgr();
		// primeAddressee = memMgr.isPrimeAddressee(this.membershipID);
		// }
		boolean primeAddressee = false;
		LogUtil.debug(this.getClass(), "isPrimeAddressee");
		if (this.membershipID != null) {
			LogUtil.debug(this.getClass(), "isPrimeAddressee " + membershipID);
			MembershipGroupVO memGroupVO = getMembershipGroup();
			if (memGroupVO != null && !memGroupVO.hasException()) {
				primeAddressee = memGroupVO.isPrimeAddressee(this.membershipID);
			} else {
				primeAddressee = true;// single
			}
		}
		return primeAddressee;
	}

	public boolean receivesEmailRenewals() throws RemoteException {
		ClientVO client = getClient();
		ClientMgrLocal clientMgrLocal = ClientEJBHelper.getClientMgrLocal();
		ClientSubscription sub = clientMgrLocal.getActiveClientSubscription(this.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL);
		LogUtil.debug(this.getClass(), "sub=" + sub);
		if (sub != null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean definedEmailRenewals() throws RemoteException {
		ClientVO client = getClient();
		ClientMgrLocal clientMgrLocal = ClientEJBHelper.getClientMgrLocal();
		ClientSubscription sub = clientMgrLocal.getClientSubscription(this.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL);
		LogUtil.debug(this.getClass(), "sub=" + sub);
		if (sub != null) {
			return true;
		} else {
			return false;
		}
	}

	public synchronized boolean isGroupMember() throws RemoteException {
		return isGroupMember(false);
	}

	/**
	 * Return true if this membership partakes in a membership group. - Add synchronized keyword.
	 */
	public synchronized boolean isGroupMember(boolean ignoreExceptions) throws RemoteException {
		MethodCounter.addToCounter("MembershipVO", "isGroupMember");
		LogUtil.debug(this.getClass(), "isGroupMember " + ignoreExceptions);
		MembershipGroupVO memGroupVO = getMembershipGroup();
		// LogUtil.debug(this.getClass(),"memGroupVO "+memGroupVO+" "+memGroupVO.hasException());

		if (memGroupVO != null) {
			if (memGroupVO.hasException()) {
				if (ignoreExceptions) {
					LogUtil.debug(this.getClass(), "isGroupMember xxx true");
					return true;
				} else {
					LogUtil.debug(this.getClass(), "isGroupMember xxx false");
					return false;
				}
			} else {
				return true;
			}
		} else {
			LogUtil.debug(this.getClass(), "isGroupMember end true");
			return false;
		}
	}

	public boolean isDirectDebit() {
		boolean directDebit = false;
		try {
			if (this.getDirectDebitAuthority() != null) {
				directDebit = true;
			}
		} catch (RemoteException e) {
			// ignore
		}
		return directDebit;

	}

	private Boolean checkRecAmount;

	/**
	 * Get the invalid reason for a transaction type
	 */
	public String getInvalidForTransactionReason(String transTypeCode) throws RemoteException {
		MethodCounter.addToCounter("MembershipVO", "getInvalidForTransactionReason");

		LogUtil.debug(this.getClass(), "getInvalidForTransactionReason transTypeCode = " + transTypeCode + ", " + this.clientNumber);

		String memStatus = getStatus();
		ClientVO clientVO = getClient();

		// Validate the client for the transaction.
		String reason = MembershipHelper.validateClientForTransaction(clientVO, transTypeCode, this);
		if (reason != null) {
			return reason;
		}

		// get the amount outstanding in receipting
		double recAmountUnpaid = 0;
		try {
			recAmountUnpaid = this.getAmountOutstanding(true, false);
			LogUtil.debug(this.getClass(), "recAmountUnpaid=" + recAmountUnpaid);
		} catch (DirectDebitFailedValidationException dde) {
			// This situation will be handled on the membership summary page
			LogUtil.warn(getClass(), "Client: " + clientVO.getClientNumber() + " has outstanding failed amounts in DD system.");
			return "Client: " + clientVO.getClientNumber() + " has outstanding failed amounts in DD system.";
		} catch (PaymentException pe) {
			throw new SystemException("Unable to get the total amount unpaid." + pe);
		}

		// check for payments in arrear and failed direct debits
		try {
			DirectDebitAuthority ddAuthority = this.getDirectDebitAuthority();
			if (ddAuthority != null) {
				String ddProblemReason = ddAuthority.getDirectDebitProblemReason();
				LogUtil.info(this.getClass(), "ddProblemReason=" + ddProblemReason);
				if (ddProblemReason != null && !ddProblemReason.equals("")
				// a rescheduled item will not cause problems so ignore
						&& !ddProblemReason.equals(DirectDebitAdapter.DD_STATUS_UNRESOLVED_PAYMENT)) {
					// payment in arrears and failed direct debits will stop ANY
					// transaction.
					return ddProblemReason;
				}
			}
		} catch (RemoteException ex1) {
			LogUtil.warn(this.getClass(), "Unable to determine direct debit problem." + ex1.getMessage());
		}

		// unresolved vouchers will stop ANY transaction
		FinanceVoucher creditDisposal = null;
		FinanceMgr financeMgr = PaymentEJBHelper.getFinanceMgr();
		Collection creditDisposals = financeMgr.getVouchersForReference(SourceSystem.MEMBERSHIP.getAbbreviation(), this.membershipID.toString());
		for (Iterator<FinanceVoucher> i = creditDisposals.iterator(); i.hasNext();) {
			creditDisposal = i.next();
			if (creditDisposal != null && creditDisposal.getStatus() == null) {
				return "There is an unprocessed voucher " + creditDisposal.getDisposalId() + " for this membership awaiting processing by " + creditDisposal.getApprovingUserId();
			}
		}

		ClientNote clientNote = null;
		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
		Collection<ClientNote> clientNoteList = clientMgr.getClientNoteList(clientNumber, SourceSystem.MEMBERSHIP.getAbbreviation());
		for (Iterator<ClientNote> i = clientNoteList.iterator(); i.hasNext();) {
			clientNote = i.next();
			if (clientNote.isBlockTransactions()) {
				return "Membership transactions are blocked. " + clientNote.getNoteText();
			}
		}

		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(transTypeCode)) {
			if (this.membershipID != null) {
				return "The membership has already been created.";
			}
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(transTypeCode)) {

			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}
			LogUtil.debug(this.getClass(), "memStatus=" + memStatus);
			// can only transfer if there is no membership or the membership is
			// not active
			if (MembershipVO.STATUS_ACTIVE.equals(memStatus) || MembershipVO.STATUS_INRENEWAL.equals(memStatus) || MembershipVO.STATUS_FUTURE.equals(memStatus)) {
				return "The membership is still active.";
			}

			if (isGroupMember()) {
				return "The membership cannot be transferred as the member is part of a group.";
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_EXPIRE_CREDIT.equals(transTypeCode)) {
			// no rules as this is an automatic process
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(transTypeCode)) {

			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}

			if (MembershipVO.STATUS_CANCELLED.equals(memStatus)) {
				return "The membership is cancelled.";
			}

			if (MembershipVO.STATUS_UNDONE.equals(memStatus)) {
				return "The membership can only be rejoined.";
			}

			if (MembershipVO.STATUS_EXPIRED.equals(memStatus)) {
				String postExpireRenewalPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.POST_EXPIRE_RENEWAL_PERIOD);
				Interval postExpireRenewalPeriod = null;
				try {
					postExpireRenewalPeriod = new Interval(postExpireRenewalPeriodSpec);
				} catch (Exception e) {
					throw new SystemException(e);
				}
				DateTime pastDate = new DateTime().subtract(postExpireRenewalPeriod);
				DateTime theExpiryDate = getExpiryDate();
				if (theExpiryDate.beforeDay(pastDate)) {
					StringBuffer dateMessage = new StringBuffer();
					if (postExpireRenewalPeriod != null) {
						dateMessage.append("The limit for renewal is ");
						dateMessage.append(postExpireRenewalPeriod.formatForDisplay(Interval.FORMAT_YEAR, Interval.FORMAT_MONTH));
						dateMessage.append(".");
					}
					if (pastDate != null) {
						dateMessage.append("The earliest renewal date for this membership is ");
						dateMessage.append(pastDate.formatShortDate());
						dateMessage.append(".");
					}
					dateMessage.append("");
					return MESSAGE_EXPIRED_TOO_LONG;
				}
			}

			if (isGroupMember()) {
				return "Renew group is the valid transaction.";
			}
			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			if (!this.getProduct().isRenewable()) {
				return "Unable to renew as product '" + this.getProductCode() + "' is not manually renewable.";
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(transTypeCode)) {
			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}

			if (MembershipVO.STATUS_ACTIVE.equals(memStatus) || MembershipVO.STATUS_INRENEWAL.equals(memStatus) || MembershipVO.STATUS_FUTURE.equals(memStatus)) {
				return "The membership is still active.";
			}

			if (MembershipVO.STATUS_ONHOLD.equals(memStatus)) {
				return "The membership is on hold.";
			}

			if (MembershipVO.STATUS_EXPIRED.equals(memStatus)) {
				if (this.isActive()) {
					return "The membership has not been expired for long enough.";
				}
			}

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			if (isGroupMember()) {
				return "Unable to do an individual rejoin on a group membership.";
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(transTypeCode)) {
			if (!this.getProduct().isUpgradable()) {
				return "This membership type cannot be upgraded. To proceed, cancel current membership then rejoin.";
			}

			if (MembershipVO.STATUS_CANCELLED.equals(memStatus)) {
				return "The membership has been cancelled.";
			}

			if (MembershipVO.STATUS_UNDONE.equals(memStatus)) {
				return "The membership can only be rejoined.";
			}

			if (MembershipVO.STATUS_ONHOLD.equals(memStatus)) {
				return "The membership has been put on hold.";
			}

			if (MembershipVO.STATUS_INRENEWAL.equals(memStatus)) {
				return "The membership is in renewal.";
			}

			if (MembershipVO.STATUS_EXPIRED.equals(memStatus)) {
				return "The membership has expired.";
			}

			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			if (isGroupMember() && !isPrimeAddressee()) {
				return "Only the prime addressee of a membership group can change the base product.";
			}

			return this.checkMaximumMembershipPeriod();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(transTypeCode)) {

			if (!this.getProduct().isUpgradable()) {
				return "This membership type cannot be upgraded. To proceed, cancel current membership then rejoin.";
			}

			if (MembershipVO.STATUS_CANCELLED.equals(memStatus)) {
				return "The membership has been cancelled.";
			}

			if (MembershipVO.STATUS_UNDONE.equals(memStatus)) {
				return "The membership can only be rejoined.";
			}

			if (MembershipVO.STATUS_ONHOLD.equals(memStatus)) {
				return "The membership has been put on hold.";
			}

			if (MembershipVO.STATUS_INRENEWAL.equals(memStatus)) {
				return "The membership is in renewal.";
			}

			// removed JH 05/03/2007
			// if(MembershipVO.STATUS_EXPIRED.equals(memStatus))
			// {
			// return "The membership has expired.";
			// }

			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			if (isGroupMember() && !isPrimeAddressee()) {
				return "Only the prime addressee of a membership group can change the product options.";
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(transTypeCode)) {

			// check that there are no dd payments that are waiting
			Collection directDebitSchedules = this.getPayableSchedules(true);
			if (directDebitSchedules != null) {
				DirectDebitSchedule ddSchedule = null;
				Iterator directDebitScheduleIt = directDebitSchedules.iterator();
				while (directDebitScheduleIt.hasNext()) {
					ddSchedule = (DirectDebitSchedule) directDebitScheduleIt.next();
					if (ddSchedule.containsAmountWaiting()) {
						return "The client has direct debit payments that are still waiting for the bank to process. No changes may be made.";
					}
				}
			}

			if (MembershipVO.STATUS_CANCELLED.equals(memStatus)) {
				return "The membership has already been cancelled.";
			}

			if (MembershipVO.STATUS_UNDONE.equals(memStatus)) {
				return "The membership can only be rejoined.";
			}

			if (this.isGroupMember()) {
				return "A member of a group cannot be directly cancelled.  You must first remove the member from the group, then cancel the membership.";
			}

			if (MembershipVO.STATUS_ONHOLD.equals(memStatus)) {
				// receipting method only for on-hold
				if (recAmountUnpaid > 0.0) {
					return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
				}
			}

			return this.checkMaximumMembershipPeriod();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP.equals(transTypeCode)) {
			if (MembershipVO.STATUS_UNDONE.equals(memStatus)) {
				return "The membership is in an undone state and may not be edited.";
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(transTypeCode)) {
			if (this.getProduct().isVehicleBased()) {
				return "This membership type cannot be placed on hold.";
			}
			// LogUtil.log(this.getClass(),">>>>>>>>>>>>>>>>>>checking on hold 1");
			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}
			// LogUtil.log(this.getClass(),">>>>>>>>>>>>>>>>>>checking on hold 2");
			if (MembershipVO.STATUS_CANCELLED.equals(memStatus)) {
				return "The membership has been cancelled.";
			}
			// LogUtil.log(this.getClass(),">>>>>>>>>>>>>>>>>>checking on hold 3");
			// Greg Hankin CR. 22/07/2003
			DateTime today = new DateTime();
			Interval expiredMonths = null;

			String maxExpired = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MAX_HOLD_EXPIRED);
			try {
				expiredMonths = new Interval(maxExpired);
			} catch (Exception e) {
				throw new RemoteException("Unable to construct interval to determine expiry months.", e);
			}
			DateTime maxHoldThreshold = today.subtract(expiredMonths);
			if (maxHoldThreshold.after(this.getExpiryDate())) {
				return "Membership is expired more than " + expiredMonths.formatForDisplay(Interval.FORMAT_MONTH, Interval.FORMAT_MONTH);
			}
			LogUtil.debug(this.getClass(), ">>>>>>>>>>>>>>>>>>checking on hold 4");
			// Membership does not have value and it is not already on hold
			// CR MEM-239
			// UnearnedValue unearnedValue =
			// this.getPaidRemainingMembershipValue();
			// LogUtil.debug(this.getClass(),"yyy "+memStatus+" "+unearnedValue.toString());
			// if (!MembershipVO.STATUS_ONHOLD.equals(memStatus) &&
			// unearnedValue.getTransactionCreditAmount().equals(new
			// BigDecimal(0)))
			// {
			// return "Membership does not have any unearned credit to store.";
			// }

			LogUtil.debug(this.getClass(), ">>>>>>>>>>>>>>>>>>checking on hold 5");

			// This is a valid transaction and should extend the expiry by the
			// hold period
			// but not affect credit or financials.
			// JH 25/06/2003
			// if (MembershipVO.STATUS_ONHOLD.equals(memStatus))
			// return "The membership is already on hold.";

			if (MembershipVO.STATUS_UNDONE.equals(memStatus)) {
				return "The membership has been undone and can only be rejoined.";
			}
			LogUtil.debug(this.getClass(), ">>>>>>>>>>>>>>>>>>checking on hold 6");
			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}
			LogUtil.debug(this.getClass(), ">>>>>>>>>>>>>>>>>>checking on hold 7");
			LogUtil.debug(this.getClass(), "getInvalid... Hold");
			if (this.isGroupMember()) {
				return "The member of a group cannot be put on hold.  You must first remove the member from the group then hold the membership.";
			}
			LogUtil.debug(this.getClass(), ">>>>>>>>>>>>>>>>>>checking on hold 8");
			return this.checkMaximumMembershipPeriod();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST.equals(transTypeCode)) {
			if (MembershipVO.STATUS_CANCELLED.equals(memStatus)) {
				return "The membership has been cancelled.";
			}

			if (MembershipVO.STATUS_EXPIRED.equals(memStatus)) {
				return "The membership has expired.";
			}

			if (MembershipVO.STATUS_ONHOLD.equals(memStatus)) {
				return "The membership is on hold.";
			}

			if (MembershipVO.STATUS_UNDONE.equals(memStatus)) {
				return "The membership can only be rejoined.";
			}

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(transTypeCode)) {

			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}

			// No longer invalid. TB 19/03/2003 see CR 19.
			// if (MembershipVO.STATUS_ONHOLD.equals(memStatus))
			// return "The membership is on hold.";

			if (!this.getMembershipProfile().isGroupable()) {
				return "The membership has a profile of '" + this.profileCode + "' and cannot be used when creating a group.";
			}

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			// Can't be already in an existing non-expired membership group.
			MembershipGroupVO memGroupVO = getMembershipGroup();
			if (memGroupVO != null) {
				if (memGroupVO.hasException()) {
					return "The membership belongs to an invalid membership group. " + memGroupVO.getMembershipGroupExceptionMessages();
				} else if (memGroupVO.isNotHistory()) {
					if (!memGroupVO.isMembershipGroupExpired()) {
						DateTime groupExpiryDate = memGroupVO.getMembershipGroupExpiryDate();
						DateTime today = new DateTime();
						if (today.onOrBeforeDay(groupExpiryDate)) {
							return "The membership already belongs to an active membership group.";
						}
					} else if (!this.isPrimeAddressee()) {
						return "The current group must be disbanded, created from the existing PA or the PA changed.";
					}
				}
			}

			return this.checkMaximumMembershipPeriod();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(transTypeCode)) {

			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}

			// No longer invalid. TB 19/03/2003 see CR 19.
			// if (MembershipVO.STATUS_ONHOLD.equals(memStatus))
			// return "The membership is on hold.";

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			if (!this.getMembershipProfile().isGroupable()) {
				return "The membership has a profile of '" + this.profileCode + "' and cannot be used when creating a group.";
			}

			MembershipGroupVO memGroupVO = getMembershipGroup();
			if (memGroupVO != null) {
				if (memGroupVO.hasException()) {
					return "The membership belongs to an invalid membership group. " + memGroupVO.getMembershipGroupExceptionMessages();
				}
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(transTypeCode)) {

			LogUtil.debug(this.getClass(), "this.isPrimeAddressee() " + this.isPrimeAddressee());
			LogUtil.debug(this.getClass(), "this.hasDirectDebitDebt() " + this.hasDirectDebitDebt());
			LogUtil.debug(this.getClass(), "this.isAutoRenewable() " + this.isAutoRenewable());
			LogUtil.debug(this.getClass(), "this.allowToLapse() " + this.allowToLapse);
			LogUtil.debug(this.getClass(), "this.getStatus()() " + this.getStatus());
			LogUtil.debug(this.getClass(), "this.getAuthority()() " + this.getDirectDebitAuthority());

			// can't renew a group containing access memberships
			if (!this.getProduct().isRenewable()) {
				return "Unable to renew a group containing '" + this.getProductCode() + "' members.";
			}

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			if (this.isAllowToLapse() != null && this.isAllowToLapse().booleanValue()) {
				return "The membership is to lapse when it expires.";
			}

			if (!this.getMembershipProfile().isGroupable()) {
				return "The membership has a profile of '" + this.profileCode + "' and cannot be used when creating a group.";
			}

			// No longer invalid. TB 19/03/2003 see CR 19.
			// if (MembershipVO.STATUS_ONHOLD.equals(memStatus))
			// return "The membership is on hold.";

			if (MembershipVO.STATUS_EXPIRED.equals(memStatus)) {
				String postExpireRenewalPeriodSpec = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.POST_EXPIRE_RENEWAL_PERIOD);
				Interval postExpireRenewalPeriod = null;
				try {
					postExpireRenewalPeriod = new Interval(postExpireRenewalPeriodSpec);
				} catch (ParseException pe) {
					throw new SystemException(pe);
				}
				DateTime pastDate = new DateTime().subtract(postExpireRenewalPeriod);
				if (getExpiryDate().beforeDay(pastDate)) {
					return MembershipVO.MESSAGE_EXPIRED_TOO_LONG;
				}
			}

			// if (!this.isGroupMember())
			// {
			// return "Renew group is only available for groups.";
			// }
			// if (!this.isPrimeAddressee())
			// {
			// return "Renew group is only available from the prime addressee.";
			// }

			MembershipGroupVO memGroupVO = getMembershipGroup();
			if (memGroupVO != null) {
				if (memGroupVO.hasException()) {
					return "The membership belongs to an invalid membership group. " + memGroupVO.getMembershipGroupExceptionMessages();
				}
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(transTypeCode)) {
			if (this.isPrimeAddressee()) {
				return "The prime addressee cannot be removed.  Appoint a new prime addressee and then remove the membership.";
			}
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REFUND.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_WRITE_OFF.equals(transTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER_CREDIT.equals(transTypeCode)) {

			if (this.isGroupMember()) {
				return "Cannot perform refund, write off or transfer credit on group members.";
			} else if (this.creditAmount <= 0) {
				return "There is no amount to dispose of.";
			}

		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(transTypeCode)) {

			MembershipGroupVO memGroupVO = getMembershipGroup();
			if (memGroupVO != null) {
				if (memGroupVO.hasException()) {
					return "The membership belongs to an invalid membership group. " + memGroupVO.getMembershipGroupExceptionMessages();
				}
			}

			if (recAmountUnpaid > 0) {
				return "There is an unpaid amount of " + CurrencyUtil.formatDollarValue(recAmountUnpaid) + " owing in the receipting system.";
			}

			if (!this.isGroupMember()) {
				return "Change group PA only applies to a group.";
			}

			if (!this.isPrimeAddressee()) {
				return "Change group PA may only be performed on the current prime addressee.";
			}

		} else {
			// We don't know what the transaction type is!!
			return "Unknown transaction type '" + transTypeCode + "'.";
		}
		return null;
	}

	/**
	 * Is the membership still active.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public boolean isActive() throws RemoteException {
		return MembershipHelper.isMembershipActive(getExpiryDate());
	}

	/**
	 * Examine the prime addressee's payable item list to determine the total amount payable.
	 * 
	 * Get the amount outstanding for each if the amount outstanding is greater than 0 then get the associated transaction.
	 * 
	 * note: this will only work for the current membership state. Will not work for historical memberships.
	 */
	public AmountOutstanding getIndividualAmountOutstanding() throws RemoteException {
		AmountOutstanding amountOs = new AmountOutstanding();
		MembershipTransactionVO memTrans = null;

		LogUtil.debug(this.getClass(), "in individual get amount outstanding for " + this.clientNumber);

		Collection currentTransactionList = this.getCurrentTransactionList();
		if (currentTransactionList != null && currentTransactionList.size() > 0) {
			BigDecimal amountOutstanding = null;
			int piCounter = 0;
			Iterator transIt = currentTransactionList.iterator();
			while (transIt.hasNext()) {
				memTrans = (MembershipTransactionVO) transIt.next();
				amountOs = memTrans.getUnearnedAmountOutstanding();
				amountOutstanding = amountOs.getAmountOutstanding();
				// get the amount outstanding
				if (amountOutstanding.compareTo(new BigDecimal(0)) > 0) {
					piCounter++;
					if (piCounter > 1) {
						throw new SystemException("There is more than one payable item that has an amount outstanding for client '" + this.clientNumber + "'");
					}
				}
			}
		}
		return amountOs;
	}

	/**
	 * Attribute holding dd and receipting information.
	 */
	private Hashtable amountOutstanding;

	/**
	 * Return the total amount that remains to be paid on payable items that this membership is directly responsible for.
	 * 
	 * @param includeReceipting Sum outstanding amounts on receipting items
	 * @param includeDirectDebit Sum outstanding amounts on direct debit items.
	 */
	public double getAmountOutstanding(boolean includeReceipting, boolean includeDirectDebit) throws RemoteException, PaymentException {
		MethodCounter.addToCounter("MembershipVO", "getAmountOutstanding");
		if (amountOutstanding == null) {
			amountOutstanding = new Hashtable();
		}
		BigDecimal amountUnpaid = new BigDecimal(0);

		if ((amountOutstanding.get(PaymentMethod.DIRECT_DEBIT) == null && includeDirectDebit) || (amountOutstanding.get(PaymentMethod.RECEIPTING) == null && includeReceipting)) {
			LogUtil.debug(this.getClass(), "getAmountOutstanding mem = " + this.clientNumber);
			LogUtil.debug(this.getClass(), "includeDirectDebit" + includeDirectDebit);
			LogUtil.debug(this.getClass(), "includeReceipting" + includeReceipting);
			// BigDecimal amountUnpaid = new BigDecimal(0);

			// get current payable item list
			Collection payableItemList = this.getCurrentPayableItemList();
			LogUtil.debug(this.getClass(), "pay size = " + (payableItemList == null ? "null" : "" + payableItemList.size()));
			if (payableItemList != null && payableItemList.size() > 0) {
				PayableItemVO payableItem;
				String paymentMethodDescription = null;
				Iterator payableItemListIterator = payableItemList.iterator();
				while (payableItemListIterator.hasNext()) {
					payableItem = (PayableItemVO) payableItemListIterator.next();
					LogUtil.debug(this.getClass(), "payableItem=" + payableItem.toString());
					if (payableItem.getAmountOutstanding() > 0) {
						LogUtil.debug(this.getClass(), "payableItem o/s = " + payableItem.getAmountOutstanding());
						paymentMethodDescription = payableItem.getPaymentMethodDescription();
						this.setAmountOutstandingForPaymentMethod(payableItem, paymentMethodDescription);
					}
				}
			}
		}

		// set the values if there are none set so that it doesn't intialise the
		// objects
		// again.

		if (amountOutstanding.get(PaymentMethod.DIRECT_DEBIT) == null) {
			amountOutstanding.put(PaymentMethod.DIRECT_DEBIT, new BigDecimal(0));
		}

		if (amountOutstanding.get(PaymentMethod.RECEIPTING) == null) {
			amountOutstanding.put(PaymentMethod.RECEIPTING, new BigDecimal(0));
		}

		BigDecimal tmpAmount = null;

		// get the stored values
		if (includeReceipting) {
			tmpAmount = (BigDecimal) amountOutstanding.get(PaymentMethod.RECEIPTING);
			if (tmpAmount != null) {
				// LogUtil.log(this.getClass(), "rectmpAmount " + tmpAmount);
				amountUnpaid = amountUnpaid.add(tmpAmount);
			}
		}
		if (includeDirectDebit) {
			tmpAmount = (BigDecimal) amountOutstanding.get(PaymentMethod.DIRECT_DEBIT);
			if (tmpAmount != null) {
				// LogUtil.log(this.getClass(), "ddtmpAmount " + tmpAmount);
				amountUnpaid = amountUnpaid.add(tmpAmount);
				LogUtil.debug(this.getClass(), "amountUnpaid " + amountUnpaid);
			}

		}

		LogUtil.debug(this.getClass(), "amount outstanding " + amountOutstanding);
		return amountUnpaid.doubleValue();
	}

	/**
	 * Populate the amount outstanding hashtable with key of payment method and value of total amount outstanding for that payment method.
	 * 
	 * @param payableItem
	 * @param paymentMethod
	 */
	private void setAmountOutstandingForPaymentMethod(PayableItemVO payableItem, String paymentMethod) throws ValidationException {
		BigDecimal amountUnpaid = (BigDecimal) amountOutstanding.get(paymentMethod);
		LogUtil.debug(this.getClass(), "amountUnpaid" + amountUnpaid);
		BigDecimal amountOS = new BigDecimal(payableItem.getAmountOutstanding());
		if (amountUnpaid == null) {
			amountUnpaid = new BigDecimal(0);
		}

		this.amountOutstanding.put(paymentMethod, amountUnpaid.add(amountOS));
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="allow_to_lapse"
	 */
	public Boolean isAllowToLapse() {
		return this.allowToLapse;
	}

	public boolean isExpired() throws RemoteException {
		return MembershipVO.STATUS_EXPIRED.equals(getStatus());
	}

	public boolean isPendingTierChange() {
		boolean pendingTierChange = false;
		try {
			if (!this.getMembershipTier().equals(this.getRenewalMembershipTier())) {
				pendingTierChange = true;
			}
		} catch (Exception ex) {
			LogUtil.warn(this.getClass(), "Unable to get tier" + ex);
		}
		return pendingTierChange;
	}

	public boolean isNewCardRequired() throws RemoteException {
		boolean newCardRequired = true;
		// If there are no cards then a new one needs to be requested.
		Collection cardList = getMembershipCardList(true);
		if (cardList != null && !cardList.isEmpty()) {
			// Look for a current card with the same product on it.
			MembershipCardVO memCardVO;
			DateTime cardExpiryDate;
			DateTime today = new DateTime();
			Iterator cardListIterator = cardList.iterator();
			while (newCardRequired && cardListIterator.hasNext()) {
				memCardVO = (MembershipCardVO) cardListIterator.next();

				// If the card does not have an expiry date then it is a request
				// for a new card and so we don't need to request another one.
				cardExpiryDate = memCardVO.getExpiryDate();
				if (cardExpiryDate == null) {
					newCardRequired = false;
				} else {
					// If the card is not expired and it is for the same product
					// as the memberships product then we don't need a new card.
					if (cardExpiryDate.afterDay(today) && this.productCode.equals(memCardVO.getProductCode())) {
						newCardRequired = false;
					}
				}
			}
		}
		return newCardRequired;
	}

	/**
	 * Find the oldest client in this group that isn't the prime addressee
	 * 
	 * @return MembershipVO
	 */
	public MembershipVO findOldestClientInGroup() throws RemoteException, ObjectNotFoundException {
		MethodCounter.addToCounter("MembershipVO", "findOldestClientInGroup");
		MembershipVO retMemVO = null;
		MembershipGroupVO memGroupVO = getMembershipGroup();
		if (memGroupVO != null && !memGroupVO.hasException()) {
			MembershipGroupDetailVO memGroupDetailVO = null;
			MembershipVO memVO = null;
			ClientVO oldestClient = null;
			ClientVO thisClient = null;

			Collection membershipGroupList = memGroupVO.getMembershipGroupDetailList();
			Iterator groupIterator = membershipGroupList.iterator();

			while (groupIterator.hasNext()) {
				memGroupDetailVO = (MembershipGroupDetailVO) groupIterator.next();
				memVO = (MembershipVO) memGroupDetailVO.getMembership();
				if (!memVO.isPrimeAddressee()) {
					thisClient = memVO.getClient();
					if (thisClient != null) {
						try {
							if (oldestClient == null || thisClient.getBirthDate().beforeDay(oldestClient.getBirthDate())) {
								retMemVO = memVO.copy();
								oldestClient = thisClient;
							}
						} catch (NullPointerException e) {
							if (oldestClient == null) {
								retMemVO = memVO.copy();
								oldestClient = thisClient;
							}
						}
					}
				}
			}
		} else if (memGroupVO != null && memGroupVO.hasException()) {
			Vector exceptionList = memGroupVO.getExceptionList();
			MembershipGroupException mge = (MembershipGroupException) exceptionList.get(0);
			throw mge;
		}
		return retMemVO;
	}

	/************************** setter methods ************************/

	public void setMembershipNumber(String membershipNumber) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.membershipNumber = membershipNumber;
	}

	/**
	 * Set the new membership type code. Make sure it is valid by initialising the reference to the membership type value object.
	 */
	public void setMembershipTypeCode(String memTypeCode) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (memTypeCode != null) {
			this.membershipTypeCode = memTypeCode;
			this.membershipType = null;

			MembershipTypeVO tmpVO = getMembershipType(); // This will
			// initialise the
			// reference to the
			// membership type VO

			if (tmpVO == null) {
				this.membershipTypeCode = null;
				throw new ValidationException("Invalid membership type code : " + memTypeCode);
			}
		} else {
			this.membershipTypeCode = null;
			this.membershipType = null;
		}
	}

	/**
	 * Set the membership type given a membership type value object. Set the membership type code as well.
	 */
	public void setMembershipType(MembershipTypeVO memType) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.membershipType = memType;
		if (this.membershipType != null) {
			this.membershipTypeCode = this.membershipType.getMembershipTypeCode();
		} else {
			this.membershipTypeCode = null;
		}
	}

	/**
	 * Reset the reference to the profile value object if the profile code has changed.
	 */
	public void setMembershipProfileCode(String profileCode) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (this.profileCode != null && !this.profileCode.equals(profileCode)) {
			this.profile = null;
		}
		this.profileCode = profileCode;
	}

	/**
	 * Set the profile for the membership. Also set the profile code. If the profile specifies an expiry day/month then change the memberships expiry date to match the profiles.
	 */
	public void setMembershipProfile(MembershipProfileVO profile) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.profile = profile;
		if (this.profile == null) {
			this.profileCode = null;
		} else {
			this.profileCode = this.profile.getProfileCode();
			// String expiryMonth = this.profile.getExpiryMonth();
			// if (expiryMonth != null && expiryMonth.length() > 0)
			// {
			// Construct a new expiry date
			// DateTime today = new DateTime();
			// int month = Integer.parseInt(expiryMonth); // The new month that
			// the membership expires in
			// int year = DateUtil.getYear(today); // The current year
			// int thisMonth = DateUtil.getMonthOfYear(today); // The current
			// month
			// If the month to expire in has already passed for this year
			// then increment the year
			// if (month < thisMonth)
			// year++;

			// Now construct the new date and set it as the expiry date.
			// Profiles that specify an expiry month expiry on the first of that
			// month.
			// GregorianCalendar gc = new GregorianCalendar(year,month,1);
			// this.expiryDate = new DateTime(gc.getTime());
			// }
		}
	}

	/**
	 * Set the client number. Convert the string to an integer.
	 */
	public void setClientNumber(String clientNumber) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		Integer theClientNumber = null;
		if (clientNumber != null && clientNumber.length() > 0) {
			try {
				theClientNumber = new Integer(clientNumber);
			} catch (Exception e) {
				throw new ValidationException("Invalid client number (" + clientNumber + "). It must be an integer.");
			}
		}
		setClientNumber(theClientNumber);
	}

	/**
	 * Set the client number. If an existing client is already referenced then make sure it is the same client as the one being set. If not then set the reference to null so that the getClient() method will fetch the reference to the new client.
	 */
	public void setClientNumber(Integer clientNumber) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (this.clientNumber != null && !this.clientNumber.equals(clientNumber)) {
			this.client = null;
		}
		this.clientNumber = clientNumber;
	}

	/**
	 * Set the client reference. Also set the client number.
	 */
	public void setClient(ClientVO theClient) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.client = theClient;
		if (this.client != null) {
			this.clientNumber = this.client.getClientNumber();
		} else {
			this.clientNumber = null;
		}
	}

	public void setDirectDebitAuthorityID(Integer ddAuthorityID) throws RemoteException {
		checkUpdateAllowed();
		this.directDebitAuthorityID = ddAuthorityID;
		this.directDebitAuthority = null;
	}

	/**
	 * Set the payment method to use when paying transactions on this membership.
	 */
	public void setDirectDebitAuthority(DirectDebitAuthority ddAuthority) throws RemoteException {
		checkUpdateAllowed();
		this.directDebitAuthority = ddAuthority;

		if (this.directDebitAuthority == null) {
			this.directDebitAuthorityID = null;
		} else {
			this.directDebitAuthorityID = this.directDebitAuthority.getDirectDebitAuthorityID();
		}
	}

	/**
	 * Set the product code that the membership is issued with. Set the product reference to null to force a refetch. Set the product effective date to today. Check that the product options are valid for the new product.
	 */
	public void setProductCode(String prodCode) throws ValidationException, RemoteException {
		checkUpdateAllowed();

		if (prodCode == null) {
			this.productCode = null;
			this.product = null;
			this.productEffectiveDate = null;
			this.productOptionList = null;
		} else {
			if (!prodCode.equals(this.productCode)) {
				this.productCode = prodCode;
				this.product = null;
				this.productEffectiveDate = DateUtil.clearTime(new DateTime());
				checkProductOptions();
			}
		}
	}

	/**
	 * Set the product that the membership has been issued with. Also set the product code and the membership value. Reset the product effective date to today. Remove any product options that are not valid for the new product.
	 */
	public void setProduct(ProductVO prod) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (prod != null) {
			// Only do the processing if the product has been changed
			if (!prod.getProductCode().equals(this.productCode)) {
				this.product = prod;
				this.productCode = prod.getProductCode();
				this.productEffectiveDate = DateUtil.clearTime(new DateTime());

				// Setting of the membership value is done in the
				// TransactionGroup
				// MembershipRefMgr refMgr =
				// MembershipEJBHelper.getMembershipRefMgr();
				// int groupSize = 1;
				// if (this.getMembershipGroup() != null)
				// groupSize = this.getMembershipGroup().getMemberCount();
				//
				// Collection feeList =
				// refMgr.findProductFee(prod.getProductCode(),
				// this.membershipTypeCode,
				// MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE,
				// groupSize, new DateTime());
				// Iterator feeListIterator = feeList.iterator();
				// double value = 0.0;
				// while (feeListIterator.hasNext())
				// {
				// FeeSpecificationVO feeSpec = (FeeSpecificationVO)
				// feeListIterator.next();
				// if (feeSpec.getEarnedAccountID() != null &&
				// feeSpec.getUnearnedAccount() != null)
				// value += feeSpec.getFeePerMember() +
				// feeSpec.getFeePerVehicle() * this.vehicleCount;
				// }
				// CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
				// value = CurrencyUtil.calculateGSTExclusiveAmount(value,
				// commonMgr.getCurrentTaxRate().getGstRate());
				// value = NumberUtil.roundDouble(value, 2);
				//
				// this.setMembershipValue(value);

				checkProductOptions();
			}
		} else {
			this.product = null;
			this.productCode = null;
			this.productEffectiveDate = null;
			this.productOptionList = null;
		}
	}

	/**
	 * Set the code of the next product to be issued to the membership.
	 */
	public void setNextProductCode(String nextProductCode) throws RemoteException {
		checkUpdateAllowed();
		this.nextProductCode = nextProductCode;
	}

	/**
	 * Set the product effective date. Clear any time components of the date so that it starts from 12am.
	 */
	public void setProductEffectiveDate(DateTime effectiveDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		LogUtil.debug(this.getClass(), "clientNumber=" + this.clientNumber);
		effectiveDate = DateUtil.clearTime(effectiveDate);
		LogUtil.debug(this.getClass(), "effectiveDate=" + effectiveDate);
		this.productEffectiveDate = effectiveDate;
	}

	/**
	 * Set the effective dates of all of the attached product options.
	 */
	public void setProductOptionEffectiveDates(DateTime effectiveDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		effectiveDate = DateUtil.clearTime(effectiveDate);
		Collection optionList = getProductOptionList();
		if (optionList != null && !optionList.isEmpty()) {
			MembershipProductOption option = null;
			Iterator optionListIterator = optionList.iterator();
			while (optionListIterator.hasNext()) {
				option = (MembershipProductOption) optionListIterator.next();
				option.setEffectiveDate(effectiveDate);
			}
		}
	}

	/**
	 * Set the join date from the date specified as a string. Parse the string using the allowed list of date formats.
	 */
	public void setJoinDate(String joinDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		DateTime theDate = null;
		if (joinDate != null && joinDate.length() > 0) {
			try {
				Date aDate = DateUtil.parseDate(joinDate);
				if (aDate != null) {
					theDate = new DateTime(aDate);
				}
			} catch (ParseException pe) {
				throw new ValidationException("The join date is not a valid date : " + pe.getMessage());
			}
		}
		setJoinDate(theDate);
	}

	/**
	 * Set the join date of the membership. Clear any time components of the date so that it starts from 12am.
	 */
	public void setJoinDate(DateTime jDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		// Clear the time components.
		jDate = DateUtil.clearTime(jDate);
		LogUtil.debug(this.getClass(), "setting join date" + jDate);
		this.joinDate = jDate;
	}

	/**
	 * Set the commence date of the membership. Convert the string to a date and call the actual implementation method.
	 */
	public void setCommenceDate(String commenceDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		DateTime theDate = null;
		if (commenceDate != null && commenceDate.length() > 0) {
			try {
				Date aDate = DateUtil.parseDate(commenceDate);
				if (aDate != null) {
					theDate = new DateTime(aDate);
				}
			} catch (ParseException pe) {
				throw new ValidationException("The commence date is not a valid date : " + pe.getMessage());
			}
		}
		setCommenceDate(theDate);
	}

	/**
	 * Set the commence date. Clear any time components of the date so that it starts from 12am. It must be on or after the join date. The expiry date must be after the commence date.
	 */
	public void setCommenceDate(DateTime comDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		comDate = DateUtil.clearTime(comDate);

		// Can't have an expiry date without a commence date.
		if (comDate == null) {
			this.commenceDate = null;
			this.expiryDate = null;
		} else {
			// Must have a join date first
			if (this.joinDate == null) {
				throw new SystemException("The join date must be set before the commence date is set.");
			}

			if (comDate.before(this.joinDate)) {
				LogUtil.warn(this.getClass(), "The commence date (" + comDate.formatShortDate() + ") must be after the join date (" + this.joinDate.formatShortDate() + ") for member " + this.membershipID + ". Setting commence date to join date.");
				this.commenceDate = this.joinDate;
			}

			// Finally we can set the new commence date.
			this.commenceDate = comDate;

			// The expiry date cannot be before the commence date.
			if (this.expiryDate != null && this.expiryDate.before(this.commenceDate)) {
				this.expiryDate = this.commenceDate;
			}
		}
	}

	/**
	 * Convert the string date specification into a DateTime object.
	 */
	public void setExpiryDate(String expDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		DateTime theDate = null;
		if (expDate != null && expDate.length() > 0) {
			try {
				Date aDate = DateUtil.parseDate(expDate);
				if (aDate != null) {
					theDate = new DateTime(aDate);
				}
			} catch (ParseException pe) {
				throw new ValidationException("The expiry date is not a valid date : " + pe.getMessage());
			}
		}
		setExpiryDate(theDate);
	}

	/**
	 * Re-cast the Date into a DateTime
	 */
	public void setExpiryDate(Date expDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		DateTime dt = new DateTime(expDate);
		setExpiryDate(dt);
	}

	/**
	 * Set the membership expiry date. The expiry date cannot be before the commence date.
	 */
	public void setExpiryDate(DateTime expDate) throws ValidationException, RemoteException {
		checkUpdateAllowed();

		// Not interested in the time component.
		expDate = DateUtil.clearTime(expDate);

		if (expDate != null && this.commenceDate != null && expDate.before(this.commenceDate)) {
			throw new ValidationException("The expiry date (" + expDate.formatShortDate() + ") cannot be before the commence date (" + this.commenceDate.formatShortDate() + ")");
		}

		this.expiryDate = expDate;
	}

	/**
	 * Set the term of the membership. The term cannot be greater than the maximum allowed term. //TODO validation for setmembershipTerm(). Fetch the maximum membership term from a system parameter
	 */
	public void setMembershipTerm(Interval term) throws ValidationException, RemoteException {
		// LogUtil.log(this.getClass(),"term="+term);
		Interval tmpTerm = null;
		if (term != null) {
			try {
				tmpTerm = new Interval(term);
			} catch (Exception e) {
				throw new ValidationException("The membership term '" + term + "' is not valid.");
			}
		}
		this.membershipTerm = tmpTerm;
	}

	/**
	 * Set the affiliated club expiry date.
	 * 
	 * @param affiliatedClubExpiryDate DateTime
	 * @throws ValidationException
	 * @throws RemoteException
	 */
	public void setAffiliatedClubExpiryDate(DateTime affiliatedClubExpiryDate) throws ValidationException, RemoteException {
		// validate if not loading or not history
		// if(!this.isLoading() && !this.isHistory() &&
		// affiliatedClubExpiryDate != null)
		// {
		// if(!MembershipHelper.isMembershipActive(affiliatedClubExpiryDate))
		// {
		// throw new ValidationException(
		// "Expiry date for affiliated club has been expired for too long! "
		// + affiliatedClubExpiryDate);
		// }
		// }
		checkUpdateAllowed();
		LogUtil.debug(this.getClass(), "affiliated club expiry date=" + affiliatedClubExpiryDate);
		this.affiliatedClubExpiryDate = affiliatedClubExpiryDate;
		LogUtil.debug(this.getClass(), "after=" + this.affiliatedClubExpiryDate);
	}

	/**
	 * //TODO validation for setAffiliatedClubNumber()
	 */
	public void setAffiliatedClubId(String affiliatedClubId) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.affiliatedClubId = affiliatedClubId;
	}

	/**
	 * //TODO validation for setAffiliatedClub()
	 */
	public void setAffiliatedClubCode(String affiliatedClubCode) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.affiliatedClubCode = affiliatedClubCode;
	}

	// public void setAffiliatedClubCreditAmount(BigDecimal
	// affiliatedClubCreditAmount)
	// throws
	// ValidationException, RemoteException
	// {
	// this.affiliatedClubCreditAmount = affiliatedClubCreditAmount; //was
	// rounded
	// this.setModified(true);
	// }

	/**
	 * Set the vehicle count. Convert the string to an int and call the actuall implementation method.
	 */
	public void setVehicleCount(String vCount) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		int theCount = 0;
		if (vCount != null && vCount.length() > 0) {
			theCount = new Integer(vCount).intValue();
		}
		setVehicleCount(theCount);
	}

	/**
	 * Set the vehicle count. Must not be less than zero.
	 */
	public void setVehicleCount(int vCount) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (vCount < 0) {
			throw new ValidationException("The vehicle count must be greater than or equal to zero.");
		}

		this.vehicleCount = vCount;
	}

	/**
	 * //TODO validation for setVehicleExpiryType()
	 */
	public void setVehicleExpiryType(String vehicleExpiryType) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.vehicleExpiryType = vehicleExpiryType;
	}

	/**
	 * Set the status of the membership. Derived statuses are converted to there base status equivalents.
	 */
	public void setStatus(String memStatus) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (MembershipVO.STATUS_ACTIVE.equals(memStatus) || MembershipVO.STATUS_FUTURE.equals(memStatus) || MembershipVO.STATUS_EXPIRED.equals(memStatus) || MembershipVO.STATUS_INRENEWAL.equals(memStatus) || MembershipVO.STATUS_CANCELLED.equals(memStatus) || MembershipVO.STATUS_ONHOLD.equals(memStatus) || MembershipVO.STATUS_UNDONE.equals(memStatus)) {
			// Convert derived statuses to their base status value
			if (MembershipVO.STATUS_FUTURE.equals(memStatus) || MembershipVO.STATUS_INRENEWAL.equals(memStatus) || MembershipVO.STATUS_EXPIRED.equals(memStatus)) {
				memStatus = MembershipVO.STATUS_ACTIVE;
			}
			this.baseStatus = memStatus;
			this.virtualStatus = null;
		} else {
			throw new ValidationException("Invalid membership status (" + memStatus + ")");
		}
	}

	public void setBaseStatus(String memStatus) {
		this.baseStatus = memStatus;
	}

	public void setAllowToLapse(Boolean lapseFlag) throws RemoteException {
		checkUpdateAllowed();
		// if loading and a group member this will be an infinite loop unless it
		// is specifically searched for.
		if (!this.isLoading() && !this.isHistory() && lapseFlag != null && lapseFlag.booleanValue() && isGroupMember()) {
			throw new SystemException("A group member may not be allowed to lapse. The group member should be removed first.");
		}
		this.allowToLapse = lapseFlag;
	}

	/**
	 * Set the discounts that recur for this membership.
	 */
	public void setDiscountList(ArrayList dtList) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (dtList == null) {
			this.discountList = null;
		} else {
			this.discountList = new Vector(dtList);
		}
	}

	/**
	 * Set the discount period. This is a period of free membership that is added onto the expiry date. The discount period is not stored as part of the membership so the modified flag does not need to be set.
	 */
	public void setDiscountPeriod(Interval discPeriod) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.discountPeriod = discPeriod;
	}

	/**
	 * Return the amount previously stored with the specified key.
	 * 
	 * @param key Description of the Parameter
	 * @return The creditAmount value
	 */
	public TransactionCredit getTransactionCredit(String key) {
		// LogUtil.debug(this.getClass(),"getTransactionCredit="+this.creditMap.toString());
		return (TransactionCredit) this.creditMap.get(key);
	}

	/**
	 * Store an amount which can be retrieved with the specified key. E.g. unearned credits, loyalty credits and rewards credits may be held on the membership while processing a transaction.
	 * 
	 * @param key The new creditAmount value
	 * @param obj The new creditAmount value
	 */
	public void setTransactionCredit(String key, TransactionCredit transCredit) throws SystemException {
		LogUtil.debug(this.getClass(), "adding to credit map=" + key + (transCredit == null ? " is null" : ""));
		// if (TransactionCredit.UNEARNED_CREDIT.equals(key)
		// || TransactionCredit.QUOTE_CREDIT.equals(key))
		// {
		if (transCredit != null) {
			LogUtil.debug(this.getClass(), "adding credit " + key);
			this.creditMap.put(key, transCredit);
		} else {
			LogUtil.debug(this.getClass(), "removing credit " + key);
			this.creditMap.remove(key);
		}
		// }
		// else
		// {
		// throw new SystemException("Failed to set transaction credit. '" + key
		// +
		// "' is not a valid key.");
		// }
	}

	/**
	 * Set the credit amount on the membership. Hard coded to disallow amounts greater than 200.
	 */
	public void setCreditAmount(double creditAmount) throws ValidationException, RemoteException {
		// checkUpdateAllowed();
		int creditLimit = 500;
		if (creditAmount > creditLimit) {
			throw new ValidationException("The credit amount (" + CurrencyUtil.formatDollarValue(creditAmount) + ") being set on the membership for client " + this.clientNumber + " appears to be too high! (Credit limit is " + CurrencyUtil.formatDollarValue(creditLimit) + ")");
		}
		this.creditAmount = NumberUtil.roundDouble(creditAmount, 2);
	}

	/**
	 * Set the credit amount on the membership.
	 */
	// public void setSuspenseAmount(double suspenseAmount) throws
	// RemoteException
	// {
	// checkUpdateAllowed();
	// this.suspenseAmount = suspenseAmount;
	// this.setModified(true);
	// }

	/**
	 * //TODO validation for setCreditPeriod()
	 */
	public void setCreditPeriod(Interval creditPeriod) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		this.creditPeriod = creditPeriod;
	}

	public void setVehicleList(ArrayList vehicleList) throws ValidationException, RemoteException {
		checkUpdateAllowed();
		if (vehicleList == null) {
			this.vehicleList = null;
		} else {
			this.vehicleList = new Vector(vehicleList);
		}

	}

	public void setMembershipValue(BigDecimal memValue) throws RemoteException {
		// LogUtil.log(this.getClass(),":"+memValue);
		try {
			checkUpdateAllowed();
			if (memValue != null) {
				this.membershipValue = memValue.setScale(4, BigDecimal.ROUND_HALF_UP);
			}
		} catch (Exception e) {
			throw new RemoteException("Error setting membership value: " + e);
		}
	}

	/************************** Utility methods ******************************/

	/**
	 * Clear the list of product options attached to the membership. Initialises the list if required an then clears it to an empty list.
	 */
	public void clearProductOptionList() throws RemoteException {
		checkUpdateAllowed();
		// Fetch the existing list of options
		// Collection optionList = getProductOptionList();

		// If that didn't initialise the list then initialise it.
		if (this.productOptionList == null) {
			this.productOptionList = new Vector();
		}

		// Now clear it
		this.productOptionList.clear();
	}

	/**
	 * Add the discount type as a recurring discount for the membership. The discount type must be flagged as a recurring type of discount. Don't do the add if the discount type already exists.
	 */
	public void addRecurringDiscount(DiscountTypeVO discTypeVO) throws RemoteException {
		checkUpdateAllowed();

		if (discTypeVO == null) {
			throw new RemoteException("Trying to add a null discount type as a recurring discount on membership '" + this.getMembershipNumber() + "'");
		}

		if (!discTypeVO.isRecurringDiscount()) {
			throw new RemoteException("Trying to add a non recurring discount type '" + discTypeVO.getDiscountTypeCode() + "' as a recurring discount on membership '" + this.getMembershipNumber() + "'");
		}

		if (!hasRecurringDiscount(discTypeVO.getDiscountTypeCode())) {
			if (this.discountList == null) {
				this.discountList = new Vector();
			}

			this.discountList.add(new MembershipDiscount(discTypeVO));
		}
	}

	/**
	 * Add the specified product option to the product option list. Don't do the add if the option already exists. Set the effective date of the option to today.
	 */
	public void addProductOption(MembershipProductOption prodOption) throws RemoteException {
		checkUpdateAllowed();
		if (prodOption == null) {
			throw new RemoteException("The membership product option cannot be null.");
		}

		// Check for an existing product option.
		// This will also initialise the list if required.
		if (!hasProductOption(prodOption.getProductBenefitTypeCode())) {
			// If that didn't initialise the list then initialise it.
			if (this.productOptionList == null) {
				this.productOptionList = new Vector();
			}

			// Set the effective date of the product option to today
			prodOption.setEffectiveDate(DateUtil.clearTime(new DateTime()));

			// Now add the new product option
			this.productOptionList.add(prodOption);
		}
	}

	/**
	 * Remove the specified product option from the product option list.
	 */
	public void removeProductOption(String productBenefitTypeCode) throws RemoteException {
		checkUpdateAllowed();

		// Make sure the product option list is initialise but don't use the
		// list
		// returned as it is a copy. We need the actual list to be able to
		// delete from it.
		// ArrayList optionList = getProductOptionList();
		if (this.productOptionList != null) {
			MembershipProductOption memProdOption;
			ListIterator optionListIterator = this.productOptionList.listIterator();
			while (optionListIterator.hasNext()) {
				memProdOption = (MembershipProductOption) optionListIterator.next();
				if (memProdOption.getProductBenefitTypeCode().equals(productBenefitTypeCode)) {
					optionListIterator.remove();
				}
			}
		}
	}

	public MembershipVO copy() throws RemoteException {
		return copy(false);
	}

	public MembershipVO copy(boolean deepCopy) throws RemoteException {
		// LogUtil.log(this.getClass(),"copy="+membershipID);

		MembershipVO memVO = null;
		try {
			memVO = new MembershipVO(this.membershipID);
			memVO.setMode(ValueObject.MODE_LOADING);

			memVO.setAffiliatedClubExpiryDate(this.affiliatedClubExpiryDate);
			memVO.setAffiliatedClubId(this.affiliatedClubId);
			memVO.setAffiliatedClubCode(this.affiliatedClubCode);
			memVO.setAffiliatedClubProductLevel(this.affiliatedClubProductLevel);

			memVO.setAllowToLapse(this.allowToLapse);
			memVO.setClientNumber(this.clientNumber);
			memVO.setJoinDate(this.joinDate); // Must set before commence date
			memVO.setCommenceDate(this.commenceDate);
			memVO.setCreditAmount(this.creditAmount);
			memVO.setCreditPeriod(this.creditPeriod);
			memVO.setExpiryDate(this.expiryDate);
			memVO.setMembershipNumber(this.membershipNumber);
			memVO.setMembershipProfileCode(this.profileCode);
			memVO.setMembershipTerm(this.membershipTerm);
			memVO.setMembershipTypeCode(this.membershipTypeCode);
			memVO.setMembershipValue(this.membershipValue);
			memVO.setNextProductCode(this.nextProductCode);
			memVO.setDirectDebitAuthorityID(this.directDebitAuthorityID);
			memVO.setProductCode(this.productCode);
			memVO.setProductEffectiveDate(this.productEffectiveDate);
			memVO.setStatus(this.baseStatus);
			memVO.setTransactionReference(this.transactionReference);
			memVO.setVehicleCount(this.vehicleCount);
			memVO.setVehicleExpiryType(this.vehicleExpiryType);
			memVO.documentList = this.documentList;
			memVO.setAllowToLapseReasonCode(this.allowToLapseReasonCode);
			memVO.setRoadsideCreateDate(this.roadsideCreateDate);
			memVO.setRego(this.rego);
			memVO.setVin(this.vin);
			memVO.setMake(this.make);
			memVO.setModel(this.model);
			memVO.setYear(this.year);

			// copy credit attribute
			memVO.setOutstandingReducedCredit(this.outstandingReducedCredit);
			if (deepCopy) {
				// Copy the product options
				MembershipProductOption thisProdOption = null;
				Collection thisProductOptionList = getProductOptionList();
				if (thisProductOptionList != null) {
					Iterator thisProductOptionListIT = thisProductOptionList.iterator();
					while (thisProductOptionListIT.hasNext()) {
						thisProdOption = (MembershipProductOption) thisProductOptionListIT.next();
						memVO.addProductOption(thisProdOption.copy());
					}
				}

				// Copy the membership discounts
				ArrayList thisDiscountList = getDiscountList();
				memVO.setDiscountList(thisDiscountList);
			}

			memVO.setMode(ValueObject.MODE_NORMAL);
		} catch (Exception e) {
			throw new RemoteException("Error copying memVO for " + this.membershipID + ": " + ExceptionHelper.getExceptionStackTrace(e));
		}
		return memVO;
	}

	/**
	 * Remove any product options that are not valid for the product. This can become necessary when the membership was issued with one product and options for that product and then later the product is changed. In this case the product options for the old product may not be valid for the new product.
	 */
	private void checkProductOptions() throws RemoteException {
		// Only do the check if product options already exist.
		if (this.productOptionList != null && this.productOptionList.size() > 0) {
			ProductVO prodVO = getProduct();
			if (prodVO != null) {
				MembershipProductOption prodOption = null;
				ListIterator productOptionListIT = this.productOptionList.listIterator();
				while (productOptionListIT.hasNext()) {
					prodOption = (MembershipProductOption) productOptionListIT.next();
					// Remove the product option from the product option list if
					// it is not
					// a valid benefit for the new product.
					if (!prodVO.hasBenefit(prodOption.getProductBenefitTypeCode())) {
						productOptionListIT.remove();
					}
				}
			} else {
				// No product so delete all existing product options
				this.productOptionList = null;
			}
		}
	}

	/**
	 * Check to see if the membership value object is allowed to be updated. Throw an exception if the value object cannot be updated. Can't update the value object if it is operating in history mode. Can't update the value object if it has been set to read only.
	 */
	private void checkUpdateAllowed() throws RemoteException {
		if (this.isHistory()) {
			throw new SystemException("The membership history cannot be changed.");
		}

		// checkReadOnly();
	}

	/********************** Writable interface methods ************************/

	public String getWritableClassName() {
		return this.WRITABLE_CLASSNAME;
	}

	/**
	 * Write the data contained in this instance of the membership class using the specifiedclass writer. Deep writting is defaulted to false which means that only immediate membership data will be written and associated reference will be left out.
	 */
	public void write(ClassWriter cw) throws RemoteException {
		if (cw == null) {
			throw new RemoteException("A class writer must be specified.");
		}
		// Write the class name
		cw.startClass(WRITABLE_CLASSNAME);
		// Write single instance attributes
		if (this.affiliatedClubExpiryDate != null) {
			cw.writeAttribute("affiliatedClubExpiryDate", this.affiliatedClubExpiryDate);
		}
		if (this.affiliatedClubCode != null && this.affiliatedClubCode.length() > 0) {
			cw.writeAttribute("affiliatedClubCode", this.affiliatedClubCode);
		}
		if (this.affiliatedClubId != null && this.affiliatedClubId.length() > 0) {
			cw.writeAttribute("affiliatedClubId", this.affiliatedClubId);
		}
		if (this.affiliatedClubProductLevel != null && this.affiliatedClubProductLevel.length() > 0) {
			cw.writeAttribute("affiliatedClubProductLevel", this.affiliatedClubProductLevel);
		}
		cw.writeAttribute("allowToLapse", this.allowToLapse);
		if (this.allowToLapseReasonCode != null && this.allowToLapseReasonCode.length() > 0) {
			cw.writeAttribute("allowToLapseReasonCode", this.allowToLapseReasonCode);
		}
		cw.writeAttribute("clientNumber", this.clientNumber);
		cw.writeAttribute("commenceDate", this.commenceDate);

		if (this.creditAmount > 0.0) {
			cw.writeAttribute("creditAmount", NumberUtil.formatValue(this.creditAmount, "########0.00"));
		}
		if (this.creditPeriod != null) {
			cw.writeAttribute("creditPeriod", this.creditPeriod);
		}
		cw.writeAttribute("expiryDate", this.expiryDate);
		cw.writeAttribute("joinDate", this.joinDate);
		cw.writeAttribute("membershipID", this.membershipID);
		cw.writeAttribute("membershipNumber", this.membershipNumber);
		cw.writeAttribute("membershipTerm", this.membershipTerm);
		cw.writeAttribute("membershipTypeCode", this.membershipTypeCode);
		cw.writeAttribute("membershipValue", this.membershipValue);
		if (this.nextProductCode != null && this.nextProductCode.length() > 0) {
			cw.writeAttribute("nextProductCode", this.nextProductCode);
		}

		if (this.directDebitAuthorityID != null) {
			cw.writeAttribute("directDebitAuthorityID", this.directDebitAuthorityID);
		}
		cw.writeAttribute("productCode", this.productCode);
		cw.writeAttribute("productEffectiveDate", this.productEffectiveDate);
		cw.writeAttribute("profileCode", this.profileCode);
		cw.writeAttribute("status", this.baseStatus);
		cw.writeAttribute("inceptionSalesBranch", this.inceptionSalesBranch);
		cw.writeAttribute("transactionReference", this.transactionReference);
		int vehicleCnt = this.getVehicleCount();

		if (vehicleCnt > 0) {
			cw.writeAttribute("vehicleCount", this.vehicleCount);
		}
		if (this.vehicleExpiryType != null && this.vehicleExpiryType.length() > 0) {
			cw.writeAttribute("vehicleExpiryType", this.vehicleExpiryType);
		}

		cw.writeAttributeList("discountList", getDiscountList());
		LogUtil.debug(this.getClass(), "writing document list");
		cw.writeAttributeList("documentList", getDocumentList(false));
		LogUtil.debug(this.getClass(), "writing card list");
		cw.writeAttributeList("membershipCardList", getMembershipCardList(false));
		cw.writeAttributeList("productOptionList", getProductOptionList());
		cw.writeAttributeList("vehicleList", getVehicleList());
		cw.writeAttribute("client", getClient());
		Collection memGroupDetailList = null;
		// as a result of a change to check the PA for any documents to derive
		// the "in renewal" status
		// it is necessary to ensure that we have not already got a group at a
		// point in time against the object.
		MembershipGroupVO memGroupVO = getMembershipGroup(true);
		if (memGroupVO != null && !memGroupVO.hasException()) {
			memGroupDetailList = memGroupVO.getMembershipGroupDetailList();
			if (memGroupDetailList != null && memGroupDetailList.size() > 0) {
				cw.writeAttributeList("membershipGroupList", memGroupDetailList);
			}
		}

		if (this.rego != null) {
			cw.writeAttribute("rego", this.rego);
		}
		if (this.vin != null) {
			cw.writeAttribute("vin", this.vin);
		}
		if (this.make != null) {
			cw.writeAttribute("make", this.make);
		}
		if (this.model != null) {
			cw.writeAttribute("model", this.model);
		}
		if (this.year != null) {
			cw.writeAttribute("year", this.year);
		}
		// cw.writeAttribute("membershipType",getMembershipType());
		// cw.writeAttribute("product",getProduct());
		// cw.writeAttribute("profile",getMembershipProfile());
		cw.endClass();
	}

	/**
	 * Have outstanding amounts been deducted from the credits?
	 */
	public boolean hasOutstandingReducedCredit() {
		return outstandingReducedCredit;
	}

	/**
	 * Set the flag to allow removal of payments that have provided credit.
	 */
	private void setOutstandingReducedCredit(boolean outstandingReducedCredit) {
		this.outstandingReducedCredit = outstandingReducedCredit;
	}

	/**
	 * getPreviousMembership Retrieves the membership data of the previous version of the membership from the history file.
	 * 
	 * @return MembershipVO
	 */
	public MembershipVO getPreviousMembership() throws RemoteException {
		MembershipVO oldMemVO = null;
		Collection transList = getTransactionList();
		MembershipTransactionVO lastTransVO = null, thisTransVO = null;
		boolean flag;
		int thisTransId = 0, lastTransID = 0;
		if (transList != null && !transList.isEmpty()) {
			MembershipTransactionVO memTransVO;
			MembershipHistory memHistory;
			Iterator transListIterator = transList.iterator();
			flag = false;
			while (transListIterator.hasNext()) {
				if (flag) {
					lastTransID = thisTransId;
					lastTransVO = thisTransVO;
				}
				memTransVO = (MembershipTransactionVO) transListIterator.next();
				if (memTransVO.getTransactionGroupID() > thisTransId) {
					thisTransId = memTransVO.getTransactionGroupID();
					thisTransVO = memTransVO;
					flag = true;
				} else {
					flag = false;
				}
			}
			// if lastTransVO is null, then there is no history
			if (lastTransVO == null) {
				return null;
			}
			try {
				memHistory = lastTransVO.getMembershipHistory();
			} catch (RemoteException re) {
				// Some error occured getting the history so ignore it
				memHistory = null;
			}
			if (memHistory != null) {
				try {
					oldMemVO.setAffiliatedClubExpiryDate(memHistory.getAffiliatedClubExpiryDate());
					oldMemVO.setAffiliatedClubId(memHistory.getAffiliatedClubId());
					oldMemVO.setAffiliatedClubCode(memHistory.getAffiliatedClubCode());
					oldMemVO.setAffiliatedClubProductLevel(memHistory.getAffiliatedClubProductLevel());
					oldMemVO.setClientNumber(memHistory.getClientNumber());
					oldMemVO.setCommenceDate(memHistory.getCommenceDate());
					oldMemVO.setCreditAmount(memHistory.getCreditAmount());
					oldMemVO.setCreditPeriod(memHistory.getCreditPeriod());
					oldMemVO.setDiscountList(memHistory.getDiscountList());
					oldMemVO.setExpiryDate(memHistory.getExpiryDate());
					oldMemVO.setJoinDate(memHistory.getJoinDate());
					oldMemVO.setMembershipNumber(memHistory.getMembershipNumber());
					oldMemVO.setMembershipProfileCode(memHistory.getMembershipProfileCode());
					oldMemVO.setMembershipTerm(memHistory.getMembershipTerm());
					oldMemVO.setMembershipType(memHistory.getMembershipType());
					oldMemVO.setMembershipTypeCode(memHistory.getMembershipTypeCode());
					oldMemVO.setProduct(memHistory.getProduct());
					oldMemVO.setProductCode(memHistory.getProductCode());
					oldMemVO.setProductEffectiveDate(memHistory.getProductEffectiveDate());

					oldMemVO.setYear(memHistory.getYear());
					oldMemVO.setMake(memHistory.getMake());
					oldMemVO.setModel(memHistory.getModel());
					oldMemVO.setRego(memHistory.getRego());
					oldMemVO.setVin(memHistory.getVin());

					/*
					 * oldMemVO.productOptionList = memHistory.getProductOptionList(); oldMemVO.vehicleList = memHistory.getVehicleList(); oldMemVO.vehicleCount = memHistory.getVehicleCount();
					 */
				} catch (Exception e) {
					return null; // unable to get previous data, assume new
					// client
				}
			}
		}
		return oldMemVO;
	}

	/**
	 * Transactions that create a replacement payable item cannot be allowed for memberships with more than 2 years to run until the getPaidRemainingMembershipValue() method is made recursive to handle memberships of unknown length. JH 305/2004 fixed.
	 */
	private String checkMaximumMembershipPeriod() throws SystemException {
		String reason = null;
		// try
		// {
		// if (this.getExpiryDate().subtract(new
		// Interval(2,0,0,0,0,0,0)).afterDay(new DateTime()))
		// reason =
		// "Unable to perform this transaction on a membership with more than 2 years to run.";
		// }
		// catch (Exception e)
		// {
		// throw new SystemException(e);
		// }
		return reason;
	}

	/**
	 * Given a membership, return the XML representation of the membership as a string
	 */
	public String toXML() throws SystemException {

		// Exclude some classes from the class map that is written out
		ArrayList excludedClassList = new ArrayList();
		try {
			excludedClassList.add(Class.forName("com.ract.membership.DiscountTypeVO"));
			excludedClassList.add(Class.forName("com.ract.membership.MembershipProfileVO"));
			excludedClassList.add(Class.forName("com.ract.membership.MembershipTransactionVO"));
			excludedClassList.add(Class.forName("com.ract.membership.MembershipTypeVO"));
			excludedClassList.add(Class.forName("com.ract.membership.ProductVO"));
			excludedClassList.add(Class.forName("com.ract.membership.ProductBenefitTypeVO"));
			excludedClassList.add(Class.forName("com.ract.common.StreetSuburbVO"));
		} catch (ClassNotFoundException cnfe) {
			throw new SystemException("Failed to find class", cnfe);
		}
		return XMLHelper.toXML(excludedClassList, this);
	}

	public String getPendingFeeId() {
		return pendingFeeId;
	}

	/**
	 * @hibernate.property
	 * @hibernate.column name="roadside_create_date"
	 */
	public DateTime getRoadsideCreateDate() {
		return roadsideCreateDate;
	}

	public void setRoadsideCreateDate(DateTime roadsideCreateDate) {
		this.roadsideCreateDate = roadsideCreateDate;
	}

	@Override
	public String toString() {
		return "MembershipVO [affiliatedClubCode=" + affiliatedClubCode + ", affiliatedClubExpiryDate=" + affiliatedClubExpiryDate + ", affiliatedClubId=" + affiliatedClubId + ", affiliatedClubProductLevel=" + affiliatedClubProductLevel + ", allowToLapse=" + allowToLapse + ", allowToLapseReasonCode=" + allowToLapseReasonCode + ", amountOutstanding=" + amountOutstanding + ", baseStatus=" + baseStatus + ", checkRecAmount=" + checkRecAmount + ", client=" + client + ", clientNumber=" + clientNumber + ", commenceDate=" + commenceDate + ", creditAmount=" + creditAmount + ", creditMap=" + creditMap + ", creditPeriod=" + creditPeriod + ", currentProductCode=" + currentProductCode + ", currentProductEffectiveDate=" + currentProductEffectiveDate + ", directDebitAuthority=" + directDebitAuthority + ", directDebitAuthorityID=" + directDebitAuthorityID + ", discountList=" + discountList + ", discountPeriod=" + discountPeriod + ", documentList=" + documentList + ", expiryDate=" + expiryDate + ", groupMembers="
				+ groupMembers + ", inceptionSalesBranch=" + inceptionSalesBranch + ", joinDate=" + joinDate + ", membershipCardList=" + membershipCardList + ", membershipGroupVO=" + membershipGroupVO + ", membershipID=" + membershipID + ", membershipNumber=" + membershipNumber + ", membershipTerm=" + membershipTerm + ", membershipType=" + membershipType + ", membershipTypeCode=" + membershipTypeCode + ", membershipValue=" + membershipValue + ", nextProductCode=" + nextProductCode + ", oldPayableItemList=" + oldPayableItemList + ", outstandingReducedCredit=" + outstandingReducedCredit + ", pendingFeeId=" + pendingFeeId + ", product=" + product + ", productCode=" + productCode + ", productEffectiveDate=" + productEffectiveDate + ", productOptionList=" + productOptionList + ", profile=" + profile + ", profileCode=" + profileCode + ", transactionReference=" + transactionReference + ", vehicleCount=" + vehicleCount + ", vehicleExpiryType=" + vehicleExpiryType + ", vehicleList=" + vehicleList + ", virtualStatus="
				+ virtualStatus + ", rego=" + rego + "]";
	}

	/**
	 * @hibernate.property
	 */
	public String getRego() {
		return rego;
	}

	public void setRego(String rego) {
		this.rego = rego;
	}

	/**
	 * @hibernate.property
	 */
	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	/**
	 * @hibernate.property
	 */
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	/**
	 * @hibernate.property
	 */
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @hibernate.property
	 */
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @hibernate.property
	 */
	public DateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(DateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
