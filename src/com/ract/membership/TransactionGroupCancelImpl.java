package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.client.ClientVO;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.membership.ui.MembershipUIHelper;
import com.ract.payment.PayableItemComponentVO;
import com.ract.payment.PayableItemGenerator;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PostingContainer;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.user.User;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

/**
 * Provides implementation of methods specific to the "Cancel" transaction.
 * 
 * @author GW, JYH
 * @created 2 May 2003
 * @version 1.0
 */

public class TransactionGroupCancelImpl extends TransactionGroup implements PayableItemGenerator {

    public void setTransactionBlocking(boolean transactionBlocking) {
	this.transactionBlocking = transactionBlocking;
    }

    public boolean isTransactionBlocking() {
	return transactionBlocking;
    }

    public TransactionGroupCancelImpl(User user) throws RemoteException {
	super(MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL, user);
	this.paymentMethodListToRemove = new ArrayList();
    }

    /**
     * developer attribute
     */
    // private boolean writeLog = true;

    private boolean transactionBlocking;

    /**
     * Generate a list of payable items that need to be sent to the payment
     * manager. The returned list will contain one new payable item which
     * contains the information required to cancel the membership. The list may
     * contain one or more existing payable items which need to be updated so
     * that they no longer reflect an amount owed or to be earned.
     */
    public ArrayList generatePayableItemList() throws RemoteException, RollBackException {
	ArrayList newPayableItemList = new ArrayList();
	Collection activeTransactionList = null;

	PayableItemVO oldPayableItem = null;
	PayableItemVO newPayableItem = null;
	PayableItemComponentVO newComponent = null;
	PostingContainer postingList = null;

	ArrayList newComponentList = null;
	MembershipTransactionVO memTxVO = null;

	// old transaction
	MembershipTransactionVO oldMemTxVO = null;

	MembershipVO memVO = null;

	PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();

	// get the new transaction list
	Collection memTxList = this.getTransactionList();
	if (memTxList == null || memTxList.size() == 0) {
	    throw new SystemException("No membership transactions found!");
	} else if (memTxList.size() != 1) {
	    throw new SystemException("A cancel transaction may not be done in the context of other transactions.");
	}

	Iterator memTxListIterator = memTxList.iterator();

	// should only be one - cancel
	if (memTxListIterator.hasNext()) {
	    memTxVO = (MembershipTransactionVO) memTxListIterator.next();

	    memVO = memTxVO.getMembership();

	    // only process the membership actually being cancelled
	    if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(memTxVO.getTransactionTypeCode())) {
		throw new SystemException("There should only be one transaction of type " + MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL + " and the transaction was '" + memTxVO.getTransactionTypeCode() + "' for " + memVO.getClientNumber());
	    }

	    LogUtil.log(this.getClass(), "mem " + memVO.getMembershipNumber());

	    activeTransactionList = memVO.getCurrentTransactionList();

	    LogUtil.log(this.getClass(), "Cancel size " + (activeTransactionList != null ? activeTransactionList.size() : 0));
	    PostingContainer discList = new PostingContainer();

	    if (activeTransactionList != null && activeTransactionList.size() > 0) {
		// create a new array list to store ALL postings
		postingList = new PostingContainer();
		double amountOutstanding = 0;
		Iterator activeTransactionListIterator = activeTransactionList.iterator();
		while (activeTransactionListIterator.hasNext()) {
		    boolean fullyUnpaid = false;
		    oldMemTxVO = (MembershipTransactionVO) activeTransactionListIterator.next();

		    LogUtil.log(this.getClass(), "test i");

		    // get payableItem

		    // potential lock?
		    if (oldMemTxVO.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP)) {
			oldPayableItem = oldMemTxVO.getPayableItem();
		    } else {
			oldPayableItem = oldMemTxVO.getPayableItemForPA();
		    }
		    if (oldPayableItem != null) {
			amountOutstanding = oldPayableItem.getAmountOutstanding();

			LogUtil.log(this.getClass(), "o/s=" + amountOutstanding + ",ap=" + oldPayableItem.getAmountPayable());
			if (amountOutstanding > 0.0) {
			    fullyUnpaid = oldPayableItem.isFullyUnpaid();

			    PaymentMethod paymentMethod = oldPayableItem.getPaymentMethod();
			    paymentMethod.setClientNumber(oldPayableItem.getClientNo());
			    this.addPaymentMethodToRemove(paymentMethod);
			}
		    }

		    Collection cancPostings = createCancellationPostings(oldMemTxVO, fullyUnpaid);
		    LogUtil.log(this.getClass(), "createCancellationPostings 2 postingList=" + postingList);

		    LogUtil.log(this.getClass(), "createCancellationPostings 2 cancPostings=" + cancPostings);
		    postingList.addAll(cancPostings);
		    LogUtil.log(this.getClass(), "createCancellationPostings 3 postingList=" + postingList);

		}
	    }

	    LogUtil.log(this.getClass(), "newComponentList 1 " + (newComponentList != null ? newComponentList.size() : 0));

	    // now create the component to hold the postings
	    if (postingList != null && postingList.size() > 0) {
		// get the fee for the transaction
		double amountPayable = memTxVO.getNetTransactionFee();
		String description = memTxVO.getTransactionTypeCode() + " membership  Ref : " + memTxVO.getTransactionID();

		// Create a component for each Membership Transaction.
		try {
		    // initialise the new component list
		    newComponentList = new ArrayList();
		    newComponent = new PayableItemComponentVO(postingList, new BigDecimal(amountPayable).setScale(2, BigDecimal.ROUND_HALF_UP), description, null // componentID
			    , memTxVO.getNewMembership().getMembershipNumber() // the
									       // component's
									       // source
									       // system
									       // reference
			    , null // supersedingComponentID
		    );

		    // and add it to the list to attach to the payable item
		    newComponentList.add(newComponent);
		} catch (PaymentException e) {
		    throw new SystemException("Error creating component : " + e.getMessage(), e);
		}
	    }

	    LogUtil.log(this.getClass(), "newComponentList 1 " + (newComponentList != null ? newComponentList.size() : 0));

	    // now create the payable item
	    if (newComponentList != null && newComponentList.size() > 0) {
		// The product period that the payable item is for starts on the
		// preferred commence date unless overridden below.
		DateTime startDate = this.getPeriodStartDate();
		DateTime endDate = this.getPeriodEndDate();

		PaymentMethod paymentMethod = new ReceiptingPaymentMethod(memVO.getClientNumber(), SourceSystem.MEMBERSHIP);
		Integer clientBranchNumber = this.getContextClient().getBranchNumber();
		String description = MembershipUIHelper.getTransactionHeadingText(this.getTransactionGroupTypeCode());
		Integer payableItemID = null;
		try {
		    newPayableItem = new PayableItemVO(payableItemID, memVO.getClientNumber(), paymentMethod, newComponentList, SourceSystem.MEMBERSHIP, memTxVO.getTransactionID() // sourceSystemReferenceID
			    , memTxVO.getSalesBranchCode(), clientBranchNumber, memTxVO.getNewMembership().getProductCode(), memTxVO.getUsername(), this.getPeriodStartDate(), this.getPeriodEndDate(), description);

		    newPayableItemList.add(newPayableItem);
		} catch (PaymentException e) {
		    throw new SystemException("Error creating payable item : " + e.getMessage(), e);
		}

	    }
	}

	LogUtil.log(this.getClass(), " >>------> Final postings : " + postingList);

	return newPayableItemList;
    }

    private Collection createCancellationPostings(MembershipTransactionVO mtVO, boolean fullyUnpaid) throws RemoteException, RollBackException {
	PaymentMgr pMgr = PaymentEJBHelper.getPaymentMgr();
	String referenceNumber = mtVO.getMembership().getMembershipNumber();
	return pMgr.createCancelTypePostings(referenceNumber, /* picVO, */mtVO, MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL, fullyUnpaid);
    }

    public void createTransactionsForSelectedClients() throws RemoteException, ValidationException {
	doCancelTransaction();
    }

    private void doCancelTransaction() throws RemoteException, ValidationException {
	MembershipTransactionVO memTransVO = null;
	MembershipVO oldMemVO = null;
	MembershipVO newMemVO = null;
	SelectedClient selectedClient;
	ClientVO clientVO = null;
	DateTime transDate = getTransactionDate();
	Collection clientList = this.getSelectedClientMap().values();

	Iterator clientListIT = clientList.iterator();
	while (clientListIT.hasNext()) {
	    selectedClient = (SelectedClient) clientListIT.next();
	    clientVO = selectedClient.getClient();
	    oldMemVO = selectedClient.getMembership();

	    // The context client is the one being cancelled.
	    if (oldMemVO.getMembershipID().equals(this.contextClient.getMembership().getMembershipID())) {
		// Copy the membership being cancelled and set its status
		newMemVO = oldMemVO.copy();
		newMemVO.setStatus(MembershipVO.STATUS_CANCELLED);
		newMemVO.setAllowToLapse(Boolean.valueOf(false));

		// Create a new transaction object for the membership
		// transaction
		memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.getTransactionGroupTypeCode(), MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL, selectedClient.getMembershipGroupJoinDate());
		memTransVO.setTransactionDate(transDate);
		memTransVO.setUsername(this.getUser().getUserID());
		memTransVO.setSalesBranchCode(this.getUser().getSalesBranchCode());
		memTransVO.setMembership(oldMemVO);
		memTransVO.setNewMembership(newMemVO);
		// set start and end date
		memTransVO.setEffectiveDate(transDate);
		memTransVO.setEndDate(oldMemVO.getExpiryDate());

		memTransVO.setPrimeAddressee(selectedClient.getKey().equals(this.getPaSelectedClientKey()));
		if (memTransVO.isPrimeAddressee()) {
		    setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
		} else {
		    newMemVO.setDirectDebitAuthorityID(null);
		}

		// Work out the unearned credit that can be used on the next
		// transaction
		this.adjustUnearnedCreditAmount(memTransVO);
		this.getTransactionList().add(memTransVO);
	    } else {
		throw new SystemException("Can't cancel a member directly from a group.");
	    }
	}
    }

}
