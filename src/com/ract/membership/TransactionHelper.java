package com.ract.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

import com.ract.client.Client;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ReferenceDataVO;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.notifier.NotificationMgrLocal;
import com.ract.membership.notifier.MembershipChangeNotificationEvent;
import com.ract.membership.ui.MembershipUIHelper;
import com.ract.payment.PayableItemComponentVO;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentTransactionMgr;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.payment.receipting.ReceiptingPaymentMethod;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

public class TransactionHelper {

    /**
     * @todo This duplicates a lot of code from processAmountPayable.
     * 
     *       Process the amount payable from the transaction group for a
     *       straight renewal. Does not create receipting records, as the
     *       receipting is already complete when this process is called.
     * @param TransactionGroup
     *            transGroup
     * @param double amount
     * @param double receiptingDiscount
     * @param Integer
     *            sequenceNo
     * @param PaymentTransactionMgr
     *            paymentTxMgr
     * @throws paymentException
     * @throws RemoteException
     * @throws RollBackException
     */
    public PayableItemVO processRenewalAmountPayable(TransactionGroup transGroup, double amount, double receiptingDiscount, Integer sequenceNo, PaymentTransactionMgr paymentTxMgr, String receiptNo) throws PaymentException, RemoteException, RollBackException {
	LogUtil.debug(this.getClass(), "processRenewalAmountPayable start");
	LogUtil.debug(this.getClass(), "paymentTxMgr is not local!");
	MembershipVO memVO = null;
	PayableItemVO newPayableItem = null;
	PayableItemVO payableItem = null;
	MembershipTransactionVO primeMemTransVO = transGroup.getMembershipTransactionForPA();
	Integer transactionReference = primeMemTransVO.getTransactionID();
	MembershipVO primeMemVO = primeMemTransVO.getNewMembership();
	double amountPayable = 0.0;
	double totalPayable = 0.0;
	Integer payableItemID = null; // No payable item id yet. This gets
				      // returned

	Collection memTxList = transGroup.getTransactionList();
	Vector componentList = new Vector();
	MembershipTransactionVO memTxVO = null;
	String description = "";
	Vector postingList = null;
	Iterator memTxListIterator = memTxList.iterator();
	while (memTxListIterator.hasNext()) {
	    memTxVO = (MembershipTransactionVO) memTxListIterator.next();

	    if (transactionReference == null) {
		transactionReference = memTxVO.getTransactionID();
	    }

	    String txType = memTxVO.getTransactionTypeCode();
	    // create the postings for the transaction
	    postingList = new MembershipPostingContainer(memTxVO, transactionReference, transGroup);
	    // get the fee for the transaction
	    amountPayable = memTxVO.getNetTransactionFee();
	    totalPayable += amountPayable;

	    description = txType + " membership  Ref : " + transactionReference;

	    // Create a component for each Membership Transaction.
	    Integer componentID = null; // This is set by the payment system.
	    PayableItemComponentVO component = new PayableItemComponentVO(postingList, new BigDecimal(amountPayable).setScale(2, BigDecimal.ROUND_HALF_UP), description, componentID, memTxVO.getNewMembership().getMembershipNumber(), null); // supersedingComponentID
	    // and add it to the list to attach to the payable item
	    componentList.add(component);
	} // while

	// round the total amount payable
	totalPayable = NumberUtil.roundDouble(totalPayable, 2);

	LogUtil.debug("processRenewalAmountPayable", "processRenewalAmountPayable(): totalPayable=" + totalPayable);

	if (amount != totalPayable) {
	    throw new PaymentException("Amounts do not balance in processRenewalAmountPayable: " + "Amount paid = " + amount + "; Amount payable = " + totalPayable);
	}

	// if no components were created throw an exception
	if (componentList.size() == 0) {
	    throw new PaymentException("Component list size = 0");
	}

	// The schedule specification fetched below uses the start and end dates
	// worked out by the transaction group so the payable item should use
	// the same dates.
	DateTime startDate = transGroup.getPeriodStartDate();
	DateTime endDate = transGroup.getPeriodEndDate();

	PaymentMethod paymentMethod = null;

	// Use a receipting payment method if direct debit has not been chosen.

	/** @todo How can it be a direct debit when it is called from OCR? */
	if (transGroup.getDirectDebitAuthority() == null) {
	    paymentMethod = new ReceiptingPaymentMethod(primeMemTransVO.getNewMembership().getClientNumber(), SourceSystem.MEMBERSHIP);
	} else {
	    throw new PaymentException("Receipting system has initiated this transaction but it is a direct debit payment.");
	}

	Integer clientBranchNumber = primeMemVO.getClient().getBranchNumber();

	description = MembershipUIHelper.getTransactionHeadingText(transGroup.getTransactionGroupTypeCode());
	// and create the payableItem.
	payableItem = new PayableItemVO(payableItemID, primeMemTransVO.getNewMembership().getClientNumber(), paymentMethod, componentList, SourceSystem.MEMBERSHIP, transactionReference, primeMemTransVO.getSalesBranchCode(), // sourceSystemReferenceID
		clientBranchNumber, primeMemTransVO.getNewMembership().getProductCode(), primeMemTransVO.getUsername(), startDate, endDate, description);

	payableItem.setAmountOutstanding(totalPayable - amount); // should
								 // always be
								 // zero
	payableItem.setReceiptNo(receiptNo);

	// Record the receipting ID which was used.
	// This will also prevent a new receipting record from being created
	payableItem.setRepairPaymentMethodID(sequenceNo.toString());

	newPayableItem = paymentTxMgr.addPayableItem(payableItem);

	LogUtil.debug(this.getClass(), "processRenewalAmountPayable end");

	// return the payable item so we can update dependant objects
	return newPayableItem;
    } // processRenewalAmountPayable

    /**
     * Process Renewal transactions for renewals created from OCR payments
     * 
     * @param TransactionGroup
     *            transGroup
     * @param NotificationMgr
     *            notificationMgr
     * @throws RemoteException
     * @todo remove duplicate code!!!
     */
    public ArrayList processRenewalTransaction(MembershipMgrLocal membershipMgrLocal, TransactionGroup transGroup, NotificationMgrLocal notificationMgrLocal) throws RemoteException {
	LogUtil.debug(this.getClass(), "processRenewalTransaction start");

	if (transGroup == null) {
	    throw new SystemException("Unable to process transaction. Transaction object is null.");
	}
	String transGroupTypeCode = transGroup.getTransactionGroupTypeCode();
	// for logging
	Integer memTransCount = null;
	Integer paClientNumber = null;
	String userID = "";
	try {
	    memTransCount = new Integer(transGroup.getMembershipTransactionCount());
	    paClientNumber = transGroup.getClientForPA().getClientNumber();
	    userID = transGroup.getUserID();
	} catch (Exception exp) {
	    throw new SystemException(exp);
	}

	ArrayList completedTransactionList = new ArrayList();
	Vector transactionList = transGroup.getTransactionList();
	if (transactionList == null || transactionList.isEmpty()) {
	    throw new SystemException("Transaction list is empty. Cannot process " + transGroup.getTransactionGroupTypeCode() + " transaction.");
	}
	// Set the transaction date to the exact time the transaction is saved.
	transGroup.setTransactionDate(new DateTime());

	/** @todo This should probably be generalised. */
	// Set the transaction group ID if more than one membership transaction
	// is being processed.
	transGroup.setTransactionGroupID();

	transGroup.setMembershipTransactionIDs();

	// Get the membership group ID. Will be null if a membership group
	// is not being created or updated.
	Integer membershipGroupID = getMembershipGroupID(transGroup);

	MembershipVO oldMemVO;
	MembershipVO newMemVO;
	boolean primeAddressee = false;
	MembershipChangeNotificationEvent notificationEvent = null;

	ListIterator transListIT = transactionList.listIterator();
	int groupCount = transGroup.getActualGroupMemberCount();
	if (groupCount == 0) {
	    groupCount = 1;
	    /** @todo Why? */
	}

	while (transListIT.hasNext()) {
	    // check if membership value retrieved from all memberships to
	    // offset total ???
	    MembershipTransactionVO memTransVO = (MembershipTransactionVO) transListIT.next();
	    oldMemVO = memTransVO.getMembership();
	    newMemVO = memTransVO.getNewMembership();

	    newMemVO.setDirectDebitAuthorityID(null);
	    newMemVO.setExpiryDate(transGroup.getExpiryDate());

	    memTransVO.setProductCode(newMemVO.getProductCode());
	    memTransVO.setGroupCount(new Integer(groupCount));
	    LogUtil.debug(this.getClass(), "processRenewalTransaction 1 newMemVO=" + newMemVO);
	    newMemVO = membershipMgrLocal.updateMembership(newMemVO);
	    LogUtil.debug(this.getClass(), "processRenewalTransaction 2 newMemVO=" + newMemVO);
	    notificationEvent = new MembershipChangeNotificationEvent(MembershipChangeNotificationEvent.ACTION_UPDATE, newMemVO);
	    try {
		notificationMgrLocal.notifyEvent(notificationEvent);
	    } catch (RollBackException rbe) {
		throw new SystemException(rbe);
	    }

	    // Set the membership ID on the membership transaction now that we
	    // have it.
	    memTransVO.setNewMembership(newMemVO);
	    memTransVO = createMembershipTransaction(membershipMgrLocal, memTransVO);

	    // Create a request for a new membership card if the state of
	    // the membership indicates that a new card is required.
	    createMembershipCardRequest(membershipMgrLocal, newMemVO, null, userID, false); // Work
											    // out
											    // a
											    // reason
											    // to
											    // request
											    // the
											    // card.

	    // Add the membership to the returned list of memberships that were
	    // affected by this transaction.
	    completedTransactionList.add(memTransVO);
	} // while (transListIT.hasNext())
	LogUtil.debug(this.getClass(), "processRenewalTransaction end");
	return completedTransactionList;

    } // processRenewalTransaction

    /**
     * Examine the state of the membership and decide if a new membership card
     * should be issued of if an existing card request should be updated.
     */
    public void createMembershipCardRequest(MembershipMgr membershipMgr, MembershipVO memVO, String reasonCode, String userId, boolean replacementRequest) throws RemoteException {
//	LogUtil.debug(this.getClass(), "createMembershipCardRequest start");
//	boolean requestCard = false;
//	MembershipCardVO cardVO = null;
//	int issuanceNumber = 0;
//	DateTime joinDate = null;
//	String cardName = null;
//	String productCode = null;
//	DateTime cardExpiryDate = null;
//
//	LogUtil.debug(this.getClass(), "memVO=" + memVO);
//	LogUtil.debug(this.getClass(), "client=" + memVO.getClient());
//	LogUtil.debug(this.getClass(), "reasonCode=" + reasonCode);
//	LogUtil.debug(this.getClass(), "userId=" + userId);
//
//	// If the client is deceased then don't bother requesting a card.
//	String clientStatus = memVO.getClient().getStatus();
//	if (Client.STATUS_DECEASED.equals(clientStatus)) {
//	    LogUtil.warn(this.getClass(), "Client " + memVO.getClientNumber() + "is " + clientStatus + ".");
//	    return;
//	}
//
//	// Don't request a card if the membership is expired.
//	if (memVO.isExpired()) {
//	    LogUtil.warn(this.getClass(), "Membership " + memVO.getMembershipID() + " is expired.");
//	    return;
//	}
//
//	// See if they have a membership card
//	ArrayList cardList = membershipMgr.getMembershipCardList(memVO.getMembershipID());
//	if (cardList == null || cardList.isEmpty()) {
//	    requestCard = true;
//	    // Workout a reason for the request if a reason was not given.
//	    reasonCode = (reasonCode == null ? ReferenceDataVO.CARD_REQUEST_NEW_MEMBERSHIP : reasonCode);
//	} else {
//	    // Sort the cards into reverse request date order and get the top
//	    // one off the list.
//	    Collections.reverse(cardList);
//	    Iterator cardIterator = cardList.iterator();
//	    if (cardIterator.hasNext()) {
//		cardVO = (MembershipCardVO) cardIterator.next();
//	    }
//
//	    // If we still don't get a card then request a new one.
//	    if (cardVO == null) {
//		requestCard = true;
//		// Workout a reason for the request if a reason was not given.
//		reasonCode = (reasonCode == null ? ReferenceDataVO.CARD_REQUEST_NEW_MEMBERSHIP : reasonCode);
//	    }
//	}
//
//	// If we aren't going to request a card because they haven't got one
//	// and a replacement card was requested
//	// and the last card has been issued
//	// then request a new one
//	if (!requestCard && replacementRequest && cardVO.isIssued()) {
//	    requestCard = true;
//	    // Workout a reason for the request if a reason was not given.
//	    reasonCode = (reasonCode == null ? ReferenceDataVO.CARD_REQUEST_LOST : reasonCode);
//	    // Increment the issuance number for replacement requests.
//	    issuanceNumber = cardVO.getIssuanceNumber() + 1;
//	}
//
//	// CR 117 Rejoin always request a new card if an unissued one exists.
//	if (!requestCard && ReferenceDataVO.CARD_REQUEST_REJOIN_MEMBERSHIP.equals(reasonCode) && cardVO.isIssued()) {
//	    requestCard = true;
//	}
//
//	// See if there is another reason to request a new card.
//	// If a card request already exists then we don't need to request
//	// another one.
//	if (!requestCard && cardVO != null && cardVO.isIssued()) {
//	    // Use the data from the last card to see if a new one is required.
//	    cardName = cardVO.getCardName();
//	    joinDate = cardVO.getJoinDate();
//	    productCode = cardVO.getProductCode();
//	    cardExpiryDate = cardVO.getExpiryDate();
//
//	    LogUtil.debug(this.getClass(), "cardVO=" + cardName);
//	    LogUtil.debug(this.getClass(), "cardName=" + cardName);
//	    LogUtil.debug(this.getClass(), "joinDate=" + joinDate);
//	    LogUtil.debug(this.getClass(), "productCode=" + productCode);
//	    LogUtil.debug(this.getClass(), "cardExpiryDate=" + cardExpiryDate);
//
//	    if (!requestCard && productCode != null) {
//		requestCard = !memVO.getProductCode().equalsIgnoreCase(productCode);
//		reasonCode = (requestCard && reasonCode == null ? ReferenceDataVO.CARD_REQUEST_CHANGE_PRODUCT : reasonCode);
//	    }
//
//	    if (reasonCode != null && reasonCode.equals(ReferenceDataVO.CARD_REQUEST_CHANGE_REGO)) {
//		requestCard = true;
//	    }
//	}
//
//	boolean explicitTierChange = false;
//	if (requestCard && ReferenceDataVO.CARD_REQUEST_TIER_CHANGE.equalsIgnoreCase(reasonCode)) {
//	    explicitTierChange = true;
//	}
//
//	boolean issueTierBasedCards = false;
//	try {
//	    CommonMgrLocal commonMgrLocal = CommonEJBHelper.getCommonMgrLocal();
//	    issueTierBasedCards = Boolean.valueOf(commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_TIER_BASED_CARDS)).booleanValue();
//	} catch (RemoteException ex1) {
//	    // remain false
//	    LogUtil.warn(this.getClass(), ex1);
//	}
//
//	LogUtil.debug(this.getClass(), "requestCard=" + requestCard);
//	LogUtil.debug(this.getClass(), "issueTierBasedCards=" + issueTierBasedCards);
//	if (cardVO != null) {
//	    LogUtil.debug(this.getClass(), "issued=" + cardVO.isIssued());
//	    LogUtil.debug(this.getClass(), "card tier=" + (cardVO.getTier() != null ? "" + cardVO.getTier().isEligibleForLoyaltyScheme() : ""));
//	}
//	LogUtil.debug(this.getClass(), "mem tier=" + (memVO.getRenewalMembershipTier() != null ? "" + memVO.getRenewalMembershipTier().isEligibleForLoyaltyScheme() : ""));
//
//	// issue a card if the renewal membership tier has changed.
//	if (!requestCard && ((cardVO != null && cardVO.isIssued() && cardVO.getTier() != null && !cardVO.getTier().isEligibleForLoyaltyScheme()) || // no
//																		    // card
//																		    // issued
//																		    // for
//																		    // GM
//		cardVO == null) && // no card at all
//		memVO.getRenewalMembershipTier() != null && memVO.getRenewalMembershipTier().isEligibleForLoyaltyScheme() && // eligible
//															     // on
//															     // renewal
//															     // for
//															     // GM
//		issueTierBasedCards) {
//	    requestCard = true;
//	    reasonCode = ReferenceDataVO.CARD_REQUEST_TIER_CHANGE;
//	}
//
//	LogUtil.debug(this.getClass(), "createMembershipCardRequest " + memVO.getMembershipID() + ", requestCard: " + requestCard + " " + reasonCode);
//
//	String tierCode = null;
//
//	// Only request a new card if we have determined that a new card is
//	// required.
//	if (requestCard) {
//	    LogUtil.debug(this.getClass(), "createMembershipCardRequest 5");
//	    // Default the reason code if we still havn't set it
//	    reasonCode = (reasonCode == null ? ReferenceDataVO.CARD_REQUEST_NEW_MEMBERSHIP : reasonCode);
//
//	    // Issue card at next renewal tier
//	    if (explicitTierChange) {
//		// renewal membership tier
//		tierCode = memVO.getRenewalMembershipTier() != null ? memVO.getRenewalMembershipTier().getTierCode() : null;
//	    } else {
//		// current tier
//		tierCode = memVO.getMembershipTier() != null ? memVO.getMembershipTier().getTierCode() : null;
//	    }
//	    LogUtil.debug(this.getClass(), "createMembershipCardRequest 6");
//	    cardVO = new MembershipCardVO(memVO.getMembershipID(), issuanceNumber, new DateTime(), ReferenceDataVO.REF_TYPE_MEMBERSHIP_CARD_REQUEST, reasonCode, tierCode, memVO.getJoinDate(), memVO.getRego());
//	    cardVO.setUserId(userId);
//
//	    membershipMgr.createMembershipCard(cardVO);
//	    LogUtil.debug(this.getClass(), "createMembershipCardRequest 7");
//	}
//	LogUtil.debug(this.getClass(), "createMembershipCardRequest end");
    }

    /**
     * Return the appropriate membership group ID for the circumstances. If
     * there is only one member in the new group then return null. If the
     * memberships are being joined to an existing group then return that
     * membership group ID. Otherwise get a new membership group ID.
     * 
     * @param transGroup
     *            Description of the Parameter
     * @return The membershipGroupID value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Integer getMembershipGroupID(TransactionGroup transGroup) throws RemoteException {
	LogUtil.debug(this.getClass(), "getMembershipGroupID start");
	Integer memGroupID = null;
	if (transGroup.getActualGroupMemberCount() > 1) {
	    if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(transGroup.getTransactionGroupTypeCode()) || transGroup.getMembershipGroupID() == null) {
		MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
		memGroupID = new Integer(idMgr.getNextGroupID());
	    } else {
		memGroupID = transGroup.getMembershipGroupID();
	    }
	}
	LogUtil.debug(this.getClass(), "getMembershipGroupID end");
	return memGroupID;
    }

    /**
     * Update membership transaction with the xml from the membership.
     */
    public void updateMembershipTransactionMembershipXML(MembershipMgr membershipMgr, MembershipTransactionVO memTransVO) throws RemoteException {
	LogUtil.debug(this.getClass(), "updateMembershipTransactionMembershipXML start");

	LogUtil.debug(this.getClass(), "transactionId=" + memTransVO.getTransactionID());
	String memXML = memTransVO.getNewMembership().toXML();
	// LogUtil.debug(this.getClass(),
	// "updateMembershipTransactionMembershipXML "+memXML);
	if (memXML == null) {
	    throw new RemoteException("XML may not be null when saving the XML.");
	}
	LogUtil.debug(this.getClass(), "memXML=" + memXML);
	memTransVO.setMembershipXML(memXML);
	// LogUtil.debug(this.getClass(),"updateMembershipTransactionMembershipXML memTransVO"+memTransVO.getMembershipXML());
	membershipMgr.updateMembershipTransaction(memTransVO);
	LogUtil.debug(this.getClass(), "updateMembershipTransactionMembershipXML end");
    }

    /**
     * Update the prime addressee's transaction with the payable item id.
     * 
     * @param transGroup
     *            TransactionGroup
     * @param payableItem
     *            PayableItemVO
     * @throws RemoteException
     */
    public void setMembershipTransactions(MembershipMgrLocal membershipMgrLocal, TransactionGroup transGroup, PayableItemVO payableItem) throws RemoteException {
	LogUtil.debug(this.getClass(), "setMembershipTransactions start");
	LogUtil.debug(this.getClass(), "setMembershipTransactions 1");
	LogUtil.debug(this.getClass(), "payableItem=" + payableItem);
	if (payableItem != null) {
	    LogUtil.debug(this.getClass(), "setMembershipTransactions 2 " + payableItem.getClientNo());
	    LogUtil.debug(this.getClass(), "setMembershipTransactions 3 " + transGroup != null ? "autorenewable:" + transGroup.isAutomaticallyRenewable() : "");
	    LogUtil.debug(this.getClass(), "setMembershipTransactions 4 " + payableItem.isDirectDebit());
	    // need to update the prime addressee transaction with the returned
	    // payable item ID.
	    // If the PAs transaction type is one of these then it does not get
	    // saved and the
	    // membership does not get saved so there is no point in setting the
	    // payable item ID
	    // or payment method ID.
	    MembershipTransactionVO paMemTransVO = transGroup.getMembershipTransactionForPA();
	    paMemTransVO.setPayableItem(payableItem);
	    updateMembershipTransaction(membershipMgrLocal, paMemTransVO, payableItem);

	    // need to update the prime addressee membership with the payment
	    // method id if
	    // it is direct debit and set to null if it isn't.
	    MembershipVO primeMemVO = paMemTransVO.getNewMembership();
	    // if direct debit
	    if (transGroup.isAutomaticallyRenewable() && payableItem.isDirectDebit()) {
		updatePaymentDetailsForMembership(membershipMgrLocal, primeMemVO, payableItem);
	    }
	}
	LogUtil.debug(this.getClass(), "setMembershipTransactions end");
    }

    /**
     * Update the membership with the direct debit authority ID
     * 
     * @param memVO
     *            Description of the Parameter - must be prime addressee
     * @param payableItem
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipVO updatePaymentDetailsForMembership(MembershipMgrLocal membershipMgrLocal, MembershipVO memVO, PayableItemVO payableItem) throws RemoteException {
	LogUtil.debug(this.getClass(), "updatePaymentDetailsForMembership start");
	PaymentMethod paymentMethod = null;
	paymentMethod = payableItem.getPaymentMethod();
	memVO.setReadOnly(false);
	LogUtil.debug(this.getClass(), "updatePaymentDetailsForMembership " + memVO + ": " + payableItem + " " + paymentMethod);
	if (paymentMethod != null && paymentMethod.isDirectDebitPaymentMethod()) {
	    DirectDebitSchedule ddSchedule = (DirectDebitSchedule) paymentMethod;
	    memVO.setDirectDebitAuthorityID(ddSchedule.getDirectDebitAuthorityID());
	} else {
	    memVO.setDirectDebitAuthorityID(null);
	}

	membershipMgrLocal.updateMembership(memVO);
	LogUtil.debug(this.getClass(), "updatePaymentDetailsForMembership end");
	return memVO;

    }

    /**
     * create the membership transaction NOTE: The membershipXML attribute is
     * not set here as, historically, it caused database locks.
     * 
     * @param memTransVO
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipTransactionVO createMembershipTransaction(MembershipMgrLocal membershipMgrLocal, MembershipTransactionVO memTransVO) throws RemoteException {
	LogUtil.debug(this.getClass(), "createMembershipTransaction start");

	LogUtil.debug(this.getClass(), "createMembershipTransaction oldMemVO=" + memTransVO.getMembership());
	MembershipVO newMemVO = memTransVO.getNewMembership();
	if (newMemVO == null) {
	    throw new SystemException("Failed to create membership transaction. A new version of the membership must be attached to the membership transaction before it can be saved.");
	}
	membershipMgrLocal.createMembershipTransaction(memTransVO);
	LogUtil.debug(this.getClass(), "createMembershipTransaction oldMemVO=" + memTransVO.getMembership());
	LogUtil.debug(this.getClass(), "createMembershipTransaction end");
	return memTransVO;
    }

    /**
     * Update the membership transaction record with payable item id
     * 
     * @param memTransVO
     *            Description of the Parameter
     * @param payableItem
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void updateMembershipTransaction(MembershipMgrLocal membershipMgrLocal, MembershipTransactionVO memTransVO, PayableItemVO payableItem) throws RemoteException {
	LogUtil.debug(this.getClass(), "updateMembershipTransaction start");
	memTransVO.setPayableItemID(payableItem.getPayableItemID());
	membershipMgrLocal.updateMembershipTransaction(memTransVO);
	LogUtil.debug(this.getClass(), "updateMembershipTransaction end");
    }

}
