package com.ract.membership;

import java.util.Hashtable;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemParameterVO;

public class ShowProcessRenewalFileAction extends ActionSupport implements Preparable {
    CommonMgr commonMgr;

    protected Hashtable<String, String> merge;
    protected Hashtable<String, String> email;
    protected Hashtable<String, String> update;
    protected Hashtable<String, String> test;
    protected Hashtable<String, String> count;

    protected String process;
    protected String emailOption;

    public static String PROCESS_MERGE_RENEWALS = "merge";
    public static String UPDATE_DOCS = "update";
    public static String TEST_EMAILS = "test";
    public static String COUNT_ONLY = "count";
    public static String PROCESS_EMAIL_RENEWALS = "emailRenewals";
    public final String SUBJECT_AUDIT_EMAIL = "Merge File Audit";

    @Override
    public void prepare() {
	commonMgr = CommonEJBHelper.getCommonMgrLocal();

	process = PROCESS_MERGE_RENEWALS;
	emailOption = TEST_EMAILS;

	email = new Hashtable<String, String>();
	email.put(PROCESS_EMAIL_RENEWALS, "Email Renewals");
	merge = new Hashtable<String, String>();
	merge.put(PROCESS_MERGE_RENEWALS, "Merge Renewal Files");

	String overrideEmail = null;
	String testLimitString = null;
	try {
	    overrideEmail = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "RENEWALEMAILOVERRIDE");
	    testLimitString = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "TESTEMAILLIMIT");
	} catch (Exception e) {
	    // do nothing
	}

	test = new Hashtable<String, String>();
	test.put(TEST_EMAILS, "Send the first " + testLimitString + " emails in a renewal file to the override email address (" + overrideEmail + ") - for testing purposes only.");
	update = new Hashtable<String, String>();
	update.put(UPDATE_DOCS, "Send email renewals to customers (Update email date of documents).");
	count = new Hashtable<String, String>();
	count.put(COUNT_ONLY, "Renewal file counts.");
    }

    @Override
    public String execute() throws Exception {

	return SUCCESS;
    }

    public String getProcess() {
	return process;
    }

    public void setProcess(String process) {
	this.process = process;
    }

    public Hashtable<String, String> getUpdate() {
	return update;
    }

    public void setUpdate(Hashtable<String, String> update) {
	this.update = update;
    }

    public Hashtable<String, String> getTest() {
	return test;
    }

    public void setTest(Hashtable<String, String> test) {
	this.test = test;
    }

    public String getEmailOption() {
	return emailOption;
    }

    public void setEmailOption(String emailOption) {
	this.emailOption = emailOption;
    }

    public Hashtable<String, String> getCount() {
	return count;
    }

    public void setCount(Hashtable<String, String> count) {
	this.count = count;
    }

    public Hashtable<String, String> getMerge() {
	return merge;
    }

    public void setMerge(Hashtable<String, String> merge) {
	this.merge = merge;
    }

    public Hashtable<String, String> getEmail() {
	return email;
    }

    public void setEmail(Hashtable<String, String> email) {
	this.email = email;
    }
}