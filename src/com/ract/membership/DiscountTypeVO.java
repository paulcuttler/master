package com.ract.membership;

import java.rmi.RemoteException;
import java.util.Collection;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;

/**
 * Specifies a type of discount. The discount can be specified as a fixed amount
 * or as a percentage. Some discounts are controlled by the system and cannot be
 * changed by the user. Some discounts are automatically applied. Some discounts
 * can be attached to a membership so that they apply every time a transaction
 * is done on the membership.
 * 
 * @hibernate.class table="mem_discount" lazy="false"
 * 
 * @author T. Bakker. created 1 August 2002
 */
public class DiscountTypeVO extends ValueObject implements Writable, CachedItem, Comparable {

    /**
     * @hibernate.property
     * @hibernate.column name="reduces_membership_value"
     */
    public boolean isMembershipValueReducing() {
	return membershipValueReducing;
    }

    public void setMembershipValueReducing(boolean membershipValueReducing) {
	this.membershipValueReducing = membershipValueReducing;
    }

    /**
     * A constant for the active status of the discount type.
     */
    public static final String STATUS_Active = "Active";

    /**
     * A constant for the inactive status of the discount type.
     */
    public static final String STATUS_Inactive = "Inactive";

    // ************** Automatic discount types ****************
    /**
     * A constant for a credit type of discount. This discount type is used when
     * a credsit amount held against the membership can be applied to the next
     * transaction.
     */
    public static final String TYPE_CREDIT = "Credit";

    /**
     * A constant for an unearned type of credit which is treated as a discount.
     * This discount type is used when the unearned portion of a membership is
     * creditied to a transaction.
     */
    public static final String TYPE_UNEARNED_CREDIT = "Unearned Credit";

    /**
     * A constant for the Loyalty discount type. This discount type requires
     * special processing in the fee calculator.
     */
    public static final String TYPE_LOYALTY = "Loyalty"; // discount type code
							 // must start with
							 // Loyalty

    /**
     * A constant for Life discount type.
     */
    public static final String TYPE_LIFE = "Life";

    /**
     * A constant for the Rewards discount type. This discount type requires
     * special processing in the fee calculator.
     */
    public static final String TYPE_REWARD = "Reward";

    /**
     * The discount type to represent transfer credit.
     */
    public static final String TYPE_TRANSFER_CREDIT = "Transfer Credit";

    /**
     * A discount available to RACT staff members. This discount will be applied
     * if the membership has the RACT Staff profile.
     */
    public static final String TYPE_RACT_STAFF = "RACT Staff";

    /**
     * A discount available to ISCU staff members. This discount will be applied
     * if the membership has the ISCU Staff profile.
     */
    public static final String TYPE_ISCU_STAFF = "ISCU Staff";

    /**
     * A discount available to Honorary members. This discount will be applied
     * if the membership has the Honorary profile.
     */
    public static final String TYPE_HONORARY = "Honorary";

    /**
     * An automatic discount applied to the join fee for affiliated club
     * transfer.
     */
    public static final String TYPE_TRANSFER_JOIN = "Transfer-J";

    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "DiscountType";

    /**
     * Automatic discount for driver training package 2nd year
     */
    public final static String TYPE_DRIVER_TRAINING_YEAR_2 = "DriverTrainingYear2";

    /**
     * Discount for Driver training lesson package (year 1)
     */
    public final static String TYPE_DRIVER_TRAINING = "Driver training lesson package";

    /**
     * A unique code that identifies the discount. may be used for display.
     */
    protected String discountTypeCode;

    /**
     * A description of the discount and how it is used.
     */
    protected String description;

    /**
     * Indicates if this discount type is controlled by the system. If it is
     * then the user cannot delete it. However, they can still change the
     * discount values. This value will be set when the discount type data is
     * loaded into the database and can only be changed by changing the data in
     * the database.
     */
    protected boolean systemControlled;

    /**
     * Indicates if this discount type can be attached to a membership so that
     * it applies to every transaction done on the membership. Recurring
     * discounts and Automatic discounts are mutually exclusive.
     */
    protected boolean recurringDiscount;

    /**
     * Indicates if the discount is automatically applied to the transaction for
     * all members. That is, the user does not have to select it every time a
     * transaction is performed. Sticky (recurring) discounts and Automatic
     * discounts are mutually exclusive.
     */
    protected boolean automaticDiscount;

    /**
     * Indicates if this discount can cause the fee to go below the minimum fee.
     * True means that is cannot cause the fee to go below the minimum fee.
     * False means that is can cause the fee to go below the minimum fee.
     */
    protected boolean minimumFeeApplicable;

    /**
     * Indicates if the discount type can be applied. Set to "Active" to allow
     * the discount to be applied. Set to "Inactive" to stop the discount from
     * being applied.
     */
    protected String status;

    /**
     * A fixed dollar amount to subtract from the transaction.
     */
    protected double discountAmount;

    /**
     * A percentage used to calculate an amount to be subtracted from the
     * transaction. Valid range is 0 to 1. e.g. 0.5 = 50% discount.
     */
    protected double discountPercentage;

    /**
     * Where the discount is related to a different product this holds that
     * product.
     */
    protected String percentageOf;
    /**
     * A period of free membership that is to be applied as part of the
     * discount.
     */
    protected Interval discountPeriod;

    /**
     * If specified, the account that the discount amount is posted to when
     * transfering the payable fee to the payment system.
     */
    protected Integer discountAccountID;

    private MembershipAccountVO discountAccount;

    /**
     * The date on which the discount type may be used on transactions. Use to
     * specify a special promotion start date.
     */
    protected DateTime effectiveFromDate;

    /**
     * The date on which the discount may no longer be used on transactions. Use
     * to specify a special promotion end date.
     */
    protected DateTime effectiveToDate;

    /**
     * Indicates whether this discount reduces the value of the membership as
     * well as the amount payable.
     */
    protected boolean membershipValueReducing;

    /**
     * Indicates whether this discount is used to offset part or all of the
     * amount payable to another account, replacing the clearing account. This
     * is identified by the discount having an associated account.
     */
    // protected boolean isOffsetDiscount;

    protected Collection feeTypeList;

    /**
     * Constructors *****************************
     */

    public Collection getDiscountAttributes() throws RemoteException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	return membershipMgr.getDiscountAttributes(this.getDiscountTypeCode());
    }

    /**
     * Default constructor.
     */
    public DiscountTypeVO() {
    }

    public DiscountTypeVO(String discountTypeCode, String description, boolean systemControlled, boolean recurringDiscount, boolean automaticDiscount, boolean minimumFeeApplicable, String status, double discountAmount, double discountPercentage, Interval discountPeriod, Integer discountAccountID, DateTime effectiveFromDate, DateTime effectiveToDate, boolean membershipValueReducing) {
	this.discountTypeCode = discountTypeCode;
	this.description = description;
	this.systemControlled = systemControlled;
	this.recurringDiscount = recurringDiscount;
	this.automaticDiscount = automaticDiscount;
	this.minimumFeeApplicable = minimumFeeApplicable;
	this.status = status;
	this.discountAmount = discountAmount;
	this.discountPercentage = discountPercentage;
	this.discountPeriod = discountPeriod;
	this.discountAccountID = discountAccountID;
	this.effectiveFromDate = effectiveFromDate;
	this.effectiveToDate = effectiveToDate;
	this.membershipValueReducing = membershipValueReducing;
    }

    /**
     * @hibernate.id column="discount_type_code" generator-class="assigned"
     * 
     * @return The discountTypeCode value
     */
    public String getDiscountTypeCode() {
	return this.discountTypeCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="description"
     */
    public String getDescription() {
	return this.description;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="system_controlled"
     */
    public boolean isSystemControlled() {
	return this.systemControlled;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="recurring_discount"
     */
    public boolean isRecurringDiscount() {
	return this.recurringDiscount;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="automatic_discount"
     */
    public boolean isAutomaticDiscount() {
	return this.automaticDiscount;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="minimum_fee_applies"
     */
    public boolean isMinimumFeeApplicable() {
	return this.minimumFeeApplicable;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="discount_type_status"
     */
    public String getStatus() {
	return this.status;
    }

    public boolean isNotInEffect() {
	return !isInEffect();
    }

    /**
     * returns true if the discount type is Active and today is between the
     * effective dates
     * 
     * @return true if in effect - false otherwise
     */
    public boolean isInEffect() {
	DateTime fromDate = new DateTime();
	DateTime toDate = new DateTime();
	DateTime today = new DateTime();

	try {
	    fromDate = this.getEffectiveFromDate() == null ? new DateTime("01-jan-1901") : this.getEffectiveFromDate();
	    toDate = this.getEffectiveToDate() == null ? new DateTime("31-dec-3999") : this.getEffectiveToDate();

	} catch (Exception e) {
	    // dates up the creek. Do nothing
	}

	return (((today.onOrAfterDay(fromDate)) && (today.onOrBeforeDay(toDate))) && this.status.equalsIgnoreCase(STATUS_Active));

    }

    /**
     * @hibernate.property
     * @hibernate.column name="discount_amount"
     */
    public double getDiscountAmount() {
	return this.discountAmount;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="discount_percentage"
     */
    public double getDiscountPercentage() {
	return this.discountPercentage;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="percentage_of"
     */
    public String getPercentageOf() {
	return this.percentageOf;
    }

    public void setPercentageOf(String pOf) {
	this.percentageOf = pOf;
    }

    /**
     * Returns the discount period. If the discount type is an automatic type of
     * discount then no discount period is applicable (whether it has been set
     * or not) and a null is returned.
     * 
     * @hibernate.property
     * @hibernate.column name="discount_period"
     * 
     * @return The discountPeriod value
     */
    public Interval getDiscountPeriod() {
	return discountPeriod;

	// if(this.automaticDiscount)
	// {
	// return null;
	// }
	// else
	// {
	// return this.discountPeriod;
	// }
    }

    /**
     * @hibernate.property
     * @hibernate.column name="account_id"
     */
    public Integer getDiscountAccountID() {
	return this.discountAccountID;
    }

    /**
     * Return the discount account associated with this discount type. Return
     * null if no discount account is associated.
     * 
     * @return The discountAccount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipAccountVO getDiscountAccount() throws RemoteException {
	if (this.discountAccount == null && this.discountAccountID != null && this.discountAccountID.intValue() > 0) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.discountAccount = refMgr.getMembershipAccount(this.discountAccountID);
	}
	return this.discountAccount;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="effective_from_date"
     */
    public DateTime getEffectiveFromDate() {
	return this.effectiveFromDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="effective_to_date"
     */
    public DateTime getEffectiveToDate() {
	return this.effectiveToDate;
    }

    public boolean isOffsetDiscount() {
	MembershipAccountVO account = null;
	try {
	    account = this.getDiscountAccount();
	} catch (Exception ex) {
	    LogUtil.warn(this.getClass(), "Unable to get discount account." + ex);
	}
	return (account != null);
    }

    public Collection getFeeTypeList() throws RemoteException {
	if (this.feeTypeList == null) {
	    this.feeTypeList = MembershipEJBHelper.getMembershipRefMgr().getDiscountFeeTypeList(this.discountTypeCode);
	}

	return feeTypeList;
    }

    /**
     * Setter methods *************************
     * 
     * @param discountTypeCode
     *            The new discountTypeCode value
     * @exception RemoteException
     *                Description of the Exception
     */

    public void setDiscountTypeCode(String discountTypeCode) throws RemoteException {
	checkReadOnly();
	this.discountTypeCode = discountTypeCode;
    }

    /**
     * Sets the description attribute of the DiscountTypeVO object
     * 
     * @param description
     *            The new description value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    public void setFeeTypeList(Collection newList) throws RemoteException {
	checkReadOnly();
	this.feeTypeList = newList;
    }

    /**
     * Sets the systemControlled attribute of the DiscountTypeVO object
     * 
     * @param systemControlled
     *            The new systemControlled value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setSystemControlled(boolean systemControlled) throws RemoteException {
	checkReadOnly();
	this.systemControlled = systemControlled;
    }

    /**
     * Sets the recurring attribute of the DiscountTypeVO object
     * 
     * @param recurring
     *            The new recurring value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setRecurringDiscount(boolean recurringDiscount) throws RemoteException {
	checkReadOnly();
	this.recurringDiscount = recurringDiscount;
    }

    /**
     * Sets the automaticDiscount attribute of the DiscountTypeVO object
     * 
     * @param automaticDiscount
     *            The new automaticDiscount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setAutomaticDiscount(boolean automaticDiscount) throws RemoteException {
	checkReadOnly();
	this.automaticDiscount = automaticDiscount;
    }

    /**
     * Sets the minimumFeeApplicable attribute of the DiscountTypeVO object
     * 
     * @param minimumFeeApplies
     *            The new minimumFeeApplicable value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setMinimumFeeApplicable(boolean minimumFeeApplies) throws RemoteException {
	checkReadOnly();
	this.minimumFeeApplicable = minimumFeeApplies;
    }

    /**
     * Sets the status attribute of the DiscountTypeVO object
     * 
     * @param status
     *            The new status value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setStatus(String status) throws RemoteException {
	checkReadOnly();
	this.status = status;
    }

    /**
     * Sets the discountAmount attribute of the DiscountTypeVO object
     * 
     * @param discountAmount
     *            The new discountAmount value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDiscountAmount(double discountAmount) throws RemoteException {
	checkReadOnly();
	this.discountAmount = discountAmount;
    }

    /**
     * Sets the discountPercentage attribute of the DiscountTypeVO object
     * 
     * @param discountPercentage
     *            The new discountPercentage value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDiscountPercentage(double discountPercentage) throws RemoteException {
	checkReadOnly();
	this.discountPercentage = discountPercentage;
    }

    /**
     * Sets the discountPeriod attribute of the DiscountTypeVO object
     * 
     * @param discPeriodSpec
     *            The new discountPeriod value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDiscountPeriod(String discPeriodSpec) throws RemoteException {
	checkReadOnly();
	Interval discPeriod = null;
	if (discPeriodSpec != null) {
	    try {
		discPeriod = new Interval(discPeriodSpec);
	    } catch (Exception e) {
		throw new RemoteException(e.getMessage());
	    }
	}
	setDiscountPeriod(discPeriod);
    }

    /**
     * Sets the discountPeriod attribute of the DiscountTypeVO object
     * 
     * @param discPeriod
     *            The new discountPeriod value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDiscountPeriod(Interval discPeriod) throws RemoteException {
	checkReadOnly();
	this.discountPeriod = discPeriod;
    }

    /**
     * Sets the discountAccountID attribute of the DiscountTypeVO object
     * 
     * @param discountAccountID
     *            The new discountAccountID value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDiscountAccountID(Integer discountAccountID) throws RemoteException {
	checkReadOnly();
	this.discountAccountID = discountAccountID;
	this.discountAccount = null;
    }

    /**
     * Sets the effectiveFromDate attribute of the DiscountTypeVO object
     * 
     * @param effectiveFromDate
     *            The new effectiveFromDate value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setEffectiveFromDate(DateTime effectiveFromDate) throws RemoteException {
	checkReadOnly();
	this.effectiveFromDate = effectiveFromDate;
    }

    /**
     * Sets the effectiveToDate attribute of the DiscountTypeVO object
     * 
     * @param effectiveToDate
     *            The new effectiveToDate value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setEffectiveToDate(DateTime effectiveToDate) throws RemoteException {
	checkReadOnly();
	this.effectiveToDate = effectiveToDate;
    }

    /**
     * String representation
     */
    public String toString() {
	StringBuffer description = new StringBuffer();
	description.append(this.discountTypeCode + ", ");
	description.append(this.discountAccountID + ", ");
	// description.append(this.isOffsetDiscount+", ");
	description.append(this.membershipValueReducing + ", ");
	description.append(this.discountAmount);
	return description.toString();
    }

    /**
     * Description of the Method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
    }

    /**
     * CachedItem interface methods *********************
     * 
     * @return The key value
     */

    /**
     * Return the membership transaction type code as the cached item key.
     * 
     * @return The key value
     */
    public String getKey() {
	return this.getDiscountTypeCode();
    }

    /**
     * Determine if the specified key matches the discount type code. Return
     * true if both the key and the discount type code are null.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public void write(ClassWriter cw) throws RemoteException {
	write(cw, false);
    }

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw, boolean deepWrite) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("automaticDiscount", this.automaticDiscount);
	cw.writeAttribute("description", this.description);
	if (this.discountAccountID != null && this.discountAccountID.intValue() > 0) {
	    cw.writeAttribute("discountAccount", this.discountAccountID);
	}
	if (this.discountAmount > 0.0) {
	    cw.writeAttribute("discountAmount", this.discountAmount);
	}
	if (this.discountPercentage > 0.0) {
	    cw.writeAttribute("discountPercentage", this.discountPercentage);
	}
	if (this.discountPeriod != null) {
	    cw.writeAttribute("discountPeriod", this.discountPeriod);
	}
	cw.writeAttribute("discountTypeCode", this.discountTypeCode);
	if (this.effectiveFromDate != null) {
	    cw.writeAttribute("effectiveFromDate", this.effectiveFromDate);
	}
	if (this.effectiveToDate != null) {
	    cw.writeAttribute("effectiveToDate", this.effectiveToDate);
	}
	cw.writeAttribute("minimumFeeApplicable", this.minimumFeeApplicable);
	cw.writeAttribute("recurringDiscount", this.recurringDiscount);
	cw.writeAttribute("status", this.status);
	cw.writeAttribute("systemControlled", this.systemControlled);

	cw.endClass();
    }

    /************************ Comparable interface methods **********************/
    /**
     * Compare this product sort order to another products sort order
     */
    public int compareTo(Object obj) {
	if (obj != null && obj instanceof DiscountTypeVO) {
	    DiscountTypeVO discountVO = (DiscountTypeVO) obj;
	    return this.discountTypeCode.compareToIgnoreCase(discountVO.getDiscountTypeCode());
	} else {
	    return 0;
	}
    }

    /**
     * If the passed discount type has the same discount type code as this
     * instances discount type code then the two are equal.
     */
    public boolean equals(Object obj) {
	if (obj != null && obj instanceof DiscountTypeVO) {
	    DiscountTypeVO that = (DiscountTypeVO) obj;
	    return (this.discountTypeCode.compareToIgnoreCase(that.getDiscountTypeCode()) == 0);
	}

	return false;
    }

}
