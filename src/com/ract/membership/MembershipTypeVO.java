package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ract.common.CachedItem;
import com.ract.common.ClassWriter;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;

/**
 * The type of membership issued. This relates to what receives the membership
 * benefit. For a "Personal" membership type the benefit is received by an
 * individual person regardless of the vehicle they drive. For a "Fleet"
 * membership type the benefit is received by a vehicle regardless of the person
 * driving the vehicle.
 * 
 * @author T. Bakker.
 * @see MembershipType EJB
 */
@Entity
@Table(name = "mem_membership_type")
public class MembershipTypeVO extends ValueObject implements Writable, CachedItem, Comparable {

    // These constants define the allowed membership types.
    public static final String MEMBERSHIP_TYPE_PERSONAL = "Personal";
    public static final String MEMBERSHIP_TYPE_PERSONAL_ABBREV = "P";

    public static final String MEMBERSHIP_TYPE_FLEET = "Fleet";
    public static final String MEMBERSHIP_TYPE_FLEET_ABBREV = "F";
    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "MembershipType";

    /**
     * A unique string that identifies each type of membership. This can be used
     * as the display name for the membership type. e.g. "Personal", "Fleet".
     */
    @Id
    @Column(name = "membership_type_code")
    protected String membershipTypeCode;

    /**
     * A description of what this membership type is used for.
     */
    protected String description;

    /**
     * Indicates if this membership type is allowed to have vehicles attached to
     * it.
     */
    @Column(name = "vehicles_allowed")
    protected boolean vehiclesAllowed;

    /**
     * Indicates if this membership type must have vehicles attached to it.
     */
    @Column(name = "vehicles_required")
    protected boolean vehiclesRequired;

    /**
     * Indicates if multiple memberships are allowed.
     */
    @Column(name = "multiple_allowed")
    protected boolean multipleMembershipsAllowed;

    /**
     * Indicates if group memberships type are allowed.
     */
    @Column(name = "group_allowed")
    protected boolean groupMembershipAllowed;

    /**
     * Indicates if this type is the default that should be selected.
     */
    @Column(name = "is_default")
    protected boolean isDefault = false;

    /**
     * Indicates the preferred sort order when displaying lists of membership
     * types
     */
    @Column(name = "sort_order")
    protected int sortOrder = 0;

    /**
     * A list of client types that are allowed to take out this type of
     * membership.
     */
    @Transient
    protected ArrayList allowedClientTypeList;

    /**
     * A list of base product types that are allowed to be issued to this
     * membership type.
     */
    @Transient
    protected ArrayList allowedProductList;

    /**
     * Holds a reference to the membership reference manager.
     */
    // private MembershipRefMgr myMembershipRefMgr = null;

    /**
     * Constructors ******************************
     */

    /**
     * Default constructor. Required by MembershipBean which extends this class.
     */
    public MembershipTypeVO() {
    }

    /**
     * Construct a new membership type with the given data.
     * 
     * @param membershipTypeCode
     *            Description of the Parameter
     */
    public MembershipTypeVO(String membershipTypeCode) {
	this.membershipTypeCode = membershipTypeCode;
    }

    /**
     * Initialise the membership type with the data from the XML element.
     * 
     * @return The membershipTypeCode value
     */
    // public MembershipTypeVO(String membershipTypeXML)
    // {
    //
    // }

    /**
     * Getter methods *****************************
     * 
     * @return The membershipTypeCode value
     */

    public String getMembershipTypeCode() {
	return this.membershipTypeCode;
    }

    /**
     * Gets the description attribute of the MembershipTypeVO object
     * 
     * @return The description value
     */
    public String getDescription() {
	return this.description;
    }

    /**
     * Gets the allowedClientTypeList attribute of the MembershipTypeVO object
     * 
     * @return The allowedClientTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getAllowedClientTypeList() throws RemoteException {
	if (this.allowedClientTypeList == null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.allowedClientTypeList = new ArrayList(refMgr.getClientTypeListByMembershipType(this.membershipTypeCode));
	}
	return this.allowedClientTypeList;
    }

    /**
     * Gets the allowedProductList attribute of the MembershipTypeVO object
     * 
     * @return The allowedProductList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public ArrayList getAllowedProductList() throws RemoteException {
	if (this.allowedProductList == null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.allowedProductList = new ArrayList(refMgr.getProductListByMembershipType(this.membershipTypeCode));
	}
	return this.allowedProductList;
    }

    /**
     * Gets the multipleMembershipsAllowed attribute of the MembershipTypeVO
     * object
     * 
     * @return The multipleMembershipsAllowed value
     */
    public boolean isMultipleMembershipsAllowed() {
	return this.multipleMembershipsAllowed;
    }

    /**
     * Gets the groupMembershipAllowed attribute of the MembershipTypeVO object
     * 
     * @return The groupMembershipAllowed value
     */
    public boolean isGroupMembershipAllowed() {
	return this.groupMembershipAllowed;
    }

    /**
     * Gets the sortOrder attribute of the MembershipTypeVO object
     * 
     * @return The sortOrder value
     */
    public int getSortOrder() {
	return this.sortOrder;
    }

    /**
     * Gets the default attribute of the MembershipTypeVO object
     * 
     * @return The default value
     */
    public boolean isDefault() {
	return this.isDefault;
    }

    /**
     * Gets the vehiclesAllowed attribute of the MembershipTypeVO object
     * 
     * @return The vehiclesAllowed value
     */
    public boolean isVehiclesAllowed() {
	return this.vehiclesAllowed;
    }

    /**
     * Gets the vehiclesRequired attribute of the MembershipTypeVO object
     * 
     * @return The vehiclesRequired value
     */
    public boolean isVehiclesRequired() {
	return this.vehiclesRequired;
    }

    /**
     * Setter methods *****************************
     * 
     * @param description
     *            The new description value
     * @exception RemoteException
     *                Description of the Exception
     */

    public void setDescription(String description) throws RemoteException {
	checkReadOnly();
	this.description = description;
    }

    /**
     * Set the flag that indicates if this is the membership type that should be
     * selected by default.
     * 
     * @param isDefault
     *            The new default value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setDefault(boolean isDefault) throws RemoteException {
	checkReadOnly();
	this.isDefault = isDefault;
    }

    /**
     * Sets the sortOrder attribute of the MembershipTypeVO object
     * 
     * @param sOrder
     *            The new sortOrder value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setSortOrder(int sOrder) throws RemoteException {
	checkReadOnly();
	this.sortOrder = sOrder;
    }

    /**
     * Sets the vehiclesAllowed attribute of the MembershipTypeVO object
     * 
     * @param allowed
     *            The new vehiclesAllowed value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setVehiclesAllowed(boolean allowed) throws RemoteException {
	checkReadOnly();
	this.vehiclesAllowed = allowed;
    }

    /**
     * Sets the vehiclesRequired attribute of the MembershipTypeVO object
     * 
     * @param required
     *            The new vehiclesRequired value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setVehiclesRequired(boolean required) throws RemoteException {
	checkReadOnly();
	this.vehiclesRequired = required;
    }

    /**
     * Sets the allowedClientTypeList attribute of the MembershipTypeVO object
     * 
     * @param allowedClientTypeList
     *            The new allowedClientTypeList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setAllowedClientTypeList(ArrayList allowedClientTypeList) throws RemoteException {
	checkReadOnly();
	this.allowedClientTypeList = allowedClientTypeList;
    }

    /**
     * Sets the allowedProductList attribute of the MembershipTypeVO object
     * 
     * @param allowedProductList
     *            The new allowedProductList value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setAllowedProductList(ArrayList allowedProductList) throws RemoteException {
	checkReadOnly();
	this.allowedProductList = allowedProductList;
    }

    /**
     * Sets the multipleMembershipsAllowed attribute of the MembershipTypeVO
     * object
     * 
     * @param multiMemAllowed
     *            The new multipleMembershipsAllowed value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setMultipleMembershipsAllowed(boolean multiMemAllowed) throws RemoteException {
	checkReadOnly();
	this.multipleMembershipsAllowed = multiMemAllowed;
    }

    /**
     * Sets the groupMembershipAllowed attribute of the MembershipTypeVO object
     * 
     * @param groupMemAllowed
     *            The new groupMembershipAllowed value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setGroupMembershipAllowed(boolean groupMemAllowed) throws RemoteException {
	checkReadOnly();
	this.groupMembershipAllowed = groupMemAllowed;
    }

    /**
     * Description of the Method
     * 
     * @param newMode
     *            Description of the Parameter
     * @exception ValidationException
     *                Description of the Exception
     */
    protected void validateMode(String newMode) throws ValidationException {
    }

    /**
     * CachedItem interface methods ***************
     * 
     * @return The key value
     */

    /**
     * Return the product benefit type code as the cached item key.
     * 
     * @return The key value
     */
    public String getKey() {
	return this.getMembershipTypeCode();
    }

    /**
     * Determine if the specified key matches the product code. Return true if
     * both the key and the product code are null.
     * 
     * @param key
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public boolean keyEquals(String key) {
	String myKey = this.getKey();
	if (key == null) {
	    // See if the code is also null.
	    if (myKey == null) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    // We now know the key is not null so this wont throw a null pointer
	    // exception.
	    return key.equalsIgnoreCase(myKey);
	}
    }

    /**
     * Comparable interface methods ******************
     * 
     * @param obj
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    /**
     * Compare this membership type sort order to another membership types sort
     * order
     * 
     * @param obj
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int compareTo(Object obj) {
	if (obj instanceof MembershipTypeVO) {
	    MembershipTypeVO memTypeVO = (MembershipTypeVO) obj;
	    if (this.sortOrder < memTypeVO.getSortOrder()) {
		return -1;
	    } else if (this.sortOrder > memTypeVO.getSortOrder()) {
		return 1;
	    } else {
		return 0;
	    }
	} else {
	    return 0;
	}
    }

    /**
     * Return a reference to the membership reference manager session bean.
     * Initialise the reference if this is the first call.
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */
    // public MembershipRefMgr getMembershipRefMgr() throws RemoteException
    // {
    // if (this.myMembershipRefMgr == null)
    // {
    // this.myMembershipRefMgr = MembershipEJBHelper.getMembershipRefMgr();
    // }
    // return this.myMembershipRefMgr;
    // }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("description", this.description);
	cw.writeAttribute("groupMembershipAllowed", this.groupMembershipAllowed);
	cw.writeAttribute("isDefault", this.isDefault);
	cw.writeAttribute("membershipTypeCode", this.membershipTypeCode);
	cw.writeAttribute("multipleMembershipsAllowed", this.multipleMembershipsAllowed);
	cw.writeAttribute("sortOrder", this.sortOrder);
	cw.writeAttribute("vehiclesAllowed", this.vehiclesAllowed);
	cw.writeAttribute("vehiclesRequired", this.vehiclesRequired);
	cw.writeAttributeList("allowedClientTypeList", getAllowedClientTypeList());
	cw.writeAttributeList("allowedProductList", getAllowedProductList());
	cw.endClass();
    }

}