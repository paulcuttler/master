package com.ract.membership;

/**
 * 
 * Attributes that are associated with a discount. eg. GM Client Number.
 * 
 * @hibernate.class table="mem_discount_attribute" lazy="false"
 * 
 * @author not attributable
 * @version 1.0
 */
public class MembershipDiscountAttribute {

    public final static String ATTRIBUTE_GM_MEMBERSHIP_NUMBER = "GM Membership Number";

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public MembershipDiscountAttributePK getMembershipDiscountAttributePK() {
	return membershipDiscountAttributePK;
    }

    /**
     * @hibernate.property column="attribute_description"
     * @return String
     */
    public String getAttributeDescription() {
	return attributeDescription;
    }

    /**
     * @hibernate.property column="mandatory"
     * @return String
     */
    public boolean isMandatory() {
	return mandatory;
    }

    public void setMembershipDiscountAttributePK(MembershipDiscountAttributePK membershipDiscountAttributePK) {
	this.membershipDiscountAttributePK = membershipDiscountAttributePK;
    }

    public void setAttributeDescription(String attributeDescription) {
	this.attributeDescription = attributeDescription;
    }

    public void setMandatory(boolean mandatory) {
	this.mandatory = mandatory;
    }

    public MembershipDiscountAttribute() {
    }

    private String attributeDescription;

    private MembershipDiscountAttributePK membershipDiscountAttributePK;

    private boolean mandatory;

    public String toString() {
	return this.getMembershipDiscountAttributePK().toString() + " " + this.attributeDescription;
    }

}
