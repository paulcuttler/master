package com.ract.membership;

import java.util.Hashtable;

import com.ract.common.SystemParameterVO;
import com.ract.util.StringUtil;

/**
 * Display the Email Advice form.
 * 
 * @author newtong
 * 
 */
public class EmailAdviceFormAction extends EmailAdviceBaseAction {

    private String emailTo;
    private String message;

    @Override
    public String execute() throws Exception {

	if (this.getClientVO() != null) {
	    this.setEmailTo(this.getClientVO().getEmailAddress());
	}

	this.setMessage(this.retrieveMessage());

	return SUCCESS;
    }

    private String retrieveMessage() throws Exception {
	String msg = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, this.getAdviceType() + "EMAILMESSAGE");
	String fromName = this.getUser().getUsername();
	String fromTitle = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, this.getAdviceType() + "EMAILFROMTITLE");

	Hashtable<String, String> var = new Hashtable<String, String>();
	
	// In tone update for Oct 2020, the first name is now used in the email body (replaces addressTitle = Title + surname)
	if (this.getClientVO() != null) {
		String camelLCFirstName = StringUtil.toProperCase(this.getClientVO().getFirstNameOnly().toLowerCase());
		var.put("firstName", camelLCFirstName);
	} else {
	    var.put("firstName", "Sir or Madam");
	}
	var.put("fromName", fromName);
	var.put("fromTitle", fromTitle);

	// email parameters
	if (msg != null) {
	    msg = StringUtil.replaceTag(msg, "<", ">", var);
	    msg = StringUtil.stripHTML(msg);
	}

	return msg;
    }

    public void setEmailTo(String emailTo) {
	this.emailTo = emailTo;
    }

    public String getEmailTo() {
	return emailTo;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    }
}
