package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import com.ract.common.CacheBase;
import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;

/**
 * Cache of fee specifications
 * 
 * @author Terry Bakker, Leigh Giles
 * @created 12 March 2003
 * @version 1.0
 */

public class FeeSpecificationCache extends CacheBase {
    private static FeeSpecificationCache instance = new FeeSpecificationCache();

    private Hashtable keyedFeeList = null;

    public FeeSpecificationCache() {
	this.keyedFeeList = new Hashtable();
    }

    /**
     * Return a reference the instance of this class.
     * 
     * @return The instance value
     */
    public static FeeSpecificationCache getInstance() {
	return instance;
    }

    // this bit of code does not appear ever to be used
    public void initialiseList() throws RemoteException {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	Collection dataList;
	Collection allowableDiscountList;

	try {
	    dataList = membershipMgr.getFeeSpecifications();
	    if (dataList != null) {
		Iterator<FeeSpecificationVO> it = dataList.iterator();
		while (it.hasNext()) {
		    FeeSpecificationVO vo = it.next();
		    LogUtil.debug(this.getClass(), "\ninitialiseList() effective from date " + vo.effectiveFromDate + "\ninitialiseList() feePerMember =      " + vo.feePerMember + "\ninitialiseList() feeTypeCode =       " + vo.feeTypeCode + "\ninitialiseList() productCode =       " + vo.productCode + "\ninitialiseList() ID =                " + vo.feeSpecificationID);
		    // Cause the list of allowable discounts to be loaded as
		    // well.
		    allowableDiscountList = vo.getAllowableDiscountList();
		    vo.setReadOnly(true);
		    add(vo);
		}
		this.keyedFeeList.clear();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Return the Fee specification value object.
     * 
     * @param feeSpecificationId
     *            Description of the Parameter
     * @return The feeSpecification value
     * @exception RemoteException
     *                Description of the Exception
     */
    public FeeSpecificationVO getFeeSpecification(Integer feeSpecificationId) throws RemoteException {
	return (FeeSpecificationVO) getItem(feeSpecificationId.toString());
    }

    public ArrayList getFeeSpecificationList() throws RemoteException {
	return getItemList();
    }

    /**
     * Return the list of fees, as FeeSpecificationVO, that apply for the
     * product when doing the specified transaction type on the specified
     * membership type with the specified number of memberships in the group and
     * as at the effective date.
     * 
     * @param productCode
     *            Description of the Parameter
     * @param productBenefitCode
     *            Description of the Parameter
     * @param memTypeCode
     *            Description of the Parameter
     * @param transTypeCode
     *            Description of the Parameter
     * @param numberInGroup
     *            Description of the Parameter
     * @param effectiveDate
     *            Description of the Parameter
     * @return The fee value
     * @exception RemoteException
     *                Description of the Exception
     */
    public Collection getFee(String productCode, String productBenefitCode, String memTypeCode, String transTypeCode, int numberInGroup, DateTime effectiveDate) throws RemoteException {
	LogUtil.debug(this.getClass(), "getFee productCode" + productCode);
	LogUtil.debug(this.getClass(), "getFee productBenefitCode" + productBenefitCode);
	LogUtil.debug(this.getClass(), "getFee memTypeCode" + memTypeCode);
	LogUtil.debug(this.getClass(), "getFee transTypeCode" + transTypeCode);
	LogUtil.debug(this.getClass(), "getFee numberInGroup" + numberInGroup);
	LogUtil.debug(this.getClass(), "getFee effectiveDate" + effectiveDate);
	// Try and find the fees in the keyed fee cache first
	String key = buildKey(productCode, productBenefitCode, transTypeCode, memTypeCode, numberInGroup);
	// LogUtil.debug(this.getClass(),"Looking in cache with key : " + key);
	Collection feeList = (ArrayList) this.keyedFeeList.get(key);
	// If we couldn't find the fees in the keyed fee cache get the fees
	// from the full fee list.
	if (feeList == null) {
	    LogUtil.debug(this.getClass(), "Looking in fee list.");
	    // Search for fees specified for the number of people in the group.
	    // If not found then search for fees specified for fewer people in
	    // the
	    // group until we get to zero group members.
	    feeList = findFeeByDiminishingGroupNumber(productCode, productBenefitCode, memTypeCode, transTypeCode, numberInGroup);

	    // If we could not find a fee for the specific membership type then
	    // try and find a fee for all membership types.
	    if (feeList == null || feeList.isEmpty()) {
		feeList = findFeeByDiminishingGroupNumber(productCode, productBenefitCode, null, transTypeCode, numberInGroup);
	    }

	    // If we couldn't find a fee for the transaction type then
	    // try and find a fee for all transaction types.
	    if (feeList == null || feeList.isEmpty()) {
		feeList = findFeeByDiminishingGroupNumber(productCode, productBenefitCode, memTypeCode, null, numberInGroup);
	    }

	    // If we still havn't got a fee then try and find a fee for all
	    // membership types and all transaction types.
	    if (feeList == null || feeList.isEmpty()) {
		feeList = findFeeByDiminishingGroupNumber(productCode, productBenefitCode, null, null, numberInGroup);
	    }

	    // If no fees were found then store an empty fee list in the cache
	    // against the key otherwise store the fees that were found against
	    // the key.
	    if (feeList == null) {
		feeList = new ArrayList();
	    }
	    // Store the fee list in the keyed fee cache so we get it next time
	    // out of the cache.
	    LogUtil.debug(this.getClass(), "Storing fee list with " + feeList.size() + " fees in cache with key : " + key);
	    this.keyedFeeList.put(key, feeList);
	}

	// Now filter out the fees that don't apply on the effective date.
	LogUtil.debug(this.getClass(), "feeList.size() pre-filter = " + (feeList == null ? "null list" : Integer.toString(feeList.size())));
	Collection outList = findEffectiveFees(feeList, effectiveDate);
	LogUtil.debug(this.getClass(), "outList.size() 1 = " + (outList == null ? "null list" : Integer.toString(outList.size())));
	try {
	    redundantFees((ArrayList) outList);
	    LogUtil.debug(this.getClass(), "outList.size() 2 = " + (outList == null ? "null list" : Integer.toString(outList.size())));
	} catch (RemoteException re) {
	    throw new RemoteException(re + ".  Fee Effective Date = " + effectiveDate);
	}
	LogUtil.debug(this.getClass(), "outList.size() 3 = " + (outList == null ? "null list" : Integer.toString(outList.size())));
	return outList;
    }

    /**
     * Checks to make sure that there are no fees specified more than once.
     * 
     * @param feeList
     * @return
     */
    private void redundantFees(ArrayList feeList) throws RemoteException {
	FeeSpecificationVO feeSpec1 = null, feeSpec2 = null;
	ArrayList tempList = (ArrayList) feeList.clone();
	for (int x = 0; x < feeList.size(); x++) {
	    feeSpec1 = (FeeSpecificationVO) feeList.get(x);
	    for (int y = x + 1; y < tempList.size(); y++) {
		feeSpec2 = (FeeSpecificationVO) tempList.get(y);
		if (feeSpec2.feeTypeCode.equals(feeSpec1.feeTypeCode) && feeSpec2.membershipTypeCode.equals(feeSpec1.membershipTypeCode) && feeSpec2.transactionTypeCode.equals(feeSpec1.transactionTypeCode) && feeSpec2.productCode.equals(feeSpec1.productCode) && feeSpec2.productBenefitCode.equals(feeSpec1.productBenefitCode) && feeSpec2.groupNumber == feeSpec1.groupNumber) {
		    throw new RemoteException("Redundant fee specification found for " + feeSpec1.membershipTypeCode + "/" + feeSpec1.feeTypeCode + "/" + feeSpec1.transactionTypeCode + "/" + feeSpec1.groupNumber + " (feeID = " + feeSpec1.feeSpecificationID + " is redundant with feeId " + feeSpec2.feeSpecificationID + ")");
		}
	    }
	}
    }

    /**
     * Seach for fees specified for the number of people in the group. If not
     * found then search for fees specified for fewer people in the group until
     * we get to zero group members. May return a null or empty list.
     * 
     * @param productCode
     *            Description of the Parameter
     * @param productBenefitCode
     *            Description of the Parameter
     * @param memTypeCode
     *            Description of the Parameter
     * @param transTypeCode
     *            Description of the Parameter
     * @param numberInGroup
     *            Description of the Parameter
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    private Collection findFeeByDiminishingGroupNumber(String productCode, String productBenefitCode, String memTypeCode, String transTypeCode, int numberInGroup) throws RemoteException {
	LogUtil.debug(this.getClass(), "Finding fee by diminishing group number : " + numberInGroup);
	Collection feeList = null;
	int groupNumber = numberInGroup;
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	while (groupNumber >= 0 && (feeList == null || feeList.isEmpty())) {
	    try {
		LogUtil.debug(this.getClass(), "findFeeByDiminishingGroupNumber:" + productCode + "," + productBenefitCode + "," + memTypeCode + "," + transTypeCode + ",null," + groupNumber);
		feeList = membershipMgr.getFeeSpecifications(productCode, productBenefitCode, memTypeCode, transTypeCode, groupNumber);
	    } catch (GenericException e) {
		// TODO Auto-generated catch block
		throw new RemoteException("Unable to get fee specification.", e);
	    }
	    groupNumber--;
	}
	return feeList;
    }

    /**
     * Build the key that will be used to store the fee specification list in
     * the keyed fee cache
     * 
     * @param productCode
     *            Description of the Parameter
     * @param productBenefitCode
     *            Description of the Parameter
     * @param transactionType
     *            Description of the Parameter
     * @param membershipType
     *            Description of the Parameter
     * @param numberInGroup
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    private String buildKey(String productCode, String productBenefitCode, String transactionType, String membershipType, int numberInGroup) {
	String missing = "?";
	StringBuffer sb = new StringBuffer();

	sb.append((productCode != null ? productCode : missing));
	sb.append((productBenefitCode != null ? productBenefitCode : missing));
	sb.append((transactionType != null ? transactionType : missing));
	sb.append((membershipType != null ? membershipType : missing));
	sb.append(numberInGroup);

	return sb.toString();
    }

    /**
     * Scoot through the fee list and filter out those that don't apply on the
     * effective date. Returns the full list if the effective date is null.
     * 
     * @param feeList
     *            Description of the Parameter
     * @param effectiveDate
     *            Description of the Parameter
     */
    private Collection findEffectiveFees(Collection feeList, DateTime effectiveDate) {
	LogUtil.debug(this.getClass(), "Filtering " + feeList.size() + " fees by effective date : " + effectiveDate.formatShortDate());
	// Now filter out the fees that don't apply on the effective date.
	Collection outList = new ArrayList();
	if (effectiveDate != null && feeList != null && !feeList.isEmpty()) {
	    effectiveDate = DateUtil.clearTime(effectiveDate);
	    // Copy the product fee list and then clear it as we will only add
	    // fees to it that apply on the effective date.
	    Collection tmpList = new ArrayList(feeList);

	    // feeList.clear();

	    FeeSpecificationVO feeSpecVO = null;
	    DateTime fromDate = null;
	    DateTime toDate = null;
	    Iterator listIterator = tmpList.iterator();
	    while (listIterator.hasNext()) {
		feeSpecVO = (FeeSpecificationVO) listIterator.next();
		LogUtil.debug(this.getClass(), "feeSpecVO=" + feeSpecVO);
		LogUtil.debug(this.getClass(), "getEffectiveFromDate()=" + feeSpecVO.getEffectiveFromDate() + ", getEffectiveToDate()=" + feeSpecVO.getEffectiveToDate());
		fromDate = DateUtil.clearTime(feeSpecVO.getEffectiveFromDate());

		if (fromDate != null && fromDate.after(effectiveDate)) {
		    LogUtil.debug(this.getClass(), "fromDate [" + fromDate.formatLongDate() + "] after effectiveDate [" + effectiveDate.formatLongDate() + "]");
		    // This fee does not apply yet so don't add it to the fee
		    // list.
		    continue;
		}

		toDate = DateUtil.clearTime(feeSpecVO.getEffectiveToDate());
		if (toDate != null && toDate.before(effectiveDate)) {
		    LogUtil.debug(this.getClass(), "toDate [" + fromDate.formatLongDate() + "] before effectiveDate [" + effectiveDate.formatLongDate() + "]");
		    // This fee no longer applies so don't add it to the fee
		    // list.
		    continue;
		}

		// The fee is being applied within the effective from and
		// to dates so it can be added to the returned list.
		// feeList.add(feeSpecVO);
		outList.add(feeSpecVO);
	    }
	}
	LogUtil.debug(this.getClass(), "Filtered fee list contains " + outList.size() + " fees.");
	return outList;
    }

    public void reset() {
	super.reset();
	this.keyedFeeList.clear();
    }

}