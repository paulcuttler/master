package com.ract.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.client.ClientVO;
import com.ract.common.ClassWriter;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.TaxRateVO;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.membership.ui.MembershipUIHelper;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.directdebit.DirectDebitAdapter;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitSchedule;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.MethodCounter;
import com.ract.util.NumberUtil;
import com.ract.util.XMLHelper;

/**
 * Holds together all of the membership transactions and consequently memberships that are being affected as a group by a business transaction. Also holds the common attributes shared by all memberships in the group. This class is not persisted. It only lives for the duration of the business transaction.
 * 
 * @author Terry Bakker
 */
public class TransactionGroup extends TransactionContainer implements Serializable, Writable {

	private boolean printRenewal = false;
	DateTime expiryDate = null;

	public void setPrintRenewal(boolean renPrint) {
		this.printRenewal = renPrint;
	}

	public static final String WRITABLE_CLASSNAME = "TransactionGroup";

	/**
	 * A unique ID to identify all members of the group.
	 */
	private int transactionGroupID = 0;

	/**
	 * Is this transaction group part of a batch process?
	 */
	private boolean batch;

	/**
	 * Payment Method ID to be used for repairing DD. This is also used by the transaction manager to prevent a DD schedule being generated.
	 */
	private String repairDDPaymentMethodId = null;

	/**
	 * Override transaction date - used for repairing DD.
	 */
	private DateTime overrideTransactionDate;

	/**
	 * The date and time that the transaction is taking place. This defaults to today.
	 */
	protected DateTime transactionDate = new DateTime();

	/**
	 * The type of transaction that is being processed for the transaction group. This may be the same as an individual memberships transaction type.
	 */
	protected String transactionGroupTypeCode;

	/**
	 * The reason why the transaction was done.
	 */
	private ReferenceDataVO transactionReason;

	/**
	 * The type of rejoin being performed.
	 */
	private ReferenceDataVO rejoinType;

	/**
	 * The user doing the transaction.
	 */
	protected User user;

	public void setUser(User user) throws RemoteException {
		this.user = user;
		// set on all transactions
		Collection transactionGroupTransactionList = this.getTransactionList();
		if (transactionGroupTransactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator txGroupIt = transactionGroupTransactionList.iterator();
			while (txGroupIt.hasNext()) {
				memTransVO = (MembershipTransactionVO) txGroupIt.next();
				memTransVO.setUsername(user.getUserID());
				memTransVO.setSalesBranchCode(user.getSalesBranchCode());
			}
		}

	}

	/**
	 * The base membership product that all members of the group will share.
	 */
	protected ProductVO selectedProduct;

	/**
	 * The date that the membership is to start on.
	 */
	private DateTime preferredCommenceDate;

	/**
	 * The selected interval of membership that all members in the group will share.
	 */
	private Interval membershipTerm;

	/**
	 * The membership type for all members in the group.
	 */
	private MembershipTypeVO membershipType;

	/**
	 * A list of membership transactions and consequently memberships that are being affected by the business transaction.
	 */
	private Vector transactionList = new Vector();

	/**
	 * A list of clients, as SelectedClient, that are to be treated as a membership group.
	 */
	private Hashtable selectedClientMap = new Hashtable();

	/**
	 * A list of the clients found that resulted from the last search done.
	 */
	private Vector clientSearchList;

	private MembershipGroupVO selectedMembershipGroup;

	/**
	 * The client number of the client that is the context of the transaction. Cannot remove this client from the client group.
	 */
	protected SelectedClient contextClient;

	/**
	 * The client that has been selected as the prime addressee of the membership group. Will reference one of the clients in the client list.
	 */
	private String paSelectedClientKey;

	public String getPaSelectedClientKey() {
		return this.paSelectedClientKey;
	}

	/**
	 * The membership profile shared by all members of the membership group.
	 */
	protected MembershipProfileVO membershipProfileVO;

	/**
	 * Holds the list of adjustments that apply to the transaction. e.g. Discretionary discount, quote adjustment. These are NOT the discounts that apply to the transaction fees.
	 */
	private Hashtable adjustmentList;

	/**
	 * The amount payable for the transaction. Calcuated as the sum of the fully discounted transaction fees for each membership transaction plus the gst. Be sure to call the calculateTransactionFee() method before using the getter methods.
	 */
	private double amountPayable = 0.0;

	/**
	 * The gross transaction fee for all membership transactions. No discount amounts have been deducted.
	 */
	private double grossTransactionFee = 0.0;

	/**
	 * The total amount of discount given to all membership transactions. This includes the discretionary discount amount as well as the non discretionary discount amount.
	 */
	private double totalDiscountAmount = 0.0;

	/**
	 * The amount of gst payable for the transaction. Calculated as a percentage of the sum of the fully discounted transaction fees for the membership transactions. Be sure to call the calculateTransactionFee() method before using the getter methods.
	 */
	private double gstAmount = 0.0;

	private double earnedAmount = 0.0;

	private double unearnedAmount = 0.0;

	private double gstRate = 0.0;

	/**
	 * Holds the details of the direct debit authority to be used whan paying for the transaction. If NULL then the transaction will be paid by receipting.
	 */
	private DirectDebitAuthority directDebitAuthority;

	/**
	 * Indicates if the membership is to be automatically renewed by the payment method selected. True is only valid for direct debit payments. If true the payment method will be attached to the prime addressees membership.
	 */
	private boolean automaticallyRenewable = false;

	/**
	 * holds payment method Id for repair transactions This is also used by the transaction manager to prevent a receipting record being generated.
	 */
	private String repairPaymentMethodId = null;

	/**
	 * The quote that is to be attached to this transaction.
	 */
	private MembershipQuote attachedQuote = null;

	/**
	 * Discretionary discounts are added to the adjustments list. However, we need to distinguish between a discretionary discount entered on the discretionary discount page and a discretionary discount that was included in a quote. If no discretionary discount was entered on the discretionary discount page and a discretionary discount was specified on the quote then the discretionary discount on the quote is added as an adjustment. But we then need to handle the user going back pages or reentering transaction details so we need to if the discretionary discount adjustment was derived from the user entering it on the discretionary discount page of if it derived from the quote.
	 */
	private double discretionaryDiscountAmount;

	private String discretionaryDiscountReason;

	private DateTime feeEffectiveDate;

	private int joinFeeCount;

	/**
	 * valid for subclasses only
	 */
	protected ArrayList paymentMethodListToRemove;

	private String rego;

	private String vin;

	private String make;

	private String model;

	private String year;

	private boolean forceCancelDueToFailedDD;

	/*************************** Constructors *******************************/

	public DateTime getFeeEffectiveDate() {
		if (this.feeEffectiveDate == null) {
			this.feeEffectiveDate = this.transactionDate;
		}
		return this.feeEffectiveDate;
	}

	protected Hashtable getSelectedClientMap() {
		return this.selectedClientMap;
	}

	/**
	 * Construct the transaction group specifying the transaction group ID, transaction type and user. The transaction date defaults to today. The expiry date defaults to today plus one standard term.
	 * 
	 * @param String
	 *            Transaction type code
	 * @param com
	 *            .ract.User
	 * @throws RemoteException
	 */
	public TransactionGroup(String transTypeCode, User user) throws RemoteException {
		LogUtil.debug(this.getClass(), "transTypeCode=" + transTypeCode);
		LogUtil.debug(this.getClass(), "user=" + user);
		if (transTypeCode == null || transTypeCode.length() < 1) {
			throw new SystemException("The transaction type code is not valid.");
		}

		if (user == null) {
			throw new SystemException("The user is not valid.");
		}

		this.transactionGroupTypeCode = transTypeCode;

		this.user = user;

		// Set the default membership term.
		// Get the default term of membership from system parameters
		String defaultMembershipTermText = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DEFAULT_MEMBERSHIP_TERM);
		try {
			this.membershipTerm = new Interval(defaultMembershipTermText);
		} catch (Exception e) {
			throw new SystemException("The default membership term specification from system parameters '" + defaultMembershipTermText + "' is not valid. : " + e.getMessage());
		}

		// Default the profile
		this.membershipProfileVO = MembershipEJBHelper.getMembershipRefMgr().getDefaultMembershipProfile();
		if (this.membershipProfileVO == null) {
			throw new SystemException("Failed to get default membership profile when creating a new transaction group.");
		}
	}

	public TransactionGroup(Node node) throws RemoteException {
		setFromXML(node);

		// Default any values not set in the xml document where possible
		if (this.membershipTerm == null) {
			String defaultMembershipTermText = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.DEFAULT_MEMBERSHIP_TERM);
			try {
				this.membershipTerm = new Interval(defaultMembershipTermText);
			} catch (Exception e) {
				throw new SystemException("The default membership term specification from system parameters '" + defaultMembershipTermText + "' is not valid. : " + e.getMessage());
			}
		}

		if (this.membershipProfileVO == null) {

			this.membershipProfileVO = MembershipEJBHelper.getMembershipRefMgr().getDefaultMembershipProfile();
			{
				throw new SystemException("Failed to get default membership profile when creating a new transaction group.");
			}
		}

		// Check for required data.
		if (this.transactionGroupTypeCode == null || this.transactionGroupTypeCode.length() < 1) {
			throw new SystemException("The transaction group type code is not valid.");
		}

	}

	public String addSelectedClient(SelectedClient selectedClient) throws RemoteException, ValidationException {
		if (selectedClient.getMembership() != null) {
			return addSelectedClient(selectedClient.getMembership());
		} else {
			return addSelectedClient(selectedClient.getClient());
		}
	}

	/**
	 * Add the client to the selected client list if they are not already in the list.
	 */
	public String addSelectedClient(ClientVO clientVO) throws RemoteException {
		StringBuffer msg = new StringBuffer();
		SelectedClient sc = new SelectedClient(clientVO, new DateTime());
		if (!this.selectedClientMap.containsKey(sc.getKey())) {
			String invalidReason = MembershipHelper.validateClientForTransaction(clientVO, this.transactionGroupTypeCode, null);
			if (invalidReason == null) {
				this.selectedClientMap.put(sc.getKey(), sc);

				// If a PA has not yet been set then set this membership as the
				// PA
				if (this.paSelectedClientKey == null) {
					this.paSelectedClientKey = sc.getKey();
				}
			} else {
				msg.append(clientVO.getDisplayName());
				msg.append(" cannot be transacted. ");
				msg.append(invalidReason);
			}
		} else {
			msg.append(clientVO.getDisplayName());
			msg.append(" has already been selected.");
		}

		return msg.toString();
	}

	/**
	 * Add the selected membership and all other memberships that are in the same membership group to the selected client list.
	 */
	public String addSelectedClient(MembershipVO memVO) throws RemoteException, ValidationException {
		StringBuffer msg = new StringBuffer();
		MembershipGroupVO memGroupVO;
		if (this.repairDDPaymentMethodId != null) {
			memGroupVO = memVO.getMembershipGroup(true); // if repairing DD,
			// load group
		} else {
			memGroupVO = memVO.getMembershipGroup();
		}

		LogUtil.debug(this.getClass(), "sel a");

		// Add all of the members in the membership group if this is not a
		// single membership type
		// of primary transaction
		if (memGroupVO != null && !(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(this.transactionGroupTypeCode))) {
			LogUtil.debug(this.getClass(), "sel b");
			msg.append(addSelectedClient(memGroupVO));
		} else {
			SelectedClient sc = new SelectedClient(memVO, new DateTime());
			LogUtil.debug(this.getClass(), "sel c");
			if (!this.selectedClientMap.containsKey(sc.getKey())) {
				String invalidReason = memVO.getInvalidForTransactionReason(this.transactionGroupTypeCode);

				if (invalidReason == null || (this.repairDDPaymentMethodId != null && invalidReason.equals(MembershipVO.MESSAGE_EXPIRED_TOO_LONG)) || (this.forceCancelDueToFailedDD && invalidReason.equals(DirectDebitAdapter.DD_STATUS_FAILED))) {
					LogUtil.debug(this.getClass(), "sel d");
					// Set the profile if it has not already been set.
					if (this.membershipProfileVO == null) {
						setMembershipProfile(memVO.getMembershipProfile());
					}

					// If the profile is still null then this is a new
					// membership
					// that does not have a profile. Set the default profile.
					if (this.membershipProfileVO == null) {
						setMembershipProfile(MembershipEJBHelper.getMembershipRefMgr().getDefaultMembershipProfile());
					}
					LogUtil.debug(this.getClass(), "sel e");
					this.selectedClientMap.put(sc.getKey(), sc);

					// If a PA has not yet been set then set this membership as
					// the PA
					if (this.paSelectedClientKey == null) {
						this.paSelectedClientKey = sc.getKey();
					}
				} else {
					LogUtil.debug(this.getClass(), "sel f");
					msg.append("The membership for ");
					msg.append(memVO.getClient().getDisplayName());
					msg.append(" cannot be transacted. ");
					msg.append(invalidReason);
				}
			} else {
				LogUtil.debug(this.getClass(), "sel g");
				msg.append("The membership for ");
				msg.append(memVO.getClient().getDisplayName());
				msg.append(" has already been selected.");
			}
		}
		LogUtil.debug(this.getClass(), "msg " + msg);
		LogUtil.debug(this.getClass(), "sel " + this.getSelectedClientCount());
		return msg.toString();
	}

	/**
	 * Add all of the clients and their memberships from the membership group to the selected client list. If creating a membership group then disallow as groups can't be merged. If a membership group has already been selected and this is a different membership group then disallow. Set the selected prime addressee to the groups current prime addressee. Set the membership profile to be the same as the profile shared by the group members. Add every group member even if they are invalid for the transaction. We need them to be able to identify which memberships need to be removed from the membership group later. The createTransactionsForSelectedMemberships method wil create a "remove from group" transaction for invalid memberships.
	 */
	public String addSelectedClient(MembershipGroupVO memGroupVO) throws RemoteException, ValidationException {
		// Can't select a group membership in an inherently single membership
		// primary transaction.
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(this.transactionGroupTypeCode)) {
			throw new SystemException("Cannot select a membership group (ID=" + memGroupVO.getMembershipGroupID() + ") in a single membership transaction type '" + this.transactionGroupTypeCode + "'.");
		}

		if (this.selectedMembershipGroup != null && !this.selectedMembershipGroup.getMembershipGroupID().equals(memGroupVO.getMembershipGroupID())) {
			throw new ValidationException("A membership group has already been selected. Clients with memberships in another membership group cannot be added to the selected membership group.");
		}

		// Temporarily fix any problems that the membership group may have.
		// i.e. divorce, love triangles, PA shirking their responsibilities etc.
		memGroupVO.fixProblems();

		StringBuffer msg = new StringBuffer();
		// MembershipProfileVO memProfileVO = null;
		MembershipGroupDetailVO memGroupDetailVO;
		MembershipVO memVO;
		// String invalidReason;
		Collection memGroupDetailList = memGroupVO.getMembershipGroupDetailList();
		Iterator memGroupDetailListIterator = memGroupDetailList.iterator();
		while (memGroupDetailListIterator.hasNext()) {
			memGroupDetailVO = (MembershipGroupDetailVO) memGroupDetailListIterator.next();

			memVO = memGroupDetailVO.getMembership();

			// Add the client if they are not already in the group.
			SelectedClient sc = new SelectedClient(memVO, memGroupDetailVO.getGroupJoinDate());

			if (!this.selectedClientMap.containsKey(sc.getKey())) {
				sc.setInvalidReason(memVO.getInvalidForTransactionReason(this.transactionGroupTypeCode));

				// Set the profile if it has not already been set.
				if (this.membershipProfileVO == null) {
					setMembershipProfile(memVO.getMembershipProfile());
				}

				this.selectedClientMap.put(sc.getKey(), sc);

				// If this is the PA of the group then set them as the selected
				// PA.
				if (memGroupDetailVO.isPrimeAddressee()) {
					this.paSelectedClientKey = sc.getKey();
				}
			} else {
				msg.append("The membership for ");
				msg.append(memVO.getClient().getDisplayName());
				msg.append(" has already been selected.");
			}
		}
		this.selectedMembershipGroup = memGroupVO;
		return msg.toString();
	}

	public int getTransactionGroupID() {
		return this.transactionGroupID;
	}

	public DateTime getTransactionDate() {
		return this.transactionDate;
	}

	public String getTransactionGroupTypeCode() {
		return this.transactionGroupTypeCode;
	}

	/**
	 * Retrun the heading to use for the type of transaction being perfomed.
	 */
	public String getTransactionHeadingText() {
		return MembershipUIHelper.getTransactionHeadingText(this.transactionGroupTypeCode);
	}

	public User getUser() {
		return this.user;
	}

	public String getUserID() {
		return this.user.getUserID();
	}

	public String getSalesBranchCode() {
		return this.user.getSalesBranchCode();
	}

	/**
	 * Return the start of the membership period that the transaction covers. The period starts on the preferred commence date unless doing a renewal in which case it start at the end of the old period. If no preferred commence date has been defined then it starts on the transaction date.
	 */
	public DateTime getPeriodStartDate() throws RemoteException {
		DateTime startDate = getPreferredCommenceDate();

		// When renewing, the membership period starts on the expiry date
		// of the membership being renewed.
		MembershipVO oldMemVO = getMembershipTransactionForPA().getMembership();
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(this.transactionGroupTypeCode)) {
			if (!MembershipVO.STATUS_ONHOLD.equals(oldMemVO.getStatus())) {
				startDate = oldMemVO.getExpiryDate();
			}
			if (startDate == null) {
				throw new RemoteException("startDate is null in getPeriodStartDate");
			}
		}
		// If a preferred commence date was not set and the membership is not
		// being renewed then the product period starts on the transaction date.
		else if (startDate == null) {
			startDate = DateUtil.clearTime(getTransactionDate());
		}
		// LogUtil.debug(this.getClass(),"startDate="+startDate);
		return startDate;
	}

	/**
	 * Return the end date of the period covered by the transaction. If cancelling the end date is the transaction date otherwise the end date is the new expiry date of the membership.
	 */
	public DateTime getPeriodEndDate() throws RemoteException {
		DateTime endDate = null;
		MembershipTransactionVO memTransVO = getMembershipTransactionForPA();
		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(this.transactionGroupTypeCode)) {
			// cancel never creates payable items so it will never get to here
			endDate = getTransactionDate();
		}
		// use the expiry date of the affiliated club
		else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(memTransVO.getTransactionTypeCode())) {
			endDate = memTransVO.getNewMembership().getAffiliatedClubExpiryDate();
		} else {
			endDate = memTransVO.getNewMembership().getBaseExpiryDate(); // Don't
			// add
			// any
			// discount
			// periods.
		}
		return endDate;
	}

	/**
	 * If an explicit preferred commence date has not been set it will default to the transaction date with the time component cleared.
	 */
	public DateTime getPreferredCommenceDate() {
		if (this.preferredCommenceDate == null) {
			LogUtil.debug(this.getClass(), "preferredCommenceDate=" + transactionDate);
			if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionGroupTypeCode)) {
				try {
					int waitPeriod = 2;
					SystemParameterVO refData = CommonEJBHelper.getCommonMgr().getSystemParameter("MEM", "WaitPeriod");
					if (refData != null) {
						waitPeriod = refData.getNumericValue1().intValue();
					}
					this.preferredCommenceDate = new DateTime();
					Interval twoDays = new Interval(0, 0, waitPeriod, 0, 0, 0, 0);
					this.preferredCommenceDate = this.preferredCommenceDate.add(twoDays);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				this.preferredCommenceDate = DateUtil.clearTime(this.transactionDate);
			}
		}

		LogUtil.debug(this.getClass(), "preferredCommenceDate=" + preferredCommenceDate);

		return this.preferredCommenceDate;
	}

	/**
	 * Return the expiry date on the new membership from the first transaction that updates a membership in the transaction list. If this is a membership group transaction all memberships must share the same expiry date except those being removed from the membership group. This will pick up any discount periods that have been added to the transaction fees. Make sure that the calculateTransactionFees() method has been called first so that the discount periods have been transferred to the new memberships. If there are no transactions from which to get the expiry date then a default expiry date is returned. This is calculated as the preferred commence date plus the default term of membership.
	 */
	public DateTime getExpiryDate() throws RemoteException {
		if (this.expiryDate != null) {
			return this.expiryDate;
		}
		if (this.transactionList != null && !this.transactionList.isEmpty()) {
			// Try and get the expiry date off one of the new memberships.
			MembershipTransactionVO memTransVO = null;
			Iterator iterator = this.transactionList.iterator();
			while (expiryDate == null && iterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) iterator.next();
				if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransVO.getTransactionTypeCode())) // Won't
				// have
				// an
				// indicative
				// expiry
				// date
				// on
				// it
				{
					expiryDate = memTransVO.getNewMembership().getExpiryDate();
				}
			}
		}

		// If the expiry date is still null then default it to one term
		// after the preferred commence date.
		if (expiryDate == null) {
			Interval memTerm = getMembershipTerm();
			expiryDate = getPreferredCommenceDate().add(memTerm);
		}

		return expiryDate;
	}

	/**
	 * Return the expiry date of the existing membership group that is being processed. Return null if no membership group has been selected.
	 */
	public DateTime getMembershipGroupExpiryDate() throws RemoteException {
		if (this.selectedMembershipGroup != null) {
			return this.selectedMembershipGroup.getMembershipGroupExpiryDate();
		}

		return null;
	}

	/**
	 * Get the PA for the membership group. Returns null for an individual
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public MembershipVO getMembershipGroupPA() throws RemoteException {
		if (this.selectedMembershipGroup != null) {
			return this.selectedMembershipGroup.getMembershipGroupDetailForPA().getMembership();
		}

		return null;

	}

	/**
	 * Return the base product of the existing membership group that is being processed. Return null if no membership group has been selected.
	 */
	public ProductVO getMembershipGroupProduct() throws RemoteException {
		if (this.selectedMembershipGroup != null) {
			this.selectedMembershipGroup.getMembershipGroupProduct();
		}

		return null;
	}

	public ProductVO getSelectedProduct() {
		return this.selectedProduct;
	}

	public MembershipProfileVO getMembershipProfile() {
		return this.membershipProfileVO;
	}

	public Interval getMembershipTerm() {
		return this.membershipTerm;
	}

	public MembershipTypeVO getMembershipType() {
		return this.membershipType;
	}

	/**
	 * Return the membership group ID of the existing membership group that is being transacted. Return null if no membership group has been selected.
	 */
	public Integer getMembershipGroupID() {
		if (this.selectedMembershipGroup != null) {
			return this.selectedMembershipGroup.getMembershipGroupID();
		}

		return null;
	}

	/**
	 * Return the term of the memberships in the existing membership group by getting the term of the membership groups prime addressees membership. Return null if no membership group has been selected.
	 */
	public Interval getMembershipGroupMembershipTerm() throws RemoteException {
		if (this.selectedMembershipGroup != null) {
			Interval term = null;
			try {
				term = new Interval(this.selectedMembershipGroup.getMembershipGroupDetailForPA().getMembership().getMembershipTerm());
			} catch (Exception e) {
				throw new RemoteException(e.getMessage());
			}
		}
		return null;
	}

	/**
	 * Set the values of the memberships. The membership values are set for memberships that are being: - Created - Rejoined - Renewed - Transferred - Change of Product if the product has been changed. The membership value is defined as the sum of the membership fees that apply to a renew transaction and where the fee is a fee that is earned over the life of the membership. The membership value will be set according to the number of members in the group. If the fee specifies $60 for the first three members and $40 for each additional member then the first three memberships (in arbitrary order) will have a value of $60 and the rest will have a value of $40.
	 */
	private void setMembershipValues() throws RemoteException {
		LogUtil.debug(this.getClass(), "setMemVal");
		// double value = 0.0;
		if (this.transactionList != null && this.selectedProduct != null) {
			// MembershipRefMgr refMgr =
			// MembershipEJBHelper.getMembershipRefMgr();
			// int groupSize = getActualGroupMemberCount();

			// can we get this from the current list of transactions???
			/*
			 * Collection feeList = refMgr.findProductFee(this.selectedProduct. getProductCode(), this.membershipType. getMembershipTypeCode(), MembershipTransactionTypeVO. TRANSACTION_TYPE_RENEW, groupSize, this.getFeeEffectiveDate());
			 */
			CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
			double gstRate = commonMgr.getCurrentTaxRate().getGstRate();

			Iterator feeListIterator;
			MembershipTransactionVO memTransVO;
			String memTransTypeCode;
			FeeSpecificationVO feeSpec;
			BigDecimal membershipValue;
			double memValue = 0;
			int memberCount = 0;
			int vehicleCount = 0;
			double feeSpecValue = 0;
			Collections.sort(this.transactionList);
			Iterator transListIterator = this.transactionList.iterator();

			MembershipTransactionFee txFee = null;
			MembershipTransactionDiscount txFeeDiscount = null;

			Collection txFeeList = null;
			Iterator txFeeIt = null;

			Collection discList = null;
			Iterator discIt = null;
			DiscountTypeVO discountType = null;

			while (transListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIterator.next();
				memTransTypeCode = memTransVO.getTransactionTypeCode();
				memberCount++;
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(memTransTypeCode) ||
				// MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(
				// memTransTypeCode) //doesn't change the value
				// ||
				// for change group the new values need to be
				// recalculated for
				// dd but it doesn't hurt to recalculate anyway
						MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(memTransTypeCode) ||
						//
						MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(memTransTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(memTransTypeCode) || (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(memTransTypeCode) && memTransVO.isProductChanged())) {
					// value is initialised per transaction
					memValue = 0;

					vehicleCount = memTransVO.getNewMembership().getVehicleCount();

					// new calculations
					txFeeList = memTransVO.getTransactionFeeList();
					// has fees attached (rule out cancel)
					if (txFeeList != null) {

						LogUtil.debug(this.getClass(), "is fee list");
						txFeeIt = txFeeList.iterator();
						while (txFeeIt.hasNext()) {
							txFee = (MembershipTransactionFee) txFeeIt.next();

							LogUtil.debug(this.getClass(), "is earned " + txFee);
							feeSpec = txFee.getFeeSpecificationVO();
							// add the base value if a "value" fee
							feeSpecValue = feeSpec.getMembershipValue(memberCount, vehicleCount);
							// add the current fee
							memValue += feeSpecValue;

							LogUtil.debug(this.getClass(), "mem val = " + memValue);
							// look for any membership value, non-offset
							// reducing discounts.
							discList = txFee.getDiscountList();
							LogUtil.debug(this.getClass(), "txFee=" + txFee.isValueFee());
							// Check if fee gives value. if not then don't
							// subtract discounts!!!
							if (txFee.isValueFee() && discList != null) {
								discIt = discList.iterator();
								while (discIt.hasNext()) {
									LogUtil.debug(this.getClass(), "is disc");
									txFeeDiscount = (MembershipTransactionDiscount) discIt.next();

									discountType = txFeeDiscount.getDiscountTypeVO();
									LogUtil.debug(this.getClass(), "is disc " + discountType);
									// when no account is recorded for the
									// discount type and the discount type
									// is marked to reduce the membership value
									// then reduce the membership's perceived
									// client value.
									if (!discountType.isOffsetDiscount() && discountType.isMembershipValueReducing()) {
										LogUtil.debug(this.getClass(), "is reducing 1 " + memValue);
										// subtract discount amount
										memValue -= txFeeDiscount.getDiscountAmount();
										LogUtil.debug(this.getClass(), "is reducing 2 " + memValue);
									}
								}
							}
						}

						if (memValue < 0) {
							throw new SystemException("Value of membership is being set to less than zero (" + memValue + ")");
						}

						membershipValue = CurrencyUtil.calculateGSTExclusiveAmount(memValue, gstRate, 4);
						LogUtil.debug(this.getClass(), "setting the memValue = " + membershipValue);

						memTransVO.getNewMembership().setMembershipValue(membershipValue);

					}

				}
			}
		}
	}

	public Vector getTransactionList() {
		return this.transactionList;
	}

	public ClientVO getContextClient() {
		if (this.contextClient != null) {
			return this.contextClient.getClient();
		} else {
			return null;
		}
	}

	public MembershipVO getContextClientMembership() {
		if (this.contextClient != null) {
			return this.contextClient.getMembership();
		} else {
			return null;
		}
	}

	/**
	 * Return the selected client identified by the given key. Returns null if the key does not exist.
	 */
	public SelectedClient getSelectedClient(String selectedClientKey) {
		return (SelectedClient) this.selectedClientMap.get(selectedClientKey);
	}

	public int getSelectedClientCount() {
		int clientCount = 0;
		if (this.selectedClientMap != null) {
			clientCount = this.selectedClientMap.size();
		}
		return clientCount;
	}

	public Collection getSelectedClientList() {
		return this.selectedClientMap.values();
	}

	protected Enumeration getSelectedClientKeys() {
		return this.selectedClientMap.keys();
	}

	public MembershipGroupVO getSelectedMembershipGroup() {
		return this.selectedMembershipGroup;
	}

	/**
	 * Return a list of distinct transaction fee types for the transaction fees associated to the membeships transactions. Returns an empty list if there are no fee types.
	 */
	public Collection getDistinctTransactionFeeTypeList() throws RemoteException {
		Hashtable distinctFeeList = new Hashtable();
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Collection feeTypeList = null;
			Iterator feeTypeListIterator = null;
			String feeTypeCode = null;
			Iterator transListIterator = this.transactionList.iterator();
			while (transListIterator.hasNext())

			{
				memTransVO = (MembershipTransactionVO) transListIterator.next();

				// Get the list of fee types from the membership transaction.
				// This list may contain duplicates
				feeTypeList = memTransVO.getFeeTypeList();
				feeTypeListIterator = feeTypeList.iterator();
				while (feeTypeListIterator.hasNext()) {
					feeTypeCode = (String) feeTypeListIterator.next();
					// LogUtil.debug(this.getClass(),"getDistinctTransactionFeeTypeList() : feeTypeCode="+feeTypeCode);
					// If the fee type code is not already in the distinct list
					// then add it.
					if (!distinctFeeList.contains(feeTypeCode)) {
						distinctFeeList.put(feeTypeCode, feeTypeCode);
					}
				}
			}
		}
		return distinctFeeList.values();
	}

	public int getMembershipTransactionCount() {
		int count = 0;
		if (this.transactionList != null) {
			count = this.transactionList.size();

		}
		return count;
	}

	/**
	 * Return the membership transaction for the specified client. Returns null if the transaction could not be found.
	 */
	public MembershipTransactionVO getMembershipTransactionForClient(Integer clientNumber) throws RemoteException {
		MembershipTransactionVO memTransVO = null;
		MembershipTransactionVO tmpMemTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		Iterator transListIT = this.transactionList.iterator();
		while (transListIT.hasNext() && memTransVO == null) {
			tmpMemTransVO = (MembershipTransactionVO) transListIT.next();
			oldMemVO = tmpMemTransVO.getMembership();
			newMemVO = tmpMemTransVO.getNewMembership();
			if (newMemVO != null) {
				if (newMemVO.getClientNumber().equals(clientNumber)) {
					memTransVO = tmpMemTransVO;
				}
			} else if (oldMemVO != null) {
				if (oldMemVO.getClientNumber().equals(clientNumber)) {
					memTransVO = tmpMemTransVO;
				}
			} else {
				throw new RemoteException("Both the old membership and the new membership are null on the membership transaction!");
			}
		}
		return memTransVO;
	}

	/**
	 * Return the client vo for the selected prime addressee. Returns null if no prime addressee was selected.
	 */
	public ClientVO getClientForPA() {
		// The client VO can be obtained from the selected client map even
		// when membership transactions have been created since the selected
		// client
		// map is not cleared and the PA flag on the membership transactions is
		// set
		// from the selected pa key.
		ClientVO clientVO = null;
		if (this.selectedClientMap != null && this.paSelectedClientKey != null) {
			SelectedClient selectedClient = (SelectedClient) this.selectedClientMap.get(this.paSelectedClientKey);
			if (selectedClient != null) {
				clientVO = selectedClient.getClient();
			}
		}

		return clientVO;
	}

	public Vector getClientSearchList() {
		return this.clientSearchList;
	}

	public String getSelectedClientKeyForPA() {
		return this.paSelectedClientKey;
	}

	/**
	 * Return the membership transaction for the prime addressee. This is only available after creating the transactions for the transaction group.
	 */
	public MembershipTransactionVO getMembershipTransactionForPA() throws RemoteException {
		LogUtil.debug(this.getClass(), "getMembershipTransactionForPA start");

		MembershipTransactionVO memTransVO = null;
		MembershipTransactionVO tmpMemTransVO = null;

		if (this.transactionList != null) {
			LogUtil.debug(this.getClass(), "transactionList is not null");
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext() && memTransVO == null) {
				tmpMemTransVO = (MembershipTransactionVO) transListIT.next();
				LogUtil.debug(this.getClass(), "tmpTransVO=" + tmpMemTransVO);
				if (tmpMemTransVO.isPrimeAddressee()) {
					LogUtil.debug(this.getClass(), "pa=" + tmpMemTransVO);
					memTransVO = tmpMemTransVO;
				}
			}
		}
		LogUtil.debug(this.getClass(), "getMembershipTransactionForPA = " + memTransVO);
		LogUtil.debug(this.getClass(), "getMembershipTransactionForPA end");
		return memTransVO;
	}

	public ReferenceDataVO getTransactionReason() {
		return this.transactionReason;
	}

	public ReferenceDataVO getRejoinType() {
		LogUtil.debug(this.getClass(), "getRejoinType()=" + rejoinType);
		return this.rejoinType;
	}

	/**
	 * Return the gross transaction fee. This is the sum of all of the fees charged without deducting any discounts. NOTE: The calculateTransactionFees() method must be called first.
	 */
	public double getGrossTransactionFee() throws RemoteException {
		return this.grossTransactionFee;
	}

	/**
	 * Return the net transaction fee. The net transaction fee is calculated as the sum of all transaction fees applied to each membership less the non-discretionary discounts applied to each membership.
	 */
	public double getNetTransactionFee() throws RemoteException {
		double netTransFee = 0.0;
		if (this.transactionList != null && !this.transactionList.isEmpty()) {
			// Loop through the membership transactions and sum
			// up the net transaction fees.
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				netTransFee += memTransVO.getNetTransactionFee();
			}
		}
		return netTransFee;
	}

	/**
	 * Return the net transaction fee for the specified fee type code. The net transaction fee is calculated as the sum of all transaction fees applied to each membership less the non-discretionary discounts applied to each membership.
	 */
	public double getNetTransactionFee(String feeTypeCode) throws RemoteException {
		double netTransFee = 0.0;
		if (this.transactionList != null && !this.transactionList.isEmpty()) {
			// Loop through the membership transactions and sum
			// up the net transaction fees for the fee type code.
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				netTransFee += memTransVO.getNetTransactionFee(feeTypeCode);
			}
		}
		return netTransFee;
	}

	/**
	 * Return the total amount of discount given. This includes both discretionary and non-discretionary discounts. NOTE: The calculateTransactionFees() method must be called first.
	 */
	public double getTotalDiscount() throws RemoteException {
		return this.totalDiscountAmount;
	}

	/**
	 * Return the amount of gst to pay. Be sure to call the calculateTransactionFee() method before using this value.
	 */
	public double getGstAmount() {
		return this.gstAmount;
	}

	/**
	 * Return the amount payable for the prime addressee of the membership group. Be sure to call the calculateTransactionFee() method before using this value.
	 */
	public double getAmountPayable() {
		return NumberUtil.roundDouble(this.amountPayable, 2);
	}

	public double getEarnedAmount() {
		return this.earnedAmount;
	}

	public double getUnearnedAmount() {
		return this.unearnedAmount;
	}

	/**
	 * Return the maximum discretionary doscount amount that can be given. The sum of the fees that allow discretionary discounts is obtained less any discounts already given.
	 */
	public double getMaxDiscretionaryDiscountAmount() throws RemoteException {
		double maxDiscount = 0.0;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator tranactionIterator = this.transactionList.iterator();
			while (tranactionIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) tranactionIterator.next();
				maxDiscount += memTransVO.getMaxDiscretionaryDiscountAmount();
			}
		}
		return maxDiscount;
	}

	/**
	 * Return the maximum discretionary doscount amount that can be given for applied fees with the specified fee type code. The sum of the fees that allow discretionary discounts is obtained less any discounts already given.
	 */
	public double getMaxDiscretionaryDiscountAmount(String feeTypeCode) throws RemoteException {
		double maxDiscount = 0.0;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator tranactionIterator = this.transactionList.iterator();
			while (tranactionIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) tranactionIterator.next();
				maxDiscount += memTransVO.getMaxDiscretionaryDiscountAmount(feeTypeCode);
			}
		}
		return maxDiscount;
	}

	public String getRepairPaymentMethodId() {
		return this.repairPaymentMethodId;
	}

	/*************************** Setter methods ******************************/

	/**
	 * Set the amount payable on the membership transaction that is responsible for making the payment. The membership flagged as the prime addressee in the membership list is responsible for making the payment.
	 */
	public void setAmountPayable(double amtPayable, double gstAmt) throws RemoteException {
		boolean found = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				if (!found && memTransVO.isPrimeAddressee()) {
					memTransVO.setAmountPayable(amtPayable);
					memTransVO.setGstFee(gstAmt);
					found = true;
				} else if (found && memTransVO.isPrimeAddressee()) {
					throw new SystemException("Failed to set amount payable on the membership transaction. There are too many prime addressees!");
				} else {
					memTransVO.setAmountPayable(0.0);
					memTransVO.setGstFee(0.0);
				}
			}
		}
		if (!found) {
			throw new SystemException("Failed to set amount payable on the membership transaction.");
		}
	}

	/**
	 * set the payment method id to attach to.
	 */
	public void setRepairPaymentMethodId(String repairPaymentMethodId) {
		LogUtil.debug(this.getClass(), "setting repairPaymentMethodId = " + repairPaymentMethodId);
		this.repairPaymentMethodId = repairPaymentMethodId;
	}

	/**
	 * Set the preferred commence date for all memberships in the group. Only applies to new memberships. Also set the join date, commence date and product effective date to the preferred commence date.
	 */
	public void setPreferredCommenceDate(DateTime prefComDate) throws RemoteException, ValidationException {
		this.preferredCommenceDate = prefComDate;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.setPreferredCommenceDate(prefComDate, true);
				/** @todo check this! */
			}
		}
	}

	/**
	 * Set the product that all memberships in the group will share. Also set the product on each membership in the membership transaction group list. Delete any existing transaction fees and discounts that may be dependant on a previously selected product. If you set the product effective date first then this will be propogated to memberships in the transaction list. Note: the product effective date defaults to the transaction date if it is not set.
	 */
	public void setSelectedProduct(ProductVO newProduct) throws ValidationException, RemoteException {
		// Update the membership transactions if setting a new product,
		// clearing the existing product or changing the existing product
		if ((this.selectedProduct == null && newProduct != null) || (this.selectedProduct != null && newProduct == null) || (this.selectedProduct != null && newProduct != null && !this.selectedProduct.getProductCode().equals(newProduct.getProductCode()))) {
			this.selectedProduct = newProduct;

			MembershipTransactionVO memTransVO = null;
			MembershipVO oldMemVO = null;
			MembershipVO newMemVO = null;
			ProductVO oldProductVO;
			String oldMembershipStatus;
			DateTime oldMemVOExpiryDate;
			Iterator transListIT = null;
			boolean existingGroupMember = false;
			Integer membershipID;
			boolean isOldPA = false;
			boolean isNewPA = false;
			MembershipVO oldPAMemVO = getMembershipGroupPA();

			// If there are membership transactions then synchronize them with
			// the transaction group.
			if (this.transactionList != null) {
				transListIT = this.transactionList.iterator();
				while (transListIT.hasNext()) {
					memTransVO = (MembershipTransactionVO) transListIT.next();
					oldMemVO = memTransVO.getMembership();
					newMemVO = memTransVO.getNewMembership();

					if (oldMemVO != null) {
						oldProductVO = oldMemVO.getProduct();
						oldMembershipStatus = oldMemVO.getStatus();
						oldMemVOExpiryDate = oldMemVO.getExpiryDate();
						membershipID = oldMemVO.getMembershipID();
					} else {
						oldProductVO = null;
						oldMembershipStatus = null;
						oldMemVOExpiryDate = null;
						membershipID = null;
					}
					existingGroupMember = isMembershipInMembershipGroup(membershipID);
					isOldPA = oldPAMemVO != null && oldPAMemVO.getMembershipID().equals(membershipID);
					isNewPA = memTransVO.isPrimeAddressee();

					// Clear any existing fees and discounts for the old product
					memTransVO.clearTransactionFeeList();

					if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(this.transactionGroupTypeCode)) {
						// No transaction type change necessary
						newMemVO.setNextProductCode(null);
						newMemVO.setProduct(newProduct); // Sets the product effective date to today if the product has been changed.
					}
					// Check to see if a lower risk product is being set during
					// a change of product transaction.
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.transactionGroupTypeCode) && newProduct.compareTo(oldProductVO) > 0) {
						// The selected product is a lower risk product than the
						// old
						// product on the membership. Flag the change of product
						// for the next renewal and don't change it.
						// No transaction type change necessary
						newMemVO.setNextProductCode(newProduct.getProductCode());
					}
					// Check to see if a higher risk product is being set during
					// a change of product transaction.
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.transactionGroupTypeCode) && newProduct.compareTo(oldProductVO) <= 0) {
						// No transaction type change necessary
						newMemVO.setNextProductCode(null);
						newMemVO.setProduct(newProduct); // Sets the product
						// effective date to
						// today if the product
						// has been changed.
					} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.transactionGroupTypeCode)) {
						if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(memTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(memTransVO.getTransactionTypeCode())) {
							// No transaction type change necessary
						} else {
							// If the product has changed we do a change of
							// product transaction otherwise a renewal
							// transaction.
							if (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || oldMemVO.getProductCode().equals(this.selectedProduct.getProductCode())) {
								// The membership is on hold or the product has
								// not been changed.
								// Do a renewal on the membership
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW);
							} else {
								// The product has been changed.
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT);
							}
						}
						newMemVO.setNextProductCode(null);
						newMemVO.setProduct(newProduct); // Sets the product
						// effective date to
						// today if the product
						// has been changed.
					} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(this.transactionGroupTypeCode)) {
						if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(memTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(memTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransVO.getTransactionTypeCode())) {
							// No transaction type change necessary.
						} else if (existingGroupMember && isMembershipGroupProductChanged()) {
							// The product has been changed so do a variation of
							// a
							// change of product transaction.
							memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT);
						} else if (existingGroupMember && !isMembershipGroupProductChanged()) // Base
						// product
						// has
						// not
						// changed
						{
							// The membership is already an existing member of
							// the group
							// They are not being added or removed.
							// No change has been made to cause the membership
							// itself to be updated.
							// See if the membership group details need
							// updating.
							if (isOldPA == isNewPA) {
								// No change to the PA status
								// Tag the transaction with the change of group
								// transaction type.
								// This indicates that a change was made to the
								// membership group
								// but this particular membership was not
								// directly affected.
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP);
							} else {
								// They are were either the old PA and are not
								// the new PA
								// or they were not the old PA and are the new
								// PA
								// Tag the transaction with a change PA
								// transaction type
								// so that the transactiuon manager updates the
								// membership group records.
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA);
							}
						} else if (!existingGroupMember // and is on hold
								&& (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || (!MembershipVO.STATUS_EXPIRED.equals(oldMembershipStatus) // or
								// is
								// not
								// expired
								// and
								// not
								// cancelled
								&& !MembershipVO.STATUS_CANCELLED.equals(oldMembershipStatus)))) {
							// If the product has changed we do a change of
							// product transaction
							// otherwise a renewal transaction.
							if (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || oldMemVO.getProductCode().equals(this.selectedProduct.getProductCode())) {
								// The membership is on hold or the product has
								// not been changed.
								// Do a part term renewal on the membership
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW);
							} else {
								// The product has been changed so do a
								// variation of a
								// change of product transaction. Down grade of
								// product is done
								// now and not flagged for next renewal.
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT);
							}
						}
						newMemVO.setNextProductCode(null);
						newMemVO.setProduct(newProduct); // Sets the product
						// effective date to
						// today if the product
						// has been changed.
					} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(this.transactionGroupTypeCode)) {
						if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(memTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(memTransVO.getTransactionTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransVO.getTransactionTypeCode())) {
							// No transaction type change necessary.
						} else if (MembershipVO.STATUS_ACTIVE.equals(oldMembershipStatus) || MembershipVO.STATUS_FUTURE.equals(oldMembershipStatus) || MembershipVO.STATUS_INRENEWAL.equals(oldMembershipStatus) || MembershipVO.STATUS_EXPIRED.equals(oldMembershipStatus) || MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus)) {
							// If the product has changed we do a change of
							// product transaction otherwise a renewal
							// transaction.
							if (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || oldMemVO.getProductCode().equals(this.selectedProduct.getProductCode())) {
								// The membership is on hold or the product has
								// not been changed.
								// Do a part term renewal on the membership
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW);
							} else {
								// The product has been changed so do a
								// variation of a
								// change of product transaction. Down grade of
								// product is done
								// now and not flagged for next renewal.
								memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT);
							}

							newMemVO.setNextProductCode(null);
							newMemVO.setProduct(newProduct); // Sets the product
							// effective date
							// to today if the
							// product has been
							// changed.

						}

					} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(this.transactionGroupTypeCode)) {
						// no change necessary
					}
					// cater for transaction types of transfer or new
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(this.transactionGroupTypeCode)) {
						newMemVO.setNextProductCode(null);
						newMemVO.setProduct(newProduct); // Sets the product
						// effective date to
						// today if the product
						// has been changed.
					} else {
						throw new SystemException("Transaction type '" + this.transactionGroupTypeCode + "' is not supported yet.");
					}

				} // while (transListIT.hasNext())
			} // if (this.transactionList != null)
		} else {
			this.selectedProduct = newProduct;
		}
		// LogUtil.debug(this.getClass(),"------>setselected "+this.selectedProduct);
		setProductEffectiveDates();

		// Make sure that the attached quote is still valid. It must have the
		// same
		// product as the one selected.
		if (this.selectedProduct == null || (this.attachedQuote != null && !this.selectedProduct.getProductCode().equals(this.attachedQuote.getProductCode()))) {
			this.attachedQuote = null;
		}

	} // setSelectedProduct

	public void setFeeEffectiveDate(DateTime effDate) {
		// LogUtil.debug(this.getClass(),"effDate="+effDate);
		this.feeEffectiveDate = effDate;
	}

	public void setProductEffectiveDates() throws ValidationException, RemoteException {
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			// MembershipVO newMemVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.setRenewalPrint(this.printRenewal);
				memTransVO.setProductEffectiveDate();
			}
		}
	}

	/**
	 * Set the term of membership. For transfer transactions only set the additional terms of membership. The transaction list will most likely be populated when the membership term is set.
	 */
	public void setMembershipTerm(Interval membershipTerm) throws RemoteException, ValidationException {
		this.membershipTerm = membershipTerm;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				// Only set the term on new memberships. Not updated
				// memberships.
				if (memTransVO.getNewMembership() != null) {
					memTransVO.getNewMembership().setMembershipTerm(membershipTerm);
				}
			}
		}
	}

	/**
	 * The transaction list may or may not be populated when the membership type is set.
	 */
	public void setMembershipType(MembershipTypeVO membershipType) throws RemoteException, ValidationException {
		this.membershipType = membershipType;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				if (memTransVO.getNewMembership() != null) {
					memTransVO.getNewMembership().setMembershipType(membershipType);
				}
			}
		}
	}

	/**
	 * Set the expiry date for the membership in the group and synchronise the expiry date of every new membership in the transaction group.
	 */
	public void setExpiryDate(DateTime expDate) throws ValidationException {
		this.expiryDate = expDate;
		if (this.transactionList != null) {
			try {
				MembershipTransactionVO transVO = null;
				Iterator it = this.transactionList.iterator();
				while (it.hasNext()) {
					transVO = (MembershipTransactionVO) it.next();
					if (transVO.getNewMembership() != null) {
						transVO.getNewMembership().setExpiryDate(expDate);
					}
				}
			} catch (Exception e) {
				throw new ValidationException("Failed to set expiry date for memberships in the group.", e);
			}
		}
	}

	/**
	 * Set the transaction date and if there are membership transactions set them all to the same transaction date.
	 */
	public void setTransactionDate(DateTime transDate) throws RemoteException, ValidationException {
		this.transactionDate = transDate;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.setTransactionDate(this.transactionDate);
			}
		}
	}

	public void setTransactionGroupID() throws RemoteException {
		int transGroupID = 0;
		if (this.getMembershipTransactionCount() > 1) {
			MembershipIDMgr idMgr = MembershipEJBHelper.getMembershipIDMgr();
			transGroupID = idMgr.getNextTransactionGroupID();
			this.transactionGroupID = transGroupID;
		}
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.setTransactionGroupID(transGroupID); // if 1 then 0
			}
		}
	}

	/**
	 * The transaction list will most likely be populated when the transaction reason is set so propogate the transaction reason to each membership transaction.
	 */
	public void setTransactionReason(ReferenceDataVO transReason) throws RemoteException, ValidationException {
		this.transactionReason = transReason;
		if (this.transactionList != null) {
			String reasonType = null;
			String reasonCode = null;
			if (this.transactionReason != null) {
				// Make sure that the transaction reason is active
				if (!this.transactionReason.isActive()) {
					throw new SystemException("The transaction reason '" + this.transactionReason.getReferenceDataPK().getReferenceCode() + "' is not active and cannot be used.");
				}
				reasonType = this.transactionReason.getReferenceDataPK().getReferenceType();
				reasonCode = this.transactionReason.getReferenceDataPK().getReferenceCode();
			}
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.setTransactionReason(reasonType, reasonCode);
			}
		}
	}

	/**
	 * Set the rejoin type for the transaction. Also set the rejoin type on each membership transaction. If the rejoin type is "new" then the join date and commence date will be set to the preferred commence date.
	 */
	public void setRejoinType(ReferenceDataVO rejoinTypeVO) throws RemoteException, ValidationException {
		this.rejoinType = rejoinTypeVO;

		if (this.rejoinType != null && !this.rejoinType.isActive()) {
			throw new SystemException("The rejoin '" + this.rejoinType.getReferenceDataPK().getReferenceCode() + "' is not active and cannot be used.");
		}

		if (this.transactionList != null && !this.transactionList.isEmpty()) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIterator = this.transactionList.iterator();
			while (transListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIterator.next();
				memTransVO.setRejoinType(rejoinTypeVO);
			}
		} else {
			LogUtil.debug(this.getClass(), "setRejoinType - no transactions");
		}
	}

	/**
	 * Is the rejoin type new?
	 * 
	 * @return boolean
	 */
	public boolean isRejoinTypeNew() {
		boolean rejoinTypeNew = false;
		LogUtil.debug(this.getClass(), "rejoinTypeNew=" + rejoinTypeNew);
		ReferenceDataVO rejoinType = this.getRejoinType();
		if (rejoinType != null) {
			LogUtil.debug(this.getClass(), "rejoinType=" + rejoinType);
			if (rejoinType.getReferenceDataPK().getReferenceCode().equals(ReferenceDataVO.REJOIN_TYPE_NEW)) {
				rejoinTypeNew = true;
			}
		}
		LogUtil.debug(this.getClass(), "rejoinTypeNew=" + rejoinTypeNew);
		return rejoinTypeNew;
	}

	public void setClientSearchList(Vector searchList) {
		this.clientSearchList = searchList;
	}

	public void setContextClient(ClientVO clientVO) throws RemoteException {
		// Find the client in the selected client list.
		SelectedClient selectedClient = getSelectedClient(SelectedClient.getKey(clientVO));
		if (selectedClient == null) {
			throw new SystemException("The context client must be added to the selected client list before being set as the context client.");
		}
		this.contextClient = selectedClient;
	}

	public void setContextClient(MembershipVO memVO) throws RemoteException {
		SelectedClient selectedClient = getSelectedClient(SelectedClient.getKey(memVO));
		if (selectedClient == null) {
			throw new SystemException("The context client must be added to the selected client list before being set as the context client.");
		}
		this.contextClient = selectedClient;
	}

	// private SelectedClient find

	/**
	 * If doing a create, rejoin or renew then set the discount period on the new membership.
	 */
	public void setDiscountPeriod(Interval discountPeriod) throws RemoteException {
		// Must have some transactions to update
		// And we must be doing a single membership transaction
		// i.e Not a create group, renew group or change product for a group
		if (this.transactionList != null && getActualGroupMemberCount() == 1) {
			try {
				MembershipTransactionVO memTransVO;
				Iterator transListIterator = this.transactionList.iterator();
				while (transListIterator.hasNext()) {
					memTransVO = (MembershipTransactionVO) transListIterator.next();
					memTransVO.getNewMembership().setDiscountPeriod(discountPeriod);
				}
			} catch (ValidationException ve) {
				throw new SystemException("Failed to set discount period on memberships : " + ve.getMessage(), ve);
			}
		}
	}

	/**
	 * Add a user specified discretionary discount
	 */
	public void setDiscretionaryDiscount(double amount, String reason) throws RemoteException {
		this.discretionaryDiscountAmount = amount;
		this.discretionaryDiscountReason = reason;

		// Remove any existing discretionary discount adjustment whether
		// it derived from the user or the quote.
		// User discretionary discounts override quote discretionary discounts
		removeAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT);

		if (amount > 0.0) {
			BigDecimal adjustmentAmount = new BigDecimal(amount);
			MembershipTransactionAdjustment memTransAdjustment = new MembershipTransactionAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT, adjustmentAmount, reason);
			addAdjustment(memTransAdjustment);
		}
	}

	public double getDiscretionaryDiscountAmount() {
		return this.discretionaryDiscountAmount;
	}

	public String getDiscretionaryDiscountReason() {
		return this.discretionaryDiscountReason;
	}

	public void setPrimeAddresseeSelectedClientKey(String key) throws RemoteException {
		if (key == null) {
			throw new SystemException("Failed to set prime addressee. The key is null.");
		}

		SelectedClient selectedClient = (SelectedClient) this.selectedClientMap.get(key);
		if (selectedClient == null) {
			throw new RemoteException("The list of selected clients does not contain the key specified!");
		}
		this.paSelectedClientKey = key;

		if (this.transactionList != null) {
			MembershipVO oldMemVO = selectedClient.getMembership();
			MembershipVO newMemVO;
			ClientVO clientVO = selectedClient.getClient();
			Integer clientNumber = clientVO.getClientNumber();
			Integer membershipID = oldMemVO == null ? null : oldMemVO.getMembershipID();
			boolean found = false;
			MembershipTransactionVO memTransVO = null;
			Iterator listIT = this.transactionList.iterator();
			while (listIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) listIT.next();
				oldMemVO = memTransVO.getMembership();
				newMemVO = memTransVO.getNewMembership();

				// Try and match by membership ID otherwise use client ID
				if (!found && newMemVO != null && newMemVO.getMembershipID() != null && membershipID != null && newMemVO.getMembershipID().equals(membershipID)) {
					LogUtil.debug(this.getClass(), "a setting prime addressee" + newMemVO);
					memTransVO.setPrimeAddressee(true);
					newMemVO.setDirectDebitAuthorityID(oldMemVO == null ? null : oldMemVO.getDirectDebitAuthorityID());
					found = true;
				} else if (!found && oldMemVO != null && membershipID != null && oldMemVO.getMembershipID().equals(membershipID)) {
					LogUtil.debug(this.getClass(), "b setting prime addressee" + oldMemVO);
					memTransVO.setPrimeAddressee(true);
					newMemVO.setDirectDebitAuthorityID(oldMemVO == null ? null : oldMemVO.getDirectDebitAuthorityID());
					found = true;
				} else if (!found && newMemVO != null && newMemVO.getClientNumber().equals(clientNumber)) {
					LogUtil.debug(this.getClass(), "c setting prime addressee" + newMemVO);
					memTransVO.setPrimeAddressee(true);
					newMemVO.setDirectDebitAuthorityID(oldMemVO == null ? null : oldMemVO.getDirectDebitAuthorityID());
					found = true;
				} else if (!found && oldMemVO != null && oldMemVO.getClientNumber().equals(clientNumber)) {
					LogUtil.debug(this.getClass(), "d setting prime addressee" + oldMemVO);
					memTransVO.setPrimeAddressee(true);
					newMemVO.setDirectDebitAuthorityID(oldMemVO == null ? null : oldMemVO.getDirectDebitAuthorityID());
					found = true;
				} else {
					LogUtil.debug(this.getClass(), "e setting prime addressee");
					memTransVO.setPrimeAddressee(false);
					newMemVO.setDirectDebitAuthorityID(null);
				}
			}
		}
	}

	/**
	 * Set the new prime addressee
	 */
	public void setPrimeAddressee(Integer membershipID) throws RemoteException {
		if (membershipID == null) {
			throw new SystemException("Unable to set prime addressee. Membership ID is null!");
		}

		if (this.transactionList == null) {
			throw new SystemException("Unable to set prime addressee. Transaction list is empty!");
		}

		this.paSelectedClientKey = null;
		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO;
		MembershipVO newMemVO;
		Iterator listIT = this.transactionList.iterator();
		while (listIT.hasNext()) {
			memTransVO = (MembershipTransactionVO) listIT.next();
			oldMemVO = memTransVO.getMembership();
			newMemVO = memTransVO.getNewMembership();
			if (membershipID.equals(memTransVO.getMembershipID())) {
				memTransVO.setPrimeAddressee(true);
				memTransVO.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA);
				this.paSelectedClientKey = SelectedClient.getKey(oldMemVO);
				newMemVO.setDirectDebitAuthorityID(oldMemVO.getDirectDebitAuthorityID());
			} else {
				memTransVO.setPrimeAddressee(false);
				newMemVO.setDirectDebitAuthorityID(null);
			}
		}

		if (this.paSelectedClientKey == null) {
			throw new SystemException("Failed to set prime addressee. A transaction for membership ID " + membershipID + " could not be found.");
		}

	}

	public void setMembershipProfile(String profileCode) throws RemoteException, ValidationException {
		if (profileCode == null) {
			throw new RemoteException("The membership profile code cannot be set to null.");
		}
		// See if the profile is being changed
		if (this.membershipProfileVO == null || (this.membershipProfileVO != null && !this.membershipProfileVO.getProfileCode().equals(profileCode))) {
			// It is being set or changed so get the new profile value object
			// and set it.
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			MembershipProfileVO profileVO = refMgr.getMembershipProfile(profileCode);
			if (profileVO == null) {
				throw new RemoteException("The membership profile code '" + profileCode + "' could not be found.");
			}

			setMembershipProfile(profileVO);
		}
	}

	public void setMembershipProfile(MembershipProfileVO memProfileVO) throws RemoteException, ValidationException {
		if (memProfileVO == null) {
			throw new RemoteException("The membership profile cannot be set to null.");
		}

		this.membershipProfileVO = memProfileVO;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator listIT = this.transactionList.iterator();
			while (listIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) listIT.next();
				memTransVO.getNewMembership().setMembershipProfile(memProfileVO);
			}
		}
	}

	/************************* Utility methods ******************************/

	/**
	 * Remove the client from the list of clients that will participate in the transaction. Cannot remove the context client.
	 */
	public void removeSelectedClient(String selectedClientKey) {
		if (selectedClientKey != null && this.selectedClientMap != null) {
			SelectedClient selectedClient = (SelectedClient) this.selectedClientMap.get(selectedClientKey);
			if (selectedClient != null) {
				// Can't remove the context client if they have been set.
				if (this.contextClient == null || (this.contextClient != null && !this.contextClient.getKey().equals(selectedClientKey))) {
					// If the client to be removed was the prime addressee then
					// unset them.
					if (selectedClientKey.equals(this.paSelectedClientKey)) {
						this.paSelectedClientKey = null;
					}

					// If the client is a member of the selected membership
					// group
					// then just flag them for removal from the group.
					if (selectedClient.getInvalidReason() == null && selectedClient.getMembership() != null // Has
							// an
							// existing
							// membership
							&& isMembershipInMembershipGroup(selectedClient.getMembership().getMembershipID())) // Is
					// in
					// the
					// selected
					// membership
					// group
					{
						selectedClient.setRemovedFromGroup(true);
					} else {
						this.selectedClientMap.remove(selectedClientKey);
					}
				}
			}
		}
	}

	public void addAdjustment(MembershipTransactionAdjustment memTransAdjustment) throws RemoteException {
		if (hasAdjustment(memTransAdjustment.getAdjustmentTypeCode())) {
			throw new SystemException("Only one adjustment of type '" + memTransAdjustment.getAdjustmentTypeCode() + "' may be added.");
		}
		if (this.adjustmentList == null) {
			this.adjustmentList = new Hashtable();
		}
		this.adjustmentList.put(memTransAdjustment.getAdjustmentTypeCode(), memTransAdjustment);
	}

	public boolean hasAdjustment(String adjTypeCode) throws RemoteException {
		boolean hasAdjustment = false;
		if (this.adjustmentList != null && adjTypeCode != null) {
			hasAdjustment = this.adjustmentList.containsKey(adjTypeCode);
		}
		return hasAdjustment;
	}

	public void removeAdjustment(String adjTypeCode) throws RemoteException {
		if (this.adjustmentList != null) {
			this.adjustmentList.remove(adjTypeCode);
		}
	}

	public MembershipTransactionAdjustment getAdjustment(String adjTypeCode) throws RemoteException {
		MembershipTransactionAdjustment memTransAdjustment = null;
		if (this.adjustmentList != null) {
			memTransAdjustment = (MembershipTransactionAdjustment) this.adjustmentList.get(adjTypeCode);
		}
		return memTransAdjustment;
	}

	public ArrayList getAdjustmentList() throws RemoteException {
		// Return a copy of the list as the list cannot be maintained outside of
		// this class.
		ArrayList returnList = null;
		if (this.adjustmentList != null && !this.adjustmentList.isEmpty()) {
			returnList = new ArrayList(this.adjustmentList.values());
		} else {
			returnList = new ArrayList();
		}
		return returnList;
	}

	/**
	 * Add the recurring discount to all new memberships
	 */
	public void addRecurringDiscount(DiscountTypeVO discTypeVO) throws RemoteException {
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			MembershipVO newMemVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				newMemVO = memTransVO.getNewMembership();
				if (newMemVO != null) {
					newMemVO.addRecurringDiscount(discTypeVO);
				}
			}
		}
	}

	/**
	 * Remove the recurring discount from all new memberships
	 */
	public void removeRecurringDiscount(String discTypeCode) throws RemoteException {
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			MembershipVO newMemVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				newMemVO = memTransVO.getNewMembership();
				if (newMemVO != null) {
					newMemVO.removeRecurringDiscount(discTypeCode);
				}
			}
		}
	}

	/**
	 * Remove the specified discount from all fees attached to all membership transactions
	 */
	public void removeDiscountForAllFees(String discTypeCode) throws RemoteException {
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			// MembershipTransactionFee memTransFee = null;

			// Get the membership transaction fee list for each membership
			// transaction.
			Iterator transListIterator = this.transactionList.iterator();
			while (transListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIterator.next();
				memTransVO.removeDiscountForAllFees(discTypeCode);
			}
		}
	}

	/**
	 * For each membeship transaction add the specified discount to the membership transaction fee for the specified fee type.
	 */
	public void addTransactionFeeDiscount(FeeTypeVO feeTypeVO, DiscountTypeVO discTypeVO) throws RemoteException {
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.addTransactionFeeDiscount(feeTypeVO.getFeeTypeCode(), discTypeVO);
			}
		}
	}

	/**
	 * For each membeship transaction remove all of the discounts attached to the transaction fees.
	 */
	public void removeAllTransactionFeeDiscounts() throws RemoteException {
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				memTransVO.removeAllTransactionFeeDiscounts();

				// unable to revert all credits, etc. as they are only recreated
				// on creation of transactions

			}
		}

	}

	/**
	 * Create a membership transaction for each of the clients that have been selected to be transacted. If an existing membership group has been identified then additional transactions may also need to be generated for the group members even though they do not directly partake in this transaction.
	 */
	public void createTransactionsForSelectedClients() throws RemoteException, ValidationException {
		// logMembershipTransactions("createTransactionsForSelectedClients");
		if (this.getTransactionReason() != null && !(ReferenceDataVO.JOIN_REASON_ON_ROAD.equals(this.getTransactionReason().getReferenceDataPK().getReferenceCode()) || ReferenceDataVO.REJOIN_REASON_ON_ROAD.equals(this.getTransactionReason().getReferenceDataPK().getReferenceCode()))) {
			if (this.getMembershipTerm() != null && this.getMembershipTerm().getTotalYears() > 1) {
				throw new ValidationException("Multiple membership terms are supported for 'On Road' transactions only.");
			}
		}

		LogUtil.debug(this.getClass(), "createTransactionsForSelectedClients");
		if (this.selectedClientMap == null || this.selectedClientMap.isEmpty()) {
			throw new RemoteException("Cannot create membership transactions. The list of selected clients is empty.");
		}
		// Remove any existing membership transactions. This may occur where the
		// user has gone back after completing other parts of the process.
		this.transactionList.clear();

		// remove reference to renewal version of the schedule
		this.directDebitScheduleId = null;

		if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionGroupTypeCode)) {
			doCreateTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionGroupTypeCode)) {
			doRejoinTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionGroupTypeCode)) {
			doRenewTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.transactionGroupTypeCode)) {
			doChangeProductTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.transactionGroupTypeCode)) {
			doCreateGroupTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(this.transactionGroupTypeCode)) {
			doChangeGroupTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(this.transactionGroupTypeCode)) {
			doRenewGroupTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP.equals(this.transactionGroupTypeCode)) {
			doEditMembershipTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST.equals(this.transactionGroupTypeCode)) {
			doRequestCardTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(this.transactionGroupTypeCode)) {
			doHoldTransaction();
		} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(this.transactionGroupTypeCode)) {
			doChangeGroupPATransaction();
		} else {
			throw new SystemException("Transaction type '" + this.transactionGroupTypeCode + "' is not supported yet.");
		}

		// Make sure that the attached quote is still valid. The transaction
		// types
		// and memberships being transacted must be the same.
		if (this.attachedQuote != null && !compareMemberTransactions(this.attachedQuote.getTransactionGroup())) {
			this.attachedQuote = null;
		}
	}

	private void doChangeGroupPATransaction() throws RemoteException, ValidationException {
		if (this.selectedClientMap.size() < 2) {
			throw new ValidationException("At least two clients must be selected when changing a membership group.");
		}
		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		String oldMembershipStatus = null;
		String transTypeCode = null;
		SelectedClient selectedClient;
		DateTime transDate = getTransactionDate();
		Collection clientList = this.selectedClientMap.values();

		LogUtil.debug(this.getClass(), "doChangeGroupPATransaction " + this.paSelectedClientKey + " " + (clientList != null ? "" + clientList.size() : ""));
		Integer membershipID;
		boolean existingGroupMember = false;
		boolean isOldPA = false;
		boolean isNewPA = false;
		MembershipVO oldPAMemVO = getMembershipGroupPA();
		String invalidReason;
		Iterator clientListIterator = clientList.iterator();

		while (clientListIterator.hasNext()) {
			selectedClient = (SelectedClient) clientListIterator.next();
			oldMemVO = selectedClient.getMembership();
			oldMembershipStatus = oldMemVO == null ? null : oldMemVO.getStatus();
			membershipID = oldMemVO == null ? null : oldMemVO.getMembershipID();
			existingGroupMember = isMembershipInMembershipGroup(membershipID);
			isOldPA = oldPAMemVO != null && oldPAMemVO.getMembershipID().equals(membershipID);

			isNewPA = selectedClient.getKey().equals(this.paSelectedClientKey);
			memTransVO = null;

			// old mem may not be null
			LogUtil.debug(this.getClass(), "oldMembershipStatus=" + oldMembershipStatus);
			LogUtil.debug(this.getClass(), "oldMemVO=" + (oldMemVO != null ? oldMemVO.toString() : "null"));
			LogUtil.debug(this.getClass(), "oldPAMemVO=" + (oldPAMemVO != null ? oldPAMemVO.toString() : "null"));
			LogUtil.debug(this.getClass(), "membershipID=" + membershipID);
			LogUtil.debug(this.getClass(), "newPA=" + isNewPA + ",oldPA=" + isOldPA + ",existingGroupMember=" + existingGroupMember);

			invalidReason = selectedClient.getInvalidReason();

			if (existingGroupMember) {
				if (isOldPA != isNewPA) {
					transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA;
				} else {
					// no change so give it a change group transaction
					transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP;

				}
				LogUtil.debug(this.getClass(), " cl" + transTypeCode);
				newMemVO = oldMemVO.copy();
				memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, transTypeCode, selectedClient.getMembershipGroupJoinDate());
				memTransVO.setTransactionDate(transDate);
				memTransVO.setUsername(this.user.getUserID());
				memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
				memTransVO.setMembership(oldMemVO);
				memTransVO.setNewMembership(newMemVO);

				memTransVO.setEffectiveDate(transDate);
				memTransVO.setEndDate(newMemVO.getExpiryDate());
			} else {
				throw new SystemException("invalid reason '" + invalidReason + "' " + existingGroupMember);
			}
			if (memTransVO != null) {
				LogUtil.debug(this.getClass(), "setting new PA!!");
				memTransVO.setPrimeAddressee(isNewPA);
				if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
					setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
				} else {
					memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
				}

				this.transactionList.add(memTransVO);
			}
		}
	}

	private void doCreateTransaction() throws RemoteException, ValidationException {
		MembershipTransactionVO memTransVO = null;
		SelectedClient selectedClient;

		if (this.selectedClientMap.size() > 1) {
			throw new SystemException("Only one client can be selected for create transaction.");
		}

		// Get the one client VO from the list.
		Enumeration keyList = this.selectedClientMap.keys();

		selectedClient = (SelectedClient) this.selectedClientMap.get(keyList.nextElement());
		this.paSelectedClientKey = selectedClient.getKey();
		DateTime expiryDate = getPreferredCommenceDate().add(this.membershipTerm);

		memTransVO = getCreateTransaction(selectedClient, expiryDate);
		memTransVO.setPrimeAddressee(selectedClient.getKey().equals(this.paSelectedClientKey));

		if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
			setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
		} else {
			memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
		}

		this.transactionList.add(memTransVO);

	}

	private void doRejoinTransaction() throws RemoteException, ValidationException {
		MembershipTransactionVO memTransVO = null;
		SelectedClient selectedClient;

		if (this.selectedClientMap.size() > 1) {
			throw new SystemException("Only one client can be selected for rejoin transaction.");
		}

		// Get the one client VO from the list.
		Enumeration keyList = this.selectedClientMap.keys();
		selectedClient = (SelectedClient) this.selectedClientMap.get(keyList.nextElement());
		this.paSelectedClientKey = selectedClient.getKey();
		DateTime prefCommenceDate = getPreferredCommenceDate(); // Defaults to
		// transaction
		// date.
		DateTime expiryDate = prefCommenceDate.add(this.membershipTerm);

		DateTime joinDate = null;
		if (isRejoinTypeNew()) {
			joinDate = prefCommenceDate;
		} else {
			// should alway be an old membership on rejoin
			joinDate = selectedClient.getMembership().getJoinDate();
		}

		memTransVO = getRejoinTransaction(selectedClient, joinDate, prefCommenceDate, prefCommenceDate, expiryDate, prefCommenceDate);
		memTransVO.setPrimeAddressee(selectedClient.getKey().equals(this.paSelectedClientKey));

		if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
			setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
		} else {
			memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
		}

		this.transactionList.add(memTransVO);
	}

	/**
	 * Identify the transaction types that may result in the replacement of a direct debit schedule if one exists.
	 * 
	 * @return
	 */
	public boolean replacesTransaction() throws RemoteException {
		boolean replacesTransaction = false;
		Collection txList = this.transactionList;
		Iterator txIt = txList.iterator();
		String paymentMethodDescription = (this.getDirectDebitAuthority() != null ? PaymentMethod.DIRECT_DEBIT : PaymentMethod.RECEIPTING);
		MembershipTransactionVO txVO = null;
		while (txIt.hasNext() && !replacesTransaction) {
			txVO = (MembershipTransactionVO) txIt.next();
			replacesTransaction = txVO.isSuperseding(paymentMethodDescription);
		}
		return replacesTransaction;

	}

	private void doRenewTransaction() throws RemoteException, ValidationException {

		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		SelectedClient selectedClient;
		if (this.selectedClientMap.size() > 1) {
			throw new SystemException("Only one client can be selected for renew transaction.");
		}

		if (this.selectedProduct == null) {
			throw new SystemException("A product must be set before the renewal transaction is created.");
		}

		// Get the one client VO from the list.
		Enumeration keyList = this.selectedClientMap.keys();
		selectedClient = (SelectedClient) this.selectedClientMap.get(keyList.nextElement());
		this.paSelectedClientKey = selectedClient.getKey();
		oldMemVO = selectedClient.getMembership();

		DateTime oldExpiryDate = oldMemVO.getExpiryDate();

		DateTime newExpiryDate = null;
		if (MembershipVO.STATUS_ONHOLD.equals(oldMemVO.getStatus())) {
			if (this.printRenewal) {
				newExpiryDate = oldMemVO.getExpiryDate().add(this.membershipTerm);
			} else {
				newExpiryDate = this.transactionDate.add(this.membershipTerm);
			}
		} else {
			newExpiryDate = oldExpiryDate.add(this.membershipTerm);
		}

		memTransVO = getRenewTransaction(selectedClient, newExpiryDate);
		memTransVO.setPrimeAddressee(selectedClient.getKey().equals(this.paSelectedClientKey));
		newMemVO = memTransVO.getNewMembership();

		if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
			setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
		} else {
			newMemVO.setDirectDebitAuthorityID(null);
		}

		this.transactionList.add(memTransVO);
	}

	private void doChangeProductTransaction() throws RemoteException, ValidationException {
		MembershipTransactionVO memTransVO = null;
		SelectedClient selectedClient;

		if (this.selectedProduct == null) {
			throw new SystemException("A product must be set before the change product transaction is created.");
		}

		Collection clientList = this.selectedClientMap.values();
		Iterator clientListIterator = clientList.iterator();
		while (clientListIterator.hasNext()) {
			selectedClient = (SelectedClient) clientListIterator.next();
			memTransVO = getChangeProductTransaction(selectedClient);
			memTransVO.setPrimeAddressee(selectedClient.getKey().equals(this.paSelectedClientKey));

			if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
				setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
			} else {
				memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
			}

			this.transactionList.add(memTransVO);
		}
	}

	/**
	 * Update the credit amount to reflect the used unearned value.
	 * 
	 * @param oldMemVO
	 *            MembershipVO
	 * @param newMemVO
	 *            MembershipVO
	 * @throws RemoteException
	 */
	// private void adjustUnearnedValue(MembershipTransactionVO memTransVO)
	// throws RemoteException
	// {
	// MembershipVO oldMemVO = memTransVO.getMembership();
	// MembershipVO newMemVO = memTransVO.getNewMembership();
	// UnearnedValue unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
	// if(unearnedValue.getUnearnedAmount().doubleValue() > 0.0)
	// {
	// double creditAmount = newMemVO.getCreditAmount();
	// LogUtil.debug(this.getClass(),"adjust unearned value "+creditAmount+" "+unearnedValue.toString());
	// newMemVO.setCreditAmount(creditAmount +
	// unearnedValue.getUnearnedAmount().doubleValue());
	// }
	// }

	/**
	 * Adjust the transfer credit to reflect the used transfer credit.
	 * 
	 * @param oldMemVO
	 *            MembershipVO
	 * @param newMemVO
	 *            MembershipVO
	 * @throws RemoteException
	 */
	// private void adjustTransferCredit(MembershipTransactionVO memTransVO)
	// throws RemoteException
	// {
	// MembershipVO oldMemVO = memTransVO.getMembership();
	// MembershipVO newMemVO = memTransVO.getNewMembership();
	// double affiliatedTransferCreditAmount =
	// oldMemVO.getAffiliatedClubCreditAmount();
	// if(affiliatedTransferCreditAmount > 0.0)
	// {
	// double creditAmount = newMemVO.getAffiliatedClubCreditAmount();
	// LogUtil.debug(this.getClass(),"adjust transfer credit "+creditAmount
	// +affiliatedTransferCreditAmount);
	// newMemVO.setAffiliatedClubCreditAmount(affiliatedTransferCreditAmount +
	// creditAmount);
	// }
	// }

	private void doHoldTransaction() throws RemoteException, ValidationException {
		MembershipVO oldMemVO = null;
		SelectedClient selectedClient;
		ClientVO clientVO = null;
		Collection clientList = this.selectedClientMap.values();

		Iterator clientListIT = clientList.iterator();
		// only one
		while (clientListIT.hasNext()) {
			selectedClient = (SelectedClient) clientListIT.next();
			clientVO = selectedClient.getClient();
			oldMemVO = selectedClient.getMembership();

			// The context client is the one being put on hold.
			if (oldMemVO.getMembershipID().equals(this.contextClient.getMembership().getMembershipID())) {

				MembershipTransactionVO memTransHold = new MemTransHold(this.transactionGroupTypeCode, this.transactionDate, oldMemVO, this.user, selectedClient.getKey().equals(this.paSelectedClientKey), selectedClient.getMembershipGroupJoinDate());

				if (memTransHold.isPrimeAddressee()) {
					setDirectDebitAuthority(memTransHold.getMembership().getDirectDebitAuthority(), true);
				} else {
					memTransHold.getNewMembership().setDirectDebitAuthorityID(null);
				}

				// Work out the unearned credit that can be used on the next
				// transaction
				// if the status is not already on hold.
				if (!MembershipVO.STATUS_ONHOLD.equals(oldMemVO.getStatus())) {
					adjustUnearnedCreditAmount(memTransHold);
				}
				this.transactionList.add(memTransHold);
			} else {
				throw new SystemException("Hold may be performed on an individual only.");
			}
		}
	}

	/**
	 * Create membership transaction instances for all selected clients where the group transaction is EDIT_MEMEBRSHIP
	 */
	private void doEditMembershipTransaction() throws RemoteException, ValidationException {
		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		SelectedClient selectedClient;
		ClientVO clientVO = null;
		DateTime transDate = getTransactionDate();
		Collection clientList = this.selectedClientMap.values();

		Iterator clientListIT = clientList.iterator();
		while (clientListIT.hasNext()) {
			selectedClient = (SelectedClient) clientListIT.next();
			clientVO = selectedClient.getClient();
			oldMemVO = selectedClient.getMembership();

			// The context client is the one being edited.
			if (oldMemVO.getMembershipID().equals(this.contextClient.getMembership().getMembershipID())) {
				// Copy the membership being cancelled and set its status
				newMemVO = oldMemVO.copy();

				// Create a new transaction object for the membership
				// transaction
				memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_EDIT_MEMBERSHIP, selectedClient.getMembershipGroupJoinDate());
				memTransVO.setTransactionDate(transDate);
				memTransVO.setUsername(this.user.getUserID());
				memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
				memTransVO.setMembership(oldMemVO);
				memTransVO.setNewMembership(newMemVO);

				memTransVO.setEffectiveDate(transDate);
				memTransVO.setEndDate(newMemVO.getExpiryDate());
			} else {
				// The other memberships belong to the same group as the
				// membership
				// being edited. Create a change of group transaction.
				newMemVO = oldMemVO.copy();

				// Create a membership transaction
				memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP, selectedClient.getMembershipGroupJoinDate());
				memTransVO.setTransactionDate(transDate);
				memTransVO.setUsername(this.user.getUserID());
				memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
				memTransVO.setMembership(oldMemVO);
				memTransVO.setNewMembership(newMemVO);

				memTransVO.setEffectiveDate(transDate);
				memTransVO.setEndDate(newMemVO.getExpiryDate());
			}
			memTransVO.setPrimeAddressee(oldMemVO.isPrimeAddressee());
			this.transactionList.add(memTransVO);
		}
	}

	/**
	 * Create card request transaction
	 */
	private void doRequestCardTransaction() throws RemoteException, ValidationException {
		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		SelectedClient selectedClient;
		ClientVO clientVO = null;
		DateTime transDate = getTransactionDate();
		Collection clientList = this.selectedClientMap.values();
		String txType = "";

		Iterator clientListIT = clientList.iterator();
		while (clientListIT.hasNext()) {
			selectedClient = (SelectedClient) clientListIT.next();
			clientVO = selectedClient.getClient();
			oldMemVO = selectedClient.getMembership();

			// The context client is the one being edited.
			if (oldMemVO.getMembershipID().equals(this.contextClient.getMembership().getMembershipID())) {
				// Copy the membership being cancelled and set its status
				txType = MembershipTransactionTypeVO.TRANSACTION_TYPE_CARD_REPLACEMENT_REQUEST;
			} else {
				// The other memberships belong to the same group.
				// Create a change of group transaction
				txType = MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP;
			}

			// new membership same as old
			newMemVO = oldMemVO.copy();

			// Create a new transaction object for the membership transaction
			memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, txType, selectedClient.getMembershipGroupJoinDate());
			// reset the old membership PA
			memTransVO.setPrimeAddressee(oldMemVO.isPrimeAddressee());
			memTransVO.setTransactionDate(transDate);
			memTransVO.setUsername(this.user.getUserID());
			memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
			memTransVO.setMembership(oldMemVO);
			memTransVO.setNewMembership(newMemVO);

			memTransVO.setEffectiveDate(transDate);
			memTransVO.setEndDate(newMemVO.getExpiryDate());

			this.transactionList.add(memTransVO);
		}
	}

	/**
	 * Create membership transaction for memberships that are added to a new membership group. All memberships in the new membership group will expire one term from the transaction date.
	 */
	private void doCreateGroupTransaction() throws RemoteException, ValidationException {
		if (this.selectedClientMap.size() < 2) {
			throw new ValidationException("At least two clients must be selected when creating a membership group.");
		}
		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		String oldMembershipStatus = null;
		SelectedClient selectedClient;
		ClientVO clientVO = null;
		DateTime transDate = getTransactionDate();
		Collection clientList = this.selectedClientMap.values();
		// DateTime groupExpiryDate = transDate.add(this.membershipTerm);
		DateTime groupExpiryDate = this.getExpiryDate();
		if (groupExpiryDate == null) {
			groupExpiryDate = transDate.add(this.membershipTerm);
		}
		// check for any product that may not be grouped together
		if (!this.selectedProduct.isGroupable()) {
			throw new RemoteException("Membership has product '" + this.selectedProduct.getProductCode() + "' which is not groupable.");
		}
		LogUtil.debug(this.getClass(), "creating group " + this.selectedProduct);
		Iterator clientListIterator = clientList.iterator();
		while (clientListIterator.hasNext()) {
			selectedClient = (SelectedClient) clientListIterator.next();
			clientVO = selectedClient.getClient();
			oldMemVO = selectedClient.getMembership();

			oldMembershipStatus = oldMemVO == null ? null : oldMemVO.getStatus();
			memTransVO = null;

			// If the client does not have an existing membership then we create
			// a new one.
			if (oldMemVO == null) {
				// Create a new membership
				memTransVO = getCreateTransaction(selectedClient, groupExpiryDate);
			}
			// If the existing membership is cancelled or it has expired and
			// its not on hold then we rejoin it.
			else if (MembershipVO.STATUS_CANCELLED.equals(oldMembershipStatus) || (MembershipVO.STATUS_EXPIRED.equals(oldMembershipStatus) // this.transactionDate.afterDay(oldMemVO.getExpiryDate())
					&& !oldMemVO.isActive() // MEM-67 16/08/2006
			&& !MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus)) || MembershipVO.STATUS_UNDONE.equals(oldMembershipStatus)) {
				// Rejoin the membership. Leave the join date unchanged.
				DateTime oldJoinDate = oldMemVO.getJoinDate();
				memTransVO = getRejoinTransaction(selectedClient, oldJoinDate, transDate, transDate, groupExpiryDate, transDate);

			}
			// If the membership is on hold or the membership has not expired
			// yet
			// and its not cancelled then we look at the product on the
			// membership
			// to decide what to do.
			else if (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || (oldMemVO.isActive() && !MembershipVO.STATUS_CANCELLED.equals(oldMembershipStatus))) {
				LogUtil.debug(this.getClass(), "in onhold or not expired yet");
				// If the product has changed we do a change of product
				// transaction otherwise a renewal transaction.
				if (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || this.selectedProduct == null || oldMemVO.getProductCode().equals(this.selectedProduct.getProductCode())) {
					LogUtil.debug(this.getClass(), " onhold and no change");
					// No product selected yet or the membership is on hold or
					// the product has not been changed.
					// Do a renewal on the membership.
					memTransVO = getRenewTransaction(selectedClient, groupExpiryDate);
					// renew is normally from the last expiry date
					memTransVO.setEffectiveDate(transDate);
					memTransVO.setEndDate(groupExpiryDate);

				} else {
					LogUtil.debug(this.getClass(), " else part");
					// The product has been changed so do a variation of a
					// change of product transaction. Down grade of product is
					// done
					// now and not flagged for next renewal.
					newMemVO = oldMemVO.copy(true);
					newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
					newMemVO.setAllowToLapse(Boolean.valueOf(false)); // Should
					// already
					// be
					// false
					// according
					// to the
					// business
					// rules
					// but do
					// it any
					// way in
					// case
					// the
					// rules
					// have
					// been
					// changed
					// elsewhere.
					newMemVO.setCreditPeriod(null);
					newMemVO.setExpiryDate(groupExpiryDate);
					newMemVO.setNextProductCode(null);
					newMemVO.setProduct(this.selectedProduct);

					memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, selectedClient.getMembershipGroupJoinDate());
					memTransVO.setTransactionDate(transDate);
					memTransVO.setUsername(this.user.getUserID());
					memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
					memTransVO.setMembership(oldMemVO);
					memTransVO.setNewMembership(newMemVO);

					LogUtil.debug(this.getClass(), "prod eff" + memTransVO.getProductEffectiveDate());
					memTransVO.setProductEffectiveDate();

					// could be the wrong date
					LogUtil.debug(this.getClass(), "prod eff" + memTransVO.getProductEffectiveDate());
					memTransVO.setEffectiveDate(memTransVO.getProductEffectiveDate());
					memTransVO.setEndDate(newMemVO.getExpiryDate());
				}
			} else {
				throw new SystemException("Unable to create a membership group on " + this.transactionDate + " with membership number '" + oldMemVO.getMembershipNumber() + "'. Status = " + oldMemVO.getStatus() + ", expiry date = " + oldMemVO.getExpiryDate());
			}

			if (memTransVO != null) {
				memTransVO.setPrimeAddressee(selectedClient.getKey().equals(this.paSelectedClientKey));
				if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
					setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
				} else {
					memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
				}

				this.transactionList.add(memTransVO);
			}
		}
	}

	/**
	 * Create membership transactions for the memberships in the existing membership group. The expiry date of the existing members of the group will not change. New and existing non group members added to the group will have their expiry date synchronised with the groups expiry date. Existing group members may be removed from the group. A membership transaction is created for these memberships and tag with the remove from group transaction type. This tells the processTransaction method just update the membership group records. The prime addressee of the group may be changed. If no other change is made to either the old prime addressee or the new prime addressee then a membership transactioin is created for them which is tagged with the change PA transaction type. This tells the processTransaction method to just update the membership group records.
	 */
	private void doChangeGroupTransaction() throws RemoteException, ValidationException {
		if (this.selectedClientMap.size() < 2) {
			throw new ValidationException("At least two clients must be selected when changing a membership group.");
		}
		LogUtil.debug(this.getClass(), "doChangeGroupTransaction");
		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		String oldMembershipStatus = null;
		String transTypeCode;
		SelectedClient selectedClient;
		// ClientVO clientVO = null;
		DateTime transDate = getTransactionDate();
		Collection clientList = this.selectedClientMap.values();
		DateTime expiryDate = this.getMembershipGroupExpiryDate();
		Integer membershipID;
		boolean existingGroupMember = false;
		boolean isOldPA = false;
		boolean isNewPA = false;
		MembershipVO oldPAMemVO = getMembershipGroupPA();
		Iterator clientListIterator = clientList.iterator();
		while (clientListIterator.hasNext()) {
			selectedClient = (SelectedClient) clientListIterator.next();
			oldMemVO = selectedClient.getMembership();
			oldMembershipStatus = oldMemVO == null ? null : oldMemVO.getStatus();
			membershipID = oldMemVO == null ? null : oldMemVO.getMembershipID();
			existingGroupMember = isMembershipInMembershipGroup(membershipID);
			isOldPA = oldPAMemVO != null && oldPAMemVO.getMembershipID().equals(membershipID);
			isNewPA = selectedClient.getKey().equals(this.paSelectedClientKey);
			memTransVO = null;

			if (!existingGroupMember && this.transactionDate.after(expiryDate)) // expired
			// group
			{
				throw new SystemException("Unable to add member into group as group has expired. Add the member during a renew group transaction.");
			}

			if (oldMemVO == null) {
				// Create a new membership
				memTransVO = getCreateTransaction(selectedClient, expiryDate);
			} else if (selectedClient.getInvalidReason() != null || selectedClient.isRemovedFromGroup()) {
				// The client/membership is not valid for the transaction
				// or it is to be removed from the membership group.
				// Create a "remove from group" transaction if they are a member
				// of the selected membership group. Otherwise don't create a
				// transaction
				if (existingGroupMember) {
					newMemVO = oldMemVO.copy();
					memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP, selectedClient.getMembershipGroupJoinDate());
					memTransVO.setTransactionDate(transDate);
					memTransVO.setUsername(this.user.getUserID());
					memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
					memTransVO.setMembership(oldMemVO);

					memTransVO.setNewMembership(newMemVO);

					// if the membership has not expired
					if (!MembershipVO.STATUS_EXPIRED.equals(oldMembershipStatus)) {
						LogUtil.debug(this.getClass(), "doChangeGroupTransaction:  get the unearned value");
						// get the unearned credit directly as it has not been
						// set as credit
						UnearnedValue unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
						LogUtil.debug(this.getClass(), "\nunearned value for " + oldMemVO.getClientNumber() + " = " + unearnedValue);
						DateTime newExpiry = null;
						if (unearnedValue != null) {
							// new expiry is now plus unearned and paid period
							newExpiry = transDate.add(unearnedValue.getUnearnedPeriod());
							LogUtil.debug(this.getClass(), "\nunearned period = " + unearnedValue.getUnearnedPeriod().getTotalDays() + "\nnew Expiry date = " + newExpiry);

							newMemVO.setExpiryDate(newExpiry);
						} else {
							// no credit so default to today otherwise the
							// default getExpiryDate will return today plus one
							// default term.

							// should be in the past
							newMemVO.setExpiryDate(transDate);
						}

						LogUtil.debug(this.getClass(), "newExpiryDate " + newMemVO.getExpiryDate());
						/*
						 * not fully implemented //cater for automatic complimentary credit ComplimentaryCredit compCredit = (ComplimentaryCredit)this. getComplimentaryCredit(memTransVO);
						 * 
						 * //only applicable if they have actually payed at least one payment or no debt. if(compCredit != null && this.hasPAPaidFirstPayment(oldMemVO)) { newExpiry = newMemVO.getExpiryDate().add(compCredit. getComplimentaryCreditPeriod()); LogUtil.debug(this.getClass(), "compCredit " +compCredit.getCreditDescription()); newMemVO.setTransactionCredit(TransactionCredit. COMPLIMENTARY_CREDIT, compCredit); //update for changes attributable to complimentary credit newMemVO.setExpiryDate(newExpiry); throw new SystemException("Member '"+membershipID+ "' will not have sufficient credit for 14 days "+ "following remove from group. Payment is required for the transaction to proceed." ); }
						 */
					}
					LogUtil.debug(this.getClass(), "newExpiryDate " + newMemVO.getExpiryDate());

					memTransVO.setEffectiveDate(transDate);
					memTransVO.setEndDate(newMemVO.getExpiryDate());
				} else {
					throw new SystemException("Failed to remove membership from group for client " + selectedClient.getClient().getClientNumber() + ". The client does not belong to the group!");
				}
			} else if (existingGroupMember && isMembershipGroupProductChanged()) {
				// The product has been changed so do a variation of a
				// change of product transaction. Down grade of product is done
				// now and not flagged for next renewal.
				newMemVO = oldMemVO.copy(true);
				newMemVO.setProduct(this.selectedProduct);

				memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, selectedClient.getMembershipGroupJoinDate());
				memTransVO.setTransactionDate(transDate);
				memTransVO.setUsername(this.user.getUserID());
				memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
				memTransVO.setMembership(oldMemVO);
				memTransVO.setNewMembership(newMemVO);
				memTransVO.setProductEffectiveDate();

				memTransVO.setEffectiveDate(newMemVO.getCommenceDate());
				memTransVO.setEndDate(newMemVO.getExpiryDate());
			} else if (existingGroupMember && !isMembershipGroupProductChanged()) // Already
			// a
			// member
			// of
			// the
			// group
			{
				// Base product has not changed
				// The membership already an existing member of the group
				// They are not being added or removed.
				// No change has been made to cause the membership itself to be
				// updated.
				// See if the membership group details need updating.
				if (isOldPA == isNewPA) {
					// No change to the PA status
					// Tag the transaction with the change of group transaction.
					transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP;
				} else {
					// They are were either the old PA and are not the new PA
					// or they were not the old PA and are the new PA
					// Tag the transaction with a change PA transaction type
					// so that the transactiuon manager updates the membership
					// group records.
					throw new SystemException("A change group PA transaction is not allowed in the context of another transaction");
				}

				newMemVO = oldMemVO.copy();
				memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, transTypeCode, selectedClient.getMembershipGroupJoinDate());
				memTransVO.setTransactionDate(transDate);
				memTransVO.setUsername(this.user.getUserID());
				memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
				memTransVO.setMembership(oldMemVO);
				memTransVO.setNewMembership(newMemVO);

				memTransVO.setEffectiveDate(transDate);
				memTransVO.setEndDate(newMemVO.getExpiryDate());

			} else if (!existingGroupMember && ((MembershipVO.STATUS_CANCELLED.equals(oldMembershipStatus) // and
					// is
					// cancelled
					|| (MembershipVO.STATUS_EXPIRED.equals(oldMembershipStatus) // or
					// expired
					// and
					// not
					// on
					// hold
					&& !MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus))) || MembershipVO.STATUS_UNDONE.equals(oldMembershipStatus))) {
				// Rejoin the membership. Leave the join date unchanged.
				DateTime oldJoinDate = oldMemVO.getJoinDate();
				memTransVO = getRejoinTransaction(selectedClient, oldJoinDate, transDate, transDate, expiryDate, transDate);

			} else if (!existingGroupMember && (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) // and
					// is
					// on
					// hold
					|| (!MembershipVO.STATUS_EXPIRED.equals(oldMembershipStatus) // or
					// is
					// not
					// expired
					// and
					// not
					// cancelled
					&& !MembershipVO.STATUS_CANCELLED.equals(oldMembershipStatus)))) {
				LogUtil.debug(this.getClass(), "not existing");
				// If the product has changed we do a change of product
				// transaction
				// otherwise a renewal transaction.
				if (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || this.selectedProduct == null || oldMemVO.getProductCode().equals(this.selectedProduct.getProductCode())) {
					LogUtil.debug(this.getClass(), "no prod change or on hold");
					// No product selected yet or the membership is on hold or
					// the product has not been changed.
					// Do a part term renewal on the membership
					memTransVO = getRenewTransaction(selectedClient, expiryDate);
					// renew is normally from the last expiry date
					memTransVO.setEffectiveDate(transDate);
					memTransVO.setEndDate(expiryDate);

				} else {
					LogUtil.debug(this.getClass(), "not on hold");
					// The product has been changed so do a variation of a
					// change of product transaction. Down grade of product is
					// done
					// now and not flagged for next renewal.
					newMemVO = oldMemVO.copy(true);
					newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
					newMemVO.setAllowToLapse(Boolean.valueOf(false)); // Should
					// already
					// be
					// false
					// according
					// to the
					// business
					// rules
					// but do
					// it any
					// way in
					// case
					// the
					// rules
					// have
					// been
					// changed
					// elsewhere.
					newMemVO.setCreditPeriod(null);
					newMemVO.setExpiryDate(expiryDate);
					newMemVO.setNextProductCode(null);
					newMemVO.setProduct(this.selectedProduct);

					memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, selectedClient.getMembershipGroupJoinDate());
					memTransVO.setTransactionDate(transDate);
					memTransVO.setUsername(this.user.getUserID());
					memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
					memTransVO.setMembership(oldMemVO);
					memTransVO.setNewMembership(newMemVO);
					memTransVO.setProductEffectiveDate();

					memTransVO.setEffectiveDate(newMemVO.getCommenceDate());
					memTransVO.setEndDate(newMemVO.getExpiryDate());
				}
			} else {
				throw new SystemException("Unable to change a membership group on " + this.transactionDate + " with membership number '" + oldMemVO.getMembershipNumber() + "'. Status = " + oldMemVO.getStatus() + ", expiry date = " + oldMemVO.getExpiryDate());
			}

			if (memTransVO != null) {
				memTransVO.setPrimeAddressee(isNewPA);
				if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
					setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
				} else {
					LogUtil.debug(this.getClass(), "" + memTransVO + " " + memTransVO.getNewMembership());
					memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
				}

				this.transactionList.add(memTransVO);
			}
		}
	}

	/**
	 * Create membership transactions for the memberships in the existing membership group that is being renewed. The expiry date of the members in the group will be set to one term after the current expiry date. New and existing non group members added to the group will have their expiry date synchronised with the groups new expiry date. Existing group members may be removed from the group. A membership transaction is created for these memberships and tag with the remove from group transaction type. This tells the processTransaction method to just update the membership group records. If no other change is made to either the old prime addressee or the new prime addressee then a membership transaction is created for them which is tagged with the change PA transaction type. This tells the processTransaction method to just update the membership group records.
	 */
	private void doRenewGroupTransaction() throws RemoteException, ValidationException {
		MembershipTransactionVO memTransVO = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		String oldMembershipStatus = null;
		SelectedClient selectedClient;
		// ClientVO clientVO = null;
		DateTime transDate = getTransactionDate();
		BigDecimal affiliatedClubCreditAmount = new BigDecimal(0);
		Collection clientList = this.selectedClientMap.values();

		if (clientList.size() < 2) {
			throw new ValidationException("At least two clients must be selected when renewing a membership group.");
		}
		if (this.selectedMembershipGroup == null) {
			throw new SystemException("Failed to get the membership group being renewed.");
		}
		DateTime expiryDate = this.selectedMembershipGroup.getMembershipGroupExpiryDate().add(this.membershipTerm);

		Iterator clientListIterator = clientList.iterator();
		while (clientListIterator.hasNext()) {
			selectedClient = (SelectedClient) clientListIterator.next();
			oldMemVO = selectedClient.getMembership();
			oldMembershipStatus = oldMemVO == null ? null : oldMemVO.getStatus();
			memTransVO = null;

			if (oldMemVO == null) {
				// Create a new membership
				memTransVO = getCreateTransaction(selectedClient, expiryDate);
			} else if (selectedClient.getInvalidReason() != null || selectedClient.isRemovedFromGroup()) {
				// The client/membership is not valid for the transaction
				// or it is to be removed from the membership group.
				// Create a "remove from group" transaction if they are a member
				// of the selected membership group. Otherwise don't create a
				// transaction
				if (isMembershipInMembershipGroup(oldMemVO.getMembershipID())) {
					newMemVO = oldMemVO.copy();

					memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP, selectedClient.getMembershipGroupJoinDate());
					memTransVO.setTransactionDate(transDate);
					memTransVO.setUsername(this.user.getUserID());
					memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
					memTransVO.setMembership(oldMemVO);
					memTransVO.setNewMembership(newMemVO);

					memTransVO.setEffectiveDate(transDate);
					memTransVO.setEndDate(newMemVO.getExpiryDate());
				}

			} else if (MembershipVO.STATUS_CANCELLED.equals(oldMembershipStatus) || MembershipVO.STATUS_UNDONE.equals(oldMembershipStatus)) {
				// Rejoin the membership. Leave the join date unchanged.
				DateTime oldJoinDate = oldMemVO.getJoinDate();
				memTransVO = getRejoinTransaction(selectedClient, oldJoinDate, transDate, transDate, expiryDate, transDate);
			} else if (MembershipVO.STATUS_ACTIVE.equals(oldMembershipStatus) || MembershipVO.STATUS_FUTURE.equals(oldMembershipStatus) || MembershipVO.STATUS_INRENEWAL.equals(oldMembershipStatus) || MembershipVO.STATUS_EXPIRED.equals(oldMembershipStatus) || MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus)) {
				// If the product has changed, on hold or undone? we do a change
				// of product transaction otherwise a renewal transaction.
				if (MembershipVO.STATUS_ONHOLD.equals(oldMembershipStatus) || this.selectedProduct == null || oldMemVO.getProductCode().equals(this.selectedProduct.getProductCode())) {

					// No product selected yet or the membership is on hold or
					// the product has not been changed.
					// Do a part term renewal on the membership
					memTransVO = getRenewTransaction(selectedClient, expiryDate);
				} else {
					// The product has been changed so do a variation of a
					// change of product transaction. Down grade of product is
					// done
					// now and not flagged for next renewal.
					newMemVO = oldMemVO.copy(true);
					newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
					newMemVO.setAllowToLapse(Boolean.valueOf(false)); // Should
					// already
					// be
					// false
					// according
					// to the
					// business
					// rules
					// but do
					// it any
					// way in
					// case
					// the
					// rules
					// have
					// been
					// changed
					// elsewhere.
					newMemVO.setCreditPeriod(null);
					newMemVO.setExpiryDate(expiryDate);
					newMemVO.setNextProductCode(null);
					newMemVO.setProduct(this.selectedProduct);

					memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, selectedClient.getMembershipGroupJoinDate());
					memTransVO.setTransactionDate(transDate);
					memTransVO.setUsername(this.user.getUserID());
					memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
					memTransVO.setMembership(oldMemVO);
					memTransVO.setNewMembership(newMemVO);
					memTransVO.setProductEffectiveDate();

					memTransVO.setEffectiveDate(memTransVO.getProductEffectiveDate());
					memTransVO.setEndDate(newMemVO.getExpiryDate());
				}
			} else {
				throw new SystemException("Unable to renew membership group on " + this.transactionDate + " with membership number '" + oldMemVO.getMembershipNumber() + "'. Status = " + oldMemVO.getStatus() + ", expiry date = " + oldMemVO.getExpiryDate());
			}

			if (memTransVO != null) {
				memTransVO.setPrimeAddressee(selectedClient.getKey().equals(this.paSelectedClientKey));
				if (memTransVO.isUpdatingMembership() && memTransVO.isPrimeAddressee()) {
					setDirectDebitAuthority(memTransVO.getMembership().getDirectDebitAuthority(), true);
				} else {
					memTransVO.getNewMembership().setDirectDebitAuthorityID(null);
				}

				this.transactionList.add(memTransVO);
			}
		}
	}

	protected MembershipTransactionVO getCreateTransaction(SelectedClient selectedClient, DateTime expiryDate) throws RemoteException, ValidationException {
		ClientVO clientVO = selectedClient.getClient();
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = new MembershipVO();
		newMemVO.setMembershipType(this.membershipType);
		newMemVO.setMembershipNumber(clientVO.getClientNumber().toString()); // Membership
		// numbers
		// are
		// the
		// same
		// as
		// client
		// numbers
		// for
		// the
		// moment
		newMemVO.setClient(clientVO);
		newMemVO.setMembershipTerm(this.membershipTerm);

		DateTime preferredCommenceDate = getPreferredCommenceDate(); // Defaults
		// to the
		// transaction
		// date.

		// The join date, commence date, product effective date and product
		// option effective dates of the membership are set when the
		// preferred commence date of the membership transaction is set below.
		newMemVO.setExpiryDate(expiryDate);

		if (this.membershipProfileVO != null) {
			newMemVO.setMembershipProfile(this.membershipProfileVO);
		} else {
			newMemVO.setMembershipProfile(MembershipEJBHelper.getMembershipRefMgr().getDefaultMembershipProfile());

		}
		if (this.selectedProduct != null) {
			newMemVO.setProduct(this.selectedProduct);
		}

		// Create a new transaction object for the membership transaction
		MembershipTransactionVO memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, selectedClient.getMembershipGroupJoinDate());
		memTransVO.setTransactionDate(this.transactionDate);
		memTransVO.setMembership(oldMemVO);
		memTransVO.setNewMembership(newMemVO);
		memTransVO.setPreferredCommenceDate(preferredCommenceDate, true); // Also
		// sets
		// the
		// join,
		// commence,
		// product
		// effective
		// and
		// product
		// option
		// effective
		// dates
		memTransVO.setUsername(this.user.getUserID());
		memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
		memTransVO.setProductEffectiveDate();

		memTransVO.setEffectiveDate(preferredCommenceDate);
		memTransVO.setEndDate(newMemVO.getExpiryDate());
		return memTransVO;
	}

	private MembershipTransactionVO getRejoinTransaction(SelectedClient selectedClient, DateTime joinDate, DateTime commenceDate, DateTime productEffectiveDate, DateTime expiryDate, DateTime prefComDate) throws RemoteException, ValidationException {
		MembershipVO oldMemVO = selectedClient.getMembership();
		MembershipVO newMemVO = oldMemVO.copy(true);

		newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
		newMemVO.setAllowToLapse(Boolean.valueOf(false));
		newMemVO.setJoinDate(joinDate);
		newMemVO.setCommenceDate(commenceDate);
		newMemVO.setProductEffectiveDate(productEffectiveDate);
		newMemVO.setExpiryDate(expiryDate);
		newMemVO.setNextProductCode(null);
		// Since the newMemVO is a copy of the oldMemVO you only need to set the
		// things that have changed. TB
		// newMemVO.setCreditAmount(oldMemVO.getCreditAmount());

		if (this.selectedProduct != null) {
			newMemVO.setProduct(this.selectedProduct);
		}

		// Create a new transaction object for the membership transaction
		MembershipTransactionVO memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN, selectedClient.getMembershipGroupJoinDate());
		memTransVO.setTransactionDate(this.transactionDate);
		memTransVO.setUsername(this.user.getUserID());
		memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
		memTransVO.setMembership(oldMemVO);
		memTransVO.setNewMembership(newMemVO);
		memTransVO.setPreferredCommenceDate(prefComDate, true);
		memTransVO.setProductEffectiveDate();

		memTransVO.setEffectiveDate(prefComDate);
		memTransVO.setEndDate(newMemVO.getExpiryDate());
		return memTransVO;
	}

	private MembershipTransactionVO getRenewTransaction(SelectedClient selectedClient, DateTime expiryDate) throws RemoteException, ValidationException {
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;
		MembershipTransactionVO memTransVO = null;
		try {

			oldMemVO = selectedClient.getMembership();

			newMemVO = oldMemVO.copy(true);

			newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
			newMemVO.setAllowToLapse(Boolean.valueOf(false)); // Should already
			// be false
			// according to
			// the business
			// rules but do it
			// any way in case
			// the rules have
			// been changed
			// elsewhere.
			newMemVO.setExpiryDate(expiryDate);

			LogUtil.debug(this.getClass(), "getRenewTransaction " + newMemVO.getMembershipID() + " " + expiryDate + " " + newMemVO.getExpiryDate());
			newMemVO.setNextProductCode(null);

			if (this.selectedProduct != null) {
				newMemVO.setProduct(this.selectedProduct); // If the product has
				// changed this will
				// also set the
				// product effective
				// date to today.
			}

			// Create a new transaction object for the membership transaction
			memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW, selectedClient.getMembershipGroupJoinDate());
			memTransVO.setTransactionDate(this.transactionDate);
			memTransVO.setUsername(this.user.getUserID());
			memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
			memTransVO.setMembership(oldMemVO);
			memTransVO.setNewMembership(newMemVO);
			memTransVO.setProductEffectiveDate();

			// MEM-14 - effective date should be the transaction date for on
			// hold
			if (MembershipVO.STATUS_ONHOLD.equals(oldMemVO.getStatus())) {
				// transaction date
				memTransVO.setEffectiveDate(getPreferredCommenceDate());
			} else {
				memTransVO.setEffectiveDate(oldMemVO.getExpiryDate());
			}

			memTransVO.setEndDate(newMemVO.getExpiryDate());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RemoteException("" + e);
		}
		return memTransVO;
	}

	private MembershipTransactionVO getChangeProductTransaction(SelectedClient selectedClient) throws RemoteException, ValidationException {
		MembershipVO oldMemVO = selectedClient.getMembership();
		// LogUtil.debug(this.getClass(),"old mem "+oldMemVO.toString());
		MembershipVO newMemVO = oldMemVO.copy(true);
		ProductVO oldProductVO = oldMemVO.getProduct();

		// These should already be set this way according to the business rules
		// but do it any way in case the rule implementation has been changed
		// elsewhere.
		newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
		newMemVO.setAllowToLapse(Boolean.valueOf(false));

		if (this.selectedProduct.compareTo(oldProductVO) > 0) {
			// The selected product is a lower risk product than the old
			// product on the membership. Flag the change of product
			// for the next renewal and don't change it.
			newMemVO.setNextProductCode(this.selectedProduct.getProductCode());
		} else {
			// The selected product is the same or a higher risk.
			// The membership gets extended by atleast one term from today.
			DateTime newExpiryDate = this.transactionDate.add(this.membershipTerm);

			// If the membership has been renewed multiple times then keep the
			// existing expiry date.
			// if (oldMemVO.getExpiryDate().afterDay(newExpiryDate))
			// newExpiryDate = oldMemVO.getExpiryDate();

			newMemVO.setExpiryDate(newExpiryDate);
			newMemVO.setNextProductCode(null);
			newMemVO.setProduct(this.selectedProduct); // Sets the product effective date to today if the product has been chaneged.
		}

		// Create a new transaction object for the membership transaction
		MembershipTransactionVO memTransVO = new MembershipTransactionVO(ValueObject.MODE_CREATE, this.transactionGroupTypeCode, MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT, selectedClient.getMembershipGroupJoinDate());
		memTransVO.setTransactionDate(this.transactionDate);
		memTransVO.setUsername(this.user.getUserID());
		memTransVO.setSalesBranchCode(this.user.getSalesBranchCode());
		memTransVO.setMembership(oldMemVO);
		memTransVO.setNewMembership(newMemVO);
		memTransVO.setProductEffectiveDate();

		memTransVO.setEffectiveDate(memTransVO.getProductEffectiveDate());
		memTransVO.setEndDate(newMemVO.getExpiryDate());

		return memTransVO;
	}

	/**
	 * Create the transaction fees for each membership transaction. Delete any existing transaction fees and Discounts that may be dependant on a previously selected product.
	 */
	public void createTransactionFees() throws RemoteException {
		this.logMembershipTransactions("createTransactionFees");
		LogUtil.debug(this.getClass(), "createTransactionFees() 0");
		if (this.membershipType == null) {
			throw new RemoteException("The membership type must be set before the transaction fees can be calculated.");
		}

		if (this.selectedProduct == null) {
			throw new RemoteException("The product must be set before the transaction fees can be calculated.");
		}

		if (this.transactionList == null) {
			throw new RemoteException("The membership transactions must be added before the transaction fees can be calculated.");
		}

		// MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();

		// The number of chargeable members is the number of members that will
		// be remaining in the membership group.
		int chargeableMemberCount = getActualGroupMemberCount();
		int memberCount = 0;

		// If a new membership is being joined to existing active memberships
		// to form a new membership group or is being joined to an existing
		// membership group then don't apply the join fee. Otherwise apply it.
		getJoinFeeCount(true);

		LogUtil.debug(this.getClass(), "the join fee count is " + getJoinFeeCount());

		// When applying fees to transactions apply the fees
		// in the order that they joined the membership group.
		MembershipTransactionComparator memTransComparator = new MembershipTransactionComparator(MembershipTransactionComparator.SORTBY_MEMBERSHIP_GROUP_JOIN_DATE, false);
		Collections.sort(this.transactionList, memTransComparator);

		// For each membership transaction clear the existing transaction fees.
		// Then set the new fee list for the new product on the membership
		// transaction.
		MembershipTransactionVO memTransVO = null;
		Iterator transListIT = this.transactionList.iterator();
		while (transListIT.hasNext()) {
			memTransVO = (MembershipTransactionVO) transListIT.next();
			memberCount++;

			// JH 1/12/2003 require fees for the group alteration transaction
			// types
			// if they have a direct debit debt.

			// LogUtil.debug(this.getClass(),"createTransactionFee() 1");
			if (memTransVO instanceof MemTransCreator) {
				// Creating of fees has been delegated to the MemTransHold class
				// LogUtil.debug(this.getClass(),"createTransactionFee() 2");
				LogUtil.debug(this.getClass(), "hold instance of MemTransCreator" + memberCount);
				MemTransCreator trans = (MemTransCreator) memTransVO;
				trans.createTransactionFees(chargeableMemberCount, memberCount);
			} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(memTransVO.getTransactionTypeCode())) {
				// uses this code when it is a change product option in the
				// context of a change group
				// uses subclass if the actual transaction is a change product
				// option.

				// if there is a direct debit debt and and the first payment has
				// been made and there is no
				// remaining debt then recalculate fees otherwise calculate the
				// product option only and pay
				// via receipting.
				if (this.hasPAPaidFirstPayment(memTransVO.getMembership())) {
					// Just create product option fees. These fees aren't
					// earned.
					createProductOptionFees(memTransVO, chargeableMemberCount);
				} else {
					// creates product option fees also
					createTransactionFees(chargeableMemberCount, chargeableMemberCount, memTransVO);
				}
			} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransVO.getTransactionTypeCode())) {
				// no individual fee for remove from group
				LogUtil.debug(this.getClass(), "remove from group");
			} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(memTransVO.getTransactionTypeCode())) {
				// no fees for change group pa
			} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(memTransVO.getTransactionTypeCode())) {
				LogUtil.debug(this.getClass(), "change group");
				// if they have existing direct debit debt then recalculate
				// fees.
				if (this.hasPADirectDebitDebt()) // regardless of immediate fees
				{
					LogUtil.debug(this.getClass(), "change group createTransactionFees");
					createTransactionFees(chargeableMemberCount, memberCount, memTransVO);
				}
			} else {
				LogUtil.debug(this.getClass(), "createTransactionFee() 5");
				// Clear the existing fee list in the membership transaction.
				createTransactionFees(chargeableMemberCount, memberCount, memTransVO);
			}
		}

		// this.logMembershipTransactions("b4 discounts");

		createDiscounts();

		// this.logMembershipTransactions("createAutomaticDiscounts");

	} // createTransactionFees

	protected void createDiscounts() throws RemoteException {
		// Now create some discounts
		this.logMembershipTransactions("createMemberRecurringDiscounts");
		createMemberRecurringDiscounts();
		this.logMembershipTransactions("createMemberProfileDiscounts");
		createMemberProfileDiscounts();
		this.logMembershipTransactions("createCreditDiscounts");
		// apply held over credit first
		createCreditDiscounts(); // added JH 8/10/2007
		this.logMembershipTransactions("createAutomaticDiscounts");
		// apply other automatic discounts
		createAutomaticDiscounts(); // was initially applied first but should
		// not be used if there are other
		// superseding discounts.
	}

	/**
	 * Utility method for grouping transaction fee creation and associated methods
	 * 
	 * @param chargeableMemberCount
	 * @param memberCount
	 * @param memTransVO
	 * @throws RemoteException
	 */
	protected void createTransactionFees(int chargeableMemberCount, int memberCount, MembershipTransactionVO memTransVO) throws RemoteException {
		memTransVO.clearTransactionFeeList();
		createFeesForMembershipTransaction(memTransVO, chargeableMemberCount, memberCount);
		createProductOptionFees(memTransVO, chargeableMemberCount);
		setMembershipsUnearnedCredit(memTransVO);

	} // create transaction fees

	/**
	 * Set the unearned credit on the new memberships in the transaction group.
	 * 
	 * @param memTransVO
	 * @throws RemoteException
	 */
	private void setMembershipsUnearnedCredit(MembershipTransactionVO memTransVO) throws RemoteException {
		MembershipVO oldMemVO = memTransVO.getMembership();
		UnearnedValue unearnedValue = new UnearnedValue();

		// LogUtil.debug(this.getClass(),
		// "setMembershipsUnearnedCredit transactionGroupTypeCode" +
		// transactionGroupTypeCode);
		logMembershipTransactions("setMembershipsUnearnedCredit");
		String memTransTypeCode = memTransVO.getTransactionTypeCode();
		// Unearned credit is not applicable to new memberships
		if (oldMemVO != null) {
			LogUtil.debug(this.getClass(), "memTransTypeCode" + memTransTypeCode);
			if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(memTransTypeCode)) {
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.transactionGroupTypeCode)) {
					// The membership is being renewed in the context of
					// creating a new
					// group. It must be an existing membership.
					unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(this.transactionGroupTypeCode)) {
					// The membership is being renewed in the context of
					// changing a
					// membership group. It must be an existing membership that
					// is being
					// added to the group. The expiry date of the group does not
					// change
					// when changing the group so existing group members wont
					// get a renew
					// transaction.
					unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(this.transactionGroupTypeCode) && !isMembershipInMembershipGroup(oldMemVO.getMembershipID())) {
					// The membership is being renewed in the context of
					// renewing a
					// membership group and the membership was not an original
					// group member.

					unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
				}

				// a normal renew will not generate any unearned value!!!
			} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(memTransTypeCode)) {
				if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.transactionGroupTypeCode)) {
					// The membership product is being changed in the context of
					// creating a new membership group. Must be an existing
					// membership.
					unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(this.transactionGroupTypeCode)) {
					// The membership product is being changed in the context of
					// changing a membership group. Both existing group
					// memberships
					// and non-group memberships get an unearned credit.
					unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(this.transactionGroupTypeCode) && !isMembershipInMembershipGroup(oldMemVO.getMembershipID())) {
					// The membership product is being changed in the context of
					// renewing a membership group and the membership is not an
					// existing
					// group member.
					unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
				} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.transactionGroupTypeCode) && memTransVO.isProductChanged()) {
					// The membership product is being changed on a single
					// membership and the product was changed.
					unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
				}
			} else {
				// Change group and Change group PA will now fall into this else
				unearnedValue = oldMemVO.getPaidRemainingMembershipValue();
			}
		}
		LogUtil.debug(this.getClass(), ">>>setting unearned credit");
		LogUtil.debug(this.getClass(), unearnedValue + " " + this.transactionGroupTypeCode + "   " + memTransTypeCode);

		// set transaction credit on the new membership
		memTransVO.getNewMembership().setTransactionCredit(TransactionCredit.CREDIT_UNEARNED, unearnedValue);
	}

	/**
	 * Return the count of memberships that will be remaining in the group at the end of the transactions. A membership will be remaining in the group if the transaction for the membership does not remove it from the group.
	 */
	public int getActualGroupMemberCount() {
		int actualGroupMemberCount = 0;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO;
			String transTypeCode;
			Iterator transListIterator = this.transactionList.iterator();
			while (transListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIterator.next();
				transTypeCode = memTransVO.getTransactionTypeCode();
				if (!MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(transTypeCode)
				// Not valid in the context of other transactions now
				// &&
				// !MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(transTypeCode)
				// &&
				// !MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(transTypeCode)
				) {
					actualGroupMemberCount++;
				}
			}
		}
		return actualGroupMemberCount;
	}

	/**
	 * Create the fees that apply to the membership transaction. Select the fees that apply to the transaction type being performed on the membership transaction. Only apply the join fee if the count of join fees that can be applied is greater than zero. Return the count of join fees that can still be applied.
	 */
	private void createFeesForMembershipTransaction(MembershipTransactionVO memTransVO, int numberOfMembers, int memberCount) throws RemoteException {

		int locJoinFeeCount = getJoinFeeCount();
		LogUtil.debug(this.getClass(), "----->locJoinFeeCount=" + locJoinFeeCount);
		// LogUtil.debug(this.getClass(),"createFeesForMembershipTransaction(<memTransVO>,numberOfMembers="+numberOfMembers+", memberCount="+memberCount+", joinFeeCount="+joinFeeCount+")");
		// LogUtil.debug(this.getClass(),"createFeesForMembershipTransaction(): getFeeEffectiveDate()="+getFeeEffectiveDate());

		boolean applyJoinFee = false;

		// if they are the PA
		if (memTransVO.isPrimeAddressee()) {
			applyJoinFee = true;
		}
		LogUtil.debug(this.getClass(), "applyJoinFee" + applyJoinFee);
		LogUtil.debug(this.getClass(), "productCode=" + this.selectedProduct.getProductCode() + ", memberTypeCode=" + this.membershipType.getMembershipTypeCode() + ", transTypeCode=" + memTransVO.getTransactionTypeCode() + ", numberOfMembers=" + numberOfMembers + ", feeEffectiveDate=" + getFeeEffectiveDate());

		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		Collection feeList = refMgr.findProductFee(this.selectedProduct.getProductCode(), this.membershipType.getMembershipTypeCode(), memTransVO.getTransactionTypeCode(), numberOfMembers, getFeeEffectiveDate());

		LogUtil.debug(this.getClass(), "locJoinFeeCount = " + locJoinFeeCount);

		LogUtil.debug(this.getClass(), "feeCount = " + (feeList == null ? "null fee list" : Integer.toString(feeList.size())));
		if (feeList != null) {
			DateTime transDate = getTransactionDate();
			DateTime fromDate;
			DateTime toDate;
			MembershipVO oldMemVO = memTransVO.getMembership();
			Integer membershipID = oldMemVO == null ? null : oldMemVO.getMembershipID();
			boolean existingGroupMember = isMembershipInMembershipGroup(membershipID);
			String memTransTypeCode = memTransVO.getTransactionTypeCode();
			Iterator feeListIterator = feeList.iterator();
			FeeSpecificationVO feeSpecVO = null;
			while (feeListIterator.hasNext()) {
				feeSpecVO = (FeeSpecificationVO) feeListIterator.next();
				LogUtil.debug(this.getClass(), "feeSpecID=" + feeSpecVO.feeSpecificationID + ", feeTypeCode=" + feeSpecVO.getFeeTypeCode() + ", feePermember=" + feeSpecVO.getFeePerMember());

				// Apply the number of join fees according to the join fee rules
				// This is realised by the join fee count. Add to the prime
				// addressee only.
				if (feeSpecVO.getFeeType().isJoinType()) {
					LogUtil.debug(this.getClass(), "match locJoinFeeCount=" + locJoinFeeCount + " " + applyJoinFee);
					// how do we know a transferred member is in the transaction
					// list?
					if (locJoinFeeCount > 0 && applyJoinFee) {
						LogUtil.debug(this.getClass(), "join fee added");
						createFee(memTransVO, feeSpecVO, numberOfMembers, memberCount);
						locJoinFeeCount--; // reduce to 0 typically
						setJoinFeeCount(locJoinFeeCount);
					}
				} else {
					LogUtil.debug(this.getClass(), "non join fee=" + locJoinFeeCount + " " + applyJoinFee);
					// create/rejoin transactions only
					// check join/rejoin type to apply on road fee
					if (feeSpecVO.getFeeType().isOnRoad()) {
						// check rejoin type code - may not exist
						String rejoinTypeCode = this.getTransactionReason() != null ? this.getTransactionReason().getReferenceDataPK().getReferenceCode() : "";
						LogUtil.debug(this.getClass(), "rejoinTypeCode=" + rejoinTypeCode);

						LogUtil.debug(this.getClass(), "is on road " + rejoinTypeCode + " " + transactionGroupTypeCode);
						// create, rejoin or change product and a join type of
						// on road
						if (((MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionGroupTypeCode))
						// conversion of rejoin to change product transaction
								|| MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.transactionGroupTypeCode))
								&& (ReferenceDataVO.REJOIN_REASON_ON_ROAD.equals(rejoinTypeCode) || ReferenceDataVO.JOIN_REASON_ON_ROAD.equals(rejoinTypeCode))) {
							LogUtil.debug(this.getClass(), "Apply 'on road' type fee");
						} else {
							LogUtil.debug(this.getClass(), "Fee type is 'on road' and join reason is not on road");
							continue;
						}
					}
					// LogUtil.debug(this.getClass(),"transactionGroupTypeCode="+this.transactionGroupTypeCode);

					if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(this.transactionGroupTypeCode)) {
						LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 1");
						createFee(memTransVO, feeSpecVO, numberOfMembers, memberCount);
					}
					// individual transaction types are create and rejoin and
					// should be prorated
					// to the expiry date of the affiliated club.
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(this.transactionGroupTypeCode)) {
						fromDate = this.transactionDate;
						toDate = memTransVO.getNewMembership().getExpiryDate();

						createPartTermFee(memTransVO, feeSpecVO, fromDate, toDate, memberCount);
					}
					// If the context transaction is a change of product then
					// only create change of product fees if the product has
					// been changed.
					// When changing to a lower risk product the changed is only
					// flagged for the next renewal.
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(this.transactionGroupTypeCode) && memTransVO.isProductChanged()) {
						// Charge a fee for the term starting now to when the
						// membership expires
						// If the membership has been renewed multiple times
						// then
						// it will expire more than one term in the future.
						LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 2");
						fromDate = this.transactionDate;
						toDate = memTransVO.getNewMembership().getExpiryDate();

						createPartTermFee(memTransVO, feeSpecVO, fromDate, toDate, memberCount);

					}
					// If the context transaction is a create group then all
					// membership
					// transaction types get a fee from the transaction date to
					// the
					// membership expiry date. The expiry date will be one term
					// after
					// the transaction date
					// The createCreditDiscounts() method will add credits for
					// un-used
					// portions of existing memberships.
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE_GROUP.equals(this.transactionGroupTypeCode)) {
						LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 3");
						fromDate = transDate;
						toDate = memTransVO.getNewMembership().getExpiryDate();

						createPartTermFee(memTransVO, feeSpecVO, fromDate, toDate, memberCount);
					}
					// If the context transaction is a change group then all
					// membership
					// transaction types get a fee from the transaction date to
					// the
					// membership expiry date. The expiry date will be the
					// existing groups
					// expiry date which must be after the transaction date as
					// you can't
					// change a group once it has expired.
					// The createCreditDiscounts() method will add credits for
					// un-used
					// portions of existing memberships.
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_GROUP_PA.equals(this.transactionGroupTypeCode)) {
						LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 4");

						fromDate = transDate;
						toDate = memTransVO.getNewMembership().getExpiryDate();

						// from date is where the group has paid up to
						createPartTermFee(memTransVO, feeSpecVO, fromDate, toDate, memberCount);
					}
					// If the context transaction is a renew group and the group
					// is in renewal
					// then the period that the fee is charged for depends on
					// the state
					// of the membership.
					// The expiry date will be the existing groups expiry date
					// plus one term
					// which must be after the transaction date as you can't
					// renew
					// a group once it has been expired for more than 11 months.
					// The createCreditDiscounts() method will add credits for
					// un-used
					// portions of existing memberships.
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(this.transactionGroupTypeCode))
					// &&
					// MembershipVO.STATUS_INRENEWAL.equals(this.selectedMembershipGroup.
					// getMembershipGroupStatus()))
					{
						LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 5");
						if (/*
							 * MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE .equals(memTransTypeCode) // Create on non group member || MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN .equals(memTransTypeCode) // Rejoin on non group member ||
							 */(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(memTransTypeCode) && existingGroupMember) // Renew
								// on
								// existing
								// group
								// member
								|| (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT.equals(memTransTypeCode) && existingGroupMember)) // Change
						// product
						// on
						// existing
						// group
						// member

						{
							// Charge a standard one term fee
							LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 5a");
							createFee(memTransVO, feeSpecVO, numberOfMembers, memberCount);
						} else {

							LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 5b");

							// Only charge a part of the fee for the period that
							// the membership has been extended.
							// Applies to existing memberships that were not
							// part of the group.
							fromDate = transDate;
							toDate = memTransVO.getNewMembership().getExpiryDate();
							createPartTermFee(memTransVO, feeSpecVO, fromDate, toDate, memberCount);
						}
					}
					/*
					 * // If the context transaction is a renew group and the group is NOT in renewal // then the period that the fee is charged for depends on the state // of the membership. // The expiry date will be the existing groups expiry date plus one term // which must be after the transaction date as you can't renew // a group once it has been expired for more than 11 months. // The createCreditDiscounts() method will add credits for un-used // portions of existing memberships. else if(MembershipTransactionTypeVO .TRANSACTION_TYPE_RENEW_GROUP.equals(this. transactionGroupTypeCode) && !MembershipVO.STATUS_INRENEWAL .equals(this.selectedMembershipGroup. getMembershipGroupStatus())) { LogUtil.debug(this.getClass (),"createFeesForMembershipTransaction(): 5c");
					 * 
					 * if((MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals (memTransTypeCode) && existingGroupMember) // Renew on existing group member || (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT .equals( memTransTypeCode) && existingGroupMember)) // Change product on existing group member { LogUtil.debug(this .getClass(),"createFeesForMembershipTransaction(): 5c1"); // Charge a standard one term fee createFee(memTransVO, feeSpecVO, numberOfMembers, memberCount);
					 * 
					 * } else { LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): 5c2"); // Fees get charged from fromDate = transDate; toDate = memTransVO.getNewMembership().getExpiryDate();
					 * 
					 * // Only charge a part of the fee for the period that the membership has been extended. createPartTermFee(memTransVO, feeSpecVO, fromDate, toDate, memberCount); } } }
					 */
					else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_CHANGE_PRODUCT_OPTION.equals(this.transactionGroupTypeCode)) {
						fromDate = transDate;
						toDate = memTransVO.getNewMembership().getExpiryDate();
						createPartTermFee(memTransVO, feeSpecVO, fromDate, toDate, memberCount);
					} else if (MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP.equals(memTransTypeCode)) {
						// no fee required but credits should be calculated
					} else {
						throw new SystemException("Transaction '" + this.transactionGroupTypeCode + "' did not meet any fee criteria!");
					}
				}
			}
		}

		LogUtil.debug(this.getClass(), "createFeesForMembershipTransaction(): end. locJoinFeeCount=" + locJoinFeeCount);

		// return joinFeeCount;
	}

	/**
	 * Create a membership transaction fee for the given membership transaction and fee specification. The full fee times the number of membership terms is charged. The number of memberships up to and including the number of memberships that the fee has been specified at are charged the fee per member. Additional memberships are charged the additional member fee if it has been specified otherwise they are charged the fee per member.
	 */
	private void createFee(MembershipTransactionVO memTransVO, FeeSpecificationVO feeSpecVO, int numberOfMembers, int memberCount) throws RemoteException {

		boolean multiplyTerms = false;
		if (feeSpecVO.getFeeType().isTermMultiplier()) {
			multiplyTerms = true;
		}

		double feeCharged = feeSpecVO.getFeePerMember(memberCount);
		LogUtil.debug(this.getClass(), "xxxxin " + feeSpecVO.toString());

		double feePerMember = multiplyTerms ? multiplyFeeByTerms(feeCharged) : feeCharged;
		double feePerVehicle = multiplyTerms ? multiplyFeeByTerms(feeSpecVO.getFeePerVehicle()) : feeSpecVO.getFeePerVehicle();

		// double feePerMember = feeCharged;
		// double feePerVehicle = feeSpecVO.getFeePerVehicle();

		int vehicleCount = memTransVO.getNewMembership() != null ? memTransVO.getNewMembership().getVehicleCount() : memTransVO.getMembership().getVehicleCount();
		LogUtil.debug(this.getClass(), "vehicleCount=" + vehicleCount);
		MembershipTransactionFee memTransFee = new MembershipTransactionFee(feeSpecVO, feePerMember, feePerVehicle, vehicleCount);

		StringBuffer feeDesc;
		feeDesc = new StringBuffer();
		feeDesc.append("Standard fee");
		if (feePerVehicle > 0.0 && vehicleCount > 0) {
			feeDesc.append(" ");
			feeDesc.append(CurrencyUtil.formatDollarValue(feeCharged));
			feeDesc.append(" per year and ");
			feeDesc.append(CurrencyUtil.formatDollarValue(feePerVehicle));
			feeDesc.append(" per vehicle (");
			feeDesc.append(vehicleCount);
			feeDesc.append(" vehicles) per year = ");
			feeDesc.append(CurrencyUtil.formatDollarValue(memTransFee.getGrossFee()));
		} else {
			feeDesc.append(" = ");
			feeDesc.append(CurrencyUtil.formatDollarValue(memTransFee.getGrossFee()));
		}
		memTransFee.setFeeDescription(feeDesc.toString());

		memTransVO.addTransactionFee(memTransFee);
	}

	/**
	 * Add a proportion of the fees for the difference in period between the from and to dates.
	 */
	private void createPartTermFee(MembershipTransactionVO memTransVO, FeeSpecificationVO feeSpecVO, DateTime fromDate, DateTime toDate, int memberCount) throws RemoteException {
		// Work out the portion of a full years membership fee that is to be
		// charged.
		Interval periodToCharge = null;
		try {
			periodToCharge = new Interval(fromDate, toDate);
		} catch (Exception e) {
			throw new SystemException(e);
		}
		int daysInYear = (int) periodToCharge.getDaysInYear();
		int daysToCharge = periodToCharge.getTotalDays();

		// Add a proportion of each fee to the transaction.
		double memberCharge = feeSpecVO.getFeePerMember(memberCount);
		LogUtil.debug(this.getClass(), "memberCharge = " + memberCharge);
		LogUtil.debug(this.getClass(), "daysInYear = " + daysInYear);
		LogUtil.debug(this.getClass(), "daysToCharge = " + daysToCharge);
		BigDecimal feePerMember = MembershipHelper.getUnearnedRatio(daysToCharge, daysInYear);

		LogUtil.debug(this.getClass(), "feePerMember = " + feePerMember);
		feePerMember = feePerMember.multiply(new BigDecimal(memberCharge));
		LogUtil.debug(this.getClass(), "feePerMember = " + feePerMember);
		feePerMember = feePerMember.setScale(2, BigDecimal.ROUND_HALF_UP);

		double vehicleCharge = feeSpecVO.getFeePerVehicle();
		BigDecimal feePerVehicle = MembershipHelper.getUnearnedRatio(daysToCharge, daysInYear);
		feePerVehicle = feePerVehicle.multiply(new BigDecimal(vehicleCharge));
		feePerVehicle = feePerVehicle.setScale(2, BigDecimal.ROUND_HALF_UP);

		if (feePerMember.compareTo(new BigDecimal(0.0)) != 0 || feePerVehicle.compareTo(new BigDecimal(0.0)) != 0) {
			int vehicleCount = memTransVO.getNewMembership().getVehicleCount();

			MembershipTransactionFee memTransFee = new MembershipTransactionFee(feeSpecVO, feePerMember.doubleValue(), feePerVehicle.doubleValue(), vehicleCount);

			StringBuffer feeDesc;
			feeDesc = new StringBuffer();
			feeDesc.append(daysToCharge);
			feeDesc.append((daysToCharge == 1 ? " day" : " days"));
			feeDesc.append(" of membership @ ");
			feeDesc.append(CurrencyUtil.formatDollarValue(memberCharge));
			if (feePerVehicle.compareTo(new BigDecimal(0.0)) != 0 && vehicleCount > 0) {
				feeDesc.append(" per year and ");
				feeDesc.append(CurrencyUtil.formatDollarValue(vehicleCharge));
				feeDesc.append(" per vehicle (");
				feeDesc.append(vehicleCount);
				feeDesc.append(" vehicles) per year = ");
			} else {
				feeDesc.append(" per year = ");
			}
			LogUtil.debug(this.getClass(), "memTransFee = " + memTransFee.toString());
			feeDesc.append(CurrencyUtil.formatDollarValue(memTransFee.getGrossFee()));
			memTransFee.setFeeDescription(feeDesc.toString());

			memTransVO.addTransactionFee(memTransFee);
		}
	}

	/**
	 * Get the payment method id for the payable item that is direct debit and has an amount outstanding. This is the one we are going to replace.
	 */
	public String getDirectDebitScheduleIdToReplace() throws RemoteException {

		// hold the id to return to the client
		String paymentMethodID = null;

		LogUtil.debug(this.getClass(), "getPreviousPaymentMethodID1");

		DirectDebitSchedule ddSchedule = null;

		LogUtil.debug(this.getClass(), "getPreviousPaymentMethodID2");
		MembershipTransactionVO primeMemTransVO = this.getMembershipTransactionForPA();
		MembershipVO primeMemVO = primeMemTransVO.getMembership(); // new mem

		if (primeMemVO == null) {
			// may not be an old membership
		} else {
			LogUtil.debug(this.getClass(), "getPreviousPaymentMethodID3");
			// get any payable schedules
			Collection ddSchedules = primeMemVO.getPayableSchedules();
			if (ddSchedules != null) {
				if (ddSchedules.size() > 1) {
					throw new RemoteException("There are multiple payable direct debit schedules - it" + " is not possible to find exactly one schedule to replace.");
				}

				Iterator ddScheduleIt = ddSchedules.iterator();
				while (ddScheduleIt.hasNext()) {

					ddSchedule = (DirectDebitSchedule) ddScheduleIt.next();

					LogUtil.debug(this.getClass(), "getPreviousPaymentMethodID4");
					if (ddSchedule != null) {
						// if not null get the payment method id
						paymentMethodID = ddSchedule.getPaymentMethodID();
						LogUtil.debug(this.getClass(), "paymentMethodID " + paymentMethodID);
						LogUtil.debug(this.getClass(), "getPreviousPaymentMethodID5");
						LogUtil.debug(this.getClass(), "primeMemTransVO.getNetTransactionFee()=" + primeMemTransVO.getNetTransactionFee());

						boolean amountOSAttrPA = false;
						try {
							BigDecimal amountOutstanding = primeMemTransVO.getAmountOutstanding().getAmountOutstanding();
							if (amountOutstanding.compareTo(new BigDecimal(0)) > 0) {
								amountOSAttrPA = true;
							}
						} catch (Exception e) {
							LogUtil.warn(this.getClass(), "Unable to get individual portion of amount outstanding. " + e.getMessage());
						}

						// ensure that the outstanding amount in direct debit
						// has been
						// used to reduce the unearned credit if the transaction
						// group replaces
						// a previous transaction and PA has a debt attributable
						// to them for a past transaction. If not then throw an
						// exception.
						if (!primeMemVO.hasOutstandingReducedCredit() && replacesTransaction() && amountOSAttrPA) {
							throw new ValidationException("The outstanding amount in the payment system has not been used to affect the amount payable.");
						}
					}
				}
			}
		}
		LogUtil.debug(this.getClass(), "getPreviousPaymentMethodID6");
		return paymentMethodID;
	}

	/**
	 * Create fees for the product options that have been attached to the memberships in this transaction. Don't charge product option fees if the membership already had the product option before this transaction. This means that memberships that had a product option and have had their expiry date extended as part of this transaction wont be charged any additional fees for the product option to cover the additional term of membership. If the membership will expire in more than six months from the transaction date then charge the full fee. If the membership will expire in six months or less then charge half the product option fee.
	 */
	protected void createProductOptionFees(MembershipTransactionVO memTransVO, int numberOfMembers) throws RemoteException { // Calculate
		// the
		// date
		// six
		// months
		// from
		// now.
		Interval sixMonths = null;
		try {
			sixMonths = new Interval("0:6");
		} catch (ParseException e) {
			throw new SystemException(e);
		}
		DateTime sixMonthsHence = getTransactionDate().add(sixMonths);

		MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
		MembershipVO oldMemVO = memTransVO.getMembership();
		MembershipVO newMemVO = memTransVO.getNewMembership();
		Collection productOptionList = newMemVO.getProductOptionList();
		if (productOptionList != null && !productOptionList.isEmpty()) {
			MembershipProductOption memProdOption = null;
			Collection feeList = null;
			FeeSpecificationVO feeSpecVO = null;
			Iterator productOptionIterator = productOptionList.iterator();
			while (productOptionIterator.hasNext()) {
				memProdOption = (MembershipProductOption) productOptionIterator.next();
				// For each product option see if it has been added in this
				// transaction
				if (oldMemVO == null
				// or has the product option, payable by direct debit, not been
				// paid for
						|| (!oldMemVO.hasProductOption(memProdOption.getProductBenefitTypeCode())) || (oldMemVO.hasProductOption(memProdOption.getProductBenefitTypeCode()) && !this.hasPAPaidFirstPayment(oldMemVO)) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW.equals(this.transactionGroupTypeCode) || MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP.equals(this.transactionGroupTypeCode)) {
					// Get the fees that apply to the product option.
					feeList = refMgr.findProductBenefitFee(memProdOption.getProductBenefitTypeCode(), this.membershipType.getMembershipTypeCode(), memTransVO.getTransactionTypeCode(), numberOfMembers, this.getFeeEffectiveDate());
					// Charge each of the fees that apply
					if (feeList != null) {
						Iterator feeListIT = feeList.iterator();
						while (feeListIT.hasNext()) {
							feeSpecVO = (FeeSpecificationVO) feeListIT.next();
							MembershipTransactionFee memTransFee = new MembershipTransactionFee(feeSpecVO, newMemVO.getVehicleCount());
							// The fee halves if < 6 months of membership
							// remaining
							if (newMemVO.getExpiryDate().beforeDay(sixMonthsHence)) {
								memTransFee.setFeePerMember(feeSpecVO.getFeePerMember() / 2);
								memTransFee.setFeePerVehicle(feeSpecVO.getFeePerVehicle() / 2);
							}
							memTransVO.addTransactionFee(memTransFee);
						} // while (feeListIT.hasNext())
					} // if (feeList != null)
				} // if
					// (!oldMemVO.hasProductOption(memProdOption.getProductBenefitTypeCode()))
			} // while (productOptionIterator.hasNext())
		}
	}

	/**
	 * Multiply the fee by the number of membership terms or part terms. The fee is specified as a yearly fee and a membership term is one year. Work out the number of terms or part terms and multiply the fee by this number. Throws RemoteException if the membership term has not been set on the transaction group.
	 */
	private double multiplyFeeByTerms(double fee) throws RemoteException {
		if (this.membershipTerm == null) {
			throw new RemoteException("Failed to add terms to fees. Membership term is null.");
		}

		int terms = this.membershipTerm.getTotalYears();
		Interval yearsInterval = null;
		try {
			yearsInterval = new Interval(terms, 0, 0, 0, 0, 0, 0);
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}

		// If there is part of a term left then add one to the terms.
		if (yearsInterval.getTotalDays() < this.membershipTerm.getTotalDays()) {
			terms++;

			// Make sure that atleast the full fee is charged by not allowing
			// the
			// number of terms to go below 1.
		}
		if (terms < 1) {
			terms = 1;
		}
		return fee * terms;
	}

	/**
	 * Attribute to hold whether or not an immediate amount has been paid.
	 */
	private Boolean immediateAmount;

	/**
	 * Has the prime addressee paid the immediate portion of the schedule.
	 * 
	 * If a schedule does not exist or there is not an immediate amount payable it will return true.
	 * 
	 * @throws RemoteException
	 */
	public boolean hasPAPaidFirstPayment(MembershipVO memVO) // join fee
			throws RemoteException {
		LogUtil.debug(this.getClass(), "hasPAPaidFirstPayment " + memVO);
		if (immediateAmount == null) {
			MembershipVO paMemVO = null;
			LogUtil.debug(this.getClass(), "hasPAPaidFirstPayment1");
			// identify the pa
			if (memVO.isGroupMember()) {
				// get from database as history will be null
				MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
				paMemVO = membershipMgr.getMembership(memVO.getMembershipGroup().getMembershipGroupDetailForPA().getMembershipGroupDetailPK().getMembershipID());
			} else {
				paMemVO = memVO;
			}
			LogUtil.debug(this.getClass(), "hasPAPaidFirstPayment2");
			if (paMemVO != null) {
				// if there is a debt in direct debit for the PA
				if (paMemVO.hasDirectDebitDebt()) {
					LogUtil.debug(this.getClass(), "dd debt");
					// get payable schedules
					Collection ddSchedules = paMemVO.getPayableSchedules();
					if (ddSchedules != null) {
						if (ddSchedules.size() > 1) {
							throw new RemoteException("Multiple schedules not supported when determining if PA has paid first payment.");
						}
						DirectDebitSchedule ddSchedule = null;
						Iterator ddScheduleIt = ddSchedules.iterator();
						while (ddScheduleIt.hasNext()) {
							ddSchedule = (DirectDebitSchedule) ddScheduleIt.next();

							LogUtil.debug(this.getClass(), "hasPAPaidFirstPayment3");
							// and a schedule payable
							if (ddSchedule != null) {
								LogUtil.debug(this.getClass(), "sched present" + ddSchedule.toString());

								// is there an immediate amount?
								if (ddSchedule.getImmediateAmount().doubleValue() > 0) {
									// if so has the immediate amount been paid?
									immediateAmount = new Boolean(ddSchedule.hasPaidFirstPayment());
								} else {
									immediateAmount = new Boolean(true);
								}
							}
						}
					}

				}
				// no dd debt
				else {
					LogUtil.debug(this.getClass(), "no dd debt");
					immediateAmount = new Boolean(true);
				}

			} else {
				throw new SystemException("No prime addressee has been defined.");
			}
		}
		LogUtil.debug(this.getClass(), "immediateAmount=" + immediateAmount);
		return immediateAmount.booleanValue();
	}

	/**
	 * Set the global join fee count.
	 * 
	 * @param newJoinFeeCount
	 *            int
	 */
	private void setJoinFeeCount(int newJoinFeeCount) {
		LogUtil.debug(this.getClass(), "setting joinFeeCount=" + newJoinFeeCount);
		this.joinFeeCount = newJoinFeeCount;
	}

	private boolean joinFeeCalculated = false;

	private int getJoinFeeCount() throws RemoteException {
		return getJoinFeeCount(false);
	}

	/**
	 * Return the number of join fees that apply to this transaction. If a new membership is being joined to existing active memberships to form a new membership group or is being joined to an existing membership group then don't apply the join fee. Otherwise apply it. If the membership has not been paid for then apply a join fee.
	 * 
	 * Calculated once.
	 */
	private int getJoinFeeCount(boolean forceRecalculation) throws RemoteException {
		LogUtil.debug(this.getClass(), "forceRecalculation" + forceRecalculation);
		if (!joinFeeCalculated || forceRecalculation) {
			joinFeeCount = 1;
			LogUtil.debug(this.getClass(), "calcing joinFeeCount=" + joinFeeCount);
			if (this.transactionList != null && this.transactionList.size() > 0) // WAS
			// 1!!!
			{
				// Look for an existing active membership. If found then the
				// join fee does not apply.
				// what if it was never paid?
				MembershipTransactionVO memTransVO;
				MembershipVO oldMemVO;
				Iterator transListIterator = this.transactionList.iterator();
				while (joinFeeCount > 0 && transListIterator.hasNext()) {
					memTransVO = (MembershipTransactionVO) transListIterator.next();
					oldMemVO = memTransVO.getMembership();
					boolean paidFirstPayment = true;
					if (oldMemVO != null) {
						paidFirstPayment = this.hasPAPaidFirstPayment(oldMemVO);
						LogUtil.debug(this.getClass(), "oldMem: status=" + oldMemVO.getStatus() + ", paidFirstPayment=" + paidFirstPayment + ", active=" + oldMemVO.isActive() + ", repairDD=" + this.getRepairDDPaymentMethodId());
					}
					// at least one valid member
					if (oldMemVO != null && (oldMemVO.isActive() || this.getRepairDDPaymentMethodId() != null) // ignore
							// active
							// test
							// if
							// repairing
							// DD
							&& (MembershipVO.STATUS_ACTIVE.equals(oldMemVO.getStatus()) || MembershipVO.STATUS_ONHOLD.equals(oldMemVO.getStatus()) || MembershipVO.STATUS_INRENEWAL.equals(oldMemVO.getStatus()) || MembershipVO.STATUS_EXPIRED.equals(oldMemVO.getStatus()) && !MembershipVO.STATUS_UNDONE.equals(oldMemVO.getStatus())) && paidFirstPayment) // if
					// immediate
					// amount
					// exists
					// and
					// is
					// paid
					// only
					{
						LogUtil.debug(this.getClass(), "one valid member joinFeeCount=" + joinFeeCount);
						joinFeeCount = 0;
					}
					LogUtil.debug(this.getClass(), "joinFeeCount=" + joinFeeCount);
				}
			}
			joinFeeCalculated = true;
		}
		LogUtil.debug(this.getClass(), "return joinFeeCount=" + joinFeeCount);
		return joinFeeCount;
	}

	/**
	 * Create the automatic discounts that apply to membership transactions. The list of transaction fees must be set first by calling the createTransactionFees() method.
	 */
	protected void createAutomaticDiscounts() throws RemoteException {
		if (this.transactionList == null) {
			throw new RemoteException("The membership transactions must be added before the automatic discounts can be created.");
		}

		if (this.hasAutomaticDiscountsApplied()) {
			LogUtil.warn(this.getClass(), "Automatic discounts may only be applied once during a transaction.");
			return;
		}
		MembershipTransactionVO memTransVO = null;
		MembershipTransactionFee memTransFee = null;
		Collection memTransFeeList = null;
		Iterator memTransFeeListIterator = null;
		MembershipVO oldMemVO = null;
		MembershipVO newMemVO = null;

		// Get the membership transaction fee list for each membership
		// transaction.
		Iterator transListIterator = this.transactionList.iterator();
		while (transListIterator.hasNext()) {
			memTransVO = (MembershipTransactionVO) transListIterator.next();
			memTransFeeList = memTransVO.getTransactionFeeList();
			oldMemVO = memTransVO.getMembership();
			newMemVO = memTransVO.getNewMembership();
			if (memTransFeeList != null) {
				// Loop through the membership transaction fees attached to the
				// membership transaction and add any automatic discounts that
				// might apply.
				memTransFeeListIterator = memTransFeeList.iterator();
				while (memTransFeeListIterator.hasNext()) {
					memTransFee = (MembershipTransactionFee) memTransFeeListIterator.next();
					if (memTransFee.getNetTransactionFee() > 0.0) {
						addAutomaticTransactionFeeDiscounts(memTransVO, memTransFee);
					}
				}
			}
			// note: will be reverted by createCreditDiscounts
			adjustCreditAmount(memTransVO);
		}
	} // createAutomaticDiscounts

	/**
	 * Only used in Cancel/Hold!!!
	 * 
	 * @param memTransVO
	 *            MembershipTransactionVO
	 * @throws ValidationException
	 * @throws RemoteException
	 */
	protected void adjustUnearnedCreditAmount(MembershipTransactionVO memTransVO) throws ValidationException, RemoteException {
		MembershipVO newMemVO = memTransVO.getNewMembership();
		MembershipVO oldMemVO = memTransVO.getMembership();

		/**
		 * @todo check impact on speed
		 */
		UnearnedValue unearnedValue = null;
		double creditAmount = 0;
		if (oldMemVO != null) {
			unearnedValue = oldMemVO.getPaidRemainingMembershipValue();

			if (unearnedValue != null && unearnedValue.getUnearnedAmount().doubleValue() > 0) {
				creditAmount = newMemVO.getCreditAmount();
			}
			newMemVO.setCreditAmount(creditAmount + unearnedValue.getUnearnedAmount().doubleValue());
		}

	}

	private void adjustCreditAmount(MembershipTransactionVO memTransVO) throws ValidationException, RemoteException {
		MembershipVO newMemVO = memTransVO.getNewMembership();
		LogUtil.debug(this.getClass(), "adjustCreditAmount=" + newMemVO);
		// Convert any left over unearned credits into credits to be used
		// on the next transaction.
		TransactionCredit unearnedCredit = newMemVO.getTransactionCredit(TransactionCredit.CREDIT_UNEARNED);

		double existingCreditAmount = 0;

		if (unearnedCredit != null) {
			existingCreditAmount = newMemVO.getCreditAmount();
			memTransVO.getNewMembership().setCreditAmount(existingCreditAmount + unearnedCredit.getTransactionCreditAmount().doubleValue());
		}

		// Convert any left over affiliated credits
		// TransactionCredit affiliatedCredit = newMemVO.getTransactionCredit(
		// TransactionCredit.CREDIT_AFFILIATED_CLUB);
		//
		// if(affiliatedCredit != null)
		// {
		// existingCreditAmount = newMemVO.getCreditAmount();
		// memTransVO.getNewMembership().setCreditAmount(existingCreditAmount
		// + affiliatedCredit.getTransactionCreditAmount().doubleValue());
		// }

	}

	/**
	 * Add any automatic discounts that might apply to the membership transaction fee.
	 * 
	 * @todo calculate rewards discount amounts.
	 */
	private void addAutomaticTransactionFeeDiscounts(MembershipTransactionVO memTransVO, MembershipTransactionFee memTransFee) throws RemoteException {
		LogUtil.debug(this.getClass(), "adding automatic fee discounts" + memTransFee.toString());

		MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();

		double discountableFee = 0;
		MembershipVO oldMemVO = memTransVO.getMembership();
		MembershipVO newMemVO = memTransVO.getNewMembership();
		DiscountTypeVO discTypeVO;
		String discTypeCode;
		Collection allowedDiscountList = memTransFee.getAllowableDiscountList();
		// Get the allowed list of discounts that can be applied to the
		// membership
		// transaction fee.
		Iterator allowedDiscountListIterator = allowedDiscountList.iterator();

		// Get the affiliated club credit
		// TransactionCredit affiliatedClubCredit =
		// newMemVO.getTransactionCredit(
		// TransactionCredit.CREDIT_AFFILIATED_CLUB);

		// Get the paid membership value
		TransactionCredit unearnedCredit = newMemVO.getTransactionCredit(TransactionCredit.CREDIT_UNEARNED);

		while (allowedDiscountListIterator.hasNext()) {
			discTypeVO = (DiscountTypeVO) allowedDiscountListIterator.next();
			// Only apply automatic discount types
			if (discTypeVO.isAutomaticDiscount() && DiscountTypeVO.STATUS_Active.equals(discTypeVO.getStatus())) {
				// Determine what type of automatic discount it is.
				discTypeCode = discTypeVO.getDiscountTypeCode();

				LogUtil.debug(this.getClass(), "discTypeCode = " + discTypeCode);

				if (DiscountTypeVO.TYPE_UNEARNED_CREDIT.equals(discTypeCode)) {
					if (unearnedCredit != null) {
						// We know that it is a type of UnearnedValue.
						UnearnedValue unearnedValue = (UnearnedValue) unearnedCredit;
						double unearnedAmount = unearnedValue.getUnearnedAmount().doubleValue();
						if (unearnedAmount >= 0 && oldMemVO != null) // WAS !=
						{
							discountableFee = memTransFee.getNetTransactionFee();
							// Apply the remaining unearned amount as a credit
							// when changing the product.
							String discountReason = "Unearned credit for " + unearnedValue.getDaysRemaining() + " days remaining on " + oldMemVO.getProductCode() + " membership.";
							memTransFee.removeDiscount(DiscountTypeVO.TYPE_UNEARNED_CREDIT);
							if (unearnedAmount < discountableFee) {
								// Use all of the unearned amount
								memTransFee.addDiscount(discTypeVO, unearnedAmount, discountReason, unearnedValue.getDescription());
								unearnedValue.setUnearnedAmount(new BigDecimal(0));
							} else {
								// Use some of the unearned amount and put the
								// rest back
								// for the next transaction fee
								memTransFee.addDiscount(discTypeVO, discountableFee, discountReason, unearnedValue.getDescription());
								unearnedValue.setUnearnedAmount(new BigDecimal(unearnedAmount - discountableFee));
							}
						}
					}
				} else if (discTypeCode != null && discTypeCode.startsWith(DiscountTypeVO.TYPE_LOYALTY)) // loyalty
				// discount
				// types
				{
					LogUtil.debug(this.getClass(), "loyalty 1");

					// determine if the membership tier has discounts available
					// to it.

					String tierCode = (oldMemVO == null || oldMemVO.getRenewalMembershipTier() == null) ? "" : oldMemVO.getRenewalMembershipTier().getTierCode(); // Bug
					// fix
					// JH
					// 28/08/2007
					LogUtil.debug(this.getClass(), "loyalty 2" + tierCode);
					Collection discountList = membershipMgr.getDiscountListByTier(tierCode); // should
					// only
					// be
					// one

					MembershipDiscount disc = null;
					DiscountTypeVO discType = null;

					discountableFee = memTransFee.getNetTransactionFee();
					LogUtil.debug(this.getClass(), "loyalty 3" + discountableFee);
					double discAmount = 0;
					double discPercentage = 0;
					Interval discPeriod = null;

					if (discountList != null && !discountList.isEmpty()) {
						Iterator discIt = discountList.iterator();
						int counter = 0;
						while (discIt.hasNext()) {
							counter++;
							disc = (MembershipDiscount) discIt.next();
							discType = disc.getDiscountType();

							discPercentage = discType.getDiscountPercentage();
							discAmount = discType.getDiscountAmount();
							discPeriod = discType.getDiscountPeriod();

							LogUtil.debug(this.getClass(), "loyalty 4 - " + counter + " " + discType.getDiscountTypeCode() + " " + discPercentage + " " + discAmount + " " + discPeriod);

							memTransFee.removeDiscount(discTypeVO.getDiscountTypeCode());
							memTransFee.addDiscount(discTypeVO);
						}
					}

				} else if (DiscountTypeVO.TYPE_REWARD.equals(discTypeCode)) {
					memTransFee.removeDiscount(DiscountTypeVO.TYPE_REWARD);
					memTransFee.addDiscount(discTypeVO);
				} else if (DiscountTypeVO.TYPE_TRANSFER_JOIN.equals(discTypeCode)) {
					// assumes that they will be the pa
					LogUtil.debug(this.getClass(), "memTransFee.isJoinFee()=" + memTransFee.isJoinFee());
					// waive join fee with a specific discount if there has been
					// as transfer transaction in the last three months.
					if (memTransFee.isJoinFee() && MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(memTransVO.getTransactionGroupTypeCode())) {
						String description = description = newMemVO.getAffiliatedClubId() + "@" + newMemVO.getAffiliatedClubCode();
						discountableFee = memTransFee.getNetTransactionFee();
						// add discount
						memTransFee.addDiscount(discTypeVO, discountableFee, "", "Waive join fee for transfer: " + description + ".");
						discountableFee = 0;
					}
				} else if (DiscountTypeVO.TYPE_TRANSFER_CREDIT.equals(discTypeCode)) {
					LogUtil.debug(this.getClass(), "considering transfer...");
					LogUtil.debug(this.getClass(), "memTransFee = " + memTransFee.toString());

					discountableFee = memTransFee.getNetTransactionFee();

					if (!memTransFee.isJoinFee() && // not applicable to join
							// fee
							MembershipTransactionTypeVO.TRANSACTION_TYPE_TRANSFER.equals(memTransVO.getTransactionGroupTypeCode()) && discountableFee > 0) {
						String description = description = newMemVO.getAffiliatedClubId() + "@" + newMemVO.getAffiliatedClubCode();
						memTransFee.addDiscount(discTypeVO, discountableFee, memTransVO.getTransactionGroupTypeCode(), description);
					}
				}// end of transfer credit
				else if (discTypeCode.equals(DiscountTypeVO.TYPE_DRIVER_TRAINING_YEAR_2)) {
					// look for the existence of a driver training discount on
					// the previous
					// membership period

					boolean found = false;
					boolean foundY2 = false;
					found = membershipMgr.hasDiscount(newMemVO.getMembershipID(), DiscountTypeVO.TYPE_DRIVER_TRAINING, null);
					foundY2 = membershipMgr.hasDiscount(newMemVO.getMembershipID(), DiscountTypeVO.TYPE_DRIVER_TRAINING_YEAR_2, null);
					if (found && !foundY2) /*
											 * there was a driver training package discount on the old membership
											 */
					{
						String description = "Driver Training package second year discount";
						memTransFee.addDiscount(discTypeVO);
						// discountableFee,
						// memTransVO.getTransactionGroupTypeCode(),
						// description);
					}
				}
				else if (discTypeCode.equals("Half Price")) {
					// The adhoc half price discount - for Lifestyle
					memTransFee.removeDiscount(discTypeVO.getDiscountTypeCode());
					memTransFee.addDiscount(discTypeVO);
				}
				// else
				// {
				// //default action
				// memTransFee.removeDiscount(discTypeVO.getDiscountTypeCode());
				// memTransFee.addDiscount(discTypeVO);
				// }
			}
		}
	}

	/**
	 * Remove and reapply any credit discounts. Should only be called once in a transaction. Separate to automatic discounts as it should be applied first.
	 * 
	 * @throws RemoteException
	 */
	protected void createCreditDiscounts() throws RemoteException {
		if (this.transactionList == null) {
			throw new RemoteException("The membership transactions must be added before the credit discounts can be created.");
		}

		if (this.hasCreditDiscountsApplied()) {
			LogUtil.warn(this.getClass(), "Credit discounts may only be applied once during a transaction.");
			return;
		}

		LogUtil.debug(this.getClass(), "removed credit discounts");
		MembershipTransactionVO memTransVO = null;
		MembershipTransactionFee memTransFee = null;
		Collection memTransFeeList = null;
		Iterator memTransFeeListIterator = null;

		// Get the membership transaction fee list for each membership
		// transaction.
		Iterator transListIterator = this.transactionList.iterator();
		while (transListIterator.hasNext()) {
			memTransVO = (MembershipTransactionVO) transListIterator.next();
			memTransFeeList = memTransVO.getTransactionFeeList();
			if (memTransFeeList != null) {
				// Loop through the membership transaction fees attached to the
				// membership transaction and add the credit discount
				memTransFeeListIterator = memTransFeeList.iterator();
				while (memTransFeeListIterator.hasNext()) {
					memTransFee = (MembershipTransactionFee) memTransFeeListIterator.next();
					if (memTransFee.getNetTransactionFee() > 0.0) {
						addCreditDiscount(memTransVO, memTransFee);
					}
				}
			}
		}
	}

	/**
	 * Add any automatic discounts that might apply to the membership transaction fee.
	 */
	private void addCreditDiscount(MembershipTransactionVO memTransVO, MembershipTransactionFee memTransFee) throws RemoteException {
		// See if the credit discount is an allowable discount for the fee type
		DiscountTypeVO creditDiscountTypeVO = memTransFee.getAllowableDiscountType(DiscountTypeVO.TYPE_CREDIT);
		LogUtil.debug(this.getClass(), "addCreditDiscount creditDiscountTypeVO=" + creditDiscountTypeVO);
		if (creditDiscountTypeVO != null && DiscountTypeVO.STATUS_Active.equals(creditDiscountTypeVO.getStatus())) {
			double feeAmount = memTransFee.getNetTransactionFee();
			MembershipVO oldMemVO = memTransVO.getMembership();
			MembershipVO newMemVO = memTransVO.getNewMembership();
			if (oldMemVO != null) // ?old
			{
				double availableCredit = newMemVO.getCreditAmount();
				LogUtil.debug(this.getClass(), newMemVO.getClientNumber() + ": availableCredit=" + availableCredit);
				if (availableCredit > 0) {
					// A credit can be applied to the fee.
					if (feeAmount >= availableCredit) {
						// The available credit is less than the fee so use it
						// all
						memTransFee.addDiscount(creditDiscountTypeVO, availableCredit, null);
						newMemVO.setCreditAmount(0);
					} else {
						// Credit the fee amount and put the remainder on the
						// the new membership.
						memTransFee.addDiscount(creditDiscountTypeVO, feeAmount, null);
						newMemVO.setCreditAmount(availableCredit - feeAmount);
					}
				}
			}
		}
	}

	/**
	 * Add a membership transaction object to the transaction list. Assumes that the transaction list has already been initailised. This is called by subclasses of the TransactionGroup.
	 */
	protected void addMembershipTransaction(MembershipTransactionVO memTransVO) {
		this.transactionList.add(memTransVO);
	}

	/**
	 * Create the discounts that the member is entitled to based on their profile.
	 */
	protected void createMemberProfileDiscounts() throws RemoteException {
		LogUtil.debug(this.getClass(), "createMemberProfileDiscounts 1");
		if (this.transactionList != null) {
			LogUtil.debug(this.getClass(), "createMemberProfileDiscounts 2");
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				LogUtil.debug(this.getClass(), "createMemberProfileDiscounts 3" + memTransVO);
				memTransVO.createMemberProfileDiscounts();
			}
		}
	}

	/**
	 * Create transaction fee discounts for each membership transaction where the membership has recurring discounts and those discounts apply to the transaction fee. Does not remove any existing discounts first.
	 */
	protected void createMemberRecurringDiscounts() throws RemoteException {
		LogUtil.debug(this.getClass(), "createMemberRecurringDiscounts 0");
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				LogUtil.debug(this.getClass(), "createMemberRecurringDiscounts 1" + memTransVO);
				memTransVO.createMemberRecurringDiscounts();
			}
		}
		LogUtil.debug(this.getClass(), "createMemberRecurringDiscounts 2");
	}

	/**
	 * Set the membership transaction IDs on all of the membership transactions. This should be done while processing the transactions in the MembershipTransactionMgr so that the IDs are not fetched too early but before the fees are sent to the payment system which needs the IDs as a back reference.
	 */
	public void setMembershipTransactionIDs() throws RemoteException {
		MembershipIDMgr memIDMgr = MembershipEJBHelper.getMembershipIDMgr();
		int transID = 0;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				transID = memIDMgr.getNextTransactionID();
				memTransVO.setTransactionID(new Integer(transID));
			}
		}
	}

	/**
	 * Clear the membership transaction list.
	 */
	public void clearTransactionList() {
		this.transactionList.clear();
		this.amountPayable = 0.0;
		// this.discretionaryDiscountAmount = null;
		this.earnedAmount = 0.0;
		this.grossTransactionFee = 0.0;
		this.gstAmount = 0.0;
		this.totalDiscountAmount = 0.0;
		this.unearnedAmount = 0.0;
	}

	/**
	 * Calculate the gst fee and amount payable. The GST is calculated by summing the fees that are subject to GST, subtracting the total amount of discount given and then taking a percentage of the result. The amount payable is calculated as the the sum of all of the fees less the total amount of discount given plus the amount of GST that is payable.
	 * 
	 * @todo GW review GST calculations.
	 */
	public void calculateTransactionFee() throws RemoteException {

		// this.logMembershipTransactions("calculateTransactionFee");

		// Calculate the total amount of earned income, unearned income and
		// discretionary discount. Add up the total discount period.
		this.earnedAmount = 0.0;
		this.unearnedAmount = 0.0;
		this.grossTransactionFee = 0.0;
		this.totalDiscountAmount = 0.0;
		// BigDecimal advertisedFeeAdjustmentAmount = null;
		MembershipTransactionVO paMemTransVO = null;
		MembershipTransactionAdjustment advertisedFeeAdjustment = null;
		if (this.transactionList != null && !this.transactionList.isEmpty()) {
			LogUtil.debug(this.getClass(), "calculateTransactionFee transactionList=" + transactionList.toString());
			// Loop through the membership transactions and sum up the earned
			// amounts.
			// Also find the maximum period of discount given.
			Interval maxDiscountPeriod = null;
			Interval tmpPeriod = null;
			MembershipTransactionVO memTransVO = null;

			int advFeeCounter = 0;
			int ultFeeCounter = 0;
			MembershipVO oldMemVO = null;
			Iterator transListIT = this.transactionList.iterator();
			while (transListIT.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIT.next();
				this.earnedAmount += memTransVO.getEarnedIncomeAmount();
				this.unearnedAmount += memTransVO.getUnearnedIncomeAmount();
				this.grossTransactionFee += memTransVO.getGrossTransactionFee();
				this.totalDiscountAmount += memTransVO.getTotalDiscount();
				// Tag the pa's transaction and remove any quotes or adjustments
				// from the other transactions
				memTransVO.setAttachQuote(null);
				memTransVO.setAdjustmentList(null);
				LogUtil.debug(this.getClass(), "memTransVO=" + memTransVO.toString());
				if (memTransVO.isPrimeAddressee()) {
					paMemTransVO = memTransVO;
				}
				// Get the maximum discount period.
				tmpPeriod = memTransVO.getMaxDiscountPeriod();
				if (tmpPeriod != null) {
					if (maxDiscountPeriod == null || tmpPeriod.compareTo(maxDiscountPeriod) > 0) {
						maxDiscountPeriod = tmpPeriod;
					}
				}
				// Clear any discount period currently set.
				memTransVO.getNewMembership().setDiscountPeriod(null);

				// This is a nasty piece of work and will cease to do anything
				// when the
				// fees change. It's a fix for making the gross fee equal to the
				// advertised fee
				// where three people in a group are involved.
				if (memTransVO.hasFeeWithAmount(86.33)) {
					ultFeeCounter++;
				} else if (memTransVO.hasFeeWithAmount(59.67)) {
					advFeeCounter++;
				}
			}

			// Calculate the advertised price fee adjustment amount
			// If there are exactly three advantage fees for three members
			// or exactly three ultimate fees for three members then adjust
			// the gross fee so that it reflects the advertised amount.
			if (advFeeCounter == 3) { // Three times 59.67 = 179.01 whereas
				// advertised fee is 179.00
				// advertisedFeeAdjustmentAmount = new
				// BigDecimal(-0.01);
				advertisedFeeAdjustment = new MembershipTransactionAdjustment(AdjustmentTypeVO.ADVERTISED_FEE_DISCOUNT, new BigDecimal(0.01), "Advertised fee adjustment for 3 '" + this.selectedProduct.getProductCode() + "' memberships.");
			} else if (ultFeeCounter == 3) { // Three times 86.33 = 258.99
				// whereas the advertised fee is
				// 259.00
				// advertisedFeeAdjustmentAmount =
				// new BigDecimal(0.01);
				advertisedFeeAdjustment = new MembershipTransactionAdjustment(AdjustmentTypeVO.ADVERTISED_FEE_ADJUSTMENT, new BigDecimal(0.01), "Advertised fee discount for 3 '" + this.selectedProduct.getProductCode() + "' memberships.");
			}

			setDiscountPeriod(maxDiscountPeriod);
		}
		LogUtil.debug(this.getClass(), "grossTransactionFee=" + grossTransactionFee);
		LogUtil.debug(this.getClass(), "totalDiscountAmount=" + totalDiscountAmount);
		// Calculate the total amount payable.
		this.amountPayable = this.grossTransactionFee - this.totalDiscountAmount;
		LogUtil.debug(this.getClass(), "amountPayable=" + amountPayable);
		// Remove any previously added advertised fee adjustements first.
		removeAdjustment(AdjustmentTypeVO.ADVERTISED_FEE_ADJUSTMENT);
		removeAdjustment(AdjustmentTypeVO.ADVERTISED_FEE_DISCOUNT);

		// Apply the advertised fee adjustment as an adjustment to the
		// transaction total
		// if the transaction total has not been fully discounted.
		if (this.amountPayable > 0.0 && advertisedFeeAdjustment != null) {
			addAdjustment(advertisedFeeAdjustment);
		}

		// Add or subtract the adjustments as required
		if (this.adjustmentList != null && !this.adjustmentList.isEmpty()) {

			// Remove any quote adjustments first. These will get re-applied
			// further down.
			this.adjustmentList.remove(AdjustmentTypeVO.QUOTE_ADJUSTMENT);
			this.adjustmentList.remove(AdjustmentTypeVO.QUOTE_DISCOUNT);

			MembershipTransactionAdjustment memTransAdjustment = null;
			Collection values = this.adjustmentList.values();
			Iterator valuesIterator = values.iterator();
			while (valuesIterator.hasNext()) {
				memTransAdjustment = (MembershipTransactionAdjustment) valuesIterator.next();
				LogUtil.debug(this.getClass(), memTransAdjustment == null ? "null" : "adj");
				if (memTransAdjustment.isDebitAdjustment()) {
					this.amountPayable -= memTransAdjustment.getAdjustmentAmount().doubleValue();
				} else {
					this.amountPayable += memTransAdjustment.getAdjustmentAmount().doubleValue();
				}
			}
		}
		LogUtil.debug(this.getClass(), "after adjustments amountPayable=" + amountPayable);
		// If a quote is attached then add a final adjustment to align the
		// amount payable
		// with the quoted amount.
		if (this.attachedQuote != null) {
			// The amount to adjustment the fee by is the difference between the
			// quotes amount payable and the transactions amount payable.
			double quoteAdjustmentAmount = this.attachedQuote.getAmountPayable() - this.amountPayable;

			MembershipTransactionAdjustment quoteAdjustment = null;
			if (quoteAdjustmentAmount > 0.0) {
				quoteAdjustment = new MembershipTransactionAdjustment(AdjustmentTypeVO.QUOTE_ADJUSTMENT, new BigDecimal(quoteAdjustmentAmount), "Alignment to quotes transaction fee.");
				this.amountPayable += quoteAdjustmentAmount;
			} else if (quoteAdjustmentAmount < 0.0) {
				quoteAdjustment = new MembershipTransactionAdjustment(AdjustmentTypeVO.QUOTE_DISCOUNT, new BigDecimal(Math.abs(quoteAdjustmentAmount)), "Alignment to quotes transaction fee.");
				this.amountPayable += quoteAdjustmentAmount;
			}

			if (quoteAdjustment != null) {
				addAdjustment(quoteAdjustment);
			}
		}
		LogUtil.debug(this.getClass(), "after quotes amountPayable=" + amountPayable);
		// Attach the quote and adjustment list to the pa's transaction.
		if (paMemTransVO == null) {
			throw new SystemException("Failed to find prime addressees transaction.  Unable to set quote or adjustments.");
		}
		paMemTransVO.setAttachQuote(this.attachedQuote);
		paMemTransVO.setAdjustmentList(this.adjustmentList);

		// If the adjustment list includes a quote discount then this means that
		// that customer is paying the quoted fee but due to time progressing
		// the transaction fee calculated now is less than that quoted. Hold
		// the quote discount as a credit on the membership.
		// If the adjustment list does not have the quote discount then this
		// will
		// remove it from the credit amount list on the pa's transaction.
		MembershipTransactionAdjustment quoteAdjustment = null;
		if (this.adjustmentList != null) {
			quoteAdjustment = (MembershipTransactionAdjustment) this.adjustmentList.get(AdjustmentTypeVO.QUOTE_ADJUSTMENT);
		}
		paMemTransVO.getNewMembership().setTransactionCredit(TransactionCredit.CREDIT_QUOTE, quoteAdjustment);

		// Calculate the amount of GST to pay.
		// GST is included in the amount payable
		double gstRate = getGstRate();
		this.gstAmount = CurrencyUtil.calculateGSTincluded(this.amountPayable, gstRate);

		// Now loop through all of the membership transactions and set the
		// amount payable for the prime addressee of the membership group.
		LogUtil.debug(this.getClass(), "b4 set amountPayable=" + amountPayable);
		setAmountPayable(this.amountPayable, this.gstAmount);
		LogUtil.debug(this.getClass(), "after set amountPayable=" + amountPayable);

		// Set the value of the memberships
		setMembershipValues();

	} // calculateTransactionFee

	public double getGstRate() throws RemoteException {
		if (this.gstRate == 0.0) {
			CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
			TaxRateVO taxRateVO = commonMgr.getCurrentTaxRate();
			this.gstRate = taxRateVO.getGstRate();
		}
		return this.gstRate;
	}

	/**
	 * Returns the details of the direct debit authority to be used whan paying for the transaction. If NULL then the transaction will be paid by receipting.
	 */
	public DirectDebitAuthority getDirectDebitAuthority() {
		LogUtil.debug(this.getClass(), "getDirectDebitAuthority=" + directDebitAuthority);
		return this.directDebitAuthority;
	}

	public void setDirectDebitAuthority(DirectDebitAuthority authority, boolean autoRenew) throws RemoteException {
		LogUtil.debug(this.getClass(), "setDirectDebitAuthority=" + authority);

		// usually set with no membership id
		this.directDebitAuthority = authority;
		this.automaticallyRenewable = autoRenew;
	}

	public DirectDebitSchedule getDirectDebitSchedule() throws RemoteException {
		DirectDebitSchedule ddSchedule = null;
		LogUtil.debug(this.getClass(), "getDirectDebitSchedule directDebitScheduleId=" + this.directDebitScheduleId);
		if (this.directDebitScheduleId != null) {
			LogUtil.debug(this.getClass(), "directDebitScheduleId 1");
			PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
			try {
				ddSchedule = paymentMgr.getDirectDebitSchedule(this.directDebitScheduleId);
			} catch (PaymentException ex) {
				throw new RemoteException("Unable to get direct debit schedule.", ex);
			}
		}
		LogUtil.debug(this.getClass(), "directDebitScheduleId 2");
		return ddSchedule;
	}

	/**
	 * Return a new direct debit schedule specification based on the direct debit authority set.
	 * 
	 * The calculateTransactionFee() method must be called first.
	 * 
	 * An exception is thrown if a direct debit authority has not been set.
	 */
	public DirectDebitSchedule getDirectDebitScheduleSpecification() throws RemoteException {
		if (this.directDebitAuthority == null) {
			throw new SystemException("A direct debit authority has not been set for the transaction.  A direct debit schedule specification cannot be created.");
		}

		return getDirectDebitScheduleSpecification(this.directDebitAuthority);
	}

	/**
	 * Return a new direct debit schedule specification based on the passed direct debit authority. The amount to deduct immediately is the earned amount for the transaction less the discretionary discount. The amount to amortise over the period of the membership is the unearned amount for the transaction less any remaining discretionary discount.
	 * 
	 * The calculateTransactionFee() method must be called first.
	 * 
	 */
	public DirectDebitSchedule getDirectDebitScheduleSpecification(DirectDebitAuthority ddAuthority) throws RemoteException {
		if (ddAuthority == null) {
			throw new SystemException("A direct debit authority has not bee specified.  A direct debit schedule specification cannot be created.");
		}

		//
		// The amount to amortise over the period of the membership is the
		// unearned amount for the transaction less any remaining discretionary
		// discount.
		BigDecimal amortisedAmount = new BigDecimal(getEarnedAmount());
		BigDecimal immediateAmount = new BigDecimal(getUnearnedAmount());
		BigDecimal discretionaryAmount = null;
		MembershipTransactionAdjustment memTransAdjustment = getAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT);
		if (memTransAdjustment != null) {
			discretionaryAmount = memTransAdjustment.getAdjustmentAmount();
		} else {
			discretionaryAmount = new BigDecimal(0);
		}

		LogUtil.debug(this.getClass(), "amortisedAmount = " + amortisedAmount);
		LogUtil.debug(this.getClass(), "immediateAmount = " + immediateAmount);
		LogUtil.debug(this.getClass(), "discretionaryAmount = " + discretionaryAmount);

		if (discretionaryAmount.doubleValue() > 0.0) {
			// The amount to use to reduce the immediate amount
			BigDecimal amount = discretionaryAmount;
			immediateAmount = immediateAmount.subtract(amount);

			// if we have discounted the immediate amount too much
			if (immediateAmount.doubleValue() < 0) {
				// amount left to discount
				// diff between amount and immediate amount
				amount = amount.subtract(immediateAmount.abs());

				// reset to 0 as the remainder will be taken from
				// the amortised
				immediateAmount = new BigDecimal(0);
			}

			// Reduce the discretionary discount amount by the amount used
			// against the immediate amount
			discretionaryAmount = discretionaryAmount.subtract(amount);

			// If there is anything left then take it off the amortised amount.
			if (discretionaryAmount.doubleValue() > 0.0) {
				amortisedAmount = amortisedAmount.subtract(discretionaryAmount);
			}
		}
		amortisedAmount = new BigDecimal(NumberUtil.roundDouble(amortisedAmount.doubleValue(), 2));
		immediateAmount = new BigDecimal(NumberUtil.roundDouble(immediateAmount.doubleValue(), 2));

		/** @todo This is not an elegant fix to the fee rounding problem */
		double total = immediateAmount.doubleValue() + amortisedAmount.doubleValue();
		if (total != this.amountPayable) {
			amortisedAmount = new BigDecimal(amortisedAmount.doubleValue() + amountPayable - total);
		}

		// Create a direct debit schedule specification based on the
		// direct debit authority and membership details.
		DirectDebitSchedule ddSchedule = new DirectDebitSchedule(ddAuthority, getPeriodStartDate(), getPeriodEndDate(), amortisedAmount, immediateAmount, null, getMembershipTransactionForPA().getNewMembership().getClientNumber(), this.user.getUserID(), // payable
				// item
				// ID
				this.user.getSalesBranchCode(), SourceSystem.MEMBERSHIP, (this.selectedProduct == null ? null : this.selectedProduct.getProductCode()), MembershipUIHelper.getTransactionHeadingText(this.getTransactionGroupTypeCode()));

		return ddSchedule;
	}

	/**
	 * Loop through the membership transactions and see if one of them has offset discounts applied.
	 */
	public boolean hasOffsetDiscountsApplied() throws RemoteException {
		boolean found = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTxVO;
			Iterator txListIterator = this.transactionList.iterator();
			while (!found && txListIterator.hasNext()) {
				memTxVO = (MembershipTransactionVO) txListIterator.next();
				found = memTxVO.hasOffsetDiscountsApplied();
			}
		}
		return found;
	}

	/**
	 * Loop through the membership transactions and see if one of them has credit discounts applied.
	 */
	public boolean hasCreditDiscountsApplied() throws RemoteException {
		boolean found = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTxVO;
			Iterator txListIterator = this.transactionList.iterator();
			while (!found && txListIterator.hasNext()) {
				memTxVO = (MembershipTransactionVO) txListIterator.next();
				found = memTxVO.hasCreditDiscountsApplied();
			}
		}
		return found;
	}

	public boolean hasAutomaticDiscountsApplied() throws RemoteException {
		boolean found = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTxVO;
			Iterator txListIterator = this.transactionList.iterator();
			while (!found && txListIterator.hasNext()) {
				memTxVO = (MembershipTransactionVO) txListIterator.next();
				found = memTxVO.hasAutomaticDiscountApplied();
			}
		}
		return found;
	}

	/**
	 * Does the transaction contain a membership that has a complimentary credit attached.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public boolean hasComplimentaryCredit() throws RemoteException {
		boolean found = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTxVO;
			Iterator txListIterator = this.transactionList.iterator();
			while (!found && txListIterator.hasNext()) {
				memTxVO = (MembershipTransactionVO) txListIterator.next();
				found = memTxVO.hasComplimentaryCredit();
			}
		}
		return found;
	}

	/**
	 * Loop through the membership transactions and see if one of them has an unearned credit discount applied.
	 */
	public boolean hasUnearnedDiscount() throws RemoteException {
		boolean found = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTxVO;
			Iterator txListIterator = this.transactionList.iterator();
			while (!found && txListIterator.hasNext()) {
				memTxVO = (MembershipTransactionVO) txListIterator.next();
				found = memTxVO.hasUnearnedDiscount();
			}
		}
		return found;
	}

	public boolean requiresPostings() throws RemoteException {
		if (this.getAmountPayable() > 0 || MembershipTransactionTypeVO.TRANSACTION_TYPE_CANCEL.equals(this.getTransactionGroupTypeCode()) || MembershipTransactionTypeVO.TRANSACTION_TYPE_HOLD.equals(this.getTransactionGroupTypeCode()) || this.hasOffsetDiscountsApplied() || this.hasCreditDiscountsApplied() || this.hasUnearnedDiscount() || this.hasComplimentaryCredit()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Loop through the membership transactions and see if one of them has a fee attached to it that has non-discretionary discounts allowed.
	 */
	public boolean canHaveDiscounts(boolean includeAutomaticDiscounts) throws RemoteException {
		boolean found = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO;
			Iterator transListIterator = this.transactionList.iterator();
			while (!found && transListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIterator.next();
				found = memTransVO.canHaveDiscounts(includeAutomaticDiscounts);
			}
		}
		return found;
	}

	/**
	 * Return true if there is a fee attached to any membership transaction that can be non discretionarily discounted.
	 */
	public boolean canBeDiscounted() throws RemoteException {
		boolean discountableFeeFound = false;
		if (this.transactionList != null) {
			MembershipTransactionVO memTransVO;
			Iterator transListIterator = this.transactionList.iterator();
			while (!discountableFeeFound && transListIterator.hasNext()) {
				memTransVO = (MembershipTransactionVO) transListIterator.next();
				discountableFeeFound = memTransVO.canBeDiscounted();
			}
		}
		return discountableFeeFound;
	}

	public MembershipQuote getAttachedQuote() {
		return this.attachedQuote;
	}

	public void setAttachedQuote(MembershipQuote q) throws RemoteException {
		this.attachedQuote = q;

		// If the user has not entered a discretionary discount and the quote
		// did specify a discretionary discount then add the discretionary
		// discount from the quote as an adjustment to the transaction
		if (this.discretionaryDiscountAmount == 0.0) {

			// Make sure there isn't a discretionary discount
			// adjustment deriving from a previously attached quote.
			removeAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT);

			// Add the adjutsment from the quote.
			if (this.attachedQuote != null) {
				MembershipTransactionAdjustment discretionaryDiscountAdjustment = this.attachedQuote.getAdjustment(AdjustmentTypeVO.DISCRETIONARY_DISCOUNT);
				if (discretionaryDiscountAdjustment != null) {
					addAdjustment(discretionaryDiscountAdjustment);
				}
			}
		}
	}

	/**
	 * Return true if the prime addressee for the transaction has an unattached quote with the same transaction group type code. Return false otherwise.
	 */
	public boolean canAttachQuote() throws RemoteException {
		Vector allowedQuoteList = getAllowedQuoteList();
		LogUtil.debug(this.getClass(), "allowedQuoteList" + allowedQuoteList.size());
		return !allowedQuoteList.isEmpty();
	}

	private Vector allowedQuoteList = null;

	/**
	 * Returns a list of roadside quotes that can be attached to this transaction. Returnes an empty list if no roadside quotes can be attached. Unattached quotes belonging to the client who is selected as the PA in this transaction that have the same transaction group type code can be attached to the transaction.
	 */
	public Vector getAllowedQuoteList() throws RemoteException {
		LogUtil.debug(this.getClass(), "q a");
		if (allowedQuoteList == null) {
			SelectedClient paSelectedClient = getSelectedClient(this.paSelectedClientKey);
			if (paSelectedClient != null) {
				// Get all of the unattached quotes for the client.
				allowedQuoteList = MembershipEJBHelper.getMembershipMgr().getMembershipQuoteListByClientNumber(paSelectedClient.getClient().getClientNumber(), false);
				// Filter the list of quotes according to the business rules.
				// We have already applied the rule that says a quote cannot be
				// attached to more than one transaction.
				// The remove causes all the elements that are higher in the
				// list to
				// be shifted down one and the list size to be reduced by one.
				// So we don't need to increment the counter.
				MembershipQuote memQuote;
				TransactionGroup quoteTransGroup;
				int i = 0;
				while (i < allowedQuoteList.size()) {
					memQuote = (MembershipQuote) allowedQuoteList.get(i);

					// The quote must be current
					if (!memQuote.isCurrent()) {
						allowedQuoteList.removeElementAt(i);
					}
					// The primary transaction type must be the same.
					else if (!memQuote.getTransactionGroupTypeCode().equals(this.transactionGroupTypeCode)) {
						allowedQuoteList.removeElementAt(i);
					}
					// Load the transaction group and perform additional checks
					else {

						LogUtil.debug(this.getClass(), "q1");
						quoteTransGroup = memQuote.getTransactionGroup();
						LogUtil.debug(this.getClass(), "q2");
						// The quote must have valid transaction group data
						// attached to it.
						if (quoteTransGroup == null) {
							LogUtil.debug(this.getClass(), "q3");
							allowedQuoteList.removeElementAt(i);
						}
						// The product should be the same.
						else if (this.selectedProduct == null || !this.selectedProduct.equals(quoteTransGroup.getSelectedProduct())) {
							LogUtil.debug(this.getClass(), "q4");
							allowedQuoteList.removeElementAt(i);
						}
						// The quote should have the same transaction types for
						// the same members as this transaction
						else if (!compareMemberTransactions(quoteTransGroup)) {
							LogUtil.debug(this.getClass(), "q5");
							allowedQuoteList.removeElementAt(i);
						} else {
							i++;
						}
					}
					LogUtil.debug(this.getClass(), "q6");
				}
			} else {
				allowedQuoteList = new Vector();
			}
		}
		return allowedQuoteList;
	}

	/**
	 * Compares the membership transactions in the passed transaction group with this transaction groups membership transactions. Returns true if the passed transaction group has transactions and this transaction group has transactions and the transaction types and memberships are the same.
	 */
	private boolean compareMemberTransactions(TransactionGroup transGroup) throws RemoteException {
		boolean transactionsAreTheSame = true;
		if (transGroup != null && transGroup.hasTransactions() && this.transactionList != null && this.transactionList.size() == transGroup.getTransactionList().size()) {
			MembershipTransactionVO thisTransVO;
			MembershipTransactionVO thatTransVO;
			for (int i = 0; i < this.transactionList.size() && transactionsAreTheSame; i++) {
				thisTransVO = (MembershipTransactionVO) this.transactionList.get(i);
				thatTransVO = transGroup.getMembershipTransactionForClient(thisTransVO.getNewMembership().getClientNumber());
				// Must have a transaction for the same client
				if (thatTransVO == null) {
					transactionsAreTheSame = false;
				}
				// The transaction must be for the same membership. Either a new
				// membership or a membership with the same ID.
				else if (!((thisTransVO.getNewMembership().getMembershipID() == null && thatTransVO.getNewMembership().getMembershipID() == null) || (thisTransVO.getNewMembership().getMembershipID() != null && thatTransVO.getNewMembership().getMembershipID() != null && thisTransVO.getNewMembership().getMembershipID().equals(thatTransVO.getNewMembership().getMembershipID())))) {
					transactionsAreTheSame = false;
				} else {
					transactionsAreTheSame = thisTransVO.getTransactionTypeCode().equals(thatTransVO.getTransactionTypeCode());
				}
			}
		} else {
			transactionsAreTheSame = false;
		}
		return transactionsAreTheSame;
	}

	/**
	 * Return true if the transaction group contains membership transactions
	 */
	public boolean hasTransactions() {
		return this.transactionList != null && !this.transactionList.isEmpty();
	}

	/**
	 * Return true if the prime addressee is automatically renewable by the selected payment method.
	 */
	public boolean isAutomaticallyRenewable() {
		return this.automaticallyRenewable;
	}

	/**
	 * Return true if the client/membership exists in the selected client list.
	 */
	public boolean isClientSelected(SelectedClient selectedClient) throws RemoteException {
		if (this.selectedClientMap != null && selectedClient != null) {
			return this.selectedClientMap.containsKey(selectedClient.getKey());
		}

		return false;
	}

	/**
	 * Return true if the transaction involves an existing membership group.
	 */
	public boolean isExistingMembershipGroupTransaction() {
		return this.selectedMembershipGroup != null;
	}

	/**
	 * Return true if the membership group being transacted contains a membership for the specified membership ID. Return false if no membership group has been selected or the membership ID is not in the selected membership group.
	 */
	public boolean isMembershipInMembershipGroup(Integer membershipID) {
		if (this.selectedMembershipGroup != null) {
			return this.selectedMembershipGroup.isMembershipInMembershipGroup(membershipID);
		}
		return false;
	}

	/**
	 * Return true if a membership group transaction is being processed. A membership group transaction is being processed if:
	 * <ul>
	 * <li>an existing membership group hase been selected.
	 * <li>there are two or more Clients in the selected client list
	 * <li>there are two or more transactions in the transaction list.
	 * </ul>
	 */
	public boolean isMembershipGroupTransaction() {
		if (this.selectedMembershipGroup != null) {
			return true;
		}

		if (this.selectedClientMap.size() > 1) {
			return true;
		}

		return this.transactionList.size() > 1;
	}

	/**
	 * Locate the members being removed from the group. Determine their paid membership value and if the period of cover is less than 14 days then give them the difference as free membership.
	 * 
	 * @todo call this code from remove from group transaction.
	 * @todo implement this code. It was deactivated by request of GH.
	 */
	private TransactionCredit calculateComplimentaryCredit(MembershipTransactionVO memTransVO) throws RemoteException {
		// get memberships attached to the transaction
		MembershipVO newMemVO = memTransVO.getNewMembership();
		DateTime newExpiryDate = newMemVO.getExpiryDate();

		// minimum days of membership
		final int MINIMUM_DAYS = 14;
		DateTime minimumExpiryDate = this.getTransactionDate();
		Interval minimumDays = null;
		Interval activeMembershipPeriod = null;
		try {
			minimumDays = new Interval(0, 0, MINIMUM_DAYS, 0, 0, 0, 0);
			minimumExpiryDate = minimumExpiryDate.add(minimumDays);
			activeMembershipPeriod = new Interval(newExpiryDate, minimumExpiryDate);
		} catch (Exception ex1) {
		}

		LogUtil.debug(this.getClass(), "getComplimentaryCredit");
		if (!memTransVO.getTransactionTypeCode().equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP)) {
			throw new SystemException("Membership transaction '" + memTransVO.getTransactionGroupTypeCode() + "' is not '" + MembershipTransactionTypeVO.TRANSACTION_TYPE_REMOVE_FROM_GROUP + "'.");
		}

		ComplimentaryCredit compCredit = null;

		if (newExpiryDate.before(minimumExpiryDate)) {

			BigDecimal memTerm = new BigDecimal(newMemVO.getMembershipTerm().getTotalDays());
			BigDecimal memValue = newMemVO.getMembershipValue(true, true);
			BigDecimal ratePerDay = memValue.divide(memTerm, BigDecimal.ROUND_HALF_DOWN);
			// difference between remaining days and mimimum days
			int creditDays = activeMembershipPeriod.getTotalDays();
			Interval creditPeriod = null;

			try {
				creditPeriod = new Interval(0, 0, creditDays, 0, 0, 0, 0);
			} catch (Exception ex) {
				throw new RemoteException("Unable to construct the credit period for " + creditDays + ".", ex);
			}

			// get daily old membership rate and multiply by difference
			BigDecimal creditAmount = ratePerDay.multiply(new BigDecimal(creditDays));
			compCredit = new ComplimentaryCredit(creditAmount, creditPeriod, "Credit for less than prescribed remaining membership (" + MINIMUM_DAYS + " days) following a remove from group.");
			LogUtil.debug(this.getClass(), compCredit.toString());
		}
		// else have minimum days left so no actions required.

		return compCredit;

	}

	/**
	 * Does PA still have amounts outstanding in the direct debit system.
	 * 
	 * Requires that transactions have been created first!!!
	 * 
	 * @return
	 */
	public boolean hasPADirectDebitDebt() throws RemoteException {
		MembershipVO oldPAMemVO = getMembershipTransactionForPA().getMembership();
		if (oldPAMemVO == null) {
			return false;
		}
		return oldPAMemVO.hasDirectDebitDebt();
	}

	/**
	 * Return true if a membership group has been selected and the product of the membership group has been changed.
	 */
	public boolean isMembershipGroupProductChanged() {
		if (this.selectedMembershipGroup != null && this.selectedProduct != null) {
			return !this.selectedMembershipGroup.getMembershipGroupProductCode().equals(this.selectedProduct.getProductCode());
		}
		return false;
	}

	/**
	 * Creates and returns a TransactionGroup for a renewal of the specified membership. If the membership does not belong to a membership group then a standard renewal is done. If the membership belongs to a membership group (and they are the prime addressee) then a membership group renewal is done. Creates transactions for all selected memberships. Create transaction fees, and automatic discounts for all transactions. Does not create credit discounts. Does not calculate the amount payable. Throws a ValidationException if the membership cannot be renewed.
	 */
	public static TransactionGroup getRenewalTransactionGroup(MembershipVO contextMemVO, User user, DateTime feeDate) throws RemoteException {
		return TransactionGroup.getRenewalTransactionGroup(contextMemVO, user, feeDate, null, null, false);
	}

	public static TransactionGroup getRenewalTransactionGroup(MembershipVO contextMemVO, User user, DateTime feeDate, boolean renewalPrint) throws RemoteException {
		return TransactionGroup.getRenewalTransactionGroup(contextMemVO, user, feeDate, null, null, renewalPrint);
	}

	public static TransactionGroup getRenewalTransactionGroup(MembershipVO contextMemVO, User user, DateTime feeDate, DateTime overrideTransactionDate, String repairDDPaymentMethodId, boolean renewalPrint) throws RemoteException {
		LogUtil.debug("TransactionGroup", "getRenewalTransactionGroup");
		TransactionGroup transGroup = null;
		try {
			LogUtil.debug(Class.forName("com.ract.membership.TransactionGroup"), "getRenewalTransactionGroup(" + (contextMemVO == null ? "<null>" : contextMemVO.getMembershipNumber()) + "," + (user == null ? "<null>" : user.getUserID()) + "," + (feeDate == null ? "<null>" : feeDate.formatLongDate()) + ")");
		} catch (Exception e) {
			LogUtil.warn("TransactionGroup", "TransactionGroup.getRenewalTransactionGroup() start");
		}

		try {

			// Determine the type of renewal depending on the group membership;
			String transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW;
			LogUtil.debug("TransactionGroup", "isGroupMember");
			if (contextMemVO.isGroupMember()) {
				transTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP;
			}

			LogUtil.debug("TransactionGroup", "invalidTransactionReason");
			// Make sure that the membership can be renewed.
			String invalidTransactionReason = contextMemVO.getInvalidForTransactionReason(transTypeCode);

			if (invalidTransactionReason != null) {
				if (repairDDPaymentMethodId != null && invalidTransactionReason.equals(MembershipVO.MESSAGE_EXPIRED_TOO_LONG)) {
					// ignore expired membership message when repairing DD.
				} else {
					throw new ValidationException("The membership cannot be renewed. " + invalidTransactionReason);
				}
			}
			// If the membership has a current document there should now be xml
			// attached to reconstitute
			// the transaction group as at a point in time. This will eventually
			// be all renewals as a result
			// of a notice, reminder, etc.
			MembershipDocument memDoc = contextMemVO.getCurrentRenewalDocument();
			if (memDoc != null && !contextMemVO.getStatus().equals(MembershipVO.STATUS_ONHOLD)) {
				LogUtil.debug("TransactionGroup", "getRenewalTransactionGroup xml");

				String membershipTransactionXML = memDoc.getTransactionXML();
				LogUtil.debug("TransactionGroup", memDoc.getDocumentTypeCode());
				// System.out.println(membershipTransactionXML);
				if (membershipTransactionXML != null && !membershipTransactionXML.equals("")) {
					LogUtil.debug("TransactionGroup", "xml=\n" + membershipTransactionXML);
					// construct a transaction group from the xml attached to
					// the document.
					transGroup = MembershipHelper.getTransactionGroup(membershipTransactionXML);

					// address a possible caching issue?
					if (MembershipDocument.RENEWAL_NOTICE.equals(memDoc.getDocumentTypeCode())) {
						if (transGroup.getDirectDebitAuthority() != null) {
							LogUtil.debug("TransactionGroup", "Setting the direct debit authority to null");
							transGroup.setDirectDebitAuthority(null, false);
						}
					}

					// set the transaction date as otherwise it will default to
					// when the document was created.
					transGroup.setTransactionDate(new DateTime());

					// set the fee effective date as otherwise it will default
					// to when the document was created
					// transGroup.setFeeEffectiveDate(feeDate);
					if (transGroup.getFeeEffectiveDate() == null) {
						transGroup.setFeeEffectiveDate(contextMemVO.getExpiryDate());
					}

					// set the user who is doing the transaction (critical for
					// IVR/BPAY)
					transGroup.setUser(user);

					// fees/discounts are already loaded as at fee date

					// used for repairing a DD
					transGroup.setOverrideTransactionDate(overrideTransactionDate);
					transGroup.setRepairDDPaymentMethodId(repairDDPaymentMethodId);

					// Calculate the transaction fee
					transGroup.calculateTransactionFee();
				}
			}

			// ie. no document found with xml. In the future this will only be
			// renewals in advance or in arrears or those
			// created via the renewal process. ie.
			// outside the renewal period.
			if (transGroup == null) {
				// System.out.println("\n**\n* txn group is null, recreating... \n*/");

				LogUtil.debug("TransactionGroup", "getRenewalTransactionGroup no xml");
				// Get a transaction ID to reference the transaction group with
				// while
				// processing this transaction.
				MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
				LogUtil.debug("TransactionGroup", "new TransactionGroup");
				// Create a transaction group.
				// The transaction date, preferred commence date and product
				// effective
				// dates are initialised to today when constructed.
				transGroup = new TransactionGroup(transTypeCode, user);
				LogUtil.debug("TransactionGroup", "setMembershipType");
				transGroup.setPrintRenewal(renewalPrint);
				// used for repairing a DD
				transGroup.setOverrideTransactionDate(overrideTransactionDate);
				transGroup.setRepairDDPaymentMethodId(repairDDPaymentMethodId);
				transGroup.setMembershipType(contextMemVO.getMembershipType());
				transGroup.addSelectedClient(contextMemVO); // Also adds other
				// group members
				transGroup.setContextClient(contextMemVO);
				transGroup.validateMembersForTransaction();
				LogUtil.debug("TransactionGroup", "setFeeEffectiveDate");
				// set before calculating the fees
				transGroup.setFeeEffectiveDate(feeDate);
				transGroup.setExpiryDate(feeDate.add(transGroup.getDefaultPeriod()));
				// Use the next product if specified.
				ProductVO productVO = null;
				String nextProductCode = contextMemVO.getNextProductCode();
				LogUtil.debug("TransactionGroup", "nextProductCode=" + nextProductCode);
				if (nextProductCode != null) {
					productVO = refMgr.getProduct(nextProductCode);
				}

				LogUtil.debug("TransactionGroup", "productVO=" + productVO);

				// Use the current product if a next product could not be
				// determined.
				if (productVO == null) {
					productVO = contextMemVO.getProduct();
				}
				
				LogUtil.debug("TransactionGroup", "setSelectedProduct=" + productVO);
				transGroup.setSelectedProduct(productVO);
				LogUtil.debug("TransactionGroup", "createTransactionsForSelectedClients");
				// Create the transactions
				transGroup.createTransactionsForSelectedClients();
				LogUtil.debug("TransactionGroup", "createTransactionFees");
				// Create transaction fees
				transGroup.createTransactionFees();

				// Calculate the transaction fee
				transGroup.calculateTransactionFee();

			}
		} catch (ValidationException ve) {
			throw ve;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("Error creating renewal Transaction group: ", e);
		}

		return transGroup;
	}

	/**
	 * Submit the transaction group to the transaction manager. If the transaction group has already been submitted then it is not sent to the transaction manager and a null list is returned. 3/1/03 dgk Add parameter to handle repair transactions - do not create receipts
	 */
	public synchronized Collection submit() throws RemoteException, ValidationException {
		Collection transactionList = null;

		String transactionGroupSummary = "";
		try {
			Collection transactionGroupTransactionList = this.getTransactionList();
			if (transactionGroupTransactionList != null) {
				MembershipTransactionVO memTransVO = null;
				Iterator txGroupIt = transactionGroupTransactionList.iterator();
				while (txGroupIt.hasNext()) {
					memTransVO = (MembershipTransactionVO) txGroupIt.next();
					// add transaction id
					transactionGroupSummary += ("|" + memTransVO.getTransactionID());
				}
			}
		} catch (Exception ex) {
			LogUtil.debug(this.getClass(), "Unable to log submit() lines.");
		}

		LogUtil.info(this.getClass(), "submit() 1 '" + this.transactionGroupID + "' '" + transactionGroupSummary + "'.");

		if (!this.submitted) {
			// @todo use local?
			MembershipTransactionMgr memTransMgr = MembershipEJBHelper.getMembershipTransactionMgr();
			try {
				this.submitted = true;
				
				LogUtil.info(getClass(), "begin process transaction");
				transactionList = memTransMgr.processTransaction(this);
				LogUtil.info(getClass(), "end process transaction");
			} catch (Exception e) {
				e.printStackTrace();
				this.submitted = false;
				if (e instanceof ValidationException) {
					throw (ValidationException) e;
				} else if (e instanceof SystemException) {
					throw (SystemException) e;
				} else if (e instanceof RemoteException) {
					throw (RemoteException) e;
				} else {
					throw new SystemException(e);
				}
			}
		}

		LogUtil.info(this.getClass(), "submit() 2 '" + this.transactionGroupID + "' '" + transactionGroupSummary + "'.");
		return transactionList;
	}

	public String getWritableClassName() {
		return this.WRITABLE_CLASSNAME;
	}

	/**
	 * XML String representation of the transaction group
	 * 
	 * @return
	 */
	public String toXML() throws SystemException {
		String transactionXML = null;
		ArrayList excludedClassList = new ArrayList();
		try {
			excludedClassList.add(Class.forName("com.ract.membership.DiscountTypeVO"));
			excludedClassList.add(Class.forName("com.ract.membership.MembershipCardVO"));
			excludedClassList.add(Class.forName("com.ract.membership.MembershipDocument"));
			excludedClassList.add(Class.forName("com.ract.membership.MembershipProfileVO"));
			excludedClassList.add(Class.forName("com.ract.membership.MembershipTypeVO"));
			excludedClassList.add(Class.forName("com.ract.membership.ProductVO"));
			excludedClassList.add(Class.forName("com.ract.membership.ProductBenefitTypeVO"));
			excludedClassList.add(Class.forName("com.ract.common.StreetSuburbVO"));

		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
			// throw new SystemException("Failed to find class : " +
			// cnfe.getMessage());
		}
		transactionXML = XMLHelper.toXML(excludedClassList, this); // writer.toString();
		return transactionXML;
	}

	private Integer directDebitScheduleId;

	public Integer getDirectDebitScheduleId() {
		return this.directDebitScheduleId;
	}

	public void setDirectDebitScheduleId(Integer directDebitScheduleId) {
		this.directDebitScheduleId = directDebitScheduleId;
	}

	public void write(ClassWriter cw) throws RemoteException {
		LogUtil.debug(this.getClass(), "write");
		cw.startClass(WRITABLE_CLASSNAME);
		cw.writeAttributeList("adjustmentList", this.adjustmentList);
		cw.writeAttribute("amountPayable", NumberUtil.formatValue(this.amountPayable, "########0.00"));
		cw.writeAttribute("automaticallyRenewable", this.automaticallyRenewable);
		LogUtil.debug(this.getClass(), "contextClient");
		cw.writeAttribute("contextClient", this.contextClient);
		cw.writeAttribute("directDebitAuthority", this.directDebitAuthority);
		cw.writeAttribute("directDebitScheduleId", this.directDebitScheduleId);
		cw.writeAttribute("earnedAmount", NumberUtil.formatValue(this.earnedAmount, "########0.00"));
		cw.writeAttribute("expiryDate", this.getExpiryDate().formatShortDate());
		cw.writeAttribute("effectiveDate", this.getFeeEffectiveDate().formatShortDate());
		cw.writeAttribute("grossTransactionFee", NumberUtil.formatValue(this.grossTransactionFee, "########0.00"));
		cw.writeAttribute("gstAmount", NumberUtil.formatValue(this.gstAmount, "########0.00"));
		cw.writeAttribute("gstRate", this.gstRate);
		cw.writeAttribute("membershipProfileCode", (this.membershipProfileVO != null ? this.membershipProfileVO.getProfileCode() : ""));
		cw.writeAttribute("membershipTerm", this.membershipTerm);
		cw.writeAttribute("membershipTypeCode", (this.membershipType != null ? this.membershipType.getMembershipTypeCode() : ""));
		cw.writeAttribute("paSelectedClientKey", this.paSelectedClientKey);
		cw.writeAttribute("netTransactionFee", NumberUtil.formatValue(this.getNetTransactionFee(), "########0.00"));
		cw.writeAttribute("preferredCommenceDate", this.preferredCommenceDate);
		cw.writeAttribute("rejoinType", this.rejoinType);
		cw.writeAttribute("repairPaymentMethodId", this.repairPaymentMethodId);
		cw.writeAttribute("salesBranchCode", (this.user != null ? this.user.getSalesBranchCode() : ""));
		LogUtil.debug(this.getClass(), "selectedClientMap");
		cw.writeAttributeList("selectedClientMap", this.selectedClientMap);
		Collection memGroupDetailList = null;
		LogUtil.debug(this.getClass(), "selectedMembershipGroup");
		MembershipGroupVO memGroupVO = getSelectedMembershipGroup();
		if (memGroupVO != null && !memGroupVO.hasException()) {
			memGroupDetailList = memGroupVO.getMembershipGroupDetailList();
		}
		cw.writeAttributeList("selectedMembershipGroup", memGroupDetailList);
		cw.writeAttribute("selectedProductCode", (this.selectedProduct != null ? this.selectedProduct.getProductCode() : ""));
		cw.writeAttribute("totalDiscountAmount", NumberUtil.formatValue(this.totalDiscountAmount, "########0.00"));
		cw.writeAttribute("transactionDate", this.transactionDate);
		cw.writeAttribute("transactionGroupTypeCode", this.transactionGroupTypeCode);
		LogUtil.debug(this.getClass(), "b4 transactionList");
		cw.writeAttributeList("transactionList", this.transactionList);
		LogUtil.debug(this.getClass(), "after transactionList");
		cw.writeAttribute("transactionReason", this.transactionReason);
		cw.writeAttribute("unearnedAmount", NumberUtil.formatValue(this.unearnedAmount, "########0.00"));
		cw.writeAttribute("userID", (this.user != null ? this.user.getUserID() : ""));
		cw.writeAttribute("username", (this.user != null ? this.user.getUsername() : ""));
		cw.endClass();
	}

	private void setFromXML(Node transGroupNode) throws RemoteException {
		LogUtil.debug(this.getClass(), "loading TransactionGroup from xml");
		MethodCounter.addToCounter("TransactionGroup", "loading xml");
		if (transGroupNode == null || !transGroupNode.getNodeName().equals(WRITABLE_CLASSNAME)) {
			throw new SystemException("Failed to create TransactionGroup from XML node. The node is not valid.");
		}

		// Initialise the lists
		this.adjustmentList = new Hashtable();
		this.clientSearchList = new Vector();
		this.transactionList = new Vector();

		NodeList elementList = transGroupNode.getChildNodes();
		Node elementNode = null;
		Node writableNode;
		String nodeName = null;
		String valueText = null;
		for (int i = 0; i < elementList.getLength(); i++) {
			elementNode = elementList.item(i);
			nodeName = elementNode.getNodeName();

			writableNode = XMLHelper.getWritableNode(elementNode);
			valueText = elementNode.hasChildNodes() ? elementNode.getFirstChild().getNodeValue() : null;

			LogUtil.debug(this.getClass(), "nodeName=" + nodeName + " " + valueText);

			if ("adjustmentList".equals(nodeName)) {
				NodeList adjustmentNodeList = elementNode.getChildNodes();
				if (adjustmentNodeList != null) {
					for (int j = 0; j < adjustmentNodeList.getLength(); j++) {
						Node adjustmentNode = adjustmentNodeList.item(j);
						if (adjustmentNode.getNodeType() == Node.ELEMENT_NODE) {
							MembershipTransactionAdjustment memTransAdj = new MembershipTransactionAdjustment(adjustmentNode);
							addAdjustment(memTransAdj);
						}
					}
				}
			}
			// else if ("amountPayable".equals(nodeName) && valueText != null)
			// {
			// // This is a calculated value.
			// }
			else if ("automaticallyRenewable".equals(nodeName) && valueText != null) {
				this.automaticallyRenewable = Boolean.valueOf(valueText).booleanValue();
			} else if ("contextClient".equals(nodeName) && writableNode != null) {
				this.contextClient = new SelectedClient(writableNode);
			} else if ("directDebitAuthority".equals(nodeName) && writableNode != null) {
				this.directDebitAuthority = new DirectDebitAuthority(writableNode);
			} else if ("directDebitScheduleId".equals(nodeName) && valueText != null) {
				LogUtil.debug(this.getClass(), "node directDebitScheduleId=" + directDebitScheduleId + " " + valueText);
				this.directDebitScheduleId = new Integer(valueText);
			}
			// else if ("earnedAmount".equals(nodeName) && valueText != null)
			// {
			// // This is a calculated value.
			// }
			else if ("expiryDate".equals(nodeName) && valueText != null) {
				// The expiry date for the transaction group in the xml file was
				// derived from one of the memberships in being transacted.
				// It does not need to go anywhere.
				try {
					this.setExpiryDate(new DateTime(valueText));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if ("effectiveDate".equals(nodeName) && valueText != null) {
				try {
					this.setFeeEffectiveDate(new DateTime(valueText));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// else if ("grossTransactionFee".equals(nodeName) && valueText !=
			// null)
			// {
			// // This is a calculated value.
			// }
			// else if ("gstAmount".equals(nodeName) && valueText != null)
			// {
			// // This is a calculated value.
			// }
			// else if ("gstRate".equals(nodeName) && valueText != null)
			// {
			// // Let the gst rate be reinitialised.
			// }
			else if ("membershipProfileCode".equals(nodeName) && valueText != null) {
				MembershipProfileVO memProfileVO = MembershipEJBHelper.getMembershipRefMgr().getMembershipProfile(valueText);
				if (memProfileVO == null) {
					throw new SystemException("Failed to find membership profile '" + valueText + "'.");
				}
				this.membershipProfileVO = memProfileVO;
			} else if ("membershipTerm".equals(nodeName) && valueText != null) {
				try {
					this.membershipTerm = new Interval(valueText);
				} catch (Exception e) {
					throw new SystemException("The membership term '" + valueText + "' is not a valid interval. " + e.getMessage());
				}
			} else if ("membershipTypeCode".equals(nodeName) && valueText != null) {
				MembershipTypeVO memTypeVO = MembershipEJBHelper.getMembershipRefMgr().getMembershipType(valueText);
				if (memTypeVO == null) {
					throw new SystemException("Failed to find membership type '" + valueText + "'.");
				}
				this.membershipType = memTypeVO;
			} else if ("paSelectedClientKey".equals(nodeName) && valueText != null) {
				this.paSelectedClientKey = valueText;
			}
			// else if ("netTransactionFee".equals(nodeName) && valueText !=
			// null)
			// {
			// // This is a calculated value.
			// }
			else if ("preferredCommenceDate".equals(nodeName) && valueText != null) {
				try {
					this.preferredCommenceDate = new DateTime(valueText);
				} catch (ParseException pe) {
					throw new SystemException("The preferred commence date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("rejoinType".equals(nodeName) && writableNode != null) {
				this.rejoinType = new ReferenceDataVO(writableNode);
			} else if ("repairPaymentMethodId".equals(nodeName) && valueText != null) {
				LogUtil.debug(this.getClass(), "repairPaymentMethodId=" + valueText);
				this.repairPaymentMethodId = valueText;
			} else if ("selectedClientMap".equals(nodeName)) {
				NodeList selectList = elementNode.getChildNodes();
				if (selectList != null) {
					Node node;
					SelectedClient selectedClient;
					for (int j = 0; j < selectList.getLength(); j++) {
						node = (Node) selectList.item(j);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							selectedClient = new SelectedClient(node);
							this.selectedClientMap.put(selectedClient.getKey(), selectedClient);
						}
					}
				}
			} else if ("selectedMembershipGroup".equals(nodeName)) {
				NodeList groupNodeList = elementNode.getChildNodes();
				if (groupNodeList != null) {
					ArrayList groupDetailList = new ArrayList();
					for (int j = 0; j < groupNodeList.getLength(); j++) {
						Node groupNode = groupNodeList.item(j);
						if (groupNode.getNodeType() == Node.ELEMENT_NODE) {
							MembershipGroupDetailVO groupDetVO = new MembershipGroupDetailVO(groupNode);
							groupDetVO.setMode(ValueObject.MODE_NORMAL);
							groupDetailList.add(groupDetVO);
						}
					}
					if (!groupDetailList.isEmpty()) {
						this.selectedMembershipGroup = new MembershipGroupVO(groupDetailList);
					}
				}
			} else if ("selectedProductCode".equals(nodeName) && valueText != null) {
				ProductVO productVO = MembershipEJBHelper.getMembershipRefMgr().getProduct(valueText);
				if (productVO == null) {
					throw new SystemException("Failed to find membership product '" + valueText + "'.");
				}
				this.selectedProduct = productVO;
			}
			// else if ("totalDiscountAmount".equals(nodeName) && valueText !=
			// null)
			// {
			// // This is a calculated value.
			// }
			else if ("transactionDate".equals(nodeName) && valueText != null) {
				try {
					this.transactionDate = new DateTime(valueText);
				} catch (ParseException pe) {
					throw new SystemException("The transaction date '" + valueText + "' is not a valid date : " + pe.getMessage());
				}
			} else if ("transactionGroupTypeCode".equals(nodeName) && valueText != null) {
				this.transactionGroupTypeCode = valueText;
			} else if ("transactionList".equals(nodeName)) {
				NodeList selectList = elementNode.getChildNodes();
				if (selectList != null) {
					Node node;
					MembershipTransactionVO memTransVO;
					for (int j = 0; j < selectList.getLength(); j++) {
						node = (Node) selectList.item(j);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							memTransVO = new MembershipTransactionVO(node);
							this.transactionList.add(memTransVO);
						}
					}
				}
			} else if ("transactionReason".equals(nodeName) && writableNode != null) {
				this.transactionReason = new ReferenceDataVO(writableNode);
			}
			// else if ("unearnedAmount".equals(nodeName))
			// {
			// // This is a calculated value.
			// }
			else if ("userID".equals(nodeName) && valueText != null) {
				// Lookup the user.
				this.user = UserEJBHelper.getUserMgr().getUser(valueText);
			}
		}

	}

	/**
	 * Log the membership transaction.
	 */
	public void logMembershipTransactions(String context) throws RemoteException {
		LogUtil.debug(this.getClass(), "???? Membership Transactions - " + context + " ????");
		LogUtil.debug(this.getClass(), "???? Group transaction type code = " + this.transactionGroupTypeCode);
		LogUtil.debug(this.getClass(), "???? Selected product = " + (this.selectedProduct == null ? "null" : this.selectedProduct.getProductCode()) + " ????");
		if (this.transactionList != null && !this.transactionList.isEmpty()) {
			MembershipTransactionVO memTransVO;
			MembershipTransactionFee memTransFee;
			MembershipTransactionDiscount memTransDisc;
			Iterator it = this.transactionList.iterator();
			while (it.hasNext()) {
				memTransVO = (MembershipTransactionVO) it.next();
				LogUtil.debug(this.getClass(), "???? Transaction ID     = " + memTransVO.getTransactionID());
				LogUtil.debug(this.getClass(), "???? Transaction type   = " + memTransVO.getTransactionTypeCode());
				LogUtil.debug(this.getClass(), "???? Membership number  = " + memTransVO.getNewMembership().getMembershipNumber());
				LogUtil.debug(this.getClass(), "???? Membership product = " + memTransVO.getNewMembership().getProductCode() + " (" + (memTransVO.getMembership() == null ? "null" : memTransVO.getMembership().getProductCode()) + ")");
				LogUtil.debug(this.getClass(), "???? Membership expiry  = " + memTransVO.getNewMembership().getExpiryDate());
				LogUtil.debug(this.getClass(), "???? Membership credit  = " + memTransVO.getNewMembership().getCreditAmount());
				LogUtil.debug(this.getClass(), "???? Membership value   = " + memTransVO.getNewMembership().getMembershipValue(false) + "(" + memTransVO.getNewMembership().getMembershipValue(true) + ")");
				LogUtil.debug(this.getClass(), "???? Prime addressee    = " + memTransVO.isPrimeAddressee());
				Collection feeList = memTransVO.getTransactionFeeList();
				if (feeList != null && !feeList.isEmpty()) {
					Iterator feeListIT = feeList.iterator();
					while (feeListIT.hasNext()) {
						memTransFee = (MembershipTransactionFee) feeListIT.next();
						LogUtil.debug(this.getClass(), "????? Fee spec ID = " + memTransFee.getFeeSpecificationID());
						LogUtil.debug(this.getClass(), "????? Fee type    = " + memTransFee.getFeeSpecificationVO().getFeeTypeCode());
						LogUtil.debug(this.getClass(), "????? Gross fee   = " + memTransFee.getGrossFee());
						Collection discList = memTransFee.getDiscountList();
						if (discList != null && !discList.isEmpty()) {
							Iterator discListIT = discList.iterator();
							while (discListIT.hasNext()) {
								memTransDisc = (MembershipTransactionDiscount) discListIT.next();
								LogUtil.debug(this.getClass(), "?????? Discount type   = " + memTransDisc.getDiscountCode());
								LogUtil.debug(this.getClass(), "?????? Discount amount = " + memTransDisc.getDiscountAmount());
								LogUtil.debug(this.getClass(), "?????? Discount period = " + (memTransDisc.getDiscountPeriod() == null ? "null" : memTransDisc.getDiscountPeriod().toString()));
							}
						} else {
							LogUtil.debug(this.getClass(), "????? Discount list is empty.");
						}
					}
				} else {
					LogUtil.debug(this.getClass(), "???? Fee list is empty.");
				}
			}
		} else {
			LogUtil.debug(this.getClass(), "???? Transaction list is empty.");
		}
	}

	/**
	 * add a schedule to remove.
	 */
	protected void addPaymentMethodToRemove(PaymentMethod paymentMethod) {
		if (this.paymentMethodListToRemove != null) {
			this.paymentMethodListToRemove.add(paymentMethod);
		}
	}

	public ArrayList getPaymentMethodListToRemove() {
		return this.paymentMethodListToRemove;
	}

	/**
	 * check that all memberships are valid for this transaction. If not, throw an exception Assumes memberships have already been added to the transaction using 'addSelectedClient'
	 * 
	 * @throws ValidationException
	 */
	private void validateMembersForTransaction() throws ValidationException {
		// MembershipTransactionVO transVo = null;
		// MembershipVO memVO = null;
		Iterator en = this.getSelectedClientList().iterator();
		SelectedClient sc = null;
		while (en.hasNext()) {
			sc = (SelectedClient) en.next();
			if (sc.getInvalidReason() != null) {
				throw new ValidationException(sc.getInvalidReason());
			}
		}
	}

	/**
	 * Looks through a transaction group and totals all the credit amounts (as distinct from discounts)
	 * 
	 * @param transGrp
	 *            Description of the Parameter
	 * @return double The total amount of credits.
	 */
	public double getTotalCredits() {
		double creditAmount = 0;
		Vector groupTransList = this.getTransactionList();
		MembershipTransactionVO trans = null;
		// double discounts = 0;
		// double credits = 0;
		try {
			for (int x = 0; x < groupTransList.size(); x++) {
				trans = (MembershipTransactionVO) groupTransList.get(x);
				creditAmount += trans.getCreditDiscountApplied();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return creditAmount;
	}

	public boolean isBatch() {
		return batch;
	}

	public void setBatch(boolean batch) {
		this.batch = batch;
	}

	public DateTime getOverrideTransactionDate() {
		return overrideTransactionDate;
	}

	public void setOverrideTransactionDate(DateTime overrideTransactionDate) {
		this.overrideTransactionDate = overrideTransactionDate;
	}

	public String getRepairDDPaymentMethodId() {
		return repairDDPaymentMethodId;
	}

	public void setRepairDDPaymentMethodId(String repairDDPaymentMethodId) {
		this.repairDDPaymentMethodId = repairDDPaymentMethodId;
	}

	public String toString() {
		String grpString = "";
		try {
			grpString = "\n>>---------- Transaction Group -----------------<<" + "\namountPayable =               " + amountPayable + "\nautomaticallyRenewable =      " + this.automaticallyRenewable + "\nbatch =                       " + this.batch + "\ncontextClient =               " + this.contextClient.getClient().getClientNumber() + "\ndiscretionaryDiscountAmount = " + this.discretionaryDiscountAmount + "\nearnedAmount =                " + this.earnedAmount + "\nfeeEffectiveDate =            " + this.feeEffectiveDate + "\ngrossTransactionFee =         " + this.grossTransactionFee + "\ngstAmount =                   " + this.gstAmount + "\nimmediateAmount =             " + this.immediateAmount + "\njoinFeeCalculated =           " + this.joinFeeCalculated + "\njoinFeeCount =                " + this.joinFeeCount + "\nmembershipType =              " + this.membershipType + "\nmembershipTerm =              " + this.membershipTerm.getTotalDays() + "\ntotalDiscountAmount =         " + this.totalDiscountAmount
					+ "\nmaxDiscretionaryDiscountAmt = " + this.getMaxDiscretionaryDiscountAmount() + "\nnetTransactionFee =           " + this.getNetTransactionFee();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return grpString;
	}

	/**
	 * Utility method to show the content of this transaction group
	 * 
	 * @return String
	 */
	public String listTransactions() {
		String tList = "\n......" + this.transactionGroupID + ".......";
		MembershipTransactionVO tTrans = null;
		for (int xx = 0; xx < this.transactionList.size(); xx++) {
			tTrans = (MembershipTransactionVO) this.transactionList.get(xx);
			try {
				tList += "\nmembershipId = " + tTrans.getMembershipID() + "\namountPayable = " + tTrans.getAmountPayable() + "\ncreditDiscountApplied = " + tTrans.getCreditDiscountApplied() + "\ntransferCreditApplied = " + tTrans.getTransferCreditApplied() + "\nunearnedCreditApplied = " + tTrans.getUnearnedCreditApplied() + "\nunearnedAmountOutstanding = " + tTrans.getUnearnedAmountOutstanding();

			} catch (Exception ex) {
				System.out.println(ex + "");
			}
		}

		return tList;
	}

	/**
	 * Create a new renewal object for a membership May simply re-construct an existing membership document or create a new document.
	 * 
	 * @param TransactionGroup
	 *            The transaction group object for the current (not renewal) transaction
	 * @param User
	 *            The user object
	 * @param DateTime
	 *            The date on which the renewal period begins
	 */

	public static TransactionGroup getReplaceRenewalTransactionGroup(MembershipVO contextMemVO, User user) throws Exception {
		DateTime startDate = contextMemVO.getExpiryDate();
		CommonMgrLocal cMgr = CommonEJBHelper.getCommonMgrLocal();
		String intervalSpec = cMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "DEFTERM");
		Interval renPeriod = null;
		boolean isGroup = false;
		try {
			renPeriod = new Interval(intervalSpec);
		} catch (Exception e) {
			renPeriod = new Interval(1, 0, 0, 0, 0, 0, 0);
		}
		DateTime expiryDate = startDate.add(renPeriod);
		String transType = MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP;
		TransactionGroup transGroup = null;
		try {
			MembershipMgrLocal mgr = MembershipEJBHelper.getMembershipMgrLocal();
			transGroup = new TransactionGroup(transType, user);
			transGroup.setPrintRenewal(true);
			transGroup.setMembershipType(contextMemVO.getMembershipType());
			transGroup.setFeeEffectiveDate(startDate);
			transGroup.setExpiryDate(expiryDate);
			transGroup.setOverrideTransactionDate(startDate);

			MembershipVO memVO;
			MembershipVO newMemVO;
			MembershipGroupVO memGroupVO = contextMemVO.getMembershipGroup();
			ArrayList<MembershipVO> memberList = mgr.getMembershipGroup(contextMemVO.getMembershipID());
			if (memberList.size() > 1) {
				isGroup = true;
			}
			MembershipTransactionVO newTransVo;
			for (int xx = 0; xx < memberList.size(); xx++) {
				memVO = memberList.get(xx);
				SelectedClient sClient = new SelectedClient(memVO, memVO.getJoinDate());
				newTransVo = transGroup.getRenewTransaction(sClient, startDate);
				newTransVo.setPrimeAddressee(memVO.isPrimeAddressee());
				newTransVo.setEffectiveDate(startDate);
				newTransVo.setEndDate(expiryDate);
				if (isGroup) {
					newTransVo.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP);
				} else {
					newTransVo.setTransactionTypeCode(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW);
				}
				newTransVo.setMembership(memVO);

				newMemVO = memVO.copy(true);
				newMemVO.setStatus(MembershipVO.STATUS_ACTIVE);
				newMemVO.setAllowToLapse(Boolean.valueOf(false)); // Should
				// already be
				// false
				// according
				// to the
				// business
				// rules but
				// do it any
				// way in case
				// the rules
				// have been
				// changed
				// elsewhere.
				newMemVO.setCreditPeriod(null);
				newMemVO.setExpiryDate(expiryDate);
				newMemVO.setNextProductCode(null);
				newMemVO.setProduct(transGroup.selectedProduct);
				newTransVo.setNewMembership(newMemVO);

				transGroup.addSelectedClient(sClient);
				transGroup.addMembershipTransaction(newTransVo);
				if (memVO.isPrimeAddressee()) {
					transGroup.setPrimeAddressee(memVO.getMembershipID());
					transGroup.paSelectedClientKey = sClient.getKey();
				}
			}

			transGroup.setContextClient(contextMemVO);
			// Use the next product if specified.
			ProductVO productVO = null;
			String nextProductCode = contextMemVO.getNextProductCode();
			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
			if (nextProductCode != null) {
				productVO = refMgr.getProduct(nextProductCode);
			}
			// Use the current product if a next product could not be
			// determined.
			if (productVO == null) {
				productVO = contextMemVO.getProduct();
			}
			transGroup.setSelectedProduct(productVO);
			transGroup.createTransactionFees();
			transGroup.calculateTransactionFee();
		} catch (ValidationException ve) {
			throw ve;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("Error creating renewal Transaction group: ", e);
		}
		return transGroup;
	}

	public Interval getDefaultPeriod() {
		CommonMgr cMgr = CommonEJBHelper.getCommonMgr();
		String intervalSpec = null;
		try {
			intervalSpec = cMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, "DEFTERM");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		Interval renPeriod = null;
		try {
			renPeriod = new Interval(intervalSpec);
		} catch (Exception e) {
			try {
				renPeriod = new Interval(1, 0, 0, 0, 0, 0, 0);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return renPeriod;
	}

	public boolean getForceCancelDueToFailedDD() {
		return forceCancelDueToFailedDD;
	}

	public void setForceCancelDueToFailedDD(boolean forceCancelDueToFailedDD) {
		this.forceCancelDueToFailedDD = forceCancelDueToFailedDD;
	}

}
