package com.ract.membership;

import java.util.Collection;
import java.util.Iterator;

import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.DateTime;

/**
 * <p>
 * Runs the SMS renewal reminders.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class SMSRenewalReminderRunner {
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("Usage: SMSRenewalsReminerRunner aston 1099 [01/01/2007] true");
			return;
		}

		try {
			String host = args[0];
			int port = Integer.parseInt(args[1]);
			DateTime targetDate = null;
			boolean constructNotices = false;
			if (args.length == 3) {
				constructNotices = Boolean.valueOf(args[2]).booleanValue();
				// default to today
				targetDate = new DateTime().getDateOnly();
			}
			if (args.length == 4) {
				targetDate = new DateTime(args[2]);
				constructNotices = Boolean.valueOf(args[3]).booleanValue();
			}

			System.out.println("host=" + host);
			System.out.println("port=" + port);
			System.out.println("targetDate=" + targetDate);
			System.out.println("constructNotices=" + constructNotices);

			RACTPropertiesProvider.setContextURL(host, port);
			RenewalMgr renewalMgr = MembershipEJBHelper.getRenewalMgr();

			MembershipVO memVO = null;

			// get list of eligible members
			int counter = 0;
			Collection reminderList = renewalMgr.getSMSRenewalReminderList(targetDate);
			for (Iterator reminderIt = reminderList.iterator(); reminderIt.hasNext();) {
				counter++;
				memVO = (MembershipVO) reminderIt.next();
				System.out.println("Membership=" + memVO.getClientNumber() + " " + memVO.getExpiryDate());
			}
			System.out.println("Memberships=" + counter);

			if (constructNotices) {
				System.out.println("constructNotices");

				// construct the messages - send in null target date as we
				// already have the list
				renewalMgr.constructSMSRenewalReminders(null, reminderList);
			}

		} catch (Exception ex) {
			System.err.println(ex);
		}

	}

}
