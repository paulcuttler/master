package com.ract.membership;

import java.rmi.RemoteException;

import javax.ejb.CreateException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ract.common.CheckDigit;
import com.ract.common.ClassWriter;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ReferenceDataVO;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.ValueObject;
import com.ract.common.Writable;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.util.XMLHelper;

/**
 * A membership card that has been requested or issued to the membership. The
 * issue date and expiry date will be null if the card has been requested but
 * not issued.
 * 
 * @hibernate.class table="mem_membership_card" lazy="false"
 * 
 * @author T. Bakker created 5 March 2003
 */
public class MembershipCardVO extends ValueObject implements Writable, Comparable {
    /**
     * The name to call this class when writing the class data using a
     * ClassWriter
     */
    public final static String WRITABLE_CLASSNAME = "MembershipCard";

    // These attributes are set when the card is requested
    protected Integer cardID;

    protected int issuanceNumber;

    protected Integer membershipID;

    protected DateTime requestDate;

    protected String reasonCode;

    protected String reasonType;

    // These attributes are set when the card is issued.
    protected String cardName;

    protected String cardNumber;

    protected DateTime expiryDate;

    protected DateTime issueDate;

    protected DateTime joinDate;

    protected String productCode;

    private ProductVO product;

    protected String tierCode;

    private String userId;

    private String rego;

    static public final String DATA_STORE_DATABASE = "DB";
    static public final String DATA_STORE_WEB_SERVICE = "WS";

    /**
     * Default constructor. Required for entity bean.
     */
    public MembershipCardVO() {
    }

    /**
     * Use this constructor when copying a card.
     * 
     * @param cardID
     *            Description of the Parameter
     * @param cardNumber
     *            Description of the Parameter
     * @param expiryDate
     *            Description of the Parameter
     * @param issuanceNumber
     *            Description of the Parameter
     * @param issueDate
     *            Description of the Parameter
     * @param joinDate
     *            Description of the Parameter
     * @param cardName
     *            Description of the Parameter
     * @param memID
     *            Description of the Parameter
     * @param prodCode
     *            Description of the Parameter
     * @param reasonType
     *            Description of the Parameter
     * @param reasonCode
     *            Description of the Parameter
     * @param requestDate
     *            Description of the Parameter
     */
    public MembershipCardVO(Integer cardID, String cardNumber, DateTime expiryDate, int issuanceNumber, DateTime issueDate, DateTime joinDate, String cardName, Integer memID, String prodCode, String reasonType, String reasonCode, DateTime requestDate, String tierCode, String rego) {
	this.cardID = cardID;
	this.cardNumber = cardNumber;
	this.expiryDate = expiryDate;
	this.issuanceNumber = issuanceNumber;
	this.issueDate = issueDate;
	this.joinDate = joinDate;
	this.cardName = cardName;
	this.membershipID = memID;
	this.productCode = prodCode;
	this.reasonType = reasonType;
	this.reasonCode = reasonCode;
	this.requestDate = requestDate;
	this.tierCode = tierCode;
	this.rego = rego;
    }

    /**
     * Use this constructor when requesting a new card.
     * 
     * @param membershipID
     *            Description of the Parameter
     * @param issuanceNumber
     *            Description of the Parameter
     * @param requestDate
     *            Description of the Parameter
     * @param reasonType
     *            Description of the Parameter
     * @param reasonCode
     *            Description of the Parameter
     */
    public MembershipCardVO(Integer membershipID, int issuanceNumber, DateTime requestDate, String reasonType, String reasonCode, String tierCode, DateTime joinDate, String rego) {
	this.membershipID = membershipID;
	this.issuanceNumber = issuanceNumber;
	this.requestDate = requestDate;
	this.reasonCode = reasonCode;
	this.reasonType = reasonType;
	this.tierCode = tierCode;
	this.joinDate = joinDate;
	this.rego = rego;

    }

    /**
     * Use this constructor when loading card data from an XML history file.
     * 
     * @param node
     *            Description of the Parameter
     * @exception CreateException
     *                Description of the Exception
     */
    public MembershipCardVO(Node node) throws SystemException {
	NodeList elementList = node.getChildNodes();
	Node elementNode;
	String valueText;
	for (int i = 0; i < elementList.getLength(); i++) {
	    elementNode = elementList.item(i);

	    if (elementNode.hasChildNodes()) {
		valueText = elementNode.getFirstChild().getNodeValue();
	    } else {
		valueText = null;
	    }

	    if (valueText != null) {
		if ("cardID".equals(elementNode.getNodeName())) {
		    this.cardID = new Integer(valueText);
		} else if ("cardNumber".equals(elementNode.getNodeName())) {
		    this.cardNumber = valueText;
		} else if ("expiryDate".equals(elementNode.getNodeName())) {
		    try {
			this.expiryDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			addWarning("The card expiry expiry date '" + valueText + "' is not a valid date : " + pe.getMessage());
		    }
		} else if ("issuanceNumber".equals(elementNode.getNodeName())) {
		    this.issuanceNumber = Integer.parseInt(valueText);
		} else if ("issueDate".equals(elementNode.getNodeName())) {
		    try {
			this.issueDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			addWarning("The card issue date '" + valueText + "' is not a valid date : " + pe.getMessage());
		    }
		} else if ("joinDate".equals(elementNode.getNodeName())) {
		    try {
			this.joinDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			addWarning("The card join date '" + valueText + "' is not a valid date : " + pe.getMessage());
		    }
		} else if ("membershipID".equals(elementNode.getNodeName())) {
		    this.membershipID = new Integer(valueText);
		} else if ("cardName".equals(elementNode.getNodeName())) {
		    this.cardName = valueText;
		} else if ("productCode".equals(elementNode.getNodeName())) {
		    this.productCode = valueText;
		} else if ("reasonCode".equals(elementNode.getNodeName())) {
		    this.reasonCode = valueText;
		} else if ("reasonType".equals(elementNode.getNodeName())) {
		    this.reasonType = valueText;
		} else if ("rego".equals(elementNode.getNodeName())) {
		    this.rego = valueText;
		} else if ("requestDate".equals(elementNode.getNodeName())) {
		    try {
			this.requestDate = new DateTime(valueText);
		    } catch (java.text.ParseException pe) {
			addWarning("The card request date '" + valueText + "' is not a valid date : " + pe.getMessage());
		    }
		}
	    }
	}

	setReadOnly(true);
	try {
	    setMode(ValueObject.MODE_HISTORY);
	} catch (ValidationException ve) {
	    // Should be setting it to a valid value so pass the exception
	    // back as a system exception
	    throw new SystemException(ve.getMessage());
	}
    }

    /**
     * @hibernate.id column="card_id" generator-class="assigned"
     */
    public Integer getCardID() {
	return this.cardID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="membership_id"
     */
    public Integer getMembershipID() {
	return this.membershipID;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="card_number"
     */
    public String getCardNumber() {
	return this.cardNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="issuance_number"
     */
    public int getIssuanceNumber() {
	return this.issuanceNumber;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="request_date"
     */
    public DateTime getRequestDate() {
	return this.requestDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="issue_date"
     */
    public DateTime getIssueDate() {
	return this.issueDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="join_date"
     */
    public DateTime getJoinDate() {
	return this.joinDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="expiry_date"
     */
    public DateTime getExpiryDate() {
	return this.expiryDate;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="card_name"
     */
    public String getCardName() {
	return this.cardName;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="ref_code"
     */
    public String getReasonCode() {
	return this.reasonCode;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="ref_type"
     */
    public String getReasonType() {
	return this.reasonType;
    }

    public ReferenceDataVO getReason() throws RemoteException {
	ReferenceDataVO refDataVO = null;
	if (this.reasonType != null && this.reasonCode != null) {
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	    refDataVO = commonMgr.getReferenceData(this.reasonType, this.reasonCode);
	}
	return refDataVO;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="product_code"
     */
    public String getProductCode() {
	return this.productCode;
    }

    public ProductVO getProduct() throws RemoteException {
	ProductVO prodVO = null;
	if (this.productCode != null && this.product == null) {
	    MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();
	    this.product = refMgr.getProduct(this.productCode);
	}
	return prodVO;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="tier_code"
     */
    public String getTierCode() {
	return tierCode;
    }

    public MembershipTier getTier() throws RemoteException 
    {
	MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	MembershipTier tier = null;
	
	if (this.getTierCode() != null) 
	{
	    tier = membershipMgr.getMembershipTier(this.getTierCode());
	}
	else
	{
	    tier = membershipMgr.getMembershipTierByJoinDate ( this.joinDate );
	}
	
	return tier;
    }

    /**
     * @hibernate.property
     * @hibernate.column name="user_id"
     */
    public String getUserId() {
	return userId;
    }

    /**
     * @hibernate.property
     */
    public String getRego() {
	return rego;
    }

    public void setRego(String rego) {
	this.rego = rego;
    }

    /**
     * Returns true if the card has been issued. I.e. an issued date has been
     * set.
     * 
     * @return The issued value
     */
    public boolean isIssued() {
	return this.issueDate != null;
    }

    /**
     * Set the membership card number. The card number is a concatenation of:
     * IIN, issuance number, product identifier number, membership number padded
     * out to 7 characters with leading zeros, and check digit. NOTE: The
     * productVO must be set first.
     * 
     * @param IIN
     *            The new cardNumber value
     * @param membershipNumber
     *            The new cardNumber value
     * @exception RemoteException
     *                Description of the Exception
     */
    public void setCardNumber(String IIN, String membershipNumber) throws RemoteException {
	StringBuffer cardNum = new StringBuffer(IIN);
	cardNum.append(this.issuanceNumber);
	cardNum.append(this.product.getProductIdentifier());

	/**
	 * Change the product identifier based on membership tier.
	 */

	// the membership # must be padded out to 7 chars
	for (int i = membershipNumber.length(); i < 7; i++) {
	    cardNum.append("0");
	}

	cardNum.append(membershipNumber);
	cardNum.append(CheckDigit.getCheckDigit(cardNum.toString(), false));
	LogUtil.debug(this.getClass(), "cardNum = " + cardNum);
	this.cardNumber = cardNum.toString();
    }

    public void setExpiryDate(DateTime expiryDate) throws RemoteException {
	checkReadOnly();
	this.expiryDate = expiryDate;
    }

    public void setIssueDate(DateTime issueDate) throws RemoteException {
	checkReadOnly();
	this.issueDate = issueDate;
    }

    public void setJoinDate(DateTime jDate) throws RemoteException {
	checkReadOnly();
	this.joinDate = jDate;
    }

    public void setCardName(String memName) throws RemoteException {
	checkReadOnly();
	this.cardName = memName;
    }

    public void setProductCode(String prodCode) throws RemoteException {
	checkReadOnly();
	this.productCode = prodCode;
	this.product = null;
    }

    public void setProduct(ProductVO prodVO) throws RemoteException {
	checkReadOnly();
	this.productCode = prodVO.getProductCode();
	this.product = prodVO;
    }

    public void setTierCode(String tierCode) {
	this.tierCode = tierCode;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public void setRequestDate(DateTime requestDate) {
	this.requestDate = requestDate;
    }

    public void setIssuanceNumber(int issuanceNumber) {
	this.issuanceNumber = issuanceNumber;
    }

    public void setCardID(Integer cardID) {
	this.cardID = cardID;
    }

    public void setReasonType(String reasonType) {
	this.reasonType = reasonType;
    }

    public void setReasonCode(String reasonCode) {
	this.reasonCode = reasonCode;
    }

    public void setMembershipID(Integer membershipID) {
	this.membershipID = membershipID;
    }

    public void setCardNumber(String cardNumber) {
	this.cardNumber = cardNumber;
    }

    /**
     * Return the contents of the object as a string. Ouput is formatted as XML
     * unless an error occured.
     * 
     * @return Description of the Return Value
     */
    public String toXML() throws SystemException {
	return XMLHelper.toXML(null, this);
	// String out = null;
	// try
	// {
	// StringWriter sw = new StringWriter();
	// XMLClassWriter xmlCW = new XMLClassWriter(sw,
	// this.WRITABLE_CLASSNAME, null);
	// write(xmlCW);
	// sw.close();
	// out = sw.toString();
	// }
	// catch(CreateException ce)
	// {
	// out = ce.getMessage();
	// }
	// catch(RemoteException re)
	// {
	// out = re.getMessage();
	// }
	// catch(IOException ioe)
	// {
	// out = ioe.getMessage();
	// }
	// return out;
    }

    /**
     * Writable interface methods ***********************
     * 
     * @param cw
     *            Description of the Parameter
     * @exception RemoteException
     *                Description of the Exception
     */

    public String getWritableClassName() {
	return this.WRITABLE_CLASSNAME;
    }

    public void write(ClassWriter cw) throws RemoteException {
	cw.startClass(WRITABLE_CLASSNAME);
	cw.writeAttribute("cardID", this.cardID);
	cw.writeAttribute("cardName", this.cardName);
	cw.writeAttribute("cardNumber", this.cardNumber);
	cw.writeAttribute("expiryDate", this.expiryDate);
	cw.writeAttribute("issuanceNumber", this.issuanceNumber);
	cw.writeAttribute("issueDate", this.issueDate);
	cw.writeAttribute("joinDate", this.joinDate);
	cw.writeAttribute("membershipID", this.membershipID);
	cw.writeAttribute("productCode", this.productCode);
	cw.writeAttribute("reasonCode", this.reasonCode);
	cw.writeAttribute("reasonType", this.reasonType);
	cw.writeAttribute("requestDate", this.requestDate);
	cw.writeAttribute("rego", this.rego);
	cw.endClass();
    }

    /**
     * Return a copy of the memberships card as a value object.
     * 
     * @return Description of the Return Value
     * @exception RemoteException
     *                Description of the Exception
     */
    public MembershipCardVO copy() throws RemoteException {
	MembershipCardVO memCardVO = new MembershipCardVO(this.cardID, this.cardNumber, this.expiryDate, this.issuanceNumber, this.issueDate, this.joinDate, this.cardName, this.membershipID, this.productCode, this.reasonType, this.reasonCode, this.requestDate, this.tierCode, this.rego);
	return memCardVO;
    }

    @Override
    public int hashCode() {
	return this.getCardID().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
	LogUtil.debug(this.getClass(), "equals " + this.getCardID());
	if (obj instanceof MembershipCardVO) {
	    MembershipCardVO card = (MembershipCardVO) obj;
	    if (card != null && this != null && this.getCardID() != null && card.getCardID() != null && this.getCardID().equals(card.getCardID())) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    return false;
	}
    }

    @Override
    public String toString() {
	return "MembershipCardVO [cardID=" + cardID + ", cardName=" + cardName + ", cardNumber=" + cardNumber + ", expiryDate=" + expiryDate + ", issuanceNumber=" + issuanceNumber + ", issueDate=" + issueDate + ", joinDate=" + joinDate + ", membershipID=" + membershipID + ", product=" + product + ", productCode=" + productCode + ", reasonCode=" + reasonCode + ", reasonType=" + reasonType + ", requestDate=" + requestDate + ", tierCode=" + tierCode + ", userId=" + userId + ", rego=" + rego + "]";
    }

    /**
     * The default sort order is by request date.
     * 
     * @param obj
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int compareTo(Object obj) {
	MembershipCardVO that = (MembershipCardVO) obj;
	if (this.requestDate != null && that != null && that.getRequestDate() != null) {
	    return this.requestDate.compareTo(that.getRequestDate());
	} else {
	    return 0;
	}
    }
}
