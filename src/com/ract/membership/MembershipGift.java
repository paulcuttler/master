package com.ract.membership;

import java.rmi.RemoteException;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.Gift;
import com.ract.util.DateTime;

/**
 * <p>
 * Represent the association between a membership and a gift
 * </p>
 * 
 * @hibernate.class table="mem_membership_gift" lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class MembershipGift implements java.io.Serializable {

    /**
     * @hibernate.property column="create_date"
     * @return DateTime
     */
    public DateTime getCreateDate() {
	return createDate;
    }

    @Override
    public String toString() {
	return "MembershipGift [createDate=" + createDate + ", membershipGiftPK=" + membershipGiftPK + ", membershipNumber=" + membershipNumber + ", notes=" + notes + ", salesBranchCode=" + salesBranchCode + ", userId=" + userId + "]";
    }

    public void setCreateDate(DateTime createDate) {
	this.createDate = createDate;
    }

    private MembershipGiftPK membershipGiftPK;

    private String notes;

    private String membershipNumber;

    private String userId;

    private String salesBranchCode;

    private DateTime createDate;

    public void setMembershipGiftPK(MembershipGiftPK membershipGiftPK) {
	this.membershipGiftPK = membershipGiftPK;
    }

    public void setNotes(String Notes) {
	this.notes = Notes;
    }

    public void setMembershipNumber(String membershipNumber) {
	this.membershipNumber = membershipNumber;
    }

    public void setSalesBranchCode(String salesBranchCode) {
	this.salesBranchCode = salesBranchCode;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    /**
     * @hibernate.composite-id unsaved-value="none"
     */
    public MembershipGiftPK getMembershipGiftPK() {
	return membershipGiftPK;
    }

    /**
     * @hibernate.property column="notes"
     * @return String
     */
    public String getNotes() {
	return notes;
    }

    /**
     * @hibernate.property column="membership_number"
     * @return DateTime
     */
    public String getMembershipNumber() {
	return membershipNumber;
    }

    /**
     * @hibernate.property column="sales_branch_code"
     * @return DateTime
     */
    public String getSalesBranchCode() {
	return salesBranchCode;
    }

    /**
     * @hibernate.property column="user_id"
     * @return DateTime
     */
    public String getUserId() {
	return userId;
    }

    /**
     * 
     * Membership gift details
     * 
     * id/membership id create date gift type - cooler bag (maintained in the
     * system) tier code - if applicable notes
     * 
     * 
     * 
     */
    public MembershipGift() {
    }

    public Gift getGift() throws RemoteException {
	CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
	return commonMgr.getGift(this.getMembershipGiftPK().getGiftCode());
    }
}
