package com.ract.membership;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientSubscription;
import com.ract.client.ClientSubscriptionPK;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.common.Publication;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.StringUtil;

public class ToggleEmailRenewalsAction extends ActionSupport implements SessionAware, ServletRequestAware {

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    private Map session;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    @InjectEJB(name = "ClientMgrBean")
    private ClientMgrLocal clientMgr;

    private Integer membershipId;

    public Integer getMembershipId() {
	return membershipId;
    }

    public void setMembershipId(Integer membershipId) {
	this.membershipId = membershipId;
    }

    /**
     * @return
     */
    public String execute() {

	UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);
	MembershipVO membership = null;

	try {

	    membership = membershipMgr.getMembership(membershipId);
	    servletRequest.setAttribute("membershipVO", membership);

	    ClientVO client = membership.getClient();
	    String emailAddress = client.getEmailAddress();
	    if (emailAddress == null || emailAddress.trim().length() == 0) {
		throw new GenericException("There is currently no email address for client " + client.getClientNumber());
	    } else {
		StringUtil.validateEmail(emailAddress, false);
	    }

	    ClientSubscription sub = clientMgr.getActiveClientSubscription(membership.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL);
	    if (sub != null) {
		// remove subscription
		clientMgr.deleteClientSubscription(sub.getClientSubscriptionPK());
	    } else {
		ClientSubscriptionPK pk = new ClientSubscriptionPK(membership.getClientNumber(), Publication.ROADSIDE_EMAIL_RENEWAL, ClientSubscription.STATUS_ACTIVE, new DateTime());
		ClientSubscription clientSubscription = new ClientSubscription();
		clientSubscription.setClientSubscriptionPK(pk);
		clientSubscription.setUserId(userSession.getUserId());

		// create subscription
		clientMgr.createClientSubscription(clientSubscription);
	    }

	} catch (Exception e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	// TODO Auto-generated method stub
	return SUCCESS;
    }
}