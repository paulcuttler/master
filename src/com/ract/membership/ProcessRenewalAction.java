package com.ract.membership;

import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.CommonMgrLocal;
import com.ract.membership.reporting.MembershipReportHelper;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.security.ui.LoginUIConstants;
import com.ract.user.User;
import com.ract.user.UserSession;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class ProcessRenewalAction extends ActionSupport implements SessionAware, ServletRequestAware {

    private HttpServletRequest servletRequest;

    public HttpServletRequest getServletRequest() {
	return servletRequest;
    }

    public void setServletRequest(HttpServletRequest request) {
	this.servletRequest = request;
    }

    private Map session;

    public Map getSession() {
	return session;
    }

    public void setSession(Map session) {
	this.session = session;
    }

    private MembershipVO membershipVO;

    public MembershipVO getMembershipVO() {
	return membershipVO;
    }

    public void setMembershipVO(MembershipVO membershipVO) {
	this.membershipVO = membershipVO;
    }

    private String noticeAction;

    private Integer membershipId;

    private String message;

    @InjectEJB(name = "MembershipMgrBean")
    private MembershipMgrLocal membershipMgr;

    @InjectEJB(name = "MembershipRenewalMgrBean")
    private MembershipRenewalMgrLocal membershipRenewalMgr;

    @InjectEJB(name = "CommonMgrBean")
    private CommonMgrLocal commonMgr;

    /**
     * @return
     */
    public String execute() {
	UserSession userSession = (UserSession) session.get(LoginUIConstants.USER_SESSION);

	try {
	    membershipVO = membershipMgr.getMembership(membershipId);

	    MembershipDocument document = membershipVO.getCurrentRenewalDocument();

	    DateTime feeEffectiveDate = MembershipHelper.calculateRenewalEffectiveDate(membershipVO.getExpiryDate(), null);

	    if (noticeAction.equals(PrintRenewalAction.ACTION_CREATE_RENEWAL_DOCUMENT)) {
		PaymentTransactionMgrLocal paymentTransactionMgrLocal = PaymentEJBHelper.getPaymentTransactionMgrLocal();
		// cancel existing
		try {
		    membershipMgr.removeRenewalDocument(document.getDocumentId(), MembershipMgr.MODE_CANCEL, paymentTransactionMgrLocal, false);
		    paymentTransactionMgrLocal.commit();
		} catch (RenewalNoticeException e) {
		    paymentTransactionMgrLocal.rollback();
		    addActionError(e.getMessage());
		    return ERROR;
		}
		message = "A new renewal document has been generated and successfully printed.";
	    } else if (noticeAction.equals(PrintRenewalAction.ACTION_PRINT_EXISTING_RENEWAL_DOCUMENT)) {
		message = "Existing renewal document has been successfully printed.";
	    }

	    TransactionGroup transactionGroup = TransactionGroup.getRenewalTransactionGroup(membershipVO, userSession.getUser(), feeEffectiveDate);

	    User user = userSession.getUser();
	    String printGroup = user.getPrinterGroup();
	    LogUtil.log(this.getClass(), "ProcessRenewalAction 1");
	    Properties renewalProperties = membershipRenewalMgr.produceIndividualRenewalNotice(membershipVO, transactionGroup, user, null);
	    LogUtil.log(this.getClass(), "ProcessRenewalAction 2");
	    MembershipReportHelper.printMembershipRenewal(user, printGroup, renewalProperties, transactionGroup);

	    servletRequest.setAttribute("membershipVO", membershipVO);

	} catch (Exception e) {
	    addActionError(e.getMessage());
	    return ERROR;
	}

	// TODO Auto-generated method stub
	return SUCCESS;
    }

    public String getNoticeAction() {
	return noticeAction;
    }

    public void setNoticeAction(String noticeAction) {
	this.noticeAction = noticeAction;
    }

    public Integer getMembershipId() {
	return membershipId;
    }

    public void setMembershipId(Integer membershipId) {
	this.membershipId = membershipId;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

}