package com.ract.membership;

import java.rmi.RemoteException;

import javax.ejb.Remote;

@Remote
public interface MembershipIDMgr {
    public int getNextDocumentID() throws RemoteException;

    public int getNextGroupID() throws RemoteException;

    public int getNextMembershipID() throws RemoteException;

    public int getNextQuoteNumber() throws RemoteException;

    public int getNextTransactionID() throws RemoteException;

    public int getNextTransactionGroupID() throws RemoteException;

    public void reset() throws RemoteException;
}
