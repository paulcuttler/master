package com.ract.membership.notifier;

import java.rmi.RemoteException;
import java.util.Collection;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.transaction.TransactionAdapter;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipVO;
import com.ract.membership.TransactionMgr;
import com.ract.payment.PaymentMgr;
import com.ract.payment.electronicpayment.ElectronicPaymentHelper;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

/**
 * <p>
 * Payment adapter to handle the notification of a payment specifically for
 * membership
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

public class MembershipPaymentAdapter extends TransactionAdapterType implements TransactionAdapter {

    // private final String SUBJECT_FAILURE = "Payment Notification Failure";

    private final String USER_ID_IVR = "ivr";
    private final String USER_ID_BPAY = "bpay";
    private final String USER_ID_WEB = "web";

    private SourceSystem paymentSystem;
    private String reference;
    private Integer sequenceNumber;
    private double amountPaid;
    private double amountOutstanding;
    private String description;
    private String createdBy;
    private String receiptedBy;
    private SourceSystem sourceSystem;

    /**
     * Setup the data for the commmit.
     * 
     * @param paymentSystem
     *            SourceSystem
     * @param reference
     *            String
     * @param sequenceNumber
     *            Integer
     * @param amountPaid
     *            double
     * @param amountOutstanding
     *            double
     * @param description
     *            String
     * @param createdBy
     *            String
     * @param receiptedBy
     *            String
     * @param sourceSystem
     *            SourceSystem
     * @throws RemoteException
     */
    public MembershipPaymentAdapter(SourceSystem paymentSystem, String reference, String clientNumber, Integer sequenceNumber, double amountPaid, double amountOutstanding, String description, String createdBy, String receiptedBy, SourceSystem sourceSystem) throws RemoteException {

	LogUtil.debug(this.getClass(), "MembershipPaymentAdapter start");

	LogUtil.debug(this.getClass(), "paymentSystem=" + paymentSystem);
	LogUtil.debug(this.getClass(), "reference=" + reference);
	LogUtil.debug(this.getClass(), "clientNumber=" + clientNumber);
	LogUtil.debug(this.getClass(), "sequenceNumber=" + sequenceNumber);
	LogUtil.debug(this.getClass(), "amountPaid=" + amountPaid);

	this.paymentSystem = paymentSystem;
	this.reference = reference;
	this.clientNumber = clientNumber;
	this.sequenceNumber = sequenceNumber;
	this.amountPaid = amountPaid;
	this.amountOutstanding = amountOutstanding;
	this.description = description;
	this.createdBy = createdBy;
	this.receiptedBy = receiptedBy;
	this.sourceSystem = sourceSystem;

	constructMessage();

	LogUtil.debug(this.getClass(), "message=" + message);

	notifyMembershipPayment();
	LogUtil.debug(this.getClass(), "MembershipPaymentAdapter end");
    }

    /**
     * Notify the payment
     */
    private void notifyMembershipPayment() throws RemoteException {
	LogUtil.debug(this.getClass(), "notifyMembershipPayment start");

	if (ElectronicPaymentHelper.isElectronicPaymentUser(receiptedBy) && isMembershipDirectDebit(clientNumber)) {
	    throw new RemoteException("Membership for client " + clientNumber + " has a Direct Debit ");
	}

	// Make sure that the payment notification is coming from the correct
	// payment system
	String receiptingSystem = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_PAYMENT, SystemParameterVO.RECEIPTING_SYSTEM + sourceSystem);

	LogUtil.debug(this.getClass(), "receiptingSystem=" + receiptingSystem);
	LogUtil.debug(this.getClass(), "paymentSystem=" + paymentSystem);

	if (!paymentSystem.getAbbreviation().equals(receiptingSystem)) {
	    throw new RemoteException("The current receipting system being used is '" + receiptingSystem + "' and not '" + paymentSystem + "'. Payment notifications can only be accepted by the current receipting system.");
	}
	if (amountPaid <= 0) {
	    throw new RemoteException("Amount paid is less than zero when notifying payment in paymentMgr");
	}

	if (amountOutstanding < 0) // the amount outstanding according to the
				   // receipting system
	{
	    throw new RemoteException("Amount outstanding is less than zero when notifying payment in paymentMgr");
	}

	// Determine if the payment notification was for a pending fee payment
	// or a payable item payment

	// TODO I'm not sure this is foolproof logic
	boolean electronicPayment = receiptedBy.equalsIgnoreCase(USER_ID_WEB) || receiptedBy.equalsIgnoreCase(USER_ID_BPAY) || receiptedBy.equalsIgnoreCase(USER_ID_IVR);

	boolean paymentOfPendingFee = PaymentMgr.MEM_AUTO_RENEWAL.equals(description) || PaymentMgr.MEM_MANUAL_RENEWAL.equals(description);

	paymentOfPendingFee = paymentOfPendingFee || electronicPayment;

	LogUtil.debug(this.getClass(), "electronicPayment=" + electronicPayment);
	LogUtil.debug(this.getClass(), "paymentOfPendingFee=" + paymentOfPendingFee);

	LogUtil.debug(this.getClass(), "reference=" + reference);
	LogUtil.debug(this.getClass(), "sequenceNumber=" + sequenceNumber);
	LogUtil.debug(this.getClass(), "amountPaid=" + amountPaid);
	LogUtil.debug(this.getClass(), "amountOutstanding=" + amountOutstanding);
	LogUtil.debug(this.getClass(), "description=" + description);
	LogUtil.debug(this.getClass(), "receiptedBy=" + receiptedBy);

	if (paymentOfPendingFee) {
	    transactionMgr = MembershipEJBHelper.getTransactionMgr();
	    transactionMgr.notifyPendingFeePaid(reference, // in this case the
							   // pendingFeeId = IVR
							   // number
		    sequenceNumber, // in this case the mem_document.document_id
		    amountPaid, amountOutstanding, description, receiptedBy, null);

	} else {
	    transactionMgr = MembershipEJBHelper.getTransactionMgr();
	    transactionMgr.notifyPayableFeePaid(reference, sequenceNumber, amountPaid, amountOutstanding, description, receiptedBy, null);
	}
	LogUtil.debug(this.getClass(), "notifyMembershipPayment end");
    }

    private TransactionMgr transactionMgr;

    private StringBuffer message;

    private String clientNumber;

    private void constructMessage() {
	// create the message text for failure notification
	message = new StringBuffer();
	message.append("PAYMENT NOTIFICATION:" + FileUtil.NEW_LINE);
	message.append(formatMessageLine("Payment System", this.paymentSystem));
	message.append(formatMessageLine("Reference Number", this.reference));
	message.append(formatMessageLine("Client Number", this.clientNumber));
	message.append(formatMessageLine("Sequence Number", this.sequenceNumber));
	message.append(formatMessageLine("Amount Paid", String.valueOf(this.amountPaid)));
	message.append(formatMessageLine("Amount Outstanding", String.valueOf(this.amountOutstanding)));
	message.append(formatMessageLine("Description", this.description));
	message.append(formatMessageLine("Created By", this.createdBy));
	message.append(formatMessageLine("Receipted by", this.receiptedBy));
	message.append(formatMessageLine("Source System", this.sourceSystem));
    }

    private String formatMessageLine(String label, Object value) {
	String messageLine = StringUtil.rightPadString(label, 20) + ": " + (value == null ? "" : value.toString()) + FileUtil.NEW_LINE;
	return messageLine;
    }

    public String getMessage() {
	return message.toString();
    }

    public void commit() throws SystemException {
	LogUtil.debug(this.getClass(), "commit start");
	try {
	    if (transactionMgr != null) {
		transactionMgr.commit();
	    } else {
		LogUtil.debug(this.getClass(), "transactionMgr is null!");
	    }
	} catch (RemoteException e) {
	    throw new SystemException(e);
	}
	LogUtil.debug(this.getClass(), "commit end");
    }

    public void rollback() throws SystemException {
	LogUtil.debug(this.getClass(), "rollback start");
	try {
	    if (transactionMgr != null) {
		transactionMgr.rollback();
	    }
	} catch (RemoteException e) {
	    throw new SystemException(e);
	}
	LogUtil.debug(this.getClass(), "rollback end");
    }

    public void release() {
	// nothing to release
	LogUtil.debug(this.getClass(), "release");
    }

    public String getSystemName() {
	return SourceSystem.MEMBERSHIP.getAbbreviation();
    }

    private boolean isMembershipDirectDebit(String clientId) throws RemoteException {
	LogUtil.debug(this.getClass(), "isMembershipDirectDebit start");
	Integer ddId = null;
	boolean isDD = false;

	Collection mems = MembershipEJBHelper.getMembershipMgrLocal().findMembershipByClientNumber(new Integer(clientId));
	LogUtil.debug(this.getClass(), "mems=" + mems);

	if (mems.size() > 1) {
	    throw new RemoteException("More than 1 membership for " + clientId);
	}

	try {
	    ddId = ((MembershipVO) mems.iterator().next()).getDirectDebitAuthorityID();
	} catch (Exception ex) {
	    ddId = new Integer(0);
	}

	if ((ddId != null) && ddId.intValue() > 0) {
	    isDD = true;
	}
	LogUtil.debug(this.getClass(), "isDD=" + isDD);
	LogUtil.debug(this.getClass(), "isMembershipDirectDebit end");
	return isDD;
    }
}
