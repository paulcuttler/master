package com.ract.membership.notifier;

/**
 * Title:        NotificationEventSubscriberMembershipImpl
 * Description:  Provides a concrete implementation of a NotificationEventSubscriber
 *               which is required to be able to subscribe to notification events
 *               in the notification manager.
 *               This class does some event filtering and then passes on the
 *               notification event to the MembershipMgr. The MembershipMgr
 *               class is an interface cannot directly implement the
 *               NotificationEventSubscriber interface.
 * Copyright:    Copyright (c) 2002
 * Company:      RACT
 * @author       T. Bakker
 * @version 1.0
 */
import java.io.Serializable;
import java.rmi.RemoteException;

import com.ract.common.SourceSystem;
import com.ract.common.notifier.NotificationEvent;
import com.ract.common.notifier.NotificationEventSubscriber;
import com.ract.membership.MembershipEJBHelper;

public class NotificationEventSubscriberMembershipImpl implements NotificationEventSubscriber, Serializable {
    public NotificationEventSubscriberMembershipImpl() {
    }

    public void processNotificationEvent(NotificationEvent event) throws RemoteException {
	if (event != null && event.eventEquals(NotificationEvent.EVENT_CLIENT_CHANGE)) {
	    MembershipEJBHelper.getMembershipMgr().processNotificationEvent(event);
	}
    }

    public String getSubscriberName() {
	return SourceSystem.MEMBERSHIP.getAbbreviation();
    }
}