package com.ract.membership.notifier;

import com.ract.common.notifier.NotificationEvent;
import com.ract.membership.MembershipVO;

/**
 * Title: MembershipChangeNotificationEvent Description: Is a concrete
 * implementation of a NotificationEvent which is sent to the notification
 * manager any time a membership is changed. Other systems interested in changes
 * to membership data subscribe to this notification event and respond as they
 * need to.
 * 
 * @author T. Bakker
 * @created 27 February 2003
 * @version 1.0
 */

public class MembershipChangeNotificationEvent extends NotificationEvent {
    public final static String ACTION_CREATE = "Create";

    public final static String ACTION_UPDATE = "Update";

    public final static String ACTION_UPDATE_CLIENT = "UpdateClient";

    public final static String ACTION_UPDATE_GROUP = "UpdateGroup";

    public final static String ACTION_REMOVE = "Remove";

    private String action = null;

    private Integer membershipID = null;

    private MembershipVO membershipVO = null;

    /**
     * Constructor for the MembershipChangeNotificationEvent object
     * 
     * @param action
     *            Description of the Parameter
     * @param memID
     *            Description of the Parameter
     */
    public MembershipChangeNotificationEvent(String action, Integer memID) {
	this.action = action;
	this.membershipID = memID;
	setEventName(NotificationEvent.EVENT_MEMBERSHIP_CHANGE);
    }

    /**
     * Construct a membership change notification event using the new membership
     * data. This saves the subscriber from having to look it up again.
     * 
     * @param action
     *            Description of the Parameter
     * @param memVO
     *            Description of the Parameter
     */
    public MembershipChangeNotificationEvent(String action, MembershipVO memVO) {
	this.action = action;
	this.membershipVO = memVO;
	this.membershipID = memVO.getMembershipID();
	setEventName(NotificationEvent.EVENT_MEMBERSHIP_CHANGE);
    }

    /**
     * Gets the membershipID attribute of the MembershipChangeNotificationEvent
     * object
     * 
     * @return The membershipID value
     */
    public Integer getMembershipID() {
	return this.membershipID;
    }

    /**
     * Gets the membershipVO attribute of the MembershipChangeNotificationEvent
     * object
     * 
     * @return The membershipVO value
     */
    public MembershipVO getMembershipVO() {
	return this.membershipVO;
    }

    /**
     * Gets the action attribute of the MembershipChangeNotificationEvent object
     * 
     * @return The action value
     */
    public String getAction() {
	return this.action;
    }

    public boolean isCreateAction() {
	return this.ACTION_CREATE.equals(this.action);
    }

    public boolean isUpdateAction() {
	return this.ACTION_UPDATE.equals(this.action);
    }

    public boolean isUpdateClientAction() {
	return this.ACTION_UPDATE_CLIENT.equals(this.action);
    }

    public boolean isRemoveAction() {
	return this.ACTION_REMOVE.equals(this.action);
    }

    public String toString() {
	StringBuffer str = new StringBuffer();
	str.append(getEventName());
	str.append(", action=");
	str.append(this.action);
	str.append(", membershipID=");
	str.append(this.membershipID);
	return str.toString();
    }

}
