package com.ract.membership;

import com.ract.common.GenericException;

/**
 * 
 * @author Terry Bakker
 * @version 1.0
 */

public class GroupDefinedException extends GenericException {

    public GroupDefinedException() {
    }
}
