package com.ract.membership;

import com.ract.common.GenericException;

public class UndoAccessMemberException extends GenericException {
    public UndoAccessMemberException(String message) {
	super(message);
    }
}
