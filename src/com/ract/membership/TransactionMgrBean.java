package com.ract.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;

import com.ract.client.ClientVO;
import com.ract.common.ExceptionHelper;
import com.ract.common.SystemException;
import com.ract.common.ValidationException;
import com.ract.common.notifier.NotificationMgrLocal;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.billpay.PendingFee;
import com.ract.user.User;
import com.ract.user.UserEJBHelper;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;

/**
 * BMT to allow rollback and commit independent of CMT transaction. This is to
 * 
 * @author hollidayj
 * 
 */
@Stateful
@TransactionManagement(TransactionManagementType.BEAN)
@Remote({ TransactionMgr.class })
@Local({ TransactionMgrLocal.class })
public class TransactionMgrBean {
    public boolean isPendingTransaction() {
	return pendingTransaction;
    }

    public void setPendingTransaction(boolean pendingTransaction) {
	this.pendingTransaction = pendingTransaction;
    }

    @EJB
    private MembershipMgrLocal membershipMgrLocal;

    @EJB
    private MembershipTransactionMgrLocal membershipTransactionMgrLocal;

    @Resource
    private UserTransaction userTransaction;

    private TransactionHelper transactionHelper = new TransactionHelper();

    /**
     * Handles payment of pending fee through the receipting system.
     * 
     * Historically triggered by Progress memoipst when an OCR payment is
     * accepted but it is now triggered by a Receptor receipt.
     * 
     * Receipt has already been created. Now create the transaction group and
     * update the pending fee record.
     * 
     * @param String
     *            pendingFeeId The id of the corresponding pending fee record
     * @param double amountPaid The actual amount receipted
     * @param double amountOutstanding The
     */
    public void notifyPendingFeePaid(String pendingFeeId, Integer sequenceNo, double amountPaid, double amountOutstanding, String description, String logonId, String receiptNo) throws RemoteException {

	LogUtil.log(this.getClass(), "notifyPendingFeePaid start");

	setPendingTransaction(true);

	LogUtil.log(this.getClass(), "pendingFeeId=" + pendingFeeId);
	LogUtil.log(this.getClass(), "sequenceNo=" + sequenceNo);
	LogUtil.log(this.getClass(), "amountPaid=" + amountPaid);
	LogUtil.log(this.getClass(), "amountOutstanding=" + amountOutstanding);
	LogUtil.log(this.getClass(), "description=" + description);
	LogUtil.log(this.getClass(), "logonId=" + logonId);
	LogUtil.log(this.getClass(), "receiptNo=" + receiptNo);

	// Get the PendingFee entity bean remote interface
	PendingFee pendingFee = null;
	try {
	    pendingFee = PaymentEJBHelper.getPaymentMgrLocal().getPendingFee(pendingFeeId);
	} catch (PaymentException bpe) {
	    throw new SystemException(bpe);
	}
	LogUtil.log(this.getClass(), "pendingFee=" + pendingFee);
	if (pendingFee == null) {
	    throw new SystemException("Failed to find pending fee with ID = " + pendingFeeId);
	}

	Integer membershipID = pendingFee.getSourceSystemReferenceNo();
	LogUtil.debug(this.getClass(), "membershipID=" + membershipID);
	// Get the membership
	MembershipVO contextMemVO = membershipMgrLocal.getMembership(membershipID);
	LogUtil.debug(this.getClass(), "contextMemVO=" + contextMemVO);

	PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();

	// check that not already receipted.
	ClientVO client = contextMemVO.getClient();
	LogUtil.debug(this.getClass(), "client=" + client);
	Integer clientNumber = client.getClientNumber();
	Collection payableItems = paymentMgrLocal.getPayableItemsByClientReceipt(clientNumber, sequenceNo.toString());
	if (payableItems.size() > 0) {
	    throw new SystemException("At least one payable item already exists for client '" + client.getClientNumber() + "' with sequence number '" + sequenceNo.toString() + "'.");
	}

	if (amountPaid != pendingFee.getAmount()) {
	    StringBuffer msg = new StringBuffer();
	    msg.append("Payment Notification Failure");
	    msg.append("\nAmount paid in the notification is ").append(amountPaid);
	    msg.append("\nwhereas the amount to be paid from the pending fee is ").append(pendingFee.getAmount());
	    msg.append("\tpendingFeeID: ").append(pendingFeeId);
	    msg.append("\tamountPaid: ").append(amountPaid);
	    msg.append("\tamountOutstanding: ").append(amountOutstanding);
	    msg.append("\tdescription: ").append(description);
	    msg.append("\tlogonId: ").append(logonId);
	    msg.append("\tsourceSystem: MEM");
	    throw new RemoteException(msg.toString());
	}

	// Generate a renewal transaction using the fee effective date from the
	// pending fee that was paid.
	LogUtil.debug(this.getClass(), "logonId=" + logonId);

	User user = UserEJBHelper.getUserMgr().getUser(logonId);
	LogUtil.debug(this.getClass(), "user=" + user);

	TransactionGroup transGroup = TransactionGroup.getRenewalTransactionGroup(contextMemVO, user, pendingFee.getFeeEffectiveDate());

	double amountPayable = NumberUtil.roundDouble(transGroup.getAmountPayable(), 2);

	LogUtil.debug(this.getClass(), "amountPayable=" + amountPayable);

	// set the pendingFeeId in the MembershipTransaction for later use. This
	// is not written to the database.
	transGroup.getMembershipTransactionForPA().setPendingFeeID(pendingFeeId);

	ArrayList completedTransactionList = null;
	try {
	    LogUtil.debug(this.getClass(), "userTransaction begin");
	    // start transaction
	    userTransaction.begin();

	    // set pending fee date paid
	    pendingFee.setDatePaid(transGroup.getTransactionDate());
	    paymentTransactionMgrLocal.updatePendingFee(pendingFee);

	    LogUtil.debug(this.getClass(), "updatePendingFee");

	    // pending fee is now set in billpaymgr

	    completedTransactionList = transactionHelper.processRenewalTransaction(membershipMgrLocal, transGroup, notificationMgrLocal); // ,"Receipt");

	    PayableItemVO payableItem = transactionHelper.processRenewalAmountPayable(transGroup, amountPaid, 0, sequenceNo, paymentTransactionMgrLocal, receiptNo); // receiptingDiscount

	    MembershipTransactionVO memTransVO = null;

	    Iterator completedListIterator = completedTransactionList.iterator();
	    while (completedListIterator.hasNext()) {
		memTransVO = (MembershipTransactionVO) completedListIterator.next();
		transactionHelper.updateMembershipTransactionMembershipXML(membershipMgrLocal, memTransVO);
	    }

	    transactionHelper.setMembershipTransactions(membershipMgrLocal, transGroup, payableItem);
	} catch (Exception e) {
		e.printStackTrace();
		
	    LogUtil.fatal(this.getClass(), "Error processing transaction. " + e);

	    rollback();

	    // The transaction has been rolled back.
	    // Throw the exception back up so the UI can handle it.
	    if (e instanceof ValidationException) {
		throw (ValidationException) e;
	    } else if (e instanceof SystemException) {
		throw (SystemException) e;
	    } else if (e instanceof RemoteException) {
		throw (RemoteException) e;
	    } else {
		throw new SystemException(e.getMessage());
	    }
	}// end catch
	LogUtil.log(this.getClass(), "notifyPendingFeePaid end");
    }// end method

    /**
     * Handles payment of payable item through the receipting system.
     */
    public void notifyPayableFeePaid(String payableItemId, Integer sequenceNo, double amountPaid, double amountOutstanding, String description, String logonId, String receiptNo) throws RemoteException {

	setPendingTransaction(true);

	PaymentMgrLocal paymentMgrLocal = PaymentEJBHelper.getPaymentMgrLocal();
	PayableItemVO payableItem = null;
	try {
	    LogUtil.info(getClass(), "*** looking up payableItemId:" + payableItemId);
	    payableItem = paymentMgrLocal.getPayableItem(Integer.parseInt(payableItemId));
	    LogUtil.info(getClass(), "*** retrieved payableItem");
	} catch (Exception e) {
	    throw (RemoteException) e;
	}

	if (amountOutstanding != 0) {
	    StringBuffer msg = new StringBuffer();
	    msg.append("Payment Notification Failure");
	    // msg.append("\nAmount paid in the notification is ").append(amountPaid);
	    // msg.append("\nwhereas the amount to be paid from the payable item is ").append(payableItem.getAmountOutstanding());
	    msg.append("\tpayableItemID: ").append(payableItemId);
	    msg.append("\tamountPaid: ").append(amountPaid);
	    msg.append("\tamountOutstanding: ").append(amountOutstanding);
	    msg.append("\tdescription: ").append(description);
	    msg.append("\tlogonId: ").append(logonId);
	    msg.append("\tsourceSystem: MEM");

	    LogUtil.info(getClass(), "*** non-zero amount outstanding!: " + amountOutstanding);

	    throw new RemoteException(msg.toString());
	}
	LogUtil.info(getClass(), "*** no amount outstanding");

	try {
	    LogUtil.debug(this.getClass(), "userTransaction begin");
	    // start transaction
	    userTransaction.begin();

	    payableItem.setAmountOutstanding(amountOutstanding);
	    payableItem.setReceiptNo(receiptNo);
	    LogUtil.info(getClass(), "*** setAmountOutstanding");
	    paymentTransactionMgrLocal.updatePayableItem(payableItem);
	    LogUtil.info(getClass(), "*** updatePayableItem");
	    // paymentTransactionMgrLocal.commit();
	    // LogUtil.info(getClass(), "*** commit");

	} catch (Exception e) {
		e.printStackTrace();
		
	    LogUtil.fatal(this.getClass(), "Error processing transaction. " + e);

	    rollback();

	    if (e instanceof ValidationException) {
		throw (ValidationException) e;
	    } else if (e instanceof SystemException) {
		throw (SystemException) e;
	    } else if (e instanceof RemoteException) {
		throw (RemoteException) e;
	    } else {
		throw new SystemException(e.getMessage());
	    }
	}
    }

    public Collection processTransaction(TransactionGroup transGroup) throws RemoteException {
	LogUtil.debug(this.getClass(), "processTransaction start");
	setPendingTransaction(true);
	// start transaction
	try {
	    userTransaction.begin();
	} catch (Exception e) {
	    throw new RemoteException("Unable to begin transaction.", e);
	}
	Collection transactionList = membershipTransactionMgrLocal.processTransaction(transGroup, notificationMgrLocal, paymentTransactionMgrLocal);

	LogUtil.debug(this.getClass(), "transactionList=" + transactionList);
	LogUtil.debug(this.getClass(), "processTransaction end");
	return transactionList;
    }

    /**
     * Flags whether to commit or rollback internally or externally.
     */
    public boolean pendingTransaction = false;

    @EJB
    private PaymentTransactionMgrLocal paymentTransactionMgrLocal;

    @EJB
    private NotificationMgrLocal notificationMgrLocal;

    /**
     * Commit a pending transaction.
     */
    public void commit() {
	LogUtil.log(this.getClass(), "commit start");
	LogUtil.debug(this.getClass(), "pendingTransaction=" + pendingTransaction);
	if (pendingTransaction) {
	    try {

		if (userTransaction != null) {
		    LogUtil.log(this.getClass(), "userTransaction commit");
		    userTransaction.commit();
		}
		if (paymentTransactionMgrLocal != null) {
		    LogUtil.log(this.getClass(), "paymentTransactionMgrLocal commit");
		    // commit payment data
		    paymentTransactionMgrLocal.commit();
		}
		if (notificationMgrLocal != null) {
		    LogUtil.log(this.getClass(), "notificationMgrLocal commit");
		    // commit notifications
		    notificationMgrLocal.commit();
		}
	    } catch (Exception e) {
		LogUtil.fatal(this.getClass(), "Commit error: " + ExceptionHelper.getExceptionStackTrace(e));
		rollback();
	    }
	} else {
	    LogUtil.warn(this.getClass(), "Commit of a non pending transaction.");
	}
	LogUtil.log(this.getClass(), "commit end");
    }

    public void rollback() {
	LogUtil.log(this.getClass(), "rollback start");
	LogUtil.debug(this.getClass(), "pendingTransaction=" + pendingTransaction);
	if (pendingTransaction) {

	    if (userTransaction != null) {
		try {
		    LogUtil.log(this.getClass(), "userTransaction rollback");
		    userTransaction.rollback();
		} catch (javax.transaction.SystemException e) {
		    LogUtil.fatal(this.getClass(), "Rollback error userTransaction: " + ExceptionHelper.getExceptionStackTrace(e));
		}
	    }

	    // rollback payment data
	    try {
		if (paymentTransactionMgrLocal != null) {
		    LogUtil.log(this.getClass(), "paymentTransactionMgrLocal rollback");
		    paymentTransactionMgrLocal.rollback();
		}
	    } catch (Exception e) {
		LogUtil.fatal(this.getClass(), "Rollback error paymentTxMgr: " + ExceptionHelper.getExceptionStackTrace(e));
	    }

	    // rollback notifications
	    try {
		if (notificationMgrLocal != null) {
		    LogUtil.log(this.getClass(), "notificationMgrLocal rollback");
		    notificationMgrLocal.rollback();
		}
	    } catch (Exception e) {
		LogUtil.fatal(this.getClass(), "Rollback error notificationMgr: " + ExceptionHelper.getExceptionStackTrace(e));
	    }

	} else {
	    LogUtil.warn(this.getClass(), "Rollback of a non pending transaction.");
	}

	LogUtil.log(this.getClass(), "rollback end");
    }

}
