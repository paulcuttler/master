package com.ract.membership;

import java.io.FileWriter;
import java.util.List;
import java.util.Properties;



import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.ExceptionHelper;
import com.ract.common.SystemParameterVO;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;

/**
 * Auto cancel the following memberships: - client deceased - expired since
 * interval - expired and allow to lapse
 * 
 * Typically run daily.
 * 
 * @author jyh,gzn
 * @version 1.0
 */

public class MembershipAutoCancelRunner
{


    public static void main (String[] args ) 
    {

	String providerURL = args[0];
	String initialContext = "org.jnp.interfaces.NamingContextFactory";
	Properties environment = new Properties();
	environment.setProperty(javax.naming.Context.PROVIDER_URL,providerURL);
	environment.setProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY,initialContext);

	try 
	{
	    CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();

	    String outputDirectoryName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.MEM_LOG_DIR);
	    if (outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') 
	    {
		outputDirectoryName += "/";
	    }

	    String fileName = outputDirectoryName + DateUtil.formatYYYYMMDD(new DateTime()) + "autocancel.csv";

	    MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
	    LogUtil.log("MembershipAutoCancelRunner", "Membership Auto Cancel started.");

	    FileWriter fileWriter = new FileWriter(fileName, true); // append
	    fileWriter.write("Client number,Reason\n");

	    try 
	    {
		List<Integer> cancelledDeceasedMemberships = membershipMgr.autoCancelDeceased();
		for (Integer clientNumber : cancelledDeceasedMemberships) 
		{
		    fileWriter.write(clientNumber.toString() + ",Deceased\n");
		}
	    }
	    catch (Exception e)
	    {
		LogUtil.log("MembershipAutoCancelRunner", "An exception occurred processing auto-cancels for Deceased memberships: " + e);
	    }

	    try 
	    {
		List<Integer> cancelledExpiredMemberships = membershipMgr.autoCancelExpired();
		for (Integer clientNumber : cancelledExpiredMemberships) 
		{
		    fileWriter.write(clientNumber.toString() + ",Expired\n");
		}
	    } 
	    catch (Exception e)
	    {
		LogUtil.log("MembershipAutoCancelRunner", "An exception occurred processing auto-cancels for Expired memberships: " + e);
	    }

	    try 
	    {
		List<Integer> cancelledAllowToLapseMemberships = membershipMgr.autoCancelAllowToLapse();
		for (Integer clientNumber : cancelledAllowToLapseMemberships) 
		{
		    fileWriter.write(clientNumber.toString() + ",Allow to lapse\n");
		}
	    } 
	    catch (Exception e) 
	    {
		LogUtil.log("MembershipAutoCancelRunner", "An exception occurred processing auto-cancels for Allow to lapse memberships: " + e);
	    }

	    fileWriter.flush();
	    fileWriter.close();

	    try 
	    {
		membershipMgr.deleteGroupsOfOne();
	    } 
	    catch (Exception e) 
	    {
		LogUtil.fatal("MembershipAutoCancelRunner", "An exception occurred deleting groups of one: " + e);
	    }

	    LogUtil.log("MembershipAutoCancelRunner", "Membership Auto Cancel finished.");
	} 
	catch (Exception ex) 
	{
	    LogUtil.fatal("MembershipAutoCancelRunner", "Membership Auto Cancel failed, see following message");
	    System.err.println(ExceptionHelper.getExceptionStackTrace(ex));
	}
    }
}
