package com.ract.help;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.SimpleExpression;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.ExceptionHelper;
import com.ract.common.SequenceMgr;
import com.ract.util.LogUtil;

/**
 * <p>
 * Trial Hibernate implementation.
 * </p>
 * <p>
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */

@Stateless
@Remote({ HelpMgr.class })
public class HelpMgrBean {

    @PersistenceContext(unitName = "master")
    Session hsession;

    @Resource(mappedName = "java:/ClientDS")
    private DataSource dataSource;

    /**
     * Get the help page object.
     * 
     * @throws RemoteException
     * @return HelpPageVO
     */
    public HelpPage getHelpPage(Integer helpId) {
	LogUtil.log(this.getClass(), "helpId=" + helpId);
	HelpPage help = (HelpPage) hsession.get(HelpPage.class, helpId);
	LogUtil.debug(this.getClass(), "getHelpPage " + help.getHelpId());
	return help;
    }

    public HelpPage getHelpPageByPageReference(String helpPageReference) {
	HelpPage help = null;
	LogUtil.log(this.getClass(), "helpPageReference=" + helpPageReference);
	Criteria crit = hsession.createCriteria(HelpPage.class).add(Expression.eq("helpPageReference", helpPageReference));
	List helpList = crit.list();
	if (helpList.size() > 0) {
	    Iterator it = helpList.iterator();
	    if (it.hasNext()) {
		help = (HelpPage) it.next();
	    }
	}

	return help;
    }

    public HelpPage createHelp(HelpPage helpPage) {
	SequenceMgr sequenceMgr = CommonEJBHelper.getSequenceMgr();
	try {
	    Integer helpId = sequenceMgr.getNextID(SequenceMgr.SEQUENCE_HELP);
	    helpPage.setHelpId(helpId);
	} catch (RemoteException e) {
	    LogUtil.fatal(this.getClass(), ExceptionHelper.getExceptionStackTrace(e));
	}
	hsession.save(helpPage);
	return helpPage;
    }

    public HelpPage updateHelp(HelpPage helpPage) {
	hsession.update(helpPage);
	return helpPage;
    }

    /**
     * Search help pages by keyword and text.
     * 
     * @param keyword
     *            String
     * @throws RemoteException
     * @return Collection
     */
    public Collection searchHelpPages(String searchTerm) throws RemoteException {
	LogUtil.debug(this.getClass(), "searchTerm=" + searchTerm);
	List helpPageList = null;
	SimpleExpression helpTextExpression = null;
	Criteria crit = hsession.createCriteria(HelpPage.class);
	Collection searchResults = new ArrayList();

	if (searchTerm != null || !searchTerm.equals("")) {

	    // add word matches
	    if (searchTerm.indexOf(" ") > 0) {
		// more than one search term
		String term = null;
		String[] searchTerms = searchTerm.split(" ");
		for (int i = 0; i < searchTerms.length; i++) {
		    term = searchTerms[i];
		    LogUtil.debug(this.getClass(), "term=" + term);
		    // search per term
		    term = CommonConstants.WILDCARD + term + CommonConstants.WILDCARD;
		    LogUtil.debug(this.getClass(), "term=" + term);
		    helpTextExpression = Expression.like("helpText", term);
		    crit = crit.add(helpTextExpression);
		    LogUtil.debug(this.getClass(), "crit=" + crit.toString());
		    helpPageList = crit.list();
		    LogUtil.debug(this.getClass(), "word helpPageList " + helpPageList);
		    addUniqueSearchResults(helpPageList, searchResults);
		}
	    }

	    // add exact match
	    else {
		searchTerm = CommonConstants.WILDCARD + searchTerm + CommonConstants.WILDCARD;
		helpTextExpression = Expression.like("helpText", searchTerm);
		crit = crit.add(helpTextExpression);
		LogUtil.debug(this.getClass(), "crit=" + crit.toString());
		helpPageList = crit.list();
		LogUtil.debug(this.getClass(), "exact helpPageList " + helpPageList);
		addUniqueSearchResults(helpPageList, searchResults);
	    }

	} else {
	    addUniqueSearchResults(getAllHelpPages(), searchResults);
	}

	LogUtil.debug(this.getClass(), "searchResults=" + searchResults);
	LogUtil.debug(this.getClass(), "size=" + searchResults.size());
	return searchResults;
    }

    private void addUniqueSearchResults(Collection queryResults, Collection searchResults) {
	LogUtil.debug(this.getClass(), "queryResults=" + queryResults);
	LogUtil.debug(this.getClass(), "searchResults=" + searchResults);
	HelpPage tmp = null;
	for (Iterator<HelpPage> i = queryResults.iterator(); i.hasNext();) {
	    tmp = i.next();
	    if (!searchResults.contains(tmp)) {
		LogUtil.debug(this.getClass(), "adding " + tmp);
		searchResults.add(tmp);
	    }
	}
    }

    public Collection getAllHelpPages() {
	ArrayList helpPageList = null;
	Criteria crit = hsession.createCriteria(HelpPage.class);
	return crit.list();
    }

}
