package com.ract.help.test;

import java.rmi.RemoteException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.ract.common.CommonEJBHelper;
import com.ract.help.HelpMgr;

public class TestHelpMgr extends TestCase {

    protected void setUp() throws Exception {
	super.setUp();
	// get the membership
    }

    public void testGetHelpPageByKeyword() throws RemoteException {
	String keyword = "CANCEL";
	HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
	ArrayList helpList = (ArrayList) helpMgr.searchHelpPages(keyword);
	// System.out.println("helpList="+helpList);
	assertTrue(helpList.size() > 0);
    }

    protected void tearDown() throws Exception {
	super.tearDown();
    }

}
