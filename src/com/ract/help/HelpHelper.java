package com.ract.help;

import com.ract.common.CommonEJBHelper;
import com.ract.common.ui.CommonUIConstants;
import com.ract.util.LogUtil;

/**
 * <p>
 * Class to provide page codes to map to urls.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class HelpHelper {
    /**
     * Add help icon to page with hyperlink to help screen.
     */
    public static String displayHelpIcon(String JSPReference) {
	LogUtil.log("displayHelpIcon", "JSPReference=" + JSPReference);
	String helpIconHTML = "";

	// get url using help page code
	try {
	    HelpMgr helpMgr = CommonEJBHelper.getHelpMgr();
	    HelpPage helpPageVO = helpMgr.getHelpPageByPageReference(JSPReference);
	    LogUtil.log("displayHelpIcon", "helpPageVO=" + helpPageVO);
	    if (helpPageVO != null) {
		// add image with hyperlink to url
		helpIconHTML = "<a href=\"" + CommonUIConstants.PAGE_CommonAdminUIC + "?event=viewHelpText&helpId=" + helpPageVO.getHelpId() + "\" target=\"_blank\">" + "?" + "</a>";
	    }
	} catch (Exception ex) {
	    // no help available
	    LogUtil.warn("HelpHelper", "Unable to display help icon. " + ex.getMessage());
	    // ignore
	}

	LogUtil.log("displayHelpIcon", "helpIconHTML=" + helpIconHTML);

	return helpIconHTML;
    }
}
