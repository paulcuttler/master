package com.ract.help;

import java.io.Serializable;

/**
 * <p>
 * Represent a help page.
 * </p>
 * 
 * @hibernate.class lazy="false"
 * 
 * @author jyh
 * @version 1.0
 */
public class HelpPage implements Serializable {
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((helpId == null) ? 0 : helpId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	HelpPage other = (HelpPage) obj;
	if (helpId == null) {
	    if (other.helpId != null)
		return false;
	} else if (!helpId.equals(other.helpId))
	    return false;
	return true;
    }

    /**
     * @hibernate.property
     */
    public String getHelpPageReference() {
	return helpPageReference;
    }

    /**
     * @hibernate.id generator-class="assigned"
     */
    public Integer getHelpId() {
	return helpId;
    }

    public void setHelpId(Integer helpId) {
	this.helpId = helpId;
    }

    private Integer helpId;

    private String helpPageReference;

    private String helpCategory;

    private String helpHeading;

    private String helpText;

    /**
     * @hibernate.property
     */
    public String getHelpCategory() {
	return helpCategory;
    }

    public void setHelpCategory(String helpCategory) {
	this.helpCategory = helpCategory;
    }

    public void setHelpText(String helpText) {
	this.helpText = helpText;
    }

    public void setHelpHeading(String helpHeading) {
	this.helpHeading = helpHeading;
    }

    public void setHelpPageReference(String helpPageReference) {
	this.helpPageReference = helpPageReference;
    }

    /**
     * @hibernate.property
     */
    public String getHelpText() {
	return helpText;
    }

    /**
     * @hibernate.property
     */
    public String getHelpHeading() {
	return helpHeading;
    }

    public HelpPage() {
    }

    public String toString() {
	StringBuffer helpDescription = new StringBuffer();
	helpDescription.append("helpPageId=" + helpId);
	helpDescription.append(",helpDescription=" + helpDescription);
	helpDescription.append(",helpPageReference=" + helpPageReference);
	helpDescription.append(",helpCategory=" + helpCategory);
	return helpDescription.toString();
    }

}
