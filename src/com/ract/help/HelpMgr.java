package com.ract.help;

import java.rmi.RemoteException;;
import javax.ejb.*;
import java.util.*;
import com.ract.help.*;

@Remote
public interface HelpMgr
{
  public HelpPage getHelpPage(Integer helpId) throws RemoteException;
  public Collection searchHelpPages(String searchTerm) throws RemoteException;
  public HelpPage createHelp(HelpPage helpPage) throws RemoteException;
  public HelpPage updateHelp(HelpPage helpPage) throws RemoteException;
  public HelpPage getHelpPageByPageReference(String helpPageReference) throws RemoteException;
  public Collection getAllHelpPages() throws RemoteException;
}
