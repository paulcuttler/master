package com.ract.vehicleinspection;

/**
 * <p>
 * Title: Master Project
 * </p>
 * <p>
 * Description: Brings all of the projects together into one master project for
 * deployment.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company: RACT
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class VehicleInspectionFactory {
    public static VehicleInspectionAdapter getVehicleInspectionAdapter() {
	return (VehicleInspectionAdapter) new ProgressVehicleInspectionAdapter();
    }
}