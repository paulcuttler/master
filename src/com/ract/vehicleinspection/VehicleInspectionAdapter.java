package com.ract.vehicleinspection;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.ract.common.SystemException;
import com.ract.common.transaction.TransactionAdapter;

/**
 * 
 * <p>
 * Title: VehicleInspectionAdapter
 * </p>
 * <p>
 * Description: Connection interface to vehicle inspection subsystem
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company: RACT
 * </p>
 * 
 * @dgk
 * @version 1.0 21/8/2003
 */
public interface VehicleInspectionAdapter extends TransactionAdapter {
    /**
     * Commit any pending transactions in the vehicle inspection sub system. The
     * connection to the vehicle inspection sub-system will also be released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void commit() throws SystemException;

    /**
     * Rollback any pending transactions in the vehicle inspection sub system.
     * The connection to the vehicle inspection sub-system will also be
     * released.
     * 
     * @exception SystemException
     *                Description of the Exception
     */
    public void rollback() throws SystemException;

    public ArrayList getVIList(Integer clientNumber) throws SystemException;

    /**
     * Release any connections held to the vehicle inspection sub-system. If a
     * transaction is pending and not committed then the transaction will be
     * rolled back.
     */
    public void release();

    /**
     * Notifies the vehicle inspection system of the payment of a premium
     * 
     * @param Integer
     *            receiptNo
     * @param Integer
     *            clientNo ClientNo and sequence no provide the key into the
     *            insurance payable amount records (oi-payable)
     * @param Integer
     *            seqNo
     * @param BigDecimal
     *            amount
     * @param String
     *            logonId
     * @param String
     *            salesBranch
     * @throws SystemException
     */
    public void notifyVehicleInspectionPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch) throws SystemException;

}
