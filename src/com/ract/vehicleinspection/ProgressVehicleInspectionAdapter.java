package com.ract.vehicleinspection;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.ResultSetHolder;
import com.ract.common.SystemException;
import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.common.pool.ProgressProxyObjectPool;
import com.ract.common.transaction.TransactionAdapterType;
import com.ract.util.LogUtil;

/**
 * <p>
 * Vehicle adapter implementation for the Progress system.
 * </p>
 * 
 * @author jyh
 * @version 1.0
 */
public class ProgressVehicleInspectionAdapter extends TransactionAdapterType implements VehicleInspectionAdapter {
    private viTXProxy viTransactionProxy = null;

    private VIProgressProxy transactionVIProgressProxy = null;

    public ProgressVehicleInspectionAdapter() {
    }

    public void commit() throws SystemException {
	if (this.viTransactionProxy != null) {
	    try {
		this.viTransactionProxy.commit();
	    } catch (Open4GLException gle) {
		throw new SystemException(gle);
	    } finally {
		release();
	    }
	}
    }

    public ArrayList getVIList(Integer clientNumber) throws SystemException {
	ArrayList viList = new ArrayList();
	ResultSetHolder viResultSetHolder = new ResultSetHolder();
	String viId = null;
	VIProgressProxy viProgressProxy = null;
	try {
	    viProgressProxy = this.getVIProgressProxy();
	    viProgressProxy.getVIList(clientNumber.intValue(), viResultSetHolder);

	    if (viResultSetHolder != null) {
		try {
		    ResultSet viResultSet = viResultSetHolder.getResultSetValue();
		    while (viResultSet.next()) {
			// get nextt
			viId = viResultSet.getString(1);
			viList.add(viId);
		    }

		} catch (SQLException ex1) {
		    throw new SystemException(ex1);
		}
	    }

	} catch (Open4GLException ex) {
	    throw new SystemException("Unable to get VI list for client '" + clientNumber + "'.", ex);
	} finally {
	    releaseVIProgressProxy(viProgressProxy);
	}

	return viList;
    }

    private final static String SYSTEM_PROGRESS_VI = "ProgressVI";

    public String getSystemName() {
	return SYSTEM_PROGRESS_VI;
    }

    public void rollback() throws SystemException {
	LogUtil.debug(this.getClass(), "Rolling back VI adapter");
	if (this.viTransactionProxy != null) {
	    LogUtil.debug(this.getClass(), "VI rollback()");
	    try {
		this.viTransactionProxy.rollback();
	    } catch (Open4GLException gle) {
		throw new SystemException(gle);
	    } finally {
		LogUtil.debug(this.getClass(), "VI release()");
		release();
	    }
	}
    }

    public void release() {
	LogUtil.debug(this.getClass(), "VI release() end");
	releaseVITransactionProxy();
    }

    private void releaseVITransactionProxy() {
	if (this.viTransactionProxy != null) {
	    try {
		this.viTransactionProxy._release();
	    } catch (Open4GLException gle) {
		LogUtil.fatal(this.getClass(), gle.getMessage());
	    } finally {
		this.viTransactionProxy = null;
	    }
	}
	// release proxy connection controlling transaction
	releaseVIProgressProxy(this.transactionVIProgressProxy);
	this.transactionVIProgressProxy = null;
    }

    public void notifyVehicleInspectionPayment(Integer receiptNo, Integer clientNo, Integer seqNo, BigDecimal amount, String logonId, String salesBranch) throws SystemException {
	try {

	    this.viTransactionProxy = this.getVITransactionProxy();
	    this.viTransactionProxy.notifyPayment(receiptNo.intValue(), clientNo.intValue(), seqNo.intValue(), amount, logonId, salesBranch);
	    handleProxyReturnString();
	} catch (Open4GLException ex) {
	    handleProxyReturnString();
	    throw new SystemException("Unable to notify payment to Vehicle Inspection system.", ex);
	}

    }

    private VIProgressProxy getVIProgressProxy() throws SystemException {
	VIProgressProxy viProgressProxy = null;
	try {
	    viProgressProxy = (VIProgressProxy) ProgressProxyObjectPool.getInstance().borrowObject(ProgressProxyObjectFactory.KEY_VI_PROXY);
	} catch (Exception ex) {
	    throw new SystemException(ex);
	}
	return viProgressProxy;
    }

    private viTXProxy getVITransactionProxy() throws SystemException {
	if (this.viTransactionProxy == null) {
	    transactionVIProgressProxy = this.getVIProgressProxy();
	    try {
		this.viTransactionProxy = transactionVIProgressProxy.createPO_viTXProxy();
	    } catch (Open4GLException ex) {
		ex.printStackTrace();
		throw new SystemException("Unable to get Vehicle Inspection Proxy (viTxProxy) : " + ex);
	    }
	}
	return this.viTransactionProxy;
    }

    private void handleProxyReturnString() throws SystemException {
	try {
	    if (this.viTransactionProxy != null) {
		String returnString = viTransactionProxy._getProcReturnString();
		LogUtil.log(this.getClass(), "returnString=" + returnString);
		if (returnString != null && returnString.length() > 0) {
		    throw new SystemException(returnString);
		}
	    }
	} catch (Open4GLException gle) {
	    throw new SystemException(gle);
	}
    }

    private void releaseVIProgressProxy(VIProgressProxy viProgressProxy) {
	try {
	    ProgressProxyObjectPool.getInstance().returnObject(ProgressProxyObjectFactory.KEY_VI_PROXY, viProgressProxy);
	} catch (Exception ex) {
	    LogUtil.fatal(this.getClass(), ex.getMessage());
	} finally {
	    viProgressProxy = null;
	}

    }

}
